
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoInfoDetallePlanEstudios complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoDetallePlanEstudios">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Asignaturas" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoAsignatura" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoDetallePlanEstudios", propOrder = {
    "asignaturas"
})
public class TipoInfoDetallePlanEstudios {

    @XmlElement(name = "Asignaturas", required = true)
    protected List<TipoAsignatura> asignaturas;

    /**
     * Gets the value of the asignaturas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the asignaturas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAsignaturas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoAsignatura }
     * 
     * 
     */
    public List<TipoAsignatura> getAsignaturas() {
        if (asignaturas == null) {
            asignaturas = new ArrayList<TipoAsignatura>();
        }
        return this.asignaturas;
    }

}
