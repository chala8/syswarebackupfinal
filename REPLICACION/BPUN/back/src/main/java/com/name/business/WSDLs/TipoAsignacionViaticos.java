
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * indica el monto de para la asignacion de viaticos de un empleado
 * 
 * <p>Java class for tipoAsignacionViaticos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoAsignacionViaticos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AsignacionViaticos" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMontoMonetario"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoAsignacionViaticos", propOrder = {
    "asignacionViaticos"
})
public class TipoAsignacionViaticos {

    @XmlElement(name = "AsignacionViaticos", required = true)
    protected TipoMontoMonetario asignacionViaticos;

    /**
     * Gets the value of the asignacionViaticos property.
     * 
     * @return
     *     possible object is
     *     {@link TipoMontoMonetario }
     *     
     */
    public TipoMontoMonetario getAsignacionViaticos() {
        return asignacionViaticos;
    }

    /**
     * Sets the value of the asignacionViaticos property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoMontoMonetario }
     *     
     */
    public void setAsignacionViaticos(TipoMontoMonetario value) {
        this.asignacionViaticos = value;
    }

}
