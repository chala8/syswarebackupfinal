
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Información de una ciudad para IOP
 * 
 * <p>Java class for tipoDep complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoDep">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="departamento" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoDatosDep"/>
 *         &lt;element name="departmentoAnterior" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoDatosDep"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoDep", propOrder = {
    "departamento",
    "departmentoAnterior"
})
public class TipoDep {

    @XmlElement(required = true)
    protected TipoDatosDep departamento;
    @XmlElement(required = true)
    protected TipoDatosDep departmentoAnterior;

    /**
     * Gets the value of the departamento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDatosDep }
     *     
     */
    public TipoDatosDep getDepartamento() {
        return departamento;
    }

    /**
     * Sets the value of the departamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDatosDep }
     *     
     */
    public void setDepartamento(TipoDatosDep value) {
        this.departamento = value;
    }

    /**
     * Gets the value of the departmentoAnterior property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDatosDep }
     *     
     */
    public TipoDatosDep getDepartmentoAnterior() {
        return departmentoAnterior;
    }

    /**
     * Sets the value of the departmentoAnterior property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDatosDep }
     *     
     */
    public void setDepartmentoAnterior(TipoDatosDep value) {
        this.departmentoAnterior = value;
    }

}
