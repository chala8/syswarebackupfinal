
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la información de cuenta bancaria de un tercero, este mensaje contiene:
 * 			la identificación del tercero
 * 			la Nueva cuenta bancaria
 * 			la cuenta bancaria anterior si existe
 * 			
 * 
 * <p>Java class for tipoMsjCuentaBancariaTercero complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjCuentaBancariaTercero">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="idTercero" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdTercero"/>
 *         &lt;element name="InformacionCuentaNueva" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoCuenta"/>
 *         &lt;element name="InformacionCuentaAntigua" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoCuenta"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjCuentaBancariaTercero", propOrder = {
    "idTerceroAndInformacionCuentaNuevaAndInformacionCuentaAntigua"
})
public class TipoMsjCuentaBancariaTercero
    extends TipoMsjGenerico
{

    @XmlElementRefs({
        @XmlElementRef(name = "idTercero", namespace = "http://bpuncloud.unal.edu.co/WS_BPUN/xsd", type = JAXBElement.class),
        @XmlElementRef(name = "InformacionCuentaNueva", namespace = "http://bpuncloud.unal.edu.co/WS_BPUN/xsd", type = JAXBElement.class),
        @XmlElementRef(name = "InformacionCuentaAntigua", namespace = "http://bpuncloud.unal.edu.co/WS_BPUN/xsd", type = JAXBElement.class)
    })
    protected List<JAXBElement<?>> idTerceroAndInformacionCuentaNuevaAndInformacionCuentaAntigua;

    /**
     * Gets the value of the idTerceroAndInformacionCuentaNuevaAndInformacionCuentaAntigua property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idTerceroAndInformacionCuentaNuevaAndInformacionCuentaAntigua property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdTerceroAndInformacionCuentaNuevaAndInformacionCuentaAntigua().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link TipoInfoCuenta }{@code >}
     * {@link JAXBElement }{@code <}{@link TipoIdTercero }{@code >}
     * {@link JAXBElement }{@code <}{@link TipoInfoCuenta }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getIdTerceroAndInformacionCuentaNuevaAndInformacionCuentaAntigua() {
        if (idTerceroAndInformacionCuentaNuevaAndInformacionCuentaAntigua == null) {
            idTerceroAndInformacionCuentaNuevaAndInformacionCuentaAntigua = new ArrayList<JAXBElement<?>>();
        }
        return this.idTerceroAndInformacionCuentaNuevaAndInformacionCuentaAntigua;
    }

}
