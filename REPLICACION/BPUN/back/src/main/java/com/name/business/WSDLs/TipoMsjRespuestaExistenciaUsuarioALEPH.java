
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo de mensaje para consultar la existencia de un usuario en el sistema de información bibliografica ALEPH
 * 
 * <p>Java class for tipoMsjRespuestaExistenciaUsuarioALEPH complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaExistenciaUsuarioALEPH">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjRespuestaExistencia">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaExistenciaUsuarioALEPH")
public class TipoMsjRespuestaExistenciaUsuarioALEPH
    extends TipoMsjRespuestaExistencia
{


}
