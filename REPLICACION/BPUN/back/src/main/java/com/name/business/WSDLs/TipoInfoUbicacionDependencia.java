
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Información ubicacion de una dependencia para IOP
 * 
 * <p>Java class for tipoInfoUbicacionDependencia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoUbicacionDependencia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Edificio" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoEdificio"/>
 *         &lt;element name="Oficina" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoOficina"/>
 *         &lt;element name="ext" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoExtensionTelefonica"/>
 *         &lt;element name="CorreoElectronico" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoCorreoE"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoUbicacionDependencia", propOrder = {
    "edificio",
    "oficina",
    "ext",
    "correoElectronico"
})
public class TipoInfoUbicacionDependencia {

    @XmlElement(name = "Edificio", required = true)
    protected TipoEdificio edificio;
    @XmlElement(name = "Oficina", required = true)
    protected TipoOficina oficina;
    @XmlElement(required = true)
    protected String ext;
    @XmlElement(name = "CorreoElectronico", required = true)
    protected String correoElectronico;

    /**
     * Gets the value of the edificio property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEdificio }
     *     
     */
    public TipoEdificio getEdificio() {
        return edificio;
    }

    /**
     * Sets the value of the edificio property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEdificio }
     *     
     */
    public void setEdificio(TipoEdificio value) {
        this.edificio = value;
    }

    /**
     * Gets the value of the oficina property.
     * 
     * @return
     *     possible object is
     *     {@link TipoOficina }
     *     
     */
    public TipoOficina getOficina() {
        return oficina;
    }

    /**
     * Sets the value of the oficina property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoOficina }
     *     
     */
    public void setOficina(TipoOficina value) {
        this.oficina = value;
    }

    /**
     * Gets the value of the ext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExt() {
        return ext;
    }

    /**
     * Sets the value of the ext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExt(String value) {
        this.ext = value;
    }

    /**
     * Gets the value of the correoElectronico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    /**
     * Sets the value of the correoElectronico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoElectronico(String value) {
        this.correoElectronico = value;
    }

}
