
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de solicitud para la creación de una persona en los sistemas de información HERMES INVESTIGACION Y HERMES EXTENSION
 * 
 * <p>Java class for tipoSolicitudCreacionPersonaHermes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoSolicitudCreacionPersonaHermes">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Persona" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoPersonaHermes"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoSolicitudCreacionPersonaHermes", propOrder = {
    "persona"
})
public class TipoSolicitudCreacionPersonaHermes
    extends TipoMsjGenerico
{

    @XmlElement(name = "Persona", required = true)
    protected List<TipoPersonaHermes> persona;

    /**
     * Gets the value of the persona property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the persona property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPersona().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoPersonaHermes }
     * 
     * 
     */
    public List<TipoPersonaHermes> getPersona() {
        if (persona == null) {
            persona = new ArrayList<TipoPersonaHermes>();
        }
        return this.persona;
    }

}
