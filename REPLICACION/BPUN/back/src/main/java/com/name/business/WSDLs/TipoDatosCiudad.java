
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Información de una ciudad para IOP
 * 
 * <p>Java class for tipoDatosCiudad complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoDatosCiudad">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nombreCiudad" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNombreCiudad"/>
 *         &lt;element name="codigoCiudad" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdCiudad"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoDatosCiudad", propOrder = {
    "nombreCiudad",
    "codigoCiudad"
})
public class TipoDatosCiudad {

    @XmlElement(required = true)
    protected String nombreCiudad;
    @XmlElement(required = true)
    protected String codigoCiudad;

    /**
     * Gets the value of the nombreCiudad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCiudad() {
        return nombreCiudad;
    }

    /**
     * Sets the value of the nombreCiudad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCiudad(String value) {
        this.nombreCiudad = value;
    }

    /**
     * Gets the value of the codigoCiudad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCiudad() {
        return codigoCiudad;
    }

    /**
     * Sets the value of the codigoCiudad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCiudad(String value) {
        this.codigoCiudad = value;
    }

}
