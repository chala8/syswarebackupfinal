
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Respuesta de la información complementaria de un empleado de la UN para IOP
 * 
 * <p>Java class for tipoMsjRespuestaInfoComplementaria complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaInfoComplementaria">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="InformacionComplementaria" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoComplementaria"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaInfoComplementaria", propOrder = {
    "informacionComplementaria"
})
public class TipoMsjRespuestaInfoComplementaria
    extends TipoMsjGenerico
{

    @XmlElement(name = "InformacionComplementaria", required = true)
    protected List<TipoInfoComplementaria> informacionComplementaria;

    /**
     * Gets the value of the informacionComplementaria property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the informacionComplementaria property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInformacionComplementaria().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoComplementaria }
     * 
     * 
     */
    public List<TipoInfoComplementaria> getInformacionComplementaria() {
        if (informacionComplementaria == null) {
            informacionComplementaria = new ArrayList<TipoInfoComplementaria>();
        }
        return this.informacionComplementaria;
    }

}
