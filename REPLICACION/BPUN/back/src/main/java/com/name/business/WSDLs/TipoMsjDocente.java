
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjDocente complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjDocente">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="docente" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoDocente"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjDocente", propOrder = {
    "docente"
})
public class TipoMsjDocente
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected TipoDocente docente;

    /**
     * Gets the value of the docente property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocente }
     *     
     */
    public TipoDocente getDocente() {
        return docente;
    }

    /**
     * Sets the value of the docente property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocente }
     *     
     */
    public void setDocente(TipoDocente value) {
        this.docente = value;
    }

}
