
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Información de un centro de costos para IOP
 * 
 * <p>Java class for tipoCentroDeCostos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoCentroDeCostos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ZonaEconómica" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoCodigoZonaEconómica"/>
 *         &lt;element name="Nombre" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNombreCentroDeCostos"/>
 *         &lt;element name="Codigo" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoCodigoCentroDeCostos"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoCentroDeCostos", propOrder = {
    "zonaEcon\u00f3mica",
    "nombre",
    "codigo"
})
public class TipoCentroDeCostos {

    @XmlElement(name = "ZonaEcon\u00f3mica", required = true)
    protected String zonaEconómica;
    @XmlElement(name = "Nombre", required = true)
    protected String nombre;
    @XmlElement(name = "Codigo", required = true)
    protected String codigo;

    /**
     * Gets the value of the zonaEconómica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZonaEconómica() {
        return zonaEconómica;
    }

    /**
     * Sets the value of the zonaEconómica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZonaEconómica(String value) {
        this.zonaEconómica = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

}
