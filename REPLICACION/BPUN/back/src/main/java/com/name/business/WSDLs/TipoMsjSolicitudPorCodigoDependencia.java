
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la solicitud de un tercero por su id para interop
 * 
 * <p>Java class for tipoMsjSolicitudPorCodigoDependencia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudPorCodigoDependencia">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="CodigoDependencia" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoDependencia"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudPorCodigoDependencia", propOrder = {
    "codigoDependencia"
})
public class TipoMsjSolicitudPorCodigoDependencia
    extends TipoMsjGenerico
{

    @XmlElement(name = "CodigoDependencia", required = true)
    protected String codigoDependencia;

    /**
     * Gets the value of the codigoDependencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDependencia() {
        return codigoDependencia;
    }

    /**
     * Sets the value of the codigoDependencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDependencia(String value) {
        this.codigoDependencia = value;
    }

}
