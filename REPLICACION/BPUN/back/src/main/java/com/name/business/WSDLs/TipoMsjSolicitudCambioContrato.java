
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de solicitud para e cambio de contrato en el sistem de infomación SIBU
 * 
 * <p>Java class for tipoMsjSolicitudCambioContrato complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudCambioContrato">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="Contrato" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoContrato"/>
 *         &lt;element name="ContratoAnterior" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoContrato"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudCambioContrato", propOrder = {
    "contrato",
    "contratoAnterior"
})
public class TipoMsjSolicitudCambioContrato
    extends TipoMsjGenerico
{

    @XmlElement(name = "Contrato", required = true)
    protected TipoContrato contrato;
    @XmlElement(name = "ContratoAnterior", required = true)
    protected TipoContrato contratoAnterior;

    /**
     * Gets the value of the contrato property.
     * 
     * @return
     *     possible object is
     *     {@link TipoContrato }
     *     
     */
    public TipoContrato getContrato() {
        return contrato;
    }

    /**
     * Sets the value of the contrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoContrato }
     *     
     */
    public void setContrato(TipoContrato value) {
        this.contrato = value;
    }

    /**
     * Gets the value of the contratoAnterior property.
     * 
     * @return
     *     possible object is
     *     {@link TipoContrato }
     *     
     */
    public TipoContrato getContratoAnterior() {
        return contratoAnterior;
    }

    /**
     * Sets the value of the contratoAnterior property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoContrato }
     *     
     */
    public void setContratoAnterior(TipoContrato value) {
        this.contratoAnterior = value;
    }

}
