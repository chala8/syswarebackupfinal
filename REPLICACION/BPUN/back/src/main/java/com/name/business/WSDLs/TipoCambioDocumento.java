
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * indica el tipo de documento y el numero que identifica a un tercero para interop
 * 
 * <p>Java class for tipoCambioDocumento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoCambioDocumento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipoDocumento" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoTipoDocumento"/>
 *         &lt;element name="numeroDocumento" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNumeroDocumento"/>
 *         &lt;element name="tipoDocumentoAnterior" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoTipoDocumento"/>
 *         &lt;element name="numeroDocumentoAnterior" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNumeroDocumento"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoCambioDocumento", propOrder = {
    "tipoDocumento",
    "numeroDocumento",
    "tipoDocumentoAnterior",
    "numeroDocumentoAnterior"
})
public class TipoCambioDocumento {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoTipoDocumento tipoDocumento;
    @XmlElement(required = true)
    protected String numeroDocumento;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoTipoDocumento tipoDocumentoAnterior;
    @XmlElement(required = true)
    protected String numeroDocumentoAnterior;

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoTipoDocumento }
     *     
     */
    public TipoTipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTipoDocumento }
     *     
     */
    public void setTipoDocumento(TipoTipoDocumento value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the numeroDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    /**
     * Sets the value of the numeroDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroDocumento(String value) {
        this.numeroDocumento = value;
    }

    /**
     * Gets the value of the tipoDocumentoAnterior property.
     * 
     * @return
     *     possible object is
     *     {@link TipoTipoDocumento }
     *     
     */
    public TipoTipoDocumento getTipoDocumentoAnterior() {
        return tipoDocumentoAnterior;
    }

    /**
     * Sets the value of the tipoDocumentoAnterior property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTipoDocumento }
     *     
     */
    public void setTipoDocumentoAnterior(TipoTipoDocumento value) {
        this.tipoDocumentoAnterior = value;
    }

    /**
     * Gets the value of the numeroDocumentoAnterior property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroDocumentoAnterior() {
        return numeroDocumentoAnterior;
    }

    /**
     * Sets the value of the numeroDocumentoAnterior property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroDocumentoAnterior(String value) {
        this.numeroDocumentoAnterior = value;
    }

}
