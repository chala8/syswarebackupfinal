
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para dar la respuesta de la existencia o no de una entidad para interop
 * 
 * <p>Java class for tipoMsjRespuestaViaticos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaViaticos">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="RespuestaViaticos" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoAsignacionViaticos"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaViaticos", propOrder = {
    "respuestaViaticos"
})
public class TipoMsjRespuestaViaticos
    extends TipoMsjGenerico
{

    @XmlElement(name = "RespuestaViaticos", required = true)
    protected TipoAsignacionViaticos respuestaViaticos;

    /**
     * Gets the value of the respuestaViaticos property.
     * 
     * @return
     *     possible object is
     *     {@link TipoAsignacionViaticos }
     *     
     */
    public TipoAsignacionViaticos getRespuestaViaticos() {
        return respuestaViaticos;
    }

    /**
     * Sets the value of the respuestaViaticos property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoAsignacionViaticos }
     *     
     */
    public void setRespuestaViaticos(TipoAsignacionViaticos value) {
        this.respuestaViaticos = value;
    }

}
