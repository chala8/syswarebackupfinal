
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for tipoContratoTercero complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoContratoTercero">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoContrato" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoCodigoContrato"/>
 *         &lt;element name="FechaInicioContrato" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoFechaInicioContrato"/>
 *         &lt;element name="FechaFinContrato" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipofechaFinContrato"/>
 *         &lt;element name="TipoContrato" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoTipoContrato"/>
 *         &lt;element name="Vigencia" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoVigenciaContrato"/>
 *         &lt;element name="Tercero" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoTercero"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoContratoTercero", propOrder = {
    "codigoContrato",
    "fechaInicioContrato",
    "fechaFinContrato",
    "tipoContrato",
    "vigencia",
    "tercero"
})
public class TipoContratoTercero {

    @XmlElement(name = "CodigoContrato", required = true)
    protected String codigoContrato;
    @XmlElement(name = "FechaInicioContrato", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaInicioContrato;
    @XmlElement(name = "FechaFinContrato", required = true)
    protected String fechaFinContrato;
    @XmlElement(name = "TipoContrato", required = true)
    protected String tipoContrato;
    @XmlElement(name = "Vigencia", required = true)
    protected String vigencia;
    @XmlElement(name = "Tercero", required = true)
    protected TipoTercero tercero;

    /**
     * Gets the value of the codigoContrato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoContrato() {
        return codigoContrato;
    }

    /**
     * Sets the value of the codigoContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoContrato(String value) {
        this.codigoContrato = value;
    }

    /**
     * Gets the value of the fechaInicioContrato property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaInicioContrato() {
        return fechaInicioContrato;
    }

    /**
     * Sets the value of the fechaInicioContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaInicioContrato(XMLGregorianCalendar value) {
        this.fechaInicioContrato = value;
    }

    /**
     * Gets the value of the fechaFinContrato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaFinContrato() {
        return fechaFinContrato;
    }

    /**
     * Sets the value of the fechaFinContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaFinContrato(String value) {
        this.fechaFinContrato = value;
    }

    /**
     * Gets the value of the tipoContrato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoContrato() {
        return tipoContrato;
    }

    /**
     * Sets the value of the tipoContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoContrato(String value) {
        this.tipoContrato = value;
    }

    /**
     * Gets the value of the vigencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVigencia() {
        return vigencia;
    }

    /**
     * Sets the value of the vigencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVigencia(String value) {
        this.vigencia = value;
    }

    /**
     * Gets the value of the tercero property.
     * 
     * @return
     *     possible object is
     *     {@link TipoTercero }
     *     
     */
    public TipoTercero getTercero() {
        return tercero;
    }

    /**
     * Sets the value of the tercero property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTercero }
     *     
     */
    public void setTercero(TipoTercero value) {
        this.tercero = value;
    }

}
