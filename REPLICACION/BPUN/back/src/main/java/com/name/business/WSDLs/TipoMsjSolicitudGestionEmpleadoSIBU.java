
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de solicitud para la creación de una persona en el sistem de infomación SIBU
 * 
 * <p>Java class for tipoMsjSolicitudGestionEmpleadoSIBU complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudGestionEmpleadoSIBU">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="informacionBasicaPersona" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoEmpleado"/>
 *         &lt;element name="informacionComplementariaPersona" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoComplementaria"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudGestionEmpleadoSIBU", propOrder = {
    "informacionBasicaPersonaAndInformacionComplementariaPersona"
})
public class TipoMsjSolicitudGestionEmpleadoSIBU
    extends TipoMsjGenerico
{

    @XmlElements({
        @XmlElement(name = "informacionBasicaPersona", required = true, type = TipoEmpleado.class),
        @XmlElement(name = "informacionComplementariaPersona", required = true, type = TipoInfoComplementaria.class)
    })
    protected List<Object> informacionBasicaPersonaAndInformacionComplementariaPersona;

    /**
     * Gets the value of the informacionBasicaPersonaAndInformacionComplementariaPersona property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the informacionBasicaPersonaAndInformacionComplementariaPersona property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInformacionBasicaPersonaAndInformacionComplementariaPersona().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoEmpleado }
     * {@link TipoInfoComplementaria }
     * 
     * 
     */
    public List<Object> getInformacionBasicaPersonaAndInformacionComplementariaPersona() {
        if (informacionBasicaPersonaAndInformacionComplementariaPersona == null) {
            informacionBasicaPersonaAndInformacionComplementariaPersona = new ArrayList<Object>();
        }
        return this.informacionBasicaPersonaAndInformacionComplementariaPersona;
    }

}
