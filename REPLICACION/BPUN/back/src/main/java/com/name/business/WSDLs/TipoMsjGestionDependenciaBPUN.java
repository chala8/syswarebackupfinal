
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la respuesta de una dependencia para interop
 * 
 * <p>Java class for tipoMsjGestionDependenciaBPUN complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjGestionDependenciaBPUN">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="Dependencia" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoDetalleDependencia"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjGestionDependenciaBPUN", propOrder = {
    "dependencia"
})
public class TipoMsjGestionDependenciaBPUN
    extends TipoMsjGenerico
{

    @XmlElement(name = "Dependencia", required = true)
    protected TipoInfoDetalleDependencia dependencia;

    /**
     * Gets the value of the dependencia property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoDetalleDependencia }
     *     
     */
    public TipoInfoDetalleDependencia getDependencia() {
        return dependencia;
    }

    /**
     * Sets the value of the dependencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoDetalleDependencia }
     *     
     */
    public void setDependencia(TipoInfoDetalleDependencia value) {
        this.dependencia = value;
    }

}
