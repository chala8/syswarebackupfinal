
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * indica la informacion basica de persona para proyecto BPUN
 * 
 * <p>Java class for tipoInfoBasicaPersonaProyectos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoBasicaPersonaProyectos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTercero" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdTercero"/>
 *         &lt;element name="nombrePersonaNatural" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNombrePersonaNatural"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoBasicaPersonaProyectos", propOrder = {
    "idTercero",
    "nombrePersonaNatural"
})
public class TipoInfoBasicaPersonaProyectos {

    @XmlElement(required = true)
    protected TipoIdTercero idTercero;
    @XmlElement(required = true)
    protected TipoNombrePersonaNatural nombrePersonaNatural;

    /**
     * Gets the value of the idTercero property.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getIdTercero() {
        return idTercero;
    }

    /**
     * Sets the value of the idTercero property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setIdTercero(TipoIdTercero value) {
        this.idTercero = value;
    }

    /**
     * Gets the value of the nombrePersonaNatural property.
     * 
     * @return
     *     possible object is
     *     {@link TipoNombrePersonaNatural }
     *     
     */
    public TipoNombrePersonaNatural getNombrePersonaNatural() {
        return nombrePersonaNatural;
    }

    /**
     * Sets the value of the nombrePersonaNatural property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoNombrePersonaNatural }
     *     
     */
    public void setNombrePersonaNatural(TipoNombrePersonaNatural value) {
        this.nombrePersonaNatural = value;
    }

}
