
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoRollProyecto.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoRollProyecto">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Director"/>
 *     &lt;enumeration value="Participante"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoRollProyecto")
@XmlEnum
public enum TipoRollProyecto {


    /**
     * Director de un proyecto en la universidad nacional de colombia
     * 
     */
    @XmlEnumValue("Director")
    DIRECTOR("Director"),

    /**
     * Participante de un proyecto en la universidad nacional de colombia
     * 
     */
    @XmlEnumValue("Participante")
    PARTICIPANTE("Participante");
    private final String value;

    TipoRollProyecto(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoRollProyecto fromValue(String v) {
        for (TipoRollProyecto c: TipoRollProyecto.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
