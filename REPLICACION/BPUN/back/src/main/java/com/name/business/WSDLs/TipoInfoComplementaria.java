
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Información complementaria de un empleado para interoperabilidad
 * 
 * <p>Java class for tipoInfoComplementaria complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoComplementaria">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CiudadDeExpedicionID" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdCiudad"/>
 *         &lt;element name="NumeroCelular" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNumeroCelular"/>
 *         &lt;element name="FondoDePension" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoFondoPension"/>
 *         &lt;element name="EPS" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoEPS"/>
 *         &lt;element name="GrupoSanguineo" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoRH"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoComplementaria", propOrder = {
    "ciudadDeExpedicionID",
    "numeroCelular",
    "fondoDePension",
    "eps",
    "grupoSanguineo"
})
public class TipoInfoComplementaria {

    @XmlElement(name = "CiudadDeExpedicionID", required = true)
    protected String ciudadDeExpedicionID;
    @XmlElement(name = "NumeroCelular", required = true)
    protected String numeroCelular;
    @XmlElement(name = "FondoDePension", required = true)
    protected String fondoDePension;
    @XmlElement(name = "EPS", required = true)
    protected String eps;
    @XmlElement(name = "GrupoSanguineo", required = true)
    protected String grupoSanguineo;

    /**
     * Gets the value of the ciudadDeExpedicionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadDeExpedicionID() {
        return ciudadDeExpedicionID;
    }

    /**
     * Sets the value of the ciudadDeExpedicionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadDeExpedicionID(String value) {
        this.ciudadDeExpedicionID = value;
    }

    /**
     * Gets the value of the numeroCelular property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCelular() {
        return numeroCelular;
    }

    /**
     * Sets the value of the numeroCelular property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCelular(String value) {
        this.numeroCelular = value;
    }

    /**
     * Gets the value of the fondoDePension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFondoDePension() {
        return fondoDePension;
    }

    /**
     * Sets the value of the fondoDePension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFondoDePension(String value) {
        this.fondoDePension = value;
    }

    /**
     * Gets the value of the eps property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEPS() {
        return eps;
    }

    /**
     * Sets the value of the eps property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEPS(String value) {
        this.eps = value;
    }

    /**
     * Gets the value of the grupoSanguineo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrupoSanguineo() {
        return grupoSanguineo;
    }

    /**
     * Sets the value of the grupoSanguineo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrupoSanguineo(String value) {
        this.grupoSanguineo = value;
    }

}
