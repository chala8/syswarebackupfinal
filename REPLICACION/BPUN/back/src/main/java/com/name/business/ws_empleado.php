<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<wsdl:definitions
	xmlns:EndPoint="https://bpuncloud.unal.edu.co/WS_BPUN/ws_empleado.php"
	xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/"
	xmlns:iopun="https://bpuncloud.unal.edu.co/WS_BPUN/xsd"
	name="GestionEmpleadoBPUN" targetNamespace="https://bpuncloud.unal.edu.co/WS_BPUN/ws_empleado.php"
	xmlns:tns1="https://bpuncloud.unal.edu.co/WS_BPUN/xsd"
	xmlns="http://schemas.xmlsoap.org/wsdl/" xmlns:p="http://schemas.xmlsoap.org/wsdl/soap/">	
    <wsdl:types>
		<xsd:schema targetNamespace="https://bpuncloud.unal.edu.co/WS_BPUN/ws_empleado.php">
			<xsd:element name="GestionEmpleadoBPUN">
				<xsd:complexType>
				  <xsd:sequence>
					<xsd:element name="in" type="xsd:string"/>
				  </xsd:sequence>
				</xsd:complexType>
			</xsd:element>
			<xsd:element name="GestionEmpleadoBPUNResponse">
				<xsd:complexType>
					<xsd:sequence>
						<xsd:element name="out" type="xsd:string"/>
					</xsd:sequence>
				</xsd:complexType>
			</xsd:element>
		</xsd:schema>

		<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">
			<xsd:import
				namespace="https://bpuncloud.unal.edu.co/WS_BPUN/xsd"
				schemaLocation="https://bpuncloud.unal.edu.co/WS_BPUN/xsd/EsquemasElementos.xsd">
			</xsd:import>
		</xsd:schema>
	</wsdl:types>


	<wsdl:message name="GestionEmpleadoBPUNRequest">
		<wsdl:part element="iopun:elemSolicitudGestionPersonaBPUN" name="elemGestionarPersona"/>
	</wsdl:message>

	<wsdl:message name="GestionEmpleadoBPUNResponse">
		<wsdl:part name="elemRespuestaOp"
			element="iopun:elemRespuestaGestionEmpleadoBPUN">
		</wsdl:part>
	</wsdl:message>

	<wsdl:message name="GestionEmpleadoBPUNFault">

	</wsdl:message>

	<wsdl:portType name="GestionEmpleadoBPUN">
		<wsdl:operation name="GestionEmpleadoBPUN">
			<wsdl:input message="EndPoint:GestionEmpleadoBPUNRequest"/>
			<wsdl:output message="EndPoint:GestionEmpleadoBPUNResponse"/>
        </wsdl:operation>
	</wsdl:portType>

	<wsdl:binding name="GestionEmpleadoBPUNSOAP" type="EndPoint:GestionEmpleadoBPUN">
		<soap:binding style="document" transport="http://schemas.xmlsoap.org/soap/http"/>
		<wsdl:operation name="GestionEmpleadoBPUN">
			<soap:operation soapAction="https://bpuncloud.unal.edu.co/WS_BPUN/ws_empleado.php"/>
			<wsdl:input>
				<soap:body use="literal"/>
			</wsdl:input>
			<wsdl:output>
				<soap:body use="literal"/>
			</wsdl:output>
		</wsdl:operation>
	</wsdl:binding>

	<wsdl:service name="GestionEmpleadoBPUN">
		<wsdl:port binding="EndPoint:GestionEmpleadoBPUNSOAP" name="EndPoint">
			<soap:address location="https://bpuncloud.unal.edu.co/WS_BPUN/ws_empleado.php"/>
		</wsdl:port>
	</wsdl:service>
</wsdl:definitions>
