
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Información de un participante de proyectos de inversion para la universidad nacional de colombia
 * 
 * <p>Java class for tipoPersonaBPUN complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoPersonaBPUN">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InformacionBasica" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoBasicaPersonaProyectos"/>
 *         &lt;element name="InfoContacto" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoContacto"/>
 *         &lt;sequence>
 *           &lt;element name="InfoVinculación">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;choice>
 *                     &lt;element name="VinculacionPlantal" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoContrato"/>
 *                     &lt;element name="Externos" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoTipoVinculacion"/>
 *                   &lt;/choice>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoPersonaBPUN", propOrder = {
    "informacionBasica",
    "infoContacto",
    "infoVinculación"
})
public class TipoPersonaBPUN {

    @XmlElement(name = "InformacionBasica", required = true)
    protected TipoInfoBasicaPersonaProyectos informacionBasica;
    @XmlElement(name = "InfoContacto", required = true)
    protected TipoInfoContacto infoContacto;
    @XmlElement(name = "InfoVinculaci\u00f3n", required = true)
    protected TipoPersonaBPUN.InfoVinculación infoVinculación;

    /**
     * Gets the value of the informacionBasica property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPersonaProyectos }
     *     
     */
    public TipoInfoBasicaPersonaProyectos getInformacionBasica() {
        return informacionBasica;
    }

    /**
     * Sets the value of the informacionBasica property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPersonaProyectos }
     *     
     */
    public void setInformacionBasica(TipoInfoBasicaPersonaProyectos value) {
        this.informacionBasica = value;
    }

    /**
     * Gets the value of the infoContacto property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoContacto }
     *     
     */
    public TipoInfoContacto getInfoContacto() {
        return infoContacto;
    }

    /**
     * Sets the value of the infoContacto property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoContacto }
     *     
     */
    public void setInfoContacto(TipoInfoContacto value) {
        this.infoContacto = value;
    }

    /**
     * Gets the value of the infoVinculación property.
     * 
     * @return
     *     possible object is
     *     {@link TipoPersonaBPUN.InfoVinculación }
     *     
     */
    public TipoPersonaBPUN.InfoVinculación getInfoVinculación() {
        return infoVinculación;
    }

    /**
     * Sets the value of the infoVinculación property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPersonaBPUN.InfoVinculación }
     *     
     */
    public void setInfoVinculación(TipoPersonaBPUN.InfoVinculación value) {
        this.infoVinculación = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="VinculacionPlantal" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoContrato"/>
     *         &lt;element name="Externos" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoTipoVinculacion"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vinculacionPlantal",
        "externos"
    })
    public static class InfoVinculación {

        @XmlElement(name = "VinculacionPlantal")
        protected TipoInfoContrato vinculacionPlantal;
        @XmlElement(name = "Externos")
        protected String externos;

        /**
         * Gets the value of the vinculacionPlantal property.
         * 
         * @return
         *     possible object is
         *     {@link TipoInfoContrato }
         *     
         */
        public TipoInfoContrato getVinculacionPlantal() {
            return vinculacionPlantal;
        }

        /**
         * Sets the value of the vinculacionPlantal property.
         * 
         * @param value
         *     allowed object is
         *     {@link TipoInfoContrato }
         *     
         */
        public void setVinculacionPlantal(TipoInfoContrato value) {
            this.vinculacionPlantal = value;
        }

        /**
         * Gets the value of the externos property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExternos() {
            return externos;
        }

        /**
         * Sets the value of the externos property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExternos(String value) {
            this.externos = value;
        }

    }

}
