
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Información Basica de una dependencia para IOP
 * 
 * <p>Java class for tipoInfoBasicaDependencia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoBasicaDependencia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoDependencia" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoDependencia"/>
 *         &lt;element name="NombreDependencia" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNombreDependencia"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoBasicaDependencia", propOrder = {
    "codigoDependencia",
    "nombreDependencia"
})
public class TipoInfoBasicaDependencia {

    @XmlElement(name = "CodigoDependencia", required = true)
    protected String codigoDependencia;
    @XmlElement(name = "NombreDependencia", required = true)
    protected String nombreDependencia;

    /**
     * Gets the value of the codigoDependencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDependencia() {
        return codigoDependencia;
    }

    /**
     * Sets the value of the codigoDependencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDependencia(String value) {
        this.codigoDependencia = value;
    }

    /**
     * Gets the value of the nombreDependencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreDependencia() {
        return nombreDependencia;
    }

    /**
     * Sets the value of the nombreDependencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreDependencia(String value) {
        this.nombreDependencia = value;
    }

}
