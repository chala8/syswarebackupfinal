
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoConceptoPresupuesto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoConceptoPresupuesto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoConcepto" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoCodigoConceptoPresupuesto"/>
 *         &lt;element name="DescripcionConcepto" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoDescripcionConcepto"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoConceptoPresupuesto", propOrder = {
    "codigoConcepto",
    "descripcionConcepto"
})
public class TipoConceptoPresupuesto {

    @XmlElement(name = "CodigoConcepto", required = true)
    protected String codigoConcepto;
    @XmlElement(name = "DescripcionConcepto", required = true)
    protected String descripcionConcepto;

    /**
     * Gets the value of the codigoConcepto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoConcepto() {
        return codigoConcepto;
    }

    /**
     * Sets the value of the codigoConcepto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoConcepto(String value) {
        this.codigoConcepto = value;
    }

    /**
     * Gets the value of the descripcionConcepto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionConcepto() {
        return descripcionConcepto;
    }

    /**
     * Sets the value of the descripcionConcepto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionConcepto(String value) {
        this.descripcionConcepto = value;
    }

}
