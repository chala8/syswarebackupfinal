
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Solicitud de la información complementaria de un empleado de la UN para IOP
 * 
 * <p>Java class for tipoMsjSolicitudInfoComplementaria complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudInfoComplementaria">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="idTercero" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdTercero"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudInfoComplementaria", propOrder = {
    "idTercero"
})
public class TipoMsjSolicitudInfoComplementaria
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected List<TipoIdTercero> idTercero;

    /**
     * Gets the value of the idTercero property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idTercero property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdTercero().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoIdTercero }
     * 
     * 
     */
    public List<TipoIdTercero> getIdTercero() {
        if (idTercero == null) {
            idTercero = new ArrayList<TipoIdTercero>();
        }
        return this.idTercero;
    }

}
