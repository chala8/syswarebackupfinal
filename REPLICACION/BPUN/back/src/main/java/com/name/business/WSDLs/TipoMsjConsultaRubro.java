
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for tipoMsjConsultaRubro complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjConsultaRubro">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="fecha" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoFecha"/>
 *         &lt;element name="Empresa" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoEmpresa"/>
 *         &lt;element name="CodigoArea" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoCodigoAreaResponsabilidad"/>
 *         &lt;element name="CodigoConcepto" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoCodigoConceptoPresupuesto"/>
 *         &lt;element name="CodigoRecurso" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoRecurso"/>
 *         &lt;element name="Proyecto" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoProyecto"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjConsultaRubro", propOrder = {
    "fecha",
    "empresa",
    "codigoArea",
    "codigoConcepto",
    "codigoRecurso",
    "proyecto"
})
public class TipoMsjConsultaRubro
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fecha;
    @XmlElement(name = "Empresa", required = true)
    protected String empresa;
    @XmlElement(name = "CodigoArea", required = true)
    protected String codigoArea;
    @XmlElement(name = "CodigoConcepto", required = true)
    protected String codigoConcepto;
    @XmlElement(name = "CodigoRecurso", required = true)
    protected String codigoRecurso;
    @XmlElement(name = "Proyecto", required = true)
    protected String proyecto;

    /**
     * Gets the value of the fecha property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFecha() {
        return fecha;
    }

    /**
     * Sets the value of the fecha property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFecha(XMLGregorianCalendar value) {
        this.fecha = value;
    }

    /**
     * Gets the value of the empresa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Sets the value of the empresa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpresa(String value) {
        this.empresa = value;
    }

    /**
     * Gets the value of the codigoArea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoArea() {
        return codigoArea;
    }

    /**
     * Sets the value of the codigoArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoArea(String value) {
        this.codigoArea = value;
    }

    /**
     * Gets the value of the codigoConcepto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoConcepto() {
        return codigoConcepto;
    }

    /**
     * Sets the value of the codigoConcepto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoConcepto(String value) {
        this.codigoConcepto = value;
    }

    /**
     * Gets the value of the codigoRecurso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoRecurso() {
        return codigoRecurso;
    }

    /**
     * Sets the value of the codigoRecurso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoRecurso(String value) {
        this.codigoRecurso = value;
    }

    /**
     * Gets the value of the proyecto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProyecto() {
        return proyecto;
    }

    /**
     * Sets the value of the proyecto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProyecto(String value) {
        this.proyecto = value;
    }

}
