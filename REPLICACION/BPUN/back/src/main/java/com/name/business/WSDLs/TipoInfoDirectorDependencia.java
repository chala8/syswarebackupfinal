
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Información del director de una dependencia para IOP
 * 
 * <p>Java class for tipoInfoDirectorDependencia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoDirectorDependencia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdDirector" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdTercero"/>
 *         &lt;element name="NombreDirector" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNombrePersonaNatural"/>
 *         &lt;element name="CorreoElectronico" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoCorreoE"/>
 *         &lt;element name="ext" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoExtensionTelefonica"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoDirectorDependencia", propOrder = {
    "idDirector",
    "nombreDirector",
    "correoElectronico",
    "ext"
})
public class TipoInfoDirectorDependencia {

    @XmlElement(name = "IdDirector", required = true)
    protected TipoIdTercero idDirector;
    @XmlElement(name = "NombreDirector", required = true)
    protected TipoNombrePersonaNatural nombreDirector;
    @XmlElement(name = "CorreoElectronico", required = true)
    protected String correoElectronico;
    @XmlElement(required = true)
    protected String ext;

    /**
     * Gets the value of the idDirector property.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getIdDirector() {
        return idDirector;
    }

    /**
     * Sets the value of the idDirector property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setIdDirector(TipoIdTercero value) {
        this.idDirector = value;
    }

    /**
     * Gets the value of the nombreDirector property.
     * 
     * @return
     *     possible object is
     *     {@link TipoNombrePersonaNatural }
     *     
     */
    public TipoNombrePersonaNatural getNombreDirector() {
        return nombreDirector;
    }

    /**
     * Sets the value of the nombreDirector property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoNombrePersonaNatural }
     *     
     */
    public void setNombreDirector(TipoNombrePersonaNatural value) {
        this.nombreDirector = value;
    }

    /**
     * Gets the value of the correoElectronico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    /**
     * Sets the value of the correoElectronico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoElectronico(String value) {
        this.correoElectronico = value;
    }

    /**
     * Gets the value of the ext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExt() {
        return ext;
    }

    /**
     * Sets the value of the ext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExt(String value) {
        this.ext = value;
    }

}
