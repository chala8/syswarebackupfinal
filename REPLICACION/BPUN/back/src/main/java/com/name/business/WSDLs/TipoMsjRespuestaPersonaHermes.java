
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje que brinda la información de una persona en los sistemas de información HERMES INVESTIGACION Y HERMES EXTENSION
 * 
 * <p>Java class for tipoMsjRespuestaPersonaHermes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaPersonaHermes">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="InfoInvestigacion" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoInvestigacionPersona" maxOccurs="unbounded"/>
 *         &lt;element name="InfoExtension" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoExtensionPersona" maxOccurs="unbounded"/>
 *         &lt;element name="InfoGruposInv" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoGruposInvPersona" maxOccurs="unbounded"/>
 *         &lt;element name="InfoLaboratorios" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoGruposColeccionesPersona" maxOccurs="unbounded"/>
 *         &lt;element name="InfoColecciones" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoLaboratoriosPersona" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaPersonaHermes", propOrder = {
    "infoInvestigacion",
    "infoExtension",
    "infoGruposInv",
    "infoLaboratorios",
    "infoColecciones"
})
public class TipoMsjRespuestaPersonaHermes
    extends TipoMsjGenerico
{

    @XmlElement(name = "InfoInvestigacion", required = true)
    protected List<TipoInfoInvestigacionPersona> infoInvestigacion;
    @XmlElement(name = "InfoExtension", required = true)
    protected List<TipoInfoExtensionPersona> infoExtension;
    @XmlElement(name = "InfoGruposInv", required = true)
    protected List<TipoInfoGruposInvPersona> infoGruposInv;
    @XmlElement(name = "InfoLaboratorios", required = true)
    protected List<TipoInfoGruposColeccionesPersona> infoLaboratorios;
    @XmlElement(name = "InfoColecciones", required = true)
    protected List<TipoInfoLaboratoriosPersona> infoColecciones;

    /**
     * Gets the value of the infoInvestigacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infoInvestigacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfoInvestigacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoInvestigacionPersona }
     * 
     * 
     */
    public List<TipoInfoInvestigacionPersona> getInfoInvestigacion() {
        if (infoInvestigacion == null) {
            infoInvestigacion = new ArrayList<TipoInfoInvestigacionPersona>();
        }
        return this.infoInvestigacion;
    }

    /**
     * Gets the value of the infoExtension property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infoExtension property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfoExtension().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoExtensionPersona }
     * 
     * 
     */
    public List<TipoInfoExtensionPersona> getInfoExtension() {
        if (infoExtension == null) {
            infoExtension = new ArrayList<TipoInfoExtensionPersona>();
        }
        return this.infoExtension;
    }

    /**
     * Gets the value of the infoGruposInv property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infoGruposInv property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfoGruposInv().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoGruposInvPersona }
     * 
     * 
     */
    public List<TipoInfoGruposInvPersona> getInfoGruposInv() {
        if (infoGruposInv == null) {
            infoGruposInv = new ArrayList<TipoInfoGruposInvPersona>();
        }
        return this.infoGruposInv;
    }

    /**
     * Gets the value of the infoLaboratorios property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infoLaboratorios property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfoLaboratorios().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoGruposColeccionesPersona }
     * 
     * 
     */
    public List<TipoInfoGruposColeccionesPersona> getInfoLaboratorios() {
        if (infoLaboratorios == null) {
            infoLaboratorios = new ArrayList<TipoInfoGruposColeccionesPersona>();
        }
        return this.infoLaboratorios;
    }

    /**
     * Gets the value of the infoColecciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infoColecciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfoColecciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoLaboratoriosPersona }
     * 
     * 
     */
    public List<TipoInfoLaboratoriosPersona> getInfoColecciones() {
        if (infoColecciones == null) {
            infoColecciones = new ArrayList<TipoInfoLaboratoriosPersona>();
        }
        return this.infoColecciones;
    }

}
