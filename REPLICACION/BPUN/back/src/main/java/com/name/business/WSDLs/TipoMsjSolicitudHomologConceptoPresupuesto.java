
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjSolicitudHomologConceptoPresupuesto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudHomologConceptoPresupuesto">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Interface" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoCodigoInterface"/>
 *         &lt;element name="Empresa" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoEmpresa"/>
 *         &lt;element name="NumeroItem" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNumeroItem"/>
 *         &lt;element name="TipoRegistro" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoTipoRegistro"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudHomologConceptoPresupuesto", propOrder = {
    "interfaceAndEmpresaAndNumeroItem"
})
public class TipoMsjSolicitudHomologConceptoPresupuesto
    extends TipoMsjGenerico
{

    @XmlElementRefs({
        @XmlElementRef(name = "NumeroItem", namespace = "http://bpuncloud.unal.edu.co/WS_BPUN/xsd", type = JAXBElement.class),
        @XmlElementRef(name = "Interface", namespace = "http://bpuncloud.unal.edu.co/WS_BPUN/xsd", type = JAXBElement.class),
        @XmlElementRef(name = "TipoRegistro", namespace = "http://bpuncloud.unal.edu.co/WS_BPUN/xsd", type = JAXBElement.class),
        @XmlElementRef(name = "Empresa", namespace = "http://bpuncloud.unal.edu.co/WS_BPUN/xsd", type = JAXBElement.class)
    })
    protected List<JAXBElement<?>> interfaceAndEmpresaAndNumeroItem;

    /**
     * Gets the value of the interfaceAndEmpresaAndNumeroItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the interfaceAndEmpresaAndNumeroItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInterfaceAndEmpresaAndNumeroItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link TipoTipoRegistro }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getInterfaceAndEmpresaAndNumeroItem() {
        if (interfaceAndEmpresaAndNumeroItem == null) {
            interfaceAndEmpresaAndNumeroItem = new ArrayList<JAXBElement<?>>();
        }
        return this.interfaceAndEmpresaAndNumeroItem;
    }

}
