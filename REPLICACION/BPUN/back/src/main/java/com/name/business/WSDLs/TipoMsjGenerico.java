
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje generico para interop
 * 
 * <p>Java class for tipoMsjGenerico complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjGenerico">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infoMensaje" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoMensaje"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjGenerico", propOrder = {
    "infoMensaje"
})
@XmlSeeAlso({
    TipoMsjRespuestaCabeceraContratos.class,
    TipoSolicitudCreacionPersonaHermes.class,
    TipoMsjTercero.class,
    TipoMsjRespuestaOp.class,
    TipoMsjSolicitudCambioContrato.class,
    TipoMsjSolicitudPorIdTercero.class,
    TipoMsjRespuestaHomologConceptoPresupuesto.class,
    TipoMsjRespuestaCambioContrato.class,
    TipoMsjSolicitudPorCodigoDependencia.class,
    TipoMsjRespuestaEmpleado.class,
    TipoMsjTerceroFinanciero.class,
    TipoMsjRespuestaRubro.class,
    TipoMsjCuentaBancariaTercero.class,
    TipoMsjRespuestaPersonaHermes.class,
    TipoMsjSolicitudGestionPersonaBPUN.class,
    TipoMsjRespuestaViaticos.class,
    TipoMsjRespuestaHomologArea.class,
    TipoMsjSolicitudHomologConceptoPresupuesto.class,
    TipoMsjRespuestaRegistroCuentaNomina.class,
    TipoMsjSolicitudGestionEmpleadoSIBU.class,
    TipoMsjSolicitudCreacionUsuarioALEPH.class,
    TipoMsjSolicitudInfoComplementaria.class,
    TipoMsjrespuestaDetalleDocente.class,
    TipoMsjRespuestaDetalleEstudiante.class,
    TipoMsjRespuestaExistencia.class,
    TipoMsjDetalleContratos.class,
    TipoMsjRespuestaInformaciónGeografica.class,
    TipoMsjRespuestaInfoComplementaria.class,
    TipoMsjRespuestaDependencia.class,
    TipoMsjConsultaRubro.class,
    TipoMsjSolicitudInformacionGeografica.class,
    TipoMsjRespuestaConsultaEstudiante.class,
    TipoMsjRespuestaEstadoNomina.class,
    TipoMsjSolicitudCambioDocumentoSIBU.class,
    TipoMsjSolicitudHomlogArea.class,
    TipoMsjSolicitudActualizacionUsuarioALEPH.class,
    TipoSolicitudActualizacionPersonaHermes.class,
    TipoMsjSolicitudEstadoCargueNomina.class,
    TipoMsjSolicitudPorTipoVinculacion.class,
    TipoMsjGestionDependenciaBPUN.class,
    TipoMsjRespuestaContratos.class,
    TipoMsjRespuestaCambioDocumento.class,
    TipoConsultaPersonaHermes.class,
    TipoMsjRespuestaContratista.class,
    TipoMsjDocente.class,
    TipoMsjRespuestaEstudiante.class,
    TipoMsjSolicitudContrato.class
})
public class TipoMsjGenerico {

    @XmlElement(required = true)
    protected TipoInfoMensaje infoMensaje;

    /**
     * Gets the value of the infoMensaje property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoMensaje }
     *     
     */
    public TipoInfoMensaje getInfoMensaje() {
        return infoMensaje;
    }

    /**
     * Sets the value of the infoMensaje property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoMensaje }
     *     
     */
    public void setInfoMensaje(TipoInfoMensaje value) {
        this.infoMensaje = value;
    }

}
