
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoInfoDetalleAsignatura complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoDetalleAsignatura">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="HorarioDias" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoDia"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoDetalleAsignatura", propOrder = {
    "horarioDias"
})
public class TipoInfoDetalleAsignatura {

    @XmlElement(name = "HorarioDias", required = true)
    protected List<TipoDia> horarioDias;

    /**
     * Gets the value of the horarioDias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the horarioDias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHorarioDias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoDia }
     * 
     * 
     */
    public List<TipoDia> getHorarioDias() {
        if (horarioDias == null) {
            horarioDias = new ArrayList<TipoDia>();
        }
        return this.horarioDias;
    }

}
