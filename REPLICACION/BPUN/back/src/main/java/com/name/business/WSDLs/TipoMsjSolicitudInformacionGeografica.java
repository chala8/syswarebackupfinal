
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la solicitud de infroamción geofrica
 * 
 * <p>Java class for tipoMsjSolicitudInformacionGeografica complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudInformacionGeografica">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="CodigoPais" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdPais"/>
 *         &lt;element name="CodigoDepartamento" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdCiudad"/>
 *         &lt;element name="CodigoCiudad" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdCiudad"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudInformacionGeografica", propOrder = {
    "codigoPais",
    "codigoDepartamento",
    "codigoCiudad"
})
public class TipoMsjSolicitudInformacionGeografica
    extends TipoMsjGenerico
{

    @XmlElement(name = "CodigoPais", required = true)
    protected String codigoPais;
    @XmlElement(name = "CodigoDepartamento", required = true)
    protected String codigoDepartamento;
    @XmlElement(name = "CodigoCiudad", required = true)
    protected String codigoCiudad;

    /**
     * Gets the value of the codigoPais property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPais() {
        return codigoPais;
    }

    /**
     * Sets the value of the codigoPais property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPais(String value) {
        this.codigoPais = value;
    }

    /**
     * Gets the value of the codigoDepartamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDepartamento() {
        return codigoDepartamento;
    }

    /**
     * Sets the value of the codigoDepartamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDepartamento(String value) {
        this.codigoDepartamento = value;
    }

    /**
     * Gets the value of the codigoCiudad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCiudad() {
        return codigoCiudad;
    }

    /**
     * Sets the value of the codigoCiudad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCiudad(String value) {
        this.codigoCiudad = value;
    }

}
