
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Información de un participante de proyectos de investigación o extensión para la universidad nacional de colombia
 * 
 * <p>Java class for tipoPersonaHermes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoPersonaHermes">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InformacionBasica" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoBasicaPersonal"/>
 *         &lt;element name="CiudadDeExpedicionID" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdCiudad"/>
 *         &lt;element name="InformacionTributaria" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoTributariaTercero"/>
 *         &lt;element name="Vinculacion" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoContrato"/>
 *         &lt;element name="Edificio" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoEdificio"/>
 *         &lt;element name="Oficina" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoOficina"/>
 *         &lt;element name="ExtensionTelefonica" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoExtensionTelefonica"/>
 *         &lt;element name="NombreCargo" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNombreCargoEmpleado"/>
 *         &lt;element name="NombreVinculacion" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNombreVinculacionEmpleado"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoPersonaHermes", propOrder = {
    "informacionBasica",
    "ciudadDeExpedicionID",
    "informacionTributaria",
    "vinculacion",
    "edificio",
    "oficina",
    "extensionTelefonica",
    "nombreCargo",
    "nombreVinculacion"
})
public class TipoPersonaHermes {

    @XmlElement(name = "InformacionBasica", required = true)
    protected TipoInfoBasicaPersonal informacionBasica;
    @XmlElement(name = "CiudadDeExpedicionID", required = true)
    protected String ciudadDeExpedicionID;
    @XmlElement(name = "InformacionTributaria", required = true)
    protected TipoInfoTributariaTercero informacionTributaria;
    @XmlElement(name = "Vinculacion", required = true)
    protected TipoInfoContrato vinculacion;
    @XmlElement(name = "Edificio", required = true)
    protected TipoEdificio edificio;
    @XmlElement(name = "Oficina", required = true)
    protected TipoOficina oficina;
    @XmlElement(name = "ExtensionTelefonica", required = true)
    protected String extensionTelefonica;
    @XmlElement(name = "NombreCargo", required = true)
    protected String nombreCargo;
    @XmlElement(name = "NombreVinculacion", required = true)
    protected String nombreVinculacion;

    /**
     * Gets the value of the informacionBasica property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPersonal }
     *     
     */
    public TipoInfoBasicaPersonal getInformacionBasica() {
        return informacionBasica;
    }

    /**
     * Sets the value of the informacionBasica property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPersonal }
     *     
     */
    public void setInformacionBasica(TipoInfoBasicaPersonal value) {
        this.informacionBasica = value;
    }

    /**
     * Gets the value of the ciudadDeExpedicionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadDeExpedicionID() {
        return ciudadDeExpedicionID;
    }

    /**
     * Sets the value of the ciudadDeExpedicionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadDeExpedicionID(String value) {
        this.ciudadDeExpedicionID = value;
    }

    /**
     * Gets the value of the informacionTributaria property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoTributariaTercero }
     *     
     */
    public TipoInfoTributariaTercero getInformacionTributaria() {
        return informacionTributaria;
    }

    /**
     * Sets the value of the informacionTributaria property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoTributariaTercero }
     *     
     */
    public void setInformacionTributaria(TipoInfoTributariaTercero value) {
        this.informacionTributaria = value;
    }

    /**
     * Gets the value of the vinculacion property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoContrato }
     *     
     */
    public TipoInfoContrato getVinculacion() {
        return vinculacion;
    }

    /**
     * Sets the value of the vinculacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoContrato }
     *     
     */
    public void setVinculacion(TipoInfoContrato value) {
        this.vinculacion = value;
    }

    /**
     * Gets the value of the edificio property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEdificio }
     *     
     */
    public TipoEdificio getEdificio() {
        return edificio;
    }

    /**
     * Sets the value of the edificio property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEdificio }
     *     
     */
    public void setEdificio(TipoEdificio value) {
        this.edificio = value;
    }

    /**
     * Gets the value of the oficina property.
     * 
     * @return
     *     possible object is
     *     {@link TipoOficina }
     *     
     */
    public TipoOficina getOficina() {
        return oficina;
    }

    /**
     * Sets the value of the oficina property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoOficina }
     *     
     */
    public void setOficina(TipoOficina value) {
        this.oficina = value;
    }

    /**
     * Gets the value of the extensionTelefonica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtensionTelefonica() {
        return extensionTelefonica;
    }

    /**
     * Sets the value of the extensionTelefonica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtensionTelefonica(String value) {
        this.extensionTelefonica = value;
    }

    /**
     * Gets the value of the nombreCargo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCargo() {
        return nombreCargo;
    }

    /**
     * Sets the value of the nombreCargo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCargo(String value) {
        this.nombreCargo = value;
    }

    /**
     * Gets the value of the nombreVinculacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreVinculacion() {
        return nombreVinculacion;
    }

    /**
     * Sets the value of the nombreVinculacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreVinculacion(String value) {
        this.nombreVinculacion = value;
    }

}
