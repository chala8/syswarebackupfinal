
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo empleado para interop
 * 
 * <p>Java class for tipoEmpleado complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoEmpleado">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoTercero">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Vinculacion" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoContrato"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoEmpleado", propOrder = {
    "vinculacion"
})
@XmlSeeAlso({
    TipoDocente.class,
    TipoUsuarioALEPH.class
})
public class TipoEmpleado
    extends TipoTercero
{

    @XmlElement(name = "Vinculacion", required = true)
    protected List<TipoInfoContrato> vinculacion;

    /**
     * Gets the value of the vinculacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vinculacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVinculacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoContrato }
     * 
     * 
     */
    public List<TipoInfoContrato> getVinculacion() {
        if (vinculacion == null) {
            vinculacion = new ArrayList<TipoInfoContrato>();
        }
        return this.vinculacion;
    }

}
