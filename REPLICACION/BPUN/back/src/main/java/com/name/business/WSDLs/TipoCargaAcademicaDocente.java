
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoCargaAcademicaDocente complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoCargaAcademicaDocente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Asignatura" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoBasicaAsignatura"/>
 *         &lt;element name="NumVecesDictada" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNumVecesAsignaturas"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoCargaAcademicaDocente", propOrder = {
    "asignaturaAndNumVecesDictada"
})
public class TipoCargaAcademicaDocente {

    @XmlElements({
        @XmlElement(name = "Asignatura", required = true, type = TipoInfoBasicaAsignatura.class),
        @XmlElement(name = "NumVecesDictada", required = true, type = Long.class)
    })
    protected List<Object> asignaturaAndNumVecesDictada;

    /**
     * Gets the value of the asignaturaAndNumVecesDictada property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the asignaturaAndNumVecesDictada property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAsignaturaAndNumVecesDictada().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoBasicaAsignatura }
     * {@link Long }
     * 
     * 
     */
    public List<Object> getAsignaturaAndNumVecesDictada() {
        if (asignaturaAndNumVecesDictada == null) {
            asignaturaAndNumVecesDictada = new ArrayList<Object>();
        }
        return this.asignaturaAndNumVecesDictada;
    }

}
