package com.name.business.representations;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class inputData {

    private String idSede;
    private String idSistema;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String tipoDocumento;
    private String numDocumento;
    private String direccionContacto;
    private String ciudadContacto;
    private String telefonoContacto;
    private String correoContacto;
    private String codContrato;
    private String tipoVinculacion;
    private String estadoAdministrativo;
    private String area;
    private String dependencia;
    private String valor;
    private String idMoneda;
    private String dedicacion;
    private String fechaInicio;
    private String cargoEmpleado;
    private String fechaFin;
    private String tipoPeriodo;
    private String externos;
    private String data;

    @JsonCreator
    public inputData(@JsonProperty("idSede") String idSede,
                     @JsonProperty("idSistema") String idSistema,
                     @JsonProperty("primerNombre") String primerNombre,
                     @JsonProperty("segundoNombre") String segundoNombre,
                     @JsonProperty("primerApellido") String primerApellido,
                     @JsonProperty("segundoApellido") String segundoApellido,
                     @JsonProperty("tipoDocumento") String tipoDocumento,
                     @JsonProperty("numDocumento") String numDocumento,
                     @JsonProperty("direccionContacto") String direccionContacto,
                     @JsonProperty("ciudadContacto") String ciudadContacto,
                     @JsonProperty("telefonoContacto") String telefonoContacto,
                     @JsonProperty("correoContacto") String correoContacto,
                     @JsonProperty("codContrato") String codContrato,
                     @JsonProperty("tipoVinculacion") String tipoVinculacion,
                     @JsonProperty("estadoAdministrativo") String estadoAdministrativo,
                     @JsonProperty("area") String area,
                     @JsonProperty("dependencia") String dependencia,
                     @JsonProperty("valor") String valor,
                     @JsonProperty("idMoneda") String idMoneda,
                     @JsonProperty("dedicacion") String dedicacion,
                     @JsonProperty("fechaInicio") String fechaInicio,
                     @JsonProperty("cargoEmpleado") String cargoEmpleado,
                     @JsonProperty("fechaFin") String fechaFin,
                     @JsonProperty("tipoPeriodo") String tipoPeriodo,
                     @JsonProperty("data") String data,
                     @JsonProperty("externos") String externos) {
        this.data = data;
        this.externos = externos;
        this.codContrato = codContrato;
        this.tipoVinculacion = tipoVinculacion;
        this.estadoAdministrativo = estadoAdministrativo;
        this.area = area;
        this.dependencia = dependencia;
        this.valor = valor;
        this.idMoneda = idMoneda;
        this.dedicacion = dedicacion;
        this.fechaInicio = fechaInicio;
        this.cargoEmpleado = cargoEmpleado;
        this.fechaFin = fechaFin;
        this.tipoPeriodo = tipoPeriodo;
        this.idSede = idSede;
        this.idSistema = idSistema;
        this.primerNombre = primerNombre;
        this.segundoNombre = segundoNombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.tipoDocumento = tipoDocumento;
        this.numDocumento = numDocumento;
        this.direccionContacto = direccionContacto;
        this.ciudadContacto = ciudadContacto;
        this.telefonoContacto = telefonoContacto;
        this.correoContacto = correoContacto;
    }

    public String getData() { return data; }

    public void setData(String data) { this.data = data; }

    public String getExternos() {
        return externos;
    }

    public void setExternos(String externos) {
        this.externos = externos;
    }

    public String getCodContrato() {
        return codContrato;
    }

    public void setCodContrato(String codContrato) {
        this.codContrato = codContrato;
    }

    public String getTipoVinculacion() {
        return tipoVinculacion;
    }

    public void setTipoVinculacion(String tipoVinculacion) {
        this.tipoVinculacion = tipoVinculacion;
    }

    public String getEstadoAdministrativo() {
        return estadoAdministrativo;
    }

    public void setEstadoAdministrativo(String estadoAdministrativo) { this.estadoAdministrativo = estadoAdministrativo; }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getDependencia() {
        return dependencia;
    }

    public void setDependencia(String dependencia) {
        this.dependencia = dependencia;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(String idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getDedicacion() {
        return dedicacion;
    }

    public void setDedicacion(String dedicacion) {
        this.dedicacion = dedicacion;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getCargoEmpleado() {
        return cargoEmpleado;
    }

    public void setCargoEmpleado(String cargoEmpleado) {
        this.cargoEmpleado = cargoEmpleado;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getTipoPeriodo() {
        return tipoPeriodo;
    }

    public void setTipoPeriodo(String tipoPeriodo) {
        this.tipoPeriodo = tipoPeriodo;
    }

    public String getIdSede() {
        return idSede;
    }

    public void setIdSede(String idSede) {
        this.idSede = idSede;
    }

    public String getIdSistema() {
        return idSistema;
    }

    public void setIdSistema(String idSistema) {
        this.idSistema = idSistema;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumDocumento() {
        return numDocumento;
    }

    public void setNumDocumento(String numDocumento) {
        this.numDocumento = numDocumento;
    }

    public String getDireccionContacto() {
        return direccionContacto;
    }

    public void setDireccionContacto(String direccionContacto) {
        this.direccionContacto = direccionContacto;
    }

    public String getCiudadContacto() {
        return ciudadContacto;
    }

    public void setCiudadContacto(String ciudadContacto) {
        this.ciudadContacto = ciudadContacto;
    }

    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    public void setTelefonoContacto(String telefonoContacto) {
        this.telefonoContacto = telefonoContacto;
    }

    public String getCorreoContacto() {
        return correoContacto;
    }

    public void setCorreoContacto(String correoContacto) {
        this.correoContacto = correoContacto;
    }





}
