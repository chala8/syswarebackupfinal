
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Información de una ciudad para IOP
 * 
 * <p>Java class for tipoPais complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoPais">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pais" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoDatosPais"/>
 *         &lt;element name="paisAnterior" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoDatosPais"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoPais", propOrder = {
    "pais",
    "paisAnterior"
})
public class TipoPais {

    @XmlElement(required = true)
    protected TipoDatosPais pais;
    @XmlElement(required = true)
    protected TipoDatosPais paisAnterior;

    /**
     * Gets the value of the pais property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDatosPais }
     *     
     */
    public TipoDatosPais getPais() {
        return pais;
    }

    /**
     * Sets the value of the pais property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDatosPais }
     *     
     */
    public void setPais(TipoDatosPais value) {
        this.pais = value;
    }

    /**
     * Gets the value of the paisAnterior property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDatosPais }
     *     
     */
    public TipoDatosPais getPaisAnterior() {
        return paisAnterior;
    }

    /**
     * Sets the value of the paisAnterior property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDatosPais }
     *     
     */
    public void setPaisAnterior(TipoDatosPais value) {
        this.paisAnterior = value;
    }

}
