
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje con la respuesta para las cabeceras de los contratos en interop
 * 
 * <p>Java class for tipoMsjRespuestaCabeceraContratos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaCabeceraContratos">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="elemCabeceraContrato" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoCabeceraContrato"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaCabeceraContratos", propOrder = {
    "elemCabeceraContrato"
})
public class TipoMsjRespuestaCabeceraContratos
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected List<TipoCabeceraContrato> elemCabeceraContrato;

    /**
     * Gets the value of the elemCabeceraContrato property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elemCabeceraContrato property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElemCabeceraContrato().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoCabeceraContrato }
     * 
     * 
     */
    public List<TipoCabeceraContrato> getElemCabeceraContrato() {
        if (elemCabeceraContrato == null) {
            elemCabeceraContrato = new ArrayList<TipoCabeceraContrato>();
        }
        return this.elemCabeceraContrato;
    }

}
