
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de respuesta para el cambio de un documento
 * 
 * <p>Java class for tipoMsjRespuestaCambioDocumento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaCambioDocumento">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="Identificacion" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdTercero"/>
 *         &lt;element name="IdentificacionAnterior" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdTercero"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaCambioDocumento", propOrder = {
    "identificacion",
    "identificacionAnterior"
})
public class TipoMsjRespuestaCambioDocumento
    extends TipoMsjGenerico
{

    @XmlElement(name = "Identificacion", required = true)
    protected TipoIdTercero identificacion;
    @XmlElement(name = "IdentificacionAnterior", required = true)
    protected TipoIdTercero identificacionAnterior;

    /**
     * Gets the value of the identificacion property.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getIdentificacion() {
        return identificacion;
    }

    /**
     * Sets the value of the identificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setIdentificacion(TipoIdTercero value) {
        this.identificacion = value;
    }

    /**
     * Gets the value of the identificacionAnterior property.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getIdentificacionAnterior() {
        return identificacionAnterior;
    }

    /**
     * Sets the value of the identificacionAnterior property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setIdentificacionAnterior(TipoIdTercero value) {
        this.identificacionAnterior = value;
    }

}
