package com.name.business.businesses;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.WSDLs.*;
import com.name.business.entities.TypeDocument;
import com.name.business.representations.inputData;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URL;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.DAOs.*;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import org.apache.poi.ss.formula.functions.T;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.FileOutputStream;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;



public class UNDropBusiness {

    private UNDropDAO billDAO;
    public UNDropBusiness(UNDropDAO billDAO) {
        this.billDAO = billDAO;
    }
/*
    public Either<IException, String> getCC() {
        try {

            List <TypeDocument> ListElement = billDAO.getCC("P");

            System.out.println("List Size: "+ListElement.size());


            for (TypeDocument element:ListElement) {
                GestiongeneralterceroClientEp wsdl = new GestiongeneralterceroClientEp();
                GestionGeneralTerceroPortType instancedWSDL = wsdl.getGestionGeneralTerceroPort();
                TipoMsjSolicitudPorIdTercero objectCC = new TipoMsjSolicitudPorIdTercero();
                TipoInfoMensaje infoMensaje = new TipoInfoMensaje();

                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(new Date());
                XMLGregorianCalendar calendarFinal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);

                infoMensaje.setFechaHora(calendarFinal);
                infoMensaje.setIdSede("01");
                infoMensaje.setIdSistema("100005");

                objectCC.setInfoMensaje(infoMensaje);

                TipoIdTercero idType = new TipoIdTercero();

                idType.setNumeroDocumento(element.getCedula());


                idType.setTipoDocumento(TipoTipoDocumento.CC);

                if(element.getTipoDocumentoSoa()=="TI"){
                    idType.setTipoDocumento(TipoTipoDocumento.TI);
                }

                if(element.getTipoDocumentoSoa()=="CE"){
                    idType.setTipoDocumento(TipoTipoDocumento.CE);
                }

                if(element.getTipoDocumentoSoa()=="PA"){
                    idType.setTipoDocumento(TipoTipoDocumento.PA);
                }

                if(element.getTipoDocumentoSoa()=="RC"){
                    idType.setTipoDocumento(TipoTipoDocumento.RC);
                }

                if(element.getTipoDocumentoSoa()=="NI"){
                    idType.setTipoDocumento(TipoTipoDocumento.NI);
                }

                if(element.getTipoDocumentoSoa()=="IV"){
                    idType.setTipoDocumento(TipoTipoDocumento.IV);
                }



                objectCC.setIdTercero(idType);

                TipoMsjRespuestaOp response = instancedWSDL.opReplicarEmpleado(objectCC);

                billDAO.updateCC(element.getCedula());

                System.out.println(response.getRespuestaOP().getMsjError() + ", " + response.getRespuestaOP().getResultadoOp().value());
            }

            return Either.right("OK");

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }
*/

    public Either<IException, Integer> getCC(inputData input) {
        try {

            String[] data = input.getData().split("º");
            String CodCargo = data[0];
            String CodSede = data[1];
            String CodUnidad = data[2];
            String CodVinculacion = data[3];
            String Email = data[4];
            String NombreCargo = data[5];
            String NombreSede = data[6];
            String NombreUnidad = data[7];
            String Vinculacion = data[8];
            String depa1 = data[9].split("-")[0];
            String depa2 = data[9].split("-")[2];
            String dependenciaArea = depa1+"-"+depa2;

            String Area = CodUnidad+"-"+NombreUnidad;
            String Dependencia = CodVinculacion+"-"+NombreUnidad;
            String CargoEmpleado = CodVinculacion+"-"+Vinculacion;
            System.out.println(CodCargo);
            System.out.println(CodSede);
            System.out.println(CodUnidad);
            System.out.println(CodVinculacion);
            System.out.println(Email);
            System.out.println(NombreCargo);
            System.out.println(NombreSede);
            System.out.println(NombreUnidad);
            System.out.println(Vinculacion);

            System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");

            GestionEmpleadoBPUN_Service wsdl = new GestionEmpleadoBPUN_Service();
            GestionEmpleadoBPUN instancedWSDL = wsdl.getEndPoint();

            TipoMsjSolicitudGestionPersonaBPUN elemSolicitudGestionPersonaBPUN = new TipoMsjSolicitudGestionPersonaBPUN();


            TipoInfoMensaje infomensaje = new TipoInfoMensaje();

            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(new Date());
            XMLGregorianCalendar calendarFinal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);

            infomensaje.setFechaHora(calendarFinal);
            infomensaje.setIdSede(input.getIdSede());
            infomensaje.setIdSistema(input.getIdSistema());

            elemSolicitudGestionPersonaBPUN.setInfoMensaje(infomensaje);



            TipoPersonaBPUN persona = new TipoPersonaBPUN();

            TipoInfoBasicaPersonaProyectos informacionBasica = new TipoInfoBasicaPersonaProyectos();

            TipoNombrePersonaNatural nombrePersonaNatural = new TipoNombrePersonaNatural();
            nombrePersonaNatural.setPrimerNombre(input.getPrimerNombre());
            nombrePersonaNatural.setSegundoNombre(input.getSegundoNombre());
            nombrePersonaNatural.setPrimerApellido(input.getPrimerApellido());
            nombrePersonaNatural.setSegundoApellido(input.getSegundoApellido());
            informacionBasica.setNombrePersonaNatural(nombrePersonaNatural);

            TipoIdTercero idTercero = new TipoIdTercero();

            TipoTipoDocumento doc = TipoTipoDocumento.CC;
            if(input.getTipoDocumento().equals("CE")){
                doc = TipoTipoDocumento.CE;
            }
            if(input.getTipoDocumento().equals("IV")){
                doc = TipoTipoDocumento.IV;
            }
            if(input.getTipoDocumento().equals("NI")){
                doc = TipoTipoDocumento.NI;
            }
            if(input.getTipoDocumento().equals("PA")){
                doc = TipoTipoDocumento.PA;
            }
            if(input.getTipoDocumento().equals("RC")){
                doc = TipoTipoDocumento.RC;
            }
            if(input.getTipoDocumento().equals("TI")){
                doc = TipoTipoDocumento.TI;
            }

            idTercero.setTipoDocumento(doc);
            idTercero.setNumeroDocumento(input.getNumDocumento());
            informacionBasica.setIdTercero(idTercero);
            persona.setInformacionBasica(informacionBasica);


            TipoInfoContacto infoContacto = new TipoInfoContacto();
            infoContacto.setDireccionContacto(input.getDireccionContacto());
            infoContacto.setCiudadContacto(CodSede);
            infoContacto.setTelefonoContacto(input.getTelefonoContacto());
            infoContacto.setCorreoElectronicoContacto(Email);
            persona.setInfoContacto(infoContacto);

            TipoPersonaBPUN.InfoVinculación infoVinculación = new TipoPersonaBPUN.InfoVinculación();


            TipoInfoContrato vinculacionPlanta = new TipoInfoContrato();
            TipoCabeceraContrato cabeceraContrato = new TipoCabeceraContrato();

            cabeceraContrato.setCodContrato(input.getCodContrato());
            cabeceraContrato.setTipoVinculacion(input.getTipoVinculacion());


            TipoEstadoAdministrativo doc2 = TipoEstadoAdministrativo.AUS;
            if(input.getEstadoAdministrativo().equals("ESP")){
                doc2 = TipoEstadoAdministrativo.ESP;
            }
            if(input.getTipoDocumento().equals("CEM")){
                doc2 = TipoEstadoAdministrativo.CEM;
            }
            if(input.getTipoDocumento().equals("MOP")){
                doc2 = TipoEstadoAdministrativo.MOP;
            }
            if(input.getTipoDocumento().equals("NOR")){
                doc2 = TipoEstadoAdministrativo.NOR;
            }
            if(input.getTipoDocumento().equals("RET")){
                doc2 = TipoEstadoAdministrativo.RET;
            }


            cabeceraContrato.setEstadoAdministrativo(doc2);
            cabeceraContrato.setArea(Area);
            cabeceraContrato.setDependencia(dependenciaArea);


            TipoDetalleContrato detalleContrato = new TipoDetalleContrato();

            TipoMontoMonetario asignacionBasica = new TipoMontoMonetario();


            TipoIdMoneda doc3 = TipoIdMoneda.COP;

            asignacionBasica.setValor(new BigDecimal(input.getValor()));
            asignacionBasica.setIdMoneda(doc3);


            detalleContrato.setAsignacionBasica(asignacionBasica);

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            Date date = format.parse(input.getFechaInicio());

            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(date);
            XMLGregorianCalendar xmlGregCal =  DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);

            detalleContrato.setDedicacion(new Double(input.getDedicacion()));
            detalleContrato.setFechaInicio(xmlGregCal);
            detalleContrato.setCargoEmpleado(CargoEmpleado);
            detalleContrato.setFechaFin(input.getFechaInicio());

            detalleContrato.setTipoPeriodo(input.getTipoPeriodo());



            vinculacionPlanta.setDetalleContrato(detalleContrato);
            vinculacionPlanta.setCabeceraContrato(cabeceraContrato);
            infoVinculación.setVinculacionPlantal(vinculacionPlanta);

            String externos = input.getExternos();


            infoVinculación.setExternos(externos);

            persona.setInfoVinculación(infoVinculación);



            elemSolicitudGestionPersonaBPUN.getPersona().add(persona);
            TipoMsjRespuestaOp response = instancedWSDL.gestionEmpleadoBPUN(elemSolicitudGestionPersonaBPUN);





            System.out.println("------------------------RESPUESTA--------------------------");

            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("");

            System.out.println("ID SISTEMA: "+response.getInfoMensaje().getIdSistema());
            System.out.println("ID SEDE: "+response.getInfoMensaje().getIdSede());
            System.out.println("FECHA HORA: "+response.getInfoMensaje().getFechaHora());
            System.out.println("RESULTADO OP: "+response.getRespuestaOP().getResultadoOp());
            System.out.println("ID ERROR: "+response.getRespuestaOP().getIdError());
            System.out.println("MSJ ERROR: "+response.getRespuestaOP().getMsjError());

            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("");

            System.out.println("TEST ITEMS: "+elemSolicitudGestionPersonaBPUN.getPersona().get(0).getInformacionBasica().getNombrePersonaNatural().getPrimerNombre() + ", " + elemSolicitudGestionPersonaBPUN.getPersona().get(0).getInformacionBasica().getNombrePersonaNatural().getPrimerApellido());

            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("");

            return Either.right(1);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }





}