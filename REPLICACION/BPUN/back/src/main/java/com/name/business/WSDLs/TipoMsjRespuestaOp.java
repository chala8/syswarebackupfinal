
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la solicitud de un tercero por su id para interop
 * 
 * <p>Java class for tipoMsjRespuestaOp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaOp">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="respuestaOP" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoRespuestaOp"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaOp", propOrder = {
    "respuestaOP"
})
public class TipoMsjRespuestaOp
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected TipoRespuestaOp respuestaOP;

    /**
     * Gets the value of the respuestaOP property.
     * 
     * @return
     *     possible object is
     *     {@link TipoRespuestaOp }
     *     
     */
    public TipoRespuestaOp getRespuestaOP() {
        return respuestaOP;
    }

    /**
     * Sets the value of the respuestaOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoRespuestaOp }
     *     
     */
    public void setRespuestaOP(TipoRespuestaOp value) {
        this.respuestaOP = value;
    }

}
