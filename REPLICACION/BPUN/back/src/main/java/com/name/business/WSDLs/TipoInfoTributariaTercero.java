
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoInfoTributariaTercero complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoTributariaTercero">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipoTercero" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoTipoTercero"/>
 *         &lt;element name="tipoContribuyente" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoTipoContribuyente"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoTributariaTercero", propOrder = {
    "tipoTercero",
    "tipoContribuyente"
})
public class TipoInfoTributariaTercero {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoTipoTercero tipoTercero;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoTipoContribuyente tipoContribuyente;

    /**
     * Gets the value of the tipoTercero property.
     * 
     * @return
     *     possible object is
     *     {@link TipoTipoTercero }
     *     
     */
    public TipoTipoTercero getTipoTercero() {
        return tipoTercero;
    }

    /**
     * Sets the value of the tipoTercero property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTipoTercero }
     *     
     */
    public void setTipoTercero(TipoTipoTercero value) {
        this.tipoTercero = value;
    }

    /**
     * Gets the value of the tipoContribuyente property.
     * 
     * @return
     *     possible object is
     *     {@link TipoTipoContribuyente }
     *     
     */
    public TipoTipoContribuyente getTipoContribuyente() {
        return tipoContribuyente;
    }

    /**
     * Sets the value of the tipoContribuyente property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTipoContribuyente }
     *     
     */
    public void setTipoContribuyente(TipoTipoContribuyente value) {
        this.tipoContribuyente = value;
    }

}
