
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjDetalleContratos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjDetalleContratos">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="elemDetallesContratos" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoDetalleContrato"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjDetalleContratos", propOrder = {
    "elemDetallesContratos"
})
public class TipoMsjDetalleContratos
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected List<TipoDetalleContrato> elemDetallesContratos;

    /**
     * Gets the value of the elemDetallesContratos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elemDetallesContratos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElemDetallesContratos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoDetalleContrato }
     * 
     * 
     */
    public List<TipoDetalleContrato> getElemDetallesContratos() {
        if (elemDetallesContratos == null) {
            elemDetallesContratos = new ArrayList<TipoDetalleContrato>();
        }
        return this.elemDetallesContratos;
    }

}
