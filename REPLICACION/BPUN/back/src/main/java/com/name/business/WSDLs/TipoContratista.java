
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Información de un investigador para la universidad nacional de colombia
 * 
 * <p>Java class for tipoContratista complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoContratista">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infoBasicaTercero" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoBasicaPersonal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoContratista", propOrder = {
    "infoBasicaTercero"
})
public class TipoContratista {

    @XmlElement(required = true)
    protected TipoInfoBasicaPersonal infoBasicaTercero;

    /**
     * Gets the value of the infoBasicaTercero property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPersonal }
     *     
     */
    public TipoInfoBasicaPersonal getInfoBasicaTercero() {
        return infoBasicaTercero;
    }

    /**
     * Sets the value of the infoBasicaTercero property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPersonal }
     *     
     */
    public void setInfoBasicaTercero(TipoInfoBasicaPersonal value) {
        this.infoBasicaTercero = value;
    }

}
