
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Información detallada de una ciudad para IOP
 * 
 * <p>Java class for tipoInfoCiudad complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoCiudad">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdCiudad" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoIdCiudad"/>
 *         &lt;element name="NombreCiudad" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoNombreCiudad"/>
 *         &lt;element name="idPais" type="{http://bpuncloud.unal.edu.co/WS_BPUN/xsd}tipoInfoPais"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoCiudad", propOrder = {
    "idCiudad",
    "nombreCiudad",
    "idPais"
})
public class TipoInfoCiudad {

    @XmlElement(name = "IdCiudad", required = true)
    protected String idCiudad;
    @XmlElement(name = "NombreCiudad", required = true)
    protected String nombreCiudad;
    @XmlElement(required = true)
    protected TipoInfoPais idPais;

    /**
     * Gets the value of the idCiudad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCiudad() {
        return idCiudad;
    }

    /**
     * Sets the value of the idCiudad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCiudad(String value) {
        this.idCiudad = value;
    }

    /**
     * Gets the value of the nombreCiudad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCiudad() {
        return nombreCiudad;
    }

    /**
     * Sets the value of the nombreCiudad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCiudad(String value) {
        this.nombreCiudad = value;
    }

    /**
     * Gets the value of the idPais property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoPais }
     *     
     */
    public TipoInfoPais getIdPais() {
        return idPais;
    }

    /**
     * Sets the value of the idPais property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoPais }
     *     
     */
    public void setIdPais(TipoInfoPais value) {
        this.idPais = value;
    }

}
