
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjRespuestaDetalleEstudiante complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaDetalleEstudiante">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="PlanEstudios" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoPlanEstudios"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaDetalleEstudiante", propOrder = {
    "planEstudios"
})
public class TipoMsjRespuestaDetalleEstudiante
    extends TipoMsjGenerico
{

    @XmlElement(name = "PlanEstudios", required = true)
    protected List<TipoPlanEstudios> planEstudios;

    /**
     * Gets the value of the planEstudios property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the planEstudios property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlanEstudios().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoPlanEstudios }
     * 
     * 
     */
    public List<TipoPlanEstudios> getPlanEstudios() {
        if (planEstudios == null) {
            planEstudios = new ArrayList<TipoPlanEstudios>();
        }
        return this.planEstudios;
    }

}
