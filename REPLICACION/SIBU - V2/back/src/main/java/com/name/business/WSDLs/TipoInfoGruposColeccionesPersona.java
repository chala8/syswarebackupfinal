
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de un participante de proyectos de investigaci�n o extensi�n para la universidad nacional de colombia
 * 
 * <p>Java class for tipoInfoGruposColeccionesPersona complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoGruposColeccionesPersona">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Colecciones" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoColecciones"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoGruposColeccionesPersona", propOrder = {
    "colecciones"
})
public class TipoInfoGruposColeccionesPersona {

    @XmlElement(name = "Colecciones", required = true)
    protected TipoInfoColecciones colecciones;

    /**
     * Gets the value of the colecciones property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoColecciones }
     *     
     */
    public TipoInfoColecciones getColecciones() {
        return colecciones;
    }

    /**
     * Sets the value of the colecciones property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoColecciones }
     *     
     */
    public void setColecciones(TipoInfoColecciones value) {
        this.colecciones = value;
    }

}
