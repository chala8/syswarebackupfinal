
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la respuesta de iformaci�n geografica
 * 
 * <p>Java class for tipoMsjRespuestaInformacionGeografica complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaInformacionGeografica">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="InformacionGeografica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoGeografica"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaInformacionGeografica", propOrder = {
    "informacionGeografica"
})
public class TipoMsjRespuestaInformacionGeografica
    extends TipoMsjGenerico
{

    @XmlElement(name = "InformacionGeografica", required = true)
    protected TipoInfoGeografica informacionGeografica;

    /**
     * Gets the value of the informacionGeografica property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoGeografica }
     *     
     */
    public TipoInfoGeografica getInformacionGeografica() {
        return informacionGeografica;
    }

    /**
     * Sets the value of the informacionGeografica property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoGeografica }
     *     
     */
    public void setInformacionGeografica(TipoInfoGeografica value) {
        this.informacionGeografica = value;
    }

}
