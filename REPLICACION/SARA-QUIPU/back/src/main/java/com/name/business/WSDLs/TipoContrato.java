
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoContrato complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoContrato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Documento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *         &lt;element name="CodigoContrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoContrato"/>
 *         &lt;element name="TipoPeriodo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoPeriodo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoContrato", propOrder = {
    "documento",
    "codigoContrato",
    "tipoPeriodo"
})
public class TipoContrato {

    @XmlElement(name = "Documento", required = true)
    protected TipoIdTercero documento;
    @XmlElement(name = "CodigoContrato", required = true)
    protected String codigoContrato;
    @XmlElement(name = "TipoPeriodo", required = true)
    protected String tipoPeriodo;

    /**
     * Gets the value of the documento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getDocumento() {
        return documento;
    }

    /**
     * Sets the value of the documento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setDocumento(TipoIdTercero value) {
        this.documento = value;
    }

    /**
     * Gets the value of the codigoContrato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoContrato() {
        return codigoContrato;
    }

    /**
     * Sets the value of the codigoContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoContrato(String value) {
        this.codigoContrato = value;
    }

    /**
     * Gets the value of the tipoPeriodo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoPeriodo() {
        return tipoPeriodo;
    }

    /**
     * Sets the value of the tipoPeriodo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoPeriodo(String value) {
        this.tipoPeriodo = value;
    }

}
