
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * indica la informacion principal un contrato para interop
 * 
 * <p>Java class for tipoCabeceraContrato complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoCabeceraContrato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codContrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodContrato"/>
 *         &lt;element name="tipoVinculacion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoVinculacion"/>
 *         &lt;element name="EstadoAdministrativo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEstadoAdministrativo"/>
 *         &lt;element name="area" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoArea"/>
 *         &lt;element name="Dependencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDependencia"/>
 *         &lt;element name="tipoPeriodo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoPeriodo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoCabeceraContrato", propOrder = {
    "codContrato",
    "tipoVinculacion",
    "estadoAdministrativo",
    "area",
    "dependencia",
    "tipoPeriodo"
})
public class TipoCabeceraContrato {

    @XmlElement(required = true)
    protected String codContrato;
    @XmlElement(required = true)
    protected String tipoVinculacion;
    @XmlElement(name = "EstadoAdministrativo", required = true)
    @XmlSchemaType(name = "string")
    protected TipoEstadoAdministrativo estadoAdministrativo;
    @XmlElement(required = true)
    protected String area;
    @XmlElement(name = "Dependencia", required = true)
    protected String dependencia;
    @XmlElement(required = true)
    protected String tipoPeriodo;

    /**
     * Gets the value of the codContrato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodContrato() {
        return codContrato;
    }

    /**
     * Sets the value of the codContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodContrato(String value) {
        this.codContrato = value;
    }

    /**
     * Gets the value of the tipoVinculacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoVinculacion() {
        return tipoVinculacion;
    }

    /**
     * Sets the value of the tipoVinculacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoVinculacion(String value) {
        this.tipoVinculacion = value;
    }

    /**
     * Gets the value of the estadoAdministrativo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstadoAdministrativo }
     *     
     */
    public TipoEstadoAdministrativo getEstadoAdministrativo() {
        return estadoAdministrativo;
    }

    /**
     * Sets the value of the estadoAdministrativo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstadoAdministrativo }
     *     
     */
    public void setEstadoAdministrativo(TipoEstadoAdministrativo value) {
        this.estadoAdministrativo = value;
    }

    /**
     * Gets the value of the area property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArea() {
        return area;
    }

    /**
     * Sets the value of the area property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArea(String value) {
        this.area = value;
    }

    /**
     * Gets the value of the dependencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependencia() {
        return dependencia;
    }

    /**
     * Sets the value of the dependencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependencia(String value) {
        this.dependencia = value;
    }

    /**
     * Gets the value of the tipoPeriodo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoPeriodo() {
        return tipoPeriodo;
    }

    /**
     * Sets the value of the tipoPeriodo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoPeriodo(String value) {
        this.tipoPeriodo = value;
    }

}
