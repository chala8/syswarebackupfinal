package com.name.business.DAOs;

import com.name.business.entities.TypeDocument;
import com.name.business.mappers.TypeDocMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

public interface UNDropDAO {
    @SqlUpdate("insert into LOG_REPLICACION values (sysdate,:cedula,:sistema,:estado,:mensaje)")
    void insertLog(@Bind("cedula") String cedula,@Bind("sistema") String sistema,@Bind("estado") String estado,@Bind("mensaje") String mensaje);


    @SqlQuery("select codquipu from homol_sara_quipu_empresa where codsara=:codsara")
    List <String> getCodQuipu(@Bind("codsara") String codsara);

}
