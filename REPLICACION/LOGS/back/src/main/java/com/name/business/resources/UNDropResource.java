package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.representations.logData;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.UNDropBusiness;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;
import org.skife.jdbi.v2.sqlobject.Bind;


@Path("/log")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UNDropResource {
    private UNDropBusiness uNDropBusiness;

    public UNDropResource(UNDropBusiness uNDropBusiness) {
        this.uNDropBusiness = uNDropBusiness;
    }

    @POST
    @Path("/insertLog")
    @Timed
    @PermitAll()
    public Response insertLog(logData doc){

        Response response;
        Either<IException, Integer> getResponseGeneric = uNDropBusiness.insertLog(doc.getCedula(), doc.getSistema(), doc.getEstado(), doc.getMensaje());

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/getCodQuipu")
    @Timed
    @PermitAll()
        public Response getCodQuipu(@QueryParam("codsara") String codsara){

        Response response;
        Either<IException, List <String>> getResponseGeneric = uNDropBusiness.getCodQuipu(codsara);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




}