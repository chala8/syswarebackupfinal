package com.name.business.representations;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.skife.jdbi.v2.sqlobject.Bind;

public class logData {

    private String cedula;
    private String sistema;
    private String estado;
    private String mensaje;

    @JsonCreator
    public logData(@JsonProperty("cedula") String cedula,
                   @JsonProperty("sistema") String sistema,
                   @JsonProperty("estado") String estado,
                   @JsonProperty("mensaje") String mensaje) {
        this.cedula = cedula;
        this.sistema = sistema;
        this.estado = estado;
        this.mensaje = mensaje;

    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
