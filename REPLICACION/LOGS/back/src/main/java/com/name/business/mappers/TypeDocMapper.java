package com.name.business.mappers;

import com.name.business.entities.TypeDocument;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TypeDocMapper implements ResultSetMapper<TypeDocument> {
    @Override
    public TypeDocument map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new TypeDocument(
                resultSet.getString("tipoDocumentoSoa"),
                resultSet.getString("cedula")
        );
    }
}