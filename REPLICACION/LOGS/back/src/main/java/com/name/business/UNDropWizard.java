package com.name.business;

import com.name.business.DAOs.UNDropDAO;
import com.name.business.businesses.UNDropBusiness;
import com.name.business.resources.UNDropResource;
import io.dropwizard.Application;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.java8.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.commons.dbcp2.BasicDataSource;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.EnumSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.name.business.utils.constans.K.URI_BASE;


public class UNDropWizard extends Application<UNDropAppConfiguration> {



    public static void main(String[] args) throws Exception  {
        new UNDropWizard().run(args);
    }





    @Override
    public void initialize(Bootstrap<UNDropAppConfiguration> bootstrap) {

        bootstrap.addBundle(new Java8Bundle());
        bootstrap.addBundle(new MultiPartBundle());


    }
    @Override
    public void run(UNDropAppConfiguration configuration, Environment environment) throws Exception {
        environment.jersey().setUrlPattern(URI_BASE+"/*");



        // Se estable el ambiente de coneccion con JDBI con la base de datos de oracle
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "oracle");


        final UNDropDAO closeDAO= jdbi.onDemand(UNDropDAO.class);
        final UNDropBusiness closeBusiness= new UNDropBusiness(closeDAO);



        //EXECUTE REORDER DAILY

        /*
        ZonedDateTime now = ZonedDateTime.now(ZoneId.systemDefault());
        ZonedDateTime nextRun = now.withHour(16).withMinute(7).withSecond(0);
        if(now.compareTo(nextRun) > 0)
            nextRun = nextRun.plusDays(1);

        Duration duration = Duration.between(now, nextRun);
        long initalDelay = duration.getSeconds();

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        Runnable taskWrapper = new Runnable(){

            @Override
            public void run()
            {
                execute(closeBusiness);
            }

        };

        scheduler.scheduleAtFixedRate(taskWrapper,
                initalDelay,
                TimeUnit.DAYS.toSeconds(1),
                TimeUnit.SECONDS);
*/

        CloseDatabaseConfiguration routesDatabaseConfiguration = configuration.getDatabaseConfiguration();
        BasicDataSource datasource = createDataSource(routesDatabaseConfiguration);



        environment.jersey().register(new UNDropResource(closeBusiness));
    }
    /**
     * @Método:  Hace  la configuración para establecer conexión con las bases de datos que se
     * va a utilizar como persistencia del servidor
     * */
    private BasicDataSource createDataSource(CloseDatabaseConfiguration closeDatabaseConfiguration){

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(closeDatabaseConfiguration.getDriverClassName());
        basicDataSource.setUrl(closeDatabaseConfiguration.getUrl());
        basicDataSource.setUsername(closeDatabaseConfiguration.getUsername());
        basicDataSource.setPassword(closeDatabaseConfiguration.getPassword());
        basicDataSource.addConnectionProperty("characterEncoding", "UTF-8");

        basicDataSource.setRemoveAbandonedOnMaintenance(true);
        basicDataSource.setRemoveAbandonedOnBorrow(true);
        basicDataSource.setRemoveAbandonedTimeout(30000);

        return basicDataSource;

    }


    //public void execute(UNDropBusiness a){
        //System.out.println(a.getCC());
    //}



}
