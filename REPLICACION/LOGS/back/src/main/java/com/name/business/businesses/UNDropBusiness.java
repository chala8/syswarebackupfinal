package com.name.business.businesses;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.entities.TypeDocument;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.DAOs.*;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import org.skife.jdbi.v2.sqlobject.Bind;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.FileOutputStream;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
public class UNDropBusiness {

    private UNDropDAO billDAO;
    public UNDropBusiness(UNDropDAO billDAO) {
        this.billDAO = billDAO;
    }

    public Either<IException, Integer> insertLog(String cedula, String sistema, String estado, String mensaje) {
        try {

            billDAO.insertLog(cedula, sistema, estado, mensaje);
            return Either.right(1);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }

    }




    public Either<IException, List <String>> getCodQuipu(String codsara) {
        try {


            return Either.right(billDAO.getCodQuipu(codsara));

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }

    }


}