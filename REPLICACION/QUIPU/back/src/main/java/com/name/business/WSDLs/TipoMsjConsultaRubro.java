
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for tipoMsjConsultaRubro complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjConsultaRubro">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="fecha" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFecha"/>
 *         &lt;element name="Empresa" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEmpresa"/>
 *         &lt;element name="CodigoArea" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoAreaResponsabilidad"/>
 *         &lt;element name="CodigoConcepto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoConceptoPresupuesto"/>
 *         &lt;element name="CodigoRecurso" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoRecurso"/>
 *         &lt;element name="Proyecto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoProyecto"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjConsultaRubro", propOrder = {
    "fechaAndEmpresaAndCodigoArea"
})
public class TipoMsjConsultaRubro
    extends TipoMsjGenerico
{

    @XmlElementRefs({
        @XmlElementRef(name = "CodigoConcepto", namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", type = JAXBElement.class),
        @XmlElementRef(name = "CodigoRecurso", namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", type = JAXBElement.class),
        @XmlElementRef(name = "Proyecto", namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", type = JAXBElement.class),
        @XmlElementRef(name = "fecha", namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", type = JAXBElement.class),
        @XmlElementRef(name = "Empresa", namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", type = JAXBElement.class),
        @XmlElementRef(name = "CodigoArea", namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", type = JAXBElement.class)
    })
    protected List<JAXBElement<?>> fechaAndEmpresaAndCodigoArea;

    /**
     * Gets the value of the fechaAndEmpresaAndCodigoArea property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fechaAndEmpresaAndCodigoArea property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFechaAndEmpresaAndCodigoArea().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getFechaAndEmpresaAndCodigoArea() {
        if (fechaAndEmpresaAndCodigoArea == null) {
            fechaAndEmpresaAndCodigoArea = new ArrayList<JAXBElement<?>>();
        }
        return this.fechaAndEmpresaAndCodigoArea;
    }

}
