
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoRespuestaOp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoRespuestaOp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultadoOp" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoResultadoOp"/>
 *         &lt;element name="idError" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdError"/>
 *         &lt;element name="msjError" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMensajeError"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoRespuestaOp", propOrder = {
    "resultadoOp",
    "idError",
    "msjError"
})
public class TipoRespuestaOp {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoResultadoOp resultadoOp;
    @XmlElement(required = true)
    protected String idError;
    @XmlElement(required = true)
    protected String msjError;

    /**
     * Gets the value of the resultadoOp property.
     * 
     * @return
     *     possible object is
     *     {@link TipoResultadoOp }
     *     
     */
    public TipoResultadoOp getResultadoOp() {
        return resultadoOp;
    }

    /**
     * Sets the value of the resultadoOp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoResultadoOp }
     *     
     */
    public void setResultadoOp(TipoResultadoOp value) {
        this.resultadoOp = value;
    }

    /**
     * Gets the value of the idError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdError() {
        return idError;
    }

    /**
     * Sets the value of the idError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdError(String value) {
        this.idError = value;
    }

    /**
     * Gets the value of the msjError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsjError() {
        return msjError;
    }

    /**
     * Sets the value of the msjError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsjError(String value) {
        this.msjError = value;
    }

}
