
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjTerceroFinanciero complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjTerceroFinanciero">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="TerceroFinanciero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTerceroFinanciero"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjTerceroFinanciero", propOrder = {
    "terceroFinanciero"
})
public class TipoMsjTerceroFinanciero
    extends TipoMsjGenerico
{

    @XmlElement(name = "TerceroFinanciero", required = true)
    protected List<TipoTerceroFinanciero> terceroFinanciero;

    /**
     * Gets the value of the terceroFinanciero property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the terceroFinanciero property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTerceroFinanciero().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoTerceroFinanciero }
     * 
     * 
     */
    public List<TipoTerceroFinanciero> getTerceroFinanciero() {
        if (terceroFinanciero == null) {
            terceroFinanciero = new ArrayList<TipoTerceroFinanciero>();
        }
        return this.terceroFinanciero;
    }

}
