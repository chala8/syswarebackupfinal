
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjSolicitudPorTipoVinculacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudPorTipoVinculacion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="elemTipoVinculacion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoVinculacion"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudPorTipoVinculacion", propOrder = {
    "elemTipoVinculacion"
})
public class TipoMsjSolicitudPorTipoVinculacion
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected List<String> elemTipoVinculacion;

    /**
     * Gets the value of the elemTipoVinculacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elemTipoVinculacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElemTipoVinculacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getElemTipoVinculacion() {
        if (elemTipoVinculacion == null) {
            elemTipoVinculacion = new ArrayList<String>();
        }
        return this.elemTipoVinculacion;
    }

}
