
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoInfoResponsable complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoResponsable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cargo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCargoEmpleado"/>
 *         &lt;element name="dependencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDependencia"/>
 *         &lt;element name="tipoResponsable" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoResponsble"/>
 *         &lt;element name="empresa" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEmpresa"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoResponsable", propOrder = {
    "cargo",
    "dependencia",
    "tipoResponsable",
    "empresa"
})
public class TipoInfoResponsable {

    @XmlElement(required = true)
    protected String cargo;
    @XmlElement(required = true)
    protected String dependencia;
    @XmlElement(required = true)
    protected String tipoResponsable;
    @XmlElement(required = true)
    protected String empresa;

    /**
     * Gets the value of the cargo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * Sets the value of the cargo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCargo(String value) {
        this.cargo = value;
    }

    /**
     * Gets the value of the dependencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependencia() {
        return dependencia;
    }

    /**
     * Sets the value of the dependencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependencia(String value) {
        this.dependencia = value;
    }

    /**
     * Gets the value of the tipoResponsable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoResponsable() {
        return tipoResponsable;
    }

    /**
     * Sets the value of the tipoResponsable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoResponsable(String value) {
        this.tipoResponsable = value;
    }

    /**
     * Gets the value of the empresa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Sets the value of the empresa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpresa(String value) {
        this.empresa = value;
    }

}
