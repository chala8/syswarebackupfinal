
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjGenerico complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjGenerico">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infoMensaje" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoMensaje"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjGenerico", propOrder = {
    "infoMensaje"
})
@XmlSeeAlso({
    TipoMsjRespuestaCabeceraContratos.class,
    TipoMsjTercero.class,
    TipoMsjrespuestaDetalleDocente.class,
    TipoMsjRespuestaDetalleEstudiante.class,
    TipoMsjRespuestaOp.class,
    TipoMsjRespuestaExistencia.class,
    TipoMsjRespuestaHomologConceptoPresupuesto.class,
    TipoMsjRespuestaEmpleado.class,
    TipoMsjTerceroFinanciero.class,
    TipoMsjRespuestaRubro.class,
    TipoMsjCuentaBancariaTercero.class,
    TipoMsjSolicitudPorIdTercero.class,
    TipoMsjDetalleContratos.class,
    TipoMsjConsultaRubro.class,
    TipoMsjRespuestaConsultaEstudiante.class,
    TipoMsjRespuestaEstadoNomina.class,
    TipoMsjRespuestaHomologArea.class,
    TipoMsjSolicitudHomologConceptoPresupuesto.class,
    TipoMsjRespuestaRegistroCuentaNomina.class,
    TipoMsjSolicitudHomlogArea.class,
    TipoMsjSolicitudEstadoCargueNomina.class,
    TipoMsjSolicitudPorTipoVinculacion.class,
    TipoMsjDocente.class,
    TipoMsjRespuestaContratos.class,
    TipoMsjSolicitudContrato.class
})
public class TipoMsjGenerico {

    @XmlElement(required = true)
    protected TipoInfoMensaje infoMensaje;

    /**
     * Gets the value of the infoMensaje property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoMensaje }
     *     
     */
    public TipoInfoMensaje getInfoMensaje() {
        return infoMensaje;
    }

    /**
     * Sets the value of the infoMensaje property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoMensaje }
     *     
     */
    public void setInfoMensaje(TipoInfoMensaje value) {
        this.infoMensaje = value;
    }

}
