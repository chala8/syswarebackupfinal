package com.name.business.businesses;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.WSDLs.*;
import com.name.business.entities.TypeDocument;
import com.name.business.representations.inputData;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.DAOs.*;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import org.apache.poi.ss.formula.functions.T;
import org.json.JSONObject;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.FileOutputStream;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;



public class UNDropBusiness {

    private UNDropDAO billDAO;
    public UNDropBusiness(UNDropDAO billDAO) {
        this.billDAO = billDAO;
    }
/*
    public Either<IException, String> getCC() {
        try {

            List <TypeDocument> ListElement = billDAO.getCC("P");

            System.out.println("List Size: "+ListElement.size());


            for (TypeDocument element:ListElement) {
                GestiongeneralterceroClientEp wsdl = new GestiongeneralterceroClientEp();
                GestionGeneralTerceroPortType instancedWSDL = wsdl.getGestionGeneralTerceroPort();
                TipoMsjSolicitudPorIdTercero objectCC = new TipoMsjSolicitudPorIdTercero();
                TipoInfoMensaje infoMensaje = new TipoInfoMensaje();

                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(new Date());
                XMLGregorianCalendar calendarFinal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);

                infoMensaje.setFechaHora(calendarFinal);
                infoMensaje.setIdSede("01");
                infoMensaje.setIdSistema("100005");

                objectCC.setInfoMensaje(infoMensaje);

                TipoIdTercero idType = new TipoIdTercero();

                idType.setNumeroDocumento(element.getCedula());


                idType.setTipoDocumento(TipoTipoDocumento.CC);

                if(element.getTipoDocumentoSoa()=="TI"){
                    idType.setTipoDocumento(TipoTipoDocumento.TI);
                }

                if(element.getTipoDocumentoSoa()=="CE"){
                    idType.setTipoDocumento(TipoTipoDocumento.CE);
                }

                if(element.getTipoDocumentoSoa()=="PA"){
                    idType.setTipoDocumento(TipoTipoDocumento.PA);
                }

                if(element.getTipoDocumentoSoa()=="RC"){
                    idType.setTipoDocumento(TipoTipoDocumento.RC);
                }

                if(element.getTipoDocumentoSoa()=="NI"){
                    idType.setTipoDocumento(TipoTipoDocumento.NI);
                }

                if(element.getTipoDocumentoSoa()=="IV"){
                    idType.setTipoDocumento(TipoTipoDocumento.IV);
                }



                objectCC.setIdTercero(idType);

                TipoMsjRespuestaOp response = instancedWSDL.opReplicarEmpleado(objectCC);

                billDAO.updateCC(element.getCedula());

                System.out.println(response.getRespuestaOP().getMsjError() + ", " + response.getRespuestaOP().getResultadoOp().value());
            }

            return Either.right("OK");

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }
*/

    public Either<IException, String> getCC(inputData input) {
        String error = "";
        try {

            String newExterno = input.getExternos().replaceAll("'","\"");


            System.out.println("--------------------EXTERNO ANTES--------------------------------");
            System.out.println(input.getExternos());

            JSONObject jsonObject = new JSONObject(newExterno);

            System.out.println("--------------------EXTERNO DESPUES--------------------------------");
            System.out.println(jsonObject.toString());

            System.out.println("--------------------EXTERNO END--------------------------------");

            System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");

            QUIPUTERCEROFINANCIEROWSNacional wsdl = new QUIPUTERCEROFINANCIEROWSNacional();
            QUIPUTERCEROFINANCIEROWSNacionalPortType instancedWSDL = wsdl.getQUIPUTERCEROFINANCIEROWSNacionalSOAP11PortHttp();

            TipoMsjSolicitudPorIdTercero elemSolicitudGestionPersonaBPUN = new TipoMsjSolicitudPorIdTercero();


            TipoInfoMensaje infomensaje = new TipoInfoMensaje();

            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(new Date());
            XMLGregorianCalendar calendarFinal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
            calendarFinal.setTime(DatatypeConstants.FIELD_UNDEFINED,
                    DatatypeConstants.FIELD_UNDEFINED,
                    DatatypeConstants.FIELD_UNDEFINED,
                    DatatypeConstants.FIELD_UNDEFINED);

            infomensaje.setFechaHora(calendarFinal);
            infomensaje.setIdSede(input.getIdSede());
            infomensaje.setIdSistema(input.getIdSistema());

            elemSolicitudGestionPersonaBPUN.setInfoMensaje(infomensaje);

            TipoIdTercero idTercero = new TipoIdTercero();

            TipoTipoDocumento doc = TipoTipoDocumento.CC;
            if (input.getTipoDocumento().equals("CE")) {
                doc = TipoTipoDocumento.CE;
            }
            if (input.getTipoDocumento().equals("IV")) {
                doc = TipoTipoDocumento.IV;
            }
            if (input.getTipoDocumento().equals("NI")) {
                doc = TipoTipoDocumento.NI;
            }
            if (input.getTipoDocumento().equals("PA")) {
                doc = TipoTipoDocumento.PA;
            }
            if (input.getTipoDocumento().equals("RC")) {
                doc = TipoTipoDocumento.RC;
            }
            if (input.getTipoDocumento().equals("TI")) {
                doc = TipoTipoDocumento.TI;
            }

            idTercero.setTipoDocumento(doc);
            idTercero.setNumeroDocumento(input.getNumDocumento());

            elemSolicitudGestionPersonaBPUN.setIdTercero(idTercero);

            TipoMsjRespuestaExistencia response = instancedWSDL.opExistenciaTercero(elemSolicitudGestionPersonaBPUN);
            String OpDone = "";


            TipoMsjTerceroFinanciero elemSolicitudGestion = new TipoMsjTerceroFinanciero();
            elemSolicitudGestion.setInfoMensaje(infomensaje);

            TipoTerceroFinanciero terceroFinanciero = new TipoTerceroFinanciero();


            //TIPO TERCERO
            TipoTercero tipoTercero = new TipoTercero();


            TipoInfoBasicaPersonal infoBasicaPersonal= new TipoInfoBasicaPersonal();
            TipoInfoContacto infoContacto = new TipoInfoContacto();
            TipoInfoTributariaTercero infoTributariaTercero = new TipoInfoTributariaTercero();

            //TIPO INFO BASICA PERSONAL
            infoBasicaPersonal.setIdTercero(idTercero);
            TipoUbicacion ubicacionResidencia = new TipoUbicacion();
            ubicacionResidencia.setIdPais(jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getJSONObject("lugarResidencia").getString("idPais"));
            ubicacionResidencia.setIdCiudad(jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getJSONObject("lugarResidencia").getString("idCiudad").substring(0,5));

            infoBasicaPersonal.setLugarResidencia(ubicacionResidencia);

            TipoUbicacion ubicacionNacimiento = new TipoUbicacion();
            ubicacionNacimiento.setIdPais(jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getJSONObject("lugarNacimiento").getString("idPais"));
            ubicacionNacimiento.setIdCiudad(jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getJSONObject("lugarNacimiento").getString("idCiudad").substring(0,5));
            infoBasicaPersonal.setLugarNacimiento(ubicacionNacimiento);


            String fechaNacimiento = "";
            String año = jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getJSONObject("fechaNacimiento").getBigInteger("year").toString();
            String mes = jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getJSONObject("fechaNacimiento").getBigInteger("month").toString();
            String dia = jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getJSONObject("fechaNacimiento").getBigInteger("day").toString();
            if(dia.length()==1){dia = "0"+dia; }
            if(mes.length()==1){mes = "0"+mes; }

            fechaNacimiento = año+"-"+mes+"-"+dia;
            infoBasicaPersonal.setFechaNacimiento(fechaNacimiento);


            TipoEstadoCivil estadoCivil = TipoEstadoCivil.C;
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getString("estadoCivil").equals("C")) {
                estadoCivil = TipoEstadoCivil.C;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getString("estadoCivil").equals("D")) {
                estadoCivil = TipoEstadoCivil.D;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getString("estadoCivil").equals("S")) {
                estadoCivil = TipoEstadoCivil.S;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getString("estadoCivil").equals("U")) {
                estadoCivil = TipoEstadoCivil.U;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getString("estadoCivil").equals("V")) {
                estadoCivil = TipoEstadoCivil.V;
            }
            infoBasicaPersonal.setEstadoCivil(estadoCivil);


            TipoGenerotercero genero = TipoGenerotercero.M;
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getString("generoTercero").equals("M")) {
                genero = TipoGenerotercero.M;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getString("generoTercero").equals("F")) {
                genero = TipoGenerotercero.F;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getString("generoTercero").equals("O")) {
                genero = TipoGenerotercero.O;
            }
            infoBasicaPersonal.setGeneroTercero(genero);



            TipoNaturalezaTercero natu = TipoNaturalezaTercero.NAT;
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getString("naturalezaTercero").equals("NAT")) {
                natu = TipoNaturalezaTercero.NAT;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getString("naturalezaTercero").equals("JUR")) {
                natu = TipoNaturalezaTercero.JUR;
            }
            infoBasicaPersonal.setNaturalezaTercero(natu);


            TipoInfoBasicaPersonal.NomTercero nomTercero = new TipoInfoBasicaPersonal.NomTercero();
            TipoNombrePersonaNatural personaNatural = new TipoNombrePersonaNatural();
            personaNatural.setPrimerApellido(jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getJSONObject("nomTercero").getJSONObject("nombrePersonaNatural").getString("primerApellido"));
            personaNatural.setSegundoApellido(jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getJSONObject("nomTercero").getJSONObject("nombrePersonaNatural").getString("segundoApellido"));
            personaNatural.setPrimerNombre(jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getJSONObject("nomTercero").getJSONObject("nombrePersonaNatural").getString("primerNombre"));
            personaNatural.setSegundoNombre(jsonObject.getJSONObject("empleado").getJSONObject("infoBasicaTercero").getJSONObject("nomTercero").getJSONObject("nombrePersonaNatural").getString("segundoNombre"));
            nomTercero.setNombrePersonaNatural(personaNatural);
            infoBasicaPersonal.setNomTercero(nomTercero);




            //SET INFO CONTACTO
            infoContacto.setCiudadContacto(jsonObject.getJSONObject("empleado").getJSONObject("infoContacto").getString("ciudadContacto"));
            infoContacto.setTelefonoContacto(jsonObject.getJSONObject("empleado").getJSONObject("infoContacto").getString("telefonoContacto"));
            infoContacto.setDireccionContacto(jsonObject.getJSONObject("empleado").getJSONObject("infoContacto").getString("direccionContacto"));
            infoContacto.setCorreoElectronicoContacto(jsonObject.getJSONObject("empleado").getJSONObject("infoContacto").getString("correoElectronicoContacto"));


            //SET INFO TRIBUTARIA
            TipoTipoTercero tipoTipoTercero = TipoTipoTercero.AD;
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("AD")) {
                tipoTipoTercero = TipoTipoTercero.AD;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("AF")) {
                tipoTipoTercero = TipoTipoTercero.AF;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("AL")) {
                tipoTipoTercero = TipoTipoTercero.AL;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("AP")) {
                tipoTipoTercero = TipoTipoTercero.AP;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("AS")) {
                tipoTipoTercero = TipoTipoTercero.AS;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("AV")) {
                tipoTipoTercero = TipoTipoTercero.AV;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("AX")) {
                tipoTipoTercero = TipoTipoTercero.AX;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("BA")) {
                tipoTipoTercero = TipoTipoTercero.BA;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("BC")) {
                tipoTipoTercero = TipoTipoTercero.BC;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("BG")) {
                tipoTipoTercero = TipoTipoTercero.BG;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("CC")) {
                tipoTipoTercero = TipoTipoTercero.CC;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("CD")) {
                tipoTipoTercero = TipoTipoTercero.CD;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("CN")) {
                tipoTipoTercero = TipoTipoTercero.CN;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("CP")) {
                tipoTipoTercero = TipoTipoTercero.CP;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("CT")) {
                tipoTipoTercero = TipoTipoTercero.CT;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("DI")) {
                tipoTipoTercero = TipoTipoTercero.DI;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("DN")) {
                tipoTipoTercero = TipoTipoTercero.DN;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("EM")) {
                tipoTipoTercero = TipoTipoTercero.EM;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("ES")) {
                tipoTipoTercero = TipoTipoTercero.ES;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("EX")) {
                tipoTipoTercero = TipoTipoTercero.EX;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("FC")) {
                tipoTipoTercero = TipoTipoTercero.FC;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("FP")) {
                tipoTipoTercero = TipoTipoTercero.FP;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("FS")) {
                tipoTipoTercero = TipoTipoTercero.FS;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("GO")) {
                tipoTipoTercero = TipoTipoTercero.GO;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("IG")) {
                tipoTipoTercero = TipoTipoTercero.IG;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("IN")) {
                tipoTipoTercero = TipoTipoTercero.IN;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("IV")) {
                tipoTipoTercero = TipoTipoTercero.IV;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("OI")) {
                tipoTipoTercero = TipoTipoTercero.OI;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("OT")) {
                tipoTipoTercero = TipoTipoTercero.OT;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("PR")) {
                tipoTipoTercero = TipoTipoTercero.PR;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("RK")) {
                tipoTipoTercero = TipoTipoTercero.RK;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("SE")) {
                tipoTipoTercero = TipoTipoTercero.SE;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("SP")) {
                tipoTipoTercero = TipoTipoTercero.SP;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("TB")) {
                tipoTipoTercero = TipoTipoTercero.TB;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("TR")) {
                tipoTipoTercero = TipoTipoTercero.TR;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("TV")) {
                tipoTipoTercero = TipoTipoTercero.TV;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("UN")) {
                tipoTipoTercero = TipoTipoTercero.UN;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("UP")) {
                tipoTipoTercero = TipoTipoTercero.UP;
            }
            if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoTercero").equals("UT")) {
                tipoTipoTercero = TipoTipoTercero.UT;
            }
            infoTributariaTercero.setTipoTercero(tipoTipoTercero);

            if(response.isConfirmacionExistencia()) {
                TipoTipoContribuyente tipoTipoContribuyente = TipoTipoContribuyente.GE;
                infoTributariaTercero.setTipoContribuyente(tipoTipoContribuyente);
            }else{
                TipoTipoContribuyente tipoTipoContribuyente = TipoTipoContribuyente.ES;
                if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoContribuyente").equals("ES")) {
                    tipoTipoContribuyente = TipoTipoContribuyente.ES;
                }
                if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoContribuyente").equals("AN")) {
                    tipoTipoContribuyente = TipoTipoContribuyente.AN;
                }
                if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoContribuyente").equals("RC")) {
                    tipoTipoContribuyente = TipoTipoContribuyente.RC;
                }
                if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoContribuyente").equals("GA")) {
                    tipoTipoContribuyente = TipoTipoContribuyente.GA;
                }
                if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoContribuyente").equals("GC")) {
                    tipoTipoContribuyente = TipoTipoContribuyente.GC;
                }
                if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoContribuyente").equals("GE")) {

                }
                if (jsonObject.getJSONObject("empleado").getJSONObject("infoTributaria").getString("tipoContribuyente").equals("RS")) {
                    tipoTipoContribuyente = TipoTipoContribuyente.RS;
                }
                infoTributariaTercero.setTipoContribuyente(tipoTipoContribuyente);
            }

            tipoTercero.setInfoBasicaTercero(infoBasicaPersonal);
            tipoTercero.setInfoContacto(infoContacto);
            tipoTercero.setInfoTributaria(infoTributariaTercero);


            // SETS AUSENTISMO

            if(response.isConfirmacionExistencia()) {
                TipoEstadoAdministrativo tipoEstadoAdministrativo = TipoEstadoAdministrativo.NOR;
                terceroFinanciero.setAusentismo(tipoEstadoAdministrativo);
            }else {
                TipoEstadoAdministrativo tipoEstadoAdministrativo = TipoEstadoAdministrativo.ESP;
                if (jsonObject.getJSONObject("empleado").getJSONArray("vinculacion").getJSONObject(0).getJSONObject("cabeceraContrato").getString("estadoAdministrativo").equals("ESP")) {
                    tipoEstadoAdministrativo = TipoEstadoAdministrativo.ESP;
                }
                if (jsonObject.getJSONObject("empleado").getJSONArray("vinculacion").getJSONObject(0).getJSONObject("cabeceraContrato").getString("estadoAdministrativo").equals("AUS")) {
                    tipoEstadoAdministrativo = TipoEstadoAdministrativo.AUS;
                }
                if (jsonObject.getJSONObject("empleado").getJSONArray("vinculacion").getJSONObject(0).getJSONObject("cabeceraContrato").getString("estadoAdministrativo").equals("CEM")) {
                    tipoEstadoAdministrativo = TipoEstadoAdministrativo.CEM;
                }
                if (jsonObject.getJSONObject("empleado").getJSONArray("vinculacion").getJSONObject(0).getJSONObject("cabeceraContrato").getString("estadoAdministrativo").equals("MOP")) {
                    tipoEstadoAdministrativo = TipoEstadoAdministrativo.MOP;
                }
                if (jsonObject.getJSONObject("empleado").getJSONArray("vinculacion").getJSONObject(0).getJSONObject("cabeceraContrato").getString("estadoAdministrativo").equals("NOR")) {
                    tipoEstadoAdministrativo = TipoEstadoAdministrativo.NOR;
                }
                if (jsonObject.getJSONObject("empleado").getJSONArray("vinculacion").getJSONObject(0).getJSONObject("cabeceraContrato").getString("estadoAdministrativo").equals("RET")) {
                    tipoEstadoAdministrativo = TipoEstadoAdministrativo.RET;
                }
                terceroFinanciero.setAusentismo(tipoEstadoAdministrativo);
            }

            TipoInfoResponsable tipoInfoResponsable = new TipoInfoResponsable();
            //tipoInfoResponsable.setTipoResponsable(jsonObject.getJSONObject("empleado").getJSONArray("vinculacion").getJSONObject(0).getJSONObject("cabeceraContrato").getString("tipoVinculacion"));
            // 1
            tipoInfoResponsable.setTipoResponsable("3");
            tipoInfoResponsable.setCargo(jsonObject.getJSONObject("empleado").getJSONArray("vinculacion").getJSONObject(0).getJSONObject("detalleContrato").getString("cargoEmpleado"));
            tipoInfoResponsable.setDependencia(jsonObject.getJSONObject("empleado").getJSONArray("vinculacion").getJSONObject(0).getJSONObject("cabeceraContrato").getString("dependencia"));

            String empresa = "101010101";
            // SETS INFO RESPONSABLE
            try{


                URL url = new URL("http://168.176.6.92:8396/v1/log/getCodQuipu?codsara="+input.getArea());//your url i.e fetch data from .
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");

                conn.setDoOutput(true);


                System.out.println("5 - ");
                System.out.println(conn.getResponseCode());
                System.out.println(conn.getResponseMessage());

                if (conn.getResponseCode() != 200) {
                    InputStreamReader in2 = new InputStreamReader(conn.getErrorStream());
                    BufferedReader br2 = new BufferedReader(in2);
                    String output2;
                    System.out.println("ERROR: ");
                    String ErrorOutput = "";
                    while ((output2 = br2.readLine()) != null) {
                        System.out.println(output2);
                        ErrorOutput+=(" ; "+output2);
                    }
                    error = ErrorOutput.replaceAll(";","-").replaceAll("\\{","").replaceAll("\\}","").replaceAll("\\[","").replaceAll("\\]","").replaceAll(":","");
                    throw new RuntimeException("Failed : HTTP Error code : "
                            + conn.getResponseCode());

                }else{
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    BufferedReader br = new BufferedReader(in);
                    String output;

                    while ((output = br.readLine()) != null) {
                        empresa = output;
                    }
                    conn.disconnect();

                    if(empresa==null || empresa.equals("")){ empresa = "101010101";}
                }
            }catch (Exception e){
                e.printStackTrace();
            }


            tipoInfoResponsable.setEmpresa(empresa.replaceAll("\\[","").replaceAll("\\]","").replaceAll("\"",""));





            //SETS TERCEROFINANCIERO INFO
            terceroFinanciero.setInfoTipoTercero(tipoTercero);
            terceroFinanciero.setInfoResponsable(tipoInfoResponsable);


            elemSolicitudGestion.getTerceroFinanciero().add(terceroFinanciero);
            TipoMsjRespuestaOp response2;
            if(response.isConfirmacionExistencia()){
                OpDone = "DISABLED - UPDATE";
                //response2 = instancedWSDL.opActualizarTercero(elemSolicitudGestion);
                //response2 = new TipoMsjRespuestaOp();
                OpDone ="CREATE";
                response2 = instancedWSDL.opCrearTercero(elemSolicitudGestion);
            }else{
                OpDone ="CREATE";
                response2 = instancedWSDL.opCrearTercero(elemSolicitudGestion);
            }






            System.out.println("------------------------RESPUESTA--------------------------");

            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("");

            try{

                System.out.println("ID SISTEMA: " + response.getInfoMensaje().getIdSistema());
                System.out.println("ID SEDE: " + response.getInfoMensaje().getIdSede());
                System.out.println("FECHA HORA: " + response.getInfoMensaje().getFechaHora());
                System.out.println("RESULTADO OP EXISTENCIA: " + response.isConfirmacionExistencia());
                System.out.println("OP HECHA: " + OpDone);
                System.out.println("RESULTADO OP HECHA: " + response2.getRespuestaOP().getResultadoOp());
                System.out.println("ERROR MSJ: " + response2.getRespuestaOP().getResultadoOp().toString() + " | " + response2.getRespuestaOP().getIdError() + " | " + response2.getRespuestaOP().getMsjError());


                error = "RESULTADO OP EXISTENCIA: " + response.isConfirmacionExistencia() + "OP HECHA: " + OpDone + "RESULTADO OP HECHA: " + response2.getRespuestaOP().getResultadoOp() + "ERROR MSJ: " + response2.getRespuestaOP().getResultadoOp().toString() + " | " + response2.getRespuestaOP().getIdError() + " | " + response2.getRespuestaOP().getMsjError();
                System.out.println("");
                System.out.println("");
                System.out.println("");
                System.out.println("");


                if(response2.getRespuestaOP().getResultadoOp().toString().contains("FALLO")){
                    return Either.left(new TechnicalException(error));
                }
            }catch(Exception e){

                System.out.println("Response2 was null, it is likely that it was a update, so it did not execute");
                return Either.left(new TechnicalException(error));
            }


            return Either.right("EXITO");

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(error));
        }
    }





}