
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjRespuestaRegistroCuentaNomina complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaRegistroCuentaNomina">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="InfoResultado" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoRespuestaOp"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaRegistroCuentaNomina", propOrder = {
    "infoResultado"
})
public class TipoMsjRespuestaRegistroCuentaNomina
    extends TipoMsjGenerico
{

    @XmlElement(name = "InfoResultado", required = true)
    protected List<TipoRespuestaOp> infoResultado;

    /**
     * Gets the value of the infoResultado property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infoResultado property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfoResultado().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoRespuestaOp }
     * 
     * 
     */
    public List<TipoRespuestaOp> getInfoResultado() {
        if (infoResultado == null) {
            infoResultado = new ArrayList<TipoRespuestaOp>();
        }
        return this.infoResultado;
    }

}
