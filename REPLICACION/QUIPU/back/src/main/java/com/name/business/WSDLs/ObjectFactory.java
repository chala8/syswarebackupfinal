
package com.name.business.WSDLs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the WSDLs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ElemConsultaEstudiante_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaEstudiante");
    private final static QName _ElemRespuestaAdicionarCuentaBancaria_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaAdicionarCuentaBancaria");
    private final static QName _ElemConsultaDetalleDocente_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaDetalleDocente");
    private final static QName _ElemSolicitudCreacionTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudCreacionTercero");
    private final static QName _ElemSolicitudRubro_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudRubro");
    private final static QName _ElemRespuestaConsultaEstudiante_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaConsultaEstudiante");
    private final static QName _ElemRespuestaEstadoNomina_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaEstadoNomina");
    private final static QName _ElemRespuestaHomologAreaResponsabilidad_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaHomologAreaResponsabilidad");
    private final static QName _ElemSolicitudContableNomina_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudContableNomina");
    private final static QName _ElemSolicitudHomologConceptoPresupuesto_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudHomologConceptoPresupuesto");
    private final static QName _ElemRespuestaRegistroCuentaNomina_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaRegistroCuentaNomina");
    private final static QName _ElemSolicitudHomologAreaResponsabilidad_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudHomologAreaResponsabilidad");
    private final static QName _ElemConsultaTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaTercero");
    private final static QName _ElemRespuestaEmpleado_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaEmpleado");
    private final static QName _ElemSolicitudCreacionDocente_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudCreacionDocente");
    private final static QName _ElemSolicitudEstadoCargueNomina_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudEstadoCargueNomina");
    private final static QName _ElemSolicitudReplicaTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudReplicaTercero");
    private final static QName _ElemConsultaEmpleadoPorIdTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaEmpleadoPorIdTercero");
    private final static QName _ElemConsultaPorTipoVinculacion_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaPorTipoVinculacion");
    private final static QName _ElemRespuestaCabeceraContratosNacional_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCabeceraContratosNacional");
    private final static QName _ElemSolicitudCreacionActualizacionTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudCreacionActualizacionTercero");
    private final static QName _ElemRespuestaConsultaTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaConsultaTercero");
    private final static QName _ElemRespuestaDetalleDocente_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaDetalleDocente");
    private final static QName _ElemRespuestaDetalleEstudiante_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaDetalleEstudiante");
    private final static QName _ElemRespuestaActualizacionTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaActualizacionTercero");
    private final static QName _ElemRespuestaExistenciaDocente_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaExistenciaDocente");
    private final static QName _ElemRespuestaHomologConceptoPresupuesto_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaHomologConceptoPresupuesto");
    private final static QName _ElemRespuestaReplicaTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaReplicaTercero");
    private final static QName _ElemRespuestaCreacionActualizacionTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCreacionActualizacionTercero");
    private final static QName _ElemSolicitudActualizacionDocente_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudActualizacionDocente");
    private final static QName _ElemSolicitudActualizacionTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudActualizacionTercero");
    private final static QName _ElemRespuestaCreacionTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCreacionTercero");
    private final static QName _ElemRespuestaCabeceraContratos_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCabeceraContratos");
    private final static QName _ElemRespuestaRubro_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaRubro");
    private final static QName _ElemRespuestaExistenciaTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaExistenciaTercero");
    private final static QName _ElemSolicitudAdicionarCuentaBancaria_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudAdicionarCuentaBancaria");
    private final static QName _ElemConsultaCabeceraContrato_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaCabeceraContrato");
    private final static QName _ElemRespuestaConsultaDetalleEmpleado_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaConsultaDetalleEmpleado");
    private final static QName _ElemRespuestaSolicitudContableNomina_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaSolicitudContableNomina");
    private final static QName _ElemConsultaExistenciaTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaExistenciaTercero");
    private final static QName _ElemExistenciaDocente_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemExistenciaDocente");
    private final static QName _TipoMsjCuentaBancariaTerceroInformacionCuentaNueva_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "InformacionCuentaNueva");
    private final static QName _TipoMsjCuentaBancariaTerceroIdTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "idTercero");
    private final static QName _TipoMsjCuentaBancariaTerceroInformacionCuentaAntigua_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "InformacionCuentaAntigua");
    private final static QName _TipoMsjSolicitudHomlogAreaEmpresa_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "Empresa");
    private final static QName _TipoMsjSolicitudHomlogAreaNumeroItem_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "NumeroItem");
    private final static QName _TipoMsjSolicitudHomlogAreaTipoRegistro_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "TipoRegistro");
    private final static QName _TipoMsjSolicitudHomlogAreaInterface_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "Interface");
    private final static QName _TipoMsjConsultaRubroCodigoConcepto_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "CodigoConcepto");
    private final static QName _TipoMsjConsultaRubroFecha_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "fecha");
    private final static QName _TipoMsjConsultaRubroCodigoArea_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "CodigoArea");
    private final static QName _TipoMsjConsultaRubroProyecto_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "Proyecto");
    private final static QName _TipoMsjConsultaRubroCodigoRecurso_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "CodigoRecurso");
    private final static QName _TipoMsjRespuestaHomologAreaDescripcionAreaResponsabilidad_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "DescripcionAreaResponsabilidad");
    private final static QName _TipoMsjRespuestaHomologAreaCodigoAreaResponsabilidad_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "CodigoAreaResponsabilidad");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: WSDLs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TipoInfoBasicaPersonal }
     * 
     */
    public TipoInfoBasicaPersonal createTipoInfoBasicaPersonal() {
        return new TipoInfoBasicaPersonal();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaCabeceraContratos }
     * 
     */
    public TipoMsjRespuestaCabeceraContratos createTipoMsjRespuestaCabeceraContratos() {
        return new TipoMsjRespuestaCabeceraContratos();
    }

    /**
     * Create an instance of {@link TipoMsjTercero }
     * 
     */
    public TipoMsjTercero createTipoMsjTercero() {
        return new TipoMsjTercero();
    }

    /**
     * Create an instance of {@link TipoMsjrespuestaDetalleDocente }
     * 
     */
    public TipoMsjrespuestaDetalleDocente createTipoMsjrespuestaDetalleDocente() {
        return new TipoMsjrespuestaDetalleDocente();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaDetalleEstudiante }
     * 
     */
    public TipoMsjRespuestaDetalleEstudiante createTipoMsjRespuestaDetalleEstudiante() {
        return new TipoMsjRespuestaDetalleEstudiante();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaOp }
     * 
     */
    public TipoMsjRespuestaOp createTipoMsjRespuestaOp() {
        return new TipoMsjRespuestaOp();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaExistencia }
     * 
     */
    public TipoMsjRespuestaExistencia createTipoMsjRespuestaExistencia() {
        return new TipoMsjRespuestaExistencia();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaHomologConceptoPresupuesto }
     * 
     */
    public TipoMsjRespuestaHomologConceptoPresupuesto createTipoMsjRespuestaHomologConceptoPresupuesto() {
        return new TipoMsjRespuestaHomologConceptoPresupuesto();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaEmpleado }
     * 
     */
    public TipoMsjRespuestaEmpleado createTipoMsjRespuestaEmpleado() {
        return new TipoMsjRespuestaEmpleado();
    }

    /**
     * Create an instance of {@link TipoMsjTerceroFinanciero }
     * 
     */
    public TipoMsjTerceroFinanciero createTipoMsjTerceroFinanciero() {
        return new TipoMsjTerceroFinanciero();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaRubro }
     * 
     */
    public TipoMsjRespuestaRubro createTipoMsjRespuestaRubro() {
        return new TipoMsjRespuestaRubro();
    }

    /**
     * Create an instance of {@link TipoMsjCuentaBancariaTercero }
     * 
     */
    public TipoMsjCuentaBancariaTercero createTipoMsjCuentaBancariaTercero() {
        return new TipoMsjCuentaBancariaTercero();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudPorIdTercero }
     * 
     */
    public TipoMsjSolicitudPorIdTercero createTipoMsjSolicitudPorIdTercero() {
        return new TipoMsjSolicitudPorIdTercero();
    }

    /**
     * Create an instance of {@link TipoMsjDetalleContratos }
     * 
     */
    public TipoMsjDetalleContratos createTipoMsjDetalleContratos() {
        return new TipoMsjDetalleContratos();
    }

    /**
     * Create an instance of {@link TipoRespuestaOp }
     * 
     */
    public TipoRespuestaOp createTipoRespuestaOp() {
        return new TipoRespuestaOp();
    }

    /**
     * Create an instance of {@link TipoMsjConsultaRubro }
     * 
     */
    public TipoMsjConsultaRubro createTipoMsjConsultaRubro() {
        return new TipoMsjConsultaRubro();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaConsultaEstudiante }
     * 
     */
    public TipoMsjRespuestaConsultaEstudiante createTipoMsjRespuestaConsultaEstudiante() {
        return new TipoMsjRespuestaConsultaEstudiante();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaEstadoNomina }
     * 
     */
    public TipoMsjRespuestaEstadoNomina createTipoMsjRespuestaEstadoNomina() {
        return new TipoMsjRespuestaEstadoNomina();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaHomologArea }
     * 
     */
    public TipoMsjRespuestaHomologArea createTipoMsjRespuestaHomologArea() {
        return new TipoMsjRespuestaHomologArea();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudHomologConceptoPresupuesto }
     * 
     */
    public TipoMsjSolicitudHomologConceptoPresupuesto createTipoMsjSolicitudHomologConceptoPresupuesto() {
        return new TipoMsjSolicitudHomologConceptoPresupuesto();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaRegistroCuentaNomina }
     * 
     */
    public TipoMsjRespuestaRegistroCuentaNomina createTipoMsjRespuestaRegistroCuentaNomina() {
        return new TipoMsjRespuestaRegistroCuentaNomina();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudHomlogArea }
     * 
     */
    public TipoMsjSolicitudHomlogArea createTipoMsjSolicitudHomlogArea() {
        return new TipoMsjSolicitudHomlogArea();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudEstadoCargueNomina }
     * 
     */
    public TipoMsjSolicitudEstadoCargueNomina createTipoMsjSolicitudEstadoCargueNomina() {
        return new TipoMsjSolicitudEstadoCargueNomina();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudPorTipoVinculacion }
     * 
     */
    public TipoMsjSolicitudPorTipoVinculacion createTipoMsjSolicitudPorTipoVinculacion() {
        return new TipoMsjSolicitudPorTipoVinculacion();
    }

    /**
     * Create an instance of {@link TipoAsignatura }
     * 
     */
    public TipoAsignatura createTipoAsignatura() {
        return new TipoAsignatura();
    }

    /**
     * Create an instance of {@link TipoUBGAA }
     * 
     */
    public TipoUBGAA createTipoUBGAA() {
        return new TipoUBGAA();
    }

    /**
     * Create an instance of {@link TipoEmpleado }
     * 
     */
    public TipoEmpleado createTipoEmpleado() {
        return new TipoEmpleado();
    }

    /**
     * Create an instance of {@link TipoInfoCuenta }
     * 
     */
    public TipoInfoCuenta createTipoInfoCuenta() {
        return new TipoInfoCuenta();
    }

    /**
     * Create an instance of {@link TipoMsjDocente }
     * 
     */
    public TipoMsjDocente createTipoMsjDocente() {
        return new TipoMsjDocente();
    }

    /**
     * Create an instance of {@link TipoPlanEstudios }
     * 
     */
    public TipoPlanEstudios createTipoPlanEstudios() {
        return new TipoPlanEstudios();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaContratos }
     * 
     */
    public TipoMsjRespuestaContratos createTipoMsjRespuestaContratos() {
        return new TipoMsjRespuestaContratos();
    }

    /**
     * Create an instance of {@link TipoNombrePersonaNatural }
     * 
     */
    public TipoNombrePersonaNatural createTipoNombrePersonaNatural() {
        return new TipoNombrePersonaNatural();
    }

    /**
     * Create an instance of {@link TipoUbicacion }
     * 
     */
    public TipoUbicacion createTipoUbicacion() {
        return new TipoUbicacion();
    }

    /**
     * Create an instance of {@link TipoInfoMensaje }
     * 
     */
    public TipoInfoMensaje createTipoInfoMensaje() {
        return new TipoInfoMensaje();
    }

    /**
     * Create an instance of {@link TipoTercero }
     * 
     */
    public TipoTercero createTipoTercero() {
        return new TipoTercero();
    }

    /**
     * Create an instance of {@link TipoDetalleContrato }
     * 
     */
    public TipoDetalleContrato createTipoDetalleContrato() {
        return new TipoDetalleContrato();
    }

    /**
     * Create an instance of {@link TipoInfoTributariaTercero }
     * 
     */
    public TipoInfoTributariaTercero createTipoInfoTributariaTercero() {
        return new TipoInfoTributariaTercero();
    }

    /**
     * Create an instance of {@link TipoInfoBasicaPlanEstudios }
     * 
     */
    public TipoInfoBasicaPlanEstudios createTipoInfoBasicaPlanEstudios() {
        return new TipoInfoBasicaPlanEstudios();
    }

    /**
     * Create an instance of {@link TipoCargaAcademicaDocente }
     * 
     */
    public TipoCargaAcademicaDocente createTipoCargaAcademicaDocente() {
        return new TipoCargaAcademicaDocente();
    }

    /**
     * Create an instance of {@link TipoTerceroFinanciero }
     * 
     */
    public TipoTerceroFinanciero createTipoTerceroFinanciero() {
        return new TipoTerceroFinanciero();
    }

    /**
     * Create an instance of {@link TipoInfoDetallePlanEstudios }
     * 
     */
    public TipoInfoDetallePlanEstudios createTipoInfoDetallePlanEstudios() {
        return new TipoInfoDetallePlanEstudios();
    }

    /**
     * Create an instance of {@link TipoReciboMatricula }
     * 
     */
    public TipoReciboMatricula createTipoReciboMatricula() {
        return new TipoReciboMatricula();
    }

    /**
     * Create an instance of {@link TipoDia }
     * 
     */
    public TipoDia createTipoDia() {
        return new TipoDia();
    }

    /**
     * Create an instance of {@link TipoCabeceraContrato }
     * 
     */
    public TipoCabeceraContrato createTipoCabeceraContrato() {
        return new TipoCabeceraContrato();
    }

    /**
     * Create an instance of {@link TipoDetalleDocente }
     * 
     */
    public TipoDetalleDocente createTipoDetalleDocente() {
        return new TipoDetalleDocente();
    }

    /**
     * Create an instance of {@link TipoContratoTercero }
     * 
     */
    public TipoContratoTercero createTipoContratoTercero() {
        return new TipoContratoTercero();
    }

    /**
     * Create an instance of {@link TipoInfoContacto }
     * 
     */
    public TipoInfoContacto createTipoInfoContacto() {
        return new TipoInfoContacto();
    }

    /**
     * Create an instance of {@link TipoIdTercero }
     * 
     */
    public TipoIdTercero createTipoIdTercero() {
        return new TipoIdTercero();
    }

    /**
     * Create an instance of {@link TipoEstudiante }
     * 
     */
    public TipoEstudiante createTipoEstudiante() {
        return new TipoEstudiante();
    }

    /**
     * Create an instance of {@link TipoInfoResponsable }
     * 
     */
    public TipoInfoResponsable createTipoInfoResponsable() {
        return new TipoInfoResponsable();
    }

    /**
     * Create an instance of {@link TipoConceptoPresupuesto }
     * 
     */
    public TipoConceptoPresupuesto createTipoConceptoPresupuesto() {
        return new TipoConceptoPresupuesto();
    }

    /**
     * Create an instance of {@link TipoDocente }
     * 
     */
    public TipoDocente createTipoDocente() {
        return new TipoDocente();
    }

    /**
     * Create an instance of {@link TipoMontoMonetario }
     * 
     */
    public TipoMontoMonetario createTipoMontoMonetario() {
        return new TipoMontoMonetario();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudContrato }
     * 
     */
    public TipoMsjSolicitudContrato createTipoMsjSolicitudContrato() {
        return new TipoMsjSolicitudContrato();
    }

    /**
     * Create an instance of {@link TipoInfoDetalleAsignatura }
     * 
     */
    public TipoInfoDetalleAsignatura createTipoInfoDetalleAsignatura() {
        return new TipoInfoDetalleAsignatura();
    }

    /**
     * Create an instance of {@link TipoMsjGenerico }
     * 
     */
    public TipoMsjGenerico createTipoMsjGenerico() {
        return new TipoMsjGenerico();
    }

    /**
     * Create an instance of {@link TipoPlaza }
     * 
     */
    public TipoPlaza createTipoPlaza() {
        return new TipoPlaza();
    }

    /**
     * Create an instance of {@link TipoInfoContrato }
     * 
     */
    public TipoInfoContrato createTipoInfoContrato() {
        return new TipoInfoContrato();
    }

    /**
     * Create an instance of {@link TipoRubroPresupuestal }
     * 
     */
    public TipoRubroPresupuestal createTipoRubroPresupuestal() {
        return new TipoRubroPresupuestal();
    }

    /**
     * Create an instance of {@link TipoInfoBasicaAsignatura }
     * 
     */
    public TipoInfoBasicaAsignatura createTipoInfoBasicaAsignatura() {
        return new TipoInfoBasicaAsignatura();
    }

    /**
     * Create an instance of {@link TipoInfoBasicaPersonal.NomTercero }
     * 
     */
    public TipoInfoBasicaPersonal.NomTercero createTipoInfoBasicaPersonalNomTercero() {
        return new TipoInfoBasicaPersonal.NomTercero();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaEstudiante")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemConsultaEstudiante(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemConsultaEstudiante_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaAdicionarCuentaBancaria")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaAdicionarCuentaBancaria(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaAdicionarCuentaBancaria_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaDetalleDocente")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemConsultaDetalleDocente(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemConsultaDetalleDocente_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjTerceroFinanciero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudCreacionTercero")
    public JAXBElement<TipoMsjTerceroFinanciero> createElemSolicitudCreacionTercero(TipoMsjTerceroFinanciero value) {
        return new JAXBElement<TipoMsjTerceroFinanciero>(_ElemSolicitudCreacionTercero_QNAME, TipoMsjTerceroFinanciero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjConsultaRubro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudRubro")
    public JAXBElement<TipoMsjConsultaRubro> createElemSolicitudRubro(TipoMsjConsultaRubro value) {
        return new JAXBElement<TipoMsjConsultaRubro>(_ElemSolicitudRubro_QNAME, TipoMsjConsultaRubro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaConsultaEstudiante }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaConsultaEstudiante")
    public JAXBElement<TipoMsjRespuestaConsultaEstudiante> createElemRespuestaConsultaEstudiante(TipoMsjRespuestaConsultaEstudiante value) {
        return new JAXBElement<TipoMsjRespuestaConsultaEstudiante>(_ElemRespuestaConsultaEstudiante_QNAME, TipoMsjRespuestaConsultaEstudiante.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaEstadoNomina }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaEstadoNomina")
    public JAXBElement<TipoMsjRespuestaEstadoNomina> createElemRespuestaEstadoNomina(TipoMsjRespuestaEstadoNomina value) {
        return new JAXBElement<TipoMsjRespuestaEstadoNomina>(_ElemRespuestaEstadoNomina_QNAME, TipoMsjRespuestaEstadoNomina.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaHomologArea }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaHomologAreaResponsabilidad")
    public JAXBElement<TipoMsjRespuestaHomologArea> createElemRespuestaHomologAreaResponsabilidad(TipoMsjRespuestaHomologArea value) {
        return new JAXBElement<TipoMsjRespuestaHomologArea>(_ElemRespuestaHomologAreaResponsabilidad_QNAME, TipoMsjRespuestaHomologArea.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudContableNomina")
    public JAXBElement<Byte> createElemSolicitudContableNomina(Byte value) {
        return new JAXBElement<Byte>(_ElemSolicitudContableNomina_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudHomologConceptoPresupuesto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudHomologConceptoPresupuesto")
    public JAXBElement<TipoMsjSolicitudHomologConceptoPresupuesto> createElemSolicitudHomologConceptoPresupuesto(TipoMsjSolicitudHomologConceptoPresupuesto value) {
        return new JAXBElement<TipoMsjSolicitudHomologConceptoPresupuesto>(_ElemSolicitudHomologConceptoPresupuesto_QNAME, TipoMsjSolicitudHomologConceptoPresupuesto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaRegistroCuentaNomina }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaRegistroCuentaNomina")
    public JAXBElement<TipoMsjRespuestaRegistroCuentaNomina> createElemRespuestaRegistroCuentaNomina(TipoMsjRespuestaRegistroCuentaNomina value) {
        return new JAXBElement<TipoMsjRespuestaRegistroCuentaNomina>(_ElemRespuestaRegistroCuentaNomina_QNAME, TipoMsjRespuestaRegistroCuentaNomina.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudHomlogArea }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudHomologAreaResponsabilidad")
    public JAXBElement<TipoMsjSolicitudHomlogArea> createElemSolicitudHomologAreaResponsabilidad(TipoMsjSolicitudHomlogArea value) {
        return new JAXBElement<TipoMsjSolicitudHomlogArea>(_ElemSolicitudHomologAreaResponsabilidad_QNAME, TipoMsjSolicitudHomlogArea.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaTercero")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemConsultaTercero(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemConsultaTercero_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaEmpleado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaEmpleado")
    public JAXBElement<TipoMsjRespuestaEmpleado> createElemRespuestaEmpleado(TipoMsjRespuestaEmpleado value) {
        return new JAXBElement<TipoMsjRespuestaEmpleado>(_ElemRespuestaEmpleado_QNAME, TipoMsjRespuestaEmpleado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaEmpleado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudCreacionDocente")
    public JAXBElement<TipoMsjRespuestaEmpleado> createElemSolicitudCreacionDocente(TipoMsjRespuestaEmpleado value) {
        return new JAXBElement<TipoMsjRespuestaEmpleado>(_ElemSolicitudCreacionDocente_QNAME, TipoMsjRespuestaEmpleado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudEstadoCargueNomina }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudEstadoCargueNomina")
    public JAXBElement<TipoMsjSolicitudEstadoCargueNomina> createElemSolicitudEstadoCargueNomina(TipoMsjSolicitudEstadoCargueNomina value) {
        return new JAXBElement<TipoMsjSolicitudEstadoCargueNomina>(_ElemSolicitudEstadoCargueNomina_QNAME, TipoMsjSolicitudEstadoCargueNomina.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudReplicaTercero")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemSolicitudReplicaTercero(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemSolicitudReplicaTercero_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaEmpleadoPorIdTercero")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemConsultaEmpleadoPorIdTercero(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemConsultaEmpleadoPorIdTercero_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorTipoVinculacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaPorTipoVinculacion")
    public JAXBElement<TipoMsjSolicitudPorTipoVinculacion> createElemConsultaPorTipoVinculacion(TipoMsjSolicitudPorTipoVinculacion value) {
        return new JAXBElement<TipoMsjSolicitudPorTipoVinculacion>(_ElemConsultaPorTipoVinculacion_QNAME, TipoMsjSolicitudPorTipoVinculacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaCabeceraContratos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCabeceraContratosNacional")
    public JAXBElement<TipoMsjRespuestaCabeceraContratos> createElemRespuestaCabeceraContratosNacional(TipoMsjRespuestaCabeceraContratos value) {
        return new JAXBElement<TipoMsjRespuestaCabeceraContratos>(_ElemRespuestaCabeceraContratosNacional_QNAME, TipoMsjRespuestaCabeceraContratos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudCreacionActualizacionTercero")
    public JAXBElement<TipoMsjTercero> createElemSolicitudCreacionActualizacionTercero(TipoMsjTercero value) {
        return new JAXBElement<TipoMsjTercero>(_ElemSolicitudCreacionActualizacionTercero_QNAME, TipoMsjTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaConsultaTercero")
    public JAXBElement<TipoMsjTercero> createElemRespuestaConsultaTercero(TipoMsjTercero value) {
        return new JAXBElement<TipoMsjTercero>(_ElemRespuestaConsultaTercero_QNAME, TipoMsjTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjrespuestaDetalleDocente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaDetalleDocente")
    public JAXBElement<TipoMsjrespuestaDetalleDocente> createElemRespuestaDetalleDocente(TipoMsjrespuestaDetalleDocente value) {
        return new JAXBElement<TipoMsjrespuestaDetalleDocente>(_ElemRespuestaDetalleDocente_QNAME, TipoMsjrespuestaDetalleDocente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaDetalleEstudiante }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaDetalleEstudiante")
    public JAXBElement<TipoMsjRespuestaDetalleEstudiante> createElemRespuestaDetalleEstudiante(TipoMsjRespuestaDetalleEstudiante value) {
        return new JAXBElement<TipoMsjRespuestaDetalleEstudiante>(_ElemRespuestaDetalleEstudiante_QNAME, TipoMsjRespuestaDetalleEstudiante.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaActualizacionTercero")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaActualizacionTercero(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaActualizacionTercero_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaExistencia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaExistenciaDocente")
    public JAXBElement<TipoMsjRespuestaExistencia> createElemRespuestaExistenciaDocente(TipoMsjRespuestaExistencia value) {
        return new JAXBElement<TipoMsjRespuestaExistencia>(_ElemRespuestaExistenciaDocente_QNAME, TipoMsjRespuestaExistencia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaHomologConceptoPresupuesto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaHomologConceptoPresupuesto")
    public JAXBElement<TipoMsjRespuestaHomologConceptoPresupuesto> createElemRespuestaHomologConceptoPresupuesto(TipoMsjRespuestaHomologConceptoPresupuesto value) {
        return new JAXBElement<TipoMsjRespuestaHomologConceptoPresupuesto>(_ElemRespuestaHomologConceptoPresupuesto_QNAME, TipoMsjRespuestaHomologConceptoPresupuesto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaReplicaTercero")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaReplicaTercero(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaReplicaTercero_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCreacionActualizacionTercero")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaCreacionActualizacionTercero(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaCreacionActualizacionTercero_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaEmpleado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudActualizacionDocente")
    public JAXBElement<TipoMsjRespuestaEmpleado> createElemSolicitudActualizacionDocente(TipoMsjRespuestaEmpleado value) {
        return new JAXBElement<TipoMsjRespuestaEmpleado>(_ElemSolicitudActualizacionDocente_QNAME, TipoMsjRespuestaEmpleado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjTerceroFinanciero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudActualizacionTercero")
    public JAXBElement<TipoMsjTerceroFinanciero> createElemSolicitudActualizacionTercero(TipoMsjTerceroFinanciero value) {
        return new JAXBElement<TipoMsjTerceroFinanciero>(_ElemSolicitudActualizacionTercero_QNAME, TipoMsjTerceroFinanciero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCreacionTercero")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaCreacionTercero(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaCreacionTercero_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaCabeceraContratos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCabeceraContratos")
    public JAXBElement<TipoMsjRespuestaCabeceraContratos> createElemRespuestaCabeceraContratos(TipoMsjRespuestaCabeceraContratos value) {
        return new JAXBElement<TipoMsjRespuestaCabeceraContratos>(_ElemRespuestaCabeceraContratos_QNAME, TipoMsjRespuestaCabeceraContratos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaRubro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaRubro")
    public JAXBElement<TipoMsjRespuestaRubro> createElemRespuestaRubro(TipoMsjRespuestaRubro value) {
        return new JAXBElement<TipoMsjRespuestaRubro>(_ElemRespuestaRubro_QNAME, TipoMsjRespuestaRubro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaExistencia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaExistenciaTercero")
    public JAXBElement<TipoMsjRespuestaExistencia> createElemRespuestaExistenciaTercero(TipoMsjRespuestaExistencia value) {
        return new JAXBElement<TipoMsjRespuestaExistencia>(_ElemRespuestaExistenciaTercero_QNAME, TipoMsjRespuestaExistencia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjCuentaBancariaTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudAdicionarCuentaBancaria")
    public JAXBElement<TipoMsjCuentaBancariaTercero> createElemSolicitudAdicionarCuentaBancaria(TipoMsjCuentaBancariaTercero value) {
        return new JAXBElement<TipoMsjCuentaBancariaTercero>(_ElemSolicitudAdicionarCuentaBancaria_QNAME, TipoMsjCuentaBancariaTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaCabeceraContrato")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemConsultaCabeceraContrato(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemConsultaCabeceraContrato_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjDetalleContratos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaConsultaDetalleEmpleado")
    public JAXBElement<TipoMsjDetalleContratos> createElemRespuestaConsultaDetalleEmpleado(TipoMsjDetalleContratos value) {
        return new JAXBElement<TipoMsjDetalleContratos>(_ElemRespuestaConsultaDetalleEmpleado_QNAME, TipoMsjDetalleContratos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaSolicitudContableNomina")
    public JAXBElement<TipoRespuestaOp> createElemRespuestaSolicitudContableNomina(TipoRespuestaOp value) {
        return new JAXBElement<TipoRespuestaOp>(_ElemRespuestaSolicitudContableNomina_QNAME, TipoRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaExistenciaTercero")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemConsultaExistenciaTercero(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemConsultaExistenciaTercero_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemExistenciaDocente")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemExistenciaDocente(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemExistenciaDocente_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoInfoCuenta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "InformacionCuentaNueva", scope = TipoMsjCuentaBancariaTercero.class)
    public JAXBElement<TipoInfoCuenta> createTipoMsjCuentaBancariaTerceroInformacionCuentaNueva(TipoInfoCuenta value) {
        return new JAXBElement<TipoInfoCuenta>(_TipoMsjCuentaBancariaTerceroInformacionCuentaNueva_QNAME, TipoInfoCuenta.class, TipoMsjCuentaBancariaTercero.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "idTercero", scope = TipoMsjCuentaBancariaTercero.class)
    public JAXBElement<TipoIdTercero> createTipoMsjCuentaBancariaTerceroIdTercero(TipoIdTercero value) {
        return new JAXBElement<TipoIdTercero>(_TipoMsjCuentaBancariaTerceroIdTercero_QNAME, TipoIdTercero.class, TipoMsjCuentaBancariaTercero.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoInfoCuenta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "InformacionCuentaAntigua", scope = TipoMsjCuentaBancariaTercero.class)
    public JAXBElement<TipoInfoCuenta> createTipoMsjCuentaBancariaTerceroInformacionCuentaAntigua(TipoInfoCuenta value) {
        return new JAXBElement<TipoInfoCuenta>(_TipoMsjCuentaBancariaTerceroInformacionCuentaAntigua_QNAME, TipoInfoCuenta.class, TipoMsjCuentaBancariaTercero.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "Empresa", scope = TipoMsjSolicitudHomlogArea.class)
    public JAXBElement<String> createTipoMsjSolicitudHomlogAreaEmpresa(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomlogAreaEmpresa_QNAME, String.class, TipoMsjSolicitudHomlogArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "NumeroItem", scope = TipoMsjSolicitudHomlogArea.class)
    public JAXBElement<String> createTipoMsjSolicitudHomlogAreaNumeroItem(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomlogAreaNumeroItem_QNAME, String.class, TipoMsjSolicitudHomlogArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoTipoRegistro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "TipoRegistro", scope = TipoMsjSolicitudHomlogArea.class)
    public JAXBElement<TipoTipoRegistro> createTipoMsjSolicitudHomlogAreaTipoRegistro(TipoTipoRegistro value) {
        return new JAXBElement<TipoTipoRegistro>(_TipoMsjSolicitudHomlogAreaTipoRegistro_QNAME, TipoTipoRegistro.class, TipoMsjSolicitudHomlogArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "Interface", scope = TipoMsjSolicitudHomlogArea.class)
    public JAXBElement<String> createTipoMsjSolicitudHomlogAreaInterface(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomlogAreaInterface_QNAME, String.class, TipoMsjSolicitudHomlogArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "CodigoConcepto", scope = TipoMsjConsultaRubro.class)
    public JAXBElement<String> createTipoMsjConsultaRubroCodigoConcepto(String value) {
        return new JAXBElement<String>(_TipoMsjConsultaRubroCodigoConcepto_QNAME, String.class, TipoMsjConsultaRubro.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "fecha", scope = TipoMsjConsultaRubro.class)
    public JAXBElement<XMLGregorianCalendar> createTipoMsjConsultaRubroFecha(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_TipoMsjConsultaRubroFecha_QNAME, XMLGregorianCalendar.class, TipoMsjConsultaRubro.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "CodigoArea", scope = TipoMsjConsultaRubro.class)
    public JAXBElement<String> createTipoMsjConsultaRubroCodigoArea(String value) {
        return new JAXBElement<String>(_TipoMsjConsultaRubroCodigoArea_QNAME, String.class, TipoMsjConsultaRubro.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "Proyecto", scope = TipoMsjConsultaRubro.class)
    public JAXBElement<String> createTipoMsjConsultaRubroProyecto(String value) {
        return new JAXBElement<String>(_TipoMsjConsultaRubroProyecto_QNAME, String.class, TipoMsjConsultaRubro.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "Empresa", scope = TipoMsjConsultaRubro.class)
    public JAXBElement<String> createTipoMsjConsultaRubroEmpresa(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomlogAreaEmpresa_QNAME, String.class, TipoMsjConsultaRubro.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "CodigoRecurso", scope = TipoMsjConsultaRubro.class)
    public JAXBElement<String> createTipoMsjConsultaRubroCodigoRecurso(String value) {
        return new JAXBElement<String>(_TipoMsjConsultaRubroCodigoRecurso_QNAME, String.class, TipoMsjConsultaRubro.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "Empresa", scope = TipoMsjSolicitudHomologConceptoPresupuesto.class)
    public JAXBElement<String> createTipoMsjSolicitudHomologConceptoPresupuestoEmpresa(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomlogAreaEmpresa_QNAME, String.class, TipoMsjSolicitudHomologConceptoPresupuesto.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "NumeroItem", scope = TipoMsjSolicitudHomologConceptoPresupuesto.class)
    public JAXBElement<String> createTipoMsjSolicitudHomologConceptoPresupuestoNumeroItem(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomlogAreaNumeroItem_QNAME, String.class, TipoMsjSolicitudHomologConceptoPresupuesto.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoTipoRegistro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "TipoRegistro", scope = TipoMsjSolicitudHomologConceptoPresupuesto.class)
    public JAXBElement<TipoTipoRegistro> createTipoMsjSolicitudHomologConceptoPresupuestoTipoRegistro(TipoTipoRegistro value) {
        return new JAXBElement<TipoTipoRegistro>(_TipoMsjSolicitudHomlogAreaTipoRegistro_QNAME, TipoTipoRegistro.class, TipoMsjSolicitudHomologConceptoPresupuesto.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "Interface", scope = TipoMsjSolicitudHomologConceptoPresupuesto.class)
    public JAXBElement<String> createTipoMsjSolicitudHomologConceptoPresupuestoInterface(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomlogAreaInterface_QNAME, String.class, TipoMsjSolicitudHomologConceptoPresupuesto.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "DescripcionAreaResponsabilidad", scope = TipoMsjRespuestaHomologArea.class)
    public JAXBElement<Object> createTipoMsjRespuestaHomologAreaDescripcionAreaResponsabilidad(Object value) {
        return new JAXBElement<Object>(_TipoMsjRespuestaHomologAreaDescripcionAreaResponsabilidad_QNAME, Object.class, TipoMsjRespuestaHomologArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "CodigoAreaResponsabilidad", scope = TipoMsjRespuestaHomologArea.class)
    public JAXBElement<Object> createTipoMsjRespuestaHomologAreaCodigoAreaResponsabilidad(Object value) {
        return new JAXBElement<Object>(_TipoMsjRespuestaHomologAreaCodigoAreaResponsabilidad_QNAME, Object.class, TipoMsjRespuestaHomologArea.class, value);
    }

}
