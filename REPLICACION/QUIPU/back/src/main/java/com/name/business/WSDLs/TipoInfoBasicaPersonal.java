
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for tipoInfoBasicaPersonal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoBasicaPersonal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *         &lt;element name="generoTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoGenerotercero"/>
 *         &lt;element name="nomTercero">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="nombrePersonaNatural" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombrePersonaNatural"/>
 *                   &lt;element name="nombrePersonaJuridica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombrePersonaJuridica"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="naturalezaTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNaturalezaTercero"/>
 *         &lt;element name="estadoCivil" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEstadoCivil"/>
 *         &lt;element name="fechaNacimiento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFecha"/>
 *         &lt;element name="lugarNacimiento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoUbicacion"/>
 *         &lt;element name="lugarResidencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoUbicacion"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoBasicaPersonal", propOrder = {
    "idTercero",
    "generoTercero",
    "nomTercero",
    "naturalezaTercero",
    "estadoCivil",
    "fechaNacimiento",
    "lugarNacimiento",
    "lugarResidencia"
})
public class TipoInfoBasicaPersonal {

    @XmlElement(required = true)
    protected TipoIdTercero idTercero;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoGenerotercero generoTercero;
    @XmlElement(required = true)
    protected TipoInfoBasicaPersonal.NomTercero nomTercero;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoNaturalezaTercero naturalezaTercero;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoEstadoCivil estadoCivil;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected String fechaNacimiento;
    @XmlElement(required = true)
    protected TipoUbicacion lugarNacimiento;
    @XmlElement(required = true)
    protected TipoUbicacion lugarResidencia;

    /**
     * Gets the value of the idTercero property.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getIdTercero() {
        return idTercero;
    }

    /**
     * Sets the value of the idTercero property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setIdTercero(TipoIdTercero value) {
        this.idTercero = value;
    }

    /**
     * Gets the value of the generoTercero property.
     * 
     * @return
     *     possible object is
     *     {@link TipoGenerotercero }
     *     
     */
    public TipoGenerotercero getGeneroTercero() {
        return generoTercero;
    }

    /**
     * Sets the value of the generoTercero property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoGenerotercero }
     *     
     */
    public void setGeneroTercero(TipoGenerotercero value) {
        this.generoTercero = value;
    }

    /**
     * Gets the value of the nomTercero property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPersonal.NomTercero }
     *     
     */
    public TipoInfoBasicaPersonal.NomTercero getNomTercero() {
        return nomTercero;
    }

    /**
     * Sets the value of the nomTercero property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPersonal.NomTercero }
     *     
     */
    public void setNomTercero(TipoInfoBasicaPersonal.NomTercero value) {
        this.nomTercero = value;
    }

    /**
     * Gets the value of the naturalezaTercero property.
     * 
     * @return
     *     possible object is
     *     {@link TipoNaturalezaTercero }
     *     
     */
    public TipoNaturalezaTercero getNaturalezaTercero() {
        return naturalezaTercero;
    }

    /**
     * Sets the value of the naturalezaTercero property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoNaturalezaTercero }
     *     
     */
    public void setNaturalezaTercero(TipoNaturalezaTercero value) {
        this.naturalezaTercero = value;
    }

    /**
     * Gets the value of the estadoCivil property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstadoCivil }
     *     
     */
    public TipoEstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * Sets the value of the estadoCivil property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstadoCivil }
     *     
     */
    public void setEstadoCivil(TipoEstadoCivil value) {
        this.estadoCivil = value;
    }

    /**
     * Gets the value of the fechaNacimiento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Sets the value of the fechaNacimiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaNacimiento(String value) {
        this.fechaNacimiento = value;
    }

    /**
     * Gets the value of the lugarNacimiento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoUbicacion }
     *     
     */
    public TipoUbicacion getLugarNacimiento() {
        return lugarNacimiento;
    }

    /**
     * Sets the value of the lugarNacimiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoUbicacion }
     *     
     */
    public void setLugarNacimiento(TipoUbicacion value) {
        this.lugarNacimiento = value;
    }

    /**
     * Gets the value of the lugarResidencia property.
     * 
     * @return
     *     possible object is
     *     {@link TipoUbicacion }
     *     
     */
    public TipoUbicacion getLugarResidencia() {
        return lugarResidencia;
    }

    /**
     * Sets the value of the lugarResidencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoUbicacion }
     *     
     */
    public void setLugarResidencia(TipoUbicacion value) {
        this.lugarResidencia = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="nombrePersonaNatural" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombrePersonaNatural"/>
     *         &lt;element name="nombrePersonaJuridica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombrePersonaJuridica"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nombrePersonaNatural",
        "nombrePersonaJuridica"
    })
    public static class NomTercero {

        protected TipoNombrePersonaNatural nombrePersonaNatural;
        protected String nombrePersonaJuridica;

        /**
         * Gets the value of the nombrePersonaNatural property.
         * 
         * @return
         *     possible object is
         *     {@link TipoNombrePersonaNatural }
         *     
         */
        public TipoNombrePersonaNatural getNombrePersonaNatural() {
            return nombrePersonaNatural;
        }

        /**
         * Sets the value of the nombrePersonaNatural property.
         * 
         * @param value
         *     allowed object is
         *     {@link TipoNombrePersonaNatural }
         *     
         */
        public void setNombrePersonaNatural(TipoNombrePersonaNatural value) {
            this.nombrePersonaNatural = value;
        }

        /**
         * Gets the value of the nombrePersonaJuridica property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombrePersonaJuridica() {
            return nombrePersonaJuridica;
        }

        /**
         * Sets the value of the nombrePersonaJuridica property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombrePersonaJuridica(String value) {
            this.nombrePersonaJuridica = value;
        }

    }

}
