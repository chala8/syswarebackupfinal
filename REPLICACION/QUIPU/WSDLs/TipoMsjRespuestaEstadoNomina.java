
package WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjRespuestaEstadoNomina complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaEstadoNomina">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="codigoCargue" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="EstadoCargue" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEstadoCargueNomina"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaEstadoNomina", propOrder = {
    "codigoCargue",
    "estadoCargue"
})
public class TipoMsjRespuestaEstadoNomina
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected Object codigoCargue;
    @XmlElement(name = "EstadoCargue", required = true)
    @XmlSchemaType(name = "string")
    protected TipoEstadoCargueNomina estadoCargue;

    /**
     * Gets the value of the codigoCargue property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCodigoCargue() {
        return codigoCargue;
    }

    /**
     * Sets the value of the codigoCargue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCodigoCargue(Object value) {
        this.codigoCargue = value;
    }

    /**
     * Gets the value of the estadoCargue property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstadoCargueNomina }
     *     
     */
    public TipoEstadoCargueNomina getEstadoCargue() {
        return estadoCargue;
    }

    /**
     * Sets the value of the estadoCargue property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstadoCargueNomina }
     *     
     */
    public void setEstadoCargue(TipoEstadoCargueNomina value) {
        this.estadoCargue = value;
    }

}
