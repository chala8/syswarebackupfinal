
package WSDLs;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoRubroPresupuestal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoRubroPresupuestal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Codigo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoRubro"/>
 *         &lt;element name="NombreRubro" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombreRubro"/>
 *         &lt;element name="ValorDisponibilidad" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoValorRubro"/>
 *         &lt;element name="ValorApropiacionDefinitiva" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoValorApropiacionDef"/>
 *         &lt;element name="PorcentajeEjecucion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoPorcentajeEjecucion"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoRubroPresupuestal", propOrder = {
    "codigo",
    "nombreRubro",
    "valorDisponibilidad",
    "valorApropiacionDefinitiva",
    "porcentajeEjecucion"
})
public class TipoRubroPresupuestal {

    @XmlElement(name = "Codigo", required = true)
    protected String codigo;
    @XmlElement(name = "NombreRubro", required = true)
    protected String nombreRubro;
    @XmlElement(name = "ValorDisponibilidad", required = true)
    protected BigDecimal valorDisponibilidad;
    @XmlElement(name = "ValorApropiacionDefinitiva", required = true)
    protected BigDecimal valorApropiacionDefinitiva;
    @XmlElement(name = "PorcentajeEjecucion", required = true)
    protected BigDecimal porcentajeEjecucion;

    /**
     * Gets the value of the codigo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Sets the value of the codigo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Gets the value of the nombreRubro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreRubro() {
        return nombreRubro;
    }

    /**
     * Sets the value of the nombreRubro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreRubro(String value) {
        this.nombreRubro = value;
    }

    /**
     * Gets the value of the valorDisponibilidad property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorDisponibilidad() {
        return valorDisponibilidad;
    }

    /**
     * Sets the value of the valorDisponibilidad property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorDisponibilidad(BigDecimal value) {
        this.valorDisponibilidad = value;
    }

    /**
     * Gets the value of the valorApropiacionDefinitiva property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorApropiacionDefinitiva() {
        return valorApropiacionDefinitiva;
    }

    /**
     * Sets the value of the valorApropiacionDefinitiva property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorApropiacionDefinitiva(BigDecimal value) {
        this.valorApropiacionDefinitiva = value;
    }

    /**
     * Gets the value of the porcentajeEjecucion property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPorcentajeEjecucion() {
        return porcentajeEjecucion;
    }

    /**
     * Sets the value of the porcentajeEjecucion property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPorcentajeEjecucion(BigDecimal value) {
        this.porcentajeEjecucion = value;
    }

}
