
package WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjTercero complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjTercero">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Tercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTercero"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjTercero", propOrder = {
    "tercero"
})
public class TipoMsjTercero
    extends TipoMsjGenerico
{

    @XmlElement(name = "Tercero", required = true)
    protected List<TipoTercero> tercero;

    /**
     * Gets the value of the tercero property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tercero property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTercero().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoTercero }
     * 
     * 
     */
    public List<TipoTercero> getTercero() {
        if (tercero == null) {
            tercero = new ArrayList<TipoTercero>();
        }
        return this.tercero;
    }

}
