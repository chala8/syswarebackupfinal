
package WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoNaturalezaTercero.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoNaturalezaTercero">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NAT"/>
 *     &lt;enumeration value="JUR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoNaturalezaTercero")
@XmlEnum
public enum TipoNaturalezaTercero {

    NAT,
    JUR;

    public String value() {
        return name();
    }

    public static TipoNaturalezaTercero fromValue(String v) {
        return valueOf(v);
    }

}
