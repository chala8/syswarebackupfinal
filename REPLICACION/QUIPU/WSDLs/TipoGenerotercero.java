
package WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoGenerotercero.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoGenerotercero">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="F"/>
 *     &lt;enumeration value="M"/>
 *     &lt;enumeration value="O"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoGenerotercero")
@XmlEnum
public enum TipoGenerotercero {


    /**
     * Genero Femenino
     * 
     */
    F,

    /**
     * Genero Masculino
     * 
     */
    M,

    /**
     * Otro
     * 
     */
    O;

    public String value() {
        return name();
    }

    public static TipoGenerotercero fromValue(String v) {
        return valueOf(v);
    }

}
