
package WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoInfoContacto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoContacto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ciudadContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdCiudad"/>
 *         &lt;element name="telefonoContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNumeroTelefono"/>
 *         &lt;element name="correoElectronicoContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCorreoE"/>
 *         &lt;element name="direccionContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDireccionFisica"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoContacto", propOrder = {
    "ciudadContacto",
    "telefonoContacto",
    "correoElectronicoContacto",
    "direccionContacto"
})
public class TipoInfoContacto {

    @XmlElement(required = true)
    protected String ciudadContacto;
    @XmlElement(required = true)
    protected String telefonoContacto;
    @XmlElement(required = true)
    protected String correoElectronicoContacto;
    @XmlElement(required = true)
    protected String direccionContacto;

    /**
     * Gets the value of the ciudadContacto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadContacto() {
        return ciudadContacto;
    }

    /**
     * Sets the value of the ciudadContacto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadContacto(String value) {
        this.ciudadContacto = value;
    }

    /**
     * Gets the value of the telefonoContacto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    /**
     * Sets the value of the telefonoContacto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefonoContacto(String value) {
        this.telefonoContacto = value;
    }

    /**
     * Gets the value of the correoElectronicoContacto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoElectronicoContacto() {
        return correoElectronicoContacto;
    }

    /**
     * Sets the value of the correoElectronicoContacto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoElectronicoContacto(String value) {
        this.correoElectronicoContacto = value;
    }

    /**
     * Gets the value of the direccionContacto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccionContacto() {
        return direccionContacto;
    }

    /**
     * Sets the value of the direccionContacto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccionContacto(String value) {
        this.direccionContacto = value;
    }

}
