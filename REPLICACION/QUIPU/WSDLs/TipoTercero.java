
package WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoTercero complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoTercero">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infoBasicaTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaPersonal"/>
 *         &lt;element name="infoContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoContacto"/>
 *         &lt;element name="infoTributaria" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoTributariaTercero" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoTercero", propOrder = {
    "infoBasicaTercero",
    "infoContacto",
    "infoTributaria"
})
@XmlSeeAlso({
    TipoEstudiante.class,
    TipoEmpleado.class
})
public class TipoTercero {

    @XmlElement(required = true)
    protected TipoInfoBasicaPersonal infoBasicaTercero;
    @XmlElement(required = true)
    protected TipoInfoContacto infoContacto;
    protected TipoInfoTributariaTercero infoTributaria;

    /**
     * Gets the value of the infoBasicaTercero property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPersonal }
     *     
     */
    public TipoInfoBasicaPersonal getInfoBasicaTercero() {
        return infoBasicaTercero;
    }

    /**
     * Sets the value of the infoBasicaTercero property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPersonal }
     *     
     */
    public void setInfoBasicaTercero(TipoInfoBasicaPersonal value) {
        this.infoBasicaTercero = value;
    }

    /**
     * Gets the value of the infoContacto property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoContacto }
     *     
     */
    public TipoInfoContacto getInfoContacto() {
        return infoContacto;
    }

    /**
     * Sets the value of the infoContacto property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoContacto }
     *     
     */
    public void setInfoContacto(TipoInfoContacto value) {
        this.infoContacto = value;
    }

    /**
     * Gets the value of the infoTributaria property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoTributariaTercero }
     *     
     */
    public TipoInfoTributariaTercero getInfoTributaria() {
        return infoTributaria;
    }

    /**
     * Sets the value of the infoTributaria property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoTributariaTercero }
     *     
     */
    public void setInfoTributaria(TipoInfoTributariaTercero value) {
        this.infoTributaria = value;
    }

}
