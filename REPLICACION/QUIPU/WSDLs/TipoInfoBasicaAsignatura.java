
package WSDLs;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoInfoBasicaAsignatura complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoBasicaAsignatura">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoAsignatura" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoAsignatura"/>
 *         &lt;element name="NombreAsignatura" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDescripcionAsignatura"/>
 *         &lt;element name="CreditosAsignatura" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCreditosAsignatura"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoBasicaAsignatura", propOrder = {
    "codigoAsignatura",
    "nombreAsignatura",
    "creditosAsignatura"
})
public class TipoInfoBasicaAsignatura {

    @XmlElement(name = "CodigoAsignatura", required = true)
    protected String codigoAsignatura;
    @XmlElement(name = "NombreAsignatura", required = true)
    protected String nombreAsignatura;
    @XmlElement(name = "CreditosAsignatura", required = true)
    protected BigDecimal creditosAsignatura;

    /**
     * Gets the value of the codigoAsignatura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    /**
     * Sets the value of the codigoAsignatura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAsignatura(String value) {
        this.codigoAsignatura = value;
    }

    /**
     * Gets the value of the nombreAsignatura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreAsignatura() {
        return nombreAsignatura;
    }

    /**
     * Sets the value of the nombreAsignatura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreAsignatura(String value) {
        this.nombreAsignatura = value;
    }

    /**
     * Gets the value of the creditosAsignatura property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditosAsignatura() {
        return creditosAsignatura;
    }

    /**
     * Sets the value of the creditosAsignatura property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditosAsignatura(BigDecimal value) {
        this.creditosAsignatura = value;
    }

}
