
package WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for tipoDetalleContrato complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoDetalleContrato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="asignacionBasica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMontoMonetario"/>
 *         &lt;element name="cargoEmpleado" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCargoEmpleado"/>
 *         &lt;element name="fechaInicio" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFecha"/>
 *         &lt;element name="fechaFin" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFechaVacio"/>
 *         &lt;element name="dedicacion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDedicacion"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoDetalleContrato", propOrder = {
    "asignacionBasica",
    "cargoEmpleado",
    "fechaInicio",
    "fechaFin",
    "dedicacion"
})
public class TipoDetalleContrato {

    @XmlElement(required = true)
    protected TipoMontoMonetario asignacionBasica;
    @XmlElement(required = true)
    protected String cargoEmpleado;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaInicio;
    @XmlElement(required = true)
    protected String fechaFin;
    protected double dedicacion;

    /**
     * Gets the value of the asignacionBasica property.
     * 
     * @return
     *     possible object is
     *     {@link TipoMontoMonetario }
     *     
     */
    public TipoMontoMonetario getAsignacionBasica() {
        return asignacionBasica;
    }

    /**
     * Sets the value of the asignacionBasica property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoMontoMonetario }
     *     
     */
    public void setAsignacionBasica(TipoMontoMonetario value) {
        this.asignacionBasica = value;
    }

    /**
     * Gets the value of the cargoEmpleado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCargoEmpleado() {
        return cargoEmpleado;
    }

    /**
     * Sets the value of the cargoEmpleado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCargoEmpleado(String value) {
        this.cargoEmpleado = value;
    }

    /**
     * Gets the value of the fechaInicio property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Sets the value of the fechaInicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaInicio(XMLGregorianCalendar value) {
        this.fechaInicio = value;
    }

    /**
     * Gets the value of the fechaFin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaFin() {
        return fechaFin;
    }

    /**
     * Sets the value of the fechaFin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaFin(String value) {
        this.fechaFin = value;
    }

    /**
     * Gets the value of the dedicacion property.
     * 
     */
    public double getDedicacion() {
        return dedicacion;
    }

    /**
     * Sets the value of the dedicacion property.
     * 
     */
    public void setDedicacion(double value) {
        this.dedicacion = value;
    }

}
