
package WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoPlanEstudios complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoPlanEstudios">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InfoBasicaPlanEstudios" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaPlanEstudios"/>
 *         &lt;element name="InfoDetallePlanEstudios" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoDetallePlanEstudios"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoPlanEstudios", propOrder = {
    "infoBasicaPlanEstudios",
    "infoDetallePlanEstudios"
})
public class TipoPlanEstudios {

    @XmlElement(name = "InfoBasicaPlanEstudios", required = true)
    protected TipoInfoBasicaPlanEstudios infoBasicaPlanEstudios;
    @XmlElement(name = "InfoDetallePlanEstudios", required = true)
    protected TipoInfoDetallePlanEstudios infoDetallePlanEstudios;

    /**
     * Gets the value of the infoBasicaPlanEstudios property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPlanEstudios }
     *     
     */
    public TipoInfoBasicaPlanEstudios getInfoBasicaPlanEstudios() {
        return infoBasicaPlanEstudios;
    }

    /**
     * Sets the value of the infoBasicaPlanEstudios property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPlanEstudios }
     *     
     */
    public void setInfoBasicaPlanEstudios(TipoInfoBasicaPlanEstudios value) {
        this.infoBasicaPlanEstudios = value;
    }

    /**
     * Gets the value of the infoDetallePlanEstudios property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoDetallePlanEstudios }
     *     
     */
    public TipoInfoDetallePlanEstudios getInfoDetallePlanEstudios() {
        return infoDetallePlanEstudios;
    }

    /**
     * Sets the value of the infoDetallePlanEstudios property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoDetallePlanEstudios }
     *     
     */
    public void setInfoDetallePlanEstudios(TipoInfoDetallePlanEstudios value) {
        this.infoDetallePlanEstudios = value;
    }

}
