
package WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoEstadoPlanEstudios.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoEstadoPlanEstudios">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="registrado"/>
 *     &lt;enumeration value="inscrito"/>
 *     &lt;enumeration value="matriculado "/>
 *     &lt;enumeration value="retirado"/>
 *     &lt;enumeration value="admitido"/>
 *     &lt;enumeration value="aplaza ingreso"/>
 *     &lt;enumeration value="Cancelacion semestre"/>
 *     &lt;enumeration value="Bloqueado"/>
 *     &lt;enumeration value="reserva de cupo"/>
 *     &lt;enumeration value="traslado"/>
 *     &lt;enumeration value="Graduado"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoEstadoPlanEstudios")
@XmlEnum
public enum TipoEstadoPlanEstudios {

    @XmlEnumValue("registrado")
    REGISTRADO("registrado"),
    @XmlEnumValue("inscrito")
    INSCRITO("inscrito"),
    @XmlEnumValue("matriculado ")
    MATRICULADO("matriculado "),
    @XmlEnumValue("retirado")
    RETIRADO("retirado"),
    @XmlEnumValue("admitido")
    ADMITIDO("admitido"),
    @XmlEnumValue("aplaza ingreso")
    APLAZA_INGRESO("aplaza ingreso"),
    @XmlEnumValue("Cancelacion semestre")
    CANCELACION_SEMESTRE("Cancelacion semestre"),
    @XmlEnumValue("Bloqueado")
    BLOQUEADO("Bloqueado"),
    @XmlEnumValue("reserva de cupo")
    RESERVA_DE_CUPO("reserva de cupo"),
    @XmlEnumValue("traslado")
    TRASLADO("traslado"),
    @XmlEnumValue("Graduado")
    GRADUADO("Graduado");
    private final String value;

    TipoEstadoPlanEstudios(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoEstadoPlanEstudios fromValue(String v) {
        for (TipoEstadoPlanEstudios c: TipoEstadoPlanEstudios.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
