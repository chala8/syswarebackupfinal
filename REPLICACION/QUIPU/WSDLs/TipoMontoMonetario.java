
package WSDLs;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMontoMonetario complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMontoMonetario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idMoneda" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdMoneda"/>
 *         &lt;element name="valor" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoValorMonetario"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMontoMonetario", propOrder = {
    "idMoneda",
    "valor"
})
public class TipoMontoMonetario {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoIdMoneda idMoneda;
    @XmlElement(required = true)
    protected BigDecimal valor;

    /**
     * Gets the value of the idMoneda property.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdMoneda }
     *     
     */
    public TipoIdMoneda getIdMoneda() {
        return idMoneda;
    }

    /**
     * Sets the value of the idMoneda property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdMoneda }
     *     
     */
    public void setIdMoneda(TipoIdMoneda value) {
        this.idMoneda = value;
    }

    /**
     * Gets the value of the valor property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Sets the value of the valor property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValor(BigDecimal value) {
        this.valor = value;
    }

}
