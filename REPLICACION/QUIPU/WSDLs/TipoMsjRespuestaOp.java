
package WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjRespuestaOp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaOp">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="respuestaOP" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoRespuestaOp"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaOp", propOrder = {
    "respuestaOP"
})
public class TipoMsjRespuestaOp
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected TipoRespuestaOp respuestaOP;

    /**
     * Gets the value of the respuestaOP property.
     * 
     * @return
     *     possible object is
     *     {@link TipoRespuestaOp }
     *     
     */
    public TipoRespuestaOp getRespuestaOP() {
        return respuestaOP;
    }

    /**
     * Sets the value of the respuestaOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoRespuestaOp }
     *     
     */
    public void setRespuestaOP(TipoRespuestaOp value) {
        this.respuestaOP = value;
    }

}
