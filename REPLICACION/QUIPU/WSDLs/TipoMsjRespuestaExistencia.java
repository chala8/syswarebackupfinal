
package WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjRespuestaExistencia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaExistencia">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="confirmacionExistencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoConfirmacionExistencia"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaExistencia", propOrder = {
    "confirmacionExistencia"
})
public class TipoMsjRespuestaExistencia
    extends TipoMsjGenerico
{

    protected boolean confirmacionExistencia;

    /**
     * Gets the value of the confirmacionExistencia property.
     * 
     */
    public boolean isConfirmacionExistencia() {
        return confirmacionExistencia;
    }

    /**
     * Sets the value of the confirmacionExistencia property.
     * 
     */
    public void setConfirmacionExistencia(boolean value) {
        this.confirmacionExistencia = value;
    }

}
