
package WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjRespuestaContratos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaContratos">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Contrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoContratoTercero"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaContratos", propOrder = {
    "contrato"
})
public class TipoMsjRespuestaContratos
    extends TipoMsjGenerico
{

    @XmlElement(name = "Contrato", required = true)
    protected List<TipoContratoTercero> contrato;

    /**
     * Gets the value of the contrato property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contrato property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContrato().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoContratoTercero }
     * 
     * 
     */
    public List<TipoContratoTercero> getContrato() {
        if (contrato == null) {
            contrato = new ArrayList<TipoContratoTercero>();
        }
        return this.contrato;
    }

}
