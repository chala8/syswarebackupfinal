
package WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjSolicitudEstadoCargueNomina complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudEstadoCargueNomina">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="codigoCargueNomina" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoCargaNomina"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudEstadoCargueNomina", propOrder = {
    "codigoCargueNomina"
})
public class TipoMsjSolicitudEstadoCargueNomina
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected String codigoCargueNomina;

    /**
     * Gets the value of the codigoCargueNomina property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCargueNomina() {
        return codigoCargueNomina;
    }

    /**
     * Sets the value of the codigoCargueNomina property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCargueNomina(String value) {
        this.codigoCargueNomina = value;
    }

}
