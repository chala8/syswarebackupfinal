
package WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjSolicitudHomlogArea complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudHomlogArea">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Interface" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoInterface"/>
 *         &lt;element name="Empresa" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEmpresa"/>
 *         &lt;element name="NumeroItem" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNumeroItem"/>
 *         &lt;element name="TipoRegistro" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoRegistro"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudHomlogArea", propOrder = {
    "interfaceAndEmpresaAndNumeroItem"
})
public class TipoMsjSolicitudHomlogArea
    extends TipoMsjGenerico
{

    @XmlElementRefs({
        @XmlElementRef(name = "TipoRegistro", namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", type = JAXBElement.class),
        @XmlElementRef(name = "Empresa", namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", type = JAXBElement.class),
        @XmlElementRef(name = "NumeroItem", namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", type = JAXBElement.class),
        @XmlElementRef(name = "Interface", namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", type = JAXBElement.class)
    })
    protected List<JAXBElement<?>> interfaceAndEmpresaAndNumeroItem;

    /**
     * Gets the value of the interfaceAndEmpresaAndNumeroItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the interfaceAndEmpresaAndNumeroItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInterfaceAndEmpresaAndNumeroItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link TipoTipoRegistro }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * 
     */
    public List<JAXBElement<?>> getInterfaceAndEmpresaAndNumeroItem() {
        if (interfaceAndEmpresaAndNumeroItem == null) {
            interfaceAndEmpresaAndNumeroItem = new ArrayList<JAXBElement<?>>();
        }
        return this.interfaceAndEmpresaAndNumeroItem;
    }

}
