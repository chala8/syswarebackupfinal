
package WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjRespuestaHomologConceptoPresupuesto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaHomologConceptoPresupuesto">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="ConceptoPresupuesto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoConceptoPresupuesto"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaHomologConceptoPresupuesto", propOrder = {
    "conceptoPresupuesto"
})
public class TipoMsjRespuestaHomologConceptoPresupuesto
    extends TipoMsjGenerico
{

    @XmlElement(name = "ConceptoPresupuesto", required = true)
    protected TipoConceptoPresupuesto conceptoPresupuesto;

    /**
     * Gets the value of the conceptoPresupuesto property.
     * 
     * @return
     *     possible object is
     *     {@link TipoConceptoPresupuesto }
     *     
     */
    public TipoConceptoPresupuesto getConceptoPresupuesto() {
        return conceptoPresupuesto;
    }

    /**
     * Sets the value of the conceptoPresupuesto property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoConceptoPresupuesto }
     *     
     */
    public void setConceptoPresupuesto(TipoConceptoPresupuesto value) {
        this.conceptoPresupuesto = value;
    }

}
