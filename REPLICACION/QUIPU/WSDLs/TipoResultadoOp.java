
package WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoResultadoOp.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoResultadoOp">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EXITO"/>
 *     &lt;enumeration value="FALLO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoResultadoOp")
@XmlEnum
public enum TipoResultadoOp {

    EXITO,
    FALLO;

    public String value() {
        return name();
    }

    public static TipoResultadoOp fromValue(String v) {
        return valueOf(v);
    }

}
