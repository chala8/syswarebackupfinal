
package WSDLs;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoInfoBasicaPlanEstudios complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoBasicaPlanEstudios">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoPlan" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoPlanEstudios"/>
 *         &lt;element name="DescripcionPlan" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDescripcionPlanEstudios"/>
 *         &lt;element name="Promedio" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoPromedio"/>
 *         &lt;element name="PromedioAcumulado" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoPromedioAcumulado"/>
 *         &lt;element name="Estado" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEstadoPlanEstudios"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoBasicaPlanEstudios", propOrder = {
    "codigoPlan",
    "descripcionPlan",
    "promedio",
    "promedioAcumulado",
    "estado"
})
public class TipoInfoBasicaPlanEstudios {

    @XmlElement(name = "CodigoPlan", required = true)
    protected String codigoPlan;
    @XmlElement(name = "DescripcionPlan", required = true)
    protected String descripcionPlan;
    @XmlElement(name = "Promedio", required = true)
    protected BigDecimal promedio;
    @XmlElement(name = "PromedioAcumulado", required = true)
    protected BigDecimal promedioAcumulado;
    @XmlElement(name = "Estado", required = true)
    @XmlSchemaType(name = "string")
    protected TipoEstadoPlanEstudios estado;

    /**
     * Gets the value of the codigoPlan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPlan() {
        return codigoPlan;
    }

    /**
     * Sets the value of the codigoPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPlan(String value) {
        this.codigoPlan = value;
    }

    /**
     * Gets the value of the descripcionPlan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionPlan() {
        return descripcionPlan;
    }

    /**
     * Sets the value of the descripcionPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionPlan(String value) {
        this.descripcionPlan = value;
    }

    /**
     * Gets the value of the promedio property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPromedio() {
        return promedio;
    }

    /**
     * Sets the value of the promedio property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPromedio(BigDecimal value) {
        this.promedio = value;
    }

    /**
     * Gets the value of the promedioAcumulado property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPromedioAcumulado() {
        return promedioAcumulado;
    }

    /**
     * Sets the value of the promedioAcumulado property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPromedioAcumulado(BigDecimal value) {
        this.promedioAcumulado = value;
    }

    /**
     * Gets the value of the estado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstadoPlanEstudios }
     *     
     */
    public TipoEstadoPlanEstudios getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstadoPlanEstudios }
     *     
     */
    public void setEstado(TipoEstadoPlanEstudios value) {
        this.estado = value;
    }

}
