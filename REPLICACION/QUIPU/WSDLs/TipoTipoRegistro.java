
package WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoTipoRegistro.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoTipoRegistro">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NOMINA"/>
 *     &lt;enumeration value="DESCUENTO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoTipoRegistro")
@XmlEnum
public enum TipoTipoRegistro {

    NOMINA,
    DESCUENTO;

    public String value() {
        return name();
    }

    public static TipoTipoRegistro fromValue(String v) {
        return valueOf(v);
    }

}
