
package WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMsjrespuestaDetalleDocente complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjrespuestaDetalleDocente">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="DetalleDocente" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDetalleDocente"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjrespuestaDetalleDocente", propOrder = {
    "detalleDocente"
})
public class TipoMsjrespuestaDetalleDocente
    extends TipoMsjGenerico
{

    @XmlElement(name = "DetalleDocente", required = true)
    protected List<TipoDetalleDocente> detalleDocente;

    /**
     * Gets the value of the detalleDocente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the detalleDocente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDetalleDocente().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoDetalleDocente }
     * 
     * 
     */
    public List<TipoDetalleDocente> getDetalleDocente() {
        if (detalleDocente == null) {
            detalleDocente = new ArrayList<TipoDetalleDocente>();
        }
        return this.detalleDocente;
    }

}
