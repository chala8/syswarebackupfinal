
package WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoDetalleDocente complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoDetalleDocente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Asignatura" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoAsignatura"/>
 *         &lt;element name="NumVecesDictada" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNumVecesAsignaturas"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoDetalleDocente", propOrder = {
    "asignatura",
    "numVecesDictada"
})
public class TipoDetalleDocente {

    @XmlElement(name = "Asignatura", required = true)
    protected TipoAsignatura asignatura;
    @XmlElement(name = "NumVecesDictada")
    protected long numVecesDictada;

    /**
     * Gets the value of the asignatura property.
     * 
     * @return
     *     possible object is
     *     {@link TipoAsignatura }
     *     
     */
    public TipoAsignatura getAsignatura() {
        return asignatura;
    }

    /**
     * Sets the value of the asignatura property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoAsignatura }
     *     
     */
    public void setAsignatura(TipoAsignatura value) {
        this.asignatura = value;
    }

    /**
     * Gets the value of the numVecesDictada property.
     * 
     */
    public long getNumVecesDictada() {
        return numVecesDictada;
    }

    /**
     * Sets the value of the numVecesDictada property.
     * 
     */
    public void setNumVecesDictada(long value) {
        this.numVecesDictada = value;
    }

}
