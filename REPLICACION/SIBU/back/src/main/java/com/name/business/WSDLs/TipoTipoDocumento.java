
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoTipoDocumento.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoTipoDocumento">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CC"/>
 *     &lt;enumeration value="TI"/>
 *     &lt;enumeration value="CE"/>
 *     &lt;enumeration value="PA"/>
 *     &lt;enumeration value="RC"/>
 *     &lt;enumeration value="NI"/>
 *     &lt;enumeration value="IV"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoTipoDocumento")
@XmlEnum
public enum TipoTipoDocumento {


    /**
     * Cedula de Ciudadania
     * 
     */
    CC,

    /**
     * Tarjeta de Identidad
     * 
     */
    TI,

    /**
     * Cedula de Extranjeria
     * 
     */
    CE,

    /**
     * Pasaporte
     * 
     */
    PA,

    /**
     * Registro Civil
     * 
     */
    RC,

    /**
     * Numero de Indentificacion Tributaria
     * 
     */
    NI,

    /**
     * Carnet de Identidad Por Visa
     * 
     */
    IV;

    public String value() {
        return name();
    }

    public static TipoTipoDocumento fromValue(String v) {
        return valueOf(v);
    }

}
