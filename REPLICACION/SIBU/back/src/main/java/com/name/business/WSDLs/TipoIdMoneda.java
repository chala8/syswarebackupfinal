
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoIdMoneda.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoIdMoneda">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AED"/>
 *     &lt;enumeration value="AFN"/>
 *     &lt;enumeration value="ALL"/>
 *     &lt;enumeration value="AMD"/>
 *     &lt;enumeration value="ANG"/>
 *     &lt;enumeration value="AOA"/>
 *     &lt;enumeration value="ARS"/>
 *     &lt;enumeration value="AUD"/>
 *     &lt;enumeration value="AWG"/>
 *     &lt;enumeration value="AZN"/>
 *     &lt;enumeration value="BAM"/>
 *     &lt;enumeration value="BBD"/>
 *     &lt;enumeration value="BDT"/>
 *     &lt;enumeration value="BGN"/>
 *     &lt;enumeration value="BHD"/>
 *     &lt;enumeration value="BIF"/>
 *     &lt;enumeration value="BMD"/>
 *     &lt;enumeration value="BND"/>
 *     &lt;enumeration value="BOB"/>
 *     &lt;enumeration value="BRL"/>
 *     &lt;enumeration value="BSD"/>
 *     &lt;enumeration value="BTN"/>
 *     &lt;enumeration value="BWP"/>
 *     &lt;enumeration value="BYR"/>
 *     &lt;enumeration value="BZD"/>
 *     &lt;enumeration value="CAD"/>
 *     &lt;enumeration value="CDF"/>
 *     &lt;enumeration value="CHF"/>
 *     &lt;enumeration value="CLP"/>
 *     &lt;enumeration value="CNY"/>
 *     &lt;enumeration value="COP"/>
 *     &lt;enumeration value="CRC"/>
 *     &lt;enumeration value="CUC"/>
 *     &lt;enumeration value="CUP"/>
 *     &lt;enumeration value="CVE"/>
 *     &lt;enumeration value="CZK"/>
 *     &lt;enumeration value="DJF"/>
 *     &lt;enumeration value="DKK"/>
 *     &lt;enumeration value="DOP"/>
 *     &lt;enumeration value="DZD"/>
 *     &lt;enumeration value="EGP"/>
 *     &lt;enumeration value="ERN"/>
 *     &lt;enumeration value="ETB"/>
 *     &lt;enumeration value="EUR"/>
 *     &lt;enumeration value="FJD"/>
 *     &lt;enumeration value="FKP"/>
 *     &lt;enumeration value="GBP"/>
 *     &lt;enumeration value="GEL"/>
 *     &lt;enumeration value="GGP"/>
 *     &lt;enumeration value="GHS"/>
 *     &lt;enumeration value="GIP"/>
 *     &lt;enumeration value="GMD"/>
 *     &lt;enumeration value="GNF"/>
 *     &lt;enumeration value="GTQ"/>
 *     &lt;enumeration value="GYD"/>
 *     &lt;enumeration value="HKD"/>
 *     &lt;enumeration value="HNL"/>
 *     &lt;enumeration value="HRK"/>
 *     &lt;enumeration value="HTG"/>
 *     &lt;enumeration value="HUF"/>
 *     &lt;enumeration value="IDR"/>
 *     &lt;enumeration value="ILS"/>
 *     &lt;enumeration value="IMP"/>
 *     &lt;enumeration value="INR"/>
 *     &lt;enumeration value="IQD"/>
 *     &lt;enumeration value="IRR"/>
 *     &lt;enumeration value="ISK"/>
 *     &lt;enumeration value="JEP"/>
 *     &lt;enumeration value="JMD"/>
 *     &lt;enumeration value="JOD"/>
 *     &lt;enumeration value="JPY"/>
 *     &lt;enumeration value="KES"/>
 *     &lt;enumeration value="KGS"/>
 *     &lt;enumeration value="KHR"/>
 *     &lt;enumeration value="KMF"/>
 *     &lt;enumeration value="KPW"/>
 *     &lt;enumeration value="KRW"/>
 *     &lt;enumeration value="KWD"/>
 *     &lt;enumeration value="KYD"/>
 *     &lt;enumeration value="KZT"/>
 *     &lt;enumeration value="LAK"/>
 *     &lt;enumeration value="LBP"/>
 *     &lt;enumeration value="LKR"/>
 *     &lt;enumeration value="LRD"/>
 *     &lt;enumeration value="LSL"/>
 *     &lt;enumeration value="LTL"/>
 *     &lt;enumeration value="LYD"/>
 *     &lt;enumeration value="MAD"/>
 *     &lt;enumeration value="MDL"/>
 *     &lt;enumeration value="MGA"/>
 *     &lt;enumeration value="MKD"/>
 *     &lt;enumeration value="MMK"/>
 *     &lt;enumeration value="MNT"/>
 *     &lt;enumeration value="MOP"/>
 *     &lt;enumeration value="MRO"/>
 *     &lt;enumeration value="MUR"/>
 *     &lt;enumeration value="MVR"/>
 *     &lt;enumeration value="MWK"/>
 *     &lt;enumeration value="MXN"/>
 *     &lt;enumeration value="MYR"/>
 *     &lt;enumeration value="MZN"/>
 *     &lt;enumeration value="NAD"/>
 *     &lt;enumeration value="NGN"/>
 *     &lt;enumeration value="NIO"/>
 *     &lt;enumeration value="NOK"/>
 *     &lt;enumeration value="NPR"/>
 *     &lt;enumeration value="NZD"/>
 *     &lt;enumeration value="OMR"/>
 *     &lt;enumeration value="PAB"/>
 *     &lt;enumeration value="PEN"/>
 *     &lt;enumeration value="PGK"/>
 *     &lt;enumeration value="PHP"/>
 *     &lt;enumeration value="PKR"/>
 *     &lt;enumeration value="PLN"/>
 *     &lt;enumeration value="PYG"/>
 *     &lt;enumeration value="QAR"/>
 *     &lt;enumeration value="RON"/>
 *     &lt;enumeration value="RSD"/>
 *     &lt;enumeration value="RUB"/>
 *     &lt;enumeration value="RWF"/>
 *     &lt;enumeration value="SAR"/>
 *     &lt;enumeration value="SBD"/>
 *     &lt;enumeration value="SCR"/>
 *     &lt;enumeration value="SDG"/>
 *     &lt;enumeration value="SEK"/>
 *     &lt;enumeration value="SGD"/>
 *     &lt;enumeration value="SHP"/>
 *     &lt;enumeration value="SLL"/>
 *     &lt;enumeration value="SOS"/>
 *     &lt;enumeration value="SPL*"/>
 *     &lt;enumeration value="SRD"/>
 *     &lt;enumeration value="STD"/>
 *     &lt;enumeration value="SVC"/>
 *     &lt;enumeration value="SYP"/>
 *     &lt;enumeration value="SZL"/>
 *     &lt;enumeration value="THB"/>
 *     &lt;enumeration value="TJS"/>
 *     &lt;enumeration value="TMT"/>
 *     &lt;enumeration value="TND"/>
 *     &lt;enumeration value="TOP"/>
 *     &lt;enumeration value="TRY"/>
 *     &lt;enumeration value="TTD"/>
 *     &lt;enumeration value="TVD"/>
 *     &lt;enumeration value="TWD"/>
 *     &lt;enumeration value="TZS"/>
 *     &lt;enumeration value="UAH"/>
 *     &lt;enumeration value="UGX"/>
 *     &lt;enumeration value="USD"/>
 *     &lt;enumeration value="UYU"/>
 *     &lt;enumeration value="UZS"/>
 *     &lt;enumeration value="VEF"/>
 *     &lt;enumeration value="VND"/>
 *     &lt;enumeration value="VUV"/>
 *     &lt;enumeration value="WST"/>
 *     &lt;enumeration value="XAF"/>
 *     &lt;enumeration value="XCD"/>
 *     &lt;enumeration value="XDR"/>
 *     &lt;enumeration value="XOF"/>
 *     &lt;enumeration value="XPF"/>
 *     &lt;enumeration value="YER"/>
 *     &lt;enumeration value="ZAR"/>
 *     &lt;enumeration value="ZMW"/>
 *     &lt;enumeration value="ZWD"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoIdMoneda")
@XmlEnum
public enum TipoIdMoneda {


    /**
     * United Arab Emirates Dirham
     * 
     */
    AED("AED"),

    /**
     * Afghanistan Afghani
     * 
     */
    AFN("AFN"),

    /**
     * Albania Lek
     * 
     */
    ALL("ALL"),

    /**
     * Armenia Dram
     * 
     */
    AMD("AMD"),

    /**
     * Netherlands Antilles Guilder
     * 
     */
    ANG("ANG"),

    /**
     * Angola Kwanza
     * 
     */
    AOA("AOA"),

    /**
     * Argentina Peso
     * 
     */
    ARS("ARS"),

    /**
     * Australia Dollar
     * 
     */
    AUD("AUD"),

    /**
     * Aruba Guilder
     * 
     */
    AWG("AWG"),

    /**
     * Azerbaijan New Manat
     * 
     */
    AZN("AZN"),

    /**
     * Bosnia and Herzegovina Convertible Marka
     * 
     */
    BAM("BAM"),

    /**
     * Barbados Dollar
     * 
     */
    BBD("BBD"),

    /**
     * Bangladesh Taka
     * 
     */
    BDT("BDT"),

    /**
     * Bulgaria Lev
     * 
     */
    BGN("BGN"),

    /**
     * Bahrain Dinar
     * 
     */
    BHD("BHD"),

    /**
     * Burundi Franc
     * 
     */
    BIF("BIF"),

    /**
     * Bermuda Dollar
     * 
     */
    BMD("BMD"),

    /**
     * Brunei Darussalam Dollar
     * 
     */
    BND("BND"),

    /**
     * Bolivia Boliviano
     * 
     */
    BOB("BOB"),

    /**
     * Brazil Real
     * 
     */
    BRL("BRL"),

    /**
     * Bahamas Dollar
     * 
     */
    BSD("BSD"),

    /**
     * Bhutan Ngultrum
     * 
     */
    BTN("BTN"),

    /**
     * Botswana Pula
     * 
     */
    BWP("BWP"),

    /**
     * Belarus Ruble
     * 
     */
    BYR("BYR"),

    /**
     * Belize Dollar
     * 
     */
    BZD("BZD"),

    /**
     * Canada Dollar
     * 
     */
    CAD("CAD"),

    /**
     * Congo/Kinshasa Franc
     * 
     */
    CDF("CDF"),

    /**
     * Switzerland Franc
     * 
     */
    CHF("CHF"),

    /**
     * Chile Peso
     * 
     */
    CLP("CLP"),

    /**
     * China Yuan Renminbi
     * 
     */
    CNY("CNY"),

    /**
     * Colombia Peso
     * 
     */
    COP("COP"),

    /**
     * Costa Rica Colon
     * 
     */
    CRC("CRC"),

    /**
     * Cuba Convertible Peso
     * 
     */
    CUC("CUC"),

    /**
     * Cuba Peso
     * 
     */
    CUP("CUP"),

    /**
     * Cape Verde Escudo
     * 
     */
    CVE("CVE"),

    /**
     * Czech Republic Koruna
     * 
     */
    CZK("CZK"),

    /**
     * Djibouti Franc
     * 
     */
    DJF("DJF"),

    /**
     * Denmark Krone
     * 
     */
    DKK("DKK"),

    /**
     * Dominican Republic Peso
     * 
     */
    DOP("DOP"),

    /**
     * Algeria Dinar
     * 
     */
    DZD("DZD"),

    /**
     * Egypt Pound
     * 
     */
    EGP("EGP"),

    /**
     * Eritrea Nakfa
     * 
     */
    ERN("ERN"),

    /**
     * Ethiopia Birr
     * 
     */
    ETB("ETB"),

    /**
     * Euro Member Countries
     * 
     */
    EUR("EUR"),

    /**
     * Fiji Dollar
     * 
     */
    FJD("FJD"),

    /**
     * Falkland Islands (Malvinas) Pound
     * 
     */
    FKP("FKP"),

    /**
     * United Kingdom Pound
     * 
     */
    GBP("GBP"),

    /**
     * Georgia Lari
     * 
     */
    GEL("GEL"),

    /**
     * Guernsey Pound
     * 
     */
    GGP("GGP"),

    /**
     * Ghana Cedi
     * 
     */
    GHS("GHS"),

    /**
     * Gibraltar Pound
     * 
     */
    GIP("GIP"),

    /**
     * Gambia Dalasi
     * 
     */
    GMD("GMD"),

    /**
     * Guinea Franc
     * 
     */
    GNF("GNF"),

    /**
     * Guatemala Quetzal
     * 
     */
    GTQ("GTQ"),

    /**
     * Guyana Dollar
     * 
     */
    GYD("GYD"),

    /**
     * Hong Kong Dollar
     * 
     */
    HKD("HKD"),

    /**
     * Honduras Lempira
     * 
     */
    HNL("HNL"),

    /**
     * Croatia Kuna
     * 
     */
    HRK("HRK"),

    /**
     * Haiti Gourde
     * 
     */
    HTG("HTG"),

    /**
     * Hungary Forint
     * 
     */
    HUF("HUF"),

    /**
     * Indonesia Rupiah
     * 
     */
    IDR("IDR"),

    /**
     * Israel Shekel
     * 
     */
    ILS("ILS"),

    /**
     * Isle of Man Pound
     * 
     */
    IMP("IMP"),

    /**
     * India Rupee
     * 
     */
    INR("INR"),

    /**
     * Iraq Dinar
     * 
     */
    IQD("IQD"),

    /**
     * Iran Rial
     * 
     */
    IRR("IRR"),

    /**
     * Iceland Krona
     * 
     */
    ISK("ISK"),

    /**
     * Jersey Pound
     * 
     */
    JEP("JEP"),

    /**
     * Jamaica Dollar
     * 
     */
    JMD("JMD"),

    /**
     * Jordan Dinar
     * 
     */
    JOD("JOD"),

    /**
     * Japan Yen
     * 
     */
    JPY("JPY"),

    /**
     * Kenya Shilling
     * 
     */
    KES("KES"),

    /**
     * Kyrgyzstan Som
     * 
     */
    KGS("KGS"),

    /**
     * Cambodia Riel
     * 
     */
    KHR("KHR"),

    /**
     * Comoros Franc
     * 
     */
    KMF("KMF"),

    /**
     * Korea (North) Won
     * 
     */
    KPW("KPW"),

    /**
     * Korea (South) Won
     * 
     */
    KRW("KRW"),

    /**
     * Kuwait Dinar
     * 
     */
    KWD("KWD"),

    /**
     * Cayman Islands Dollar
     * 
     */
    KYD("KYD"),

    /**
     * Kazakhstan Tenge
     * 
     */
    KZT("KZT"),

    /**
     * Laos Kip
     * 
     */
    LAK("LAK"),

    /**
     * Lebanon Pound
     * 
     */
    LBP("LBP"),

    /**
     * Sri Lanka Rupee
     * 
     */
    LKR("LKR"),

    /**
     * Liberia Dollar
     * 
     */
    LRD("LRD"),

    /**
     * Lesotho Loti
     * 
     */
    LSL("LSL"),

    /**
     * Lithuania Litas
     * 
     */
    LTL("LTL"),

    /**
     * Libya Dinar
     * 
     */
    LYD("LYD"),

    /**
     * Morocco Dirham
     * 
     */
    MAD("MAD"),

    /**
     * Moldova Leu
     * 
     */
    MDL("MDL"),

    /**
     * Madagascar Ariary
     * 
     */
    MGA("MGA"),

    /**
     * Macedonia Denar
     * 
     */
    MKD("MKD"),

    /**
     * Myanmar (Burma) Kyat
     * 
     */
    MMK("MMK"),

    /**
     * Mongolia Tughrik
     * 
     */
    MNT("MNT"),

    /**
     * Macau Pataca
     * 
     */
    MOP("MOP"),

    /**
     * Mauritania Ouguiya
     * 
     */
    MRO("MRO"),

    /**
     * Mauritius Rupee
     * 
     */
    MUR("MUR"),

    /**
     * Maldives (Maldive Islands) Rufiyaa
     * 
     */
    MVR("MVR"),

    /**
     * Malawi Kwacha
     * 
     */
    MWK("MWK"),

    /**
     * Mexico Peso
     * 
     */
    MXN("MXN"),

    /**
     * Malaysia Ringgit
     * 
     */
    MYR("MYR"),

    /**
     * Mozambique Metical
     * 
     */
    MZN("MZN"),

    /**
     * Namibia Dollar
     * 
     */
    NAD("NAD"),

    /**
     * Nigeria Naira
     * 
     */
    NGN("NGN"),

    /**
     * Nicaragua Cordoba
     * 
     */
    NIO("NIO"),

    /**
     * Norway Krone
     * 
     */
    NOK("NOK"),

    /**
     * Nepal Rupee
     * 
     */
    NPR("NPR"),

    /**
     * New Zealand Dollar
     * 
     */
    NZD("NZD"),

    /**
     * Oman Rial
     * 
     */
    OMR("OMR"),

    /**
     * Panama Balboa
     * 
     */
    PAB("PAB"),

    /**
     * Peru Nuevo Sol
     * 
     */
    PEN("PEN"),

    /**
     * Papua New Guinea Kina
     * 
     */
    PGK("PGK"),

    /**
     * Philippines Peso
     * 
     */
    PHP("PHP"),

    /**
     * Pakistan Rupee
     * 
     */
    PKR("PKR"),

    /**
     * Poland Zloty
     * 
     */
    PLN("PLN"),

    /**
     * Paraguay Guarani
     * 
     */
    PYG("PYG"),

    /**
     * Qatar Riyal
     * 
     */
    QAR("QAR"),

    /**
     * Romania New Leu
     * 
     */
    RON("RON"),

    /**
     * Serbia Dinar
     * 
     */
    RSD("RSD"),

    /**
     * Russia Ruble
     * 
     */
    RUB("RUB"),

    /**
     * Rwanda Franc
     * 
     */
    RWF("RWF"),

    /**
     * Saudi Arabia Riyal
     * 
     */
    SAR("SAR"),

    /**
     * Solomon Islands Dollar
     * 
     */
    SBD("SBD"),

    /**
     * Seychelles Rupee
     * 
     */
    SCR("SCR"),

    /**
     * Sudan Pound
     * 
     */
    SDG("SDG"),

    /**
     * Sweden Krona
     * 
     */
    SEK("SEK"),

    /**
     * Singapore Dollar
     * 
     */
    SGD("SGD"),

    /**
     * Saint Helena Pound
     * 
     */
    SHP("SHP"),

    /**
     * Sierra Leone Leone
     * 
     */
    SLL("SLL"),

    /**
     * Somalia Shilling
     * 
     */
    SOS("SOS"),

    /**
     * Seborga Luigino
     * 
     */
    @XmlEnumValue("SPL*")
    SPL("SPL*"),

    /**
     * Suriname Dollar
     * 
     */
    SRD("SRD"),

    /**
     * S�o Tom� and Pr�ncipe Dobra
     * 
     */
    STD("STD"),

    /**
     * El Salvador Colon
     * 
     */
    SVC("SVC"),

    /**
     * Syria Pound
     * 
     */
    SYP("SYP"),

    /**
     * Swaziland Lilangeni
     * 
     */
    SZL("SZL"),

    /**
     * Thailand Baht
     * 
     */
    THB("THB"),

    /**
     * Tajikistan Somoni
     * 
     */
    TJS("TJS"),

    /**
     * Turkmenistan Manat
     * 
     */
    TMT("TMT"),

    /**
     * Tunisia Dinar
     * 
     */
    TND("TND"),

    /**
     * Tonga Pa'anga
     * 
     */
    TOP("TOP"),

    /**
     * Turkey Lira
     * 
     */
    TRY("TRY"),

    /**
     * Trinidad and Tobago Dollar
     * 
     */
    TTD("TTD"),

    /**
     * Tuvalu Dollar
     * 
     */
    TVD("TVD"),

    /**
     * Taiwan New Dollar
     * 
     */
    TWD("TWD"),

    /**
     * Tanzania Shilling
     * 
     */
    TZS("TZS"),

    /**
     * Ukraine Hryvnia
     * 
     */
    UAH("UAH"),

    /**
     * Uganda Shilling
     * 
     */
    UGX("UGX"),

    /**
     * United States Dollar
     * 
     */
    USD("USD"),

    /**
     * Uruguay Peso
     * 
     */
    UYU("UYU"),

    /**
     * Uzbekistan Som
     * 
     */
    UZS("UZS"),

    /**
     * Venezuela Bolivar
     * 
     */
    VEF("VEF"),

    /**
     * Viet Nam Dong
     * 
     */
    VND("VND"),

    /**
     * Vanuatu Vatu
     * 
     */
    VUV("VUV"),

    /**
     * Samoa Tala
     * 
     */
    WST("WST"),

    /**
     * Communaut� Financi�re Africaine (BEAC) CFA Franc�BEAC
     * 
     */
    XAF("XAF"),

    /**
     * East Caribbean Dollar
     * 
     */
    XCD("XCD"),

    /**
     * International Monetary Fund (IMF) Special Drawing Rights
     * 
     */
    XDR("XDR"),

    /**
     * Communaut� Financi�re Africaine (BCEAO) Franc
     * 
     */
    XOF("XOF"),

    /**
     * Comptoirs Fran�ais du Pacifique (CFP) Franc
     * 
     */
    XPF("XPF"),

    /**
     * Yemen Rial
     * 
     */
    YER("YER"),

    /**
     * South Africa Rand
     * 
     */
    ZAR("ZAR"),

    /**
     * Zambia Kwacha
     * 
     */
    ZMW("ZMW"),

    /**
     * Zimbabwe Dollar
     * 
     */
    ZWD("ZWD");
    private final String value;

    TipoIdMoneda(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoIdMoneda fromValue(String v) {
        for (TipoIdMoneda c: TipoIdMoneda.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
