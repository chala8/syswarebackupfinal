
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * indica el tipo de documento y el numero que identifica a un tercero para interop
 * 
 * <p>Java class for tipoCambioDocumento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoCambioDocumento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Documento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *         &lt;element name="DocumentoAnterior" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoCambioDocumento", propOrder = {
    "documento",
    "documentoAnterior"
})
public class TipoCambioDocumento {

    @XmlElement(name = "Documento", required = true)
    protected TipoIdTercero documento;
    @XmlElement(name = "DocumentoAnterior", required = true)
    protected TipoIdTercero documentoAnterior;

    /**
     * Gets the value of the documento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getDocumento() {
        return documento;
    }

    /**
     * Sets the value of the documento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setDocumento(TipoIdTercero value) {
        this.documento = value;
    }

    /**
     * Gets the value of the documentoAnterior property.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getDocumentoAnterior() {
        return documentoAnterior;
    }

    /**
     * Sets the value of the documentoAnterior property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setDocumentoAnterior(TipoIdTercero value) {
        this.documentoAnterior = value;
    }

}
