
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de respuesta para el cambio de un documento
 * 
 * <p>Java class for tipoMsjSolicitudCambioDocumento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudCambioDocumento">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Documentos" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCambioDocumento"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudCambioDocumento", propOrder = {
    "documentos"
})
public class TipoMsjSolicitudCambioDocumento
    extends TipoMsjGenerico
{

    @XmlElement(name = "Documentos", required = true)
    protected List<TipoCambioDocumento> documentos;

    /**
     * Gets the value of the documentos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoCambioDocumento }
     * 
     * 
     */
    public List<TipoCambioDocumento> getDocumentos() {
        if (documentos == null) {
            documentos = new ArrayList<TipoCambioDocumento>();
        }
        return this.documentos;
    }

}
