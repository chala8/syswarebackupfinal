package com.name.business.representations;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TypeDoc {

    private String type;
    private String number;

    @JsonCreator
    public TypeDoc(@JsonProperty("type") String type,
                   @JsonProperty("number") String number) {
        this.type = type;
        this.number = number;

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
