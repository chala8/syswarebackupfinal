
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * indica a un tercero financiero generico para interop
 * 
 * <p>Java class for tipoTerceroFinanciero complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoTerceroFinanciero">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infoTipoTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTercero"/>
 *         &lt;element name="infoResponsable" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoResponsable"/>
 *         &lt;element name="ausentismo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEstadoAdministrativo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoTerceroFinanciero", propOrder = {
    "infoTipoTercero",
    "infoResponsable",
    "ausentismo"
})
public class TipoTerceroFinanciero {

    @XmlElement(required = true)
    protected TipoTercero infoTipoTercero;
    @XmlElement(required = true)
    protected TipoInfoResponsable infoResponsable;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoEstadoAdministrativo ausentismo;

    /**
     * Gets the value of the infoTipoTercero property.
     * 
     * @return
     *     possible object is
     *     {@link TipoTercero }
     *     
     */
    public TipoTercero getInfoTipoTercero() {
        return infoTipoTercero;
    }

    /**
     * Sets the value of the infoTipoTercero property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTercero }
     *     
     */
    public void setInfoTipoTercero(TipoTercero value) {
        this.infoTipoTercero = value;
    }

    /**
     * Gets the value of the infoResponsable property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoResponsable }
     *     
     */
    public TipoInfoResponsable getInfoResponsable() {
        return infoResponsable;
    }

    /**
     * Sets the value of the infoResponsable property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoResponsable }
     *     
     */
    public void setInfoResponsable(TipoInfoResponsable value) {
        this.infoResponsable = value;
    }

    /**
     * Gets the value of the ausentismo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstadoAdministrativo }
     *     
     */
    public TipoEstadoAdministrativo getAusentismo() {
        return ausentismo;
    }

    /**
     * Sets the value of the ausentismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstadoAdministrativo }
     *     
     */
    public void setAusentismo(TipoEstadoAdministrativo value) {
        this.ausentismo = value;
    }

}
