
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Borrar una vez salgan a produccion los servicios web de hermes
 * 
 * <p>Java class for tipoMsjRespuestaPersonaHermes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaPersonaHermes">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="InfoInvestigador" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoPersonaHermes"/>
 *         &lt;element name="InfoInvestigadorAdicional" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInformacionHermes"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaPersonaHermes", propOrder = {
    "infoInvestigador",
    "infoInvestigadorAdicional"
})
public class TipoMsjRespuestaPersonaHermes
    extends TipoMsjGenerico
{

    @XmlElement(name = "InfoInvestigador", required = true)
    protected TipoPersonaHermes infoInvestigador;
    @XmlElement(name = "InfoInvestigadorAdicional", required = true)
    protected String infoInvestigadorAdicional;

    /**
     * Gets the value of the infoInvestigador property.
     * 
     * @return
     *     possible object is
     *     {@link TipoPersonaHermes }
     *     
     */
    public TipoPersonaHermes getInfoInvestigador() {
        return infoInvestigador;
    }

    /**
     * Sets the value of the infoInvestigador property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPersonaHermes }
     *     
     */
    public void setInfoInvestigador(TipoPersonaHermes value) {
        this.infoInvestigador = value;
    }

    /**
     * Gets the value of the infoInvestigadorAdicional property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoInvestigadorAdicional() {
        return infoInvestigadorAdicional;
    }

    /**
     * Sets the value of the infoInvestigadorAdicional property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoInvestigadorAdicional(String value) {
        this.infoInvestigadorAdicional = value;
    }

}
