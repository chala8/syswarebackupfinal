
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de un participante de proyectos de  extensi�n para la universidad nacional de colombia
 * 
 * <p>Java class for tipoInfoExtensionPersona complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoExtensionPersona">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InformacionProyecto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaProyecto"/>
 *         &lt;element name="RollEnElProyecto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoRollProyecto"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoExtensionPersona", propOrder = {
    "informacionProyecto",
    "rollEnElProyecto"
})
public class TipoInfoExtensionPersona {

    @XmlElement(name = "InformacionProyecto", required = true)
    protected TipoInfoBasicaProyecto informacionProyecto;
    @XmlElement(name = "RollEnElProyecto", required = true)
    @XmlSchemaType(name = "string")
    protected TipoRollProyecto rollEnElProyecto;

    /**
     * Gets the value of the informacionProyecto property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaProyecto }
     *     
     */
    public TipoInfoBasicaProyecto getInformacionProyecto() {
        return informacionProyecto;
    }

    /**
     * Sets the value of the informacionProyecto property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaProyecto }
     *     
     */
    public void setInformacionProyecto(TipoInfoBasicaProyecto value) {
        this.informacionProyecto = value;
    }

    /**
     * Gets the value of the rollEnElProyecto property.
     * 
     * @return
     *     possible object is
     *     {@link TipoRollProyecto }
     *     
     */
    public TipoRollProyecto getRollEnElProyecto() {
        return rollEnElProyecto;
    }

    /**
     * Sets the value of the rollEnElProyecto property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoRollProyecto }
     *     
     */
    public void setRollEnElProyecto(TipoRollProyecto value) {
        this.rollEnElProyecto = value;
    }

}
