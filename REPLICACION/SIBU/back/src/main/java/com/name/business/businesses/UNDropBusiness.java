package com.name.business.businesses;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.entities.TypeDocument;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.DAOs.*;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.FileOutputStream;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import com.name.business.WSDLs.*;
public class UNDropBusiness {

    private UNDropDAO billDAO;
    public UNDropBusiness(UNDropDAO billDAO) {
        this.billDAO = billDAO;
    }

    public Either<IException, String> getCC(String type, String number) {
        try {

            System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");

                GestiongeneralterceroClientEp wsdl = new GestiongeneralterceroClientEp();
                GestionGeneralTerceroPortType instancedWSDL = wsdl.getGestionGeneralTerceroPort();
                TipoMsjSolicitudPorIdTercero objectCC = new TipoMsjSolicitudPorIdTercero();
                TipoInfoMensaje infoMensaje = new TipoInfoMensaje();

                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(new Date());
                XMLGregorianCalendar calendarFinal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);

                infoMensaje.setFechaHora(calendarFinal);
                infoMensaje.setIdSede("01");
                infoMensaje.setIdSistema("100005");

                objectCC.setInfoMensaje(infoMensaje);

                TipoIdTercero idType = new TipoIdTercero();

                idType.setNumeroDocumento(number);


                idType.setTipoDocumento(TipoTipoDocumento.CC);

                if(type=="TI"){
                    idType.setTipoDocumento(TipoTipoDocumento.TI);
                }

                if(type=="CE"){
                    idType.setTipoDocumento(TipoTipoDocumento.CE);
                }

                if(type=="PA"){
                    idType.setTipoDocumento(TipoTipoDocumento.PA);
                }

                if(type=="RC"){
                    idType.setTipoDocumento(TipoTipoDocumento.RC);
                }

                if(type=="NI"){
                    idType.setTipoDocumento(TipoTipoDocumento.NI);
                }

                if(type=="IV"){
                    idType.setTipoDocumento(TipoTipoDocumento.IV);
                }



                objectCC.setIdTercero(idType);

                TipoMsjRespuestaOp response = instancedWSDL.opReplicarEmpleado(objectCC);

                System.out.println("RESPONSE SIBU: "+response.getRespuestaOP().getMsjError() + ", " + response.getRespuestaOP().getResultadoOp().value());



            return Either.right("1");

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }

    }







}