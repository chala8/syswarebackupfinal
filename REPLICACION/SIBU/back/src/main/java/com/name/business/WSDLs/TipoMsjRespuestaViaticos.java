
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para dar la respuesta de la existencia o no de una entidad para interop
 * 
 * <p>Java class for tipoMsjRespuestaViaticos complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaViaticos">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="Documento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *         &lt;element name="RespuestaViaticos" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoAsignacionViaticos"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaViaticos", propOrder = {
    "documento",
    "respuestaViaticos"
})
public class TipoMsjRespuestaViaticos
    extends TipoMsjGenerico
{

    @XmlElement(name = "Documento", required = true)
    protected TipoIdTercero documento;
    @XmlElement(name = "RespuestaViaticos", required = true)
    protected TipoAsignacionViaticos respuestaViaticos;

    /**
     * Gets the value of the documento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getDocumento() {
        return documento;
    }

    /**
     * Sets the value of the documento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setDocumento(TipoIdTercero value) {
        this.documento = value;
    }

    /**
     * Gets the value of the respuestaViaticos property.
     * 
     * @return
     *     possible object is
     *     {@link TipoAsignacionViaticos }
     *     
     */
    public TipoAsignacionViaticos getRespuestaViaticos() {
        return respuestaViaticos;
    }

    /**
     * Sets the value of the respuestaViaticos property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoAsignacionViaticos }
     *     
     */
    public void setRespuestaViaticos(TipoAsignacionViaticos value) {
        this.respuestaViaticos = value;
    }

}
