
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n detallada de una dependencia para IOP
 * 
 * <p>Clase Java para tipoInfoDetalleDependencia complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoDetalleDependencia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InfoBasica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaDependencia"/>
 *         &lt;element name="InfoBasicaAnterior" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaDependencia"/>
 *         &lt;element name="InfoUbicacionDependenciat" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoUbicacionDependencia"/>
 *         &lt;element name="infoDirector" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoDirectorDependencia"/>
 *         &lt;element name="DependenciaPadre" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaDependencia"/>
 *         &lt;sequence maxOccurs="unbounded">
 *           &lt;element name="DependenciasHijas" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaDependencia"/>
 *         &lt;/sequence>
 *         &lt;element name="tipoDependencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoDependencia"/>
 *         &lt;element name="estadoDependencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEstadoDependencia"/>
 *         &lt;element name="Sede" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoSede"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoDetalleDependencia", propOrder = {
    "infoBasica",
    "infoBasicaAnterior",
    "infoUbicacionDependenciat",
    "infoDirector",
    "dependenciaPadre",
    "dependenciasHijas",
    "tipoDependencia",
    "estadoDependencia",
    "sede"
})
public class TipoInfoDetalleDependencia {

    @XmlElement(name = "InfoBasica", required = true)
    protected TipoInfoBasicaDependencia infoBasica;
    @XmlElement(name = "InfoBasicaAnterior", required = true)
    protected TipoInfoBasicaDependencia infoBasicaAnterior;
    @XmlElement(name = "InfoUbicacionDependenciat", required = true)
    protected TipoInfoUbicacionDependencia infoUbicacionDependenciat;
    @XmlElement(required = true)
    protected TipoInfoDirectorDependencia infoDirector;
    @XmlElement(name = "DependenciaPadre", required = true)
    protected TipoInfoBasicaDependencia dependenciaPadre;
    @XmlElement(name = "DependenciasHijas", required = true)
    protected List<TipoInfoBasicaDependencia> dependenciasHijas;
    @XmlElement(required = true)
    protected String tipoDependencia;
    @XmlElement(required = true)
    protected String estadoDependencia;
    @XmlElement(name = "Sede", required = true)
    protected String sede;

    /**
     * Obtiene el valor de la propiedad infoBasica.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaDependencia }
     *     
     */
    public TipoInfoBasicaDependencia getInfoBasica() {
        return infoBasica;
    }

    /**
     * Define el valor de la propiedad infoBasica.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaDependencia }
     *     
     */
    public void setInfoBasica(TipoInfoBasicaDependencia value) {
        this.infoBasica = value;
    }

    /**
     * Obtiene el valor de la propiedad infoBasicaAnterior.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaDependencia }
     *     
     */
    public TipoInfoBasicaDependencia getInfoBasicaAnterior() {
        return infoBasicaAnterior;
    }

    /**
     * Define el valor de la propiedad infoBasicaAnterior.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaDependencia }
     *     
     */
    public void setInfoBasicaAnterior(TipoInfoBasicaDependencia value) {
        this.infoBasicaAnterior = value;
    }

    /**
     * Obtiene el valor de la propiedad infoUbicacionDependenciat.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoUbicacionDependencia }
     *     
     */
    public TipoInfoUbicacionDependencia getInfoUbicacionDependenciat() {
        return infoUbicacionDependenciat;
    }

    /**
     * Define el valor de la propiedad infoUbicacionDependenciat.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoUbicacionDependencia }
     *     
     */
    public void setInfoUbicacionDependenciat(TipoInfoUbicacionDependencia value) {
        this.infoUbicacionDependenciat = value;
    }

    /**
     * Obtiene el valor de la propiedad infoDirector.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoDirectorDependencia }
     *     
     */
    public TipoInfoDirectorDependencia getInfoDirector() {
        return infoDirector;
    }

    /**
     * Define el valor de la propiedad infoDirector.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoDirectorDependencia }
     *     
     */
    public void setInfoDirector(TipoInfoDirectorDependencia value) {
        this.infoDirector = value;
    }

    /**
     * Obtiene el valor de la propiedad dependenciaPadre.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaDependencia }
     *     
     */
    public TipoInfoBasicaDependencia getDependenciaPadre() {
        return dependenciaPadre;
    }

    /**
     * Define el valor de la propiedad dependenciaPadre.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaDependencia }
     *     
     */
    public void setDependenciaPadre(TipoInfoBasicaDependencia value) {
        this.dependenciaPadre = value;
    }

    /**
     * Gets the value of the dependenciasHijas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dependenciasHijas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDependenciasHijas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoBasicaDependencia }
     * 
     * 
     */
    public List<TipoInfoBasicaDependencia> getDependenciasHijas() {
        if (dependenciasHijas == null) {
            dependenciasHijas = new ArrayList<TipoInfoBasicaDependencia>();
        }
        return this.dependenciasHijas;
    }

    /**
     * Obtiene el valor de la propiedad tipoDependencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDependencia() {
        return tipoDependencia;
    }

    /**
     * Define el valor de la propiedad tipoDependencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDependencia(String value) {
        this.tipoDependencia = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoDependencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoDependencia() {
        return estadoDependencia;
    }

    /**
     * Define el valor de la propiedad estadoDependencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoDependencia(String value) {
        this.estadoDependencia = value;
    }

    /**
     * Obtiene el valor de la propiedad sede.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSede() {
        return sede;
    }

    /**
     * Define el valor de la propiedad sede.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSede(String value) {
        this.sede = value;
    }

}
