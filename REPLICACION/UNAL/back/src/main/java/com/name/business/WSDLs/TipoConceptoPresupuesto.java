
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoConceptoPresupuesto complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoConceptoPresupuesto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoConcepto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoConceptoPresupuesto"/>
 *         &lt;element name="DescripcionConcepto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDescripcionConcepto"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoConceptoPresupuesto", propOrder = {
    "codigoConcepto",
    "descripcionConcepto"
})
public class TipoConceptoPresupuesto {

    @XmlElement(name = "CodigoConcepto", required = true)
    protected String codigoConcepto;
    @XmlElement(name = "DescripcionConcepto", required = true)
    protected String descripcionConcepto;

    /**
     * Obtiene el valor de la propiedad codigoConcepto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoConcepto() {
        return codigoConcepto;
    }

    /**
     * Define el valor de la propiedad codigoConcepto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoConcepto(String value) {
        this.codigoConcepto = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcionConcepto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionConcepto() {
        return descripcionConcepto;
    }

    /**
     * Define el valor de la propiedad descripcionConcepto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionConcepto(String value) {
        this.descripcionConcepto = value;
    }

}
