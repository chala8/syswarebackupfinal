
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo para manejar la informacion de contacto para interop
 * 
 * <p>Clase Java para tipoInfoContacto complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoContacto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ciudadContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdCiudad"/>
 *         &lt;element name="telefonoContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNumeroTelefono"/>
 *         &lt;element name="correoElectronicoContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCorreoE"/>
 *         &lt;element name="direccionContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDireccionFisica"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoContacto", propOrder = {
    "ciudadContacto",
    "telefonoContacto",
    "correoElectronicoContacto",
    "direccionContacto"
})
public class TipoInfoContacto {

    @XmlElement(required = true)
    protected String ciudadContacto;
    @XmlElement(required = true)
    protected String telefonoContacto;
    @XmlElement(required = true)
    protected String correoElectronicoContacto;
    @XmlElement(required = true)
    protected String direccionContacto;

    /**
     * Obtiene el valor de la propiedad ciudadContacto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadContacto() {
        return ciudadContacto;
    }

    /**
     * Define el valor de la propiedad ciudadContacto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadContacto(String value) {
        this.ciudadContacto = value;
    }

    /**
     * Obtiene el valor de la propiedad telefonoContacto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    /**
     * Define el valor de la propiedad telefonoContacto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefonoContacto(String value) {
        this.telefonoContacto = value;
    }

    /**
     * Obtiene el valor de la propiedad correoElectronicoContacto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoElectronicoContacto() {
        return correoElectronicoContacto;
    }

    /**
     * Define el valor de la propiedad correoElectronicoContacto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoElectronicoContacto(String value) {
        this.correoElectronicoContacto = value;
    }

    /**
     * Obtiene el valor de la propiedad direccionContacto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccionContacto() {
        return direccionContacto;
    }

    /**
     * Define el valor de la propiedad direccionContacto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccionContacto(String value) {
        this.direccionContacto = value;
    }

}
