
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de un participante de proyectos de inversion para la universidad nacional de colombia
 * 
 * <p>Clase Java para tipoPersonaBPUN complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoPersonaBPUN">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InformacionBasica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaPersonaProyectos"/>
 *         &lt;element name="InfoContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoContacto"/>
 *         &lt;sequence>
 *           &lt;element name="InfoVinculacion">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;choice>
 *                     &lt;element name="VinculacionPlanta" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoContrato"/>
 *                     &lt;element name="Externos" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoVinculacionExterna"/>
 *                   &lt;/choice>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoPersonaBPUN", propOrder = {
    "informacionBasica",
    "infoContacto",
    "infoVinculacion"
})
public class TipoPersonaBPUN {

    @XmlElement(name = "InformacionBasica", required = true)
    protected TipoInfoBasicaPersonaProyectos informacionBasica;
    @XmlElement(name = "InfoContacto", required = true)
    protected TipoInfoContacto infoContacto;
    @XmlElement(name = "InfoVinculacion", required = true)
    protected TipoPersonaBPUN.InfoVinculacion infoVinculacion;

    /**
     * Obtiene el valor de la propiedad informacionBasica.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPersonaProyectos }
     *     
     */
    public TipoInfoBasicaPersonaProyectos getInformacionBasica() {
        return informacionBasica;
    }

    /**
     * Define el valor de la propiedad informacionBasica.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPersonaProyectos }
     *     
     */
    public void setInformacionBasica(TipoInfoBasicaPersonaProyectos value) {
        this.informacionBasica = value;
    }

    /**
     * Obtiene el valor de la propiedad infoContacto.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoContacto }
     *     
     */
    public TipoInfoContacto getInfoContacto() {
        return infoContacto;
    }

    /**
     * Define el valor de la propiedad infoContacto.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoContacto }
     *     
     */
    public void setInfoContacto(TipoInfoContacto value) {
        this.infoContacto = value;
    }

    /**
     * Obtiene el valor de la propiedad infoVinculacion.
     * 
     * @return
     *     possible object is
     *     {@link TipoPersonaBPUN.InfoVinculacion }
     *     
     */
    public TipoPersonaBPUN.InfoVinculacion getInfoVinculacion() {
        return infoVinculacion;
    }

    /**
     * Define el valor de la propiedad infoVinculacion.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPersonaBPUN.InfoVinculacion }
     *     
     */
    public void setInfoVinculacion(TipoPersonaBPUN.InfoVinculacion value) {
        this.infoVinculacion = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="VinculacionPlanta" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoContrato"/>
     *         &lt;element name="Externos" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoVinculacionExterna"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vinculacionPlanta",
        "externos"
    })
    public static class InfoVinculacion {

        @XmlElement(name = "VinculacionPlanta")
        protected TipoInfoContrato vinculacionPlanta;
        @XmlElement(name = "Externos")
        protected String externos;

        /**
         * Obtiene el valor de la propiedad vinculacionPlanta.
         * 
         * @return
         *     possible object is
         *     {@link TipoInfoContrato }
         *     
         */
        public TipoInfoContrato getVinculacionPlanta() {
            return vinculacionPlanta;
        }

        /**
         * Define el valor de la propiedad vinculacionPlanta.
         * 
         * @param value
         *     allowed object is
         *     {@link TipoInfoContrato }
         *     
         */
        public void setVinculacionPlanta(TipoInfoContrato value) {
            this.vinculacionPlanta = value;
        }

        /**
         * Obtiene el valor de la propiedad externos.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExternos() {
            return externos;
        }

        /**
         * Define el valor de la propiedad externos.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExternos(String value) {
            this.externos = value;
        }

    }

}
