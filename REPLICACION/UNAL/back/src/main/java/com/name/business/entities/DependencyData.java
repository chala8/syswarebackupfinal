package com.name.business.entities;

public class DependencyData {

    private String nombredependencia;
    private String codigosede;
    private String nombresede;
    private String nombredirector;
    private String tipodoc;
    private String numdoc;
    private String email;
    private String ubicacion;


    public DependencyData(String nombredependencia, String codigosede, String nombresede, String nombredirector, String tipodoc, String numdoc, String email, String ubicacion) {
        this.nombredependencia = nombredependencia;
        this.codigosede = codigosede;
        this.nombresede = nombresede;
        this.nombredirector = nombredirector;
        this.tipodoc = tipodoc;
        this.numdoc = numdoc;
        this.email = email;
        this.ubicacion = ubicacion;
    }


    public String getNombredependencia() {
        return nombredependencia;
    }

    public void setNombredependencia(String nombredependencia) {
        this.nombredependencia = nombredependencia;
    }

    public String getCodigosede() {
        return codigosede;
    }

    public void setCodigosede(String codigosede) {
        this.codigosede = codigosede;
    }

    public String getNombresede() {
        return nombresede;
    }

    public void setNombresede(String nombresede) {
        this.nombresede = nombresede;
    }

    public String getNombredirector() {
        return nombredirector;
    }

    public void setNombredirector(String nombredirector) {
        this.nombredirector = nombredirector;
    }

    public String getTipodoc() {
        return tipodoc;
    }

    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }

    public String getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(String numdoc) {
        this.numdoc = numdoc;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
}
