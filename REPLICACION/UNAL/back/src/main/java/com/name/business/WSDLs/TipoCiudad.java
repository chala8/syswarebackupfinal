
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n anterior y nueva de una ciudad para IOP
 * 
 * <p>Clase Java para tipoCiudad complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoCiudad">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ciudad" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDatosCiudad"/>
 *         &lt;element name="ciudadAnterior" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDatosCiudad"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoCiudad", propOrder = {
    "ciudad",
    "ciudadAnterior"
})
public class TipoCiudad {

    @XmlElement(required = true)
    protected TipoDatosCiudad ciudad;
    @XmlElement(required = true)
    protected TipoDatosCiudad ciudadAnterior;

    /**
     * Obtiene el valor de la propiedad ciudad.
     * 
     * @return
     *     possible object is
     *     {@link TipoDatosCiudad }
     *     
     */
    public TipoDatosCiudad getCiudad() {
        return ciudad;
    }

    /**
     * Define el valor de la propiedad ciudad.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDatosCiudad }
     *     
     */
    public void setCiudad(TipoDatosCiudad value) {
        this.ciudad = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudadAnterior.
     * 
     * @return
     *     possible object is
     *     {@link TipoDatosCiudad }
     *     
     */
    public TipoDatosCiudad getCiudadAnterior() {
        return ciudadAnterior;
    }

    /**
     * Define el valor de la propiedad ciudadAnterior.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDatosCiudad }
     *     
     */
    public void setCiudadAnterior(TipoDatosCiudad value) {
        this.ciudadAnterior = value;
    }

}
