
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la respuesta de iformaci�n geografica
 * 
 * <p>Clase Java para tipoMsjSolicitudInformacionGeograficaSIBU complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudInformacionGeograficaSIBU">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="InformacionGeografica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoGeografica"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudInformacionGeograficaSIBU", propOrder = {
    "informacionGeografica"
})
public class TipoMsjSolicitudInformacionGeograficaSIBU
    extends TipoMsjGenerico
{

    @XmlElement(name = "InformacionGeografica", required = true)
    protected TipoInfoGeografica informacionGeografica;

    /**
     * Obtiene el valor de la propiedad informacionGeografica.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoGeografica }
     *     
     */
    public TipoInfoGeografica getInformacionGeografica() {
        return informacionGeografica;
    }

    /**
     * Define el valor de la propiedad informacionGeografica.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoGeografica }
     *     
     */
    public void setInformacionGeografica(TipoInfoGeografica value) {
        this.informacionGeografica = value;
    }

}
