package com.name.business.entities;

public class CentData {

    private String CENT_ENTIDAD;
    private String CENT_DESCRIPCION;


    public CentData(String CENT_ENTIDAD, String CENT_DESCRIPCION) {
        this.CENT_ENTIDAD = CENT_ENTIDAD;
        this.CENT_DESCRIPCION = CENT_DESCRIPCION;
    }


    public String getCENT_ENTIDAD() {
        return CENT_ENTIDAD;
    }

    public void setCENT_ENTIDAD(String CENT_ENTIDAD) {
        this.CENT_ENTIDAD = CENT_ENTIDAD;
    }

    public String getCENT_DESCRIPCION() {
        return CENT_DESCRIPCION;
    }

    public void setCENT_DESCRIPCION(String CENT_DESCRIPCION) {
        this.CENT_DESCRIPCION = CENT_DESCRIPCION;
    }
}
