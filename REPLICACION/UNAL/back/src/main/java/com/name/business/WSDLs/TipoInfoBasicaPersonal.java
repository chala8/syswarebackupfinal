
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * indica la informacion biografica de un tercero generica para interop
 * 
 * <p>Clase Java para tipoInfoBasicaPersonal complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoBasicaPersonal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *         &lt;element name="generoTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoGenerotercero"/>
 *         &lt;element name="nomTercero">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="nombrePersonaNatural" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombrePersonaNatural"/>
 *                   &lt;element name="nombrePersonaJuridica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombrePersonaJuridica"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="naturalezaTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNaturalezaTercero"/>
 *         &lt;element name="estadoCivil" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEstadoCivil"/>
 *         &lt;element name="fechaNacimiento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFecha"/>
 *         &lt;element name="lugarNacimiento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoUbicacion"/>
 *         &lt;element name="lugarResidencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoUbicacion"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoBasicaPersonal", propOrder = {
    "idTercero",
    "generoTercero",
    "nomTercero",
    "naturalezaTercero",
    "estadoCivil",
    "fechaNacimiento",
    "lugarNacimiento",
    "lugarResidencia"
})
public class TipoInfoBasicaPersonal {

    @XmlElement(required = true)
    protected TipoIdTercero idTercero;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoGenerotercero generoTercero;
    @XmlElement(required = true)
    protected TipoInfoBasicaPersonal.NomTercero nomTercero;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoNaturalezaTercero naturalezaTercero;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoEstadoCivil estadoCivil;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaNacimiento;
    @XmlElement(required = true)
    protected TipoUbicacion lugarNacimiento;
    @XmlElement(required = true)
    protected TipoUbicacion lugarResidencia;

    /**
     * Obtiene el valor de la propiedad idTercero.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getIdTercero() {
        return idTercero;
    }

    /**
     * Define el valor de la propiedad idTercero.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setIdTercero(TipoIdTercero value) {
        this.idTercero = value;
    }

    /**
     * Obtiene el valor de la propiedad generoTercero.
     * 
     * @return
     *     possible object is
     *     {@link TipoGenerotercero }
     *     
     */
    public TipoGenerotercero getGeneroTercero() {
        return generoTercero;
    }

    /**
     * Define el valor de la propiedad generoTercero.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoGenerotercero }
     *     
     */
    public void setGeneroTercero(TipoGenerotercero value) {
        this.generoTercero = value;
    }

    /**
     * Obtiene el valor de la propiedad nomTercero.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPersonal.NomTercero }
     *     
     */
    public TipoInfoBasicaPersonal.NomTercero getNomTercero() {
        return nomTercero;
    }

    /**
     * Define el valor de la propiedad nomTercero.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPersonal.NomTercero }
     *     
     */
    public void setNomTercero(TipoInfoBasicaPersonal.NomTercero value) {
        this.nomTercero = value;
    }

    /**
     * Obtiene el valor de la propiedad naturalezaTercero.
     * 
     * @return
     *     possible object is
     *     {@link TipoNaturalezaTercero }
     *     
     */
    public TipoNaturalezaTercero getNaturalezaTercero() {
        return naturalezaTercero;
    }

    /**
     * Define el valor de la propiedad naturalezaTercero.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoNaturalezaTercero }
     *     
     */
    public void setNaturalezaTercero(TipoNaturalezaTercero value) {
        this.naturalezaTercero = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoCivil.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstadoCivil }
     *     
     */
    public TipoEstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    /**
     * Define el valor de la propiedad estadoCivil.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstadoCivil }
     *     
     */
    public void setEstadoCivil(TipoEstadoCivil value) {
        this.estadoCivil = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaNacimiento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Define el valor de la propiedad fechaNacimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaNacimiento(XMLGregorianCalendar value) {
        this.fechaNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad lugarNacimiento.
     * 
     * @return
     *     possible object is
     *     {@link TipoUbicacion }
     *     
     */
    public TipoUbicacion getLugarNacimiento() {
        return lugarNacimiento;
    }

    /**
     * Define el valor de la propiedad lugarNacimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoUbicacion }
     *     
     */
    public void setLugarNacimiento(TipoUbicacion value) {
        this.lugarNacimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad lugarResidencia.
     * 
     * @return
     *     possible object is
     *     {@link TipoUbicacion }
     *     
     */
    public TipoUbicacion getLugarResidencia() {
        return lugarResidencia;
    }

    /**
     * Define el valor de la propiedad lugarResidencia.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoUbicacion }
     *     
     */
    public void setLugarResidencia(TipoUbicacion value) {
        this.lugarResidencia = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="nombrePersonaNatural" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombrePersonaNatural"/>
     *         &lt;element name="nombrePersonaJuridica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombrePersonaJuridica"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nombrePersonaNatural",
        "nombrePersonaJuridica"
    })
    public static class NomTercero {

        protected TipoNombrePersonaNatural nombrePersonaNatural;
        protected String nombrePersonaJuridica;

        /**
         * Obtiene el valor de la propiedad nombrePersonaNatural.
         * 
         * @return
         *     possible object is
         *     {@link TipoNombrePersonaNatural }
         *     
         */
        public TipoNombrePersonaNatural getNombrePersonaNatural() {
            return nombrePersonaNatural;
        }

        /**
         * Define el valor de la propiedad nombrePersonaNatural.
         * 
         * @param value
         *     allowed object is
         *     {@link TipoNombrePersonaNatural }
         *     
         */
        public void setNombrePersonaNatural(TipoNombrePersonaNatural value) {
            this.nombrePersonaNatural = value;
        }

        /**
         * Obtiene el valor de la propiedad nombrePersonaJuridica.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombrePersonaJuridica() {
            return nombrePersonaJuridica;
        }

        /**
         * Define el valor de la propiedad nombrePersonaJuridica.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombrePersonaJuridica(String value) {
            this.nombrePersonaJuridica = value;
        }

    }

}
