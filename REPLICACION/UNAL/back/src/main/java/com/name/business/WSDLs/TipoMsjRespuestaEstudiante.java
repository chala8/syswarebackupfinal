
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la informaci�n de un Estudiante para Interoperabilidad
 * 
 * <p>Clase Java para tipoMsjRespuestaEstudiante complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaEstudiante">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Estudiante" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEstudiante"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaEstudiante", propOrder = {
    "estudiante"
})
public class TipoMsjRespuestaEstudiante
    extends TipoMsjGenerico
{

    @XmlElement(name = "Estudiante", required = true)
    protected List<TipoEstudiante> estudiante;

    /**
     * Gets the value of the estudiante property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the estudiante property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEstudiante().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoEstudiante }
     * 
     * 
     */
    public List<TipoEstudiante> getEstudiante() {
        if (estudiante == null) {
            estudiante = new ArrayList<TipoEstudiante>();
        }
        return this.estudiante;
    }

}
