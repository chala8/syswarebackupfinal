
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de solicitud para e cambio de contrato en el sistem de infomaci�n SIBU
 * 
 * <p>Clase Java para tipoMsjSolicitudCambioContrato complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudCambioContrato">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="Contratos" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCambioContrato"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudCambioContrato", propOrder = {
    "contratos"
})
public class TipoMsjSolicitudCambioContrato
    extends TipoMsjGenerico
{

    @XmlElement(name = "Contratos", required = true)
    protected TipoCambioContrato contratos;

    /**
     * Obtiene el valor de la propiedad contratos.
     * 
     * @return
     *     possible object is
     *     {@link TipoCambioContrato }
     *     
     */
    public TipoCambioContrato getContratos() {
        return contratos;
    }

    /**
     * Define el valor de la propiedad contratos.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCambioContrato }
     *     
     */
    public void setContratos(TipoCambioContrato value) {
        this.contratos = value;
    }

}
