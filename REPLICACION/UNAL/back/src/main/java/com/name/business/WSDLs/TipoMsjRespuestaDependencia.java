
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la respuesta de una dependencia para interop
 * 
 * <p>Clase Java para tipoMsjRespuestaDependencia complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaDependencia">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="Dependencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoDetalleDependencia"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaDependencia", propOrder = {
    "dependencia"
})
public class TipoMsjRespuestaDependencia
    extends TipoMsjGenerico
{

    @XmlElement(name = "Dependencia", required = true)
    protected TipoInfoDetalleDependencia dependencia;

    /**
     * Obtiene el valor de la propiedad dependencia.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoDetalleDependencia }
     *     
     */
    public TipoInfoDetalleDependencia getDependencia() {
        return dependencia;
    }

    /**
     * Define el valor de la propiedad dependencia.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoDetalleDependencia }
     *     
     */
    public void setDependencia(TipoInfoDetalleDependencia value) {
        this.dependencia = value;
    }

}
