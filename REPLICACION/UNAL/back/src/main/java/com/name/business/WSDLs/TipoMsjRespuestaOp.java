
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la solicitud de un tercero por su id para interop
 * 
 * <p>Clase Java para tipoMsjRespuestaOp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaOp">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="respuestaOP" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoRespuestaOp"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaOp", propOrder = {
    "respuestaOP"
})
public class TipoMsjRespuestaOp
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected TipoRespuestaOp respuestaOP;

    /**
     * Obtiene el valor de la propiedad respuestaOP.
     * 
     * @return
     *     possible object is
     *     {@link TipoRespuestaOp }
     *     
     */
    public TipoRespuestaOp getRespuestaOP() {
        return respuestaOP;
    }

    /**
     * Define el valor de la propiedad respuestaOP.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoRespuestaOp }
     *     
     */
    public void setRespuestaOP(TipoRespuestaOp value) {
        this.respuestaOP = value;
    }

}
