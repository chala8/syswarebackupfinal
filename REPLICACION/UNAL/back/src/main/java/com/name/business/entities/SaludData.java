package com.name.business.entities;

import java.util.Date;

public class SaludData {

    private String lugexpedicion;
    private String gruposanguineo;
    private String celular;
    private String fondopension;
    private String entidadsalud;


    public SaludData(String lugexpedicion, String gruposanguineo, String celular, String fondopension, String entidadsalud) {
        this.lugexpedicion = lugexpedicion;
        this.gruposanguineo = gruposanguineo;
        this.celular = celular;
        this.fondopension = fondopension;
        this.entidadsalud = entidadsalud;
    }


    public String getLugexpedicion() {
        return lugexpedicion;
    }

    public void setLugexpedicion(String lugexpedicion) {
        this.lugexpedicion = lugexpedicion;
    }

    public String getGruposanguineo() {
        return gruposanguineo;
    }

    public void setGruposanguineo(String gruposanguineo) {
        this.gruposanguineo = gruposanguineo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getFondopension() {
        return fondopension;
    }

    public void setFondopension(String fondopension) {
        this.fondopension = fondopension;
    }

    public String getEntidadsalud() {
        return entidadsalud;
    }

    public void setEntidadsalud(String entidadsalud) {
        this.entidadsalud = entidadsalud;
    }
}
