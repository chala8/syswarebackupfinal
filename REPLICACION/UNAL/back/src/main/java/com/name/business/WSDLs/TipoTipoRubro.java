
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoTipoRubro.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoTipoRubro">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="E"/>
 *     &lt;enumeration value="I"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoTipoRubro")
@XmlEnum
public enum TipoTipoRubro {


    /**
     * Egresos
     * 
     */
    E,

    /**
     * Ingresos
     * 
     */
    I;

    public String value() {
        return name();
    }

    public static TipoTipoRubro fromValue(String v) {
        return valueOf(v);
    }

}
