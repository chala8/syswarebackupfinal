
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoDocente complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoDocente">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEmpleado">
 *       &lt;sequence>
 *         &lt;element name="UBGAA" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoUBGAA"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoDocente", propOrder = {
    "ubgaa"
})
public class TipoDocente
    extends TipoEmpleado
{

    @XmlElement(name = "UBGAA", required = true)
    protected TipoUBGAA ubgaa;

    /**
     * Obtiene el valor de la propiedad ubgaa.
     * 
     * @return
     *     possible object is
     *     {@link TipoUBGAA }
     *     
     */
    public TipoUBGAA getUBGAA() {
        return ubgaa;
    }

    /**
     * Define el valor de la propiedad ubgaa.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoUBGAA }
     *     
     */
    public void setUBGAA(TipoUBGAA value) {
        this.ubgaa = value;
    }

}
