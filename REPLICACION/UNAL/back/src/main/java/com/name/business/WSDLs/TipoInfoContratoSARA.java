
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * indica la informacion completa de un contrato para interop
 * 
 * <p>Clase Java para tipoInfoContratoSARA complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoContratoSARA">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cabeceraContrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCabeceraContrato"/>
 *         &lt;element name="detalleContrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDetalleContratoSARA"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoContratoSARA", propOrder = {
    "cabeceraContrato",
    "detalleContrato"
})
public class TipoInfoContratoSARA {

    @XmlElement(required = true)
    protected TipoCabeceraContrato cabeceraContrato;
    @XmlElement(required = true)
    protected TipoDetalleContratoSARA detalleContrato;

    /**
     * Obtiene el valor de la propiedad cabeceraContrato.
     * 
     * @return
     *     possible object is
     *     {@link TipoCabeceraContrato }
     *     
     */
    public TipoCabeceraContrato getCabeceraContrato() {
        return cabeceraContrato;
    }

    /**
     * Define el valor de la propiedad cabeceraContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCabeceraContrato }
     *     
     */
    public void setCabeceraContrato(TipoCabeceraContrato value) {
        this.cabeceraContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad detalleContrato.
     * 
     * @return
     *     possible object is
     *     {@link TipoDetalleContratoSARA }
     *     
     */
    public TipoDetalleContratoSARA getDetalleContrato() {
        return detalleContrato;
    }

    /**
     * Define el valor de la propiedad detalleContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDetalleContratoSARA }
     *     
     */
    public void setDetalleContrato(TipoDetalleContratoSARA value) {
        this.detalleContrato = value;
    }

}
