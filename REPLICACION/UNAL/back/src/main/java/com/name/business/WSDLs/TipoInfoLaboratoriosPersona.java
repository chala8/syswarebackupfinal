
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de un participante de proyectos de investigaci�n o extensi�n para la universidad nacional de colombia
 * 
 * <p>Clase Java para tipoInfoLaboratoriosPersona complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoLaboratoriosPersona">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Laboratorios" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoLaboratorio"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoLaboratoriosPersona", propOrder = {
    "laboratorios"
})
public class TipoInfoLaboratoriosPersona {

    @XmlElement(name = "Laboratorios", required = true)
    protected TipoInfoLaboratorio laboratorios;

    /**
     * Obtiene el valor de la propiedad laboratorios.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoLaboratorio }
     *     
     */
    public TipoInfoLaboratorio getLaboratorios() {
        return laboratorios;
    }

    /**
     * Define el valor de la propiedad laboratorios.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoLaboratorio }
     *     
     */
    public void setLaboratorios(TipoInfoLaboratorio value) {
        this.laboratorios = value;
    }

}
