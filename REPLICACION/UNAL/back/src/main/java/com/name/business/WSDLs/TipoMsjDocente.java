
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoMsjDocente complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjDocente">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="docente" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDocente"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjDocente", propOrder = {
    "docente"
})
public class TipoMsjDocente
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected TipoDocente docente;

    /**
     * Obtiene el valor de la propiedad docente.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocente }
     *     
     */
    public TipoDocente getDocente() {
        return docente;
    }

    /**
     * Define el valor de la propiedad docente.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocente }
     *     
     */
    public void setDocente(TipoDocente value) {
        this.docente = value;
    }

}
