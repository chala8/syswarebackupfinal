package com.name.business.mappers;

import com.name.business.entities.DependencyData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DependencyDataMapper implements ResultSetMapper<DependencyData> {
    @Override
    public DependencyData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DependencyData(
                resultSet.getString("nombredependencia"),
                resultSet.getString("codigosede"),
                resultSet.getString("nombresede"),
                resultSet.getString("nombredirector"),
                resultSet.getString("tipodoc"),
                resultSet.getString("numdoc"),
                resultSet.getString("email"),
                resultSet.getString("ubicacion")
        );
    }
}