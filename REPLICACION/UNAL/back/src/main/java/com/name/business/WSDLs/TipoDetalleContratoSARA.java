
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * indica la informacion detallada de un contrato para interop
 * 
 * <p>Clase Java para tipoDetalleContratoSARA complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoDetalleContratoSARA">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="asignacionBasica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMontoMonetario"/>
 *         &lt;element name="cargoEmpleado" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCargoEmpleado"/>
 *         &lt;element name="fechaInicio" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFecha"/>
 *         &lt;element name="fechaFin" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFechaVacio"/>
 *         &lt;element name="dedicacion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDedicacion"/>
 *         &lt;element name="tipoPeriodo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoPeriodo"/>
 *         &lt;element name="fechaAntiguedad" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFecha"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoDetalleContratoSARA", propOrder = {
    "asignacionBasica",
    "cargoEmpleado",
    "fechaInicio",
    "fechaFin",
    "dedicacion",
    "tipoPeriodo",
    "fechaAntiguedad"
})
public class TipoDetalleContratoSARA {

    @XmlElement(required = true)
    protected TipoMontoMonetario asignacionBasica;
    @XmlElement(required = true)
    protected String cargoEmpleado;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaInicio;
    @XmlElement(required = true)
    protected String fechaFin;
    protected double dedicacion;
    @XmlElement(required = true)
    protected String tipoPeriodo;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaAntiguedad;

    /**
     * Obtiene el valor de la propiedad asignacionBasica.
     * 
     * @return
     *     possible object is
     *     {@link TipoMontoMonetario }
     *     
     */
    public TipoMontoMonetario getAsignacionBasica() {
        return asignacionBasica;
    }

    /**
     * Define el valor de la propiedad asignacionBasica.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoMontoMonetario }
     *     
     */
    public void setAsignacionBasica(TipoMontoMonetario value) {
        this.asignacionBasica = value;
    }

    /**
     * Obtiene el valor de la propiedad cargoEmpleado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCargoEmpleado() {
        return cargoEmpleado;
    }

    /**
     * Define el valor de la propiedad cargoEmpleado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCargoEmpleado(String value) {
        this.cargoEmpleado = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaInicio.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Define el valor de la propiedad fechaInicio.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaInicio(XMLGregorianCalendar value) {
        this.fechaInicio = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaFin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaFin() {
        return fechaFin;
    }

    /**
     * Define el valor de la propiedad fechaFin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaFin(String value) {
        this.fechaFin = value;
    }

    /**
     * Obtiene el valor de la propiedad dedicacion.
     * 
     */
    public double getDedicacion() {
        return dedicacion;
    }

    /**
     * Define el valor de la propiedad dedicacion.
     * 
     */
    public void setDedicacion(double value) {
        this.dedicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoPeriodo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoPeriodo() {
        return tipoPeriodo;
    }

    /**
     * Define el valor de la propiedad tipoPeriodo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoPeriodo(String value) {
        this.tipoPeriodo = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaAntiguedad.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaAntiguedad() {
        return fechaAntiguedad;
    }

    /**
     * Define el valor de la propiedad fechaAntiguedad.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaAntiguedad(XMLGregorianCalendar value) {
        this.fechaAntiguedad = value;
    }

}
