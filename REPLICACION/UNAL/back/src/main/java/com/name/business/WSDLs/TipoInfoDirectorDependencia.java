
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n del director de una dependencia para IOP
 * 
 * <p>Clase Java para tipoInfoDirectorDependencia complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoDirectorDependencia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdDirector" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *         &lt;element name="NombreDirector" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombrePersonaNatural"/>
 *         &lt;element name="CorreoElectronico" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCorreoE"/>
 *         &lt;element name="ext" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoExtensionTelefonica"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoDirectorDependencia", propOrder = {
    "idDirector",
    "nombreDirector",
    "correoElectronico",
    "ext"
})
public class TipoInfoDirectorDependencia {

    @XmlElement(name = "IdDirector", required = true)
    protected TipoIdTercero idDirector;
    @XmlElement(name = "NombreDirector", required = true)
    protected TipoNombrePersonaNatural nombreDirector;
    @XmlElement(name = "CorreoElectronico", required = true)
    protected String correoElectronico;
    @XmlElement(required = true)
    protected String ext;

    /**
     * Obtiene el valor de la propiedad idDirector.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getIdDirector() {
        return idDirector;
    }

    /**
     * Define el valor de la propiedad idDirector.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setIdDirector(TipoIdTercero value) {
        this.idDirector = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreDirector.
     * 
     * @return
     *     possible object is
     *     {@link TipoNombrePersonaNatural }
     *     
     */
    public TipoNombrePersonaNatural getNombreDirector() {
        return nombreDirector;
    }

    /**
     * Define el valor de la propiedad nombreDirector.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoNombrePersonaNatural }
     *     
     */
    public void setNombreDirector(TipoNombrePersonaNatural value) {
        this.nombreDirector = value;
    }

    /**
     * Obtiene el valor de la propiedad correoElectronico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    /**
     * Define el valor de la propiedad correoElectronico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoElectronico(String value) {
        this.correoElectronico = value;
    }

    /**
     * Obtiene el valor de la propiedad ext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExt() {
        return ext;
    }

    /**
     * Define el valor de la propiedad ext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExt(String value) {
        this.ext = value;
    }

}
