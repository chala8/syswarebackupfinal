
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para dar la respuesta de la existencia o no de una entidad para interop
 * 
 * <p>Clase Java para tipoMsjRespuestaViaticos complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaViaticos">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="Documento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *         &lt;element name="RespuestaViaticos" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoAsignacionViaticos"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaViaticos", propOrder = {
    "documento",
    "respuestaViaticos"
})
public class TipoMsjRespuestaViaticos
    extends TipoMsjGenerico
{

    @XmlElement(name = "Documento", required = true)
    protected TipoIdTercero documento;
    @XmlElement(name = "RespuestaViaticos", required = true)
    protected TipoAsignacionViaticos respuestaViaticos;

    /**
     * Obtiene el valor de la propiedad documento.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getDocumento() {
        return documento;
    }

    /**
     * Define el valor de la propiedad documento.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setDocumento(TipoIdTercero value) {
        this.documento = value;
    }

    /**
     * Obtiene el valor de la propiedad respuestaViaticos.
     * 
     * @return
     *     possible object is
     *     {@link TipoAsignacionViaticos }
     *     
     */
    public TipoAsignacionViaticos getRespuestaViaticos() {
        return respuestaViaticos;
    }

    /**
     * Define el valor de la propiedad respuestaViaticos.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoAsignacionViaticos }
     *     
     */
    public void setRespuestaViaticos(TipoAsignacionViaticos value) {
        this.respuestaViaticos = value;
    }

}
