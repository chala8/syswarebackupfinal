
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de una ciudad para IOP
 * 
 * <p>Clase Java para tipoDep complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoDep">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="departamento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDatosDep"/>
 *         &lt;element name="departmentoAnterior" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDatosDep"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoDep", propOrder = {
    "departamento",
    "departmentoAnterior"
})
public class TipoDep {

    @XmlElement(required = true)
    protected TipoDatosDep departamento;
    @XmlElement(required = true)
    protected TipoDatosDep departmentoAnterior;

    /**
     * Obtiene el valor de la propiedad departamento.
     * 
     * @return
     *     possible object is
     *     {@link TipoDatosDep }
     *     
     */
    public TipoDatosDep getDepartamento() {
        return departamento;
    }

    /**
     * Define el valor de la propiedad departamento.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDatosDep }
     *     
     */
    public void setDepartamento(TipoDatosDep value) {
        this.departamento = value;
    }

    /**
     * Obtiene el valor de la propiedad departmentoAnterior.
     * 
     * @return
     *     possible object is
     *     {@link TipoDatosDep }
     *     
     */
    public TipoDatosDep getDepartmentoAnterior() {
        return departmentoAnterior;
    }

    /**
     * Define el valor de la propiedad departmentoAnterior.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDatosDep }
     *     
     */
    public void setDepartmentoAnterior(TipoDatosDep value) {
        this.departmentoAnterior = value;
    }

}
