
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoPlanEstudios complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoPlanEstudios">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InfoBasicaPlanEstudios" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaPlanEstudios"/>
 *         &lt;element name="InfoDetallePlanEstudios" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoDetallePlanEstudios"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoPlanEstudios", propOrder = {
    "infoBasicaPlanEstudios",
    "infoDetallePlanEstudios"
})
public class TipoPlanEstudios {

    @XmlElement(name = "InfoBasicaPlanEstudios", required = true)
    protected TipoInfoBasicaPlanEstudios infoBasicaPlanEstudios;
    @XmlElement(name = "InfoDetallePlanEstudios", required = true)
    protected TipoInfoDetallePlanEstudios infoDetallePlanEstudios;

    /**
     * Obtiene el valor de la propiedad infoBasicaPlanEstudios.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPlanEstudios }
     *     
     */
    public TipoInfoBasicaPlanEstudios getInfoBasicaPlanEstudios() {
        return infoBasicaPlanEstudios;
    }

    /**
     * Define el valor de la propiedad infoBasicaPlanEstudios.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPlanEstudios }
     *     
     */
    public void setInfoBasicaPlanEstudios(TipoInfoBasicaPlanEstudios value) {
        this.infoBasicaPlanEstudios = value;
    }

    /**
     * Obtiene el valor de la propiedad infoDetallePlanEstudios.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoDetallePlanEstudios }
     *     
     */
    public TipoInfoDetallePlanEstudios getInfoDetallePlanEstudios() {
        return infoDetallePlanEstudios;
    }

    /**
     * Define el valor de la propiedad infoDetallePlanEstudios.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoDetallePlanEstudios }
     *     
     */
    public void setInfoDetallePlanEstudios(TipoInfoDetallePlanEstudios value) {
        this.infoDetallePlanEstudios = value;
    }

}
