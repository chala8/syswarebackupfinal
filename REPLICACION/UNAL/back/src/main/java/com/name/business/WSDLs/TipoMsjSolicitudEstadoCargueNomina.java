
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoMsjSolicitudEstadoCargueNomina complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudEstadoCargueNomina">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="codigoCargueNomina" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoCargaNomina"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudEstadoCargueNomina", propOrder = {
    "codigoCargueNomina"
})
public class TipoMsjSolicitudEstadoCargueNomina
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected String codigoCargueNomina;

    /**
     * Obtiene el valor de la propiedad codigoCargueNomina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCargueNomina() {
        return codigoCargueNomina;
    }

    /**
     * Define el valor de la propiedad codigoCargueNomina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCargueNomina(String value) {
        this.codigoCargueNomina = value;
    }

}
