package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.entities.*;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.UNDropBusiness;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;


@Path("/un")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UNDropResource {
    private UNDropBusiness uNDropBusiness;

    public UNDropResource(UNDropBusiness uNDropBusiness) {
        this.uNDropBusiness = uNDropBusiness;
    }

    @GET
    @Path("/sql")
    @Timed
    @PermitAll()
    public Response getBillResource(@QueryParam("docUse") String docUse,@QueryParam("UN") String UN,@QueryParam("BPUN") String BPUN,@QueryParam("SIBU") String SIBU,@QueryParam("QUIPU") String QUIPU,@QueryParam("docType") String docType,@QueryParam("doc") String doc){

        Response response;

        Either<IException, String> getResponseGeneric = uNDropBusiness.getCC(Boolean.parseBoolean(docUse),Boolean.parseBoolean(UN),Boolean.parseBoolean(BPUN),Boolean.parseBoolean(SIBU),Boolean.parseBoolean(QUIPU),docType,doc);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/BPUNData")
    @Timed
    @PermitAll()
    public Response getBPUNData(@QueryParam("type") String type, @QueryParam("cc") String cc){

        Response response;

        System.out.println("resource: "+type+","+cc);
        Either<IException, BPUNData> getResponseGeneric = uNDropBusiness.getBPUNData(type,cc);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/getDatosComplementarios")
    @Timed
    @PermitAll()
    public Response getSaludData(@QueryParam("type") String type, @QueryParam("cc") String cc){

        Response response;

        System.out.println("resource: "+type+","+cc);
        Either<IException, SaludData> getResponseGeneric = uNDropBusiness.getSaludData(type,cc);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/getEntidadesData")
    @Timed
    @PermitAll()
    public Response getCentData(@QueryParam("CENT_ENTIDAD") String CENT_ENTIDAD){

        Response response;

        Either<IException, List<CentData>> getResponseGeneric = uNDropBusiness.getCentData(CENT_ENTIDAD);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/getDependencyData")
    @Timed
    @PermitAll()
    public Response getDependencyData(@QueryParam("unidad") String unidad){

        Response response;

        Either<IException, List<DependencyData>> getResponseGeneric = uNDropBusiness.getDependencyData(unidad);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/getHijaData")
    @Timed
    @PermitAll()
    public Response getHijaData(@QueryParam("DEPENDE") String DEPENDE){

        Response response;

        Either<IException, List<HijaData>> getResponseGeneric = uNDropBusiness.getHijaData(DEPENDE);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




}