
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de la cuenta Bancaria de una persona en la Universidad Nacional de Colombia
 * 
 * <p>Clase Java para tipoInfoCuenta complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoCuenta">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NumeroDeCuenta" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNumeroDeCuenta"/>
 *         &lt;element name="TipoDeCuenta" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoDeCuenta"/>
 *         &lt;element name="Banco" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoBanco"/>
 *         &lt;element name="Sucursal" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoSucursal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoCuenta", propOrder = {
    "numeroDeCuenta",
    "tipoDeCuenta",
    "banco",
    "sucursal"
})
public class TipoInfoCuenta {

    @XmlElement(name = "NumeroDeCuenta", required = true)
    protected String numeroDeCuenta;
    @XmlElement(name = "TipoDeCuenta", required = true)
    @XmlSchemaType(name = "string")
    protected TipoTipoDeCuenta tipoDeCuenta;
    @XmlElement(name = "Banco", required = true)
    protected String banco;
    @XmlElement(name = "Sucursal", required = true)
    protected String sucursal;

    /**
     * Obtiene el valor de la propiedad numeroDeCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroDeCuenta() {
        return numeroDeCuenta;
    }

    /**
     * Define el valor de la propiedad numeroDeCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroDeCuenta(String value) {
        this.numeroDeCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoDeCuenta.
     * 
     * @return
     *     possible object is
     *     {@link TipoTipoDeCuenta }
     *     
     */
    public TipoTipoDeCuenta getTipoDeCuenta() {
        return tipoDeCuenta;
    }

    /**
     * Define el valor de la propiedad tipoDeCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTipoDeCuenta }
     *     
     */
    public void setTipoDeCuenta(TipoTipoDeCuenta value) {
        this.tipoDeCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad banco.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanco() {
        return banco;
    }

    /**
     * Define el valor de la propiedad banco.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanco(String value) {
        this.banco = value;
    }

    /**
     * Obtiene el valor de la propiedad sucursal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * Define el valor de la propiedad sucursal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSucursal(String value) {
        this.sucursal = value;
    }

}
