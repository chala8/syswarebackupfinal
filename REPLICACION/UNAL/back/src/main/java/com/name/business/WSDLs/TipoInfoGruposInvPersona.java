
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de un participante de proyectos de investigaci�n o extensi�n para la universidad nacional de colombia
 * 
 * <p>Clase Java para tipoInfoGruposInvPersona complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoGruposInvPersona">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GruposDeInvestigacion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoGrupoInv"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoGruposInvPersona", propOrder = {
    "gruposDeInvestigacion"
})
public class TipoInfoGruposInvPersona {

    @XmlElement(name = "GruposDeInvestigacion", required = true)
    protected TipoInfoGrupoInv gruposDeInvestigacion;

    /**
     * Obtiene el valor de la propiedad gruposDeInvestigacion.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoGrupoInv }
     *     
     */
    public TipoInfoGrupoInv getGruposDeInvestigacion() {
        return gruposDeInvestigacion;
    }

    /**
     * Define el valor de la propiedad gruposDeInvestigacion.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoGrupoInv }
     *     
     */
    public void setGruposDeInvestigacion(TipoInfoGrupoInv value) {
        this.gruposDeInvestigacion = value;
    }

}
