package com.name.business.entities;

public class HijaData {

    private String codigodependencia;
    private String nombredependencia;


    public HijaData(String codigodependencia, String nombredependencia) {
        this.codigodependencia = codigodependencia;
        this.nombredependencia = nombredependencia;
    }


    public String getCodigodependencia() {
        return codigodependencia;
    }

    public void setCodigodependencia(String codigodependencia) {
        this.codigodependencia = codigodependencia;
    }

    public String getNombredependencia() {
        return nombredependencia;
    }

    public void setNombredependencia(String nombredependencia) {
        this.nombredependencia = nombredependencia;
    }
}
