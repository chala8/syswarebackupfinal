
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n ubicacion de una dependencia para IOP
 * 
 * <p>Clase Java para tipoInfoUbicacionDependencia complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoUbicacionDependencia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Edificio" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEdificio"/>
 *         &lt;element name="Oficina" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoOficina"/>
 *         &lt;element name="ext" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoExtensionTelefonica"/>
 *         &lt;element name="CorreoElectronico" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCorreoE"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoUbicacionDependencia", propOrder = {
    "edificio",
    "oficina",
    "ext",
    "correoElectronico"
})
public class TipoInfoUbicacionDependencia {

    @XmlElement(name = "Edificio", required = true)
    protected TipoEdificio edificio;
    @XmlElement(name = "Oficina", required = true)
    protected TipoOficina oficina;
    @XmlElement(required = true)
    protected String ext;
    @XmlElement(name = "CorreoElectronico", required = true)
    protected String correoElectronico;

    /**
     * Obtiene el valor de la propiedad edificio.
     * 
     * @return
     *     possible object is
     *     {@link TipoEdificio }
     *     
     */
    public TipoEdificio getEdificio() {
        return edificio;
    }

    /**
     * Define el valor de la propiedad edificio.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEdificio }
     *     
     */
    public void setEdificio(TipoEdificio value) {
        this.edificio = value;
    }

    /**
     * Obtiene el valor de la propiedad oficina.
     * 
     * @return
     *     possible object is
     *     {@link TipoOficina }
     *     
     */
    public TipoOficina getOficina() {
        return oficina;
    }

    /**
     * Define el valor de la propiedad oficina.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoOficina }
     *     
     */
    public void setOficina(TipoOficina value) {
        this.oficina = value;
    }

    /**
     * Obtiene el valor de la propiedad ext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExt() {
        return ext;
    }

    /**
     * Define el valor de la propiedad ext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExt(String value) {
        this.ext = value;
    }

    /**
     * Obtiene el valor de la propiedad correoElectronico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    /**
     * Define el valor de la propiedad correoElectronico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoElectronico(String value) {
        this.correoElectronico = value;
    }

}
