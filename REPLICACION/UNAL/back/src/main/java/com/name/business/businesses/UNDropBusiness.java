package com.name.business.businesses;

import com.name.business.entities.*;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;


import com.name.business.DAOs.*;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.FileOutputStream;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import com.name.business.WSDLs.*;
public class UNDropBusiness {

    private UNDropDAO billDAO;
    public UNDropBusiness(UNDropDAO billDAO) {
        this.billDAO = billDAO;
    }


    public Either<IException, BPUNData > getBPUNData(String tipo,String cc  ){

        try{
            System.out.println("business: "+tipo+","+cc);
            BPUNData list = billDAO.getBPUNData(tipo,cc);
            return Either.right(list);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }

    }

    public Either<IException, SaludData > getSaludData(String tipo,String cc  ){

        try{
            System.out.println("business: "+tipo+","+cc);
            SaludData list = billDAO.getSaludData(cc,tipo);
            return Either.right(list);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }

    }


    public Either<IException, List<CentData> > getCentData(String CENT_ENTIDAD  ){

        try{
            List<CentData> list = billDAO.getCentData(CENT_ENTIDAD);
            return Either.right(list);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }

    }


    public Either<IException, List<DependencyData> > getDependencyData(String unidad ){

        try{
            List<DependencyData> list = billDAO.getDependencyData(unidad);
            return Either.right(list);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }

    }


    public Either<IException, List<HijaData> > getHijaData(String DEPENDE){

        try{

            List<HijaData> list = billDAO.getHijaData(DEPENDE);
            return Either.right(list);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }

    }


    Boolean doIUpdate= true;

    public Either<IException, String> getCC(Boolean docUse, Boolean UN, Boolean BPUN, Boolean SIBU, Boolean QUIPU, String docType, String doc) {
        try {

            System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");


            List <TypeDocument> ListElement2;
            if(docUse){
                TypeDocument a = new TypeDocument(docType,doc);
                ListElement2 = new ArrayList<>();
                ListElement2.add(a);
            }else{
                ListElement2 = billDAO.getCC("P");
            }


            //TypeDocument element2 = ListElement.get(0);
            //ListElement2.add(element2);

            for (TypeDocument element:ListElement2) {

                try {
                    System.out.println(element.getCedula());
                    System.out.println(element.getTipoDocumentoSoa());
                    GestiongeneralterceroClientEp wsdl = new GestiongeneralterceroClientEp();
                    GestionGeneralTerceroPortType instancedWSDL = wsdl.getGestionGeneralTerceroPort();
                    TipoMsjSolicitudPorIdTercero objectCC = new TipoMsjSolicitudPorIdTercero();
                    TipoInfoMensaje infoMensaje = new TipoInfoMensaje();

                    GregorianCalendar calendar = new GregorianCalendar();
                    calendar.setTime(new Date());
                    XMLGregorianCalendar calendarFinal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);

                    infoMensaje.setFechaHora(calendarFinal);
                    infoMensaje.setIdSede("01");
                    infoMensaje.setIdSistema("100005");

                    objectCC.setInfoMensaje(infoMensaje);

                    TipoIdTercero idType = new TipoIdTercero();

                    idType.setNumeroDocumento(element.getCedula());


                    idType.setTipoDocumento(TipoTipoDocumento.CC);

                    if (element.getTipoDocumentoSoa() == "TI") {
                        idType.setTipoDocumento(TipoTipoDocumento.TI);
                    }

                    if (element.getTipoDocumentoSoa() == "CE") {
                        idType.setTipoDocumento(TipoTipoDocumento.CE);
                    }

                    if (element.getTipoDocumentoSoa() == "PA") {
                        idType.setTipoDocumento(TipoTipoDocumento.PA);
                    }

                    if (element.getTipoDocumentoSoa() == "RC") {
                        idType.setTipoDocumento(TipoTipoDocumento.RC);
                    }

                    if (element.getTipoDocumentoSoa() == "NI") {
                        idType.setTipoDocumento(TipoTipoDocumento.NI);
                    }

                    if (element.getTipoDocumentoSoa() == "IV") {
                        idType.setTipoDocumento(TipoTipoDocumento.IV);
                    }


                    objectCC.setIdTercero(idType);


                    if (UN) {
                        TipoMsjRespuestaOp response = instancedWSDL.opReplicarEmpleado(objectCC);
                        System.out.println("RESPONSE UN: " + response.getRespuestaOP().getMsjError() + ", " + response.getRespuestaOP().getResultadoOp().value());
                    }


                    if (BPUN) {
                        String responseBPUN = executeBPUN(element.getCedula(), element.getTipoDocumentoSoa());
                        System.out.println("RESPONSE BPUN: " + responseBPUN);
                    }


                    if (SIBU) {
                        String responseSIBU = executeSIBUV2(element.getCedula(), element.getTipoDocumentoSoa());
                        if (responseSIBU.contains("FALLO")) {
                            System.out.println("RESPONSE SIBU: " + responseSIBU);
                        }
                    }


                    if (QUIPU) {
                        String responseBPUN = executeQUIPU(element.getCedula(), element.getTipoDocumentoSoa());
                        System.out.println("RESPONSE QUIPU: " + responseBPUN);
                    }




                    if(true){
                        billDAO.updateCC(element.getCedula(),"X");
                    }

                }catch(Exception e){
                    e.printStackTrace();
                }

                doIUpdate = false;
            }

            return Either.right("OK");

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }

    }


    String executeBPUN(String cc,  String tipo){
        String error = "";
        try{

            System.out.println("1 - ");
            String type = tipo;

            if(tipo.equals("CC")){
                type="C";
            }

            BPUNData data = billDAO.getBPUNData(type,cc);

            System.out.println("2 - ");
            String dataS = data.getCODCARGO()+"º"+data.getCODSEDE()+"º"+data.getCODUNIDAD()+"º"+data.getCODVINCULACION()+"º"+data.getEMAIL()+"º"+data.getNOMBRECARGO()+"º"+data.getNOMBRESEDE()+"º"+data.getNombreunidad()+"º"+data.getVINCULACION()+"º"+data.getCODUNIDADPADRE()+"-"+data.getNOMBREUNIDADPADRE();

            System.out.println(dataS);
            System.out.println("3 - ");
            URL url = new URL("http://168.176.6.92:8393/v1/sara/cc");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            conn.setDoOutput(true);


            System.out.println("4 - ");
            String jsonInputString = "{\n" +
                    "    \"type\" : \""+tipo+"\",\n" +
                    "    \"data\" : \""+dataS+"\",\n" +
                    "    \"number\" : \""+cc+"\"\n" +
                    "}";



            OutputStream os = conn.getOutputStream();
            os.write(jsonInputString.getBytes());
            os.flush();
            os.close();

            System.out.println("5 - ");
            System.out.println(conn.getResponseCode());
            System.out.println(conn.getResponseMessage());

            if (conn.getResponseCode() != 200) {
                InputStreamReader in2 = new InputStreamReader(conn.getErrorStream());
                BufferedReader br2 = new BufferedReader(in2);
                String output2;
                System.out.println("ERROR: ");
                String ErrorOutput = "";
                while ((output2 = br2.readLine()) != null) {
                    System.out.println(output2);
                    ErrorOutput+=(" ; "+output2);
                }
                error = ErrorOutput.replaceAll(";","-").replaceAll("\\{","").replaceAll("\\}","").replaceAll("\\[","").replaceAll("\\]","").replaceAll(":","");
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());

            }else{
                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;

                while ((output = br.readLine()) != null) {
                    System.out.println(output);
                }
                conn.disconnect();
                insertLog(cc,"BPUN","EXITO","SE REALIZO REPLICACION CON EXITO.");
                return "IT WORKED";
            }
        }catch (Exception e){
            e.printStackTrace();
            insertLog(cc,"BPUN","FALLO",e.getMessage()+"-"+error);
            doIUpdate = false;
            return "IT FAILED";
        }

    }


    String executeSIBUV2(String cc,  String tipo){
        String error = "";
        try{

            URL url = new URL("http://168.176.6.92:8392/v1/sara/cc");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            conn.setDoOutput(true);
            String salud = "0_0_0_0_0";
            try {
                String docForSalud = tipo.substring(0,1);
                SaludData responser = billDAO.getSaludData(cc, docForSalud);
                salud = responser.getEntidadsalud() + "_" + responser.getFondopension() + "_" + responser.getCelular() + "_" + responser.getGruposanguineo() + "_" + responser.getLugexpedicion();
            }catch(Exception e){
                System.out.println("FAILED TO LOAD SALUD STRING");
            }
            String jsonInputString = "{\n" +
                    "    \"type\" : \""+tipo+"\",\n" +
                    "    \"salud\" : \""+salud+"\",\n" +
                    "    \"number\" : \""+cc+"\"\n" +
                    "}";

            System.out.println(jsonInputString);



            OutputStream os = conn.getOutputStream();
            os.write(jsonInputString.getBytes());
            os.flush();
            os.close();


            System.out.println(conn.getResponseCode());
            System.out.println(conn.getResponseMessage());

            if (conn.getResponseCode() != 200) {
                InputStreamReader in2 = new InputStreamReader(conn.getErrorStream());
                BufferedReader br2 = new BufferedReader(in2);
                String output2;
                System.out.println("ERROR: ");
                String ErrorOutput = "";
                while ((output2 = br2.readLine()) != null) {
                    System.out.println(output2);
                    ErrorOutput+=(" ; "+output2);
                }
                error = ErrorOutput.replaceAll(";","-").replaceAll("\\{","").replaceAll("\\}","").replaceAll("\\[","").replaceAll("\\]","").replaceAll(":","");
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());

            }else{
                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;

                while ((output = br.readLine()) != null) {
                    System.out.println(output);
                }
                conn.disconnect();
                insertLog(cc,"SIBU","EXITO","SE REALIZO REPLICACION CON EXITO.");
                return "IT WORKED";
            }
        }catch (Exception e){
            e.printStackTrace();
            insertLog(cc,"SIBU","FALLO",e.getMessage()+"-"+error);
            doIUpdate = false;
            return "IT FAILED";
        }

    }



    String executeQUIPU(String cc,  String tipo){
        String error = "";
        try{

            System.out.println("1 - ");
            String type = tipo;

            if(tipo.equals("CC")){
                type="C";
            }



            System.out.println("3 - ");
            URL url = new URL("http://168.176.6.92:8395/v1/sara/cc");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            conn.setDoOutput(true);


            System.out.println("4 - ");
            String jsonInputString = "{\n" +
                    "    \"type\" : \""+tipo+"\",\n" +
                    "    \"number\" : \""+cc+"\"\n" +
                    "}";



            OutputStream os = conn.getOutputStream();
            os.write(jsonInputString.getBytes());
            os.flush();
            os.close();

            System.out.println("5 - ");
            System.out.println(conn.getResponseCode());
            System.out.println(conn.getResponseMessage());

            if (conn.getResponseCode() != 200) {
                InputStreamReader in2 = new InputStreamReader(conn.getErrorStream());
                BufferedReader br2 = new BufferedReader(in2);
                String output2;
                System.out.println("ERROR: ");
                String ErrorOutput = "";
                while ((output2 = br2.readLine()) != null) {
                    System.out.println(output2);
                    ErrorOutput+=(" ; "+output2);
                }
                error = ErrorOutput.replaceAll(":","").replaceAll(";","-").replaceAll("\\{","").replaceAll("\\}","").replaceAll("\\[","").replaceAll("\\]","");
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());

            }else{
                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;

                while ((output = br.readLine()) != null) {
                    System.out.println(output);
                }
                conn.disconnect();
                insertLog(cc,"QUIPU","EXITO","SE REALIZO REPLICACION CON EXITO.");
                return "IT WORKED";
            }
        }catch (Exception e){
            e.printStackTrace();
            insertLog(cc,"QUIPU","FALLO",error.replaceAll(":","").replaceAll("\"","").replaceAll("\\\\",""));
            doIUpdate = false;
            return "IT FAILED";
        }

    }


    void insertLog( String cedula, String sistema, String estado, String mensaje){
        try {
            URL url = new URL("http://168.176.6.92:8396/v1/log/insertLog");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            conn.setDoOutput(true);


            System.out.println("6 - ");
            String jsonInputString = "{\n" +
                    "    \"cedula\" : \""+cedula+"\",\n" +
                    "    \"sistema\" : \""+sistema+"\",\n" +
                    "    \"estado\" : \""+estado+"\",\n" +
                    "    \"mensaje\" : \""+mensaje+"\"\n" +
                    "}";


            OutputStream os = conn.getOutputStream();
            os.write(jsonInputString.getBytes());
            os.flush();
            os.close();

            System.out.println(jsonInputString);
            System.out.println("7 - ");
            System.out.println(conn.getResponseCode());
            System.out.println(conn.getResponseMessage());

            if (conn.getResponseCode() != 200) {
                InputStreamReader in2 = new InputStreamReader(conn.getErrorStream());
                BufferedReader br2 = new BufferedReader(in2);
                String output2;
                System.out.println("ERROR: ");
                String ErrorOutput = "";
                while ((output2 = br2.readLine()) != null) {
                    System.out.println(output2);
                    ErrorOutput += (" ; " + output2);
                }
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());

            } else {
                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;

                while ((output = br.readLine()) != null) {
                    System.out.println(output);
                }
                conn.disconnect();
                System.out.println("LOGGED");
            }
        }catch(Exception e){

        }
    }





}