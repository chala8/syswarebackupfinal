
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoMsjDetalleContratos complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjDetalleContratos">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="elemDetallesContratos" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDetalleContratoSARA"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjDetalleContratos", propOrder = {
    "elemDetallesContratos"
})
public class TipoMsjDetalleContratos
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected List<TipoDetalleContratoSARA> elemDetallesContratos;

    /**
     * Gets the value of the elemDetallesContratos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elemDetallesContratos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElemDetallesContratos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoDetalleContratoSARA }
     * 
     * 
     */
    public List<TipoDetalleContratoSARA> getElemDetallesContratos() {
        if (elemDetallesContratos == null) {
            elemDetallesContratos = new ArrayList<TipoDetalleContratoSARA>();
        }
        return this.elemDetallesContratos;
    }

}
