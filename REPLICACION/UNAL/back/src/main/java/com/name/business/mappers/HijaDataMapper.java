package com.name.business.mappers;

import com.name.business.entities.CentData;
import com.name.business.entities.HijaData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HijaDataMapper implements ResultSetMapper<HijaData> {
    @Override
    public HijaData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new HijaData(
                resultSet.getString("codigodependencia"),
                resultSet.getString("nombredependencia")
        );
    }
}