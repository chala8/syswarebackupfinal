
package com.name.business.WSDLs;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * indica el monto tanto en moneda como en valor para interop
 * 
 * <p>Clase Java para tipoMontoMonetario complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMontoMonetario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idMoneda" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdMoneda"/>
 *         &lt;element name="valor" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoValorMonetario"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMontoMonetario", propOrder = {
    "idMoneda",
    "valor"
})
public class TipoMontoMonetario {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoIdMoneda idMoneda;
    @XmlElement(required = true)
    protected BigDecimal valor;

    /**
     * Obtiene el valor de la propiedad idMoneda.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdMoneda }
     *     
     */
    public TipoIdMoneda getIdMoneda() {
        return idMoneda;
    }

    /**
     * Define el valor de la propiedad idMoneda.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdMoneda }
     *     
     */
    public void setIdMoneda(TipoIdMoneda value) {
        this.idMoneda = value;
    }

    /**
     * Obtiene el valor de la propiedad valor.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Define el valor de la propiedad valor.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValor(BigDecimal value) {
        this.valor = value;
    }

}
