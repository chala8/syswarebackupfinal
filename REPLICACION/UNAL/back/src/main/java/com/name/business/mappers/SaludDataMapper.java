package com.name.business.mappers;

import com.name.business.entities.SaludData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SaludDataMapper implements ResultSetMapper<SaludData> {
    @Override
    public SaludData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new SaludData(
                resultSet.getString("lugexpedicion"),
                resultSet.getString("gruposanguineo"),
                resultSet.getString("celular"),
                resultSet.getString("fondopension"),
                resultSet.getString("entidadsalud")
        );
    }
}