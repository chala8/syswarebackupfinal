
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para dar la respuesta de la existencia o no de una entidad para interop
 * 
 * <p>Clase Java para tipoMsjRespuestaExistencia complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaExistencia">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="confirmacionExistencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoConfirmacionExistencia"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaExistencia", propOrder = {
    "confirmacionExistencia"
})
@XmlSeeAlso({
    TipoMsjRespuestaExistenciaUsuarioALEPH.class
})
public class TipoMsjRespuestaExistencia
    extends TipoMsjGenerico
{

    protected boolean confirmacionExistencia;

    /**
     * Obtiene el valor de la propiedad confirmacionExistencia.
     * 
     */
    public boolean isConfirmacionExistencia() {
        return confirmacionExistencia;
    }

    /**
     * Define el valor de la propiedad confirmacionExistencia.
     * 
     */
    public void setConfirmacionExistencia(boolean value) {
        this.confirmacionExistencia = value;
    }

}
