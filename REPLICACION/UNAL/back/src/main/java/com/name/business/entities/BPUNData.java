package com.name.business.entities;

public class BPUNData {

    private String CODSEDE;
    private String NOMBRESEDE;
    private String CODUNIDAD;
    private String nombreunidad;
    private String CODCARGO;
    private String NOMBRECARGO;
    private String CODVINCULACION;
    private String VINCULACION;
    private String EMAIL;
    private String CODUNIDADPADRE;
    private String NOMBREUNIDADPADRE;


    public BPUNData(String CODUNIDADPADRE, String NOMBREUNIDADPADRE, String CODSEDE, String NOMBRESEDE, String CODUNIDAD, String nombreunidad, String CODCARGO, String NOMBRECARGO, String CODVINCULACION, String VINCULACION, String EMAIL) {
        this.CODSEDE = CODSEDE;
        this.NOMBRESEDE = NOMBRESEDE;
        this.CODUNIDAD = CODUNIDAD;
        this.nombreunidad = nombreunidad;
        this.CODCARGO = CODCARGO;
        this.NOMBRECARGO = NOMBRECARGO;
        this.CODVINCULACION = CODVINCULACION;
        this.VINCULACION = VINCULACION;
        this.EMAIL = EMAIL;
        this.NOMBREUNIDADPADRE = NOMBREUNIDADPADRE;
        this.CODUNIDADPADRE = CODUNIDADPADRE;
    }

    public String getCODUNIDADPADRE() {
        return CODUNIDADPADRE;
    }

    public void setCODUNIDADPADRE(String CODUNIDADPADRE) {
        this.CODUNIDADPADRE = CODUNIDADPADRE;
    }

    public String getNOMBREUNIDADPADRE() {
        return NOMBREUNIDADPADRE;
    }

    public void setNOMBREUNIDADPADRE(String NOMBREUNIDADPADRE) {
        this.NOMBREUNIDADPADRE = NOMBREUNIDADPADRE;
    }

    public String getCODSEDE() {
        return CODSEDE;
    }

    public void setCODSEDE(String CODSEDE) {
        this.CODSEDE = CODSEDE;
    }

    public String getNOMBRESEDE() {
        return NOMBRESEDE;
    }

    public void setNOMBRESEDE(String NOMBRESEDE) {
        this.NOMBRESEDE = NOMBRESEDE;
    }

    public String getCODUNIDAD() {
        return CODUNIDAD;
    }

    public void setCODUNIDAD(String CODUNIDAD) {
        this.CODUNIDAD = CODUNIDAD;
    }

    public String getNombreunidad() {
        return nombreunidad;
    }

    public void setNombreunidad(String nombreunidad) {
        this.nombreunidad = nombreunidad;
    }

    public String getCODCARGO() {
        return CODCARGO;
    }

    public void setCODCARGO(String CODCARGO) {
        this.CODCARGO = CODCARGO;
    }

    public String getNOMBRECARGO() {
        return NOMBRECARGO;
    }

    public void setNOMBRECARGO(String NOMBRECARGO) {
        this.NOMBRECARGO = NOMBRECARGO;
    }

    public String getCODVINCULACION() {
        return CODVINCULACION;
    }

    public void setCODVINCULACION(String CODVINCULACION) {
        this.CODVINCULACION = CODVINCULACION;
    }

    public String getVINCULACION() {
        return VINCULACION;
    }

    public void setVINCULACION(String VINCULACION) {
        this.VINCULACION = VINCULACION;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }
}
