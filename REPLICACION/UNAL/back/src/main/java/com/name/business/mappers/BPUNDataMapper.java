package com.name.business.mappers;

import com.name.business.entities.BPUNData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BPUNDataMapper implements ResultSetMapper<BPUNData> {
    @Override
    public BPUNData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BPUNData(
                resultSet.getString("CODUNIDADPADRE"),
                resultSet.getString("NOMBREUNIDADPADRE"),
                resultSet.getString("CODSEDE"),
                resultSet.getString("NOMBRESEDE"),
                resultSet.getString("CODUNIDAD"),
                resultSet.getString("nombreunidad"),
                resultSet.getString("CODCARGO"),
                resultSet.getString("NOMBRECARGO"),
                resultSet.getString("CODVINCULACION"),
                resultSet.getString("VINCULACION"),
                resultSet.getString("EMAIL")
        );
    }
}