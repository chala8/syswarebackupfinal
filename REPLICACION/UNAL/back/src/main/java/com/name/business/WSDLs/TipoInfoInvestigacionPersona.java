
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de un participante de proyectos de investigaci�n para la universidad nacional de colombia
 * 
 * <p>Clase Java para tipoInfoInvestigacionPersona complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoInvestigacionPersona">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InformacionProyecto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaProyecto"/>
 *         &lt;element name="RollEnElProyecto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoRollProyecto"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoInvestigacionPersona", propOrder = {
    "informacionProyecto",
    "rollEnElProyecto"
})
public class TipoInfoInvestigacionPersona {

    @XmlElement(name = "InformacionProyecto", required = true)
    protected TipoInfoBasicaProyecto informacionProyecto;
    @XmlElement(name = "RollEnElProyecto", required = true)
    @XmlSchemaType(name = "string")
    protected TipoRollProyecto rollEnElProyecto;

    /**
     * Obtiene el valor de la propiedad informacionProyecto.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaProyecto }
     *     
     */
    public TipoInfoBasicaProyecto getInformacionProyecto() {
        return informacionProyecto;
    }

    /**
     * Define el valor de la propiedad informacionProyecto.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaProyecto }
     *     
     */
    public void setInformacionProyecto(TipoInfoBasicaProyecto value) {
        this.informacionProyecto = value;
    }

    /**
     * Obtiene el valor de la propiedad rollEnElProyecto.
     * 
     * @return
     *     possible object is
     *     {@link TipoRollProyecto }
     *     
     */
    public TipoRollProyecto getRollEnElProyecto() {
        return rollEnElProyecto;
    }

    /**
     * Define el valor de la propiedad rollEnElProyecto.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoRollProyecto }
     *     
     */
    public void setRollEnElProyecto(TipoRollProyecto value) {
        this.rollEnElProyecto = value;
    }

}
