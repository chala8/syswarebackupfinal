package com.name.business.mappers;

import com.name.business.entities.CentData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CentDataMapper implements ResultSetMapper<CentData> {
    @Override
    public CentData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CentData(
                resultSet.getString("CENT_ENTIDAD"),
                resultSet.getString("CENT_DESCRIPCION")
        );
    }
}