package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.*;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlCall;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import java.util.List;

public interface UNDropDAO {
    @Mapper(TypeDocMapper.class)
    @SqlQuery("select distinct (select documentosoa from vu_unalintegraciontipodoc where trim(hojavida_hov.chov_tipodocumento)=documentosara) as tipoDocumentoSoa,\n" +
            "       memr_cedula as cedula\n" +
            "from empleado_eml,\n" +
            "hojavida_hov,\n" +
            "empleadoreplicado_emr\n" +
            " where meml_hojavida=mhov_identifica\n" +
            "  and mhov_identifica != 0\n" +
            "  and meml_identifica = memr_contrato\n" +
            "  and neml_empresa = nemr_empresa\n" +
            "  and cemr_estado = :CEMR_ESTADO")
    List <TypeDocument> getCC(@Bind("CEMR_ESTADO") String CEMR_ESTADO);

    @SqlUpdate(" UPDATE empleadoreplicado_emr SET CEMR_ESTADO=:estado where memr_cedula=:cedula ")
    void updateCC(@Bind("cedula") String cedula,@Bind("estado") String estado);


    @Mapper(SaludDataMapper.class)
    @SqlQuery("SELECT trim(lugexpedicion) as lugexpedicion,trim(gruposanguineo) as gruposanguineo,trim(celular) as celular,trim(fondopension) as fondopension,trim(entidadsalud) as entidadsalud\n" +
            "    FROM wwtalento.V_INTEGRA_DATOS_COMPLE_SIBU\n" +
            "    WHERE  IDENTIFICACION = :IDENTIFICACION AND trim(TIPODOC) = :TIPODOC")
    SaludData getSaludData(@Bind("IDENTIFICACION") String IDENTIFICACION,@Bind("TIPODOC") String TIPODOC);

    @Mapper(CentDataMapper.class)
    @SqlQuery("SELECT CENT_ENTIDAD,CENT_DESCRIPCION FROM ENTIDAD_ENT WHERE trim(CENT_ENTIDAD) = trim(:CENT_ENTIDAD)")
    List <CentData> getCentData(@Bind("CENT_ENTIDAD") String CENT_ENTIDAD);

    @Mapper(DependencyDataMapper.class)
    @SqlQuery("select nombreunidad nombredependencia,substr(nombreunidad,1,1) codigosede,sucursal nombresede,\n" +
            "        nombres||' '||apellido1||' '||nvl(apellido2,'') nombredirector,\n" +
            "        tipoidentifica tipodoc,identificacion numdoc,email1 email, ubicacion\n" +
            "from VU_INTEGRA_UNIDADCOMPLE\n" +
            "where  trim(unidad)=trim(:unidad)")
    List <DependencyData> getDependencyData(@Bind("unidad") String unidad);


    @Mapper(HijaDataMapper.class)
    @SqlQuery("SELECT codunidad codigodependencia,NOMBREunidad nombredependencia FROM VU_INTEGRA_UNIDAD WHERE trim(DEPENDE) = trim(:DEPENDE)")
    List <HijaData> getHijaData(@Bind("DEPENDE") String DEPENDE);

    @Mapper(BPUNDataMapper.class)
    @SqlQuery("select CODSEDE,  NOMBRESEDE,  CODUNIDAD,trim(substr(NOMBREUNIDAD,3,length(NOMBREUNIDAD))) nombreunidad,CODCARGO,NOMBRECARGO,CODVINCULACION,   VINCULACION,  EMAIL, CODUNIDADPADRE, NOMBREUNIDADPADRE   \n" +
            "from VU_INTEGRA_DATOSEMPLEADO \n" +
            "where trim(tipodoc)=:tipo and identificacion = :identificacion")
    BPUNData getBPUNData(@Bind("tipo") String tipo,@Bind("identificacion") String identificacion);

    @SqlUpdate("insert into LOG_REPLICACION@bdlogiop values (sysdate,:cedula,:sistema,:estado,:mensaje);")
    void logReplicacion (@Bind("tipo") String tipo,@Bind("identificacion") String identificacion);

}
