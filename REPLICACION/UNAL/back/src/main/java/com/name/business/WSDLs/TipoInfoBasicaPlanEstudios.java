
package com.name.business.WSDLs;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoInfoBasicaPlanEstudios complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoBasicaPlanEstudios">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoPlan" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoPlanEstudios"/>
 *         &lt;element name="DescripcionPlan" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDescripcionPlanEstudios"/>
 *         &lt;element name="Promedio" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoPromedio"/>
 *         &lt;element name="PromedioAcumulado" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoPromedioAcumulado"/>
 *         &lt;element name="Estado" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEstadoPlanEstudios"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoBasicaPlanEstudios", propOrder = {
    "codigoPlan",
    "descripcionPlan",
    "promedio",
    "promedioAcumulado",
    "estado"
})
public class TipoInfoBasicaPlanEstudios {

    @XmlElement(name = "CodigoPlan", required = true)
    protected String codigoPlan;
    @XmlElement(name = "DescripcionPlan", required = true)
    protected String descripcionPlan;
    @XmlElement(name = "Promedio", required = true)
    protected BigDecimal promedio;
    @XmlElement(name = "PromedioAcumulado", required = true)
    protected BigDecimal promedioAcumulado;
    @XmlElement(name = "Estado", required = true)
    @XmlSchemaType(name = "string")
    protected TipoEstadoPlanEstudios estado;

    /**
     * Obtiene el valor de la propiedad codigoPlan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPlan() {
        return codigoPlan;
    }

    /**
     * Define el valor de la propiedad codigoPlan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPlan(String value) {
        this.codigoPlan = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcionPlan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionPlan() {
        return descripcionPlan;
    }

    /**
     * Define el valor de la propiedad descripcionPlan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionPlan(String value) {
        this.descripcionPlan = value;
    }

    /**
     * Obtiene el valor de la propiedad promedio.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPromedio() {
        return promedio;
    }

    /**
     * Define el valor de la propiedad promedio.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPromedio(BigDecimal value) {
        this.promedio = value;
    }

    /**
     * Obtiene el valor de la propiedad promedioAcumulado.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPromedioAcumulado() {
        return promedioAcumulado;
    }

    /**
     * Define el valor de la propiedad promedioAcumulado.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPromedioAcumulado(BigDecimal value) {
        this.promedioAcumulado = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstadoPlanEstudios }
     *     
     */
    public TipoEstadoPlanEstudios getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstadoPlanEstudios }
     *     
     */
    public void setEstado(TipoEstadoPlanEstudios value) {
        this.estado = value;
    }

}
