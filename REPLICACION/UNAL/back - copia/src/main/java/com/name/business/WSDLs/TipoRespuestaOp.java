
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * indica la respuesta a una operacion de insercion o actualizacion de una entidad para interop
 * 
 * <p>Clase Java para tipoRespuestaOp complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoRespuestaOp">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="resultadoOp" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoResultadoOp"/>
 *         &lt;element name="idError" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdError"/>
 *         &lt;element name="msjError" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMensajeError"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoRespuestaOp", propOrder = {
    "resultadoOp",
    "idError",
    "msjError"
})
public class TipoRespuestaOp {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoResultadoOp resultadoOp;
    @XmlElement(required = true)
    protected String idError;
    @XmlElement(required = true)
    protected String msjError;

    /**
     * Obtiene el valor de la propiedad resultadoOp.
     * 
     * @return
     *     possible object is
     *     {@link TipoResultadoOp }
     *     
     */
    public TipoResultadoOp getResultadoOp() {
        return resultadoOp;
    }

    /**
     * Define el valor de la propiedad resultadoOp.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoResultadoOp }
     *     
     */
    public void setResultadoOp(TipoResultadoOp value) {
        this.resultadoOp = value;
    }

    /**
     * Obtiene el valor de la propiedad idError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdError() {
        return idError;
    }

    /**
     * Define el valor de la propiedad idError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdError(String value) {
        this.idError = value;
    }

    /**
     * Obtiene el valor de la propiedad msjError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsjError() {
        return msjError;
    }

    /**
     * Define el valor de la propiedad msjError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsjError(String value) {
        this.msjError = value;
    }

}
