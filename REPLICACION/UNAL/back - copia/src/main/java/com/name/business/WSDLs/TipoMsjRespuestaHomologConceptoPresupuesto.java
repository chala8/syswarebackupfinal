
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoMsjRespuestaHomologConceptoPresupuesto complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaHomologConceptoPresupuesto">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="ConceptoPresupuesto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoConceptoPresupuesto"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaHomologConceptoPresupuesto", propOrder = {
    "conceptoPresupuesto"
})
public class TipoMsjRespuestaHomologConceptoPresupuesto
    extends TipoMsjGenerico
{

    @XmlElement(name = "ConceptoPresupuesto", required = true)
    protected TipoConceptoPresupuesto conceptoPresupuesto;

    /**
     * Obtiene el valor de la propiedad conceptoPresupuesto.
     * 
     * @return
     *     possible object is
     *     {@link TipoConceptoPresupuesto }
     *     
     */
    public TipoConceptoPresupuesto getConceptoPresupuesto() {
        return conceptoPresupuesto;
    }

    /**
     * Define el valor de la propiedad conceptoPresupuesto.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoConceptoPresupuesto }
     *     
     */
    public void setConceptoPresupuesto(TipoConceptoPresupuesto value) {
        this.conceptoPresupuesto = value;
    }

}
