
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoEstadoAdministrativo.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoEstadoAdministrativo">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AUS"/>
 *     &lt;enumeration value="CEM"/>
 *     &lt;enumeration value="ESP"/>
 *     &lt;enumeration value="MOP"/>
 *     &lt;enumeration value="RET"/>
 *     &lt;enumeration value="NOR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoEstadoAdministrativo")
@XmlEnum
public enum TipoEstadoAdministrativo {


    /**
     * Ausentismo
     * 
     */
    AUS,

    /**
     * Cambio en el empleado
     * 
     */
    CEM,

    /**
     * Especiales
     * 
     */
    ESP,

    /**
     * Movimiento de planta
     * 
     */
    MOP,

    /**
     * Retiro
     * 
     */
    RET,

    /**
     * Estado Normal
     * 
     */
    NOR;

    public String value() {
        return name();
    }

    public static TipoEstadoAdministrativo fromValue(String v) {
        return valueOf(v);
    }

}
