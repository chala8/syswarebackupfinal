
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de una ciudad para IOP
 * 
 * <p>Clase Java para tipoPais complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoPais">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pais" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDatosPais"/>
 *         &lt;element name="paisAnterior" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDatosPais"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoPais", propOrder = {
    "pais",
    "paisAnterior"
})
public class TipoPais {

    @XmlElement(required = true)
    protected TipoDatosPais pais;
    @XmlElement(required = true)
    protected TipoDatosPais paisAnterior;

    /**
     * Obtiene el valor de la propiedad pais.
     * 
     * @return
     *     possible object is
     *     {@link TipoDatosPais }
     *     
     */
    public TipoDatosPais getPais() {
        return pais;
    }

    /**
     * Define el valor de la propiedad pais.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDatosPais }
     *     
     */
    public void setPais(TipoDatosPais value) {
        this.pais = value;
    }

    /**
     * Obtiene el valor de la propiedad paisAnterior.
     * 
     * @return
     *     possible object is
     *     {@link TipoDatosPais }
     *     
     */
    public TipoDatosPais getPaisAnterior() {
        return paisAnterior;
    }

    /**
     * Define el valor de la propiedad paisAnterior.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDatosPais }
     *     
     */
    public void setPaisAnterior(TipoDatosPais value) {
        this.paisAnterior = value;
    }

}
