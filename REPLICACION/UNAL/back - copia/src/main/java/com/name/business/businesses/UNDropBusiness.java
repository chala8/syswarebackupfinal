package com.name.business.businesses;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.entities.TypeDocument;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.DAOs.*;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.FileOutputStream;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import com.name.business.WSDLs.*;
public class UNDropBusiness {

    private UNDropDAO billDAO;
    public UNDropBusiness(UNDropDAO billDAO) {
        this.billDAO = billDAO;
    }

    public Either<IException, String> getCC() {
        try {

            List <TypeDocument> ListElement = billDAO.getCC("P");


            for (TypeDocument element:ListElement) {
                GestiongeneralterceroClientEp wsdl = new GestiongeneralterceroClientEp();
                GestionGeneralTerceroPortType instancedWSDL = wsdl.getGestionGeneralTerceroPort();
                TipoMsjSolicitudPorIdTercero objectCC = new TipoMsjSolicitudPorIdTercero();
                TipoInfoMensaje infoMensaje = new TipoInfoMensaje();

                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(new Date());
                XMLGregorianCalendar calendarFinal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);

                infoMensaje.setFechaHora(calendarFinal);
                infoMensaje.setIdSede("01");
                infoMensaje.setIdSistema("100005");

                objectCC.setInfoMensaje(infoMensaje);

                TipoIdTercero idType = new TipoIdTercero();

                idType.setNumeroDocumento(element.getCedula());


                idType.setTipoDocumento(TipoTipoDocumento.CC);

                if(element.getTipoDocumentoSoa()=="TI"){
                    idType.setTipoDocumento(TipoTipoDocumento.TI);
                }

                if(element.getTipoDocumentoSoa()=="CE"){
                    idType.setTipoDocumento(TipoTipoDocumento.CE);
                }

                if(element.getTipoDocumentoSoa()=="PA"){
                    idType.setTipoDocumento(TipoTipoDocumento.PA);
                }

                if(element.getTipoDocumentoSoa()=="RC"){
                    idType.setTipoDocumento(TipoTipoDocumento.RC);
                }

                if(element.getTipoDocumentoSoa()=="NI"){
                    idType.setTipoDocumento(TipoTipoDocumento.NI);
                }

                if(element.getTipoDocumentoSoa()=="IV"){
                    idType.setTipoDocumento(TipoTipoDocumento.IV);
                }



                objectCC.setIdTercero(idType);

                TipoMsjRespuestaOp response = instancedWSDL.opReplicarEmpleado(objectCC);

                System.out.println(response.getRespuestaOP().getMsjError() + ", " + response.getRespuestaOP().getResultadoOp().value());
            }

            return Either.right("OK");

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


}