
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de un participante de proyectos de investigaci�n o extensi�n para la universidad nacional de colombia
 * 
 * <p>Clase Java para tipoPersonaHermes complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoPersonaHermes">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InformacionBasica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaPersonal"/>
 *         &lt;element name="CiudadDeExpedicionID" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdCiudad"/>
 *         &lt;element name="InformacionContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoContacto"/>
 *         &lt;element name="InformacionTributaria" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoTributariaTercero"/>
 *         &lt;sequence maxOccurs="unbounded">
 *           &lt;element name="Vinculaciones" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoContrato"/>
 *         &lt;/sequence>
 *         &lt;element name="Edificio" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEdificio"/>
 *         &lt;element name="Oficina" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoOficina"/>
 *         &lt;element name="ExtensionTelefonica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoExtensionTelefonica"/>
 *         &lt;element name="NombreCargo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombreCargoEmpleado"/>
 *         &lt;element name="NombreVinculacion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombreVinculacionEmpleado"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoPersonaHermes", propOrder = {
    "informacionBasica",
    "ciudadDeExpedicionID",
    "informacionContacto",
    "informacionTributaria",
    "vinculaciones",
    "edificio",
    "oficina",
    "extensionTelefonica",
    "nombreCargo",
    "nombreVinculacion"
})
public class TipoPersonaHermes {

    @XmlElement(name = "InformacionBasica", required = true)
    protected TipoInfoBasicaPersonal informacionBasica;
    @XmlElement(name = "CiudadDeExpedicionID", required = true)
    protected String ciudadDeExpedicionID;
    @XmlElement(name = "InformacionContacto", required = true)
    protected TipoInfoContacto informacionContacto;
    @XmlElement(name = "InformacionTributaria", required = true)
    protected TipoInfoTributariaTercero informacionTributaria;
    @XmlElement(name = "Vinculaciones", required = true)
    protected List<TipoInfoContrato> vinculaciones;
    @XmlElement(name = "Edificio", required = true)
    protected TipoEdificio edificio;
    @XmlElement(name = "Oficina", required = true)
    protected TipoOficina oficina;
    @XmlElement(name = "ExtensionTelefonica", required = true)
    protected String extensionTelefonica;
    @XmlElement(name = "NombreCargo", required = true)
    protected String nombreCargo;
    @XmlElement(name = "NombreVinculacion", required = true)
    protected String nombreVinculacion;

    /**
     * Obtiene el valor de la propiedad informacionBasica.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPersonal }
     *     
     */
    public TipoInfoBasicaPersonal getInformacionBasica() {
        return informacionBasica;
    }

    /**
     * Define el valor de la propiedad informacionBasica.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPersonal }
     *     
     */
    public void setInformacionBasica(TipoInfoBasicaPersonal value) {
        this.informacionBasica = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudadDeExpedicionID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadDeExpedicionID() {
        return ciudadDeExpedicionID;
    }

    /**
     * Define el valor de la propiedad ciudadDeExpedicionID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadDeExpedicionID(String value) {
        this.ciudadDeExpedicionID = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionContacto.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoContacto }
     *     
     */
    public TipoInfoContacto getInformacionContacto() {
        return informacionContacto;
    }

    /**
     * Define el valor de la propiedad informacionContacto.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoContacto }
     *     
     */
    public void setInformacionContacto(TipoInfoContacto value) {
        this.informacionContacto = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionTributaria.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoTributariaTercero }
     *     
     */
    public TipoInfoTributariaTercero getInformacionTributaria() {
        return informacionTributaria;
    }

    /**
     * Define el valor de la propiedad informacionTributaria.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoTributariaTercero }
     *     
     */
    public void setInformacionTributaria(TipoInfoTributariaTercero value) {
        this.informacionTributaria = value;
    }

    /**
     * Gets the value of the vinculaciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vinculaciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVinculaciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoContrato }
     * 
     * 
     */
    public List<TipoInfoContrato> getVinculaciones() {
        if (vinculaciones == null) {
            vinculaciones = new ArrayList<TipoInfoContrato>();
        }
        return this.vinculaciones;
    }

    /**
     * Obtiene el valor de la propiedad edificio.
     * 
     * @return
     *     possible object is
     *     {@link TipoEdificio }
     *     
     */
    public TipoEdificio getEdificio() {
        return edificio;
    }

    /**
     * Define el valor de la propiedad edificio.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEdificio }
     *     
     */
    public void setEdificio(TipoEdificio value) {
        this.edificio = value;
    }

    /**
     * Obtiene el valor de la propiedad oficina.
     * 
     * @return
     *     possible object is
     *     {@link TipoOficina }
     *     
     */
    public TipoOficina getOficina() {
        return oficina;
    }

    /**
     * Define el valor de la propiedad oficina.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoOficina }
     *     
     */
    public void setOficina(TipoOficina value) {
        this.oficina = value;
    }

    /**
     * Obtiene el valor de la propiedad extensionTelefonica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtensionTelefonica() {
        return extensionTelefonica;
    }

    /**
     * Define el valor de la propiedad extensionTelefonica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtensionTelefonica(String value) {
        this.extensionTelefonica = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCargo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCargo() {
        return nombreCargo;
    }

    /**
     * Define el valor de la propiedad nombreCargo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCargo(String value) {
        this.nombreCargo = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreVinculacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreVinculacion() {
        return nombreVinculacion;
    }

    /**
     * Define el valor de la propiedad nombreVinculacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreVinculacion(String value) {
        this.nombreVinculacion = value;
    }

}
