
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de solicitud para la actualizacion de un usuario en el sistema de informaci�n bibliografica ALEPH
 * 
 * <p>Clase Java para tipoMsjSolicitudActualizacionUsuarioALEPH complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudActualizacionUsuarioALEPH">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Usuario" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoUsuarioALEPH"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudActualizacionUsuarioALEPH", propOrder = {
    "usuario"
})
public class TipoMsjSolicitudActualizacionUsuarioALEPH
    extends TipoMsjGenerico
{

    @XmlElement(name = "Usuario", required = true)
    protected List<TipoUsuarioALEPH> usuario;

    /**
     * Gets the value of the usuario property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usuario property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsuario().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoUsuarioALEPH }
     * 
     * 
     */
    public List<TipoUsuarioALEPH> getUsuario() {
        if (usuario == null) {
            usuario = new ArrayList<TipoUsuarioALEPH>();
        }
        return this.usuario;
    }

}
