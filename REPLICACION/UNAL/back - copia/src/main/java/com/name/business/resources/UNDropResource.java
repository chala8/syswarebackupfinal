package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.UNDropBusiness;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;


@Path("/DropUN")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UNDropResource {
    private UNDropBusiness uNDropBusiness;

    public UNDropResource(UNDropBusiness uNDropBusiness) {
        this.uNDropBusiness = uNDropBusiness;
    }

    @GET
    @Path("/sql")
    @Timed
    @PermitAll()
    public Response getBillResource(){

        Response response;

        Either<IException, String> getResponseGeneric = uNDropBusiness.getCC();

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

}