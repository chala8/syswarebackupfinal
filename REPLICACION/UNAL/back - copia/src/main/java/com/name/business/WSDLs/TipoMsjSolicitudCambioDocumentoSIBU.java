
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de respuesta para el cambio de un documento
 * 
 * <p>Clase Java para tipoMsjSolicitudCambioDocumentoSIBU complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudCambioDocumentoSIBU">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="Identificaciones" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCambioDocumento"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudCambioDocumentoSIBU", propOrder = {
    "identificaciones"
})
public class TipoMsjSolicitudCambioDocumentoSIBU
    extends TipoMsjGenerico
{

    @XmlElement(name = "Identificaciones", required = true)
    protected TipoCambioDocumento identificaciones;

    /**
     * Obtiene el valor de la propiedad identificaciones.
     * 
     * @return
     *     possible object is
     *     {@link TipoCambioDocumento }
     *     
     */
    public TipoCambioDocumento getIdentificaciones() {
        return identificaciones;
    }

    /**
     * Define el valor de la propiedad identificaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCambioDocumento }
     *     
     */
    public void setIdentificaciones(TipoCambioDocumento value) {
        this.identificaciones = value;
    }

}
