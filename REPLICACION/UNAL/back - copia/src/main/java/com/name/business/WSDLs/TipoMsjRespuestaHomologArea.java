
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoMsjRespuestaHomologArea complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaHomologArea">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="CodigoAreaResponsabilidad" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="DescripcionAreaResponsabilidad" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaHomologArea", propOrder = {
    "codigoAreaResponsabilidadAndDescripcionAreaResponsabilidad"
})
public class TipoMsjRespuestaHomologArea
    extends TipoMsjGenerico
{

    @XmlElementRefs({
        @XmlElementRef(name = "DescripcionAreaResponsabilidad", namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", type = JAXBElement.class),
        @XmlElementRef(name = "CodigoAreaResponsabilidad", namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", type = JAXBElement.class)
    })
    protected List<JAXBElement<Object>> codigoAreaResponsabilidadAndDescripcionAreaResponsabilidad;

    /**
     * Gets the value of the codigoAreaResponsabilidadAndDescripcionAreaResponsabilidad property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codigoAreaResponsabilidadAndDescripcionAreaResponsabilidad property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodigoAreaResponsabilidadAndDescripcionAreaResponsabilidad().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * 
     */
    public List<JAXBElement<Object>> getCodigoAreaResponsabilidadAndDescripcionAreaResponsabilidad() {
        if (codigoAreaResponsabilidadAndDescripcionAreaResponsabilidad == null) {
            codigoAreaResponsabilidadAndDescripcionAreaResponsabilidad = new ArrayList<JAXBElement<Object>>();
        }
        return this.codigoAreaResponsabilidadAndDescripcionAreaResponsabilidad;
    }

}
