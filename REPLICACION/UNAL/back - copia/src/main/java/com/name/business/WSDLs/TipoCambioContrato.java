
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoCambioContrato complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoCambioContrato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Contrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoContrato"/>
 *         &lt;element name="ContratoAnterior" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoContrato"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoCambioContrato", propOrder = {
    "contrato",
    "contratoAnterior"
})
public class TipoCambioContrato {

    @XmlElement(name = "Contrato", required = true)
    protected TipoContrato contrato;
    @XmlElement(name = "ContratoAnterior", required = true)
    protected TipoContrato contratoAnterior;

    /**
     * Obtiene el valor de la propiedad contrato.
     * 
     * @return
     *     possible object is
     *     {@link TipoContrato }
     *     
     */
    public TipoContrato getContrato() {
        return contrato;
    }

    /**
     * Define el valor de la propiedad contrato.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoContrato }
     *     
     */
    public void setContrato(TipoContrato value) {
        this.contrato = value;
    }

    /**
     * Obtiene el valor de la propiedad contratoAnterior.
     * 
     * @return
     *     possible object is
     *     {@link TipoContrato }
     *     
     */
    public TipoContrato getContratoAnterior() {
        return contratoAnterior;
    }

    /**
     * Define el valor de la propiedad contratoAnterior.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoContrato }
     *     
     */
    public void setContratoAnterior(TipoContrato value) {
        this.contratoAnterior = value;
    }

}
