
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoDetalleDocente complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoDetalleDocente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Asignatura" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoAsignatura"/>
 *         &lt;element name="NumVecesDictada" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNumVecesAsignaturas"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoDetalleDocente", propOrder = {
    "asignatura",
    "numVecesDictada"
})
public class TipoDetalleDocente {

    @XmlElement(name = "Asignatura", required = true)
    protected TipoAsignatura asignatura;
    @XmlElement(name = "NumVecesDictada")
    protected long numVecesDictada;

    /**
     * Obtiene el valor de la propiedad asignatura.
     * 
     * @return
     *     possible object is
     *     {@link TipoAsignatura }
     *     
     */
    public TipoAsignatura getAsignatura() {
        return asignatura;
    }

    /**
     * Define el valor de la propiedad asignatura.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoAsignatura }
     *     
     */
    public void setAsignatura(TipoAsignatura value) {
        this.asignatura = value;
    }

    /**
     * Obtiene el valor de la propiedad numVecesDictada.
     * 
     */
    public long getNumVecesDictada() {
        return numVecesDictada;
    }

    /**
     * Define el valor de la propiedad numVecesDictada.
     * 
     */
    public void setNumVecesDictada(long value) {
        this.numVecesDictada = value;
    }

}
