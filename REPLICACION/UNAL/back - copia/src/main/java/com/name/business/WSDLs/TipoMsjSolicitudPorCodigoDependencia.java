
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la solicitud de un tercero por su id para interop
 * 
 * <p>Clase Java para tipoMsjSolicitudPorCodigoDependencia complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudPorCodigoDependencia">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="CodigoDependencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDependencia"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudPorCodigoDependencia", propOrder = {
    "codigoDependencia"
})
public class TipoMsjSolicitudPorCodigoDependencia
    extends TipoMsjGenerico
{

    @XmlElement(name = "CodigoDependencia", required = true)
    protected String codigoDependencia;

    /**
     * Obtiene el valor de la propiedad codigoDependencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDependencia() {
        return codigoDependencia;
    }

    /**
     * Define el valor de la propiedad codigoDependencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDependencia(String value) {
        this.codigoDependencia = value;
    }

}
