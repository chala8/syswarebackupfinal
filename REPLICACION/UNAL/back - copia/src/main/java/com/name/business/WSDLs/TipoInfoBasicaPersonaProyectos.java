
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * indica la informacion basica de persona para proyecto BPUN
 * 
 * <p>Clase Java para tipoInfoBasicaPersonaProyectos complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoBasicaPersonaProyectos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *         &lt;element name="nombrePersonaNatural" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombrePersonaNatural"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoBasicaPersonaProyectos", propOrder = {
    "idTercero",
    "nombrePersonaNatural"
})
public class TipoInfoBasicaPersonaProyectos {

    @XmlElement(required = true)
    protected TipoIdTercero idTercero;
    @XmlElement(required = true)
    protected TipoNombrePersonaNatural nombrePersonaNatural;

    /**
     * Obtiene el valor de la propiedad idTercero.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getIdTercero() {
        return idTercero;
    }

    /**
     * Define el valor de la propiedad idTercero.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setIdTercero(TipoIdTercero value) {
        this.idTercero = value;
    }

    /**
     * Obtiene el valor de la propiedad nombrePersonaNatural.
     * 
     * @return
     *     possible object is
     *     {@link TipoNombrePersonaNatural }
     *     
     */
    public TipoNombrePersonaNatural getNombrePersonaNatural() {
        return nombrePersonaNatural;
    }

    /**
     * Define el valor de la propiedad nombrePersonaNatural.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoNombrePersonaNatural }
     *     
     */
    public void setNombrePersonaNatural(TipoNombrePersonaNatural value) {
        this.nombrePersonaNatural = value;
    }

}
