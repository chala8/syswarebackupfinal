
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de respuesta para el cambio de un contrato
 * 
 * <p>Clase Java para tipoMsjSolicitudCambioContratoSARA complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudCambioContratoSARA">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Contratos" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCambioContrato"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudCambioContratoSARA", propOrder = {
    "contratos"
})
public class TipoMsjSolicitudCambioContratoSARA
    extends TipoMsjGenerico
{

    @XmlElement(name = "Contratos", required = true)
    protected List<TipoCambioContrato> contratos;

    /**
     * Gets the value of the contratos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contratos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContratos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoCambioContrato }
     * 
     * 
     */
    public List<TipoCambioContrato> getContratos() {
        if (contratos == null) {
            contratos = new ArrayList<TipoCambioContrato>();
        }
        return this.contratos;
    }

}
