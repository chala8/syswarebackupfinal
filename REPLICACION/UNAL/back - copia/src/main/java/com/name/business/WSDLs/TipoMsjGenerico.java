
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje generico para interop
 * 
 * <p>Clase Java para tipoMsjGenerico complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjGenerico">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infoMensaje" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoMensaje"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjGenerico", propOrder = {
    "infoMensaje"
})
@XmlSeeAlso({
    TipoMsjRespuestaCabeceraContratos.class,
    TipoMsjSolicitudInformacionGeograficaSIBU.class,
    TipoSolicitudCreacionPersonaHermes.class,
    TipoMsjTercero.class,
    TipoMsjRespuestaOp.class,
    TipoMsjSolicitudCambioContrato.class,
    TipoMsjSolicitudPorIdTercero.class,
    TipoMsjRespuestaHomologConceptoPresupuesto.class,
    TipoMsjSolicitudPorCodigoDependencia.class,
    TipoMsjRespuestaEmpleado.class,
    TipoMsjTerceroFinanciero.class,
    TipoMsjRespuestaRubro.class,
    TipoMsjCuentaBancariaTercero.class,
    TipoMsjRespuestaPersonaHermes.class,
    TipoMsjSolicitudGestionPersonaBPUN.class,
    TipoMsjRespuestaExistencia.class,
    TipoMsjRespuestaViaticos.class,
    TipoMsjRespuestaHomologArea.class,
    TipoMsjSolicitudCambioContratoSARA.class,
    TipoMsjSolicitudHomologConceptoPresupuesto.class,
    TipoMsjRespuestaRegistroCuentaNomina.class,
    TipoMsjGestionDependenciaSIBU.class,
    TipoMsjSolicitudGestionEmpleadoSIBU.class,
    TipoMsjRespuestaEmpleadoSARA.class,
    TipoMsjSolicitudCreacionUsuarioALEPH.class,
    TipoMsjSolicitudInfoComplementaria.class,
    TipoMsjrespuestaDetalleDocente.class,
    TipoMsjRespuestaDetalleEstudiante.class,
    TipoMsjSolicitudCambioDocumento.class,
    TipoMsjDetalleContratos.class,
    TipoMsjRespuestaInformacionGeografica.class,
    TipoMsjRespuestaInfoComplementaria.class,
    TipoMsjRespuestaDependencia.class,
    TipoMsjConsultaRubro.class,
    TipoMsjSolicitudInformacionGeografica.class,
    TipoMsjRespuestaConsultaEstudiante.class,
    TipoMsjRespuestaEstadoNomina.class,
    TipoMsjSolicitudCambioDocumentoSIBU.class,
    TipoMsjSolicitudHomlogArea.class,
    TipoMsjSolicitudActualizacionUsuarioALEPH.class,
    TipoSolicitudActualizacionPersonaHermes.class,
    TipoMsjSolicitudEstadoCargueNomina.class,
    TipoMsjSolicitudPorTipoVinculacion.class,
    TipoMsjGestionDependenciaBPUN.class,
    TipoMsjRespuestaPersonaHermesMal.class,
    TipoMsjRespuestaContratos.class,
    TipoMsjRespuestaCambioDocumento.class,
    TipoConsultaPersonaHermes.class,
    TipoMsjRespuestaContratista.class,
    TipoMsjRespuestaCambioContrato.class,
    TipoMsjDocente.class,
    TipoMsjRespuestaEstudiante.class,
    TipoMsjSolicitudContrato.class
})
public class TipoMsjGenerico {

    @XmlElement(required = true)
    protected TipoInfoMensaje infoMensaje;

    /**
     * Obtiene el valor de la propiedad infoMensaje.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoMensaje }
     *     
     */
    public TipoInfoMensaje getInfoMensaje() {
        return infoMensaje;
    }

    /**
     * Define el valor de la propiedad infoMensaje.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoMensaje }
     *     
     */
    public void setInfoMensaje(TipoInfoMensaje value) {
        this.infoMensaje = value;
    }

}
