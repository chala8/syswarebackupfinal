
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para tipoMsjConsultaRubro complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjConsultaRubro">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="fecha" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFecha"/>
 *         &lt;element name="Empresa" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEmpresa"/>
 *         &lt;element name="CodigoArea" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoAreaResponsabilidad"/>
 *         &lt;element name="CodigoConcepto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoConceptoPresupuesto"/>
 *         &lt;element name="CodigoRecurso" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoRecurso"/>
 *         &lt;element name="Proyecto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoProyecto"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjConsultaRubro", propOrder = {
    "fecha",
    "empresa",
    "codigoArea",
    "codigoConcepto",
    "codigoRecurso",
    "proyecto"
})
public class TipoMsjConsultaRubro
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fecha;
    @XmlElement(name = "Empresa", required = true)
    protected String empresa;
    @XmlElement(name = "CodigoArea", required = true)
    protected String codigoArea;
    @XmlElement(name = "CodigoConcepto", required = true)
    protected String codigoConcepto;
    @XmlElement(name = "CodigoRecurso", required = true)
    protected String codigoRecurso;
    @XmlElement(name = "Proyecto", required = true)
    protected String proyecto;

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFecha(XMLGregorianCalendar value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmpresa(String value) {
        this.empresa = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoArea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoArea() {
        return codigoArea;
    }

    /**
     * Define el valor de la propiedad codigoArea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoArea(String value) {
        this.codigoArea = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoConcepto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoConcepto() {
        return codigoConcepto;
    }

    /**
     * Define el valor de la propiedad codigoConcepto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoConcepto(String value) {
        this.codigoConcepto = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoRecurso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoRecurso() {
        return codigoRecurso;
    }

    /**
     * Define el valor de la propiedad codigoRecurso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoRecurso(String value) {
        this.codigoRecurso = value;
    }

    /**
     * Obtiene el valor de la propiedad proyecto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProyecto() {
        return proyecto;
    }

    /**
     * Define el valor de la propiedad proyecto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProyecto(String value) {
        this.proyecto = value;
    }

}
