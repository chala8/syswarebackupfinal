
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * agregado por Equipo IOP 2013 - Universidad Nacional de Colombia
 * 
 * <p>Clase Java para tipoPlaza complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoPlaza">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoPlaza" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoPlaza"/>
 *         &lt;element name="CodigoCategoria" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoCategoria"/>
 *         &lt;element name="Dedicacion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoDedicacion"/>
 *         &lt;element name="FechaInicioPlaza" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFechaInicioPlaza"/>
 *         &lt;element name="FechaFinPlaza" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFechaFinPlaza"/>
 *         &lt;element name="Asignatura" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoPlaza", propOrder = {
    "codigoPlaza",
    "codigoCategoria",
    "dedicacion",
    "fechaInicioPlaza",
    "fechaFinPlaza",
    "asignatura"
})
public class TipoPlaza {

    @XmlElement(name = "CodigoPlaza", required = true)
    protected String codigoPlaza;
    @XmlElement(name = "CodigoCategoria", required = true)
    protected String codigoCategoria;
    @XmlElement(name = "Dedicacion", required = true)
    protected String dedicacion;
    @XmlElement(name = "FechaInicioPlaza", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaInicioPlaza;
    @XmlElement(name = "FechaFinPlaza", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaFinPlaza;
    @XmlElement(name = "Asignatura", required = true)
    protected Object asignatura;

    /**
     * Obtiene el valor de la propiedad codigoPlaza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPlaza() {
        return codigoPlaza;
    }

    /**
     * Define el valor de la propiedad codigoPlaza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPlaza(String value) {
        this.codigoPlaza = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCategoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCategoria() {
        return codigoCategoria;
    }

    /**
     * Define el valor de la propiedad codigoCategoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCategoria(String value) {
        this.codigoCategoria = value;
    }

    /**
     * Obtiene el valor de la propiedad dedicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDedicacion() {
        return dedicacion;
    }

    /**
     * Define el valor de la propiedad dedicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDedicacion(String value) {
        this.dedicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaInicioPlaza.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaInicioPlaza() {
        return fechaInicioPlaza;
    }

    /**
     * Define el valor de la propiedad fechaInicioPlaza.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaInicioPlaza(XMLGregorianCalendar value) {
        this.fechaInicioPlaza = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaFinPlaza.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaFinPlaza() {
        return fechaFinPlaza;
    }

    /**
     * Define el valor de la propiedad fechaFinPlaza.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaFinPlaza(XMLGregorianCalendar value) {
        this.fechaFinPlaza = value;
    }

    /**
     * Obtiene el valor de la propiedad asignatura.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getAsignatura() {
        return asignatura;
    }

    /**
     * Define el valor de la propiedad asignatura.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setAsignatura(Object value) {
        this.asignatura = value;
    }

}
