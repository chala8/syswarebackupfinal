
package com.name.business.WSDLs;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoInfoBasicaAsignatura complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoBasicaAsignatura">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoAsignatura" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoAsignatura"/>
 *         &lt;element name="NombreAsignatura" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDescripcionAsignatura"/>
 *         &lt;element name="CreditosAsignatura" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCreditosAsignatura"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoBasicaAsignatura", propOrder = {
    "codigoAsignatura",
    "nombreAsignatura",
    "creditosAsignatura"
})
public class TipoInfoBasicaAsignatura {

    @XmlElement(name = "CodigoAsignatura", required = true)
    protected String codigoAsignatura;
    @XmlElement(name = "NombreAsignatura", required = true)
    protected String nombreAsignatura;
    @XmlElement(name = "CreditosAsignatura", required = true)
    protected BigDecimal creditosAsignatura;

    /**
     * Obtiene el valor de la propiedad codigoAsignatura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    /**
     * Define el valor de la propiedad codigoAsignatura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAsignatura(String value) {
        this.codigoAsignatura = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreAsignatura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreAsignatura() {
        return nombreAsignatura;
    }

    /**
     * Define el valor de la propiedad nombreAsignatura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreAsignatura(String value) {
        this.nombreAsignatura = value;
    }

    /**
     * Obtiene el valor de la propiedad creditosAsignatura.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditosAsignatura() {
        return creditosAsignatura;
    }

    /**
     * Define el valor de la propiedad creditosAsignatura.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditosAsignatura(BigDecimal value) {
        this.creditosAsignatura = value;
    }

}
