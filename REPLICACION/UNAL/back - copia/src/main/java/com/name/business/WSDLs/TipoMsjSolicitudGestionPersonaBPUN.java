
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de solicitud para la creaci�n de una persona en el sistem de infomaci�n BPUN
 * 
 * <p>Clase Java para tipoMsjSolicitudGestionPersonaBPUN complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudGestionPersonaBPUN">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Persona" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoPersonaBPUN"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudGestionPersonaBPUN", propOrder = {
    "persona"
})
public class TipoMsjSolicitudGestionPersonaBPUN
    extends TipoMsjGenerico
{

    @XmlElement(name = "Persona", required = true)
    protected List<TipoPersonaBPUN> persona;

    /**
     * Gets the value of the persona property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the persona property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPersona().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoPersonaBPUN }
     * 
     * 
     */
    public List<TipoPersonaBPUN> getPersona() {
        if (persona == null) {
            persona = new ArrayList<TipoPersonaBPUN>();
        }
        return this.persona;
    }

}
