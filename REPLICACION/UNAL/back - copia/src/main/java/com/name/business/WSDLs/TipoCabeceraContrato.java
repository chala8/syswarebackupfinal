
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * indica la informacion principal un contrato para interop
 * 
 * <p>Clase Java para tipoCabeceraContrato complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoCabeceraContrato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codContrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodContrato"/>
 *         &lt;element name="tipoVinculacion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoVinculacion"/>
 *         &lt;element name="EstadoAdministrativo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEstadoAdministrativo"/>
 *         &lt;element name="area" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoArea"/>
 *         &lt;element name="Dependencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDependencia"/>
 *         &lt;element name="tipoPeriodo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoPeriodo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoCabeceraContrato", propOrder = {
    "codContrato",
    "tipoVinculacion",
    "estadoAdministrativo",
    "area",
    "dependencia",
    "tipoPeriodo"
})
public class TipoCabeceraContrato {

    @XmlElement(required = true)
    protected String codContrato;
    @XmlElement(required = true)
    protected String tipoVinculacion;
    @XmlElement(name = "EstadoAdministrativo", required = true)
    @XmlSchemaType(name = "string")
    protected TipoEstadoAdministrativo estadoAdministrativo;
    @XmlElement(required = true)
    protected String area;
    @XmlElement(name = "Dependencia", required = true)
    protected String dependencia;
    @XmlElement(required = true)
    protected String tipoPeriodo;

    /**
     * Obtiene el valor de la propiedad codContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodContrato() {
        return codContrato;
    }

    /**
     * Define el valor de la propiedad codContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodContrato(String value) {
        this.codContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoVinculacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoVinculacion() {
        return tipoVinculacion;
    }

    /**
     * Define el valor de la propiedad tipoVinculacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoVinculacion(String value) {
        this.tipoVinculacion = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstadoAdministrativo }
     *     
     */
    public TipoEstadoAdministrativo getEstadoAdministrativo() {
        return estadoAdministrativo;
    }

    /**
     * Define el valor de la propiedad estadoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstadoAdministrativo }
     *     
     */
    public void setEstadoAdministrativo(TipoEstadoAdministrativo value) {
        this.estadoAdministrativo = value;
    }

    /**
     * Obtiene el valor de la propiedad area.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArea() {
        return area;
    }

    /**
     * Define el valor de la propiedad area.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArea(String value) {
        this.area = value;
    }

    /**
     * Obtiene el valor de la propiedad dependencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependencia() {
        return dependencia;
    }

    /**
     * Define el valor de la propiedad dependencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependencia(String value) {
        this.dependencia = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoPeriodo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoPeriodo() {
        return tipoPeriodo;
    }

    /**
     * Define el valor de la propiedad tipoPeriodo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoPeriodo(String value) {
        this.tipoPeriodo = value;
    }

}
