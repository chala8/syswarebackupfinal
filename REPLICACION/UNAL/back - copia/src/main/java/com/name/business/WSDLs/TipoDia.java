
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para tipoDia complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoDia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="diaSemana" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDiaSemana"/>
 *         &lt;element name="horaInicio" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFechaHora"/>
 *         &lt;element name="HoraFin" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFechaHora"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoDia", propOrder = {
    "diaSemana",
    "horaInicio",
    "horaFin"
})
public class TipoDia {

    protected int diaSemana;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar horaInicio;
    @XmlElement(name = "HoraFin", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar horaFin;

    /**
     * Obtiene el valor de la propiedad diaSemana.
     * 
     */
    public int getDiaSemana() {
        return diaSemana;
    }

    /**
     * Define el valor de la propiedad diaSemana.
     * 
     */
    public void setDiaSemana(int value) {
        this.diaSemana = value;
    }

    /**
     * Obtiene el valor de la propiedad horaInicio.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHoraInicio() {
        return horaInicio;
    }

    /**
     * Define el valor de la propiedad horaInicio.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHoraInicio(XMLGregorianCalendar value) {
        this.horaInicio = value;
    }

    /**
     * Obtiene el valor de la propiedad horaFin.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getHoraFin() {
        return horaFin;
    }

    /**
     * Define el valor de la propiedad horaFin.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setHoraFin(XMLGregorianCalendar value) {
        this.horaFin = value;
    }

}
