package com.name.business.utils.auth;

import com.google.common.base.Optional;
import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.AuthenticationException;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;

@SuppressWarnings("Guava")
@PreMatching
@Priority(Priorities.AUTHENTICATION)
public class AuthenticateFilter extends AuthFilter<CustomCredentials, CustomAuthUser> {
    private CustomAuthenticator authenticator;

    public AuthenticateFilter(CustomAuthenticator authenticator) {

        this.authenticator = authenticator;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Optional<CustomAuthUser> authenticatedUser;

        String header = "";
        try{
            header = requestContext.getHeaders().get("Key_Server").get(0);
        }catch(Exception e){
            header = "";
        }

        try{//esto es para omitir autenticación en local ya que la cookie no está en peticiones
            if(System.getenv("SYSWARE_DEVPC").equals("yes")){header = "3020D4:1FG-2U113E822-A1TE04529-68CF";}
        }catch (Exception e){
            //System.out.println("Can't Get SYSWARE_DEVPC envr var");
        }

        if(header.equals("3020D4:1FG-2U113E822-A1TE04529-68CF")){
            CustomCredentials credentials = new CustomCredentials();
            credentials.setToken("3020D4:1FG-2U113E822-A1TE04529-68CF");
            try {
                authenticatedUser = authenticator.authenticate(credentials);
                SecurityContext securityContext = new CustomSecurityContext(authenticatedUser.get(), requestContext.getSecurityContext());
                requestContext.setSecurityContext(securityContext);
            } catch (AuthenticationException e) {
                e.printStackTrace();
                throw new WebApplicationException("Credentials not valid", Response.Status.UNAUTHORIZED);
            }
        }else{
            try {
                CustomCredentials credentials = getCredentials(requestContext);
                authenticatedUser = authenticator.authenticate(credentials);
            } catch (AuthenticationException e) {
                throw new WebApplicationException("Unable to validate credentials", Response.Status.UNAUTHORIZED);
            }
            if (authenticatedUser.isPresent()) {
                SecurityContext securityContext = new CustomSecurityContext(authenticatedUser.get(), requestContext.getSecurityContext());
                requestContext.setSecurityContext(securityContext);
            } else {
                throw new WebApplicationException("Credentials not valid", Response.Status.UNAUTHORIZED);
            }
        }
    }

    private CustomCredentials getCredentials(ContainerRequestContext requestContext) {
        CustomCredentials credentials = new CustomCredentials();

        try {
            String rawToken = requestContext
                    .getCookies()
                    .get("SESSIONID")
                    .getValue();

            credentials.setToken(rawToken);
        } catch (Exception e) {
            throw new WebApplicationException("Unable to parse credentials or token not found", Response.Status.UNAUTHORIZED);
        }


        return credentials;
    }
}