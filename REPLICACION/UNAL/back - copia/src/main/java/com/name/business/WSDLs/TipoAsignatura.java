
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoAsignatura complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoAsignatura">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InfoBasica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaAsignatura"/>
 *         &lt;element name="Infodetalle" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoDetalleAsignatura"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoAsignatura", propOrder = {
    "infoBasica",
    "infodetalle"
})
public class TipoAsignatura {

    @XmlElement(name = "InfoBasica", required = true)
    protected TipoInfoBasicaAsignatura infoBasica;
    @XmlElement(name = "Infodetalle", required = true)
    protected TipoInfoDetalleAsignatura infodetalle;

    /**
     * Obtiene el valor de la propiedad infoBasica.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaAsignatura }
     *     
     */
    public TipoInfoBasicaAsignatura getInfoBasica() {
        return infoBasica;
    }

    /**
     * Define el valor de la propiedad infoBasica.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaAsignatura }
     *     
     */
    public void setInfoBasica(TipoInfoBasicaAsignatura value) {
        this.infoBasica = value;
    }

    /**
     * Obtiene el valor de la propiedad infodetalle.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoDetalleAsignatura }
     *     
     */
    public TipoInfoDetalleAsignatura getInfodetalle() {
        return infodetalle;
    }

    /**
     * Define el valor de la propiedad infodetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoDetalleAsignatura }
     *     
     */
    public void setInfodetalle(TipoInfoDetalleAsignatura value) {
        this.infodetalle = value;
    }

}
