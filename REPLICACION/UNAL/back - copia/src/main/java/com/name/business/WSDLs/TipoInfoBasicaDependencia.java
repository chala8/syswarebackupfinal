
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n Basica de una dependencia para IOP
 * 
 * <p>Clase Java para tipoInfoBasicaDependencia complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoBasicaDependencia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoDependencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDependencia"/>
 *         &lt;element name="NombreDependencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombreDependencia"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoBasicaDependencia", propOrder = {
    "codigoDependencia",
    "nombreDependencia"
})
public class TipoInfoBasicaDependencia {

    @XmlElement(name = "CodigoDependencia", required = true)
    protected String codigoDependencia;
    @XmlElement(name = "NombreDependencia", required = true)
    protected String nombreDependencia;

    /**
     * Obtiene el valor de la propiedad codigoDependencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDependencia() {
        return codigoDependencia;
    }

    /**
     * Define el valor de la propiedad codigoDependencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDependencia(String value) {
        this.codigoDependencia = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreDependencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreDependencia() {
        return nombreDependencia;
    }

    /**
     * Define el valor de la propiedad nombreDependencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreDependencia(String value) {
        this.nombreDependencia = value;
    }

}
