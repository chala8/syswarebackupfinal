
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * indica el monto de para la asignacion de viaticos de un empleado
 * 
 * <p>Clase Java para tipoAsignacionViaticos complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoAsignacionViaticos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AsignacionViaticos" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMontoMonetario"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoAsignacionViaticos", propOrder = {
    "asignacionViaticos"
})
public class TipoAsignacionViaticos {

    @XmlElement(name = "AsignacionViaticos", required = true)
    protected TipoMontoMonetario asignacionViaticos;

    /**
     * Obtiene el valor de la propiedad asignacionViaticos.
     * 
     * @return
     *     possible object is
     *     {@link TipoMontoMonetario }
     *     
     */
    public TipoMontoMonetario getAsignacionViaticos() {
        return asignacionViaticos;
    }

    /**
     * Define el valor de la propiedad asignacionViaticos.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoMontoMonetario }
     *     
     */
    public void setAsignacionViaticos(TipoMontoMonetario value) {
        this.asignacionViaticos = value;
    }

}
