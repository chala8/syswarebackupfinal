package com.name.business.DAOs;

import com.name.business.entities.TypeDocument;
import com.name.business.mappers.TypeDocMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

public interface UNDropDAO {
    @Mapper(TypeDocMapper.class)
    @SqlQuery("select distinct (select documentosoa from vu_unalintegraciontipodoc where trim(hojavida_hov.chov_tipodocumento)=documentosara) as tipoDocumentoSoa,\n" +
            "       memr_cedula as cedula\n" +
            "from empleado_eml,\n" +
            "hojavida_hov,\n" +
            "empleadoreplicado_emr\n" +
            " where meml_hojavida=mhov_identifica\n" +
            "  and mhov_identifica != 0\n" +
            "  and meml_identifica = memr_contrato\n" +
            "  and neml_empresa = nemr_empresa\n" +
            "  and cemr_estado = :CEMR_ESTADO")
    List <TypeDocument> getCC(@Bind("CEMR_ESTADO") String CEMR_ESTADO);

}
