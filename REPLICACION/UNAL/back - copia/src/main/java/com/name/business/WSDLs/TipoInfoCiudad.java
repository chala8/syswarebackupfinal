
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n detallada de una ciudad para IOP
 * 
 * <p>Clase Java para tipoInfoCiudad complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoCiudad">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdCiudad" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdCiudad"/>
 *         &lt;element name="NombreCiudad" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombreCiudad"/>
 *         &lt;element name="idPais" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoPais"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoCiudad", propOrder = {
    "idCiudad",
    "nombreCiudad",
    "idPais"
})
public class TipoInfoCiudad {

    @XmlElement(name = "IdCiudad", required = true)
    protected String idCiudad;
    @XmlElement(name = "NombreCiudad", required = true)
    protected String nombreCiudad;
    @XmlElement(required = true)
    protected TipoInfoPais idPais;

    /**
     * Obtiene el valor de la propiedad idCiudad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCiudad() {
        return idCiudad;
    }

    /**
     * Define el valor de la propiedad idCiudad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCiudad(String value) {
        this.idCiudad = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCiudad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCiudad() {
        return nombreCiudad;
    }

    /**
     * Define el valor de la propiedad nombreCiudad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCiudad(String value) {
        this.nombreCiudad = value;
    }

    /**
     * Obtiene el valor de la propiedad idPais.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoPais }
     *     
     */
    public TipoInfoPais getIdPais() {
        return idPais;
    }

    /**
     * Define el valor de la propiedad idPais.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoPais }
     *     
     */
    public void setIdPais(TipoInfoPais value) {
        this.idPais = value;
    }

}
