
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para tipoContratoTercero complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoContratoTercero">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoContrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoContrato"/>
 *         &lt;element name="FechaInicioContrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFechaInicioContrato"/>
 *         &lt;element name="FechaFinContrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipofechaFinContrato"/>
 *         &lt;element name="TipoContrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoContrato"/>
 *         &lt;element name="Vigencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoVigenciaContrato"/>
 *         &lt;element name="Tercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTercero"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoContratoTercero", propOrder = {
    "codigoContrato",
    "fechaInicioContrato",
    "fechaFinContrato",
    "tipoContrato",
    "vigencia",
    "tercero"
})
public class TipoContratoTercero {

    @XmlElement(name = "CodigoContrato", required = true)
    protected String codigoContrato;
    @XmlElement(name = "FechaInicioContrato", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaInicioContrato;
    @XmlElement(name = "FechaFinContrato", required = true)
    protected String fechaFinContrato;
    @XmlElement(name = "TipoContrato", required = true)
    protected String tipoContrato;
    @XmlElement(name = "Vigencia", required = true)
    protected String vigencia;
    @XmlElement(name = "Tercero", required = true)
    protected TipoTercero tercero;

    /**
     * Obtiene el valor de la propiedad codigoContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoContrato() {
        return codigoContrato;
    }

    /**
     * Define el valor de la propiedad codigoContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoContrato(String value) {
        this.codigoContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaInicioContrato.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaInicioContrato() {
        return fechaInicioContrato;
    }

    /**
     * Define el valor de la propiedad fechaInicioContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaInicioContrato(XMLGregorianCalendar value) {
        this.fechaInicioContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaFinContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaFinContrato() {
        return fechaFinContrato;
    }

    /**
     * Define el valor de la propiedad fechaFinContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaFinContrato(String value) {
        this.fechaFinContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoContrato() {
        return tipoContrato;
    }

    /**
     * Define el valor de la propiedad tipoContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoContrato(String value) {
        this.tipoContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad vigencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVigencia() {
        return vigencia;
    }

    /**
     * Define el valor de la propiedad vigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVigencia(String value) {
        this.vigencia = value;
    }

    /**
     * Obtiene el valor de la propiedad tercero.
     * 
     * @return
     *     possible object is
     *     {@link TipoTercero }
     *     
     */
    public TipoTercero getTercero() {
        return tercero;
    }

    /**
     * Define el valor de la propiedad tercero.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTercero }
     *     
     */
    public void setTercero(TipoTercero value) {
        this.tercero = value;
    }

}
