
package com.name.business.WSDLs;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoRubroPresupuestal complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoRubroPresupuestal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Codigo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoRubro"/>
 *         &lt;element name="NombreRubro" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombreRubro"/>
 *         &lt;element name="ValorDisponibilidad" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoValorRubro"/>
 *         &lt;element name="ValorApropiacionDefinitiva" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoValorApropiacionDef"/>
 *         &lt;element name="PorcentajeEjecucion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoPorcentajeEjecucion"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoRubroPresupuestal", propOrder = {
    "codigo",
    "nombreRubro",
    "valorDisponibilidad",
    "valorApropiacionDefinitiva",
    "porcentajeEjecucion"
})
public class TipoRubroPresupuestal {

    @XmlElement(name = "Codigo", required = true)
    protected String codigo;
    @XmlElement(name = "NombreRubro", required = true)
    protected String nombreRubro;
    @XmlElement(name = "ValorDisponibilidad", required = true)
    protected BigDecimal valorDisponibilidad;
    @XmlElement(name = "ValorApropiacionDefinitiva", required = true)
    protected BigDecimal valorApropiacionDefinitiva;
    @XmlElement(name = "PorcentajeEjecucion", required = true)
    protected BigDecimal porcentajeEjecucion;

    /**
     * Obtiene el valor de la propiedad codigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Define el valor de la propiedad codigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreRubro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreRubro() {
        return nombreRubro;
    }

    /**
     * Define el valor de la propiedad nombreRubro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreRubro(String value) {
        this.nombreRubro = value;
    }

    /**
     * Obtiene el valor de la propiedad valorDisponibilidad.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorDisponibilidad() {
        return valorDisponibilidad;
    }

    /**
     * Define el valor de la propiedad valorDisponibilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorDisponibilidad(BigDecimal value) {
        this.valorDisponibilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad valorApropiacionDefinitiva.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorApropiacionDefinitiva() {
        return valorApropiacionDefinitiva;
    }

    /**
     * Define el valor de la propiedad valorApropiacionDefinitiva.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorApropiacionDefinitiva(BigDecimal value) {
        this.valorApropiacionDefinitiva = value;
    }

    /**
     * Obtiene el valor de la propiedad porcentajeEjecucion.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPorcentajeEjecucion() {
        return porcentajeEjecucion;
    }

    /**
     * Define el valor de la propiedad porcentajeEjecucion.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPorcentajeEjecucion(BigDecimal value) {
        this.porcentajeEjecucion = value;
    }

}
