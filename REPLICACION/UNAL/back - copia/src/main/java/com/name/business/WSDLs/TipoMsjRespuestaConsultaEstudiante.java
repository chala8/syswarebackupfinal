
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoMsjRespuestaConsultaEstudiante complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaConsultaEstudiante">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="InfoBasicaPlanEstudios" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaPlanEstudios"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaConsultaEstudiante", propOrder = {
    "infoBasicaPlanEstudios"
})
public class TipoMsjRespuestaConsultaEstudiante
    extends TipoMsjGenerico
{

    @XmlElement(name = "InfoBasicaPlanEstudios", required = true)
    protected List<TipoInfoBasicaPlanEstudios> infoBasicaPlanEstudios;

    /**
     * Gets the value of the infoBasicaPlanEstudios property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infoBasicaPlanEstudios property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfoBasicaPlanEstudios().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoBasicaPlanEstudios }
     * 
     * 
     */
    public List<TipoInfoBasicaPlanEstudios> getInfoBasicaPlanEstudios() {
        if (infoBasicaPlanEstudios == null) {
            infoBasicaPlanEstudios = new ArrayList<TipoInfoBasicaPlanEstudios>();
        }
        return this.infoBasicaPlanEstudios;
    }

}
