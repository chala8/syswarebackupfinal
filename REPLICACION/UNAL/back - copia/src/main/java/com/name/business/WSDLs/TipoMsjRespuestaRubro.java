
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoMsjRespuestaRubro complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaRubro">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="Rubro" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoRubroPresupuestal"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaRubro", propOrder = {
    "rubro"
})
public class TipoMsjRespuestaRubro
    extends TipoMsjGenerico
{

    @XmlElement(name = "Rubro", required = true)
    protected TipoRubroPresupuestal rubro;

    /**
     * Obtiene el valor de la propiedad rubro.
     * 
     * @return
     *     possible object is
     *     {@link TipoRubroPresupuestal }
     *     
     */
    public TipoRubroPresupuestal getRubro() {
        return rubro;
    }

    /**
     * Define el valor de la propiedad rubro.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoRubroPresupuestal }
     *     
     */
    public void setRubro(TipoRubroPresupuestal value) {
        this.rubro = value;
    }

}
