
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * indica el tipo de documento y el numero que identifica a un tercero para interop
 * 
 * <p>Clase Java para tipoCambioDocumento complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoCambioDocumento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Documento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *         &lt;element name="DocumentoAnterior" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoCambioDocumento", propOrder = {
    "documento",
    "documentoAnterior"
})
public class TipoCambioDocumento {

    @XmlElement(name = "Documento", required = true)
    protected TipoIdTercero documento;
    @XmlElement(name = "DocumentoAnterior", required = true)
    protected TipoIdTercero documentoAnterior;

    /**
     * Obtiene el valor de la propiedad documento.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getDocumento() {
        return documento;
    }

    /**
     * Define el valor de la propiedad documento.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setDocumento(TipoIdTercero value) {
        this.documento = value;
    }

    /**
     * Obtiene el valor de la propiedad documentoAnterior.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getDocumentoAnterior() {
        return documentoAnterior;
    }

    /**
     * Define el valor de la propiedad documentoAnterior.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setDocumentoAnterior(TipoIdTercero value) {
        this.documentoAnterior = value;
    }

}
