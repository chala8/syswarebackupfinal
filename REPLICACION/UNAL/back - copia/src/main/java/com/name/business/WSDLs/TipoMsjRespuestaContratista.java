
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la informaci�n de un Contratista para Interoperabilidad
 * 
 * <p>Clase Java para tipoMsjRespuestaContratista complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaContratista">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Contratista" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoContratista"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaContratista", propOrder = {
    "contratista"
})
public class TipoMsjRespuestaContratista
    extends TipoMsjGenerico
{

    @XmlElement(name = "Contratista", required = true)
    protected List<TipoContratista> contratista;

    /**
     * Gets the value of the contratista property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contratista property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContratista().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoContratista }
     * 
     * 
     */
    public List<TipoContratista> getContratista() {
        if (contratista == null) {
            contratista = new ArrayList<TipoContratista>();
        }
        return this.contratista;
    }

}
