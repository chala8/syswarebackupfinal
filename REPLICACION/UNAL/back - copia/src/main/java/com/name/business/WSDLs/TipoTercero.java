
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * indica a un tercero generico para interop
 * 
 * <p>Clase Java para tipoTercero complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoTercero">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infoBasicaTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaPersonal"/>
 *         &lt;element name="infoContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoContacto"/>
 *         &lt;element name="infoTributaria" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoTributariaTercero" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoTercero", propOrder = {
    "infoBasicaTercero",
    "infoContacto",
    "infoTributaria"
})
@XmlSeeAlso({
    TipoEmpleado.class,
    TipoEmpleadoSARA.class
})
public class TipoTercero {

    @XmlElement(required = true)
    protected TipoInfoBasicaPersonal infoBasicaTercero;
    @XmlElement(required = true)
    protected TipoInfoContacto infoContacto;
    protected TipoInfoTributariaTercero infoTributaria;

    /**
     * Obtiene el valor de la propiedad infoBasicaTercero.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPersonal }
     *     
     */
    public TipoInfoBasicaPersonal getInfoBasicaTercero() {
        return infoBasicaTercero;
    }

    /**
     * Define el valor de la propiedad infoBasicaTercero.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPersonal }
     *     
     */
    public void setInfoBasicaTercero(TipoInfoBasicaPersonal value) {
        this.infoBasicaTercero = value;
    }

    /**
     * Obtiene el valor de la propiedad infoContacto.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoContacto }
     *     
     */
    public TipoInfoContacto getInfoContacto() {
        return infoContacto;
    }

    /**
     * Define el valor de la propiedad infoContacto.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoContacto }
     *     
     */
    public void setInfoContacto(TipoInfoContacto value) {
        this.infoContacto = value;
    }

    /**
     * Obtiene el valor de la propiedad infoTributaria.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoTributariaTercero }
     *     
     */
    public TipoInfoTributariaTercero getInfoTributaria() {
        return infoTributaria;
    }

    /**
     * Define el valor de la propiedad infoTributaria.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoTributariaTercero }
     *     
     */
    public void setInfoTributaria(TipoInfoTributariaTercero value) {
        this.infoTributaria = value;
    }

}
