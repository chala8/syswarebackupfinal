
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de un investigador para la universidad nacional de colombia
 * 
 * <p>Clase Java para tipoContratista complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoContratista">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infoBasicaTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaPersonal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoContratista", propOrder = {
    "infoBasicaTercero"
})
public class TipoContratista {

    @XmlElement(required = true)
    protected TipoInfoBasicaPersonal infoBasicaTercero;

    /**
     * Obtiene el valor de la propiedad infoBasicaTercero.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPersonal }
     *     
     */
    public TipoInfoBasicaPersonal getInfoBasicaTercero() {
        return infoBasicaTercero;
    }

    /**
     * Define el valor de la propiedad infoBasicaTercero.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPersonal }
     *     
     */
    public void setInfoBasicaTercero(TipoInfoBasicaPersonal value) {
        this.infoBasicaTercero = value;
    }

}
