
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo empleado para interop
 * 
 * <p>Clase Java para tipoEmpleadoSARA complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoEmpleadoSARA">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTercero">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="Vinculacion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoContratoSARA"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoEmpleadoSARA", propOrder = {
    "vinculacion"
})
public class TipoEmpleadoSARA
    extends TipoTercero
{

    @XmlElement(name = "Vinculacion", required = true)
    protected List<TipoInfoContratoSARA> vinculacion;

    /**
     * Gets the value of the vinculacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vinculacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVinculacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoContratoSARA }
     * 
     * 
     */
    public List<TipoInfoContratoSARA> getVinculacion() {
        if (vinculacion == null) {
            vinculacion = new ArrayList<TipoInfoContratoSARA>();
        }
        return this.vinculacion;
    }

}
