package com.name.business.entities;

import java.util.Date;

public class TypeDocument {

    public String getTipoDocumentoSoa() {
        return tipoDocumentoSoa;
    }

    public void setTipoDocumentoSoa(String tipoDocumentoSoa) {
        this.tipoDocumentoSoa = tipoDocumentoSoa;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    private String tipoDocumentoSoa;
    private String cedula;

    public TypeDocument(String tipoDocumentoSoa, String cedula) {
        this.tipoDocumentoSoa = tipoDocumentoSoa;
        this.cedula = cedula;
    }




}
