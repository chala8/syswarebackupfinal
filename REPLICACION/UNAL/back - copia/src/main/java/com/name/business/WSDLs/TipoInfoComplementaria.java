
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n complementaria de un empleado para interoperabilidad
 * 
 * <p>Clase Java para tipoInfoComplementaria complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoComplementaria">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CiudadDeExpedicionID" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdCiudad"/>
 *         &lt;element name="NumeroCelular" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNumeroCelular"/>
 *         &lt;element name="FondoDePension" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFondoPension"/>
 *         &lt;element name="EPS" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEPS"/>
 *         &lt;element name="GrupoSanguineo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoRH"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoComplementaria", propOrder = {
    "ciudadDeExpedicionID",
    "numeroCelular",
    "fondoDePension",
    "eps",
    "grupoSanguineo"
})
public class TipoInfoComplementaria {

    @XmlElement(name = "CiudadDeExpedicionID", required = true)
    protected String ciudadDeExpedicionID;
    @XmlElement(name = "NumeroCelular", required = true)
    protected String numeroCelular;
    @XmlElement(name = "FondoDePension", required = true)
    protected String fondoDePension;
    @XmlElement(name = "EPS", required = true)
    protected String eps;
    @XmlElement(name = "GrupoSanguineo", required = true)
    protected String grupoSanguineo;

    /**
     * Obtiene el valor de la propiedad ciudadDeExpedicionID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudadDeExpedicionID() {
        return ciudadDeExpedicionID;
    }

    /**
     * Define el valor de la propiedad ciudadDeExpedicionID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudadDeExpedicionID(String value) {
        this.ciudadDeExpedicionID = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroCelular.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroCelular() {
        return numeroCelular;
    }

    /**
     * Define el valor de la propiedad numeroCelular.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroCelular(String value) {
        this.numeroCelular = value;
    }

    /**
     * Obtiene el valor de la propiedad fondoDePension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFondoDePension() {
        return fondoDePension;
    }

    /**
     * Define el valor de la propiedad fondoDePension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFondoDePension(String value) {
        this.fondoDePension = value;
    }

    /**
     * Obtiene el valor de la propiedad eps.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEPS() {
        return eps;
    }

    /**
     * Define el valor de la propiedad eps.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEPS(String value) {
        this.eps = value;
    }

    /**
     * Obtiene el valor de la propiedad grupoSanguineo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrupoSanguineo() {
        return grupoSanguineo;
    }

    /**
     * Define el valor de la propiedad grupoSanguineo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrupoSanguineo(String value) {
        this.grupoSanguineo = value;
    }

}
