
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * indica a un tercero financiero generico para interop
 * 
 * <p>Clase Java para tipoTerceroFinanciero complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoTerceroFinanciero">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infoTipoTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTercero"/>
 *         &lt;element name="infoResponsable" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoResponsable"/>
 *         &lt;element name="ausentismo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEstadoAdministrativo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoTerceroFinanciero", propOrder = {
    "infoTipoTercero",
    "infoResponsable",
    "ausentismo"
})
public class TipoTerceroFinanciero {

    @XmlElement(required = true)
    protected TipoTercero infoTipoTercero;
    @XmlElement(required = true)
    protected TipoInfoResponsable infoResponsable;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoEstadoAdministrativo ausentismo;

    /**
     * Obtiene el valor de la propiedad infoTipoTercero.
     * 
     * @return
     *     possible object is
     *     {@link TipoTercero }
     *     
     */
    public TipoTercero getInfoTipoTercero() {
        return infoTipoTercero;
    }

    /**
     * Define el valor de la propiedad infoTipoTercero.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTercero }
     *     
     */
    public void setInfoTipoTercero(TipoTercero value) {
        this.infoTipoTercero = value;
    }

    /**
     * Obtiene el valor de la propiedad infoResponsable.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoResponsable }
     *     
     */
    public TipoInfoResponsable getInfoResponsable() {
        return infoResponsable;
    }

    /**
     * Define el valor de la propiedad infoResponsable.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoResponsable }
     *     
     */
    public void setInfoResponsable(TipoInfoResponsable value) {
        this.infoResponsable = value;
    }

    /**
     * Obtiene el valor de la propiedad ausentismo.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstadoAdministrativo }
     *     
     */
    public TipoEstadoAdministrativo getAusentismo() {
        return ausentismo;
    }

    /**
     * Define el valor de la propiedad ausentismo.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstadoAdministrativo }
     *     
     */
    public void setAusentismo(TipoEstadoAdministrativo value) {
        this.ausentismo = value;
    }

}
