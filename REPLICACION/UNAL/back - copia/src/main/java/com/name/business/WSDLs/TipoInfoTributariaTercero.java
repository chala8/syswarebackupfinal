
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoInfoTributariaTercero complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoTributariaTercero">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipoTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoTercero"/>
 *         &lt;element name="tipoContribuyente" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoContribuyente"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoTributariaTercero", propOrder = {
    "tipoTercero",
    "tipoContribuyente"
})
public class TipoInfoTributariaTercero {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoTipoTercero tipoTercero;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected TipoTipoContribuyente tipoContribuyente;

    /**
     * Obtiene el valor de la propiedad tipoTercero.
     * 
     * @return
     *     possible object is
     *     {@link TipoTipoTercero }
     *     
     */
    public TipoTipoTercero getTipoTercero() {
        return tipoTercero;
    }

    /**
     * Define el valor de la propiedad tipoTercero.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTipoTercero }
     *     
     */
    public void setTipoTercero(TipoTipoTercero value) {
        this.tipoTercero = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoContribuyente.
     * 
     * @return
     *     possible object is
     *     {@link TipoTipoContribuyente }
     *     
     */
    public TipoTipoContribuyente getTipoContribuyente() {
        return tipoContribuyente;
    }

    /**
     * Define el valor de la propiedad tipoContribuyente.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTipoContribuyente }
     *     
     */
    public void setTipoContribuyente(TipoTipoContribuyente value) {
        this.tipoContribuyente = value;
    }

}
