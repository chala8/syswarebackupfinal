
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Respuesta de la informaci�n complementaria de un empleado de la UN para IOP
 * 
 * <p>Clase Java para tipoMsjRespuestaInfoComplementaria complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaInfoComplementaria">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="Documento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *         &lt;element name="InformacionComplementaria" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoComplementaria"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaInfoComplementaria", propOrder = {
    "documento",
    "informacionComplementaria"
})
public class TipoMsjRespuestaInfoComplementaria
    extends TipoMsjGenerico
{

    @XmlElement(name = "Documento", required = true)
    protected TipoIdTercero documento;
    @XmlElement(name = "InformacionComplementaria", required = true)
    protected TipoInfoComplementaria informacionComplementaria;

    /**
     * Obtiene el valor de la propiedad documento.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getDocumento() {
        return documento;
    }

    /**
     * Define el valor de la propiedad documento.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setDocumento(TipoIdTercero value) {
        this.documento = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionComplementaria.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoComplementaria }
     *     
     */
    public TipoInfoComplementaria getInformacionComplementaria() {
        return informacionComplementaria;
    }

    /**
     * Define el valor de la propiedad informacionComplementaria.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoComplementaria }
     *     
     */
    public void setInformacionComplementaria(TipoInfoComplementaria value) {
        this.informacionComplementaria = value;
    }

}
