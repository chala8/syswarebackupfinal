
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipoInfoResponsable complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoResponsable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cargo" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCargoEmpleado"/>
 *         &lt;element name="dependencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDependencia"/>
 *         &lt;element name="tipoResponsable" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoResponsble"/>
 *         &lt;element name="empresa" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoResponsable", propOrder = {
    "cargo",
    "dependencia",
    "tipoResponsable",
    "empresa"
})
public class TipoInfoResponsable {

    @XmlElement(required = true)
    protected String cargo;
    @XmlElement(required = true)
    protected String dependencia;
    @XmlElement(required = true)
    protected String tipoResponsable;
    @XmlElement(required = true)
    protected Object empresa;

    /**
     * Obtiene el valor de la propiedad cargo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCargo() {
        return cargo;
    }

    /**
     * Define el valor de la propiedad cargo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCargo(String value) {
        this.cargo = value;
    }

    /**
     * Obtiene el valor de la propiedad dependencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependencia() {
        return dependencia;
    }

    /**
     * Define el valor de la propiedad dependencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependencia(String value) {
        this.dependencia = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoResponsable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoResponsable() {
        return tipoResponsable;
    }

    /**
     * Define el valor de la propiedad tipoResponsable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoResponsable(String value) {
        this.tipoResponsable = value;
    }

    /**
     * Obtiene el valor de la propiedad empresa.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getEmpresa() {
        return empresa;
    }

    /**
     * Define el valor de la propiedad empresa.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setEmpresa(Object value) {
        this.empresa = value;
    }

}
