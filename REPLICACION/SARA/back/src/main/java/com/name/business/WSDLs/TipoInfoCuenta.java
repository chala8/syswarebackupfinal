
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de la cuenta Bancaria de una persona en la Universidad Nacional de Colombia
 * 
 * <p>Java class for tipoInfoCuenta complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoCuenta">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NumeroDeCuenta" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNumeroDeCuenta"/>
 *         &lt;element name="TipoDeCuenta" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoDeCuenta"/>
 *         &lt;element name="Banco" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoBanco"/>
 *         &lt;element name="Sucursal" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoSucursal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoCuenta", propOrder = {
    "numeroDeCuenta",
    "tipoDeCuenta",
    "banco",
    "sucursal"
})
public class TipoInfoCuenta {

    @XmlElement(name = "NumeroDeCuenta", required = true)
    protected String numeroDeCuenta;
    @XmlElement(name = "TipoDeCuenta", required = true)
    @XmlSchemaType(name = "string")
    protected TipoTipoDeCuenta tipoDeCuenta;
    @XmlElement(name = "Banco", required = true)
    protected String banco;
    @XmlElement(name = "Sucursal", required = true)
    protected String sucursal;

    /**
     * Gets the value of the numeroDeCuenta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroDeCuenta() {
        return numeroDeCuenta;
    }

    /**
     * Sets the value of the numeroDeCuenta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroDeCuenta(String value) {
        this.numeroDeCuenta = value;
    }

    /**
     * Gets the value of the tipoDeCuenta property.
     * 
     * @return
     *     possible object is
     *     {@link TipoTipoDeCuenta }
     *     
     */
    public TipoTipoDeCuenta getTipoDeCuenta() {
        return tipoDeCuenta;
    }

    /**
     * Sets the value of the tipoDeCuenta property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoTipoDeCuenta }
     *     
     */
    public void setTipoDeCuenta(TipoTipoDeCuenta value) {
        this.tipoDeCuenta = value;
    }

    /**
     * Gets the value of the banco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanco() {
        return banco;
    }

    /**
     * Sets the value of the banco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanco(String value) {
        this.banco = value;
    }

    /**
     * Gets the value of the sucursal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * Sets the value of the sucursal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSucursal(String value) {
        this.sucursal = value;
    }

}
