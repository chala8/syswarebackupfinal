
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de solicitud para la creaci�n de una persona en los sistemas de informaci�n HERMES INVESTIGACION Y HERMES EXTENSION
 * 
 * <p>Java class for tipoSolicitudCreacionPersonaHermes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoSolicitudCreacionPersonaHermes">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="Persona" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoPersonaHermes"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoSolicitudCreacionPersonaHermes", propOrder = {
    "persona"
})
public class TipoSolicitudCreacionPersonaHermes
    extends TipoMsjGenerico
{

    @XmlElement(name = "Persona", required = true)
    protected TipoPersonaHermes persona;

    /**
     * Gets the value of the persona property.
     * 
     * @return
     *     possible object is
     *     {@link TipoPersonaHermes }
     *     
     */
    public TipoPersonaHermes getPersona() {
        return persona;
    }

    /**
     * Sets the value of the persona property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPersonaHermes }
     *     
     */
    public void setPersona(TipoPersonaHermes value) {
        this.persona = value;
    }

}
