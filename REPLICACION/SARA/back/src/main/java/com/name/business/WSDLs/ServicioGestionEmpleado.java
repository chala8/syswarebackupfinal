
package com.name.business.WSDLs;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "ServicioGestionEmpleado", targetNamespace = "http://interoperabilidad.unal.edu.co/WS/GestionEmpleado", wsdlLocation = "http://168.176.6.92:8280/INTEGRACION/services/ServicioGestionEmpleado?wsdl")
public class ServicioGestionEmpleado
    extends Service
{

    private final static URL SERVICIOGESTIONEMPLEADO_WSDL_LOCATION;
    private final static WebServiceException SERVICIOGESTIONEMPLEADO_EXCEPTION;
    private final static QName SERVICIOGESTIONEMPLEADO_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/GestionEmpleado", "ServicioGestionEmpleado");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://168.176.6.92:8280/INTEGRACION/services/ServicioGestionEmpleado?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SERVICIOGESTIONEMPLEADO_WSDL_LOCATION = url;
        SERVICIOGESTIONEMPLEADO_EXCEPTION = e;
    }

    public ServicioGestionEmpleado() {
        super(__getWsdlLocation(), SERVICIOGESTIONEMPLEADO_QNAME);
    }

    public ServicioGestionEmpleado(WebServiceFeature... features) {
        super(__getWsdlLocation(), SERVICIOGESTIONEMPLEADO_QNAME, features);
    }

    public ServicioGestionEmpleado(URL wsdlLocation) {
        super(wsdlLocation, SERVICIOGESTIONEMPLEADO_QNAME);
    }

    public ServicioGestionEmpleado(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SERVICIOGESTIONEMPLEADO_QNAME, features);
    }

    public ServicioGestionEmpleado(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ServicioGestionEmpleado(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns GestionEmpleadoPortType
     */
    @WebEndpoint(name = "EndPoint")
    public GestionEmpleadoPortType getEndPoint() {
        return super.getPort(new QName("http://interoperabilidad.unal.edu.co/WS/GestionEmpleado", "EndPoint"), GestionEmpleadoPortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns GestionEmpleadoPortType
     */
    @WebEndpoint(name = "EndPoint")
    public GestionEmpleadoPortType getEndPoint(WebServiceFeature... features) {
        return super.getPort(new QName("http://interoperabilidad.unal.edu.co/WS/GestionEmpleado", "EndPoint"), GestionEmpleadoPortType.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SERVICIOGESTIONEMPLEADO_EXCEPTION!= null) {
            throw SERVICIOGESTIONEMPLEADO_EXCEPTION;
        }
        return SERVICIOGESTIONEMPLEADO_WSDL_LOCATION;
    }

}
