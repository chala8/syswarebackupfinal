
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoEstadoCargueNomina.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoEstadoCargueNomina">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Cargado"/>
 *     &lt;enumeration value="Confirmado"/>
 *     &lt;enumeration value="Validado"/>
 *     &lt;enumeration value="Devuelto"/>
 *     &lt;enumeration value="Procesado"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoEstadoCargueNomina")
@XmlEnum
public enum TipoEstadoCargueNomina {

    @XmlEnumValue("Cargado")
    CARGADO("Cargado"),
    @XmlEnumValue("Confirmado")
    CONFIRMADO("Confirmado"),
    @XmlEnumValue("Validado")
    VALIDADO("Validado"),
    @XmlEnumValue("Devuelto")
    DEVUELTO("Devuelto"),
    @XmlEnumValue("Procesado")
    PROCESADO("Procesado");
    private final String value;

    TipoEstadoCargueNomina(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoEstadoCargueNomina fromValue(String v) {
        for (TipoEstadoCargueNomina c: TipoEstadoCargueNomina.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
