
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la respuesta de un empleado para interop
 * 
 * <p>Java class for tipoMsjRespuestaEmpleado complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaEmpleado">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="empleado" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEmpleado"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaEmpleado", propOrder = {
    "empleado"
})
public class TipoMsjRespuestaEmpleado
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected TipoEmpleado empleado;

    /**
     * Gets the value of the empleado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEmpleado }
     *     
     */
    public TipoEmpleado getEmpleado() {
        return empleado;
    }

    /**
     * Sets the value of the empleado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEmpleado }
     *     
     */
    public void setEmpleado(TipoEmpleado value) {
        this.empleado = value;
    }

}
