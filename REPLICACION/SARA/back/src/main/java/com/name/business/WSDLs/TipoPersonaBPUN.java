
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de un participante de proyectos de inversion para la universidad nacional de colombia
 * 
 * <p>Java class for tipoPersonaBPUN complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoPersonaBPUN">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InformacionBasica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaPersonaProyectos"/>
 *         &lt;element name="InfoContacto" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoContacto"/>
 *         &lt;sequence>
 *           &lt;element name="InfoVinculacion">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;choice>
 *                     &lt;element name="VinculacionPlanta" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoContrato"/>
 *                     &lt;element name="Externos" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoVinculacionExterna"/>
 *                   &lt;/choice>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoPersonaBPUN", propOrder = {
    "informacionBasica",
    "infoContacto",
    "infoVinculacion"
})
public class TipoPersonaBPUN {

    @XmlElement(name = "InformacionBasica", required = true)
    protected TipoInfoBasicaPersonaProyectos informacionBasica;
    @XmlElement(name = "InfoContacto", required = true)
    protected TipoInfoContacto infoContacto;
    @XmlElement(name = "InfoVinculacion", required = true)
    protected TipoPersonaBPUN.InfoVinculacion infoVinculacion;

    /**
     * Gets the value of the informacionBasica property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaPersonaProyectos }
     *     
     */
    public TipoInfoBasicaPersonaProyectos getInformacionBasica() {
        return informacionBasica;
    }

    /**
     * Sets the value of the informacionBasica property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaPersonaProyectos }
     *     
     */
    public void setInformacionBasica(TipoInfoBasicaPersonaProyectos value) {
        this.informacionBasica = value;
    }

    /**
     * Gets the value of the infoContacto property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoContacto }
     *     
     */
    public TipoInfoContacto getInfoContacto() {
        return infoContacto;
    }

    /**
     * Sets the value of the infoContacto property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoContacto }
     *     
     */
    public void setInfoContacto(TipoInfoContacto value) {
        this.infoContacto = value;
    }

    /**
     * Gets the value of the infoVinculacion property.
     * 
     * @return
     *     possible object is
     *     {@link TipoPersonaBPUN.InfoVinculacion }
     *     
     */
    public TipoPersonaBPUN.InfoVinculacion getInfoVinculacion() {
        return infoVinculacion;
    }

    /**
     * Sets the value of the infoVinculacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPersonaBPUN.InfoVinculacion }
     *     
     */
    public void setInfoVinculacion(TipoPersonaBPUN.InfoVinculacion value) {
        this.infoVinculacion = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="VinculacionPlanta" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoContrato"/>
     *         &lt;element name="Externos" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoVinculacionExterna"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "vinculacionPlanta",
        "externos"
    })
    public static class InfoVinculacion {

        @XmlElement(name = "VinculacionPlanta")
        protected TipoInfoContrato vinculacionPlanta;
        @XmlElement(name = "Externos")
        protected String externos;

        /**
         * Gets the value of the vinculacionPlanta property.
         * 
         * @return
         *     possible object is
         *     {@link TipoInfoContrato }
         *     
         */
        public TipoInfoContrato getVinculacionPlanta() {
            return vinculacionPlanta;
        }

        /**
         * Sets the value of the vinculacionPlanta property.
         * 
         * @param value
         *     allowed object is
         *     {@link TipoInfoContrato }
         *     
         */
        public void setVinculacionPlanta(TipoInfoContrato value) {
            this.vinculacionPlanta = value;
        }

        /**
         * Gets the value of the externos property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExternos() {
            return externos;
        }

        /**
         * Sets the value of the externos property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExternos(String value) {
            this.externos = value;
        }

    }

}
