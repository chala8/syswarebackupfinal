
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n detallada de una ciudad para IOP
 * 
 * <p>Java class for tipoInfoGeografica complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoGeografica">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pais" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoPais"/>
 *         &lt;element name="departamento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDep"/>
 *         &lt;element name="ciudad" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCiudad"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoGeografica", propOrder = {
    "pais",
    "departamento",
    "ciudad"
})
public class TipoInfoGeografica {

    @XmlElement(required = true)
    protected TipoPais pais;
    @XmlElement(required = true)
    protected TipoDep departamento;
    @XmlElement(required = true)
    protected TipoCiudad ciudad;

    /**
     * Gets the value of the pais property.
     * 
     * @return
     *     possible object is
     *     {@link TipoPais }
     *     
     */
    public TipoPais getPais() {
        return pais;
    }

    /**
     * Sets the value of the pais property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoPais }
     *     
     */
    public void setPais(TipoPais value) {
        this.pais = value;
    }

    /**
     * Gets the value of the departamento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDep }
     *     
     */
    public TipoDep getDepartamento() {
        return departamento;
    }

    /**
     * Sets the value of the departamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDep }
     *     
     */
    public void setDepartamento(TipoDep value) {
        this.departamento = value;
    }

    /**
     * Gets the value of the ciudad property.
     * 
     * @return
     *     possible object is
     *     {@link TipoCiudad }
     *     
     */
    public TipoCiudad getCiudad() {
        return ciudad;
    }

    /**
     * Sets the value of the ciudad property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCiudad }
     *     
     */
    public void setCiudad(TipoCiudad value) {
        this.ciudad = value;
    }

}
