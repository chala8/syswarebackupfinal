
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje para la respuesta de un empleado para interop
 * 
 * <p>Java class for tipoMsjRespuestaEmpleadoSARA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjRespuestaEmpleadoSARA">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="empleado" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEmpleadoSARA"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjRespuestaEmpleadoSARA", propOrder = {
    "empleado"
})
public class TipoMsjRespuestaEmpleadoSARA
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected TipoEmpleadoSARA empleado;

    /**
     * Gets the value of the empleado property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEmpleadoSARA }
     *     
     */
    public TipoEmpleadoSARA getEmpleado() {
        return empleado;
    }

    /**
     * Sets the value of the empleado property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEmpleadoSARA }
     *     
     */
    public void setEmpleado(TipoEmpleadoSARA value) {
        this.empleado = value;
    }

}
