
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de solicitud para e cambio de contrato en el sistem de infomaci�n SIBU
 * 
 * <p>Java class for tipoMsjSolicitudCambioContrato complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoMsjSolicitudCambioContrato">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="Contratos" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCambioContrato"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoMsjSolicitudCambioContrato", propOrder = {
    "contratos"
})
public class TipoMsjSolicitudCambioContrato
    extends TipoMsjGenerico
{

    @XmlElement(name = "Contratos", required = true)
    protected TipoCambioContrato contratos;

    /**
     * Gets the value of the contratos property.
     * 
     * @return
     *     possible object is
     *     {@link TipoCambioContrato }
     *     
     */
    public TipoCambioContrato getContratos() {
        return contratos;
    }

    /**
     * Sets the value of the contratos property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCambioContrato }
     *     
     */
    public void setContratos(TipoCambioContrato value) {
        this.contratos = value;
    }

}
