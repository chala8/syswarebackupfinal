
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoTipoDeCuenta.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoTipoDeCuenta">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CA"/>
 *     &lt;enumeration value="CC"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoTipoDeCuenta")
@XmlEnum
public enum TipoTipoDeCuenta {


    /**
     * Cuenta de Ahorros
     * 
     */
    CA,

    /**
     * Cuenta Corriente
     * 
     */
    CC;

    public String value() {
        return name();
    }

    public static TipoTipoDeCuenta fromValue(String v) {
        return valueOf(v);
    }

}
