
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de una ciudad para IOP
 * 
 * <p>Java class for tipoDatosDep complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoDatosDep">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nombreDepartamento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoNombreCiudad"/>
 *         &lt;element name="codigoDepartmento" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdDepartamento"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoDatosDep", propOrder = {
    "nombreDepartamento",
    "codigoDepartmento"
})
public class TipoDatosDep {

    @XmlElement(required = true)
    protected String nombreDepartamento;
    @XmlElement(required = true)
    protected String codigoDepartmento;

    /**
     * Gets the value of the nombreDepartamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    /**
     * Sets the value of the nombreDepartamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreDepartamento(String value) {
        this.nombreDepartamento = value;
    }

    /**
     * Gets the value of the codigoDepartmento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoDepartmento() {
        return codigoDepartmento;
    }

    /**
     * Sets the value of the codigoDepartmento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoDepartmento(String value) {
        this.codigoDepartmento = value;
    }

}
