
package com.name.business.WSDLs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n detallada de una dependencia para IOP
 * 
 * <p>Java class for tipoInfoDetalleDependencia complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoDetalleDependencia">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InfoBasica" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaDependencia"/>
 *         &lt;element name="InfoBasicaAnterior" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaDependencia"/>
 *         &lt;element name="InfoUbicacionDependenciat" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoUbicacionDependencia"/>
 *         &lt;element name="infoDirector" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoDirectorDependencia"/>
 *         &lt;element name="DependenciaPadre" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaDependencia"/>
 *         &lt;sequence maxOccurs="unbounded">
 *           &lt;element name="DependenciasHijas" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoBasicaDependencia"/>
 *         &lt;/sequence>
 *         &lt;element name="tipoDependencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoTipoDependencia"/>
 *         &lt;element name="estadoDependencia" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoEstadoDependencia"/>
 *         &lt;element name="Sede" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoSede"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoDetalleDependencia", propOrder = {
    "infoBasica",
    "infoBasicaAnterior",
    "infoUbicacionDependenciat",
    "infoDirector",
    "dependenciaPadre",
    "dependenciasHijas",
    "tipoDependencia",
    "estadoDependencia",
    "sede"
})
public class TipoInfoDetalleDependencia {

    @XmlElement(name = "InfoBasica", required = true)
    protected TipoInfoBasicaDependencia infoBasica;
    @XmlElement(name = "InfoBasicaAnterior", required = true)
    protected TipoInfoBasicaDependencia infoBasicaAnterior;
    @XmlElement(name = "InfoUbicacionDependenciat", required = true)
    protected TipoInfoUbicacionDependencia infoUbicacionDependenciat;
    @XmlElement(required = true)
    protected TipoInfoDirectorDependencia infoDirector;
    @XmlElement(name = "DependenciaPadre", required = true)
    protected TipoInfoBasicaDependencia dependenciaPadre;
    @XmlElement(name = "DependenciasHijas", required = true)
    protected List<TipoInfoBasicaDependencia> dependenciasHijas;
    @XmlElement(required = true)
    protected String tipoDependencia;
    @XmlElement(required = true)
    protected String estadoDependencia;
    @XmlElement(name = "Sede", required = true)
    protected String sede;

    /**
     * Gets the value of the infoBasica property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaDependencia }
     *     
     */
    public TipoInfoBasicaDependencia getInfoBasica() {
        return infoBasica;
    }

    /**
     * Sets the value of the infoBasica property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaDependencia }
     *     
     */
    public void setInfoBasica(TipoInfoBasicaDependencia value) {
        this.infoBasica = value;
    }

    /**
     * Gets the value of the infoBasicaAnterior property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaDependencia }
     *     
     */
    public TipoInfoBasicaDependencia getInfoBasicaAnterior() {
        return infoBasicaAnterior;
    }

    /**
     * Sets the value of the infoBasicaAnterior property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaDependencia }
     *     
     */
    public void setInfoBasicaAnterior(TipoInfoBasicaDependencia value) {
        this.infoBasicaAnterior = value;
    }

    /**
     * Gets the value of the infoUbicacionDependenciat property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoUbicacionDependencia }
     *     
     */
    public TipoInfoUbicacionDependencia getInfoUbicacionDependenciat() {
        return infoUbicacionDependenciat;
    }

    /**
     * Sets the value of the infoUbicacionDependenciat property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoUbicacionDependencia }
     *     
     */
    public void setInfoUbicacionDependenciat(TipoInfoUbicacionDependencia value) {
        this.infoUbicacionDependenciat = value;
    }

    /**
     * Gets the value of the infoDirector property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoDirectorDependencia }
     *     
     */
    public TipoInfoDirectorDependencia getInfoDirector() {
        return infoDirector;
    }

    /**
     * Sets the value of the infoDirector property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoDirectorDependencia }
     *     
     */
    public void setInfoDirector(TipoInfoDirectorDependencia value) {
        this.infoDirector = value;
    }

    /**
     * Gets the value of the dependenciaPadre property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoBasicaDependencia }
     *     
     */
    public TipoInfoBasicaDependencia getDependenciaPadre() {
        return dependenciaPadre;
    }

    /**
     * Sets the value of the dependenciaPadre property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoBasicaDependencia }
     *     
     */
    public void setDependenciaPadre(TipoInfoBasicaDependencia value) {
        this.dependenciaPadre = value;
    }

    /**
     * Gets the value of the dependenciasHijas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dependenciasHijas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDependenciasHijas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoInfoBasicaDependencia }
     * 
     * 
     */
    public List<TipoInfoBasicaDependencia> getDependenciasHijas() {
        if (dependenciasHijas == null) {
            dependenciasHijas = new ArrayList<TipoInfoBasicaDependencia>();
        }
        return this.dependenciasHijas;
    }

    /**
     * Gets the value of the tipoDependencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDependencia() {
        return tipoDependencia;
    }

    /**
     * Sets the value of the tipoDependencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDependencia(String value) {
        this.tipoDependencia = value;
    }

    /**
     * Gets the value of the estadoDependencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoDependencia() {
        return estadoDependencia;
    }

    /**
     * Sets the value of the estadoDependencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoDependencia(String value) {
        this.estadoDependencia = value;
    }

    /**
     * Gets the value of the sede property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSede() {
        return sede;
    }

    /**
     * Sets the value of the sede property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSede(String value) {
        this.sede = value;
    }

}
