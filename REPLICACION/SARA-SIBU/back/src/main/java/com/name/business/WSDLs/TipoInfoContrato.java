
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * indica la informacion completa de un contrato para interop
 * 
 * <p>Java class for tipoInfoContrato complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoContrato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cabeceraContrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCabeceraContrato"/>
 *         &lt;element name="detalleContrato" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoDetalleContrato"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoContrato", propOrder = {
    "cabeceraContrato",
    "detalleContrato"
})
public class TipoInfoContrato {

    @XmlElement(required = true)
    protected TipoCabeceraContrato cabeceraContrato;
    @XmlElement(required = true)
    protected TipoDetalleContrato detalleContrato;

    /**
     * Gets the value of the cabeceraContrato property.
     * 
     * @return
     *     possible object is
     *     {@link TipoCabeceraContrato }
     *     
     */
    public TipoCabeceraContrato getCabeceraContrato() {
        return cabeceraContrato;
    }

    /**
     * Sets the value of the cabeceraContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoCabeceraContrato }
     *     
     */
    public void setCabeceraContrato(TipoCabeceraContrato value) {
        this.cabeceraContrato = value;
    }

    /**
     * Gets the value of the detalleContrato property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDetalleContrato }
     *     
     */
    public TipoDetalleContrato getDetalleContrato() {
        return detalleContrato;
    }

    /**
     * Sets the value of the detalleContrato property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDetalleContrato }
     *     
     */
    public void setDetalleContrato(TipoDetalleContrato value) {
        this.detalleContrato = value;
    }

}
