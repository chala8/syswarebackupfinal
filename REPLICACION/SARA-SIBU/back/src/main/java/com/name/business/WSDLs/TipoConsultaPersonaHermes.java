
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * tipo de mensaje de solicitud para la consulta de una persona en los sistemas de informaci�n HERMES INVESTIGACION Y HERMES EXTENSION
 * 
 * <p>Java class for tipoConsultaPersonaHermes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoConsultaPersonaHermes">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoMsjGenerico">
 *       &lt;sequence>
 *         &lt;element name="idTercero" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoIdTercero"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoConsultaPersonaHermes", propOrder = {
    "idTercero"
})
public class TipoConsultaPersonaHermes
    extends TipoMsjGenerico
{

    @XmlElement(required = true)
    protected TipoIdTercero idTercero;

    /**
     * Gets the value of the idTercero property.
     * 
     * @return
     *     possible object is
     *     {@link TipoIdTercero }
     *     
     */
    public TipoIdTercero getIdTercero() {
        return idTercero;
    }

    /**
     * Sets the value of the idTercero property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoIdTercero }
     *     
     */
    public void setIdTercero(TipoIdTercero value) {
        this.idTercero = value;
    }

}
