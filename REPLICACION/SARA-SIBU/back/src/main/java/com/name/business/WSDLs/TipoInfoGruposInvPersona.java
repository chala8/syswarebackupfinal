
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de un participante de proyectos de investigaci�n o extensi�n para la universidad nacional de colombia
 * 
 * <p>Java class for tipoInfoGruposInvPersona complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoGruposInvPersona">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GruposDeInvestigacion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoGrupoInv"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoGruposInvPersona", propOrder = {
    "gruposDeInvestigacion"
})
public class TipoInfoGruposInvPersona {

    @XmlElement(name = "GruposDeInvestigacion", required = true)
    protected TipoInfoGrupoInv gruposDeInvestigacion;

    /**
     * Gets the value of the gruposDeInvestigacion property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoGrupoInv }
     *     
     */
    public TipoInfoGrupoInv getGruposDeInvestigacion() {
        return gruposDeInvestigacion;
    }

    /**
     * Sets the value of the gruposDeInvestigacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoGrupoInv }
     *     
     */
    public void setGruposDeInvestigacion(TipoInfoGrupoInv value) {
        this.gruposDeInvestigacion = value;
    }

}
