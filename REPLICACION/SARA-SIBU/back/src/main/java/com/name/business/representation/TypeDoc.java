package com.name.business.representation;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TypeDoc {

    private String type;
    private String number;
    private String salud;

    @JsonCreator
    public TypeDoc(@JsonProperty("type") String type,
                   @JsonProperty("salud") String salud,
                   @JsonProperty("number") String number) {
        this.type = type;
        this.number = number;
        this.salud = salud;

    }

    public String getSalud() { return salud; }

    public void setSalud(String salud) { this.salud = salud; }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
