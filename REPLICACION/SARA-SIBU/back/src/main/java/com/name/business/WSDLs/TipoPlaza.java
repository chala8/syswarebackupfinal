
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * agregado por Equipo IOP 2013 - Universidad Nacional de Colombia
 * 
 * <p>Java class for tipoPlaza complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoPlaza">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodigoPlaza" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoPlaza"/>
 *         &lt;element name="CodigoCategoria" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoCategoria"/>
 *         &lt;element name="Dedicacion" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoCodigoDedicacion"/>
 *         &lt;element name="FechaInicioPlaza" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFechaInicioPlaza"/>
 *         &lt;element name="FechaFinPlaza" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoFechaFinPlaza"/>
 *         &lt;element name="Asignatura" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoPlaza", propOrder = {
    "codigoPlaza",
    "codigoCategoria",
    "dedicacion",
    "fechaInicioPlaza",
    "fechaFinPlaza",
    "asignatura"
})
public class TipoPlaza {

    @XmlElement(name = "CodigoPlaza", required = true)
    protected String codigoPlaza;
    @XmlElement(name = "CodigoCategoria", required = true)
    protected String codigoCategoria;
    @XmlElement(name = "Dedicacion", required = true)
    protected String dedicacion;
    @XmlElement(name = "FechaInicioPlaza", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaInicioPlaza;
    @XmlElement(name = "FechaFinPlaza", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fechaFinPlaza;
    @XmlElement(name = "Asignatura", required = true)
    protected Object asignatura;

    /**
     * Gets the value of the codigoPlaza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoPlaza() {
        return codigoPlaza;
    }

    /**
     * Sets the value of the codigoPlaza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoPlaza(String value) {
        this.codigoPlaza = value;
    }

    /**
     * Gets the value of the codigoCategoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCategoria() {
        return codigoCategoria;
    }

    /**
     * Sets the value of the codigoCategoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCategoria(String value) {
        this.codigoCategoria = value;
    }

    /**
     * Gets the value of the dedicacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDedicacion() {
        return dedicacion;
    }

    /**
     * Sets the value of the dedicacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDedicacion(String value) {
        this.dedicacion = value;
    }

    /**
     * Gets the value of the fechaInicioPlaza property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaInicioPlaza() {
        return fechaInicioPlaza;
    }

    /**
     * Sets the value of the fechaInicioPlaza property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaInicioPlaza(XMLGregorianCalendar value) {
        this.fechaInicioPlaza = value;
    }

    /**
     * Gets the value of the fechaFinPlaza property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaFinPlaza() {
        return fechaFinPlaza;
    }

    /**
     * Sets the value of the fechaFinPlaza property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaFinPlaza(XMLGregorianCalendar value) {
        this.fechaFinPlaza = value;
    }

    /**
     * Gets the value of the asignatura property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getAsignatura() {
        return asignatura;
    }

    /**
     * Sets the value of the asignatura property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setAsignatura(Object value) {
        this.asignatura = value;
    }

}
