
package com.name.business.WSDLs;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the WSDLs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ElemRespuestaConsultaUsuarioALEPH_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaConsultaUsuarioALEPH");
    private final static QName _ElemConsultaDetalleDocente_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaDetalleDocente");
    private final static QName _ElemSolicitudCreacionTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudCreacionTercero");
    private final static QName _ElemRespuestaActualizacionPersonaHermes_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaActualizacionPersonaHermes");
    private final static QName _ElemSolicitudGestionPersonaBPUN_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudGestionPersonaBPUN");
    private final static QName _Out_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/GestionEmpleado", "out");
    private final static QName _ElemRespuestaExistenciaInvestigador_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaExistenciaInvestigador");
    private final static QName _ElemRespuestaViaticos_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaViaticos");
    private final static QName _ElemRespuestaHomologAreaResponsabilidad_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaHomologAreaResponsabilidad");
    private final static QName _ElemSolicitudConsultaViaticos_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudConsultaViaticos");
    private final static QName _ElemSolicitudCambioContratoSARA_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudCambioContratoSARA");
    private final static QName _ElemSolicitudHomologConceptoPresupuesto_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudHomologConceptoPresupuesto");
    private final static QName _ElemRespuestaRegistroCuentaNomina_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaRegistroCuentaNomina");
    private final static QName _ElemRespuestaGestionEmpleadoSIBU_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaGestionEmpleadoSIBU");
    private final static QName _ElemSolicitudGestionDependenciasSIBU_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudGestionDependenciasSIBU");
    private final static QName _ElemSolicitudGestionEmpleadoSIBU_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudGestionEmpleadoSIBU");
    private final static QName _ElemConsultaTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaTercero");
    private final static QName _ElemRespuestaEmpleado_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaEmpleado");
    private final static QName _ElemRespuestaCreacionUsuarioALEPH_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCreacionUsuarioALEPH");
    private final static QName _ElemSolicitudReplicaTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudReplicaTercero");
    private final static QName _ElemConsultaEmpleadoPorIdTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaEmpleadoPorIdTercero");
    private final static QName _ElemRespuestaCambioContratoSIBU_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCambioContratoSIBU");
    private final static QName _ElemRespuestaCabeceraContratosNacional_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCabeceraContratosNacional");
    private final static QName _ElemSolicitudInformacionGeograficaSIBU_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudInformacionGeograficaSIBU");
    private final static QName _ElemSolicitudCreacionPersonaHermes_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudCreacionPersonaHermes");
    private final static QName _ElemSolicitudCreacionActualizacionTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudCreacionActualizacionTercero");
    private final static QName _ElemRespuestaActualizacionContratistaInvestigador_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaActualizacionContratistaInvestigador");
    private final static QName _ElemRespuestaConsultaTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaConsultaTercero");
    private final static QName _ElemRespuestaCreacionPersonaHermes_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCreacionPersonaHermes");
    private final static QName _ElemSolicitudCambioContratoSIBU_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudCambioContratoSIBU");
    private final static QName _ElemRespuestaActualizacionTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaActualizacionTercero");
    private final static QName _ElemSolicitudConsultaUsuarioALEPH_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudConsultaUsuarioALEPH");
    private final static QName _ElemRespuestaHomologConceptoPresupuesto_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaHomologConceptoPresupuesto");
    private final static QName _ElemRespuestaReplicaTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaReplicaTercero");
    private final static QName _ElemRespuestaCambioDocumentoSARA_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCambioDocumentoSARA");
    private final static QName _ElemSolicitudConsultaDependencia_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudConsultaDependencia");
    private final static QName _ElemRespuestaCreacionActualizacionTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCreacionActualizacionTercero");
    private final static QName _ElemRespuestaInformacionGeograficaSIBU_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaInformacionGeograficaSIBU");
    private final static QName _ElemSolicitudActualizacionDocente_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudActualizacionDocente");
    private final static QName _ElemSolicitudActualizacionTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudActualizacionTercero");
    private final static QName _ElemRespuestaRubro_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaRubro");
    private final static QName _ElemRespuestaActualizacionEstudianteInvestigador_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaActualizacionEstudianteInvestigador");
    private final static QName _ElemSolicitudAdicionarCuentaBancaria_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudAdicionarCuentaBancaria");
    private final static QName _ElemRespuestaGestionDependenciasBPUN_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaGestionDependenciasBPUN");
    private final static QName _ElemRespuestaSolicitudContableNomina_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaSolicitudContableNomina");
    private final static QName _ElemConsultaExistenciaTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaExistenciaTercero");
    private final static QName _ElemRespuestaPersonaHermes_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaPersonaHermes");
    private final static QName _ElemExistenciaDocente_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemExistenciaDocente");
    private final static QName _ElemRespuestaInfoComplementaria_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaInfoComplementaria");
    private final static QName _ElemRespuestaDependencia_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaDependencia");
    private final static QName _ElemConsultaEstudiante_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaEstudiante");
    private final static QName _ElemRespuestaAdicionarCuentaBancaria_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaAdicionarCuentaBancaria");
    private final static QName _ElemRespuestaGestionDependenciaSIBU_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaGestionDependenciaSIBU");
    private final static QName _ElemRespuestaCambioDocumentoSIBU_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCambioDocumentoSIBU");
    private final static QName _ElemSolicitudRubro_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudRubro");
    private final static QName _ElemRespuestaGestionGeograficaSIBU_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaGestionGeograficaSIBU");
    private final static QName _ElemSolicitudConsultaInfoGeografica_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudConsultaInfoGeografica");
    private final static QName _ElemRespuestaCreacionEstudianteInvestigador_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCreacionEstudianteInvestigador");
    private final static QName _ElemRespuestaConsultaEstudiante_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaConsultaEstudiante");
    private final static QName _ElemSolicitudPersonaHermes_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudPersonaHermes");
    private final static QName _ElemRespuestaEstadoNomina_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaEstadoNomina");
    private final static QName _ElemSolicitudContableNomina_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudContableNomina");
    private final static QName _ElemRespuestaCreacionContratistaInvestigador_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCreacionContratistaInvestigador");
    private final static QName _ElemSolicitudCambioDocumentoSIBU_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudCambioDocumentoSIBU");
    private final static QName _ElemSolicitudHomologAreaResponsabilidad_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudHomologAreaResponsabilidad");
    private final static QName _ElemSolicitudActualizacionUsuarioALEPH_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudActualizacionUsuarioALEPH");
    private final static QName _ElemSolicitudCreacionDocente_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudCreacionDocente");
    private final static QName _ElemRespuestaGestionEmpleadoBPUN_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaGestionEmpleadoBPUN");
    private final static QName _ElemSolicitudActualizacionPersonaHermes_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudActualizacionPersonaHermes");
    private final static QName _ElemSolicitudEstadoCargueNomina_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudEstadoCargueNomina");
    private final static QName _ElemConsultaPorTipoVinculacion_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaPorTipoVinculacion");
    private final static QName _ElemSolicitudGestionDependenciasBPUN_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudGestionDependenciasBPUN");
    private final static QName _ElemRespuestaActualizacionUsuarioALEPH_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaActualizacionUsuarioALEPH");
    private final static QName _ElemSolicitudCreacionUsuarioALEPH_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudCreacionUsuarioALEPH");
    private final static QName _ElemSolicitudInfoComplementaria_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudInfoComplementaria");
    private final static QName _In_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/GestionEmpleado", "in");
    private final static QName _ElemRespuestaDetalleDocente_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaDetalleDocente");
    private final static QName _ElemRespuestaDetalleEstudiante_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaDetalleEstudiante");
    private final static QName _ElemRespuestaExistenciaDocente_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaExistenciaDocente");
    private final static QName _ElemRespuestaCambioContratoSARA_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCambioContratoSARA");
    private final static QName _ElemRespuestaCreacionTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCreacionTercero");
    private final static QName _ElemRespuestaCabeceraContratos_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaCabeceraContratos");
    private final static QName _ElemSolicitudCambioDocumentoSARA_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemSolicitudCambioDocumentoSARA");
    private final static QName _ElemRespuestaExistenciaTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaExistenciaTercero");
    private final static QName _ElemConsultaCabeceraContrato_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemConsultaCabeceraContrato");
    private final static QName _ElemRespuestaConsultaDetalleEmpleado_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaConsultaDetalleEmpleado");
    private final static QName _ElemRespuestaInformacionGeografica_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "elemRespuestaInformacionGeografica");
    private final static QName _TipoMsjRespuestaHomologAreaDescripcionAreaResponsabilidad_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "DescripcionAreaResponsabilidad");
    private final static QName _TipoMsjRespuestaHomologAreaCodigoAreaResponsabilidad_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "CodigoAreaResponsabilidad");
    private final static QName _TipoMsjCuentaBancariaTerceroInformacionCuentaNueva_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "InformacionCuentaNueva");
    private final static QName _TipoMsjCuentaBancariaTerceroIdTercero_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "idTercero");
    private final static QName _TipoMsjCuentaBancariaTerceroInformacionCuentaAntigua_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "InformacionCuentaAntigua");
    private final static QName _TipoMsjSolicitudHomologConceptoPresupuestoEmpresa_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "Empresa");
    private final static QName _TipoMsjSolicitudHomologConceptoPresupuestoNumeroItem_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "NumeroItem");
    private final static QName _TipoMsjSolicitudHomologConceptoPresupuestoTipoRegistro_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "TipoRegistro");
    private final static QName _TipoMsjSolicitudHomologConceptoPresupuestoInterface_QNAME = new QName("http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", "Interface");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: WSDLs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TipoPersonaBPUN }
     * 
     */
    public TipoPersonaBPUN createTipoPersonaBPUN() {
        return new TipoPersonaBPUN();
    }

    /**
     * Create an instance of {@link TipoInfoBasicaPersonal }
     * 
     */
    public TipoInfoBasicaPersonal createTipoInfoBasicaPersonal() {
        return new TipoInfoBasicaPersonal();
    }

    /**
     * Create an instance of {@link OpCambioContratoResponse }
     * 
     */
    public OpCambioContratoResponse createOpCambioContratoResponse() {
        return new OpCambioContratoResponse();
    }

    /**
     * Create an instance of {@link OpCambioDocumento }
     * 
     */
    public OpCambioDocumento createOpCambioDocumento() {
        return new OpCambioDocumento();
    }

    /**
     * Create an instance of {@link OpCambioDocumentoResponse }
     * 
     */
    public OpCambioDocumentoResponse createOpCambioDocumentoResponse() {
        return new OpCambioDocumentoResponse();
    }

    /**
     * Create an instance of {@link OpCambioContrato }
     * 
     */
    public OpCambioContrato createOpCambioContrato() {
        return new OpCambioContrato();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaCabeceraContratos }
     * 
     */
    public TipoMsjRespuestaCabeceraContratos createTipoMsjRespuestaCabeceraContratos() {
        return new TipoMsjRespuestaCabeceraContratos();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudInformacionGeograficaSIBU }
     * 
     */
    public TipoMsjSolicitudInformacionGeograficaSIBU createTipoMsjSolicitudInformacionGeograficaSIBU() {
        return new TipoMsjSolicitudInformacionGeograficaSIBU();
    }

    /**
     * Create an instance of {@link TipoSolicitudCreacionPersonaHermes }
     * 
     */
    public TipoSolicitudCreacionPersonaHermes createTipoSolicitudCreacionPersonaHermes() {
        return new TipoSolicitudCreacionPersonaHermes();
    }

    /**
     * Create an instance of {@link TipoMsjTercero }
     * 
     */
    public TipoMsjTercero createTipoMsjTercero() {
        return new TipoMsjTercero();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaOp }
     * 
     */
    public TipoMsjRespuestaOp createTipoMsjRespuestaOp() {
        return new TipoMsjRespuestaOp();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudCambioContrato }
     * 
     */
    public TipoMsjSolicitudCambioContrato createTipoMsjSolicitudCambioContrato() {
        return new TipoMsjSolicitudCambioContrato();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudPorIdTercero }
     * 
     */
    public TipoMsjSolicitudPorIdTercero createTipoMsjSolicitudPorIdTercero() {
        return new TipoMsjSolicitudPorIdTercero();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaHomologConceptoPresupuesto }
     * 
     */
    public TipoMsjRespuestaHomologConceptoPresupuesto createTipoMsjRespuestaHomologConceptoPresupuesto() {
        return new TipoMsjRespuestaHomologConceptoPresupuesto();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudPorCodigoDependencia }
     * 
     */
    public TipoMsjSolicitudPorCodigoDependencia createTipoMsjSolicitudPorCodigoDependencia() {
        return new TipoMsjSolicitudPorCodigoDependencia();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaEmpleado }
     * 
     */
    public TipoMsjRespuestaEmpleado createTipoMsjRespuestaEmpleado() {
        return new TipoMsjRespuestaEmpleado();
    }

    /**
     * Create an instance of {@link TipoMsjTerceroFinanciero }
     * 
     */
    public TipoMsjTerceroFinanciero createTipoMsjTerceroFinanciero() {
        return new TipoMsjTerceroFinanciero();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaRubro }
     * 
     */
    public TipoMsjRespuestaRubro createTipoMsjRespuestaRubro() {
        return new TipoMsjRespuestaRubro();
    }

    /**
     * Create an instance of {@link TipoMsjCuentaBancariaTercero }
     * 
     */
    public TipoMsjCuentaBancariaTercero createTipoMsjCuentaBancariaTercero() {
        return new TipoMsjCuentaBancariaTercero();
    }

    /**
     * Create an instance of {@link TipoRespuestaOp }
     * 
     */
    public TipoRespuestaOp createTipoRespuestaOp() {
        return new TipoRespuestaOp();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaPersonaHermes }
     * 
     */
    public TipoMsjRespuestaPersonaHermes createTipoMsjRespuestaPersonaHermes() {
        return new TipoMsjRespuestaPersonaHermes();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaExistenciaUsuarioALEPH }
     * 
     */
    public TipoMsjRespuestaExistenciaUsuarioALEPH createTipoMsjRespuestaExistenciaUsuarioALEPH() {
        return new TipoMsjRespuestaExistenciaUsuarioALEPH();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudGestionPersonaBPUN }
     * 
     */
    public TipoMsjSolicitudGestionPersonaBPUN createTipoMsjSolicitudGestionPersonaBPUN() {
        return new TipoMsjSolicitudGestionPersonaBPUN();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaExistencia }
     * 
     */
    public TipoMsjRespuestaExistencia createTipoMsjRespuestaExistencia() {
        return new TipoMsjRespuestaExistencia();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaViaticos }
     * 
     */
    public TipoMsjRespuestaViaticos createTipoMsjRespuestaViaticos() {
        return new TipoMsjRespuestaViaticos();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaHomologArea }
     * 
     */
    public TipoMsjRespuestaHomologArea createTipoMsjRespuestaHomologArea() {
        return new TipoMsjRespuestaHomologArea();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudCambioContratoSARA }
     * 
     */
    public TipoMsjSolicitudCambioContratoSARA createTipoMsjSolicitudCambioContratoSARA() {
        return new TipoMsjSolicitudCambioContratoSARA();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudHomologConceptoPresupuesto }
     * 
     */
    public TipoMsjSolicitudHomologConceptoPresupuesto createTipoMsjSolicitudHomologConceptoPresupuesto() {
        return new TipoMsjSolicitudHomologConceptoPresupuesto();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaRegistroCuentaNomina }
     * 
     */
    public TipoMsjRespuestaRegistroCuentaNomina createTipoMsjRespuestaRegistroCuentaNomina() {
        return new TipoMsjRespuestaRegistroCuentaNomina();
    }

    /**
     * Create an instance of {@link TipoMsjGestionDependenciaSIBU }
     * 
     */
    public TipoMsjGestionDependenciaSIBU createTipoMsjGestionDependenciaSIBU() {
        return new TipoMsjGestionDependenciaSIBU();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudGestionEmpleadoSIBU }
     * 
     */
    public TipoMsjSolicitudGestionEmpleadoSIBU createTipoMsjSolicitudGestionEmpleadoSIBU() {
        return new TipoMsjSolicitudGestionEmpleadoSIBU();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaEmpleadoSARA }
     * 
     */
    public TipoMsjRespuestaEmpleadoSARA createTipoMsjRespuestaEmpleadoSARA() {
        return new TipoMsjRespuestaEmpleadoSARA();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudCreacionUsuarioALEPH }
     * 
     */
    public TipoMsjSolicitudCreacionUsuarioALEPH createTipoMsjSolicitudCreacionUsuarioALEPH() {
        return new TipoMsjSolicitudCreacionUsuarioALEPH();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudInfoComplementaria }
     * 
     */
    public TipoMsjSolicitudInfoComplementaria createTipoMsjSolicitudInfoComplementaria() {
        return new TipoMsjSolicitudInfoComplementaria();
    }

    /**
     * Create an instance of {@link TipoMsjrespuestaDetalleDocente }
     * 
     */
    public TipoMsjrespuestaDetalleDocente createTipoMsjrespuestaDetalleDocente() {
        return new TipoMsjrespuestaDetalleDocente();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaDetalleEstudiante }
     * 
     */
    public TipoMsjRespuestaDetalleEstudiante createTipoMsjRespuestaDetalleEstudiante() {
        return new TipoMsjRespuestaDetalleEstudiante();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudCambioDocumento }
     * 
     */
    public TipoMsjSolicitudCambioDocumento createTipoMsjSolicitudCambioDocumento() {
        return new TipoMsjSolicitudCambioDocumento();
    }

    /**
     * Create an instance of {@link TipoMsjDetalleContratos }
     * 
     */
    public TipoMsjDetalleContratos createTipoMsjDetalleContratos() {
        return new TipoMsjDetalleContratos();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaInformacionGeografica }
     * 
     */
    public TipoMsjRespuestaInformacionGeografica createTipoMsjRespuestaInformacionGeografica() {
        return new TipoMsjRespuestaInformacionGeografica();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaInfoComplementaria }
     * 
     */
    public TipoMsjRespuestaInfoComplementaria createTipoMsjRespuestaInfoComplementaria() {
        return new TipoMsjRespuestaInfoComplementaria();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaDependencia }
     * 
     */
    public TipoMsjRespuestaDependencia createTipoMsjRespuestaDependencia() {
        return new TipoMsjRespuestaDependencia();
    }

    /**
     * Create an instance of {@link TipoMsjConsultaRubro }
     * 
     */
    public TipoMsjConsultaRubro createTipoMsjConsultaRubro() {
        return new TipoMsjConsultaRubro();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudInformacionGeografica }
     * 
     */
    public TipoMsjSolicitudInformacionGeografica createTipoMsjSolicitudInformacionGeografica() {
        return new TipoMsjSolicitudInformacionGeografica();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaConsultaEstudiante }
     * 
     */
    public TipoMsjRespuestaConsultaEstudiante createTipoMsjRespuestaConsultaEstudiante() {
        return new TipoMsjRespuestaConsultaEstudiante();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaEstadoNomina }
     * 
     */
    public TipoMsjRespuestaEstadoNomina createTipoMsjRespuestaEstadoNomina() {
        return new TipoMsjRespuestaEstadoNomina();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudCambioDocumentoSIBU }
     * 
     */
    public TipoMsjSolicitudCambioDocumentoSIBU createTipoMsjSolicitudCambioDocumentoSIBU() {
        return new TipoMsjSolicitudCambioDocumentoSIBU();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudHomlogArea }
     * 
     */
    public TipoMsjSolicitudHomlogArea createTipoMsjSolicitudHomlogArea() {
        return new TipoMsjSolicitudHomlogArea();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudActualizacionUsuarioALEPH }
     * 
     */
    public TipoMsjSolicitudActualizacionUsuarioALEPH createTipoMsjSolicitudActualizacionUsuarioALEPH() {
        return new TipoMsjSolicitudActualizacionUsuarioALEPH();
    }

    /**
     * Create an instance of {@link TipoSolicitudActualizacionPersonaHermes }
     * 
     */
    public TipoSolicitudActualizacionPersonaHermes createTipoSolicitudActualizacionPersonaHermes() {
        return new TipoSolicitudActualizacionPersonaHermes();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudEstadoCargueNomina }
     * 
     */
    public TipoMsjSolicitudEstadoCargueNomina createTipoMsjSolicitudEstadoCargueNomina() {
        return new TipoMsjSolicitudEstadoCargueNomina();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudPorTipoVinculacion }
     * 
     */
    public TipoMsjSolicitudPorTipoVinculacion createTipoMsjSolicitudPorTipoVinculacion() {
        return new TipoMsjSolicitudPorTipoVinculacion();
    }

    /**
     * Create an instance of {@link TipoMsjGestionDependenciaBPUN }
     * 
     */
    public TipoMsjGestionDependenciaBPUN createTipoMsjGestionDependenciaBPUN() {
        return new TipoMsjGestionDependenciaBPUN();
    }

    /**
     * Create an instance of {@link TipoAsignatura }
     * 
     */
    public TipoAsignatura createTipoAsignatura() {
        return new TipoAsignatura();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaPersonaHermesMal }
     * 
     */
    public TipoMsjRespuestaPersonaHermesMal createTipoMsjRespuestaPersonaHermesMal() {
        return new TipoMsjRespuestaPersonaHermesMal();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaContratos }
     * 
     */
    public TipoMsjRespuestaContratos createTipoMsjRespuestaContratos() {
        return new TipoMsjRespuestaContratos();
    }

    /**
     * Create an instance of {@link TipoInfoLaboratorio }
     * 
     */
    public TipoInfoLaboratorio createTipoInfoLaboratorio() {
        return new TipoInfoLaboratorio();
    }

    /**
     * Create an instance of {@link TipoNombrePersonaNatural }
     * 
     */
    public TipoNombrePersonaNatural createTipoNombrePersonaNatural() {
        return new TipoNombrePersonaNatural();
    }

    /**
     * Create an instance of {@link TipoCambioDocumento }
     * 
     */
    public TipoCambioDocumento createTipoCambioDocumento() {
        return new TipoCambioDocumento();
    }

    /**
     * Create an instance of {@link TipoInfoDetalleDependencia }
     * 
     */
    public TipoInfoDetalleDependencia createTipoInfoDetalleDependencia() {
        return new TipoInfoDetalleDependencia();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaCambioDocumento }
     * 
     */
    public TipoMsjRespuestaCambioDocumento createTipoMsjRespuestaCambioDocumento() {
        return new TipoMsjRespuestaCambioDocumento();
    }

    /**
     * Create an instance of {@link TipoInfoTributariaTercero }
     * 
     */
    public TipoInfoTributariaTercero createTipoInfoTributariaTercero() {
        return new TipoInfoTributariaTercero();
    }

    /**
     * Create an instance of {@link TipoInfoBasicaPlanEstudios }
     * 
     */
    public TipoInfoBasicaPlanEstudios createTipoInfoBasicaPlanEstudios() {
        return new TipoInfoBasicaPlanEstudios();
    }

    /**
     * Create an instance of {@link TipoOficina }
     * 
     */
    public TipoOficina createTipoOficina() {
        return new TipoOficina();
    }

    /**
     * Create an instance of {@link TipoContrato }
     * 
     */
    public TipoContrato createTipoContrato() {
        return new TipoContrato();
    }

    /**
     * Create an instance of {@link TipoConsultaPersonaHermes }
     * 
     */
    public TipoConsultaPersonaHermes createTipoConsultaPersonaHermes() {
        return new TipoConsultaPersonaHermes();
    }

    /**
     * Create an instance of {@link TipoCargaAcademicaDocente }
     * 
     */
    public TipoCargaAcademicaDocente createTipoCargaAcademicaDocente() {
        return new TipoCargaAcademicaDocente();
    }

    /**
     * Create an instance of {@link TipoInfoDetallePlanEstudios }
     * 
     */
    public TipoInfoDetallePlanEstudios createTipoInfoDetallePlanEstudios() {
        return new TipoInfoDetallePlanEstudios();
    }

    /**
     * Create an instance of {@link TipoReciboMatricula }
     * 
     */
    public TipoReciboMatricula createTipoReciboMatricula() {
        return new TipoReciboMatricula();
    }

    /**
     * Create an instance of {@link TipoInfoUbicacionDependencia }
     * 
     */
    public TipoInfoUbicacionDependencia createTipoInfoUbicacionDependencia() {
        return new TipoInfoUbicacionDependencia();
    }

    /**
     * Create an instance of {@link TipoInfoBasicaProyecto }
     * 
     */
    public TipoInfoBasicaProyecto createTipoInfoBasicaProyecto() {
        return new TipoInfoBasicaProyecto();
    }

    /**
     * Create an instance of {@link TipoPersonaHermes }
     * 
     */
    public TipoPersonaHermes createTipoPersonaHermes() {
        return new TipoPersonaHermes();
    }

    /**
     * Create an instance of {@link TipoInfoComplementaria }
     * 
     */
    public TipoInfoComplementaria createTipoInfoComplementaria() {
        return new TipoInfoComplementaria();
    }

    /**
     * Create an instance of {@link TipoContratoTercero }
     * 
     */
    public TipoContratoTercero createTipoContratoTercero() {
        return new TipoContratoTercero();
    }

    /**
     * Create an instance of {@link TipoInfoResponsable }
     * 
     */
    public TipoInfoResponsable createTipoInfoResponsable() {
        return new TipoInfoResponsable();
    }

    /**
     * Create an instance of {@link TipoDatosCiudad }
     * 
     */
    public TipoDatosCiudad createTipoDatosCiudad() {
        return new TipoDatosCiudad();
    }

    /**
     * Create an instance of {@link TipoConceptoPresupuesto }
     * 
     */
    public TipoConceptoPresupuesto createTipoConceptoPresupuesto() {
        return new TipoConceptoPresupuesto();
    }

    /**
     * Create an instance of {@link TipoCiudad }
     * 
     */
    public TipoCiudad createTipoCiudad() {
        return new TipoCiudad();
    }

    /**
     * Create an instance of {@link TipoDocente }
     * 
     */
    public TipoDocente createTipoDocente() {
        return new TipoDocente();
    }

    /**
     * Create an instance of {@link TipoDatosDep }
     * 
     */
    public TipoDatosDep createTipoDatosDep() {
        return new TipoDatosDep();
    }

    /**
     * Create an instance of {@link TipoMontoMonetario }
     * 
     */
    public TipoMontoMonetario createTipoMontoMonetario() {
        return new TipoMontoMonetario();
    }

    /**
     * Create an instance of {@link TipoInfoContrato }
     * 
     */
    public TipoInfoContrato createTipoInfoContrato() {
        return new TipoInfoContrato();
    }

    /**
     * Create an instance of {@link TipoRubroPresupuestal }
     * 
     */
    public TipoRubroPresupuestal createTipoRubroPresupuestal() {
        return new TipoRubroPresupuestal();
    }

    /**
     * Create an instance of {@link TipoZonaEconomica }
     * 
     */
    public TipoZonaEconomica createTipoZonaEconomica() {
        return new TipoZonaEconomica();
    }

    /**
     * Create an instance of {@link TipoInfoBasicaAsignatura }
     * 
     */
    public TipoInfoBasicaAsignatura createTipoInfoBasicaAsignatura() {
        return new TipoInfoBasicaAsignatura();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaContratista }
     * 
     */
    public TipoMsjRespuestaContratista createTipoMsjRespuestaContratista() {
        return new TipoMsjRespuestaContratista();
    }

    /**
     * Create an instance of {@link TipoUBGAA }
     * 
     */
    public TipoUBGAA createTipoUBGAA() {
        return new TipoUBGAA();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaCambioContrato }
     * 
     */
    public TipoMsjRespuestaCambioContrato createTipoMsjRespuestaCambioContrato() {
        return new TipoMsjRespuestaCambioContrato();
    }

    /**
     * Create an instance of {@link TipoEmpleado }
     * 
     */
    public TipoEmpleado createTipoEmpleado() {
        return new TipoEmpleado();
    }

    /**
     * Create an instance of {@link TipoInfoCuenta }
     * 
     */
    public TipoInfoCuenta createTipoInfoCuenta() {
        return new TipoInfoCuenta();
    }

    /**
     * Create an instance of {@link TipoCambioContrato }
     * 
     */
    public TipoCambioContrato createTipoCambioContrato() {
        return new TipoCambioContrato();
    }

    /**
     * Create an instance of {@link TipoInfoGrupoInv }
     * 
     */
    public TipoInfoGrupoInv createTipoInfoGrupoInv() {
        return new TipoInfoGrupoInv();
    }

    /**
     * Create an instance of {@link TipoUsuarioALEPH }
     * 
     */
    public TipoUsuarioALEPH createTipoUsuarioALEPH() {
        return new TipoUsuarioALEPH();
    }

    /**
     * Create an instance of {@link TipoMsjDocente }
     * 
     */
    public TipoMsjDocente createTipoMsjDocente() {
        return new TipoMsjDocente();
    }

    /**
     * Create an instance of {@link TipoInfoInvestigacionPersona }
     * 
     */
    public TipoInfoInvestigacionPersona createTipoInfoInvestigacionPersona() {
        return new TipoInfoInvestigacionPersona();
    }

    /**
     * Create an instance of {@link TipoPlanEstudios }
     * 
     */
    public TipoPlanEstudios createTipoPlanEstudios() {
        return new TipoPlanEstudios();
    }

    /**
     * Create an instance of {@link TipoInfoGruposColeccionesPersona }
     * 
     */
    public TipoInfoGruposColeccionesPersona createTipoInfoGruposColeccionesPersona() {
        return new TipoInfoGruposColeccionesPersona();
    }

    /**
     * Create an instance of {@link TipoInfoDirectorDependencia }
     * 
     */
    public TipoInfoDirectorDependencia createTipoInfoDirectorDependencia() {
        return new TipoInfoDirectorDependencia();
    }

    /**
     * Create an instance of {@link TipoUbicacion }
     * 
     */
    public TipoUbicacion createTipoUbicacion() {
        return new TipoUbicacion();
    }

    /**
     * Create an instance of {@link TipoInfoMensaje }
     * 
     */
    public TipoInfoMensaje createTipoInfoMensaje() {
        return new TipoInfoMensaje();
    }

    /**
     * Create an instance of {@link TipoMsjRespuestaEstudiante }
     * 
     */
    public TipoMsjRespuestaEstudiante createTipoMsjRespuestaEstudiante() {
        return new TipoMsjRespuestaEstudiante();
    }

    /**
     * Create an instance of {@link TipoTercero }
     * 
     */
    public TipoTercero createTipoTercero() {
        return new TipoTercero();
    }

    /**
     * Create an instance of {@link TipoDetalleContrato }
     * 
     */
    public TipoDetalleContrato createTipoDetalleContrato() {
        return new TipoDetalleContrato();
    }

    /**
     * Create an instance of {@link TipoInfoExtensionPersona }
     * 
     */
    public TipoInfoExtensionPersona createTipoInfoExtensionPersona() {
        return new TipoInfoExtensionPersona();
    }

    /**
     * Create an instance of {@link TipoInfoBasicaDependencia }
     * 
     */
    public TipoInfoBasicaDependencia createTipoInfoBasicaDependencia() {
        return new TipoInfoBasicaDependencia();
    }

    /**
     * Create an instance of {@link TipoTerceroFinanciero }
     * 
     */
    public TipoTerceroFinanciero createTipoTerceroFinanciero() {
        return new TipoTerceroFinanciero();
    }

    /**
     * Create an instance of {@link TipoDetalleContratoSARA }
     * 
     */
    public TipoDetalleContratoSARA createTipoDetalleContratoSARA() {
        return new TipoDetalleContratoSARA();
    }

    /**
     * Create an instance of {@link TipoInfoPais }
     * 
     */
    public TipoInfoPais createTipoInfoPais() {
        return new TipoInfoPais();
    }

    /**
     * Create an instance of {@link TipoDia }
     * 
     */
    public TipoDia createTipoDia() {
        return new TipoDia();
    }

    /**
     * Create an instance of {@link TipoCabeceraContrato }
     * 
     */
    public TipoCabeceraContrato createTipoCabeceraContrato() {
        return new TipoCabeceraContrato();
    }

    /**
     * Create an instance of {@link TipoInfoColecciones }
     * 
     */
    public TipoInfoColecciones createTipoInfoColecciones() {
        return new TipoInfoColecciones();
    }

    /**
     * Create an instance of {@link TipoInfoGeografica }
     * 
     */
    public TipoInfoGeografica createTipoInfoGeografica() {
        return new TipoInfoGeografica();
    }

    /**
     * Create an instance of {@link TipoDetalleDocente }
     * 
     */
    public TipoDetalleDocente createTipoDetalleDocente() {
        return new TipoDetalleDocente();
    }

    /**
     * Create an instance of {@link TipoInfoContratoSARA }
     * 
     */
    public TipoInfoContratoSARA createTipoInfoContratoSARA() {
        return new TipoInfoContratoSARA();
    }

    /**
     * Create an instance of {@link TipoInfoContacto }
     * 
     */
    public TipoInfoContacto createTipoInfoContacto() {
        return new TipoInfoContacto();
    }

    /**
     * Create an instance of {@link TipoIdTercero }
     * 
     */
    public TipoIdTercero createTipoIdTercero() {
        return new TipoIdTercero();
    }

    /**
     * Create an instance of {@link TipoEdificio }
     * 
     */
    public TipoEdificio createTipoEdificio() {
        return new TipoEdificio();
    }

    /**
     * Create an instance of {@link TipoEstudiante }
     * 
     */
    public TipoEstudiante createTipoEstudiante() {
        return new TipoEstudiante();
    }

    /**
     * Create an instance of {@link TipoInfoBasicaPersonaProyectos }
     * 
     */
    public TipoInfoBasicaPersonaProyectos createTipoInfoBasicaPersonaProyectos() {
        return new TipoInfoBasicaPersonaProyectos();
    }

    /**
     * Create an instance of {@link TipoInfoCiudad }
     * 
     */
    public TipoInfoCiudad createTipoInfoCiudad() {
        return new TipoInfoCiudad();
    }

    /**
     * Create an instance of {@link TipoCentroDeCostos }
     * 
     */
    public TipoCentroDeCostos createTipoCentroDeCostos() {
        return new TipoCentroDeCostos();
    }

    /**
     * Create an instance of {@link TipoAsignacionViaticos }
     * 
     */
    public TipoAsignacionViaticos createTipoAsignacionViaticos() {
        return new TipoAsignacionViaticos();
    }

    /**
     * Create an instance of {@link TipoEmpleadoSARA }
     * 
     */
    public TipoEmpleadoSARA createTipoEmpleadoSARA() {
        return new TipoEmpleadoSARA();
    }

    /**
     * Create an instance of {@link TipoDatosPais }
     * 
     */
    public TipoDatosPais createTipoDatosPais() {
        return new TipoDatosPais();
    }

    /**
     * Create an instance of {@link TipoInfoLaboratoriosPersona }
     * 
     */
    public TipoInfoLaboratoriosPersona createTipoInfoLaboratoriosPersona() {
        return new TipoInfoLaboratoriosPersona();
    }

    /**
     * Create an instance of {@link TipoMsjSolicitudContrato }
     * 
     */
    public TipoMsjSolicitudContrato createTipoMsjSolicitudContrato() {
        return new TipoMsjSolicitudContrato();
    }

    /**
     * Create an instance of {@link TipoInfoDetalleAsignatura }
     * 
     */
    public TipoInfoDetalleAsignatura createTipoInfoDetalleAsignatura() {
        return new TipoInfoDetalleAsignatura();
    }

    /**
     * Create an instance of {@link TipoMsjGenerico }
     * 
     */
    public TipoMsjGenerico createTipoMsjGenerico() {
        return new TipoMsjGenerico();
    }

    /**
     * Create an instance of {@link TipoPlaza }
     * 
     */
    public TipoPlaza createTipoPlaza() {
        return new TipoPlaza();
    }

    /**
     * Create an instance of {@link TipoDep }
     * 
     */
    public TipoDep createTipoDep() {
        return new TipoDep();
    }

    /**
     * Create an instance of {@link TipoInfoGruposInvPersona }
     * 
     */
    public TipoInfoGruposInvPersona createTipoInfoGruposInvPersona() {
        return new TipoInfoGruposInvPersona();
    }

    /**
     * Create an instance of {@link TipoContratista }
     * 
     */
    public TipoContratista createTipoContratista() {
        return new TipoContratista();
    }

    /**
     * Create an instance of {@link TipoPais }
     * 
     */
    public TipoPais createTipoPais() {
        return new TipoPais();
    }

    /**
     * Create an instance of {@link TipoPersonaBPUN.InfoVinculacion }
     * 
     */
    public TipoPersonaBPUN.InfoVinculacion createTipoPersonaBPUNInfoVinculacion() {
        return new TipoPersonaBPUN.InfoVinculacion();
    }

    /**
     * Create an instance of {@link TipoInfoBasicaPersonal.NomTercero }
     * 
     */
    public TipoInfoBasicaPersonal.NomTercero createTipoInfoBasicaPersonalNomTercero() {
        return new TipoInfoBasicaPersonal.NomTercero();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaExistenciaUsuarioALEPH }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaConsultaUsuarioALEPH")
    public JAXBElement<TipoMsjRespuestaExistenciaUsuarioALEPH> createElemRespuestaConsultaUsuarioALEPH(TipoMsjRespuestaExistenciaUsuarioALEPH value) {
        return new JAXBElement<TipoMsjRespuestaExistenciaUsuarioALEPH>(_ElemRespuestaConsultaUsuarioALEPH_QNAME, TipoMsjRespuestaExistenciaUsuarioALEPH.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaDetalleDocente")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemConsultaDetalleDocente(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemConsultaDetalleDocente_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjTerceroFinanciero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudCreacionTercero")
    public JAXBElement<TipoMsjTerceroFinanciero> createElemSolicitudCreacionTercero(TipoMsjTerceroFinanciero value) {
        return new JAXBElement<TipoMsjTerceroFinanciero>(_ElemSolicitudCreacionTercero_QNAME, TipoMsjTerceroFinanciero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaActualizacionPersonaHermes")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaActualizacionPersonaHermes(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaActualizacionPersonaHermes_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudGestionPersonaBPUN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudGestionPersonaBPUN")
    public JAXBElement<TipoMsjSolicitudGestionPersonaBPUN> createElemSolicitudGestionPersonaBPUN(TipoMsjSolicitudGestionPersonaBPUN value) {
        return new JAXBElement<TipoMsjSolicitudGestionPersonaBPUN>(_ElemSolicitudGestionPersonaBPUN_QNAME, TipoMsjSolicitudGestionPersonaBPUN.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/GestionEmpleado", name = "out")
    public JAXBElement<String> createOut(String value) {
        return new JAXBElement<String>(_Out_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaExistencia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaExistenciaInvestigador")
    public JAXBElement<TipoMsjRespuestaExistencia> createElemRespuestaExistenciaInvestigador(TipoMsjRespuestaExistencia value) {
        return new JAXBElement<TipoMsjRespuestaExistencia>(_ElemRespuestaExistenciaInvestigador_QNAME, TipoMsjRespuestaExistencia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaViaticos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaViaticos")
    public JAXBElement<TipoMsjRespuestaViaticos> createElemRespuestaViaticos(TipoMsjRespuestaViaticos value) {
        return new JAXBElement<TipoMsjRespuestaViaticos>(_ElemRespuestaViaticos_QNAME, TipoMsjRespuestaViaticos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaHomologArea }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaHomologAreaResponsabilidad")
    public JAXBElement<TipoMsjRespuestaHomologArea> createElemRespuestaHomologAreaResponsabilidad(TipoMsjRespuestaHomologArea value) {
        return new JAXBElement<TipoMsjRespuestaHomologArea>(_ElemRespuestaHomologAreaResponsabilidad_QNAME, TipoMsjRespuestaHomologArea.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudConsultaViaticos")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemSolicitudConsultaViaticos(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemSolicitudConsultaViaticos_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudCambioContratoSARA }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudCambioContratoSARA")
    public JAXBElement<TipoMsjSolicitudCambioContratoSARA> createElemSolicitudCambioContratoSARA(TipoMsjSolicitudCambioContratoSARA value) {
        return new JAXBElement<TipoMsjSolicitudCambioContratoSARA>(_ElemSolicitudCambioContratoSARA_QNAME, TipoMsjSolicitudCambioContratoSARA.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudHomologConceptoPresupuesto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudHomologConceptoPresupuesto")
    public JAXBElement<TipoMsjSolicitudHomologConceptoPresupuesto> createElemSolicitudHomologConceptoPresupuesto(TipoMsjSolicitudHomologConceptoPresupuesto value) {
        return new JAXBElement<TipoMsjSolicitudHomologConceptoPresupuesto>(_ElemSolicitudHomologConceptoPresupuesto_QNAME, TipoMsjSolicitudHomologConceptoPresupuesto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaRegistroCuentaNomina }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaRegistroCuentaNomina")
    public JAXBElement<TipoMsjRespuestaRegistroCuentaNomina> createElemRespuestaRegistroCuentaNomina(TipoMsjRespuestaRegistroCuentaNomina value) {
        return new JAXBElement<TipoMsjRespuestaRegistroCuentaNomina>(_ElemRespuestaRegistroCuentaNomina_QNAME, TipoMsjRespuestaRegistroCuentaNomina.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaGestionEmpleadoSIBU")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaGestionEmpleadoSIBU(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaGestionEmpleadoSIBU_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjGestionDependenciaSIBU }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudGestionDependenciasSIBU")
    public JAXBElement<TipoMsjGestionDependenciaSIBU> createElemSolicitudGestionDependenciasSIBU(TipoMsjGestionDependenciaSIBU value) {
        return new JAXBElement<TipoMsjGestionDependenciaSIBU>(_ElemSolicitudGestionDependenciasSIBU_QNAME, TipoMsjGestionDependenciaSIBU.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudGestionEmpleadoSIBU }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudGestionEmpleadoSIBU")
    public JAXBElement<TipoMsjSolicitudGestionEmpleadoSIBU> createElemSolicitudGestionEmpleadoSIBU(TipoMsjSolicitudGestionEmpleadoSIBU value) {
        return new JAXBElement<TipoMsjSolicitudGestionEmpleadoSIBU>(_ElemSolicitudGestionEmpleadoSIBU_QNAME, TipoMsjSolicitudGestionEmpleadoSIBU.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaTercero")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemConsultaTercero(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemConsultaTercero_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaEmpleadoSARA }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaEmpleado")
    public JAXBElement<TipoMsjRespuestaEmpleadoSARA> createElemRespuestaEmpleado(TipoMsjRespuestaEmpleadoSARA value) {
        return new JAXBElement<TipoMsjRespuestaEmpleadoSARA>(_ElemRespuestaEmpleado_QNAME, TipoMsjRespuestaEmpleadoSARA.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCreacionUsuarioALEPH")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaCreacionUsuarioALEPH(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaCreacionUsuarioALEPH_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudReplicaTercero")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemSolicitudReplicaTercero(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemSolicitudReplicaTercero_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaEmpleadoPorIdTercero")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemConsultaEmpleadoPorIdTercero(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemConsultaEmpleadoPorIdTercero_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCambioContratoSIBU")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaCambioContratoSIBU(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaCambioContratoSIBU_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaCabeceraContratos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCabeceraContratosNacional")
    public JAXBElement<TipoMsjRespuestaCabeceraContratos> createElemRespuestaCabeceraContratosNacional(TipoMsjRespuestaCabeceraContratos value) {
        return new JAXBElement<TipoMsjRespuestaCabeceraContratos>(_ElemRespuestaCabeceraContratosNacional_QNAME, TipoMsjRespuestaCabeceraContratos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudInformacionGeograficaSIBU }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudInformacionGeograficaSIBU")
    public JAXBElement<TipoMsjSolicitudInformacionGeograficaSIBU> createElemSolicitudInformacionGeograficaSIBU(TipoMsjSolicitudInformacionGeograficaSIBU value) {
        return new JAXBElement<TipoMsjSolicitudInformacionGeograficaSIBU>(_ElemSolicitudInformacionGeograficaSIBU_QNAME, TipoMsjSolicitudInformacionGeograficaSIBU.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoSolicitudCreacionPersonaHermes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudCreacionPersonaHermes")
    public JAXBElement<TipoSolicitudCreacionPersonaHermes> createElemSolicitudCreacionPersonaHermes(TipoSolicitudCreacionPersonaHermes value) {
        return new JAXBElement<TipoSolicitudCreacionPersonaHermes>(_ElemSolicitudCreacionPersonaHermes_QNAME, TipoSolicitudCreacionPersonaHermes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudCreacionActualizacionTercero")
    public JAXBElement<TipoMsjTercero> createElemSolicitudCreacionActualizacionTercero(TipoMsjTercero value) {
        return new JAXBElement<TipoMsjTercero>(_ElemSolicitudCreacionActualizacionTercero_QNAME, TipoMsjTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaActualizacionContratistaInvestigador")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaActualizacionContratistaInvestigador(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaActualizacionContratistaInvestigador_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaConsultaTercero")
    public JAXBElement<TipoMsjTercero> createElemRespuestaConsultaTercero(TipoMsjTercero value) {
        return new JAXBElement<TipoMsjTercero>(_ElemRespuestaConsultaTercero_QNAME, TipoMsjTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCreacionPersonaHermes")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaCreacionPersonaHermes(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaCreacionPersonaHermes_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudCambioContrato }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudCambioContratoSIBU")
    public JAXBElement<TipoMsjSolicitudCambioContrato> createElemSolicitudCambioContratoSIBU(TipoMsjSolicitudCambioContrato value) {
        return new JAXBElement<TipoMsjSolicitudCambioContrato>(_ElemSolicitudCambioContratoSIBU_QNAME, TipoMsjSolicitudCambioContrato.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaActualizacionTercero")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaActualizacionTercero(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaActualizacionTercero_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudConsultaUsuarioALEPH")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemSolicitudConsultaUsuarioALEPH(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemSolicitudConsultaUsuarioALEPH_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaHomologConceptoPresupuesto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaHomologConceptoPresupuesto")
    public JAXBElement<TipoMsjRespuestaHomologConceptoPresupuesto> createElemRespuestaHomologConceptoPresupuesto(TipoMsjRespuestaHomologConceptoPresupuesto value) {
        return new JAXBElement<TipoMsjRespuestaHomologConceptoPresupuesto>(_ElemRespuestaHomologConceptoPresupuesto_QNAME, TipoMsjRespuestaHomologConceptoPresupuesto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaReplicaTercero")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaReplicaTercero(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaReplicaTercero_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCambioDocumentoSARA")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaCambioDocumentoSARA(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaCambioDocumentoSARA_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorCodigoDependencia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudConsultaDependencia")
    public JAXBElement<TipoMsjSolicitudPorCodigoDependencia> createElemSolicitudConsultaDependencia(TipoMsjSolicitudPorCodigoDependencia value) {
        return new JAXBElement<TipoMsjSolicitudPorCodigoDependencia>(_ElemSolicitudConsultaDependencia_QNAME, TipoMsjSolicitudPorCodigoDependencia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCreacionActualizacionTercero")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaCreacionActualizacionTercero(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaCreacionActualizacionTercero_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaInformacionGeograficaSIBU")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaInformacionGeograficaSIBU(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaInformacionGeograficaSIBU_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaEmpleado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudActualizacionDocente")
    public JAXBElement<TipoMsjRespuestaEmpleado> createElemSolicitudActualizacionDocente(TipoMsjRespuestaEmpleado value) {
        return new JAXBElement<TipoMsjRespuestaEmpleado>(_ElemSolicitudActualizacionDocente_QNAME, TipoMsjRespuestaEmpleado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjTerceroFinanciero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudActualizacionTercero")
    public JAXBElement<TipoMsjTerceroFinanciero> createElemSolicitudActualizacionTercero(TipoMsjTerceroFinanciero value) {
        return new JAXBElement<TipoMsjTerceroFinanciero>(_ElemSolicitudActualizacionTercero_QNAME, TipoMsjTerceroFinanciero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaRubro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaRubro")
    public JAXBElement<TipoMsjRespuestaRubro> createElemRespuestaRubro(TipoMsjRespuestaRubro value) {
        return new JAXBElement<TipoMsjRespuestaRubro>(_ElemRespuestaRubro_QNAME, TipoMsjRespuestaRubro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaActualizacionEstudianteInvestigador")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaActualizacionEstudianteInvestigador(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaActualizacionEstudianteInvestigador_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjCuentaBancariaTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudAdicionarCuentaBancaria")
    public JAXBElement<TipoMsjCuentaBancariaTercero> createElemSolicitudAdicionarCuentaBancaria(TipoMsjCuentaBancariaTercero value) {
        return new JAXBElement<TipoMsjCuentaBancariaTercero>(_ElemSolicitudAdicionarCuentaBancaria_QNAME, TipoMsjCuentaBancariaTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaGestionDependenciasBPUN")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaGestionDependenciasBPUN(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaGestionDependenciasBPUN_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaSolicitudContableNomina")
    public JAXBElement<TipoRespuestaOp> createElemRespuestaSolicitudContableNomina(TipoRespuestaOp value) {
        return new JAXBElement<TipoRespuestaOp>(_ElemRespuestaSolicitudContableNomina_QNAME, TipoRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaExistenciaTercero")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemConsultaExistenciaTercero(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemConsultaExistenciaTercero_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaPersonaHermes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaPersonaHermes")
    public JAXBElement<TipoMsjRespuestaPersonaHermes> createElemRespuestaPersonaHermes(TipoMsjRespuestaPersonaHermes value) {
        return new JAXBElement<TipoMsjRespuestaPersonaHermes>(_ElemRespuestaPersonaHermes_QNAME, TipoMsjRespuestaPersonaHermes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemExistenciaDocente")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemExistenciaDocente(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemExistenciaDocente_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaInfoComplementaria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaInfoComplementaria")
    public JAXBElement<TipoMsjRespuestaInfoComplementaria> createElemRespuestaInfoComplementaria(TipoMsjRespuestaInfoComplementaria value) {
        return new JAXBElement<TipoMsjRespuestaInfoComplementaria>(_ElemRespuestaInfoComplementaria_QNAME, TipoMsjRespuestaInfoComplementaria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaDependencia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaDependencia")
    public JAXBElement<TipoMsjRespuestaDependencia> createElemRespuestaDependencia(TipoMsjRespuestaDependencia value) {
        return new JAXBElement<TipoMsjRespuestaDependencia>(_ElemRespuestaDependencia_QNAME, TipoMsjRespuestaDependencia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaEstudiante")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemConsultaEstudiante(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemConsultaEstudiante_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaAdicionarCuentaBancaria")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaAdicionarCuentaBancaria(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaAdicionarCuentaBancaria_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaGestionDependenciaSIBU")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaGestionDependenciaSIBU(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaGestionDependenciaSIBU_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCambioDocumentoSIBU")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaCambioDocumentoSIBU(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaCambioDocumentoSIBU_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjConsultaRubro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudRubro")
    public JAXBElement<TipoMsjConsultaRubro> createElemSolicitudRubro(TipoMsjConsultaRubro value) {
        return new JAXBElement<TipoMsjConsultaRubro>(_ElemSolicitudRubro_QNAME, TipoMsjConsultaRubro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaGestionGeograficaSIBU")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaGestionGeograficaSIBU(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaGestionGeograficaSIBU_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudInformacionGeografica }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudConsultaInfoGeografica")
    public JAXBElement<TipoMsjSolicitudInformacionGeografica> createElemSolicitudConsultaInfoGeografica(TipoMsjSolicitudInformacionGeografica value) {
        return new JAXBElement<TipoMsjSolicitudInformacionGeografica>(_ElemSolicitudConsultaInfoGeografica_QNAME, TipoMsjSolicitudInformacionGeografica.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCreacionEstudianteInvestigador")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaCreacionEstudianteInvestigador(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaCreacionEstudianteInvestigador_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaConsultaEstudiante }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaConsultaEstudiante")
    public JAXBElement<TipoMsjRespuestaConsultaEstudiante> createElemRespuestaConsultaEstudiante(TipoMsjRespuestaConsultaEstudiante value) {
        return new JAXBElement<TipoMsjRespuestaConsultaEstudiante>(_ElemRespuestaConsultaEstudiante_QNAME, TipoMsjRespuestaConsultaEstudiante.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudPersonaHermes")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemSolicitudPersonaHermes(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemSolicitudPersonaHermes_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaEstadoNomina }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaEstadoNomina")
    public JAXBElement<TipoMsjRespuestaEstadoNomina> createElemRespuestaEstadoNomina(TipoMsjRespuestaEstadoNomina value) {
        return new JAXBElement<TipoMsjRespuestaEstadoNomina>(_ElemRespuestaEstadoNomina_QNAME, TipoMsjRespuestaEstadoNomina.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudContableNomina")
    public JAXBElement<Byte> createElemSolicitudContableNomina(Byte value) {
        return new JAXBElement<Byte>(_ElemSolicitudContableNomina_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCreacionContratistaInvestigador")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaCreacionContratistaInvestigador(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaCreacionContratistaInvestigador_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudCambioDocumentoSIBU }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudCambioDocumentoSIBU")
    public JAXBElement<TipoMsjSolicitudCambioDocumentoSIBU> createElemSolicitudCambioDocumentoSIBU(TipoMsjSolicitudCambioDocumentoSIBU value) {
        return new JAXBElement<TipoMsjSolicitudCambioDocumentoSIBU>(_ElemSolicitudCambioDocumentoSIBU_QNAME, TipoMsjSolicitudCambioDocumentoSIBU.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudHomlogArea }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudHomologAreaResponsabilidad")
    public JAXBElement<TipoMsjSolicitudHomlogArea> createElemSolicitudHomologAreaResponsabilidad(TipoMsjSolicitudHomlogArea value) {
        return new JAXBElement<TipoMsjSolicitudHomlogArea>(_ElemSolicitudHomologAreaResponsabilidad_QNAME, TipoMsjSolicitudHomlogArea.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudActualizacionUsuarioALEPH }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudActualizacionUsuarioALEPH")
    public JAXBElement<TipoMsjSolicitudActualizacionUsuarioALEPH> createElemSolicitudActualizacionUsuarioALEPH(TipoMsjSolicitudActualizacionUsuarioALEPH value) {
        return new JAXBElement<TipoMsjSolicitudActualizacionUsuarioALEPH>(_ElemSolicitudActualizacionUsuarioALEPH_QNAME, TipoMsjSolicitudActualizacionUsuarioALEPH.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaEmpleado }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudCreacionDocente")
    public JAXBElement<TipoMsjRespuestaEmpleado> createElemSolicitudCreacionDocente(TipoMsjRespuestaEmpleado value) {
        return new JAXBElement<TipoMsjRespuestaEmpleado>(_ElemSolicitudCreacionDocente_QNAME, TipoMsjRespuestaEmpleado.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaGestionEmpleadoBPUN")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaGestionEmpleadoBPUN(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaGestionEmpleadoBPUN_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoSolicitudActualizacionPersonaHermes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudActualizacionPersonaHermes")
    public JAXBElement<TipoSolicitudActualizacionPersonaHermes> createElemSolicitudActualizacionPersonaHermes(TipoSolicitudActualizacionPersonaHermes value) {
        return new JAXBElement<TipoSolicitudActualizacionPersonaHermes>(_ElemSolicitudActualizacionPersonaHermes_QNAME, TipoSolicitudActualizacionPersonaHermes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudEstadoCargueNomina }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudEstadoCargueNomina")
    public JAXBElement<TipoMsjSolicitudEstadoCargueNomina> createElemSolicitudEstadoCargueNomina(TipoMsjSolicitudEstadoCargueNomina value) {
        return new JAXBElement<TipoMsjSolicitudEstadoCargueNomina>(_ElemSolicitudEstadoCargueNomina_QNAME, TipoMsjSolicitudEstadoCargueNomina.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorTipoVinculacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaPorTipoVinculacion")
    public JAXBElement<TipoMsjSolicitudPorTipoVinculacion> createElemConsultaPorTipoVinculacion(TipoMsjSolicitudPorTipoVinculacion value) {
        return new JAXBElement<TipoMsjSolicitudPorTipoVinculacion>(_ElemConsultaPorTipoVinculacion_QNAME, TipoMsjSolicitudPorTipoVinculacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjGestionDependenciaBPUN }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudGestionDependenciasBPUN")
    public JAXBElement<TipoMsjGestionDependenciaBPUN> createElemSolicitudGestionDependenciasBPUN(TipoMsjGestionDependenciaBPUN value) {
        return new JAXBElement<TipoMsjGestionDependenciaBPUN>(_ElemSolicitudGestionDependenciasBPUN_QNAME, TipoMsjGestionDependenciaBPUN.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaActualizacionUsuarioALEPH")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaActualizacionUsuarioALEPH(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaActualizacionUsuarioALEPH_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudCreacionUsuarioALEPH }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudCreacionUsuarioALEPH")
    public JAXBElement<TipoMsjSolicitudCreacionUsuarioALEPH> createElemSolicitudCreacionUsuarioALEPH(TipoMsjSolicitudCreacionUsuarioALEPH value) {
        return new JAXBElement<TipoMsjSolicitudCreacionUsuarioALEPH>(_ElemSolicitudCreacionUsuarioALEPH_QNAME, TipoMsjSolicitudCreacionUsuarioALEPH.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudInfoComplementaria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudInfoComplementaria")
    public JAXBElement<TipoMsjSolicitudInfoComplementaria> createElemSolicitudInfoComplementaria(TipoMsjSolicitudInfoComplementaria value) {
        return new JAXBElement<TipoMsjSolicitudInfoComplementaria>(_ElemSolicitudInfoComplementaria_QNAME, TipoMsjSolicitudInfoComplementaria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/GestionEmpleado", name = "in")
    public JAXBElement<String> createIn(String value) {
        return new JAXBElement<String>(_In_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjrespuestaDetalleDocente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaDetalleDocente")
    public JAXBElement<TipoMsjrespuestaDetalleDocente> createElemRespuestaDetalleDocente(TipoMsjrespuestaDetalleDocente value) {
        return new JAXBElement<TipoMsjrespuestaDetalleDocente>(_ElemRespuestaDetalleDocente_QNAME, TipoMsjrespuestaDetalleDocente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaDetalleEstudiante }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaDetalleEstudiante")
    public JAXBElement<TipoMsjRespuestaDetalleEstudiante> createElemRespuestaDetalleEstudiante(TipoMsjRespuestaDetalleEstudiante value) {
        return new JAXBElement<TipoMsjRespuestaDetalleEstudiante>(_ElemRespuestaDetalleEstudiante_QNAME, TipoMsjRespuestaDetalleEstudiante.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaExistencia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaExistenciaDocente")
    public JAXBElement<TipoMsjRespuestaExistencia> createElemRespuestaExistenciaDocente(TipoMsjRespuestaExistencia value) {
        return new JAXBElement<TipoMsjRespuestaExistencia>(_ElemRespuestaExistenciaDocente_QNAME, TipoMsjRespuestaExistencia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCambioContratoSARA")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaCambioContratoSARA(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaCambioContratoSARA_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaOp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCreacionTercero")
    public JAXBElement<TipoMsjRespuestaOp> createElemRespuestaCreacionTercero(TipoMsjRespuestaOp value) {
        return new JAXBElement<TipoMsjRespuestaOp>(_ElemRespuestaCreacionTercero_QNAME, TipoMsjRespuestaOp.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaCabeceraContratos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaCabeceraContratos")
    public JAXBElement<TipoMsjRespuestaCabeceraContratos> createElemRespuestaCabeceraContratos(TipoMsjRespuestaCabeceraContratos value) {
        return new JAXBElement<TipoMsjRespuestaCabeceraContratos>(_ElemRespuestaCabeceraContratos_QNAME, TipoMsjRespuestaCabeceraContratos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudCambioDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemSolicitudCambioDocumentoSARA")
    public JAXBElement<TipoMsjSolicitudCambioDocumento> createElemSolicitudCambioDocumentoSARA(TipoMsjSolicitudCambioDocumento value) {
        return new JAXBElement<TipoMsjSolicitudCambioDocumento>(_ElemSolicitudCambioDocumentoSARA_QNAME, TipoMsjSolicitudCambioDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaExistencia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaExistenciaTercero")
    public JAXBElement<TipoMsjRespuestaExistencia> createElemRespuestaExistenciaTercero(TipoMsjRespuestaExistencia value) {
        return new JAXBElement<TipoMsjRespuestaExistencia>(_ElemRespuestaExistenciaTercero_QNAME, TipoMsjRespuestaExistencia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjSolicitudPorIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemConsultaCabeceraContrato")
    public JAXBElement<TipoMsjSolicitudPorIdTercero> createElemConsultaCabeceraContrato(TipoMsjSolicitudPorIdTercero value) {
        return new JAXBElement<TipoMsjSolicitudPorIdTercero>(_ElemConsultaCabeceraContrato_QNAME, TipoMsjSolicitudPorIdTercero.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjDetalleContratos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaConsultaDetalleEmpleado")
    public JAXBElement<TipoMsjDetalleContratos> createElemRespuestaConsultaDetalleEmpleado(TipoMsjDetalleContratos value) {
        return new JAXBElement<TipoMsjDetalleContratos>(_ElemRespuestaConsultaDetalleEmpleado_QNAME, TipoMsjDetalleContratos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoMsjRespuestaInformacionGeografica }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "elemRespuestaInformacionGeografica")
    public JAXBElement<TipoMsjRespuestaInformacionGeografica> createElemRespuestaInformacionGeografica(TipoMsjRespuestaInformacionGeografica value) {
        return new JAXBElement<TipoMsjRespuestaInformacionGeografica>(_ElemRespuestaInformacionGeografica_QNAME, TipoMsjRespuestaInformacionGeografica.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "DescripcionAreaResponsabilidad", scope = TipoMsjRespuestaHomologArea.class)
    public JAXBElement<Object> createTipoMsjRespuestaHomologAreaDescripcionAreaResponsabilidad(Object value) {
        return new JAXBElement<Object>(_TipoMsjRespuestaHomologAreaDescripcionAreaResponsabilidad_QNAME, Object.class, TipoMsjRespuestaHomologArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "CodigoAreaResponsabilidad", scope = TipoMsjRespuestaHomologArea.class)
    public JAXBElement<Object> createTipoMsjRespuestaHomologAreaCodigoAreaResponsabilidad(Object value) {
        return new JAXBElement<Object>(_TipoMsjRespuestaHomologAreaCodigoAreaResponsabilidad_QNAME, Object.class, TipoMsjRespuestaHomologArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoInfoCuenta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "InformacionCuentaNueva", scope = TipoMsjCuentaBancariaTercero.class)
    public JAXBElement<TipoInfoCuenta> createTipoMsjCuentaBancariaTerceroInformacionCuentaNueva(TipoInfoCuenta value) {
        return new JAXBElement<TipoInfoCuenta>(_TipoMsjCuentaBancariaTerceroInformacionCuentaNueva_QNAME, TipoInfoCuenta.class, TipoMsjCuentaBancariaTercero.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoIdTercero }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "idTercero", scope = TipoMsjCuentaBancariaTercero.class)
    public JAXBElement<TipoIdTercero> createTipoMsjCuentaBancariaTerceroIdTercero(TipoIdTercero value) {
        return new JAXBElement<TipoIdTercero>(_TipoMsjCuentaBancariaTerceroIdTercero_QNAME, TipoIdTercero.class, TipoMsjCuentaBancariaTercero.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoInfoCuenta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "InformacionCuentaAntigua", scope = TipoMsjCuentaBancariaTercero.class)
    public JAXBElement<TipoInfoCuenta> createTipoMsjCuentaBancariaTerceroInformacionCuentaAntigua(TipoInfoCuenta value) {
        return new JAXBElement<TipoInfoCuenta>(_TipoMsjCuentaBancariaTerceroInformacionCuentaAntigua_QNAME, TipoInfoCuenta.class, TipoMsjCuentaBancariaTercero.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "Empresa", scope = TipoMsjSolicitudHomologConceptoPresupuesto.class)
    public JAXBElement<String> createTipoMsjSolicitudHomologConceptoPresupuestoEmpresa(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomologConceptoPresupuestoEmpresa_QNAME, String.class, TipoMsjSolicitudHomologConceptoPresupuesto.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "NumeroItem", scope = TipoMsjSolicitudHomologConceptoPresupuesto.class)
    public JAXBElement<String> createTipoMsjSolicitudHomologConceptoPresupuestoNumeroItem(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomologConceptoPresupuestoNumeroItem_QNAME, String.class, TipoMsjSolicitudHomologConceptoPresupuesto.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoTipoRegistro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "TipoRegistro", scope = TipoMsjSolicitudHomologConceptoPresupuesto.class)
    public JAXBElement<TipoTipoRegistro> createTipoMsjSolicitudHomologConceptoPresupuestoTipoRegistro(TipoTipoRegistro value) {
        return new JAXBElement<TipoTipoRegistro>(_TipoMsjSolicitudHomologConceptoPresupuestoTipoRegistro_QNAME, TipoTipoRegistro.class, TipoMsjSolicitudHomologConceptoPresupuesto.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "Interface", scope = TipoMsjSolicitudHomologConceptoPresupuesto.class)
    public JAXBElement<String> createTipoMsjSolicitudHomologConceptoPresupuestoInterface(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomologConceptoPresupuestoInterface_QNAME, String.class, TipoMsjSolicitudHomologConceptoPresupuesto.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "Empresa", scope = TipoMsjSolicitudHomlogArea.class)
    public JAXBElement<String> createTipoMsjSolicitudHomlogAreaEmpresa(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomologConceptoPresupuestoEmpresa_QNAME, String.class, TipoMsjSolicitudHomlogArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "NumeroItem", scope = TipoMsjSolicitudHomlogArea.class)
    public JAXBElement<String> createTipoMsjSolicitudHomlogAreaNumeroItem(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomologConceptoPresupuestoNumeroItem_QNAME, String.class, TipoMsjSolicitudHomlogArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TipoTipoRegistro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "TipoRegistro", scope = TipoMsjSolicitudHomlogArea.class)
    public JAXBElement<TipoTipoRegistro> createTipoMsjSolicitudHomlogAreaTipoRegistro(TipoTipoRegistro value) {
        return new JAXBElement<TipoTipoRegistro>(_TipoMsjSolicitudHomologConceptoPresupuestoTipoRegistro_QNAME, TipoTipoRegistro.class, TipoMsjSolicitudHomlogArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros", name = "Interface", scope = TipoMsjSolicitudHomlogArea.class)
    public JAXBElement<String> createTipoMsjSolicitudHomlogAreaInterface(String value) {
        return new JAXBElement<String>(_TipoMsjSolicitudHomologConceptoPresupuestoInterface_QNAME, String.class, TipoMsjSolicitudHomlogArea.class, value);
    }

}
