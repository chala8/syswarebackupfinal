
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoTipoContribuyente.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoTipoContribuyente">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="RS"/>
 *     &lt;enumeration value="RC"/>
 *     &lt;enumeration value="GE"/>
 *     &lt;enumeration value="ES"/>
 *     &lt;enumeration value="AN"/>
 *     &lt;enumeration value="GA"/>
 *     &lt;enumeration value="GC"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoTipoContribuyente")
@XmlEnum
public enum TipoTipoContribuyente {


    /**
     * REGIMEN SIMPLIFICADO
     * 
     */
    RS,

    /**
     * REGIMEN COMUN
     * 
     */
    RC,

    /**
     * GE
     * 
     */
    GE,

    /**
     * ES
     * 
     */
    ES,

    /**
     * AN
     * 
     */
    AN,

    /**
     * GA
     * 
     */
    GA,

    /**
     * GRAN CONTRIBUYENTE
     * 
     */
    GC;

    public String value() {
        return name();
    }

    public static TipoTipoContribuyente fromValue(String v) {
        return valueOf(v);
    }

}
