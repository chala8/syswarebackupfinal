
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoTipoTercero.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoTipoTercero">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SE"/>
 *     &lt;enumeration value="DI"/>
 *     &lt;enumeration value="IG"/>
 *     &lt;enumeration value="AX"/>
 *     &lt;enumeration value="EM"/>
 *     &lt;enumeration value="PR"/>
 *     &lt;enumeration value="BC"/>
 *     &lt;enumeration value="BA"/>
 *     &lt;enumeration value="AV"/>
 *     &lt;enumeration value="GO"/>
 *     &lt;enumeration value="TR"/>
 *     &lt;enumeration value="IN"/>
 *     &lt;enumeration value="UT"/>
 *     &lt;enumeration value="AD"/>
 *     &lt;enumeration value="AS"/>
 *     &lt;enumeration value="UN"/>
 *     &lt;enumeration value="ES"/>
 *     &lt;enumeration value="OT"/>
 *     &lt;enumeration value="BG"/>
 *     &lt;enumeration value="CT"/>
 *     &lt;enumeration value="AF"/>
 *     &lt;enumeration value="CP"/>
 *     &lt;enumeration value="CD"/>
 *     &lt;enumeration value="AP"/>
 *     &lt;enumeration value="AL"/>
 *     &lt;enumeration value="IV"/>
 *     &lt;enumeration value="CC"/>
 *     &lt;enumeration value="FS"/>
 *     &lt;enumeration value="FP"/>
 *     &lt;enumeration value="FC"/>
 *     &lt;enumeration value="UP"/>
 *     &lt;enumeration value="SP"/>
 *     &lt;enumeration value="RK"/>
 *     &lt;enumeration value="DN"/>
 *     &lt;enumeration value="EX"/>
 *     &lt;enumeration value="CN"/>
 *     &lt;enumeration value="TB"/>
 *     &lt;enumeration value="TV"/>
 *     &lt;enumeration value="OI"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoTipoTercero")
@XmlEnum
public enum TipoTipoTercero {


    /**
     * EMPLEADOS CONTRATOS
     * 
     */
    SE,

    /**
     * TERCERO INACTIVO
     * 
     */
    DI,

    /**
     * INGRESOS - RK
     * 
     */
    IG,

    /**
     * AUXILIAR GENERAL
     * 
     */
    AX,

    /**
     * EMPLEADOS
     * 
     */
    EM,

    /**
     * PROVEEDORES
     * 
     */
    PR,

    /**
     * BANCOS
     * 
     */
    BC,

    /**
     * BANCOS CTA CTE
     * 
     */
    BA,

    /**
     * ACREEDORES VARIOS
     * 
     */
    AV,

    /**
     * GOBIERNO
     * 
     */
    GO,

    /**
     * TERCEROS
     * 
     */
    TR,

    /**
     * INVERSIONES
     * 
     */
    IN,

    /**
     * UTILIDAD O PERDIDA
     * 
     */
    UT,

    /**
     * AMORTIZACION DIFERIDOS
     * 
     */
    AD,

    /**
     * ASEGURADORAS
     * 
     */
    AS,

    /**
     * EMPRESAS U.N.
     * 
     */
    UN,

    /**
     * ESTUDIANTES
     * 
     */
    ES,

    /**
     * OTROS
     * 
     */
    OT,

    /**
     * BANCOS EN GENERAL
     * 
     */
    BG,

    /**
     * CONTRATISTAS
     * 
     */
    CT,

    /**
     * ACTIVOS FIJOS
     * 
     */
    AF,

    /**
     * CONTRATISTAS- PROVEEDORES
     * 
     */
    CP,

    /**
     * CARGOS DIFERIDOS
     * 
     */
    CD,

    /**
     * APORTES PENSIONES
     * 
     */
    AP,

    /**
     * APORTES SALUD
     * 
     */
    AL,

    /**
     * INVENTARIOS
     * 
     */
    IV,

    /**
     * CAJA DE COMPENSACION
     * 
     */
    CC,

    /**
     * FONDO DE SOLIDARIDAD EN SALUD
     * 
     */
    FS,

    /**
     * FONDO DE SOLIDARIDAD PENSIONAL
     * 
     */
    FP,

    /**
     * FONDO DE CESANTIAS
     * 
     */
    FC,

    /**
     * UNIVERSIDAD NACIONAL - PROVEEDORES
     * 
     */
    UP,

    /**
     * SALUD - PENSION - RIESGOS PROFESIONALES
     * 
     */
    SP,

    /**
     * RECAUDOS
     * 
     */
    RK,

    /**
     * DESCUENTOS NOMINA
     * 
     */
    DN,

    /**
     * EXTERIOR
     * 
     */
    EX,

    /**
     * CONSOLIDADOR
     * 
     */
    CN,

    /**
     * TERCEROS BIENES
     * 
     */
    TB,

    /**
     * TERCEROS VARIOS
     * 
     */
    TV,

    /**
     * ORGANISMO INTERNACIONAL
     * 
     */
    OI;

    public String value() {
        return name();
    }

    public static TipoTipoTercero fromValue(String v) {
        return valueOf(v);
    }

}
