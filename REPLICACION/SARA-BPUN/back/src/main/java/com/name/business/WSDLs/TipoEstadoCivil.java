
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoEstadoCivil.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoEstadoCivil">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="S"/>
 *     &lt;enumeration value="D"/>
 *     &lt;enumeration value="C"/>
 *     &lt;enumeration value="V"/>
 *     &lt;enumeration value="U"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoEstadoCivil")
@XmlEnum
public enum TipoEstadoCivil {


    /**
     * SOLTERO
     * 
     */
    S,

    /**
     * DIVORCIADO
     * 
     */
    D,

    /**
     * CASADO
     * 
     */
    C,

    /**
     * VIUDO
     * 
     */
    V,

    /**
     * UNION LIBRE
     * 
     */
    U;

    public String value() {
        return name();
    }

    public static TipoEstadoCivil fromValue(String v) {
        return valueOf(v);
    }

}
