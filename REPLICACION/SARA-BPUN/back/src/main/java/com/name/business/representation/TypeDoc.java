package com.name.business.representation;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TypeDoc {

    private String type;
    private String number;
    private String data;

    @JsonCreator
    public TypeDoc(@JsonProperty("type") String type,
                   @JsonProperty("number") String number,
                   @JsonProperty("data") String data) {
        this.type = type;
        this.number = number;
        this.data = data;


    }

    public String getData() { return data; }

    public void setData(String data) { this.data = data; }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
