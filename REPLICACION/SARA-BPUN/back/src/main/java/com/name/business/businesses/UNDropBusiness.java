package com.name.business.businesses;
import com.name.business.WSDLs.*;
import com.name.business.entities.TypeDocument;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;

import com.name.business.DAOs.*;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;



public class UNDropBusiness {

    private UNDropDAO billDAO;
    public UNDropBusiness(UNDropDAO billDAO) {
        this.billDAO = billDAO;
    }


    public Either<IException, Integer> getCC(String typeDoc, String docNumber, String data) {
        try {

            System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
            System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
            System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dumpTreshold", "999999");

            ServicioGestionEmpleado wsdl = new ServicioGestionEmpleado();
            GestionEmpleadoPortType instancedWSDL = wsdl.getEndPoint();

            TipoMsjSolicitudPorIdTercero elemConsultaTercero = new TipoMsjSolicitudPorIdTercero();


            TipoInfoMensaje infomensaje = new TipoInfoMensaje();

            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(new Date());
            XMLGregorianCalendar calendarFinal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);

            infomensaje.setFechaHora(calendarFinal);
            infomensaje.setIdSede("02");
            infomensaje.setIdSistema("100003");

            elemConsultaTercero.setInfoMensaje(infomensaje);

            TipoIdTercero idTercero = new TipoIdTercero();

            idTercero.setTipoDocumento(TipoTipoDocumento.CC);
            if(typeDoc.equals("CE")){
                idTercero.setTipoDocumento(TipoTipoDocumento.CE);
            }
            if(typeDoc.equals("IV")){
                idTercero.setTipoDocumento(TipoTipoDocumento.IV);
            }
            if(typeDoc.equals("NI")){
                idTercero.setTipoDocumento(TipoTipoDocumento.NI);
            }
            if(typeDoc.equals("PA")){
                idTercero.setTipoDocumento(TipoTipoDocumento.PA);
            }
            if(typeDoc.equals("RC")){
                idTercero.setTipoDocumento(TipoTipoDocumento.RC);
            }
            if(typeDoc.equals("TI")){
                idTercero.setTipoDocumento(TipoTipoDocumento.TI);
            }

            idTercero.setNumeroDocumento(docNumber);

            elemConsultaTercero.setIdTercero(idTercero);




            TipoMsjRespuestaEmpleadoSARA response = instancedWSDL.opConsultaInformacionEmpleado(elemConsultaTercero);


            System.out.println("------------------------ELEMENTOS DE INFO MENSAJE--------------------------");
            System.out.println("idSistema: "+response.getInfoMensaje().getIdSistema());
            System.out.println("idSede: "+response.getInfoMensaje().getIdSede());
            System.out.println("fechaHora: "+response.getInfoMensaje().getFechaHora());


            System.out.println("------------------------ELEMENTOS DE INFO BASICA TERCERO--------------------------");
            System.out.println("tipoDocumento: "+response.getEmpleado().getInfoBasicaTercero().getIdTercero().getTipoDocumento());
            System.out.println("numeroDocumento: "+response.getEmpleado().getInfoBasicaTercero().getIdTercero().getNumeroDocumento());
            System.out.println("generoTercero: "+response.getEmpleado().getInfoBasicaTercero().getGeneroTercero());
            System.out.println("primerNombre: "+response.getEmpleado().getInfoBasicaTercero().getNomTercero().getNombrePersonaNatural().getPrimerNombre());
            System.out.println("segundoNombre: "+response.getEmpleado().getInfoBasicaTercero().getNomTercero().getNombrePersonaNatural().getSegundoNombre());
            System.out.println("primerApellido: "+response.getEmpleado().getInfoBasicaTercero().getNomTercero().getNombrePersonaNatural().getPrimerApellido());
            System.out.println("segundoApellido: "+response.getEmpleado().getInfoBasicaTercero().getNomTercero().getNombrePersonaNatural().getSegundoApellido());




            URL url = new URL("http://168.176.6.92:8391/v1/bpun/cc");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            conn.setDoOutput(true);


            int numberOfId = 0;

            try{
                numberOfId = response.getEmpleado().getVinculacion().size()-1;
            }catch(Exception e){
                numberOfId = 0;
            }


            String jsonInputString = "{\n" +
                    "    \"idSede\" : \""+response.getInfoMensaje().getIdSede()+"\",\n" +
                    "    \"idSistema\" : \""+response.getInfoMensaje().getIdSistema()+"\",\n" +
                    "    \"primerNombre\" : \""+response.getEmpleado().getInfoBasicaTercero().getNomTercero().getNombrePersonaNatural().getPrimerNombre()+"\",\n" +
                    "    \"segundoNombre\" : \""+response.getEmpleado().getInfoBasicaTercero().getNomTercero().getNombrePersonaNatural().getSegundoNombre()+"\",\n" +
                    "    \"primerApellido\" : \""+response.getEmpleado().getInfoBasicaTercero().getNomTercero().getNombrePersonaNatural().getPrimerApellido()+"\",\n" +
                    "    \"segundoApellido\" : \""+response.getEmpleado().getInfoBasicaTercero().getNomTercero().getNombrePersonaNatural().getSegundoApellido()+"\",\n" +
                    "    \"tipoDocumento\" : \""+response.getEmpleado().getInfoBasicaTercero().getIdTercero().getTipoDocumento()+"\",\n" +
                    "    \"numDocumento\" : \""+response.getEmpleado().getInfoBasicaTercero().getIdTercero().getNumeroDocumento()+"\",\n" +
                    "    \"direccionContacto\" : \""+response.getEmpleado().getInfoContacto().getDireccionContacto()+"\",\n" +
                    "    \"ciudadContacto\" : \""+response.getEmpleado().getInfoContacto().getCiudadContacto()+"\",\n" +
                    "    \"telefonoContacto\" : \""+response.getEmpleado().getInfoContacto().getTelefonoContacto()+"\",\n" +
                    "    \"correoContacto\" : \""+response.getEmpleado().getInfoContacto().getCorreoElectronicoContacto()+"\",\n" +
                    "    \"codContrato\" : \""+response.getEmpleado().getVinculacion().get(numberOfId).getCabeceraContrato().getCodContrato()+"\",\n" +
                    "    \"tipoVinculacion\" : \""+response.getEmpleado().getVinculacion().get(numberOfId).getCabeceraContrato().getTipoVinculacion()+"\",\n" +
                    "    \"estadoAdministrativo\" : \""+response.getEmpleado().getVinculacion().get(numberOfId).getCabeceraContrato().getEstadoAdministrativo()+"\",\n" +
                    "    \"area\" : \""+response.getEmpleado().getVinculacion().get(numberOfId).getCabeceraContrato().getArea()+"\",\n" +
                    "    \"dependencia\" : \""+response.getEmpleado().getVinculacion().get(numberOfId).getCabeceraContrato().getDependencia()+"\",\n" +
                    "    \"valor\" : \""+response.getEmpleado().getVinculacion().get(numberOfId).getDetalleContrato().getAsignacionBasica().getValor()+"\",\n" +
                    "    \"idMoneda\" : \""+response.getEmpleado().getVinculacion().get(numberOfId).getDetalleContrato().getAsignacionBasica().getIdMoneda()+"\",\n" +
                    "    \"dedicacion\" : \""+response.getEmpleado().getVinculacion().get(numberOfId).getDetalleContrato().getDedicacion()+"\",\n" +
                    "    \"fechaInicio\" : \""+response.getEmpleado().getVinculacion().get(numberOfId).getDetalleContrato().getFechaInicio()+"\",\n" +
                    "    \"cargoEmpleado\" : \""+response.getEmpleado().getVinculacion().get(numberOfId).getDetalleContrato().getCargoEmpleado()+"\",\n" +
                    "    \"fechaFin\" : \""+response.getEmpleado().getVinculacion().get(numberOfId).getDetalleContrato().getFechaFin()+"\",\n" +
                    "    \"tipoPeriodo\" : \""+response.getEmpleado().getVinculacion().get(numberOfId).getDetalleContrato().getTipoPeriodo()+"\",\n" +
                    "    \"data\" : \""+data+"\",\n" +
                    "    \"externos\" : \""+""+"\"\n" +
                    "}";



            OutputStream os = conn.getOutputStream();
            os.write(jsonInputString.getBytes());
            os.flush();
            os.close();


            System.out.println(conn.getResponseCode());
            System.out.println(conn.getResponseMessage());

            if (conn.getResponseCode() != 200) {
                InputStreamReader in2 = new InputStreamReader(conn.getErrorStream());
                BufferedReader br2 = new BufferedReader(in2);
                String output2;
                System.out.println("ERROR: ");
                String ErrorOutput = "";
                while ((output2 = br2.readLine()) != null) {
                    System.out.println(output2);
                    ErrorOutput+=(" ; "+output2);
                }
                 throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());

            }else{
                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;

                while ((output = br.readLine()) != null) {
                    System.out.println(output);
                }
                conn.disconnect();
                return Either.right(1);
            }





        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }





}