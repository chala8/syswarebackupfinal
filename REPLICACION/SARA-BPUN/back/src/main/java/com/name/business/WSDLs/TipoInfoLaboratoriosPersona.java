
package com.name.business.WSDLs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Informaci�n de un participante de proyectos de investigaci�n o extensi�n para la universidad nacional de colombia
 * 
 * <p>Java class for tipoInfoLaboratoriosPersona complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tipoInfoLaboratoriosPersona">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Laboratorios" type="{http://interoperabilidad.unal.edu.co/WS/Schemas/Terceros}tipoInfoLaboratorio"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoInfoLaboratoriosPersona", propOrder = {
    "laboratorios"
})
public class TipoInfoLaboratoriosPersona {

    @XmlElement(name = "Laboratorios", required = true)
    protected TipoInfoLaboratorio laboratorios;

    /**
     * Gets the value of the laboratorios property.
     * 
     * @return
     *     possible object is
     *     {@link TipoInfoLaboratorio }
     *     
     */
    public TipoInfoLaboratorio getLaboratorios() {
        return laboratorios;
    }

    /**
     * Sets the value of the laboratorios property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoInfoLaboratorio }
     *     
     */
    public void setLaboratorios(TipoInfoLaboratorio value) {
        this.laboratorios = value;
    }

}
