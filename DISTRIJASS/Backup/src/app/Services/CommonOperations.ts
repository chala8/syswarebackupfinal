import {ElementRef, Injectable, NgZone} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Urlbase} from '../utils/urls';

import { Plugins, KeyboardInfo } from '@capacitor/core';

const { Keyboard } = Plugins;

@Injectable({
    providedIn:'root'
})
export class CommonOperations{
    private headers = new HttpHeaders({
        'Content-Type': 'application/json'
    });

    public FromLoginToCheckout = false;

    public mostrandoCargando = true;

    public ModalBuscadorProductos:HTMLIonModalElement = null;
    public ModalCarrito:HTMLIonModalElement = null;

    public ModalesLineas:Array<HTMLIonModalElement> = [];

    KeyboardH = 0;
    public MainScroll: ElementRef;

    constructor(
        private http: HttpClient,
        private ngZone:NgZone) {
        Keyboard.addListener('keyboardDidShow', (info: KeyboardInfo) => {
            ngZone.run(()=>{
                console.log('keyboard did show with height', info.keyboardHeight);
                this.KeyboardH = info.keyboardHeight;
            })
        });
        window.addEventListener('keyboardDidHide', () => {
            ngZone.run(()=>{
                console.log('keyboard did hide');
                this.KeyboardH = 0;
            })
        });
    }
}
