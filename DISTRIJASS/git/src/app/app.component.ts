import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

import {ModalController, Platform} from '@ionic/angular';
import { Plugins } from '@capacitor/core';
const { SplashScreen } = Plugins;
import {InventoriesService} from './Services/inventories.service';
import {CartComponent} from './common/cart/cart.component';
import {CommonOperations} from './Services/CommonOperations';
import { LocalStorage } from './shared/localStorage';
import { HttpClient } from '@angular/common/http';
import { Urlbase } from './utils/urls';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {

  @ViewChild('scrollMe',{static:true}) scrollElement: ElementRef;

  constructor(
    public inventoryService:InventoriesService,
    public modalController: ModalController,
    //private firebaseCrashlytics: FirebaseCrashlytics,
    public commonOperations:CommonOperations,
    public locStorage: LocalStorage,
    public http: HttpClient
  ) {
    this.initializeApp();
  }

  initializeApp() {
    SplashScreen.hide();
    try {
      // const crashlytics = this.firebaseCrashlytics.initialise();
      // crashlytics.logException('my caught exception');
    }catch (e) {
      console.log("crashlytics")
      console.log(e)
    }
  }

  ngOnInit() {
    this.http.post(Urlbase.facturacion+"/pedidos/contadorAppInstaladas?deviceid='2.0.0.0'&idapp=26&appversion=1",{}).subscribe(response => {
      console.log("device counter:", response)
    });
    console.log("this.scrollElement",this.scrollElement)
    this.commonOperations.MainScroll = this.scrollElement;
  }

  roundnum(num) {
    //return Math.round(num / 50) * 50;
    return Math.round(num);
  }


  async ClickCarrito() {
    console.log("[ClickCarrito]this.inventoryService.CarrosAbiertos es ",this.inventoryService.CarrosAbiertos)
    if(this.inventoryService.CarrosAbiertos > 0){
      this.inventoryService.RefCarrito.ClickSiguiente();
    }else{
      if(this.commonOperations.ModalCarrito != null){await this.commonOperations.ModalCarrito.dismiss();}
      const modal = await this.modalController.create({
        component: CartComponent,
        cssClass: 'my-custom-modal-css',
      });
      this.commonOperations.ModalCarrito = modal;
      modal.onDidDismiss().then(value => {
        this.commonOperations.ModalCarrito = null;
      })
      await modal.present();
    }
  }
}
