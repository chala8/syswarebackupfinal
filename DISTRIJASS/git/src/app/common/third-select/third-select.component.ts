import { Component, OnInit } from '@angular/core';
import {AlertController, LoadingController, ModalController, Platform, PopoverController, IonButton} from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Urlbase } from '../../utils/urls';
import { LocalStorage } from 'src/app/shared/localStorage';
import { AccountService } from 'src/app/Services/AccountService';


@Component({
  selector: 'app-third-select',
  templateUrl: './third-select.component.html',
  styleUrls: ['./third-select.component.scss'],
})
export class ThirdSelectComponent implements OnInit {

  constructor(public alertController: AlertController,
              public http:HttpClient,
              public modalController: ModalController,
              public locStorage: LocalStorage,
              public accountService:AccountService) { }

  vendorList

  ngOnInit() {
    this.locStorage.setButtonOn(true);

    console.log("third: ",this.accountService.usuarioCompleto.third.id_third);

    this.http.get(Urlbase.tienda+"/store/vendors?id_third="+this.accountService.usuarioCompleto.third.id_third).subscribe(data => {

      this.vendorList = data;
      console.log("this is data",data);
    
    })


  }

  defaultElement= {vendedor: "Ninguno", idthirdvendedor: -1};


  setVendor(vendor){
    this.locStorage.setSelectedIdThird(vendor.idthirdvendedor);
    this.locStorage.setSelectedThird(vendor.vendedor);
  }

  CerrarModal(){

      this.locStorage.setButtonOn(false);
    this.modalController.dismiss({next: false});
    
  }

  async CloseButtonSelected(){
    if(this.locStorage.getSelectedIdThird()!=0){
      this.locStorage.setButtonOn(false); 
      this.modalController.dismiss({next: true});
    }else{
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Atención',
        message: 'Debes ielegir un vendedor para continuar',
        buttons: [
          {
            text: 'Volver',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }
        ]
      });

      await alert.present();

    }

  }

}
