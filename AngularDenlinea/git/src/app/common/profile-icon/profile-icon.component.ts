import {Component, OnInit} from '@angular/core';
import {AccountService} from "../../servicios/AccountService";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoginComponent} from "../login/login.component";
import {CommonOperations} from "../../servicios/CommonOperations";
import {ToastrService} from "ngx-toastr";
import {MatDialog} from "@angular/material/dialog";
import {BillingService, Pedidos} from "../../servicios/billing.service";
import {OrderViewerComponent} from "../order-viewer/order-viewer.component";
import * as moment from 'moment';

@Component({
  selector: 'app-profile-icon',
  templateUrl: './profile-icon.component.html',
  styleUrls: ['./profile-icon.component.scss']
})
export class ProfileIconComponent implements OnInit {

  public loginForm: FormGroup;
  MostrarClaveLogin = false;

  LogueadoSeccion = 0;

  VariableSuscriptor:any = null;


  constructor(
    public accountService:AccountService,
    public formBuilder: FormBuilder,
    public billingService : BillingService,
    public commonOperations:CommonOperations,
    private toastr: ToastrService,
    public dialog: MatDialog,
  ) {
    this.loginForm = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.VariableSuscriptor = this.accountService.LogStatusChange.subscribe((status:{status:string}) => {
      console.log("Se ejecuta el update de LogStatusChange")
      if(this.accountService.usuarioCompleto != null){
        this.ActualizarActivos(null);
        this.ActualizarFinalizadas(null);
      }
    });
  }

  ngOnInit(): void {
    if(this.accountService.usuarioCompleto != null){
      this.ActualizarActivos(null);
      this.ActualizarFinalizadas(null);
    }
  }

  async ActualizarActivos(evento){
    this.billingService.ListadoPedidosActivos = [];
    await this.billingService.GetActiveOrderList(this.accountService.usuarioCompleto.third.id_third);
    if(evento != null){evento.target.complete();}
  }

  async ActualizarFinalizadas(evento){
    await this.billingService.GetEndedOrderList(this.accountService.usuarioCompleto.third.id_third);
    if(evento != null){evento.target.complete();}
  }

  ClickMisPedidos(){
    this.LogueadoSeccion = 1;
  }

  ClickPerfil(){
    this.LogueadoSeccion = 0;
    this.accountService.MostrandoProfileIcon = !this.accountService.MostrandoProfileIcon;
  }

  async Login(){
    this.commonOperations.mostrandoCargando = true;
    try {
      await this.accountService.loginUser(this.loginForm.controls.username.value,this.loginForm.controls.password.value);
      this.toastr.success("Bienvenido, "+this.accountService.GetNormalName()+"")
      this.accountService.MostrandoProfileIcon = false;
    }catch (e) {
      console.log("e",e)
      this.toastr.error("Verifique sus credenciales",null,{disableTimeOut:true})
    }
    this.commonOperations.mostrandoCargando = false;
  }

  async ClickCerrarSesion(){
    try {
      this.commonOperations.mostrandoCargando = true;
      await this.accountService.ClearUserData();
      this.toastr.success("Sesión Cerrada");
      this.accountService.MostrandoProfileIcon = false;
    }catch (e) {
      this.toastr.error("Ocurrió un error inesperado, verifique su conexión");
    }
    this.commonOperations.mostrandoCargando = false;
  }

  GotoSignIn(){
    if(this.commonOperations.LoginModal != null){return;}
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '90vw',
      height: '90vh',
      panelClass: 'custom-mat-dialog-container-large_full_page',
      data:{
        checkout:false
      }
    });
    this.commonOperations.LoginModal = dialogRef;
    dialogRef.afterClosed().subscribe(result => {
      this.commonOperations.LoginModal = null;
      if(result != null && result == "login"){
        this.toastr.success("Bienvenido, "+this.accountService.GetNormalName()+"");
      }
    });
  }

  BillStateToText(id:number){
    switch (id) {
      case 801: return "Recibido";
      case 807: return "Procesado Con Novedad";
      case 802: return "Procesado";
      case 803: return "En Camino";
      case 808: return "Entregado Con Novedad";
      case 804: return "Entregado";
      case 902: return "Alistando el pedido"
      case 705: return "Calificado";
      case 99: return "Cancelado";
      default: return id;
    }
  }

  FormatedDate(fecha:Date){
    return moment(fecha).format("YYYY-MM-DD hh:mm a");
  }

  async ClickOrder(pedido:Pedidos,From:number){//from 0 es normal, from 1 es repetir, from 2 es calificar
    if(this.commonOperations.OrderViewerModal != null){return;}
    this.LogueadoSeccion = 0;
    this.accountService.MostrandoProfileIcon = false;
    this.commonOperations.OrderViewerModal = this.dialog.open(OrderViewerComponent, {
      width: '90vw',
      height: '90vh',
      panelClass: 'custom-mat-dialog-container-large',
      data:{
        From:0,
        Pedido:pedido
      }

    });
    this.commonOperations.OrderViewerModal.afterClosed().subscribe(result => {
      this.commonOperations.OrderViewerModal = null;
    });
    // const modal = await this.modalController.create({
    //   component: OrderViewerComponent,
    //   componentProps:{
    //     From:From,
    //     Pedido:pedido,
    //   }
    // });
    // return await modal.present();
  }
}
