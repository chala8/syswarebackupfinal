import { Component, OnInit } from '@angular/core';
import {InventoryName, ProductoTablaDTO} from "../../servicios/cart.service";
import {CommonOperations} from "../../servicios/CommonOperations";
import {InventoriesService} from "../../servicios/inventories.service";
import {ToastrService} from "ngx-toastr";
import {MatDialog} from "@angular/material/dialog";
import {LoginComponent} from "../login/login.component";
import {AccountService} from "../../servicios/AccountService";
import {CheckoutComponent} from "../checkout/checkout.component";
import {BillingService} from "../../servicios/billing.service";
import { LocalStorage } from 'src/app/shared/localStorage';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  constructor(
    public locStorage: LocalStorage,
    public inventoryService:InventoriesService,
    public commonOperations:CommonOperations,
    public billingService:BillingService,
    public dialog: MatDialog,
    public accountService:AccountService
  ) { }

  ngOnInit(): void {
  }

  focusCarrito(evento){
    console.log(evento)
  }

  // GetItemPrice(item:ProductoTablaDTO){
  //   return this.roundnum(item.price + (item.price*this.inventoryService.getPercentTax(item.tax_product)));
  // }
  
  GetItemPrice2(item:InventoryName){
    let valor = item.price + (item.price*this.inventoryService.getPercentTax(item.id_TAX));
    //Ajuste a 50 superior
    let modOpt = valor % 50;
    if(modOpt > 0){
      valor += 50 - modOpt;
    }
    return valor;
  }

  GetItemPrice(item){
    if(this.locStorage.getidCountry()=="ES"){
      return (item.price + (item.price*this.inventoryService.getPercentTax(item.id_TAX))).toFixed(2);
    }else{
      return this.GetItemPrice2(item);
    }
  }

  BlurInputCarrito(evento){
    setTimeout(()=>{
      let encontro = false;
      for(let n = 0;n<this.inventoryService.cartService.ListaTablaProductosSeleccionados.length;n++){
        let producto = this.inventoryService.cartService.ListaTablaProductosSeleccionados[n];
        let quantityINT = parseInt(producto.quantity+"");
        if(isNaN(quantityINT)){quantityINT = 1;}
        producto.quantity = quantityINT;
        if(producto.quantity == null || producto.quantity < 1){
          producto.quantity = 1;
          encontro = true;
        }
      }
      if(encontro){this.inventoryService.cartService.ActualizarTabla()}
    },100);
  }

  DesdeBotonesLaterales = false;
  AjustarCantidadInput(evento,Producto:ProductoTablaDTO){
    console.log("evento es ",evento)
    setTimeout(()=>{console.log("Producto es ",Producto);if(Producto.quantity < 0){Producto.quantity = 1;}},100);
    if(evento.detail == null){return;}
    if(this.DesdeBotonesLaterales == true){
      this.DesdeBotonesLaterales = false;
      return;
    }
    let cantidad = evento.detail.value;
    if(cantidad == ""){return;}
    cantidad = parseInt(cantidad);
    if(cantidad < 1){
      return;
    }
    this.inventoryService.cartService.ActualizarTabla()
  }

  roundnum(num) {
    //return Math.round(num / 50) * 50;
    return Math.round(num);
  }

  ClickSubir(Producto:ProductoTablaDTO){
    this.DesdeBotonesLaterales = true;
    Producto.quantity += 1;
    this.inventoryService.cartService.ActualizarTabla()
  }

  ClickBajar(Producto:ProductoTablaDTO){
    this.DesdeBotonesLaterales = true;
    Producto.quantity -= 1;
    if(Producto.quantity < 1){
      //this.EliminarProducto(Producto);
      Producto.quantity = 1;
    }else{
      this.inventoryService.cartService.ActualizarTabla()
    }
  }

  ClickBorrar(Producto:ProductoTablaDTO){
    this.EliminarProducto(Producto);
    if(this.inventoryService.cartService.ListaTablaProductosSeleccionados.length == 0){
      this.CerrarModal();
    }
  }

  async EliminarProducto(Producto:ProductoTablaDTO){
    await this.inventoryService.cartService.EliminarProductoPorParametro(Producto);
  }

  CerrarModal(){
    this.inventoryService.CarritoEnFinal = false;
    this.commonOperations.CartModal.close();
    this.commonOperations.CartModal = null;
  }

  async CompletarPedido(){
    this.commonOperations.mostrandoCargando = true;
    let logueado = false;
    try {
      await this.accountService.IsLogued()
      logueado = true;
    }catch (e) {
      logueado = false;
    }
    if(logueado && this.accountService.usuarioCompleto != null && this.accountService.usuarioCompleto.uuid != ""){//Logueado
      console.log("Logueado")
      this.IrACheckout();
    }else{//No logueado
      console.log("No Logueado")
      if(this.commonOperations.LoginModal != null || this.inventoryService.cartService.ListaTablaProductosSeleccionados.length == 0){return;}
      const dialogRef = this.dialog.open(LoginComponent, {
        width: '90vw',
        height: '90vh',
        panelClass: 'custom-mat-dialog-container-large_full_page'
      });
      this.commonOperations.LoginModal = dialogRef;
      dialogRef.afterClosed().subscribe(result => {
        this.commonOperations.LoginModal = null;
        if(result != null && result == "login"){
          this.IrACheckout();
        }
      });
    }
    this.commonOperations.mostrandoCargando = false;
  }

  IrACheckout(){
    this.CerrarModal();
    if(this.commonOperations.CheckoutModal != null || this.inventoryService.cartService.ListaTablaProductosSeleccionados.length == 0){return;}
    if(this.commonOperations.IsMobile()){
      const dialogRef = this.dialog.open(CheckoutComponent, {
        width: '100vw',
        height: 'calc(var(--vh, 1vh) * 100)',
        panelClass: 'custom-mat-dialog-container-large_full_page',
        maxHeight:'calc(var(--vh, 1vh) * 100)',
        maxWidth:'100vw'
      });
      this.commonOperations.CheckoutModal = dialogRef;
    }else{
      const dialogRef = this.dialog.open(CheckoutComponent, {
        width: '100vw',
        height: '100vh',
        panelClass: 'custom-mat-dialog-container-large_full_page',
        maxHeight:'100vh',
        maxWidth:'100vw'
      });
      this.commonOperations.CheckoutModal = dialogRef;
    }
  }
}
