import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {InventoriesService} from "../../servicios/inventories.service";
import {CommonOperations} from "../../servicios/CommonOperations";
import {AccountService} from "../../servicios/AccountService";
import {HereMapComponent} from "../here-map/here-map.component";
import {Urlbase} from "../../utils/urls";
import {HttpClient} from "@angular/common/http";
import {BillingService} from "../../servicios/billing.service";
import {ToastrService} from "ngx-toastr";
import {FCMService} from "../../servicios/fcm.service";
import {ProductoTablaDTO} from "../../servicios/cart.service";
import { LocalStorage } from 'src/app/shared/localStorage';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  @ViewChild('ElMap', {static: false}) private ElMap: HereMapComponent;

  @ViewChild('TheScroll') public TheScroll:ElementRef;
  CargandoMapa = true;
  MetodosPago = [
    ["0","Efectivo",1],
    ["1","Tarjeta de Crédito (Datáfono)",2],
    ["2","Tarjeta de Débito (Datáfono)",3]
  ]
  MetodoPagoSeleccionado = "0";

  constructor(
    public locStorage: LocalStorage,
    public inventoryService:InventoriesService,
    public commonOperations:CommonOperations,
    public accountService:AccountService,
    private http:HttpClient,
    public billingService:BillingService,
    private toastr:ToastrService,
    private fcmService:FCMService
    ) {
    //inventoryService.cartService.ListaTablaProductosSeleccionados = JSON.parse(localStorage.getItem("tempLIST"))
  }

  Distancia = -1;
  ngOnInit(): void {
    setTimeout(() => {
      this.TheScroll.nativeElement.scrollTop = 0;
    },100);//
    setTimeout(async ()=>{
      this.CargandoMapa = true;
      await this.ElMap.ReturnWhenReady();
      this.ElMap.ClearMarkers();
      this.ElMap.SetMarker({lat:this.accountService.usuarioCompleto.latitud,lng:this.accountService.usuarioCompleto.longitud},0)
      this.ElMap.SetMarker({lat:this.billingService.ProviderInfo.Lat,lng:this.billingService.ProviderInfo.Lng},0)
      this.ElMap.CentrarMapaEnMarcadores();
      setTimeout(() => {
        this.TheScroll.nativeElement.scrollTop = 0;
      },100);//
      //Mostrar Ruta
      this.Distancia = await this.ElMap.SetRoute(this.accountService.usuarioCompleto.latitud,this.accountService.usuarioCompleto.longitud,this.billingService.ProviderInfo.Lat,this.billingService.ProviderInfo.Lng);
      setTimeout(() => {
        this.TheScroll.nativeElement.scrollTop = 0;
      },100);//
      // this.ElMap.Reload();
      // this.ElMap.SetCenter(
      //   {
      //     lat:this.accountService.usuarioCompleto.latitud,
      //     lng:this.accountService.usuarioCompleto.longitud
      //   });
      this.CargandoMapa = false;
    }, 300);
  }

  roundnum(num) {
    //return Math.round(num / 50) * 50;
    return Math.round(num);
  }

  CerrarModal(){
    this.commonOperations.CheckoutModal.close();
    this.commonOperations.CheckoutModal = null;
  }

  // GetItemPrice(item:ProductoTablaDTO){
  //   return this.roundnum(item.price + (item.price*this.inventoryService.getPercentTax(item.tax_product)));
  // }

  GetItemPrice2(item:ProductoTablaDTO){
    let valor = item['price'] + (item['price']*item['tax']);
    //Ajuste a 50 superior
    let modOpt = valor % 50;
    if(modOpt > 0){
      valor += 50 - modOpt;
    }
    return valor;
  }

  GetItemPrice(item){
    if(this.locStorage.getidCountry()=="ES"){
      return (item['price'] + (item['price']*item['tax'])).toFixed(2);
    }else{
       return this.GetItemPrice2(item);
    }
  }

  CompletarPedido(){
    //Muestro loading
    this.commonOperations.mostrandoCargando = true;
    //Postear pedido
    let detailList = '';
    //GENERO LA LISTA DE DTOs DE DETALLES
    this.inventoryService.cartService.ListaTablaProductosSeleccionados.forEach(item => {
      console.log(item);
      detailList = detailList+ "{"+item.id_product_third+","+(item.price-(item.price*(0)/100))+","+item.tax_product+","+item.quantity+"},"
    });
    this.http.post(Urlbase.facturacion+ "/pedidos/crearPedidoApp?idapp=40&idstoreclient=12&idthirduseraapp="+this.accountService.usuarioCompleto.third.id_third+"&idstoreprov="+this.billingService.ProviderInfo.StoreID+"&detallepedido="+detailList.substring(0, detailList.length - 1)+"&descuento="+0,{}).subscribe(async (item) => {
      if(item==1){
        this.toastr.success("Pedido creado con exito");
        //this.CerrarModal("Algo");
        this.inventoryService.CarritoEnFinal = false;
        this.commonOperations.CheckoutModal.close();
        this.commonOperations.CheckoutModal = null;
        this.inventoryService.cartService.LimpiarCarrito();
        await this.billingService.GetActiveOrderList(this.accountService.usuarioCompleto.third.id_third);
        let lastOrder = this.billingService.ListadoPedidosActivos[0];
        this.fcmService.SendFCMpushFromClient("Orden '"+lastOrder.numdocumento+"' creada","El usuario '"+this.accountService.GetFullName()+"' ha creado una orden",
          {
            id_bill:lastOrder.id_BILL,stateC:701,stateP:801,prevState:lastOrder.id_BILL_STATE
          },this.billingService.ProviderInfo.StoreID,"");
        //const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
        this.http.get(Urlbase.facturacion+"/billing/UniversalPDF?id_bill="+lastOrder.id_BILL+"&pdf=1",{responseType: 'text' as 'json'}).subscribe(value => {
          console.log("[UniversalPDF] "+value)
        },error => {
          console.log("[UniversalPDF] ERROR: "+error)
        })
      }else{
        this.toastr.error('Se presento un error al crear el pedido.',null,{closeButton:true,disableTimeOut:true})
      }
    },async (error) => {
      this.toastr.error('Se presento un error al crear el pedido.',null,{closeButton:true,disableTimeOut:true});
    })
    this.commonOperations.mostrandoCargando = false;
  }
}
