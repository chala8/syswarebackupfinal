import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProfileIconComponent} from "./profile-icon/profile-icon.component";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatTabsModule} from "@angular/material/tabs";



@NgModule({
  declarations: [
    ProfileIconComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatTabsModule
  ],
  exports:[
    ProfileIconComponent
  ]
})
export class CommonDefinitionsModule { }
