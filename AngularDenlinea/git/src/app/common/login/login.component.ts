import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {CommonOperations} from "../../servicios/CommonOperations";
import {MatDialog} from "@angular/material/dialog";
import {AccountService} from "../../servicios/AccountService";
import {ToastrService} from "ngx-toastr";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Urlbase} from "../../utils/urls";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {HereMapComponent} from "../here-map/here-map.component";
import {MatSelect} from "@angular/material/select";
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {BillingService} from "../../servicios/billing.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @ViewChild('ElMap', {static: false}) private ElMap: HereMapComponent;
  @ViewChild('streetType', {static: true}) private streetType: MatSelect;

  MostrarClaveLogin = false;
  MostrarClaveRegistro = false;
  BusquedaAddress = false;

  FaseRegistro:number = 1;
  cityList:Array<{city_NAME:string,id_CITY:string}> = [];

  GoToCheckout = true;

  public loginForm: FormGroup;
  public signinForm: FormGroup;
  public signinFormPart2: FormGroup;

  constructor(
    public commonOperations:CommonOperations,
    public dialog: MatDialog,
    public accountService:AccountService,
    private toastr: ToastrService,
    public billingService:BillingService,
    public formBuilder: FormBuilder,
    private http:HttpClient,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if(data != null && data.checkout != null){
      this.GoToCheckout = data.checkout;
      this.GoToCheckout = this.GoToCheckout != null ? this.GoToCheckout:true;
    }
    this.loginForm = formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.signinForm = formBuilder.group({
      email: ['', Validators.email],
      password: ['', Validators.required],
      doctype: ['', Validators.required],
      doc: ['', Validators.required],
      names: ['', Validators.required],
      lastnames: [''],
      phone: ['', Validators.required],
    });
    this.signinFormPart2 = formBuilder.group({
      city: ['', Validators.required],
      street: ['', Validators.required],
      streetNumber1: ['', Validators.required],
      streetNumber2: ['', Validators.required],
      streetNumber3: ['', Validators.required],
      streetOptinalInfo: ['']
    });
    //default values
    this.signinForm.controls.doctype.setValue(3+"");
    //
    this.signinFormPart2.controls.street.disable();
    this.signinFormPart2.controls.streetNumber1.disable();
    this.signinFormPart2.controls.streetNumber2.disable();
    this.signinFormPart2.controls.streetNumber3.disable();
    this.signinFormPart2.controls.streetOptinalInfo.disable();
  }

  ngOnInit(): void {
    this.getCitylist();
  }

  CerrarModal(){
    this.commonOperations.LoginModal.close();
    this.commonOperations.LoginModal = null;
  }

  getCitylist(){
    this.http.get<any[]>(Urlbase.tercero+"/thirds/cities",{ headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': '3020D4:0DD-2F413E82B-A1EF04559-78CA',
        'Key_Server': 'FE651467B48D552C2EFBC8B13EBA9',
      }) }).subscribe(response => {
      this.cityList = response;
    });
  }

  async Login(){
    this.commonOperations.mostrandoCargando = true;
    try {
      await this.accountService.loginUser(this.loginForm.controls.username.value,this.loginForm.controls.password.value);
      this.toastr.success("Bienvenido, "+this.accountService.GetNormalName()+"")
      this.commonOperations.LoginModal.close("login");
      this.commonOperations.LoginModal = null;
    }catch (e) {
      console.log("e",e)
      this.toastr.error("Verifique sus credenciales",null,{disableTimeOut:true})
    }
    this.commonOperations.mostrandoCargando = false;
  }

  ReturnFullAddress(withCity,withComplement = true){
    let city = "";
    if(withCity){
      for(let n = 0;n<this.cityList.length;n++){
        if(this.signinFormPart2.controls.city.value == this.cityList[n].id_CITY){
          city = this.cityList[n].city_NAME;
          break;
        }
      }
    }
    let ajusteOpcional = "";
    if(this.signinFormPart2.controls.streetOptinalInfo.value != "" && withComplement){
      ajusteOpcional = "| "+this.signinFormPart2.controls.streetOptinalInfo.value;
    }
    return (this.signinFormPart2.controls.street.value+" "+
      this.signinFormPart2.controls.streetNumber1.value+" # "+
      this.signinFormPart2.controls.streetNumber2.value+" "+
      this.signinFormPart2.controls.streetNumber3.value +" "
      + city + ajusteOpcional );
  }

  async SignIn(){
    this.commonOperations.mostrandoCargando = true;
    let faseregistro = 0;
    try {
      if(!this.signinForm.valid){
        throw "Complete todos los datos";
      }
      faseregistro = 1;
      this.accountService.PreSignUp = {
        docType:this.signinForm.controls.doctype.value,
        document:this.signinForm.controls.doc.value,
        email:this.signinForm.controls.email.value,
        lastnames:this.signinForm.controls.lastnames.value,
        names:this.signinForm.controls.names.value,
        password:this.signinForm.controls.password.value,
        phone:this.signinForm.controls.phone.value
      }
      let resultado = await this.accountService.checkUser(this.accountService.PreSignUp.email);
      if(resultado.result){throw "Usuario en uso"}
      faseregistro = 2;
      resultado = await this.accountService.checkPerson(this.accountService.PreSignUp.document);
      if(resultado.response != "Not Found"){throw "Documento en uso"}
      this.FaseRegistro = 2;
    }catch (e) {
      console.log("e",e)
      if(faseregistro == 1){
        this.toastr.error("Email invalido o en uso")
      }else if(faseregistro == 2){
        this.toastr.error("Documento invalido o en uso")
      }else{
        this.toastr.error("Verifique los datos ingresados")
      }
    }
    this.commonOperations.mostrandoCargando = false;
  }

  async SignInPart2(){
    this.commonOperations.mostrandoCargando = true;
    try {
      if(!this.signinFormPart2.valid){
        throw "Complete todos los datos";
      }
      //
      let centro:{lat:number,lng:number} = this.ElMap.GetCenter();
      let Nombres = this.signinForm.controls.names.value.split(" ");
      Nombres[0] = this.titleCaseWord(Nombres[0]);
      if(Nombres[1] != null){Nombres[1] = this.titleCaseWord(Nombres[1]);}
      let Apellidos = this.signinForm.controls.lastnames.value.split(" ");
      Apellidos[0] = this.titleCaseWord(Apellidos[0]);
      if(Apellidos[1] != null){Apellidos[1] = this.titleCaseWord(Apellidos[1]);}
      let bodyPosteo = {
        usuario:this.signinForm.controls.email.value,
        clave:this.signinForm.controls.password.value,
        appid:40,
        third:{
          firstname:Nombres[0],
          secondname:Nombres[1] || "",
          firstlastname:Apellidos[0],
          secondlastname:Apellidos[1] || "",
          iddocumenttype:this.signinForm.controls.doctype.value,
          docnumber:this.signinForm.controls.doc.value,
          direccion:this.ReturnFullAddress(false),//le concatenos las coordenadas
          idcity:this.signinFormPart2.controls.city.value,
          telefono:this.signinForm.controls.phone.value,
          email:this.signinForm.controls.email.value,
          // birthDate:this.signinForm.controls.birthDate.value,
          birthDate: new Date(),
          // gender:this.signinForm.controls.gender.value,
          gender:1,
          lat:centro.lat,
          lng:centro.lng
        }
      };
      //
      await this.accountService.createUser(bodyPosteo);
      //
      this.toastr.success("Cuenta creada")
      let errorLogin = false;
      //me logueo en ella
      try {
        await this.accountService.loginUser(this.signinForm.controls.email.value,this.signinForm.controls.password.value);
        errorLogin = false;
      }catch (e) {
        errorLogin = true;
        console.log("e",e)
        this.toastr.error("Error al continuar con la cuenta creada, intenta iniciar sesión en ella",null,{disableTimeOut:true})
      }
      this.loginForm.controls.username.setValue(this.signinForm.controls.email.value);
      this.loginForm.controls.password.setValue(this.signinForm.controls.password.value);
      this.signinForm.reset();
      this.signinFormPart2.reset();
      this.FaseRegistro = 1;
      if(errorLogin){
        //algo pasó al loguearse con la cuenta recién creada
      }else{
        //continuar al checkout
        this.commonOperations.LoginModal.close("login");
        this.commonOperations.LoginModal = null;
      }
    }catch (e) {
      console.log("e",e)
      this.toastr.error("Error el completar registro: "+e)
    }
    this.commonOperations.mostrandoCargando = false;
  }

  titleCaseWord(word: string) {
    if (!word) return word;
    return word[0].toUpperCase() + word.substr(1).toLowerCase();
  }

  async BuscarAddress(){
    try {
      this.commonOperations.mostrandoCargando = true;
      console.log(this.ReturnFullAddress(true,false))
      let resultado = await this.ElMap.SearchAddress(this.ReturnFullAddress(true,false));
      this.commonOperations.mostrandoCargando = false;
      if(resultado[0]){
        if(resultado[1].length == 0){//No encontró resultados
          this.toastr.error("No se encontraron resultados, intenta especificar mas datos, como la ciudad.",null,{disableTimeOut:true,closeButton:true})
        }else{//Encontró algo
          this.BusquedaAddress = true;
          this.toastr.success("Centrando mapa");
          this.ElMap.SetCenter(
            {
              lat:resultado[1][0].result[0].location.displayPosition.latitude,
              lng:resultado[1][0].result[0].location.displayPosition.longitude
            });
          this.toastr.info("Si no logramos ubicarte bien, por favor, mueve el mapa para que el punto rojo quede donde estas realmente ubicado");
        }
      }else{
        this.toastr.error("No se pudo obtener la ubicación, revisa tu conexión e intenta de nuevo.",null,{disableTimeOut:true,closeButton:true})
        console.log(resultado[1])
      }
      console.log(this.ElMap.GetCenter())
    }catch (e) {
      this.commonOperations.mostrandoCargando = false;
      this.toastr.error("Error desconocido:.",e.toString())
      console.log(e.toString())
    }
  }
}
