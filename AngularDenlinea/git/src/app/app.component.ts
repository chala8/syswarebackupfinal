import { Component } from '@angular/core';
import 'hammerjs';
import {MatIconRegistry} from "@angular/material/icon";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Denlinea';

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
  ) {
    let vh = window.innerHeight * 0.01;
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty('--vh', `${vh}px`);
    //
    window.addEventListener('resize', () => {
      // We execute the same script as before
      let vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);
    });
    //
    this.matIconRegistry.addSvgIcon("IconoBolsa",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/iconosSVG/IconoBolsa.svg")
    );
    this.matIconRegistry.addSvgIcon("IconoPerfil",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/iconosSVG/IconoPerfil.svg")
    );
  }
}
