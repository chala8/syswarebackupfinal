import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Urlbase} from '../utils/urls';
import {Router} from '@angular/router';
import {Subject} from "rxjs";
import {CommonOperations} from "./CommonOperations";

@Injectable({
    providedIn:'root'
})
export class AccountService{
    private headers = new HttpHeaders({
        'Content-Type': 'application/json'
    })

    LogStatusChange: Subject<{status:string}> = new Subject<{status:"-1"}>();

    public MostrandoProfileIcon = false;

    constructor(private http: HttpClient, private router: Router,private commonOperations:CommonOperations) {
      commonOperations.accountService = this;
        if(localStorage.getItem("FullUser") != null && localStorage.getItem("FullUser") != "" && localStorage.getItem("FullUser") != "null"){
            this.usuarioCompleto = JSON.parse(localStorage.getItem("FullUser"))
        }
        if(this.usuarioCompleto == null || this.usuarioCompleto.third == null || this.usuarioCompleto.third.profile == null){
            localStorage.setItem("RouterHome","false");
            localStorage.setItem("FullUser",null);
            localStorage.setItem("LastCheckClosestProv",null);
            localStorage.setItem("TiendaProv",null);
        }
    }

    usuarioCompleto : {
        uuid:string,
        roles: Array<{}>,
        third: {
            id_third:number,
            id_third_father:number,
            profile:{
                id_person: number,
                first_name: string,
                second_name: string,
                first_lastname: string,
                second_lastname: string,
                birthday: Date,
                info:{
                    id_common_basicinfo: number,
                    id_document_type: number,
                    fullname: string,
                    img: string,
                    document_number: string,
                    type_document: string
                },
                directory:number
            },
            directory:number
        },
        address:string,
        phone:string,
        email:string,
        genero:string,
        latitud:string,
        longitud:string,
        city_ID:number,
        city_NAME:string
    } = null;

    public PreSignUp:{
      names:string,
      lastnames:string,
      docType:number,
      document:string,
      email:string,
      phone:string,
      password:string,
    }

    GetFullName(){
      if(this.usuarioCompleto == null){return ""}
      let salida = [];
      let name1 = this.usuarioCompleto.third.profile.first_name;
      let name2 = this.usuarioCompleto.third.profile.second_name;
      let name3 = this.usuarioCompleto.third.profile.first_lastname;
      let name4 = this.usuarioCompleto.third.profile.second_lastname;
      if(name1 != null && name1 != ""){salida.push(name1);}
      if(name2 != null && name2 != ""){salida.push(name2);}
      if(name3 != null && name3 != ""){salida.push(name3);}
      if(name4 != null && name4 != ""){salida.push(name4);}
      return salida.join(" ");
    }

    GetNormalName(){
      if(this.usuarioCompleto == null){return ""}
      let salida = [];
      let name1 = this.usuarioCompleto.third.profile.first_name;
      let name3 = this.usuarioCompleto.third.profile.first_lastname;
      if(name1 != null && name1 != ""){salida.push(name1);}
      if(name3 != null && name3 != ""){salida.push(name3);}
      return salida.join(" ");
    }

    async IsLogued():Promise<boolean>{
        return new Promise<boolean>((resolve,reject) => {
            // if(localStorage.getItem("CurrentSession") == "-1" || localStorage.getItem("CurrentSession") == null){
            //     resolve(false);
            // }else{
                //petición al servidor para verificar sesión
                this.http.post(Urlbase.auth +"/CheckSession",null).subscribe(value => {
                    resolve(true);
                },error => {
                    console.log(error);
                    reject(false);
                })
            // }
        });
    }

    async createUser(bodyPosteo:{}):Promise<any>{
        return new Promise<any>((resolve,reject) => {
            this.http.post(Urlbase.auth+"/CreateClient",bodyPosteo).subscribe(value => {
                resolve(value);
            }, error => {
              reject(error);
            })
        });
    }

    async MetodoLogin(usuario:string,clave:string,forceHeader = false):Promise<any>{
      return new Promise((resolve,reject) => {
        this.http.post(Urlbase.auth+"/loginClient?id_aplicacion=40",{usuario:usuario,clave:clave,forceHeader:forceHeader}).subscribe(value => {
          if(this.usuarioCompleto == null){ this.usuarioCompleto = {uuid:"",roles:[],third:null,address:"",email:"",phone:"",genero:"",latitud:"",longitud:"",city_ID:-1,city_NAME:""}}
          // @ts-ignore
          this.usuarioCompleto.uuid = value.uuid;
          // @ts-ignore
          this.usuarioCompleto.roles = value.roles;
          // @ts-ignore
          this.usuarioCompleto.third = value.third[0];
          // @ts-ignore
          this.usuarioCompleto.address = value.address;
          // @ts-ignore
          this.usuarioCompleto.email = value.email;
          // @ts-ignore
          this.usuarioCompleto.phone = value.phone;
          // @ts-ignore
          this.usuarioCompleto.genero = value.genero;
          // @ts-ignore
          this.usuarioCompleto.latitud = value.latitud;
          // @ts-ignore
          this.usuarioCompleto.longitud = value.longitud;
          // @ts-ignore
          this.usuarioCompleto.city_ID = value.city_ID;
          // @ts-ignore
          this.usuarioCompleto.city_NAME = value.city_NAME;
          localStorage.setItem("FullUser",JSON.stringify(this.usuarioCompleto));
          // @ts-ignore
          localStorage.setItem("session_header",value.authheader);
          resolve(value);
        }, error => {
          this.usuarioCompleto = null;
          reject(error);
        })
      });
    }


  MacProblemCheck = false;

  async loginUser(usuario:string,clave:string):Promise<any>{
    return new Promise(async (resolve,reject) => {
      this.MacProblemCheck = false;
      try {
        let retorno = await this.MetodoLogin(usuario,clave);
        this.MacProblemCheck = true;
        await this.IsLogued();//Si falla esto, se intenta de nuevo el login por el segundo metodo
        this.MacProblemCheck = false;
        this.LogStatusChange.next({status:"login"});
        resolve(retorno);
      }catch (e) {
        if(this.MacProblemCheck){
          let retorno = await this.MetodoLogin(usuario,clave,true);
          this.MacProblemCheck = false;
          resolve(retorno);
        }else{
          this.MacProblemCheck = false;
          reject(e);
        }
      }
    });
  }

    async checkUser(usuario:string):Promise<any>{
        return await new Promise<any>((resolve,reject) => {
            this.http.post(Urlbase.auth+"/userExists",{usuario:usuario,AppID:40}).subscribe(value => {
              resolve(value);
            }, error => {
              reject(error);
            })
        });
    }

    async checkPerson(docnumber:string):Promise<any>{
        return await new Promise<any>((resolve,reject) => {
            this.http.post(Urlbase.auth+"/CheckPerson",{docnumber:docnumber}).subscribe(value => {
              resolve(value);
            }, error => {
              reject(error);
            })
        });
    }

    public async ClearUserData(){
        localStorage.removeItem("RouterHome");
        localStorage.removeItem("FullUser");
        localStorage.removeItem("LastCheckClosestProv");
        localStorage.removeItem("TiendaProv");
        localStorage.removeItem("WelcomeMSG");
        this.usuarioCompleto = null;
        return new Promise<any>((resolve,reject) => {
            this.http.post(Urlbase.auth+"/logout",{}).subscribe(value => {
              this.LogStatusChange.next({status:"logout"});
              localStorage.removeItem("session_header");
              resolve(value);
            }, error => {
              localStorage.removeItem("session_header");
              reject(error);
            })
        });
    }
}
