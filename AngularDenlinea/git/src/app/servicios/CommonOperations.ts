import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CartComponent} from "../common/cart/cart.component";
import {MatDialogRef} from "@angular/material/dialog";
import {LoginComponent} from "../common/login/login.component";
import {CheckoutComponent} from "../common/checkout/checkout.component";
import {OrderViewerComponent} from "../common/order-viewer/order-viewer.component";
import {BillingService, Pedidos} from "./billing.service";
import {AccountService} from "./AccountService";
import {ToastrService} from "ngx-toastr";

@Injectable({
    providedIn:'root'
})
export class CommonOperations{

  public billingService:BillingService;
  public accountService:AccountService;
  VariableSuscriptor:any = null;

  public CartModal: MatDialogRef<CartComponent> = null;

  public LoginModal: MatDialogRef<LoginComponent> = null;

  public CheckoutModal: MatDialogRef<CheckoutComponent> = null;

  public OrderViewerModal: MatDialogRef<OrderViewerComponent> = null;

  public mostrandoCargando = false;
  public mostrandoCargandoCompleto = false;
  public ngxLoadingAnimationTypes = {
    chasingDots: 'chasing-dots',
    circle: 'sk-circle',
    circleSwish: 'circleSwish',
    cubeGrid: 'sk-cube-grid',
    doubleBounce: 'double-bounce',
    none: 'none',
    pulse: 'pulse',
    rectangleBounce: 'rectangle-bounce',
    rotatingPlane: 'rotating-plane',
    threeBounce: 'three-bounce',
    wanderingCubes: 'wandering-cubes'
  };

  ngOnDestroy() {
    console.log("Se elimina el suscribe")
    //prevent memory leak when component destroyed
    this.VariableSuscriptor.unsubscribe();
  }

  constructor(private http: HttpClient,private toastrService:ToastrService) {
    this.ngxLoadingAnimationTypes = {
      chasingDots: 'chasing-dots',
      circle: 'sk-circle',
      circleSwish: 'circleSwish',
      cubeGrid: 'sk-cube-grid',
      doubleBounce: 'double-bounce',
      none: 'none',
      pulse: 'pulse',
      rectangleBounce: 'rectangle-bounce',
      rotatingPlane: 'rotating-plane',
      threeBounce: 'three-bounce',
      wanderingCubes: 'wandering-cubes'
    };
    console.log("Se agrega el suscribe")
  }

  public IsMobile(){
    return window.innerWidth < 700;
  }

  async VerificarOrdenes(OldLisadoOrdenesActivas:Pedidos[]){
    // this.ActualizandoOrdenes = true;
    if(OldLisadoOrdenesActivas.length != this.billingService.ListadoPedidosActivos.length){
      let PedidosNoEncontrados:Pedidos[] = [];
      let PedidoEnViewerEncontrado = false;

      for(let n = 0;n<OldLisadoOrdenesActivas.length;n++){
        let PedidoViejo = OldLisadoOrdenesActivas[n];
        let Encontrado = false;
        for(let m = 0;m<this.billingService.ListadoPedidosActivos.length;m++){
          let PedidoNuevo = this.billingService.ListadoPedidosActivos[m];
          if(this.OrderViewerModal !=null && this.OrderViewerModal.componentInstance !=null && this.OrderViewerModal.componentInstance.Pedido != null && !PedidoEnViewerEncontrado){
            if(PedidoNuevo.id_BILL == this.OrderViewerModal.componentInstance.Pedido.id_BILL){
              PedidoEnViewerEncontrado = true;
            }
          }
          if(PedidoNuevo.id_BILL == PedidoViejo.id_BILL){
            Encontrado = true;
            break;
          }
        }
        if(!Encontrado){
          PedidosNoEncontrados.push(PedidoViejo);
        }
      }
      let EjecutaActualizadoViejos = false;
      if(PedidosNoEncontrados.length > 0){
        console.log("No se encontró el pedido en activos, buscamos en finalizados")
        //Actualizo los pedidos finalizados y lo busco
        await this.billingService.GetEndedOrderList(this.accountService.usuarioCompleto.third.id_third);
        EjecutaActualizadoViejos = true;
        for(let m = 0;m<PedidosNoEncontrados.length;m++) {
          let PedidoNoEncontrado = PedidosNoEncontrados[m];
          for(let n = 0;n<this.billingService.ListadoPedidosFinalizados.length;n++) {
            let PedidoFinalizado = this.billingService.ListadoPedidosFinalizados[n];
            if (PedidoFinalizado.id_BILL == PedidoNoEncontrado.id_BILL) {
              if(this.OrderViewerModal !=null && this.OrderViewerModal.componentInstance !=null && this.OrderViewerModal.componentInstance.Pedido != null){
                if(this.OrderViewerModal.componentInstance.Pedido.id_BILL == PedidoFinalizado.id_BILL){
                  continue;
                }
              }
              if(PedidoFinalizado.id_BILL_STATE == 804) {
                this.toastrService.success("Pedido '"+PedidoFinalizado.numdocumento+"' entregado exitosamente",'Entregado')
              }else if(PedidoFinalizado.id_BILL_STATE == 808) {
                this.toastrService.info("Pedido '"+PedidoFinalizado.numdocumento+"' entregado con la novedad: "+PedidoFinalizado.body,'Entregado con novedad',{closeButton:true,disableTimeOut:true})

              }else if(PedidoFinalizado.id_BILL_STATE == 99) {
                this.toastrService.error(PedidoFinalizado.body.substr(42,PedidoFinalizado.body.length),'Pedido Cancelado',{closeButton:true,disableTimeOut:true})
              }
            }
          }
        }
      }
      if(this.OrderViewerModal !=null && this.OrderViewerModal.componentInstance !=null && this.OrderViewerModal.componentInstance.Pedido != null && !PedidoEnViewerEncontrado){
        let encontrado = false;
        this.mostrandoCargando = true;
        if(!EjecutaActualizadoViejos){
          await this.billingService.GetEndedOrderList(this.accountService.usuarioCompleto.third.id_third);
        }
        for(let n = 0;n<this.billingService.ListadoPedidosFinalizados.length;n++) {
          let PedidoFinalizado = this.billingService.ListadoPedidosFinalizados[n];
          if(PedidoFinalizado.id_BILL == this.OrderViewerModal.componentInstance.Pedido.id_BILL){
            encontrado = true;
            this.OrderViewerModal.componentInstance.Pedido = PedidoFinalizado;
            this.OrderViewerModal.componentInstance.ActualizarEstadoOrden();
            try {
              this.OrderViewerModal.componentInstance.DetallesPedido = <[]>await this.billingService.GetOrderDetails(PedidoFinalizado.id_BILL);
              console.log("this.DetallesPedido es")
              console.log(this.OrderViewerModal.componentInstance.DetallesPedido)
            }catch (e) {
              console.log("No se pudieron obtener los detalles")
              console.log(e)
            }
            this.mostrandoCargando = false;
          }
        }
        /////////////////////////////
        if(!encontrado){//no encuentra el pedido por ningun lado
          this.toastrService.error('Ocurrió un error desconocido, recargue el sitio por favor.','Error',{closeButton:true,disableTimeOut:true});
        }
      }
    }else{//lista es igual
      if(this.OrderViewerModal !=null && this.OrderViewerModal.componentInstance !=null && this.OrderViewerModal.componentInstance.Pedido != null){//
        for(let n = 0;n<this.billingService.ListadoPedidosActivos.length;n++){
          let Pedido = this.billingService.ListadoPedidosActivos[n];
          if(this.OrderViewerModal.componentInstance.Pedido.id_BILL == Pedido.id_BILL){
            this.OrderViewerModal.componentInstance.Pedido = Pedido;
            this.OrderViewerModal.componentInstance.ActualizarEstadoOrden();
            break;
          }
        }
      }
    }
    // this.ActualizandoOrdenes = false;
  }
}
