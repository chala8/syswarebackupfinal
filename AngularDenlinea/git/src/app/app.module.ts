import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ToastrModule} from "ngx-toastr";
import { CartComponent } from './common/cart/cart.component';
import {CustomInterceptor} from "./utils/CustomInterceptor";
import {MatIconModule} from "@angular/material/icon";
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { LoginComponent } from './common/login/login.component';
import {MatOptionModule} from "@angular/material/core";
import {MatSelectModule} from '@angular/material/select';
import {CurrencyPipe} from "@angular/common";
import { HereMapComponent } from './common/here-map/here-map.component';
import { CheckoutComponent } from './common/checkout/checkout.component';
import {MatRadioModule} from "@angular/material/radio";
import {NgxLoadingModule} from "ngx-loading";
import { OrderViewerComponent } from './common/order-viewer/order-viewer.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatExpansionModule} from '@angular/material/expansion';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    CartComponent,
    LoginComponent,
    HereMapComponent,
    CheckoutComponent,
    OrderViewerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    MatIconModule,
    MatMenuModule,
    MatFormFieldModule,
    MatExpansionModule,
    FormsModule,
    MatOptionModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatRadioModule,
    NgxLoadingModule,
    // ToastrModule added
  ],
  providers: [
    CurrencyPipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CustomInterceptor,
      multi: true
    }
  ],
  exports: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
