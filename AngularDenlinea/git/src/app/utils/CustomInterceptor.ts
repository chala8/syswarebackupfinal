import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';

import {AccountService} from '../servicios/AccountService';
import {ToastrService} from "ngx-toastr";

@Injectable()
export class CustomInterceptor implements HttpInterceptor {

  constructor(
      public router: Router,
      public accountService: AccountService,
      private toastr: ToastrService
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if(!request.url.includes("hereapi")){
      if(localStorage.getItem("session_header") != null && localStorage.getItem("session_header") != "" && localStorage.getItem("session_header") != "-1"){
        request = request.clone({
          headers: new HttpHeaders({
            'authorization': localStorage.getItem("session_header")
          }),
          withCredentials: true
        });
      }else{
        request = request.clone({
          withCredentials: true
        });
      }
    }

    //return next.handle(request);
    return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
      return event;
    },async (EventError:HttpErrorResponse) => {
      console.log("EventError2 es:");
      console.log(EventError);
      console.log("this.accountService.MacProblemCheck es:");
      console.log(this.accountService.MacProblemCheck);
      console.log(JSON.stringify(EventError));
      if(EventError.status == 401 && this.accountService.usuarioCompleto != null && !this.accountService.MacProblemCheck){
        this.toastr.error("Su sesión ha Expirado, vuelva a iniciarla")
        await this.accountService.ClearUserData();
        localStorage.setItem("LastCheckClosestProv",null);
        localStorage.setItem("TiendaProv",null);
        return new Observable<HttpEvent<any>>();
      }
    }));
  }
}
