import {Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  title = 'denlineafront';

  mobile = false;
  ngOnInit() {
    if (window.innerWidth <= 600) { // 768px portrait
      this.mobile = true;
    }
  }

  @HostListener('window:resize', ['$event'])
  validateRender(item){
    //@ts-ignore
    if(event.target.innerWidth <= 600) { // 768px portrait
      this.mobile = true;
    }else{
      this.mobile = false;
    }
  }

  ClickWhatsApp(){
    window.open("https://api.whatsapp.com/send?phone=573208403594&text=Hola%20estoy%20interesado%20en%20Denlinea")
  }
}
