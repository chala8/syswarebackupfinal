import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LandingPageComponent} from "./landing-page/landing-page.component";


const routes: Routes = [
  {
    path: 'tiendavirtual',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  },
  {
    path:"",
    component:LandingPageComponent
  },
  { path: '**', redirectTo:'',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
