import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from '@angular/material/icon'
import {MatTabsModule} from '@angular/material/tabs';
import {NgxLoadingModule} from 'ngx-loading';
import {ScrollingModule} from "@angular/cdk/scrolling";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDialogModule} from "@angular/material/dialog";
import {MatRadioModule} from '@angular/material/radio';
import {AppModule} from "../app.module";
import {MatMenuModule} from "@angular/material/menu";
import {CommonDefinitionsModule} from "../common/common-definitions.module";
import {MatExpansionModule} from "@angular/material/expansion";
import {MatCardModule} from '@angular/material/card';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatTabsModule,
    NgxLoadingModule,
    ScrollingModule,
    MatCardModule,
    FormsModule,
    MatRadioModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatMenuModule,
    CommonDefinitionsModule,
    MatExpansionModule
  ]
})
export class HomeModule { }
