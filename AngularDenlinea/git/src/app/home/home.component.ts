import {Component, Input, OnInit} from '@angular/core';
import {AccountService} from "../servicios/AccountService";
import {InventoriesService} from "../servicios/inventories.service";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BillingService} from "../servicios/billing.service";
import {ActivatedRoute, Router} from "@angular/router";
import {LocalStorage} from "../shared/localStorage";
import {CommonOperations} from "../servicios/CommonOperations";
import {ToastrService} from "ngx-toastr";
import {InventoryName} from "../servicios/cart.service";
import {MatDialog} from "@angular/material/dialog";
import {CartComponent} from "../common/cart/cart.component";
import {OrderViewerComponent} from "../common/order-viewer/order-viewer.component";
import {Urlbase} from "../utils/urls";
import {LoginComponent} from "../common/login/login.component";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  ListadoKeysLineas = [];
  ListadoKeysCategorias = [];

  IdProv = 81;
  ProvInvalido = false;

  CategoriasOculto = false;

  CurrentLine = "";
  CurrentCat = "";

  constructor(
    public accountService:AccountService,
    public inventoryService:InventoriesService,
    private httpClient: HttpClient,
    public commonOperations:CommonOperations,
    public billingService : BillingService,
    private router:Router,
    public locStorage: LocalStorage,
    private toastr: ToastrService,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.IdProv = parseInt(this.route.snapshot.queryParamMap.get("idprov"));
    this.IdProv = this.IdProv || -1;
    if(this.IdProv == -1){
      if(localStorage.getItem("IdProv") != null && localStorage.getItem("IdProv") != ""){
        this.IdProv = parseInt(localStorage.getItem("IdProv"));
      }else{
        this.IdProv = 81;
      }
    }else{
      localStorage.setItem("IdProv",this.IdProv+"");
    }
    // setTimeout(()=>{
    //
    //   const dialogRef = this.dialog.open(LoginComponent, {
    //     width: '90vw',
    //     height: '90vh',
    //     panelClass: 'custom-mat-dialog-container-large_full_page',
    //     data:{
    //       checkout:false
    //     }
    //   });
    //   this.commonOperations.LoginModal = dialogRef;
    //   dialogRef.afterClosed().subscribe(result => {
    //     this.commonOperations.LoginModal = null;
    //     if(result != null && result == "login"){
    //       this.toastr.success("Bienvenido, "+this.accountService.GetNormalName()+"");
    //     }
    //   });
    // },400);
    // this.IdProv = 261;
    // const dialogRef = this.dialog.open(OrderViewerComponent, {
    //   width: '90vw',
    //   height: '90vh',
    //   panelClass: 'custom-mat-dialog-container-large'
    // });
    //this.tempAbrirCheckout();
  }

  // noinspection JSUnusedGlobalSymbols
  async ngAfterViewInit(){
    try {
      this.commonOperations.mostrandoCargandoCompleto = true;
      //await this.accountService.loginUser("saayalac@unal.edu.co","123")
      //
      console.log("se ejecuta el ionViewDidEnter")
       this.getStores3();
      //
      console.log("ionViewDidEnter de home2 ");
      //this.IdProv = 81;
      if(this.IdProv == -1){
        this.ProvInvalido = true;
      }else{
        this.ProvInvalido = false;
        console.log("idProv es ");
        console.log(this.IdProv+"");
        //Get prov info
        let resultadoConsulta:{
          "nombretienda": string,
          "nombrepropietario": string,
          "url_logo": string,
          "ciudad": string,
          "direccion": string,
          "telefono": string,
          "email": string,
          "latitud": number,
          "longitud": number
        } = await new Promise((resolve, reject) => {
          this.httpClient.get(Urlbase.tienda+"/resource/getData?id_store="+this.IdProv,{headers:new HttpHeaders({
              "Key_Server":"FE651467B48D552C2EFBC8B13EBA9"
            })}).subscribe(value => { 
              this.locStorage.setTelefono(value[0].telefono);
              this.locStorage.setidCountry(value[0].id_country);
              resolve(value[0])},error => {reject(error)})
        })
        let logo = null;
        if(resultadoConsulta.url_logo != null){logo = "https://tienda724.com"+resultadoConsulta.url_logo}
        //
        this.billingService.SetProv({
          Email:resultadoConsulta.email,
          CityName:resultadoConsulta.ciudad,
          Phone:resultadoConsulta.telefono,
          CityID:-1,
          Address:resultadoConsulta.direccion,
          Name:resultadoConsulta.nombretienda,
          StoreID:this.IdProv,
          Lng:resultadoConsulta.longitud,
          Lat:resultadoConsulta.latitud,
          DocType:"-1",
          DocNumber:"-1",
          LogoURL:logo || "assets/logos/"+this.IdProv+".svg"
        })
        console.log("resultadoConsulta.url_logo es ",resultadoConsulta.url_logo);
        console.log("\"assets/logos/\"+this.IdProv+\".svg\" es ","assets/logos/"+this.IdProv+".svg");
        console.log("LogoURL es ",resultadoConsulta.url_logo || "assets/logos/"+this.IdProv+".svg");
        await this.inventoryService.getInventoryList(this.billingService.ProviderInfo.StoreID);
        console.log("ListadoKeysLineas es ");
        this.ListadoKeysLineas = Object.keys(this.inventoryService.LineaList);
        console.log(this.ListadoKeysLineas);
        setTimeout(()=>{
          //mensaje de bienvenida
          // if(localStorage.getItem("WelcomeMSG") == null || localStorage.getItem("WelcomeMSG") == ""){
          //   localStorage.setItem("WelcomeMSG","true");
          //   this.PanelTienda = true;
          // }
        },1000);
        //Obtengo los pedidos
        // await this.billingService.GetEndedOrderList(this.accountService.usuarioCompleto.third.id_third);
        // await this.billingService.GetActiveOrderList(this.accountService.usuarioCompleto.third.id_third);
        await this.inventoryService.cartService.EventoInputBuscadorProductos("",this.CurrentLine,this.CurrentCat);
      }
    }catch (e) {
      this.toastr.error(e.toString())
      console.log("idProv es ERROR  ")
      console.log(e)
    }
    this.commonOperations.mostrandoCargandoCompleto = false;
  }

  CurrentFocusInputProduct:Input = null;
  FocusInputProducto(evento:FocusEvent){
    this.CurrentFocusInputProduct = <Input>evento.target;
  }

  ClickGoToAdmin(){
    window.open("https://tienda724.com/app/#/login?appid=40")
  }

  ClickPerfil(){
    this.accountService.MostrandoProfileIcon = !this.accountService.MostrandoProfileIcon;
  }

  ClickWhatsApp(){
    window.open("https://api.whatsapp.com/send?phone="+this.locStorage.getTelefono()+ "&text=Hola%20estoy%20interesado%20en%20Denlinea")
  }

  async ClickVolverProductos(newId){
    this.commonOperations.mostrandoCargando = true;
    this.ListadoKeysLineas = Object.keys(this.inventoryService.LineaList);
    this.ListadoKeysCategorias = [];
    this.CurrentCatID = -1;
    this.CurrentLineaID = -1;
    this.CurrentLine = "";
    this.CurrentCat = "";
    await this.inventoryService.cartService.EventoInputBuscadorProductos("",this.CurrentLine,this.CurrentCat);
    this.commonOperations.mostrandoCargando = false;
    this.ClickLinea(newId);
  }

  ClickMostrarCarrito(){
    if(this.commonOperations.CartModal != null || this.inventoryService.cartService.ListaTablaProductosSeleccionados.length == 0){return;}
    console.log("ClickMostrarCarrito")
    const dialogRef = this.dialog.open(CartComponent, {
      width: this.commonOperations.IsMobile() ? '90vw':'40vw',
      height: this.commonOperations.IsMobile() ? '80vh':'70vh',
      panelClass: 'custom-mat-dialog-container-large'
    });
    this.commonOperations.CartModal = dialogRef;
    dialogRef.afterClosed().subscribe(result => {
      this.commonOperations.CartModal = null;
    });
  }


  GetQuantity(item:InventoryName){
    let cantidad = 0;
    if(this.inventoryService.cartService.DiccionarioIdsProductosSeleccionados[item.ownbarcode] != null){
      for(let n = 0;n<this.inventoryService.cartService.ListaTablaProductosSeleccionados.length;n++){
        if(this.inventoryService.cartService.ListaTablaProductosSeleccionados[n].ownbarcode == item.ownbarcode){
          cantidad = this.inventoryService.cartService.ListaTablaProductosSeleccionados[n].quantity;
          this.inventoryService.cartService.ActualizarTabla();
          break;
        }
      }
    }
    return cantidad;
  }

  lastItemFocusInputProduct:InventoryName = null;
  async ClickSubirBajarCantidad(evento:MouseEvent,subir,item:InventoryName){
    evento.stopPropagation();
    console.log("evento",evento)
    // @ts-ignore
    this.CurrentFocusInputProduct = evento.target.parentElement.parentElement.children[1].children[0];
    this.lastItemFocusInputProduct = item;
    // @ts-ignore
    let valor = parseInt(this.CurrentFocusInputProduct.value);
    if(subir){valor++;}else{valor--;}
    if(valor<1){valor = 1;}
    // @ts-ignore
    this.CurrentFocusInputProduct.value = valor;
    console.log("input",this.CurrentFocusInputProduct)
    if(this.inventoryService.cartService.DiccionarioIdsProductosSeleccionados[item.ownbarcode] != null){
      for(let n = 0;n<this.inventoryService.cartService.ListaTablaProductosSeleccionados.length;n++){
        if(this.inventoryService.cartService.ListaTablaProductosSeleccionados[n].ownbarcode == item.ownbarcode){
          if(subir){this.inventoryService.cartService.ListaTablaProductosSeleccionados[n].quantity++;}
          else if(!subir && this.inventoryService.cartService.ListaTablaProductosSeleccionados[n].quantity > 1){this.inventoryService.cartService.ListaTablaProductosSeleccionados[n].quantity--;}
          this.inventoryService.cartService.ActualizarTabla();
          break;
        }
      }
    }else{
      this.commonOperations.mostrandoCargando = true;
      await this.inventoryService.cartService.AgregarProducto(item,1);
      this.commonOperations.mostrandoCargando = false;
    }
  }

  // GetItemPrice(item:InventoryName){
  //   return item.price + (item.price*this.inventoryService.getPercentTax(item.id_TAX))
  // }
  GetItemPrice2(item:InventoryName){
    let valor = item.price + (item.price*this.inventoryService.getPercentTax(item.id_TAX));
    //Ajuste a 50 superior
    let modOpt = valor % 50;
    if(modOpt > 0){
      valor += 50 - modOpt;
    }
    return valor;
  }

  GetItemPrice(item){
    if(this.locStorage.getidCountry()=="ES"){
      return (item.price + (item.price*this.inventoryService.getPercentTax(item.id_TAX))).toFixed(2);
    }else{
      return this.GetItemPrice2(item);
    }
  }


  GetItemPriceNoIt2(item){
    let valor = item;
    //Ajuste a 50 superior
    let modOpt = valor % 50;
    if(modOpt > 0){
      valor += 50 - modOpt;
    }
    return valor;
  }

  GetItemPriceNoIt(item){
    if(this.locStorage.getidCountry()=="ES"){
      return item.toFixed(2);
    }else{
      return this.GetItemPriceNoIt2(item);
    }
  }

  async ClickAgregarProducto(producto:InventoryName,evento){
    let cantidad = 1;
    //
    this.CurrentFocusInputProduct = evento.target.parentElement.parentElement.children[1].children[0].children[1].children[0];
    // let valor = parseInt(this.CurrentFocusInputProduct.value);
    // if(subir){valor++;}else{valor--;}
    // if(valor<1){valor = 1;}
    //
    if(this.CurrentFocusInputProduct != null && this.lastItemFocusInputProduct == producto){
      // @ts-ignore
      cantidad = parseInt(this.CurrentFocusInputProduct.value);
      cantidad = cantidad || 1;
    }
    // @ts-ignore
    this.CurrentFocusInputProduct.value = cantidad*2;
    this.commonOperations.mostrandoCargando = true;
    await this.inventoryService.cartService.AgregarProducto(producto,cantidad);
    this.commonOperations.mostrandoCargando = false;
  }

  CurrentLineaID:number = -1;
  CurrentCatID:number = -1;
  MostrandoCategorias = false;
  async ClickLinea(idLinea){
    if(this.CurrentLineaID == idLinea){
      await this.ClickVolverProductos(idLinea);
      this.commonOperations.mostrandoCargando = false;
      return;
    }
    this.MostrandoCategorias = true;
    this.CurrentLineaID = idLinea;
    this.ListadoKeysCategorias = Object.keys(this.inventoryService.LineaList[idLinea].categorias);
    this.commonOperations.mostrandoCargando = true;
    this.CurrentLine = this.inventoryService.LineaList[idLinea].nombre;
    await this.inventoryService.cartService.EventoInputBuscadorProductos("",this.CurrentLine,this.CurrentCat);
    setTimeout(()=>{this.commonOperations.mostrandoCargando = false;},100);
    this.CategoriasOculto = false;
  }

  BlurCategorias(){
    if(this.commonOperations.mostrandoCargando){return;}
    this.MostrandoCategorias = false
  }

  BlurInput(evento){
    console.log("evento BlurInput",evento)
    setTimeout(()=>{
      // @ts-ignore
      let quantityINT = parseInt(this.CurrentFocusInputProduct.value);
      if(isNaN(quantityINT)){quantityINT = 1;}
      if(quantityINT == null || quantityINT < 1){
        quantityINT = 1;
      }
      // @ts-ignore
      this.CurrentFocusInputProduct.value = quantityINT;
    },100);
  }

  GetCategoriasLinea(idLinea){
    return Object.keys(this.inventoryService.LineaList[idLinea].categorias);
  }

  async ClickCat(idCat){
    idCat = parseInt(idCat)
    this.commonOperations.mostrandoCargando = true;
    if(this.CurrentCatID == idCat){
      this.ListadoKeysCategorias = [];
      this.CurrentCatID = -1;
      this.CurrentCat = "";
      await this.inventoryService.cartService.EventoInputBuscadorProductos("",this.CurrentLine,this.CurrentCat);
      this.commonOperations.mostrandoCargando = false;
     return;
    }
    this.CurrentCatID = idCat;
    this.CurrentCat = this.inventoryService.LineaList[this.CurrentLineaID].categorias[idCat].nombre;
    await this.inventoryService.cartService.EventoInputBuscadorProductos("",this.CurrentLine,this.CurrentCat);
    setTimeout(()=>{this.commonOperations.mostrandoCargando = false;},100);
  }

  ClickExpandirCategorias(){
    this.CategoriasOculto = !this.CategoriasOculto;
  }

  CalcularSizeFont(text:string,base?:number){
    let constante = (window.innerWidth/100)*5;
    let sizeBase = base || (this.commonOperations.IsMobile() ? 4:2.2);
    let anchoTexto = text.length*constante
    let anchoParent = window.innerWidth/2.45;
    if(anchoTexto > anchoParent){
      //sizeBase = sizeBase / ((anchoTexto-anchoParent)/anchoParent);
      sizeBase = sizeBase * (anchoParent/anchoTexto);
    }
    return sizeBase +'vmin'
  }

  //Metodos copiados

  getStores3() {
    //Obtengo listado
    let store = 12;
    ////////
    this.locStorage.setBoxStatus(false);
    this.locStorage.setIdStore(store)
  }

  PromesaFiltrado = -1;
  ValorFiltrado = "";
  async applyFilter(filterValue: string) {
    if(this.PromesaFiltrado == -1){//Busqueda no iniciada
      this.PromesaFiltrado = 1;//Busqueda iniciada
      this.ValorFiltrado = filterValue.trim().toLowerCase();
      while(1==1){
        await new Promise(resolve => setTimeout(resolve, 250));
        if(this.PromesaFiltrado == 1){break;}
        else{ this.PromesaFiltrado = 1; }
      }
      this.commonOperations.mostrandoCargando = true;
      if(this.ValorFiltrado == ""){
        await this.inventoryService.cartService.EventoInputBuscadorProductos(this.ValorFiltrado,this.CurrentLine,this.CurrentCat);
      }else{
        await this.inventoryService.cartService.EventoInputBuscadorProductos(this.ValorFiltrado,"","");
      }
      setTimeout(()=>{this.commonOperations.mostrandoCargando = false;},100);
      // this.dataSourceBuscadoProductos.filter = this.ValorFiltrado;
      //
      // if (this.dataSourceBuscadoProductos.paginator) {
      //   this.dataSourceBuscadoProductos.paginator.firstPage();
      // }
      this.PromesaFiltrado = -1;//Busqueda finalizada
    }else{//Busqueda iniciada
      this.ValorFiltrado = filterValue.trim().toLowerCase();
      this.PromesaFiltrado = 2;
    }
  }
}
