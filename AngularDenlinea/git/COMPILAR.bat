rmdir /s /q "denlinea" & (
	node --max_old_space_size=8192 ./node_modules/@angular/cli/bin/ng build --prod --aot && (
		del denlinea.zip & (
			zip -r denlinea.zip denlinea & (
				pause
			)
		)
	) || (
	  pause
	)
)