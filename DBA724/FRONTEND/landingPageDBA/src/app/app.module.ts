import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing/landing-page/landing-page.component';
import { MenuComponent } from './menu/menu.component';
import { LandingPageBodyComponent } from './landing-page-body/landing-page-body.component';
import { BottomFormComponent } from './bottom-form/bottom-form.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    MenuComponent,
    LandingPageBodyComponent,
    BottomFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
