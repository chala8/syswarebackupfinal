package com.name.business.mappers;

import com.name.business.entities.Master;
import com.name.business.entities.Report;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReportMapper implements ResultSetMapper<Report> {

    @Override
    public Report map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Report(
                resultSet.getString("PREFIJO"),
                resultSet.getString("NUMUNAL"),
                resultSet.getString("FECHA"),
                resultSet.getString("TIPO_DOCUMENTO"),
                resultSet.getString("VALOR"),
                resultSet.getString("ESTADO"),
                resultSet.getString("CONSEC_DIAN"),
                resultSet.getString("NUM_NOTA"),
                resultSet.getString("MENSAJE_ERROR")
        );
    }
}
