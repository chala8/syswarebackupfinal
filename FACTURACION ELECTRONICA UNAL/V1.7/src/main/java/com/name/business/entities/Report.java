package com.name.business.entities;

public class Report {

    private String PREFIJO;
    private String NUMUNAL;
    private String FECHA;
    private String TIPO_DOCUMENTO;
    private String VALOR;
    private String ESTADO;
    private String CONSEC_DIAN;
    private String NUM_NOTA;
    private String MENSAJE_ERROR;


    public Report(String PREFIJO, String NUMUNAL, String FECHA, String TIPO_DOCUMENTO, String VALOR, String ESTADO, String CONSEC_DIAN, String NUM_NOTA, String MENSAJE_ERROR) {
        this.PREFIJO = PREFIJO;
        this.NUMUNAL = NUMUNAL;
        this.FECHA = FECHA;
        this.TIPO_DOCUMENTO = TIPO_DOCUMENTO;
        this.VALOR = VALOR;
        this.ESTADO = ESTADO;
        this.CONSEC_DIAN = CONSEC_DIAN;
        this.NUM_NOTA = NUM_NOTA;
        this.MENSAJE_ERROR = MENSAJE_ERROR;
    }

    public String getPREFIJO() {
        return PREFIJO;
    }

    public void setPREFIJO(String PREFIJO) {
        this.PREFIJO = PREFIJO;
    }

    public String getNUMUNAL() {
        return NUMUNAL;
    }

    public void setNUMUNAL(String NUMUNAL) {
        this.NUMUNAL = NUMUNAL;
    }

    public String getFECHA() {
        return FECHA;
    }

    public void setFECHA(String FECHA) {
        this.FECHA = FECHA;
    }

    public String getTIPO_DOCUMENTO() {
        return TIPO_DOCUMENTO;
    }

    public void setTIPO_DOCUMENTO(String TIPO_DOCUMENTO) {
        this.TIPO_DOCUMENTO = TIPO_DOCUMENTO;
    }

    public String getVALOR() {
        return VALOR;
    }

    public void setVALOR(String VALOR) {
        this.VALOR = VALOR;
    }

    public String getESTADO() {
        return ESTADO;
    }

    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }

    public String getCONSEC_DIAN() {
        return CONSEC_DIAN;
    }

    public void setCONSEC_DIAN(String CONSEC_DIAN) {
        this.CONSEC_DIAN = CONSEC_DIAN;
    }

    public String getNUM_NOTA() {
        return NUM_NOTA;
    }

    public void setNUM_NOTA(String NUM_NOTA) {
        this.NUM_NOTA = NUM_NOTA;
    }

    public String getMENSAJE_ERROR() {
        return MENSAJE_ERROR;
    }

    public void setMENSAJE_ERROR(String MENSAJE_ERROR) {
        this.MENSAJE_ERROR = MENSAJE_ERROR;
    }

}
