package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.JsonConverterBusiness;
//import com.name.business.entities.*;
import com.name.business.entities.*;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Date;


@Path("/invoice")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JsonConverterResource {
    private JsonConverterBusiness jsonConverterBusiness;

    public JsonConverterResource(JsonConverterBusiness jsonConverterBusiness) {
        this.jsonConverterBusiness = jsonConverterBusiness;
    }

    @Path("/masterQuipu")
    @GET
    @Timed
    @RolesAllowed({"Auth"})

    public Response masterProcedure(@QueryParam("fecha")String fecha){

        Response response;

        List<MasterQuipu> getResponseGeneric = jsonConverterBusiness.masterQuipu(fecha);


        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();


        return response;
    }


    @Path("/detailQuipu")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response detail(
            @QueryParam("PREFIJO") String PREFIJO,
            @QueryParam("NRORECIBO") String NRORECIBO,
            @QueryParam("SEDE") Long SEDE,
            @QueryParam("CIAQUIPU") String CIAQUIPU,
            @QueryParam("NUMDOCUHIJO") String NUMDOCUHIJO
    ){

        Response response;

        List<DetailQuipu> getResponseGeneric = jsonConverterBusiness.detailQuipu(PREFIJO, NRORECIBO, SEDE, CIAQUIPU, NUMDOCUHIJO);


        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();


        return response;
    }


    @Path("/file")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response file(@QueryParam("fileMaster") String fileMaster, @QueryParam("fileDetail") String fileDetail){

        Response response;

        List<MasterResponse> getResponseGeneric = jsonConverterBusiness.file(fileMaster,fileDetail);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;
    }


    @Path("/quipu")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response quipu(@QueryParam("fecha")String fecha){

        Response response;

        List<MasterResponse> getResponseGeneric = jsonConverterBusiness.quipu(fecha);


        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();


        return response;
    }



    @Path("/sia")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response sia(@QueryParam("fecha")String fecha){

        Response response = response=Response.status(Response.Status.OK).entity(new Long(-1)).build();;

        try{
            List<MasterResponse> getResponseGeneric = jsonConverterBusiness.sia(fecha);
            response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        }catch(Exception e){
            e.printStackTrace();
        }
        return response;

    }



    @Path("/hermes")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response hermes(@QueryParam("fecha")String fecha){

        Response response;


        List<MasterResponse> getResponseGeneric = jsonConverterBusiness.hermes(fecha);


        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();


        return response;
    }



    @Path("/detailFile")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response detailFile(){

        Response response;
        List<String> getResponseGeneric = jsonConverterBusiness.detailFile();
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;

    }


    @Path("/masterFile")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response masterFile(){

        Response response;

        List<String> getResponseGeneric = jsonConverterBusiness.masterFile();


        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();


        return response;
    }


    @GET
    @Timed
    @Path("/allProcess")
    @RolesAllowed({"Auth"})
    public Response JsonConverterMethod(@QueryParam("type") int type, @QueryParam("date") String date, @QueryParam("master") String master, @QueryParam("detail") String detail, @QueryParam("env") Long env){

        Response response;

        Long generalResponse = jsonConverterBusiness.readData(type,date,master,detail);
        Long responseObject3 = jsonConverterBusiness.asigna_consecutivo_dian();
        Long getResponseGeneric = jsonConverterBusiness.JsonConverterMethod(env);
        response = Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }





    @GET
    @Timed
    @Path("/readData")
    @RolesAllowed({"Auth"})
    public Response readData(@QueryParam("type") int type, @QueryParam("date") String date, @QueryParam("master") String master, @QueryParam("detail") String detail){

        Response response;
        Long generalResponse = jsonConverterBusiness.readData(type,date,master,detail);
        response = Response.status(Response.Status.OK).entity(generalResponse).build();
        return response;
    }


    @GET
    @Timed
    @Path("/readData/dateList")
    @RolesAllowed({"Auth"})
    public Response readDataList(@QueryParam("type") int type, @QueryParam("date") String dateList, @QueryParam("master") String master, @QueryParam("detail") String detail){

        Response response;

        List<String> myList = new ArrayList<String>(Arrays.asList(dateList.split(",")));

        String date = "";
        for(int i=0; i < myList.size();i++){

            date = myList.get(i);
            Long generalResponse = jsonConverterBusiness.readData(type,date,master,detail);

        }

        response = Response.status(Response.Status.OK).entity(1).build();
        return response;
    }



    @GET
    @Timed
    @Path("/consecDian")
    @RolesAllowed({"Auth"})
    public Response asigna_consecutivo_dian(){

        Response response;

        Long generalResponse = jsonConverterBusiness.asigna_consecutivo_dian();

        response = Response.status(Response.Status.OK).entity(generalResponse).build();

        return response;
    }

    @GET
    @Timed
    @Path("/eliminarTemporales")
    @RolesAllowed({"Auth"})
    public Response eliminar_temporales(){

        Response response;

        Long generalResponse = jsonConverterBusiness.eliminar_temporales();

        response = Response.status(Response.Status.OK).entity(generalResponse).build();

        return response;
    }


    @GET
    @Timed
    @Path("/sendxml")
    @RolesAllowed({"Auth"})
    public Response sendXmlD(@QueryParam("env") Long env){

        Response response;

        Long generalResponse = jsonConverterBusiness.JsonConverterMethod(env);

        response = Response.status(Response.Status.OK).entity(generalResponse).build();

        return response;
    }


    @GET
    @Timed
    @Path("/readData/v2")
    @RolesAllowed({"Auth"})
    public Response readDataV2(@QueryParam("type") int type,
                               @QueryParam("date") String date,
                               @QueryParam("days") int days,
                               @QueryParam("fileMaster") String fileMaster,
                               @QueryParam("fileDetail") String fileDetail){

        Response response;

        Long generalResponse = jsonConverterBusiness.readDatav2(type,date,days,fileMaster,fileDetail);

        response = Response.status(Response.Status.OK).entity(generalResponse).build();

        return response;
    }


    @GET
    @Timed
    @Path("/allProcess/v2")
    @RolesAllowed({"Auth"})
    public Response JsonConverterMethodV2(@QueryParam("date") String date, @QueryParam("days") int days, @QueryParam("fileMaster") String fileMaster, @QueryParam("fileDetail") String fileDetail){

        Response response;

        Long generalResponseFile = jsonConverterBusiness.readDatav2(1,date,days,fileMaster,fileDetail);
        Long generalResponseSIA = jsonConverterBusiness.readDatav2(2,date,days,fileMaster,fileDetail);
        Long generalResponseQuipu = jsonConverterBusiness.readDatav2(3,date,days,fileMaster,fileDetail);

        Long generalResponseConsecDian = jsonConverterBusiness.asigna_consecutivo_dian();

        Long getResponseGeneric = jsonConverterBusiness.JsonConverterMethod(new Long(1));

        response = Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;
    }


    @GET
    @Timed
    @Path("/allProcess/v2/individual")
    @RolesAllowed({"Auth"})
    public Response JsonConverterMethodV2(@QueryParam("type") int type,@QueryParam("date") String date, @QueryParam("days") int days, @QueryParam("fileMaster") String fileMaster, @QueryParam("fileDetail") String fileDetail){

        Response response;

        Long generalResponseFile = jsonConverterBusiness.readDatav2(type,date,days,fileMaster,fileDetail);

        Long generalResponseConsecDian = jsonConverterBusiness.asigna_consecutivo_dian();

        Long getResponseGeneric = jsonConverterBusiness.JsonConverterMethod(new Long(1));

        response = Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;
    }


    @GET
    @Timed
    @Path("/report")
    @RolesAllowed({"Auth"})
    public Response report(@QueryParam("date1") String date1,@QueryParam("date2") String date2,@QueryParam("list") String list){

        Response response;

        List<Report> getResponseGeneric = jsonConverterBusiness.report(date1,date2,list);

        response = Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;
    }


    @GET
    @Timed
    @Path("/report2")
    @RolesAllowed({"Auth"})
    public Response report2(@QueryParam("nrorecibo") String nrorecibo){

        Response response;

        List<Report> getResponseGeneric = jsonConverterBusiness.report2(nrorecibo);

        response = Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;
    }


    @GET
    @Timed
    @Path("/prefs")
    @RolesAllowed({"Auth"})
    public Response prefs(){

        Response response;

        List<Pref> getResponseGeneric = jsonConverterBusiness.prefs();

        response = Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;
    }

}