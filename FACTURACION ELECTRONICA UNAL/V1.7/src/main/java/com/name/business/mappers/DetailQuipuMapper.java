package com.name.business.mappers;

import com.name.business.entities.Detail;
import com.name.business.entities.DetailQuipu;
import com.name.business.entities.Master;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailQuipuMapper implements ResultSetMapper<DetailQuipu> {

    @Override
    public DetailQuipu map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DetailQuipu(
                resultSet.getString("DET_PREFIJO"),
                resultSet.getString("DET_NRORECIBO"),
                resultSet.getLong("DET_SEDE"),
                resultSet.getLong("DET_CIAQUIPU"),
                resultSet.getDouble("DET_CANTIDA"),
                resultSet.getDouble("DET_VLRUNIT"),
                resultSet.getLong("DET_PCT_DESC"),
                resultSet.getString("DET_CODIGO"),
                resultSet.getString("DET_NOMBRE"),
                resultSet.getString("DET_DESCRIP"),
                resultSet.getString("DET_NUM_DOCUHIJO")
        );
    }
}
