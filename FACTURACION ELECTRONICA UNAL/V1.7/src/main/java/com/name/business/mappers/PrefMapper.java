package com.name.business.mappers;

import com.name.business.entities.Master;
import com.name.business.entities.Pref;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PrefMapper implements ResultSetMapper<Pref> {

    @Override
    public Pref map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Pref(
                resultSet.getString("con_prefijo"),
                resultSet.getString("con_ultim_asignado")
        );
    }
}
