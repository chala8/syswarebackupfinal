package com.name.business.DAOs;
import com.name.business.entities.*;
import com.name.business.mappers.*;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.unstable.BindIn;

import java.util.Date;
import java.util.List;

//import com.name.business.entities.*;
//import com.name.business.mappers.*;

//@RegisterMapper(CloseMapper.class)
@UseStringTemplate3StatementLocator
public interface JsonConverterDao {

    @SqlCall("begin asigna_consecutivo_dian(); end; ")
    void asigna_consecutivo_dian();

    @SqlCall("begin eliminar_temporales(); end; ")
    void eliminar_temporales();

    @RegisterMapper(MasterMapper.class)
    @SqlQuery("SELECT ENC_ID," +
            "ENC_PREFIJO," +
            "ENC_NRORECIBO," +
            "ENC_SEDE," +
            "ENC_VIGENCIA," +
            "ENC_CIAQUIPU," +
            "ENC_NRO_ITEMES," +
            "ENC_NOMBRESEDE," +
            "ENC_EMPRESAQUIPU," +
            "TO_CHAR(ENC_FECEMIC, 'yyyy-mm-dd') ENC_FECEMIC," +
            "ENC_TIPODOC," +
            "ENC_FORMAPAGO," +
            "TO_CHAR(ENC_FECPAGO, 'yyyy-mm-dd') ENC_FECPAGO," +
            "ENC_MEDIOPAGO," +
            "ENC_DESCUENTO," +
            "ENC_SUBTOTAL," +
            "ENC_TOTAL," +
            "ENC_OBSERVACIONES," +
            "ENC_DATOS_PAGO," +
            "ENC_CIUDAD_SEDE," +
            "ENC_DIR_SEDE," +
            "ENC_TELEF_SEDE," +
            "ENC_EMAIL_SEDE_EMISION," +
            "ENC_CLINATU, " +
            "ENC_CLINOMB, " +
            "ENC_CLITIPOID, " +
            "ENC_CLINUMID, " +
            "ENC_CLICIUDAD, " +
            "ENC_CLIDIR, " +
            "ENC_CLITELELEF, " +
            "ENC_CLIEMAIL, " +
            "ENC_CLIRESPFISCAL, " +
            "ENC_CLIRESPIVA, " +
            "ENC_FEC_CARGA, " +
            "ENC_ESTADO, " +
            "ENC_CONSDIAN, " +
            "ENC_ORIGEN, " +
            "ENC_FORMA_DATOS_ORIGEN, " +
            "ENC_NUM_DOCUHIJO "+
            "FROM FE_TMAESTRO WHERE ENC_ESTADO IN ('T','C') " +
            "order by ENC_PREFIJO,ENC_NRORECIBO,ENC_TIPODOC")
    List<Master> getMaster();

    @RegisterMapper(BillingDianInfoMapper.class)
    @SqlQuery("SELECT ENC_CONSDIAN, TO_CHAR(enc_fecemic,'yyyy-mm-dd') ENC_FECEMIC FROM FE_TMAESTRO WHERE ENC_PREFIJO=:PREFIJO AND ENC_NRORECIBO=:RECIBO \n" +
            "AND ENC_ESTADO='A' AND ENC_TIPODOC = 1")
    BillingDianInfo getConsecDianCredito(@Bind("RECIBO") String RECIBO, @Bind("PREFIJO") String PREFIJO);

    @RegisterMapper(DetailMapper.class)
    @SqlQuery(" SELECT DET_ENC_ID," +
            "DET_PREFIJO," +
            "DET_NRORECIBO," +
            "DET_SEDE," +
            "DET_VIGENCIA," +
            "DET_CIAQUIPU," +
            "DET_CANTIDAD," +
            "DET_VLRUNIT," +
            "DET_PCT_DESC," +
            "DET_CODIGO," +
            "DET_NOMBRE," +
            "DET_DESCRIP " +
            "FROM FE_TDETALLES " +
            "WHERE DET_PREFIJO=:PREFIJO AND DET_NRORECIBO=:NRORECIBO AND DET_SEDE=:SEDE AND DET_VIGENCIA=:VIGENCIA " +
            "AND DET_CIAQUIPU=:CIAQUIPU AND DET_NUM_DOCUHIJO = :NUMDOCUHIJO ")
    List<Detail> getDetails(@Bind("PREFIJO") String PREFIJO,
                            @Bind("NRORECIBO") String NRORECIBO,
                            @Bind("SEDE") Long SEDE,
                            @Bind("VIGENCIA") Long VIGENCIA,
                            @Bind("CIAQUIPU") String CIAQUIPU,
                            @Bind("NUMDOCUHIJO") String NUMDOCUHIJO);


    @SqlUpdate("UPDATE FE_TMAESTRO SET ENC_ESTADO=:estado,MENSAJE_ERROR=:MENSAJE\n" +
            "WHERE ENC_PREFIJO=:PREFIJO AND ENC_NRORECIBO=:NRORECIBO AND ENC_SEDE=:SEDE AND ENC_VIGENCIA=:VIGENCIA\n" +
            "AND ENC_CIAQUIPU=:CIAQUIPU AND ENC_NUM_DOCUHIJO=:DOCHIJO")
    void updateTable(@Bind("estado") String estado,
                     @Bind("PREFIJO") String PREFIJO,
                     @Bind("NRORECIBO") String NRORECIBO,
                     @Bind("SEDE") Long SEDE,
                     @Bind("VIGENCIA") Long VIGENCIA,
                     @Bind("CIAQUIPU") String CIAQUIPU,
                     @Bind("DOCHIJO") String DOCHIJO,
                     @Bind("MENSAJE") String MENSAJE);



    @RegisterMapper(MasterQuipuMapper.class)
    @SqlQuery("SELECT \n" +
            "ENC_PREFIJO,\n" +
            "ENC_NRORECIBO,\n" +
            "ENC_SEDE,\n" +
            "ENC_VIGENCIA,\n" +
            "ENC_CIAQUIPU,\n" +
            "ENC_NOMBRESEDE,\n" +
            "ENC_EMPRESAQUIPU,\n" +
            "ENC_FECEMIC,\n" +
            "ENC_TIPODOC,\n" +
            "ENC_FORMAPAGO,\n" +
            "ENC_FECPAGO,\n" +
            "ENC_MEDIOPAGO,\n" +
            "ENC_DESCUENTO,\n" +
            "ENC_SUBTOTAL,\n" +
            "ENC_TOTAL,\n" +
            "ENC_OBSERVACIONES,\n" +
            "ENC_DATOS_PAGO,\n" +
            "ENC_CIUDAD_SEDE,\n" +
            "ENC_DIR_SEDE,\n" +
            "ENC_TELEF_SEDE,\n" +
            "ENC_EMAIL_SEDE_EMISION,\n" +
            "ENC_CLINATU,\n" +
            "ENC_CLINOMB,\n" +
            "ENC_CLI_TIPOID,\n" +
            "ENC_CLI_NUMID,\n" +
            "ENC_CLICIUDAD,\n" +
            "ENC_CLIDIR,\n" +
            "ENC_CLITELEF,\n" +
            "ENC_CLIEMAIL,\n" +
            "ENC_CLIRESPFIS,\n" +
            "ENC_CLIRESPIVA,\n" +
            "ENC_NUM_DOCUHIJO\n" +
            "FROM FE_VMAESTRA_QUIPU \n" +
            "WHERE ENC_fecemic=:fecha")
    List<MasterQuipu> getMasterQuipu(@Bind("fecha") String fecha);

    @RegisterMapper(DetailQuipuMapper.class)
    @SqlQuery(" SELECT \n" +
            "DET_PREFIJO,\n" +
            "DET_NRORECIBO,\n" +
            "DET_SEDE,\n" +
            "DET_CIAQUIPU,\n" +
            "DET_CANTIDA,\n" +
            "DET_VLRUNIT,\n" +
            "DET_PCT_DESC,\n" +
            "DET_CODIGO,\n" +
            "DET_NOMBRE,\n" +
            "DET_DESCRIP,\n" +
            "DET_NUM_DOCUHIJO\n" +
            "FROM FE_VDETALLES\n" +
            "WHERE DET_PREFIJO=:PREFIJO AND DET_NRORECIBO=:NRORECIBO AND DET_SEDE=:SEDE\n" +
            "AND DET_CIAQUIPU=:CIAQUIPU AND DET_NUM_DOCUHIJO=:NUMDOCUHIJO ")
    List<DetailQuipu> getDetailsQuipu(@Bind("PREFIJO") String PREFIJO,
                                      @Bind("NRORECIBO") String NRORECIBO,
                                      @Bind("SEDE") Long SEDE,
                                      @Bind("CIAQUIPU") String CIAQUIPU,
                                      @Bind("NUMDOCUHIJO") String NUMDOCUHIJO);



    @SqlQuery(" select initcap(city_name) from city where id_city=:idcity ")
    String getCiudad(@Bind("idcity") String idcity);



    @RegisterMapper(ReportMapper.class)
    @SqlQuery("select ENC_PREFIJO PREFIJO,ENC_NRORECIBO NUMUNAL,ENC_FECEMIC FECHA,ENC_TIPODOC TIPO_DOCUMENTO,ENC_TOTAL VALOR,ENC_ESTADO ESTADO,\n" +
            "ENC_CONSDIAN CONSEC_DIAN,ENC_NUM_DOCUHIJO NUM_NOTA,MENSAJE_ERROR\n" +
            "from fe_tmaestro\n" +
            "where enc_fecemic between trunc(to_date(:date1,'dd-mm-yyyy')) \n" +
            "and trunc(to_date(:date2,'dd-mm-yyyy'))+1\n" +
            "and enc_prefijo in (<list>)")
    List<Report> report(@Bind("date1") String date1,
                        @Bind("date2") String date2,
                        @BindIn("list") List<String> list);


    @RegisterMapper(ReportMapper.class)
    @SqlQuery("select ENC_PREFIJO PREFIJO,ENC_NRORECIBO NUMUNAL,ENC_FECEMIC FECHA,ENC_TIPODOC TIPO_DOCUMENTO,ENC_TOTAL VALOR,ENC_ESTADO ESTADO,\n" +
            "ENC_CONSDIAN CONSEC_DIAN,ENC_NUM_DOCUHIJO NUM_NOTA,MENSAJE_ERROR\n" +
            "from fe_tmaestro\n" +
            "where ENC_NRORECIBO=:nrorecibo")
    List<Report> report2(@Bind("nrorecibo") String nrorecibo);



    @RegisterMapper(PrefMapper.class)
    @SqlQuery("select con_prefijo,con_ultim_asignado from fe_tconsdian")
    List<Pref> prefs();


    @SqlCall("begin poblar_tmpmaestro(:in_prefijo,:in_nrorecibo,:in_sede,:in_vigencia,:in_ciaquipu,:in_nombresede,:in_empresaquipu,:in_fecemic,:in_tipodoc,:in_formapago,:in_fecpago,:in_mediopago," +
            ":in_descuento,:in_subtotal,:in_total,:in_observaciones,:in_datos_pago,:in_ciudad_sede,:in_dir_sede,:in_telef_sede,:in_email_sede_emision,:in_clinatu,:in_clinomb," +
            ":in_clitipoid,:in_clinumid,:in_cliciudad,:in_clidir,:in_clitelelef,:in_cliemail,:in_clirespfiscal,:in_clirespiva,:in_origen,:in_forma_datos_origen, :in_num_docuhijo); end; ")
    void poblar_tmpmaestro(@Bind("in_prefijo") String in_prefijo,
                           @Bind("in_nrorecibo")String in_nrorecibo,
                           @Bind("in_sede")Long in_sede,
                           @Bind("in_vigencia")Long in_vigencia,
                           @Bind("in_ciaquipu")String in_ciaquipu,
                           @Bind("in_nombresede")String in_nombresede,
                           @Bind("in_empresaquipu")String in_empresaquipu,
                           @Bind("in_fecemic")Date in_fecemic,
                           @Bind("in_tipodoc")Long in_tipodoc,
                           @Bind("in_formapago")Long in_formapago,
                           @Bind("in_fecpago")Date in_fecpago,
                           @Bind("in_mediopago")Long in_mediopago,
                           @Bind("in_descuento")Long in_descuento,
                           @Bind("in_subtotal")Double in_subtotal,
                           @Bind("in_total")Double in_total,
                           @Bind("in_observaciones")String in_observaciones,
                           @Bind("in_datos_pago")String in_datos_pago,
                           @Bind("in_ciudad_sede")String in_ciudad_sede,
                           @Bind("in_dir_sede")String in_dir_sede,
                           @Bind("in_telef_sede")String in_telef_sede,
                           @Bind("in_email_sede_emision")String in_email_sede_emision,
                           @Bind("in_clinatu")Long in_clinatu,
                           @Bind("in_clinomb")String in_clinomb,
                           @Bind("in_clitipoid")String in_clitipoid,
                           @Bind("in_clinumid")String in_clinumid,
                           @Bind("in_cliciudad")String in_cliciudad,
                           @Bind("in_clidir")String in_clidir,
                           @Bind("in_clitelelef")String in_clitelelef,
                           @Bind("in_cliemail")String in_cliemail,
                           @Bind("in_clirespfiscal")String in_clirespfiscal,
                           @Bind("in_clirespiva")Long in_clirespiva,
                           @Bind("in_origen")Long in_origen,
                           @Bind("in_forma_datos_origen")Long in_forma_datos_origen,
                           @Bind("in_num_docuhijo")String in_num_docuhijo);


    @SqlCall(" call poblar_tmpdetalles\n" +
            "(\n" +
            "    :in_prefijo,\n" +
            "    :in_nrorecibo,\n" +
            "    :in_sede,\n" +
            "    :in_vigencia,\n" +
            "    :in_ciaquipu,\n" +
            "    :in_cantidad,\n" +
            "    :in_vlrunit,\n" +
            "    :in_pct_desc,\n" +
            "    :in_codigo,\n" +
            "    :in_nombre,\n" +
            "    :in_descrip,\n" +
            "    :in_num_docuhijo\n" +
            ") ")
    void poblar_tmpdetalles(@Bind("in_prefijo") String in_prefijo,
                            @Bind("in_nrorecibo") String in_nrorecibo,
                            @Bind("in_sede") Long in_sede,
                            @Bind("in_vigencia") Long in_vigencia,
                            @Bind("in_ciaquipu") String in_ciaquipu,
                            @Bind("in_cantidad") Double in_cantidad,
                            @Bind("in_vlrunit") Double in_vlrunit,
                            @Bind("in_pct_desc") Double in_pct_desc,
                            @Bind("in_codigo") String in_codigo,
                            @Bind("in_nombre") String in_nombre,
                            @Bind("in_descrip") String in_descrip,
                            @Bind("in_num_docuhijo") String in_num_docuhijo
    );


    @RegisterMapper(PaisDataMapper.class)
    @SqlQuery("select id_pais,pais, from PAIS_dian where id_pais=:id_pais")
    PaisData getPaisData(@Bind("id_pais") String id_pais);
}
