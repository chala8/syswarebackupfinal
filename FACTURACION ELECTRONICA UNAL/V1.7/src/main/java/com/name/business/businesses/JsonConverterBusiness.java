package com.name.business.businesses;
import com.google.common.collect.Lists;
import com.name.business.DAOs.JsonConverterDao;
import com.name.business.entities.*;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.*;
import org.w3c.dom.Document;
import org.apache.commons.codec.binary.Base64;

import javax.sound.midi.SysexMessage;
import javax.validation.constraints.Null;
import javax.ws.rs.QueryParam;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import java.net.HttpURLConnection;
import java.net.URL;


public class JsonConverterBusiness {

    private JsonConverterDao billDAO;

    //VARIABLES PARA CONEXIÓN CORREO
    static final String FROM = "soporte@tienda724.com";
    static final String FROMNAME = "Soporte Tienda 724";
    static final String SMTP_USERNAME = "soporte@tienda724.com";
    static final String SMTP_PASSWORD = "Lx3BPR5j8pdt2xmm";
    static final String HOST = "smtp.mail.us-east-1.awsapps.com";
    static final int PORT = 465;
    private static DecimalFormat df2 = new DecimalFormat("#.##");
    ///////////////////////////////////

    public JsonConverterBusiness(JsonConverterDao billDAO) {
        this.billDAO = billDAO;
    }

    boolean newMethod = false;

    public Long ExecuteProcedureFromCSV(){

        String row = "";

        BufferedReader csvReader = null;

        try {
            csvReader = new BufferedReader(new FileReader("./pruebafemaestro.CSV"));
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(";", -1);

                System.out.println("-------------------------------HEADER------------------------------");


                System.out.println("LENGTH: "+data.length);

                try {

                    String in_num_docuhijosend = data[31];
                    if (data[31] == null){
                        in_num_docuhijosend = data[31];
                    }


                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String dateInString = data[7];
                    String dateInString2 = data[10];


                    Date date1 = new Date();
                    Date date2 = new Date();
                    try {


                        java.util.Date utilDate = formatter.parse(dateInString);
                        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                        date1 = sqlDate;

                        try{
                            java.util.Date utilDate2 = formatter.parse(dateInString2);
                            java.sql.Date sqlDate2 = new java.sql.Date(utilDate2.getTime());
                            date2 = sqlDate2;
                        }catch(Exception e){
                            date2 = sqlDate;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if(newMethod){

                    billDAO.poblar_tmpmaestro(  data[0],
                                                data[1],
                                                new Long(data[2]),
                                                new Long(data[3]),
                                                data[4],
                                                data[5],
                                                data[6],
                                                date1,
                                                new Long(data[8]),
                                                new Long(data[9]),
                                                date2,
                                                new Long(data[11]),
                                                new Long(data[12]),
                                                new Double(data[13]),
                                                new Double(data[14]),
                                                data[15],
                                                data[16],
                                                data[17],
                                                data[18],
                                                data[19],
                                                data[20],
                                                new Long(data[21]),
                                                data[22],
                                                data[23],
                                                data[24],
                                                data[25],
                                                data[26],
                                                data[27],
                                                data[28],
                                                data[29],
                                                new Long(data[30]),
                                                new Long(1),
                                                new Long(1),
                                                in_num_docuhijosend
                                                );


                        }else{
                    System.out.println("URL:"+"http://localhost:8680/v1/unalfe/master?in_prefijo="+data[0]
                            +"&in_nrorecibo="+data[1]
                            +"&in_sede="+data[2]
                            +"&in_vigencia="+data[3]
                            +"&in_ciaquipu="+data[4]
                            +"&in_nombresede="+data[5]
                            +"&in_empresaquipu="+data[6]
                            +"&in_fecemic="+data[7]
                            +"&in_tipodoc="+data[8]
                            +"&in_formapago="+data[9]
                            +"&in_fecpago="+data[10]
                            +"&in_mediopago="+data[11]
                            +"&in_descuento="+data[12]
                            +"&in_subtotal="+data[13]
                            +"&in_total="+data[14]
                            +"&in_observaciones="+data[15]
                            +"&in_datos_pago="+data[16]
                            +"&in_ciudad_sede="+data[17]
                            +"&in_dir_sede="+data[18]
                            +"&in_telef_sede="+data[19]
                            +"&in_email_sede_emision="+data[20]
                            +"&in_clinatu="+data[21]
                            +"&in_clinomb="+data[22]
                            +"&in_clitipoid="+data[23]
                            +"&in_clinumid="+data[24]
                            +"&in_cliciudad="+data[25]
                            +"&in_clidir="+data[26]
                            +"&in_clitelelef="+data[27]
                            +"&in_cliemail="+data[28]
                            +"&in_clirespfiscal="+data[29]
                            +"&in_clirespiva="+data[30]
                            +"&in_origen="+1
                            +"&in_forma_datos_origen="+1
                            +"&in_num_docuhijo="+data[31]);
                    URL url = new URL("http://localhost:8680/v1/unalfe/master?in_prefijo="+data[0].replaceAll(" ","%20")
                            +"&in_nrorecibo="+data[1].replaceAll(" ","%20")
                            +"&in_sede="+data[2].replaceAll(" ","%20")
                            +"&in_vigencia="+data[3].replaceAll(" ","%20")
                            +"&in_ciaquipu="+data[4].replaceAll(" ","%20")
                            +"&in_nombresede="+data[5].replaceAll(" ","%20")
                            +"&in_empresaquipu="+data[6].replaceAll(" ","%20")
                            +"&in_fecemic="+data[7].replaceAll(" ","%20")
                            +"&in_tipodoc="+data[8].replaceAll(" ","%20")
                            +"&in_formapago="+data[9].replaceAll(" ","%20")
                            +"&in_fecpago="+data[10].replaceAll(" ","%20")
                            +"&in_mediopago="+data[11].replaceAll(" ","%20")
                            +"&in_descuento="+data[12].replaceAll(" ","%20")
                            +"&in_subtotal="+data[13].replaceAll(" ","%20")
                            +"&in_total="+data[14].replaceAll(" ","%20")
                            +"&in_observaciones="+data[15].replaceAll(" ","%20")
                            +"&in_datos_pago="+data[16].replaceAll(" ","%20")
                            +"&in_ciudad_sede="+data[17].replaceAll(" ","%20")
                            +"&in_dir_sede="+data[18].replaceAll(" ","%20")
                            +"&in_telef_sede="+data[19].replaceAll(" ","%20")
                            +"&in_email_sede_emision="+data[20].replaceAll(" ","%20")
                            +"&in_clinatu="+data[21].replaceAll(" ","%20")
                            +"&in_clinomb="+data[22].replaceAll(" ","%20")
                            +"&in_clitipoid="+data[23].replaceAll(" ","%20")
                            +"&in_clinumid="+data[24].replaceAll(" ","%20")
                            +"&in_cliciudad="+data[25].replaceAll(" ","%20")
                            +"&in_clidir="+data[26].replaceAll(" ","%20")
                            +"&in_clitelelef="+data[27].replaceAll(" ","%20")
                            +"&in_cliemail="+data[28].replaceAll(" ","%20")
                            +"&in_clirespfiscal="+data[29].replaceAll(" ","%20")
                            +"&in_clirespiva="+data[30].replaceAll(" ","%20")
                            +"&in_origen="+1
                            +"&in_forma_datos_origen="+1
                            +"&in_num_docuhijo="+data[31]);//your url i.e fetch data from .
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");

                    conn.setDoOutput(true);
                    OutputStream os = conn.getOutputStream();
                    os.write((new String("")).getBytes());
                    os.flush();
                    os.close();


                    System.out.println("RESPONSE CODE: "+conn.getResponseCode());
                    System.out.println("MESSAGE CODE: "+conn.getResponseMessage());

                    if (conn.getResponseCode() != 200) {
                        throw new RuntimeException("Failed : HTTP Error code : "
                                + conn.getResponseCode());
                    }
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    BufferedReader br = new BufferedReader(in);
                    String output;
                    while ((output = br.readLine()) != null) {
                        System.out.println("OUTPUT: "+output);
                    }
                    conn.disconnect();
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    return new Long(0);
                }
                // do something with the data
            }
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }




        try {
            csvReader = new BufferedReader(new FileReader("./pruebafedetalle.csv"));
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(";");

                System.out.println("-------------------------------DETAIL------------------------------");

                System.out.println("LENGTH: "+data.length);

                try {


                    System.out.println("URL:"+"http://localhost:8680/v1/unalfe/detail?in_prefijo="+data[0]
                            +"&in_nrorecibo="+data[1]
                            +"&in_sede="+data[2]
                            +"&in_vigencia="+2021
                            +"&in_ciaquipu="+data[3]
                            +"&in_cantidad="+data[4]
                            +"&in_vlrunit="+data[5]
                            +"&in_pct_desc="+data[6]
                            +"&in_codigo="+data[7]
                            +"&in_nombre="+data[8]
                            +"&in_descrip="+data[9]
                            +"&in_num_docuhijo="+data[10]);
                    URL url = new URL("http://localhost:8680/v1/unalfe/detail?in_prefijo="+data[0].replaceAll(" ","%20")
                            +"&in_nrorecibo="+data[1].replaceAll(" ","%20")
                            +"&in_sede="+data[2].replaceAll(" ","%20")
                            +"&in_vigencia="+2021
                            +"&in_ciaquipu="+data[3].replaceAll(" ","%20")
                            +"&in_cantidad="+data[4].replaceAll(" ","%20")
                            +"&in_vlrunit="+data[5].replaceAll(" ","%20")
                            +"&in_pct_desc="+data[6].replaceAll(" ","%20")
                            +"&in_codigo="+data[7].replaceAll(" ","%20").replaceAll("","01")
                            +"&in_nombre="+data[8].replaceAll(" ","%20")
                            +"&in_descrip="+data[9].replaceAll(" ","%20")
                            +"&in_num_docuhijo="+data[10].replaceAll(" ","%20"));//your url i.e fetch data from .
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");

                    conn.setDoOutput(true);
                    OutputStream os = conn.getOutputStream();
                    os.write((new String("")).getBytes());
                    os.flush();
                    os.close();


                    System.out.println("RESPONSE CODE: "+conn.getResponseCode());
                    System.out.println("MESSAGE CODE: "+conn.getResponseMessage());

                    if (conn.getResponseCode() != 200) {
                        throw new RuntimeException("Failed : HTTP Error code : "
                                + conn.getResponseCode());
                    }
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    BufferedReader br = new BufferedReader(in);
                    String output;
                    while ((output = br.readLine()) != null) {
                        System.out.println("OUTPUT: "+output);
                    }
                    conn.disconnect();

                }catch(Exception e){
                    e.printStackTrace();
                    return new Long(0);
                }
            }
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        return new Long(1);

    }



    private static String[] insert(String[] a, String key, int index) {
        String[] result = new String[a.length + 1];

        System.arraycopy(a, 0, result, 0, index);
        result[index] = key;
        System.arraycopy(a, index, result, index + 1, a.length - index);

        return result;
    }





    public Long ExecuteProcedureFromView(String nro_factura){

        String row = "";

        BufferedReader csvReader = null;
        try {
                URL urlGet = new URL("http://localhost:8580/v1/unalfe/master?enc_nrorecibo="+nro_factura);//your url i.e fetch data from .
                HttpURLConnection conn2 = (HttpURLConnection) urlGet.openConnection();
                conn2.setRequestMethod("GET");
                conn2.setRequestProperty("Accept", "application/json");

                if (conn2.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP Error code : "
                            + conn2.getResponseCode());
                }

                InputStreamReader in = new InputStreamReader(conn2.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;
                JSONObject data_obj = new JSONObject();
                while ((output = br.readLine()) != null) {
                    System.out.println(output);
                    data_obj = new JSONObject(output.substring(1,output.length()-1));
                }

                System.out.println(data_obj.get("enc_EMPRESAQUIPU"));


                System.out.println("-------------------------------HEADER------------------------------");


                try {



                    String in_num_docuhijosend = data_obj.get("enc_NRORECIBO").toString().replace(" ","%20");
                    if (data_obj.get("enc_NRORECIBO").toString().replace(" ","%20") == null){
                        in_num_docuhijosend = data_obj.get("enc_NRORECIBO").toString().replace(" ","%20");
                    }


                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String dateInString = data_obj.get("enc_FECEMIC").toString().replace(" ","%20");
                    String dateInString2 = data_obj.get("enc_FECPAGO").toString().replace(" ","%20");


                    Date date1 = new Date();
                    Date date2 = new Date();
                    try {


                        java.util.Date utilDate = formatter.parse(dateInString);
                        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                        date1 = sqlDate;

                        try{
                            java.util.Date utilDate2 = formatter.parse(dateInString2);
                            java.sql.Date sqlDate2 = new java.sql.Date(utilDate2.getTime());
                            date2 = sqlDate2;
                        }catch(Exception e){
                            date2 = sqlDate;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if(newMethod){
                    billDAO.poblar_tmpmaestro( data_obj.get("enc_PREFIJO").toString().replace(" ","%20"),
                            data_obj.get("enc_NRORECIBO").toString().replace(" ","%20"),
                            new Long(data_obj.get("enc_SEDE").toString().replace(" ","%20")),
                            new Long(data_obj.get("enc_VIGENCIA").toString().replace(" ","%20")),
                            data_obj.get("enc_NITEMES").toString().replace(" ","%20"),
                            data_obj.get("enc_NOMBRESEDE").toString().replace(" ","%20"),
                            data_obj.get("enc_EMPRESAQUIPU").toString().replace(" ","%20"),
                            date1,
                            new Long(data_obj.get("enc_TIPODOC").toString().replace(" ","%20")),
                            new Long(data_obj.get("enc_FORMAPAGO").toString().replace(" ","%20")),
                            date2,
                            new Long(data_obj.get("enc_MEDIOPAGO").toString().replace(" ","%20")),
                            new Long(data_obj.get("enc_DESCUENTO").toString().replace(" ","%20")),
                            new Double(data_obj.get("enc_SUBTOTAL").toString().replace(" ","%20")),
                            new Double(data_obj.get("enc_TOTAL").toString().replace(" ","%20")),
                            data_obj.get("enc_OBSERVACIONES").toString().replace(" ","%20"),
                            data_obj.get("enc_DATOS_PAGO").toString().replace(" ","%20"),
                            data_obj.get("enc_CIUDAD_SEDE").toString().replace(" ","%20"),
                            data_obj.get("enc_DIR_SEDE").toString().replace(" ","%20"),
                            data_obj.get("enc_TELEF_SEDE").toString().replace(" ","%20"),
                            data_obj.get("enc_EMAIL_SEDE_EMISION").toString().replace(" ","%20"),
                            new Long(data_obj.get("enc_CLINATU").toString().replace(" ","%20")),
                            data_obj.get("enc_CLINOMB").toString().replace(" ","%20"),
                            data_obj.get("enc_CLI_TIPOID").toString().replace(" ","%20"),
                            data_obj.get("enc_CLI_NUMID").toString().replace(" ","%20"),
                            data_obj.get("enc_CLICIUDAD").toString().replace(" ","%20"),
                            data_obj.get("enc_CLIDIR").toString().replace(" ","%20"),
                            data_obj.get("enc_CLITELEF").toString().replace(" ","%20"),
                            data_obj.get("enc_CLIEMAIL").toString().replace(" ","%20"),
                            data_obj.get("enc_CLIRESPFIS").toString().replace(" ","%20"),
                            new Long(data_obj.get("enc_CLIRESPIVA").toString().replace(" ","%20")),
                            new Long(1),
                            new Long(1),
                            in_num_docuhijosend
                            );


                        }else{

                    System.out.println("URL:"+"http://localhost:8680/v1/unalfe/master?in_prefijo="+data_obj.get("enc_PREFIJO").toString().replace(" ","%20")
                            +"&in_nrorecibo="+data_obj.get("enc_NRORECIBO").toString().replace(" ","%20")
                            +"&in_sede="+data_obj.get("enc_SEDE").toString().replace(" ","%20")
                            +"&in_vigencia="+data_obj.get("enc_VIGENCIA").toString().replace(" ","%20")
                            +"&in_ciaquipu="+data_obj.get("enc_NITEMES").toString().replace(" ","%20")
                            +"&in_nombresede="+data_obj.get("enc_NOMBRESEDE").toString().replace(" ","%20")
                            +"&in_empresaquipu="+data_obj.get("enc_EMPRESAQUIPU").toString().replace(" ","%20")
                            +"&in_fecemic="+data_obj.get("enc_FECEMIC").toString().replace(" ","%20")
                            +"&in_tipodoc="+data_obj.get("enc_TIPODOC").toString().replace(" ","%20")
                            +"&in_formapago="+data_obj.get("enc_FORMAPAGO").toString().replace(" ","%20")
                            +"&in_fecpago="+data_obj.get("enc_FECPAGO").toString().replace(" ","%20")
                            +"&in_mediopago="+data_obj.get("enc_MEDIOPAGO").toString().replace(" ","%20")
                            +"&in_descuento="+data_obj.get("enc_DESCUENTO").toString().replace(" ","%20")
                            +"&in_subtotal="+data_obj.get("enc_SUBTOTAL").toString().replace(" ","%20")
                            +"&in_total="+data_obj.get("enc_TOTAL").toString().replace(" ","%20")
                            +"&in_observaciones="+data_obj.get("enc_OBSERVACIONES").toString().replace(" ","%20")
                            +"&in_datos_pago="+data_obj.get("enc_DATOS_PAGO").toString().replace(" ","%20")
                            +"&in_ciudad_sede="+data_obj.get("enc_CIUDAD_SEDE").toString().replace(" ","%20")
                            +"&in_dir_sede="+data_obj.get("enc_DIR_SEDE").toString().replace(" ","%20")
                            +"&in_telef_sede="+data_obj.get("enc_TELEF_SEDE").toString().replace(" ","%20")
                            +"&in_email_sede_emision="+data_obj.get("enc_EMAIL_SEDE_EMISION").toString().replace(" ","%20")
                            +"&in_clinatu="+data_obj.get("enc_CLINATU").toString().replace(" ","%20")
                            +"&in_clinomb="+data_obj.get("enc_CLINOMB").toString().replace(" ","%20")
                            +"&in_clitipoid="+data_obj.get("enc_CLI_TIPOID").toString().replace(" ","%20")
                            +"&in_clinumid="+data_obj.get("enc_CLI_NUMID").toString().replace(" ","%20")
                            +"&in_cliciudad="+data_obj.get("enc_CLICIUDAD").toString().replace(" ","%20")
                            +"&in_clidir="+data_obj.get("enc_CLIDIR").toString().replace(" ","%20")
                            +"&in_clitelelef="+data_obj.get("enc_CLITELEF").toString().replace(" ","%20")
                            +"&in_cliemail="+data_obj.get("enc_CLIEMAIL").toString().replace(" ","%20")
                            +"&in_clirespfiscal="+data_obj.get("enc_CLIRESPFIS").toString().replace(" ","%20")
                            +"&in_clirespiva="+data_obj.get("enc_CLIRESPIVA").toString().replace(" ","%20")
                            +"&in_origen="+1
                            +"&in_forma_datos_origen="+1
                            +"&in_num_docuhijo="+data_obj.get("enc_NRORECIBO").toString().replace(" ","%20"));
                    URL url = new URL("http://localhost:8680/v1/unalfe/master?in_prefijo="+data_obj.get("enc_PREFIJO").toString().replace(" ","%20")
                            +"&in_nrorecibo="+data_obj.get("enc_NRORECIBO").toString().replace(" ","%20")
                            +"&in_sede="+data_obj.get("enc_SEDE").toString().replace(" ","%20")
                            +"&in_vigencia="+data_obj.get("enc_VIGENCIA").toString().replace(" ","%20")
                            +"&in_ciaquipu="+data_obj.get("enc_NITEMES").toString().replace(" ","%20")
                            +"&in_nombresede="+data_obj.get("enc_NOMBRESEDE").toString().replace(" ","%20")
                            +"&in_empresaquipu="+data_obj.get("enc_EMPRESAQUIPU").toString().replace(" ","%20")
                            +"&in_fecemic="+data_obj.get("enc_FECEMIC").toString().replace(" ","%20")
                            +"&in_tipodoc="+data_obj.get("enc_TIPODOC").toString().replace(" ","%20")
                            +"&in_formapago="+data_obj.get("enc_FORMAPAGO").toString().replace(" ","%20")
                            +"&in_fecpago="+data_obj.get("enc_FECPAGO").toString().replace(" ","%20")
                            +"&in_mediopago="+data_obj.get("enc_MEDIOPAGO").toString().replace(" ","%20")
                            +"&in_descuento="+data_obj.get("enc_DESCUENTO").toString().replace(" ","%20")
                            +"&in_subtotal="+data_obj.get("enc_SUBTOTAL").toString().replace(" ","%20")
                            +"&in_total="+data_obj.get("enc_TOTAL").toString().replace(" ","%20")
                            +"&in_observaciones="+data_obj.get("enc_OBSERVACIONES").toString().replace(" ","%20")
                            +"&in_datos_pago="+data_obj.get("enc_DATOS_PAGO").toString().replace(" ","%20")
                            +"&in_ciudad_sede="+data_obj.get("enc_CIUDAD_SEDE").toString().replace(" ","%20")
                            +"&in_dir_sede="+data_obj.get("enc_DIR_SEDE").toString().replace(" ","%20")
                            +"&in_telef_sede="+data_obj.get("enc_TELEF_SEDE").toString().replace(" ","%20")
                            +"&in_email_sede_emision="+data_obj.get("enc_EMAIL_SEDE_EMISION").toString().replace(" ","%20")
                            +"&in_clinatu="+data_obj.get("enc_CLINATU").toString().replace(" ","%20")
                            +"&in_clinomb="+data_obj.get("enc_CLINOMB").toString().replace(" ","%20")
                            +"&in_clitipoid="+data_obj.get("enc_CLI_TIPOID").toString().replace(" ","%20")
                            +"&in_clinumid="+data_obj.get("enc_CLI_NUMID").toString().replace(" ","%20")
                            +"&in_cliciudad="+data_obj.get("enc_CLICIUDAD").toString().replace(" ","%20")
                            +"&in_clidir="+data_obj.get("enc_CLIDIR").toString().replace(" ","%20")
                            +"&in_clitelelef="+data_obj.get("enc_CLITELEF").toString().replace(" ","%20")
                            +"&in_cliemail="+data_obj.get("enc_CLIEMAIL").toString().replace(" ","%20")
                            +"&in_clirespfiscal="+data_obj.get("enc_CLIRESPFIS").toString().replace(" ","%20")
                            +"&in_clirespiva="+data_obj.get("enc_CLIRESPIVA").toString().replace(" ","%20")
                            +"&in_origen="+1
                            +"&in_forma_datos_origen="+1
                            +"&in_num_docuhijo="+data_obj.get("enc_NRORECIBO").toString().replace(" ","%20"));//your url i.e fetch data from .
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");

                    conn.setDoOutput(true);
                    OutputStream os = conn.getOutputStream();
                    os.write((new String("")).getBytes());
                    os.flush();
                    os.close();


                    System.out.println("RESPONSE CODE: "+conn.getResponseCode());
                    System.out.println("MESSAGE CODE: "+conn.getResponseMessage());

                    if (conn.getResponseCode() != 200) {
                        throw new RuntimeException("Failed : HTTP Error code : "
                                + conn.getResponseCode());
                    }
                    InputStreamReader in2 = new InputStreamReader(conn.getInputStream());
                    BufferedReader br2 = new BufferedReader(in2);
                    String output2;
                    while ((output2 = br2.readLine()) != null) {
                        System.out.println("OUTPUT: "+output2);
                    }
                    conn.disconnect();
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    return new Long(0);
                }
                // do something with the data

        } catch (Exception e) {
            e.printStackTrace();
        }



        try {
                URL urlGet = new URL("http://localhost:8580/v1/unalfe/detail?det_nrorecibo="+nro_factura);//your url i.e fetch data from .
                HttpURLConnection conn2 = (HttpURLConnection) urlGet.openConnection();
                conn2.setRequestMethod("GET");
                conn2.setRequestProperty("Accept", "application/json");

                if (conn2.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP Error code : "
                            + conn2.getResponseCode());
                }

                InputStreamReader in = new InputStreamReader(conn2.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;
                JSONObject data_obj = new JSONObject();
                while ((output = br.readLine()) != null) {
                    System.out.println(output);
                    data_obj = new JSONObject(output.substring(1,output.length()-1));
                }

                System.out.println("-------------------------------DETAIL------------------------------");

                List data = new ArrayList();
                 data.add("a");
                System.out.println("LENGTH: "+data.size());

                try {


                    URL url = new URL("http://localhost:8680/v1/unalfe/detail?in_prefijo="+data_obj.get("det_PREFIJO").toString().replaceAll(" ","%20")
                            +"&in_nrorecibo="+data_obj.get("det_NRORECIBO").toString().replaceAll(" ","%20")
                            +"&in_sede="+data_obj.get("det_SEDE").toString().replaceAll(" ","%20")
                            +"&in_vigencia="+2021
                            +"&in_ciaquipu="+data_obj.get("det_NITEMES").toString().replaceAll(" ","%20")
                            +"&in_cantidad="+data_obj.get("det_CANTIDA").toString().replaceAll(" ","%20")
                            +"&in_vlrunit="+data_obj.get("det_VLRUNIT").toString().replaceAll(" ","%20")
                            +"&in_pct_desc="+data_obj.get("det_PCT_DESC").toString().replaceAll(" ","%20")
                            +"&in_codigo="+data_obj.get("det_CODIGO").toString().replaceAll(" ","%20")
                            +"&in_nombre="+data_obj.get("det_NOMBRE").toString().replaceAll(" ","%20")
                            +"&in_descrip="+data_obj.get("det_DESCRIP").toString().replaceAll(" ","%20")
                            +"&in_num_docuhijo="+data_obj.get("det_NRORECIBO").toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json");

                    conn.setDoOutput(true);
                    OutputStream os = conn.getOutputStream();
                    os.write((new String("")).getBytes());
                    os.flush();
                    os.close();


                    System.out.println("RESPONSE CODE: "+conn.getResponseCode());
                    System.out.println("MESSAGE CODE: "+conn.getResponseMessage());

                    if (conn.getResponseCode() != 200) {
                        throw new RuntimeException("Failed : HTTP Error code : "
                                + conn.getResponseCode());
                    }
                    InputStreamReader in3 = new InputStreamReader(conn.getInputStream());
                    BufferedReader br3 = new BufferedReader(in3);
                    String output3;
                    while ((output3 = br3.readLine()) != null) {
                        System.out.println("OUTPUT: "+output3);
                    }
                    conn.disconnect();

                }catch(Exception e){
                    e.printStackTrace();
                    return new Long(0);
                }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return new Long(1);

    }



    public Long asigna_consecutivo_dian(){
        try{
            billDAO.asigna_consecutivo_dian();
            return new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }

    }



    public Long eliminar_temporales(){
        try{
            billDAO.eliminar_temporales();
            return new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }

    }



    public Long JsonConverterMethodInvoice(Long env, Master tmpMaster){


            String ciudadSegunCodigo = "Bogotá";
            String idCiudadCliente = "11001";
            String ciudadCliente = "Bogotá";
            String countrySubEntity = "Bogotá";

            if(tmpMaster.getENC_CIUDAD_SEDE().length()<=3){
                PaisData data = billDAO.getPaisData(tmpMaster.getENC_CIUDAD_SEDE());
                ciudadSegunCodigo = data.getPais();idCiudadCliente = data.getId_pais().toString();ciudadCliente = data.getPais();countrySubEntity = data.getPais();
            }else{
                if(tmpMaster.getENC_CIUDAD_SEDE().length()<5){tmpMaster.setENC_CIUDAD_SEDE("0"+tmpMaster.getENC_CIUDAD_SEDE());}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("11001")){ciudadSegunCodigo = "Bogotá";idCiudadCliente = "11001";ciudadCliente = "Bogotá";countrySubEntity = "Bogotá";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("05001")){ciudadSegunCodigo = "Medellín";idCiudadCliente = "05001";ciudadCliente = "Medellín";countrySubEntity = "Antioquia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("17001")){ciudadSegunCodigo = "Manizales";idCiudadCliente = "17001";ciudadCliente = "Manizales";countrySubEntity = "Caldas";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("76520")){ciudadSegunCodigo = "Palmira";idCiudadCliente = "76520";ciudadCliente = "Palmira";countrySubEntity = "Valle";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("88001")){ciudadSegunCodigo = "San Andrés";idCiudadCliente = "88001";ciudadCliente = "San Andrés";countrySubEntity = "San Andrés";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("91001")){ciudadSegunCodigo = "Leticia";idCiudadCliente = "91001";ciudadCliente = "Leticia";countrySubEntity = "Amazonas";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("81001")){ciudadSegunCodigo = "Arauca";idCiudadCliente = "81001";ciudadCliente = "Arauca";countrySubEntity = "Arauca";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("52835")){ciudadSegunCodigo = "Tumaco";idCiudadCliente = "52835";ciudadCliente = "Tumaco";countrySubEntity = "Nariño";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("20621")){ciudadSegunCodigo = "La Paz";idCiudadCliente = "20621";ciudadCliente = "La Paz";countrySubEntity = "Cesar";}
            }


        idCiudadCliente = tmpMaster.getENC_CLICIUDAD();

            try{
                idCiudadCliente = tmpMaster.getENC_CLICIUDAD();
                if(idCiudadCliente.length()==4){
                    idCiudadCliente = "0"+idCiudadCliente;
                }
                ciudadCliente = billDAO.getCiudad(idCiudadCliente);
                String subentityCode = idCiudadCliente.substring(0,2);
            }catch(Exception e){
                idCiudadCliente = tmpMaster.getENC_CIUDAD_SEDE();
                ciudadCliente = ciudadSegunCodigo;
            }

        System.out.println("CIUDAD CLIENTE: " + idCiudadCliente);

            int profileExecutionID = 2;
            String pref = "SETT";
            String pruebas = "https://apivp.efacturacadena.com/staging/vp/documentos/proceso/sincrono";
            String produccion = "https://apivp.efacturacadena.com/v1/vp/documentos/proceso/sincrono";
            String urlConsumption = pruebas;

            if(env == 1){
                profileExecutionID = 1;
                pref = tmpMaster.getENC_PREFIJO();
                urlConsumption = produccion;
            }

            Double SubTotal = tmpMaster.getENC_SUBTOTAL();
            Double discountvalue = tmpMaster.getENC_DESCUENTO();
            Double subtotDiv = new Double(1);
            if(!SubTotal.toString().equals("0")){
                subtotDiv = SubTotal;
            }
            Double discountPercentage = new Double((discountvalue * 100)/subtotDiv);

            if(Double.isNaN(discountPercentage)){
                discountPercentage = new Double(0);
            }

            String fechaPago = "";

            if(tmpMaster.getENC_FECPAGO()!=null && tmpMaster.getENC_FECPAGO()!="") {
            fechaPago = tmpMaster.getENC_FECPAGO();
            }else{
                fechaPago = tmpMaster.getENC_FECEMIC();
            }

            String obs = "";

            try {
                obs = tmpMaster.getENC_OBSERVACIONES().replaceAll("No\\.","#").replaceAll("NO\\.","#");
            }catch(Exception e){
                obs = "";
            }


            String cli_dir = "";

            try {
                cli_dir = tmpMaster.getENC_CLIDIR().replaceAll("No\\.","#").replaceAll("NO\\.","#");
            }catch(Exception e){
                cli_dir = "";
            }


            String cli_email = "";

            try {
                cli_email = tmpMaster.getENC_CLIEMAIL().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#");
            }catch(Exception e){
                cli_email = "";
            }


            String notaac = "";
            String orderReference = "";


            int indexRef1 = obs.indexOf("[");
            if(indexRef1==-1){
                orderReference = "";
            }else{
                int indexRef2 = obs.lastIndexOf("[");
                orderReference = obs.substring(indexRef1,indexRef2);
            }

            int indexNote1 = obs.indexOf("_");
            if(indexNote1==-1){
                notaac = "";
            }else{
                int indexNote2 = obs.lastIndexOf("_");
                notaac = obs.substring(indexNote1,indexNote2);
            }

        if(tmpMaster.getENC_CLITIPOID().toString().equals("42")){
            System.out.println("International");
            idCiudadCliente = tmpMaster.getENC_CLICIUDAD();
            ciudadCliente = "internacional";
            countrySubEntity = "01";
        }


            String invoiceRawXmlHead1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<Invoice xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2\" xmlns:cac=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:cbc=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ext=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:sts=\"dian:gov:co:facturaelectronica:Structures-2-1\" xmlns:xades=\"http://uri.etsi.org/01903/v1.3.2#\" xmlns:xades141=\"http://uri.etsi.org/01903/v1.4.1#\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2     http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-Invoice-2.1.xsd\">\n" +
                    "\t<cbc:CustomizationID>10</cbc:CustomizationID>\n" +
                    "\t<cbc:ProfileExecutionID>"+profileExecutionID+"</cbc:ProfileExecutionID>\n" +
                    "\t<cbc:ID>"+pref+tmpMaster.getENC_CONSDIAN()+"</cbc:ID>\n" +
                    "\t<cbc:IssueDate>"+tmpMaster.getENC_FECEMIC()+"</cbc:IssueDate>\n" +
                    "\t<cbc:IssueTime>00:00:00-05:00</cbc:IssueTime>\n" +
                    "\t<cbc:DueDate>"+fechaPago+"</cbc:DueDate>\n" +
                    "\t<cbc:InvoiceTypeCode>01</cbc:InvoiceTypeCode>\n" +
                    "\t<cbc:Note>"+obs.replaceAll("<","").replaceAll(">","")+"</cbc:Note>\n" +
                    "\t<cbc:DocumentCurrencyCode>COP</cbc:DocumentCurrencyCode>\n" +
                    "\t<cbc:LineCountNumeric>"+tmpMaster.getENC_NRO_ITEMES()+"</cbc:LineCountNumeric>\n" +
                    "\t<cac:OrderReference>\n" +
                    "\t\t<cbc:ID>"+orderReference+"</cbc:ID>\n" +
                    "\t</cac:OrderReference>\n" +
                    "\t<cac:AccountingSupplierParty>\n" +
                    "\t\t<cbc:AdditionalAccountID>1</cbc:AdditionalAccountID>\n" +
                    "\t\t<cac:Party>\n" +
                    "\t\t\t<cac:PartyName>\n" +
                    "\t\t\t\t<cbc:Name>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:Name>\n" +
                    "\t\t\t</cac:PartyName>\n" +
                    "\t\t\t<cac:PhysicalLocation>\n" +
                    "\t\t\t\t<cac:Address>\n" +
                    "\t\t\t\t\t<cbc:ID>"+tmpMaster.getENC_CIUDAD_SEDE()+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>"+ciudadSegunCodigo+"</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>"+countrySubEntity+"</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>"+tmpMaster.getENC_CIUDAD_SEDE().substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>"+tmpMaster.getENC_DIR_SEDE().replaceAll("No\\.","#").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:Address>\n" +
                    "\t\t\t</cac:PhysicalLocation>\n" +
                    "\t\t\t<cac:PartyTaxScheme>\n" +
                    "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                    "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                    "\t\t\t\t<cbc:TaxLevelCode listName=\"49\">O-13;O-23</cbc:TaxLevelCode>\n" +
                    "\t\t\t\t<cac:RegistrationAddress>\n" +
                    "\t\t\t\t\t<cbc:ID>11001</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>Bogotá</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:PostalZone>000000</cbc:PostalZone>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>Bogotá</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>11</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>CR 45   26   85 OF 481 ED URIEL GUTIERREZ</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:RegistrationAddress>\n" +
                    "\t\t\t\t<cac:TaxScheme>\n" +
                    "\t\t\t\t\t<cbc:ID></cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:Name></cbc:Name>\n" +
                    "\t\t\t\t</cac:TaxScheme>\n" +
                    "\t\t\t</cac:PartyTaxScheme>\n" +
                    "\t\t\t<cac:PartyLegalEntity>\n" +
                    "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                    "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                    "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t\t\t<cbc:ID>"+pref+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:Name>12345</cbc:Name>\n" +
                    "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t</cac:PartyLegalEntity>\n" +
                    "\t\t\t<cac:Contact>\n" +
                    "\t\t\t\t<cbc:Name></cbc:Name>\n" +
                    "\t\t\t\t<cbc:Telephone>3165000</cbc:Telephone>\n" +
                    "\t\t\t\t<cbc:ElectronicMail>efactura_nal@unal.edu.co</cbc:ElectronicMail>\n" +
                    "\t\t\t</cac:Contact>\n" +
                    "\t\t</cac:Party>\n" +
                    "\t</cac:AccountingSupplierParty>\n" +
                    "\t<cac:AccountingCustomerParty>\n"+
                    "\t\t<cbc:AdditionalAccountID>"+tmpMaster.getENC_CLINATU()+"</cbc:AdditionalAccountID>\n"+
                    "\t\t<cac:Party>\t\n" +
                    "\t\t\t<cac:PartyIdentification>\t\n" +
                    "\t\t\t\t<cbc:ID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\"31\">"+tmpMaster.getENC_CLINUMID()+"</cbc:ID>\n" +
                    "\t\t\t</cac:PartyIdentification>\n" +
                    "\t\t\t<cac:PartyName>\n" +
                    "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("_","Y").replaceAll("<","").replaceAll(">","")+"</cbc:Name>\n" +
                    "\t\t\t</cac:PartyName>\n" +
                    "\t\t\t<cac:PhysicalLocation>\n" +
                    "\t\t\t\t<cac:Address>\n" +
                    "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadCliente+"</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>"+idCiudadCliente.substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>"+cli_dir.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:Address>\n" +
                    "\t\t\t</cac:PhysicalLocation>\n" +
                    "\t\t\t<cac:PartyTaxScheme>\n" +
                    "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:RegistrationName>\n";


                    String paymentID = "Efectivo";

                    if(tmpMaster.getENC_MEDIOPAGO().toString().equals("48")){
                        paymentID = "Tarjeta Crédito";
                    }

                    if(tmpMaster.getENC_MEDIOPAGO().toString().equals("49")){
                        paymentID = "Tarjeta Débito";
                    }

                    if(tmpMaster.getENC_MEDIOPAGO().toString().equals("47")){
                        paymentID = "Transferencia Débito Bancaria";
                    }


                    String cliRespFiscal = tmpMaster.getENC_CLIRESPFISCAL();

                    try{
                        if(tmpMaster.getENC_CLIRESPFISCAL().equals("0") || tmpMaster.getENC_CLIRESPFISCAL() == null || tmpMaster.getENC_CLIRESPFISCAL().equals("null")||tmpMaster.getENC_CLIRESPFISCAL().equals("")){
                            cliRespFiscal = "R-99-PN";
                        }
                    }catch(Exception e){
                        cliRespFiscal = "R-99-PN";
                    }


                    String TaxLevelCode = "No Aplica";

                    try{
                        if(tmpMaster.getENC_CLIRESPIVA()== new Long(0)){
                            TaxLevelCode = "49";
                        }else{
                            TaxLevelCode = tmpMaster.getENC_CLIRESPIVA().toString();
                        }
                    }catch(Exception e){
                        TaxLevelCode = "49";
                    }

        String digitoVerificacion = "";
        String invoiceRawXmlHead = "";

        Boolean doesHttp = true;
        Boolean doesHttp2 = false;

        System.out.println("THIS IS CLINATU: "+tmpMaster.getENC_CLINATU());
        String schemeName = tmpMaster.getENC_CLITIPOID();
        if(schemeName.toString().equals("42")){
            System.out.println("International");
            digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
        }else{
            System.out.println("national");
            if(tmpMaster.getENC_CLINATU().toString().equals("1")){
                System.out.println("IM JURIDIC");
                try{
                    String number =  tmpMaster.getENC_CLINUMID().split("-")[0];
                    String digit = tmpMaster.getENC_CLINUMID().split("-")[1];
                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\""+digit+"\"  schemeName=\""+schemeName+"\">"+number+"</cbc:CompanyID>\n";
                }catch(Exception  e){
                    billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NO SE ENCONTRO DIGITO DE VERIFICACION EN EL DOCUMENTO. SYSWARE.");
                    doesHttp = false;
                }
            }else{
                System.out.println("IM NATURAL");
                digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
            }
        }


                    String invoiceRawXmlHead12 =  "\t\t\t\t<cbc:TaxLevelCode listName=\""+TaxLevelCode+"\">"+cliRespFiscal+"</cbc:TaxLevelCode>\n" +
                    "\t\t\t\t<cac:RegistrationAddress>\n" +
                    "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>"+countrySubEntity+"</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>"+tmpMaster.getENC_CIUDAD_SEDE().substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>"+cli_dir.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:RegistrationAddress>\n" +
                    "\t\t\t\t<cac:TaxScheme>\n" +
                    "\t\t\t\t\t<cbc:ID>01</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:Name>IVA</cbc:Name>\n" +
                    "\t\t\t\t</cac:TaxScheme>\n" +
                    "\t\t\t</cac:PartyTaxScheme>\n" +
                    "\t\t\t<cac:PartyLegalEntity>\n" +
                    "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:RegistrationName>\n"+
                            digitoVerificacion +
                    "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t\t\t<cbc:Name>11111</cbc:Name>\n" +
                    "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t</cac:PartyLegalEntity>\n" +
                    "\t\t\t<cac:Contact>\n" +
                    "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:Name>\n" +
                    "\t\t\t\t<cbc:Telephone>"+tmpMaster.getENC_CLITELELEF()+"</cbc:Telephone>\n" +
                    "\t\t\t\t<cbc:ElectronicMail>"+cli_email+"</cbc:ElectronicMail>\n" +
                    "\t\t\t</cac:Contact>\n" +
                    "\t\t</cac:Party>\n" +
                    "\t</cac:AccountingCustomerParty>\n" +
                    "\t<cac:PaymentMeans>\n" +
                    "\t\t<cbc:ID>"+tmpMaster.getENC_FORMAPAGO()+"</cbc:ID>\n" +
                    "\t\t<cbc:PaymentMeansCode>"+tmpMaster.getENC_MEDIOPAGO()+"</cbc:PaymentMeansCode>\n" +
                    "\t\t<cbc:PaymentDueDate>"+fechaPago+"</cbc:PaymentDueDate>\n" +
                    "\t\t<cbc:PaymentID>"+paymentID+"</cbc:PaymentID>\n" +
                    "\t</cac:PaymentMeans>\n" +
                    "\t<cac:AllowanceCharge>\n" +
                    "\t\t<cbc:ID>1</cbc:ID>\n" +
                    "\t\t<cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                    "\t\t<cbc:AllowanceChargeReasonCode>01</cbc:AllowanceChargeReasonCode>\n" +
                    "\t\t<cbc:AllowanceChargeReason>Descuento</cbc:AllowanceChargeReason>\n" +
                    "\t\t<cbc:MultiplierFactorNumeric>"+discountPercentage+"</cbc:MultiplierFactorNumeric>\n" +
                    "\t\t<cbc:Amount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_DESCUENTO()).replaceAll(",",".")+"</cbc:Amount>\n" +
                    "\t\t<cbc:BaseAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:BaseAmount>\n" +
                    "\t</cac:AllowanceCharge>\n" +
                    "\t<cac:LegalMonetaryTotal>\n" +
                    "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:LineExtensionAmount><!-- Valor bruto del documento-->\n" +
                    "\t\t<cbc:TaxExclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxExclusiveAmount><!-- Base imponilbe del documento-->\n" +
                    "\t\t<cbc:TaxInclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxInclusiveAmount><!-- Base imponible + impuestos-->\n" +
                    "\t\t<cbc:AllowanceTotalAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_DESCUENTO()).replaceAll(",",".")+"</cbc:AllowanceTotalAmount>\n" +
                    //"\t\t<!-- <cbc:ChargeTotalAmount currencyID=\"COP\">"+ChargeTotalAmount+"<cbc:ChargeTotalAmount> Aplica cuando es cargo a nivel general de la factura, suma de los cargos generales-->\n" +
                    "\t\t<cbc:PayableAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_TOTAL()).replaceAll(",",".")+"</cbc:PayableAmount><!--Valor a Pagar de Factura: Valor total de ítems (incluyendo cargos y descuentos a nivel de ítems)+valor tributos + valor cargos – valor descuentos – valor anticipos -->\n" +
                    "\t</cac:LegalMonetaryTotal>";



            System.out.println("THIS IS CLINATU: "+tmpMaster.getENC_CLINATU());

            if(schemeName.toString().equals("42")){
                System.out.println("International");
                digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
                invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
            }else{
                System.out.println("national");
                if(tmpMaster.getENC_CLINATU().toString().equals("1")){
                    System.out.println("IM JURIDIC");
                    try{
                        String number =  tmpMaster.getENC_CLINUMID().split("-")[0];
                        String digit = tmpMaster.getENC_CLINUMID().split("-")[1];
                        digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\""+digit+"\"  schemeName=\""+schemeName+"\">"+number+"</cbc:CompanyID>\n";
                        invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
                    }catch(Exception  e){
                        billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NO SE ENCONTRO DIGITO DE VERIFICACION EN EL DOCUMENTO. SYSWARE.");
                        doesHttp = false;
                    }
                }else{
                    System.out.println("IM NATURAL");
                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
                    invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
                }
            }


            if(tmpMaster.getENC_CLINUMID().equals(" ")){
                billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NUMERO DE IDENTIFICACION DE CLIENTE INVALIDO.");
                doesHttp = false; }




            List<Detail> details = billDAO.getDetails(tmpMaster.getENC_PREFIJO(),
                                                      tmpMaster.getENC_NRORECIBO(),
                                                      tmpMaster.getENC_SEDE(),
                                                      tmpMaster.getENC_VIGENCIA(),
                                                      tmpMaster.getENC_CIAQUIPU(),
                tmpMaster.getENC_NUM_DOCUHIJO());


            String detailXmlInvoiceLine = "";

            for(int j = 0; j<details.size();j++){

                Detail myDetail = details.get(j);

                System.out.println("BANDERA: "+myDetail.getDET_CODIGO());
                JSONObject obj1 = new JSONObject();
                obj1.put("id", (j+1));
                try{
                    obj1.put("note", myDetail.getDET_DESCRIP().replaceAll("^(?i)null$", "%20").replaceAll("null", "%20"));
                }catch(Exception e){
                    obj1.put("note", " ");
                }
                obj1.put("quantity", myDetail.getDET_CANTIDAD());
                obj1.put("amount", (myDetail.getDET_CANTIDAD() * myDetail.getDET_VLRUNIT()));
                obj1.put("taxAmount", 0.0);
                obj1.put("taxPercent", 0.0);
                obj1.put("taxId", 01);
                obj1.put("taxName", "IVA");
                obj1.put("description", myDetail.getDET_NOMBRE());
                try{
                    obj1.put("itemId", myDetail.getDET_CODIGO().replaceAll("^(?i)null$", "%20").replaceAll("null", "%20"));
                }catch(Exception e){
                    obj1.put("itemId", " ");
                }
                obj1.put("individualPrice", myDetail.getDET_VLRUNIT());
                obj1.put("baseQuantity", 1.00);

                System.out.println("itemId: "+obj1.get("itemId").toString());

                detailXmlInvoiceLine +=  generateInvoiceLines(new Integer(obj1.get("id").toString()),
                        obj1.get("note").toString().replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                        new Double(obj1.get("quantity").toString()),
                        new Double(obj1.get("amount").toString()),
                        new Double(obj1.get("taxAmount").toString()),
                        new Double(obj1.get("taxPercent").toString()),
                        new Integer(obj1.get("taxId").toString()),
                        obj1.get("taxName").toString(),
                        obj1.get("description").toString().replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                        obj1.get("itemId").toString(),
                        new Double(obj1.get("individualPrice").toString()),
                        new Double(obj1.get("baseQuantity").toString()));

            }

            String datPago = "";

            try{
                datPago = tmpMaster.getENC_DATOS_PAGO().replaceAll("%20","").replaceAll("^(?i)null$", "");
            }catch(Exception e){
                datPago = "";
            }

            String invoiceRawXmlFooter = "<DATA>\n" +
                    "\t\t<UBL21>true</UBL21>\n" +
                    "\t\t<Development>\n" +
                    "\t\t\t<ShortName>UNAL</ShortName>\n" +
                    "\t\t</Development>\n" +
                    "\t\t <Filters>\n" +
                    "\t\t\t<Product ></Product><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                    "\t\t\t<SubProduct></SubProduct><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                    "\t\t\t<CUENTA/>\n" +
                    "\t\t\t<NATURALEZA/>\n" +
                    "\t\t\t<CENTRODECOSTO/>\n" +
                    "\t\t\t<CLASE/>\n" +
                    "\t\t\t<INDICE/>\n" +
                    "\t\t</Filters>\n" +
                    "\t\t<Opcional1> Somos grandes contribuyentes, no responsables de IVA </Opcional1> <!-- ID 04 Representación Gráfica -->\n" +
                    "\t\t<Opcional2>  Agentes de retención de Impuesto sobre las ventas, no sujetos al impuesto de industria y comercio </Opcional2> <!-- ID 05 Representación Gráfica -->\n" +
                    "\t\t<Opcional3> "+tmpMaster.getENC_CIAQUIPU()+" - "+tmpMaster.getENC_NRORECIBO()+"</Opcional3> <!-- ID 10 Representación Gráfica -->\n" +
                    "\t\t<Opcional4>"+datPago.replaceAll("<","").replaceAll(">","")+
                    "#Somos Institución de Educación Superior no contribuyente del impuesto de renta,  favor abstenerse de efectuar retenciones en la  fuente. " +
                    "#Resolución de habilitación de facturación electrónica No. 18764008055116 de fecha 25/11/2020. " +
                    "#Correo: efactura_nal@unal.edu.co </Opcional4> <!-- ID 56 Representación Gráfica -->\n" +
                    "\t</DATA></Invoice>";



            //invoiceRawXmlFooter = "</Invoice>";



            String finalXML = (invoiceRawXmlHead + detailXmlInvoiceLine + invoiceRawXmlFooter).replaceAll("^(?i)null$", "%20").replaceAll("null", "%20");

            byte[] encodedBytes = Base64.encodeBase64(finalXML.getBytes());

            System.out.println("-------------------------------XML------------------------------");

            //System.out.println(finalXML);

            System.out.println("encodedBytes " + new String(encodedBytes));

            if(doesHttp){


            try {


                URL url = new URL(urlConsumption);//your url i.e fetch data from .
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("efacturaAuthorizationToken", "e14e6d8a-ed57-4de7-bc6d-7dba144240df");
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "text/plain");

                if(env == 2){
                    conn.setRequestProperty("efacturaAuthorizationToken", "d46b4d3c-d22d-41c8-af5d-6222564f9421");
                }

                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                os.write((new String(encodedBytes)).getBytes());
                os.flush();
                os.close();


                System.out.println(conn.getResponseCode());
                System.out.println(conn.getResponseMessage());

                if (conn.getResponseCode() != 200) {
                    InputStreamReader in2 = new InputStreamReader(conn.getErrorStream());
                    BufferedReader br2 = new BufferedReader(in2);
                    String output2;
                    System.out.println("ERROR: ");
                    String ErrorOutput = "";
                    while ((output2 = br2.readLine()) != null) {
                        System.out.println(output2);
                        ErrorOutput+=(" ; "+output2);
                    }
                    billDAO.updateTable("R",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),ErrorOutput);
                    throw new RuntimeException("Failed : HTTP Error code : "
                            + conn.getResponseCode());
                }else{
                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    BufferedReader br = new BufferedReader(in);
                    String output;

                    while ((output = br.readLine()) != null) {
                        System.out.println(output);

                        if(!output.contains("errorMessage")){
                            billDAO.updateTable("A",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),conn.getResponseMessage());
                        }else{
                            billDAO.updateTable("H",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),output);
                        }
                    }
                }

                conn.disconnect();
            } catch (Exception e) {
                System.out.println("Exception in NetClientGet:- " + e);
            }


            }


        return new Long(1);

    }


    public List<Report> report(String date1,String date2,String List){
        List<String> myList = new ArrayList<String>(Arrays.asList(List.split(",")));
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String date1S = format.format(new Date(date1));
        String date2S = format.format(new Date(date2));
        return billDAO.report(date1S, date2S, myList);

    }


    public List<Report> report2(String nrorecibo){
        return billDAO.report2(nrorecibo);

    }

    public List<Pref> prefs(){
        return billDAO.prefs();

    }



    public Long JsonConverterMethod(Long env){
        List<Master> mastersIr = billDAO.getMaster();

        List<Master> masters = Lists.reverse(mastersIr);

        for(int i = 0; i<masters.size();i++){
            Master tmpMaster = masters.get(i);

            Long tipo = tmpMaster.getENC_TIPODOC();

            System.out.println(tmpMaster.getENC_PREFIJO()+","+tmpMaster.getENC_OBSERVACIONES()+","+tipo);

            if(tipo.toString().equals("91")){
                System.out.print("---------CREDITO----------");
                JsonConverterMethodCredit(env,tmpMaster);
            }

            if(tipo.toString().equals("1")){
                System.out.print("---------INVOICE----------");
                JsonConverterMethodInvoice(env,tmpMaster);
            }

            if(tipo.toString().equals("92")){
                System.out.print("---------DEBIT----------");
                JsonConverterMethodDebit(env,tmpMaster);
            }

        }

        return new Long(1);

    }

    public Long JsonConverterMethodCredit(Long env, Master tmpMaster){


            String ciudadSegunCodigo = "Bogotá";
            String idCiudadCliente = "11001";
            String ciudadCliente = "Bogotá";
            String countrySubEntity = "Bogotá";

            if(tmpMaster.getENC_CIUDAD_SEDE().length()<5){tmpMaster.setENC_CIUDAD_SEDE("0"+tmpMaster.getENC_CIUDAD_SEDE());}

            if(tmpMaster.getENC_CIUDAD_SEDE().equals("11001")){ciudadSegunCodigo = "Bogotá";idCiudadCliente = "11001";ciudadCliente = "Bogotá";countrySubEntity = "Bogotá";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("11001")){ciudadSegunCodigo = "Bogotá";idCiudadCliente = "11001";ciudadCliente = "Bogotá";countrySubEntity = "Bogotá";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("05001")){ciudadSegunCodigo = "Medellín";idCiudadCliente = "05001";ciudadCliente = "Medellín";countrySubEntity = "Antioquia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("17001")){ciudadSegunCodigo = "Manizales";idCiudadCliente = "17001";ciudadCliente = "Manizales";countrySubEntity = "Caldas";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("76520")){ciudadSegunCodigo = "Palmira";idCiudadCliente = "76520";ciudadCliente = "Palmira";countrySubEntity = "Valle";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("88001")){ciudadSegunCodigo = "San Andrés";idCiudadCliente = "88001";ciudadCliente = "San Andrés";countrySubEntity = "San Andrés";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("91001")){ciudadSegunCodigo = "Leticia";idCiudadCliente = "91001";ciudadCliente = "Leticia";countrySubEntity = "Amazonas";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("81001")){ciudadSegunCodigo = "Arauca";idCiudadCliente = "81001";ciudadCliente = "Arauca";countrySubEntity = "Arauca";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("52835")){ciudadSegunCodigo = "Tumaco";idCiudadCliente = "52835";ciudadCliente = "Tumaco";countrySubEntity = "Nariño";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("20621")){ciudadSegunCodigo = "La Paz";idCiudadCliente = "20621";ciudadCliente = "La Paz";countrySubEntity = "Cesar";}

            try{
                idCiudadCliente = tmpMaster.getENC_CLICIUDAD();
                if(idCiudadCliente.length()==4){
                    idCiudadCliente = "0"+idCiudadCliente;
                }
                ciudadCliente = billDAO.getCiudad(idCiudadCliente);
                String subentityCode = idCiudadCliente.substring(0,2);
            }catch(Exception e){
                idCiudadCliente = tmpMaster.getENC_CIUDAD_SEDE();
                ciudadCliente = ciudadSegunCodigo;
            }

            int profileExecutionID = 2;
            String pref = "SETT";
            String pruebas = "https://apivp.efacturacadena.com/staging/vp/documentos/proceso/sincrono";
            String produccion = "https://apivp.efacturacadena.com/v1/vp/documentos/proceso/sincrono";
            String urlConsumption = pruebas;

            if(env == 1){
                profileExecutionID = 1;
                pref = tmpMaster.getENC_PREFIJO();
                urlConsumption = produccion;
            }

            Double SubTotal = tmpMaster.getENC_SUBTOTAL();
            Double discountvalue = tmpMaster.getENC_DESCUENTO();
            Double subtotDiv = new Double(1);
            if(!SubTotal.toString().equals("0")){
                subtotDiv = SubTotal;
            }

            Double discountPercentage = new Double(0);

            if(subtotDiv != 0){
                discountPercentage = new Double((discountvalue * 100)/subtotDiv);
            }

            if(Double.isNaN(discountPercentage)){
                discountPercentage = new Double(0);
            }



            BillingDianInfo ConsecDian = billDAO.getConsecDianCredito(tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_PREFIJO());
            if(ConsecDian == null){
                ConsecDian = new BillingDianInfo(new Long(0000),tmpMaster.getENC_FECEMIC());
            }


            String cli_email = "";

            try {
                cli_email = tmpMaster.getENC_CLIEMAIL().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#");
            }catch(Exception e){
                cli_email = "";
            }

            String obs = "";

            try {
                obs = tmpMaster.getENC_OBSERVACIONES().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#");
            }catch(Exception e){
                obs = "";
            }

            String cli_dir = "";

            try {
                cli_dir = tmpMaster.getENC_CLIDIR().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#");
            }catch(Exception e){
                cli_dir = "";
            }

            String fechaPago = "";

            if(tmpMaster.getENC_FECPAGO()!=null && tmpMaster.getENC_FECPAGO()!="") {
                fechaPago = tmpMaster.getENC_FECPAGO();
            }else{
                fechaPago = tmpMaster.getENC_FECEMIC();
            }

            String invoiceRawXmlHead1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<CreditNote xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2\" xmlns:cac=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:cbc=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ext=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:sts=\"dian:gov:co:facturaelectronica:Structures-2-1\" xmlns:xades=\"http://uri.etsi.org/01903/v1.3.2#\" xmlns:xades141=\"http://uri.etsi.org/01903/v1.4.1#\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2     http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-CreditNote-2.1.xsd\">\n" +
                    "\t<cbc:CustomizationID>10</cbc:CustomizationID>\n" +
                    "\t<cbc:ProfileExecutionID>"+profileExecutionID+"</cbc:ProfileExecutionID>\n" +
                    "\t<cbc:ID>"+"NC"+pref+tmpMaster.getENC_CONSDIAN()+"</cbc:ID>\n" +
                    "\t<cbc:IssueDate>"+tmpMaster.getENC_FECEMIC()+"</cbc:IssueDate>\n" +
                    "\t<cbc:IssueTime>00:00:00-05:00</cbc:IssueTime>\n" +
                    //"\t<cbc:DueDate>"+fechaPago+"</cbc:DueDate>\n" +
                    "\t<cbc:CreditNoteTypeCode>91</cbc:CreditNoteTypeCode>\n"+
                    //"\t<cbc:InvoiceTypeCode>01</cbc:InvoiceTypeCode>\n" +
                    "\t<cbc:Note>"+obs.replaceAll("No\\.","#").replaceAll("NO\\.","#")+"</cbc:Note>\n" +
                    "\t<cbc:DocumentCurrencyCode>COP</cbc:DocumentCurrencyCode>\n" +
                    "\t<cbc:LineCountNumeric>"+tmpMaster.getENC_NRO_ITEMES()+"</cbc:LineCountNumeric>\n" +
                    "\t<cac:DiscrepancyResponse>\n" +
                    "\t\t<cbc:ReferenceID>  </cbc:ReferenceID>\n" +
                    "\t\t<cbc:ResponseCode>2</cbc:ResponseCode>\n" +
                    "\t\t<cbc:Description>Anulación de factura electrónica</cbc:Description>\n" +
                    "\t</cac:DiscrepancyResponse>" +
                    "\t<cac:OrderReference>\n" +
                    "\t\t<cbc:ID>orderReference</cbc:ID>\n" +
                    "\t</cac:OrderReference>" +
                    "\t<cac:BillingReference>\n" +
                    "\t\t<cac:InvoiceDocumentReference>\n" +
                    "\t\t\t<cbc:ID>"+pref+ConsecDian.getENC_CONSDIAN()+"</cbc:ID>\n" +
                    "\t\t\t<!--Factura asociada debe ser mismo al de la factura a modificar -->\n" +
                    "\t\t\t<cbc:IssueDate>"+ConsecDian.getENC_FECEMIC()+"</cbc:IssueDate>\n" +
                    "\t\t\t<!-- mismo al issue date de la factura o anterior -->\n" +
                    "\t\t</cac:InvoiceDocumentReference>\n" +
                    "\t</cac:BillingReference>" +
                    "\t<cac:AccountingSupplierParty>\n" +
                    "\t\t<cbc:AdditionalAccountID>1</cbc:AdditionalAccountID>\n" +
                    "\t\t<cac:Party>\n" +
                    "\t\t\t<cac:PartyName>\n" +
                    "\t\t\t\t<cbc:Name>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:Name>\n" +
                    "\t\t\t</cac:PartyName>\n" +
                    "\t\t\t<cac:PhysicalLocation>\n" +
                    "\t\t\t\t<cac:Address>\n" +
                    "\t\t\t\t\t<cbc:ID>"+tmpMaster.getENC_CIUDAD_SEDE()+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>"+ciudadSegunCodigo+"</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadSegunCodigo+"</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>"+tmpMaster.getENC_CIUDAD_SEDE().substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>"+tmpMaster.getENC_DIR_SEDE().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:Address>\n" +
                    "\t\t\t</cac:PhysicalLocation>\n" +
                    "\t\t\t<cac:PartyTaxScheme>\n" +
                    "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                    "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                    "\t\t\t\t<cbc:TaxLevelCode listName=\"49\">O-13;O-23</cbc:TaxLevelCode>\n" +
                    "\t\t\t\t<cac:RegistrationAddress>\n" +
                    "\t\t\t\t\t<cbc:ID>11001</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>Bogotá</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:PostalZone>000000</cbc:PostalZone>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>Bogotá</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>11</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>CR 45   26   85 OF 481 ED URIEL GUTIERREZ</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:RegistrationAddress>\n" +
                    "\t\t\t\t<cac:TaxScheme>\n" +
                    "\t\t\t\t\t<cbc:ID>01</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:Name>IVA</cbc:Name>\n" +
                    "\t\t\t\t</cac:TaxScheme>\n" +
                    "\t\t\t</cac:PartyTaxScheme>\n" +
                    "\t\t\t<cac:PartyLegalEntity>\n" +
                    "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                    "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                    "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t\t\t<cbc:ID>"+pref+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:Name>12345</cbc:Name>\n" +
                    "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t</cac:PartyLegalEntity>\n" +
                    "\t\t\t<cac:Contact>\n" +
                    "\t\t\t\t<cbc:Name></cbc:Name>\n" +
                    "\t\t\t\t<cbc:Telephone>3165000</cbc:Telephone>\n" +
                    "\t\t\t\t<cbc:ElectronicMail>efactura_nal@unal.edu.co</cbc:ElectronicMail>\n" +
                    "\t\t\t</cac:Contact>\n" +
                    "\t\t</cac:Party>\n" +
                    "\t</cac:AccountingSupplierParty>\n" +
                    "\t<cac:AccountingCustomerParty>\n"+
                    "\t\t<cbc:AdditionalAccountID>"+tmpMaster.getENC_CLINATU()+"</cbc:AdditionalAccountID>\n"+
                    "\t\t<cac:Party>\t\n" +
                    "\t\t\t<cac:PartyIdentification>\t\n" +
                    "\t\t\t\t<cbc:ID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\"31\">"+tmpMaster.getENC_CLINUMID()+"</cbc:ID>\n" +
                    "\t\t\t</cac:PartyIdentification>\n" +
                    "\t\t\t<cac:PartyName>\n" +
                    "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("No\\.","#").replaceAll("_","Y").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Name>\n" +
                    "\t\t\t</cac:PartyName>\n" +
                    "\t\t\t<cac:PhysicalLocation>\n" +
                    "\t\t\t\t<cac:Address>\n" +
                    "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadCliente+"</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>"+idCiudadCliente.substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>"+cli_dir+"</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:Address>\n" +
                    "\t\t\t</cac:PhysicalLocation>\n" +
                    "\t\t\t<cac:PartyTaxScheme>\n" +
                    "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("No\\.","#").replaceAll("NO\\.","#").replaceAll("<","").replaceAll(">","")+"</cbc:RegistrationName>\n";


            String paymentID = "Efectivo";

            if(tmpMaster.getENC_MEDIOPAGO().toString().equals("48")){
                paymentID = "Tarjeta Crédito";
            }

            if(tmpMaster.getENC_MEDIOPAGO().toString().equals("49")){
                paymentID = "Tarjeta Débito";
            }

            if(tmpMaster.getENC_MEDIOPAGO().toString().equals("47")){
                paymentID = "Transferencia Débito Bancaria";
            }


            String cliRespFiscal = tmpMaster.getENC_CLIRESPFISCAL();

            try{
                if(tmpMaster.getENC_CLIRESPFISCAL().equals("0") || tmpMaster.getENC_CLIRESPFISCAL() == null || tmpMaster.getENC_CLIRESPFISCAL().equals("null")||tmpMaster.getENC_CLIRESPFISCAL().equals("")){
                    cliRespFiscal = "R-99-PN";
                }
            }catch(Exception e){
                cliRespFiscal = "R-99-PN";
            }

            String TaxLevelCode = "No Aplica";

            try{
                if(tmpMaster.getENC_CLIRESPIVA()== new Long(0)){
                    TaxLevelCode = "49";
                }else{
                    TaxLevelCode = tmpMaster.getENC_CLIRESPIVA().toString();
                }
            }catch(Exception e){
                TaxLevelCode = "49";
            }





            String invoiceRawXmlHead12 =  "\t\t\t\t<cbc:TaxLevelCode listName=\""+TaxLevelCode+"\">"+cliRespFiscal+"</cbc:TaxLevelCode>\n" +
                    "\t\t\t\t<cac:RegistrationAddress>\n" +
                    "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>"+countrySubEntity+"</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>"+tmpMaster.getENC_CIUDAD_SEDE().substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>"+cli_dir+"</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:RegistrationAddress>\n" +
                    "\t\t\t\t<cac:TaxScheme>\n" +
                    "\t\t\t\t\t<cbc:ID>01</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:Name>IVA</cbc:Name>\n" +
                    "\t\t\t\t</cac:TaxScheme>\n" +
                    "\t\t\t</cac:PartyTaxScheme>\n" +
                    "\t\t\t<cac:PartyLegalEntity>\n" +
                    "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:RegistrationName>\n" +
                    "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\"31\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n" +
                    "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t\t\t<cbc:Name>11111</cbc:Name>\n" +
                    "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t</cac:PartyLegalEntity>\n" +
                    "\t\t\t<cac:Contact>\n" +
                    "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:Name>\n" +
                    "\t\t\t\t<cbc:Telephone>"+tmpMaster.getENC_CLITELELEF()+"</cbc:Telephone>\n" +
                    "\t\t\t\t<cbc:ElectronicMail>"+cli_email+"</cbc:ElectronicMail>\n" +
                    "\t\t\t</cac:Contact>\n" +
                    "\t\t</cac:Party>\n" +
                    "\t</cac:AccountingCustomerParty>\n" +
                    "\t<cac:PaymentMeans>\n" +
                    "\t\t<cbc:ID>"+tmpMaster.getENC_FORMAPAGO()+"</cbc:ID>\n" +
                    "\t\t<cbc:PaymentMeansCode>"+tmpMaster.getENC_MEDIOPAGO()+"</cbc:PaymentMeansCode>\n" +
                    "\t\t<cbc:PaymentDueDate>"+fechaPago+"</cbc:PaymentDueDate>\n" +
                    "\t\t<cbc:PaymentID>"+paymentID+"</cbc:PaymentID>\n" +
                    "\t</cac:PaymentMeans>\n" +
                    "\t<cac:AllowanceCharge>\n" +
                    "\t\t<cbc:ID>1</cbc:ID>\n" +
                    "\t\t<cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                    "\t\t<cbc:AllowanceChargeReasonCode>01</cbc:AllowanceChargeReasonCode>\n" +
                    "\t\t<cbc:AllowanceChargeReason>Descuento</cbc:AllowanceChargeReason>\n" +
                    "\t\t<cbc:MultiplierFactorNumeric>"+discountPercentage+"</cbc:MultiplierFactorNumeric>\n" +
                    "\t\t<cbc:Amount currencyID=\"COP\">"+tmpMaster.getENC_DESCUENTO()+"</cbc:Amount>\n" +
                    "\t\t<cbc:BaseAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:BaseAmount>\n" +
                    "\t</cac:AllowanceCharge>\n" +
                    "\t<cac:LegalMonetaryTotal>\n" +
                    "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:LineExtensionAmount><!-- Valor bruto del documento-->\n" +
                    "\t\t<cbc:TaxExclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxExclusiveAmount><!-- Base imponilbe del documento-->\n" +
                    "\t\t<cbc:TaxInclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxInclusiveAmount><!-- Base imponible + impuestos-->\n" +
                    "\t\t<cbc:AllowanceTotalAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_DESCUENTO()).replaceAll(",",".")+"</cbc:AllowanceTotalAmount>\n" +
                    //"\t\t<!-- <cbc:ChargeTotalAmount currencyID=\"COP\">"+ChargeTotalAmount+"<cbc:ChargeTotalAmount> Aplica cuando es cargo a nivel general de la factura, suma de los cargos generales-->\n" +
                    "\t\t<cbc:PayableAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_TOTAL()).replaceAll(",",".")+"</cbc:PayableAmount><!--Valor a Pagar de Factura: Valor total de ítems (incluyendo cargos y descuentos a nivel de ítems)+valor tributos + valor cargos – valor descuentos – valor anticipos -->\n" +
                    "\t</cac:LegalMonetaryTotal>";


            String digitoVerificacion = "";
            String invoiceRawXmlHead = "";

            Boolean doesHttp = true;
            Boolean doesHttp2 = false;

            System.out.println("THIS IS CLINATU: "+tmpMaster.getENC_CLINATU());
            if(tmpMaster.getENC_CLINATU().toString().equals("1")){
                System.out.println("IM JURIDIC");
                try{
                    String number =  tmpMaster.getENC_CLINUMID().split("-")[0];
                    String digit = tmpMaster.getENC_CLINUMID().split("-")[1];
                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\""+digit+"\"  schemeName=\"31\">"+number+"</cbc:CompanyID>\n";
                    invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
                }catch(Exception  e){
                    billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NRORECIBO(),"NO SE ENCONTRO DIGITO DE VERIFICACION EN EL DOCUMENTO. SYSWARE.");
                    doesHttp = false;
                }
            }else{

                digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\"13\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
                invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
            }




            List<Detail> details = billDAO.getDetails(tmpMaster.getENC_PREFIJO(),
                    tmpMaster.getENC_NRORECIBO(),
                    tmpMaster.getENC_SEDE(),
                    tmpMaster.getENC_VIGENCIA(),
                    tmpMaster.getENC_CIAQUIPU(),
                    tmpMaster.getENC_NUM_DOCUHIJO());


            String detailXmlInvoiceLine = "";

            for(int j = 0; j<details.size();j++){

                Detail myDetail = details.get(j);

                System.out.println("BANDERA: "+myDetail.getDET_CODIGO());
                JSONObject obj1 = new JSONObject();
                obj1.put("id", (j+1));
                String det_descrip = " ";
                try{
                    det_descrip = myDetail.getDET_DESCRIP();
                }catch(Exception e){
                    det_descrip = " ";
                }
                obj1.put("note", det_descrip);
                obj1.put("quantity", myDetail.getDET_CANTIDAD());
                obj1.put("amount", (myDetail.getDET_CANTIDAD() * myDetail.getDET_VLRUNIT()));
                obj1.put("taxAmount", 0.0);
                obj1.put("taxPercent", 0.0);
                obj1.put("taxId", 01);
                obj1.put("taxName", "IVA");
                obj1.put("description", myDetail.getDET_NOMBRE());
                try{
                    obj1.put("itemId", myDetail.getDET_CODIGO().replaceAll("^(?i)null$", "%20").replaceAll("null", "%20"));
                }catch(Exception e){
                    obj1.put("itemId", " ");
                }
                obj1.put("individualPrice", myDetail.getDET_VLRUNIT());
                obj1.put("baseQuantity", 1.00);

                System.out.println("itemId: "+obj1.get("itemId").toString());

                detailXmlInvoiceLine +=  generateCreditLines(new Integer(obj1.get("id").toString()),
                        det_descrip.replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                        new Double(obj1.get("quantity").toString()),
                        new Double(obj1.get("amount").toString()),
                        new Double(obj1.get("taxAmount").toString()),
                        new Double(obj1.get("taxPercent").toString()),
                        new Integer(obj1.get("taxId").toString()),
                        obj1.get("taxName").toString(),
                        obj1.get("description").toString().replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                        obj1.get("itemId").toString(),
                        new Double(obj1.get("individualPrice").toString()),
                        new Double(obj1.get("baseQuantity").toString()));

            }

        String datPago = "";

        try{
            datPago = tmpMaster.getENC_DATOS_PAGO().replaceAll("%20","").replaceAll("^(?i)null$", "").replaceAll("<","").replaceAll(">","");
        }catch(Exception e){
            datPago = "";
        }

            String invoiceRawXmlFooter = "<DATA>\n" +
                    "\t\t<UBL21>true</UBL21>\n" +
                    "\t\t<Development>\n" +
                    "\t\t\t<ShortName>UNAL</ShortName>\n" +
                    "\t\t</Development>\n" +
                    "\t\t <Filters>\n" +
                    "\t\t\t<Product ></Product><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                    "\t\t\t<SubProduct></SubProduct><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                    "\t\t\t<CUENTA/>\n" +
                    "\t\t\t<NATURALEZA/>\n" +
                    "\t\t\t<CENTRODECOSTO/>\n" +
                    "\t\t\t<CLASE/>\n" +
                    "\t\t\t<INDICE/>\n" +
                    "\t\t</Filters>\n" +
                    "\t\t<Opcional1> Somos grandes contribuyentes, no responsables de IVA </Opcional1> <!-- ID 04 Representación Gráfica -->\n" +
                    "\t\t<Opcional2>  Agentes de retención de Impuesto sobre las ventas, no sujetos al impuesto de industria y comercio </Opcional2> <!-- ID 05 Representación Gráfica -->\n" +
                    "\t\t<Opcional3> "+tmpMaster.getENC_CIAQUIPU()+" - "+tmpMaster.getENC_NRORECIBO()+"</Opcional3> <!-- ID 10 Representación Gráfica -->\n" +
                    "\t\t<Opcional4>"+datPago+
                    "#Somos Institución de Educación Superior no contribuyente del impuesto de renta,  favor abstenerse de efectuar retenciones en la  fuente. " +
                    "#Resolución de habilitación de facturación electrónica No. 18764008055116 de fecha 25/11/2020. " +
                    "#Correo: efactura_nal@unal.edu.co </Opcional4> <!-- ID 56 Representación Gráfica -->\n" +
                    "\t</DATA></CreditNote>";



            //invoiceRawXmlFooter = "</Invoice>";



            String finalXML = (invoiceRawXmlHead + detailXmlInvoiceLine + invoiceRawXmlFooter).replaceAll("^(?i)null$", "%20").replaceAll("null", "%20");

            byte[] encodedBytes = Base64.encodeBase64(finalXML.getBytes());

            System.out.println("-------------------------------XML------------------------------");

            //System.out.println(finalXML);

            System.out.println("encodedBytes " + new String(encodedBytes));

            if(doesHttp){


                try {


                    URL url = new URL(urlConsumption);//your url i.e fetch data from .
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestProperty("efacturaAuthorizationToken", "e14e6d8a-ed57-4de7-bc6d-7dba144240df");
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "text/plain");
                    if(env == 2){
                        conn.setRequestProperty("efacturaAuthorizationToken", "d46b4d3c-d22d-41c8-af5d-6222564f9421");
                    }

                    conn.setDoOutput(true);
                    OutputStream os = conn.getOutputStream();
                    os.write((new String(encodedBytes)).getBytes());
                    os.flush();
                    os.close();


                    System.out.println(conn.getResponseCode());
                    System.out.println(conn.getResponseMessage());

                    if (conn.getResponseCode() != 200) {
                        InputStreamReader in2 = new InputStreamReader(conn.getErrorStream());
                        BufferedReader br2 = new BufferedReader(in2);
                        String output2;
                        System.out.println("ERROR: ");
                        String ErrorOutput = "";
                        while ((output2 = br2.readLine()) != null) {
                            System.out.println(output2);
                            ErrorOutput+=(" ; "+output2);
                        }
                        billDAO.updateTable("R",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),ErrorOutput);
                        throw new RuntimeException("Failed : HTTP Error code : "
                                + conn.getResponseCode());
                    }else{
                        billDAO.updateTable("A",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),conn.getResponseMessage());
                    }


                    InputStreamReader in = new InputStreamReader(conn.getInputStream());
                    BufferedReader br = new BufferedReader(in);
                    String output;

                    while ((output = br.readLine()) != null) {
                        System.out.println(output);
                    }

                    conn.disconnect();
                } catch (Exception e) {
                    System.out.println("Exception in NetClientGet:- " + e);
                }


            }


        return new Long(1);

    }



    public Long JsonConverterMethodDebit(Long env, Master tmpMaster){


        String ciudadSegunCodigo = "Bogotá";
        String idCiudadCliente = "11001";
        String ciudadCliente = "Bogotá";
        String countrySubEntity = "Bogotá";

        if(tmpMaster.getENC_CIUDAD_SEDE().length()<5){tmpMaster.setENC_CIUDAD_SEDE("0"+tmpMaster.getENC_CIUDAD_SEDE());}

        if(tmpMaster.getENC_CIUDAD_SEDE().equals("11001")){ciudadSegunCodigo = "Bogotá";idCiudadCliente = "11001";ciudadCliente = "Bogotá";countrySubEntity = "Bogotá";}
        if(tmpMaster.getENC_CIUDAD_SEDE().equals("11001")){ciudadSegunCodigo = "Bogotá";idCiudadCliente = "11001";ciudadCliente = "Bogotá";countrySubEntity = "Bogotá";}
        if(tmpMaster.getENC_CIUDAD_SEDE().equals("05001")){ciudadSegunCodigo = "Medellín";idCiudadCliente = "05001";ciudadCliente = "Medellín";countrySubEntity = "Antioquia";}
        if(tmpMaster.getENC_CIUDAD_SEDE().equals("17001")){ciudadSegunCodigo = "Manizales";idCiudadCliente = "17001";ciudadCliente = "Manizales";countrySubEntity = "Caldas";}
        if(tmpMaster.getENC_CIUDAD_SEDE().equals("76520")){ciudadSegunCodigo = "Palmira";idCiudadCliente = "76520";ciudadCliente = "Palmira";countrySubEntity = "Valle";}
        if(tmpMaster.getENC_CIUDAD_SEDE().equals("88001")){ciudadSegunCodigo = "San Andrés";idCiudadCliente = "88001";ciudadCliente = "San Andrés";countrySubEntity = "San Andrés";}
        if(tmpMaster.getENC_CIUDAD_SEDE().equals("91001")){ciudadSegunCodigo = "Leticia";idCiudadCliente = "91001";ciudadCliente = "Leticia";countrySubEntity = "Amazonas";}
        if(tmpMaster.getENC_CIUDAD_SEDE().equals("81001")){ciudadSegunCodigo = "Arauca";idCiudadCliente = "81001";ciudadCliente = "Arauca";countrySubEntity = "Arauca";}
        if(tmpMaster.getENC_CIUDAD_SEDE().equals("52835")){ciudadSegunCodigo = "Tumaco";idCiudadCliente = "52835";ciudadCliente = "Tumaco";countrySubEntity = "Nariño";}
        if(tmpMaster.getENC_CIUDAD_SEDE().equals("20621")){ciudadSegunCodigo = "La Paz";idCiudadCliente = "20621";ciudadCliente = "La Paz";countrySubEntity = "Cesar";}



        try{
            idCiudadCliente = tmpMaster.getENC_CLICIUDAD();
            if(idCiudadCliente.length()==4){
                idCiudadCliente = "0"+idCiudadCliente;
            }
            ciudadCliente = billDAO.getCiudad(idCiudadCliente);
            String subentityCode = idCiudadCliente.substring(0,2);
        }catch(Exception e){
            idCiudadCliente = tmpMaster.getENC_CIUDAD_SEDE();
            ciudadCliente = ciudadSegunCodigo;
        }


        int profileExecutionID = 2;
        String pref = "SETT";
        String pruebas = "https://apivp.efacturacadena.com/staging/vp/documentos/proceso/sincrono";
        String produccion = "https://apivp.efacturacadena.com/v1/vp/documentos/proceso/sincrono";
        String urlConsumption = pruebas;

        if(env == 1){
            profileExecutionID = 1;
            pref = tmpMaster.getENC_PREFIJO();
            urlConsumption = produccion;
        }

        Double SubTotal = tmpMaster.getENC_SUBTOTAL();
        Double discountvalue = tmpMaster.getENC_DESCUENTO();
        Double subtotDiv = new Double(1);
        if(!SubTotal.toString().equals("0")){
            subtotDiv = SubTotal;
        }

        Double discountPercentage = new Double(0);

        if(subtotDiv != 0){
            discountPercentage = new Double((discountvalue * 100)/subtotDiv);
        }



        BillingDianInfo ConsecDian = billDAO.getConsecDianCredito(tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_PREFIJO());

        if(ConsecDian == null){
            ConsecDian = new BillingDianInfo(new Long(0000),tmpMaster.getENC_FECEMIC());
        }


        String obs = "";

        try {
            obs = tmpMaster.getENC_OBSERVACIONES().replaceAll("No\\.","#").replaceAll("NO\\.","#");
        }catch(Exception e){
            obs = "";
        }


        String cli_dir = "";

        try {
            cli_dir = tmpMaster.getENC_CLIDIR().replaceAll("No\\.","#").replaceAll("NO\\.","#");
        }catch(Exception e){
            cli_dir = "";
        }


        String cli_email = "";

        try {
            cli_email = tmpMaster.getENC_CLIEMAIL().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#");
        }catch(Exception e){
            cli_email = "";
        }


        String fechaPago = "";

        if(tmpMaster.getENC_FECPAGO()!=null && tmpMaster.getENC_FECPAGO()!="") {
            fechaPago = tmpMaster.getENC_FECPAGO();
        }else{
            fechaPago = tmpMaster.getENC_FECEMIC();
        }


        String invoiceRawXmlHead1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<DebitNote xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2\" xmlns:cac=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:cbc=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ext=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:sts=\"dian:gov:co:facturaelectronica:Structures-2-1\" xmlns:xades=\"http://uri.etsi.org/01903/v1.3.2#\" xmlns:xades141=\"http://uri.etsi.org/01903/v1.4.1#\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2     http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-DebitNote-2.1.xsd\">\n" +
                "\t<cbc:CustomizationID>10</cbc:CustomizationID>\n" +
                "\t<cbc:ProfileExecutionID>"+profileExecutionID+"</cbc:ProfileExecutionID>\n" +
                "\t<cbc:ID>"+"ND"+pref+tmpMaster.getENC_CONSDIAN()+"</cbc:ID>\n" +
                "\t<cbc:IssueDate>"+tmpMaster.getENC_FECEMIC()+"</cbc:IssueDate>\n" +
                "\t<cbc:IssueTime>00:00:00-05:00</cbc:IssueTime>\n" +
                "\t<cbc:DueDate>"+fechaPago+"</cbc:DueDate>\n" +
                //"\t<cbc:CreditNoteTypeCode>91</cbc:CreditNoteTypeCode>\n"+
                //"\t<cbc:InvoiceTypeCode>01</cbc:InvoiceTypeCode>\n" +
                "\t<cbc:Note>"+obs.replaceAll("<","").replaceAll(">","")+"</cbc:Note>\n" +
                "\t<cbc:DocumentCurrencyCode>COP</cbc:DocumentCurrencyCode>\n" +
                "\t<cbc:LineCountNumeric>"+tmpMaster.getENC_NRO_ITEMES()+"</cbc:LineCountNumeric>\n" +
                "\t<cac:DiscrepancyResponse>\n" +
                "\t\t<cbc:ReferenceID>  </cbc:ReferenceID>\n" +
                "\t\t<cbc:ResponseCode>4</cbc:ResponseCode>\n" +
                "\t\t<cbc:Description>Otros</cbc:Description>\n" +
                "\t</cac:DiscrepancyResponse>" +
                "\t<cac:BillingReference>\n" +
                "\t\t<cac:InvoiceDocumentReference>\n" +
                "\t\t\t<cbc:ID>"+pref+ConsecDian.getENC_CONSDIAN()+"</cbc:ID>\n" +
                "\t\t\t<!--Factura asociada debe ser mismo al de la factura a modificar -->\n" +
                "\t\t\t<cbc:IssueDate>"+ConsecDian.getENC_FECEMIC()+"</cbc:IssueDate>\n" +
                "\t\t\t<!-- mismo al issue date de la factura o anterior -->\n" +
                "\t\t</cac:InvoiceDocumentReference>\n" +
                "\t</cac:BillingReference>" +
                "\t<cac:AccountingSupplierParty>\n" +
                "\t\t<cbc:AdditionalAccountID>1</cbc:AdditionalAccountID>\n" +
                "\t\t<cac:Party>\n" +
                "\t\t\t<cac:PartyName>\n" +
                "\t\t\t\t<cbc:Name>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:Name>\n" +
                "\t\t\t</cac:PartyName>\n" +
                "\t\t\t<cac:PhysicalLocation>\n" +
                "\t\t\t\t<cac:Address>\n" +
                "\t\t\t\t\t<cbc:ID>"+tmpMaster.getENC_CIUDAD_SEDE()+"</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:CityName>"+ciudadSegunCodigo+"</cbc:CityName>\n" +
                "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                "\t\t\t\t\t<cbc:CountrySubentity>"+countrySubEntity+"</cbc:CountrySubentity>\n" +
                "\t\t\t\t\t<cbc:CountrySubentityCode>"+tmpMaster.getENC_CIUDAD_SEDE().substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                "\t\t\t\t\t<cac:AddressLine>\n" +
                "\t\t\t\t\t\t<cbc:Line>"+tmpMaster.getENC_DIR_SEDE().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                "\t\t\t\t\t</cac:AddressLine>\n" +
                "\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                "\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t</cac:Address>\n" +
                "\t\t\t</cac:PhysicalLocation>\n" +
                "\t\t\t<cac:PartyTaxScheme>\n" +
                "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                "\t\t\t\t<cbc:TaxLevelCode listName=\"49\">O-13;O-23</cbc:TaxLevelCode>\n" +
                "\t\t\t\t<cac:RegistrationAddress>\n" +
                "\t\t\t\t\t<cbc:ID>11001</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:CityName>Bogotá</cbc:CityName>\n" +
                "\t\t\t\t\t<cbc:PostalZone>000000</cbc:PostalZone>\n" +
                "\t\t\t\t\t<cbc:CountrySubentity>Bogotá</cbc:CountrySubentity>\n" +
                "\t\t\t\t\t<cbc:CountrySubentityCode>11</cbc:CountrySubentityCode>\n" +
                "\t\t\t\t\t<cac:AddressLine>\n" +
                "\t\t\t\t\t\t<cbc:Line>CR 45   26   85 OF 481 ED URIEL GUTIERREZ</cbc:Line>\n" +
                "\t\t\t\t\t</cac:AddressLine>\n" +
                "\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                "\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t</cac:RegistrationAddress>\n" +
                "\t\t\t\t<cac:TaxScheme>\n" +
                "\t\t\t\t\t<cbc:ID>01</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:Name>IVA</cbc:Name>\n" +
                "\t\t\t\t</cac:TaxScheme>\n" +
                "\t\t\t</cac:PartyTaxScheme>\n" +
                "\t\t\t<cac:PartyLegalEntity>\n" +
                "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                "\t\t\t\t\t<cbc:ID>"+pref+"</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:Name>12345</cbc:Name>\n" +
                "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                "\t\t\t</cac:PartyLegalEntity>\n" +
                "\t\t\t<cac:Contact>\n" +
                "\t\t\t\t<cbc:Name></cbc:Name>\n" +
                "\t\t\t\t<cbc:Telephone>3165000</cbc:Telephone>\n" +
                "\t\t\t\t<cbc:ElectronicMail>efactura_nal@unal.edu.co</cbc:ElectronicMail>\n" +
                "\t\t\t</cac:Contact>\n" +
                "\t\t</cac:Party>\n" +
                "\t</cac:AccountingSupplierParty>\n" +
                "\t<cac:AccountingCustomerParty>\n"+
                "\t\t<cbc:AdditionalAccountID>"+tmpMaster.getENC_CLINATU()+"</cbc:AdditionalAccountID>\n"+
                "\t\t<cac:Party>\t\n" +
                "\t\t\t<cac:PartyIdentification>\t\n" +
                "\t\t\t\t<cbc:ID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\"31\">"+tmpMaster.getENC_CLINUMID()+"</cbc:ID>\n" +
                "\t\t\t</cac:PartyIdentification>\n" +
                "\t\t\t<cac:PartyName>\n" +
                "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("_","Y").replaceAll("<","").replaceAll(">","")+"</cbc:Name>\n" +
                "\t\t\t</cac:PartyName>\n" +
                "\t\t\t<cac:PhysicalLocation>\n" +
                "\t\t\t\t<cac:Address>\n" +
                "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadCliente+"</cbc:CountrySubentity>\n" +
                "\t\t\t\t\t<cbc:CountrySubentityCode>"+idCiudadCliente.substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                "\t\t\t\t\t<cac:AddressLine>\n" +
                "\t\t\t\t\t\t<cbc:Line>"+cli_dir.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                "\t\t\t\t\t</cac:AddressLine>\n" +
                "\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                "\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t</cac:Address>\n" +
                "\t\t\t</cac:PhysicalLocation>\n" +
                "\t\t\t<cac:PartyTaxScheme>\n" +
                "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:RegistrationName>\n";


        String paymentID = "Efectivo";

        if(tmpMaster.getENC_MEDIOPAGO().toString().equals("48")){
            paymentID = "Tarjeta Crédito";
        }

        if(tmpMaster.getENC_MEDIOPAGO().toString().equals("49")){
            paymentID = "Tarjeta Débito";
        }

        if(tmpMaster.getENC_MEDIOPAGO().toString().equals("47")){
            paymentID = "Transferencia Débito Bancaria";
        }




        String cliRespFiscal = tmpMaster.getENC_CLIRESPFISCAL();

        try{
            if(tmpMaster.getENC_CLIRESPFISCAL().equals("0") || tmpMaster.getENC_CLIRESPFISCAL() == null || tmpMaster.getENC_CLIRESPFISCAL().equals("null")||tmpMaster.getENC_CLIRESPFISCAL().equals("")){
                cliRespFiscal = "R-99-PN";
            }
        }catch(Exception e){
            cliRespFiscal = "R-99-PN";
        }


        String TaxLevelCode = "No Aplica";

        try{
            if(tmpMaster.getENC_CLIRESPIVA()== new Long(0)){
                TaxLevelCode = "49";
            }else{
                TaxLevelCode = tmpMaster.getENC_CLIRESPIVA().toString();
            }
        }catch(Exception e){
            TaxLevelCode = "49";
        }



        String invoiceRawXmlHead12 =  "\t\t\t\t<cbc:TaxLevelCode listName=\""+TaxLevelCode+"\">"+cliRespFiscal+"</cbc:TaxLevelCode>\n" +
                "\t\t\t\t<cac:RegistrationAddress>\n" +
                "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                "\t\t\t\t\t<cbc:CountrySubentity>"+countrySubEntity+"</cbc:CountrySubentity>\n" +
                "\t\t\t\t\t<cbc:CountrySubentityCode>"+tmpMaster.getENC_CIUDAD_SEDE().substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                "\t\t\t\t\t<cac:AddressLine>\n" +
                "\t\t\t\t\t\t<cbc:Line>"+cli_dir.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                "\t\t\t\t\t</cac:AddressLine>\n" +
                "\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                "\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t</cac:RegistrationAddress>\n" +
                "\t\t\t\t<cac:TaxScheme>\n" +
                "\t\t\t\t\t<cbc:ID>01</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:Name>IVA</cbc:Name>\n" +
                "\t\t\t\t</cac:TaxScheme>\n" +
                "\t\t\t</cac:PartyTaxScheme>\n" +
                "\t\t\t<cac:PartyLegalEntity>\n" +
                "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:RegistrationName>\n" +
                "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\"31\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n" +
                "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                "\t\t\t\t\t<cbc:Name>11111</cbc:Name>\n" +
                "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                "\t\t\t</cac:PartyLegalEntity>\n" +
                "\t\t\t<cac:Contact>\n" +
                "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:Name>\n" +
                "\t\t\t\t<cbc:Telephone>"+tmpMaster.getENC_CLITELELEF()+"</cbc:Telephone>\n" +
                "\t\t\t\t<cbc:ElectronicMail>"+cli_email+"</cbc:ElectronicMail>\n" +
                "\t\t\t</cac:Contact>\n" +
                "\t\t</cac:Party>\n" +
                "\t</cac:AccountingCustomerParty>\n" +
                "\t<cac:PaymentMeans>\n" +
                "\t\t<cbc:ID>"+paymentID+"</cbc:ID>\n" +
                "\t\t<cbc:PaymentMeansCode>"+tmpMaster.getENC_MEDIOPAGO()+"</cbc:PaymentMeansCode>\n" +
                "\t\t<cbc:PaymentDueDate>"+fechaPago+"</cbc:PaymentDueDate>\n" +
                "\t\t<cbc:PaymentID>"+paymentID+"</cbc:PaymentID>\n" +
                "\t</cac:PaymentMeans>\n" +
                "\t<cac:RequestedMonetaryTotal>\n" +
                "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:LineExtensionAmount><!-- Valor bruto del documento-->\n" +
                "\t\t<cbc:TaxExclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxExclusiveAmount><!-- Base imponilbe del documento-->\n" +
                "\t\t<cbc:TaxInclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxInclusiveAmount><!-- Base imponible + impuestos-->\n" +
                "\t\t<cbc:AllowanceTotalAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_DESCUENTO()).replaceAll(",",".")+"</cbc:AllowanceTotalAmount>\n" +
                //"\t\t<!-- <cbc:ChargeTotalAmount currencyID=\"COP\">"+ChargeTotalAmount+"<cbc:ChargeTotalAmount> Aplica cuando es cargo a nivel general de la factura, suma de los cargos generales-->\n" +
                "\t\t<cbc:PayableAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_TOTAL()).replaceAll(",",".")+"</cbc:PayableAmount><!--Valor a Pagar de Factura: Valor total de ítems (incluyendo cargos y descuentos a nivel de ítems)+valor tributos + valor cargos – valor descuentos – valor anticipos -->\n" +
                "\t</cac:RequestedMonetaryTotal>";


        String digitoVerificacion = "";
        String invoiceRawXmlHead = "";

        Boolean doesHttp = true;
        Boolean doesHttp2 = false;

        System.out.println("THIS IS CLINATU: "+tmpMaster.getENC_CLINATU());
        if(tmpMaster.getENC_CLINATU().toString().equals("1")){
            System.out.println("IM JURIDIC");
            try{
                String number =  tmpMaster.getENC_CLINUMID().split("-")[0];
                String digit = tmpMaster.getENC_CLINUMID().split("-")[1];
                digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\""+digit+"\"  schemeName=\"31\">"+number+"</cbc:CompanyID>\n";
                invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
            }catch(Exception  e){
                billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NRORECIBO(),"NO SE ENCONTRO DIGITO DE VERIFICACION EN EL DOCUMENTO. SYSWARE.");
                doesHttp = false;
            }
        }else{

            digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\"13\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
            invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
        }




        List<Detail> details = billDAO.getDetails(tmpMaster.getENC_PREFIJO(),
                tmpMaster.getENC_NRORECIBO(),
                tmpMaster.getENC_SEDE(),
                tmpMaster.getENC_VIGENCIA(),
                tmpMaster.getENC_CIAQUIPU(),
                tmpMaster.getENC_NUM_DOCUHIJO());


        String detailXmlInvoiceLine = "";

        for(int j = 0; j<details.size();j++){

            Detail myDetail = details.get(j);

            System.out.println("BANDERA: "+myDetail.getDET_CODIGO());
            JSONObject obj1 = new JSONObject();
            obj1.put("id", (j+1));
            try{
                obj1.put("note", myDetail.getDET_CODIGO().replaceAll("^(?i)null$", "%20").replaceAll("null", "%20"));
            }catch(Exception e){
                obj1.put("note", " ");
            }
            obj1.put("quantity", myDetail.getDET_CANTIDAD());
            obj1.put("amount", (myDetail.getDET_CANTIDAD() * myDetail.getDET_VLRUNIT()));
            obj1.put("taxAmount", 0.0);
            obj1.put("taxPercent", 0.0);
            obj1.put("taxId", 01);
            obj1.put("taxName", "IVA");
            obj1.put("description", myDetail.getDET_NOMBRE());
            try{
                obj1.put("itemId", myDetail.getDET_CODIGO().replaceAll("^(?i)null$", "%20").replaceAll("null", "%20"));
            }catch(Exception e){
                obj1.put("itemId", " ");
            }
            obj1.put("individualPrice", myDetail.getDET_VLRUNIT());
            obj1.put("baseQuantity", 1.00);

            System.out.println("itemId: "+obj1.get("itemId").toString());

            detailXmlInvoiceLine +=  generateDebitLines(new Integer(obj1.get("id").toString()),
                    obj1.get("note").toString().replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                    new Double(obj1.get("quantity").toString()),
                    new Double(obj1.get("amount").toString()),
                    new Double(obj1.get("taxAmount").toString()),
                    new Double(obj1.get("taxPercent").toString()),
                    new Integer(obj1.get("taxId").toString()),
                    obj1.get("taxName").toString(),
                    obj1.get("description").toString().replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                    obj1.get("itemId").toString(),
                    new Double(obj1.get("individualPrice").toString()),
                    new Double(obj1.get("baseQuantity").toString()));

        }
        String datpago = "";

        try{
            datpago = tmpMaster.getENC_DATOS_PAGO().replaceAll("%20","").replaceAll("<","").replaceAll(">","");

        }catch(Exception e){
            datpago = " ";
        }

        String invoiceRawXmlFooter = "<DATA>\n" +
                "\t\t<UBL21>true</UBL21>\n" +
                "\t\t<Development>\n" +
                "\t\t\t<ShortName>UNAL</ShortName>\n" +
                "\t\t</Development>\n" +
                "\t\t <Filters>\n" +
                "\t\t\t<Product ></Product><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                "\t\t\t<SubProduct></SubProduct><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                "\t\t\t<CUENTA/>\n" +
                "\t\t\t<NATURALEZA/>\n" +
                "\t\t\t<CENTRODECOSTO/>\n" +
                "\t\t\t<CLASE/>\n" +
                "\t\t\t<INDICE/>\n" +
                "\t\t</Filters>\n" +
                "\t\t<Opcional1> Somos grandes contribuyentes, no responsables de IVA </Opcional1> <!-- ID 04 Representación Gráfica -->\n" +
                "\t\t<Opcional2>  Agentes de retención de Impuesto sobre las ventas, no sujetos al impuesto de industria y comercio </Opcional2> <!-- ID 05 Representación Gráfica -->\n" +
                "\t\t<Opcional3> "+tmpMaster.getENC_CIAQUIPU()+" - "+tmpMaster.getENC_NRORECIBO()+"</Opcional3> <!-- ID 10 Representación Gráfica -->\n" +
                "\t\t<Opcional4>"+datpago+
                "#Somos Institución de Educación Superior no contribuyente del impuesto de renta,  favor abstenerse de efectuar retenciones en la  fuente. " +
                "#Resolución de habilitación de facturación electrónica No. 18764008055116 de fecha 25/11/2020. " +
                "#Correo: efactura_nal@unal.edu.co </Opcional4> <!-- ID 56 Representación Gráfica -->\n" +
                "\t</DATA></DebitNote>";



        //invoiceRawXmlFooter = "</Invoice>";



        String finalXML = (invoiceRawXmlHead + detailXmlInvoiceLine + invoiceRawXmlFooter).replaceAll("^(?i)null$", "%20").replaceAll("null", "%20");

        byte[] encodedBytes = Base64.encodeBase64(finalXML.getBytes());

        System.out.println("-------------------------------XML------------------------------");

        //System.out.println(finalXML);

        System.out.println("encodedBytes " + new String(encodedBytes));

        if(doesHttp){


            try {


                URL url = new URL(urlConsumption);//your url i.e fetch data from .
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("efacturaAuthorizationToken", "e14e6d8a-ed57-4de7-bc6d-7dba144240df");
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "text/plain");

                if(env == 2){
                    conn.setRequestProperty("efacturaAuthorizationToken", "d46b4d3c-d22d-41c8-af5d-6222564f9421");
                }

                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                os.write((new String(encodedBytes)).getBytes());
                os.flush();
                os.close();


                System.out.println(conn.getResponseCode());
                System.out.println(conn.getResponseMessage());

                if (conn.getResponseCode() != 200) {
                    InputStreamReader in2 = new InputStreamReader(conn.getErrorStream());
                    BufferedReader br2 = new BufferedReader(in2);
                    String output2;
                    System.out.println("ERROR: ");
                    String ErrorOutput = "";
                    while ((output2 = br2.readLine()) != null) {
                        System.out.println(output2);
                        ErrorOutput+=(" ; "+output2);
                    }
                    billDAO.updateTable("R",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),ErrorOutput);
                    throw new RuntimeException("Failed : HTTP Error code : "
                            + conn.getResponseCode());
                }else{
                    billDAO.updateTable("A",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),conn.getResponseMessage());
                }


                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;

                while ((output = br.readLine()) != null) {
                    System.out.println(output);
                }

                conn.disconnect();
            } catch (Exception e) {
                System.out.println("Exception in NetClientGet:- " + e);
            }


        }


        return new Long(1);

    }










    String generateInvoiceLines(int id,
                                String note,
                                Double quantity,
                                Double amount,
                                Double taxAmount,
                                Double taxPercent,
                                int taxId,
                                String taxName,
                                String description,
                                String itemId,
                                Double individualPrice,
                                Double baseQuantity){
        String invoiceLineEsqueleton = "<cac:InvoiceLine>\n" +
                "\t\t<cbc:ID>"+id+"</cbc:ID>\n" +
               // "\t\t<cbc:Note>"+note+"</cbc:Note><!-- deberá informarse dentro de los periodos de tres (3) días al año que la DIAN defina, en los que los bienes cubiertos se encuentren exentos del impuesto sobre las ventas.-->\n" +
                "\t\t<cbc:InvoicedQuantity unitCode=\"B7\">"+quantity+"</cbc:InvoicedQuantity>\n" +
                "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", amount).replaceAll(",",".")+"</cbc:LineExtensionAmount>\n" +
                "\t\t<!-- <cac:AllowanceCharge>\n" +
                "\t\t\t<cbc:ID>1</cbc:ID>\n" +
                "\t\t\t<cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                "\t\t\t<cbc:AllowanceChargeReason>Descuento por buen cliente</cbc:AllowanceChargeReason>\n" +
                "\t\t\t<cbc:MultiplierFactorNumeric>10.00</cbc:MultiplierFactorNumeric>\n" +
                "\t\t\t<cbc:Amount currencyID=\"COP\">10000.00</cbc:Amount>\n" +
                "\t\t\t<cbc:BaseAmount currencyID=\"COP\">100000.00</cbc:BaseAmount>\n" +
                "\t\t</cac:AllowanceCharge> Aplica cuando hay descuentos para el producto o servicio-->\n" +
                "\t\t<cac:TaxTotal>\n" +
                "\t\t\t<cbc:TaxAmount currencyID=\"COP\">"+taxAmount+"</cbc:TaxAmount><!--Sumatoria de los TaxAmount de los tax subtotal-->\t\t\t\n" +
                "\t\t\t<cac:TaxSubtotal>\n" +
                "\t\t\t\t<cbc:TaxableAmount currencyID=\"COP\">"+String.format("%.2f", amount).replaceAll(",",".")+"</cbc:TaxableAmount>\t\t\t\n" +
                "\t\t\t\t<cbc:TaxAmount currencyID=\"COP\">"+taxAmount+"</cbc:TaxAmount>\n" +
                "\t\t\t\t<cac:TaxCategory>\n" +
                "\t\t\t\t\t<cbc:Percent>"+taxPercent+"</cbc:Percent>\n" +
                "\t\t\t\t\t<cac:TaxScheme>\n" +
                "\t\t\t\t\t\t<cbc:ID>"+taxId+"</cbc:ID>\n" +
                "\t\t\t\t\t\t<cbc:Name>"+taxName+"</cbc:Name>\n" +
                "\t\t\t\t\t</cac:TaxScheme>\n" +
                "\t\t\t\t</cac:TaxCategory>\n" +
                "\t\t\t</cac:TaxSubtotal>\n" +
                "\t\t</cac:TaxTotal>\n"+
                "\t\t<cac:Item>\n" +
                "\t\t\t<cbc:Description>"+description.replaceAll("<","-").replaceAll(">","-")+" - "+note.replaceAll("<","-").replaceAll(">","-")+"</cbc:Description>\n" +
                "\t\t\t<cac:StandardItemIdentification>\n" +
                "\t\t\t\t<cbc:ID schemeID=\"999\">"+itemId+"</cbc:ID>\n" +
                "\t\t\t</cac:StandardItemIdentification>\n" +
                "\t\t</cac:Item>\n" +
                "\t\t<cac:Price>\n" +
                "\t\t\t<cbc:PriceAmount currencyID=\"COP\">"+String.format("%.2f", individualPrice).replaceAll(",",".")+"</cbc:PriceAmount>\n" +
                "\t\t\t<cbc:BaseQuantity unitCode=\"B7\">"+baseQuantity+"</cbc:BaseQuantity>\n" +
                "\t\t</cac:Price>\n" +
                "\t</cac:InvoiceLine>";


        return invoiceLineEsqueleton;

    }





    String generateCreditLines(int id,
                                String note,
                                Double quantity,
                                Double amount,
                                Double taxAmount,
                                Double taxPercent,
                                int taxId,
                                String taxName,
                                String description,
                                String itemId,
                                Double individualPrice,
                                Double baseQuantity){
        String invoiceLineEsqueleton = "<cac:CreditNoteLine>\n" +
                "\t\t<cbc:ID>"+id+"</cbc:ID>\n" +
                // "\t\t<cbc:Note>"+note+"</cbc:Note><!-- deberá informarse dentro de los periodos de tres (3) días al año que la DIAN defina, en los que los bienes cubiertos se encuentren exentos del impuesto sobre las ventas.-->\n" +
                "\t\t<cbc:CreditedQuantity unitCode=\"B7\">"+quantity+"</cbc:CreditedQuantity>\n" +
                "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", amount).replaceAll(",",".")+"</cbc:LineExtensionAmount>\n" +
                "\t\t<!-- <cac:AllowanceCharge>\n" +
                "\t\t\t<cbc:ID>1</cbc:ID>\n" +
                "\t\t\t<cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                "\t\t\t<cbc:AllowanceChargeReason>Descuento por buen cliente</cbc:AllowanceChargeReason>\n" +
                "\t\t\t<cbc:MultiplierFactorNumeric>10.00</cbc:MultiplierFactorNumeric>\n" +
                "\t\t\t<cbc:Amount currencyID=\"COP\">10000.00</cbc:Amount>\n" +
                "\t\t\t<cbc:BaseAmount currencyID=\"COP\">100000.00</cbc:BaseAmount>\n" +
                "\t\t</cac:AllowanceCharge> Aplica cuando hay descuentos para el producto o servicio-->\n" +
                "\t\t<cac:TaxTotal>\n" +
                "\t\t\t<cbc:TaxAmount currencyID=\"COP\">"+taxAmount+"</cbc:TaxAmount><!--Sumatoria de los TaxAmount de los tax subtotal-->\t\t\t\n" +
                "\t\t\t<cac:TaxSubtotal>\n" +
                "\t\t\t\t<cbc:TaxableAmount currencyID=\"COP\">"+String.format("%.2f", amount).replaceAll(",",".")+"</cbc:TaxableAmount>\t\t\t\n" +
                "\t\t\t\t<cbc:TaxAmount currencyID=\"COP\">"+taxAmount+"</cbc:TaxAmount>\n" +
                "\t\t\t\t<cac:TaxCategory>\n" +
                "\t\t\t\t\t<cbc:Percent>"+taxPercent+"</cbc:Percent>\n" +
                "\t\t\t\t\t<cac:TaxScheme>\n" +
                "\t\t\t\t\t\t<cbc:ID>"+taxId+"</cbc:ID>\n" +
                "\t\t\t\t\t\t<cbc:Name>"+taxName+"</cbc:Name>\n" +
                "\t\t\t\t\t</cac:TaxScheme>\n" +
                "\t\t\t\t</cac:TaxCategory>\n" +
                "\t\t\t</cac:TaxSubtotal>\n" +
                "\t\t</cac:TaxTotal>\n"+
                "\t\t<cac:Item>\n" +
                "\t\t\t<cbc:Description>"+description+"</cbc:Description>\n" +
                "\t\t\t<cac:StandardItemIdentification>\n" +
                "\t\t\t\t<cbc:ID schemeID=\"999\">"+itemId+"</cbc:ID>\n" +
                "\t\t\t</cac:StandardItemIdentification>\n" +
                "\t\t</cac:Item>\n" +
                "\t\t<cac:Price>\n" +
                "\t\t\t<cbc:PriceAmount currencyID=\"COP\">"+String.format("%.2f", individualPrice).replaceAll(",",".")+"</cbc:PriceAmount>\n" +
                "\t\t\t<cbc:BaseQuantity unitCode=\"B7\">"+baseQuantity+"</cbc:BaseQuantity>\n" +
                "\t\t</cac:Price>\n" +
                "\t</cac:CreditNoteLine>";


        return invoiceLineEsqueleton;

    }







    String generateDebitLines(int id,
                               String note,
                               Double quantity,
                               Double amount,
                               Double taxAmount,
                               Double taxPercent,
                               int taxId,
                               String taxName,
                               String description,
                               String itemId,
                               Double individualPrice,
                               Double baseQuantity){
        String invoiceLineEsqueleton = "<cac:DebitNoteLine>\n" +
                "\t\t<cbc:ID>"+id+"</cbc:ID>\n" +
                // "\t\t<cbc:Note>"+note+"</cbc:Note><!-- deberá informarse dentro de los periodos de tres (3) días al año que la DIAN defina, en los que los bienes cubiertos se encuentren exentos del impuesto sobre las ventas.-->\n" +
                "\t\t<cbc:DebitedQuantity unitCode=\"B7\">"+quantity+"</cbc:DebitedQuantity>\n" +
                "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", amount).replaceAll(",",".")+"</cbc:LineExtensionAmount>\n" +
                "\t\t<!-- <cac:AllowanceCharge>\n" +
                "\t\t\t<cbc:ID>1</cbc:ID>\n" +
                "\t\t\t<cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                "\t\t\t<cbc:AllowanceChargeReason>Descuento por buen cliente</cbc:AllowanceChargeReason>\n" +
                "\t\t\t<cbc:MultiplierFactorNumeric>10.00</cbc:MultiplierFactorNumeric>\n" +
                "\t\t\t<cbc:Amount currencyID=\"COP\">10000.00</cbc:Amount>\n" +
                "\t\t\t<cbc:BaseAmount currencyID=\"COP\">100000.00</cbc:BaseAmount>\n" +
                "\t\t</cac:AllowanceCharge> Aplica cuando hay descuentos para el producto o servicio-->\n" +
                "\t\t<cac:TaxTotal>\n" +
                "\t\t\t<cbc:TaxAmount currencyID=\"COP\">"+taxAmount+"</cbc:TaxAmount><!--Sumatoria de los TaxAmount de los tax subtotal-->\t\t\t\n" +
                "\t\t\t<cac:TaxSubtotal>\n" +
                "\t\t\t\t<cbc:TaxableAmount currencyID=\"COP\">"+String.format("%.2f", amount).replaceAll(",",".")+"</cbc:TaxableAmount>\t\t\t\n" +
                "\t\t\t\t<cbc:TaxAmount currencyID=\"COP\">"+taxAmount+"</cbc:TaxAmount>\n" +
                "\t\t\t\t<cac:TaxCategory>\n" +
                "\t\t\t\t\t<cbc:Percent>"+taxPercent+"</cbc:Percent>\n" +
                "\t\t\t\t\t<cac:TaxScheme>\n" +
                "\t\t\t\t\t\t<cbc:ID>"+taxId+"</cbc:ID>\n" +
                "\t\t\t\t\t\t<cbc:Name>"+taxName+"</cbc:Name>\n" +
                "\t\t\t\t\t</cac:TaxScheme>\n" +
                "\t\t\t\t</cac:TaxCategory>\n" +
                "\t\t\t</cac:TaxSubtotal>\n" +
                "\t\t</cac:TaxTotal>\n"+
                "\t\t<cac:Item>\n" +
                "\t\t\t<cbc:Description>"+description+"</cbc:Description>\n" +
                "\t\t\t<cac:StandardItemIdentification>\n" +
                "\t\t\t\t<cbc:ID schemeID=\"999\">"+itemId+"</cbc:ID>\n" +
                "\t\t\t</cac:StandardItemIdentification>\n" +
                "\t\t</cac:Item>\n" +
                "\t\t<cac:Price>\n" +
                "\t\t\t<cbc:PriceAmount currencyID=\"COP\">"+String.format("%.2f", individualPrice).replaceAll(",",".")+"</cbc:PriceAmount>\n" +
                "\t\t\t<cbc:BaseQuantity unitCode=\"B7\">"+baseQuantity+"</cbc:BaseQuantity>\n" +
                "\t\t</cac:Price>\n" +
                "\t</cac:DebitNoteLine>";


        return invoiceLineEsqueleton;

    }



    public Long readDatav2(Integer type, String fechaO, Integer days, String fileMaster, String fileDetail){

        try {
            List<MasterResponse> getResponseGeneric = new ArrayList<>();


            if(type == 1){
                getResponseGeneric = file(fileMaster, fileDetail);
            }else{

                String fecha = fechaO;



                for(int i=0; i<=days; i++){

                    System.out.println(fecha);


                    List<MasterResponse> getResponseGeneric2 = new ArrayList<>();
                    if(type == 2){
                        getResponseGeneric2 = sia(fecha);
                    }

                    if(type == 3){
                        getResponseGeneric2 = quipu(fecha);
                    }

                    if(type == 4){
                        getResponseGeneric2 = hermes(fecha);
                    }

                    if(type == 5){
                        getResponseGeneric2 = admisiones(fecha);
                    }

                    for(MasterResponse item : getResponseGeneric2){
                        getResponseGeneric.add(item);
                    }


                    Date date = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
                    Calendar c = Calendar.getInstance();
                    c.setTime(date);
                    c.add(Calendar.DATE, 1);
                    Date newDate = c.getTime();
                    fecha = new SimpleDateFormat("yyyy-MM-dd").format(newDate);

                }

            }

            for(MasterResponse item : getResponseGeneric){
                System.out.println("-----------------------------------------------------------------------------------------");




                String respFisc = item.getIn_clirespfiscal().replaceAll(" ","").replaceAll("%20","");
                //if(type == 4){
                    //respFisc = "";
                //}

                if(newMethod){
                    /*billDAO.poblar_tmpmaestro( data_obj.get("enc_PREFIJO").toString().replace(" ","%20"),
                            data_obj.get("enc_NRORECIBO").toString().replace(" ","%20"),
                            new Long(data_obj.get("enc_SEDE").toString().replace(" ","%20")),
                            new Long(data_obj.get("enc_VIGENCIA").toString().replace(" ","%20")),
                            data_obj.get("enc_NITEMES").toString().replace(" ","%20"),
                            data_obj.get("enc_NOMBRESEDE").toString().replace(" ","%20"),
                            data_obj.get("enc_EMPRESAQUIPU").toString().replace(" ","%20"),
                            date1,
                            new Long(data_obj.get("enc_TIPODOC").toString().replace(" ","%20")),
                            new Long(data_obj.get("enc_FORMAPAGO").toString().replace(" ","%20")),
                            date2,
                            new Long(data_obj.get("enc_MEDIOPAGO").toString().replace(" ","%20")),
                            new Long(data_obj.get("enc_DESCUENTO").toString().replace(" ","%20")),
                            new Double(data_obj.get("enc_SUBTOTAL").toString().replace(" ","%20")),
                            new Double(data_obj.get("enc_TOTAL").toString().replace(" ","%20")),
                            data_obj.get("enc_OBSERVACIONES").toString().replace(" ","%20"),
                            data_obj.get("enc_DATOS_PAGO").toString().replace(" ","%20"),
                            data_obj.get("enc_CIUDAD_SEDE").toString().replace(" ","%20"),
                            data_obj.get("enc_DIR_SEDE").toString().replace(" ","%20"),
                            data_obj.get("enc_TELEF_SEDE").toString().replace(" ","%20"),
                            data_obj.get("enc_EMAIL_SEDE_EMISION").toString().replace(" ","%20"),
                            new Long(data_obj.get("enc_CLINATU").toString().replace(" ","%20")),
                            data_obj.get("enc_CLINOMB").toString().replace(" ","%20"),
                            data_obj.get("enc_CLI_TIPOID").toString().replace(" ","%20"),
                            data_obj.get("enc_CLI_NUMID").toString().replace(" ","%20"),
                            data_obj.get("enc_CLICIUDAD").toString().replace(" ","%20"),
                            data_obj.get("enc_CLIDIR").toString().replace(" ","%20"),
                            data_obj.get("enc_CLITELEF").toString().replace(" ","%20"),
                            data_obj.get("enc_CLIEMAIL").toString().replace(" ","%20"),
                            data_obj.get("enc_CLIRESPFIS").toString().replace(" ","%20"),
                            new Long(data_obj.get("enc_CLIRESPIVA").toString().replace(" ","%20")),
                            new Long(1),
                            new Long(1),
                            in_num_docuhijosend
                    );*/


                }else{



                System.out.println("http://localhost:8680/v1/unalfe/master?in_prefijo="+item.getIn_prefijo().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_nrorecibo="+item.getIn_nrorecibo().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_sede="+item.getIn_sede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_vigencia="+item.getIn_vigencia().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_ciaquipu="+item.getIn_ciaquipu().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_nombresede="+item.getIn_nombresede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_empresaquipu="+item.getIn_empresaquipu().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_fecemic="+item.getIn_fecemic().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_tipodoc="+item.getIn_tipodoc().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_formapago="+item.getIn_formapago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_fecpago="+item.getIn_fecpago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_mediopago="+item.getIn_mediopago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_descuento="+item.getIn_descuento().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_subtotal="+item.getIn_subtotal().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_total="+item.getIn_total().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_observaciones="+item.getIn_observaciones().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_datos_pago="+item.getIn_datos_pago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_ciudad_sede="+item.getIn_ciudad_sede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_dir_sede="+item.getIn_dir_sede().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_telef_sede="+item.getIn_telef_sede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_email_sede_emision="+item.getIn_email_sede_emision().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clinatu="+item.getIn_clinatu().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clinomb="+item.getIn_clinomb().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_clitipoid="+item.getIn_clitipoid().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clinumid="+item.getIn_clinumid().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_cliciudad="+item.getIn_cliciudad().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clidir="+item.getIn_clidir().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_clitelelef="+item.getIn_clitelelef().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_cliemail="+item.getIn_cliemail().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_clirespfiscal="+respFisc.replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clirespiva="+item.getIn_clirespiva().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_origen="+item.getIn_origen().replaceAll("null","")
                        +"&in_forma_datos_origen="+item.getIn_forma_datos_origen().replaceAll("null","")
                        +"&in_num_docuhijo="+item.getIn_num_docuhijo().replaceAll("null","").replaceAll("^(?i)null$", "").replaceAll("\t","").replaceAll("#","No\\.").replaceAll("\n","%20")
                );
                URL url = new URL("http://localhost:8680/v1/unalfe/master?in_prefijo="+item.getIn_prefijo().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_nrorecibo="+item.getIn_nrorecibo().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_sede="+item.getIn_sede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_vigencia="+item.getIn_vigencia().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_ciaquipu="+item.getIn_ciaquipu().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_nombresede="+item.getIn_nombresede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_empresaquipu="+item.getIn_empresaquipu().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_fecemic="+item.getIn_fecemic().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_tipodoc="+item.getIn_tipodoc().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_formapago="+item.getIn_formapago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_fecpago="+item.getIn_fecpago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_mediopago="+item.getIn_mediopago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_descuento="+item.getIn_descuento().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_subtotal="+item.getIn_subtotal().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_total="+item.getIn_total().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_observaciones="+item.getIn_observaciones().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_datos_pago="+item.getIn_datos_pago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_ciudad_sede="+item.getIn_ciudad_sede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_dir_sede="+item.getIn_dir_sede().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_telef_sede="+item.getIn_telef_sede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_email_sede_emision="+item.getIn_email_sede_emision().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clinatu="+item.getIn_clinatu().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clinomb="+item.getIn_clinomb().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_clitipoid="+item.getIn_clitipoid().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clinumid="+item.getIn_clinumid().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_cliciudad="+item.getIn_cliciudad().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clidir="+item.getIn_clidir().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_clitelelef="+item.getIn_clitelelef().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_cliemail="+item.getIn_cliemail().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_clirespfiscal="+respFisc.replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clirespiva="+item.getIn_clirespiva().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_origen="+item.getIn_origen().replaceAll("null","")
                        +"&in_forma_datos_origen="+item.getIn_forma_datos_origen().replaceAll("null","")
                        +"&in_num_docuhijo="+item.getIn_num_docuhijo().replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                );//your url i.e fetch data from .
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");

                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                os.write((new String("")).getBytes());
                os.flush();
                os.close();


                System.out.println("RESPONSE CODE: "+conn.getResponseCode());
                System.out.println("MESSAGE CODE: "+conn.getResponseMessage());

                if (conn.getResponseCode() != 200) {
                    System.out.println("Failed : HTTP Error code : "
                            + conn.getResponseCode());
                    //throw new RuntimeException("Failed : HTTP Error code : "
                      //      + conn.getResponseCode());
                }
                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;
                while ((output = br.readLine()) != null) {
                    System.out.println("OUTPUT: "+output);
                }
                conn.disconnect();
                }

                List<DetailResponse> getResponseGenericDetail = item.getIn_detail();


                for(DetailResponse detalle : getResponseGenericDetail){

                    System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

                    String docNumHijo = detalle.getIn_num_docuhijo();

                    if(detalle.getIn_num_docuhijo().equals("%20")){
                        docNumHijo = detalle.getIn_nrorecibo();
                    }

                    System.out.println("http://localhost:8680/v1/unalfe/detail?in_prefijo="+detalle.getIn_prefijo().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_nrorecibo="+detalle.getIn_nrorecibo().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_sede="+detalle.getIn_sede().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_vigencia="+detalle.getIn_vigencia()
                            +"&in_ciaquipu="+detalle.getIn_ciaquipu().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_cantidad="+detalle.getIn_cantidad().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_vlrunit="+detalle.getIn_vlrunit().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_pct_desc="+detalle.getIn_pct_desc().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_codigo="+detalle.getIn_codigo().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_nombre="+detalle.getIn_nombre().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll("\r","%20")
                            +"&in_descrip="+detalle.getIn_descrip().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll("\r","%20")
                            +"&in_num_docuhijo="+docNumHijo.replaceAll(" ","").replaceAll("^(?i)null$", "").replaceAll("%20",""));
                    URL url = new URL("http://localhost:8680/v1/unalfe/detail?in_prefijo="+detalle.getIn_prefijo().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_nrorecibo="+detalle.getIn_nrorecibo().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_sede="+detalle.getIn_sede().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_vigencia="+detalle.getIn_vigencia()
                            +"&in_ciaquipu="+detalle.getIn_ciaquipu().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_cantidad="+detalle.getIn_cantidad().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_vlrunit="+detalle.getIn_vlrunit().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_pct_desc="+detalle.getIn_pct_desc().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_codigo="+detalle.getIn_codigo().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_nombre="+detalle.getIn_nombre().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\r","%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_descrip="+detalle.getIn_descrip().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\r","%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_num_docuhijo="+docNumHijo.replaceAll(" ","").replaceAll("^(?i)null$", "").replaceAll("%20",""));//your url i.e fetch data from ."\n"


                    HttpURLConnection conn2 = (HttpURLConnection)
                            url.openConnection();
                    conn2.setRequestMethod("POST");
                    conn2.setRequestProperty("Content-Type", "application/json");

                    conn2.setDoOutput(true);
                    OutputStream os2 = conn2.getOutputStream();
                    os2.write((new String("")).getBytes());
                    os2.flush();
                    os2.close();


                    System.out.println("RESPONSE CODE: "+conn2.getResponseCode());
                    System.out.println("MESSAGE CODE: "+conn2.getResponseMessage());

                    if (conn2.getResponseCode() != 200) {
                        System.out.println("Failed : HTTP Error code : "
                                + conn2.getResponseCode());
                        //throw new RuntimeException("Failed : HTTP Error code : "
                                //+ conn2.getResponseCode());
                    }
                    InputStreamReader in2 = new InputStreamReader(conn2.getInputStream());
                    BufferedReader br2 = new BufferedReader(in2);
                    String output2;
                    while ((output2 = br2.readLine()) != null) {
                        System.out.println("OUTPUT: "+output2);
                    }
                    conn2.disconnect();

                    System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                }
                System.out.println("--------------------------------------------------------------------------------------");

            }

            return new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }


    public Long readData(Integer type, String fecha, String master, String detail){

        try{

            List<List<String>> masterDataList = new ArrayList<>();
            List<List<String>> detailDataList = new ArrayList<>();

            if(type == 1){

                String row = "";

                BufferedReader csvReader = null;
                try {
                    csvReader = new BufferedReader(new FileReader("./"+master+".CSV"));
                    while ((row = csvReader.readLine()) != null) {
                        String[] masterData = row.split(";", -1);
                        System.out.println("-------------------------------HEADER------------------------------");
                        System.out.println("LENGTH: "+masterData.length);
                        System.out.println("LIST: "+masterData.toString());
                        List<String> masterDataAsList = Arrays.asList(masterData);
                        masterDataList.add(masterDataAsList);
                    }
                    csvReader.close();
                } catch (Exception e) {
                        e.printStackTrace();
                }
                try {
                    csvReader = new BufferedReader(new FileReader("./"+detail+".csv"));
                    while ((row = csvReader.readLine()) != null) {
                        String[] detailData = row.split(";",-1);
                       // detailData = insert(detailData,"2020",3);
                        System.out.println("-------------------------------DETAIL------------------------------");
                        System.out.println("LENGTH: "+detailData.length);
                        System.out.println("LIST: "+detailData.toString());
                        List<String> detailDataListAsList = Arrays.asList(detailData);
                        detailDataList.add(detailDataListAsList);
                    }
                    csvReader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                if(type == 2){
                    try {
                        URL urlGet = new URL("http://localhost:8580/v1/unalfe/master?fecha="+fecha);//your url i.e fetch data from .
                        HttpURLConnection conn2 = (HttpURLConnection) urlGet.openConnection();
                        conn2.setRequestMethod("GET");
                        conn2.setRequestProperty("Accept", "application/json");

                        if (conn2.getResponseCode() != 200) {
                            throw new RuntimeException("Failed : HTTP Error code : "
                                    + conn2.getResponseCode());
                        }

                        InputStreamReader in = new InputStreamReader(conn2.getInputStream());
                        BufferedReader br = new BufferedReader(in);
                        String output;
                        JSONObject data_obj = new JSONObject();
                        while ((output = br.readLine()) != null) {
                            System.out.println("OUTPUT:"+output);
                            String [] listaTemporalMaestros = output.substring(1,output.length()-1).split("},");
                            for(String element: listaTemporalMaestros){
                            System.out.println("ELEMENT: "+element);
                            data_obj = new JSONObject(element+"}");
                            System.out.println("OUTPUT_JSON: " + data_obj.toString());
                            ArrayList<String> ar = new ArrayList<String>();
                            ar.add(data_obj.get("enc_PREFIJO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_NRORECIBO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_SEDE").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_VIGENCIA").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_NITEMES").toString().replaceAll("null",""));
                            ar.add(data_obj.get("enc_NOMBRESEDE").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                            ar.add(data_obj.get("enc_EMPRESAQUIPU").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_FECEMIC").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_TIPODOC").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_FORMAPAGO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_FECPAGO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_MEDIOPAGO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_DESCUENTO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_SUBTOTAL").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_TOTAL").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_OBSERVACIONES").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_DATOS_PAGO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CIUDAD_SEDE").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_DIR_SEDE").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                            ar.add(data_obj.get("enc_TELEF_SEDE").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_EMAIL_SEDE_EMISION").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLINATU").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLINOMB").toString().replaceAll("&","_").replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLI_TIPOID").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLI_NUMID").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLICIUDAD").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLIDIR").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                            ar.add(data_obj.get("enc_CLITELEF").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLIEMAIL").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLIRESPFIS").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLIRESPIVA").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_NRORECIBO").toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                            masterDataList.add(ar);


                            try {
                                URL urlGet2 = new URL("http://localhost:8580/v1/unalfe/detail?det_nrorecibo="+data_obj.get("enc_NRORECIBO").toString());//your url i.e fetch data from .
                                HttpURLConnection conn22 = (HttpURLConnection) urlGet2.openConnection();
                                conn22.setRequestMethod("GET");
                                conn22.setRequestProperty("Accept", "application/json");

                                if (conn22.getResponseCode() != 200) {
                                    throw new RuntimeException("Failed : HTTP Error code : "
                                            + conn22.getResponseCode());
                                }

                                InputStreamReader in2 = new InputStreamReader(conn22.getInputStream());
                                BufferedReader br2 = new BufferedReader(in2);
                                String output2;
                                JSONObject data_obj2 = new JSONObject();
                                while ((output2 = br2.readLine()) != null) {
                                    System.out.println(output2);
                                    data_obj2 = new JSONObject(output2.substring(1,output2.length()-1));
                                    ArrayList<String> arDet = new ArrayList<String>();
                                    arDet.add(data_obj2.get("det_PREFIJO").toString());
                                    arDet.add(data_obj2.get("det_NRORECIBO").toString());
                                    arDet.add(data_obj2.get("det_SEDE").toString());
                                    arDet.add((2021+""));
                                    arDet.add(data_obj2.get("det_NITEMES").toString());
                                    arDet.add(data_obj2.get("det_CANTIDA").toString());
                                    arDet.add(data_obj2.get("det_VLRUNIT").toString());
                                    arDet.add(data_obj2.get("det_PCT_DESC").toString());
                                    arDet.add(data_obj2.get("det_CODIGO").toString());
                                    arDet.add(data_obj2.get("det_NOMBRE").toString());
                                    arDet.add(data_obj2.get("det_DESCRIP").toString());
                                    arDet.add(data_obj2.get("det_NRORECIBO").toString());
                                    detailDataList.add(arDet);
                                }

                            }catch(Exception e){
                                e.printStackTrace();
                                return new Long(0);
                            }
                            }
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                        return new Long(0);
                    }



                    }else{
                    if(type == 3){

                        try {
                            List<MasterQuipu> headerList = billDAO.getMasterQuipu(fecha);


                            for(int i = 0; i < headerList.size();i++){
                                MasterQuipu tmp = headerList.get(i);
                                ArrayList<String> ar = new ArrayList<String>();
                                try{
                                    ar.add(tmp.getENC_PREFIJO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                ar.add(tmp.getENC_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_VIGENCIA().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_NITEMES());
                                try{
                                    ar.add(tmp.getENC_NOMBRESEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                ar.add(tmp.getENC_EMPRESAQUIPU().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_FECEMIC().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_TIPODOC().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_FORMAPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                try{
                                    ar.add(tmp.getENC_FECPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                ar.add(tmp.getENC_MEDIOPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_DESCUENTO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_SUBTOTAL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_TOTAL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                try{
                                    ar.add(tmp.getENC_OBSERVACIONES().toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_DATOS_PAGO().toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_CIUDAD_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));

                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_DIR_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_TELEF_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_EMAIL_SEDE_EMISION().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                ar.add(tmp.getENC_CLINATU().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_CLINOMB().toString().replaceAll("&","_").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_CLI_TIPOID().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                try{
                                    ar.add(tmp.getENC_CLI_NUMID().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }try{
                                    ar.add(tmp.getENC_CLICIUDAD().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }  try{
                                    ar.add(tmp.getENC_CLIDIR().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_CLITELEF().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_CLIEMAIL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_CLIRESPFIS().toString().replaceAll(" ", "").replaceAll("^(?i)null$", ""));
                                }catch(NullPointerException e){
                                    ar.add("".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", ""));
                                }
                                try{
                                    ar.add(tmp.getENC_CLIRESPIVA().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", ""));
                                }
                                try{
                                    if(tmp.getENC_TIPODOC() == new Long(1)){
                                        ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                                    }else{
                                        ar.add(tmp.getENC_NUM_DOCUHIJO().toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                                    }
                                }catch(NullPointerException e){
                                    ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                System.out.print("ARRAY: ");
                                for (String element: ar) {
                                    System.out.print(element+", ");
                                }
                                System.out.println("");
                                System.out.println("SIZE: "+ar.size());

                                masterDataList.add(ar);
                                try {
                                    List<DetailQuipu> detailList = billDAO.getDetailsQuipu(tmp.getENC_PREFIJO(),
                                                                                           tmp.getENC_NRORECIBO(),
                                                                                           tmp.getENC_SEDE(),
                                                                                           tmp.getENC_NITEMES(),
                                                                                           tmp.getENC_NUM_DOCUHIJO()
                                            );
                                    for(int j = 0; j < detailList.size();j++){
                                        DetailQuipu tmp2 = detailList.get(j);
                                        ArrayList<String> arDet = new ArrayList<String>();
                                        System.out.println(tmp2);
                                        arDet.add(tmp2.getDET_PREFIJO().toString().replace(" ","%20"));
                                        arDet.add(tmp2.getDET_NRORECIBO().toString().replace(" ","%20"));
                                        arDet.add(tmp2.getDET_SEDE().toString().replace(" ","%20"));
                                        arDet.add((2021+"").replace(" ","%20"));
                                        arDet.add(tmp2.getDET_CIAQUIPU().toString().replace(" ","%20"));
                                        arDet.add(tmp2.getDET_CANTIDA().toString().replace(" ","%20"));
                                        arDet.add(tmp2.getDET_VLRUNIT().toString().replace(" ","%20"));
                                        arDet.add(tmp2.getDET_PCT_DESC().toString().replace(" ","%20"));
                                        try{
                                            arDet.add(tmp2.getDET_CODIGO().toString().replace(" ","%20"));
                                        }catch(NullPointerException e){
                                            arDet.add(" ".toString().replace(" ","%20"));
                                        }
                                        try{
                                            arDet.add(tmp2.getDET_NOMBRE().toString().replace(" ","%20"));
                                        }catch(NullPointerException e){
                                            arDet.add(" ".toString().replace(" ","%20"));
                                        }
                                        try{
                                            arDet.add(tmp2.getDET_DESCRIP().toString().replace(" ","%20"));
                                        }catch(NullPointerException e){
                                            arDet.add(" ".toString().replace(" ","%20"));
                                        }
                                        try{
                                            arDet.add(tmp2.getDET_NUM_DOCUHIJO().toString().replace(" ","%20"));
                                        }catch(NullPointerException e){
                                            arDet.add(" ".toString().replace(" ","%20"));
                                        }
                                        System.out.println("OUTPUT2: "+arDet.toString());
                                        detailDataList.add(arDet);
                                    }

                                }catch(Exception e){
                                    e.printStackTrace();
                                    return new Long(0);
                                }

                            }


                        }catch(Exception e){
                            e.printStackTrace();
                            return new Long(0);
                        }


                    }else{
                        if(type==5){

                            HttpURLConnection conn22 = (HttpURLConnection) new URL("https://uninscripciones.unal.edu.co/sitio/services/facturacionWebService/datos-facturacion-pines").openConnection();
                            conn22.setRequestProperty("Accept", "application/json");
                            conn22.setRequestProperty("Content-Type", "application/json");
                            conn22.setRequestMethod("POST");
                            conn22.setDoOutput(true);
                            String jsonInputString = "{\"fechaInscripcion\" : \""+fecha+"\"}";

                            byte[] authEncBytes = Base64.encodeBase64("facturacionElectronicaWS:rugywu5u6t".getBytes());
                            String authStringEnc = new String(authEncBytes);

                            conn22.setRequestProperty("Authorization", "Basic " + authStringEnc);


                            try(OutputStream os = conn22.getOutputStream()) {
                                byte[] input = jsonInputString.getBytes();
                                os.write(input, 0, input.length);
                            }

                            OutputStream os = conn22.getOutputStream();
                            os.write((new String("")).getBytes());
                            os.flush();
                            os.close();


                            System.out.println("RESPONSE CODE: "+conn22.getResponseCode());
                            System.out.println("MESSAGE CODE: "+conn22.getResponseMessage());

                            if (conn22.getResponseCode() != 200) {
                                throw new RuntimeException("Failed : HTTP Error code : "
                                        + conn22.getResponseCode());
                            }

                            InputStreamReader in = new InputStreamReader(conn22.getInputStream());
                            BufferedReader br = new BufferedReader(in);
                            String output;
                            while ((output = br.readLine()) != null) {
                                JSONArray array = new JSONArray(output);
                                for(Object o: array){
                                    if ( o instanceof JSONObject ) {
                                        JSONObject element = (JSONObject)o;
                                        ArrayList<String> ar = new ArrayList<String>();
                                        ar.add(element.get("in_prefijo").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_nrorecibo").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_sede").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_vigencia").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_ciaquipu").toString().replaceAll("null",""));
                                        ar.add(element.get("in_nombresede").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                        ar.add(element.get("in_empresaquipu").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_fecemic").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_tipodoc").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_formapago").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_fecpago").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_mediopago").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_descuento").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_subtotal").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_total").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_observaciones").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_datos_pago").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_ciudad_sede").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_dir_sede").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                        ar.add(element.get("in_telef_sede").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_email_sede_emision").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clinatu").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clinomb").toString().replaceAll("&","_").replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clitipoid").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clinumid").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_cliciudad").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clidir").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                        ar.add(element.get("in_clitelelef").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_cliemail").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clirespfiscal").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clirespiva").toString().replaceAll(" ", "%20"));
                                        if(element.get("in_num_docuhijo").toString().equals("null")){
                                            ar.add(element.get("in_nrorecibo").toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                                        }else{
                                            ar.add(element.get("in_num_docuhijo").toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                                        }
                                        masterDataList.add(ar);

                                        JSONObject detailNow = new JSONObject(element.get("in_detail").toString());
                                        ArrayList<String> arDet = new ArrayList<String>();
                                        arDet.add(detailNow.get("tdet_prefijo").toString());
                                        arDet.add(detailNow.get("tdet_nrorecibo").toString());
                                        arDet.add(detailNow.get("tdet_sede").toString());
                                        arDet.add((2021+""));
                                        arDet.add(detailNow.get("tdet_ciaquipu").toString());
                                        arDet.add(detailNow.get("tdet_cantidad").toString());
                                        arDet.add(detailNow.get("tdet_vlrunit").toString());
                                        arDet.add(detailNow.get("tenc_descuento").toString());
                                        arDet.add(detailNow.get("tdet_codigo").toString());
                                        arDet.add(detailNow.get("tdet_nombre").toString());
                                        arDet.add(detailNow.get("tdet_descrip").toString());
                                        arDet.add(detailNow.get("tdet_nrorecibo").toString());
                                        detailDataList.add(arDet);
                                    }
                                }
                            }
                            conn22.disconnect();
                        }else{
                            return new Long(-1);
                        }
                    }
                }
            }

            try{
                for(int i = 0; i < masterDataList.size();i++){
                    List<String> data = masterDataList.get(i);
                    System.out.println(data.toString());
                    String numDocuHijo = data.get(1);
                    try{

                        if(new Integer(data.get(8)) == 1){
                            numDocuHijo = data.get(1);
                        }else{
                            numDocuHijo = data.get(31);
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                        System.out.println("ELEMENT: "+data.toString());
                    }





                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String dateInString = data.get(7);
                    String dateInString2 = data.get(10);


                    Date date1 = new Date();
                    Date date2 = new Date();
                    try {


                        Date utilDate = formatter.parse(dateInString);
                        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                        date1 = sqlDate;

                        try{
                            Date utilDate2 = formatter.parse(dateInString2);
                            java.sql.Date sqlDate2 = new java.sql.Date(utilDate2.getTime());
                            date2 = sqlDate2;
                        }catch(Exception e){
                            date2 = sqlDate;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if(newMethod){
                        String in_num_docuhijosend = data.get(31);
                        if (data.get(31) == null){
                            in_num_docuhijosend = data.get(31);
                        }
                    billDAO.poblar_tmpmaestro(data.get(0),
                            data.get(1),
                            new Long(data.get(2)),
                            new Long(data.get(3)),
                            data.get(4),
                            data.get(5),
                            data.get(6),
                            date1,
                            new Long(data.get(8)),
                            new Long(data.get(9)),
                            date2,
                            new Long(data.get(11)),
                            new Long(data.get(12)),
                            new Double(data.get(13)),
                            new Double(data.get(14)),
                            data.get(15),
                            data.get(16),
                            data.get(17),
                            data.get(18),
                            data.get(19),
                            data.get(20),
                            new Long(data.get(21)),
                            data.get(22),
                            data.get(23),
                            data.get(24),
                            data.get(25),
                            data.get(26),
                            data.get(27),
                            data.get(28),
                            data.get(29),
                            new Long(data.get(30)),
                            new Long(1),
                            new Long(1),
                            in_num_docuhijosend
                    );
                    }else{

                    System.out.println("http://localhost:8680/v1/unalfe/master?in_prefijo="+data.get(0).replaceAll(" ","%20")
                            +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20")
                            +"&in_sede="+data.get(2).replaceAll(" ","%20")
                            +"&in_vigencia="+data.get(3).replaceAll(" ","%20")
                            +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20")
                            +"&in_nombresede="+data.get(5).replaceAll(" ","%20")
                            +"&in_empresaquipu="+data.get(6).replaceAll(" ","%20")
                            +"&in_fecemic="+data.get(7).replaceAll(" ","%20")
                            +"&in_tipodoc="+data.get(8).replaceAll(" ","%20")
                            +"&in_formapago="+data.get(9).replaceAll(" ","%20")
                            +"&in_fecpago="+data.get(10).replaceAll(" ","%20")
                            +"&in_mediopago="+data.get(11).replaceAll(" ","%20")
                            +"&in_descuento="+data.get(12).replaceAll(" ","%20")
                            +"&in_subtotal="+data.get(13).replaceAll(" ","%20")
                            +"&in_total="+data.get(14).replaceAll(" ","%20")
                            +"&in_observaciones="+data.get(15).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_datos_pago="+data.get(16).replaceAll(" ","%20")
                            +"&in_ciudad_sede="+data.get(17).replaceAll(" ","%20")
                            +"&in_dir_sede="+data.get(18).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_telef_sede="+data.get(19).replaceAll(" ","%20")
                            +"&in_email_sede_emision="+data.get(20).replaceAll(" ","%20")
                            +"&in_clinatu="+data.get(21).replaceAll(" ","%20")
                            +"&in_clinomb="+data.get(22).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_clitipoid="+data.get(23).replaceAll(" ","%20")
                            +"&in_clinumid="+data.get(24).replaceAll(" ","%20")
                            +"&in_cliciudad="+data.get(25).replaceAll(" ","%20")
                            +"&in_clidir="+data.get(26).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_clitelelef="+data.get(27).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_cliemail="+data.get(28).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_clirespfiscal="+data.get(29).replaceAll(" ","%20")
                            +"&in_clirespiva="+data.get(30).replaceAll(" ","%20")
                            +"&in_origen="+1
                            +"&in_forma_datos_origen="+1
                            +"&in_num_docuhijo="+numDocuHijo.replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                    );
                    URL url = new URL("http://localhost:8680/v1/unalfe/master?in_prefijo="+data.get(0).replaceAll(" ","%20")
                    +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20")
                    +"&in_sede="+data.get(2).replaceAll(" ","%20")
                    +"&in_vigencia="+data.get(3).replaceAll(" ","%20")
                    +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20")
                    +"&in_nombresede="+data.get(5).replaceAll(" ","%20")
                    +"&in_empresaquipu="+data.get(6).replaceAll(" ","%20")
                    +"&in_fecemic="+data.get(7).replaceAll(" ","%20")
                    +"&in_tipodoc="+data.get(8).replaceAll(" ","%20")
                    +"&in_formapago="+data.get(9).replaceAll(" ","%20")
                    +"&in_fecpago="+data.get(10).replaceAll(" ","%20")
                    +"&in_mediopago="+data.get(11).replaceAll(" ","%20")
                    +"&in_descuento="+data.get(12).replaceAll(" ","%20")
                    +"&in_subtotal="+data.get(13).replaceAll(" ","%20")
                    +"&in_total="+data.get(14).replaceAll(" ","%20")
                    +"&in_observaciones="+data.get(15).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                    +"&in_datos_pago="+data.get(16).replaceAll(" ","%20")
                    +"&in_ciudad_sede="+data.get(17).replaceAll(" ","%20")
                    +"&in_dir_sede="+data.get(18).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                    +"&in_telef_sede="+data.get(19).replaceAll(" ","%20")
                    +"&in_email_sede_emision="+data.get(20).replaceAll(" ","%20")
                    +"&in_clinatu="+data.get(21).replaceAll(" ","%20")
                    +"&in_clinomb="+data.get(22).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                    +"&in_clitipoid="+data.get(23).replaceAll(" ","%20")
                    +"&in_clinumid="+data.get(24).replaceAll(" ","%20")
                    +"&in_cliciudad="+data.get(25).replaceAll(" ","%20")
                    +"&in_clidir="+data.get(26).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                    +"&in_clitelelef="+data.get(27).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                    +"&in_cliemail="+data.get(28).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                    +"&in_clirespfiscal="+data.get(29).replaceAll(" ","%20")
                    +"&in_clirespiva="+data.get(30).replaceAll(" ","%20")
                    +"&in_origen="+1
                    +"&in_forma_datos_origen="+1
                    +"&in_num_docuhijo="+numDocuHijo.replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                    ); //your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            os.write((new String("")).getBytes());
            os.flush();
            os.close();


            System.out.println("RESPONSE CODE: "+conn.getResponseCode());
            System.out.println("MESSAGE CODE: "+conn.getResponseMessage());

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            while ((output = br.readLine()) != null) {
                System.out.println("OUTPUT: "+output);
            }
            conn.disconnect();
            }
        }
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }

            try {
                for(int i = 0; i < detailDataList.size();i++){

                List<String> data = detailDataList.get(i);

                System.out.print("---------------------------------------------------------");
                System.out.println("data stuff: "+data.toString());



                    URL url = new URL("http://localhost:8680");
                    if(type==3){
                        System.out.println("http://localhost:8680/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2021"
                                +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                                +"&in_descrip="+data.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                                +"&in_num_docuhijo="+data.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from ."\n"

                        url = new URL("http://localhost:8680/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2021"
                                +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                                +"&in_descrip="+data.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                                +"&in_num_docuhijo="+data.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from ."\n"

                    }
                    if(type==2){
                        System.out.println(data.toString());
                        System.out.println("http://localhost:8680/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2021"
                                +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(9).replaceAll("\t","%20").replaceAll("^(?i)null$", "%20").replaceAll(" ","%20")
                                +"&in_descrip="+data.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20")
                                +"&in_num_docuhijo="+data.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from .

                        url = new URL("http://localhost:8680/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2021"
                                +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                                +"&in_descrip="+data.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                                +"&in_num_docuhijo="+data.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from ."\n"

                    }
                    if(type==1){
                        String numDocuHijo;
                        if(data.size() == 10){
                            numDocuHijo = data.get(1);
                        }else{
                            numDocuHijo = data.get(10);
                        }
                        System.out.println("http://localhost:8680/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2021"
                                +"&in_ciaquipu="+data.get(3).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_descrip="+data.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_num_docuhijo="+numDocuHijo.replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from .

                        url = new URL("http://localhost:8680/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2021"
                                +"&in_ciaquipu="+data.get(3).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20")
                                +"&in_descrip="+data.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20")
                                +"&in_num_docuhijo="+numDocuHijo.replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from .

                    }


                    if(type==5){
                        System.out.println("http://localhost:8680/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2021"
                                +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(9).replaceAll("\t","%20").replaceAll("^(?i)null$", "%20").replaceAll(" ","%20")
                                +"&in_descrip="+data.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20")
                                +"&in_num_docuhijo="+data.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from .

                        url = new URL("http://localhost:8680/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2021"
                                +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                                +"&in_descrip="+data.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                                +"&in_num_docuhijo="+data.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from ."\n"

                    }

                HttpURLConnection conn = (HttpURLConnection)
                url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");

                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                os.write((new String("")).getBytes());
                os.flush();
                os.close();


                System.out.println("RESPONSE CODE: "+conn.getResponseCode());
                System.out.println("MESSAGE CODE: "+conn.getResponseMessage());

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP Error code : "
                            + conn.getResponseCode());
                }
                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;
                while ((output = br.readLine()) != null) {
                    System.out.println("OUTPUT: "+output);
                }
                conn.disconnect();
                }
            }catch(Exception e){
                e.printStackTrace();
                return new Long(0);
            }

            return new Long(1);

        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }


    Long getTablaFinal(){
        try{

            billDAO.asigna_consecutivo_dian();
            return new Long(1);

        }catch(Exception e){
            return new Long(0);
        }

    }


    Long updateTable(String estado,
                     String PREFIJO,
                     String NRORECIBO,
                     Long SEDE,
                     Long VIGENCIA,
                     String CIAQUIPU){
        try{

            billDAO.updateTable( estado,
                                 PREFIJO,
                                 NRORECIBO,
                                 SEDE,
                                 VIGENCIA,
                                 CIAQUIPU,NRORECIBO,"OK");

            return new Long(1);

        }catch(Exception e){
            return new Long(0);
        }

    }


    public List<MasterQuipu> masterQuipu(String fecha){
        return billDAO.getMasterQuipu( fecha );
    }


    public List<String> masterFile(){

        String row = "";
        BufferedReader csvReader = null;

        List<String> responseList = new ArrayList<String>();

        try {
            csvReader = new BufferedReader(new FileReader("./FE_MAESTRO.CSV"));
            while ((row = csvReader.readLine()) != null) {
                responseList.add(row);
            }
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseList;
    }


    public List<String> masterFile2(String filename){

        String row = "";
        BufferedReader csvReader = null;

        List<String> responseList = new ArrayList<String>();

        try {
            csvReader = new BufferedReader(new FileReader("./"+filename+".CSV"));
            while ((row = csvReader.readLine()) != null) {
                responseList.add(row);
            }
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseList;
    }


    public List<MasterResponse> quipu(String fecha){

        List<MasterResponse> response = new ArrayList<MasterResponse>();

        List<MasterQuipu> listMasterQuipu = masterQuipu(fecha);

        for(int i = 0; i<listMasterQuipu.size();i++){

            MasterQuipu tmp = listMasterQuipu.get(i);
            ArrayList<String> ar = new ArrayList<String>();

            ar.add(tmp.getENC_PREFIJO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            ar.add(tmp.getENC_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_VIGENCIA().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_NITEMES());
            ar.add(tmp.getENC_NOMBRESEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_EMPRESAQUIPU().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_FECEMIC().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_TIPODOC().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_FORMAPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_FECPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            ar.add(tmp.getENC_MEDIOPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_DESCUENTO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_SUBTOTAL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_TOTAL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_OBSERVACIONES().toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_DATOS_PAGO().toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            ar.add(tmp.getENC_CIUDAD_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_DIR_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_TELEF_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_EMAIL_SEDE_EMISION().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            ar.add(tmp.getENC_CLINATU().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_CLINOMB().toString().replaceAll("&","_").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_CLI_TIPOID().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_CLI_NUMID().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }try{
                ar.add(tmp.getENC_CLICIUDAD().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }  try{
                ar.add(tmp.getENC_CLIDIR().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_CLITELEF().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_CLIEMAIL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_CLIRESPFIS().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_CLIRESPIVA().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                if(tmp.getENC_TIPODOC() == new Long(1)){
                    ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                }else{
                    ar.add(tmp.getENC_NUM_DOCUHIJO().toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                }
            }catch(NullPointerException e){
                ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }

            String numDocuHijo = ar.get(1);
            try{

                if(new Integer(ar.get(8)) == 1){
                    numDocuHijo = ar.get(1);
                }else{
                    numDocuHijo = ar.get(31);
                }
            }catch(Exception e){
                e.printStackTrace();
                System.out.println("ELEMENT: "+ar.toString());
            }

            MasterResponse newMaster = new MasterResponse(ar.get(0).replaceAll(" ","%20"),
                    ar.get(1).replaceAll(" ","%20"),
                    ar.get(2).replaceAll(" ","%20"),
                    ar.get(3).replaceAll(" ","%20"),
                    ar.get(4).replaceAll(" ","%20"),
                    ar.get(5).replaceAll(" ","%20"),
                    ar.get(6).replaceAll(" ","%20"),
                    ar.get(7).replaceAll(" ","%20"),
                    ar.get(8).replaceAll(" ","%20"),
                    ar.get(9).replaceAll(" ","%20"),
                    ar.get(10).replaceAll(" ","%20"),
                    ar.get(11).replaceAll(" ","%20"),
                    ar.get(12).replaceAll(" ","%20"),
                    ar.get(13).replaceAll(" ","%20"),
                    ar.get(14).replaceAll(" ","%20"),
                    ar.get(15).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                    ar.get(16).replaceAll(" ","%20"),
                    ar.get(17).replaceAll(" ","%20"),
                    ar.get(18).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                    ar.get(19).replaceAll(" ","%20"),
                    ar.get(20).replaceAll(" ","%20"),
                    ar.get(21).replaceAll(" ","%20"),
                    ar.get(22).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                    ar.get(23).replaceAll(" ","%20"),
                    ar.get(24).replaceAll(" ","%20"),
                    ar.get(25).replaceAll(" ","%20"),
                    ar.get(26).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                    ar.get(27).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                    ar.get(28).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                    ar.get(29).replaceAll(" ","%20"),
                    ar.get(30).replaceAll(" ","%20"),
                    "1",
                    "1",
                    numDocuHijo.replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"));


            List<DetailResponse> filteredDetail = new ArrayList<>();


            List<DetailQuipu> listDetailsQuipu = billDAO.getDetailsQuipu(tmp.getENC_PREFIJO(),
                    tmp.getENC_NRORECIBO(),
                    tmp.getENC_SEDE(),
                    tmp.getENC_NITEMES(),
                    tmp.getENC_NUM_DOCUHIJO()
            );

            for(int j=0;j<listDetailsQuipu.size();j++){
                DetailQuipu tmp2 = listDetailsQuipu.get(j);
                ArrayList<String> arDet = new ArrayList<String>();

                arDet.add(tmp2.getDET_PREFIJO().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_NRORECIBO().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_SEDE().toString().replace(" ","%20"));
                arDet.add((2021+"").replace(" ","%20"));
                arDet.add(tmp2.getDET_CIAQUIPU().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_CANTIDA().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_VLRUNIT().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_PCT_DESC().toString().replace(" ","%20"));
                try{
                    arDet.add(tmp2.getDET_CODIGO().toString().replace(" ","%20"));
                }catch(NullPointerException e){
                    arDet.add(" ".toString().replace(" ","%20"));
                }
                try{
                    arDet.add(tmp2.getDET_NOMBRE().toString().replace(" ","%20"));
                }catch(NullPointerException e){
                    arDet.add(" ".toString().replace(" ","%20"));
                }
                try{
                    arDet.add(tmp2.getDET_DESCRIP().toString().replace(" ","%20"));
                }catch(NullPointerException e){
                    arDet.add(" ".toString().replace(" ","%20"));
                }
                try{
                    arDet.add(tmp2.getDET_NUM_DOCUHIJO().toString().replace(" ","%20"));
                }catch(NullPointerException e){
                    arDet.add(" ".toString().replace(" ","%20"));
                }


                DetailResponse detailToAdd = new DetailResponse(

                        arDet.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        "2021",
                        arDet.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20"),
                        arDet.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20"),
                        arDet.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")

                );

                filteredDetail.add(detailToAdd);

            }
            newMaster.setIn_detail(filteredDetail);
            response.add(newMaster);
        }
        return response;
    }



    public List<MasterResponse> sia(String fecha) throws IOException {

        List<MasterResponse> response = new ArrayList<MasterResponse>();

        try {
            URL urlGet = new URL("http://localhost:8580/v1/unalfe/master?fecha="+fecha);//your url i.e fetch data from .
            HttpURLConnection conn2 = (HttpURLConnection) urlGet.openConnection();
            conn2.setRequestMethod("GET");
            conn2.setRequestProperty("Accept", "application/json");

            if (conn2.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn2.getResponseCode());
            }

            InputStreamReader in = new InputStreamReader(conn2.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            JSONObject data_obj = new JSONObject();
            while ((output = br.readLine()) != null) {
                String [] listaTemporalMaestros = output.substring(1,output.length()-1).split("},");
                for(String element: listaTemporalMaestros){
                    data_obj = new JSONObject(element+"}");
                    ArrayList<String> ar = new ArrayList<String>();
                    ar.add(data_obj.get("enc_PREFIJO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_NRORECIBO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_SEDE").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_VIGENCIA").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_NITEMES").toString().replaceAll("null",""));
                    ar.add(data_obj.get("enc_NOMBRESEDE").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                    ar.add(data_obj.get("enc_EMPRESAQUIPU").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_FECEMIC").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_TIPODOC").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_FORMAPAGO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_FECPAGO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_MEDIOPAGO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_DESCUENTO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_SUBTOTAL").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_TOTAL").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_OBSERVACIONES").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_DATOS_PAGO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CIUDAD_SEDE").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_DIR_SEDE").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                    ar.add(data_obj.get("enc_TELEF_SEDE").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_EMAIL_SEDE_EMISION").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLINATU").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLINOMB").toString().replaceAll("&","_").replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLI_TIPOID").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLI_NUMID").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLICIUDAD").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLIDIR").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                    ar.add(data_obj.get("enc_CLITELEF").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLIEMAIL").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLIRESPFIS").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLIRESPIVA").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_NRORECIBO").toString().replaceAll(" ","%20"));//your url i.e fetch data from .

                    String numDocuHijo = ar.get(1);
                    try{

                        if(new Integer(ar.get(8)) == 1){
                            numDocuHijo = ar.get(1);
                        }else{
                            numDocuHijo = ar.get(31);
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }


                    MasterResponse newMaster = new MasterResponse(ar.get(0).replaceAll(" ","%20"),
                            ar.get(1).replaceAll(" ","%20"),
                            ar.get(2).replaceAll(" ","%20"),
                            ar.get(3).replaceAll(" ","%20"),
                            ar.get(4).replaceAll(" ","%20"),
                            ar.get(5).replaceAll(" ","%20"),
                            ar.get(6).replaceAll(" ","%20"),
                            ar.get(7).replaceAll(" ","%20"),
                            ar.get(8).replaceAll(" ","%20"),
                            ar.get(9).replaceAll(" ","%20"),
                            ar.get(10).replaceAll(" ","%20"),
                            ar.get(11).replaceAll(" ","%20"),
                            ar.get(12).replaceAll(" ","%20"),
                            ar.get(13).replaceAll(" ","%20"),
                            ar.get(14).replaceAll(" ","%20"),
                            ar.get(15).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                            ar.get(16).replaceAll(" ","%20"),
                            ar.get(17).replaceAll(" ","%20"),
                            ar.get(18).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                            ar.get(19).replaceAll(" ","%20"),
                            ar.get(20).replaceAll(" ","%20"),
                            ar.get(21).replaceAll(" ","%20"),
                            ar.get(22).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                            ar.get(23).replaceAll(" ","%20"),
                            ar.get(24).replaceAll(" ","%20"),
                            ar.get(25).replaceAll(" ","%20"),
                            ar.get(26).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                            ar.get(27).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                            ar.get(28).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                            ar.get(29).replaceAll(" ","%20"),
                            ar.get(30).replaceAll(" ","%20"),
                            "1",
                            "1",
                            numDocuHijo.replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"));


                    List<DetailResponse> filteredDetail = new ArrayList<>();

                    try {
                        URL urlGet2 = new URL("http://localhost:8580/v1/unalfe/detail?det_nrorecibo="+data_obj.get("enc_NRORECIBO").toString());//your url i.e fetch data from .
                        HttpURLConnection conn22 = (HttpURLConnection) urlGet2.openConnection();
                        conn22.setRequestMethod("GET");
                        conn22.setRequestProperty("Accept", "application/json");

                        if (conn22.getResponseCode() != 200) {
                            throw new RuntimeException("Failed : HTTP Error code : "
                                    + conn22.getResponseCode());
                        }

                        InputStreamReader in2 = new InputStreamReader(conn22.getInputStream());
                        BufferedReader br2 = new BufferedReader(in2);
                        String output2;
                        JSONObject data_obj2 = new JSONObject();
                        while ((output2 = br2.readLine()) != null) {
                            data_obj2 = new JSONObject(output2.substring(1,output2.length()-1));
                            ArrayList<String> arDet = new ArrayList<String>();
                            arDet.add(data_obj2.get("det_PREFIJO").toString());
                            arDet.add(data_obj2.get("det_NRORECIBO").toString());
                            arDet.add(data_obj2.get("det_SEDE").toString());
                            arDet.add((2021+""));
                            arDet.add(data_obj2.get("det_NITEMES").toString());
                            arDet.add(data_obj2.get("det_CANTIDA").toString());
                            arDet.add(data_obj2.get("det_VLRUNIT").toString());
                            arDet.add(data_obj2.get("det_PCT_DESC").toString());
                            arDet.add(data_obj2.get("det_CODIGO").toString());
                            arDet.add(data_obj2.get("det_NOMBRE").toString());
                            arDet.add(data_obj2.get("det_DESCRIP").toString());
                            arDet.add(data_obj2.get("det_NRORECIBO").toString());


                            DetailResponse detailToAdd = new DetailResponse(

                                    arDet.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    "2021",
                                    arDet.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20"),
                                    arDet.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20"),
                                    arDet.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")

                            );

                            filteredDetail.add(detailToAdd);

                        }

                        newMaster.setIn_detail(filteredDetail);
                        response.add(newMaster);

                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }


        }catch(Exception e){
            e.printStackTrace();
        }
        return response;
    }



    public List<MasterResponse> hermes(String fecha) {

        List<MasterResponse> response = new ArrayList<MasterResponse>();

        try {
            URL urlGet = new URL("http://localhost:8487/v1/invoice/hermes?fecha="+fecha);//your url i.e fetch data from .
            HttpURLConnection conn2 = (HttpURLConnection) urlGet.openConnection();
            conn2.setRequestMethod("GET");
            conn2.setRequestProperty("Accept", "application/json");

            if (conn2.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn2.getResponseCode());
            }

            InputStreamReader in = new InputStreamReader(conn2.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            List <MasterResponse> responseList = new ArrayList<>();
            while ((output = br.readLine()) != null) {

                String [] listaTemporalMaestros = output.substring(1,output.length()-1).split("},");
                System.out.println(output);

                for(String element: listaTemporalMaestros){
                    JSONObject data_obj = new JSONObject(element+"}");
                    MasterResponse temp = new MasterResponse();

                    temp.setIn_prefijo(data_obj.get("in_prefijo").toString());
                    temp.setIn_nrorecibo(data_obj.get("in_nrorecibo").toString());
                    temp.setIn_sede(data_obj.get("in_sede").toString());
                    temp.setIn_vigencia(data_obj.get("in_vigencia").toString());
                    temp.setIn_ciaquipu(data_obj.get("in_ciaquipu").toString());
                    temp.setIn_nombresede(data_obj.get("in_nombresede").toString());
                    temp.setIn_empresaquipu(data_obj.get("in_empresaquipu").toString());
                    temp.setIn_fecemic(data_obj.get("in_fecemic").toString());
                    temp.setIn_tipodoc(data_obj.get("in_tipodoc").toString());
                    temp.setIn_formapago(data_obj.get("in_formapago").toString());
                    temp.setIn_fecpago(data_obj.get("in_fecpago").toString());
                    temp.setIn_mediopago(data_obj.get("in_mediopago").toString());
                    temp.setIn_descuento(data_obj.get("in_descuento").toString());
                    temp.setIn_subtotal(data_obj.get("in_subtotal").toString());
                    temp.setIn_total(data_obj.get("in_total").toString());
                    temp.setIn_observaciones(data_obj.get("in_observaciones").toString());
                    temp.setIn_datos_pago(data_obj.get("in_datos_pago").toString());
                    temp.setIn_ciudad_sede(data_obj.get("in_ciudad_sede").toString());
                    temp.setIn_dir_sede(data_obj.get("in_dir_sede").toString());
                    temp.setIn_telef_sede(data_obj.get("in_telef_sede").toString());
                    temp.setIn_email_sede_emision(data_obj.get("in_email_sede_emision").toString());
                    temp.setIn_clinatu(data_obj.get("in_clinatu").toString());
                    temp.setIn_clinomb(data_obj.get("in_clinomb").toString());
                    temp.setIn_clitipoid(data_obj.get("in_clitipoid").toString());
                    temp.setIn_clinumid(data_obj.get("in_clinumid").toString());
                    temp.setIn_cliciudad(data_obj.get("in_cliciudad").toString());
                    temp.setIn_clidir(data_obj.get("in_clidir").toString());
                    temp.setIn_clitelelef(data_obj.get("in_clitelelef").toString());
                    temp.setIn_cliemail(data_obj.get("in_cliemail").toString());
                    temp.setIn_clirespfiscal(data_obj.get("in_clirespfiscal").toString());
                    temp.setIn_clirespiva(data_obj.get("in_clirespiva").toString());
                    temp.setIn_origen(data_obj.get("in_origen").toString());
                    temp.setIn_cliemail(data_obj.get("in_cliemail").toString());
                    temp.setIn_forma_datos_origen(data_obj.get("in_forma_datos_origen").toString());
                    temp.setIn_num_docuhijo(data_obj.get("in_num_docuhijo").toString());

                    List <DetailResponse> responseListDet = new ArrayList<>();


                    String [] listaTemporalDetalles = data_obj.get("in_detail").toString().substring(1,data_obj.get("in_detail").toString().length()-1).split("},");

                    for(String detail: listaTemporalDetalles){
                        JSONObject data_objD = new JSONObject(detail+"}");
                        DetailResponse tempD = new DetailResponse();

                        tempD.setIn_prefijo(data_objD.get("in_prefijo").toString());
                        tempD.setIn_nrorecibo(data_objD.get("in_nrorecibo").toString());
                        tempD.setIn_sede(data_objD.get("in_sede").toString());
                        tempD.setIn_vigencia(data_objD.get("in_vigencia").toString());
                        tempD.setIn_ciaquipu(data_objD.get("in_ciaquipu").toString());
                        tempD.setIn_cantidad(data_objD.get("in_cantidad").toString());
                        tempD.setIn_vlrunit(data_objD.get("in_vlrunit").toString());
                        tempD.setIn_pct_desc(data_objD.get("in_pct_desc").toString());
                        tempD.setIn_codigo(data_objD.get("in_codigo").toString());
                        tempD.setIn_nombre(data_objD.get("in_nombre").toString());
                        tempD.setIn_descrip(data_objD.get("in_descrip").toString());
                        tempD.setIn_num_docuhijo(data_objD.get("in_num_docuhijo").toString());

                        responseListDet.add(tempD);
                    }

                    temp.setIn_detail(responseListDet);

                    responseList.add(temp);
                }
            }


            return responseList;


        }catch(Exception e){
            e.printStackTrace();
        }
        return response;
    }



    public List<MasterResponse> admisiones(String fecha) {

        List<MasterResponse> response = new ArrayList<MasterResponse>();

        try {
            URL urlGet = new URL("http://localhost:84875/v1/invoice/hermes?fecha="+fecha);//your url i.e fetch data from .
            HttpURLConnection conn2 = (HttpURLConnection) urlGet.openConnection();
            conn2.setRequestMethod("GET");
            conn2.setRequestProperty("Accept", "application/json");

            if (conn2.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn2.getResponseCode());
            }

            InputStreamReader in = new InputStreamReader(conn2.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            List <MasterResponse> responseList = new ArrayList<>();
            while ((output = br.readLine()) != null) {

                String [] listaTemporalMaestros = output.substring(1,output.length()-1).split("},");

                for(String element: listaTemporalMaestros){
                    JSONObject data_obj = new JSONObject(element+"}");
                    MasterResponse temp = new MasterResponse();

                    temp.setIn_prefijo(data_obj.get("in_prefijo").toString());
                    temp.setIn_nrorecibo(data_obj.get("in_nrorecibo").toString());
                    temp.setIn_sede(data_obj.get("in_sede").toString());
                    temp.setIn_vigencia(data_obj.get("in_vigencia").toString());
                    temp.setIn_ciaquipu(data_obj.get("in_ciaquipu").toString());
                    temp.setIn_nombresede(data_obj.get("in_nombresede").toString());
                    temp.setIn_empresaquipu(data_obj.get("in_empresaquipu").toString());
                    temp.setIn_fecemic(data_obj.get("in_fecemic").toString());
                    temp.setIn_tipodoc(data_obj.get("in_tipodoc").toString());
                    temp.setIn_formapago(data_obj.get("in_formapago").toString());
                    temp.setIn_fecpago(data_obj.get("in_fecpago").toString());
                    temp.setIn_mediopago(data_obj.get("in_mediopago").toString());
                    temp.setIn_descuento(data_obj.get("in_descuento").toString());
                    temp.setIn_subtotal(data_obj.get("in_subtotal").toString());
                    temp.setIn_total(data_obj.get("in_total").toString());
                    temp.setIn_observaciones(data_obj.get("in_observaciones").toString());
                    temp.setIn_datos_pago(data_obj.get("in_datos_pago").toString());
                    temp.setIn_ciudad_sede(data_obj.get("in_ciudad_sede").toString());
                    temp.setIn_dir_sede(data_obj.get("in_dir_sede").toString());
                    temp.setIn_telef_sede(data_obj.get("in_telef_sede").toString());
                    temp.setIn_email_sede_emision(data_obj.get("in_email_sede_emision").toString());
                    temp.setIn_clinatu(data_obj.get("in_clinatu").toString());
                    temp.setIn_clinomb(data_obj.get("in_clinomb").toString());
                    temp.setIn_clitipoid(data_obj.get("in_clitipoid").toString());
                    temp.setIn_clinumid(data_obj.get("in_clinumid").toString());
                    temp.setIn_cliciudad(data_obj.get("in_cliciudad").toString());
                    temp.setIn_clidir(data_obj.get("in_clidir").toString());
                    temp.setIn_clitelelef(data_obj.get("in_clitelelef").toString());
                    temp.setIn_cliemail(data_obj.get("in_cliemail").toString());
                    temp.setIn_clirespfiscal(data_obj.get("in_clirespfiscal").toString());
                    temp.setIn_clirespiva(data_obj.get("in_clirespiva").toString());
                    temp.setIn_origen(data_obj.get("in_origen").toString());
                    temp.setIn_cliemail(data_obj.get("in_cliemail").toString());
                    temp.setIn_forma_datos_origen(data_obj.get("in_forma_datos_origen").toString());
                    temp.setIn_num_docuhijo(data_obj.get("in_num_docuhijo").toString());

                    List <DetailResponse> responseListDet = new ArrayList<>();


                    String [] listaTemporalDetalles = data_obj.get("in_detail").toString().substring(1,data_obj.get("in_detail").toString().length()-1).split("},");

                    for(String detail: listaTemporalDetalles){
                        JSONObject data_objD = new JSONObject(detail+"}");
                        DetailResponse tempD = new DetailResponse();

                        tempD.setIn_prefijo(data_objD.get("in_prefijo").toString());
                        tempD.setIn_nrorecibo(data_objD.get("in_nrorecibo").toString());
                        tempD.setIn_sede(data_objD.get("in_sede").toString());
                        tempD.setIn_vigencia(data_objD.get("in_vigencia").toString());
                        tempD.setIn_ciaquipu(data_objD.get("in_ciaquipu").toString());
                        tempD.setIn_cantidad(data_objD.get("in_cantidad").toString());
                        tempD.setIn_vlrunit(data_objD.get("in_vlrunit").toString());
                        tempD.setIn_pct_desc(data_objD.get("in_pct_desc").toString());
                        tempD.setIn_codigo(data_objD.get("in_codigo").toString());
                        tempD.setIn_nombre(data_objD.get("in_nombre").toString());
                        tempD.setIn_descrip(data_objD.get("in_descrip").toString());
                        tempD.setIn_num_docuhijo(data_objD.get("in_num_docuhijo").toString());

                        responseListDet.add(tempD);
                    }

                    temp.setIn_detail(responseListDet);

                    responseList.add(temp);
                }
            }


            return responseList;


        }catch(Exception e){
            e.printStackTrace();
        }
        return response;
    }


    public List<MasterResponse> file(String fileMaster, String fileDetail) {

        List<MasterResponse> response = new ArrayList<MasterResponse>();


        List<String> masterString = masterFile2(fileMaster);
        List<String> detailString = detailFile2(fileDetail);

        List<String[]> detailList = new ArrayList<String[]>();
        List<String[]> masterList = new ArrayList<String[]>();



        for(int i = 0; i< detailString.size();i++){
            String[] detail = detailString.get(i).split(";");
            detailList.add(detail);
        }

        for(int i = 0; i< masterString.size();i++){
            String[] detail = masterString.get(i).split(";");
            masterList.add(detail);
        }


        for(int i = 0; i< masterString.size();i++){

            String[] master = masterString.get(i).split(";");

            String numDocuHijo = master[1];

            try{

                if(new Integer(master[8]) == 1){
                    numDocuHijo = master[1];
                }else{
                    numDocuHijo = master[31];
                }
            }catch(Exception e){
                e.printStackTrace();
                System.out.println("ELEMENT: "+master.toString());
            }


            System.out.println(masterString.get(i));
            System.out.println(master.length);


            MasterResponse newMaster = new MasterResponse(master[0].replaceAll(" ","%20"),
                                                          master[1].replaceAll(" ","%20"),
                                                          master[2].replaceAll(" ","%20"),
                                                          master[3].replaceAll(" ","%20"),
                                                          master[4].replaceAll(" ","%20"),
                                                          master[5].replaceAll(" ","%20"),
                                                          master[6].replaceAll(" ","%20"),
                                                          master[7].replaceAll(" ","%20"),
                                                          master[8].replaceAll(" ","%20"),
                                                          master[9].replaceAll(" ","%20"),
                                                          master[10].replaceAll(" ","%20"),
                                                          master[11].replaceAll(" ","%20"),
                                                          master[12].replaceAll(" ","%20"),
                                                          master[13].replaceAll(" ","%20"),
                                                          master[14].replaceAll(" ","%20"),
                                                          master[15].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                                          master[16].replaceAll(" ","%20"),
                                                          master[17].replaceAll(" ","%20"),
                                                          master[18].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                                          master[19].replaceAll(" ","%20"),
                                                          master[20].replaceAll(" ","%20"),
                                                          master[21].replaceAll(" ","%20"),
                                                          master[22].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                                          master[23].replaceAll(" ","%20"),
                                                          master[24].replaceAll(" ","%20"),
                                                          master[25].replaceAll(" ","%20"),
                                                          master[26].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                                          master[27].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                                          master[28].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                                          master[29].replaceAll(" ","%20"),
                                                          master[30].replaceAll(" ","%20"),
                                                          "1",
                                                          "1",
                                                          numDocuHijo.replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"));

            List<DetailResponse> filteredDetail = new ArrayList<>();

            for(int j = 0; j < detailList.size();j++){
                String[] element = detailList.get(j);
                if(element[1].equals(master[1]) && element[0].equals(master[0])){
                    String numDocuHijoD;
                    if(element.length == 10){
                        numDocuHijoD = element[1];
                    }else{
                        numDocuHijoD = element[10];
                    }
                    DetailResponse detailToAdd = new DetailResponse(

                            element[0].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[1].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[2].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            "2021",
                            element[3].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[4].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[5].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[6].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[7].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[8].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20"),
                            element[9].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20"),
                            numDocuHijoD.replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")

                    );

                    filteredDetail.add(detailToAdd);
                }
            }

            newMaster.setIn_detail(filteredDetail);

            response.add(newMaster);
        }



        return response;

    }



    public List<String> detailFile(){

        String row = "";
        BufferedReader csvReader = null;

        List<String> responseList = new ArrayList<String>();

        try {
            csvReader = new BufferedReader(new FileReader("./FE_DETALLE.CSV"));
            while ((row = csvReader.readLine()) != null) {
                responseList.add(row);
            }
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseList;
    }




    public List<String> detailFile2(String filename){

        String row = "";
        BufferedReader csvReader = null;

        List<String> responseList = new ArrayList<String>();

        try {
            csvReader = new BufferedReader(new FileReader("./"+filename+".CSV"));
            while ((row = csvReader.readLine()) != null) {
                responseList.add(row);
            }
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseList;
    }



    public List<DetailQuipu> detailQuipu(String PREFIJO,
                                    String NRORECIBO,
                                    Long SEDE,
                                    String CIAQUIPU,
                                     String NUMDOCUHIJO){
        return billDAO.getDetailsQuipu( PREFIJO,
                 NRORECIBO,
                 SEDE,
                 CIAQUIPU,
                NUMDOCUHIJO);
    }


}