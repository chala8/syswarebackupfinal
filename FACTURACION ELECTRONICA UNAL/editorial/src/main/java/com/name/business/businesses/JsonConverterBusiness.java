package com.name.business.businesses;
import com.name.business.DAOs.JsonConverterDao;
import com.name.business.entities.Detail;
import com.name.business.entities.Master;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.apache.commons.codec.binary.Base64;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;
import org.json.XML;


public class JsonConverterBusiness {

    private JsonConverterDao billDAO;
    //VARIABLES PARA CONEXIÓN CORREO
    static final String FROM = "soporte@tienda724.com";
    static final String FROMNAME = "Soporte Tienda 724";
    static final String SMTP_USERNAME = "soporte@tienda724.com";
    static final String SMTP_PASSWORD = "Lx3BPR5j8pdt2xmm";
    static final String HOST = "smtp.mail.us-east-1.awsapps.com";
    static final int PORT = 465;
    ///////////////////////////////////

    public JsonConverterBusiness(JsonConverterDao billDAO) {
        this.billDAO = billDAO;
    }


    public List<Master> master(String fecha){

        return billDAO.master(convertDate1(fecha),convertDate2(fecha) );

    }

    public String convertDate1(String fecha){
        String [] list = fecha.split("-");
        return list[0] + list[1] + list[2] + " 00:00:00.000" ;
    }

    public String convertDate2(String fecha){
        String [] list = fecha.split("-");
        return list[0] + list[1] + list[2] + " 23:59:59.997" ;
    }


    public List<Detail> detail( String fecha ){
        System.out.println("FECHA: ");
        System.out.println(convertDate1(fecha)+"____"+convertDate2(fecha));
        List<Detail> lista = billDAO.detail( convertDate1(fecha),convertDate2(fecha) );
        System.out.println("TAMAÑO LISTA: ");
        System.out.println(lista.size());
        for(Detail element: lista){
            if(element.getFACTURA_RELACIONADA()=="null"||element.getFACTURA_RELACIONADA()==null){
                element.setFACTURA_RELACIONADA(element.getDET_NRORECIBO());
            }
        }
        return lista;
    }






}