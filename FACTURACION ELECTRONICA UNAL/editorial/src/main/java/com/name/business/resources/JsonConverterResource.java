package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.JsonConverterBusiness;
//import com.name.business.entities.*;
import com.name.business.entities.Detail;
import com.name.business.entities.Master;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Date;


@Path("/editorial")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JsonConverterResource {
    private JsonConverterBusiness jsonConverterBusiness;

    public JsonConverterResource(JsonConverterBusiness jsonConverterBusiness) {
        this.jsonConverterBusiness = jsonConverterBusiness;
    }


    @Path("/master")
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response masterProcedure(@QueryParam("fecha")String fecha){
        Response response;
        List<Master> getResponseGeneric = jsonConverterBusiness.master(fecha);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }

    @Path("/detail")
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response detail(
            @QueryParam("fecha")String fecha
    ){
        Response response;
        List<Detail> getResponseGeneric = jsonConverterBusiness.detail(fecha);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }

}