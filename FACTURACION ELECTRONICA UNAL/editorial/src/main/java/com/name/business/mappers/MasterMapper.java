package com.name.business.mappers;

import com.name.business.entities.Master;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MasterMapper implements ResultSetMapper<Master> {

    @Override
    public Master map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Master(
                resultSet.getString("ENC_PREFIJO"),
                resultSet.getString("ENC_NRORECIBO"),
                resultSet.getLong("ENC_SEDE"),
                resultSet.getLong("ENC_VIGENCIA"),
                resultSet.getString("ENC_NITEMES"),
                resultSet.getString("ENC_NOMBRESEDE"),
                resultSet.getString("ENC_EMPRESAQUIPU"),
                resultSet.getString("ENC_FECEMIC"),
                resultSet.getLong("ENC_TIPODOC"),
                resultSet.getLong("ENC_FORMAPAGO"),
                resultSet.getString("ENC_FECPAGO"),
                resultSet.getLong("ENC_MEDIOPAGO"),
                resultSet.getLong("ENC_DESCUENTO"),
                resultSet.getDouble("ENC_SUBTOTAL"),
                resultSet.getDouble("ENC_TOTAL"),
                resultSet.getString("ENC_OBSERVACIONES"),
                resultSet.getString("ENC_DATOS_PAGO"),
                resultSet.getString("ENC_CIUDAD_SEDE"),
                resultSet.getString("ENC_DIR_SEDE"),
                resultSet.getString("ENC_TELEF_SEDE"),
                resultSet.getString("ENC_EMAIL_SEDE_EMISION"),
                resultSet.getLong("ENC_CLINATU"),
                resultSet.getString("ENC_CLINOMB"),
                resultSet.getString("ENC_CLI_TIPOID"),
                resultSet.getString("ENC_CLI_NUMID"),
                resultSet.getString("ENC_CLICIUDAD"),
                resultSet.getString("ENC_CLIDIR"),
                resultSet.getString("ENC_CLITELEF"),
                resultSet.getString("ENC_CLIEMAIL"),
                resultSet.getString("ENC_CLIRESPFIS"),
                resultSet.getLong("ENC_CLIRESPIVA"),
                resultSet.getString("ENC_DOCU_HIJO")
        );
    }
}
