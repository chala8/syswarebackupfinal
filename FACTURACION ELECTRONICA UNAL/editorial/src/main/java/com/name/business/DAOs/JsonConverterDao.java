package com.name.business.DAOs;
import com.name.business.entities.Detail;
import com.name.business.entities.Master;
import com.name.business.mappers.DetailMapper;
import com.name.business.mappers.MasterMapper;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

//import com.name.business.entities.*;
//import com.name.business.mappers.*;

//@RegisterMapper(CloseMapper.class)
public interface JsonConverterDao {





    @RegisterMapper(MasterMapper.class)
    @SqlQuery("use geslib\n" +
            "SELECT  \n" +
            "distinct\n" +
            "'EDI' as ENC_PREFIJO,\n" +
            "(case glmcalbcli.tipdoc when 'FN' then glmmfaccli.numdoc when 'FB' then right (glmmfaccli.rectifica_obs,4) when 'FR' then glmmfaccli.numdoc when 'FA' then glmmfaccli.numdoc else glmmfaccli.numdoc end) as ENC_NRORECIBO,\n" +
            "glmmfaccli.centro as ENC_SEDE,\n" +
            "year (glmmfaccli.fecha) as ENC_VIGENCIA,\n" +
            "'1004' as ENC_NITEMES,\n" +
            "'NIVEL NACIONAL' as ENC_NOMBRESEDE,\n" +
            "'EDITORIAL UNIVERSIDAD NACIONAL' as ENC_EMPRESAQUIPU,\n" +
            "convert(date,glmmfaccli.fecha) as ENC_FECEMIC,\n" +
            "(case glmcalbcli.tipdoc when 'FN' then '1' when 'FB' then '91' when 'FR' then '1'when 'FA' then '91'else '92'end) as ENC_TIPODOC,\n" +
            "(case glmcalbcli.tipdoc when 'FN' then '1' when 'FB' then '1' when 'FR' then '2' when 'FA' then '2' else '1' end) as ENC_FORMAPAGO,   \n" +
            "convert (date,glmcalbcli.fecha_cobro) as ENC_FECPAGO,\n" +
            "MAX (case glmlcobcaj.forma_cobro when 'EF' then '10' when 'TJ' then '48' when 'TD' then '49' when 'TL' then '71' else '10' end ) as ENC_MEDIOPAGO,\n" +
            "ABS (format ((select isnull(sum(glmlalbcli.pvp * glmlalbcli.cantidad * glmmtipdoc.signo_ventas),0)     from  glmlalbcli, glmmtipdoc, glmcalbcli B where  B.albaran   = glmcalbcli.albaran and  glmlalbcli.albaran= B.albaran and  glmmtipdoc.tipdoc = B.tipdoc ) - glmcalbcli.importe,'#0.')) as ENC_DESCUENTO,\n" +
            "ABS (format ((select isnull(sum(glmlalbcli.pvp * glmlalbcli.cantidad * glmmtipdoc.signo_ventas),0)     from  glmlalbcli, glmmtipdoc, glmcalbcli B  where  B.albaran   = glmcalbcli.albaran  and  glmlalbcli.albaran= B.albaran and  glmmtipdoc.tipdoc = B.tipdoc ),'#0.')) as ENC_SUBTOTAL,\n" +
            "ABS (format(glmcalbcli.importe, '#0.')) as ENC_TOTAL,\n" +
            "(select distinct  v_lst_faccli.observa_albaran from v_lst_faccli where v_lst_faccli.albaran = glmcalbcli.albaran  ) as ENC_OBSERVACIONES,\n" +
            "glmmfaccli.certi_autorizacion  as ENC_DATOS_PAGO,\n" +
            "(case centros.centro  when '10' then '11001' when '11' then '11001' when '12' then '11001'when '13' then '11001' when '14' then '05001' else '11001'end) as ENC_CIUDAD_SEDE,\n" +
            "(case centros.centro  when '10' then 'Kr 7 Nº 19-73' when '11' then 'Kr 7 Nº 19-73' when '12' then 'Kr 7 Nº 19-73'when '13' then 'Kr 7 Nº 19-73' when '14' then 'FALTA' else 'Kr 7 Nº 19-73'end) as ENC_DIR_SEDE,\n" +
            "'3165000' as ENC_TELEF_SEDE,\n" +
            "'libreriaun_bog@unal.edu.co' as ENC_EMAIL_SEDE_EMISION,\n" +
            "(CASE glmmclient.agente when 2 THEN 2 else 1 end) as ENC_CLINATU,\n" +
            "glmmclient.nombre    as ENC_CLINOMB,\n" +
            "(CASE glmmclient.ruta when '1' THEN '31' when '2' THEN '13' when '3' THEN '41' when '4' THEN '12' else '31' end)    as ENC_CLI_TIPOID,\n" +
            "glmmclient.cif  as ENC_CLI_NUMID,\n" +
            "(CASE glmmclient.centro_creacion when '10' then '11001' when '11' then '11001' when '12' then '11001'when '13' then '11001' when '14' then '05001' else '11001'end) as ENC_CLICIUDAD,\n" +
            "glmmclient.direccion    as ENC_CLIDIR,\n" +
            "glmmclient.telefono1   as ENC_CLITELEF,\n" +
            "glmmclient.email   as ENC_CLIEMAIL,\n" +
            "glmmclient.libre_1 as ENC_CLIRESPFIS,\n" +
            "(case glmmclient.libre_2 when '48' then '48' when null then '49' else '49' end)  as ENC_CLIRESPIVA,\n" +
            "right (glmmfaccli.rectifica_obs,4)  as ENC_DOCU_HIJO\n" +
            "FROM    \n" +
            "glmcalbcli,    \n" +
            "glmmclient,\n" +
            "glmlivfacl,\n" +
            "glmlalbcli,\n" +
            "glmmfaccli,\n" +
            "v_lst_faccli,\n" +
            "glmlcobcaj,\n" +
            "glmlmovcaj,\n" +
            "centros\n" +
            "WHERE glmcalbcli.factura = glmlivfacl.factura\n" +
            "and  glmcalbcli.fecha between :date1 and :date2\n" +
            "and   glmcalbcli.tipdoc  like  'F%'  \n" +
            "and  glmmclient.cliente  = glmcalbcli.cliente\n" +
            "and glmlalbcli.albaran =  glmcalbcli.albaran\n" +
            "and glmmfaccli.factura =  glmlivfacl.factura\n" +
            "and glmmfaccli.factura =  glmlivfacl.factura\n" +
            "and glmlcobcaj.num_apunte = glmlmovcaj.num_apunte\n" +
            "and glmcalbcli.albaran = glmlmovcaj.albaran\n" +
            "and glmcalbcli.centro = centros.centro\n" +
            "GROUP BY glmmfaccli.numdoc ,glmmfaccli.centro , glmmfaccli.fecha , glmcalbcli.tipdoc ,glmcalbcli.fecha_cobro, glmcalbcli.albaran,glmcalbcli.importe ,glmmfaccli.certi_autorizacion ,centros.centro , glmmclient.agente , glmmclient.nombre\n" +
            ",glmmclient.ruta , glmmclient.cif , glmmclient.centro_creacion , glmmclient.direccion ,glmmclient.telefono1 , glmmclient.email ,glmmclient.libre_1 ,glmmclient.libre_2 ,glmmfaccli.rectifica_obs\n" +
            " HAVING COUNT(*) > 1;")
    List<Master> master(@Bind("date1") String fecha1,@Bind("date2") String fecha2);



    @RegisterMapper(DetailMapper.class)
    @SqlQuery("use geslib\n" +
            "SELECT distinct\n" +
            "'EDI'AS DET_PREFIJO,\n" +
            "(case glmcalbcli.tipdoc when 'FN' then glmmfaccli.numdoc when 'FB' then right (glmmfaccli.rectifica_obs,4) when 'FR' then glmmfaccli.numdoc when 'FA' then glmmfaccli.numdoc else glmmfaccli.numdoc end) AS DET_NRORECIBO,\n" +
            "glmmfaccli.centro AS DET_SEDE,\n" +
            "'1004' AS DET_NITEMES,\n" +
            "glmlalbcli.cantidad as DET_CANTIDA,\n" +
            "format (glmlalbcli.pvp , '#0.') AS DET_VLRUNIT,\n" +
            "glmlalbcli.descuento as DET_PCT_DESC, \n" +
            "trevenque.art_get_articulo (glmlalbcli.articulo, 'NORMAL') as DET_CODIGO,  \n" +
            "glmlalbcli.descripcion as DET_NOMBRE,\n" +
            "right (glmmfaccli.rectifica_obs,4)  as FACTURA_RELACIONADA\n" +
            "FROM glmcalbcli,      glmlalbcli,     glmmtipdoc,     v_seg_centros , glmmfaccli\n" +
            "WHERE   glmcalbcli.albaran  = glmlalbcli.albaran \n" +
            "and  glmmtipdoc.tipdoc   = glmcalbcli.tipdoc \n" +
            "and glmcalbcli.factura = glmmfaccli.factura\n" +
            "and  v_seg_centros.centro  = glmcalbcli.centro \n" +
            "and  glmcalbcli.fecha between  :date1 and :date2 \n" +
            "and   glmcalbcli.tipdoc  in ('FN', 'FS', 'FB') ")
    List<Detail> detail(@Bind("date1") String date1,@Bind("date2") String date2);

}
