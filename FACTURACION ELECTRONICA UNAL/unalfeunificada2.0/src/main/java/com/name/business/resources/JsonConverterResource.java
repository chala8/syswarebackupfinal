package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.JsonConverterBusiness;
//import com.name.business.entities.*;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Date;


@Path("/unalfe")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JsonConverterResource {
    private JsonConverterBusiness jsonConverterBusiness;

    public JsonConverterResource(JsonConverterBusiness jsonConverterBusiness) {
        this.jsonConverterBusiness = jsonConverterBusiness;
    }


    @Path("/master")
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response masterProcedure(@QueryParam("in_prefijo") String in_prefijo,
                                    @QueryParam("in_nrorecibo")String in_nrorecibo,
                                    @QueryParam("in_sede")Long in_sede,
                                    @QueryParam("in_vigencia")Long in_vigencia,
                                    @QueryParam("in_ciaquipu")String in_ciaquipu,
                                    @QueryParam("in_nombresede")String in_nombresede,
                                    @QueryParam("in_empresaquipu")String in_empresaquipu,
                                    @QueryParam("in_fecemic")String in_fecemic,
                                    @QueryParam("in_tipodoc")Long in_tipodoc,
                                    @QueryParam("in_formapago")Long in_formapago,
                                    @QueryParam("in_fecpago")String in_fecpago,
                                    @QueryParam("in_mediopago")Long in_mediopago,
                                    @QueryParam("in_descuento")Long in_descuento,
                                    @QueryParam("in_subtotal")Double in_subtotal,
                                    @QueryParam("in_total")Double in_total,
                                    @QueryParam("in_observaciones")String in_observaciones,
                                    @QueryParam("in_datos_pago")String in_datos_pago,
                                    @QueryParam("in_ciudad_sede")String in_ciudad_sede,
                                    @QueryParam("in_dir_sede")String in_dir_sede,
                                    @QueryParam("in_telef_sede")String in_telef_sede,
                                    @QueryParam("in_email_sede_emision")String in_email_sede_emision,
                                    @QueryParam("in_clinatu")Long in_clinatu,
                                    @QueryParam("in_clinomb")String in_clinomb,
                                    @QueryParam("in_clitipoid")String in_clitipoid,
                                    @QueryParam("in_clinumid")String in_clinumid,
                                    @QueryParam("in_cliciudad")String in_cliciudad,
                                    @QueryParam("in_clidir")String in_clidir,
                                    @QueryParam("in_clitelelef")String in_clitelelef,
                                    @QueryParam("in_cliemail")String in_cliemail,
                                    @QueryParam("in_clirespfiscal")String in_clirespfiscal,
                                    @QueryParam("in_clirespiva")Long in_clirespiva,
                                    @QueryParam("in_origen")Long in_origen,
                                    @QueryParam("in_forma_datos_origen")Long in_forma_datos_origen,
                                    @QueryParam("in_num_docuhijo")String in_num_docuhijo,
                                    @QueryParam("sistema")String sistema
                                    ){

        Response response;
        Long getResponseGeneric = jsonConverterBusiness.masterProcedure(in_prefijo,
                in_nrorecibo,
                in_sede,
                in_vigencia,
                in_ciaquipu,
                in_nombresede,
                in_empresaquipu,
                in_fecemic,
                in_tipodoc,
                in_formapago,
                in_fecpago,
                in_mediopago,
                in_descuento,
                in_subtotal,
                in_total,
                in_observaciones,
                in_datos_pago,
                in_ciudad_sede,
                in_dir_sede,
                in_telef_sede,
                in_email_sede_emision,
                in_clinatu,
                in_clinomb,
                in_clitipoid,
                in_clinumid,
                in_cliciudad,
                in_clidir,
                in_clitelelef,
                in_cliemail,
                in_clirespfiscal,
                in_clirespiva,
                in_origen,
                in_forma_datos_origen,
                in_num_docuhijo,
                sistema);


        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();


        return response;
    }

    @Path("/detail")
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response detailProcedure(@QueryParam("in_prefijo") String in_prefijo,
                                    @QueryParam("in_nrorecibo") String in_nrorecibo,
                                    @QueryParam("in_sede") Long in_sede,
                                    @QueryParam("in_vigencia") Long in_vigencia,
                                    @QueryParam("in_ciaquipu") String in_ciaquipu,
                                    @QueryParam("in_cantidad") String in_cantidad,
                                    @QueryParam("in_vlrunit") String in_vlrunit,
                                    @QueryParam("in_pct_desc") String in_pct_desc,
                                    @QueryParam("in_codigo") String in_codigo,
                                    @QueryParam("in_nombre") String in_nombre,
                                    @QueryParam("in_descrip") String in_descrip,
                                    @QueryParam("in_num_docuhijo") String in_num_docuhijo){

        Response response;

        System.out.println("------------------STARTING DETAILS-------------------");

        Long getResponseGeneric = jsonConverterBusiness.detailProcedure(in_prefijo,
                in_nrorecibo,
                in_sede,
                in_vigencia,
                in_ciaquipu,
                in_cantidad,
                in_vlrunit,
                in_pct_desc,
                in_codigo,
                in_nombre,
                in_descrip,
                in_num_docuhijo);


        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;
    }
}