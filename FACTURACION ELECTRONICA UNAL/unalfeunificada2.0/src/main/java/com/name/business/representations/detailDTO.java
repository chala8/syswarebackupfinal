package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class detailDTO {


        private String in_nombre;
        private String in_descrip;

        @JsonCreator
        public detailDTO(@JsonProperty("in_nombre") String in_nombre,
                         @JsonProperty("in_descrip") String in_descrip) {
            this.in_nombre = in_nombre;
            this.in_descrip = in_descrip;
        }


        public String getIn_nombre() {
            return in_nombre;
        }

        public void setIn_nombre(String in_nombre) {
            this.in_nombre = in_nombre;
        }

        public String getIn_descrip() {
            return in_descrip;
        }

        public void setIn_descrip(String in_descrip) {
            this.in_descrip = in_descrip;
        }


}


