package com.name.business.DAOs;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

//import com.name.business.entities.*;
//import com.name.business.mappers.*;

//@RegisterMapper(CloseMapper.class)
public interface JsonConverterDao {



    @SqlCall("begin poblar_tmpmaestro(:in_prefijo,:in_nrorecibo,:in_sede,:in_vigencia,:in_ciaquipu,:in_nombresede,:in_empresaquipu,:in_fecemic,:in_tipodoc,:in_formapago,:in_fecpago,:in_mediopago," +
            ":in_descuento,:in_subtotal,:in_total,:in_observaciones,:in_datos_pago,:in_ciudad_sede,:in_dir_sede,:in_telef_sede,:in_email_sede_emision,:in_clinatu,:in_clinomb," +
            ":in_clitipoid,:in_clinumid,:in_cliciudad,:in_clidir,:in_clitelelef,:in_cliemail,:in_clirespfiscal,:in_clirespiva,:in_origen,:in_forma_datos_origen, :in_num_docuhijo, :sistema); end; ")
    void poblar_tmpmaestro(@Bind("in_prefijo") String in_prefijo,
                    @Bind("in_nrorecibo")String in_nrorecibo,
                    @Bind("in_sede")Long in_sede,
                    @Bind("in_vigencia")Long in_vigencia,
                    @Bind("in_ciaquipu")String in_ciaquipu,
                    @Bind("in_nombresede")String in_nombresede,
                    @Bind("in_empresaquipu")String in_empresaquipu,
                    @Bind("in_fecemic")Date in_fecemic,
                    @Bind("in_tipodoc")Long in_tipodoc,
                    @Bind("in_formapago")Long in_formapago,
                    @Bind("in_fecpago")Date in_fecpago,
                    @Bind("in_mediopago")Long in_mediopago,
                    @Bind("in_descuento")Long in_descuento,
                    @Bind("in_subtotal")Double in_subtotal,
                    @Bind("in_total")Double in_total,
                    @Bind("in_observaciones")String in_observaciones,
                    @Bind("in_datos_pago")String in_datos_pago,
                    @Bind("in_ciudad_sede")String in_ciudad_sede,
                    @Bind("in_dir_sede")String in_dir_sede,
                    @Bind("in_telef_sede")String in_telef_sede,
                    @Bind("in_email_sede_emision")String in_email_sede_emision,
                    @Bind("in_clinatu")Long in_clinatu,
                    @Bind("in_clinomb")String in_clinomb,
                    @Bind("in_clitipoid")String in_clitipoid,
                    @Bind("in_clinumid")String in_clinumid,
                    @Bind("in_cliciudad")String in_cliciudad,
                    @Bind("in_clidir")String in_clidir,
                    @Bind("in_clitelelef")String in_clitelelef,
                    @Bind("in_cliemail")String in_cliemail,
                    @Bind("in_clirespfiscal")String in_clirespfiscal,
                    @Bind("in_clirespiva")Long in_clirespiva,
                    @Bind("in_origen")Long in_origen,
                    @Bind("in_forma_datos_origen")Long in_forma_datos_origen,
                    @Bind("in_num_docuhijo")String in_num_docuhijo,
                    @Bind("sistema")String sistema);


    @SqlCall(" call poblar_tmpdetalles\n" +
            "(\n" +
            "    :in_prefijo,\n" +
            "    :in_nrorecibo,\n" +
            "    :in_sede,\n" +
            "    :in_vigencia,\n" +
            "    :in_ciaquipu,\n" +
            "    :in_cantidad,\n" +
            "    :in_vlrunit,\n" +
            "    :in_pct_desc,\n" +
            "    :in_codigo,\n" +
            "    :in_nombre,\n" +
            "    :in_descrip,\n" +
            "    :in_num_docuhijo\n" +
            ") ")
    void poblar_tmpdetalles(@Bind("in_prefijo") String in_prefijo,
                            @Bind("in_nrorecibo") String in_nrorecibo,
                            @Bind("in_sede") Long in_sede,
                            @Bind("in_vigencia") Long in_vigencia,
                            @Bind("in_ciaquipu") String in_ciaquipu,
                            @Bind("in_cantidad") String in_cantidad,
                            @Bind("in_vlrunit") String in_vlrunit,
                            @Bind("in_pct_desc") String in_pct_desc,
                            @Bind("in_codigo") String in_codigo,
                            @Bind("in_nombre") String in_nombre,
                            @Bind("in_descrip") String in_descrip,
                            @Bind("in_num_docuhijo") String in_num_docuhijo
                            );


}
