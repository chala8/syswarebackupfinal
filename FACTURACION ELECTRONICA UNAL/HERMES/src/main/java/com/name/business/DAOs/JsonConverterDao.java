package com.name.business.DAOs;
import com.name.business.entities.*;
import com.name.business.mappers.*;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

//import com.name.business.entities.*;
//import com.name.business.mappers.*;

//@RegisterMapper(CloseMapper.class)
public interface JsonConverterDao {

    @RegisterMapper(MasterHermesMapper.class)
    @SqlQuery("SELECT \n" +
            "ENC_PREFIJO,\n" +
            "ENC_NRORECIBO,\n" +
            "ENC_SEDE,\n" +
            "ENC_VIGENCIA,\n" +
            "ENC_NITEMES,\n" +
            "ENC_NOMBRESEDE,\n" +
            "ENC_EMPRESAQUIPU,\n" +
            "ENC_FECEMIC,\n" +
            "ENC_TIPODOC,\n" +
            "ENC_FORMAPAGO,\n" +
            "ENC_FECPAGO,\n" +
            "ENC_MEDIOPAGO,\n" +
            "ENC_DESCUENTO,\n" +
            "ENC_SUBTOTAL,\n" +
            "ENC_TOTAL,\n" +
            "ENC_OBSERVACIONES,\n" +
            "ENC_DATOS_PAGO,\n" +
            "ENC_CIUDAD_SEDE,\n" +
            "ENC_DIR_SEDE,\n" +
            "ENC_TELEF_SEDE,\n" +
            "ENC_EMAIL_SEDE_EMISION,\n" +
            "ENC_CLINATU,\n" +
            "ENC_CLINOMB,\n" +
            "ENC_CLI_TIPOID,\n" +
            "ENC_CLI_NUMID,\n" +
            "ENC_CLICIUDAD,\n" +
            "ENC_CLIDIR,\n" +
            "ENC_CLITELEF,\n" +
            "ENC_CLIEMAIL,\n" +
            "ENC_CLIRESPFIS,\n" +
            "ENC_CLIRESPIVA, \n " +
            "ENC_NUM_DOCUHIJO  " +
            "FROM snw_hermes.fe_maestro\n" +
            "WHERE ENC_fecemic=:fecha")
    List<MasterHermes> getMasterHermes(@Bind("fecha") String fecha);




    @RegisterMapper(DetailHermesMapper.class)
    @SqlQuery(" SELECT \n" +
            "DET_PREFIJO,\n" +
            "DET_NRORECIBO,\n" +
            "DET_SEDE,\n" +
            "DET_NITEMES,\n" +
            "DET_CANTIDA,\n" +
            "DET_VLRUNIT,\n" +
            "DET_PCT_DESC,\n" +
            "DET_CODIGO,\n" +
            "DET_NOMBRE,\n" +
            "DET_DESCRIP, \n" +
            "DET_NUM_DOCUHIJO  " +
            "FROM snw_hermes.fe_DETALLE\n" +
            "WHERE DET_PREFIJO=:PREFIJO AND DET_NRORECIBO=:NRORECIBO AND DET_SEDE=:SEDE\n" +
            "AND DET_NITEMES=:CIAQUIPU AND DET_NUM_DOCUHIJO IS NULL")
    List<DetailHermes> getDetailsHermes(@Bind("PREFIJO") String PREFIJO,
                                      @Bind("NRORECIBO") String NRORECIBO,
                                      @Bind("SEDE") Long SEDE,
                                      @Bind("CIAQUIPU") String CIAQUIPU);


    @RegisterMapper(DetailHermesMapper.class)
    @SqlQuery(" SELECT \n" +
            "DET_PREFIJO,\n" +
            "DET_NRORECIBO,\n" +
            "DET_SEDE,\n" +
            "DET_NITEMES,\n" +
            "DET_CANTIDA,\n" +
            "DET_VLRUNIT,\n" +
            "DET_PCT_DESC,\n" +
            "DET_CODIGO,\n" +
            "DET_NOMBRE,\n" +
            "DET_DESCRIP, \n" +
            "DET_NUM_DOCUHIJO  " +
            "FROM snw_hermes.fe_DETALLE\n" +
            "WHERE DET_PREFIJO=:PREFIJO AND DET_NRORECIBO=:NRORECIBO AND DET_SEDE=:SEDE\n" +
            "AND DET_NITEMES=:CIAQUIPU AND DET_NUM_DOCUHIJO=:NUMDOCUHIJO")
    List<DetailHermes> getDetailsHermesCred(@Bind("PREFIJO") String PREFIJO,
                                            @Bind("NRORECIBO") String NRORECIBO,
                                            @Bind("SEDE") Long SEDE,
                                            @Bind("CIAQUIPU") String CIAQUIPU,
                                            @Bind("NUMDOCUHIJO") String NUMDOCUHIJO);



}
