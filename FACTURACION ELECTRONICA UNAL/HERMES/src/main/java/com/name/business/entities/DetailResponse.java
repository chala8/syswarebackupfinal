package com.name.business.entities;

public class DetailResponse {

    private String in_prefijo;
    private String in_nrorecibo;
    private String in_sede;
    private String in_vigencia;
    private String in_ciaquipu;
    private String in_cantidad;
    private String in_vlrunit;
    private String in_pct_desc;
    private String in_codigo;
    private String in_nombre;
    private String in_descrip;
    private String in_num_docuhijo;

    public DetailResponse(String in_prefijo, String in_nrorecibo, String in_sede, String in_vigencia, String in_ciaquipu, String in_cantidad, String in_vlrunit, String in_pct_desc, String in_codigo, String in_nombre, String in_descrip, String in_num_docuhijo) {
        this.in_prefijo = in_prefijo;
        this.in_nrorecibo = in_nrorecibo;
        this.in_sede = in_sede;
        this.in_vigencia = in_vigencia;
        this.in_ciaquipu = in_ciaquipu;
        this.in_cantidad = in_cantidad;
        this.in_vlrunit = in_vlrunit;
        this.in_pct_desc = in_pct_desc;
        this.in_codigo = in_codigo;
        this.in_nombre = in_nombre;
        this.in_descrip = in_descrip;
        this.in_num_docuhijo = in_num_docuhijo;
    }

    public String getIn_prefijo() {
        return in_prefijo;
    }

    public void setIn_prefijo(String in_prefijo) {
        this.in_prefijo = in_prefijo;
    }

    public String getIn_nrorecibo() {
        return in_nrorecibo;
    }

    public void setIn_nrorecibo(String in_nrorecibo) {
        this.in_nrorecibo = in_nrorecibo;
    }

    public String getIn_sede() {
        return in_sede;
    }

    public void setIn_sede(String in_sede) {
        this.in_sede = in_sede;
    }

    public String getIn_vigencia() {
        return in_vigencia;
    }

    public void setIn_vigencia(String in_vigencia) {
        this.in_vigencia = in_vigencia;
    }

    public String getIn_ciaquipu() {
        return in_ciaquipu;
    }

    public void setIn_ciaquipu(String in_ciaquipu) {
        this.in_ciaquipu = in_ciaquipu;
    }

    public String getIn_cantidad() {
        return in_cantidad;
    }

    public void setIn_cantidad(String in_cantidad) {
        this.in_cantidad = in_cantidad;
    }

    public String getIn_vlrunit() {
        return in_vlrunit;
    }

    public void setIn_vlrunit(String in_vlrunit) {
        this.in_vlrunit = in_vlrunit;
    }

    public String getIn_pct_desc() {
        return in_pct_desc;
    }

    public void setIn_pct_desc(String in_pct_desc) {
        this.in_pct_desc = in_pct_desc;
    }

    public String getIn_codigo() {
        return in_codigo;
    }

    public void setIn_codigo(String in_codigo) {
        this.in_codigo = in_codigo;
    }

    public String getIn_nombre() {
        return in_nombre;
    }

    public void setIn_nombre(String in_nombre) {
        this.in_nombre = in_nombre;
    }

    public String getIn_descrip() {
        return in_descrip;
    }

    public void setIn_descrip(String in_descrip) {
        this.in_descrip = in_descrip;
    }

    public String getIn_num_docuhijo() {
        return in_num_docuhijo;
    }

    public void setIn_num_docuhijo(String in_num_docuhijo) {
        this.in_num_docuhijo = in_num_docuhijo;
    }
}
