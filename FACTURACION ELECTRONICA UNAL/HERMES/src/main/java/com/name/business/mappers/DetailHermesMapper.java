package com.name.business.mappers;

import com.name.business.entities.DetailHermes;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailHermesMapper implements ResultSetMapper<DetailHermes> {

    @Override
    public DetailHermes map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DetailHermes(
                resultSet.getString("DET_PREFIJO"),
                resultSet.getString("DET_NRORECIBO"),
                resultSet.getLong("DET_SEDE"),
                resultSet.getLong("DET_NITEMES"),
                resultSet.getDouble("DET_CANTIDA"),
                resultSet.getDouble("DET_VLRUNIT"),
                resultSet.getLong("DET_PCT_DESC"),
                resultSet.getString("DET_CODIGO"),
                resultSet.getString("DET_NOMBRE"),
                resultSet.getString("DET_DESCRIP"),
                resultSet.getString("DET_NUM_DOCUHIJO")
        );
    }
}
