package com.name.business.entities;

public class BillingDianInfo {

    private Long ENC_CONSDIAN;
    private String ENC_FECEMIC;

    public BillingDianInfo(Long ENC_CONSDIAN, String ENC_FECEMIC) {
        this.ENC_CONSDIAN = ENC_CONSDIAN;
        this.ENC_FECEMIC = ENC_FECEMIC;
    }

    public Long getENC_CONSDIAN() {
        return ENC_CONSDIAN;
    }

    public void setENC_CONSDIAN(Long ENC_CONSDIAN) {
        this.ENC_CONSDIAN = ENC_CONSDIAN;
    }

    public String getENC_FECEMIC() {
        return ENC_FECEMIC;
    }

    public void setENC_FECEMIC(String ENC_FECEMIC) {
        this.ENC_FECEMIC = ENC_FECEMIC;
    }
}
