package com.name.business.entities;

public class DetailHermes {


    private String DET_PREFIJO;
    private String DET_NRORECIBO;
    private Long DET_SEDE;
    private Long DET_CIAQUIPU;
    private Double DET_CANTIDA;
    private Double DET_VLRUNIT;
    private Long DET_PCT_DESC;
    private String DET_CODIGO;
    private String DET_NOMBRE;
    private String DET_DESCRIP;
    private String DET_NUM_DOCUHIJO;

    public DetailHermes(String DET_PREFIJO, String DET_NRORECIBO, Long DET_SEDE, Long DET_CIAQUIPU, Double DET_CANTIDA, Double DET_VLRUNIT, Long DET_PCT_DESC, String DET_CODIGO, String DET_NOMBRE, String DET_DESCRIP, String DET_NUM_DOCUHIJO) {
        this.DET_PREFIJO = DET_PREFIJO;
        this.DET_NRORECIBO = DET_NRORECIBO;
        this.DET_SEDE = DET_SEDE;
        this.DET_CIAQUIPU = DET_CIAQUIPU;
        this.DET_CANTIDA = DET_CANTIDA;
        this.DET_VLRUNIT = DET_VLRUNIT;
        this.DET_PCT_DESC = DET_PCT_DESC;
        this.DET_CODIGO = DET_CODIGO;
        this.DET_NOMBRE = DET_NOMBRE;
        this.DET_DESCRIP = DET_DESCRIP;
        this.DET_NUM_DOCUHIJO = DET_NUM_DOCUHIJO;
    }

    public String getDET_PREFIJO() {
        return DET_PREFIJO;
    }

    public void setDET_PREFIJO(String DET_PREFIJO) {
        this.DET_PREFIJO = DET_PREFIJO;
    }

    public String getDET_NRORECIBO() {
        return DET_NRORECIBO;
    }

    public void setDET_NRORECIBO(String DET_NRORECIBO) {
        this.DET_NRORECIBO = DET_NRORECIBO;
    }

    public Long getDET_SEDE() {
        return DET_SEDE;
    }

    public void setDET_SEDE(Long DET_SEDE) {
        this.DET_SEDE = DET_SEDE;
    }

    public Long getDET_CIAQUIPU() {
        return DET_CIAQUIPU;
    }

    public void setDET_CIAQUIPU(Long DET_CIAQUIPU) {
        this.DET_CIAQUIPU = DET_CIAQUIPU;
    }

    public Double getDET_CANTIDA() {
        return DET_CANTIDA;
    }

    public void setDET_CANTIDA(Double DET_CANTIDA) {
        this.DET_CANTIDA = DET_CANTIDA;
    }

    public Double getDET_VLRUNIT() {
        return DET_VLRUNIT;
    }

    public void setDET_VLRUNIT(Double DET_VLRUNIT) {
        this.DET_VLRUNIT = DET_VLRUNIT;
    }

    public Long getDET_PCT_DESC() {
        return DET_PCT_DESC;
    }

    public void setDET_PCT_DESC(Long DET_PCT_DESC) {
        this.DET_PCT_DESC = DET_PCT_DESC;
    }

    public String getDET_CODIGO() {
        return DET_CODIGO;
    }

    public void setDET_CODIGO(String DET_CODIGO) {
        this.DET_CODIGO = DET_CODIGO;
    }

    public String getDET_NOMBRE() {
        return DET_NOMBRE;
    }

    public void setDET_NOMBRE(String DET_NOMBRE) {
        this.DET_NOMBRE = DET_NOMBRE;
    }

    public String getDET_DESCRIP() {
        return DET_DESCRIP;
    }

    public void setDET_DESCRIP(String DET_DESCRIP) {
        this.DET_DESCRIP = DET_DESCRIP;
    }

    public String getDET_NUM_DOCUHIJO() {
        return DET_NUM_DOCUHIJO;
    }

    public void setDET_NUM_DOCUHIJO(String DET_NUM_DOCUHIJO) {
        this.DET_NUM_DOCUHIJO = DET_NUM_DOCUHIJO;
    }
}
