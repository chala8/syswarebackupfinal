package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.JsonConverterBusiness;
//import com.name.business.entities.*;
import com.name.business.entities.DetailHermes;
import com.name.business.entities.MasterHermes;
import com.name.business.entities.MasterResponse;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/invoice")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JsonConverterResource {
    private JsonConverterBusiness jsonConverterBusiness;

    public JsonConverterResource(JsonConverterBusiness jsonConverterBusiness) {
        this.jsonConverterBusiness = jsonConverterBusiness;
    }

    @Path("/masterHermes")
    @GET
    @Timed
    @RolesAllowed({"Auth"})

    public Response masterProcedure(@QueryParam("fecha")String fecha){

        Response response;

        List<MasterHermes> getResponseGeneric = jsonConverterBusiness.MasterHermes(fecha);


        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();


        return response;
    }


    @Path("/detailHermes")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response detail(
            @QueryParam("PREFIJO") String PREFIJO,
            @QueryParam("NRORECIBO") String NRORECIBO,
            @QueryParam("SEDE") Long SEDE,
            @QueryParam("CIAQUIPU") String CIAQUIPU,
            @QueryParam("NUMDOCUHIJO") String NUMDOCUHIJO
    ){

        Response response;

        List<DetailHermes> getResponseGeneric = jsonConverterBusiness.detailHermes(PREFIJO,
                NRORECIBO,
                SEDE,
                CIAQUIPU,
                NUMDOCUHIJO);


        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();


        return response;
    }


    @Path("/hermes")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response hermes(@QueryParam("fecha")String fecha){

        Response response;


        List<MasterResponse> getResponseGeneric = jsonConverterBusiness.hermes(fecha);


        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();


        return response;
    }


}