package com.name.business.businesses;
import com.name.business.DAOs.JsonConverterDao;
import com.name.business.entities.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class JsonConverterBusiness {

    private JsonConverterDao billDAO;
    //VARIABLES PARA CONEXIÓN CORREO
    static final String FROM = "soporte@tienda724.com";
    static final String FROMNAME = "Soporte Tienda 724";
    static final String SMTP_USERNAME = "soporte@tienda724.com";
    static final String SMTP_PASSWORD = "Lx3BPR5j8pdt2xmm";
    static final String HOST = "smtp.mail.us-east-1.awsapps.com";
    static final int PORT = 465;
    private static DecimalFormat df2 = new DecimalFormat("#.##");
    ///////////////////////////////////

    public JsonConverterBusiness(JsonConverterDao billDAO) {
        this.billDAO = billDAO;
    }



    public List<MasterHermes> MasterHermes(String fecha){
        return billDAO.getMasterHermes( fecha );
    }

    public List<DetailHermes> detailHermes(String PREFIJO,
                                           String NRORECIBO,
                                           Long SEDE,
                                           String CIAQUIPU,
                                           String NUMDOCUHIJO){
        return billDAO.getDetailsHermes( PREFIJO,
                NRORECIBO,
                SEDE,
                CIAQUIPU);
    }


    public List<MasterResponse> hermes(String fecha){

        List<MasterResponse> response = new ArrayList<MasterResponse>();

        List<MasterHermes> listMasterQuipu = MasterHermes(fecha);

        for(int i = 0; i<listMasterQuipu.size();i++){

            MasterHermes tmp = listMasterQuipu.get(i);
            ArrayList<String> ar = new ArrayList<String>();

            ar.add(tmp.getENC_PREFIJO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            ar.add(tmp.getENC_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_VIGENCIA().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_NITEMES());
            ar.add(tmp.getENC_NOMBRESEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_EMPRESAQUIPU().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_FECEMIC().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_TIPODOC().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_FORMAPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_FECPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            ar.add(tmp.getENC_MEDIOPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_DESCUENTO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_SUBTOTAL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_TOTAL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_OBSERVACIONES().toString().replaceAll("\t","%20").replaceAll("#","No").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_DATOS_PAGO().toString().replaceAll("\t","%20").replaceAll("#","No").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            ar.add(tmp.getENC_CIUDAD_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_DIR_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_TELEF_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_EMAIL_SEDE_EMISION().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            ar.add(tmp.getENC_CLINATU().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_CLINOMB().toString().replaceAll("&","_").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_CLI_TIPOID().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_CLI_NUMID().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }try{
                ar.add(tmp.getENC_CLICIUDAD().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }  try{
                ar.add(tmp.getENC_CLIDIR().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_CLITELEF().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_CLIEMAIL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_CLIRESPFIS().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_CLIRESPIVA().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                if(tmp.getENC_TIPODOC() == new Long(1)){
                    ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                }else{
                    ar.add(tmp.getENC_NUM_DOCUHIJO().toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                }
            }catch(NullPointerException e){
                ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }

            String numDocuHijo = ar.get(1);
            try{

                if(new Integer(ar.get(8)) == 1){
                    numDocuHijo = ar.get(1);
                }else{
                    numDocuHijo = ar.get(31);
                }
            }catch(Exception e){
                e.printStackTrace();
                System.out.println("ELEMENT: "+ar.toString());
            }

            MasterResponse newMaster = new MasterResponse(ar.get(0).replaceAll(" ","%20"),
                    ar.get(1).replaceAll(" ","%20"),
                    ar.get(2).replaceAll(" ","%20"),
                    ar.get(3).replaceAll(" ","%20"),
                    ar.get(4).replaceAll(" ","%20"),
                    ar.get(5).replaceAll(" ","%20"),
                    ar.get(6).replaceAll(" ","%20"),
                    ar.get(7).replaceAll(" ","%20"),
                    ar.get(8).replaceAll(" ","%20"),
                    ar.get(9).replaceAll(" ","%20"),
                    ar.get(10).replaceAll(" ","%20"),
                    ar.get(11).replaceAll(" ","%20"),
                    ar.get(12).replaceAll(" ","%20"),
                    ar.get(13).replaceAll(" ","%20"),
                    ar.get(14).replaceAll(" ","%20"),
                    ar.get(15).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No").replaceAll("\n","%20"),
                    ar.get(16).replaceAll(" ","%20"),
                    ar.get(17).replaceAll(" ","%20"),
                    ar.get(18).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No").replaceAll("\n","%20"),
                    ar.get(19).replaceAll(" ","%20"),
                    ar.get(20).replaceAll(" ","%20"),
                    ar.get(21).replaceAll(" ","%20"),
                    ar.get(22).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No").replaceAll("\n","%20"),
                    ar.get(23).replaceAll(" ","%20"),
                    ar.get(24).replaceAll(" ","%20"),
                    ar.get(25).replaceAll(" ","%20"),
                    ar.get(26).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No").replaceAll("\n","%20"),
                    ar.get(27).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No").replaceAll("\n","%20"),
                    ar.get(28).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No").replaceAll("\n","%20"),
                    ar.get(29).replaceAll(" ","%20"),
                    ar.get(30).replaceAll(" ",""),
                    "1",
                    "1",
                    numDocuHijo.replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No").replaceAll("\n","%20"));


            List<DetailResponse> filteredDetail = new ArrayList<>();

            List<DetailHermes> listDetailsQuipu;
            if(tmp.getENC_TIPODOC().toString().equals("1")){
                listDetailsQuipu = billDAO.getDetailsHermes(tmp.getENC_PREFIJO(),
                        tmp.getENC_NRORECIBO(),
                        tmp.getENC_SEDE(),
                        tmp.getENC_NITEMES()
                );
            }else{
                listDetailsQuipu = billDAO.getDetailsHermesCred(tmp.getENC_PREFIJO(),
                        tmp.getENC_NRORECIBO(),
                        tmp.getENC_SEDE(),
                        tmp.getENC_NITEMES(),tmp.getENC_NUM_DOCUHIJO()
                );
            }


            for(int j=0;j<listDetailsQuipu.size();j++){
                DetailHermes tmp2 = listDetailsQuipu.get(j);
                ArrayList<String> arDet = new ArrayList<String>();

                arDet.add(tmp2.getDET_PREFIJO().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_NRORECIBO().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_SEDE().toString().replace(" ","%20"));
                arDet.add((2022+"").replace(" ","%20"));
                arDet.add(tmp2.getDET_CIAQUIPU().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_CANTIDA().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_VLRUNIT().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_PCT_DESC().toString().replace(" ","%20"));
                try{
                    arDet.add(tmp2.getDET_CODIGO().toString().replace(" ","%20"));
                }catch(NullPointerException e){
                    arDet.add(" ".toString().replace(" ","%20"));
                }
                try{
                    arDet.add(tmp2.getDET_NOMBRE().toString().replace(" ","%20"));
                }catch(NullPointerException e){
                    arDet.add(" ".toString().replace(" ","%20"));
                }
                try{
                    arDet.add(tmp2.getDET_DESCRIP().toString().replace(" ","%20"));
                }catch(NullPointerException e){
                    arDet.add(" ".toString().replace(" ","%20"));
                }
                try{
                    arDet.add(tmp2.getDET_NUM_DOCUHIJO().toString().replace(" ","%20"));
                }catch(NullPointerException e){
                    arDet.add(" ".toString().replace(" ","%20"));
                }


                DetailResponse detailToAdd = new DetailResponse(

                        arDet.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        "2022",
                        arDet.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No").replaceAll("\t","%20").replaceAll("\n","%20"),
                        arDet.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No").replaceAll("\t","%20").replaceAll("\n","%20"),
                        arDet.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")

                );

                filteredDetail.add(detailToAdd);

            }
            newMaster.setIn_detail(filteredDetail);
            response.add(newMaster);
        }
        return response;
    }


}