package com.name.business.entities;

import java.util.List;

public class MasterResponse {

    private String in_prefijo;
    private String in_nrorecibo;
    private String in_sede;
    private String in_vigencia;
    private String in_ciaquipu;
    private String in_nombresede;
    private String in_empresaquipu;
    private String in_fecemic;
    private String in_tipodoc;
    private String in_formapago;
    private String in_fecpago;
    private String in_mediopago;
    private String in_descuento;
    private String in_subtotal;
    private String in_total;
    private String in_observaciones;
    private String in_datos_pago;
    private String in_ciudad_sede;
    private String in_dir_sede;
    private String in_telef_sede;
    private String in_email_sede_emision;
    private String in_clinatu;
    private String in_clinomb;
    private String in_clitipoid;
    private String in_clinumid;
    private String in_cliciudad;
    private String in_clidir;
    private String in_clitelelef;
    private String in_cliemail;
    private String in_clirespfiscal;
    private String in_clirespiva;
    private String in_origen;
    private String in_forma_datos_origen;
    private String in_num_docuhijo;
    private List<DetailResponse> in_detail;

    public MasterResponse(String in_prefijo, String in_nrorecibo, String in_sede, String in_vigencia, String in_ciaquipu, String in_nombresede, String in_empresaquipu, String in_fecemic, String in_tipodoc, String in_formapago, String in_fecpago, String in_mediopago, String in_descuento, String in_subtotal, String in_total, String in_observaciones, String in_datos_pago, String in_ciudad_sede, String in_dir_sede, String in_telef_sede, String in_email_sede_emision, String in_clinatu, String in_clinomb, String in_clitipoid, String in_clinumid, String in_cliciudad, String in_clidir, String in_clitelelef, String in_cliemail, String in_clirespfiscal, String in_clirespiva, String in_origen, String in_forma_datos_origen, String in_num_docuhijo) {
        this.in_prefijo = in_prefijo;
        this.in_nrorecibo = in_nrorecibo;
        this.in_sede = in_sede;
        this.in_vigencia = in_vigencia;
        this.in_ciaquipu = in_ciaquipu;
        this.in_nombresede = in_nombresede;
        this.in_empresaquipu = in_empresaquipu;
        this.in_fecemic = in_fecemic;
        this.in_tipodoc = in_tipodoc;
        this.in_formapago = in_formapago;
        this.in_fecpago = in_fecpago;
        this.in_mediopago = in_mediopago;
        this.in_descuento = in_descuento;
        this.in_subtotal = in_subtotal;
        this.in_total = in_total;
        this.in_observaciones = in_observaciones;
        this.in_datos_pago = in_datos_pago;
        this.in_ciudad_sede = in_ciudad_sede;
        this.in_dir_sede = in_dir_sede;
        this.in_telef_sede = in_telef_sede;
        this.in_email_sede_emision = in_email_sede_emision;
        this.in_clinatu = in_clinatu;
        this.in_clinomb = in_clinomb;
        this.in_clitipoid = in_clitipoid;
        this.in_clinumid = in_clinumid;
        this.in_cliciudad = in_cliciudad;
        this.in_clidir = in_clidir;
        this.in_clitelelef = in_clitelelef;
        this.in_cliemail = in_cliemail;
        this.in_clirespfiscal = in_clirespfiscal;
        this.in_clirespiva = in_clirespiva;
        this.in_origen = in_origen;
        this.in_forma_datos_origen = in_forma_datos_origen;
        this.in_num_docuhijo = in_num_docuhijo;
    }

    public List<DetailResponse> getIn_detail() {
        return in_detail;
    }

    public void setIn_detail(List<DetailResponse> in_detail) {
        this.in_detail = in_detail;
    }

    public String getIn_prefijo() {
        return in_prefijo;
    }

    public void setIn_prefijo(String in_prefijo) {
        this.in_prefijo = in_prefijo;
    }

    public String getIn_nrorecibo() {
        return in_nrorecibo;
    }

    public void setIn_nrorecibo(String in_nrorecibo) {
        this.in_nrorecibo = in_nrorecibo;
    }

    public String getIn_sede() {
        return in_sede;
    }

    public void setIn_sede(String in_sede) {
        this.in_sede = in_sede;
    }

    public String getIn_vigencia() {
        return in_vigencia;
    }

    public void setIn_vigencia(String in_vigencia) {
        this.in_vigencia = in_vigencia;
    }

    public String getIn_ciaquipu() {
        return in_ciaquipu;
    }

    public void setIn_ciaquipu(String in_ciaquipu) {
        this.in_ciaquipu = in_ciaquipu;
    }

    public String getIn_nombresede() {
        return in_nombresede;
    }

    public void setIn_nombresede(String in_nombresede) {
        this.in_nombresede = in_nombresede;
    }

    public String getIn_empresaquipu() {
        return in_empresaquipu;
    }

    public void setIn_empresaquipu(String in_empresaquipu) {
        this.in_empresaquipu = in_empresaquipu;
    }

    public String getIn_fecemic() {
        return in_fecemic;
    }

    public void setIn_fecemic(String in_fecemic) {
        this.in_fecemic = in_fecemic;
    }

    public String getIn_tipodoc() {
        return in_tipodoc;
    }

    public void setIn_tipodoc(String in_tipodoc) {
        this.in_tipodoc = in_tipodoc;
    }

    public String getIn_formapago() {
        return in_formapago;
    }

    public void setIn_formapago(String in_formapago) {
        this.in_formapago = in_formapago;
    }

    public String getIn_fecpago() {
        return in_fecpago;
    }

    public void setIn_fecpago(String in_fecpago) {
        this.in_fecpago = in_fecpago;
    }

    public String getIn_mediopago() {
        return in_mediopago;
    }

    public void setIn_mediopago(String in_mediopago) {
        this.in_mediopago = in_mediopago;
    }

    public String getIn_descuento() {
        return in_descuento;
    }

    public void setIn_descuento(String in_descuento) {
        this.in_descuento = in_descuento;
    }

    public String getIn_subtotal() {
        return in_subtotal;
    }

    public void setIn_subtotal(String in_subtotal) {
        this.in_subtotal = in_subtotal;
    }

    public String getIn_total() {
        return in_total;
    }

    public void setIn_total(String in_total) {
        this.in_total = in_total;
    }

    public String getIn_observaciones() {
        return in_observaciones;
    }

    public void setIn_observaciones(String in_observaciones) {
        this.in_observaciones = in_observaciones;
    }

    public String getIn_datos_pago() {
        return in_datos_pago;
    }

    public void setIn_datos_pago(String in_datos_pago) {
        this.in_datos_pago = in_datos_pago;
    }

    public String getIn_ciudad_sede() {
        return in_ciudad_sede;
    }

    public void setIn_ciudad_sede(String in_ciudad_sede) {
        this.in_ciudad_sede = in_ciudad_sede;
    }

    public String getIn_dir_sede() {
        return in_dir_sede;
    }

    public void setIn_dir_sede(String in_dir_sede) {
        this.in_dir_sede = in_dir_sede;
    }

    public String getIn_telef_sede() {
        return in_telef_sede;
    }

    public void setIn_telef_sede(String in_telef_sede) {
        this.in_telef_sede = in_telef_sede;
    }

    public String getIn_email_sede_emision() {
        return in_email_sede_emision;
    }

    public void setIn_email_sede_emision(String in_email_sede_emision) {
        this.in_email_sede_emision = in_email_sede_emision;
    }

    public String getIn_clinatu() {
        return in_clinatu;
    }

    public void setIn_clinatu(String in_clinatu) {
        this.in_clinatu = in_clinatu;
    }

    public String getIn_clinomb() {
        return in_clinomb;
    }

    public void setIn_clinomb(String in_clinomb) {
        this.in_clinomb = in_clinomb;
    }

    public String getIn_clitipoid() {
        return in_clitipoid;
    }

    public void setIn_clitipoid(String in_clitipoid) {
        this.in_clitipoid = in_clitipoid;
    }

    public String getIn_clinumid() {
        return in_clinumid;
    }

    public void setIn_clinumid(String in_clinumid) {
        this.in_clinumid = in_clinumid;
    }

    public String getIn_cliciudad() {
        return in_cliciudad;
    }

    public void setIn_cliciudad(String in_cliciudad) {
        this.in_cliciudad = in_cliciudad;
    }

    public String getIn_clidir() {
        return in_clidir;
    }

    public void setIn_clidir(String in_clidir) {
        this.in_clidir = in_clidir;
    }

    public String getIn_clitelelef() {
        return in_clitelelef;
    }

    public void setIn_clitelelef(String in_clitelelef) {
        this.in_clitelelef = in_clitelelef;
    }

    public String getIn_cliemail() {
        return in_cliemail;
    }

    public void setIn_cliemail(String in_cliemail) {
        this.in_cliemail = in_cliemail;
    }

    public String getIn_clirespfiscal() {
        return in_clirespfiscal;
    }

    public void setIn_clirespfiscal(String in_clirespfiscal) {
        this.in_clirespfiscal = in_clirespfiscal;
    }

    public String getIn_clirespiva() {
        return in_clirespiva;
    }

    public void setIn_clirespiva(String in_clirespiva) {
        this.in_clirespiva = in_clirespiva;
    }

    public String getIn_origen() {
        return in_origen;
    }

    public void setIn_origen(String in_origen) {
        this.in_origen = in_origen;
    }

    public String getIn_forma_datos_origen() {
        return in_forma_datos_origen;
    }

    public void setIn_forma_datos_origen(String in_forma_datos_origen) {
        this.in_forma_datos_origen = in_forma_datos_origen;
    }

    public String getIn_num_docuhijo() {
        return in_num_docuhijo;
    }

    public void setIn_num_docuhijo(String in_num_docuhijo) {
        this.in_num_docuhijo = in_num_docuhijo;
    }
}
