package com.name.business.mappers;

import com.name.business.entities.BillingDianInfo;
import com.name.business.entities.Detail;
import com.name.business.entities.Master;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillingDianInfoMapper implements ResultSetMapper<BillingDianInfo> {

    @Override
    public BillingDianInfo map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BillingDianInfo(
                resultSet.getLong("ENC_CONSDIAN"),
                resultSet.getString("ENC_FECEMIC")
        );
    }
}
