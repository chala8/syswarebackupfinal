package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.JsonConverterBusiness;
//import com.name.business.entities.*;
import com.name.business.entities.Detail;
import com.name.business.entities.Master;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Date;


@Path("/unalfe")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JsonConverterResource {
    private JsonConverterBusiness jsonConverterBusiness;

    public JsonConverterResource(JsonConverterBusiness jsonConverterBusiness) {
        this.jsonConverterBusiness = jsonConverterBusiness;
    }


    @Path("/master")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response masterProcedure(@QueryParam("fecha")String fecha){
        Response response;
        List<Master> getResponseGeneric = jsonConverterBusiness.master(fecha);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }

    @Path("/detail")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response detail(
            @QueryParam("det_nrorecibo")String det_nrorecibo
    ){
        Response response;
        List<Detail> getResponseGeneric = jsonConverterBusiness.detail(det_nrorecibo);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }

    @GET
    @Timed
        @Path("/refrescar_vistas_sia_fe")
    @RolesAllowed({"Auth"})
    public Response refrescar_vistas_sia_fe(){
        Response response;
        Long generalResponse = jsonConverterBusiness.refrescar_vistas_sia_fe();
        response = Response.status(Response.Status.OK).entity(generalResponse).build();
        return response;
    }
}