package com.name.business.DAOs;
import com.name.business.entities.Detail;
import com.name.business.entities.Master;
import com.name.business.mappers.DetailMapper;
import com.name.business.mappers.MasterMapper;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

//import com.name.business.entities.*;
//import com.name.business.mappers.*;

//@RegisterMapper(CloseMapper.class)
public interface JsonConverterDao {





    @RegisterMapper(MasterMapper.class)
    @SqlQuery(" select * from sia_fe_maestro where enc_fecemic=:fecha and enc_nrorecibo!='2021908613' ")
    List<Master> master(@Bind("fecha") String fecha);



    @RegisterMapper(DetailMapper.class)
    @SqlQuery(" select * from sia_fe_detalle where det_nrorecibo=:det_nrorecibo ")
    List<Detail> detail(@Bind("det_nrorecibo") String det_nrorecibo);



    @SqlCall("begin refrescar_vistas_sia_fe(); end; ")
    void refrescar_vistas_sia_fe();

}
