package com.name.business.entities;

public class Master {

    private String ENC_PREFIJO;
    private String ENC_NRORECIBO;
    private Long ENC_SEDE;
    private Long ENC_VIGENCIA;
    private String ENC_NITEMES;
    private String ENC_NOMBRESEDE;
    private String ENC_EMPRESAQUIPU;
    private String ENC_FECEMIC;
    private Long ENC_TIPODOC;
    private Long ENC_FORMAPAGO;
    private String ENC_FECPAGO;
    private Long ENC_MEDIOPAGO;
    private Long ENC_DESCUENTO;
    private Double ENC_SUBTOTAL;
    private Double ENC_TOTAL;
    private String ENC_OBSERVACIONES;
    private String ENC_DATOS_PAGO;
    private String ENC_CIUDAD_SEDE;
    private String ENC_DIR_SEDE;
    private String ENC_TELEF_SEDE;
    private String ENC_EMAIL_SEDE_EMISION;
    private Long ENC_CLINATU;
    private String ENC_CLINOMB;
    private String ENC_CLI_TIPOID;
    private String ENC_CLI_NUMID;
    private String ENC_CLICIUDAD;
    private String ENC_CLIDIR;
    private String ENC_CLITELEF;
    private String ENC_CLIEMAIL;
    private Long ENC_CLIRESPFIS;
    private Long ENC_CLIRESPIVA;
    private String FECHA_ACTUALIZA;


    public Master(String ENC_PREFIJO, String ENC_NRORECIBO, Long ENC_SEDE, Long ENC_VIGENCIA, String ENC_NITEMES, String ENC_NOMBRESEDE, String ENC_EMPRESAQUIPU, String ENC_FECEMIC, Long ENC_TIPODOC, Long ENC_FORMAPAGO, String ENC_FECPAGO, Long ENC_MEDIOPAGO, Long ENC_DESCUENTO, Double ENC_SUBTOTAL, Double ENC_TOTAL, String ENC_OBSERVACIONES, String ENC_DATOS_PAGO, String ENC_CIUDAD_SEDE, String ENC_DIR_SEDE, String ENC_TELEF_SEDE, String ENC_EMAIL_SEDE_EMISION, Long ENC_CLINATU, String ENC_CLINOMB, String ENC_CLI_TIPOID, String ENC_CLI_NUMID, String ENC_CLICIUDAD, String ENC_CLIDIR, String ENC_CLITELEF, String ENC_CLIEMAIL, Long ENC_CLIRESPFIS, Long ENC_CLIRESPIVA, String FECHA_ACTUALIZA) {
        this.ENC_PREFIJO = ENC_PREFIJO;
        this.ENC_NRORECIBO = ENC_NRORECIBO;
        this.ENC_SEDE = ENC_SEDE;
        this.ENC_VIGENCIA = ENC_VIGENCIA;
        this.ENC_NITEMES = ENC_NITEMES;
        this.ENC_NOMBRESEDE = ENC_NOMBRESEDE;
        this.ENC_EMPRESAQUIPU = ENC_EMPRESAQUIPU;
        this.ENC_FECEMIC = ENC_FECEMIC;
        this.ENC_TIPODOC = ENC_TIPODOC;
        this.ENC_FORMAPAGO = ENC_FORMAPAGO;
        this.ENC_FECPAGO = ENC_FECPAGO;
        this.ENC_MEDIOPAGO = ENC_MEDIOPAGO;
        this.ENC_DESCUENTO = ENC_DESCUENTO;
        this.ENC_SUBTOTAL = ENC_SUBTOTAL;
        this.ENC_TOTAL = ENC_TOTAL;
        this.ENC_OBSERVACIONES = ENC_OBSERVACIONES;
        this.ENC_DATOS_PAGO = ENC_DATOS_PAGO;
        this.ENC_CIUDAD_SEDE = ENC_CIUDAD_SEDE;
        this.ENC_DIR_SEDE = ENC_DIR_SEDE;
        this.ENC_TELEF_SEDE = ENC_TELEF_SEDE;
        this.ENC_EMAIL_SEDE_EMISION = ENC_EMAIL_SEDE_EMISION;
        this.ENC_CLINATU = ENC_CLINATU;
        this.ENC_CLINOMB = ENC_CLINOMB;
        this.ENC_CLI_TIPOID = ENC_CLI_TIPOID;
        this.ENC_CLI_NUMID = ENC_CLI_NUMID;
        this.ENC_CLICIUDAD = ENC_CLICIUDAD;
        this.ENC_CLIDIR = ENC_CLIDIR;
        this.ENC_CLITELEF = ENC_CLITELEF;
        this.ENC_CLIEMAIL = ENC_CLIEMAIL;
        this.ENC_CLIRESPFIS = ENC_CLIRESPFIS;
        this.ENC_CLIRESPIVA = ENC_CLIRESPIVA;
        this.FECHA_ACTUALIZA = FECHA_ACTUALIZA;
    }

    public String getENC_PREFIJO() {
        return ENC_PREFIJO;
    }

    public void setENC_PREFIJO(String ENC_PREFIJO) {
        this.ENC_PREFIJO = ENC_PREFIJO;
    }

    public String getENC_NRORECIBO() {
        return ENC_NRORECIBO;
    }

    public void setENC_NRORECIBO(String ENC_NRORECIBO) {
        this.ENC_NRORECIBO = ENC_NRORECIBO;
    }

    public Long getENC_SEDE() {
        return ENC_SEDE;
    }

    public void setENC_SEDE(Long ENC_SEDE) {
        this.ENC_SEDE = ENC_SEDE;
    }

    public Long getENC_VIGENCIA() {
        return ENC_VIGENCIA;
    }

    public void setENC_VIGENCIA(Long ENC_VIGENCIA) {
        this.ENC_VIGENCIA = ENC_VIGENCIA;
    }

    public String getENC_NITEMES() {
        return ENC_NITEMES;
    }

    public void setENC_NITEMES(String ENC_NITEMES) {
        this.ENC_NITEMES = ENC_NITEMES;
    }

    public String getENC_NOMBRESEDE() {
        return ENC_NOMBRESEDE;
    }

    public void setENC_NOMBRESEDE(String ENC_NOMBRESEDE) {
        this.ENC_NOMBRESEDE = ENC_NOMBRESEDE;
    }

    public String getENC_EMPRESAQUIPU() {
        return ENC_EMPRESAQUIPU;
    }

    public void setENC_EMPRESAQUIPU(String ENC_EMPRESAQUIPU) {
        this.ENC_EMPRESAQUIPU = ENC_EMPRESAQUIPU;
    }

    public String getENC_FECEMIC() {
        return ENC_FECEMIC;
    }

    public void setENC_FECEMIC(String ENC_FECEMIC) {
        this.ENC_FECEMIC = ENC_FECEMIC;
    }

    public Long getENC_TIPODOC() {
        return ENC_TIPODOC;
    }

    public void setENC_TIPODOC(Long ENC_TIPODOC) {
        this.ENC_TIPODOC = ENC_TIPODOC;
    }

    public Long getENC_FORMAPAGO() {
        return ENC_FORMAPAGO;
    }

    public void setENC_FORMAPAGO(Long ENC_FORMAPAGO) {
        this.ENC_FORMAPAGO = ENC_FORMAPAGO;
    }

    public String getENC_FECPAGO() {
        return ENC_FECPAGO;
    }

    public void setENC_FECPAGO(String ENC_FECPAGO) {
        this.ENC_FECPAGO = ENC_FECPAGO;
    }

    public Long getENC_MEDIOPAGO() {
        return ENC_MEDIOPAGO;
    }

    public void setENC_MEDIOPAGO(Long ENC_MEDIOPAGO) {
        this.ENC_MEDIOPAGO = ENC_MEDIOPAGO;
    }

    public Long getENC_DESCUENTO() {
        return ENC_DESCUENTO;
    }

    public void setENC_DESCUENTO(Long ENC_DESCUENTO) {
        this.ENC_DESCUENTO = ENC_DESCUENTO;
    }

    public Double getENC_SUBTOTAL() {
        return ENC_SUBTOTAL;
    }

    public void setENC_SUBTOTAL(Double ENC_SUBTOTAL) {
        this.ENC_SUBTOTAL = ENC_SUBTOTAL;
    }

    public Double getENC_TOTAL() {
        return ENC_TOTAL;
    }

    public void setENC_TOTAL(Double ENC_TOTAL) {
        this.ENC_TOTAL = ENC_TOTAL;
    }

    public String getENC_OBSERVACIONES() {
        return ENC_OBSERVACIONES;
    }

    public void setENC_OBSERVACIONES(String ENC_OBSERVACIONES) {
        this.ENC_OBSERVACIONES = ENC_OBSERVACIONES;
    }

    public String getENC_DATOS_PAGO() {
        return ENC_DATOS_PAGO;
    }

    public void setENC_DATOS_PAGO(String ENC_DATOS_PAGO) {
        this.ENC_DATOS_PAGO = ENC_DATOS_PAGO;
    }

    public String getENC_CIUDAD_SEDE() {
        return ENC_CIUDAD_SEDE;
    }

    public void setENC_CIUDAD_SEDE(String ENC_CIUDAD_SEDE) {
        this.ENC_CIUDAD_SEDE = ENC_CIUDAD_SEDE;
    }

    public String getENC_DIR_SEDE() {
        return ENC_DIR_SEDE;
    }

    public void setENC_DIR_SEDE(String ENC_DIR_SEDE) {
        this.ENC_DIR_SEDE = ENC_DIR_SEDE;
    }

    public String getENC_TELEF_SEDE() {
        return ENC_TELEF_SEDE;
    }

    public void setENC_TELEF_SEDE(String ENC_TELEF_SEDE) {
        this.ENC_TELEF_SEDE = ENC_TELEF_SEDE;
    }

    public String getENC_EMAIL_SEDE_EMISION() {
        return ENC_EMAIL_SEDE_EMISION;
    }

    public void setENC_EMAIL_SEDE_EMISION(String ENC_EMAIL_SEDE_EMISION) {
        this.ENC_EMAIL_SEDE_EMISION = ENC_EMAIL_SEDE_EMISION;
    }

    public Long getENC_CLINATU() {
        return ENC_CLINATU;
    }

    public void setENC_CLINATU(Long ENC_CLINATU) {
        this.ENC_CLINATU = ENC_CLINATU;
    }

    public String getENC_CLINOMB() {
        return ENC_CLINOMB;
    }

    public void setENC_CLINOMB(String ENC_CLINOMB) {
        this.ENC_CLINOMB = ENC_CLINOMB;
    }

    public String getENC_CLI_TIPOID() {
        return ENC_CLI_TIPOID;
    }

    public void setENC_CLI_TIPOID(String ENC_CLI_TIPOID) {
        this.ENC_CLI_TIPOID = ENC_CLI_TIPOID;
    }

    public String getENC_CLI_NUMID() {
        return ENC_CLI_NUMID;
    }

    public void setENC_CLI_NUMID(String ENC_CLI_NUMID) {
        this.ENC_CLI_NUMID = ENC_CLI_NUMID;
    }

    public String getENC_CLICIUDAD() {
        return ENC_CLICIUDAD;
    }

    public void setENC_CLICIUDAD(String ENC_CLICIUDAD) {
        this.ENC_CLICIUDAD = ENC_CLICIUDAD;
    }

    public String getENC_CLIDIR() {
        return ENC_CLIDIR;
    }

    public void setENC_CLIDIR(String ENC_CLIDIR) {
        this.ENC_CLIDIR = ENC_CLIDIR;
    }

    public String getENC_CLITELEF() {
        return ENC_CLITELEF;
    }

    public void setENC_CLITELEF(String ENC_CLITELEF) {
        this.ENC_CLITELEF = ENC_CLITELEF;
    }

    public String getENC_CLIEMAIL() {
        return ENC_CLIEMAIL;
    }

    public void setENC_CLIEMAIL(String ENC_CLIEMAIL) {
        this.ENC_CLIEMAIL = ENC_CLIEMAIL;
    }

    public Long getENC_CLIRESPFIS() {
        return ENC_CLIRESPFIS;
    }

    public void setENC_CLIRESPFIS(Long ENC_CLIRESPFIS) {
        this.ENC_CLIRESPFIS = ENC_CLIRESPFIS;
    }

    public Long getENC_CLIRESPIVA() {
        return ENC_CLIRESPIVA;
    }

    public void setENC_CLIRESPIVA(Long ENC_CLIRESPIVA) {
        this.ENC_CLIRESPIVA = ENC_CLIRESPIVA;
    }

    public String getFECHA_ACTUALIZA() {
        return FECHA_ACTUALIZA;
    }

    public void setFECHA_ACTUALIZA(String FECHA_ACTUALIZA) {
        this.FECHA_ACTUALIZA = FECHA_ACTUALIZA;
    }
}
