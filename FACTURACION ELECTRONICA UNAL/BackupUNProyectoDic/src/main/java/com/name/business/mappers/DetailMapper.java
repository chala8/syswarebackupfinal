package com.name.business.mappers;

import com.name.business.entities.Detail;
import com.name.business.entities.Master;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailMapper implements ResultSetMapper<Detail> {

    @Override
    public Detail map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Detail(
                resultSet.getLong("DET_ENC_ID"),
                resultSet.getString("DET_PREFIJO"),
                resultSet.getString("DET_NRORECIBO"),
                resultSet.getLong("DET_SEDE"),
                resultSet.getLong("DET_VIGENCIA"),
                resultSet.getString("DET_CIAQUIPU"),
                resultSet.getLong("DET_CANTIDAD"),
                resultSet.getLong("DET_VLRUNIT"),
                resultSet.getLong("DET_PCT_DESC"),
                resultSet.getString("DET_CODIGO"),
                resultSet.getString("DET_NOMBRE"),
                resultSet.getString("DET_DESCRIP")
        );
    }
}
