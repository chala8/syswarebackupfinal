package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.JsonConverterBusiness;
//import com.name.business.entities.*;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Date;


@Path("/convert")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JsonConverterResource {
    private JsonConverterBusiness jsonConverterBusiness;

    public JsonConverterResource(JsonConverterBusiness jsonConverterBusiness) {
        this.jsonConverterBusiness = jsonConverterBusiness;
    }


    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response JsonConverterMethod(){

        Response response;

        //Long responseObject = jsonConverterBusiness.ExecuteProcedureFromCSV();

        //Long responseObject2 = jsonConverterBusiness.asigna_consecutivo_dian();

        Long getResponseGeneric = jsonConverterBusiness.JsonConverterMethodCredit();

        response = Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;
    }



    @GET
    @Timed
    @Path("consecutivoDian")
    @RolesAllowed({"Auth"})
    public Response asigna_consecutivo_dian(){

        Response response;

        Long responseObject = jsonConverterBusiness.asigna_consecutivo_dian();

        response = Response.status(Response.Status.OK).entity(responseObject).build();

        return response;
    }
}