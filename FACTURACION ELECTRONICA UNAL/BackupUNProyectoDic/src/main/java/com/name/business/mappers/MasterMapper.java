package com.name.business.mappers;

import com.name.business.entities.Master;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MasterMapper implements ResultSetMapper<Master> {

    @Override
    public Master map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Master(
                resultSet.getLong("ENC_ID"),
                resultSet.getString("ENC_PREFIJO"),
                resultSet.getString("ENC_NRORECIBO"),
                resultSet.getLong("ENC_SEDE"),
                resultSet.getLong("ENC_VIGENCIA"),
                resultSet.getString("ENC_CIAQUIPU"),
                resultSet.getLong("ENC_NRO_ITEMES"),
                resultSet.getString("ENC_NOMBRESEDE"),
                resultSet.getString("ENC_EMPRESAQUIPU"),
                resultSet.getString("ENC_FECEMIC"),
                resultSet.getLong("ENC_TIPODOC"),
                resultSet.getLong("ENC_FORMAPAGO"),
                resultSet.getString("ENC_FECPAGO"),
                resultSet.getLong("ENC_MEDIOPAGO"),
                resultSet.getLong("ENC_DESCUENTO"),
                resultSet.getLong("ENC_SUBTOTAL"),
                resultSet.getLong("ENC_TOTAL"),
                resultSet.getString("ENC_OBSERVACIONES"),
                resultSet.getString("ENC_DATOS_PAGO"),
                resultSet.getString("ENC_CIUDAD_SEDE"),
                resultSet.getString("ENC_DIR_SEDE"),
                resultSet.getString("ENC_TELEF_SEDE"),
                resultSet.getString("ENC_EMAIL_SEDE_EMISION"),
                resultSet.getLong("ENC_CLINATU"),
                resultSet.getString("ENC_CLINOMB"),
                resultSet.getString("ENC_CLITIPOID"),
                resultSet.getString("ENC_CLINUMID"),
                resultSet.getString("ENC_CLICIUDAD"),
                resultSet.getString("ENC_CLIDIR"),
                resultSet.getString("ENC_CLITELELEF"),
                resultSet.getString("ENC_CLIEMAIL"),
                resultSet.getString("ENC_CLIRESPFISCAL"),
                resultSet.getLong("ENC_CLIRESPIVA"),
                resultSet.getString("ENC_FEC_CARGA"),
                resultSet.getString("ENC_ESTADO"),
                resultSet.getLong("ENC_CONSDIAN"),
                resultSet.getLong("ENC_ORIGEN"),
                resultSet.getLong("ENC_FORMA_DATOS_ORIGEN")
        );
    }
}
