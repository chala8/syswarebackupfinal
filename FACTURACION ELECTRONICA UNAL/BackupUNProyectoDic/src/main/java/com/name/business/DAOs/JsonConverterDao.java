package com.name.business.DAOs;
import com.name.business.entities.Detail;
import com.name.business.entities.Master;
import com.name.business.mappers.DetailMapper;
import com.name.business.mappers.MasterMapper;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

//import com.name.business.entities.*;
//import com.name.business.mappers.*;

//@RegisterMapper(CloseMapper.class)
public interface JsonConverterDao {

    @SqlCall("begin asigna_consecutivo_dian(); end; ")
    void asigna_consecutivo_dian();

    @RegisterMapper(MasterMapper.class)
    @SqlQuery("SELECT ENC_ID," +
            "ENC_PREFIJO," +
            "ENC_NRORECIBO," +
            "ENC_SEDE," +
            "ENC_VIGENCIA," +
            "ENC_CIAQUIPU," +
            "ENC_NRO_ITEMES," +
            "ENC_NOMBRESEDE," +
            "ENC_EMPRESAQUIPU," +
            "TO_CHAR(ENC_FECEMIC, 'yyyy-mm-dd') ENC_FECEMIC," +
            "ENC_TIPODOC," +
            "ENC_FORMAPAGO," +
            "TO_CHAR(ENC_FECPAGO, 'yyyy-mm-dd') ENC_FECPAGO," +
            "ENC_MEDIOPAGO," +
            "ENC_DESCUENTO," +
            "ENC_SUBTOTAL," +
            "ENC_TOTAL," +
            "ENC_OBSERVACIONES," +
            "ENC_DATOS_PAGO," +
            "ENC_CIUDAD_SEDE," +
            "ENC_DIR_SEDE," +
            "ENC_TELEF_SEDE," +
            "ENC_EMAIL_SEDE_EMISION," +
            "ENC_CLINATU, " +
            "ENC_CLINOMB, " +
            "ENC_CLITIPOID, " +
            "ENC_CLINUMID, " +
            "ENC_CLICIUDAD, " +
            "ENC_CLIDIR, " +
            "ENC_CLITELELEF, " +
            "ENC_CLIEMAIL, " +
            "ENC_CLIRESPFISCAL, " +
            "ENC_CLIRESPIVA, " +
            "ENC_FEC_CARGA, " +
            "ENC_ESTADO, " +
            "ENC_CONSDIAN, " +
            "ENC_ORIGEN, " +
            "ENC_FORMA_DATOS_ORIGEN " +
            "FROM FE_TMAESTRO WHERE ENC_ESTADO IN ('T','C')")
    List<Master> getMaster();

    @RegisterMapper(DetailMapper.class)
    @SqlQuery(" SELECT DET_ENC_ID," +
            "DET_PREFIJO," +
            "DET_NRORECIBO," +
            "DET_SEDE," +
            "DET_VIGENCIA," +
            "DET_CIAQUIPU," +
            "DET_CANTIDAD," +
            "DET_VLRUNIT," +
            "DET_PCT_DESC," +
            "DET_CODIGO," +
            "DET_NOMBRE," +
            "DET_DESCRIP " +
            "FROM FE_TDETALLES " +
            "WHERE DET_PREFIJO=:PREFIJO AND DET_NRORECIBO=:NRORECIBO AND DET_SEDE=:SEDE AND DET_VIGENCIA=:VIGENCIA " +
            "AND DET_CIAQUIPU=:CIAQUIPU ")
    List<Detail> getDetails(@Bind("PREFIJO") String PREFIJO,
                            @Bind("NRORECIBO") String NRORECIBO,
                            @Bind("SEDE") Long SEDE,
                            @Bind("VIGENCIA") Long VIGENCIA,
                            @Bind("CIAQUIPU") String CIAQUIPU);


    @SqlUpdate("UPDATE FE_TMAESTRO SET ENC_ESTADO=:estado \n" +
            "WHERE DET_PREFIJO=:PREFIJO AND DET_NRORECIBO=:NRORECIBO AND DET_SEDE=:SEDE AND DET_VIGENCIA=:VIGENCIA\n" +
            "AND DET_CIAQUIPU=:CIAQUIPU")
    void updateTable(@Bind("estado") String estado,
                     @Bind("PREFIJO") String PREFIJO,
                     @Bind("NRORECIBO") String NRORECIBO,
                     @Bind("SEDE") Long SEDE,
                     @Bind("VIGENCIA") Long VIGENCIA,
                     @Bind("CIAQUIPU") String CIAQUIPU);


}
