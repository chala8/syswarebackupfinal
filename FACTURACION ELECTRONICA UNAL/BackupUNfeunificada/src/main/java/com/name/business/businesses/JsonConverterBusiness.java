package com.name.business.businesses;
import com.name.business.DAOs.JsonConverterDao;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.apache.commons.codec.binary.Base64;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;
import org.json.XML;


public class JsonConverterBusiness {

    private JsonConverterDao billDAO;
    //VARIABLES PARA CONEXIÓN CORREO
    static final String FROM = "soporte@tienda724.com";
    static final String FROMNAME = "Soporte Tienda 724";
    static final String SMTP_USERNAME = "soporte@tienda724.com";
    static final String SMTP_PASSWORD = "Lx3BPR5j8pdt2xmm";
    static final String HOST = "smtp.mail.us-east-1.awsapps.com";
    static final int PORT = 465;
    ///////////////////////////////////

    public JsonConverterBusiness(JsonConverterDao billDAO) {
        this.billDAO = billDAO;
    }


    public Long masterProcedure(String in_prefijo,
            String in_nrorecibo,
            Long in_sede,
            Long in_vigencia,
            String in_ciaquipu,
            String in_nombresede,
            String in_empresaquipu,
            String in_fecemic,
            Long in_tipodoc,
            Long in_formapago,
            String in_fecpago,
            Long in_mediopago,
            Long in_descuento,
            Long in_subtotal,
            Long in_total,
            String in_observaciones,
            String in_datos_pago,
            String in_ciudad_sede,
            String in_dir_sede,
            String in_telef_sede,
            String in_email_sede_emision,
            Long in_clinatu,
            String in_clinomb,
            String in_clitipoid,
            String in_clinumid,
            String in_cliciudad,
            String in_clidir,
            String in_clitelelef,
            String in_cliemail,
            String in_clirespfiscal,
            Long in_clirespiva,
            Long in_origen,
            Long in_forma_datos_origen,
            String in_num_docuhijo){

        try{
            System.out.println(in_fecemic);
            System.out.println(in_fecpago);


            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateInString = in_fecemic;
            String dateInString2 = in_fecpago;


            Date date1 = new Date();
            Date date2 = new Date();
            try {


                java.util.Date utilDate = formatter.parse(dateInString);
                java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                date1 = sqlDate;

                try{
                    java.util.Date utilDate2 = formatter.parse(dateInString2);
                    java.sql.Date sqlDate2 = new java.sql.Date(utilDate2.getTime());
                    date2 = sqlDate2;
                }catch(Exception e){
                    date2 = sqlDate;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            /* Below code snippet convert a java util Date into a sql Date for use in databases */



            System.out.println(" call poblar_tmpmaestro" +
                    "(" +
                    in_prefijo+","+
                    in_nrorecibo+","+
                    in_sede+","+
                    in_vigencia+","+
                    in_ciaquipu+","+
                    in_nombresede+","+
                    in_empresaquipu+","+
                    in_fecemic+","+
                    in_tipodoc+","+
                    in_formapago+","+
                    in_fecpago+","+
                    in_mediopago+","+
                    in_descuento+","+
                    in_subtotal+","+
                    in_total+","+
                    in_observaciones+","+
                    in_datos_pago+","+
                    in_ciudad_sede+","+
                    in_dir_sede+","+
                    in_telef_sede+","+
                    in_email_sede_emision+","+
                    in_clinatu+","+
                    in_clinomb+","+
                    in_clitipoid+","+
                    in_clinumid+","+
                    in_cliciudad+","+
                    in_clidir+","+
                    in_clitelelef+","+
                    in_cliemail+","+
                    in_clirespfiscal+","+
                    in_clirespiva+","+
                    in_origen+","+
                    in_forma_datos_origen+
                    ")");


            System.out.println("'USBG','0003520938',10,2020,'1002','UNISALUD-BOGOTA','UNISALUD-BOGOTA',sysdate,1,1,sysdate,3,0,6500,6500,'Pago de Moderadora','007769998720','11001','CRA 45 26 85','3165000','tesounisal_bog@unal.edu.co',2,'ROSA JULIETH CIFUENTES GUZMAN','13','1012355727','11001','KR 78 J 58 M 5 SUR  BR JOSE ANTONIO GALAN BOSA','111','rjcifuentesg@unal.edu.co','1',1,'1','1'");
            billDAO.poblar_tmpmaestro( in_prefijo,
                     in_nrorecibo,
                     in_sede,
                     in_vigencia,
                     in_ciaquipu,
                     in_nombresede,
                     in_empresaquipu,
                     date1,
                     in_tipodoc,
                     in_formapago,
                     date2,
                     in_mediopago,
                     in_descuento,
                     in_subtotal,
                     in_total,
                     in_observaciones,
                     in_datos_pago,
                     in_ciudad_sede,
                     in_dir_sede,
                     in_telef_sede,
                     in_email_sede_emision,
                     in_clinatu,
                     in_clinomb,
                     in_clitipoid,
                     in_clinumid,
                     in_cliciudad,
                     in_clidir,
                     in_clitelelef,
                     in_cliemail,
                     in_clirespfiscal,
                     in_clirespiva,
                     in_origen,
                     in_forma_datos_origen,
                     in_num_docuhijo);

            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;

            return new Long(0);

        }
    }


    public Long detailProcedure(    String in_prefijo,
                                    String in_nrorecibo,
                                    Long in_sede,
                                    Long in_vigencia,
                                    String in_ciaquipu,
                                    Long in_cantidad,
                                    Long in_vlrunit,
                                    Long in_pct_desc,
                                    String in_codigo,
                                    String in_nombre,
                                    String in_descrip,
                                    String in_num_docuhijo){

        try{

            billDAO.poblar_tmpdetalles( in_prefijo,
                    in_nrorecibo,
                    in_sede,
                    in_vigencia,
                    in_ciaquipu,
                    in_cantidad,
                    in_vlrunit,
                    in_pct_desc,
                    in_codigo,
                    in_nombre,
                    in_descrip,
                    in_num_docuhijo);

            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;

            return new Long(0);

        }
    }
}