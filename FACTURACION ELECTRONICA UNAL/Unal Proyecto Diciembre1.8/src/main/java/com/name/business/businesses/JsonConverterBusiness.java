package com.name.business.businesses;
import com.google.common.collect.Lists;
import com.name.business.DAOs.JsonConverterDao;
import com.name.business.entities.*;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.*;
import org.w3c.dom.Document;
import org.apache.commons.codec.binary.Base64;

import javax.sound.midi.SysexMessage;
import javax.validation.constraints.Null;
import javax.ws.rs.QueryParam;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import java.net.HttpURLConnection;
import java.net.URL;


public class JsonConverterBusiness {

    private JsonConverterDao billDAO;

    //VARIABLES PARA CONEXIÓN CORREO
    static final String FROM = "soporte@tienda724.com";
    static final String FROMNAME = "Soporte Tienda 724";
    static final String SMTP_USERNAME = "soporte@tienda724.com";
    static final String SMTP_PASSWORD = "Lx3BPR5j8pdt2xmm";
    static final String HOST = "smtp.mail.us-east-1.awsapps.com";
    static final int PORT = 465;
    private static DecimalFormat df2 = new DecimalFormat("#.##");
    public ArrayList<String> myList = new ArrayList<String>();
    public int httpCounter = 0;
    public int httpCall = 0;
    ///////////////////////////////////

    public JsonConverterBusiness(JsonConverterDao billDAO) {
        this.billDAO = billDAO;
    }

    boolean newMethod = false;








    public Long asigna_consecutivo_dian(){
        try{
            billDAO.asigna_consecutivo_dian();
            return new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }

    }



    public Long eliminar_temporales(){
        try{
            billDAO.eliminar_temporales();
            return new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }

    }



    public Long JsonConverterMethodInvoice(Long env, Master tmpMaster){


            String ciudadSegunCodigo = "Bogotá";
            String idCiudadCliente = "11001";
            String ciudadCliente = "Bogotá";
            String countrySubEntity = "Bogotá";

            if(tmpMaster.getENC_CIUDAD_SEDE().length()<=3){
                PaisData data = billDAO.getPaisData(tmpMaster.getENC_CIUDAD_SEDE());
                ciudadSegunCodigo = data.getPais();idCiudadCliente = data.getId_pais().toString();ciudadCliente = data.getPais();countrySubEntity = data.getPais();
            }else{
                if(tmpMaster.getENC_CIUDAD_SEDE().length()<5){tmpMaster.setENC_CIUDAD_SEDE("0"+tmpMaster.getENC_CIUDAD_SEDE());}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("11001")){ciudadSegunCodigo = "Bogotá";idCiudadCliente = "11001";ciudadCliente = "Bogotá";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("05001")){ciudadSegunCodigo = "Medellín";idCiudadCliente = "05001";ciudadCliente = "Medellín";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("17001")){ciudadSegunCodigo = "Manizales";idCiudadCliente = "17001";ciudadCliente = "Manizales";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("76520")){ciudadSegunCodigo = "Palmira";idCiudadCliente = "76520";ciudadCliente = "Palmira";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("88001")){ciudadSegunCodigo = "San Andrés";idCiudadCliente = "88001";ciudadCliente = "San Andrés";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("91001")){ciudadSegunCodigo = "Leticia";idCiudadCliente = "91001";ciudadCliente = "Leticia";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("81001")){ciudadSegunCodigo = "Arauca";idCiudadCliente = "81001";ciudadCliente = "Arauca";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("52835")){ciudadSegunCodigo = "Tumaco";idCiudadCliente = "52835";ciudadCliente = "Tumaco";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("20621")){ciudadSegunCodigo = "La Paz";idCiudadCliente = "20621";ciudadCliente = "La Paz";countrySubEntity = "Colombia";}
            }




            try{
                System.out.println("CIUDAD CLIENTE: " + tmpMaster.getENC_CLICIUDAD());
                idCiudadCliente = tmpMaster.getENC_CLICIUDAD().replaceAll("%20","").replaceAll("null","");
                if(idCiudadCliente.length() == 0){
                    idCiudadCliente = tmpMaster.getENC_CIUDAD_SEDE();
                    ciudadCliente = ciudadSegunCodigo;
                }else{

                    if(idCiudadCliente.length()==4){
                        idCiudadCliente = "0"+idCiudadCliente;
                    }
                    ciudadCliente = billDAO.getCiudad(idCiudadCliente);
                    String subentityCode = idCiudadCliente.substring(0,2);
                }
            }catch(Exception e){
                idCiudadCliente = tmpMaster.getENC_CIUDAD_SEDE();
                if(idCiudadCliente.length()==4){
                    idCiudadCliente = "0"+idCiudadCliente;
                }
                ciudadCliente = ciudadSegunCodigo;
            }

            System.out.println("CIUDAD CLIENTE: " + idCiudadCliente);

            int profileExecutionID = 2;
            String pref = "SETT";
            String pruebas = "https://apivp.efacturacadena.com/staging/vp/documentos/proceso/sincrono";
            String produccion = "https://apivp.efacturacadena.com/v1/vp/documentos/proceso/sincrono";
            String urlConsumption = pruebas;

            if(env == 1){
                profileExecutionID = 1;
                pref = tmpMaster.getENC_PREFIJO();
                urlConsumption = produccion;
            }

            Double SubTotal = tmpMaster.getENC_SUBTOTAL();
            Double discountvalue = tmpMaster.getENC_DESCUENTO();
            Double subtotDiv = new Double(1);
            if(!SubTotal.toString().equals("0")){
                subtotDiv = SubTotal;
            }
            Double discountPercentage = new Double((discountvalue * 100)/subtotDiv);

            if(Double.isNaN(discountPercentage)){
                discountPercentage = new Double(0);
            }

            String fechaPago = "";

            if(tmpMaster.getENC_FECPAGO()!=null && tmpMaster.getENC_FECPAGO()!="") {
            fechaPago = tmpMaster.getENC_FECPAGO();
            }else{
                fechaPago = tmpMaster.getENC_FECEMIC();
            }

            String obs = "";

            try {
                obs = tmpMaster.getENC_OBSERVACIONES().replaceAll("No\\.","#").replaceAll("NO\\.","#");
            }catch(Exception e){
                obs = "";
            }


            String cli_dir = "";

            try {
                cli_dir = tmpMaster.getENC_CLIDIR().replaceAll("No\\.","#").replaceAll("NO\\.","#");
            }catch(Exception e){
                cli_dir = "";
            }



            String cli_email = "";
            String[] obss;
            try{
                obss = obs.split("<<");
                if(obss[1].contains("@")){
                    cli_email = obss[1];
                }else{
                    cli_email = tmpMaster.getENC_CLIEMAIL();
                }
            }catch(Exception e){
                cli_email = tmpMaster.getENC_CLIEMAIL();
            }

            try {
                cli_email = cli_email.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#");
            }catch(Exception e){
                cli_email = "";
            }


            String notaac = "";
            String orderReference = "";


            int indexRef1 = obs.indexOf("[");
            if(indexRef1==-1){
                orderReference = "";
            }else{
                int indexRef2 = obs.lastIndexOf("[");
                orderReference = obs.substring(indexRef1+1,indexRef2);
            }

            int indexNote1 = obs.indexOf("_");
            if(indexNote1==-1){
                notaac = "";
            }else{
                int indexNote2 = obs.lastIndexOf("_");
                notaac = obs.substring(indexNote1,indexNote2);
            }
        String countrySubEntityCode = "CO";
        if(tmpMaster.getENC_CLITIPOID().toString().equals("42")||tmpMaster.getENC_CLITIPOID().toString().equals("41")||tmpMaster.getENC_CLITIPOID().toString().equals("50")){
            System.out.println("International");
            try {
                InternationalData data = billDAO.getInternationalData(tmpMaster.getENC_CLICIUDAD());
                String [] listing = tmpMaster.getENC_CLINUMID().split("-");
                tmpMaster.setENC_CLINUMID(listing[0]);
                idCiudadCliente = "01000";

                if(tmpMaster.getENC_CLICIUDAD().length()==1){
                    idCiudadCliente = "0100"+tmpMaster.getENC_CLICIUDAD();
                }
                if(tmpMaster.getENC_CLICIUDAD().length()==2){
                    idCiudadCliente = "010"+tmpMaster.getENC_CLICIUDAD();
                }
                if(tmpMaster.getENC_CLICIUDAD().length()==3){
                    idCiudadCliente = "01"+tmpMaster.getENC_CLICIUDAD();
                }
                if(tmpMaster.getENC_CLICIUDAD().length()==4){
                    idCiudadCliente = "0"+tmpMaster.getENC_CLICIUDAD();
                }
                if(tmpMaster.getENC_CLICIUDAD().length()==5){
                    idCiudadCliente = tmpMaster.getENC_CLICIUDAD();
                }

                int ciudadClientePos1 = tmpMaster.getENC_CLIDIR().indexOf("{");
                int ciudadClientePos2 = tmpMaster.getENC_CLIDIR().indexOf("}");
                ciudadCliente = tmpMaster.getENC_CLIDIR().substring(ciudadClientePos1+1,ciudadClientePos2);
                countrySubEntity = data.getNOMBRE_PAIS();
                countrySubEntityCode = data.getPREFIJO_DIAN();

            }catch(Exception e){
                e.printStackTrace();
                billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NO SE ENCONTRARON DATOS . SYSWARE.");
            }



        }




            //getCliEmail




            String invoiceRawXmlHead1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<Invoice xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2\" xmlns:cac=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:cbc=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ext=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:sts=\"dian:gov:co:facturaelectronica:Structures-2-1\" xmlns:xades=\"http://uri.etsi.org/01903/v1.3.2#\" xmlns:xades141=\"http://uri.etsi.org/01903/v1.4.1#\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2     http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-Invoice-2.1.xsd\">\n" +
                    "\t<cbc:CustomizationID>10</cbc:CustomizationID>\n" +
                    "\t<cbc:ProfileExecutionID>"+profileExecutionID+"</cbc:ProfileExecutionID>\n" +
                    "\t<cbc:ID>"+pref+tmpMaster.getENC_CONSDIAN()+"</cbc:ID>\n" +
                    "\t<cbc:IssueDate>"+tmpMaster.getENC_FECEMIC()+"</cbc:IssueDate>\n" +
                    "\t<cbc:IssueTime>00:00:00-05:00</cbc:IssueTime>\n" +
                    "\t<cbc:DueDate>"+fechaPago+"</cbc:DueDate>\n" +
                    "\t<cbc:InvoiceTypeCode>01</cbc:InvoiceTypeCode>\n" +
                    "\t<cbc:Note>"+obs.replaceAll("<","").replaceAll(">","")+"</cbc:Note>\n" +
                    "\t<cbc:DocumentCurrencyCode>COP</cbc:DocumentCurrencyCode>\n" +
                    "\t<cbc:LineCountNumeric>"+tmpMaster.getENC_NRO_ITEMES()+"</cbc:LineCountNumeric>\n" +
                    "\t<cac:OrderReference>\n" +
                    "\t\t<cbc:ID>"+orderReference+"</cbc:ID>\n" +
                    "\t</cac:OrderReference>\n" +
                    "\t<cac:AccountingSupplierParty>\n" +
                    "\t\t<cbc:AdditionalAccountID>1</cbc:AdditionalAccountID>\n" +
                    "\t\t<cac:Party>\n" +
                    "\t\t\t<cac:PartyName>\n" +
                    "\t\t\t\t<cbc:Name>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:Name>\n" +
                    "\t\t\t</cac:PartyName>\n" +
                    "\t\t\t<cac:PhysicalLocation>\n" +
                    "\t\t\t\t<cac:Address>\n" +
                    "\t\t\t\t\t<cbc:ID>"+tmpMaster.getENC_CIUDAD_SEDE()+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>"+ciudadSegunCodigo+"</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>"+"Colombia"+"</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>"+tmpMaster.getENC_CIUDAD_SEDE().substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>"+tmpMaster.getENC_DIR_SEDE().replaceAll("No\\.","#").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:Address>\n" +
                    "\t\t\t</cac:PhysicalLocation>\n" +
                    "\t\t\t<cac:PartyTaxScheme>\n" +
                    "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                    "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                    "\t\t\t\t<cbc:TaxLevelCode listName=\"No Aplica\">O-13;O-23</cbc:TaxLevelCode>\n" +
                    "\t\t\t\t<cac:RegistrationAddress>\n" +
                    "\t\t\t\t\t<cbc:ID>11001</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>Bogotá</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:PostalZone>000000</cbc:PostalZone>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>Bogotá</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>11</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>CR 45   26   85 OF 481 ED URIEL GUTIERREZ</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:RegistrationAddress>\n" +
                    "\t\t\t\t<cac:TaxScheme>\n" +
                    "\t\t\t\t\t<cbc:ID></cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:Name></cbc:Name>\n" +
                    "\t\t\t\t</cac:TaxScheme>\n" +
                    "\t\t\t</cac:PartyTaxScheme>\n" +
                    "\t\t\t<cac:PartyLegalEntity>\n" +
                    "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                    "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                    "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t\t\t<cbc:ID>"+pref+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CONSDIAN()+"</cbc:Name>\n" +
                    "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t</cac:PartyLegalEntity>\n" +
                    "\t\t\t<cac:Contact>\n" +
                    "\t\t\t\t<cbc:Name></cbc:Name>\n" +
                    "\t\t\t\t<cbc:Telephone>3165000</cbc:Telephone>\n" +
                    "\t\t\t\t<cbc:ElectronicMail>efactura_nal@unal.edu.co</cbc:ElectronicMail>\n" +
                    "\t\t\t</cac:Contact>\n" +
                    "\t\t</cac:Party>\n" +
                    "\t</cac:AccountingSupplierParty>\n" +
                    "\t<cac:AccountingCustomerParty>\n"+
                    "\t\t<cbc:AdditionalAccountID>"+tmpMaster.getENC_CLINATU()+"</cbc:AdditionalAccountID>\n"+
                    "\t\t<cac:Party>\t\n" +
                    "\t\t\t<cac:PartyIdentification>\t\n" +
                    "\t\t\t\t<cbc:ID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\"31\">"+tmpMaster.getENC_CLINUMID()+"</cbc:ID>\n" +
                    "\t\t\t</cac:PartyIdentification>\n" +
                    "\t\t\t<cac:PartyName>\n" +
                    "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("_","Y").replaceAll("<","").replaceAll(">","")+"</cbc:Name>\n" +
                    "\t\t\t</cac:PartyName>\n" +
                    "\t\t\t<cac:PhysicalLocation>\n" +
                    "\t\t\t\t<cac:Address>\n" +
                    "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:PostalZone>"+"00000"+"</cbc:PostalZone>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadCliente+"</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>"+idCiudadCliente.substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>"+cli_dir.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>"+countrySubEntityCode+"</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">"+countrySubEntity+"</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:Address>\n" +
                    "\t\t\t</cac:PhysicalLocation>\n" +
                    "\t\t\t<cac:PartyTaxScheme>\n" +
                    "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:RegistrationName>\n";


                    String paymentID = "Efectivo";

                    if(tmpMaster.getENC_MEDIOPAGO().toString().equals("48")){
                        paymentID = "Tarjeta Crédito";
                    }

                    if(tmpMaster.getENC_MEDIOPAGO().toString().equals("49")){
                        paymentID = "Tarjeta Débito";
                    }

                    if(tmpMaster.getENC_MEDIOPAGO().toString().equals("47")){
                        paymentID = "Transferencia Débito Bancaria";
                    }


                    String cliRespFiscal = tmpMaster.getENC_CLIRESPFISCAL();

                    try{
                        if(tmpMaster.getENC_CLIRESPFISCAL().equals("0") || tmpMaster.getENC_CLIRESPFISCAL() == null || tmpMaster.getENC_CLIRESPFISCAL().equals("null")||tmpMaster.getENC_CLIRESPFISCAL().equals("")){
                            cliRespFiscal = "R-99-PN";
                        }
                    }catch(Exception e){
                        cliRespFiscal = "R-99-PN";
                    }


                    String TaxLevelCode = "No Aplica";

                    /*try{
                        if(tmpMaster.getENC_CLIRESPIVA()== new Long(0)){
                            TaxLevelCode = "49";
                        }else{
                            TaxLevelCode = tmpMaster.getENC_CLIRESPIVA().toString();
                        }
                    }catch(Exception e){
                        TaxLevelCode = "49";
                    }
                    */
        String digitoVerificacion = "";
        String invoiceRawXmlHead = "";

        Boolean doesHttp = true;
        Boolean doesHttp2 = false;

        System.out.println("THIS IS CLINATU: "+tmpMaster.getENC_CLINATU());
        String schemeName = tmpMaster.getENC_CLITIPOID();
        if(schemeName.toString().equals("42")){
            System.out.println("International");



            digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
        }else{
            System.out.println("national");
            if(tmpMaster.getENC_CLINATU().toString().equals("1") && tmpMaster.getENC_CLITIPOID().toString().equals("31")){
                System.out.println("IM JURIDIC");
                try{
                    System.out.println("------------------------JURIDICAAAA------------------");
                    String number =  tmpMaster.getENC_CLINUMID().split("-")[0];
                    String digit = tmpMaster.getENC_CLINUMID().split("-")[1];
                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\""+digit+"\"  schemeName=\""+schemeName+"\">"+number+"</cbc:CompanyID>\n";
                }catch(Exception  e){
                    e.printStackTrace();
                    billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NO SE ENCONTRO DIGITO DE VERIFICACION EN EL DOCUMENTO. SYSWARE.");
                    doesHttp = false;
                }
            }else{
                System.out.println("------------------------NATURAL------------------");
                System.out.println("IM NATURAL");
                if(schemeName.equals("16")){

                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";

                }else{

                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";

                }
            }
        }


                    String invoiceRawXmlHead12 =  "\t\t\t\t<cbc:TaxLevelCode listName=\""+TaxLevelCode+"\">"+cliRespFiscal+"</cbc:TaxLevelCode>\n" +
                    "\t\t\t\t<cac:RegistrationAddress>\n" +
                    "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadCliente+"</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>"+idCiudadCliente.substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>"+cli_dir.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>"+countrySubEntityCode+"</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">"+countrySubEntity+"</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:RegistrationAddress>\n" +
                    "\t\t\t\t<cac:TaxScheme>\n" +
                    "\t\t\t\t\t<cbc:ID>01</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:Name>IVA</cbc:Name>\n" +
                    "\t\t\t\t</cac:TaxScheme>\n" +
                    "\t\t\t</cac:PartyTaxScheme>\n" +
                    "\t\t\t<cac:PartyLegalEntity>\n" +
                    "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:RegistrationName>\n"+
                            digitoVerificacion +
                    "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t\t\t<cbc:Name>11111</cbc:Name>\n" +
                    "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t</cac:PartyLegalEntity>\n" +
                    "\t\t\t<cac:Contact>\n" +
                    "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:Name>\n" +
                    "\t\t\t\t<cbc:Telephone>"+tmpMaster.getENC_CLITELELEF()+"</cbc:Telephone>\n" +
                    "\t\t\t\t<cbc:ElectronicMail>"+cli_email+"</cbc:ElectronicMail>\n" +
                    "\t\t\t</cac:Contact>\n" +
                    "\t\t</cac:Party>\n" +
                    "\t</cac:AccountingCustomerParty>\n" +
                    "\t<cac:PaymentMeans>\n" +
                    "\t\t<cbc:ID>"+tmpMaster.getENC_FORMAPAGO()+"</cbc:ID>\n" +
                    "\t\t<cbc:PaymentMeansCode>"+tmpMaster.getENC_MEDIOPAGO()+"</cbc:PaymentMeansCode>\n" +
                    "\t\t<cbc:PaymentDueDate>"+fechaPago+"</cbc:PaymentDueDate>\n" +
                    "\t\t<cbc:PaymentID>"+paymentID+"</cbc:PaymentID>\n" +
                    "\t</cac:PaymentMeans>\n" +
                    "\t<cac:AllowanceCharge>\n" +
                    "\t\t<cbc:ID>1</cbc:ID>\n" +
                    "\t\t<cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                    "\t\t<cbc:AllowanceChargeReasonCode>01</cbc:AllowanceChargeReasonCode>\n" +
                    "\t\t<cbc:AllowanceChargeReason>Descuento</cbc:AllowanceChargeReason>\n" +
                    "\t\t<cbc:MultiplierFactorNumeric>"+discountPercentage+"</cbc:MultiplierFactorNumeric>\n" +
                    "\t\t<cbc:Amount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_DESCUENTO()).replaceAll(",",".")+"</cbc:Amount>\n" +
                    "\t\t<cbc:BaseAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:BaseAmount>\n" +
                    "\t</cac:AllowanceCharge>\n" +
                    "\t<cac:LegalMonetaryTotal>\n" +
                    "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:LineExtensionAmount><!-- Valor bruto del documento-->\n" +
                    "\t\t<cbc:TaxExclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxExclusiveAmount><!-- Base imponilbe del documento-->\n" +
                    "\t\t<cbc:TaxInclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxInclusiveAmount><!-- Base imponible + impuestos-->\n" +
                    "\t\t<cbc:AllowanceTotalAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_DESCUENTO()).replaceAll(",",".")+"</cbc:AllowanceTotalAmount>\n" +
                    //"\t\t<!-- <cbc:ChargeTotalAmount currencyID=\"COP\">"+ChargeTotalAmount+"<cbc:ChargeTotalAmount> Aplica cuando es cargo a nivel general de la factura, suma de los cargos generales-->\n" +
                    "\t\t<cbc:PayableAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_TOTAL()).replaceAll(",",".")+"</cbc:PayableAmount><!--Valor a Pagar de Factura: Valor total de ítems (incluyendo cargos y descuentos a nivel de ítems)+valor tributos + valor cargos – valor descuentos – valor anticipos -->\n" +
                    "\t</cac:LegalMonetaryTotal>";



            System.out.println("THIS IS CLINATU: "+tmpMaster.getENC_CLINATU());

            if(schemeName.toString().equals("42")){
                System.out.println("International");
                digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
                invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
            }else{
                System.out.println("national");
                if(tmpMaster.getENC_CLINATU().toString().equals("1") && tmpMaster.getENC_CLITIPOID().toString().equals("31")){
                    System.out.println("IM JURIDIC");
                    try{
                        String number =  tmpMaster.getENC_CLINUMID().split("-")[0];
                        String digit = tmpMaster.getENC_CLINUMID().split("-")[1];
                        digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\""+digit+"\"  schemeName=\""+schemeName+"\">"+number+"</cbc:CompanyID>\n";
                        invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
                    }catch(Exception  e){
                        e.printStackTrace();
                        billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NO SE ENCONTRO DIGITO DE VERIFICACION EN EL DOCUMENTO. SYSWARE.");
                        doesHttp = false;
                    }
                }else{
                    System.out.println("IM NATURAL");
                    if(schemeName.equals("16")){

                        digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";

                    }else{

                        digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";

                    }
                    invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;


                }
            }


            if(tmpMaster.getENC_CLINUMID().equals(" ")){
                billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NUMERO DE IDENTIFICACION DE CLIENTE INVALIDO.");
                doesHttp = false; }




            List<Detail> details = billDAO.getDetails(tmpMaster.getENC_PREFIJO(),
                                                      tmpMaster.getENC_NRORECIBO(),
                                                      tmpMaster.getENC_SEDE(),
                                                      tmpMaster.getENC_VIGENCIA(),
                                                      tmpMaster.getENC_CIAQUIPU(),
                tmpMaster.getENC_NUM_DOCUHIJO());


            String detailXmlInvoiceLine = "";

            for(int j = 0; j<details.size();j++){

                Detail myDetail = details.get(j);

                System.out.println("BANDERA: "+myDetail.getDET_CODIGO());
                JSONObject obj1 = new JSONObject();
                obj1.put("id", (j+1));
                try{
                    obj1.put("note", myDetail.getDET_DESCRIP().replaceAll("^(?i)null$", "%20").replaceAll("null", "%20"));
                }catch(Exception e){
                    obj1.put("note", " ");
                }
                obj1.put("quantity", myDetail.getDET_CANTIDAD());
                obj1.put("amount", (new Double(myDetail.getDET_CANTIDAD()) * myDetail.getDET_VLRUNIT()));
                obj1.put("taxAmount", 0.0);
                obj1.put("taxPercent", 0.0);
                obj1.put("taxId", 01);
                obj1.put("taxName", "IVA");
                obj1.put("description", myDetail.getDET_NOMBRE());
                try{
                    obj1.put("itemId", myDetail.getDET_CODIGO().replaceAll("^(?i)null$", "%20").replaceAll("null", "%20"));
                }catch(Exception e){
                    obj1.put("itemId", " ");
                }
                obj1.put("individualPrice", myDetail.getDET_VLRUNIT());
                obj1.put("baseQuantity", 1.00);

                System.out.println("itemId: "+obj1.get("itemId").toString());

                System.out.println("converter QUANTITY: "+myDetail.getDET_CANTIDAD());

                System.out.println("QUANTITY: "+obj1.get("quantity"));
                System.out.println("QUANTITY2: "+obj1.get("quantity").toString());
                System.out.println("QUANTITY3: "+new Double(obj1.get("quantity").toString()));

                detailXmlInvoiceLine +=  generateInvoiceLines(new Integer(obj1.get("id").toString()),
                        obj1.get("note").toString().replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                        myDetail.getDET_CANTIDAD(),
                        new Double(obj1.get("amount").toString()),
                        new Double(obj1.get("taxAmount").toString()),
                        new Double(obj1.get("taxPercent").toString()),
                        new Integer(obj1.get("taxId").toString()),
                        obj1.get("taxName").toString(),
                        obj1.get("description").toString().replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                        obj1.get("itemId").toString(),
                        new Double(obj1.get("individualPrice").toString()),
                        new Double(obj1.get("baseQuantity").toString()));

            }

            String datPago = "";

            try{
                datPago = tmpMaster.getENC_DATOS_PAGO().replaceAll("%20","").replaceAll("^(?i)null$", "");
            }catch(Exception e){
                datPago = "";
            }



            String ResolucionOpcional4="Resolución de habilitación de facturación electrónica No. 18764008055116 de fecha 25/11/2020.";

            if(pref.equals("BOG")){
                ResolucionOpcional4="Resolución nueva";
            }

            if(pref.equals("SIA")){
                ResolucionOpcional4="Número Autorización: 18764029138790 Rango Autorizado Desde: SIA300001 Rango Autorizado Hasta: SIA500000 Vigencia: 2023-10-20\n" +
                        "Representación Gráfica del documento electrónico según resolución DIAN 042 del 05 de mayo del 2020.";
            }


            String invoiceRawXmlFooter = "<DATA>\n" +
                    "\t\t<UBL21>true</UBL21>\n" +
                    "\t\t<Development>\n" +
                    "\t\t\t<ShortName>UNAL</ShortName>\n" +
                    "\t\t</Development>\n" +
                    "\t\t <Filters>\n" +
                    "\t\t\t<Product ></Product><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                    "\t\t\t<SubProduct></SubProduct><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                    "\t\t\t<CUENTA/>\n" +
                    "\t\t\t<NATURALEZA/>\n" +
                    "\t\t\t<CENTRODECOSTO/>\n" +
                    "\t\t\t<CLASE/>\n" +
                    "\t\t\t<INDICE/>\n" +
                    "\t\t</Filters>\n" +
                    "\t\t<Opcional1> Somos grandes contribuyentes, no responsables de IVA </Opcional1> <!-- ID 04 Representación Gráfica -->\n" +
                    "\t\t<Opcional2>  Agentes de retención de Impuesto sobre las ventas, no sujetos al impuesto de industria y comercio </Opcional2> <!-- ID 05 Representación Gráfica -->\n" +
                    "\t\t<Opcional3> "+tmpMaster.getENC_CIAQUIPU()+" - "+tmpMaster.getENC_NRORECIBO()+"</Opcional3> <!-- ID 10 Representación Gráfica -->\n" +
                    "\t\t<Opcional4>"+datPago.replaceAll("<","").replaceAll(">","")+
                    "#Somos Institución de Educación Superior no contribuyente del impuesto de renta,  favor abstenerse de efectuar retenciones en la  fuente. " +
                    //"#"+ResolucionOpcional4+" " +
                    "#Correo: efactura_nal@unal.edu.co </Opcional4> <!-- ID 56 Representación Gráfica -->\n" +
                    "\t</DATA></Invoice>";



            //invoiceRawXmlFooter = "</Invoice>";



            String finalXML = (invoiceRawXmlHead + detailXmlInvoiceLine + invoiceRawXmlFooter).replaceAll("^(?i)null$", "%20").replaceAll("null", "%20");

            byte[] encodedBytes = Base64.encodeBase64(finalXML.getBytes());

            System.out.println("-------------------------------XML------------------------------");

            //System.out.println(finalXML);

            System.out.println("encodedBytes " + new String(encodedBytes));

        Boolean responseFinal = false;
        if(doesHttp){
            httpCall++;
            responseFinal = sendHttp(urlConsumption,env,encodedBytes,tmpMaster);
        }

        myList.add(pref+tmpMaster.getENC_CONSDIAN()+" : "+httpCounter+" : "+httpCall);


        if(responseFinal){
            return new Long(1);
        }else{
            return new Long(0);
        }
    }



    public Long JsonConverterMethodInvoiceMed(Long env, Master tmpMaster, List<SaludPaisData> listaSalud){


        String ciudadSegunCodigo = "Bogotá";
        String idCiudadCliente = "11001";
        String ciudadCliente = "Bogotá";
        String countrySubEntity = "Bogotá";

        if(tmpMaster.getENC_CIUDAD_SEDE().length()<=3){
            PaisData data = billDAO.getPaisData(tmpMaster.getENC_CIUDAD_SEDE());
            ciudadSegunCodigo = data.getPais();idCiudadCliente = data.getId_pais().toString();ciudadCliente = data.getPais();countrySubEntity = data.getPais();
        }else{
            if(tmpMaster.getENC_CIUDAD_SEDE().length()<5){tmpMaster.setENC_CIUDAD_SEDE("0"+tmpMaster.getENC_CIUDAD_SEDE());}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("11001")){ciudadSegunCodigo = "Bogotá";idCiudadCliente = "11001";ciudadCliente = "Bogotá";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("05001")){ciudadSegunCodigo = "Medellín";idCiudadCliente = "05001";ciudadCliente = "Medellín";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("17001")){ciudadSegunCodigo = "Manizales";idCiudadCliente = "17001";ciudadCliente = "Manizales";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("76520")){ciudadSegunCodigo = "Palmira";idCiudadCliente = "76520";ciudadCliente = "Palmira";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("88001")){ciudadSegunCodigo = "San Andrés";idCiudadCliente = "88001";ciudadCliente = "San Andrés";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("91001")){ciudadSegunCodigo = "Leticia";idCiudadCliente = "91001";ciudadCliente = "Leticia";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("81001")){ciudadSegunCodigo = "Arauca";idCiudadCliente = "81001";ciudadCliente = "Arauca";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("52835")){ciudadSegunCodigo = "Tumaco";idCiudadCliente = "52835";ciudadCliente = "Tumaco";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("20621")){ciudadSegunCodigo = "La Paz";idCiudadCliente = "20621";ciudadCliente = "La Paz";countrySubEntity = "Colombia";}
        }




        try{
            System.out.println("CIUDAD CLIENTE: " + tmpMaster.getENC_CLICIUDAD());
            idCiudadCliente = tmpMaster.getENC_CLICIUDAD().replaceAll("%20","").replaceAll("null","");
            if(idCiudadCliente.length() == 0){
                idCiudadCliente = tmpMaster.getENC_CIUDAD_SEDE();
                ciudadCliente = ciudadSegunCodigo;
            }else{

                if(idCiudadCliente.length()==4){
                    idCiudadCliente = "0"+idCiudadCliente;
                }
                ciudadCliente = billDAO.getCiudad(idCiudadCliente);
                String subentityCode = idCiudadCliente.substring(0,2);
            }
        }catch(Exception e){
            idCiudadCliente = tmpMaster.getENC_CIUDAD_SEDE();
            if(idCiudadCliente.length()==4){
                idCiudadCliente = "0"+idCiudadCliente;
            }
            ciudadCliente = ciudadSegunCodigo;
        }

        System.out.println("CIUDAD CLIENTE: " + idCiudadCliente);

        int profileExecutionID = 2;
        String pref = "SETT";
        String pruebas = "https://apivp.efacturacadena.com/staging/vp/documentos/proceso/sincrono";
        String produccion = "https://apivp.efacturacadena.com/v1/vp/documentos/proceso/sincrono";
        String urlConsumption = pruebas;



        if(env == 1){
            System.out.println("ENV");
            profileExecutionID = 1;
            pref = tmpMaster.getENC_PREFIJO();
            urlConsumption = produccion;
        }

        Double SubTotal = tmpMaster.getENC_SUBTOTAL();
        Double discountvalue = tmpMaster.getENC_DESCUENTO();
        Double subtotDiv = new Double(1);
        if(!SubTotal.toString().equals("0")){
            subtotDiv = SubTotal;

            System.out.println("812");
        }
        Double discountPercentage = new Double((discountvalue * 100)/subtotDiv);

        if(Double.isNaN(discountPercentage)){
            discountPercentage = new Double(0);
        }

        String fechaPago = "";

        if(tmpMaster.getENC_FECPAGO()!=null && tmpMaster.getENC_FECPAGO()!="") {
            fechaPago = tmpMaster.getENC_FECPAGO();
        }else{
            fechaPago = tmpMaster.getENC_FECEMIC();
        }

        String obs = "";

        try {
            obs = tmpMaster.getENC_OBSERVACIONES().replaceAll("No\\.","#").replaceAll("NO\\.","#");
        }catch(Exception e){
            obs = "";
        }


        String cli_dir = "";

        try {
            cli_dir = tmpMaster.getENC_CLIDIR().replaceAll("No\\.","#").replaceAll("NO\\.","#");
        }catch(Exception e){
            cli_dir = "";
        }



        System.out.println("849");
        String cli_email = "";
        String[] obss;
        try{
            obss = obs.split("<<");
            if(obss[1].contains("@")){
                cli_email = obss[1];
            }else{
                cli_email = tmpMaster.getENC_CLIEMAIL();
            }
        }catch(Exception e){
            cli_email = tmpMaster.getENC_CLIEMAIL();
        }

        try {
            cli_email = cli_email.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#");
        }catch(Exception e){
            cli_email = "";
        }


        String notaac = "";
        String orderReference = "";


        int indexRef1 = obs.indexOf("[");
        if(indexRef1==-1){
            orderReference = "";
        }else{
            int indexRef2 = obs.lastIndexOf("[");
            orderReference = obs.substring(indexRef1+1,indexRef2);
        }

        int indexNote1 = obs.indexOf("_");
        if(indexNote1==-1){
            notaac = "";
        }else{
            int indexNote2 = obs.lastIndexOf("_");
            notaac = obs.substring(indexNote1,indexNote2);
        }


        String countrySubEntityCode = "CO";
        if(tmpMaster.getENC_CLITIPOID().toString().equals("42")||tmpMaster.getENC_CLITIPOID().toString().equals("41")||tmpMaster.getENC_CLITIPOID().toString().equals("50")){
            System.out.println("International");
            try {
                InternationalData data = billDAO.getInternationalData(tmpMaster.getENC_CLICIUDAD());
                String [] listing = tmpMaster.getENC_CLINUMID().split("-");
                tmpMaster.setENC_CLINUMID(listing[0]);
                idCiudadCliente = "01000";

                if(tmpMaster.getENC_CLICIUDAD().length()==1){
                    idCiudadCliente = "0100"+tmpMaster.getENC_CLICIUDAD();
                }
                if(tmpMaster.getENC_CLICIUDAD().length()==2){
                    idCiudadCliente = "010"+tmpMaster.getENC_CLICIUDAD();
                }
                if(tmpMaster.getENC_CLICIUDAD().length()==3){
                    idCiudadCliente = "01"+tmpMaster.getENC_CLICIUDAD();
                }
                if(tmpMaster.getENC_CLICIUDAD().length()==4){
                    idCiudadCliente = "0"+tmpMaster.getENC_CLICIUDAD();
                }
                if(tmpMaster.getENC_CLICIUDAD().length()==5){
                    idCiudadCliente = tmpMaster.getENC_CLICIUDAD();
                }

                int ciudadClientePos1 = tmpMaster.getENC_CLIDIR().indexOf("{");
                int ciudadClientePos2 = tmpMaster.getENC_CLIDIR().indexOf("}");
                ciudadCliente = tmpMaster.getENC_CLIDIR().substring(ciudadClientePos1+1,ciudadClientePos2);
                countrySubEntity = data.getNOMBRE_PAIS();
                countrySubEntityCode = data.getPREFIJO_DIAN();

            }catch(Exception e){
                e.printStackTrace();
                billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NO SE ENCONTRARON DATOS . SYSWARE.");
            }



        }


        //getCliEmail


        SaludPaisData saludElement = listaSalud.get(0);


        String invoiceRawXmlHead1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<Invoice xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2\" xmlns:cac=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:cbc=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ext=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:sts=\"dian:gov:co:facturaelectronica:Structures-2-1\" xmlns:xades141=\"http://uri.etsi.org/01903/v1.4.1#\">\n" +
                "\t<ext:UBLExtensions>\n" +
                "\t\t<ext:UBLExtension>\n" +
                "\t\t\t<!--Información adicional específica para el sector salud [1..N]-->\n" +
                "\t\t\t<ext:ExtensionContent>\n" +
                "\t\t\t\t<!--Grupo que contiene la información personalizada del sector[1..1]-->\n" +
                "\t\t\t\t<CustomTagGeneral>\n" +
                "\t\t\t\t\t<!--Grupo de información personalizable dependiendo el sector [1..1]-->\n" +
                "\t\t\t\t\t<Name>Responsable</Name>\n" +
                "\t\t\t\t\t<!--Responsable [1..1]-->\n" +
                "\t\t\t\t\t<Value>www.minSalud.gov.co</Value>\n" +
                "\t\t\t\t\t<!--Valor fijo www.minSalud.gov.co[1..1]-->\n" +
                "\t\t\t\t\t<Name>Tipo, identificador:año del acto administrativo</Name>\n" +
                "\t\t\t\t\t<!--Tipo, identificador:año del acto administrativo[1..1]-->\n" +
                "\t\t\t\t\t<Value>Resolución 001000:2020</Value>\n" +
                "\t\t\t\t\t<!--Resolución 001000:2020[1..1]-->\n" +
                "\t\t\t\t\t<Interoperabilidad>\n" +
                "\t\t\t\t\t\t<!-- [1..1] -->\n" +
                "\t\t\t\t\t\t<Group schemeName=\"Sector Salud\">\n" +
                "\t\t\t\t\t\t\t<!-- Fragmento de sector que se reporta [1..1]-->\n" +
                "\t\t\t\t\t\t\t<Collection schemeName=\""+saludElement.getIN_ENC_PRIMER_NOMBRE()+" "+saludElement.getIN_ENC_SEGUNDO_NOMBRE()+" "+saludElement.getIN_ENC_PRIMER_APELLIDO()+" "+saludElement.getIN_ENC_SEGUNDO_APELLIDO()+"\">\n" +
                "\t\t\t\t\t\t\t\t<!-- Detalles individuales de lainformación de un miembro [1..N] -->\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>CODIGO_PRESTADOR</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “CODIGO_PRESTADOR” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_COD_PRESTADOR()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [0..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>TIPO_DOCUMENTO_IDENTIFICACION</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “TIPO_DOCUMENTO_IDENTIFICACION” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value schemeID=\"Carné Diplomático\"\n" +
                "\t\t\t\t\t\t\t\t\t       schemeName=\"salud_identificación.gc\">"+saludElement.getIN_ENC_TIPODOC_USUARIO()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Tipo de documento de identificación del usuario del numeral (18.4.1)@schemeID: Abreviacion de #18.4.1 @schemeName: Debe ser informado el Literal salud_identificación.gc [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>NUMERO_DOCUMENTO_IDENTIFICACION</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “NUMERO_DOCUMENTO_IDENTIFICACION” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_NUM_IDENTIFICACION()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!--  [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>PRIMER_APELLIDO</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “PRIMER_APELLIDO” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_PRIMER_APELLIDO()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>SEGUNDO_APELLIDO</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “SEGUNDO_APELLIDO” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_SEGUNDO_APELLIDO()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [0..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>PRIMER_NOMBRE</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “PRIMER_NOMBRE” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_PRIMER_NOMBRE()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>SEGUNDO_NOMBRE</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “SEGUNDO_NOMBRE” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_SEGUNDO_NOMBRE()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [0..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>TIPO_USUARIO</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “TIPO_USUARIO” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value schemeID=\"01\"\n" +
                "\t\t\t\t\t\t\t\t\t       schemeName=\"salud_tipo_usuario.gc\">"+saludElement.getIN_ENC_TIPO_USUARIO()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Tipo de Usuario numeral (18.4.2)@schemeID: Código de #18.4.2 @schemeName: Debe ser informado el Literal salud_tipo_usuario.gc [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>MODALIDAD_CONTRATACION</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal“MODALIDAD_CONTRATACION”[1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value schemeID=\"06\"\n" +
                "\t\t\t\t\t\t\t\t\t       schemeName=\"salud_modalidad_pago.gc\">"+saludElement.getIN_ENC_MODALIDAD_CONTRATO()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Modalidades de contratación y de pago numeral(18.4.3)@schemeID: Código de #18.4.3 @schemeName: Debe ser informado el Literal salud_modalidad_pago.gc [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>COBERTURA_PLAN_BENEFICIOS</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “COBERTURA_PLAN_BENEFICIOS” 1..1 -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value schemeID=\"10\"\n" +
                "\t\t\t\t\t\t\t\t\t       schemeName=\"salud_cobertuta.gc\">"+saludElement.getIN_ENC_COBERTURA()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- valores posibles en la columna cobertura (18.4.4)@schemeID: Código de #18.4.3 @schemeName: Debe ser informado el Literal salud_modalidad_pago.gc [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>NUMERO_AUTORIZACION</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “NUMERO_AUTORIZACIÓN” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_NUM_AUTORIZACION()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [0..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>NUMERO_MIPRES</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “NUMERO_MIPRES” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_NUM_MIPRES()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [0..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>NUMERO_ENTREGA_MIPRES</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “NUMERO_ENTREGA_MIPRES” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_NUM_ENTREGA_MIPRES()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [0..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>NUMERO_CONTRATO</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “NUMERO_CONTRATO” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_NUM_CONTRATO()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [0..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>NUMERO_POLIZA</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “NUMERO_POLIZA” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_POLIZA()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [0..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>COPAGO</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “COPAGO” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_COPAGO()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [0..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>CUOTA_MODERADORA</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “CUOTA_MODERADORA” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_CUOTA_MODERADORA()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [0..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>CUOTA_RECUPERACION</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “CUOTA_RECUPERACION” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_CUOTA_RECUPERACION()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [0..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t<AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t\t\t<Name>PAGOS_COMPARTIDOS</Name>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- Debe ser informado el literal “PAGOS_COMPARTIDOS” [1..1] -->\n" +
                "\t\t\t\t\t\t\t\t\t<Value>"+saludElement.getIN_ENC_PAGOS_COMPARTIDOS()+"</Value>\n" +
                "\t\t\t\t\t\t\t\t\t<!-- [0..1] -->\n" +
                "\t\t\t\t\t\t\t\t</AdditionalInformation>\n" +
                "\t\t\t\t\t\t\t</Collection>\n" +
                "\t\t\t\t\t\t</Group>\n" +
               /* "\t\t\t\t\t\t<InteroperabilidadPT>\n" +
                "\t\t\t\t\t\t\t<!-- Grupo de información complementaria a la transacción OPCIONAL [0..1] -->\n" +
                "\t\t\t\t\t\t\t<URLDescargaAdjuntos>\n" +
                "\t\t\t\t\t\t\t\t<URL>www.ips-1.com.co</URL>\n" +
                "\t\t\t\t\t\t\t\t<ParametrosArgumentos>\n" +
                "\t\t\t\t\t\t\t\t\t<ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Name>excelFile</Name>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Value>a1b2c3.xlsx</Value>\n" +
                "\t\t\t\t\t\t\t\t\t</ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t<ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Name>txtFile</Name>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Value>a1b2c3.txt</Value>\n" +
                "\t\t\t\t\t\t\t\t\t</ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t</ParametrosArgumentos>\n" +
                "\t\t\t\t\t\t\t</URLDescargaAdjuntos>\n" +
                "\t\t\t\t\t\t\t<EntregaDocumento>\n" +
                "\t\t\t\t\t\t\t\t<WS>https://ws4erp.ips-987.com.co/WcfRecibiendoDocs4ERP.svc?wsdl</WS>\n" +
                "\t\t\t\t\t\t\t\t<ParametrosArgumentos>\n" +
                "\t\t\t\t\t\t\t\t\t<ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Name>Método-1</Name>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Value>ClienteEntregaAcuseDeReciboDeFEV-VP</Value>\n" +
                "\t\t\t\t\t\t\t\t\t</ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t<ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Name>Método-2</Name>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Value>ClienteEntregaConstanciaDeMercanciaEntregada</Value>\n" +
                "\t\t\t\t\t\t\t\t\t</ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t<ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Name>Método-3</Name>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Value>ClienteEntregaRechazoDeFEVVP</Value>\n" +
                "\t\t\t\t\t\t\t\t\t</ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t<ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Name>Método-4</Name>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Value>ClienteEntregaAceptacionDeFEVVP</Value>\n" +
                "\t\t\t\t\t\t\t\t\t</ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t<ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Name>Método-5</Name>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Value>ClienteEntregaReclamosDeFEVVP</Value>\n" +
                "\t\t\t\t\t\t\t\t\t</ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t<ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Name>Contraseña OTP</Name>\n" +
                "\t\t\t\t\t\t\t\t\t\t<Value>Solicitela y recíbala al teléfono móvil 3123456789 por Google Autenticador</Value>\n" +
                "\t\t\t\t\t\t\t\t\t</ParametroArgumento>\n" +
                "\t\t\t\t\t\t\t\t</ParametrosArgumentos>\n" +
                "\t\t\t\t\t\t\t</EntregaDocumento>\n" +
                "\t\t\t\t\t\t</InteroperabilidadPT>\n" + */
                "\t\t\t\t\t</Interoperabilidad>\n" +
                "\t\t\t\t</CustomTagGeneral>\n" +
                "\t\t\t</ext:ExtensionContent>\n" +
                "\t\t</ext:UBLExtension>\n" +
                "\t</ext:UBLExtensions>\n" +
                "\t<cbc:CustomizationID schemeID=\"SS-CUFE\">SS-Recaudo</cbc:CustomizationID>\n" +
                "\t<cbc:ProfileExecutionID>"+profileExecutionID+"</cbc:ProfileExecutionID>\n" +
                "\t<cbc:ID>"+pref+tmpMaster.getENC_CONSDIAN()+"</cbc:ID>\n" +
                "\t<cbc:IssueDate>"+tmpMaster.getENC_FECEMIC()+"</cbc:IssueDate>\n" +
                "\t<cbc:IssueTime>00:00:00-05:00</cbc:IssueTime>\n" +
                "\t<cbc:DueDate>"+fechaPago+"</cbc:DueDate>\n" +
                "\t<cbc:InvoiceTypeCode>01</cbc:InvoiceTypeCode>\n" +
                "\t<cbc:Note>"+obs.replaceAll("<","").replaceAll(">","")+"</cbc:Note>\n" +
                "\t<cbc:DocumentCurrencyCode>COP</cbc:DocumentCurrencyCode>\n" +
                "\t<cbc:LineCountNumeric>"+tmpMaster.getENC_NRO_ITEMES()+"</cbc:LineCountNumeric>\n" +
                "<cac:InvoicePeriod>\n" +
                "\t\t<!--Grupo de campos relativos al Periodo de Facturación[1..1]-->\n" +
                "\t\t<cbc:StartDate>"+saludElement.getIN_ENC_FECHA_INICIO().split(" ")[0]+"</cbc:StartDate>\n" +
                "\t\t<!-- [1..1] -->\n" +
                "\t\t<cbc:StartTime>23:59:59-05:00</cbc:StartTime>\n" +
                "\t\t<!-- [0..1] -->\n" +
                "\t\t<cbc:EndDate>"+saludElement.getIN_ENC_FECHA_FIN().split(" ")[0]+"</cbc:EndDate>\n" +
                "\t\t<!-- [1..1] -->\n" +
                "\t\t<cbc:EndTime>23:59:59-05:00</cbc:EndTime>\n" +
                "\t\t<!-- [0..1] -->\n" +
                "\t</cac:InvoicePeriod>" +
                "\t<cac:OrderReference>\n" +
                "\t\t<cbc:ID>"+orderReference+"</cbc:ID>\n" +
                "<cbc:IssueDate>2022-02-17</cbc:IssueDate>" +
                "\t</cac:OrderReference>\n" +
                "\t<cac:AccountingSupplierParty>\n" +
                "\t\t<cbc:AdditionalAccountID>1</cbc:AdditionalAccountID>\n" +
                "\t\t<cac:Party>\n" +
                "\t\t\t<cac:PartyName>\n" +
                "\t\t\t\t<cbc:Name>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:Name>\n" +
                "\t\t\t</cac:PartyName>\n" +
                "\t\t\t<cac:PhysicalLocation>\n" +
                "\t\t\t\t<cac:Address>\n" +
                "\t\t\t\t\t<cbc:ID>"+tmpMaster.getENC_CIUDAD_SEDE()+"</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:CityName>"+ciudadSegunCodigo+"</cbc:CityName>\n" +
                "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                "\t\t\t\t\t<cbc:CountrySubentity>"+countrySubEntity+"</cbc:CountrySubentity>\n" +
                "\t\t\t\t\t<cbc:CountrySubentityCode>"+tmpMaster.getENC_CIUDAD_SEDE().substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                "\t\t\t\t\t<cac:AddressLine>\n" +
                "\t\t\t\t\t\t<cbc:Line>"+tmpMaster.getENC_DIR_SEDE().replaceAll("No\\.","#").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                "\t\t\t\t\t</cac:AddressLine>\n" +
                "\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                "\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t</cac:Address>\n" +
                "\t\t\t</cac:PhysicalLocation>\n" +
                "\t\t\t<cac:PartyTaxScheme>\n" +
                "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                "\t\t\t\t<cbc:TaxLevelCode listName=\"No Aplica\">O-13;O-23</cbc:TaxLevelCode>\n" +
                "\t\t\t\t<cac:RegistrationAddress>\n" +
                "\t\t\t\t\t<cbc:ID>11001</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:CityName>Bogotá</cbc:CityName>\n" +
                "\t\t\t\t\t<cbc:PostalZone>000000</cbc:PostalZone>\n" +
                "\t\t\t\t\t<cbc:CountrySubentity>Bogotá</cbc:CountrySubentity>\n" +
                "\t\t\t\t\t<cbc:CountrySubentityCode>11</cbc:CountrySubentityCode>\n" +
                "\t\t\t\t\t<cac:AddressLine>\n" +
                "\t\t\t\t\t\t<cbc:Line>CR 45   26   85 OF 481 ED URIEL GUTIERREZ</cbc:Line>\n" +
                "\t\t\t\t\t</cac:AddressLine>\n" +
                "\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                "\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t</cac:RegistrationAddress>\n" +
                "\t\t\t\t<cac:TaxScheme>\n" +
                "\t\t\t\t\t<cbc:ID></cbc:ID>\n" +
                "\t\t\t\t\t<cbc:Name></cbc:Name>\n" +
                "\t\t\t\t</cac:TaxScheme>\n" +
                "\t\t\t</cac:PartyTaxScheme>\n" +
                "\t\t\t<cac:PartyLegalEntity>\n" +
                "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                "\t\t\t\t\t<cbc:ID>"+pref+"</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:Name>12345</cbc:Name>\n" +
                "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                "\t\t\t</cac:PartyLegalEntity>\n" +
                "\t\t\t<cac:Contact>\n" +
                "\t\t\t\t<cbc:Name></cbc:Name>\n" +
                "\t\t\t\t<cbc:Telephone>3165000</cbc:Telephone>\n" +
                "\t\t\t\t<cbc:ElectronicMail>efactura_nal@unal.edu.co</cbc:ElectronicMail>\n" +
                "\t\t\t</cac:Contact>\n" +
                "\t\t\t<cac:Person>\n" +
                "\t\t\t\t<!-- identificador del usuario beneficiario del servicio de salud [0..1]-->\n" +
                "\t\t\t\t<cbc:FirstName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:FirstName>\n" +
                "\t\t\t</cac:Person>" +
                "\t\t</cac:Party>\n" +
                "\t</cac:AccountingSupplierParty>\n" +
                "\t<cac:AccountingCustomerParty>\n"+
                "\t\t<cbc:AdditionalAccountID>"+tmpMaster.getENC_CLINATU()+"</cbc:AdditionalAccountID>\n"+
                "\t\t<cac:Party>\t\n" +
                "\t\t\t<cac:PartyIdentification>\t\n" +
                "\t\t\t\t<cbc:ID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\"31\">"+tmpMaster.getENC_CLINUMID()+"</cbc:ID>\n" +
                "\t\t\t</cac:PartyIdentification>\n" +
                "\t\t\t<cac:PartyName>\n" +
                "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("_","Y").replaceAll("<","").replaceAll(">","")+"</cbc:Name>\n" +
                "\t\t\t</cac:PartyName>\n" +
                "\t\t\t<cac:PhysicalLocation>\n" +
                "\t\t\t\t<cac:Address>\n" +
                "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadCliente+"</cbc:CountrySubentity>\n" +
                "\t\t\t\t\t<cbc:CountrySubentityCode>"+idCiudadCliente.substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                "\t\t\t\t\t<cac:AddressLine>\n" +
                "\t\t\t\t\t\t<cbc:Line>"+cli_dir.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                "\t\t\t\t\t</cac:AddressLine>\n" +
                "\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t<cbc:IdentificationCode>"+countrySubEntityCode+"</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t<cbc:Name languageID=\"es\">"+countrySubEntity+"</cbc:Name>\n" +
                "\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t</cac:Address>\n" +
                "\t\t\t</cac:PhysicalLocation>\n" +
                "\t\t\t<cac:PartyTaxScheme>\n" +
                "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:RegistrationName>\n";



        String paymentID = "Efectivo";

        if(tmpMaster.getENC_MEDIOPAGO().toString().equals("48")){
            paymentID = "Tarjeta Crédito";
        }

        if(tmpMaster.getENC_MEDIOPAGO().toString().equals("49")){
            paymentID = "Tarjeta Débito";
        }

        if(tmpMaster.getENC_MEDIOPAGO().toString().equals("47")){
            paymentID = "Transferencia Débito Bancaria";
        }


        String cliRespFiscal = tmpMaster.getENC_CLIRESPFISCAL();

        try{
            if(tmpMaster.getENC_CLIRESPFISCAL().equals("0") || tmpMaster.getENC_CLIRESPFISCAL() == null || tmpMaster.getENC_CLIRESPFISCAL().equals("null")||tmpMaster.getENC_CLIRESPFISCAL().equals("")){
                cliRespFiscal = "R-99-PN";
            }
        }catch(Exception e){
            cliRespFiscal = "R-99-PN";
        }


        String TaxLevelCode = "No Aplica";

                    /*try{
                        if(tmpMaster.getENC_CLIRESPIVA()== new Long(0)){
                            TaxLevelCode = "49";
                        }else{
                            TaxLevelCode = tmpMaster.getENC_CLIRESPIVA().toString();
                        }
                    }catch(Exception e){
                        TaxLevelCode = "49";
                    }
                    */
        String digitoVerificacion = "";
        String invoiceRawXmlHead = "";

        Boolean doesHttp = true;
        Boolean doesHttp2 = false;

        System.out.println("THIS IS CLINATU: "+tmpMaster.getENC_CLINATU());
        String schemeName = tmpMaster.getENC_CLITIPOID();
        if(schemeName.toString().equals("42")){
            System.out.println("International");
            digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
        }else{
            System.out.println("national");
            if(tmpMaster.getENC_CLINATU().toString().equals("1") && tmpMaster.getENC_CLITIPOID().toString().equals("31")) {
                System.out.println("IM JURIDIC");
                try{
                    String number =  tmpMaster.getENC_CLINUMID().split("-")[0];
                    String digit = tmpMaster.getENC_CLINUMID().split("-")[1];
                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\""+digit+"\"  schemeName=\""+schemeName+"\">"+number+"</cbc:CompanyID>\n";
                }catch(Exception  e){
                    billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NO SE ENCONTRO DIGITO DE VERIFICACION EN EL DOCUMENTO. SYSWARE.");
                    doesHttp = false;
                }
            }else{
                System.out.println("IM NATURAL");
                digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
            }
        }



        String invoiceRawXmlHead12 =  "\t\t\t\t<cbc:TaxLevelCode listName=\""+TaxLevelCode+"\">"+cliRespFiscal+"</cbc:TaxLevelCode>\n" +
                "\t\t\t\t<cac:RegistrationAddress>\n" +
                "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadCliente+"</cbc:CountrySubentity>\n" +
                "\t\t\t\t\t<cbc:CountrySubentityCode>"+idCiudadCliente.substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                "\t\t\t\t\t<cac:AddressLine>\n" +
                "\t\t\t\t\t\t<cbc:Line>"+cli_dir.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                "\t\t\t\t\t</cac:AddressLine>\n" +
                "\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t<cbc:IdentificationCode>"+countrySubEntityCode+"</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t<cbc:Name languageID=\"es\">"+countrySubEntity+"</cbc:Name>\n" +
                "\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t</cac:RegistrationAddress>\n" +
                "\t\t\t\t<cac:TaxScheme>\n" +
                "\t\t\t\t\t<cbc:ID>01</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:Name>IVA</cbc:Name>\n" +
                "\t\t\t\t</cac:TaxScheme>\n" +
                "\t\t\t</cac:PartyTaxScheme>\n" +
                "\t\t\t<cac:PartyLegalEntity>\n" +
                "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:RegistrationName>\n"+
                digitoVerificacion +
                "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                "\t\t\t\t\t<cbc:Name>11111</cbc:Name>\n" +
                "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                "\t\t\t</cac:PartyLegalEntity>\n" +
                "\t\t\t<cac:Contact>\n" +
                "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:Name>\n" +
                "\t\t\t\t<cbc:Telephone>"+tmpMaster.getENC_CLITELELEF()+"</cbc:Telephone>\n" +
                "\t\t\t\t<cbc:ElectronicMail>"+cli_email+"</cbc:ElectronicMail>\n" +
                "\t\t\t</cac:Contact>\n" +
                "\t\t\t<cac:Person>\n"+
                "<!-- Datos del Usuario del servicio de salud en //cac:Person [1..1]-->\n" +
                "\t\t\t\t<cbc:ID schemeID=\"13\">"+tmpMaster.getENC_CLINUMID()+"</cbc:ID>\n" +   //ACA NO SE
                "\t\t\t\t<!-- identificador del usuario beneficiario del servicio de salud [0..1]-->\n" +
                "\t\t\t\t<cbc:FirstName>"+saludElement.getIN_ENC_PRIMER_NOMBRE()+"</cbc:FirstName>\n" +
                "\t\t\t\t<!-- Nombres del Usuario beneficiario del servicio de salud [0..1]-->\n" +
                "\t\t\t\t<cbc:FamilyName>"+saludElement.getIN_ENC_PRIMER_APELLIDO()+"</cbc:FamilyName>\n" +
                "\t\t\t\t<!-- Apellidos del Usuario beneficiario del servicio de salud [0..1] -->\n" +
                "\t\t\t\t<cac:IdentityDocumentReference>\n" +
                "\t\t\t\t\t<!-- identificador del usuario benficiario del servicio de salud [0..1]-->\n" +
                "\t\t\t\t\t<cbc:ID schemeName=\"_nombre_del_documento_lista_minSalud_\">"+tmpMaster.getENC_CLINUMID()+"</cbc:ID>\n" +   //ACA NO SE
                "\t\t\t\t\t<!-- identificador del usuario[0..1]-->\n" +
                "\t\t\t\t\t<cac:IssuerParty>\n" +
                "\t\t\t\t\t\t<!-- parte expedidora del documento del usuario [1..1]-->\n" +
                "\t\t\t\t\t\t<cac:PartyName>\n" +
                "\t\t\t\t\t\t\t<cbc:Name>Registraduria del estado civil</cbc:Name>\n" +
                "\t\t\t\t\t\t\t<!-- nombre de la entidad expedidora del documento[0..1]-->\n" +
                "\t\t\t\t\t\t</cac:PartyName>\n" +
                "\t\t\t\t\t\t<cac:PostalAddress>\n" +
                "\t\t\t\t\t\t\t<!-- código postal de la ciudad de expedidora del documento [0..1]-->\n" +
                "\t\t\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                "\t\t\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t\t\t\t<!-- Nombre del país de la entidad expedidora del documento [1..1] -->\n" +
                "\t\t\t\t\t\t</cac:PostalAddress>\n" +
                "\t\t\t\t\t</cac:IssuerParty>\n" +
                "\t\t\t\t</cac:IdentityDocumentReference>\n"+
                "\t\t\t</cac:Person>\n"+
                "\t\t</cac:Party>\n" +
                "\t</cac:AccountingCustomerParty>\n" +
                "<cac:BuyerCustomerParty>\n" +
                "\t\t<!-- Grupo nuevo de información [0..1]-->\n" +
                "\t\t<cbc:AdditionalAccountID>111222333444</cbc:AdditionalAccountID>\n" +
                "\t\t<!-- Identificador del usuario beneficiario del servicio de Salud [1..1] -->\n" +
                "\t</cac:BuyerCustomerParty>" +
                "\t<cac:PaymentMeans>\n" +
                "\t\t<cbc:ID>"+tmpMaster.getENC_FORMAPAGO()+"</cbc:ID>\n" +
                "\t\t<cbc:PaymentMeansCode>"+tmpMaster.getENC_MEDIOPAGO()+"</cbc:PaymentMeansCode>\n" +
                "\t\t<cbc:PaymentDueDate>"+fechaPago+"</cbc:PaymentDueDate>\n" +
                "\t\t<cbc:PaymentID>"+paymentID+"</cbc:PaymentID>\n" +
                "\t</cac:PaymentMeans>\n" +
                "\t<cac:AllowanceCharge>\n" +
                "\t\t<cbc:ID>1</cbc:ID>\n" +
                "\t\t<cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                "\t\t<cbc:AllowanceChargeReasonCode>01</cbc:AllowanceChargeReasonCode>\n" +
                "\t\t<cbc:AllowanceChargeReason>Descuento</cbc:AllowanceChargeReason>\n" +
                "\t\t<cbc:MultiplierFactorNumeric>"+discountPercentage+"</cbc:MultiplierFactorNumeric>\n" +
                "\t\t<cbc:Amount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_DESCUENTO()).replaceAll(",",".")+"</cbc:Amount>\n" +
                "\t\t<cbc:BaseAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:BaseAmount>\n" +
                "\t</cac:AllowanceCharge>\n" +
                "\t<cac:LegalMonetaryTotal>\n" +
                "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:LineExtensionAmount><!-- Valor bruto del documento-->\n" +
                "\t\t<cbc:TaxExclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxExclusiveAmount><!-- Base imponilbe del documento-->\n" +
                "\t\t<cbc:TaxInclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxInclusiveAmount><!-- Base imponible + impuestos-->\n" +
                "\t\t<cbc:AllowanceTotalAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_DESCUENTO()).replaceAll(",",".")+"</cbc:AllowanceTotalAmount>\n" +
                //"\t\t<!-- <cbc:ChargeTotalAmount currencyID=\"COP\">"+ChargeTotalAmount+"<cbc:ChargeTotalAmount> Aplica cuando es cargo a nivel general de la factura, suma de los cargos generales-->\n" +
                "\t\t<cbc:PayableAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_TOTAL()).replaceAll(",",".")+"</cbc:PayableAmount><!--Valor a Pagar de Factura: Valor total de ítems (incluyendo cargos y descuentos a nivel de ítems)+valor tributos + valor cargos – valor descuentos – valor anticipos -->\n" +
                "\t</cac:LegalMonetaryTotal>";



        System.out.println("THIS IS CLINATU: "+tmpMaster.getENC_CLINATU());

        if(schemeName.toString().equals("42")){
            System.out.println("International");
            digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
            invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
        }else{
            System.out.println("national");
            if(tmpMaster.getENC_CLINATU().toString().equals("1") && tmpMaster.getENC_CLITIPOID().toString().equals("31")){
                System.out.println("IM JURIDIC");
                try{
                    String number =  tmpMaster.getENC_CLINUMID().split("-")[0];
                    String digit = tmpMaster.getENC_CLINUMID().split("-")[1];
                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\""+digit+"\"  schemeName=\""+schemeName+"\">"+number+"</cbc:CompanyID>\n";
                    invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
                }catch(Exception  e){
                    billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NO SE ENCONTRO DIGITO DE VERIFICACION EN EL DOCUMENTO. SYSWARE.");
                    doesHttp = false;
                }
            }else{
                System.out.println("IM NATURAL");
                digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
                invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
            }
        }


        if(tmpMaster.getENC_CLINUMID().equals(" ")){
            billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NUMERO DE IDENTIFICACION DE CLIENTE INVALIDO.");
            doesHttp = false; }




        List<Detail> details = billDAO.getDetails(tmpMaster.getENC_PREFIJO(),
                tmpMaster.getENC_NRORECIBO(),
                tmpMaster.getENC_SEDE(),
                tmpMaster.getENC_VIGENCIA(),
                tmpMaster.getENC_CIAQUIPU(),
                tmpMaster.getENC_NUM_DOCUHIJO());


        String detailXmlInvoiceLine = "";

        for(int j = 0; j<details.size();j++){

            Detail myDetail = details.get(j);

            System.out.println("BANDERA: "+myDetail.getDET_CODIGO());
            JSONObject obj1 = new JSONObject();
            obj1.put("id", (j+1));
            try{
                obj1.put("note", myDetail.getDET_DESCRIP().replaceAll("^(?i)null$", "%20").replaceAll("null", "%20"));
            }catch(Exception e){
                obj1.put("note", " ");
            }
            obj1.put("quantity", myDetail.getDET_CANTIDAD());
            obj1.put("amount", (new Double(myDetail.getDET_CANTIDAD())  * myDetail.getDET_VLRUNIT()));
            obj1.put("taxAmount", 0.0);
            obj1.put("taxPercent", 0.0);
            obj1.put("taxId", 01);
            obj1.put("taxName", "IVA");
            obj1.put("description", myDetail.getDET_NOMBRE());
            try{
                obj1.put("itemId", myDetail.getDET_CODIGO().replaceAll("^(?i)null$", "%20").replaceAll("null", "%20"));
            }catch(Exception e){
                obj1.put("itemId", " ");
            }
            obj1.put("individualPrice", myDetail.getDET_VLRUNIT());
            obj1.put("baseQuantity", 1.00);

            System.out.println("itemId: "+obj1.get("itemId").toString());

            System.out.println("QUANTITY: "+obj1.get("quantity"));
            System.out.println("QUANTITY2: "+obj1.get("quantity").toString());
            System.out.println("QUANTITY3: "+new Double(obj1.get("quantity").toString()));

            detailXmlInvoiceLine +=  generateInvoiceLines(new Integer(obj1.get("id").toString()),
                    obj1.get("note").toString().replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                    myDetail.getDET_CANTIDAD(),
                    new Double(obj1.get("amount").toString()),
                    new Double(obj1.get("taxAmount").toString()),
                    new Double(obj1.get("taxPercent").toString()),
                    new Integer(obj1.get("taxId").toString()),
                    obj1.get("taxName").toString(),
                    obj1.get("description").toString().replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                    obj1.get("itemId").toString(),
                    new Double(obj1.get("individualPrice").toString()),
                    new Double(obj1.get("baseQuantity").toString()));

        }

        String datPago = "";

        try{
            datPago = tmpMaster.getENC_DATOS_PAGO().replaceAll("%20","").replaceAll("^(?i)null$", "");
        }catch(Exception e){
            datPago = "";
        }

        String ResolucionOpcional4="Resolución de habilitación de facturación electrónica No. 18764008055116 de fecha 25/11/2020.";

        if(pref.equals("BOG")){
            ResolucionOpcional4="Resolución nueva";
        }

        if(pref.equals("SIA")){
            ResolucionOpcional4="Número Autorización: 18764029138790 Rango Autorizado Desde: SIA300001 Rango Autorizado Hasta: SIA500000 Vigencia: 2023-10-20\n" +
                    "Representación Gráfica del documento electrónico según resolución DIAN 042 del 05 de mayo del 2020.";
        }

        String invoiceRawXmlFooter = "<DATA>\n" +
                "\t\t<UBL21>true</UBL21>\n" +
                "\t\t<Development>\n" +
                "\t\t\t<ShortName>UNAL</ShortName>\n" +
                "\t\t</Development>\n" +
                "\t\t <Filters>\n" +
                "\t\t\t<Product ></Product><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                "\t\t\t<SubProduct></SubProduct><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                "\t\t\t<CUENTA/>\n" +
                "\t\t\t<NATURALEZA/>\n" +
                "\t\t\t<CENTRODECOSTO/>\n" +
                "\t\t\t<CLASE/>\n" +
                "\t\t\t<INDICE/>\n" +
                "\t\t</Filters>\n" +
                "\t\t<Opcional1> Somos grandes contribuyentes, no responsables de IVA </Opcional1> <!-- ID 04 Representación Gráfica -->\n" +
                "\t\t<Opcional2>  Agentes de retención de Impuesto sobre las ventas, no sujetos al impuesto de industria y comercio </Opcional2> <!-- ID 05 Representación Gráfica -->\n" +
                "\t\t<Opcional3> "+tmpMaster.getENC_CIAQUIPU()+" - "+tmpMaster.getENC_NRORECIBO()+"</Opcional3> <!-- ID 10 Representación Gráfica -->\n" +
                "\t\t<Opcional4>"+datPago.replaceAll("<","").replaceAll(">","")+
                "#Somos Institución de Educación Superior no contribuyente del impuesto de renta,  favor abstenerse de efectuar retenciones en la  fuente. " +
                //
                "#Correo: efactura_nal@unal.edu.co </Opcional4> <!-- ID 56 Representación Gráfica -->\n" +
                "\t</DATA></Invoice>";



        //invoiceRawXmlFooter = "</Invoice>";



        String finalXML = (invoiceRawXmlHead + detailXmlInvoiceLine + invoiceRawXmlFooter).replaceAll("^(?i)null$", "%20").replaceAll("null", "%20");

        byte[] encodedBytes = Base64.encodeBase64(finalXML.getBytes());

        System.out.println("-------------------------------XML------------------------------");

        //System.out.println(finalXML);

        System.out.println("encodedBytes " + new String(encodedBytes));

        Boolean responseFinal = false;
        if(doesHttp){
            httpCall++;
            responseFinal = sendHttp(urlConsumption,env,encodedBytes,tmpMaster);
        }

        myList.add(pref+tmpMaster.getENC_CONSDIAN()+" : "+httpCounter+" : "+httpCall);


        if(responseFinal){
            return new Long(1);
        }else{
            return new Long(0);
        }
    }


    public List<Report> report(String date1,String date2,String List){
        List<String> myList = new ArrayList<String>(Arrays.asList(List.split(",")));
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        String date1S = format.format(new Date(date1));
        String date2S = format.format(new Date(date2));
        return billDAO.report(date1S, date2S, myList);

    }


    public List<Report> report2(String nrorecibo){
        return billDAO.report2(nrorecibo);

    }

    public List<Pref> prefs(){
        return billDAO.prefs();

    }



    public Long JsonConverterMethod(Long env){
        List<Master> masters = billDAO.getMaster();
        myList = new ArrayList<>();
        httpCounter = 0;
        httpCall = 0;
        System.out.println("FIRST SIZE: "+masters.size());

        System.out.println("FIRST PRINT");

        for(int i = 0; i<masters.size();i++){
            try {
                Master tmpMaster = masters.get(i);

                Long tipo = tmpMaster.getENC_TIPODOC();


                System.out.println(tmpMaster.getENC_PREFIJO() + "," + tmpMaster.getENC_OBSERVACIONES() + "," + tipo);

                if (tipo.toString().equals("91")) {
                    System.out.print("---------CREDITO----------");
                    JsonConverterMethodCredit(env, tmpMaster);
                }

                if (tipo.toString().equals("1")) {

                    List<SaludPaisData> listaDataSalud = billDAO.getSaludData(tmpMaster.getENC_PREFIJO(), tmpMaster.getENC_NRORECIBO(), tmpMaster.getENC_SEDE(), tmpMaster.getENC_VIGENCIA(), tmpMaster.getENC_CIAQUIPU(), tmpMaster.getENC_NUM_DOCUHIJO());
                    if (listaDataSalud.size() > 0) {
                        System.out.print("---------INVOICE MED----------");
                        JsonConverterMethodInvoiceMed(env, tmpMaster, listaDataSalud);
                    } else {
                        System.out.print("---------INVOICE----------");
                        JsonConverterMethodInvoice(env, tmpMaster);
                    }
                }

                if (tipo.toString().equals("92")) {
                    System.out.print("---------DEBIT----------");
                    JsonConverterMethodDebit(env, tmpMaster);
                }

                System.out.println("SECOND PRINT");
                System.out.println("i="+i+" : "+tmpMaster.getENC_CONSDIAN());
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        System.out.println("SECOND SIZE: "+myList.size());
        System.out.println("LIST: "+myList);

        return new Long(1);


    }

    public Long JsonConverterMethodCredit(Long env, Master tmpMaster){


            String ciudadSegunCodigo = "Bogotá";
            String idCiudadCliente = "11001";
            String ciudadCliente = "Bogotá";
            String countrySubEntity = "Bogotá";

            if(tmpMaster.getENC_CIUDAD_SEDE().length()<=3){
                PaisData data = billDAO.getPaisData(tmpMaster.getENC_CIUDAD_SEDE());
                ciudadSegunCodigo = data.getPais();idCiudadCliente = data.getId_pais().toString();ciudadCliente = data.getPais();countrySubEntity = data.getPais();
            }else{
                if(tmpMaster.getENC_CIUDAD_SEDE().length()<5){tmpMaster.setENC_CIUDAD_SEDE("0"+tmpMaster.getENC_CIUDAD_SEDE());}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("11001")){ciudadSegunCodigo = "Bogotá";idCiudadCliente = "11001";ciudadCliente = "Bogotá";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("05001")){ciudadSegunCodigo = "Medellín";idCiudadCliente = "05001";ciudadCliente = "Medellín";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("17001")){ciudadSegunCodigo = "Manizales";idCiudadCliente = "17001";ciudadCliente = "Manizales";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("76520")){ciudadSegunCodigo = "Palmira";idCiudadCliente = "76520";ciudadCliente = "Palmira";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("88001")){ciudadSegunCodigo = "San Andrés";idCiudadCliente = "88001";ciudadCliente = "San Andrés";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("91001")){ciudadSegunCodigo = "Leticia";idCiudadCliente = "91001";ciudadCliente = "Leticia";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("81001")){ciudadSegunCodigo = "Arauca";idCiudadCliente = "81001";ciudadCliente = "Arauca";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("52835")){ciudadSegunCodigo = "Tumaco";idCiudadCliente = "52835";ciudadCliente = "Tumaco";countrySubEntity = "Colombia";}
                if(tmpMaster.getENC_CIUDAD_SEDE().equals("20621")){ciudadSegunCodigo = "La Paz";idCiudadCliente = "20621";ciudadCliente = "La Paz";countrySubEntity = "Colombia";}
            }

            try{
                idCiudadCliente = tmpMaster.getENC_CLICIUDAD();
                if(idCiudadCliente.length()==4){
                    idCiudadCliente = "0"+idCiudadCliente;
                }
                ciudadCliente = billDAO.getCiudad(idCiudadCliente);
                String subentityCode = idCiudadCliente.substring(0,2);
            }catch(Exception e){
                idCiudadCliente = tmpMaster.getENC_CIUDAD_SEDE();
                ciudadCliente = ciudadSegunCodigo;
            }

            int profileExecutionID = 2;
            String pref = "SETT";
            String pruebas = "https://apivp.efacturacadena.com/staging/vp/documentos/proceso/sincrono";
            String produccion = "https://apivp.efacturacadena.com/v1/vp/documentos/proceso/sincrono";
            String urlConsumption = pruebas;

            if(env == 1){
                profileExecutionID = 1;
                pref = tmpMaster.getENC_PREFIJO();
                urlConsumption = produccion;
            }

            Double SubTotal = tmpMaster.getENC_SUBTOTAL();
            Double discountvalue = tmpMaster.getENC_DESCUENTO();
            Double subtotDiv = new Double(1);
            if(!SubTotal.toString().equals("0")){
                subtotDiv = SubTotal;
            }

            Double discountPercentage = new Double(0);

            if(subtotDiv != 0){
                discountPercentage = new Double((discountvalue * 100)/subtotDiv);
            }

            if(Double.isNaN(discountPercentage)){
                discountPercentage = new Double(0);
            }



            BillingDianInfo ConsecDian = billDAO.getConsecDianCredito(tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_PREFIJO());
            if(ConsecDian == null){
                ConsecDian = new BillingDianInfo(new Long(0000),tmpMaster.getENC_FECEMIC());
            }

            String obs = "";

            try {
                obs = tmpMaster.getENC_OBSERVACIONES().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#");
            }catch(Exception e){
                obs = "";
            }

            String cli_email = "";
            String[] obss;
            try{
                obss = obs.split("<<");
                if(obss[1].contains("@")){
                    cli_email = obss[1];
                }else{
                    cli_email = tmpMaster.getENC_CLIEMAIL();
                }
            }catch(Exception e){
                cli_email = tmpMaster.getENC_CLIEMAIL();
            }

            try {
                cli_email = cli_email.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#");
            }catch(Exception e){
                cli_email = "";
            }



            String cli_dir = "";

            try {
                cli_dir = tmpMaster.getENC_CLIDIR().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#");
            }catch(Exception e){
                cli_dir = "";
            }


            String fechaPago = "";

            if(tmpMaster.getENC_FECPAGO()!=null && tmpMaster.getENC_FECPAGO()!="") {
                fechaPago = tmpMaster.getENC_FECPAGO();
            }else{
                fechaPago = tmpMaster.getENC_FECEMIC();
            }

            String countrySubEntityCode = "CO";
            if(tmpMaster.getENC_CLITIPOID().toString().equals("42")||tmpMaster.getENC_CLITIPOID().toString().equals("41")||tmpMaster.getENC_CLITIPOID().toString().equals("50")){
                System.out.println("International");
                try {
                    InternationalData data = billDAO.getInternationalData(tmpMaster.getENC_CLICIUDAD());
                    String [] listing = tmpMaster.getENC_CLINUMID().split("-");
                    tmpMaster.setENC_CLINUMID(listing[0]);
                    idCiudadCliente = "01000";

                    if(tmpMaster.getENC_CLICIUDAD().length()==1){
                        idCiudadCliente = "0100"+tmpMaster.getENC_CLICIUDAD();
                    }
                    if(tmpMaster.getENC_CLICIUDAD().length()==2){
                        idCiudadCliente = "010"+tmpMaster.getENC_CLICIUDAD();
                    }
                    if(tmpMaster.getENC_CLICIUDAD().length()==3){
                        idCiudadCliente = "01"+tmpMaster.getENC_CLICIUDAD();
                    }
                    if(tmpMaster.getENC_CLICIUDAD().length()==4){
                        idCiudadCliente = "0"+tmpMaster.getENC_CLICIUDAD();
                    }
                    if(tmpMaster.getENC_CLICIUDAD().length()==5){
                        idCiudadCliente = tmpMaster.getENC_CLICIUDAD();
                    }

                    int ciudadClientePos1 = tmpMaster.getENC_CLIDIR().indexOf("{");
                    int ciudadClientePos2 = tmpMaster.getENC_CLIDIR().indexOf("}");
                    ciudadCliente = tmpMaster.getENC_CLIDIR().substring(ciudadClientePos1+1,ciudadClientePos2);
                    countrySubEntity = data.getNOMBRE_PAIS();
                    countrySubEntityCode = data.getPREFIJO_DIAN();

                }catch(Exception e){
                    e.printStackTrace();
                    billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NO SE ENCONTRARON DATOS . SYSWARE.");
                }



            }

            Boolean doesHttp = true;



            String invoiceRawXmlHead1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<CreditNote xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2\" xmlns:cac=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:cbc=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ext=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:sts=\"dian:gov:co:facturaelectronica:Structures-2-1\" xmlns:xades=\"http://uri.etsi.org/01903/v1.3.2#\" xmlns:xades141=\"http://uri.etsi.org/01903/v1.4.1#\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2     http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-CreditNote-2.1.xsd\">\n" +
                    "\t<cbc:CustomizationID>20</cbc:CustomizationID>\n" +
                    "\t<cbc:ProfileExecutionID>"+profileExecutionID+"</cbc:ProfileExecutionID>\n" +
                    "\t<cbc:ID>"+"NC"+pref+tmpMaster.getENC_CONSDIAN()+"</cbc:ID>\n" +
                    "\t<cbc:IssueDate>"+tmpMaster.getENC_FECEMIC()+"</cbc:IssueDate>\n" +
                    "\t<cbc:IssueTime>00:00:00-05:00</cbc:IssueTime>\n" +
                    "\t<cbc:CreditNoteTypeCode>91</cbc:CreditNoteTypeCode>\n"+
                    //"\t<cbc:InvoiceTypeCode>01</cbc:InvoiceTypeCode>\n" +
                    "\t<cbc:Note>"+obs.replaceAll("No\\.","#").replaceAll("NO\\.","#")+"</cbc:Note>\n" +
                    "\t<cbc:DocumentCurrencyCode>COP</cbc:DocumentCurrencyCode>\n" +
                    "\t<cbc:LineCountNumeric>"+tmpMaster.getENC_NRO_ITEMES()+"</cbc:LineCountNumeric>\n" +
                    "\t<cac:DiscrepancyResponse>\n" +
                    "\t\t<cbc:ReferenceID>  </cbc:ReferenceID>\n" +
                    "\t\t<cbc:ResponseCode>2</cbc:ResponseCode>\n" +
                    "\t\t<cbc:Description>Anulación de factura electrónica</cbc:Description>\n" +
                    "\t</cac:DiscrepancyResponse>" +
                    "\t<cac:OrderReference>\n" +
                    "\t\t<cbc:ID>orderReference</cbc:ID>\n" +
                    "\t</cac:OrderReference>" +
                    "\t<cac:BillingReference>\n" +
                    "\t\t<cac:InvoiceDocumentReference>\n" +
                    "\t\t\t<cbc:ID>"+pref+ConsecDian.getENC_CONSDIAN()+"</cbc:ID>\n" +
                    "\t\t\t<!--Factura asociada debe ser mismo al de la factura a modificar -->\n" +
                    "\t\t\t<cbc:IssueDate>"+ConsecDian.getENC_FECEMIC()+"</cbc:IssueDate>\n" +
                    "\t\t\t<!-- mismo al issue date de la factura o anterior -->\n" +
                    "\t\t</cac:InvoiceDocumentReference>\n" +
                    "\t</cac:BillingReference>" +
                    "\t<cac:AccountingSupplierParty>\n" +
                    "\t\t<cbc:AdditionalAccountID>1</cbc:AdditionalAccountID>\n" +
                    "\t\t<cac:Party>\n" +
                    "\t\t\t<cac:PartyName>\n" +
                    "\t\t\t\t<cbc:Name>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:Name>\n" +
                    "\t\t\t</cac:PartyName>\n" +
                    "\t\t\t<cac:PhysicalLocation>\n" +
                    "\t\t\t\t<cac:Address>\n" +
                    "\t\t\t\t\t<cbc:ID>"+tmpMaster.getENC_CIUDAD_SEDE()+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>"+ciudadSegunCodigo+"</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadSegunCodigo+"</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>"+tmpMaster.getENC_CIUDAD_SEDE().substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>"+tmpMaster.getENC_DIR_SEDE().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:Address>\n" +
                    "\t\t\t</cac:PhysicalLocation>\n" +
                    "\t\t\t<cac:PartyTaxScheme>\n" +
                    "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                    "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                    "\t\t\t\t<cbc:TaxLevelCode listName=\"No Aplica\">O-13;O-23</cbc:TaxLevelCode>\n" +
                    "\t\t\t\t<cac:RegistrationAddress>\n" +
                    "\t\t\t\t\t<cbc:ID>11001</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>Bogotá</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:PostalZone>000000</cbc:PostalZone>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>Bogotá</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>11</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>CR 45   26   85 OF 481 ED URIEL GUTIERREZ</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:RegistrationAddress>\n" +
                    "\t\t\t\t<cac:TaxScheme>\n" +
                    "\t\t\t\t\t<cbc:ID>01</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:Name>IVA</cbc:Name>\n" +
                    "\t\t\t\t</cac:TaxScheme>\n" +
                    "\t\t\t</cac:PartyTaxScheme>\n" +
                    "\t\t\t<cac:PartyLegalEntity>\n" +
                    "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                    "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                    "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t\t\t<cbc:ID>"+"NC"+pref+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CONSDIAN()+"</cbc:Name>\n" +
                    "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t</cac:PartyLegalEntity>\n" +
                    "\t\t\t<cac:Contact>\n" +
                    "\t\t\t\t<cbc:Name></cbc:Name>\n" +
                    "\t\t\t\t<cbc:Telephone>3165000</cbc:Telephone>\n" +
                    "\t\t\t\t<cbc:ElectronicMail>efactura_nal@unal.edu.co</cbc:ElectronicMail>\n" +
                    "\t\t\t</cac:Contact>\n" +
                    "\t\t</cac:Party>\n" +
                    "\t</cac:AccountingSupplierParty>\n" +
                    "\t<cac:AccountingCustomerParty>\n"+
                    "\t\t<cbc:AdditionalAccountID>"+tmpMaster.getENC_CLINATU()+"</cbc:AdditionalAccountID>\n"+
                    "\t\t<cac:Party>\t\n" +
                    "\t\t\t<cac:PartyIdentification>\t\n" +
                    "\t\t\t\t<cbc:ID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\"31\">"+tmpMaster.getENC_CLINUMID()+"</cbc:ID>\n" +
                    "\t\t\t</cac:PartyIdentification>\n" +
                    "\t\t\t<cac:PartyName>\n" +
                    "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("No\\.","#").replaceAll("_","Y").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Name>\n" +
                    "\t\t\t</cac:PartyName>\n" +
                    "\t\t\t<cac:PhysicalLocation>\n" +
                    "\t\t\t\t<cac:Address>\n" +
                    "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadCliente+"</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>"+idCiudadCliente.substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>"+cli_dir+"</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>"+countrySubEntityCode+"</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">"+countrySubEntity+"</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:Address>\n" +
                    "\t\t\t</cac:PhysicalLocation>\n" +
                    "\t\t\t<cac:PartyTaxScheme>\n" +
                    "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("No\\.","#").replaceAll("NO\\.","#").replaceAll("<","").replaceAll(">","")+"</cbc:RegistrationName>\n";




            String paymentID = "Efectivo";

            if(tmpMaster.getENC_MEDIOPAGO().toString().equals("48")){
                paymentID = "Tarjeta Crédito";
            }

            if(tmpMaster.getENC_MEDIOPAGO().toString().equals("49")){
                paymentID = "Tarjeta Débito";
            }

            if(tmpMaster.getENC_MEDIOPAGO().toString().equals("47")){
                paymentID = "Transferencia Débito Bancaria";
            }


            String cliRespFiscal = tmpMaster.getENC_CLIRESPFISCAL();

            try{
                if(tmpMaster.getENC_CLIRESPFISCAL().equals("0") || tmpMaster.getENC_CLIRESPFISCAL() == null || tmpMaster.getENC_CLIRESPFISCAL().equals("null")||tmpMaster.getENC_CLIRESPFISCAL().equals("")){
                    cliRespFiscal = "R-99-PN";
                }
            }catch(Exception e){
                cliRespFiscal = "R-99-PN";
            }

            String TaxLevelCode = "No Aplica";
/*
            try{
                if(tmpMaster.getENC_CLIRESPIVA()== new Long(0)){
                    TaxLevelCode = "49";
                }else{
                    TaxLevelCode = tmpMaster.getENC_CLIRESPIVA().toString();
                }
            }catch(Exception e){
                TaxLevelCode = "49";
            }



*/      String digitoVerificacion = "";
        String invoiceRawXmlHead = "";

        String schemeName = tmpMaster.getENC_CLITIPOID();
        if(schemeName.toString().equals("42")){
            System.out.println("International");
            digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
        }else{
            System.out.println("national");
            if(tmpMaster.getENC_CLINATU().toString().equals("1") && tmpMaster.getENC_CLITIPOID().toString().equals("31")){
                System.out.println("IM JURIDIC");
                try{
                    System.out.println("------------------------JURIDICAAAA------------------");
                    String number =  tmpMaster.getENC_CLINUMID().split("-")[0];
                    String digit = tmpMaster.getENC_CLINUMID().split("-")[1];
                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\""+digit+"\"  schemeName=\""+schemeName+"\">"+number+"</cbc:CompanyID>\n";
                }catch(Exception  e){
                    e.printStackTrace();
                    billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NO SE ENCONTRO DIGITO DE VERIFICACION EN EL DOCUMENTO. SYSWARE.");
                    doesHttp = false;
                }
            }else{
                System.out.println("------------------------NATURAL------------------");
                System.out.println("IM NATURAL");
                if(schemeName.equals("16")){
                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
                }else{
                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
                }
            }
        }



            String invoiceRawXmlHead12 =  "\t\t\t\t<cbc:TaxLevelCode listName=\""+TaxLevelCode+"\">"+cliRespFiscal+"</cbc:TaxLevelCode>\n" +
                    "\t\t\t\t<cac:RegistrationAddress>\n" +
                    "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadCliente+"</cbc:CountrySubentity>\n" +
                    "\t\t\t\t\t<cbc:CountrySubentityCode>"+idCiudadCliente.substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                    "\t\t\t\t\t<cac:AddressLine>\n" +
                    "\t\t\t\t\t\t<cbc:Line>"+cli_dir+"</cbc:Line>\n" +
                    "\t\t\t\t\t</cac:AddressLine>\n" +
                    "\t\t\t\t\t<cac:Country>\n" +
                    "\t\t\t\t\t\t<cbc:IdentificationCode>"+countrySubEntityCode+"</cbc:IdentificationCode>\n" +
                    "\t\t\t\t\t\t<cbc:Name languageID=\"es\">"+countrySubEntity+"</cbc:Name>\n" +
                    "\t\t\t\t\t</cac:Country>\n" +
                    "\t\t\t\t</cac:RegistrationAddress>\n" +
                    "\t\t\t\t<cac:TaxScheme>\n" +
                    "\t\t\t\t\t<cbc:ID>01</cbc:ID>\n" +
                    "\t\t\t\t\t<cbc:Name>IVA</cbc:Name>\n" +
                    "\t\t\t\t</cac:TaxScheme>\n" +
                    "\t\t\t</cac:PartyTaxScheme>\n" +
                    "\t\t\t<cac:PartyLegalEntity>\n" +
                    "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:RegistrationName>\n" +
                    "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\"31\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n" +
                    "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t\t\t<cbc:Name>11111</cbc:Name>\n" +
                    "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                    "\t\t\t</cac:PartyLegalEntity>\n" +
                    "\t\t\t<cac:Contact>\n" +
                    "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:Name>\n" +
                    "\t\t\t\t<cbc:Telephone>"+tmpMaster.getENC_CLITELELEF()+"</cbc:Telephone>\n" +
                    "\t\t\t\t<cbc:ElectronicMail>"+cli_email+"</cbc:ElectronicMail>\n" +
                    "\t\t\t</cac:Contact>\n" +
                    "\t\t</cac:Party>\n" +
                    "\t</cac:AccountingCustomerParty>\n" +
                    "\t<cac:PaymentMeans>\n" +
                    "\t\t<cbc:ID>"+tmpMaster.getENC_FORMAPAGO()+"</cbc:ID>\n" +
                    "\t\t<cbc:PaymentMeansCode>"+tmpMaster.getENC_MEDIOPAGO()+"</cbc:PaymentMeansCode>\n" +
                    "\t\t<cbc:PaymentDueDate>"+fechaPago+"</cbc:PaymentDueDate>\n" +
                    "\t\t<cbc:PaymentID>"+paymentID+"</cbc:PaymentID>\n" +
                    "\t</cac:PaymentMeans>\n" +
                    "\t<cac:AllowanceCharge>\n" +
                    "\t\t<cbc:ID>1</cbc:ID>\n" +
                    "\t\t<cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                    "\t\t<cbc:AllowanceChargeReasonCode>01</cbc:AllowanceChargeReasonCode>\n" +
                    "\t\t<cbc:AllowanceChargeReason>Descuento</cbc:AllowanceChargeReason>\n" +
                    "\t\t<cbc:MultiplierFactorNumeric>"+discountPercentage+"</cbc:MultiplierFactorNumeric>\n" +
                    "\t\t<cbc:Amount currencyID=\"COP\">"+tmpMaster.getENC_DESCUENTO()+"</cbc:Amount>\n" +
                    "\t\t<cbc:BaseAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:BaseAmount>\n" +
                    "\t</cac:AllowanceCharge>\n" +
                    "\t<cac:LegalMonetaryTotal>\n" +
                    "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:LineExtensionAmount><!-- Valor bruto del documento-->\n" +
                    "\t\t<cbc:TaxExclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxExclusiveAmount><!-- Base imponilbe del documento-->\n" +
                    "\t\t<cbc:TaxInclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxInclusiveAmount><!-- Base imponible + impuestos-->\n" +
                    "\t\t<cbc:AllowanceTotalAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_DESCUENTO()).replaceAll(",",".")+"</cbc:AllowanceTotalAmount>\n" +
                    //"\t\t<!-- <cbc:ChargeTotalAmount currencyID=\"COP\">"+ChargeTotalAmount+"<cbc:ChargeTotalAmount> Aplica cuando es cargo a nivel general de la factura, suma de los cargos generales-->\n" +
                    "\t\t<cbc:PayableAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_TOTAL()).replaceAll(",",".")+"</cbc:PayableAmount><!--Valor a Pagar de Factura: Valor total de ítems (incluyendo cargos y descuentos a nivel de ítems)+valor tributos + valor cargos – valor descuentos – valor anticipos -->\n" +
                    "\t</cac:LegalMonetaryTotal>";



        if(schemeName.toString().equals("42")){
            System.out.println("International");
            digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
            invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
        }else{
            System.out.println("national");
            if(tmpMaster.getENC_CLINATU().toString().equals("1") && tmpMaster.getENC_CLITIPOID().toString().equals("31")){
                System.out.println("IM JURIDIC");
                try{
                    String number =  tmpMaster.getENC_CLINUMID().split("-")[0];
                    String digit = tmpMaster.getENC_CLINUMID().split("-")[1];
                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\""+digit+"\"  schemeName=\""+schemeName+"\">"+number+"</cbc:CompanyID>\n";
                    invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
                }catch(Exception  e){
                    billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NO SE ENCONTRO DIGITO DE VERIFICACION EN EL DOCUMENTO. SYSWARE.");
                    doesHttp = false;
                }
            }else{
                System.out.println("IM NATURAL");
                if(schemeName.equals("16")){

                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";

                }else{

                    digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\""+schemeName+"\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";

                }
                invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;


            }
        }




            List<Detail> details = billDAO.getDetails(tmpMaster.getENC_PREFIJO(),
                    tmpMaster.getENC_NRORECIBO(),
                    tmpMaster.getENC_SEDE(),
                    tmpMaster.getENC_VIGENCIA(),
                    tmpMaster.getENC_CIAQUIPU(),
                    tmpMaster.getENC_NUM_DOCUHIJO());


            String detailXmlInvoiceLine = "";

            for(int j = 0; j<details.size();j++){

                Detail myDetail = details.get(j);

                System.out.println("BANDERA: "+myDetail.getDET_CODIGO());
                JSONObject obj1 = new JSONObject();
                obj1.put("id", (j+1));
                String det_descrip = " ";
                try{
                    det_descrip = myDetail.getDET_DESCRIP();
                }catch(Exception e){
                    det_descrip = " ";
                }
                obj1.put("note", det_descrip);
                obj1.put("quantity", myDetail.getDET_CANTIDAD());
                obj1.put("amount", (new Double(myDetail.getDET_CANTIDAD()) * myDetail.getDET_VLRUNIT()));
                obj1.put("taxAmount", 0.0);
                obj1.put("taxPercent", 0.0);
                obj1.put("taxId", 01);
                obj1.put("taxName", "IVA");
                obj1.put("description", myDetail.getDET_NOMBRE());
                try{
                    obj1.put("itemId", myDetail.getDET_CODIGO().replaceAll("^(?i)null$", "%20").replaceAll("null", "%20"));
                }catch(Exception e){
                    obj1.put("itemId", " ");
                }
                obj1.put("individualPrice", myDetail.getDET_VLRUNIT());
                obj1.put("baseQuantity", 1.00);

                System.out.println("itemId: "+obj1.get("itemId").toString());
/*----------------------------------------------------------------------------*/
                detailXmlInvoiceLine +=  generateCreditLines(new Integer(obj1.get("id").toString()),
                        det_descrip.replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                        myDetail.getDET_CANTIDAD(),
                        new Double(obj1.get("amount").toString()),
                        new Double(obj1.get("taxAmount").toString()),
                        new Double(obj1.get("taxPercent").toString()),
                        new Integer(obj1.get("taxId").toString()),
                        obj1.get("taxName").toString(),
                        obj1.get("description").toString().replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                        obj1.get("itemId").toString(),
                        new Double(obj1.get("individualPrice").toString()),
                        new Double(obj1.get("baseQuantity").toString()));

            }

        String datPago = "";

        try{
            datPago = tmpMaster.getENC_DATOS_PAGO().replaceAll("%20","").replaceAll("^(?i)null$", "").replaceAll("<","").replaceAll(">","");
        }catch(Exception e){
            datPago = "";
        }


        String ResolucionOpcional4="Resolución de habilitación de facturación electrónica No. 18764008055116 de fecha 25/11/2020.";

        if(pref.equals("BOG")){
            ResolucionOpcional4="Resolución nueva";
        }

        if(pref.equals("SIA")){
            ResolucionOpcional4="Número Autorización: 18764029138790 Rango Autorizado Desde: SIA300001 Rango Autorizado Hasta: SIA500000 Vigencia: 2023-10-20\n" +
                    "Representación Gráfica del documento electrónico según resolución DIAN 042 del 05 de mayo del 2020.";
        }

            String invoiceRawXmlFooter = "<DATA>\n" +
                    "\t\t<UBL21>true</UBL21>\n" +
                    "\t\t<Development>\n" +
                    "\t\t\t<ShortName>UNAL</ShortName>\n" +
                    "\t\t</Development>\n" +
                    "\t\t <Filters>\n" +
                    "\t\t\t<Product ></Product><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                    "\t\t\t<SubProduct></SubProduct><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                    "\t\t\t<CUENTA/>\n" +
                    "\t\t\t<NATURALEZA/>\n" +
                    "\t\t\t<CENTRODECOSTO/>\n" +
                    "\t\t\t<CLASE/>\n" +
                    "\t\t\t<INDICE/>\n" +
                    "\t\t</Filters>\n" +
                    "\t\t<Opcional1> Somos grandes contribuyentes, no responsables de IVA </Opcional1> <!-- ID 04 Representación Gráfica -->\n" +
                    "\t\t<Opcional2>  Agentes de retención de Impuesto sobre las ventas, no sujetos al impuesto de industria y comercio </Opcional2> <!-- ID 05 Representación Gráfica -->\n" +
                    "\t\t<Opcional3> "+tmpMaster.getENC_CIAQUIPU()+" - "+tmpMaster.getENC_NRORECIBO()+"</Opcional3> <!-- ID 10 Representación Gráfica -->\n" +
                    "\t\t<Opcional4>"+datPago+
                    "#Somos Institución de Educación Superior no contribuyente del impuesto de renta,  favor abstenerse de efectuar retenciones en la  fuente. " +
                    //"#"+ResolucionOpcional4+" " +
                    "#Correo: efactura_nal@unal.edu.co </Opcional4> <!-- ID 56 Representación Gráfica -->\n" +
                    "\t</DATA></CreditNote>";



            //invoiceRawXmlFooter = "</Invoice>";



            String finalXML = (invoiceRawXmlHead + detailXmlInvoiceLine + invoiceRawXmlFooter).replaceAll("^(?i)null$", "%20").replaceAll("null", "%20");

            byte[] encodedBytes = Base64.encodeBase64(finalXML.getBytes());

            System.out.println("-------------------------------XML------------------------------");

            //System.out.println(finalXML);

            System.out.println("encodedBytes " + new String(encodedBytes));

            Boolean responseFinal = false;
        if(doesHttp){
            httpCall++;
            responseFinal = sendHttp(urlConsumption,env,encodedBytes,tmpMaster);
        }

        myList.add(pref+tmpMaster.getENC_CONSDIAN()+" : "+httpCounter+" : "+httpCall);


            if(responseFinal){
                return new Long(1);
            }else{
                return new Long(0);
            }

    }


    public Boolean sendHttp(String urlConsumption, Long env, byte[] encodedBytes, Master tmpMaster){
        try {
            httpCounter++;
            URL url = new URL(urlConsumption);//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("efacturaAuthorizationToken", "e14e6d8a-ed57-4de7-bc6d-7dba144240df");
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "text/plain");
            if(env == 2){
                conn.setRequestProperty("efacturaAuthorizationToken", "d46b4d3c-d22d-41c8-af5d-6222564f9421");
            }

            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            os.write((new String(encodedBytes)).getBytes());
            os.flush();
            os.close();


            int responseCode = conn.getResponseCode();
            System.out.println(responseCode);
            System.out.println(conn.getResponseMessage());

            if (responseCode != 200) {
                InputStreamReader in2 = new InputStreamReader(conn.getErrorStream());
                BufferedReader br2 = new BufferedReader(in2);
                String output2;
                System.out.println("ERROR: ");
                String ErrorOutput = "";
                while ((output2 = br2.readLine()) != null) {
                    System.out.println(output2);
                    ErrorOutput+=(" ; "+output2);
                }
                billDAO.updateTable("R",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),ErrorOutput);
                conn.disconnect();
                throw new RuntimeException("Failed : HTTP Error code : "
                        + responseCode);
            }else{
                billDAO.updateTable("A",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),conn.getResponseMessage());
                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;

                while ((output = br.readLine()) != null) {
                    System.out.println(output);
                }

                conn.disconnect();
            }




            return true;
        } catch (Exception e) {
            System.out.println("Exception in NetClientGet:- " + e);
            return false;
        }

    }



    public Long JsonConverterMethodDebit(Long env, Master tmpMaster){


        String ciudadSegunCodigo = "Bogotá";
        String idCiudadCliente = "11001";
        String ciudadCliente = "Bogotá";
        String countrySubEntity = "Bogotá";

        if(tmpMaster.getENC_CIUDAD_SEDE().length()<=3){
            PaisData data = billDAO.getPaisData(tmpMaster.getENC_CIUDAD_SEDE());
            ciudadSegunCodigo = data.getPais();idCiudadCliente = data.getId_pais().toString();ciudadCliente = data.getPais();countrySubEntity = data.getPais();
        }else{
            if(tmpMaster.getENC_CIUDAD_SEDE().length()<5){tmpMaster.setENC_CIUDAD_SEDE("0"+tmpMaster.getENC_CIUDAD_SEDE());}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("11001")){ciudadSegunCodigo = "Bogotá";idCiudadCliente = "11001";ciudadCliente = "Bogotá";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("05001")){ciudadSegunCodigo = "Medellín";idCiudadCliente = "05001";ciudadCliente = "Medellín";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("17001")){ciudadSegunCodigo = "Manizales";idCiudadCliente = "17001";ciudadCliente = "Manizales";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("76520")){ciudadSegunCodigo = "Palmira";idCiudadCliente = "76520";ciudadCliente = "Palmira";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("88001")){ciudadSegunCodigo = "San Andrés";idCiudadCliente = "88001";ciudadCliente = "San Andrés";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("91001")){ciudadSegunCodigo = "Leticia";idCiudadCliente = "91001";ciudadCliente = "Leticia";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("81001")){ciudadSegunCodigo = "Arauca";idCiudadCliente = "81001";ciudadCliente = "Arauca";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("52835")){ciudadSegunCodigo = "Tumaco";idCiudadCliente = "52835";ciudadCliente = "Tumaco";countrySubEntity = "Colombia";}
            if(tmpMaster.getENC_CIUDAD_SEDE().equals("20621")){ciudadSegunCodigo = "La Paz";idCiudadCliente = "20621";ciudadCliente = "La Paz";countrySubEntity = "Colombia";}
        }

        try{
            idCiudadCliente = tmpMaster.getENC_CLICIUDAD();
            if(idCiudadCliente.length()==4){
                idCiudadCliente = "0"+idCiudadCliente;
            }
            ciudadCliente = billDAO.getCiudad(idCiudadCliente);
            String subentityCode = idCiudadCliente.substring(0,2);
        }catch(Exception e){
            idCiudadCliente = tmpMaster.getENC_CIUDAD_SEDE();
            ciudadCliente = ciudadSegunCodigo;
        }


        int profileExecutionID = 2;
        String pref = "SETT";
        String pruebas = "https://apivp.efacturacadena.com/staging/vp/documentos/proceso/sincrono";
        String produccion = "https://apivp.efacturacadena.com/v1/vp/documentos/proceso/sincrono";
        String urlConsumption = pruebas;

        if(env == 1){
            profileExecutionID = 1;
            pref = tmpMaster.getENC_PREFIJO();
            urlConsumption = produccion;
        }

        Double SubTotal = tmpMaster.getENC_SUBTOTAL();
        Double discountvalue = tmpMaster.getENC_DESCUENTO();
        Double subtotDiv = new Double(1);
        if(!SubTotal.toString().equals("0")){
            subtotDiv = SubTotal;
        }

        Double discountPercentage = new Double(0);

        if(subtotDiv != 0){
            discountPercentage = new Double((discountvalue * 100)/subtotDiv);
        }



        BillingDianInfo ConsecDian = billDAO.getConsecDianCredito(tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_PREFIJO());

        if(ConsecDian == null){
            ConsecDian = new BillingDianInfo(new Long(0000),tmpMaster.getENC_FECEMIC());
        }


        String obs = "";

        try {
            obs = tmpMaster.getENC_OBSERVACIONES().replaceAll("No\\.","#").replaceAll("NO\\.","#");
        }catch(Exception e){
            obs = "";
        }


        String cli_dir = "";

        try {
            cli_dir = tmpMaster.getENC_CLIDIR().replaceAll("No\\.","#").replaceAll("NO\\.","#");
        }catch(Exception e){
            cli_dir = "";
        }


        String cli_email = "";
        String[] obss;
        try{
            obss = obs.split("<<");
            if(obss[1].contains("@")){
                cli_email = obss[1];
            }else{
                cli_email = tmpMaster.getENC_CLIEMAIL();
            }
        }catch(Exception e){
            cli_email = tmpMaster.getENC_CLIEMAIL();
        }

        try {
            cli_email = cli_email.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#");
        }catch(Exception e){
            cli_email = "";
        }


        String fechaPago = "";

        if(tmpMaster.getENC_FECPAGO()!=null && tmpMaster.getENC_FECPAGO()!="") {
            fechaPago = tmpMaster.getENC_FECPAGO();
        }else{
            fechaPago = tmpMaster.getENC_FECEMIC();
        }

        String countrySubEntityCode = "CO";
        if(tmpMaster.getENC_CLITIPOID().toString().equals("42")||tmpMaster.getENC_CLITIPOID().toString().equals("41")||tmpMaster.getENC_CLITIPOID().toString().equals("50")){
            System.out.println("International");
            try {
                InternationalData data = billDAO.getInternationalData(tmpMaster.getENC_CLICIUDAD());
                String [] listing = tmpMaster.getENC_CLINUMID().split("-");
                tmpMaster.setENC_CLINUMID(listing[0]);
                idCiudadCliente = "01000";

                if(tmpMaster.getENC_CLICIUDAD().length()==1){
                    idCiudadCliente = "0100"+tmpMaster.getENC_CLICIUDAD();
                }
                if(tmpMaster.getENC_CLICIUDAD().length()==2){
                    idCiudadCliente = "010"+tmpMaster.getENC_CLICIUDAD();
                }
                if(tmpMaster.getENC_CLICIUDAD().length()==3){
                    idCiudadCliente = "01"+tmpMaster.getENC_CLICIUDAD();
                }
                if(tmpMaster.getENC_CLICIUDAD().length()==4){
                    idCiudadCliente = "0"+tmpMaster.getENC_CLICIUDAD();
                }
                if(tmpMaster.getENC_CLICIUDAD().length()==5){
                    idCiudadCliente = tmpMaster.getENC_CLICIUDAD();
                }

                int ciudadClientePos1 = tmpMaster.getENC_CLIDIR().indexOf("{");
                int ciudadClientePos2 = tmpMaster.getENC_CLIDIR().indexOf("}");
                ciudadCliente = tmpMaster.getENC_CLIDIR().substring(ciudadClientePos1+1,ciudadClientePos2);
                countrySubEntity = data.getNOMBRE_PAIS();
                countrySubEntityCode = data.getPREFIJO_DIAN();

            }catch(Exception e){
                e.printStackTrace();
                billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NUM_DOCUHIJO(),"NO SE ENCONTRARON DATOS . SYSWARE.");
            }



        }


        String invoiceRawXmlHead1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<DebitNote xmlns=\"urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2\" xmlns:cac=\"urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2\" xmlns:cbc=\"urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2\" xmlns:ext=\"urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2\" xmlns:sts=\"dian:gov:co:facturaelectronica:Structures-2-1\" xmlns:xades=\"http://uri.etsi.org/01903/v1.3.2#\" xmlns:xades141=\"http://uri.etsi.org/01903/v1.4.1#\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2     http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-DebitNote-2.1.xsd\">\n" +
                "\t<cbc:CustomizationID>30</cbc:CustomizationID>\n" +
                "\t<cbc:ProfileExecutionID>"+profileExecutionID+"</cbc:ProfileExecutionID>\n" +
                "\t<cbc:ID>"+"ND"+pref+tmpMaster.getENC_CONSDIAN()+"</cbc:ID>\n" +
                "\t<cbc:IssueDate>"+tmpMaster.getENC_FECEMIC()+"</cbc:IssueDate>\n" +
                "\t<cbc:IssueTime>00:00:00-05:00</cbc:IssueTime>\n" +
                //"\t<cbc:CreditNoteTypeCode>91</cbc:CreditNoteTypeCode>\n"+
                //"\t<cbc:InvoiceTypeCode>01</cbc:InvoiceTypeCode>\n" +
                "\t<cbc:Note>"+obs.replaceAll("<","").replaceAll(">","")+"</cbc:Note>\n" +
                "\t<cbc:DocumentCurrencyCode>COP</cbc:DocumentCurrencyCode>\n" +
                "\t<cbc:LineCountNumeric>"+tmpMaster.getENC_NRO_ITEMES()+"</cbc:LineCountNumeric>\n" +
                "\t<cac:DiscrepancyResponse>\n" +
                "\t\t<cbc:ReferenceID>  </cbc:ReferenceID>\n" +
                "\t\t<cbc:ResponseCode>4</cbc:ResponseCode>\n" +
                "\t\t<cbc:Description>Otros</cbc:Description>\n" +
                "\t</cac:DiscrepancyResponse>" +
                "\t<cac:BillingReference>\n" +
                "\t\t<cac:InvoiceDocumentReference>\n" +
                "\t\t\t<cbc:ID>"+pref+ConsecDian.getENC_CONSDIAN()+"</cbc:ID>\n" +
                "\t\t\t<!--Factura asociada debe ser mismo al de la factura a modificar -->\n" +
                "\t\t\t<cbc:IssueDate>"+ConsecDian.getENC_FECEMIC()+"</cbc:IssueDate>\n" +
                "\t\t\t<!-- mismo al issue date de la factura o anterior -->\n" +
                "\t\t</cac:InvoiceDocumentReference>\n" +
                "\t</cac:BillingReference>" +
                "\t<cac:AccountingSupplierParty>\n" +
                "\t\t<cbc:AdditionalAccountID>1</cbc:AdditionalAccountID>\n" +
                "\t\t<cac:Party>\n" +
                "\t\t\t<cac:PartyName>\n" +
                "\t\t\t\t<cbc:Name>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:Name>\n" +
                "\t\t\t</cac:PartyName>\n" +
                "\t\t\t<cac:PhysicalLocation>\n" +
                "\t\t\t\t<cac:Address>\n" +
                "\t\t\t\t\t<cbc:ID>"+tmpMaster.getENC_CIUDAD_SEDE()+"</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:CityName>"+ciudadSegunCodigo+"</cbc:CityName>\n" +
                "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                "\t\t\t\t\t<cbc:CountrySubentity>"+countrySubEntity+"</cbc:CountrySubentity>\n" +
                "\t\t\t\t\t<cbc:CountrySubentityCode>"+tmpMaster.getENC_CIUDAD_SEDE().substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                "\t\t\t\t\t<cac:AddressLine>\n" +
                "\t\t\t\t\t\t<cbc:Line>"+tmpMaster.getENC_DIR_SEDE().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                "\t\t\t\t\t</cac:AddressLine>\n" +
                "\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                "\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t</cac:Address>\n" +
                "\t\t\t</cac:PhysicalLocation>\n" +
                "\t\t\t<cac:PartyTaxScheme>\n" +
                "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                "\t\t\t\t<cbc:TaxLevelCode listName=\"No Aplica\">O-13;O-23</cbc:TaxLevelCode>\n" +
                "\t\t\t\t<cac:RegistrationAddress>\n" +
                "\t\t\t\t\t<cbc:ID>11001</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:CityName>Bogotá</cbc:CityName>\n" +
                "\t\t\t\t\t<cbc:PostalZone>000000</cbc:PostalZone>\n" +
                "\t\t\t\t\t<cbc:CountrySubentity>Bogotá</cbc:CountrySubentity>\n" +
                "\t\t\t\t\t<cbc:CountrySubentityCode>11</cbc:CountrySubentityCode>\n" +
                "\t\t\t\t\t<cac:AddressLine>\n" +
                "\t\t\t\t\t\t<cbc:Line>CR 45   26   85 OF 481 ED URIEL GUTIERREZ</cbc:Line>\n" +
                "\t\t\t\t\t</cac:AddressLine>\n" +
                "\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t<cbc:IdentificationCode>CO</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t<cbc:Name languageID=\"es\">Colombia</cbc:Name>\n" +
                "\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t</cac:RegistrationAddress>\n" +
                "\t\t\t\t<cac:TaxScheme>\n" +
                "\t\t\t\t\t<cbc:ID>01</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:Name>IVA</cbc:Name>\n" +
                "\t\t\t\t</cac:TaxScheme>\n" +
                "\t\t\t</cac:PartyTaxScheme>\n" +
                "\t\t\t<cac:PartyLegalEntity>\n" +
                "\t\t\t\t<cbc:RegistrationName>UNIVERSIDAD NACIONAL DE COLOMBIA</cbc:RegistrationName>\n" +
                "\t\t\t\t<cbc:CompanyID schemeID=\"3\" schemeName=\"31\" schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\">899999063</cbc:CompanyID>\n" +
                "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                "\t\t\t\t\t<cbc:ID>"+"ND"+pref+"</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CONSDIAN()+"</cbc:Name>\n" +
                "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                "\t\t\t</cac:PartyLegalEntity>\n" +
                "\t\t\t<cac:Contact>\n" +
                "\t\t\t\t<cbc:Name></cbc:Name>\n" +
                "\t\t\t\t<cbc:Telephone>3165000</cbc:Telephone>\n" +
                "\t\t\t\t<cbc:ElectronicMail>efactura_nal@unal.edu.co</cbc:ElectronicMail>\n" +
                "\t\t\t</cac:Contact>\n" +
                "\t\t</cac:Party>\n" +
                "\t</cac:AccountingSupplierParty>\n" +
                "\t<cac:AccountingCustomerParty>\n"+
                "\t\t<cbc:AdditionalAccountID>"+tmpMaster.getENC_CLINATU()+"</cbc:AdditionalAccountID>\n"+
                "\t\t<cac:Party>\t\n" +
                "\t\t\t<cac:PartyIdentification>\t\n" +
                "\t\t\t\t<cbc:ID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\"31\">"+tmpMaster.getENC_CLINUMID()+"</cbc:ID>\n" +
                "\t\t\t</cac:PartyIdentification>\n" +
                "\t\t\t<cac:PartyName>\n" +
                "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("_","Y").replaceAll("<","").replaceAll(">","")+"</cbc:Name>\n" +
                "\t\t\t</cac:PartyName>\n" +
                "\t\t\t<cac:PhysicalLocation>\n" +
                "\t\t\t\t<cac:Address>\n" +
                "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                "\t\t\t\t\t<cbc:PostalZone>00000</cbc:PostalZone>\n" +
                "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadCliente+"</cbc:CountrySubentity>\n" +
                "\t\t\t\t\t<cbc:CountrySubentityCode>"+idCiudadCliente.substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                "\t\t\t\t\t<cac:AddressLine>\n" +
                "\t\t\t\t\t\t<cbc:Line>"+cli_dir.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                "\t\t\t\t\t</cac:AddressLine>\n" +
                "\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t<cbc:IdentificationCode>"+countrySubEntityCode+"</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t<cbc:Name languageID=\"es\">"+countrySubEntity+"</cbc:Name>\n" +
                "\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t</cac:Address>\n" +
                "\t\t\t</cac:PhysicalLocation>\n" +
                "\t\t\t<cac:PartyTaxScheme>\n" +
                "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:RegistrationName>\n";



        String paymentID = "Efectivo";

        if(tmpMaster.getENC_MEDIOPAGO().toString().equals("48")){
            paymentID = "Tarjeta Crédito";
        }

        if(tmpMaster.getENC_MEDIOPAGO().toString().equals("49")){
            paymentID = "Tarjeta Débito";
        }

        if(tmpMaster.getENC_MEDIOPAGO().toString().equals("47")){
            paymentID = "Transferencia Débito Bancaria";
        }




        String cliRespFiscal = tmpMaster.getENC_CLIRESPFISCAL();

        try{
            if(tmpMaster.getENC_CLIRESPFISCAL().equals("0") || tmpMaster.getENC_CLIRESPFISCAL() == null || tmpMaster.getENC_CLIRESPFISCAL().equals("null")||tmpMaster.getENC_CLIRESPFISCAL().equals("")){
                cliRespFiscal = "R-99-PN";
            }
        }catch(Exception e){
            cliRespFiscal = "R-99-PN";
        }


        String TaxLevelCode = "No Aplica";
/*
        try{
            if(tmpMaster.getENC_CLIRESPIVA()== new Long(0)){
                TaxLevelCode = "49";
            }else{
                TaxLevelCode = tmpMaster.getENC_CLIRESPIVA().toString();
            }
        }catch(Exception e){
            TaxLevelCode = "49";
        }


*/




        String invoiceRawXmlHead12 =  "\t\t\t\t<cbc:TaxLevelCode listName=\""+TaxLevelCode+"\">"+cliRespFiscal+"</cbc:TaxLevelCode>\n" +
                "\t\t\t\t<cac:RegistrationAddress>\n" +
                "\t\t\t\t\t<cbc:ID>"+idCiudadCliente+"</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:CityName>"+ciudadCliente+"</cbc:CityName>\n" +
                "\t\t\t\t\t<cbc:CountrySubentity>"+ciudadCliente+"</cbc:CountrySubentity>\n" +
                "\t\t\t\t\t<cbc:CountrySubentityCode>"+idCiudadCliente.substring(0,2)+"</cbc:CountrySubentityCode>\n" +
                "\t\t\t\t\t<cac:AddressLine>\n" +
                "\t\t\t\t\t\t<cbc:Line>"+cli_dir.replaceAll("No\\.","#").replaceAll("<","").replaceAll(">","").replaceAll("NO\\.","#")+"</cbc:Line>\n" +
                "\t\t\t\t\t</cac:AddressLine>\n" +
                "\t\t\t\t\t<cac:Country>\n" +
                "\t\t\t\t\t\t<cbc:IdentificationCode>"+countrySubEntityCode+"</cbc:IdentificationCode>\n" +
                "\t\t\t\t\t\t<cbc:Name languageID=\"es\">"+countrySubEntity+"</cbc:Name>\n" +
                "\t\t\t\t\t</cac:Country>\n" +
                "\t\t\t\t</cac:RegistrationAddress>\n" +
                "\t\t\t\t<cac:TaxScheme>\n" +
                "\t\t\t\t\t<cbc:ID>01</cbc:ID>\n" +
                "\t\t\t\t\t<cbc:Name>IVA</cbc:Name>\n" +
                "\t\t\t\t</cac:TaxScheme>\n" +
                "\t\t\t</cac:PartyTaxScheme>\n" +
                "\t\t\t<cac:PartyLegalEntity>\n" +
                "\t\t\t\t<cbc:RegistrationName>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:RegistrationName>\n" +
                "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\"0\" schemeName=\"31\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n" +
                "\t\t\t\t<cac:CorporateRegistrationScheme>\n" +
                "\t\t\t\t\t<cbc:Name>11111</cbc:Name>\n" +
                "\t\t\t\t</cac:CorporateRegistrationScheme>\n" +
                "\t\t\t</cac:PartyLegalEntity>\n" +
                "\t\t\t<cac:Contact>\n" +
                "\t\t\t\t<cbc:Name>"+tmpMaster.getENC_CLINOMB().replaceAll("<","").replaceAll(">","")+"</cbc:Name>\n" +
                "\t\t\t\t<cbc:Telephone>"+tmpMaster.getENC_CLITELELEF()+"</cbc:Telephone>\n" +
                "\t\t\t\t<cbc:ElectronicMail>"+cli_email+"</cbc:ElectronicMail>\n" +
                "\t\t\t</cac:Contact>\n" +
                "\t\t</cac:Party>\n" +
                "\t</cac:AccountingCustomerParty>\n" +
                "\t<cac:PaymentMeans>\n" +
                "\t\t<cbc:ID>"+paymentID+"</cbc:ID>\n" +
                "\t\t<cbc:PaymentMeansCode>"+tmpMaster.getENC_MEDIOPAGO()+"</cbc:PaymentMeansCode>\n" +
                "\t\t<cbc:PaymentDueDate>"+tmpMaster.getENC_FECEMIC()+"</cbc:PaymentDueDate>\n" +
                "\t\t<cbc:PaymentID>"+paymentID+"</cbc:PaymentID>\n" +
                "\t</cac:PaymentMeans>\n" +
                "\t<cac:RequestedMonetaryTotal>\n" +
                "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:LineExtensionAmount><!-- Valor bruto del documento-->\n" +
                "\t\t<cbc:TaxExclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxExclusiveAmount><!-- Base imponilbe del documento-->\n" +
                "\t\t<cbc:TaxInclusiveAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_SUBTOTAL()).replaceAll(",",".")+"</cbc:TaxInclusiveAmount><!-- Base imponible + impuestos-->\n" +
                "\t\t<cbc:AllowanceTotalAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_DESCUENTO()).replaceAll(",",".")+"</cbc:AllowanceTotalAmount>\n" +
                //"\t\t<!-- <cbc:ChargeTotalAmount currencyID=\"COP\">"+ChargeTotalAmount+"<cbc:ChargeTotalAmount> Aplica cuando es cargo a nivel general de la factura, suma de los cargos generales-->\n" +
                "\t\t<cbc:PayableAmount currencyID=\"COP\">"+String.format("%.2f", tmpMaster.getENC_TOTAL()).replaceAll(",",".")+"</cbc:PayableAmount><!--Valor a Pagar de Factura: Valor total de ítems (incluyendo cargos y descuentos a nivel de ítems)+valor tributos + valor cargos – valor descuentos – valor anticipos -->\n" +
                "\t</cac:RequestedMonetaryTotal>";


        String digitoVerificacion = "";
        String invoiceRawXmlHead = "";

        Boolean doesHttp = true;
        Boolean doesHttp2 = false;

        System.out.println("THIS IS CLINATU: "+tmpMaster.getENC_CLINATU());
        if(tmpMaster.getENC_CLINATU().toString().equals("1") && tmpMaster.getENC_CLITIPOID().toString().equals("31")){
            System.out.println("IM JURIDIC");
            try{
                System.out.println("------------------------JURIDICAAAA------------------");
                String number =  tmpMaster.getENC_CLINUMID().split("-")[0];
                String digit = tmpMaster.getENC_CLINUMID().split("-")[1];
                digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\" schemeID=\""+digit+"\"  schemeName=\"31\">"+number+"</cbc:CompanyID>\n";
                invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
            }catch(Exception  e){
                billDAO.updateTable("X",tmpMaster.getENC_PREFIJO(),tmpMaster.getENC_NRORECIBO(),tmpMaster.getENC_SEDE(),tmpMaster.getENC_VIGENCIA(),tmpMaster.getENC_CIAQUIPU(),tmpMaster.getENC_NRORECIBO(),"NO SE ENCONTRO DIGITO DE VERIFICACION EN EL DOCUMENTO. SYSWARE.");
                doesHttp = false;
            }
        }else{
            System.out.println("------------------------NATURAL------------------");
            digitoVerificacion = "\t\t\t\t<cbc:CompanyID schemeAgencyID=\"195\" schemeAgencyName=\"CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)\"  schemeName=\"13\">"+tmpMaster.getENC_CLINUMID()+"</cbc:CompanyID>\n";
            invoiceRawXmlHead = invoiceRawXmlHead1+digitoVerificacion+invoiceRawXmlHead12;
        }




        List<Detail> details = billDAO.getDetails(tmpMaster.getENC_PREFIJO(),
                tmpMaster.getENC_NRORECIBO(),
                tmpMaster.getENC_SEDE(),
                tmpMaster.getENC_VIGENCIA(),
                tmpMaster.getENC_CIAQUIPU(),
                tmpMaster.getENC_NUM_DOCUHIJO());


        String detailXmlInvoiceLine = "";

        for(int j = 0; j<details.size();j++){

            Detail myDetail = details.get(j);

            System.out.println("BANDERA: "+myDetail.getDET_CODIGO());
            JSONObject obj1 = new JSONObject();
            obj1.put("id", (j+1));
            try{
                obj1.put("note", myDetail.getDET_CODIGO().replaceAll("^(?i)null$", "%20").replaceAll("null", "%20"));
            }catch(Exception e){
                obj1.put("note", " ");
            }
            obj1.put("quantity", myDetail.getDET_CANTIDAD());
            obj1.put("amount", (new Double(myDetail.getDET_CANTIDAD())  * myDetail.getDET_VLRUNIT()));
            obj1.put("taxAmount", 0.0);
            obj1.put("taxPercent", 0.0);
            obj1.put("taxId", 01);
            obj1.put("taxName", "IVA");
            obj1.put("description", myDetail.getDET_NOMBRE());
            try{
                obj1.put("itemId", myDetail.getDET_CODIGO().replaceAll("^(?i)null$", "%20").replaceAll("null", "%20"));
            }catch(Exception e){
                obj1.put("itemId", " ");
            }
            obj1.put("individualPrice", myDetail.getDET_VLRUNIT());
            obj1.put("baseQuantity", 1.00);

            System.out.println("itemId: "+obj1.get("itemId").toString());

            detailXmlInvoiceLine +=  generateDebitLines(new Integer(obj1.get("id").toString()),
                    obj1.get("note").toString().replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                    myDetail.getDET_CANTIDAD(),
                    new Double(obj1.get("amount").toString()),
                    new Double(obj1.get("taxAmount").toString()),
                    new Double(obj1.get("taxPercent").toString()),
                    new Integer(obj1.get("taxId").toString()),
                    obj1.get("taxName").toString(),
                    obj1.get("description").toString().replaceAll("No\\.","#").replaceAll("NO\\.","#"),
                    obj1.get("itemId").toString(),
                    new Double(obj1.get("individualPrice").toString()),
                    new Double(obj1.get("baseQuantity").toString()));

        }
        String datpago = "";

        try{
            datpago = tmpMaster.getENC_DATOS_PAGO().replaceAll("%20","").replaceAll("<","").replaceAll(">","");

        }catch(Exception e){
            datpago = " ";
        }

        String ResolucionOpcional4="Resolución de habilitación de facturación electrónica No. 18764008055116 de fecha 25/11/2020.";

        if(pref.equals("BOG")){
            ResolucionOpcional4="Resolución nueva";
        }

        if(pref.equals("SIA")){
            ResolucionOpcional4="Número Autorización: 18764029138790 Rango Autorizado Desde: SIA300001 Rango Autorizado Hasta: SIA500000 Vigencia: 2023-11-20\n" +
                    "Representación Gráfica del documento electrónico según resolución DIAN 042 del 05 de mayo del 2020.";
        }

        String invoiceRawXmlFooter = "<DATA>\n" +
                "\t\t<UBL21>true</UBL21>\n" +
                "\t\t<Development>\n" +
                "\t\t\t<ShortName>UNAL</ShortName>\n" +
                "\t\t</Development>\n" +
                "\t\t <Filters>\n" +
                "\t\t\t<Product ></Product><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                "\t\t\t<SubProduct></SubProduct><!-- Campo utilizado para filtrar en la plataforma e-factura.-->\n" +
                "\t\t\t<CUENTA/>\n" +
                "\t\t\t<NATURALEZA/>\n" +
                "\t\t\t<CENTRODECOSTO/>\n" +
                "\t\t\t<CLASE/>\n" +
                "\t\t\t<INDICE/>\n" +
                "\t\t</Filters>\n" +
                "\t\t<Opcional1> Somos grandes contribuyentes, no responsables de IVA </Opcional1> <!-- ID 04 Representación Gráfica -->\n" +
                "\t\t<Opcional2>  Agentes de retención de Impuesto sobre las ventas, no sujetos al impuesto de industria y comercio </Opcional2> <!-- ID 05 Representación Gráfica -->\n" +
                "\t\t<Opcional3> "+tmpMaster.getENC_CIAQUIPU()+" - "+tmpMaster.getENC_NRORECIBO()+"</Opcional3> <!-- ID 10 Representación Gráfica -->\n" +
                "\t\t<Opcional4>"+datpago+
                "#Somos Institución de Educación Superior no contribuyente del impuesto de renta,  favor abstenerse de efectuar retenciones en la  fuente. " +
                //"#"+ResolucionOpcional4+"" +
                "#Correo: efactura_nal@unal.edu.co </Opcional4> <!-- ID 56 Representación Gráfica -->\n" +
                "\t</DATA></DebitNote>";



        //invoiceRawXmlFooter = "</Invoice>";



        String finalXML = (invoiceRawXmlHead + detailXmlInvoiceLine + invoiceRawXmlFooter).replaceAll("^(?i)null$", "%20").replaceAll("null", "%20");

        byte[] encodedBytes = Base64.encodeBase64(finalXML.getBytes());

        System.out.println("-------------------------------XML------------------------------");

        //System.out.println(finalXML);

        System.out.println("encodedBytes " + new String(encodedBytes));

        Boolean responseFinal = false;
        if(doesHttp){
            httpCall++;
            responseFinal = sendHttp(urlConsumption,env,encodedBytes,tmpMaster);
        }

        myList.add(pref+tmpMaster.getENC_CONSDIAN()+" : "+httpCounter+" : "+httpCall);


        if(responseFinal){
            return new Long(1);
        }else{
            return new Long(0);
        }
    }










    String generateInvoiceLines(int id,
                                String note,
                                String quantity,
                                Double amount,
                                Double taxAmount,
                                Double taxPercent,
                                int taxId,
                                String taxName,
                                String description,
                                String itemId,
                                Double individualPrice,
                                Double baseQuantity){
        String invoiceLineEsqueleton = "<cac:InvoiceLine>\n" +
                "\t\t<cbc:ID>"+id+"</cbc:ID>\n" +
               // "\t\t<cbc:Note>"+note+"</cbc:Note><!-- deberá informarse dentro de los periodos de tres (3) días al año que la DIAN defina, en los que los bienes cubiertos se encuentren exentos del impuesto sobre las ventas.-->\n" +
                "\t\t<cbc:InvoicedQuantity unitCode=\"B7\">"+quantity+"</cbc:InvoicedQuantity>\n" +
                "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", amount).replaceAll(",",".")+"</cbc:LineExtensionAmount>\n" +
                "\t\t<!-- <cac:AllowanceCharge>\n" +
                "\t\t\t<cbc:ID>1</cbc:ID>\n" +
                "\t\t\t<cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                "\t\t\t<cbc:AllowanceChargeReason>Descuento por buen cliente</cbc:AllowanceChargeReason>\n" +
                "\t\t\t<cbc:MultiplierFactorNumeric>10.00</cbc:MultiplierFactorNumeric>\n" +
                "\t\t\t<cbc:Amount currencyID=\"COP\">10000.00</cbc:Amount>\n" +
                "\t\t\t<cbc:BaseAmount currencyID=\"COP\">100000.00</cbc:BaseAmount>\n" +
                "\t\t</cac:AllowanceCharge> Aplica cuando hay descuentos para el producto o servicio-->\n" +
                "\t\t<cac:TaxTotal>\n" +
                "\t\t\t<cbc:TaxAmount currencyID=\"COP\">"+taxAmount+"</cbc:TaxAmount><!--Sumatoria de los TaxAmount de los tax subtotal-->\t\t\t\n" +
                "\t\t\t<cac:TaxSubtotal>\n" +
                "\t\t\t\t<cbc:TaxableAmount currencyID=\"COP\">"+String.format("%.2f", amount).replaceAll(",",".")+"</cbc:TaxableAmount>\t\t\t\n" +
                "\t\t\t\t<cbc:TaxAmount currencyID=\"COP\">"+taxAmount+"</cbc:TaxAmount>\n" +
                "\t\t\t\t<cac:TaxCategory>\n" +
                "\t\t\t\t\t<cbc:Percent>"+taxPercent+"</cbc:Percent>\n" +
                "\t\t\t\t\t<cac:TaxScheme>\n" +
                "\t\t\t\t\t\t<cbc:ID>"+taxId+"</cbc:ID>\n" +
                "\t\t\t\t\t\t<cbc:Name>"+taxName+"</cbc:Name>\n" +
                "\t\t\t\t\t</cac:TaxScheme>\n" +
                "\t\t\t\t</cac:TaxCategory>\n" +
                "\t\t\t</cac:TaxSubtotal>\n" +
                "\t\t</cac:TaxTotal>\n"+
                "\t\t<cac:Item>\n" +
                "\t\t\t<cbc:Description>"+description.replaceAll("<","-").replaceAll(">","-")+" - "+note.replaceAll("<","-").replaceAll(">","-")+"</cbc:Description>\n" +
                "\t\t\t<cac:StandardItemIdentification>\n" +
                "\t\t\t\t<cbc:ID schemeID=\"999\">"+itemId+"</cbc:ID>\n" +
                "\t\t\t</cac:StandardItemIdentification>\n" +
                "\t\t</cac:Item>\n" +
                "\t\t<cac:Price>\n" +
                "\t\t\t<cbc:PriceAmount currencyID=\"COP\">"+String.format("%.2f", individualPrice).replaceAll(",",".")+"</cbc:PriceAmount>\n" +
                "\t\t\t<cbc:BaseQuantity unitCode=\"B7\">"+baseQuantity+"</cbc:BaseQuantity>\n" +
                "\t\t</cac:Price>\n" +
                "\t</cac:InvoiceLine>";


        return invoiceLineEsqueleton;

    }





    String generateCreditLines(int id,
                                String note,
                                String quantity,
                                Double amount,
                                Double taxAmount,
                                Double taxPercent,
                                int taxId,
                                String taxName,
                                String description,
                                String itemId,
                                Double individualPrice,
                                Double baseQuantity){
        String invoiceLineEsqueleton = "<cac:CreditNoteLine>\n" +
                "\t\t<cbc:ID>"+id+"</cbc:ID>\n" +
                // "\t\t<cbc:Note>"+note+"</cbc:Note><!-- deberá informarse dentro de los periodos de tres (3) días al año que la DIAN defina, en los que los bienes cubiertos se encuentren exentos del impuesto sobre las ventas.-->\n" +
                "\t\t<cbc:CreditedQuantity unitCode=\"B7\">"+quantity+"</cbc:CreditedQuantity>\n" +
                "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", amount).replaceAll(",",".")+"</cbc:LineExtensionAmount>\n" +
                "\t\t<!-- <cac:AllowanceCharge>\n" +
                "\t\t\t<cbc:ID>1</cbc:ID>\n" +
                "\t\t\t<cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                "\t\t\t<cbc:AllowanceChargeReason>Descuento por buen cliente</cbc:AllowanceChargeReason>\n" +
                "\t\t\t<cbc:MultiplierFactorNumeric>10.00</cbc:MultiplierFactorNumeric>\n" +
                "\t\t\t<cbc:Amount currencyID=\"COP\">10000.00</cbc:Amount>\n" +
                "\t\t\t<cbc:BaseAmount currencyID=\"COP\">100000.00</cbc:BaseAmount>\n" +
                "\t\t</cac:AllowanceCharge> Aplica cuando hay descuentos para el producto o servicio-->\n" +
                "\t\t<cac:TaxTotal>\n" +
                "\t\t\t<cbc:TaxAmount currencyID=\"COP\">"+taxAmount+"</cbc:TaxAmount><!--Sumatoria de los TaxAmount de los tax subtotal-->\t\t\t\n" +
                "\t\t\t<cac:TaxSubtotal>\n" +
                "\t\t\t\t<cbc:TaxableAmount currencyID=\"COP\">"+String.format("%.2f", amount).replaceAll(",",".")+"</cbc:TaxableAmount>\t\t\t\n" +
                "\t\t\t\t<cbc:TaxAmount currencyID=\"COP\">"+taxAmount+"</cbc:TaxAmount>\n" +
                "\t\t\t\t<cac:TaxCategory>\n" +
                "\t\t\t\t\t<cbc:Percent>"+taxPercent+"</cbc:Percent>\n" +
                "\t\t\t\t\t<cac:TaxScheme>\n" +
                "\t\t\t\t\t\t<cbc:ID>"+taxId+"</cbc:ID>\n" +
                "\t\t\t\t\t\t<cbc:Name>"+taxName+"</cbc:Name>\n" +
                "\t\t\t\t\t</cac:TaxScheme>\n" +
                "\t\t\t\t</cac:TaxCategory>\n" +
                "\t\t\t</cac:TaxSubtotal>\n" +
                "\t\t</cac:TaxTotal>\n"+
                "\t\t<cac:Item>\n" +
                "\t\t\t<cbc:Description>"+description+"</cbc:Description>\n" +
                "\t\t\t<cac:StandardItemIdentification>\n" +
                "\t\t\t\t<cbc:ID schemeID=\"999\">"+itemId+"</cbc:ID>\n" +
                "\t\t\t</cac:StandardItemIdentification>\n" +
                "\t\t</cac:Item>\n" +
                "\t\t<cac:Price>\n" +
                "\t\t\t<cbc:PriceAmount currencyID=\"COP\">"+String.format("%.2f", individualPrice).replaceAll(",",".")+"</cbc:PriceAmount>\n" +
                "\t\t\t<cbc:BaseQuantity unitCode=\"B7\">"+baseQuantity+"</cbc:BaseQuantity>\n" +
                "\t\t</cac:Price>\n" +
                "\t</cac:CreditNoteLine>";


        return invoiceLineEsqueleton;

    }







    String generateDebitLines(int id,
                               String note,
                               String quantity,
                               Double amount,
                               Double taxAmount,
                               Double taxPercent,
                               int taxId,
                               String taxName,
                               String description,
                               String itemId,
                               Double individualPrice,
                               Double baseQuantity){
        String invoiceLineEsqueleton = "<cac:DebitNoteLine>\n" +
                "\t\t<cbc:ID>"+id+"</cbc:ID>\n" +
                // "\t\t<cbc:Note>"+note+"</cbc:Note><!-- deberá informarse dentro de los periodos de tres (3) días al año que la DIAN defina, en los que los bienes cubiertos se encuentren exentos del impuesto sobre las ventas.-->\n" +
                "\t\t<cbc:DebitedQuantity unitCode=\"B7\">"+quantity+"</cbc:DebitedQuantity>\n" +
                "\t\t<cbc:LineExtensionAmount currencyID=\"COP\">"+String.format("%.2f", amount).replaceAll(",",".")+"</cbc:LineExtensionAmount>\n" +
                "\t\t<!-- <cac:AllowanceCharge>\n" +
                "\t\t\t<cbc:ID>1</cbc:ID>\n" +
                "\t\t\t<cbc:ChargeIndicator>false</cbc:ChargeIndicator>\n" +
                "\t\t\t<cbc:AllowanceChargeReason>Descuento por buen cliente</cbc:AllowanceChargeReason>\n" +
                "\t\t\t<cbc:MultiplierFactorNumeric>10.00</cbc:MultiplierFactorNumeric>\n" +
                "\t\t\t<cbc:Amount currencyID=\"COP\">10000.00</cbc:Amount>\n" +
                "\t\t\t<cbc:BaseAmount currencyID=\"COP\">100000.00</cbc:BaseAmount>\n" +
                "\t\t</cac:AllowanceCharge> Aplica cuando hay descuentos para el producto o servicio-->\n" +
                "\t\t<cac:TaxTotal>\n" +
                "\t\t\t<cbc:TaxAmount currencyID=\"COP\">"+taxAmount+"</cbc:TaxAmount><!--Sumatoria de los TaxAmount de los tax subtotal-->\t\t\t\n" +
                "\t\t\t<cac:TaxSubtotal>\n" +
                "\t\t\t\t<cbc:TaxableAmount currencyID=\"COP\">"+String.format("%.2f", amount).replaceAll(",",".")+"</cbc:TaxableAmount>\t\t\t\n" +
                "\t\t\t\t<cbc:TaxAmount currencyID=\"COP\">"+taxAmount+"</cbc:TaxAmount>\n" +
                "\t\t\t\t<cac:TaxCategory>\n" +
                "\t\t\t\t\t<cbc:Percent>"+taxPercent+"</cbc:Percent>\n" +
                "\t\t\t\t\t<cac:TaxScheme>\n" +
                "\t\t\t\t\t\t<cbc:ID>"+taxId+"</cbc:ID>\n" +
                "\t\t\t\t\t\t<cbc:Name>"+taxName+"</cbc:Name>\n" +
                "\t\t\t\t\t</cac:TaxScheme>\n" +
                "\t\t\t\t</cac:TaxCategory>\n" +
                "\t\t\t</cac:TaxSubtotal>\n" +
                "\t\t</cac:TaxTotal>\n"+
                "\t\t<cac:Item>\n" +
                "\t\t\t<cbc:Description>"+description+"</cbc:Description>\n" +
                "\t\t\t<cac:StandardItemIdentification>\n" +
                "\t\t\t\t<cbc:ID schemeID=\"999\">"+itemId+"</cbc:ID>\n" +
                "\t\t\t</cac:StandardItemIdentification>\n" +
                "\t\t</cac:Item>\n" +
                "\t\t<cac:Price>\n" +
                "\t\t\t<cbc:PriceAmount currencyID=\"COP\">"+String.format("%.2f", individualPrice).replaceAll(",",".")+"</cbc:PriceAmount>\n" +
                "\t\t\t<cbc:BaseQuantity unitCode=\"B7\">"+baseQuantity+"</cbc:BaseQuantity>\n" +
                "\t\t</cac:Price>\n" +
                "\t</cac:DebitNoteLine>";


        return invoiceLineEsqueleton;

    }



    public Long readDatav2(Integer type, String fechaO, Integer days, String fileMaster, String fileDetail, String sistema){

        try {
            List<MasterResponse> getResponseGeneric = new ArrayList<>();


            if(type == 1){
                getResponseGeneric = file(fileMaster, fileDetail);
            }else{

                String fecha = fechaO;



                for(int i=0; i<=days; i++){

                    System.out.println(fecha);


                    List<MasterResponse> getResponseGeneric2 = new ArrayList<>();
                    if(type == 2){
                        getResponseGeneric2 = sia(fecha);
                    }

                    if(type == 3){
                        getResponseGeneric2 = quipu(fecha);
                    }

                    if(type == 4){
                        getResponseGeneric2 = hermes(fecha);
                    }

                    if(type == 5){
                        getResponseGeneric2 = admisiones(fecha);
                    }

                    for(MasterResponse item : getResponseGeneric2){
                        getResponseGeneric.add(item);
                    }


                    Date date = new SimpleDateFormat("yyyy-MM-dd").parse(fecha);
                    Calendar c = Calendar.getInstance();
                    c.setTime(date);
                    c.add(Calendar.DATE, 1);
                    Date newDate = c.getTime();
                    fecha = new SimpleDateFormat("yyyy-MM-dd").format(newDate);

                }

            }

            for(MasterResponse item : getResponseGeneric){
                System.out.println("-----------------------------------------------------------------------------------------");




                String respFisc = item.getIn_clirespfiscal().replaceAll(" ","").replaceAll("%20","");
                //if(type == 4){
                    //respFisc = "";
                //}

                if(newMethod){
                    /*billDAO.poblar_tmpmaestro( data_obj.get("enc_PREFIJO").toString().replace(" ","%20"),
                            data_obj.get("enc_NRORECIBO").toString().replace(" ","%20"),
                            new Long(data_obj.get("enc_SEDE").toString().replace(" ","%20")),
                            new Long(data_obj.get("enc_VIGENCIA").toString().replace(" ","%20")),
                            data_obj.get("enc_NITEMES").toString().replace(" ","%20"),
                            data_obj.get("enc_NOMBRESEDE").toString().replace(" ","%20"),
                            data_obj.get("enc_EMPRESAQUIPU").toString().replace(" ","%20"),
                            date1,
                            new Long(data_obj.get("enc_TIPODOC").toString().replace(" ","%20")),
                            new Long(data_obj.get("enc_FORMAPAGO").toString().replace(" ","%20")),
                            date2,
                            new Long(data_obj.get("enc_MEDIOPAGO").toString().replace(" ","%20")),
                            new Long(data_obj.get("enc_DESCUENTO").toString().replace(" ","%20")),
                            new Double(data_obj.get("enc_SUBTOTAL").toString().replace(" ","%20")),
                            new Double(data_obj.get("enc_TOTAL").toString().replace(" ","%20")),
                            data_obj.get("enc_OBSERVACIONES").toString().replace(" ","%20"),
                            data_obj.get("enc_DATOS_PAGO").toString().replace(" ","%20"),
                            data_obj.get("enc_CIUDAD_SEDE").toString().replace(" ","%20"),
                            data_obj.get("enc_DIR_SEDE").toString().replace(" ","%20"),
                            data_obj.get("enc_TELEF_SEDE").toString().replace(" ","%20"),
                            data_obj.get("enc_EMAIL_SEDE_EMISION").toString().replace(" ","%20"),
                            new Long(data_obj.get("enc_CLINATU").toString().replace(" ","%20")),
                            data_obj.get("enc_CLINOMB").toString().replace(" ","%20"),
                            data_obj.get("enc_CLI_TIPOID").toString().replace(" ","%20"),
                            data_obj.get("enc_CLI_NUMID").toString().replace(" ","%20"),
                            data_obj.get("enc_CLICIUDAD").toString().replace(" ","%20"),
                            data_obj.get("enc_CLIDIR").toString().replace(" ","%20"),
                            data_obj.get("enc_CLITELEF").toString().replace(" ","%20"),
                            data_obj.get("enc_CLIEMAIL").toString().replace(" ","%20"),
                            data_obj.get("enc_CLIRESPFIS").toString().replace(" ","%20"),
                            new Long(data_obj.get("enc_CLIRESPIVA").toString().replace(" ","%20")),
                            new Long(1),
                            new Long(1),
                            in_num_docuhijosend
                    );*/


                }else{



                System.out.println("http://168.176.6.92:8982/v1/unalfe/master?in_prefijo="+item.getIn_prefijo().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_nrorecibo="+item.getIn_nrorecibo().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_sede="+item.getIn_sede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_vigencia="+item.getIn_vigencia().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_ciaquipu="+item.getIn_ciaquipu().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_nombresede="+item.getIn_nombresede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_empresaquipu="+item.getIn_empresaquipu().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_fecemic="+item.getIn_fecemic().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_tipodoc="+item.getIn_tipodoc().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_formapago="+item.getIn_formapago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_fecpago="+item.getIn_fecpago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_mediopago="+item.getIn_mediopago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_descuento="+item.getIn_descuento().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_subtotal="+item.getIn_subtotal().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_total="+item.getIn_total().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_observaciones="+item.getIn_observaciones().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_datos_pago="+item.getIn_datos_pago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_ciudad_sede="+item.getIn_ciudad_sede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_dir_sede="+item.getIn_dir_sede().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_telef_sede="+item.getIn_telef_sede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_email_sede_emision="+item.getIn_email_sede_emision().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clinatu="+item.getIn_clinatu().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clinomb="+item.getIn_clinomb().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_clitipoid="+item.getIn_clitipoid().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clinumid="+item.getIn_clinumid().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_cliciudad="+item.getIn_cliciudad().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clidir="+item.getIn_clidir().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_clitelelef="+item.getIn_clitelelef().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_cliemail="+item.getIn_cliemail().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_clirespfiscal="+respFisc.replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clirespiva="+item.getIn_clirespiva().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_origen="+item.getIn_origen().replaceAll("null","")
                        +"&in_forma_datos_origen="+item.getIn_forma_datos_origen().replaceAll("null","")
                        +"&in_num_docuhijo="+item.getIn_num_docuhijo().replaceAll("null","").replaceAll("^(?i)null$", "").replaceAll("\t","").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&sistema="+sistema
                );
                URL url = new URL("http://168.176.6.92:8982/v1/unalfe/master?in_prefijo="+item.getIn_prefijo().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_nrorecibo="+item.getIn_nrorecibo().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_sede="+item.getIn_sede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_vigencia="+item.getIn_vigencia().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_ciaquipu="+item.getIn_ciaquipu().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_nombresede="+item.getIn_nombresede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_empresaquipu="+item.getIn_empresaquipu().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_fecemic="+item.getIn_fecemic().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_tipodoc="+item.getIn_tipodoc().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_formapago="+item.getIn_formapago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_fecpago="+item.getIn_fecpago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_mediopago="+item.getIn_mediopago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_descuento="+item.getIn_descuento().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_subtotal="+item.getIn_subtotal().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_total="+item.getIn_total().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_observaciones="+item.getIn_observaciones().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_datos_pago="+item.getIn_datos_pago().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_ciudad_sede="+item.getIn_ciudad_sede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_dir_sede="+item.getIn_dir_sede().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_telef_sede="+item.getIn_telef_sede().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_email_sede_emision="+item.getIn_email_sede_emision().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clinatu="+item.getIn_clinatu().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clinomb="+item.getIn_clinomb().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_clitipoid="+item.getIn_clitipoid().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clinumid="+item.getIn_clinumid().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_cliciudad="+item.getIn_cliciudad().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clidir="+item.getIn_clidir().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_clitelelef="+item.getIn_clitelelef().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_cliemail="+item.getIn_cliemail().replaceAll(" ","%20").replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&in_clirespfiscal="+respFisc.replaceAll(" ","%20").replaceAll("null","")
                        +"&in_clirespiva="+item.getIn_clirespiva().replaceAll(" ","%20").replaceAll("null","")
                        +"&in_origen="+item.getIn_origen().replaceAll("null","")
                        +"&in_forma_datos_origen="+item.getIn_forma_datos_origen().replaceAll("null","")
                        +"&in_num_docuhijo="+item.getIn_num_docuhijo().replaceAll("null","").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                        +"&sistema="+sistema
                );//your url i.e fetch data from .
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");

                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                os.write((new String("")).getBytes());
                os.flush();
                os.close();


                System.out.println("RESPONSE CODE: "+conn.getResponseCode());
                System.out.println("MESSAGE CODE: "+conn.getResponseMessage());

                if (conn.getResponseCode() != 200) {
                    System.out.println("Failed : HTTP Error code : "
                            + conn.getResponseCode());
                    //throw new RuntimeException("Failed : HTTP Error code : "
                      //      + conn.getResponseCode());
                }
                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;
                while ((output = br.readLine()) != null) {
                    System.out.println("OUTPUT: "+output);
                }
                conn.disconnect();
                }


                List<DetailResponse> getResponseGenericDetail = item.getIn_detail();


                for(DetailResponse detalle : getResponseGenericDetail){

                    System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

                    String docNumHijo = detalle.getIn_num_docuhijo();

                    if(detalle.getIn_num_docuhijo().equals("%20")){
                        docNumHijo = detalle.getIn_nrorecibo();
                    }

                    System.out.println("http://168.176.6.92:8982/v1/unalfe/detail?in_prefijo="+detalle.getIn_prefijo().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_nrorecibo="+detalle.getIn_nrorecibo().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_sede="+detalle.getIn_sede().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_vigencia="+detalle.getIn_vigencia()
                            +"&in_ciaquipu="+detalle.getIn_ciaquipu().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_cantidad="+detalle.getIn_cantidad().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_vlrunit="+detalle.getIn_vlrunit().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_pct_desc="+detalle.getIn_pct_desc().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_codigo="+detalle.getIn_codigo().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_nombre="+detalle.getIn_nombre().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll("\r","%20")
                            +"&in_descrip="+detalle.getIn_descrip().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll("\r","%20")
                            +"&in_num_docuhijo="+docNumHijo.replaceAll(" ","%20").replaceAll("^(?i)null$", "").replaceAll("%20",""));
                    URL url = new URL("http://168.176.6.92:8982/v1/unalfe/detail?in_prefijo="+detalle.getIn_prefijo().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_nrorecibo="+detalle.getIn_nrorecibo().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_sede="+detalle.getIn_sede().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_vigencia="+detalle.getIn_vigencia()
                            +"&in_ciaquipu="+detalle.getIn_ciaquipu().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_cantidad="+detalle.getIn_cantidad().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_vlrunit="+detalle.getIn_vlrunit().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_pct_desc="+detalle.getIn_pct_desc().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_codigo="+detalle.getIn_codigo().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                            +"&in_nombre="+detalle.getIn_nombre().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\r","%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_descrip="+detalle.getIn_descrip().replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\r","%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_num_docuhijo="+docNumHijo.replaceAll(" ","%20").replaceAll("^(?i)null$", ""));//your url i.e fetch data from ."\n"


                    HttpURLConnection conn2 = (HttpURLConnection)
                            url.openConnection();
                    conn2.setRequestMethod("POST");
                    conn2.setRequestProperty("Content-Type", "application/json");

                    conn2.setDoOutput(true);
                    OutputStream os2 = conn2.getOutputStream();
                    os2.write((new String("")).getBytes());
                    os2.flush();
                    os2.close();


                    System.out.println("RESPONSE CODE: "+conn2.getResponseCode());
                    System.out.println("MESSAGE CODE: "+conn2.getResponseMessage());

                    if (conn2.getResponseCode() != 200) {
                        System.out.println("Failed : HTTP Error code : "
                                + conn2.getResponseCode());
                        //throw new RuntimeException("Failed : HTTP Error code : "
                                //+ conn2.getResponseCode());
                    }
                    InputStreamReader in2 = new InputStreamReader(conn2.getInputStream());
                    BufferedReader br2 = new BufferedReader(in2);
                    String output2;
                    while ((output2 = br2.readLine()) != null) {
                        System.out.println("OUTPUT: "+output2);
                    }
                    conn2.disconnect();

                    System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                }
                System.out.println("--------------------------------------------------------------------------------------");

            }

            return new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }


    public Long readData(Integer type, String fecha, String master, String detail, String sistema){

        try{

            List<List<String>> masterDataList = new ArrayList<>();
            List<List<String>> detailDataList = new ArrayList<>();

            if(type == 1){

                String row = "";

                BufferedReader csvReader = null;
                try {
                    csvReader = new BufferedReader(new FileReader("/home/appadmin/archivosfe/"+master+".csv"));
                    while ((row = csvReader.readLine()) != null) {
                        String[] masterData = row.split(";", -1);
                        System.out.println("-------------------------------HEADER------------------------------");
                        System.out.println("LENGTH: "+masterData.length);
                        System.out.println("LIST: "+masterData.toString());
                        List<String> masterDataAsList = Arrays.asList(masterData);
                        masterDataList.add(masterDataAsList);
                    }
                    csvReader.close();
                } catch (Exception e) {
                        e.printStackTrace();
                }
                try {
                    csvReader = new BufferedReader(new FileReader("/home/appadmin/archivosfe/"+detail+".csv"));
                    while ((row = csvReader.readLine()) != null) {
                        String[] detailData = row.split(";",-1);
                       // detailData = insert(detailData,"2020",3);
                        System.out.println("-------------------------------DETAIL------------------------------");
                        System.out.println("LENGTH: "+detailData.length);
                        System.out.println("LIST: "+detailData.toString());
                        List<String> detailDataListAsList = Arrays.asList(detailData);
                        detailDataList.add(detailDataListAsList);
                    }
                    csvReader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                if(type == 2){
                    try {
                        URL urlGet = new URL("http://168.176.6.92:8983/v1/unalfe/master?fecha="+fecha);//your url i.e fetch data from .
                        HttpURLConnection conn2 = (HttpURLConnection) urlGet.openConnection();
                        conn2.setRequestMethod("GET");
                        conn2.setRequestProperty("Accept", "application/json");

                        if (conn2.getResponseCode() != 200) {
                            throw new RuntimeException("Failed : HTTP Error code : "
                                    + conn2.getResponseCode());
                        }

                        InputStreamReader in = new InputStreamReader(conn2.getInputStream());
                        BufferedReader br = new BufferedReader(in);
                        String output;
                        JSONObject data_obj = new JSONObject();
                        while ((output = br.readLine()) != null) {
                            System.out.println("OUTPUT:"+output);
                            String [] listaTemporalMaestros = output.substring(1,output.length()-1).split("},");
                            if(output.equals("[]")){
                                System.out.println("NO HAY FACTURAS ESE DIA");
                                return new Long(0);
                            }
                            for(String element: listaTemporalMaestros){
                            System.out.println("ELEMENT: "+element);
                            data_obj = new JSONObject(element+"}");
                            System.out.println("OUTPUT_JSON: " + data_obj.toString());
                            ArrayList<String> ar = new ArrayList<String>();
                            ar.add(data_obj.get("enc_PREFIJO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_NRORECIBO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_SEDE").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_VIGENCIA").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_NITEMES").toString().replaceAll("null",""));
                            ar.add(data_obj.get("enc_NOMBRESEDE").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                            ar.add(data_obj.get("enc_EMPRESAQUIPU").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_FECEMIC").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_TIPODOC").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_FORMAPAGO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_FECPAGO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_MEDIOPAGO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_DESCUENTO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_SUBTOTAL").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_TOTAL").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_OBSERVACIONES").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_DATOS_PAGO").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CIUDAD_SEDE").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_DIR_SEDE").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                            ar.add(data_obj.get("enc_TELEF_SEDE").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_EMAIL_SEDE_EMISION").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLINATU").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLINOMB").toString().replaceAll("&","_").replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLI_TIPOID").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLI_NUMID").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLICIUDAD").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLIDIR").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                            ar.add(data_obj.get("enc_CLITELEF").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLIEMAIL").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLIRESPFIS").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_CLIRESPIVA").toString().replaceAll(" ", "%20"));
                            ar.add(data_obj.get("enc_NRORECIBO").toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                            masterDataList.add(ar);


                            try {
                                URL urlGet2 = new URL("http://168.176.6.92:8983/v1/unalfe/detail?det_nrorecibo="+data_obj.get("enc_NRORECIBO").toString());//your url i.e fetch data from .
                                HttpURLConnection conn22 = (HttpURLConnection) urlGet2.openConnection();
                                conn22.setRequestMethod("GET");
                                conn22.setRequestProperty("Accept", "application/json");

                                if (conn22.getResponseCode() != 200) {
                                    throw new RuntimeException("Failed : HTTP Error code : "
                                            + conn22.getResponseCode());
                                }

                                InputStreamReader in2 = new InputStreamReader(conn22.getInputStream());
                                BufferedReader br2 = new BufferedReader(in2);
                                String output2;
                                JSONObject data_obj2 = new JSONObject();
                                while ((output2 = br2.readLine()) != null) {
                                    System.out.println(output2);
                                    data_obj2 = new JSONObject(output2.substring(1,output2.length()-1));
                                    ArrayList<String> arDet = new ArrayList<String>();
                                    arDet.add(data_obj2.get("det_PREFIJO").toString());
                                    arDet.add(data_obj2.get("det_NRORECIBO").toString());
                                    arDet.add(data_obj2.get("det_SEDE").toString());
                                    arDet.add((2022+""));
                                    arDet.add(data_obj2.get("det_NITEMES").toString());
                                    arDet.add(data_obj2.get("det_CANTIDA").toString());
                                    arDet.add(data_obj2.get("det_VLRUNIT").toString());
                                    arDet.add(data_obj2.get("det_PCT_DESC").toString());
                                    arDet.add(data_obj2.get("det_CODIGO").toString());
                                    arDet.add(data_obj2.get("det_NOMBRE").toString());
                                    arDet.add(data_obj2.get("det_DESCRIP").toString());
                                    arDet.add(data_obj2.get("det_NRORECIBO").toString());
                                    detailDataList.add(arDet);
                                }

                            }catch(Exception e){
                                e.printStackTrace();
                                return new Long(0);
                            }
                            }
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                        return new Long(0);
                    }



                    }else{
                    if(type == 3){

                        try {
                            List<MasterQuipu> headerList = billDAO.getMasterQuipu(fecha);


                            for(int i = 0; i < headerList.size();i++){
                                MasterQuipu tmp = headerList.get(i);
                                ArrayList<String> ar = new ArrayList<String>();
                                try{
                                    ar.add(tmp.getENC_PREFIJO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                ar.add(tmp.getENC_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_VIGENCIA().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_NITEMES());
                                try{
                                    ar.add(tmp.getENC_NOMBRESEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                ar.add(tmp.getENC_EMPRESAQUIPU().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_FECEMIC().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_TIPODOC().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_FORMAPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                try{
                                    ar.add(tmp.getENC_FECPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                ar.add(tmp.getENC_MEDIOPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_DESCUENTO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_SUBTOTAL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_TOTAL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                try{
                                    ar.add(tmp.getENC_OBSERVACIONES().toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_DATOS_PAGO().toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_CIUDAD_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));

                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_DIR_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_TELEF_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_EMAIL_SEDE_EMISION().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                ar.add(tmp.getENC_CLINATU().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_CLINOMB().toString().replaceAll("&","_").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                ar.add(tmp.getENC_CLI_TIPOID().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                try{
                                    ar.add(tmp.getENC_CLI_NUMID().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }try{
                                    ar.add(tmp.getENC_CLICIUDAD().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }  try{
                                    ar.add(tmp.getENC_CLIDIR().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_CLITELEF().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_CLIEMAIL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                try{
                                    ar.add(tmp.getENC_CLIRESPFIS().toString().replaceAll(" ", "").replaceAll("^(?i)null$", ""));
                                }catch(NullPointerException e){
                                    ar.add("".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", ""));
                                }
                                try{
                                    ar.add(tmp.getENC_CLIRESPIVA().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }catch(NullPointerException e){
                                    ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", ""));
                                }
                                try{
                                    if(tmp.getENC_TIPODOC() == new Long(1)){
                                        ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                                    }else{
                                        ar.add(tmp.getENC_NUM_DOCUHIJO().toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                                    }
                                }catch(NullPointerException e){
                                    ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                }
                                System.out.print("ARRAY: ");
                                for (String element: ar) {
                                    System.out.print(element+", ");
                                }
                                System.out.println("");
                                System.out.println("SIZE: "+ar.size());

                                masterDataList.add(ar);
                                try {
                                    List<DetailQuipu> detailList = billDAO.getDetailsQuipu(tmp.getENC_PREFIJO(),
                                                                                           tmp.getENC_NRORECIBO(),
                                                                                           tmp.getENC_SEDE(),
                                                                                           tmp.getENC_NITEMES(),
                                                                                           tmp.getENC_NUM_DOCUHIJO()
                                            );
                                    for(int j = 0; j < detailList.size();j++){
                                        DetailQuipu tmp2 = detailList.get(j);
                                        ArrayList<String> arDet = new ArrayList<String>();
                                        System.out.println(tmp2);
                                        arDet.add(tmp2.getDET_PREFIJO().toString().replace(" ","%20"));
                                        arDet.add(tmp2.getDET_NRORECIBO().toString().replace(" ","%20"));
                                        arDet.add(tmp2.getDET_SEDE().toString().replace(" ","%20"));
                                        arDet.add((2022+"").replace(" ","%20"));
                                        arDet.add(tmp2.getDET_CIAQUIPU().toString().replace(" ","%20"));
                                        arDet.add(tmp2.getDET_CANTIDA().toString().replace(" ","%20"));
                                        arDet.add(tmp2.getDET_VLRUNIT().toString().replace(" ","%20"));
                                        arDet.add(tmp2.getDET_PCT_DESC().toString().replace(" ","%20"));
                                        try{
                                            arDet.add(tmp2.getDET_CODIGO().toString().replace(" ","%20"));
                                        }catch(NullPointerException e){
                                            arDet.add(" ".toString().replace(" ","%20"));
                                        }
                                        try{
                                            arDet.add(tmp2.getDET_NOMBRE().toString().replace(" ","%20"));
                                        }catch(NullPointerException e){
                                            arDet.add(" ".toString().replace(" ","%20"));
                                        }
                                        try{
                                            arDet.add(tmp2.getDET_DESCRIP().toString().replace(" ","%20"));
                                        }catch(NullPointerException e){
                                            arDet.add(" ".toString().replace(" ","%20"));
                                        }
                                        try{
                                            arDet.add(tmp2.getDET_NUM_DOCUHIJO().toString().replace(" ","%20"));
                                        }catch(NullPointerException e){
                                            arDet.add(" ".toString().replace(" ","%20"));
                                        }
                                        System.out.println("OUTPUT2: "+arDet.toString());
                                        detailDataList.add(arDet);
                                    }

                                }catch(Exception e){
                                    e.printStackTrace();
                                    return new Long(0);
                                }

                            }


                        }catch(Exception e){
                            e.printStackTrace();
                            return new Long(0);
                        }


                    }else{
                        if(type==5){

                            HttpURLConnection conn22 = (HttpURLConnection) new URL("https://uninscripciones.unal.edu.co/sitio/services/facturacionWebService/datos-facturacion-pines").openConnection();
                            conn22.setRequestProperty("Accept", "application/json");
                            conn22.setRequestProperty("Content-Type", "application/json");
                            conn22.setRequestMethod("POST");
                            conn22.setDoOutput(true);
                            String jsonInputString = "{\"fechaInscripcion\" : \""+fecha+"\"}";

                            byte[] authEncBytes = Base64.encodeBase64("facturacionElectronicaWS:rugywu5u6t".getBytes());
                            String authStringEnc = new String(authEncBytes);

                            conn22.setRequestProperty("Authorization", "Basic " + authStringEnc);


                            try(OutputStream os = conn22.getOutputStream()) {
                                byte[] input = jsonInputString.getBytes();
                                os.write(input, 0, input.length);
                            }

                            OutputStream os = conn22.getOutputStream();
                            os.write((new String("")).getBytes());
                            os.flush();
                            os.close();


                            System.out.println("RESPONSE CODE: "+conn22.getResponseCode());
                            System.out.println("MESSAGE CODE: "+conn22.getResponseMessage());

                            if (conn22.getResponseCode() != 200) {
                                throw new RuntimeException("Failed : HTTP Error code : "
                                        + conn22.getResponseCode());
                            }

                            InputStreamReader in = new InputStreamReader(conn22.getInputStream());
                            BufferedReader br = new BufferedReader(in);
                            String output;
                            while ((output = br.readLine()) != null) {
                                JSONArray array = new JSONArray(output);
                                for(Object o: array){
                                    if ( o instanceof JSONObject ) {
                                        JSONObject element = (JSONObject)o;
                                        ArrayList<String> ar = new ArrayList<String>();
                                        ar.add(element.get("in_prefijo").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_nrorecibo").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_sede").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_vigencia").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_ciaquipu").toString().replaceAll("null",""));
                                        ar.add(element.get("in_nombresede").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                        ar.add(element.get("in_empresaquipu").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_fecemic").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_tipodoc").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_formapago").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_fecpago").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_mediopago").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_descuento").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_subtotal").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_total").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_observaciones").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_datos_pago").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_ciudad_sede").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_dir_sede").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                        ar.add(element.get("in_telef_sede").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_email_sede_emision").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clinatu").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clinomb").toString().replaceAll("&","_").replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clitipoid").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clinumid").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_cliciudad").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clidir").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                        ar.add(element.get("in_clitelelef").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_cliemail").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clirespfiscal").toString().replaceAll(" ", "%20"));
                                        ar.add(element.get("in_clirespiva").toString().replaceAll(" ", "%20"));
                                        if(element.get("in_num_docuhijo").toString().equals("null")){
                                            ar.add(element.get("in_nrorecibo").toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                                        }else{
                                            ar.add(element.get("in_num_docuhijo").toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                                        }
                                        masterDataList.add(ar);

                                        JSONObject detailNow = new JSONObject(element.get("in_detail").toString());
                                        ArrayList<String> arDet = new ArrayList<String>();
                                        arDet.add(detailNow.get("tdet_prefijo").toString());
                                        arDet.add(detailNow.get("tdet_nrorecibo").toString());
                                        arDet.add(detailNow.get("tdet_sede").toString());
                                        arDet.add((2022+""));
                                        arDet.add(detailNow.get("tdet_ciaquipu").toString());
                                        arDet.add(detailNow.get("tdet_cantidad").toString());
                                        arDet.add(detailNow.get("tdet_vlrunit").toString());
                                        arDet.add(detailNow.get("tenc_descuento").toString());
                                        arDet.add(detailNow.get("tdet_codigo").toString());
                                        arDet.add(detailNow.get("tdet_nombre").toString());
                                        arDet.add(detailNow.get("tdet_descrip").toString());
                                        arDet.add(detailNow.get("tdet_nrorecibo").toString());
                                        detailDataList.add(arDet);
                                    }
                                }
                            }
                            conn22.disconnect();
                        }else{
                            if(type==7){
                                String row = "";

                                System.out.println("7");
                                BufferedReader csvReader = null;
                                try {
                                    csvReader = new BufferedReader(new FileReader("/home/appadmin/archivosfe/"+master+".csv"));
                                    while ((row = csvReader.readLine()) != null) {
                                        String[] masterData = row.split(";", -1);
                                        System.out.println("-------------------------------HEADER------------------------------");
                                        System.out.println("LENGTH: "+masterData.length);
                                        System.out.println("LIST: "+masterData.toString());
                                        List<String> masterDataAsList = Arrays.asList(masterData);
                                        masterDataList.add(masterDataAsList);
                                    }
                                    csvReader.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    csvReader = new BufferedReader(new FileReader("/home/appadmin/archivosfe/"+detail+".csv"));
                                    while ((row = csvReader.readLine()) != null) {
                                        String[] detailData = row.split(";",-1);
                                        // detailData = insert(detailData,"2020",3);
                                        System.out.println("-------------------------------DETAIL------------------------------");
                                        System.out.println("LENGTH: "+detailData.length);
                                        System.out.println("LIST: "+detailData.toString());
                                        List<String> detailDataListAsList = Arrays.asList(detailData);
                                        detailDataList.add(detailDataListAsList);
                                    }
                                    csvReader.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }else{
                                if(type==6){
                                    HttpURLConnection conn22 = (HttpURLConnection) new URL("http://localhost:8589/v1/editorial/master?fecha="+fecha).openConnection();
                                    conn22.setRequestProperty("Accept", "application/json");
                                    conn22.setRequestProperty("Content-Type", "application/json");
                                    conn22.setRequestMethod("POST");
                                    conn22.setDoOutput(true);





                                    OutputStream os = conn22.getOutputStream();
                                    os.write((new String("")).getBytes());
                                    os.flush();
                                    os.close();


                                    System.out.println("RESPONSE CODE: "+conn22.getResponseCode());
                                    System.out.println("MESSAGE CODE: "+conn22.getResponseMessage());

                                    if (conn22.getResponseCode() != 200) {
                                        throw new RuntimeException("Failed : HTTP Error code : "
                                                + conn22.getResponseCode());
                                    }

                                    InputStreamReader in = new InputStreamReader(conn22.getInputStream());
                                    BufferedReader br = new BufferedReader(in);
                                    String output;
                                    while ((output = br.readLine()) != null) {
                                        JSONArray array = new JSONArray(output);
                                        for(Object o: array){
                                            if ( o instanceof JSONObject ) {
                                                JSONObject element = (JSONObject)o;
                                                ArrayList<String> ar = new ArrayList<String>();
                                                ar.add(element.get("enc_PREFIJO").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_NRORECIBO").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_SEDE").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_VIGENCIA").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_NITEMES").toString().replaceAll("null",""));
                                                ar.add(element.get("enc_NOMBRESEDE").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                                ar.add(element.get("enc_EMPRESAQUIPU").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_FECEMIC").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_TIPODOC").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_FORMAPAGO").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_FECPAGO").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_MEDIOPAGO").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_DESCUENTO").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_SUBTOTAL").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_TOTAL").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_OBSERVACIONES").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_DATOS_PAGO").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_CIUDAD_SEDE").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_DIR_SEDE").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                                ar.add(element.get("enc_TELEF_SEDE").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_EMAIL_SEDE_EMISION").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_CLINATU").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_CLINOMB").toString().replaceAll("&","_").replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_CLI_TIPOID").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_CLI_NUMID").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_CLICIUDAD").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_CLIDIR").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                                ar.add(element.get("enc_CLITELEF").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_CLIEMAIL").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_CLIRESPFIS").toString().replaceAll(" ", "%20"));
                                                ar.add(element.get("enc_CLIRESPIVA").toString().replaceAll(" ", "%20"));
                                                if(element.get("enc_NRORECIBO").toString().equals("null")){
                                                    ar.add(element.get("enc_NRORECIBO").toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                                                }else{
                                                    ar.add(element.get("enc_NRORECIBO").toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                                                }
                                                masterDataList.add(ar);

                                            }
                                        }
                                    }
                                    conn22.disconnect();



                                    //---DETALLES EDITORIAL---
                                    HttpURLConnection conn223 = (HttpURLConnection) new URL("http://localhost:8589/v1/editorial/detail?fecha="+fecha).openConnection();
                                    conn223.setRequestProperty("Accept", "application/json");
                                    conn223.setRequestProperty("Content-Type", "application/json");
                                    conn223.setRequestMethod("POST");
                                    conn223.setDoOutput(true);





                                    OutputStream os2 = conn223.getOutputStream();
                                    os2.write((new String("")).getBytes());
                                    os2.flush();
                                    os2.close();


                                    System.out.println("RESPONSE CODE: "+conn223.getResponseCode());
                                    System.out.println("MESSAGE CODE: "+conn223.getResponseMessage());

                                    if (conn223.getResponseCode() != 200) {
                                        throw new RuntimeException("Failed : HTTP Error code : "
                                                + conn223.getResponseCode());
                                    }

                                    InputStreamReader in2 = new InputStreamReader(conn223.getInputStream());
                                    BufferedReader br2 = new BufferedReader(in2);
                                    String output2;
                                    while ((output2 = br2.readLine()) != null) {
                                        JSONArray array = new JSONArray(output2);
                                        for(Object o: array){
                                            if ( o instanceof JSONObject ) {
                                                JSONObject detailNow = (JSONObject)o;

                                                ArrayList<String> arDet = new ArrayList<String>();
                                                arDet.add(detailNow.get("det_PREFIJO").toString());
                                                arDet.add(detailNow.get("det_NRORECIBO").toString());
                                                arDet.add(detailNow.get("det_SEDE").toString());
                                                arDet.add((2022+""));
                                                arDet.add(detailNow.get("det_NITEMES").toString());
                                                arDet.add(detailNow.get("det_CANTIDA").toString());
                                                arDet.add(detailNow.get("det_VLRUNIT").toString());
                                                arDet.add(detailNow.get("det_PCT_DESC").toString());
                                                arDet.add(detailNow.get("det_CODIGO").toString());
                                                arDet.add(detailNow.get("det_NOMBRE").toString());
                                                arDet.add("");
                                                arDet.add(detailNow.get("factura_RELACIONADA").toString());
                                                detailDataList.add(arDet);
                                            }
                                        }
                                    }
                                    conn223.disconnect();
                                }else{
                                    if(type==8){

                                        HttpURLConnection conn22 = (HttpURLConnection) new URL("http://168.176.18.10:8080/maestro/date?date="+fecha).openConnection();
                                        conn22.setRequestProperty("Accept", "application/json");
                                        conn22.setRequestMethod("GET");
                                        //String jsonInputString = "{\"fechaInscripcion\" : \""+fecha+"\"}";

                                        //byte[] authEncBytes = Base64.encodeBase64("facturacionElectronicaWS:rugywu5u6t".getBytes());
                                        //String authStringEnc = new String(authEncBytes);

                                        //conn22.setRequestProperty("Authorization", "Basic " + authStringEnc);


                                        //try(OutputStream os = conn22.getOutputStream()) {
                                            //byte[] input = jsonInputString.getBytes();
                                            //os.write(input, 0, input.length);
                                        //}

                                        //OutputStream os = conn22.getOutputStream();
                                        //os.write((new String("")).getBytes());
                                        //os.flush();
                                        //os.close();

                                        System.out.println("ESTOY EN EL BACK");
                                        System.out.println("RESPONSE CODE: "+conn22.getResponseCode());
                                        System.out.println("MESSAGE CODE: "+conn22.getResponseMessage());

                                        if (conn22.getResponseCode() != 200) {
                                            throw new RuntimeException("Failed : HTTP Error code : "
                                                    + conn22.getResponseCode());
                                        }

                                        InputStreamReader in = new InputStreamReader(conn22.getInputStream());
                                        BufferedReader br = new BufferedReader(in);
                                        String output;
                                        while ((output = br.readLine()) != null) {
                                            JSONArray array = new JSONArray(output);
                                            for(Object o: array){
                                                if ( o instanceof JSONObject ) {
                                                    JSONObject element = (JSONObject)o;
                                                    ArrayList<String> ar = new ArrayList<String>();
                                                    ar.add(element.get("enc_PREFIJO").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_NRORECIB").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_SEDE").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_VIGENCIA").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_NITEMES").toString().replaceAll("null",""));
                                                    ar.add(element.get("enc_NOMBRESEDE").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                                    ar.add(element.get("enc_EMPRESAQUIPU").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_FECEMIC").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_TIPODOC").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_FORMAPAGO").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_FECPAGO").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_MEDIOPAGO").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_DESCUENTO").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_SUBTOTAL").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_TOTAL").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_OBSERVACIONES").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_DATOS_PAGO").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_CIUDAD_SEDE").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_DIR_SEDE").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                                    ar.add(element.get("enc_TELEF_SEDE").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_EMAIL_SEDE_EMISION").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_CLINATU").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_CLINOMB").toString().replaceAll("&","_").replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_CLI_TIPOID").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_CLI_NUMID").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_CLICIUDAD").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_CLIDIR").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                                                    ar.add(element.get("enc_CLITELEF").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_CLIEMAIL").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_CLIRESPFIS").toString().replaceAll(" ", "%20"));
                                                    ar.add(element.get("enc_CLIRESPIVA").toString().replaceAll(" ", "%20"));
                                                    if(element.get("enc_DOCU_HIJO").toString().equals("null")){
                                                        ar.add(element.get("enc_DOCU_HIJO").toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                                                    }else{
                                                        ar.add(element.get("enc_DOCU_HIJO").toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                                                    }
                                                    masterDataList.add(ar);


                                                }
                                            }
                                        }
                                        conn22.disconnect();




                                        HttpURLConnection conn223 = (HttpURLConnection) new URL("http://168.176.18.10:8080/detalle/date?date="+fecha).openConnection();
                                        conn223.setRequestProperty("Accept", "application/json");
                                        conn223.setRequestMethod("GET");
                                        //String jsonInputString = "{\"fechaInscripcion\" : \""+fecha+"\"}";

                                        //byte[] authEncBytes = Base64.encodeBase64("facturacionElectronicaWS:rugywu5u6t".getBytes());
                                        //String authStringEnc = new String(authEncBytes);

                                        //conn22.setRequestProperty("Authorization", "Basic " + authStringEnc);


                                        //try(OutputStream os = conn22.getOutputStream()) {
                                        //byte[] input = jsonInputString.getBytes();
                                        //os.write(input, 0, input.length);
                                        //}

                                        //OutputStream os223 = conn223.getOutputStream();
                                        //os223.write((new String("")).getBytes());
                                        //os223.flush();
                                        //os223.close();


                                        System.out.println("RESPONSE CODE: "+conn223.getResponseCode());
                                        System.out.println("MESSAGE CODE: "+conn223.getResponseMessage());

                                        if (conn223.getResponseCode() != 200) {
                                            throw new RuntimeException("Failed : HTTP Error code : "
                                                    + conn223.getResponseCode());
                                        }

                                        InputStreamReader in223 = new InputStreamReader(conn223.getInputStream());
                                        BufferedReader br223 = new BufferedReader(in223);
                                        String output223;
                                        while ((output223 = br223.readLine()) != null) {
                                            JSONArray array = new JSONArray(output223);
                                            for(Object o: array){
                                                if ( o instanceof JSONObject ) {
                                                    JSONObject detailNow = (JSONObject)o;
                                                    ArrayList<String> arDet = new ArrayList<String>();
                                                    arDet.add(detailNow.get("det_PREFIJO").toString());
                                                    arDet.add(detailNow.get("det_NRORECIBO").toString());
                                                    arDet.add(detailNow.get("det_SEDE").toString());
                                                    arDet.add((2022+""));
                                                    arDet.add(detailNow.get("det_NITEMES").toString());
                                                    arDet.add(detailNow.get("det_CANTIDA").toString());
                                                    arDet.add(detailNow.get("det_VLRUNIT").toString());
                                                    arDet.add(detailNow.get("det_PCT_DESC").toString());
                                                    arDet.add(detailNow.get("det_CODIGO").toString());
                                                    arDet.add(detailNow.get("det_NOMBRE").toString());
                                                    arDet.add(detailNow.get("det_DESCRIP").toString());
                                                    arDet.add(detailNow.get("det_DOCU_HIJO").toString());
                                                    detailDataList.add(arDet);


                                                }
                                            }
                                        }
                                        conn223.disconnect();



                                    }else{
                                    return new Long(-1);
                                }}
                            }
                        }
                    }
                }
            }

            try{
                for(int i = 0; i < masterDataList.size();i++){
                    List<String> data = masterDataList.get(i);
                    System.out.println(data.toString());
                    String numDocuHijo = data.get(1);
                    try{

                        if(new Integer(data.get(8)) == 1){
                            numDocuHijo = data.get(1);
                        }else{
                            numDocuHijo = data.get(31);
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                        System.out.println("ELEMENT: "+data.toString());
                    }





                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String dateInString = data.get(7);
                    String dateInString2 = data.get(10);


                    Date date1 = new Date();
                    Date date2 = new Date();
                    try {


                        Date utilDate = formatter.parse(dateInString);
                        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
                        date1 = sqlDate;

                        try{
                            Date utilDate2 = formatter.parse(dateInString2);
                            java.sql.Date sqlDate2 = new java.sql.Date(utilDate2.getTime());
                            date2 = sqlDate2;
                        }catch(Exception e){
                            date2 = sqlDate;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if(newMethod){
                        String in_num_docuhijosend = data.get(31);
                        if (data.get(31) == null){
                            in_num_docuhijosend = data.get(31);
                        }
                    billDAO.poblar_tmpmaestro(data.get(0),
                            data.get(1),
                            new Long(data.get(2)),
                            new Long(data.get(3)),
                            data.get(4),
                            data.get(5),
                            data.get(6),
                            date1,
                            new Long(data.get(8)),
                            new Long(data.get(9)),
                            date2,
                            new Long(data.get(11)),
                            new Long(data.get(12)),
                            new Double(data.get(13)),
                            new Double(data.get(14)),
                            data.get(15),
                            data.get(16),
                            data.get(17),
                            data.get(18),
                            data.get(19),
                            data.get(20),
                            new Long(data.get(21)),
                            data.get(22),
                            data.get(23),
                            data.get(24),
                            data.get(25),
                            data.get(26),
                            data.get(27),
                            data.get(28),
                            data.get(29),
                            new Long(data.get(30)),
                            new Long(1),
                            new Long(1),
                            in_num_docuhijosend
                    );
                    }else{

                    System.out.println("http://168.176.6.92:8982/v1/unalfe/master?in_prefijo="+data.get(0).replaceAll(" ","%20")
                            +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20")
                            +"&in_sede="+data.get(2).replaceAll(" ","%20")
                            +"&in_vigencia="+data.get(3).replaceAll(" ","%20")
                            +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20")
                            +"&in_nombresede="+data.get(5).replaceAll(" ","%20")
                            +"&in_empresaquipu="+data.get(6).replaceAll(" ","%20")
                            +"&in_fecemic="+data.get(7).replaceAll(" ","%20")
                            +"&in_tipodoc="+data.get(8).replaceAll(" ","%20")
                            +"&in_formapago="+data.get(9).replaceAll(" ","%20")
                            +"&in_fecpago="+data.get(10).replaceAll(" ","%20")
                            +"&in_mediopago="+data.get(11).replaceAll(" ","%20")
                            +"&in_descuento="+data.get(12).replaceAll(" ","%20")
                            +"&in_subtotal="+data.get(13).replaceAll(" ","%20")
                            +"&in_total="+data.get(14).replaceAll(" ","%20")
                            +"&in_observaciones="+data.get(15).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_datos_pago="+data.get(16).replaceAll(" ","%20")
                            +"&in_ciudad_sede="+data.get(17).replaceAll(" ","%20")
                            +"&in_dir_sede="+data.get(18).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_telef_sede="+data.get(19).replaceAll(" ","%20")
                            +"&in_email_sede_emision="+data.get(20).replaceAll(" ","%20")
                            +"&in_clinatu="+data.get(21).replaceAll(" ","%20")
                            +"&in_clinomb="+data.get(22).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_clitipoid="+data.get(23).replaceAll(" ","%20")
                            +"&in_clinumid="+data.get(24).replaceAll(" ","%20")
                            +"&in_cliciudad="+data.get(25).replaceAll(" ","%20")
                            +"&in_clidir="+data.get(26).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_clitelelef="+data.get(27).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_cliemail="+data.get(28).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            +"&in_clirespfiscal="+data.get(29).replaceAll(" ","%20")
                            +"&in_clirespiva="+data.get(30).replaceAll(" ","%20")
                            +"&in_origen="+1
                            +"&in_forma_datos_origen="+1
                            +"&in_num_docuhijo="+numDocuHijo.replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                            + "&sistema=" + sistema
                    );
                    try {
                        URL url = new URL("http://168.176.6.92:8982/v1/unalfe/master?in_prefijo=" + data.get(0).replaceAll(" ", "%20")
                                + "&in_nrorecibo=" + data.get(1).replaceAll(" ", "%20")
                                + "&in_sede=" + data.get(2).replaceAll(" ", "%20")
                                + "&in_vigencia=" + data.get(3).replaceAll(" ", "%20")
                                + "&in_ciaquipu=" + data.get(4).replaceAll(" ", "%20")
                                + "&in_nombresede=" + data.get(5).replaceAll(" ", "%20")
                                + "&in_empresaquipu=" + data.get(6).replaceAll(" ", "%20")
                                + "&in_fecemic=" + data.get(7).replaceAll(" ", "%20")
                                + "&in_tipodoc=" + data.get(8).replaceAll(" ", "%20")
                                + "&in_formapago=" + data.get(9).replaceAll(" ", "%20")
                                + "&in_fecpago=" + data.get(10).replaceAll(" ", "%20")
                                + "&in_mediopago=" + data.get(11).replaceAll(" ", "%20")
                                + "&in_descuento=" + data.get(12).replaceAll(" ", "%20")
                                + "&in_subtotal=" + data.get(13).replaceAll(" ", "%20")
                                + "&in_total=" + data.get(14).replaceAll(" ", "%20")
                                + "&in_observaciones=" + data.get(15).replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20").replaceAll("\t", "%20").replaceAll("#", "No\\.").replaceAll("\n", "%20")
                                + "&in_datos_pago=" + data.get(16).replaceAll(" ", "%20")
                                + "&in_ciudad_sede=" + data.get(17).replaceAll(" ", "%20")
                                + "&in_dir_sede=" + data.get(18).replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20").replaceAll("\t", "%20").replaceAll("#", "No\\.").replaceAll("\n", "%20")
                                + "&in_telef_sede=" + data.get(19).replaceAll(" ", "%20")
                                + "&in_email_sede_emision=" + data.get(20).replaceAll(" ", "%20")
                                + "&in_clinatu=" + data.get(21).replaceAll(" ", "%20")
                                + "&in_clinomb=" + data.get(22).replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20").replaceAll("\t", "%20").replaceAll("#", "No\\.").replaceAll("\n", "%20")
                                + "&in_clitipoid=" + data.get(23).replaceAll(" ", "%20")
                                + "&in_clinumid=" + data.get(24).replaceAll(" ", "%20")
                                + "&in_cliciudad=" + data.get(25).replaceAll(" ", "%20")
                                + "&in_clidir=" + data.get(26).replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20").replaceAll("\t", "%20").replaceAll("#", "No\\.").replaceAll("\n", "%20").replaceAll("\u001C", "")
                                + "&in_clitelelef=" + data.get(27).replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20").replaceAll("\t", "%20").replaceAll("#", "No\\.").replaceAll("\n", "%20")
                                + "&in_cliemail=" + data.get(28).replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20").replaceAll("\t", "%20").replaceAll("#", "No\\.").replaceAll("\n", "%20")
                                + "&in_clirespfiscal=" + data.get(29).replaceAll(" ", "%20")
                                + "&in_clirespiva=" + data.get(30).replaceAll(" ", "%20")
                                + "&in_origen=" + 1
                                + "&in_forma_datos_origen=" + 1
                                + "&in_num_docuhijo=" + numDocuHijo.replaceAll("^(?i)null$", "%20").replaceAll("\t", "%20").replaceAll("#", "No\\.").replaceAll("\n", "%20")
                                + "&sistema=" + sistema
                        ); //your url i.e fetch data from .
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Content-Type", "application/json");

                        conn.setDoOutput(true);
                        OutputStream os = conn.getOutputStream();
                        os.write((new String("")).getBytes());
                        os.flush();
                        os.close();


                        System.out.println("RESPONSE CODE: " + conn.getResponseCode());
                        System.out.println("MESSAGE CODE: " + conn.getResponseMessage());

                        if (conn.getResponseCode() != 200) {
                            throw new RuntimeException("Failed : HTTP Error code : "
                                    + conn.getResponseCode());
                        }
                        InputStreamReader in = new InputStreamReader(conn.getInputStream());
                        BufferedReader br = new BufferedReader(in);
                        String output;
                        while ((output = br.readLine()) != null) {
                            System.out.println("OUTPUT: " + output);
                        }
                        conn.disconnect();
                    }catch(Exception e){
                       e.printStackTrace();
                    }

            Long Num1 = new Long(0);
            Long Num2 = new Long(0);
            Long Num3 = new Long(0);
            Long Num4 = new Long(0);



            try{

                Num1 = new Long(data.get(47));
            }catch(Exception e){

            }

            try{

                Num2 = new Long(data.get(48));
            }catch(Exception e){

            }

            try{

                Num3 = new Long(data.get(49));
            }catch(Exception e){

            }

            try{

                Num4 = new Long(data.get(50));
            }catch(Exception e){

            }



            System.out.println("POBLAR SALUD");
            System.out.println(data.size());

            if(data.size()>36){
                System.out.println("POBLAR SALUD - 2");
                billDAO.poblar_salud(data.get(0).replaceAll(" ","%20"),
                                     data.get(1).replaceAll(" ","%20"),
                                     new Long(data.get(2)),
                                     new Long(data.get(3)),
                                     data.get(4).replaceAll(" ","%20"),
                                     numDocuHijo.replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                     data.get(32).replaceAll(" ","%20"),
                                     data.get(33).replaceAll(" ","%20"),
                                     data.get(34).replaceAll(" ","%20"),
                                     data.get(35).replaceAll(" ","%20"),
                                     data.get(36).replaceAll(" ","%20"),
                                     data.get(37).replaceAll(" ","%20"),
                                     data.get(38).replaceAll(" ","%20"),
                                     data.get(39).replaceAll(" ","%20"),
                                     data.get(40).replaceAll(" ","%20"),
                                     data.get(41).replaceAll(" ","%20"),
                                     data.get(42).replaceAll(" ","%20"),
                                     data.get(43).replaceAll(" ","%20"),
                                     data.get(44).replaceAll(" ","%20"),
                                     data.get(45).replaceAll(" ","%20"),
                                     data.get(46).replaceAll(" ","%20"),
                                     Num1,
                                     Num2,
                                     Num3,
                                     Num4,
                                     data.get(51).replaceAll(" ","%20"),
                                     data.get(52).replaceAll(" ","%20"),
                                     sistema
                                    );
            }


            }
        }
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }


                for(int i = 0; i < detailDataList.size();i++){
                    try {
                List<String> data = detailDataList.get(i);

                System.out.print("---------------------------------------------------------");
                System.out.println("data stuff: "+data.toString());


                    int a = 02;
                    char elem = (char)a;
                    URL url = new URL("http://168.176.6.92:8982");
                    if(type==3){
                        System.out.println("http://168.176.6.92:8982/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2022"
                                +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                                +"&in_descrip="+data.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20")
                                +"&in_num_docuhijo="+data.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from ."\n"

                        url = new URL("http://168.176.6.92:8982/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2022"
                                +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll("¬","").replaceAll("\u0002","").replaceAll(Character.toString(elem),"")
                                +"&in_descrip="+data.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll("¬","").replaceAll("\u0002","").replaceAll(Character.toString(elem),"")
                                +"&in_num_docuhijo="+data.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from ."\n"

                    }
                    if(type==2){
                        System.out.println(data.toString());
                        System.out.println("http://168.176.6.92:8982/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2022"
                                +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(9).replaceAll("\t","%20").replaceAll("^(?i)null$", "%20").replaceAll(" ","%20")
                                +"&in_descrip="+data.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20")
                                +"&in_num_docuhijo="+data.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from .

                        url = new URL("http://168.176.6.92:8982/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2022"
                                +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll("¬","").replaceAll("\u0002","").replaceAll(Character.toString(elem),"")
                                +"&in_descrip="+data.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll("¬","").replaceAll("\u0002","").replaceAll(Character.toString(elem),"")
                                +"&in_num_docuhijo="+data.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from ."\n"

                    }
                    if(type==1||type==7){
                        String numDocuHijo;
                        if(data.size() == 10){
                            numDocuHijo = data.get(1);
                        }else{
                            numDocuHijo = data.get(10);
                        }
                        System.out.println("http://168.176.6.92:8982/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2022"
                                +"&in_ciaquipu="+data.get(3).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_descrip="+data.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_num_docuhijo="+numDocuHijo.replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from .

                        url = new URL("http://168.176.6.92:8982/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2022"
                                +"&in_ciaquipu="+data.get(3).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20").replaceAll("¬","").replaceAll("\u0002","").replaceAll(Character.toString(elem),"")
                                +"&in_descrip="+data.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20").replaceAll("¬","").replaceAll("\u0002","").replaceAll(Character.toString(elem),"")
                                +"&in_num_docuhijo="+numDocuHijo.replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from .

                    }


                    if(type==5||type==6||type==8){
                        System.out.println("http://168.176.6.92:8982/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2022"
                                +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(9).replaceAll("\t","%20").replaceAll("^(?i)null$", "%20").replaceAll(" ","%20")
                                +"&in_descrip="+data.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("¬","")
                                +"&in_num_docuhijo="+data.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from .




                        url = new URL("http://168.176.6.92:8982/v1/unalfe/detail?in_prefijo="+data.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nrorecibo="+data.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_sede="+data.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vigencia=2022"
                                +"&in_ciaquipu="+data.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_cantidad="+data.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_vlrunit="+data.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_pct_desc="+data.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_codigo="+data.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")
                                +"&in_nombre="+data.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll("¬","").replaceAll("\u0002","").replaceAll(Character.toString(elem),"")
                                +"&in_descrip="+data.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll("¬","").replaceAll("\u0002","").replaceAll(Character.toString(elem),"")
                                +"&in_num_docuhijo="+data.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"));//your url i.e fetch data from ."\n"

                    }

                HttpURLConnection conn = (HttpURLConnection)
                url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");

                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                os.write((new String("")).getBytes());
                os.flush();
                os.close();


                System.out.println("RESPONSE CODE: "+conn.getResponseCode());
                System.out.println("MESSAGE CODE: "+conn.getResponseMessage());

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP Error code : "
                            + conn.getResponseCode());
                }
                InputStreamReader in = new InputStreamReader(conn.getInputStream());
                BufferedReader br = new BufferedReader(in);
                String output;
                while ((output = br.readLine()) != null) {
                    System.out.println("OUTPUT: "+output);
                }
                conn.disconnect();
                    }catch(Exception e){
                        e.printStackTrace();
                        return new Long(0);
                    }
                }


            return new Long(1);

        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }


    Long getTablaFinal(){
        try{

            billDAO.asigna_consecutivo_dian();
            return new Long(1);

        }catch(Exception e){
            return new Long(0);
        }

    }


    Long updateTable(String estado,
                     String PREFIJO,
                     String NRORECIBO,
                     Long SEDE,
                     Long VIGENCIA,
                     String CIAQUIPU){
        try{

            billDAO.updateTable( estado,
                                 PREFIJO,
                                 NRORECIBO,
                                 SEDE,
                                 VIGENCIA,
                                 CIAQUIPU,NRORECIBO,"OK");

            return new Long(1);

        }catch(Exception e){
            return new Long(0);
        }

    }


    public List<MasterQuipu> masterQuipu(String fecha){
        return billDAO.getMasterQuipu( fecha );
    }


    public List<String> masterFile(){

        String row = "";
        BufferedReader csvReader = null;

        List<String> responseList = new ArrayList<String>();

        try {
            csvReader = new BufferedReader(new FileReader("/home/appadmin/archivosfe/FE_MAESTRO.csv"));
            while ((row = csvReader.readLine()) != null) {
                responseList.add(row);
            }
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseList;
    }


    public List<String> masterFile2(String filename){

        String row = "";
        BufferedReader csvReader = null;

        List<String> responseList = new ArrayList<String>();

        try {
            csvReader = new BufferedReader(new FileReader("/home/appadmin/archivosfe/"+filename+".csv"));
            while ((row = csvReader.readLine()) != null) {
                responseList.add(row);
            }
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseList;
    }


    public List<MasterResponse> quipu(String fecha){

        List<MasterResponse> response = new ArrayList<MasterResponse>();

        List<MasterQuipu> listMasterQuipu = masterQuipu(fecha);

        for(int i = 0; i<listMasterQuipu.size();i++){

            MasterQuipu tmp = listMasterQuipu.get(i);
            ArrayList<String> ar = new ArrayList<String>();

            ar.add(tmp.getENC_PREFIJO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            ar.add(tmp.getENC_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_VIGENCIA().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_NITEMES());
            ar.add(tmp.getENC_NOMBRESEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_EMPRESAQUIPU().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_FECEMIC().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_TIPODOC().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_FORMAPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_FECPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            ar.add(tmp.getENC_MEDIOPAGO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_DESCUENTO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_SUBTOTAL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_TOTAL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_OBSERVACIONES().toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_DATOS_PAGO().toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            ar.add(tmp.getENC_CIUDAD_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_DIR_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_TELEF_SEDE().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_EMAIL_SEDE_EMISION().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            ar.add(tmp.getENC_CLINATU().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_CLINOMB().toString().replaceAll("&","_").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            ar.add(tmp.getENC_CLI_TIPOID().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            try{
                ar.add(tmp.getENC_CLI_NUMID().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }try{
                ar.add(tmp.getENC_CLICIUDAD().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }  try{
                ar.add(tmp.getENC_CLIDIR().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_CLITELEF().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_CLIEMAIL().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_CLIRESPFIS().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                ar.add(tmp.getENC_CLIRESPIVA().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }catch(NullPointerException e){
                ar.add(" ".toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }
            try{
                if(tmp.getENC_TIPODOC() == new Long(1)){
                    ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                }else{
                    ar.add(tmp.getENC_NUM_DOCUHIJO().toString().replaceAll(" ","%20"));//your url i.e fetch data from .
                }
            }catch(NullPointerException e){
                ar.add(tmp.getENC_NRORECIBO().toString().replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
            }

            String numDocuHijo = ar.get(1);
            try{

                if(new Integer(ar.get(8)) == 1){
                    numDocuHijo = ar.get(1);
                }else{
                    numDocuHijo = ar.get(31);
                }
            }catch(Exception e){
                e.printStackTrace();
                System.out.println("ELEMENT: "+ar.toString());
            }

            MasterResponse newMaster = new MasterResponse(ar.get(0).replaceAll(" ","%20"),
                    ar.get(1).replaceAll(" ","%20"),
                    ar.get(2).replaceAll(" ","%20"),
                    ar.get(3).replaceAll(" ","%20"),
                    ar.get(4).replaceAll(" ","%20"),
                    ar.get(5).replaceAll(" ","%20"),
                    ar.get(6).replaceAll(" ","%20"),
                    ar.get(7).replaceAll(" ","%20"),
                    ar.get(8).replaceAll(" ","%20"),
                    ar.get(9).replaceAll(" ","%20"),
                    ar.get(10).replaceAll(" ","%20"),
                    ar.get(11).replaceAll(" ","%20"),
                    ar.get(12).replaceAll(" ","%20"),
                    ar.get(13).replaceAll(" ","%20"),
                    ar.get(14).replaceAll(" ","%20"),
                    ar.get(15).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                    ar.get(16).replaceAll(" ","%20"),
                    ar.get(17).replaceAll(" ","%20"),
                    ar.get(18).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                    ar.get(19).replaceAll(" ","%20"),
                    ar.get(20).replaceAll(" ","%20"),
                    ar.get(21).replaceAll(" ","%20"),
                    ar.get(22).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                    ar.get(23).replaceAll(" ","%20"),
                    ar.get(24).replaceAll(" ","%20"),
                    ar.get(25).replaceAll(" ","%20"),
                    ar.get(26).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                    ar.get(27).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                    ar.get(28).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                    ar.get(29).replaceAll(" ","%20"),
                    ar.get(30).replaceAll(" ","%20"),
                    "1",
                    "1",
                    numDocuHijo.replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"));


            List<DetailResponse> filteredDetail = new ArrayList<>();


            List<DetailQuipu> listDetailsQuipu = billDAO.getDetailsQuipu(tmp.getENC_PREFIJO(),
                    tmp.getENC_NRORECIBO(),
                    tmp.getENC_SEDE(),
                    tmp.getENC_NITEMES(),
                    tmp.getENC_NUM_DOCUHIJO()
            );

            for(int j=0;j<listDetailsQuipu.size();j++){
                DetailQuipu tmp2 = listDetailsQuipu.get(j);
                ArrayList<String> arDet = new ArrayList<String>();

                arDet.add(tmp2.getDET_PREFIJO().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_NRORECIBO().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_SEDE().toString().replace(" ","%20"));
                arDet.add((2022+"").replace(" ","%20"));
                arDet.add(tmp2.getDET_CIAQUIPU().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_CANTIDA().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_VLRUNIT().toString().replace(" ","%20"));
                arDet.add(tmp2.getDET_PCT_DESC().toString().replace(" ","%20"));
                try{
                    arDet.add(tmp2.getDET_CODIGO().toString().replace(" ","%20"));
                }catch(NullPointerException e){
                    arDet.add(" ".toString().replace(" ","%20"));
                }
                try{
                    arDet.add(tmp2.getDET_NOMBRE().toString().replace(" ","%20"));
                }catch(NullPointerException e){
                    arDet.add(" ".toString().replace(" ","%20"));
                }
                try{
                    arDet.add(tmp2.getDET_DESCRIP().toString().replace(" ","%20"));
                }catch(NullPointerException e){
                    arDet.add(" ".toString().replace(" ","%20"));
                }
                try{
                    arDet.add(tmp2.getDET_NUM_DOCUHIJO().toString().replace(" ","%20"));
                }catch(NullPointerException e){
                    arDet.add(" ".toString().replace(" ","%20"));
                }


                DetailResponse detailToAdd = new DetailResponse(

                        arDet.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        "2022",
                        arDet.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                        arDet.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20"),
                        arDet.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20"),
                        arDet.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")

                );

                filteredDetail.add(detailToAdd);

            }
            newMaster.setIn_detail(filteredDetail);
            response.add(newMaster);
        }
        return response;
    }



    public List<MasterResponse> sia(String fecha) throws IOException {

        List<MasterResponse> response = new ArrayList<MasterResponse>();

        try {
            URL urlGet = new URL("http://168.176.6.92:8983/v1/unalfe/master?fecha="+fecha);//your url i.e fetch data from .
            HttpURLConnection conn2 = (HttpURLConnection) urlGet.openConnection();
            conn2.setRequestMethod("GET");
            conn2.setRequestProperty("Accept", "application/json");

            if (conn2.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn2.getResponseCode());
            }

            InputStreamReader in = new InputStreamReader(conn2.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            JSONObject data_obj = new JSONObject();
            while ((output = br.readLine()) != null) {
                String [] listaTemporalMaestros = output.substring(1,output.length()-1).split("},");
                for(String element: listaTemporalMaestros){
                    data_obj = new JSONObject(element+"}");
                    ArrayList<String> ar = new ArrayList<String>();
                    ar.add(data_obj.get("enc_PREFIJO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_NRORECIBO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_SEDE").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_VIGENCIA").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_NITEMES").toString().replaceAll("null",""));
                    ar.add(data_obj.get("enc_NOMBRESEDE").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                    ar.add(data_obj.get("enc_EMPRESAQUIPU").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_FECEMIC").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_TIPODOC").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_FORMAPAGO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_FECPAGO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_MEDIOPAGO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_DESCUENTO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_SUBTOTAL").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_TOTAL").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_OBSERVACIONES").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_DATOS_PAGO").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CIUDAD_SEDE").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_DIR_SEDE").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                    ar.add(data_obj.get("enc_TELEF_SEDE").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_EMAIL_SEDE_EMISION").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLINATU").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLINOMB").toString().replaceAll("&","_").replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLI_TIPOID").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLI_NUMID").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLICIUDAD").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLIDIR").toString().replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20").replaceAll(" ", "%20").replaceAll("^(?i)null$", "%20"));
                    ar.add(data_obj.get("enc_CLITELEF").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLIEMAIL").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLIRESPFIS").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_CLIRESPIVA").toString().replaceAll(" ", "%20"));
                    ar.add(data_obj.get("enc_NRORECIBO").toString().replaceAll(" ","%20"));//your url i.e fetch data from .

                    String numDocuHijo = ar.get(1);
                    try{

                        if(new Integer(ar.get(8)) == 1){
                            numDocuHijo = ar.get(1);
                        }else{
                            numDocuHijo = ar.get(31);
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }


                    MasterResponse newMaster = new MasterResponse(ar.get(0).replaceAll(" ","%20"),
                            ar.get(1).replaceAll(" ","%20"),
                            ar.get(2).replaceAll(" ","%20"),
                            ar.get(3).replaceAll(" ","%20"),
                            ar.get(4).replaceAll(" ","%20"),
                            ar.get(5).replaceAll(" ","%20"),
                            ar.get(6).replaceAll(" ","%20"),
                            ar.get(7).replaceAll(" ","%20"),
                            ar.get(8).replaceAll(" ","%20"),
                            ar.get(9).replaceAll(" ","%20"),
                            ar.get(10).replaceAll(" ","%20"),
                            ar.get(11).replaceAll(" ","%20"),
                            ar.get(12).replaceAll(" ","%20"),
                            ar.get(13).replaceAll(" ","%20"),
                            ar.get(14).replaceAll(" ","%20"),
                            ar.get(15).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                            ar.get(16).replaceAll(" ","%20"),
                            ar.get(17).replaceAll(" ","%20"),
                            ar.get(18).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                            ar.get(19).replaceAll(" ","%20"),
                            ar.get(20).replaceAll(" ","%20"),
                            ar.get(21).replaceAll(" ","%20"),
                            ar.get(22).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                            ar.get(23).replaceAll(" ","%20"),
                            ar.get(24).replaceAll(" ","%20"),
                            ar.get(25).replaceAll(" ","%20"),
                            ar.get(26).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                            ar.get(27).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                            ar.get(28).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                            ar.get(29).replaceAll(" ","%20"),
                            ar.get(30).replaceAll(" ","%20"),
                            "1",
                            "1",
                            numDocuHijo.replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"));


                    List<DetailResponse> filteredDetail = new ArrayList<>();

                    try {
                        URL urlGet2 = new URL("http://168.176.6.92:8983/v1/unalfe/detail?det_nrorecibo="+data_obj.get("enc_NRORECIBO").toString());//your url i.e fetch data from .
                        HttpURLConnection conn22 = (HttpURLConnection) urlGet2.openConnection();
                        conn22.setRequestMethod("GET");
                        conn22.setRequestProperty("Accept", "application/json");

                        if (conn22.getResponseCode() != 200) {
                            throw new RuntimeException("Failed : HTTP Error code : "
                                    + conn22.getResponseCode());
                        }

                        InputStreamReader in2 = new InputStreamReader(conn22.getInputStream());
                        BufferedReader br2 = new BufferedReader(in2);
                        String output2;
                        JSONObject data_obj2 = new JSONObject();
                        while ((output2 = br2.readLine()) != null) {
                            data_obj2 = new JSONObject(output2.substring(1,output2.length()-1));
                            ArrayList<String> arDet = new ArrayList<String>();
                            arDet.add(data_obj2.get("det_PREFIJO").toString());
                            arDet.add(data_obj2.get("det_NRORECIBO").toString());
                            arDet.add(data_obj2.get("det_SEDE").toString());
                            arDet.add((2022+""));
                            arDet.add(data_obj2.get("det_NITEMES").toString());
                            arDet.add(data_obj2.get("det_CANTIDA").toString());
                            arDet.add(data_obj2.get("det_VLRUNIT").toString());
                            arDet.add(data_obj2.get("det_PCT_DESC").toString());
                            arDet.add(data_obj2.get("det_CODIGO").toString());
                            arDet.add(data_obj2.get("det_NOMBRE").toString());
                            arDet.add(data_obj2.get("det_DESCRIP").toString());
                            arDet.add(data_obj2.get("det_NRORECIBO").toString());


                            DetailResponse detailToAdd = new DetailResponse(

                                    arDet.get(0).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(1).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(2).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    "2022",
                                    arDet.get(4).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(5).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(6).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(7).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(8).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                                    arDet.get(9).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20"),
                                    arDet.get(10).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20"),
                                    arDet.get(11).replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")

                            );

                            filteredDetail.add(detailToAdd);

                        }

                        newMaster.setIn_detail(filteredDetail);
                        response.add(newMaster);

                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }


        }catch(Exception e){
            e.printStackTrace();
        }
        return response;
    }



    public List<MasterResponse> hermes(String fecha) {

        List<MasterResponse> response = new ArrayList<MasterResponse>();

        try {
            URL urlGet = new URL("http://168.176.6.92:8984/v1/invoice/hermes?fecha="+fecha);//your url i.e fetch data from .
            HttpURLConnection conn2 = (HttpURLConnection) urlGet.openConnection();
            conn2.setRequestMethod("GET");
            conn2.setRequestProperty("Accept", "application/json");

            if (conn2.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn2.getResponseCode());
            }

            InputStreamReader in = new InputStreamReader(conn2.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            List <MasterResponse> responseList = new ArrayList<>();
            while ((output = br.readLine()) != null) {

                String [] listaTemporalMaestros = output.substring(1,output.length()-1).split("},");
                System.out.println(output);

                for(String element: listaTemporalMaestros){
                    try {
                        System.out.println("******************CHECK******************");
                        System.out.println(element);
                        JSONObject data_obj = new JSONObject(element + "}");
                        MasterResponse temp = new MasterResponse();



                        temp.setIn_prefijo(data_obj.get("in_prefijo").toString());
                        temp.setIn_nrorecibo(data_obj.get("in_nrorecibo").toString());
                        temp.setIn_sede(data_obj.get("in_sede").toString());
                        temp.setIn_vigencia(data_obj.get("in_vigencia").toString());
                        temp.setIn_ciaquipu(data_obj.get("in_ciaquipu").toString());
                        temp.setIn_nombresede(data_obj.get("in_nombresede").toString());
                        temp.setIn_empresaquipu(data_obj.get("in_empresaquipu").toString());
                        temp.setIn_fecemic(data_obj.get("in_fecemic").toString());
                        temp.setIn_tipodoc(data_obj.get("in_tipodoc").toString());
                        temp.setIn_formapago(data_obj.get("in_formapago").toString());
                        temp.setIn_fecpago(data_obj.get("in_fecpago").toString());
                        temp.setIn_mediopago(data_obj.get("in_mediopago").toString());
                        temp.setIn_descuento(data_obj.get("in_descuento").toString());
                        temp.setIn_subtotal(data_obj.get("in_subtotal").toString());
                        temp.setIn_total(data_obj.get("in_total").toString());
                        temp.setIn_observaciones(data_obj.get("in_observaciones").toString());
                        temp.setIn_datos_pago(data_obj.get("in_datos_pago").toString());
                        temp.setIn_ciudad_sede(data_obj.get("in_ciudad_sede").toString());
                        temp.setIn_dir_sede(data_obj.get("in_dir_sede").toString());
                        temp.setIn_telef_sede(data_obj.get("in_telef_sede").toString());
                        temp.setIn_email_sede_emision(data_obj.get("in_email_sede_emision").toString());
                        temp.setIn_clinatu(data_obj.get("in_clinatu").toString());
                        temp.setIn_clinomb(data_obj.get("in_clinomb").toString());
                        temp.setIn_clitipoid(data_obj.get("in_clitipoid").toString());
                        temp.setIn_clinumid(data_obj.get("in_clinumid").toString());
                        temp.setIn_cliciudad(data_obj.get("in_cliciudad").toString());
                        temp.setIn_clidir(data_obj.get("in_clidir").toString());
                        temp.setIn_clitelelef(data_obj.get("in_clitelelef").toString());
                        temp.setIn_cliemail(data_obj.get("in_cliemail").toString());
                        temp.setIn_clirespfiscal(data_obj.get("in_clirespfiscal").toString());
                        temp.setIn_clirespiva(data_obj.get("in_clirespiva").toString());
                        temp.setIn_origen(data_obj.get("in_origen").toString());
                        temp.setIn_cliemail(data_obj.get("in_cliemail").toString());
                        temp.setIn_forma_datos_origen(data_obj.get("in_forma_datos_origen").toString());
                        temp.setIn_num_docuhijo(data_obj.get("in_num_docuhijo").toString());

                        List<DetailResponse> responseListDet = new ArrayList<>();


                        String[] listaTemporalDetalles = data_obj.get("in_detail").toString().substring(1, data_obj.get("in_detail").toString().length() - 1).split("},");

                        for (String detail : listaTemporalDetalles) {
                            JSONObject data_objD = new JSONObject(detail + "}");
                            DetailResponse tempD = new DetailResponse();

                            tempD.setIn_prefijo(data_objD.get("in_prefijo").toString());
                            tempD.setIn_nrorecibo(data_objD.get("in_nrorecibo").toString());
                            tempD.setIn_sede(data_objD.get("in_sede").toString());
                            tempD.setIn_vigencia(data_objD.get("in_vigencia").toString());
                            tempD.setIn_ciaquipu(data_objD.get("in_ciaquipu").toString());
                            tempD.setIn_cantidad(data_objD.get("in_cantidad").toString());
                            tempD.setIn_vlrunit(data_objD.get("in_vlrunit").toString());
                            tempD.setIn_pct_desc(data_objD.get("in_pct_desc").toString());
                            tempD.setIn_codigo(data_objD.get("in_codigo").toString());
                            tempD.setIn_nombre(data_objD.get("in_nombre").toString());
                            tempD.setIn_descrip(data_objD.get("in_descrip").toString());
                            tempD.setIn_num_docuhijo(data_objD.get("in_num_docuhijo").toString());

                            responseListDet.add(tempD);
                        }

                        temp.setIn_detail(responseListDet);

                        responseList.add(temp);
                    }catch(Exception e){
                        e.printStackTrace();
                        System.out.println("----------------------------------------------------------------");
                        System.out.println(element);
                    }
                }
            }


            return responseList;


        }catch(Exception e){
            e.printStackTrace();
        }
        return response;
    }



    public List<MasterResponse> admisiones(String fecha) {

        List<MasterResponse> response = new ArrayList<MasterResponse>();

        try {
            URL urlGet = new URL("http://168.176.6.92:8984/v1/invoice/hermes?fecha="+fecha);//your url i.e fetch data from .
            HttpURLConnection conn2 = (HttpURLConnection) urlGet.openConnection();
            conn2.setRequestMethod("GET");
            conn2.setRequestProperty("Accept", "application/json");

            if (conn2.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn2.getResponseCode());
            }

            InputStreamReader in = new InputStreamReader(conn2.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            List <MasterResponse> responseList = new ArrayList<>();
            while ((output = br.readLine()) != null) {

                String [] listaTemporalMaestros = output.substring(1,output.length()-1).split("},");

                for(String element: listaTemporalMaestros){
                    JSONObject data_obj = new JSONObject(element+"}");
                    MasterResponse temp = new MasterResponse();

                    temp.setIn_prefijo(data_obj.get("in_prefijo").toString());
                    temp.setIn_nrorecibo(data_obj.get("in_nrorecibo").toString());
                    temp.setIn_sede(data_obj.get("in_sede").toString());
                    temp.setIn_vigencia(data_obj.get("in_vigencia").toString());
                    temp.setIn_ciaquipu(data_obj.get("in_ciaquipu").toString());
                    temp.setIn_nombresede(data_obj.get("in_nombresede").toString());
                    temp.setIn_empresaquipu(data_obj.get("in_empresaquipu").toString());
                    temp.setIn_fecemic(data_obj.get("in_fecemic").toString());
                    temp.setIn_tipodoc(data_obj.get("in_tipodoc").toString());
                    temp.setIn_formapago(data_obj.get("in_formapago").toString());
                    temp.setIn_fecpago(data_obj.get("in_fecpago").toString());
                    temp.setIn_mediopago(data_obj.get("in_mediopago").toString());
                    temp.setIn_descuento(data_obj.get("in_descuento").toString());
                    temp.setIn_subtotal(data_obj.get("in_subtotal").toString());
                    temp.setIn_total(data_obj.get("in_total").toString());
                    temp.setIn_observaciones(data_obj.get("in_observaciones").toString());
                    temp.setIn_datos_pago(data_obj.get("in_datos_pago").toString());
                    temp.setIn_ciudad_sede(data_obj.get("in_ciudad_sede").toString());
                    temp.setIn_dir_sede(data_obj.get("in_dir_sede").toString());
                    temp.setIn_telef_sede(data_obj.get("in_telef_sede").toString());
                    temp.setIn_email_sede_emision(data_obj.get("in_email_sede_emision").toString());
                    temp.setIn_clinatu(data_obj.get("in_clinatu").toString());
                    temp.setIn_clinomb(data_obj.get("in_clinomb").toString());
                    temp.setIn_clitipoid(data_obj.get("in_clitipoid").toString());
                    temp.setIn_clinumid(data_obj.get("in_clinumid").toString());
                    temp.setIn_cliciudad(data_obj.get("in_cliciudad").toString());
                    temp.setIn_clidir(data_obj.get("in_clidir").toString());
                    temp.setIn_clitelelef(data_obj.get("in_clitelelef").toString());
                    temp.setIn_cliemail(data_obj.get("in_cliemail").toString());
                    temp.setIn_clirespfiscal(data_obj.get("in_clirespfiscal").toString());
                    temp.setIn_clirespiva(data_obj.get("in_clirespiva").toString());
                    temp.setIn_origen(data_obj.get("in_origen").toString());
                    temp.setIn_cliemail(data_obj.get("in_cliemail").toString());
                    temp.setIn_forma_datos_origen(data_obj.get("in_forma_datos_origen").toString());
                    temp.setIn_num_docuhijo(data_obj.get("in_num_docuhijo").toString());

                    List <DetailResponse> responseListDet = new ArrayList<>();


                    String [] listaTemporalDetalles = data_obj.get("in_detail").toString().substring(1,data_obj.get("in_detail").toString().length()-1).split("},");

                    for(String detail: listaTemporalDetalles){
                        JSONObject data_objD = new JSONObject(detail+"}");
                        DetailResponse tempD = new DetailResponse();

                        tempD.setIn_prefijo(data_objD.get("in_prefijo").toString());
                        tempD.setIn_nrorecibo(data_objD.get("in_nrorecibo").toString());
                        tempD.setIn_sede(data_objD.get("in_sede").toString());
                        tempD.setIn_vigencia(data_objD.get("in_vigencia").toString());
                        tempD.setIn_ciaquipu(data_objD.get("in_ciaquipu").toString());
                        tempD.setIn_cantidad(data_objD.get("in_cantidad").toString());
                        tempD.setIn_vlrunit(data_objD.get("in_vlrunit").toString());
                        tempD.setIn_pct_desc(data_objD.get("in_pct_desc").toString());
                        tempD.setIn_codigo(data_objD.get("in_codigo").toString());
                        tempD.setIn_nombre(data_objD.get("in_nombre").toString());
                        tempD.setIn_descrip(data_objD.get("in_descrip").toString());
                        tempD.setIn_num_docuhijo(data_objD.get("in_num_docuhijo").toString());

                        responseListDet.add(tempD);
                    }

                    temp.setIn_detail(responseListDet);

                    responseList.add(temp);
                }
            }


            return responseList;


        }catch(Exception e){
            e.printStackTrace();
        }
        return response;
    }


    public List<MasterResponse> file(String fileMaster, String fileDetail) {

        List<MasterResponse> response = new ArrayList<MasterResponse>();


        List<String> masterString = masterFile2(fileMaster);
        List<String> detailString = detailFile2(fileDetail);

        List<String[]> detailList = new ArrayList<String[]>();
        List<String[]> masterList = new ArrayList<String[]>();



        for(int i = 0; i< detailString.size();i++){
            String[] detail = detailString.get(i).split(";");
            detailList.add(detail);
        }

        for(int i = 0; i< masterString.size();i++){
            String[] detail = masterString.get(i).split(";");
            masterList.add(detail);
        }


        for(int i = 0; i< masterString.size();i++){

            String[] master = masterString.get(i).split(";");

            String numDocuHijo = master[1];

            try{

                if(new Integer(master[8]) == 1){
                    numDocuHijo = master[1];
                }else{
                    numDocuHijo = master[31];
                }
            }catch(Exception e){
                e.printStackTrace();
                System.out.println("ELEMENT: "+master.toString());
            }


            System.out.println(masterString.get(i));
            System.out.println(master.length);


            MasterResponse newMaster = new MasterResponse(master[0].replaceAll(" ","%20"),
                                                          master[1].replaceAll(" ","%20"),
                                                          master[2].replaceAll(" ","%20"),
                                                          master[3].replaceAll(" ","%20"),
                                                          master[4].replaceAll(" ","%20"),
                                                          master[5].replaceAll(" ","%20"),
                                                          master[6].replaceAll(" ","%20"),
                                                          master[7].replaceAll(" ","%20"),
                                                          master[8].replaceAll(" ","%20"),
                                                          master[9].replaceAll(" ","%20"),
                                                          master[10].replaceAll(" ","%20"),
                                                          master[11].replaceAll(" ","%20"),
                                                          master[12].replaceAll(" ","%20"),
                                                          master[13].replaceAll(" ","%20"),
                                                          master[14].replaceAll(" ","%20"),
                                                          master[15].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                                          master[16].replaceAll(" ","%20"),
                                                          master[17].replaceAll(" ","%20"),
                                                          master[18].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                                          master[19].replaceAll(" ","%20"),
                                                          master[20].replaceAll(" ","%20"),
                                                          master[21].replaceAll(" ","%20"),
                                                          master[22].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                                          master[23].replaceAll(" ","%20"),
                                                          master[24].replaceAll(" ","%20"),
                                                          master[25].replaceAll(" ","%20"),
                                                          master[26].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                                          master[27].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                                          master[28].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"),
                                                          master[29].replaceAll(" ","%20"),
                                                          master[30].replaceAll(" ","%20"),
                                                          "1",
                                                          "1",
                                                          numDocuHijo.replaceAll("^(?i)null$", "%20").replaceAll("\t","%20").replaceAll("#","No\\.").replaceAll("\n","%20"));

            List<DetailResponse> filteredDetail = new ArrayList<>();

            for(int j = 0; j < detailList.size();j++){
                String[] element = detailList.get(j);
                if(element[1].equals(master[1]) && element[0].equals(master[0])){
                    String numDocuHijoD;
                    if(element.length == 10){
                        numDocuHijoD = element[1];
                    }else{
                        numDocuHijoD = element[10];
                    }
                    DetailResponse detailToAdd = new DetailResponse(

                            element[0].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[1].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[2].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            "2022",
                            element[3].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[4].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[5].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[6].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[7].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20"),
                            element[8].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20"),
                            element[9].replaceAll(" ","%20").replaceAll("^(?i)null$", "%20").replaceAll("#","No\\.").replaceAll("\t","%20").replaceAll("\n","%20"),
                            numDocuHijoD.replaceAll(" ","%20").replaceAll("^(?i)null$", "%20")

                    );

                    filteredDetail.add(detailToAdd);
                }
            }

            newMaster.setIn_detail(filteredDetail);

            response.add(newMaster);
        }



        return response;

    }



    public List<String> detailFile(){

        String row = "";
        BufferedReader csvReader = null;

        List<String> responseList = new ArrayList<String>();

        try {
            csvReader = new BufferedReader(new FileReader("/home/appadmin/archivosfe/FE_DETALLE.csv"));
            while ((row = csvReader.readLine()) != null) {
                responseList.add(row);
            }
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseList;
    }




    public List<String> detailFile2(String filename){

        String row = "";
        BufferedReader csvReader = null;

        List<String> responseList = new ArrayList<String>();

        try {
            csvReader = new BufferedReader(new FileReader("/home/appadmin/archivosfe/"+filename+".csv"));
            while ((row = csvReader.readLine()) != null) {
                responseList.add(row);
            }
            csvReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseList;
    }



    public List<DetailQuipu> detailQuipu(String PREFIJO,
                                    String NRORECIBO,
                                    Long SEDE,
                                    String CIAQUIPU,
                                     String NUMDOCUHIJO){
        return billDAO.getDetailsQuipu( PREFIJO,
                 NRORECIBO,
                 SEDE,
                 CIAQUIPU,
                NUMDOCUHIJO);
    }


    public Long updateTableData(String estado,
                                String nrorecibo,
                                String prefijo){

        try{

            billDAO.updateTableData( estado,
                    nrorecibo,
                    prefijo);

            return new Long(1);
        }catch(Exception e ){
            e.printStackTrace();

            return new Long(0);
        }


    }



    public Long updateTableDataP(String estado1,
                                String estado2){

        try{

            billDAO.updateTableDataP( estado1,
                    estado2);

            return new Long(1);
        }catch(Exception e ){
            e.printStackTrace();

            return new Long(0);
        }


    }


}