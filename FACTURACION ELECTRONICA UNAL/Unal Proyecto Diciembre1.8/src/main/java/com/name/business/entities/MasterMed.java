package com.name.business.entities;

public class MasterMed {

    private Long ENC_ID;
    private String ENC_PREFIJO;
    private String ENC_NRORECIBO;
    private Long ENC_SEDE;
    private Long ENC_VIGENCIA;
    private String ENC_CIAQUIPU;
    private Long ENC_NRO_ITEMES;
    private String ENC_NOMBRESEDE;
    private String ENC_EMPRESAQUIPU;
    private String ENC_FECEMIC;
    private Long ENC_TIPODOC;
    private Long ENC_FORMAPAGO;
    private String ENC_FECPAGO;
    private Long ENC_MEDIOPAGO;
    private Double ENC_DESCUENTO;
    private Double ENC_SUBTOTAL;
    private Double ENC_TOTAL;
    private String ENC_OBSERVACIONES;
    private String ENC_DATOS_PAGO;
    private String ENC_CIUDAD_SEDE;
    private String ENC_DIR_SEDE;
    private String ENC_TELEF_SEDE;
    private String ENC_EMAIL_SEDE_EMISION;
    private Long ENC_CLINATU;
    private String ENC_CLINOMB;
    private String ENC_CLITIPOID;
    private String ENC_CLINUMID;
    private String ENC_CLICIUDAD;
    private String ENC_CLIDIR;
    private String ENC_CLITELELEF;
    private String ENC_CLIEMAIL;
    private String ENC_CLIRESPFISCAL;
    private Long ENC_CLIRESPIVA;
    private String ENC_FEC_CARGA;
    private String ENC_ESTADO;
    private Long ENC_CONSDIAN;
    private Long ENC_ORIGEN;
    private Long ENC_FORMA_DATOS_ORIGEN;
    private String ENC_NUM_DOCUHIJO;

    //-----NEW DATA-----//

    private String ENC_COD_PRESTADOR;
    private String ENC_TIPODOC_USUARIO;
    private String ENC_NUM_IDENTIFICACION;
    private String ENC_PRIMER_NOMBRE;
    private String ENC_SEGUNDO_NOMBRE;
    private String ENC_TIPO_USUARIO;
    private String ENC_MODALIDAD_CONTRATO;
    private String ENC_COBERTURA;
    private String ENC_NUM_AUTORIZACION;
    private String ENC_NUM_MIPRES;
    private String ENC_NUM_ENTREGA_MIPRES;
    private String ENC_NUM_CONTRATO;
    private String ENC_POLIZA;
    private Long ENC_COPAGO;
    private Long ENC_CUOTA_MODERADORA;
    private Long ENC_CUOTA_RECUPERACION;
    private Long ENC_PAGOS_COMPARTIDOS;
    private String ENC_FECHA_INICIO_PERIODO;
    private String ENC_FECHA_FIN_PERIODO;


    public MasterMed(Long ENC_ID, String ENC_PREFIJO, String ENC_NRORECIBO, Long ENC_SEDE, Long ENC_VIGENCIA, String ENC_CIAQUIPU, Long ENC_NRO_ITEMES, String ENC_NOMBRESEDE, String ENC_EMPRESAQUIPU, String ENC_FECEMIC, Long ENC_TIPODOC, Long ENC_FORMAPAGO, String ENC_FECPAGO, Long ENC_MEDIOPAGO, Double ENC_DESCUENTO, Double ENC_SUBTOTAL, Double ENC_TOTAL, String ENC_OBSERVACIONES, String ENC_DATOS_PAGO, String ENC_CIUDAD_SEDE, String ENC_DIR_SEDE, String ENC_TELEF_SEDE, String ENC_EMAIL_SEDE_EMISION, Long ENC_CLINATU, String ENC_CLINOMB, String ENC_CLITIPOID, String ENC_CLINUMID, String ENC_CLICIUDAD, String ENC_CLIDIR, String ENC_CLITELELEF, String ENC_CLIEMAIL, String ENC_CLIRESPFISCAL, Long ENC_CLIRESPIVA, String ENC_FEC_CARGA, String ENC_ESTADO, Long ENC_CONSDIAN, Long ENC_ORIGEN, Long ENC_FORMA_DATOS_ORIGEN, String ENC_NUM_DOCUHIJO, String ENC_COD_PRESTADOR, String ENC_TIPODOC_USUARIO, String ENC_NUM_IDENTIFICACION, String ENC_PRIMER_NOMBRE, String ENC_SEGUNDO_NOMBRE, String ENC_TIPO_USUARIO, String ENC_MODALIDAD_CONTRATO, String ENC_COBERTURA, String ENC_NUM_AUTORIZACION, String ENC_NUM_MIPRES, String ENC_NUM_ENTREGA_MIPRES, String ENC_NUM_CONTRATO, String ENC_POLIZA, Long ENC_COPAGO, Long ENC_CUOTA_MODERADORA, Long ENC_CUOTA_RECUPERACION, Long ENC_PAGOS_COMPARTIDOS, String ENC_FECHA_INICIO_PERIODO, String ENC_FECHA_FIN_PERIODO) {
        this.ENC_ID = ENC_ID;
        this.ENC_PREFIJO = ENC_PREFIJO;
        this.ENC_NRORECIBO = ENC_NRORECIBO;
        this.ENC_SEDE = ENC_SEDE;
        this.ENC_VIGENCIA = ENC_VIGENCIA;
        this.ENC_CIAQUIPU = ENC_CIAQUIPU;
        this.ENC_NRO_ITEMES = ENC_NRO_ITEMES;
        this.ENC_NOMBRESEDE = ENC_NOMBRESEDE;
        this.ENC_EMPRESAQUIPU = ENC_EMPRESAQUIPU;
        this.ENC_FECEMIC = ENC_FECEMIC;
        this.ENC_TIPODOC = ENC_TIPODOC;
        this.ENC_FORMAPAGO = ENC_FORMAPAGO;
        this.ENC_FECPAGO = ENC_FECPAGO;
        this.ENC_MEDIOPAGO = ENC_MEDIOPAGO;
        this.ENC_DESCUENTO = ENC_DESCUENTO;
        this.ENC_SUBTOTAL = ENC_SUBTOTAL;
        this.ENC_TOTAL = ENC_TOTAL;
        this.ENC_OBSERVACIONES = ENC_OBSERVACIONES;
        this.ENC_DATOS_PAGO = ENC_DATOS_PAGO;
        this.ENC_CIUDAD_SEDE = ENC_CIUDAD_SEDE;
        this.ENC_DIR_SEDE = ENC_DIR_SEDE;
        this.ENC_TELEF_SEDE = ENC_TELEF_SEDE;
        this.ENC_EMAIL_SEDE_EMISION = ENC_EMAIL_SEDE_EMISION;
        this.ENC_CLINATU = ENC_CLINATU;
        this.ENC_CLINOMB = ENC_CLINOMB;
        this.ENC_CLITIPOID = ENC_CLITIPOID;
        this.ENC_CLINUMID = ENC_CLINUMID;
        this.ENC_CLICIUDAD = ENC_CLICIUDAD;
        this.ENC_CLIDIR = ENC_CLIDIR;
        this.ENC_CLITELELEF = ENC_CLITELELEF;
        this.ENC_CLIEMAIL = ENC_CLIEMAIL;
        this.ENC_CLIRESPFISCAL = ENC_CLIRESPFISCAL;
        this.ENC_CLIRESPIVA = ENC_CLIRESPIVA;
        this.ENC_FEC_CARGA = ENC_FEC_CARGA;
        this.ENC_ESTADO = ENC_ESTADO;
        this.ENC_CONSDIAN = ENC_CONSDIAN;
        this.ENC_ORIGEN = ENC_ORIGEN;
        this.ENC_FORMA_DATOS_ORIGEN = ENC_FORMA_DATOS_ORIGEN;
        this.ENC_NUM_DOCUHIJO = ENC_NUM_DOCUHIJO;
        this.ENC_COD_PRESTADOR = ENC_COD_PRESTADOR;
        this.ENC_TIPODOC_USUARIO = ENC_TIPODOC_USUARIO;
        this.ENC_NUM_IDENTIFICACION = ENC_NUM_IDENTIFICACION;
        this.ENC_PRIMER_NOMBRE = ENC_PRIMER_NOMBRE;
        this.ENC_SEGUNDO_NOMBRE = ENC_SEGUNDO_NOMBRE;
        this.ENC_TIPO_USUARIO = ENC_TIPO_USUARIO;
        this.ENC_MODALIDAD_CONTRATO = ENC_MODALIDAD_CONTRATO;
        this.ENC_COBERTURA = ENC_COBERTURA;
        this.ENC_NUM_AUTORIZACION = ENC_NUM_AUTORIZACION;
        this.ENC_NUM_MIPRES = ENC_NUM_MIPRES;
        this.ENC_NUM_ENTREGA_MIPRES = ENC_NUM_ENTREGA_MIPRES;
        this.ENC_NUM_CONTRATO = ENC_NUM_CONTRATO;
        this.ENC_POLIZA = ENC_POLIZA;
        this.ENC_COPAGO = ENC_COPAGO;
        this.ENC_CUOTA_MODERADORA = ENC_CUOTA_MODERADORA;
        this.ENC_CUOTA_RECUPERACION = ENC_CUOTA_RECUPERACION;
        this.ENC_PAGOS_COMPARTIDOS = ENC_PAGOS_COMPARTIDOS;
        this.ENC_FECHA_INICIO_PERIODO = ENC_FECHA_INICIO_PERIODO;
        this.ENC_FECHA_FIN_PERIODO = ENC_FECHA_FIN_PERIODO;
    }

    public Long getENC_ID() {
        return ENC_ID;
    }

    public void setENC_ID(Long ENC_ID) {
        this.ENC_ID = ENC_ID;
    }

    public String getENC_PREFIJO() {
        return ENC_PREFIJO;
    }

    public void setENC_PREFIJO(String ENC_PREFIJO) {
        this.ENC_PREFIJO = ENC_PREFIJO;
    }

    public String getENC_NRORECIBO() {
        return ENC_NRORECIBO;
    }

    public void setENC_NRORECIBO(String ENC_NRORECIBO) {
        this.ENC_NRORECIBO = ENC_NRORECIBO;
    }

    public Long getENC_SEDE() {
        return ENC_SEDE;
    }

    public void setENC_SEDE(Long ENC_SEDE) {
        this.ENC_SEDE = ENC_SEDE;
    }

    public Long getENC_VIGENCIA() {
        return ENC_VIGENCIA;
    }

    public void setENC_VIGENCIA(Long ENC_VIGENCIA) {
        this.ENC_VIGENCIA = ENC_VIGENCIA;
    }

    public String getENC_CIAQUIPU() {
        return ENC_CIAQUIPU;
    }

    public void setENC_CIAQUIPU(String ENC_CIAQUIPU) {
        this.ENC_CIAQUIPU = ENC_CIAQUIPU;
    }

    public Long getENC_NRO_ITEMES() {
        return ENC_NRO_ITEMES;
    }

    public void setENC_NRO_ITEMES(Long ENC_NRO_ITEMES) {
        this.ENC_NRO_ITEMES = ENC_NRO_ITEMES;
    }

    public String getENC_NOMBRESEDE() {
        return ENC_NOMBRESEDE;
    }

    public void setENC_NOMBRESEDE(String ENC_NOMBRESEDE) {
        this.ENC_NOMBRESEDE = ENC_NOMBRESEDE;
    }

    public String getENC_EMPRESAQUIPU() {
        return ENC_EMPRESAQUIPU;
    }

    public void setENC_EMPRESAQUIPU(String ENC_EMPRESAQUIPU) {
        this.ENC_EMPRESAQUIPU = ENC_EMPRESAQUIPU;
    }

    public String getENC_FECEMIC() {
        return ENC_FECEMIC;
    }

    public void setENC_FECEMIC(String ENC_FECEMIC) {
        this.ENC_FECEMIC = ENC_FECEMIC;
    }

    public Long getENC_TIPODOC() {
        return ENC_TIPODOC;
    }

    public void setENC_TIPODOC(Long ENC_TIPODOC) {
        this.ENC_TIPODOC = ENC_TIPODOC;
    }

    public Long getENC_FORMAPAGO() {
        return ENC_FORMAPAGO;
    }

    public void setENC_FORMAPAGO(Long ENC_FORMAPAGO) {
        this.ENC_FORMAPAGO = ENC_FORMAPAGO;
    }

    public String getENC_FECPAGO() {
        return ENC_FECPAGO;
    }

    public void setENC_FECPAGO(String ENC_FECPAGO) {
        this.ENC_FECPAGO = ENC_FECPAGO;
    }

    public Long getENC_MEDIOPAGO() {
        return ENC_MEDIOPAGO;
    }

    public void setENC_MEDIOPAGO(Long ENC_MEDIOPAGO) {
        this.ENC_MEDIOPAGO = ENC_MEDIOPAGO;
    }

    public Double getENC_DESCUENTO() {
        return ENC_DESCUENTO;
    }

    public void setENC_DESCUENTO(Double ENC_DESCUENTO) {
        this.ENC_DESCUENTO = ENC_DESCUENTO;
    }

    public Double getENC_SUBTOTAL() {
        return ENC_SUBTOTAL;
    }

    public void setENC_SUBTOTAL(Double ENC_SUBTOTAL) {
        this.ENC_SUBTOTAL = ENC_SUBTOTAL;
    }

    public Double getENC_TOTAL() {
        return ENC_TOTAL;
    }

    public void setENC_TOTAL(Double ENC_TOTAL) {
        this.ENC_TOTAL = ENC_TOTAL;
    }

    public String getENC_OBSERVACIONES() {
        return ENC_OBSERVACIONES;
    }

    public void setENC_OBSERVACIONES(String ENC_OBSERVACIONES) {
        this.ENC_OBSERVACIONES = ENC_OBSERVACIONES;
    }

    public String getENC_DATOS_PAGO() {
        return ENC_DATOS_PAGO;
    }

    public void setENC_DATOS_PAGO(String ENC_DATOS_PAGO) {
        this.ENC_DATOS_PAGO = ENC_DATOS_PAGO;
    }

    public String getENC_CIUDAD_SEDE() {
        return ENC_CIUDAD_SEDE;
    }

    public void setENC_CIUDAD_SEDE(String ENC_CIUDAD_SEDE) {
        this.ENC_CIUDAD_SEDE = ENC_CIUDAD_SEDE;
    }

    public String getENC_DIR_SEDE() {
        return ENC_DIR_SEDE;
    }

    public void setENC_DIR_SEDE(String ENC_DIR_SEDE) {
        this.ENC_DIR_SEDE = ENC_DIR_SEDE;
    }

    public String getENC_TELEF_SEDE() {
        return ENC_TELEF_SEDE;
    }

    public void setENC_TELEF_SEDE(String ENC_TELEF_SEDE) {
        this.ENC_TELEF_SEDE = ENC_TELEF_SEDE;
    }

    public String getENC_EMAIL_SEDE_EMISION() {
        return ENC_EMAIL_SEDE_EMISION;
    }

    public void setENC_EMAIL_SEDE_EMISION(String ENC_EMAIL_SEDE_EMISION) {
        this.ENC_EMAIL_SEDE_EMISION = ENC_EMAIL_SEDE_EMISION;
    }

    public Long getENC_CLINATU() {
        return ENC_CLINATU;
    }

    public void setENC_CLINATU(Long ENC_CLINATU) {
        this.ENC_CLINATU = ENC_CLINATU;
    }

    public String getENC_CLINOMB() {
        return ENC_CLINOMB;
    }

    public void setENC_CLINOMB(String ENC_CLINOMB) {
        this.ENC_CLINOMB = ENC_CLINOMB;
    }

    public String getENC_CLITIPOID() {
        return ENC_CLITIPOID;
    }

    public void setENC_CLITIPOID(String ENC_CLITIPOID) {
        this.ENC_CLITIPOID = ENC_CLITIPOID;
    }

    public String getENC_CLINUMID() {
        return ENC_CLINUMID;
    }

    public void setENC_CLINUMID(String ENC_CLINUMID) {
        this.ENC_CLINUMID = ENC_CLINUMID;
    }

    public String getENC_CLICIUDAD() {
        return ENC_CLICIUDAD;
    }

    public void setENC_CLICIUDAD(String ENC_CLICIUDAD) {
        this.ENC_CLICIUDAD = ENC_CLICIUDAD;
    }

    public String getENC_CLIDIR() {
        return ENC_CLIDIR;
    }

    public void setENC_CLIDIR(String ENC_CLIDIR) {
        this.ENC_CLIDIR = ENC_CLIDIR;
    }

    public String getENC_CLITELELEF() {
        return ENC_CLITELELEF;
    }

    public void setENC_CLITELELEF(String ENC_CLITELELEF) {
        this.ENC_CLITELELEF = ENC_CLITELELEF;
    }

    public String getENC_CLIEMAIL() {
        return ENC_CLIEMAIL;
    }

    public void setENC_CLIEMAIL(String ENC_CLIEMAIL) {
        this.ENC_CLIEMAIL = ENC_CLIEMAIL;
    }

    public String getENC_CLIRESPFISCAL() {
        return ENC_CLIRESPFISCAL;
    }

    public void setENC_CLIRESPFISCAL(String ENC_CLIRESPFISCAL) {
        this.ENC_CLIRESPFISCAL = ENC_CLIRESPFISCAL;
    }

    public Long getENC_CLIRESPIVA() {
        return ENC_CLIRESPIVA;
    }

    public void setENC_CLIRESPIVA(Long ENC_CLIRESPIVA) {
        this.ENC_CLIRESPIVA = ENC_CLIRESPIVA;
    }

    public String getENC_FEC_CARGA() {
        return ENC_FEC_CARGA;
    }

    public void setENC_FEC_CARGA(String ENC_FEC_CARGA) {
        this.ENC_FEC_CARGA = ENC_FEC_CARGA;
    }

    public String getENC_ESTADO() {
        return ENC_ESTADO;
    }

    public void setENC_ESTADO(String ENC_ESTADO) {
        this.ENC_ESTADO = ENC_ESTADO;
    }

    public Long getENC_CONSDIAN() {
        return ENC_CONSDIAN;
    }

    public void setENC_CONSDIAN(Long ENC_CONSDIAN) {
        this.ENC_CONSDIAN = ENC_CONSDIAN;
    }

    public Long getENC_ORIGEN() {
        return ENC_ORIGEN;
    }

    public void setENC_ORIGEN(Long ENC_ORIGEN) {
        this.ENC_ORIGEN = ENC_ORIGEN;
    }

    public Long getENC_FORMA_DATOS_ORIGEN() {
        return ENC_FORMA_DATOS_ORIGEN;
    }

    public void setENC_FORMA_DATOS_ORIGEN(Long ENC_FORMA_DATOS_ORIGEN) {
        this.ENC_FORMA_DATOS_ORIGEN = ENC_FORMA_DATOS_ORIGEN;
    }

    public String getENC_NUM_DOCUHIJO() {
        return ENC_NUM_DOCUHIJO;
    }

    public void setENC_NUM_DOCUHIJO(String ENC_NUM_DOCUHIJO) {
        this.ENC_NUM_DOCUHIJO = ENC_NUM_DOCUHIJO;
    }

    public String getENC_COD_PRESTADOR() {
        return ENC_COD_PRESTADOR;
    }

    public void setENC_COD_PRESTADOR(String ENC_COD_PRESTADOR) {
        this.ENC_COD_PRESTADOR = ENC_COD_PRESTADOR;
    }

    public String getENC_TIPODOC_USUARIO() {
        return ENC_TIPODOC_USUARIO;
    }

    public void setENC_TIPODOC_USUARIO(String ENC_TIPODOC_USUARIO) {
        this.ENC_TIPODOC_USUARIO = ENC_TIPODOC_USUARIO;
    }

    public String getENC_NUM_IDENTIFICACION() {
        return ENC_NUM_IDENTIFICACION;
    }

    public void setENC_NUM_IDENTIFICACION(String ENC_NUM_IDENTIFICACION) {
        this.ENC_NUM_IDENTIFICACION = ENC_NUM_IDENTIFICACION;
    }

    public String getENC_PRIMER_NOMBRE() {
        return ENC_PRIMER_NOMBRE;
    }

    public void setENC_PRIMER_NOMBRE(String ENC_PRIMER_NOMBRE) {
        this.ENC_PRIMER_NOMBRE = ENC_PRIMER_NOMBRE;
    }

    public String getENC_SEGUNDO_NOMBRE() {
        return ENC_SEGUNDO_NOMBRE;
    }

    public void setENC_SEGUNDO_NOMBRE(String ENC_SEGUNDO_NOMBRE) {
        this.ENC_SEGUNDO_NOMBRE = ENC_SEGUNDO_NOMBRE;
    }

    public String getENC_TIPO_USUARIO() {
        return ENC_TIPO_USUARIO;
    }

    public void setENC_TIPO_USUARIO(String ENC_TIPO_USUARIO) {
        this.ENC_TIPO_USUARIO = ENC_TIPO_USUARIO;
    }

    public String getENC_MODALIDAD_CONTRATO() {
        return ENC_MODALIDAD_CONTRATO;
    }

    public void setENC_MODALIDAD_CONTRATO(String ENC_MODALIDAD_CONTRATO) {
        this.ENC_MODALIDAD_CONTRATO = ENC_MODALIDAD_CONTRATO;
    }

    public String getENC_COBERTURA() {
        return ENC_COBERTURA;
    }

    public void setENC_COBERTURA(String ENC_COBERTURA) {
        this.ENC_COBERTURA = ENC_COBERTURA;
    }

    public String getENC_NUM_AUTORIZACION() {
        return ENC_NUM_AUTORIZACION;
    }

    public void setENC_NUM_AUTORIZACION(String ENC_NUM_AUTORIZACION) {
        this.ENC_NUM_AUTORIZACION = ENC_NUM_AUTORIZACION;
    }

    public String getENC_NUM_MIPRES() {
        return ENC_NUM_MIPRES;
    }

    public void setENC_NUM_MIPRES(String ENC_NUM_MIPRES) {
        this.ENC_NUM_MIPRES = ENC_NUM_MIPRES;
    }

    public String getENC_NUM_ENTREGA_MIPRES() {
        return ENC_NUM_ENTREGA_MIPRES;
    }

    public void setENC_NUM_ENTREGA_MIPRES(String ENC_NUM_ENTREGA_MIPRES) {
        this.ENC_NUM_ENTREGA_MIPRES = ENC_NUM_ENTREGA_MIPRES;
    }

    public String getENC_NUM_CONTRATO() {
        return ENC_NUM_CONTRATO;
    }

    public void setENC_NUM_CONTRATO(String ENC_NUM_CONTRATO) {
        this.ENC_NUM_CONTRATO = ENC_NUM_CONTRATO;
    }

    public String getENC_POLIZA() {
        return ENC_POLIZA;
    }

    public void setENC_POLIZA(String ENC_POLIZA) {
        this.ENC_POLIZA = ENC_POLIZA;
    }

    public Long getENC_COPAGO() {
        return ENC_COPAGO;
    }

    public void setENC_COPAGO(Long ENC_COPAGO) {
        this.ENC_COPAGO = ENC_COPAGO;
    }

    public Long getENC_CUOTA_MODERADORA() {
        return ENC_CUOTA_MODERADORA;
    }

    public void setENC_CUOTA_MODERADORA(Long ENC_CUOTA_MODERADORA) {
        this.ENC_CUOTA_MODERADORA = ENC_CUOTA_MODERADORA;
    }

    public Long getENC_CUOTA_RECUPERACION() {
        return ENC_CUOTA_RECUPERACION;
    }

    public void setENC_CUOTA_RECUPERACION(Long ENC_CUOTA_RECUPERACION) {
        this.ENC_CUOTA_RECUPERACION = ENC_CUOTA_RECUPERACION;
    }

    public Long getENC_PAGOS_COMPARTIDOS() {
        return ENC_PAGOS_COMPARTIDOS;
    }

    public void setENC_PAGOS_COMPARTIDOS(Long ENC_PAGOS_COMPARTIDOS) {
        this.ENC_PAGOS_COMPARTIDOS = ENC_PAGOS_COMPARTIDOS;
    }

    public String getENC_FECHA_INICIO_PERIODO() {
        return ENC_FECHA_INICIO_PERIODO;
    }

    public void setENC_FECHA_INICIO_PERIODO(String ENC_FECHA_INICIO_PERIODO) {
        this.ENC_FECHA_INICIO_PERIODO = ENC_FECHA_INICIO_PERIODO;
    }

    public String getENC_FECHA_FIN_PERIODO() {
        return ENC_FECHA_FIN_PERIODO;
    }

    public void setENC_FECHA_FIN_PERIODO(String ENC_FECHA_FIN_PERIODO) {
        this.ENC_FECHA_FIN_PERIODO = ENC_FECHA_FIN_PERIODO;
    }
}
