package com.name.business.entities;

import org.skife.jdbi.v2.sqlobject.Bind;

public class SaludPaisData {

    private String IN_ENC_PREFIJO;
    private String IN_ENC_NRORECIBO;
    private Long IN_ENC_SEDE;
    private Long IN_ENC_VIGENCIA;
    private String IN_ENC_CIAQUIPU;
    private String IN_ENC_NUM_DOCUHIJO;
    private String IN_ENC_COD_PRESTADOR;
    private String IN_ENC_TIPODOC_USUARIO;
    private String IN_ENC_NUM_IDENTIFICACION;
    private String IN_ENC_PRIMER_APELLIDO;
    private String IN_ENC_SEGUNDO_APELLIDO;
    private String IN_ENC_PRIMER_NOMBRE;
    private String IN_ENC_SEGUNDO_NOMBRE;
    private String IN_ENC_TIPO_USUARIO;
    private String IN_ENC_MODALIDAD_CONTRATO;
    private String IN_ENC_COBERTURA;
    private String IN_ENC_NUM_AUTORIZACION;
    private String IN_ENC_NUM_MIPRES;
    private String IN_ENC_NUM_ENTREGA_MIPRES;
    private String IN_ENC_NUM_CONTRATO;
    private String IN_ENC_POLIZA;
    private Long IN_ENC_COPAGO;
    private Long IN_ENC_CUOTA_MODERADORA;
    private Long IN_ENC_CUOTA_RECUPERACION;
    private Long IN_ENC_PAGOS_COMPARTIDOS;
    private String IN_ENC_FECHA_INICIO;
    private String IN_ENC_FECHA_FIN;

    public SaludPaisData(String IN_ENC_PREFIJO, String IN_ENC_NRORECIBO, Long IN_ENC_SEDE, Long IN_ENC_VIGENCIA, String IN_ENC_CIAQUIPU, String IN_ENC_NUM_DOCUHIJO, String IN_ENC_COD_PRESTADOR, String IN_ENC_TIPODOC_USUARIO, String IN_ENC_NUM_IDENTIFICACION, String IN_ENC_PRIMER_APELLIDO, String IN_ENC_SEGUNDO_APELLIDO, String IN_ENC_PRIMER_NOMBRE, String IN_ENC_SEGUNDO_NOMBRE, String IN_ENC_TIPO_USUARIO, String IN_ENC_MODALIDAD_CONTRATO, String IN_ENC_COBERTURA, String IN_ENC_NUM_AUTORIZACION, String IN_ENC_NUM_MIPRES, String IN_ENC_NUM_ENTREGA_MIPRES, String IN_ENC_NUM_CONTRATO, String IN_ENC_POLIZA, Long IN_ENC_COPAGO, Long IN_ENC_CUOTA_MODERADORA, Long IN_ENC_CUOTA_RECUPERACION, Long IN_ENC_PAGOS_COMPARTIDOS, String IN_ENC_FECHA_INICIO, String IN_ENC_FECHA_FIN) {
        this.IN_ENC_PREFIJO = IN_ENC_PREFIJO;
        this.IN_ENC_NRORECIBO = IN_ENC_NRORECIBO;
        this.IN_ENC_SEDE = IN_ENC_SEDE;
        this.IN_ENC_VIGENCIA = IN_ENC_VIGENCIA;
        this.IN_ENC_CIAQUIPU = IN_ENC_CIAQUIPU;
        this.IN_ENC_NUM_DOCUHIJO = IN_ENC_NUM_DOCUHIJO;
        this.IN_ENC_COD_PRESTADOR = IN_ENC_COD_PRESTADOR;
        this.IN_ENC_TIPODOC_USUARIO = IN_ENC_TIPODOC_USUARIO;
        this.IN_ENC_NUM_IDENTIFICACION = IN_ENC_NUM_IDENTIFICACION;
        this.IN_ENC_PRIMER_APELLIDO = IN_ENC_PRIMER_APELLIDO;
        this.IN_ENC_SEGUNDO_APELLIDO = IN_ENC_SEGUNDO_APELLIDO;
        this.IN_ENC_PRIMER_NOMBRE = IN_ENC_PRIMER_NOMBRE;
        this.IN_ENC_SEGUNDO_NOMBRE = IN_ENC_SEGUNDO_NOMBRE;
        this.IN_ENC_TIPO_USUARIO = IN_ENC_TIPO_USUARIO;
        this.IN_ENC_MODALIDAD_CONTRATO = IN_ENC_MODALIDAD_CONTRATO;
        this.IN_ENC_COBERTURA = IN_ENC_COBERTURA;
        this.IN_ENC_NUM_AUTORIZACION = IN_ENC_NUM_AUTORIZACION;
        this.IN_ENC_NUM_MIPRES = IN_ENC_NUM_MIPRES;
        this.IN_ENC_NUM_ENTREGA_MIPRES = IN_ENC_NUM_ENTREGA_MIPRES;
        this.IN_ENC_NUM_CONTRATO = IN_ENC_NUM_CONTRATO;
        this.IN_ENC_POLIZA = IN_ENC_POLIZA;
        this.IN_ENC_COPAGO = IN_ENC_COPAGO;
        this.IN_ENC_CUOTA_MODERADORA = IN_ENC_CUOTA_MODERADORA;
        this.IN_ENC_CUOTA_RECUPERACION = IN_ENC_CUOTA_RECUPERACION;
        this.IN_ENC_PAGOS_COMPARTIDOS = IN_ENC_PAGOS_COMPARTIDOS;
        this.IN_ENC_FECHA_INICIO = IN_ENC_FECHA_INICIO;
        this.IN_ENC_FECHA_FIN = IN_ENC_FECHA_FIN;
    }

    public String getIN_ENC_PREFIJO() {
        return IN_ENC_PREFIJO;
    }

    public void setIN_ENC_PREFIJO(String IN_ENC_PREFIJO) {
        this.IN_ENC_PREFIJO = IN_ENC_PREFIJO;
    }

    public String getIN_ENC_NRORECIBO() {
        return IN_ENC_NRORECIBO;
    }

    public void setIN_ENC_NRORECIBO(String IN_ENC_NRORECIBO) {
        this.IN_ENC_NRORECIBO = IN_ENC_NRORECIBO;
    }

    public Long getIN_ENC_SEDE() {
        return IN_ENC_SEDE;
    }

    public void setIN_ENC_SEDE(Long IN_ENC_SEDE) {
        this.IN_ENC_SEDE = IN_ENC_SEDE;
    }

    public Long getIN_ENC_VIGENCIA() {
        return IN_ENC_VIGENCIA;
    }

    public void setIN_ENC_VIGENCIA(Long IN_ENC_VIGENCIA) {
        this.IN_ENC_VIGENCIA = IN_ENC_VIGENCIA;
    }

    public String getIN_ENC_CIAQUIPU() {
        return IN_ENC_CIAQUIPU;
    }

    public void setIN_ENC_CIAQUIPU(String IN_ENC_CIAQUIPU) {
        this.IN_ENC_CIAQUIPU = IN_ENC_CIAQUIPU;
    }

    public String getIN_ENC_NUM_DOCUHIJO() {
        return IN_ENC_NUM_DOCUHIJO;
    }

    public void setIN_ENC_NUM_DOCUHIJO(String IN_ENC_NUM_DOCUHIJO) {
        this.IN_ENC_NUM_DOCUHIJO = IN_ENC_NUM_DOCUHIJO;
    }

    public String getIN_ENC_COD_PRESTADOR() {
        return IN_ENC_COD_PRESTADOR;
    }

    public void setIN_ENC_COD_PRESTADOR(String IN_ENC_COD_PRESTADOR) {
        this.IN_ENC_COD_PRESTADOR = IN_ENC_COD_PRESTADOR;
    }

    public String getIN_ENC_TIPODOC_USUARIO() {
        return IN_ENC_TIPODOC_USUARIO;
    }

    public void setIN_ENC_TIPODOC_USUARIO(String IN_ENC_TIPODOC_USUARIO) {
        this.IN_ENC_TIPODOC_USUARIO = IN_ENC_TIPODOC_USUARIO;
    }

    public String getIN_ENC_NUM_IDENTIFICACION() {
        return IN_ENC_NUM_IDENTIFICACION;
    }

    public void setIN_ENC_NUM_IDENTIFICACION(String IN_ENC_NUM_IDENTIFICACION) {
        this.IN_ENC_NUM_IDENTIFICACION = IN_ENC_NUM_IDENTIFICACION;
    }

    public String getIN_ENC_PRIMER_APELLIDO() {
        return IN_ENC_PRIMER_APELLIDO;
    }

    public void setIN_ENC_PRIMER_APELLIDO(String IN_ENC_PRIMER_APELLIDO) {
        this.IN_ENC_PRIMER_APELLIDO = IN_ENC_PRIMER_APELLIDO;
    }

    public String getIN_ENC_SEGUNDO_APELLIDO() {
        return IN_ENC_SEGUNDO_APELLIDO;
    }

    public void setIN_ENC_SEGUNDO_APELLIDO(String IN_ENC_SEGUNDO_APELLIDO) {
        this.IN_ENC_SEGUNDO_APELLIDO = IN_ENC_SEGUNDO_APELLIDO;
    }

    public String getIN_ENC_PRIMER_NOMBRE() {
        return IN_ENC_PRIMER_NOMBRE;
    }

    public void setIN_ENC_PRIMER_NOMBRE(String IN_ENC_PRIMER_NOMBRE) {
        this.IN_ENC_PRIMER_NOMBRE = IN_ENC_PRIMER_NOMBRE;
    }

    public String getIN_ENC_SEGUNDO_NOMBRE() {
        return IN_ENC_SEGUNDO_NOMBRE;
    }

    public void setIN_ENC_SEGUNDO_NOMBRE(String IN_ENC_SEGUNDO_NOMBRE) {
        this.IN_ENC_SEGUNDO_NOMBRE = IN_ENC_SEGUNDO_NOMBRE;
    }

    public String getIN_ENC_TIPO_USUARIO() {
        return IN_ENC_TIPO_USUARIO;
    }

    public void setIN_ENC_TIPO_USUARIO(String IN_ENC_TIPO_USUARIO) {
        this.IN_ENC_TIPO_USUARIO = IN_ENC_TIPO_USUARIO;
    }

    public String getIN_ENC_MODALIDAD_CONTRATO() {
        return IN_ENC_MODALIDAD_CONTRATO;
    }

    public void setIN_ENC_MODALIDAD_CONTRATO(String IN_ENC_MODALIDAD_CONTRATO) {
        this.IN_ENC_MODALIDAD_CONTRATO = IN_ENC_MODALIDAD_CONTRATO;
    }

    public String getIN_ENC_COBERTURA() {
        return IN_ENC_COBERTURA;
    }

    public void setIN_ENC_COBERTURA(String IN_ENC_COBERTURA) {
        this.IN_ENC_COBERTURA = IN_ENC_COBERTURA;
    }

    public String getIN_ENC_NUM_AUTORIZACION() {
        return IN_ENC_NUM_AUTORIZACION;
    }

    public void setIN_ENC_NUM_AUTORIZACION(String IN_ENC_NUM_AUTORIZACION) {
        this.IN_ENC_NUM_AUTORIZACION = IN_ENC_NUM_AUTORIZACION;
    }

    public String getIN_ENC_NUM_MIPRES() {
        return IN_ENC_NUM_MIPRES;
    }

    public void setIN_ENC_NUM_MIPRES(String IN_ENC_NUM_MIPRES) {
        this.IN_ENC_NUM_MIPRES = IN_ENC_NUM_MIPRES;
    }

    public String getIN_ENC_NUM_ENTREGA_MIPRES() {
        return IN_ENC_NUM_ENTREGA_MIPRES;
    }

    public void setIN_ENC_NUM_ENTREGA_MIPRES(String IN_ENC_NUM_ENTREGA_MIPRES) {
        this.IN_ENC_NUM_ENTREGA_MIPRES = IN_ENC_NUM_ENTREGA_MIPRES;
    }

    public String getIN_ENC_NUM_CONTRATO() {
        return IN_ENC_NUM_CONTRATO;
    }

    public void setIN_ENC_NUM_CONTRATO(String IN_ENC_NUM_CONTRATO) {
        this.IN_ENC_NUM_CONTRATO = IN_ENC_NUM_CONTRATO;
    }

    public String getIN_ENC_POLIZA() {
        return IN_ENC_POLIZA;
    }

    public void setIN_ENC_POLIZA(String IN_ENC_POLIZA) {
        this.IN_ENC_POLIZA = IN_ENC_POLIZA;
    }

    public Long getIN_ENC_COPAGO() {
        return IN_ENC_COPAGO;
    }

    public void setIN_ENC_COPAGO(Long IN_ENC_COPAGO) {
        this.IN_ENC_COPAGO = IN_ENC_COPAGO;
    }

    public Long getIN_ENC_CUOTA_MODERADORA() {
        return IN_ENC_CUOTA_MODERADORA;
    }

    public void setIN_ENC_CUOTA_MODERADORA(Long IN_ENC_CUOTA_MODERADORA) {
        this.IN_ENC_CUOTA_MODERADORA = IN_ENC_CUOTA_MODERADORA;
    }

    public Long getIN_ENC_CUOTA_RECUPERACION() {
        return IN_ENC_CUOTA_RECUPERACION;
    }

    public void setIN_ENC_CUOTA_RECUPERACION(Long IN_ENC_CUOTA_RECUPERACION) {
        this.IN_ENC_CUOTA_RECUPERACION = IN_ENC_CUOTA_RECUPERACION;
    }

    public Long getIN_ENC_PAGOS_COMPARTIDOS() {
        return IN_ENC_PAGOS_COMPARTIDOS;
    }

    public void setIN_ENC_PAGOS_COMPARTIDOS(Long IN_ENC_PAGOS_COMPARTIDOS) {
        this.IN_ENC_PAGOS_COMPARTIDOS = IN_ENC_PAGOS_COMPARTIDOS;
    }

    public String getIN_ENC_FECHA_INICIO() {
        return IN_ENC_FECHA_INICIO;
    }

    public void setIN_ENC_FECHA_INICIO(String IN_ENC_FECHA_INICIO) {
        this.IN_ENC_FECHA_INICIO = IN_ENC_FECHA_INICIO;
    }

    public String getIN_ENC_FECHA_FIN() {
        return IN_ENC_FECHA_FIN;
    }

    public void setIN_ENC_FECHA_FIN(String IN_ENC_FECHA_FIN) {
        this.IN_ENC_FECHA_FIN = IN_ENC_FECHA_FIN;
    }
}
