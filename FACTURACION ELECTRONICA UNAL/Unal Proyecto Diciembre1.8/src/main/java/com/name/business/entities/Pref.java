package com.name.business.entities;

public class Pref {

    private String con_prefijo;
    private String con_ultim_asignado;
    private boolean check;


    public Pref(String con_prefijo, String con_ultim_asignado) {
        this.con_prefijo = con_prefijo;
        this.con_ultim_asignado = con_ultim_asignado;
        this.check = false;
    }

    public String getCon_prefijo() {
        return con_prefijo;
    }

    public void setCon_prefijo(String con_prefijo) {
        this.con_prefijo = con_prefijo;
    }

    public String getCon_ultim_asignado() {
        return con_ultim_asignado;
    }

    public void setCon_ultim_asignado(String con_ultim_asignado) {
        this.con_ultim_asignado = con_ultim_asignado;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
