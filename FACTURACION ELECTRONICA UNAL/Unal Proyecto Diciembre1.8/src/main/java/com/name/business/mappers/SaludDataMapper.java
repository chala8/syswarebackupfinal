package com.name.business.mappers;

import com.name.business.entities.Master;
import com.name.business.entities.Report;
import com.name.business.entities.SaludPaisData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SaludDataMapper implements ResultSetMapper<SaludPaisData> {

    @Override
    public SaludPaisData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new SaludPaisData(
                resultSet.getString("ENC_PREFIJO"),
                resultSet.getString("ENC_NRORECIBO"),
                resultSet.getLong("ENC_SEDE"),
                resultSet.getLong("ENC_VIGENCIA"),
                resultSet.getString("ENC_CIAQUIPU"),
                resultSet.getString("ENC_NUM_DOCUHIJO"),
                resultSet.getString("ENC_COD_PRESTADOR"),
                resultSet.getString("ENC_TIPODOC_USUARIO"),
                resultSet.getString("ENC_NUM_IDENTIFICACION"),
                resultSet.getString("ENC_PRIMER_APELLIDO"),
                resultSet.getString("ENC_SEGUNDO_APELLIDO"),
                resultSet.getString("ENC_PRIMER_NOMBRE"),
                resultSet.getString("ENC_SEGUNDO_NOMBRE"),
                resultSet.getString("ENC_TIPO_USUARIO"),
                resultSet.getString("ENC_MODALIDAD_CONTRATO"),
                resultSet.getString("ENC_COBERTURA"),
                resultSet.getString("ENC_NUM_AUTORIZACION"),
                resultSet.getString("ENC_NUM_MIPRES"),
                resultSet.getString("ENC_NUM_ENTREGA_MIPRES"),
                resultSet.getString("ENC_NUM_CONTRATO"),
                resultSet.getString("ENC_POLIZA"),
                resultSet.getLong("ENC_COPAGO"),
                resultSet.getLong("ENC_CUOTA_MODERADORA"),
                resultSet.getLong("ENC_CUOTA_RECUPERACION"),
                resultSet.getLong("ENC_PAGOS_COMPARTIDOS"),
                resultSet.getString("ENC_FECHA_INICIO"),
                resultSet.getString("ENC_FECHA_FIN")
        );
    }
}
