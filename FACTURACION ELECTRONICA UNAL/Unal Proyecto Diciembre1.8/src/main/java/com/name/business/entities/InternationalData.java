package com.name.business.entities;

public class InternationalData {
    private String NOMBRE_PAIS;
    private String PREFIJO_DIAN;

    public InternationalData(String NOMBRE_PAIS, String PREFIJO_DIAN) {
        this.NOMBRE_PAIS = NOMBRE_PAIS;
        this.PREFIJO_DIAN = PREFIJO_DIAN;
    }

    public String getNOMBRE_PAIS() {
        return NOMBRE_PAIS;
    }

    public void setNOMBRE_PAIS(String NOMBRE_PAIS) {
        this.NOMBRE_PAIS = NOMBRE_PAIS;
    }

    public String getPREFIJO_DIAN() {
        return PREFIJO_DIAN;
    }

    public void setPREFIJO_DIAN(String PREFIJO_DIAN) {
        this.PREFIJO_DIAN = PREFIJO_DIAN;
    }
}
