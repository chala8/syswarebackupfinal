package com.name.business.mappers;

import com.name.business.entities.Master;
import com.name.business.entities.PaisData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PaisDataMapper implements ResultSetMapper<PaisData> {

    @Override
    public PaisData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PaisData(
                resultSet.getLong("id_pais"),
                resultSet.getString("pais")
        );
    }
}
