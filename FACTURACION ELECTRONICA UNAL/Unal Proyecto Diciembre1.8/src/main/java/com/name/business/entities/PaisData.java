package com.name.business.entities;

public class PaisData {
    private Long id_pais;
    private String pais;

    public PaisData(Long id_pais, String pais) {
        this.id_pais = id_pais;
        this.pais = pais;
    }

    public Long getId_pais() {
        return id_pais;
    }

    public void setId_pais(Long id_pais) {
        this.id_pais = id_pais;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }
}
