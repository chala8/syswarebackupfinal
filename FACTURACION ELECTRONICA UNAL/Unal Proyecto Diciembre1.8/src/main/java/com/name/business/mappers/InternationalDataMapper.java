package com.name.business.mappers;

import com.name.business.entities.Detail;
import com.name.business.entities.InternationalData;
import com.name.business.entities.Master;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InternationalDataMapper implements ResultSetMapper<InternationalData> {

    @Override
    public InternationalData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new InternationalData(
                resultSet.getString("NOMBRE_PAIS"),
                resultSet.getString("PREFIJO_DIAN")
        );
    }
}
