package com.name.business.mappers;

import com.name.business.entities.Master;
import com.name.business.entities.MasterMed;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MasterMedMapper implements ResultSetMapper<MasterMed> {

    @Override
    public MasterMed map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new MasterMed(
                resultSet.getLong("ENC_ID"),
                resultSet.getString("ENC_PREFIJO"),
                resultSet.getString("ENC_NRORECIBO"),
                resultSet.getLong("ENC_SEDE"),
                resultSet.getLong("ENC_VIGENCIA"),
                resultSet.getString("ENC_CIAQUIPU"),
                resultSet.getLong("ENC_NRO_ITEMES"),
                resultSet.getString("ENC_NOMBRESEDE"),
                resultSet.getString("ENC_EMPRESAQUIPU"),
                resultSet.getString("ENC_FECEMIC"),
                resultSet.getLong("ENC_TIPODOC"),
                resultSet.getLong("ENC_FORMAPAGO"),
                resultSet.getString("ENC_FECPAGO"),
                resultSet.getLong("ENC_MEDIOPAGO"),
                resultSet.getDouble("ENC_DESCUENTO"),
                resultSet.getDouble("ENC_SUBTOTAL"),
                resultSet.getDouble("ENC_TOTAL"),
                resultSet.getString("ENC_OBSERVACIONES"),
                resultSet.getString("ENC_DATOS_PAGO"),
                resultSet.getString("ENC_CIUDAD_SEDE"),
                resultSet.getString("ENC_DIR_SEDE"),
                resultSet.getString("ENC_TELEF_SEDE"),
                resultSet.getString("ENC_EMAIL_SEDE_EMISION"),
                resultSet.getLong("ENC_CLINATU"),
                resultSet.getString("ENC_CLINOMB"),
                resultSet.getString("ENC_CLITIPOID"),
                resultSet.getString("ENC_CLINUMID"),
                resultSet.getString("ENC_CLICIUDAD"),
                resultSet.getString("ENC_CLIDIR"),
                resultSet.getString("ENC_CLITELELEF"),
                resultSet.getString("ENC_CLIEMAIL"),
                resultSet.getString("ENC_CLIRESPFISCAL"),
                resultSet.getLong("ENC_CLIRESPIVA"),
                resultSet.getString("ENC_FEC_CARGA"),
                resultSet.getString("ENC_ESTADO"),
                resultSet.getLong("ENC_CONSDIAN"),
                resultSet.getLong("ENC_ORIGEN"),
                resultSet.getLong("ENC_FORMA_DATOS_ORIGEN"),
                resultSet.getString("ENC_NUM_DOCUHIJO"),
                resultSet.getString("ENC_COD_PRESTADOR"),
                resultSet.getString("ENC_TIPODOC_USUARIO"),
                resultSet.getString("ENC_NUM_IDENTIFICACION"),
                resultSet.getString("ENC_PRIMER_NOMBRE"),
                resultSet.getString("ENC_SEGUNDO_NOMBRE"),
                resultSet.getString("ENC_TIPO_USUARIO"),
                resultSet.getString("ENC_MODALIDAD_CONTRATO"),
                resultSet.getString("ENC_COBERTURA"),
                resultSet.getString("ENC_NUM_AUTORIZACION"),
                resultSet.getString("ENC_NUM_MIPRES"),
                resultSet.getString("ENC_NUM_ENTREGA_MIPRES"),
                resultSet.getString("ENC_NUM_CONTRATO"),
                resultSet.getString("ENC_POLIZA"),
                resultSet.getLong("ENC_COPAGO"),
                resultSet.getLong("ENC_CUOTA_MODERADORA"),
                resultSet.getLong("ENC_CUOTA_RECUPERACION"),
                resultSet.getLong("ENC_PAGOS_COMPARTIDOS"),
                resultSet.getString("ENC_FECHA_INICIO_PERIODO"),
                resultSet.getString("ENC_FECHA_FIN_PERIODO")
        );
    }
}
