import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  constructor(private http: HttpClient) { }


  page = 1;
  count = 0;
  tableSize = 20;
  listaPublicacionesPadre=[];
  listaCopyPadre;
  listaPrefs;
  date1;
  date2;
  nrorecibo;
  selectedState = 'M'


  ngOnInit(): void {
    this.http.get("http://168.176.6.92:8981/v1/invoice/prefs").subscribe(response => {
      this.listaPrefs = response;
    })
  }


  onTableDataChange(event){
    this.page = event;
  }

  getAmountShown(){

    let max = Math.ceil( this.listaPublicacionesPadre.length/20);
    if(this.page == max){
      return this.listaPublicacionesPadre.length%20;
    }else{
      return 20;
    }

  }

  showList(){
    let list = this.listaPrefs.filter(element => element.check == true);
    console.log(list)
  }

  setValue(item){
    item.check = !item.check;
  }

  getReport(){
    let stringToSend = "";

    let list = this.listaPrefs.filter(element => element.check == true);

    if(list.length!=0){
      console.log("im wrongly in");
      for(let i = 0; i<this.listaPrefs.length;i++){
        let element = this.listaPrefs[i];
        if(element.check){
          stringToSend = stringToSend + element.con_prefijo+",";
        }
      }
    }else{
      console.log("im in");
      for(let j = 0; j<this.listaPrefs.length;j++){
        let element = this.listaPrefs[j];
        stringToSend = stringToSend + element.con_prefijo+","

      }
    }

    console.log(stringToSend);

    this.http.get("http://168.176.6.92:8981/v1/invoice/report?date1="+this.date1+"&date2="+this.date2+"&&list="+stringToSend.substring(0,stringToSend.length-1)).subscribe(response => {
      //@ts-ignore
      this.listaPublicacionesPadre = response;
      this.listaCopyPadre = response;
      this.page = 1;
      this.selectedState = 'M';
    })

  }


  getReport2(){
    this.http.get("http://168.176.6.92:8981/v1/invoice/report2?nrorecibo="+this.nrorecibo).subscribe(response => {
      //@ts-ignore
      this.listaPublicacionesPadre = response;
      this.listaCopyPadre = response;
      this.page = 1;
      this.selectedState = 'M';
    })
  }



filter(){
  if(this.selectedState === 'M'){
    this.listaPublicacionesPadre = this.listaCopyPadre;
  }else{
    this.listaPublicacionesPadre = this.listaCopyPadre.filter(element => element.estado===this.selectedState)
  }
  this.page = 1;
}


}
