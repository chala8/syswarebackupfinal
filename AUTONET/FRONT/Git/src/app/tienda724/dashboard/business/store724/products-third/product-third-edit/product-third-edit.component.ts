import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
//import 'rxjs/add/operator/filter';
/*
*    Material modules for component
*/
/*
*     others component
*/
/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { ProductThirdDTO } from '../models/productThirdDTO'
import { ProductThird } from '../models/productThird'
import { CommonStateStoreDTO } from '../../commons/CommonStateStoreDTO'

import { CodeDTO } from '../../bar-codes/models/codeDTO'
import { Code } from '../../bar-codes/models/code'

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { ProductThirdService } from '../product-third.service';
import { BarCodeService } from '../../bar-codes/bar-code.service'

/*
*     constant of  your component
*/
let $: any = jQuery;
import * as jQuery from 'jquery';
import 'bootstrap-notify';
@Component({
  selector: 'app-product-third-edit',
  templateUrl: './product-third-edit.component.html',
  styleUrls: ['./product-third-edit.component.scss']
})
export class ProductThirdEditComponent implements OnInit {

  form: FormGroup;
  token: Token;
  id_productThird: number = 0;
  newProductThird: ProductThirdDTO;
  currentProductThird: ProductThird;
  productThirdList: ProductThird[];

  codeDTO: CodeDTO;
  commonStateStoreDTO: CommonStateStoreDTO;
  codeList: Code[];


  //attributes
  elstring = "adhkjshakjhsd";

  CURRENT_ID_THIRD = 0;
  CURRENT_ID_CATEGORY = 0;
  STATE = 1;

  CURRENT_FATHER = 0;

  constructor(private router: Router, private route: ActivatedRoute, private productThirdService: ProductThirdService, private barCodeService: BarCodeService,
    private fb: FormBuilder, private locStorage: LocalStorage) {

    this.commonStateStoreDTO = new CommonStateStoreDTO(1, null, null)
    this.codeDTO = new CodeDTO(null, null, null, null, null,null,null, this.commonStateStoreDTO)
    this.newProductThird = new ProductThirdDTO(null, null, null, null, null, this.commonStateStoreDTO, null, null, null, this.codeDTO)
  }

  ngOnInit() {

    this.createControls();

    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {

      this.token=this.locStorage.getToken();
      this.route.params
        .subscribe(params => {
          this.id_productThird=+params['id']
          console.log("ID_PRODUCTHIRD "+this.id_productThird)

          // Defaults to 0 if no query param provided.

          this.CURRENT_ID_CATEGORY = params.father;


          this.getProductThirdList(this.STATE, this.id_productThird);
        });
    }

  }

  Login() {
    let link = ['/auth'];
    this.router.navigate(link);
  }


  createControls() {
    this.form = this.fb.group({

      //profile

      min_price: ['', Validators.compose([

      ])],

      standard_price: ['', Validators.compose([

      ])],
      location: ['', Validators.compose([
        Validators.required
      ])]
    });
  }


  loadData() {
    console.log("CURRENT PRODUCT TRHIRD "+this.currentProductThird)

    this.form.patchValue({
      min_price: this.currentProductThird.description.min_price,
      standard_price: this.currentProductThird.description.standard_price,
      location: this.currentProductThird.description.location,
    });

  }


  resetForm() {

    this.form.reset();

  }

  goBack() {
    let link = ['/dashboard/business/product-third/list'];
    this.router.navigate(link);
  }

  getCodeList(state?: number, id_category?: number, id_category_father?: number, id_third?: number) {

    this.barCodeService.getCodeList(state)
      .subscribe((data: Code[]) => this.codeList = data,
      error => console.log(error),
      () => {
        this.loadData();
      });
  }


  getProductThirdList(state?:number,id_productThird?:number){
        this.productThirdService.getProductThirdList(state,id_productThird)
        .subscribe((data: ProductThird[]) => this.productThirdList = data,
        error => console.log(error),
        () => {
          if(this.productThirdList.length>0){
              this.currentProductThird=this.productThirdList[0];
              this.getCodeList(this.STATE);
          }else{
            alert("No se encuetra elemento para editar")
            this.goBack();
          }
        });
      }

  editProductThird() {
    this.commonStateStoreDTO.state=1;
    //this.newProductThird.id_third = this.CURRENT_ID_CATEGORY > 0 ? this.CURRENT_ID_CATEGORY : null;
    this.newProductThird.min_price = this.form.value["min_price"];
    this.newProductThird.standard_price = this.form.value["standard_price"];
    this.newProductThird.location = this.form.value["location"];
    this.newProductThird.state = this.commonStateStoreDTO;

    //this.common.name= this.form.value["name"];
    let id_code = this.form.value["selectCode"];

    console.log("PRODUCTO TERCEROOOO ", this.newProductThird, id_code)
    this.productThirdService.putProductThird(this.id_productThird, this.newProductThird, id_code)
      .subscribe(
      result => {

        if (result === true) {
          this.showNotification('top', 'right', 2, "Cambios guardados correctamente");
          this.resetForm();
          this.goBack();



          return;
        } else {
          this.showNotification('top', 'right', 3, "Problemas al actualizar");


          //this.openDialog();
          return;
        }
      })
  }

  showNotification(from, align, id_type?, msn?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }

}
