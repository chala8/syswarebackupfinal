import { Component,Inject, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormArray,FormBuilder, Validators } from '@angular/forms';

import * as _ from 'lodash';
/*
*    Material modules for component
*/
import {MatTabChangeEvent, VERSION} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
/*
*     others component
*/
/*
*     models for  your component
*/
import { InventoryDetail } from '../models/inventoryDetail'

// DTO's

import { InventoryDetailDTO } from '../models/inventoryDetailDTO'
import { InventoryQuantityDTO } from '../models/inventoryQuantityDTO'


import { CommonStateStoreDTO } from '../../commons/CommonStateStoreDTO'
import { CommonStoreDTO } from '../../commons/CommonStoreDTO'

import { LocalStorage } from '../../../../../../shared/localStorage'
import { InventoriesService } from '../inventories.service';

declare var $ :any

@Component({
  selector: 'app-dialog-quantity',
  templateUrl: './dialog-quantity.component.html',
  styleUrls: ['./dialog-quantity.component.scss']
})
export class DialogQuantityComponent implements OnInit {

  form_qu: FormGroup;
  isValue=false;

  inventoryDetailDTO:InventoryDetailDTO;
  inventoryQuantityDTO : InventoryQuantityDTO;
  inventoryQuantityDTOList : InventoryQuantityDTO[];
  commonStateStoreDTO:CommonStateStoreDTO;
  common:CommonStoreDTO;

  inventoryDetailData: InventoryDetail;
  

  isAddOnlyInventory: InventoryDetail;

  constructor(public dialogRef: MatDialogRef<DialogQuantityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder,
    public inventoriesService: InventoriesService) {

      this.data;
      this.inventoryDetailData = this.data.quantityTemp;
      console.log("DATA -> " , this.data.quantityTemp )
      this.inventoryQuantityDTO = new InventoryQuantityDTO(null,null,null,null,null);
      this.inventoryQuantityDTOList=[]
     }




  ngOnInit() {
    this.createQuantityControls();
    this.loadData();
  }

  loadData(){    
    this.form_qu.patchValue({
      quantity: this.inventoryDetailData.detail.quantity
    });
  }

  save(){

    console.log("DTO QUANTITY0 "+this.inventoryQuantityDTO);
    

    this.inventoryQuantityDTO.id_inventory_detail = this.inventoryDetailData.detail.id_inventory_detail;
    this.inventoryQuantityDTO.id_product_third = this.inventoryDetailData.detail.id_product_third;    
    if(this.form_qu.value['quantity'] !== null && this.form_qu.value['quantity'] != this.inventoryDetailData.detail.quantity){
      this.inventoryQuantityDTO.quantity = (this.form_qu.value['quantity'] - this.inventoryDetailData.detail.quantity);
    }else{
      this.dialogRef.close();      
      return
    }
    //this.inventoryQuantityDTO.quantity = this.form_qu.value['quantity'];    
    this.inventoryQuantityDTO.code = this.inventoryDetailData.detail.code;    
    this.inventoryQuantityDTOList.push(this.inventoryQuantityDTO)
    
    this.inventoriesService.putPlusInventory(this.inventoryDetailData.detail.id_inventory, this.inventoryQuantityDTOList)
    .subscribe(
      result => {

        if (result === true) {
          this.showNotification('top', 'right', 2, "Cambios guardados correctamente");
          this.dialogRef.close();

          return;
        } else {
          this.showNotification('top', 'right', 3, "Problemas al actualizar");
          this.dialogRef.close();

          //this.openDialog();
          return;
        }
      })


    alert("Guardado :v")
  }

  showNotification(from, align, id_type?, msn?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }

  createQuantityControls() {
    this.form_qu = this.fb.group({
      quantity: ['', Validators.compose([
        Validators.required
      ])]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}

