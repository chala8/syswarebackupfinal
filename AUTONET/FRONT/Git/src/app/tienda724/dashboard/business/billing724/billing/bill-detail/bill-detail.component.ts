import { Component, OnChanges, SimpleChanges, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import {Observable,of} from 'rxjs';
//import 'rxjs/add/observable/of';
import * as _ from 'lodash';

import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';


import { MatTabChangeEvent, VERSION, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { BillDialogQuantityComponent } from '../bill-dialog-quantity/bill-dialog-quantity.component'


/*
*     Models of  your component
*/
import { Token } from '../../../../../../shared/token';

import { Third } from '../../../thirds724/third/models/third';
import { InventoryDetail } from '../../../store724/inventories/models/inventoryDetail'
import { Inventory } from '../../../store724/inventories/models/inventory';

import { BillDTO } from '../models/billDTO';
import { DetailBillDTO } from '../models/detailBillDTO';
import { CommonStateDTO } from '../../commons/commonStateDTO';
import { DocumentDTO } from '../../commons/documentDTO'
import { InventoryQuantityDTO } from '../../../store724/inventories/models/inventoryQuantityDTO'

import { InventoryDTO } from '../../../store724/inventories/models/inventoryDTO';
import { InventoryDetailDTO } from '../../../store724/inventories/models/inventoryDetailDTO';
import { CommonStateStoreDTO } from '../../../store724/commons/CommonStateStoreDTO'

import { CommonStoreDTO } from '../../../store724/commons/CommonStoreDTO'


/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage';

import { InventoriesService } from '../../../store724/inventories/inventories.service';
import { DocumentService } from '../../document/document.service';
import { BillingService } from '../billing.service';
import { ThirdService } from '../../../thirds724/third/third.service'
import { BarCodeService } from '../../../store724/bar-codes/bar-code.service';
import { Bill } from '../models/bill';


@Component({
  selector: 'app-bill-detail',
  templateUrl: './bill-detail.component.html',
  styleUrls: ['./bill-detail.component.scss']
})
export class BillDetailComponent implements OnInit, OnChanges {
  // Attributes
  @Input()
  TOTAL_PRODUCTS = 123;
  @Input()
  TOTAL_PRICE = 0.00;
  /* flag */
  isDetail = true;
  isPrice = true;


  inventoryDetailForBilling: InventoryDetail[];
  newProductsForMovement: any[];

  // DTO's
  @Input()
  DETAILLIST: any[]

  @Input()
  TYPE_NAME: string;

  detailBillingDTOList: any[]

  @Output() detailEmitter = new EventEmitter<any[]>();

  constructor(public locStorage: LocalStorage, private fb: FormBuilder,
    public thirdService: ThirdService,

    private _router: Router, public billingService: BillingService,
    public barCodeService: BarCodeService, public documentService: DocumentService,
    public inventoriesService: InventoriesService, public dialog: MatDialog) {
    this.newProductsForMovement = [];
    this.inventoryDetailForBilling = [];

  }

  ngOnInit() {

  }
  ngOnChanges(changes: SimpleChanges) {
    // changes.prop contains the old and the new value...
    if (this.TYPE_NAME != null && (this.TYPE_NAME.toLocaleLowerCase().includes("venta") || this.TYPE_NAME.toLocaleLowerCase().includes("compra"))) {


      this.isPrice = true;

    } else {

      this.isPrice = false;
    }

    if (this.DETAILLIST != null) {
      this.detailBillingDTOList = this.DETAILLIST
    }
  }

  sentNewChangeDetailBill() {
    this.detailEmitter.emit(this.DETAILLIST);
  }

  changeQuantity(element) {
    let oldQuantity = element.quantity;
    let inventoryDetail = null;
    let flag = true;

    let dialogRef = this.dialog.open(BillDialogQuantityComponent, {
      height: '450px',
      width: '600px',
      data: {
        quantityTemp: element, type_name: this.TYPE_NAME, is_exit: element.is_exit,
        currentList: this.inventoryDetailForBilling
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      if (result) {
        this.sentNewChangeDetailBill();
      }
    });

  }


}

