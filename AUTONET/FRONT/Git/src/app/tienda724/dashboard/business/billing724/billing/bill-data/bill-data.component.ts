
import { Component, OnInit } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import {Observable,of} from 'rxjs';
//import 'rxjs/add/observable/of';
import * as _ from 'lodash';

import { FormGroup, FormArray,FormBuilder, Validators } from '@angular/forms';
import {FormControl} from '@angular/forms';



import {MatTabChangeEvent, VERSION, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


import { LocalStorage } from '../../../../../../shared/localStorage';
import { Token } from '../../../../../../shared/token';

import { InventoriesService } from '../../../store724/inventories/inventories.service';
import { DocumentService } from '../../document/document.service';
import { BillingService } from '../billing.service';

import { BarCodeService } from '../../../store724/bar-codes/bar-code.service';
import { AttributeService } from '../../../store724/attributes/attribute.service';


import { Third } from '../../../thirds724/third/models/third';


@Component({
  selector: 'app-bill-data',
  templateUrl: './bill-data.component.html',
  styleUrls: ['./bill-data.component.scss']
})
export class BillDataComponent implements OnInit {

  public selectedVal: string;
  date = new FormControl({value:new Date(), disabled:true});
  ID_BILL_TYPE=1


  isMyProduct=true;
  token:Token;
  ID_THIRD_TYPE: number;
  STATE=1
  CURRENT_ID_THIRD = 0;
  CURRENT_ID_THIRD_PATHER=0;
  isExit=true;
  form: FormGroup;

  thirdFathet:Third;


  constructor(public locStorage: LocalStorage, private fb: FormBuilder,
    private _router: Router,public billingService:BillingService,
    public barCodeService:BarCodeService,public documentService:DocumentService,
    public attributeService:AttributeService,
    public inventoriesService: InventoriesService, public dialog: MatDialog) {

    }




  ngOnInit() {

    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD=this.token.id_third;
      this.CURRENT_ID_THIRD_PATHER=this.token.id_third_father;
      this.ID_THIRD_TYPE = 23;
      this.selectedVal ='option1';

      if (this.CURRENT_ID_THIRD!==null && this.CURRENT_ID_THIRD>0){



      }


    }

  }


public onValChange(val: string) {
  this.selectedVal = val;
  if(this.selectedVal=="option1"){
    this.ID_BILL_TYPE=1
  }else{
    this.ID_BILL_TYPE=2
  }



}


  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }

}



