import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
//import 'rxjs/add/operator/filter';
/*
*    Material modules for component
*/
/*
*     others component
*/
import { ProductOverviewComponent } from '../../products/product-overview/product-overview.component'
/*
*     models for  your component
*/
import { Product } from '../../products/models/product';
import { MeasureUnit } from '../../measure-unit/models/measureUnit';
import { Attribute } from '../../attributes/models/attribute';
import { AttributeList } from '../../attributes/models/attributeList';
import { AttributeDetailList } from '../../attributes/models/attributeDetailList';
import { AttributeValue } from '../../attributes/models/attributeValue';
import { Category } from '../../categories/models/category';
import { Token } from '../../../../../../shared/token'
import { ProductThirdDTO } from '../models/productThirdDTO'
import { CodeDTO } from '../../bar-codes/models/codeDTO'
import { CommonStateStoreDTO } from '../../commons/CommonStateStoreDTO'
import { CommonStoreDTO } from '../../commons/CommonStoreDTO'
import { Code } from '../../bar-codes/models/code'

import { Inventory } from '../../inventories/models/inventory';
import { InventoryDTO } from '../../inventories/models/inventoryDTO';
import { InventoryQuantityDTO } from '../../inventories/models/inventoryQuantityDTO';
import { InventoryDetailDTO } from '../../inventories/models/inventoryDetailDTO';

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { ProductThirdService } from '../product-third.service'
import { BarCodeService } from '../../bar-codes/bar-code.service'
import { InventoriesService } from '../../inventories/inventories.service'
import { ProductsService } from '../../products/products.service';
import { CategoriesService } from '../../categories/categories.service';
import { MeasureUnitService } from '../../measure-unit/measure-unit.service';
import { AttributeService } from '../../attributes/attribute.service';
import { promise } from 'protractor';
import { resolve } from 'dns';

/*
*     constant of  your component
*/
declare var $: any
@Component({
  selector: 'app-product-third-new',
  templateUrl: './product-third-new.component.html',
  styleUrls: ['./product-third-new.component.scss']
})
export class ProductThirdNewComponent implements OnInit {
  @Output() saveThird = new EventEmitter();
  @Output() cancelThird = new EventEmitter();
  @Input() flagRedirect: boolean = true;
  @Input() inputCode: any = {code:''};

  form: FormGroup;
  token: Token;
  codeDTO: CodeDTO;
  commonStateStoreDTO: CommonStateStoreDTO;
  commonDTO: CommonStoreDTO;
  codeList: Code[];
  currentCode: Code;
  newProductThird: ProductThirdDTO;


  currentProduct: Product;

  attributeDetailList: AttributeDetailList[]
  attributes: Attribute[]
  measureUnit: MeasureUnit;
  categoryList: Category[];
  categoryListFathers: Category[];
  productor: Category;

  inventories: Inventory[];
  inventorieDTO: InventoryDTO;
  inventoryDetailDTO: InventoryDetailDTO;
  inventoryDetailDTOList: InventoryDetailDTO[];




  //generics
  CURRENT_ID_THIRD = 0;
  CURRENT_ID_THIRD_FATHER = 0;
  CURRENT_ID_CATEGORY = 0;
  // attributes


  STATE = 1;

  isVisible: boolean = false;

  constructor(private productService: ProductsService,
    private measureUnitService: MeasureUnitService,
    private attributeService: AttributeService, private router: Router, private route: ActivatedRoute,
    private productThirdService: ProductThirdService,
    private categoriesService: CategoriesService,
    private barCodeService: BarCodeService,
    public inventoriesService: InventoriesService,
    private fb: FormBuilder, private locStorage: LocalStorage
  ) {


    this.commonDTO = new CommonStoreDTO(null, null);
    this.commonStateStoreDTO = new CommonStateStoreDTO(1, null, null)
    this.codeDTO = new CodeDTO(null, null, null, null, null, null, null, this.commonStateStoreDTO)
    this.newProductThird = new ProductThirdDTO(null, null, null, null, null, this.commonStateStoreDTO, null, null, null, this.codeDTO)

    this.inventorieDTO = new InventoryDTO(null, this.commonDTO, this.commonStateStoreDTO);
    this.inventoryDetailDTO = new InventoryDetailDTO(null, null, null, null, this.commonStateStoreDTO)
    this.inventoryDetailDTOList = []
    this.categoryListFathers = []
    this.createControls();
    this.logNameChange();
  }

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  ngOnInit() {
    this.firstFormGroup = this.fb.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this.fb.group({
      secondCtrl: ['', Validators.required]
    });

    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {

      this.route.queryParams
        .subscribe(params => {

          // Defaults to 0 if no query param provided.
          this.token = this.locStorage.getToken();
          this.CURRENT_ID_THIRD = this.token.id_third ? this.token.id_third : null;
          this.CURRENT_ID_THIRD_FATHER = this.token.id_third_father > 0 ? this.token.id_third_father : null;

          this.CURRENT_ID_CATEGORY = params.father;


          this.getCodeList(this.STATE).then(() => {
            if (!this.flagRedirect) {
              this.isVisible= true;
              console.log('PASOOOOOOOOOOOOOOOO AQUI');
              console.log(this.inputCode);
              var code = this.codeList.find(item => item.code == this.inputCode['code']);

              if(code){
                this.form.patchValue({
                  selectCode: code
                });
              }else{
                this.cancelThird.emit('noProduct');
              }
              console.log(this.codeList);
            }
          });
          this.getAttributes();
          //
          // this.fatherListRecursive(1,1)
        });
    }

  }

  Login() {
    let link = ['/auth'];
    this.router.navigate(link);
  }

  createControls() {
    this.form = this.fb.group({

      //profile

      min_price: [{ value: '', disabled: true }, Validators.compose([


      ])],
      quantity: [10, Validators.compose([

      ])],

      standard_price: ['', Validators.compose([

      ])],
      location: ['', Validators.compose([

      ])],

      selectCode: ['', Validators.compose([
        Validators.required
      ])]
    });
  }


  loadData() {
    this.form.patchValue({
      min_price: this.form.value['selectCode'].suggested_price,
      standard_price: this.form.value['selectCode'].suggested_price
    });
  }

  logNameChange() {
    console.log('PASO POR LOGNAMECHANGE');
    this.isVisible = false;
    const code = this.form.get('selectCode');
    console.log(code);
    code.valueChanges.forEach(
      (value: string) => {

        if (code['value'] !== null) {
          this.isVisible = true;

          this.currentCode = code['value'];

          let id_prod = +code['value'].id_product;
          let id_attribute_list = +code['value'].id_attribute_list;
          let id_measure_unit = +code['value'].id_measure_unit;

          console.log("Code +> ", code['value']);
          //this.loadCode(id_prod,id_attribute_list,id_measure_unit);

          if (id_prod > 0) {
            this.getProductList(this.STATE, id_prod)
          }
          if (id_attribute_list > 0) {
            this.getAttributeList(this.STATE, null, id_attribute_list);
          }

          if (id_measure_unit > 0) {
            this.getMeasureUnitList(this.STATE, id_measure_unit);
          }
        }
      }
    );
  }

  createNewProductThird() {
    this.isVisible = false

    //this.newProductThird.id_third = this.CURRENT_ID_CATEGORY > 0 ? this.CURRENT_ID_CATEGORY : null;

    this.newProductThird.min_price = this.form.value["min_price"];
    this.newProductThird.standard_price = this.form.value["standard_price"];
    this.newProductThird.location = this.form.value["location"];
    this.newProductThird.state = this.commonStateStoreDTO;
    alert(this.CURRENT_ID_THIRD_FATHER)
    this.newProductThird.id_third = this.CURRENT_ID_THIRD_FATHER > 0 ? this.CURRENT_ID_THIRD_FATHER : this.CURRENT_ID_THIRD;
    alert(this.newProductThird.id_third)
    //this.common.name= this.form.value["name"];
    let id_code = this.form.value["selectCode"].id_code;

    console.log("PRODUCTO TERCEROOOO ", this.newProductThird, id_code)
    this.productThirdService.postCategory(this.newProductThird, id_code).subscribe(
      result => {
        if (result > 0) {
          // this.resetForm();
          // this.goBack();
          this.getInventoryList(this.STATE, null, this.newProductThird.id_third, this.newProductThird, result);
          this.saveThird.emit(true);
          return;
        } else {
          //this.openDialog();
          return;
        }
      }
    );
  }

  isVisibleReset = true;

  resetForm() {

    this.isVisible = false;

    this.form.reset();

    this.isVisible = false;

    this.cancelThird.emit('Cancelando');

  }

  goBack() {
    let link = ['/dashboard/business/inventory'];
    this.router.navigate(link);
  }
  getAttriByValue(id_attribute: number) {
    let attribute: Attribute
    attribute = this.attributes.find(attrib => attrib.id_attribute == id_attribute)
    return attribute.common.name ? attribute.common.name : ''
  }
  // The function recursive for build a father list, The List will use the categoryList

  fatherListRecursive(id_category?: number, id_category_father?: number) {

    // Base case when The category is father or productor
    if (id_category_father === undefined || id_category_father === null || id_category_father <= 0) {

      this.productor = this.categoryList.find(category => category.id_category == id_category)
      return this.categoryListFathers

    } else {

      let selector = this.categoryList.find(category => category.id_category == id_category_father)
      if (selector.id_category_father > 0) {
        this.categoryListFathers.push(selector)
      }
      return this.fatherListRecursive(selector.id_category, selector.id_category_father)

    }
  }


  // services
  getCodeList(state?: number, id_category?: number, id_category_father?: number, id_third?: number) {
    return new Promise((resolve, reject) => {
      this.barCodeService.getCodeList(state)
        .subscribe(
          (data: Code[]) => {
            this.codeList = data;
            resolve();
          },
          (error) => {
            console.log(error);
            reject();
          },
          () => {
          });
    });
  }

  getInventoryList(state?: number, id_inventory?: number, id_third?: number,
    newProductThird?: ProductThirdDTO, id_prod_third?: number) {

    this.inventoriesService.getInventoriesList(state, id_inventory, id_third)
      .subscribe((data: Inventory[]) => this.inventories = data,
        error => console.log(error),
        () => {
          if (this.inventories.length > 0) {
            let id_inv = this.inventories[0].id_inventory
            if (id_inv > 0) {
              this.managementDetailInventory(id_inv, newProductThird, id_prod_third)
            } else {
              this.createInventory(newProductThird, id_prod_third)
            }



          } else {
            this.createInventory(newProductThird, id_prod_third)
          }
        });
  }

  createInventory(newProductThird?: ProductThirdDTO, id_prod_third?: number) {

    this.commonDTO.name = "Inventario 1"
    this.commonDTO.name = "Inventario para contabilidad de su establecimiento"
    this.commonStateStoreDTO.state = 1

    this.inventorieDTO.id_third = newProductThird.id_third
    this.inventorieDTO.common = this.commonDTO
    this.inventorieDTO.state = this.commonStateStoreDTO

    let quantity = this.form.value["quantity"];
    this.inventoryDetailDTO.quantity = this.form.value["quantity"]
    this.inventoryDetailDTO.id_product_third = id_prod_third
    this.inventoryDetailDTO.state = this.commonStateStoreDTO
    this.inventoryDetailDTOList.push(this.inventoryDetailDTO)

    this.inventorieDTO.details = this.inventoryDetailDTOList



    this.inventoriesService.postInventory(this.inventorieDTO)
      .subscribe(
        result => {

          if (result > 0) {
            this.isVisible = false
            this.resetForm();


          }
        })
  }



  managementDetailInventory(id_inventory: number, newProductThird?: ProductThirdDTO, id_prod_third?: number) {

    this.commonDTO.name = "Inventario 1"
    this.commonDTO.name = "Inventario para contabilidad de su establecimiento"
    this.commonStateStoreDTO.state = 1



    let quantity = this.form.value["quantity"];
    this.inventoryDetailDTO.quantity = this.form.value["quantity"]
    this.inventoryDetailDTO.id_product_third = id_prod_third
    this.inventoryDetailDTO.state = this.commonStateStoreDTO
    this.inventoryDetailDTOList.push(this.inventoryDetailDTO)




    this.inventoriesService.postInventoryDetail(id_inventory, this.inventoryDetailDTOList)
      .subscribe(
        result => {

          if (result > 0) {

            this.resetForm();

          }
        })
  }


  //You are calling Services there

  getProductList(state_product?: number, id_product?: number, id_category?: number, stock?: number, stock_min?: number,
    img_url?: string, code?: string, id_tax?: number, id_common_product?: number, name_product?: number,
    description_product?: string, id_state_product?: number) {
    let productList: any[]

    this.productService.getProductList(state_product, id_product, id_category, stock, stock_min,
      img_url, code, id_tax, id_common_product, name_product,
      description_product, id_state_product)
      .subscribe((data: Product[]) => productList = data,
        error => console.log(error),
        () => {
          if (productList.length > 0) {
            this.currentProduct = productList[0];
            this.loadData();
            this.getCategory();


            console.log("EMITIR YA ")

          }

        });

  }

  // @todo You must see this method's name, If It name fail
  getMeasureUnitList(state?: number, id_category?: number, id_category_father?: number, id_third?: number) {
    let measureUnitList
    this.measureUnitService.getCategoryList(state)
      .subscribe((data: MeasureUnit[]) => measureUnitList = data,
        error => console.log(error),
        () => {
          if (measureUnitList.length > 0) {
            this.measureUnit = measureUnitList[0]
          }

        });

  }
  getCategory(state?: number, id_category?: number, id_common?: number, id_category_father?: number, img_url?: string, name?: string,
    description?: string, id_state?: number, creation_attribute?: Date, modify_attribute?: Date): void {


    this.categoryListFathers = []
    this.productor = new Category();

    this.categoriesService.getCategoryList(state, id_category, id_common, id_category_father, img_url, name,
      description, id_state, creation_attribute, modify_attribute)

      .subscribe((data: Category[]) => this.categoryList = data,
        error => console.log(error),
        () => {

          if (this.currentProduct != null && this.currentProduct.id_category > 0) {
            if (this.categoryList.length > 0) {

              let begin = this.categoryList.find(cat => cat.id_category === this.currentProduct.id_category);
              if (begin) {
                if (begin.id_category_father !== null && begin.id_category_father > 0) {
                  this.categoryListFathers.push(begin)
                  this.fatherListRecursive(begin.id_category, begin.id_category_father)
                  this.fatherListRecursive

                } else {
                  this.productor = begin
                }
              }



            } else {
              this.showNotification('top', 'right', 3, "No hay categorias", 'warning');
            }
          } else {
            this.showNotification('top', 'right', 3, "No hay datos de producto", 'warning');
          }


        });

  }

  getAttributeList(state_attrib_detail_list?: number, id_attribute_detail_list?: number, id_attribute_list?: number,
    id_state_attrib_detail_list?: number, id_attribute_value?: number,
    id_attribute?: number, id_common_attrib_value?: number, name_attrib_value?: string,
    description_attrib_value?: string, id_state_attrib_value?: number, state_attrib_value?: number) {

    this.attributeService.getAttributeDetailList(id_attribute_detail_list, id_attribute_list,
      id_state_attrib_detail_list, state_attrib_detail_list, id_attribute_value,
      id_attribute, id_common_attrib_value, name_attrib_value,
      description_attrib_value, id_state_attrib_value, state_attrib_value)
      .subscribe((data: AttributeDetailList[]) => this.attributeDetailList = data,
        error => console.log(error),
        () => {

          if (this.attributeDetailList.length > 0) {

          }


        });

  }
  getAttributes() {

    this.attributeService.getAttribute()
      .subscribe((data: Attribute[]) => this.attributes = data,
        error => console.log(error),
        () => {
        });
  }



  // Notification
  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: typeStr ? typeStr : type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }
}

