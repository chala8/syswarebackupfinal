import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';
import {from, Observable, of} from 'rxjs';
/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';

/** Import Model Data */
import { CommonState } from '../commons/commonState'
import { BillState } from './models/billState'
/** Import Model Data Transfer Object */
import { CommonStateDTO } from '../commons/commonStateDTO'
import { BillStateDTO } from './models/billStateDTO'
import { Bill } from '../billing/models/bill';


@Injectable()
export class BillStateService {
  api_uri = Urlbase[3] + '/billing-state';


  private options;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private locStorage: LocalStorage) {


    this.headers = new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization':  this.locStorage.getTokenValue(),
    });

    let token = localStorage.getItem('currentUser');

    this.options = { headers: this.headers };
  }

  public getInventoriesList = (state_inventory?: number, id_inventory?: number,
    id_third?: number, id_common_inventory?: number,
    name_inventory?: string, description_inventory?: string,
    id_state_inventory?: number): Observable<{} | Bill[]> => {

    let params = new HttpParams();
    let Operaciones = [];

    Operaciones.push(['id_inventory', id_inventory ? "" + id_inventory : null]);
    Operaciones.push(['id_third', id_third ? "" + id_third : null]);
    Operaciones.push(['id_common_inventory', id_common_inventory ? "" + id_common_inventory : null]);
    Operaciones.push(['name_inventory', name_inventory ? "" + name_inventory : null]);
    Operaciones.push(['description_inventory', description_inventory ? "" + description_inventory : null]);
    Operaciones.push(['id_state_inventory', id_state_inventory ? "" + id_state_inventory : null]);
    Operaciones.push(['state_inventory', state_inventory ? "" + state_inventory : null]);

    for(let n = 0;n<Operaciones.length;n++){ if(Operaciones[n][1] != null){ params = params.append(Operaciones[n][0],  Operaciones[n][1]); } }
    this.options.params = params;
    let promesa = new Promise((resolve, reject) => {
      this.http.get<Bill[]>(this.api_uri, this.options).subscribe(value => {resolve(value)},error => {this.handleError(error);reject(error)});
    });
    return from(promesa);
  };



  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      // @ts-ignore
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return null;//Observable.throw(errMsg);
  }


}
