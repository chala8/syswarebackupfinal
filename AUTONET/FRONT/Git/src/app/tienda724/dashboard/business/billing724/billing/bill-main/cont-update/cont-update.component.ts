import { Component, OnInit, Inject } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { LocalStorage } from 'src/app/shared/localStorage';
import { Urlbase } from 'src/app/shared/urls';
import { MAT_DIALOG_DATA,MatDialogRef, MatDialog } from '@angular/material/dialog';
import 'bootstrap-notify';
import * as jQuery from 'jquery';
let $: any = jQuery;


@Component({
  selector: 'app-cont-update',
  templateUrl: './cont-update.component.html',
  styleUrls: ['./cont-update.component.css']
})
export class ContUpdateComponent implements OnInit {



  constructor(public dialogRef: MatDialogRef<ContUpdateComponent>,public dialog: MatDialog,
              private http2: HttpClient,
              private locStorage: LocalStorage,
              @Inject(MAT_DIALOG_DATA) public data: any ) { }

  details=[]
  
  //nivel 1
  selectedFirstCC = "";
  firstLvlCC = [];
  //nivel 2
  selectedCC2 = "";
  lvlCC2 = [];
  //nivel 3
  selectedCC3 = "";
  lvlCC3 = [];
  //nivel 4
  selectedCC4 = "";
  lvlCC4 = [];
  //nivel 5
  selectedCC5 = "";
  lvlCC5 = [];
  //naturaleza
  selectedNat = "C"
  //valorCuenta
  cuenta = "0";
  //notasDetalle
  notesD= "";


  ngOnInit() {
    this.getFirstlvlCC();
    console.log(this.data)
    this.http2.get(Urlbase[2]+"/kazu724/getDetailsDocument?id_document="+this.data.element.id_DOCUMENT).subscribe(
      element => {
        //@ts-ignore
        this.details =  element;
      }
     );
  }

  calculateBalance(){
    let creds = 0;
    let debts = 0;
    this.details.forEach( (item, index) => {
      if(item.naturaleza == 'C') creds+=item.valor;
      if(item.naturaleza == 'D') debts+=item.valor;
    });
    return debts-creds;
  }


  approveDoc(){
    if(this.calculateBalance()==0){
      this.http2.put(Urlbase[2]+"/kazu724/updateDocument?id_status="+2+"&notes="+this.data.element.notes+"&id_doc="+this.data.element.id_DOCUMENT,{}).subscribe( item => {
        this.showNotification('top', 'center', 3, "<h3>El documento se aprobo exitosamente.</h3> ", 'info');
        this.data.element.document_STATUS = "Documento Aprobado"
      });
    }else{
      this.showNotification('top', 'center', 3, "<h3>Los debitos y creditos no son iguales.</h3> ", 'danger');
    }
  }


  updateNotes(){
    this.http2.put(Urlbase[2]+"/kazu724/updateDocument?id_status="+1+"&notes="+this.data.element.notes+"&id_doc="+this.data.element.id_DOCUMENT,{}).subscribe( item => {
      this.showNotification('top', 'center', 3, "<h3>El documento se anulo exitosamente.</h3> ", 'info');

    });
}



  killDoc(){
      this.http2.put(Urlbase[2]+"/kazu724/updateDocument?id_status="+3+"&notes="+this.data.element.notes+"&id_doc="+this.data.element.id_DOCUMENT,{}).subscribe( item => {
        this.showNotification('top', 'center', 3, "<h3>El documento se anulo exitosamente.</h3> ", 'info');
        this.data.element.document_STATUS = "Documento Anulado"
      });
  }



  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
      type: typeStr ? typeStr : type[id_type ? id_type : 2],
      timer: 200,
      placement: {
        from: from,
        align: align
      }
    });
  }


  
  getFirstlvlCC(){
    this.http2.get(Urlbase[2] + "/kazu724/getcodcuentagen").subscribe(list => {
      //@ts-ignore
      this.firstLvlCC = list;
      this.selectedCC2 = "";
      this.selectedCC3 = "";
      this.selectedCC4 = "";
      this.selectedCC5 = "";
    })
  }

  getlvlCC2(){
    this.http2.get(Urlbase[2] + "/kazu724/getcodcuenta?cp="+this.selectedFirstCC).subscribe(list => {
      //@ts-ignore
      this.lvlCC2 = list;
      this.selectedCC3 = "";
      this.selectedCC4 = "";
      this.selectedCC5 = "";
    })
  }

  getlvlCC3(){
    this.http2.get(Urlbase[2] + "/kazu724/getcodcuenta?cp="+this.selectedCC2).subscribe(list => {
      //@ts-ignore
      this.lvlCC3 = list;
      this.selectedCC4 = "";
      this.selectedCC5 = "";
    })
  }

  getlvlCC4(){
    this.http2.get(Urlbase[2] + "/kazu724/getcodcuenta?cp="+this.selectedCC3).subscribe(list => {
      //@ts-ignore
      this.lvlCC4 = list;
      this.selectedCC5 = "";
    })
  }

  getlvlCC5(){
    this.http2.get(Urlbase[2] + "/kazu724/getcodcuenta?cp="+this.selectedCC4).subscribe(list => {
      //@ts-ignore
      this.lvlCC5 = list;
    })
  }  


  
  addDetail(){
  if(this.selectedFirstCC=="" || this.cuenta=="0" || this.notesD == ""){
    this.showNotification('top', 'center', 3, "<h3>Faltan datos para poder agregar el detalle.</h3> ", 'danger');
  }else{
    if(this.selectedCC2==""){
      this.http2.post(Urlbase[2]+"/kazu724/detail",{
          cc: this.selectedFirstCC,
          naturaleza: this.selectedNat,
          valor: Number(this.cuenta),
          notes: this.notesD,
          id_document: this.data.element.id_DOCUMENT,
          id_country: 169
        }).subscribe(response => {
          this.http2.get(Urlbase[2]+"/kazu724/getDetailsDocument?id_document="+this.data.element.id_DOCUMENT).subscribe(
          element => {
            this.showNotification('top', 'center', 3, "<h3>Se agrego el detalle exitosamente.</h3> ", 'info');
            //@ts-ignore
            this.details =  element;
          });
        });
    }else{
      if(this.selectedCC3==""){
        this.http2.post(Urlbase[2]+"/kazu724/detail",{
          cc: this.selectedCC2,
          naturaleza: this.selectedNat,
          valor: Number(this.cuenta),
          notes: this.notesD,
          id_document: this.data.element.id_DOCUMENT,
          id_country: 169
        }).subscribe(response => {
          this.http2.get(Urlbase[2]+"/kazu724/getDetailsDocument?id_document="+this.data.element.id_DOCUMENT).subscribe(
          element => {
            this.showNotification('top', 'center', 3, "<h3>Se agrego el detalle exitosamente.</h3> ", 'info');
            //@ts-ignore
            this.details =  element;
          });
        });
      }else{
        if(this.selectedCC4==""){
          this.http2.post(Urlbase[2]+"/kazu724/detail",{
            cc: this.selectedCC3,
            naturaleza: this.selectedNat,
            valor: Number(this.cuenta),
            notes: this.notesD,
            id_document: this.data.element.id_DOCUMENT,
            id_country: 169
          }).subscribe(response => {
            this.http2.get(Urlbase[2]+"/kazu724/getDetailsDocument?id_document="+this.data.element.id_DOCUMENT).subscribe(
            element => {
              this.showNotification('top', 'center', 3, "<h3>Se agrego el detalle exitosamente.</h3> ", 'info');
              //@ts-ignore
              this.details =  element;
            });
          });
        }else{
          if(this.selectedCC5==""){
            this.http2.post(Urlbase[2]+"/kazu724/detail",{
              cc: this.selectedCC4,
              naturaleza: this.selectedNat,
              valor: Number(this.cuenta),
              notes: this.notesD,
              id_document: this.data.element.id_DOCUMENT,
              id_country: 169
            }).subscribe(response => {
              this.http2.get(Urlbase[2]+"/kazu724/getDetailsDocument?id_document="+this.data.element.id_DOCUMENT).subscribe(
              element => {
                this.showNotification('top', 'center', 3, "<h3>Se agrego el detalle exitosamente.</h3> ", 'info');
                //@ts-ignore
                this.details =  element;
              });
            });
          }else{
            this.http2.post(Urlbase[2]+"/kazu724/detail",{
              cc: this.selectedCC5,
              naturaleza: this.selectedNat,
              valor: Number(this.cuenta),
              notes: this.notesD,
              id_document: this.data.element.id_DOCUMENT,
              id_country: 169
            }).subscribe(response => {
              this.http2.get(Urlbase[2]+"/kazu724/getDetailsDocument?id_document="+this.data.element.id_DOCUMENT).subscribe(
              element => {
                this.showNotification('top', 'center', 3, "<h3>Se agrego el detalle exitosamente.</h3> ", 'info');
                //@ts-ignore
                this.details =  element;
              });
            });
          }
        }

      }
    }

    }
    this.selectedFirstCC="";
    this.selectedCC2="";
    this.selectedCC3="";
    this.selectedCC4="";
    this.selectedCC5="";
    this.selectedNat="C";
    this.cuenta="0";
    this.notesD="";
  }


  deleteDetail(item){
    this.http2.delete(Urlbase[2]+"/kazu724/deleteDocumentDetail?id_detail="+item.id_DOCUMENT_DETAIL).subscribe(item => {
      this.showNotification('top', 'center', 3, "<h3>El detalle se borro exitosamente.</h3> ", 'info');
      this.http2.get(Urlbase[2]+"/kazu724/getDetailsDocument?id_document="+this.data.element.id_DOCUMENT).subscribe(
      element => {
        //@ts-ignore
        this.details =  element;
      }
     );

    })
  }



}
