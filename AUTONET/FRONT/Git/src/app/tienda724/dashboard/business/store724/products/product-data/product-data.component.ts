import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import {Observable,of} from 'rxjs';
//import 'rxjs/add/observable/of';
import * as _ from 'lodash';



import {MatTabChangeEvent, VERSION} from '@angular/material';


import { LocalStorage } from '../../../../../../shared/localStorage';
import { Token } from '../../../../../../shared/token';

import { ProductsService } from '../products.service';
import { Product } from '../models/product';


var categoryList:Product[];
var categoryListGlobal:Product[];

@Component({
  selector: 'app-product-data',
  templateUrl: './product-data.component.html',
  styleUrls: ['./product-data.component.scss']
})
export class ProductDataComponent implements OnInit {

  isMyProduct=true;
  token:Token;
  ID_THIRD_TYPE: number;
  STATE=1
  CURRENT_ID_THIRD = 0;

  thirdAux:Product[];






    displayedColumns = ['position', 'name', 'weight', 'symbol', 'direccion', 'opciones'];
    displayedColumns_General = ['position', 'name', 'weight', 'symbol', 'direccion'];
    dataSource:ProductDataSource

  constructor(public locStorage: LocalStorage,
    private _router: Router,
    public productService: ProductsService) { }

  ngOnInit() {
    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD=this.token.id_third;
      this.ID_THIRD_TYPE = 23;

      if (this.CURRENT_ID_THIRD!==null && this.CURRENT_ID_THIRD>0){

        this.getProductList(this.STATE)


      }


    }
  }

  getProductList(state?:number,id_category?:number,id_category_father?:number,id_third?:number){

        this.productService.getProductList(state)
        .subscribe((data: Product[]) => categoryList = data,
        error => console.log(error),
        () => {

          this.dataSource = new ProductDataSource();
        });

      }

      Login() {
        let link = ['/auth'];
        this._router.navigate(link);
      }

      editProduct(product:Product){
        this._router.navigate(['/dashboard/business/product/edit',product.id_product] );

      }

      addProduct(product:Product){

        this._router.navigate(['/dashboard/business/product/new'],{queryParams:{father:product.id_product}} );

      }

      deleteCategory(id_product) {

            this.productService.Delete(id_product)
              .subscribe(
              result => {

                if (result === true) {
                  categoryList = _.filter(categoryList, function (f) { return f.id_product !== id_product; });
                  this.dataSource = new ProductDataSource();

                  alert("Eliminado correctamente");

                  return;
                } else {
                  //this.openDialog();
                  return;
                }
              })

          }

}
export class ProductDataSource extends DataSource<any> {
  /** Connect function called by the table to retrieve one stream containing the data to render. */

  connect(): Observable<Product[]> {

    return of(categoryList);
  }

  disconnect() {}
}
