import { Injectable } from '@angular/core';

import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import {from, Observable, of} from 'rxjs';


/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';
import { Code } from './models/code'
import { CodeDTO } from './models/codeDTO'

@Injectable()
export class BarCodeService {

  api_uri = Urlbase[2] + '/codes';
  private options;
  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(private http: HttpClient, private locStorage: LocalStorage) {


    this.headers = new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization':  this.locStorage.getTokenValue(),
    });


    this.options = {
      headers: this.headers
    };
  }

  public getCodeList = (state ? : number, id_code ? : number, id_third_cod ? : number, code ? : string,
                        id_product ? : number, img_url ? : string, id_measure_unit ? : number,
                        id_attribute_list ? : number, id_state ? : number, modify_attribute ? : Date): Observable < {} | Code[] > => {

    let params = new HttpParams();
    let Operaciones = [];

    Operaciones.push(['id_code', id_code ? "" + id_code : null]);
    Operaciones.push(['code', code ? "" + code : null]);
    Operaciones.push(['img', img_url ? "" + img_url : null]);
    Operaciones.push(['id_product', id_product ? "" + id_product : null]);
    Operaciones.push(['id_measure_unit', id_measure_unit ? "" + id_measure_unit : null]);
    Operaciones.push(['id_attribute_list', id_attribute_list ? "" + id_attribute_list : null]);
    Operaciones.push(['id_state', id_state ? "" + id_state : null]);
    Operaciones.push(['state', state ? "" + state : null]);
    Operaciones.push(['id_third_cod', id_third_cod ? "" + id_third_cod : null]);
    Operaciones.push(['modify_code', modify_attribute ? "" + modify_attribute : null]);

    for (let n = 0; n < Operaciones.length; n++) {
      if (Operaciones[n][1] != null) {
        params = params.append(Operaciones[n][0], Operaciones[n][1]);
      }
    }
    this.options.params = params;
    let promesa = new Promise((resolve, reject) => {
      this.http.get< Code[] >(this.api_uri, this.options)
        .subscribe(value => {resolve(value)},error => {this.handleError(error);reject(error)});
    });
    return from(promesa);
  };

  public Delete = (id_category: number): Observable < Response | any > => {
    let promesa = new Promise((resolve, reject) => {
      this.http.delete(this.api_uri + '/' + id_category, this.options)
        .subscribe(value => {resolve(true)},error => {this.handleError(error);reject(false)});
    });
    return from(promesa);
  };

  public postCategory = (body: CodeDTO): Observable < Boolean | any > => {

    let promesa = new Promise((resolve, reject) => {
      this.http.post(this.api_uri, body, {
        headers: this.headers
      })
        .subscribe(value => {resolve(true)},error => {this.handleError(error);reject(false)});
    });
    return from(promesa);
  };


  public putCode = (id: number, body: CodeDTO): Observable < Boolean | any > => {
    let promesa = new Promise((resolve, reject) => {
      this.http.put(this.api_uri + "/" + id, body, {
        headers: this.headers
      })
        .subscribe(value => {resolve(true)},error => {this.handleError(error);reject(false)});
    });
    return from(promesa);
  };

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      // @ts-ignore
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return null; //Observable.throw(errMsg);
  }

}
