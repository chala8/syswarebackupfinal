import { Component, OnInit,Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// materials
import { MatTabChangeEvent, VERSION, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

/*
*     others component
*/
import { BillDocumentComponent } from '../bill-document/bill-document.component'


/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { InventoryDetail } from '../../../store724/inventories/models/inventoryDetail'
import { Inventory } from '../../../store724/inventories/models/inventory';
// DTO
import { CommonStateDTO } from '../../commons/commonStateDTO'
import { DocumentDTO } from '../../commons/documentDTO'
import { InventoryQuantityDTO } from '../../../store724/inventories/models/inventoryQuantityDTO'
import { BillDTO } from '../models/billDTO';
import { DetailBillDTO } from '../models/detailBillDTO';


/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { InventoriesService } from '../../../store724/inventories/inventories.service';
import { BillingService } from '../billing.service';
import { DetailBill } from '../models/detailBill';


/*
*     constant of  your component
*/
let $: any = jQuery;
import * as jQuery from 'jquery';
import 'bootstrap-notify';
@Component({
  selector: 'app-bill-sale',
  templateUrl: './bill-sale.component.html',
  styleUrls: ['./bill-sale.component.scss']
})
export class BillSaleComponent implements OnInit {
  // attribute
  CURRENT_ID_THIRD = 0;
  CURRENT_ID_THIRD_PATHER = 0;
  ID_INVENTORY_TEMP = 0;
  STATE = 1
  ID_BILL_TYPE = 0;
  TOTAL_PRICE = 0;

  TYPE_NAME: string;

  // models
  token: Token;
  form: FormGroup;

  //list
  itemLoadBilling: InventoryDetail;
  inventoryList: InventoryDetail[];




  // DTO's
  inventoryQuantityDTO: InventoryQuantityDTO;
  inventoryQuantityDTOList: InventoryQuantityDTO[];
  detailBillingDTOList: any[]
  commonStateDTO: CommonStateDTO;
  documentDTO: DocumentDTO;
  detailBillDTO: DetailBillDTO;
  detailBillDTOList: DetailBillDTO[];
  billDTO: BillDTO;
  @Input() ID_BILLTYPE:number=0;





  constructor(public locStorage: LocalStorage, private _router: Router,
    private fb: FormBuilder, private billingService: BillingService,
    public inventoriesService: InventoriesService,public dialog: MatDialog,
    private route: ActivatedRoute) {

    this.detailBillingDTOList = []
    this.inventoryList = []
    this.inventoryQuantityDTOList = [];
    this.detailBillDTOList = [];

    this.commonStateDTO = new CommonStateDTO(1, null, null);
    this.documentDTO = new DocumentDTO(null, null);
    this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null, null);
    this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO,null);
    this.billDTO = new BillDTO(this.locStorage.getIdStore(),null, null, null, null, null, null, null, null, null, null, null, null, this.commonStateDTO, null, this.detailBillDTOList, this.documentDTO);


    this.createControls();
    this.logNameChange();



  }

  ngOnInit() {

    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD = this.token.id_third;
      this.CURRENT_ID_THIRD_PATHER = this.token.id_third_father;

      if (this.CURRENT_ID_THIRD !== null && this.CURRENT_ID_THIRD > 0) {

        this.route.queryParams
          .subscribe(params => {
            // get id _bill_type
            this.ID_BILL_TYPE = +params['type'] || 0;
          });


      }
    }
    if(this.ID_BILLTYPE>0){
      this.ID_BILL_TYPE=this.ID_BILLTYPE
    }

  }



  //There is to receive DETAILS
  getDetails(event) {
    this.detailBillingDTOList = event;
    console.log("Nueva <lista ", this.detailBillingDTOList)
  }


  showType(event) {

    this.TYPE_NAME = event.type;
    this.ID_INVENTORY_TEMP = event.inventory;
    this.getInventoryList(this.STATE, null, null, this.ID_INVENTORY_TEMP)


  }

  //The user auth is not valid
  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }

  // start controls
  createControls() {
    this.form = this.fb.group({
      codeProd: ['', Validators.compose([
        Validators.required
      ])],
      title: ['', Validators.compose([
        Validators.required
      ])],
      body: ['', Validators.compose([
        Validators.required
      ])],
    });
  }

  loadData() {
    this.form.patchValue({
      codeProd: ''
    });
  }

  logNameChange() {
    const codeProdControl = this.form.get('codeProd');
    codeProdControl.valueChanges.forEach((value: string) => {
      this.itemLoadBilling = codeProdControl['value'];
      console.log(" PROD", codeProdControl);

      if (this.itemLoadBilling) { // IF Active all
        console.log(" NUEVO PRODUCT ", this.itemLoadBilling);
        // call function  loadDatailBillin()
        this.loadDatailBilling(this.itemLoadBilling)

      }
    });
  }// end controls

  loadDatailBilling(element) {
    let flag = false;
    //element.detail.quantity=1;
    if (element) {
      if (element.detail.quantity > 0) {

        if (this.detailBillingDTOList.length > 0) {

          for (let i = 0; i < this.detailBillingDTOList.length; i++) {
            console.log("ITEM-> ", this.detailBillingDTOList[i])
            console.log("ELEMEN ", element)

            if (this.detailBillingDTOList[i].item.detail.id_inventory_detail === element.detail.id_inventory_detail) {
              flag = true;
            }
          }

          if (!flag) {
            this.TOTAL_PRICE = this.TOTAL_PRICE + (+element.description.standard_price)
            this.detailBillingDTOList.push(
              { 'is_exit': 1, 'item': element, 'OldQuantity': element.detail.quantity, 'quantity': 1 }
            );
          }

        } else {
          this.TOTAL_PRICE = this.TOTAL_PRICE + (+element.description.standard_price)
          this.detailBillingDTOList.push(
            { 'is_exit': 1, 'item': element, 'OldQuantity': element.detail.quantity, 'quantity': 1 }
          );
        }

      } else {
        this.showNotification('top', 'center', 3, "<h3>El Producto esta agotado</h3> ", 'warning')
      }



      console.log(" -  > ", this.TOTAL_PRICE)

      this.loadData();


    }
  }

  // end process
  save() {
    this.detailBillDTOList = []
    console.log("INICIA PROCESO DE, COMPRA");
    this.commonStateDTO.state = 1
    this.inventoryQuantityDTOList = [];
    this.documentDTO.title = 'Movimiento tipo ' + this.TYPE_NAME
    this.documentDTO.body = 'TEST';
    /**
     * building detailBill and Quantity
     */
    for (let element of this.detailBillingDTOList) {
      this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null, null);
      this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO,null);
      //building detailBill
      this.detailBillDTO.price = (+element.quantity) * (+element.item.description.standard_price)
      this.detailBillDTO.tax = 0
      this.detailBillDTO.id_product_third = element.item.description.id_product_third
      this.detailBillDTO.tax_product = 0
      this.detailBillDTO.state = this.commonStateDTO;

      this.detailBillDTO.quantity = element.quantity;
      this.detailBillDTOList.push(this.detailBillDTO);

      // Inv Quantity
      this.inventoryQuantityDTO.id_inventory_detail = element.item.detail.id_inventory_detail;
      this.inventoryQuantityDTO.id_product_third = element.item.description.id_product_third;
      this.inventoryQuantityDTO.code = element.item.detail.code;

      // Inv Quantity for discount tienda
      this.inventoryQuantityDTO.quantity = element.quantity;

      this.inventoryQuantityDTOList.push(
        this.inventoryQuantityDTO
      );
    }

    if (this.detailBillDTOList.length > 0) {

      this.billDTO.id_third_employee = this.token.id_third;
      this.billDTO.id_third = this.token.id_third_father;
      this.billDTO.id_bill_type = this.ID_BILL_TYPE;
      this.billDTO.id_bill_state = 1;
      this.billDTO.purchase_date = new Date();
      this.billDTO.subtotal = this.TOTAL_PRICE;
      this.billDTO.tax = 0;
      this.billDTO.totalprice = this.TOTAL_PRICE;
      this.billDTO.discount = 0;
      this.billDTO.documentDTO = this.documentDTO;
      this.billDTO.state = this.commonStateDTO;

      this.billDTO.details = this.detailBillDTOList;
      console.log("this.billDTO -> ", this.billDTO)
      this.billingService.postBillResource(this.billDTO,0)
        .subscribe(
        result => {
          if (result) {
            // alert(this.ID_INVENTORY_TEMP)
            this.beginPlusOrDiscount(this.ID_INVENTORY_TEMP, this.inventoryQuantityDTOList)
          }
        });

    }


  }

  // Notifications

  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: typeStr ? typeStr : type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }

  // services
  getInventoryList(state_inv_detail?: number, state_product?: number, id_inventory_detail?: number,
    id_inventory?: number, id_product_third?: number, location?: number,
    id_third?: number, id_category_third?: number, quantity?: number, id_state_inv_detail?: number,
    id_product?: number, id_category?: number, stock?: number,
    stock_min?: number, img_url?: string, id_tax?: number,
    id_common_product?: number, name_product?: string,
    description_product?: string, id_state_product?: number,


    id_state_prod_third?: number, state_prod_third?: number,
    id_measure_unit?: number, id_measure_unit_father?: number,
    id_common_measure_unit?: number, name_measure_unit?: string,
    description_measure_unit?: string, id_state_measure_unit?: number,
    state_measure_unit?: number, id_code?: number,
    code?: number, img?: string,
    id_attribute_list?: number,
    id_state_cod?: number, state_cod?: number,
    attribute?: number,
    attribute_value?: number) {

    this.inventoriesService.getInventoriesDetailList(state_inv_detail, state_product, id_inventory_detail,

      id_inventory, quantity, id_state_inv_detail,
      id_product, id_category, stock,
      stock_min, img_url, id_tax,
      id_common_product, name_product,
      description_product, id_state_product,
      id_product_third, location,
      id_third, id_category_third,

      id_state_prod_third, state_prod_third,
      id_measure_unit, id_measure_unit_father,
      id_common_measure_unit, name_measure_unit,
      description_measure_unit, id_state_measure_unit,
      state_measure_unit, id_code,
      code, img,
      id_state_cod, state_cod)


      .subscribe((data: InventoryDetail[]) => this.inventoryList = data,
      error => console.log(error),
      () => {
        if (this.inventoryList.length > 0) {

        }
      });
  }

  beginPlusOrDiscount(id_inventory, inventoryQuantityDTOList) {
    console.log("enviar... ", this.inventoryQuantityDTOList);

    this.inventoriesService.putDiscountInventory(id_inventory, this.inventoryQuantityDTOList)
      .subscribe(result => {
        if (result) {
          console.log("CORRECT")
          this.itemLoadBilling = null;
          this.detailBillingDTOList = [];
          this.form.reset();
          this.getInventoryList(this.STATE, null, null, this.ID_INVENTORY_TEMP)

          this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info')

        } else {
          this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'info')

        }
      })
  }

  addDocument(element) {
    let oldQuantity = element.quantity;
    let inventoryDetail = null;
    let flag = true;

    let dialogRef = this.dialog.open(BillDocumentComponent, {
      height: '450px',
      width: '600px',
      data: {
        type_name:this.TYPE_NAME,
        doc: this.documentDTO
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        console.log('DOC ->> ',this.documentDTO);
        console.log('RESULT ->> ',result);
      }
    });

  }

}
