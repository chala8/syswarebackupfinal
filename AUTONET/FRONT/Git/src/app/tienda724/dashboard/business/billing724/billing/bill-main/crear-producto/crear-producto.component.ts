import {Component, OnInit, ViewChild} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from 'src/app/shared/localStorage';
import { Router } from '@angular/router';
import { Urlbase } from 'src/app/shared/urls';
import { BillingService } from '../../billing.service';
let $: any = jQuery;
import * as jQuery from 'jquery';
import 'bootstrap-notify'
import { MatDialog } from '@angular/material/dialog';
import { UpdateNewProductComponent } from '../update-new-product/update-new-product.component';
import {MatTableDataSourceWithCustomSort} from '../pedidos/pedidos.component';
import {MatSort, MatSortable} from '@angular/material';

@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styleUrls: ['./crear-producto.component.css']
})
export class CrearProductoComponent implements OnInit {

  tax=0;
  price = 0;
  cost = 0;
  SelectedStore=this.locStorage.getIdStore();
  Stores = [];
  prodname = "";
  barcode = "";
  quantity=0;
  id_menu = 262;
  products = [];

  //COSAS PARA LA TABLA
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  dataSource = new MatTableDataSourceWithCustomSort(this.products);
  expandedElement: any | null;
  GetKeys(){
    return ["product_STORE_NAME","id_CODE","code"];
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  constructor(public dialog: MatDialog, private billingService : BillingService,private http: HttpClient, public locStorage: LocalStorage,public router: Router,) { }

  ngOnInit() {

    //PROTECCION URL INICIA
    console.log(JSON.stringify(this.locStorage.getMenu()));
    const elem = this.locStorage.getMenu().find(item => item.id_menu == this.id_menu);

    if(!elem){
      // noinspection JSIgnoredPromiseFromCall
      this.router.navigateByUrl("/dashboard/business/movement/nopermision");
    }
    //PROTECCION URL TERMINA


    this.billingService.getStoresByThird(this.locStorage.getToken().id_third).subscribe(data => {
      console.log(data);
      this.Stores = data;
      let storeList = "";
      data.forEach(element => {
        //@ts-ignore
        storeList = storeList + element.id_STORE + ",";
        this.http.post(Urlbase[2]+"/resource/getnewproducts?id_store="+storeList.substring(0,storeList.length-1),{}).subscribe(data => {
        // @ts-ignore
          this.products = data;
          this.dataSource = new MatTableDataSourceWithCustomSort(this.products);
          this.dataSource.sort = this.sort;
      })
      });

    });
    this.tax=0

  }

  acept(){
    if(this.cost!=0 && this.price!=0 && this.price>= this.cost && this.price>0 && this.cost>0 &&
      this.quantity>=0 && this.barcode!='' && this.prodname!=''){
        this.http.post( Urlbase[2]+"/products/v2?barcode="+this.barcode+"&idstore="+this.SelectedStore+"&producto="+this.prodname+"&costo="+this.cost+"&iva="+this.tax+"&precio="+this.price+"&cantidad="+this.quantity,{}).subscribe(data=> {
          if(data==1){
            this.showNotification('top', 'center', 3, "<h3>Producto Creado con exito.</h3> ", 'info');
            this.cancel();
          }else{
            this.showNotification('top', 'center', 3, "<h3>Algo fallo al crear el producto</h3> ", 'danger');
            this.cancel();
          }
        });
      console.log("URL: ", Urlbase[2]+"/products/v2?barcode="+this.barcode+"&idstore="+this.SelectedStore+"&producto="+this.prodname+"&costo="+this.cost+"&iva="+this.tax+"&precio="+this.price+"&cantidad="+this.quantity)
    }else{
      this.showNotification('top', 'center', 3, "<h3>Los datos ingresados no son validos, revise que ningun dato sea vacio y que el precio sea mayor o igual al costo.</h3> ", 'danger');
    }
 }

  cancel(){
    this.tax=0;
    this.price = 0;
    this.cost = 0;
    this.SelectedStore=this.locStorage.getIdStore();
    this.Stores = [];
    this.prodname = "";
    this.barcode = "";
    this.quantity=0;
  }


  updateButton(elem){

      const dialogRef = this.dialog.open(UpdateNewProductComponent, {
        width: '60vw',
        height: '75vh',
        data: { element: elem }
      });

      dialogRef.afterClosed().subscribe(result => {
        if(result){
          this.ngOnInit();
        }
      })


  }


  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
      type: typeStr ? typeStr : type[id_type ? id_type : 2],
      timer: 200,
      placement: {
        from: from,
        align: align
      }
    });
  }


}
