import { Component, OnInit } from '@angular/core';
import {ThirdService} from '../../../../thirds724/third/third.service';

@Component({
  selector: 'app-update-legal-data',
  templateUrl: './update-legal-data.component.html',
  styleUrls: ['./update-legal-data.component.scss']
})
export class UpdateLegalDataComponent implements OnInit {
  legalData;
  idThirdFather;
  constructor(private thirdService: ThirdService) { }

  ngOnInit() {
    this.thirdService.getThirdList().subscribe(res=>{
      console.log(res,"the third")
      //this.id_employee = JSON.parse(localStorage.getItem("currentPerson")).id_person;
      var employee;
      // @ts-ignore
      employee = res.filter(item => item.profile.id_person === this.id_employee);
      console.log(employee)
      this.idThirdFather = employee[0].id_third_father;
      this.thirdService.getIdLegalDataByThird(this.idThirdFather).subscribe(res=>{
        this.thirdService.getLegalData(res).subscribe(res2=>{
          console.log(res)
          this.legalData = res;
        })
      })
    })
  }

}
