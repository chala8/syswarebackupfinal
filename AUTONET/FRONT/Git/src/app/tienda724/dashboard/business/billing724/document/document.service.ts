import { Injectable } from '@angular/core';

import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import {from, Observable, of} from 'rxjs';


/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';
import { Document } from '../commons/document'
import { DocumentDTO } from '../commons/documentDTO'


@Injectable()
export class DocumentService {

  api_uri = Urlbase[3] + '/documents';
  private options;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private locStorage: LocalStorage) {


    this.headers = new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization':  this.locStorage.getTokenValue(),
    });

    let token = localStorage.getItem('currentUser');

        this.options = { headers: this.headers };
  }

  public getCategoryList = (state_measure_unit?: number,id_measure_unit?:number,id_measure_unit_father?:number,
    id_common_measure_unit?:number, name_measure_unit?: string,
    description_measure_unit?:string, id_state_measure_unit?:number, creation_measure_unit?: Date, modify_measure_unit?:Date): Observable<{}|Document[]> => {


      let params = new HttpParams();
      let Operaciones = [];

      Operaciones.push(['id_measure_unit',  id_measure_unit?""+id_measure_unit:null]);
      Operaciones.push(['id_measure_unit_father',  id_measure_unit_father?""+id_measure_unit_father:null]);
      Operaciones.push(['id_common_measure_unit', id_common_measure_unit?""+id_common_measure_unit:null]);
      Operaciones.push(['name_measure_unit', name_measure_unit?""+name_measure_unit:null]);
      Operaciones.push(['description_measure_unit', description_measure_unit?""+description_measure_unit:null]);
      Operaciones.push(['id_state_measure_unit', id_state_measure_unit?""+id_state_measure_unit:null]);
      Operaciones.push(['state_measure_unit', state_measure_unit?""+state_measure_unit:null]);
      Operaciones.push(['creation_measure_unit', creation_measure_unit?""+creation_measure_unit:null]);
      Operaciones.push(['modify_measure_unit', modify_measure_unit?""+modify_measure_unit:null]);

      for(let n = 0;n<Operaciones.length;n++){ if(Operaciones[n][1] != null){ params = params.append(Operaciones[n][0],  Operaciones[n][1]); } }
      this.options.params = params;
    let promesa = new Promise((resolve, reject) => {
      this.http.get<Document[]>(this.api_uri, this.options )
        .subscribe(value => {resolve(value)},error => {this.handleError(error);reject(error)});
    });
    return from(promesa);
  };

  public Delete = (id_category: number): Observable<Response|any> => {
    let promesa = new Promise((resolve, reject) => {
      this.http.delete(this.api_uri +'/'+ id_category,this.options)
        .subscribe(value => {resolve(true)},error => {this.handleError(error);reject(false)});
    });
    return from(promesa);
  };

  public postDocument = (body: DocumentDTO): Observable<Number|any> => {
    let promesa = new Promise((resolve, reject) => {
      this.http.post(this.api_uri , body, { headers: this.headers })
        .subscribe(value => {resolve(value)},error => {this.handleError(error);reject(error)});
    });
    return from(promesa);
  };



  public putMeasureUnit = (id:number,body: DocumentDTO): Observable<Boolean|any> => {
      let promesa = new Promise((resolve, reject) => {
        this.http.put(this.api_uri +"/"+ id, body, { headers: this.headers })
          .subscribe(value => {resolve(true)},error => {this.handleError(error);reject(false)});
      });
      return from(promesa);
  };



  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      // @ts-ignore
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return null;//Observable.throw(errMsg);
  }
}

