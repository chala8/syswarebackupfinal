import {Component, OnInit, ViewChild} from '@angular/core';
import { LocalStorage } from '../../../../../../../shared/localStorage';
import { DatePipe } from '@angular/common';
import { BillingService } from '../../billing.service';
import {MatDatepicker, MatDialog, MatSortable, MatTableDataSource} from '@angular/material';
import { PedidosDetailComponent } from '../pedidos-detail/pedidos-detail.component'
import { PedidosDetail2Component } from '../pedidos-detail2/pedidos-detail2.component'
import { StatechangeComponent } from '../statechange/statechange.component'
import { NotesModalComponent } from '../notes-modal/notes-modal.component';
import { NotesOnOrderComponent } from '../notes-on-order/notes-on-order.component';
import {Urlbase} from '../../../../../../../shared/urls';
import {HttpClient} from '@angular/common/http';
import {MatSort} from '@angular/material/sort';

let $: any = jQuery;
import * as jQuery from 'jquery';
import 'bootstrap-notify';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss']
})
export class PedidosComponent implements OnInit {
  id_store;

  date1='';
  date2='';

  type2 = "1";
  SelectedStore2;
  isListProdFull2=false;
  ListReportProd2;
  dateP12;
  dateP22;
  hours2=24;

  Stores = [];
  SelectedStore = this.locStorage.getIdStore();
  SelectedBillType = '87';
  SelectedBillState = '61';
  isListProdFull = false;
  ListReportProd;

  SelectedVehicle = "";
  ListVehicles;
  isPlanillaFull = false;
  ListPlanilla=[];

  CampoSorteando = -1;
  Invertido = false;

  mostrandoCargando = false;
  //expandedElement = null;
  estadoCargando = "Procesando - Iniciando";

  //variables tabla
  DiccionarioColumnas = {};
  AnchosColumnas = {};
  dataSource = new MatTableDataSourceWithCustomSort(this.ListReportProd);
  DictSelection = {};
  expandedElement: any | null;
  @ViewChild('picker2') SelectorFechaFinal_Facturacion: MatDatepicker<Date>;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  GetKeysSeleccionados(){
    return Object.keys(this.DictSelection);
  }

  CustomSelect(row){
    if(this.DictSelection[row.id_BILL] != null){
      delete this.DictSelection[row.id_BILL];
    }else{
      this.DictSelection[row.id_BILL] = row;
    }
  }

  constructor(public router: Router,private datePipe: DatePipe, public dialog: MatDialog, public locStorage: LocalStorage,private categoriesService: BillingService,private http2: HttpClient) { }

  id_menu = 221;

  ngOnInit() {
    //PROTECCION URL INICIA
    console.log(JSON.stringify(this.locStorage.getMenu()))
    const elem = this.locStorage.getMenu().find(item => item.id_menu == this.id_menu);

    if(!elem){
      this.router.navigateByUrl("/dashboard/business/movement/nopermision");
    }
    //PROTECCION URL TERMINA
    this.getVehicles();
    this.getStores();
    this.id_store = this.locStorage.getIdStore();
    console.log("THIS IS MY ID STORE: ",this.id_store)
  }

  getPlanillaList(){
    this.ListPlanilla = [];
    this.ListVehicles.forEach(element => {

      this.http2.get(Urlbase[2]+"/pedidos/planillas?idvehiculo="+element.id_VEHICULO+"&idstore="+this.locStorage.getIdStore()).subscribe(response => {

        //@ts-ignore
        response.forEach(element => {
          this.isPlanillaFull= true;
          this.ListPlanilla.push(element);
        });
      })
    });
  }

  getVehicles(){
    this.http2.get(Urlbase[2]+"/pedidos/vehiculos").subscribe(response => {
      this.ListVehicles = response;
      this.SelectedVehicle = response[0].id_VEHICULO;
    })
  }

  exportarPdf(elem){
    this.http2.get(Urlbase[2]+"/pedidos/planillaDetail?idplanilla="+elem.id_PLANILLA).subscribe(response => {
      this.http2.post(Urlbase[2]+"/pedidos/pdf",{
        logo: this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,
        master: elem,
        detalles: response
      },{responseType: 'text'}).subscribe(answer => {
        console.log("THIS IS RESPONSE: ",answer);
        console.log("EL BODY ES: ",JSON.stringify({
          logo: this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,
          master: elem,
          detalles: response
        }));
        window.open(Urlbase[10]+"planillas/"+answer, "_blank");
      })
    })
  }

  openClosePanilla(idplanilla){
    let dialogRef;
    dialogRef = this.dialog.open(NotesModalComponent,{
      height: '275px',
      width: '850px',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {

      if(result.resp){
        this.http2.put(Urlbase[2]+"/pedidos/closeplanilla?observaciones="+result.notes+"&idplanilla="+idplanilla,{}).subscribe(answer => {
          console.log("THIS IS ANSWER: ",answer);
          this.getPlanillaList()
        })
      }
    });

  }

  confirmarEnv(elem) {
    console.log("this is element dude, ", elem);
    this.http2.post(Urlbase[2]+"/store/confirmarPC?numpedido="+elem.numdocumento+"&idbilltype=46&idstore="+this.locStorage.getIdStore(),{}).subscribe(
        response => {
          this.http2.post(Urlbase[2]+"/store/confirmarPC?numpedido="+elem.numdocumento+"&idbilltype=47&idstore="+this.locStorage.getIdStore(),{}).subscribe(
              response2 => {
                const dialogRef = this.dialog.open(NotesOnOrderComponent, {
                  height: '38vh',
                  width: '80vw',
                  data: {
                    elem: elem
                  },
                  disableClose: true
                });
                dialogRef.afterClosed().subscribe(response => {

                  //  this.http2.put(Urlbase[3] + "/pedidos/billstate?billstate=81&billid="+elem.id_BILL,null).subscribe(a=> {
                  this.getRepProdList();
                });
                //  })
              })
        })
  }

  getStores() {
    this.categoriesService.getStoresByThird(this.locStorage.getToken().id_third).subscribe(data => {
      console.log(data);this.Stores = data;
      this.SelectedStore =  this.locStorage.getIdStore();})
  }

  //VARIABLES PARA LA TABLA
  GetKeys(){
    return Object.keys(this.DiccionarioColumnas);
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    return Object.keys(this.DictSelection).length === this.dataSource.data.length;
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.DictSelection[row.id_BILL] ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.DictSelection = {} :
      this.dataSource.data.forEach(row => {
        this.DictSelection[row["id_BILL"]] = row;
      });
  }

  ///////////////

  getRepProdList(){
    //DEFINO LAS COLUMNAS SEGUN LA SELECCIÓN
    this.DiccionarioColumnas = {};
    this.AnchosColumnas = {};
    if(this.SelectedBillType == '86' && this.SelectedBillState == '82'){this.DiccionarioColumnas["select"] = "";}
    this.DiccionarioColumnas["fecha"] = "Fecha";this.AnchosColumnas["fecha"] = "6vw";
    this.DiccionarioColumnas["numdocumento"] = "Documento";this.AnchosColumnas["numdocumento"] = "5vw";
    if(this.SelectedBillType == '86'){this.DiccionarioColumnas["numpedido"] = "Pedido Cliente";this.AnchosColumnas["numpedido"] = "4vw";}
    this.DiccionarioColumnas["cliente"] = "Cliente";this.AnchosColumnas["cliente"] = "7vw";
    this.DiccionarioColumnas["tienda"] = "Tienda";this.AnchosColumnas["tienda"] = "7vw";
    this.DiccionarioColumnas["address"] = "Dirección";this.AnchosColumnas["address"] = "6vw";
    this.DiccionarioColumnas["mail"] = "E-Mail";this.AnchosColumnas["mail"] = "7vw";
    this.DiccionarioColumnas["phone"] = "Teléfono";this.AnchosColumnas["phone"] = "4vw";
    this.DiccionarioColumnas["body"] = "Observaciones";this.AnchosColumnas["body"] = "5vw";
    /////////////////////////////////////////////
    this.mostrandoCargando = true;
    this.estadoCargando = "Consultando Pedidos";
    console.log(Urlbase[3]+"/pedidos/master?id_store="+this.SelectedStore+"&id_bill_state="+this.SelectedBillState+"&id_bill_type="+this.SelectedBillType+"&date1="+this.date1+"&date2="+this.date2);
    this.http2.get(Urlbase[3]+"/pedidos/master?id_store="+this.SelectedStore+"&id_bill_state="+this.SelectedBillState+"&id_bill_type="+this.SelectedBillType+"&date1="+this.date1+"&date2="+this.date2).subscribe(
      data => {
          console.log("THIS IS DATA: ",data);
          this.ListReportProd = data;
          this.isListProdFull = true;
          this.mostrandoCargando = false;
          this.estadoCargando = "";
          this.DictSelection = {};
          this.dataSource = new MatTableDataSourceWithCustomSort(this.ListReportProd);
          this.sort.sort(({ id: 'numdocumento', start: 'asc'}) as MatSortable);
          this.dataSource.sort = this.sort;
        }
    )
  }

  cleanList(){
    this.isListProdFull = false;
    this.ListReportProd = [];
    this.DictSelection = {};
    this.dataSource = new MatTableDataSourceWithCustomSort(this.ListReportProd);
    this.sort.sort(({ id: 'numdocumento', start: 'asc'}) as MatSortable);
    this.dataSource.sort = this.sort;
  }

  cleanList2(){
    this.cleanList();
    if(this.SelectedBillType=='1' || this.SelectedBillType=='2' || this.SelectedBillType=='3' || this.SelectedBillType=='4' )
    { this.SelectedBillState = '1' }
    if(this.SelectedBillType=='86')
    { this.SelectedBillState = '62' }
    if(this.SelectedBillType=='87')
    { this.SelectedBillState = '61' }
    if(this.SelectedBillType=='88')
    { this.SelectedBillState = '1' }
    if(this.SelectedBillType=='89')
    { this.SelectedBillState = '65' }
    if(this.SelectedBillType=='90')
    { this.SelectedBillState = '67' }
  }

  goToSubMenu(element){
    let dialogRef = this.dialog.open(PedidosDetailComponent,{
      height: '90vh',
      width: '90vw',
      maxWidth:'90vw',
      data: {
        elem: element
      }
    })
  }

  getPicking(){
    let Selection = Object.keys(this.DictSelection);
    console.log("Selection es");
    console.log(Selection);
    let dialogRef;
    dialogRef = this.dialog.open(PedidosDetail2Component,{
      height: '90vh',
      width: '90vw',
      maxWidth:'90vw',
      data: {
        elem: Selection
      }
    })
    // for(let i = 0; i < Selection.length; i++){
    //   if(Selection[i]){
    //     billList.push(this.ListReportProd[i].id_BILL);
    //   }
    //   if(i+1==Selection.length){
    //     console.log(billList);
    //     let dialogRef;
    //     dialogRef = this.dialog.open(PedidosDetail2Component,{
    //       height: '90vh',
    //       width: '90vw',
    //       maxWidth:'90vw',
    //       data: {
    //         elem: billList
    //       }
    //     })
    //   }
    // }
  }

  pedidoInterno(){
    console.log("is null");
    return false;
  }

  changeState(data){
    let dialogRef = this.dialog.open(StatechangeComponent,{
      height: '90vh',
      width: '90vw',
      maxWidth: '90vw',
      data: {
        elem: data
      }
    }).afterClosed().subscribe(res=>{
      this.getRepProdList()
    })
  }

  dynamicSort(property) {
    let sortOrder = 1;
    if(property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function (a,b) {
      /* next line works with strings and numbers,
       * and you may want to customize it to your needs
       */
      const result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
    }
  }

  getRepProdList2(){
    console.log(Urlbase[3] + "/reorder2?id_third="+this.locStorage.getThird().id_third+"&id_store="+this.id_store);
    this.http2.get(Urlbase[3] + "/reorder/reorder2?id_third="+this.locStorage.getThird().id_third+"&id_store="+this.id_store+"&hours="+this.hours2).subscribe(
        data => {
          console.log("THIS IS DATA: ",data);
          if(this.type2=="1"){
            //@ts-ignore
            this.ListReportProd2 = data.sort(this.dynamicSort("linea"));
          }
          if(this.type2=="2"){
            //@ts-ignore
            this.ListReportProd2 = data.sort(this.dynamicSort("categoria"));
          }
          if(this.type2=="3"){
            //@ts-ignore
            this.ListReportProd2 = data.sort(this.dynamicSort("marca"));
          }
          this.isListProdFull2 = true;
        }
    )
  }

  transformDate(date){
    return this.datePipe.transform(date, 'yyyy/MM/dd');
  }

  genPedido(){
    this.showNotification('top', 'center', 3, "<h3>El pedido se esta generando, por favor espera a la notificacion de <b>EXITO</b></h3> ", 'info');
    try{
      console.log("THIS IS JSON, ", JSON.stringify({reorder: this.ListReportProd2,idstore: this.id_store}));
      this.http2.post(Urlbase[3] + "/pedidos/detailing",{reorder: this.ListReportProd2,idstore: this.id_store},{responseType: 'text'}).subscribe(
          response => {
            this.showNotification('top', 'center', 3, "<h3>El pedido se realizo con <b>EXITO</b></h3> ", 'success')
          })

    }catch(e){
      this.showNotification('top', 'center', 3, "<h3>El pedido presento <b>PROBLEMAS</b></h3> ", 'danger')
    }
  }

  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
      type: typeStr ? typeStr : type[id_type ? id_type : 2],
      timer: 200,
      placement: {
        from: from,
        align: align
      }
    });
  }
}

export class MatTableDataSourceWithCustomSort<T> extends MatTableDataSource<T> {

  sortData: ((data: T[], sort: MatSort) => T[]) = (data: T[], sort: MatSort): T[] => {
    const active = sort.active;
    const direction = sort.direction;
    if (!active || direction == '') { return data; }

    return data.sort((a, b) => {
      const valueA = this.sortingDataAccessor(a, active) + "";
      const valueB = this.sortingDataAccessor(b, active) + "";

      let Terminado = false;
      let i = 0;
       // console.log("valueA es "+valueA);
       // console.log("valueB es "+valueB);
      let Arreglo1 = valueA.trim().toLowerCase().split(' ').join(',').split('.').join(',').split('-').join(',').split(':').join(',').split(',');
      let Arreglo2 = valueB.trim().toLowerCase().split(' ').join(',').split('.').join(',').split('-').join(',').split(':').join(',').split(',');
      // for(let n = 0;n<Arreglo1.length;n++){
      //   console.log("Arreglo1["+n+"] es "+Arreglo1[n]);
      // }
      // for(let n = 0;n<Arreglo2.length;n++){
      //   console.log("Arreglo2["+n+"] es "+Arreglo1[n]);
      // }
      let retorno = 0;
      while(!Terminado){
        if(Arreglo1.length <= i && Arreglo2.length > i){
          Terminado = true;
          retorno = -1;
          break;
        }else if(Arreglo2.length <= i && Arreglo1.length > i){
          Terminado = true;
          retorno = 1;
          break;
        }else if(Arreglo1.length <= i && Arreglo2.length <= i){
          Terminado = true;
          retorno = 0;
          break;
        }
        let Parte1 = Arreglo1[i];
        let Parte2 = Arreglo2[i];
        // console.log("Parte1 es "+Parte1);
        // console.log("Parte2 es "+Parte2);
        let A : any = parseInt(Parte1);
        let B : any = parseInt(Parte2);
        if(isNaN(A)){/*console.log("se ejecuta isNaN(A)");*/ A = Parte1;}
        if(isNaN(B)){/*console.log("se ejecuta isNaN(B)");*/ B = Parte2;}
        i++;
        if (A < B){
          retorno = -1;
          Terminado = true;
          break;
        }else if (A > B){
          retorno = 1;
          Terminado = true;
          break;
        }
      }
      return retorno * (direction == 'asc' ? 1 : -1);
    });
  }
}
