import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


/*
*    Material modules for component
*/

/*
*     others component
*/
/*
*     models for  your component
*/


/*
*     services of  your component
*/
import { ThirdService } from '../third.service';
/*
*     constant of  your component
*/


@Component({
  selector: 'app-edit-third',
  templateUrl: './edit-third.component.html',
  styleUrls: ['./edit-third.component.css']
})
export class EditThirdComponent implements OnInit {
  form: FormGroup;

  constructor(private thirdService:ThirdService,  private fb: FormBuilder  ) { }

  ngOnInit() {
  }

}
