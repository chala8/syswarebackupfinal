import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
//import 'rxjs/add/operator/filter';
/*
*    Material modules for component
*/
/*
*     others component
*/
/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { ProductThird } from '../models/productThird'
import { Category } from '../../categories/models/category';

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { ProductThirdService } from '../product-third.service';
import { CategoriesService } from '../../categories/categories.service';

/*
*     constant of  your component
*/
declare var $:any

@Component({
  selector: 'app-product-third-detail',
  templateUrl: './product-third-detail.component.html',
  styleUrls: ['./product-third-detail.component.scss']
})
export class ProductThirdDetailComponent implements OnInit {


  ID_ATTRIBUTE_LIST:number=0;
  ID_MEASURE_UNIT:number=0;
  ID_PRODUCT:number=0;

  token:Token;


  codeList:ProductThird[];
  currentCode:ProductThird;
  categoryList:Category[]
  category:Category

   //attributes
   STATE=1
   pathParam:number=0;

  constructor(private router: Router, private route:ActivatedRoute,
                    private productThirdService: ProductThirdService,
                    public categoryService: CategoriesService,
                    private fb: FormBuilder, private locStorage: LocalStorage) {

                    this.category= new Category(null,null,null,null,null)
     }

  ngOnInit() {
    let session=this.locStorage.getSession();
    if(!session){
        this.Login();
    }else{
      this.route.params
      .subscribe( params =>{
          this.pathParam=+params['id']
          // Defaults to 0 if no query param provided.
          if(this.pathParam>0){
            this.getCodeList(this.STATE,this.pathParam)
          }else{
            this.showNotification('top','right',3,"Indicador de código invalido!");
            this.goBack();
          }



      });

      this.token=this.locStorage.getToken();

      this.route.queryParams
          .subscribe(params => {
            // Defaults to 0 if no query param provided.


        });

        if(this.pathParam<=0){
          this.showNotification('top','right',3,"Indicador de código invalido!");
          this.goBack();
        }


    }
}

getCodeList(state?: number,id_code?:number,code?:string,
id_product?: number, img_url?:string, id_measure_unit?: number,
id_attribute_list?:number, id_state?:number){

    this.productThirdService.getProductThirdList(state,id_code)
    .subscribe((data: ProductThird[]) => this.codeList = data,
    error => console.log(error),
    () => {

      if(this.codeList.length>0){

        this.currentCode=this.codeList[0];
        this.currentCode.code=this.codeList[0].code
        this.getCategory(state,this.currentCode.product.id_category)



        console.log(this.currentCode)

      }else{
        this.showNotification('top','right',3,"Inconsistencias con el !");
        this.goBack();

      }


    });

  }

  detailProduct(id_attribute_list:number){
    console.log("Detalle Attr ",id_attribute_list)
    this.router.navigate(['/dashboard/business/attribute/detail',id_attribute_list] );

  }

  Login() {
    let link = ['/auth'];
    this.router.navigate(link);
  }

  getCategory(state?:number,id_category?:number,id_category_father?:number,id_third?:number){

        this.categoryService.getCategoryList(state)
        .subscribe((data: Category[]) => this.categoryList = data,
        error => console.log(error),
        () => {

          if(this.categoryList.length>0){

            this.category=this.categoryList[0]
          }

        });

      }



  goBack() {
    let link = ['/dashboard/business/product-third'];
    this.router.navigate(link);
  }


  showNotification(from, align,id_type?, msn?){
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: msn?msn:"<b>Noficación automatica </b>"

    },{
        type: type[id_type?id_type:2],
        timer: 200,
        placement: {
            from: from,
            align: align
        }
    });
  }

}
