import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ClientData } from '../../models/clientData';
import { MatDialog } from '@angular/material';
import { GenerateThirdComponent2Component } from '../generate-third-component2/generate-third-component2.component';
import { ThirdselectComponent } from '../../thirdselect/thirdselect.component';
import { BillingService } from '../../billing.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LocalStorage} from '../../../../../../../shared/localStorage';
let $: any = jQuery;
import * as jQuery from 'jquery';
import 'bootstrap-notify';
import {Urlbase} from '../../../../../../../shared/urls';
import { Router } from '@angular/router';
@Component({
  selector: 'app-thirds',
  templateUrl: './thirds.component.html',
  styleUrls: ['./thirds.component.scss']
})
export class ThirdsComponent implements OnInit {

  constructor(public router: Router,private httpClient: HttpClient,private http2: HttpClient, private locStorage: LocalStorage, public dialog: MatDialog,private billingService : BillingService) {

    this.headers = new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization':  this.locStorage.getTokenValue(),
    });
   }
   @ViewChild('nameit') private elementRef: ElementRef;
   usuario="";
  password="";
  SelectedRol = "8888";
  document = "";
  private readonly headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  address = "";
  city = "";
  doc = "";
  docType = "";
  fullname = "";
  phones = [];
  mails = [];
  cliente="";
  id_person=0;
  ccClient = "";
  id_directory=0;
 clientData = new ClientData(true, 'Cliente Ocacional', '--', '000', 'N/A', '000', 'N/A', null);
 @ViewChild('nameot') private elementRef2: ElementRef;



  searchClient2(){
    this.id_person=0;

    console.log("THIS ARE HEADERS",this.headers);
    const identificacionCliente = this.ccClient;
    let aux;
    this.httpClient.get<any[]>(Urlbase[1] + '/persons/search?doc_person='+String(identificacionCliente),{ headers: this.headers }).subscribe(data =>{
      console.log(data);
      if (data.length == 0){
        this.openDialogClient2();
        // this.searchClient(event);
      }else{
        const dialogRef = this.dialog.open(ThirdselectComponent, {
          width: '60vw',
          height: '80vh',

          data: { thirdList: data }
        });

        dialogRef.afterClosed().subscribe(result => {
          if(result){

            aux = this.locStorage.getPersonClient();
            console.log("THIS THE AUX I NEED:", aux);
            this.cliente = aux.fullname;
            this.clientData.is_natural_person = true;
            this.clientData.fullname = aux.fullname;
            this.clientData.document_type = aux.document_TYPE;
            this.clientData.document_number = aux.document_NUMBER;
            this.clientData.id_third = aux.id_PERSON;
            this.clientData.address = aux.address;
            this.clientData.email = aux.city;
            this.clientData.phone = aux.phone;
            this.id_directory = aux.id_DIRECTORY;
            console.log("THIS IS THE CLIENT",this.clientData);
            this.httpClient.get(Urlbase[1] + "/persons/idperson?id_third="+aux.id_PERSON,{headers: this.headers}).subscribe(idperson => {
              //@ts-ignore
              this.id_person = Number(idperson);
              console.log(this.id_person);
            });
            setTimeout(() => {

              this.elementRef2.nativeElement.focus();
              this.elementRef2.nativeElement.select();

              }, 100);

          }
        });


      }


    });


  }


  asociarCajaTienda(){
    this.http2.post(Urlbase[2] + "/resource/asociarAPerson?idperson="+this.id_person+"&idstore="+this.SelectedStore+"&idcaja="+this.SelectedBox,{}).subscribe(resp => {
      if(resp == 1){
        this.showNotification('top', 'center', 3, "<h3>SE ASOCIO LA CAJA Y LA TIENDA EXITOSAMENTE</h3> ", 'info');
      }else{
        this.showNotification('top', 'center', 3, "<h3>NO SE REALIZO LA ASOCIACON ADECUADAMENTE</h3> ", 'info');
      }
    })
  }


  update(){
    console.log(Urlbase[1] + "/directories/phoneAndMail?id_directory="+this.id_directory+"&phone="+this.clientData.phone+"&mail="+this.clientData.email+"&address="+this.clientData.address);
    this.httpClient.put(Urlbase[1] + "/directories/phoneAndMail?id_directory="+this.id_directory+"&phone="+this.clientData.phone+"&mail="+this.clientData.email+"&address="+this.clientData.address.split("#").join("_"), {} ,{ headers: this.headers }).subscribe(resp => {
      console.log("THIS IS MY RTESP: ",resp);
      this.clientData = new ClientData(true, 'Cliente Ocacional', '--', '000', 'N/A', '000', 'N/A', null);
      this.ccClient= "";
      this.cliente = "";
    })
  }


  searchClient(event){
    this.id_person=0;
    const identificacionCliente = String(event.target.value);
    let aux;
    this.httpClient.get<any[]>(Urlbase[1] + '/persons/search?doc_person='+String(identificacionCliente),{ headers: this.headers }).subscribe(data =>{
      console.log(data);
      if (data.length == 0){
        this.openDialogClient2();
        // this.searchClient(event);
      }else{
        const dialogRef = this.dialog.open(ThirdselectComponent, {
          width: '60vw',
          height: '80vh',

          data: { thirdList: data }
        });

        dialogRef.afterClosed().subscribe(result => {
          if(result){

            aux = this.locStorage.getPersonClient();
            console.log("THIS THE AUX I NEED:", aux);
            this.cliente = aux.fullname;
            this.clientData.is_natural_person = true;
            this.clientData.fullname = aux.fullname;
            this.clientData.document_type = aux.document_TYPE;
            this.clientData.document_number = aux.document_NUMBER;
            this.clientData.id_third = aux.id_PERSON;
            this.clientData.address = aux.address;
            this.clientData.email = aux.city;
            this.clientData.phone = aux.phone;
            this.id_directory = aux.id_DIRECTORY;
            console.log("THIS IS THE CLIENT",this.clientData);
            this.httpClient.get(Urlbase[1] + "/persons/idperson?id_third="+aux.id_PERSON,{headers: this.headers}).subscribe(idperson => {
              //@ts-ignore
              this.id_person = Number(idperson);
              console.log(this.id_person);
            })

          }
          setTimeout(() => {

            this.elementRef.nativeElement.focus();
            this.elementRef.nativeElement.select();

            }, 100);
        });
      }


    });
  }

  crearUsuario(){

     //@ts-ignore
     this.httpClient.post(Urlbase[1] + "/employees?id_person="+this.id_person,{"salary":1100000,
     "state":{"state": 1,
              "creation_date":new Date(),
              "modify_date": new Date()}},
        {headers: this.headers}).subscribe(idEmployee => {


        //POST A USER_THIRD
        //@ts-ignore
        this.httpClient.post(Urlbase[1] + "/employees/userThird?id_person="+this.id_person,{},
        {headers: this.headers}).subscribe(postedUt => {

          //POST A USUARIOS
          //@ts-ignore
          this.httpClient.post(Urlbase[1] + "/employees/user?id_person="+this.id_person+"&usuario="+this.usuario+"&clave="+this.password,{},
          {headers: this.headers}).subscribe(postedU => {

            //POST A USUARIOS_ROLES
            //@ts-ignore
            this.httpClient.post(Urlbase[1] + "/employees/rol?id_person="+this.id_person+"&id_rol="+this.SelectedRol,{},
            {headers: this.headers}).subscribe(postedRol => {

            //POST A STORE_EMPLEADO

            //POST A CAJA_PERSON
            this.showNotification('top', 'center', 3, "<h3>SE REGISTRO EL USUARIO EXITOSAMENTE</h3> ", 'info');
            })
          })
        })
      })


  }




  openDialogClient2(): void {



    const dialogRef = this.dialog.open(GenerateThirdComponent2Component, {
      width: '60vw',
      data: {}
    });


dialogRef.afterClosed().subscribe(result => {
  if (result) {
    // console.log('CREATE CLIENT SUCCESS');
    // console.log(result);s
    let isNaturalPerson= result.data.hasOwnProperty('profile');
    let dataPerson= isNaturalPerson?result.data.profile:result.data;
    this.clientData.is_natural_person = isNaturalPerson;
    this.clientData.fullname= dataPerson.info.fullname;
    this.clientData.document_type = dataPerson.info.id_document_type;
    this.clientData.document_number = dataPerson.info.document_number;
    this.clientData.address = dataPerson.directory.address;
    this.clientData.phone = dataPerson.directory.phones[0].phone;
    this.clientData.email = dataPerson.directory.hasOwnProperty('mails')?dataPerson.directory.mails[0].mail:'N/A';
    setTimeout(() => {

      this.elementRef2.nativeElement.focus();
      this.elementRef2.nativeElement.select();

      }, 100);
  }
  setTimeout(() => {

    this.elementRef2.nativeElement.focus();
    this.elementRef2.nativeElement.select();

    }, 100);
});
}





Stores;
SelectedStore;
id_menu = 161;

ngOnInit() {
  //PROTECCION URL INICIA
  console.log(JSON.stringify(this.locStorage.getMenu()))
  const elem = this.locStorage.getMenu().find(item => item.id_menu == this.id_menu);

  if(!elem){
    this.router.navigateByUrl("/dashboard/business/movement/nopermision");
  }
  //PROTECCION URL TERMINA

    this.billingService.getStoresByThird(this.locStorage.getToken().id_third).subscribe(data => {
      this.Stores = data;
     this.SelectedStore = data[0].id_STORE;
     this.getBoxes()
    })

  }

  boxes;
  SelectedBox;

  getBoxes(){
    this.http2.get(Urlbase[2] + "/resource/boxes?id_store="+this.SelectedStore).subscribe(response => {
     this.SelectedBox = response[0].id_CAJA;
     this.boxes = response;
    });
  }

  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: typeStr ? typeStr : type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }


  buscarTercero(){
    let aux;
    this.httpClient.get<any[]>(Urlbase[1] + '/persons/search?doc_person='+this.document,{ headers: this.headers }).subscribe(data =>{
    this.phones = [];
    this.mails = [];
    if (data.length == 0){
        this.showNotification('top', 'center', 3, "<h3>NO SE ENCONTRO EL DOCUMENTO DE FACTURA SOLICITADO</h3> ", 'danger');
        // this.searchClient(event);
      }else{
        aux = data[0];
        this.httpClient.get<any>(Urlbase[1] + "/directories/generalDir?id_directory="+aux.id_DIRECTORY,{ headers: this.headers }).subscribe(response =>{
          this.httpClient.get<any>(Urlbase[1] + "/directories/phone?id_directory="+aux.id_DIRECTORY,{ headers: this.headers }).subscribe(responsePhone =>{
            this.httpClient.get<any>(Urlbase[1] + "/directories/mail?id_directory="+aux.id_DIRECTORY,{ headers: this.headers }).subscribe(responseMail=>{
              this.address = aux.address;
              this.city = response.city;
              this.doc = aux.document_NUMBER;
              this.docType = aux.document_TYPE;
              this.fullname = aux.fullname;
              responsePhone.forEach(element => {
                this.phones.push(element)
              });
              responseMail.forEach(element => {
                this.mails.push(element)
              });

              console.log(this.address,this.city,this.doc,this.docType,this.fullname,this.phones,this.mails)
            })
          })
        })
      }


    });
  }

}
