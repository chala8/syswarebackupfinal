import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {Observable,of} from 'rxjs';
//import 'rxjs/add/observable/of';
import * as _ from 'lodash';


/*
*    Material modules for component
*/

/*
*     others component
*/

/*
*     models for  your component
*/
import { Inventory } from '../models/inventory'
import { FilterInventory } from '../models/filters'

import { DocumentType } from '../../../thirds724/document-type/models/document-type'
import { Third } from '../../../thirds724/third/models/third'
import { Category } from '../../categories/models/category'
import { InventoryParameters } from '../models/inventaryParameter';


import { MeasureUnit } from '../../measure-unit/models/measureUnit'
import { AttributeComplete } from '../../attributes/models/attributeComplete'

import { Attribute } from '../../attributes/models/attribute'
import { AttributeValue } from '../../attributes/models/attributeValue'
import { Token } from '../../../../../../shared/token';

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage';
import { InventoriesService } from '../inventories.service'
import { DocumentTypeService } from '../../../thirds724/document-type/document-type.service'
import { ThirdService } from '../../../thirds724/third/third.service'
import { CategoriesService } from '../../categories/categories.service'
import { MeasureUnitService } from '../../measure-unit/measure-unit.service'
import { AttributeService } from '../../attributes/attribute.service'

/*
*     constant of  your component
*/



@Component({
  selector: 'app-inventory-filter',
  templateUrl: './inventory-filter.component.html',
  styleUrls: ['./inventory-filter.component.scss']
})
export class InventoryFilterComponent implements OnInit {

    form: FormGroup;

    // models
    filterInventory:FilterInventory;
    token:Token;
    documentTypes: DocumentType[];
    inventories: Inventory[];
    inventoryParameters:InventoryParameters;
    response:any[];

    thirdListList:Third[];
    thirdStores:Third[];
    thirdStoreVenue:Third[];

    categoryList:Category[];
    productorList:Category[];
    categoryProductorList:Category[];
    MyCategoryList:Category[];

    measureUnitList:MeasureUnit[];
    attributeComplete:AttributeComplete[]=[]
    attributeList:Attribute[];
    attributeValueList:AttributeValue[];
    selectValues:AttributeValue[]
    thirdFathet:Third;


    // we use the decorator as Output
    @Output() EmitInventoryFilterType = new EventEmitter();

    // Here definition of attribute for filters
    CURRENT_ID_THIRD=0;
    ID_THIRD_FATHER=0;
    public name: string="Hello World!";
    STATE = 1
    public document_type:number;
    public document_number:String;
    // activated Filter by Third If the user is not have ID_THIRD
    isThird=true;

    // activated Venue If the ID_THIRD's user  have venues
    isVenueThird=true;
    isseenAttribute:boolean=false;



  constructor(  public locStorage: LocalStorage,private _router: Router,
                public inventoriesService: InventoriesService,
                public docTypeService:DocumentTypeService,
                public thirdService:ThirdService,
                public categoriesService:CategoriesService,
                public MeasureUnitService:MeasureUnitService,
                public attributeService:AttributeService,
                private fb: FormBuilder ) {
                  this.response=[]
                  this.inventoryParameters= new InventoryParameters(this.response);
                  this.thirdStoreVenue=[]
                  this.filterInventory= new FilterInventory();
                  this.createControls();
                  this.logNameChange();
                }

    ngOnInit() {


        let session = this.locStorage.getSession();
        if (!session) {
          this.Login();
        } else {
          this.thirdFathet=this.locStorage.getThird()

          this.token = this.locStorage.getToken();
          this.CURRENT_ID_THIRD = this.token.id_third? this.token.id_third:null;
          this.ID_THIRD_FATHER = this.token.id_third_father?this.token.id_third_father:null;


          if(this.ID_THIRD_FATHER>0){
            this.isThird=false;
          }
          this.loadInventoryParameter(null,this.ID_THIRD_FATHER);
          this.getDocumentType()
         // this.getMeasureUnitList(this.STATE)
          this.getAttributes(this.STATE)
          this.getThird(true,this.STATE,null,this.CURRENT_ID_THIRD)
          this.getThird(true,this.STATE,null,this.ID_THIRD_FATHER)

        }
    }

    Login() {
      let link = ['/auth'];
      this._router.navigate(link);
    }

    /**
     * Emitte data from this to other component
     * @param event
     */
    responseFilter(event){

      this.filterInventory.attributeValues=this.selectValues;

       if(this.form.value['is_catal_general']){
         this.filterInventory.id_category_th=null;

         if(this.form.value['is_id_productor']){

            this.filterInventory.id_productor=null;


         }else{
           this.filterInventory.id_productor=this.form.value['id_productor']>0?this.form.value['id_productor']:null;
           this.filterInventory.id_cat_productor=this.form.value['id_cat_prod']>0?this.form.value['id_cat_prod']:null;
         }




       }

       if(this.form.value['is_catal_propio']){

         this.filterInventory.id_productor=null;
          this.filterInventory.id_cat_productor=null;

         if (this.form.value['is_id_category_th']) {
            this.filterInventory.id_category_th=null;
         }else{
           this.filterInventory.id_category_th=this.form.value['is_id_category_th']>0?this.form.value['is_id_category_th']:null;
         }

       }

      //

      //
      if(this.form.value['is_venue']){
        this.filterInventory.id_venue=this.form.value['id_th_venue']?this.form.value['id_th_venue']:null;
      }else{
        this.filterInventory.id_venue=this.ID_THIRD_FATHER?this.ID_THIRD_FATHER:this.CURRENT_ID_THIRD;
      }

      if(!this.form.value['is_id_category_th']){
        this.filterInventory.id_category_th=this.form.value['id_category_th']?this.form.value['id_category_th']:-1;
      }else{
        this.filterInventory.id_category_th=null;
      }

      // if(!this.form.value['is_productor_cat']){
      //   this.filterInventory.id_category_th=this.form.value['id_category_th']?this.form.value['id_category_th']:-1;
      // }else{
      //   this.filterInventory.id_category_th=null;
      // }

      // is_productor_cat

      if(!this.form.value['is_measure_unit']){
        this.filterInventory.id_measure_unit=this.form.value['measure_unit']?this.form.value['measure_unit']:null;
      }else{
        this.filterInventory.id_measure_unit=null;

      }


      this.filterInventory.id_document_type_th=this.form.value['document_type_th']?this.form.value['document_type_th']:null;
      this.filterInventory.number_document_type_th=this.form.value['document_number_th']?this.form.value['document_number_th']:null;
      this.filterInventory.attributeValues=this.selectValues

        this.EmitInventoryFilterType.emit(this.filterInventory);

    }

    loadInventoryParameter(id_inventory?:number,
      id_third?:number){
        this.inventoriesService.getInventoriesParamters(id_inventory,id_third)


              .subscribe((data: InventoryParameters) => this.inventoryParameters = data,
              error => console.log(error),
              () => {
                console.log("PARAMETERS -> ", this.inventoryParameters)
                this.productorList=this.inventoryParameters['productors']
                console.log("PARAMETERS PRODUCTORES-> ", this.productorList)
                this.measureUnitList=this.inventoryParameters['units']
                console.log("PARAMETERS UNIT-> ", this.measureUnitList)
                this.attributeComplete=this.inventoryParameters['attributes'];
                console.log("PARAMETERS ATTR-> ", this.attributeComplete)
                this.getCategory(this.STATE, null, null)

        });


      }

      seenAttribute(){
        this.isseenAttribute=!this.isseenAttribute;
        this.selectValues=[];
      }
    addValue(id_attribute?:any,value?:AttributeValue){
        if(!id_attribute){
          if(this.selectValues.length>0){

                  let exit=this.selectValues.find(val=>
                    (val.id_attribute===value.id_attribute)&&(val.id_attribute_value===value.id_attribute_value)
                  )
                  if(!exit){

                    this.selectValues=this.selectValues.filter(val=>
                      (val.id_attribute!=value.id_attribute)
                    )

                    console.log("VALUE ", this.selectValues)

                    this.selectValues.push(value)


                  }

                }else{
                  this.selectValues.push(value)

                }

                console.log("- LIS- ", this.selectValues);
        }else{

          this.selectValues=this.selectValues.filter(val=>
            (val.id_attribute!=(+id_attribute))
          )
          console.log("VALUE ", this.selectValues)




        }

        this.filterInventory.attributeValues=this.selectValues;
        //this.EmitInventoryFilterType.emit(this.filterInventory);
        this.responseFilter(this.filterInventory)

      }


  createControls() {
    this.form = this.fb.group({
      is_name_third: [false, Validators.compose([

      ])],
      name_third: ['', Validators.compose([
      ])],
      document_type_th: ['', Validators.compose([

      ])],
      document_number_th: ['', Validators.compose([

      ])],

      is_name_third_venue: [false, Validators.compose([

      ])],




      name_third_venue: ['', Validators.compose([
      ])],
      document_type_th_venue: ['', Validators.compose([

      ])],
      document_number_th_venue: ['', Validators.compose([

      ])],
      is_venue: [false, Validators.compose([

      ])],

      id_th_venue: ['', Validators.compose([

      ])],

      is_cat_prod: [true, Validators.compose([

      ])],

      //cad productors
      is_catal_general: [true, Validators.compose([

      ])],
      id_productor: ['', Validators.compose([
      ])],
      is_id_productor: [false, Validators.compose([

      ])],


      id_cat_prod: ['', Validators.compose([

      ])],


      // card my category
      is_catal_propio: [false, Validators.compose([

      ])],
      is_id_category_th: [false, Validators.compose([

      ])],

      is_productor_cat: [true, Validators.compose([

      ])],

      id_category_th: ['', Validators.compose([
      ])],



      measure_unit: ['', Validators.compose([

      ])],
      is_measure_unit: [false, Validators.compose([

       ])],
       is_attribute: [false, Validators.compose([

       ])],

      attribute_value: ['', Validators.compose([

      ])],







    });
  }

  logNameChange() {

      const is_measure_unitControl = this.form.get('is_measure_unit');

        is_measure_unitControl.valueChanges.forEach((value: string) =>{

            let is_measure_unit:boolean =is_measure_unitControl['_value'];

            if (is_measure_unit){ // IF Active all
              this.responseFilter(this.filterInventory)
            }
          }
      );
      const measure_unitControl = this.form.get('measure_unit');

        measure_unitControl.valueChanges.forEach((value: string) =>{

            let measure_unit =measure_unitControl['_value'];
            this.responseFilter(this.filterInventory)

          }
      );
      const is_id_category_thControl = this.form.get('is_id_category_th');

        is_id_category_thControl.valueChanges.forEach((value: string) =>{

            let is_id_category_th:boolean =is_id_category_thControl['_value'];

            if (is_id_category_th){ // IF Active all
              this.responseFilter(this.filterInventory)
            }
          }
      );

     const id_category_thControl = this.form.get('id_category_th');

      id_category_thControl.valueChanges.forEach((value: string) =>{

          let measure_unit =id_category_thControl['_value'];
          this.responseFilter(this.filterInventory)

        }
    );

      const is_id_productorControl = this.form.get('is_id_category_th');

        is_id_productorControl.valueChanges.forEach((value: string) =>{

            let is_id_category_th:boolean =is_id_productorControl['_value'];

            if (is_id_category_th){ // IF Active all
              this.responseFilter(this.filterInventory)
            }
          }
      );
    const id_productorControl = this.form.get('id_productor');

      id_productorControl.valueChanges.forEach((value: string) =>{
          let id_cat_prod =id_productorControl['_value'];
          this.responseFilter(this.filterInventory)

        }
    );
     const id_cat_prodControl = this.form.get('id_cat_prod');

      id_cat_prodControl.valueChanges.forEach((value: string) =>{

          let id_cat_prod =id_cat_prodControl['_value'];
          this.responseFilter(this.filterInventory)

        }
    );

      ///THIRDSSS


       const is_venueControl = this.form.get('is_venue');

        is_venueControl.valueChanges.forEach((value: string) =>{

            let is_venue:boolean =is_venueControl['_value'];

            if (is_venue==false){ // IF is desactive
              this.form.patchValue({
                'id_th_venue': ''
                });
              this.responseFilter(this.filterInventory)
            }
          }
      );
      const id_th_venueControl = this.form.get('id_th_venue');

        id_th_venueControl.valueChanges.forEach((value: string) =>{

            let id_th_venue:boolean =id_th_venueControl['_value'];


              this.responseFilter(this.filterInventory)

          }
      );

        const is_catal_generalControl = this.form.get('is_catal_general');

        is_catal_generalControl.valueChanges.forEach((value: string) =>{

            let is_id_category_th:boolean =is_catal_generalControl['_value'];

            if (is_id_category_th){ // IF Active all
              this.form.patchValue({
                'is_catal_propio': false,
                'id_category_th':''
                });

              this.responseFilter(this.filterInventory)
            }
          }
      );
        const is_catal_propioControl = this.form.get('is_catal_propio');

        is_catal_propioControl.valueChanges.forEach((value: string) =>{

            let is_id_category_th:boolean =is_catal_propioControl['_value'];

            if (is_id_category_th){ // IF Active all
              this.form.patchValue({
                'is_catal_general': false,
                'id_cat_prod':'',
                'id_productor':'',
                'is_id_productor':false
                });
              this.responseFilter(this.filterInventory)
            }
          }
      );


  }


  // Get Services
   // GET /documents-types
   getDocumentType(): void {

        this.docTypeService.getDocumentTypeList()
          .subscribe((data: DocumentType[]) => this.documentTypes = data,
          error => console.log(error),
          () => {

          });
      }
    getCategory(state?:number,id_category?:number,id_category_father?:number,id_third?:number){
          this.categoryProductorList=[];
          this.categoriesService.getCategoryList(state)
          .subscribe((data: Category[]) => this.categoryList = data,
          error => console.log(error),
          () => {
            if(this.categoryList.length>0){
              // prodcutos
              //this.productorList = _.filter(this.categoryList, function (element) {
              //      return (element.id_category_father == null|| element.id_category_father < 1) && (element.id_third == null || element.id_third < 1);
              //});

              for(let prod of this.productorList){
                for(let element of this.categoryList){
                  if(prod.id_category===element.id_category_father){
                    this.categoryProductorList.push(element)
                  }

                }
              }

              this.MyCategoryList = _.filter(this.categoryList, function (element) {
                return  (element.id_third !== null && element.id_third > 0);
          });
            }

          });
    }

    getMeasureUnitList(state?:number,id_category?:number,id_category_father?:number,id_third?:number){

          this.MeasureUnitService.getCategoryList(state)
          .subscribe((data: MeasureUnit[]) => this.measureUnitList = data,
          error => console.log(error),
          () => {
          });

        }

      getAttributes(state?:number,id_category?:number,id_category_father?:number,id_third?:number){

          this.attributeService.getAttribute()
          .subscribe((data: Attribute[]) => this.attributeList = data,
          error => console.log(error),
          () => {

            if(this.attributeList.length>0){
              //this.getAttributeValues()

            }
          });
        }
      getAttributeValues(state?: number,id_attribute_value?:number,id_attribute?:number){
        this.attributeService.getAttributeValue(state,id_attribute_value,id_attribute,)
        .subscribe((data: AttributeValue[]) => this.attributeValueList = data,
        error => console.log(error),
        () => {
          if(this.attributeValueList.length>0 ){

          }
        });
    }


  // GET /Thirds
  getThird(is_venue:boolean,state?:number,id_third?:number,id_third_father?:number,document_type?:number,document_number?:string,id_doctype_person?:number,doc_person?:string,id_third_type?:number,id_person?:number): void {

        this.thirdService.getThirdList(id_third,id_third_father,document_type,document_number,
          id_doctype_person,doc_person,id_third_type,state,id_person)
          .subscribe((data: Third[]) => this.thirdListList = data,
          error => console.log(error),
          () => {
           this.prosessingDataThird(is_venue);
          });
      }

      prosessingDataThird(is_venue:boolean){

        if(is_venue){


          for(let element of this.thirdListList){


            if((element.profile===null ||  element.profile===0) && (element.id_third_father===this.CURRENT_ID_THIRD || element.id_third_father===this.ID_THIRD_FATHER)){
              if(this.thirdStoreVenue.length>0){
                for(let objet of this.thirdStoreVenue){

                  if(element.id_third !==objet.id_third){

                    this.thirdStoreVenue.push(element);
                  }
                }
            }else{
              this.thirdStoreVenue.push(element);
            }
          }
          }
          if(this.thirdStoreVenue.length>0){
            this.isVenueThird=true;
          }else{
            this.isVenueThird=false;
          }
      }else{

      }
    }

}
