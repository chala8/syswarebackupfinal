import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import {Observable,of} from 'rxjs';
//import 'rxjs/add/observable/of';
import * as _ from 'lodash';



import {MatTabChangeEvent, VERSION} from '@angular/material';


import { LocalStorage } from '../../../../../../shared/localStorage';
import { Token } from '../../../../../../shared/token';

import { ProductThirdService } from '../product-third.service';
import { ProductThird } from '../models/productThird';


var categoryList:ProductThird[];
var categoryListGlobal:ProductThird[];
@Component({
  selector: 'app-product-third-list',
  templateUrl: './product-third-list.component.html',
  styleUrls: ['./product-third-list.component.scss']
})
export class ProductThirdListComponent implements OnInit {

  isMyProduct=true;
  token:Token;
  ID_THIRD_TYPE: number;
  STATE=1
  CURRENT_ID_THIRD = 0;

  thirdAux:ProductThird[];






    displayedColumns = ['position', 'name', 'weight', 'symbol', 'direccion', 'opciones'];
    displayedColumns_General = ['position', 'name', 'weight', 'symbol', 'direccion'];
    dataSource:ProductDataSource


  constructor(public locStorage: LocalStorage,
    private _router: Router,
    public productService: ProductThirdService) { }

  ngOnInit() {
    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD=this.token.id_third;
      this.ID_THIRD_TYPE = 23;

      if (this.CURRENT_ID_THIRD!==null && this.CURRENT_ID_THIRD>0){

        this.getProductList(this.STATE)


      }


    }
  }


  getProductList(state?:number,id_category?:number,id_category_father?:number,id_third?:number){

        this.productService.getProductThirdList(state)
        .subscribe((data: ProductThird[]) => categoryList = data,
        error => console.log(error),
        () => {

          this.dataSource = new ProductDataSource();
        });

      }

      Login() {
        let link = ['/auth'];
        this._router.navigate(link);
      }

      editProduct(product:ProductThird){

        this._router.navigate(['/dashboard/business/product-third/edit',product.description.id_product_third] );

      }

      detailProduct(product:ProductThird){
        this._router.navigate(['/dashboard/business/product-third/detail',product.description.id_product_third] );



      }



      addProduct(product:ProductThird){

        this._router.navigate(['/dashboard/business/product-third/new'],{queryParams:{father:product.description.id_product_third}} );

      }

      deleteCategory(id_product_third) {

            this.productService.Delete(id_product_third)
              .subscribe(
              result => {

                if (result === true) {
                  categoryList = _.filter(categoryList, function (f) { return f.description.id_product_third !== id_product_third; });
                  this.dataSource = new ProductDataSource();

                  alert("Eliminado correctamente");

                  return;
                } else {
                  //this.openDialog();
                  return;
                }
              })

          }
}

export class ProductDataSource extends DataSource<any> {
  /** Connect function called by the table to retrieve one stream containing the data to render. */

  connect(): Observable<ProductThird[]> {

    return of(categoryList);
  }

  disconnect() {}
}

