import { Injectable } from '@angular/core';
//import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';
import {Observable,of} from 'rxjs';

/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';

/** Import Model Data */
import { CommonState } from '../commons/commonState'
import { WayToPay } from './models/wayToPay'
/** Import Model Data Transfer Object */
import { CommonStateDTO } from '../commons/commonStateDTO'
import { WayToPayDTO } from './models/wayToPayDTO'
@Injectable()
export class WayToPayService {
  api_uri = Urlbase[3] + '/way-pay';
  private options;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private locStorage: LocalStorage) {


    this.headers = new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization':  this.locStorage.getTokenValue(),
    });

    let token = localStorage.getItem('currentUser');

    this.options = { headers: this.headers };
  }


}
