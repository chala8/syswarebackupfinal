import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
//import 'rxjs/add/operator/filter';
/*
*    Material modules for component
*/
/*
*     others component
*/
/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { MeasureUnitDTO } from '../models/measureUnitDTO'
import { MeasureUnit } from '../models/measureUnit'
import { CommonStateStoreDTO } from '../../commons/CommonStateStoreDTO'
import { CommonStoreDTO } from '../../commons/CommonStoreDTO'

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { MeasureUnitService } from '../measure-unit.service';
/*
*     constant of  your component
*/
let $: any = jQuery;
import * as jQuery from 'jquery';
import 'bootstrap-notify';

@Component({
  selector: 'app-measure-unit-edit',
  templateUrl: './measure-unit-edit.component.html',
  styleUrls: ['./measure-unit-edit.component.scss']
})
export class MeasureUnitEditComponent implements OnInit {

  form: FormGroup;
  token:Token;
  id_measureUnit:number=0;
  newMeasureUnit:MeasureUnitDTO;
  currentMeasureUnit:MeasureUnit;
  measureUnitList:MeasureUnit[];

  commonStateStoreDTO:CommonStateStoreDTO;

  common:CommonStoreDTO;
  STATE=1




   //attributes
   CURRENT_FATHER = 0;
   CURRENT_ID_CATEGORY = 0;

  constructor(private router: Router, private route:ActivatedRoute ,private measureUnitService: MeasureUnitService,
              private fb: FormBuilder, private locStorage: LocalStorage) {

                this.common=new CommonStoreDTO(null,null)
                this.commonStateStoreDTO= new CommonStateStoreDTO(1,null,null)
                this.newMeasureUnit=new MeasureUnitDTO(null,this.common,this.commonStateStoreDTO)
              }

  ngOnInit() {
    this.createControls();

    let session=this.locStorage.getSession();
    if(!session){
        this.Login();
    }else{
      this.token=this.locStorage.getToken();
      this.route.params
          .subscribe( params =>{
              this.id_measureUnit=+params['id']


            // Defaults to 0 if no query param provided.


              this.CURRENT_FATHER=params.father;

            this.getMeasureUnitList(this.STATE,this.id_measureUnit)


          });

      this.route.queryParams
          .subscribe(params => {

        });
    }
  }

  getMeasureUnitList(state?:number,id_measure_unit?:number,id_category_father?:number,id_third?:number){

        this.measureUnitService.getCategoryList(state,id_measure_unit)
        .subscribe((data: MeasureUnit[]) => this.measureUnitList = data,
        error => console.log(error),
        () => {
          if(this.measureUnitList.length>0){
              this.currentMeasureUnit=this.measureUnitList[0];
              this.loadData();
          }else{
            alert("No se encuetra elemento para editar")
            this.goBack();
          }
        });
      }

  Login() {
    let link = ['/auth'];
    this.router.navigate(link);
  }


  createControls() {
    this.form = this.fb.group({
      name: ['', Validators.compose([
        Validators.required
      ])],
      description: ['', Validators.compose([

      ])]
    });
  }


  loadData() {
    this.form.patchValue({
      name: this.currentMeasureUnit.name_measure_unit,
      description:this.currentMeasureUnit.description_measure_unit
    });

  }


  createNewCategory() {

    this.common.name= this.form.value["name"];
    this.common.description= this.form.value["description"];
    this.newMeasureUnit.common=this.common;
    this.commonStateStoreDTO.state=this.currentMeasureUnit.state_measure_unit
    this.newMeasureUnit.id_measure_unit_father=this.currentMeasureUnit.id_measure_unit_father;
    if(this.common.name===this.currentMeasureUnit.name_measure_unit &&
      this.common.description===this.currentMeasureUnit.description_measure_unit){


    }
    this.measureUnitService.putMeasureUnit(this.id_measureUnit,this.newMeasureUnit)
    .subscribe(
      result => {
        if (result === true) {
           this.showNotification('top','right',2,"Cambios guardados correctamente");
           this.resetForm();
           this.goBack();
          return;
        } else {
          this.showNotification('top','right',3,"Problemas al actualizar");
          //this.openDialog();
          return;
        }
      })
  }

  resetForm() {

    this.form.reset();

  }

  goBack() {
    let link = ['/dashboard/business/measure-unit'];
    this.router.navigate(link);
  }


  showNotification(from, align,id_type?, msn?){
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: msn?msn:"<b>Noficación automatica </b>"

    },{
        type: type[id_type?id_type:2],
        timer: 200,
        placement: {
            from: from,
            align: align
        }
    });
  }
}
