import { Component,Inject, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
//import 'rxjs/add/operator/filter';
import { DataSource } from '@angular/cdk/collections';
import {Observable,of} from 'rxjs';
//import 'rxjs/add/observable/of';
import * as _ from 'lodash';
/*
*    Material modules for component
*/
import {MatTabChangeEvent, VERSION} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

/*
*     others component
*/
/*
*     models for  your component
*/
import { Token } from '../../../../../../../shared/token'
import { AttributeList} from '../../models/attributeList'
import { AttributeComplete } from '../../models/attributeComplete'

import { AttributeDetailList} from '../../models/attributeDetailList'
import { Attribute} from '../../models/attribute'
import { AttributeValue} from '../../models/attributeValue'
// DTO's
import { AttributeListDTO} from '../../models/attributeListDTO'
import { AttributeDetailListDTO} from '../../models/attributeDetailListDTO'
import { AttributeDTO} from '../../models/attributeDTO'
import { AttributeValueDTO} from '../../models/attributeValueDTO'

import { CommonStateStoreDTO } from '../../../commons/CommonStateStoreDTO'
import { CommonStoreDTO } from '../../../commons/CommonStoreDTO'

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../../shared/localStorage'
import { AttributeService } from '../../attribute.service';

import { DialogAttributeComponent } from './dialog-attribute/dialog-attribute.component'

/*
*     constant of  your component
*/
declare var $ :any

@Component({
  selector: 'app-attribute-new',
  templateUrl: './attribute-new.component.html',
  styleUrls: ['./attribute-new.component.scss']
})
export class AttributeNewComponent implements OnInit {
 //attributes

  STATE=1
  CURRENT_ID_THIRD = 0;
  form: FormGroup;
  activateValue=true;
  IsCreateAttr=false;
  IsValueToAdd=false;
  animal: string="HOLAAA";
  name: string="MUNDOOO";

  attributeValuePOSTList:any[]



   //entities
    token:Token;
    attributeComplete:AttributeComplete[]
    thirdAux:Attribute[];
    attributes:Attribute[];
    attributeValues:AttributeValue[];


  //DTO's
  attributeListDTO:AttributeListDTO;
  attributeDetailListDTO:AttributeDetailListDTO;
  attributeDetailListDTOs:AttributeDetailListDTO[];
  commonStateStoreDTO:CommonStateStoreDTO;
  common:CommonStoreDTO;


  constructor(public locStorage: LocalStorage,
    private _router: Router,
    private fb: FormBuilder,
    public attributeService: AttributeService,
    public dialog: MatDialog) {

      this.attributeValuePOSTList=[]
      this.attributeDetailListDTOs=[]
      this.common=new CommonStoreDTO(null,null)
      this.commonStateStoreDTO= new CommonStateStoreDTO(1,null,null)
      this.attributeListDTO= new AttributeListDTO(this.commonStateStoreDTO)

      this.attributeDetailListDTO= new AttributeDetailListDTO(null,this.commonStateStoreDTO)
      this.createAttributeListControls();
      this.logNameChange();

    }

  ngOnInit() {
    //this.createAttributeListControls();
    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {

      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD=this.token.id_third;


      if (this.CURRENT_ID_THIRD!==null && this.CURRENT_ID_THIRD>0){
        this.getAttributeComplete();
        this.getAttributes()
      }

    }
  }

  addAttribute(isAddOnlyValue?:boolean){
    this.IsCreateAttr=!this.IsCreateAttr
    console.log("Add Attribute ")

    let dialogRef = this.dialog.open(DialogAttributeComponent, {
      width: '650px',
      data: { attributeTemp:this.form.value['selectAttribute'] }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;

      console.log(result);
      this.getAttributes(this.STATE)
    });

  }

  addAttributeValue(){
    console.log("Add Attribute Value ")
  }

  addAttributeDetailList(){
    this.commonStateStoreDTO.state=1;

    // First Step: Assigemen to DTO a id_attribute_value
    this.attributeDetailListDTO.id_attribute_value=this.form.value['selectAttributeValue'].id_attribute_value
    console.log("Valor ",this.attributeValuePOSTList.length)


    // verify list of type ANY If It is empty or not
    if(this.attributeValuePOSTList.length>0){ //If It is not empty, You should verify all elements,This should are unique

        // @TODO Create a for
        for(let attrValAny of this.attributeValuePOSTList ){
          console.log("Condicional ",attrValAny,this.attributeDetailListDTO.id_attribute_value)


              if(attrValAny.dto.id_attribute_value!==this.form.value['selectAttributeValue'].id_attribute_value){
                 alert("IGUAL")
                this.attributeDetailListDTO.state=this.commonStateStoreDTO;


                  }
        }

    }else{// If the List is empty, You could to insert first element to List
      this.attributeDetailListDTO.id_attribute_value=this.form.value['selectAttributeValue'].id_attribute_value
      this.attributeDetailListDTO.state=this.commonStateStoreDTO;
      this.attributeValuePOSTList=[
        {"attribute":this.form.value['selectAttribute'],
        "value":this.form.value['selectAttributeValue'],
        "dto":this.attributeDetailListDTO}]
    }

    console.log(this.attributeValuePOSTList)
  }


  getAttributes(state?:number,id_category?:number,id_category_father?:number,id_third?:number){

      this.attributeService.getAttribute()
      .subscribe((data: Attribute[]) => this.attributes = data,
      error => console.log(error),
      () => {

        if(this.attributes.length>0){
          console.log(" Attribute ,", this.attributes)
          //this.getAttributeValues()

        }
      });
    }

    getAttributeComplete(state?: number,is_values?:Boolean,id_attribute?:number,id_common?:number,
      name?: string,description?:string, id_state?:number){
      this.attributeService.getAttributeComplete(state)
      .subscribe((data: AttributeComplete[]) => this.attributeComplete = data,
      error => console.log(error),
      () => {
        if(this.attributeComplete.length>0 ){

        }
      });
    }

    getAttributeValues(state?: number,id_attribute_value?:number,id_attribute?:number){
          this.attributeService.getAttributeValue(state,id_attribute_value,id_attribute,)
          .subscribe((data: AttributeValue[]) => this.attributeValues = data,
          error => console.log(error),
          () => {
            if(this.attributeValues.length>0 ){

            }
          });
    }

    createAttributeListControls() {
      this.form = this.fb.group({
        name: ['', Validators.compose([
          Validators.required
        ])],
        selectAttribute: ['', Validators.compose([
          Validators.required
        ])],
        selectAttributeValue: ['', Validators.compose([
          Validators.required
        ])],

        description: ['', Validators.compose([

        ])]
      });
    }
    // Delete elements from list for POST
    removeAttributeValue(attributePOSTList){
      this.attributeValuePOSTList=_.filter(this.attributeValuePOSTList, function (element) {
         return element.dto.id_attribute_value !== attributePOSTList.dto.id_attribute_value;
        });
    }

    logNameChange() {



      const attributeControl = this.form.get('selectAttribute');

      attributeControl.valueChanges.forEach(
        (value: string) =>{




          console.log("VALUE ", attributeControl['_value'].id_attribute)

          this.IsValueToAdd=true;
          let attribute =+attributeControl['_value'].id_attribute;

          if (attribute>0){

            this.getAttributeValues(this.STATE,null,attribute)

          }else{
            this.attributes=[]
            if (attribute===-1){
              this.IsCreateAttr=true;
            }else{
              //this.showNotification('top',)
            }
          }

        }
      );

      const attributeValueControl = this.form.get('selectAttributeValue');
      attributeValueControl.valueChanges.forEach(
        (value: string) =>{
          let id=+attributeValueControl['_value'].id_attribute_value

          if(id>0){
            this.IsValueToAdd=true
          }

          console.log("VALUE ", attributeValueControl['_value'].id_attribute_value)
        }
      );
    }

      Login() {
        let link = ['/auth'];
        this._router.navigate(link);
      }


      save(){
        this.attributeDetailListDTOs=[]

        for(let attrValAny of this.attributeValuePOSTList ){
          this.attributeDetailListDTOs.push(attrValAny.dto)
        }



        if(this.attributeDetailListDTOs.length>0){

          // 1. Sent Attribute List, and Get ID attribute_list
          this.commonStateStoreDTO.state=1;
          this.attributeListDTO.state=this.commonStateStoreDTO;
          // 1.1 Sent only a common state
          this.attributeService.postAttributeList(this.attributeListDTO)
          .subscribe(
            result => {

              if (result) {
                console.log("RESPONSE 1 ",result)

                // 2. Sent  ID attribute_list as Query Params to Attr Detail List

                this.attributeService.postAttributeDetailList(result,this.attributeDetailListDTOs)
                .subscribe(
                  result => {

                    if (result) {

                      this.showNotification('top','right',2,"Se ha <b>CREADO</b> Correctamente");
                      this.attributeDetailListDTOs=[]
                      this.attributeValuePOSTList=[]
                      this.resetForm();
                      //this.goBack();
                    }
                  })

                //3.


                return;
              } else {
                this.showNotification('top','right',3,"Problemas al <b>CREAR</b>");
                //this.openDialog();
                return;
              }
            })
        }

        console.log("After of do sent ",this.attributeDetailListDTOs)
      }


      resetForm() {

        this.form.reset();

      }

      goBack() {
        let link = ['/dashboard/business/measure-unit'];
        this._router.navigate(link);
      }

      showNotification(from, align,id_type?, msn?){
        const type = ['','info','success','warning','danger'];

        const color = Math.floor((Math.random() * 4) + 1);

        $.notify({
            icon: "notifications",
            message: msn?msn:"<b>Noficación automatica </b>"

        },{
            type: type[id_type?id_type:2],
            timer: 200,
            placement: {
                from: from,
                align: align
            }
        });
      }
}
@Component({
  selector: 'dialog-attrib',
  templateUrl: 'dialog-attrib.html',
})
export class DialogAttribute {

  name_2:string="LUISSS"
  form_attr: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogAttribute>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder) {

      console.log("DATA -> " , this.data )

    }

    addAttributeValue(){

      console.log("CREAR VALORES")
    }

    createAttributeControls() {
      this.form_attr = this.fb.group({
        name: ['', Validators.compose([
          Validators.required
        ])],
        descrition: ['', Validators.compose([
          Validators.required
        ])],
        selectAttributeValue: ['', Validators.compose([
          Validators.required
        ])],

        description: ['', Validators.compose([

        ])]
      });
    }

  onNoClick(): void {


    this.dialogRef.close();
  }

}
