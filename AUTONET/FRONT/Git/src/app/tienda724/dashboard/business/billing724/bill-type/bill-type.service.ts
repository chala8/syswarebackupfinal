import { Injectable } from '@angular/core';
//import { Http, Headers, Response, URLSearchParams } from '@angular/http';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';
import {from, Observable, of} from 'rxjs';
/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';

/** Import Model Data */
import { CommonState } from '../commons/commonState'
import { BillType } from './models/billType'
/** Import Model Data Transfer Object */
import { CommonStateDTO } from '../commons/commonStateDTO'
import { BillTypeDTO } from './models/billTypeDTO'

@Injectable()
export class BillTypeService {
  api_uri = Urlbase[3] + '/billing-type';
  private options;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private locStorage: LocalStorage) {


    this.headers = new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization':  this.locStorage.getTokenValue(),
    });

    let token = localStorage.getItem('currentUser');

    this.options = { headers: this.headers };
  }


  public getBillTypeResource = (state_bill?:number,id_bill_type?:number ,id_bill?:number ): Observable<{} | BillType[]> => {

    let params = new HttpParams();
    let Operaciones = [];

    Operaciones.push(['id_bill_type', id_bill_type ? "" + id_bill_type : null]);
    Operaciones.push(['id_bill', id_bill ? "" + id_bill : null]);
    Operaciones.push(['state_bill', state_bill ? "" + state_bill : null]);

    for(let n = 0;n<Operaciones.length;n++){ if(Operaciones[n][1] != null){ params = params.append(Operaciones[n][0],  Operaciones[n][1]); } }
    this.options.params = params;
    let promesa = new Promise((resolve, reject) => {
      this.http.get<any[]>(this.api_uri, this.options)
        .subscribe(value => {resolve(value)},error => {this.handleError(error);reject(error)});
    });
    return from(promesa);
  };

  public postBillTypeResource = (body:BillTypeDTO): Observable<number[] | any> => {
    let params = new HttpParams();
    this.options.params = params;

    let promesa = new Promise((resolve, reject) => {
      this.http.post<any>(this.api_uri, body, this.options)
        .subscribe(value => {resolve(value)},error => {this.handleError(error);reject(null)});
    });
    return from(promesa);
  };


  private handleError(error: Response | any) {
      // In a real world app, we might use a remote logging infrastructure
      let errMsg: string;
      if (error instanceof Response) {
        const body = error.json() || '';
        // @ts-ignore
      const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      alert(errMsg);
      return null;//Observable.throw(errMsg);
  }
}
