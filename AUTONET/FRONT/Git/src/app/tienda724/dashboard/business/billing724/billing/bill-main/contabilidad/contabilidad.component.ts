import { Component, OnInit } from '@angular/core';
//import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '../../../../../../../shared/localStorage';
import { BillingService } from '../../billing.service';
import {HttpClient} from '@angular/common/http';
import {Urlbase} from '../../../../../../../shared/urls';
import { MatDialog } from '@angular/material';
import { ContUpdateComponent } from '../cont-update/cont-update.component';
import 'bootstrap-notify';
import * as jQuery from 'jquery';
import { DatePipe } from '@angular/common';
let $: any = jQuery;

@Component({
  selector: 'app-contabilidad',
  templateUrl: './contabilidad.component.html',
  styleUrls: ['./contabilidad.component.scss']
})
export class ContabilidadComponent implements OnInit {

  constructor(public datepipe: DatePipe, public ContUpdateComponent: ContUpdateComponent, public dialog: MatDialog,private http2: HttpClient,
              public locStorage: LocalStorage,
              private categoriesService: BillingService) { }
              panelOpenState = false;
  selectedTypeDoc=3;
  docTypeList;
  notes = "";
  selectedStore2 = String(this.locStorage.getIdStore());
  selectedStore = String(this.locStorage.getIdStore());
  selectedStore3 = String(this.locStorage.getIdStore());
  selectedType = -1
  selectedState = -1
  dateC1;
  dateC11;
  dateC2;
  dateC22;
  Stores;
  docList = [];
  
  profundidad = 1;

  //nivel 1
  selectedFirstCC = "";
  firstLvlCC = [];
  //nivel 2
  selectedCC2 = "";
  lvlCC2 = [];
  //nivel 3
  selectedCC3 = "";
  lvlCC3 = [];
  //nivel 4
  selectedCC4 = "";
  lvlCC4 = [];
  //nivel 5
  selectedCC5 = "";
  lvlCC5 = [];
  //naturaleza
  selectedNat = "C"
  //valorCuenta
  cuenta = "0";
  //notasDetalle
  notesD= "";

  //Tabla Detalles
  tablaDetalles = []

  ngOnInit() {

    this.getDocTypeList();
    this.getStores();
    this.getFirstlvlCC();

  }

  generateBalance(){
    this.http2.post(Urlbase[2]+"/kazu724/generateBalance?id_store="+this.selectedStore3+"&profundidad="+this.profundidad+"&fecha_inicial="+this.dateC11+"&fecha_final="+this.dateC22,{}).subscribe(response => {
      if(response==1){
        this.http2.get(Urlbase[2]+"/kazu724/getBalance?id_store="+this.selectedStore3).subscribe(responseList => {
          console.log("tsting",responseList);
           this.http2.post(Urlbase[3]+"/billing/Test",{empresa: this.locStorage.getThird().info.fullname,
                                                                    NIT: this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,
                                                                    Fecha: this.datepipe.transform(new Date(), 'yyyy_MM_dd'),
                                                                    Filas: responseList},{
                                                                     responseType: 'text'
                                                                   }).subscribe(response => {
                                                                      console.log(response);
                                                                      window.open(Urlbase[7]+"/"+response, "_blank");
            this.showNotification('top', 'center', 3, "<h3>Se genero su balance exitosamente.</h3> ", 'info');
            console.log(JSON.stringify({empresa: this.locStorage.getThird().info.fullname,
              NIT: this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,
              Fecha: this.datepipe.transform(new Date(), 'yyyy/MM/dd'),
              Filas: responseList}))
           })
        })
      }else{
        this.showNotification('top', 'center', 3, "<h3>Se presento un error al generar el balance.</h3> ", 'danger');
      }
    });
  }

  
  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
      type: typeStr ? typeStr : type[id_type ? id_type : 2],
      timer: 200,
      placement: {
        from: from,
        align: align
      }
    });
  }



  addDetail(){
    if(this.selectedFirstCC=="" || this.cuenta=="0" || this.notesD == ""){
      this.showNotification('top', 'center', 3, "<h3>Faltan datos para poder agregar el detalle.</h3> ", 'danger');
    }else{
    if(this.selectedCC2==""){
      this.tablaDetalles.push({
        cuenta: this.selectedFirstCC,
        naturaleza: this.selectedNat,
        valor: Number(this.cuenta),
        nota: this.notesD
      })
    }else{
      if(this.selectedCC3==""){
        this.tablaDetalles.push({
          cuenta: this.selectedCC2,
          naturaleza: this.selectedNat,
          valor: Number(this.cuenta),
          nota: this.notesD
        })
      }else{
        if(this.selectedCC4==""){
          this.tablaDetalles.push({
            cuenta: this.selectedCC3,
            naturaleza: this.selectedNat,
            valor: Number(this.cuenta),
            nota: this.notesD
          })
        }else{
          if(this.selectedCC5==""){
            this.tablaDetalles.push({
              cuenta: this.selectedCC4,
              naturaleza: this.selectedNat,
              valor: Number(this.cuenta),
              nota: this.notesD
            })
          }else{
            this.tablaDetalles.push({
              cuenta: this.selectedCC5,
              naturaleza: this.selectedNat,
              valor: Number(this.cuenta),
              nota: this.notesD
            })
          }
        }

      }
    }
    }

    this.selectedFirstCC="";
    this.selectedCC2="";
    this.selectedCC3="";
    this.selectedCC4="";
    this.selectedCC5="";
    this.selectedNat="C";
    this.cuenta="0";
    this.notesD="";
  }

  getFirstlvlCC(){
    this.http2.get(Urlbase[2] + "/kazu724/getcodcuentagen").subscribe(list => {
      //@ts-ignore
      this.firstLvlCC = list;
      this.selectedCC2 = "";
      this.selectedCC3 = "";
      this.selectedCC4 = "";
      this.selectedCC5 = "";
    })
  }

  getlvlCC2(){
    this.http2.get(Urlbase[2] + "/kazu724/getcodcuenta?cp="+this.selectedFirstCC).subscribe(list => {
      //@ts-ignore
      this.lvlCC2 = list;
      this.selectedCC3 = "";
      this.selectedCC4 = "";
      this.selectedCC5 = "";
    })
  }

  getlvlCC3(){
    this.http2.get(Urlbase[2] + "/kazu724/getcodcuenta?cp="+this.selectedCC2).subscribe(list => {
      //@ts-ignore
      this.lvlCC3 = list;
      this.selectedCC4 = "";
      this.selectedCC5 = "";
    })
  }

  getlvlCC4(){
    this.http2.get(Urlbase[2] + "/kazu724/getcodcuenta?cp="+this.selectedCC3).subscribe(list => {
      //@ts-ignore
      this.lvlCC4 = list;
      this.selectedCC5 = "";
    })
  }

  getlvlCC5(){
    this.http2.get(Urlbase[2] + "/kazu724/getcodcuenta?cp="+this.selectedCC4).subscribe(list => {
      //@ts-ignore
      this.lvlCC5 = list;
    })
  }

  getStores() {
    this.categoriesService.getStoresByThird(this.locStorage.getToken().id_third).subscribe(data => {
        console.log(data);this.Stores = data
        this.selectedStore = data[0].id_STORE})
}

  getDocTypeList(){
    this.http2.get(Urlbase[2] + "/kazu724/getdoctype").subscribe(list => {
      this.docTypeList = list;
    })
  }

  postMaster(){
    this.http2.post(Urlbase[2] + "/kazu724/doc",{
    id_document_status: 1,
    id_document_type: this.selectedTypeDoc,
    notes: this.notes,
    id_store: this.selectedStore,
    id_third_user: this.locStorage.getThird().id_third}).subscribe(response => {
      this.tablaDetalles.forEach(element => {
        console.log("THIS IS IT, ",element)
        let body ={
          cc: Number(element.cuenta),
          valor: Number(element.valor),
          naturaleza: element.naturaleza,
          notes: element.nota,
          id_document: Number(response),
          id_country: 169
        }
        console.log("THIS IS BODY", body);
        this.http2.post(Urlbase[2] + "/kazu724/detail",body).subscribe(element2 => {})
      })
      console.log("THIS IS MY RESPONSE: ",response);
      this.notes = "";
      this.tablaDetalles = [];
    })
  }


  individualDelete(doc){
    this.tablaDetalles.forEach( (item, index) => {
      if(item === doc) this.tablaDetalles.splice(index,1);
    });
 }

 calculateBalance(){
   let creds = 0;
   let debts = 0;
  this.tablaDetalles.forEach( (item, index) => {
    if(item.naturaleza == 'C') creds+=item.valor;
    if(item.naturaleza == 'D') debts+=item.valor;
  });
  return debts-creds;
 }

 getDocs(){
   let list;
   let list2;
   if(this.selectedType==-1){
    list = "1,2,3,4";
   }else{
    list = ""+this.selectedType;
   }
   if(this.selectedState==-1){
    list2 = "1,2,3";
   }else{
    list2 = ""+this.selectedState;
   }
   this.http2.get(Urlbase[2]+"/kazu724/getDocumentHeader?id_store="+this.selectedStore2+"&date1="+this.dateC1+"&date2="+this.dateC2+"&statusList="+list2+"&typeList="+list).subscribe(
    element => {
      //@ts-ignore
      this.docList =  element;
    }
   );
 }

 openDetailWindow(element){
  const dialogRef = this.dialog.open(ContUpdateComponent, {
    width: '60vw',
    height: '80vh',
    data: { element }
  });
  dialogRef.afterClosed().subscribe(result => {

  });
 }

}
