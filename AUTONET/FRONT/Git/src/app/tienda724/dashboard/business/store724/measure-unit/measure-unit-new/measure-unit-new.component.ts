import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
//import 'rxjs/add/operator/filter';
/*
*    Material modules for component
*/
/*
*     others component
*/
/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { MeasureUnitDTO } from '../models/measureUnitDTO'
import { CommonStateStoreDTO } from '../../commons/CommonStateStoreDTO'
import { CommonStoreDTO } from '../../commons/CommonStoreDTO'

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { MeasureUnitService } from '../measure-unit.service';
/*
*     constant of  your component
*/
let $: any = jQuery;
import * as jQuery from 'jquery';
import 'bootstrap-notify';

@Component({
  selector: 'app-measure-unit-new',
  templateUrl: './measure-unit-new.component.html',
  styleUrls: ['./measure-unit-new.component.scss']
})
export class MeasureUnitNewComponent implements OnInit {

  form: FormGroup;
  token:Token;
  newMeasureUnit:MeasureUnitDTO;
  commonStateStoreDTO:CommonStateStoreDTO;
  common:CommonStoreDTO;

   //attributes
   CURRENT_FATHER = 0;
   CURRENT_ID_CATEGORY = 0;

  constructor(private router: Router, private route:ActivatedRoute ,private categoriesService: MeasureUnitService,
              private fb: FormBuilder, private locStorage: LocalStorage) {

                this.common=new CommonStoreDTO(null,null)
                this.commonStateStoreDTO= new CommonStateStoreDTO(1,null,null)
                this.newMeasureUnit=new MeasureUnitDTO(null,this.common,this.commonStateStoreDTO)
              }

  ngOnInit() {
    this.createControls();

    let session=this.locStorage.getSession();
    if(!session){
        this.Login();
    }else{

      this.route.queryParams
          .subscribe(params => {

            // Defaults to 0 if no query param provided.
              this.token=this.locStorage.getToken();

              this.CURRENT_FATHER=params.father;

            this.loadData()
        });
    }
  }

  Login() {
    let link = ['/auth'];
    this.router.navigate(link);
  }


  createControls() {
    this.form = this.fb.group({
      name: ['', Validators.compose([
        Validators.required
      ])],
      description: ['', Validators.compose([

      ])]
    });
  }


  loadData() {
    this.form.patchValue({
      name: "Test",
      description:"Descripción"
    });

  }


  createNewCategory() {

    this.common.name= this.form.value["name"];
    this.common.description= this.form.value["description"];

    this.newMeasureUnit.state=this.commonStateStoreDTO;
    this.newMeasureUnit.common=this.common;
    this.newMeasureUnit.id_measure_unit_father=this.CURRENT_FATHER>0?this.CURRENT_FATHER:null;

    console.log("CAE ",this.newMeasureUnit)
    this.categoriesService.postCategory(this.newMeasureUnit)
    .subscribe(
    result => {

      if (result === true) {
          this.showNotification('top','right',2,"Se ha <b>CREADO</b> Correctamente");
         this.resetForm();
         this.goBack();



        return;
      } else {
        this.showNotification('top','right',3,"Problemas al <b>ACTUALIZAR</b>");
        //this.openDialog();
        return;
      }
    })
  }

  resetForm() {

    this.form.reset();

  }

  goBack() {
    let link = ['/dashboard/business/measure-unit'];
    this.router.navigate(link);
  }

  showNotification(from, align,id_type?, msn?){
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: msn?msn:"<b>Noficación automatica </b>"

    },{
        type: type[id_type?id_type:2],
        timer: 200,
        placement: {
            from: from,
            align: align
        }
    });
  }

}
