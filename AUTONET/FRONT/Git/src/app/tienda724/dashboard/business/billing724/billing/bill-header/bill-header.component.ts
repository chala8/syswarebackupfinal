import { Component, OnInit,Input,Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import {Observable,of} from 'rxjs';
/*
*     others component
*/

/*
*     models for  your component
*/
import { BillType } from '../../bill-type/models/billType';
import { Token } from '../../../../../../shared/token'
import { Session } from '../../../../../../shared/session'
import { Person } from '../../../../../../shared/models/person'
import { Third } from '../../../thirds724/third/models/third';
import { Inventory } from '../../../store724/inventories/models/inventory';

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { BillTypeService } from '../../bill-type/bill-type.service'
import { InventoriesService } from '../../../store724/inventories/inventories.service';


/*
*     constant of  your component
*/
@Component({
  selector: 'app-bill-header',
  templateUrl: './bill-header.component.html',
  styleUrls: ['./bill-header.component.scss']
})
export class BillHeaderComponent implements OnInit {
    // callbacks
    @Input() ID_BILLTYPE:number;

    @Output() emmitType = new EventEmitter<any>();

    // attribute
    CURRENT_ID_THIRD = 0;
    CURRENT_ID_THIRD_PATHER=0;
    STATE=1
    today = Date.now();

    // models
    third:Third;
    thirdFather:Third;
    token:Token;
    session:Session;
    person:Person;
    billType:BillType;
    inventory:Inventory;

  constructor(public locStorage: LocalStorage,private _router:Router,
    public billTypeService:BillTypeService,private inventoriesService:InventoriesService) { }

  ngOnInit() {
    this.session = this.locStorage.getSession();
    if (!this.session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.third = this.locStorage.getThird();
      this.person= this.locStorage.getPerson();

      this.CURRENT_ID_THIRD=this.token.id_third;
      this.CURRENT_ID_THIRD_PATHER=this.token.id_third_father;
      if (this.CURRENT_ID_THIRD!==null && this.CURRENT_ID_THIRD>0){
           this.getBillType(this.STATE,this.ID_BILLTYPE);

      }
    }
  }

  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }

    // Services
    getBillType(STATE?:number,id_bill_type?:number){
      let response;
        this.billTypeService.getBillTypeResource(STATE,id_bill_type)
      .subscribe((data: BillType[]) =>  response=data,
      error => console.log(error),
      () => {
        if(response.length>0){
          this.billType=response[0];
          this.getInventory(this.STATE,this.CURRENT_ID_THIRD_PATHER)

        }else{
          alert(" NO SE PUEDE GENERAR UNA FACTURA [TIPO DE FACTURA]")
        }
      });
    }


    getInventory(STATE?:number,id_third?:number){
      let inv:Inventory[]=[];
      this.inventoriesService.getInventoriesList(STATE,null,id_third)
      .subscribe((response:Inventory[])=> inv=response,
        error=> console.log(error),
        ()=>{
          if(inv.length>0){
            this.inventory=inv[0]
            this.emmitType.emit({"type":this.billType.name_bill_type,"inventory":this.inventory.id_inventory});
          }

        }
      );

    }

}
