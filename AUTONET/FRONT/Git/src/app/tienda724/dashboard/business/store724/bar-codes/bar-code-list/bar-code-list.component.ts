import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import {Observable,of} from 'rxjs';
//import 'rxjs/add/observable/of';
import * as _ from 'lodash';

import {MatTabChangeEvent, VERSION} from '@angular/material';


import { LocalStorage } from '../../../../../../shared/localStorage';
import { Token } from '../../../../../../shared/token';

import { BarCodeService } from '../bar-code.service';
import { Code } from '../models/code';


var categoryList:Code[];
var categoryListGlobal:Code[];

declare var $:any


@Component({
  selector: 'app-bar-code-list',
  templateUrl: './bar-code-list.component.html',
  styleUrls: ['./bar-code-list.component.scss']
})
export class BarCodeListComponent implements OnInit {

  isMyProduct=true;
  token:Token;
  ID_THIRD_TYPE: number;
  STATE=1
  CURRENT_ID_THIRD = 0;
  codeGeneral:Code[];
  codePersonal:Code[];

  thirdAux:Code[];
  indexTab:number=0;






    displayedColumns = ['position', 'name', 'weight', 'symbol', 'direccion', 'opciones'];
    displayedColumns_General = ['position', 'name', 'weight', 'symbol', 'direccion'];
    dataSource:ProductDataSource

  constructor(public locStorage: LocalStorage,
    private _router: Router,
    public productService: BarCodeService) {
      this.codeGeneral=[];
      this.codePersonal=[];
    }

  ngOnInit() {
    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD=this.token.id_third;
      this.ID_THIRD_TYPE = 23;
      this.getCodeList(this.STATE)

      if (this.CURRENT_ID_THIRD!==null && this.CURRENT_ID_THIRD>0){

      }


    }
  }

  getCodeList(state?: number,id_code?:number, id_third_cod?: number,code?:string,
    id_product?: number, img_url?:string, id_measure_unit?: number,
    id_attribute_list?:number, id_state?:number, modify_attribute?:Date){

        this.productService.getCodeList(state,id_code,id_third_cod)
        .subscribe((data: Code[]) => categoryList = data,
        error => console.log(error),
        () => {
          if(categoryList.length>0){
            this.codeGeneral=categoryList.filter(e=>{
              return e.id_third_cod==null || e.id_third_cod<1
            })
            this.codePersonal=categoryList.filter(e=>{
              return e.id_third_cod>0
            })
          }
          categoryList=this.codeGeneral;
          this.dataSource = new ProductDataSource();
        });

      }

      Login() {
        let link = ['/auth'];
        this._router.navigate(link);
      }

      editCategory(category:Code){
        this._router.navigate(['/dashboard/business/bar-code/edit',category.id_code] );



      }
      tabChanged = (tabChangeEvent: MatTabChangeEvent): void => {

        this.indexTab=tabChangeEvent.index;
        if(this.indexTab==1){

          categoryList=this.codePersonal;
          this.dataSource = new ProductDataSource();

        }else{
          categoryList=this.codeGeneral;
          this.dataSource = new ProductDataSource();
        }
            console.log(tabChangeEvent)
    }

      addCategory(category:Code){

        this._router.navigate(['/dashboard/business/bar-code/new'],{queryParams:{father:category.id_code}} );

      }

      deleteCategory(id_product) {

            this.productService.Delete(id_product)
              .subscribe(
              result => {

                if (result === true) {
                  categoryList = _.filter(categoryList, function (f) { return f.id_code !== id_product; });
                  this.dataSource = new ProductDataSource();

                  this.showNotification('top','right',2,"Se ha <b>ELIMINADO</b> correctamente!");

                  return;
                } else {
                  this.showNotification('top','right',3,"Problemas al <b>ELIMINAR</b>");

                  return;
                }
              })

          }

          showNotification(from, align,id_type?, msn?){
            const type = ['','info','success','warning','danger'];

            const color = Math.floor((Math.random() * 4) + 1);

            $.notify({
                icon: "notifications",
                message: msn?msn:"<b>Noficación automatica </b>"

            },{
                type: type[id_type?id_type:2],
                timer: 200,
                placement: {
                    from: from,
                    align: align
                }
            });
          }

}
export class ProductDataSource extends DataSource<any> {
  /** Connect function called by the table to retrieve one stream containing the data to render. */

  connect(): Observable<Code[]> {

    return of(categoryList);
  }

  disconnect() {}
}
