import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import {Observable,of} from 'rxjs';
//import 'rxjs/add/observable/of';
import * as _ from 'lodash';

import {MatTabChangeEvent, VERSION} from '@angular/material';


import { LocalStorage } from '../../../../../../shared/localStorage';
import { Token } from '../../../../../../shared/token';

import { MeasureUnitService } from '../measure-unit.service';
import { MeasureUnit } from '../models/measureUnit';


var categoryList:MeasureUnit[];
var categoryListGlobal:MeasureUnit[];

declare var $ :any
@Component({
  selector: 'app-measure-unit-list',
  templateUrl: './measure-unit-list.component.html',
  styleUrls: ['./measure-unit-list.component.scss']
})
export class MeasureUnitListComponent implements OnInit {

  isMyProduct=true;
  token:Token;
  ID_THIRD_TYPE: number;
  STATE=1
  CURRENT_ID_THIRD = 0;

  thirdAux:MeasureUnit[];

    displayedColumns = ['position', 'name', 'weight', 'symbol', 'direccion', 'opciones'];
    displayedColumns_General = ['position', 'name', 'weight', 'symbol', 'direccion'];
    dataSource:ProductDataSource

  constructor(public locStorage: LocalStorage,
    private _router: Router,
    public productService: MeasureUnitService) { }

  ngOnInit() {
    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD=this.token.id_third;
      this.ID_THIRD_TYPE = 23;

      if (this.CURRENT_ID_THIRD!==null && this.CURRENT_ID_THIRD>0){

        this.getMeasureUnitList(this.STATE)

      }


    }
  }
// @todo You must see this method's name, If It name fail
  getMeasureUnitList(state?:number,id_category?:number,id_category_father?:number,id_third?:number){

      this.productService.getCategoryList(state)
      .subscribe((data: MeasureUnit[]) => categoryList = data,
      error => console.log(error),
      () => {

        this.dataSource = new ProductDataSource();
      });

    }

      Login() {
        let link = ['/auth'];
        this._router.navigate(link);
      }

      editCategory(category:MeasureUnit){
        this._router.navigate(['/dashboard/business/measure-unit/edit',category.id_measure_unit] );



      }

      addCategory(category:MeasureUnit){

        this._router.navigate(['/dashboard/business/measure-unit/new'],{queryParams:{father:category.id_measure_unit}} );

      }

      deleteCategory(id_measure_unit) {

            this.productService.Delete(id_measure_unit)
              .subscribe(
              result => {

                if (result === true) {
                  categoryList = _.filter(categoryList, function (f) { return f.id_measure_unit !== id_measure_unit; });
                  this.dataSource = new ProductDataSource();


                  this.showNotification('top','right',2,"Se ha <b>ELIMINADO</b> correctamente!");

                  return;
                } else {
                  this.showNotification('top','right',3,"Problemas al <b>ELIMINAR</b>");

                  return;
                }
              })

          }



          showNotification(from, align,id_type?, msn?){
            const type = ['','info','success','warning','danger'];

            const color = Math.floor((Math.random() * 4) + 1);

            $.notify({
                icon: "notifications",
                message: msn?msn:"<b>Noficación automatica </b>"

            },{
                type: type[id_type?id_type:2],
                timer: 200,
                placement: {
                    from: from,
                    align: align
                }
            });
          }

}
export class ProductDataSource extends DataSource<any> {
  /** Connect function called by the table to retrieve one stream containing the data to render. */

  connect(): Observable<MeasureUnit[]> {

    return of(categoryList);
  }

  disconnect() {}
}
