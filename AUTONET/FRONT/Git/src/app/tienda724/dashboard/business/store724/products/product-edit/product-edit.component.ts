import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
//import 'rxjs/add/operator/filter';
/*
*    Material modules for component
*/
/*
*     others component
*/
/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { ProductDTO } from '../models/productDTO'
import { Product } from '../models/product'
import { CommonStateStoreDTO } from '../../commons/CommonStateStoreDTO'
import { CommonStoreDTO } from '../../commons/CommonStoreDTO'

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { ProductsService } from '../products.service';
/*
*     constant of  your component
*/

let $: any = jQuery;
import * as jQuery from 'jquery';
import 'bootstrap-notify';
@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {

  form: FormGroup;
  token:Token;
  id_product:number=0;
  newProduct:ProductDTO;
  currentProduct:Product;
  productList:Product[];

  commonStateStoreDTO:CommonStateStoreDTO;

  common:CommonStoreDTO;
  STATE=1


  CURRENT_FATHER = 0;
  CURRENT_ID_CATEGORY = 0;

  constructor(private router: Router, private route:ActivatedRoute ,private productService: ProductsService,
    private fb: FormBuilder, private locStorage: LocalStorage) {

      this.common=new CommonStoreDTO(null,null)
      this.commonStateStoreDTO= new CommonStateStoreDTO(1,null,null)
      this.newProduct=new ProductDTO(null,null,null,null, this.common,this.commonStateStoreDTO)
    }
  ngOnInit() {
    this.createControls();


    let session=this.locStorage.getSession();
    if(!session){
        this.Login();
    }else{
      this.token=this.locStorage.getToken();
      this.route.params
          .subscribe( params =>{
              this.id_product=+params['id']


            // Defaults to 0 if no query param provided.


            this.CURRENT_FATHER=params.father;

            this.getProductList(this.STATE,this.id_product)


          });

      this.route.queryParams
          .subscribe(params => {

        });
    }

  }

  Login() {
    let link = ['/auth'];
    this.router.navigate(link);
  }

  createControls() {
    this.form = this.fb.group({

      //profile
      img_url: ['', Validators.compose([

      ])],
      name: ['', Validators.compose([
        Validators.required
      ])],
      description: ['', Validators.compose([

      ])],

      stock: ['', Validators.compose([
        Validators.required
      ])],

      stock_min: ['', Validators.compose([
        Validators.required
      ])]

    });
  }

  getProductList(state?:number,id_product?:number){

        this.productService.getProductList(state,id_product)
        .subscribe((data: Product[]) => this.productList = data,
        error => console.log(error),
        () => {
          if(this.productList.length>0){
              this.currentProduct=this.productList[0];
              this.loadData();
          }else{
            alert("No se encuetra elemento para editar")
            this.goBack();
          }
        });
      }

      loadData() {
        this.form.patchValue({
          name: this.currentProduct.name_product,
          description:this.currentProduct.description_product,
          img_url: this.currentProduct.img_url,
          stock: this.currentProduct.stock,
          stock_min: this.currentProduct.stock_min
    });

      }

      goBack() {
        let link = ['/dashboard/business/product'];
        this.router.navigate(link);
      }


      editProduct() {

            this.newProduct.img_url = this.form.value["img_url"],
            this.common.name= this.form.value["name"];
            this.common.description= this.form.value["description"];
            this.newProduct.common=this.common;
            this.newProduct.id_category=this.currentProduct.id_category;
            this.commonStateStoreDTO.state=this.currentProduct.state_product
            if(this.common.name===this.currentProduct.name_product &&
              this.common.description===this.currentProduct.description_product){


            }
            this.productService.putProduct(this.id_product,this.newProduct)
            .subscribe(
              result => {

                if (result === true) {
                   this.showNotification('top','right',2,"Cambios guardados correctamente");
                   this.resetForm();
                   this.goBack();
                  return;
                } else {
                  this.showNotification('top','right',3,"Problemas al actualizar");
                  //this.openDialog();
                  return;
                }
              })
          }

          showNotification(from, align,id_type?, msn?){
            const type = ['','info','success','warning','danger'];

            const color = Math.floor((Math.random() * 4) + 1);

            $.notify({
                icon: "notifications",
                message: msn?msn:"<b>Noficación automatica </b>"

            },{
                type: type[id_type?id_type:2],
                timer: 200,
                placement: {
                    from: from,
                    align: align
                }
            });
          }

          resetForm() {

            this.form.reset();

          }

}
