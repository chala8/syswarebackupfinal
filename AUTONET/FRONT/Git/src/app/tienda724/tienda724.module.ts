import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import 'hammerjs';
// This Module's Components
import { Tienda724Component } from './tienda724.component';
/*
************************************************
*     modules of  your app
*************************************************
*/
import { MaterialModule } from '../app.material';
import { ComponentsModule } from '../components/components.module';
import { BusinessModule } from './dashboard/business/business.module';
/*
*************************************************
*     services of  your app
*************************************************
*/

import { UserThirdService } from './dashboard/business/thirds724/user-third/user-third.service'
import { ThirdService } from './dashboard/business/thirds724/third/third.service'

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    BrowserModule,
    FormsModule, ReactiveFormsModule,
    ComponentsModule,
    BusinessModule
  ],
  declarations: [
      Tienda724Component,
  ],
  providers:[UserThirdService,ThirdService],
  exports: [
      Tienda724Component,
  ]
})
export class Tienda724Module {
 }
