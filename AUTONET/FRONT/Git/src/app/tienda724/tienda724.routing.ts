import { Routes } from '@angular/router';


import { Tienda724Component } from './tienda724.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/
import { BusinessRouting } from './dashboard/business/business.routing';
import { NewThirdComponent } from './dashboard/business/thirds724/third/new-third/new-third.component';


export const Tienda724Routing: Routes= [
    { path: 'dashboard', component: Tienda724Component,
        children: [
          { path: '', redirectTo: 'business', pathMatch: 'full'},
          ...BusinessRouting,
          { path: "new-third",      component: NewThirdComponent }
        ]
    }

];
