import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';






/*
************************************************
*    Material modules for app
*************************************************
*/
import { LocalStorage } from './localStorage';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,

  ],
  declarations: [  ],
  providers: [  ],
exports: [  ]
})
export class SharedModule { }
