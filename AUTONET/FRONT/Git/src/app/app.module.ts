import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CommonModule, CurrencyPipe, HashLocationStrategy, LocationStrategy} from '@angular/common';
import {MAT_DATE_LOCALE} from '@angular/material';

//import { MaterialModule } from '@angular/material';
import { MaterialModule } from './app.material';

import 'hammerjs';

/*
************************************************
*     modules of  your app
*************************************************
*/
import {  Tienda724Module } from "./tienda724/tienda724.module";


import { AppRouting } from './app.routing';


import { AppComponent } from './app.component';

import { ComponentsModule } from './components/components.module';
import { BillDialogQuantityComponent } from './bill-dialog-quantity/bill-dialog-quantity.component';
import { BillDialogThirdComponent } from './bill-dialog-third/bill-dialog-third.component';
import { ProductOnCategoryComponent } from './product-on-category/product-on-category.component';
import { AutofocusDirective } from '../app/shared/auto-focus.directive';
import {UserIdleModule} from 'angular-user-idle';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    BillDialogQuantityComponent,
    BillDialogThirdComponent,
    ProductOnCategoryComponent,
    AutofocusDirective,

  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule.forRoot(AppRouting),
    MaterialModule,
    //WelcomeModule,
    Tienda724Module,
    UserIdleModule.forRoot({idle: 7200, timeout: 10, ping: 0})
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy},CurrencyPipe,{ provide: MAT_DATE_LOCALE, useValue: 'en-US' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
