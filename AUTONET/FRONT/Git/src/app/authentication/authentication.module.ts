import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import 'hammerjs';

import { LoginComponent } from './login/login.component';
import { AuthenticationComponent } from './authentication.component';

import { LogoutComponent } from './logout/logout.component';

import { AuthenticationService } from './authentication.service';
import { NotAuthGuard } from './not-auth.guard';

import { LocalStorage } from '../shared/localStorage';

import {AuthenticationRouting} from './authentication.routing';


@NgModule({
  imports: [
    RouterModule,
    ReactiveFormsModule,
    AuthenticationRouting
  ],
  declarations: [LoginComponent, LogoutComponent, AuthenticationComponent],
  providers: [ AuthenticationService, NotAuthGuard, LocalStorage ],
exports: [ LoginComponent, LogoutComponent ]
})
export class AuthenticationModule { }
