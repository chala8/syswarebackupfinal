import {RouterModule, Routes} from '@angular/router';


import { LoginComponent }  from './login/login.component';
import { LogoutComponent }  from './logout/logout.component';
import { AuthenticationComponent } from './authentication.component';
import {NgModule} from '@angular/core';


const rutas: Routes = [
  {
    path: '', component: AuthenticationComponent,
    children: [
        { path: '', redirectTo: 'login', pathMatch: 'full'},
        { path: 'login', component: LoginComponent },
        { path: 'register', component: LogoutComponent },
    ]
  },
];


@NgModule({
  imports: [RouterModule.forChild(rutas)],
  exports: [RouterModule],
})
export class AuthenticationRouting {}
