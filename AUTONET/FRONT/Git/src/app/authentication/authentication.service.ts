import { Injectable } from '@angular/core';
//import { Http, Headers, Response, URLSearchParams } from '@angular/http';

//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';
import {from, Observable, of} from 'rxjs';

/** Files for auth process  */
import { Urlbase } from '../shared/urls';
import { LocalStorage } from '../shared/localStorage';
import { Auth } from './auth';
import { UsuarioDTO } from '../shared/models/auth/UsuarioDTO';

import { Token } from '../shared/token';
import { Session } from '../shared/session';
import {HttpClient, HttpHeaders} from '@angular/common/http';


@Injectable()
export class AuthenticationService {
  username: string;
  loggedIn: boolean;
  token: Token;
  session: Session;
  urlAuth = Urlbase[0] + '';
  private options;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient, private locStorage: LocalStorage) { }

  public login = (auth: Auth): Observable<{}|Boolean> => {
    let promesa = new Promise((resolve, reject) => {
      this.http.post<any>(this.urlAuth + '/login?id_aplicacion=' + 21 + '&dispositivo=WEB', auth, { headers: this.headers })
        .subscribe(value => {
          this.token = value;
          this.session = value;
          if (this.session.person) {
            localStorage.setItem('currentUserPersonStore724', JSON.stringify(this.session.person));
          }

          localStorage.setItem('currentUserSessionStore724', JSON.stringify(this.session));

          localStorage.setItem('currentUserTokenStore724', JSON.stringify(this.token));
          localStorage.setItem('currentUserMenuStore724', JSON.stringify(this.session['menus']));
          localStorage.setItem('currentUserRolStore724', JSON.stringify(this.session['roles']));

          this.loggedIn = true;

          resolve(true)
        },error => {
          alert("Usuario o Contraseña Incorrecto");
          this.loggedIn = false;
          this.handleError(error);
          reject(false);
        });
    });
    return from(promesa);
  };

  public postUserAUTH = (auth: UsuarioDTO): Observable<Number|any> => {
    let promesa = new Promise((resolve, reject) => {
      this.http.post(this.urlAuth, auth, { headers: this.headers })
        .subscribe(value => {
          resolve(value)
        },error => {
          this.handleError(error);reject(null)
        });
    });
    return from(promesa);
  };

  public register = (auth: Auth): Observable<{}|Boolean> => {
    let promesa = new Promise((resolve, reject) => {
      this.http.post(this.urlAuth, auth, { headers: this.headers })
        .subscribe(value => {resolve(true)},error => {this.handleError(error);reject(false)});
    });
    return from(promesa);
  };

  public logout = (): Observable<{}|Boolean> => {
    if (this.locStorage.isSession()) {
      this.options = {headers:this.headers};
      let promesa = new Promise((resolve, reject) => {
        this.http.delete(this.urlAuth, this.options)
          .subscribe(value => {
            this.locStorage.cleanSession();
            resolve(true)},error => {this.handleError(error);reject(false)});
      });
      return from(promesa);
    }
  };

  isLoggedIn() {
    return this.loggedIn;
  }

  private handleError(error: Response | any): Observable<Boolean> {
    //alert("Usuario o Contraseña Innnnncorrecto")
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      // @ts-ignore
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;

    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert("Usuario o Contraseña Erroneos.");
    return Observable.throw(false);
    //return Observable.throw(errMsg);
  }
}
