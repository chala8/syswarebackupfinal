import {MatDialog, MatTableDataSource} from '@angular/material';
import { DetalleCreditosComponent } from '../detalle-creditos/detalle-creditos.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { LocalStorage } from 'src/app/services/localStorage';
import { ClientData } from '../../models/clientData';
import { Urlbase } from '../../../../../../../../app/shared/urls';
import * as jQuery from 'jquery';
import { ThirdselectComponent } from '../../thirdselect/thirdselect.component';
import { GenerateThirdComponent2Component } from '../generate-third-component2/generate-third-component2.component';
let $: any = jQuery;
import { formatDate } from "@angular/common";

@Component({
  selector: 'app-creditos',
  templateUrl: './creditos.component.html',
  styleUrls: ['./creditos.component.css']
})
export class CreditosComponent implements OnInit {

  constructor(public dialog: MatDialog,
              private httpClient: HttpClient,
              private locStorage: LocalStorage) {
                this.headers = new HttpHeaders({
                  'Content-Type':  'application/json',
                  'Authorization':  this.locStorage.getTokenValue(),
                });
              }

  dataSource = [
  ];
  dataSource2 = [
    {position: 1, name: 'Pepe', weight: 150000, symbol: '05/02/2022'},
    {position: 2, name: 'Pepo', weight: 150000, symbol: '05/02/2022'},
    {position: 3, name: 'Pepa', weight: 150000, symbol: '05/02/2022'},
    {position: 4, name: 'Pepu', weight: 150000, symbol: '05/02/2022'},
    {position: 5, name: 'Pepi', weight: 150000, symbol: '05/02/2022'},
    {position: 6, name: 'Pipo', weight: 150000, symbol: '05/02/2022'},
  ];
  displayedColumns: string[] = ['fecha_CREACION', 'id_THIRD_CLIENT', 'id_THIRD_CODEUDOR', 'monto', 'num_CUOTAS', 'others'];
  displayedColumns2: string[] = ['position', 'others'];
  panel1 = true;
  panel2 = false;
  panel3 = false;
  panel4 = false;
  panel5 = false;
  ccClient:String=""
  ccCodeudor:String=""
  fileToUpload:any;
  fechacreacion;
  valor;
  numcuotas;
  interes=0;
  interesmora=0;
  periodicidad = "1";
  cuotainicial = 0;
  idthirdcliente;
  idthirdprov;
  idthirdcodeudor;
  tab=0;
  cedulaCred = "";
  public id_person;
  public cliente = "Ocasional";
  public clientData = new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ',null);
  public id_person_codeudor;
  public codeudor = "Ocasional";
  public codeudorData = new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ',null);
  private readonly headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  ngOnInit(): void {
  }

  setTab(value){
    this.tab = value
  }


  crearCredito(){
    this.httpClient.get(Urlbase.facturacion + "/billing/crear_credito?fechacreacion="+formatDate(this.fechacreacion,"dd-MM-yyyy","en-US")+"&valor="+this.valor+"&numcuotas="+this.numcuotas+"&interes="+this.interes+"&interesmora="+this.interesmora+"&periodicidad="+this.periodicidad+"&cuotainicial="+this.cuotainicial+"&idthirdcliente="+this.clientData.id_third+"&idthirdprov="+802+"&idthirdcodeudor="+this.codeudorData.id_third).subscribe(response=>{
      this.tab = 1;
      this.buscarCredito2(Number(this.clientData.document_number));
    })
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  next(now){
    if(now==1) {
      this.panel1 = false
      this.panel2 = true
    }
    if(now==2) {
      this.panel2 = false
      this.panel3 = true
    }
    if(now==3) {
      this.panel3 = false
      this.panel4 = true
    }
    if(now==4) {
      this.panel4 = false
      this.panel5 = true
    }
  }

  back(now){
    if(now==2) {
      this.panel1 = true
      this.panel2 = false
    }
    if(now==3) {
      this.panel2 = true
      this.panel3 = false
    }
    if(now==4) {
      this.panel3 = true
      this.panel4 = false
    }
    if(now==5) {
      this.panel4 = true
      this.panel5 = false
    }
  }

  verDetalle(element,value){
    const dialogRef = this.dialog.open(DetalleCreditosComponent, {
      width: '60vw',
      height: '80vh',
      data: {element: element,
             value: value},
    });
  }

  searchCodeudor2(){
    console.log("THIS ARE HEADERS",this.headers);
    const identificacionCliente = this.ccCodeudor;
    let aux;
    if(identificacionCliente.length>4){
    this.httpClient.get<any[]>(Urlbase.tercero + '/persons/search?doc_person='+String(identificacionCliente),{ headers: this.headers }).subscribe(data =>{
      console.log(data);
      if (data.length == 0){
        this.openDialogCodeudor2();
        // this.searchClient(event);
      }else{
        const dialogRef = this.dialog.open(ThirdselectComponent, {
          width: '60vw',
          height: '80vh',

          data: { thirdList: data }
        });

        dialogRef.afterClosed().subscribe(result => {
          if(result){

            aux = this.locStorage.getPersonClient();
            console.log("THIS THE AUX I NEED:", aux);
            this.codeudor = aux.fullname;
            this.codeudorData.is_natural_person = true;
            this.codeudorData.fullname = aux.fullname;
            this.codeudorData.document_type = aux.document_TYPE;
            this.codeudorData.document_number = aux.document_NUMBER;
            this.codeudorData.id_third = aux.id_PERSON;
            this.codeudorData.address = aux.address;
            this.codeudorData.email = aux.city;
            this.codeudorData.phone = aux.phone;
            this.id_person_codeudor = aux.id_PERSON;
            console.log("THIS IS THE CLIENT",this.codeudorData)
          }
        });
      }
    });
  }
  else{
    this.showNotification('top', 'center', 3, "<h3>El elemento de busqueda debe ser de longitud mayor a 4.</h3> ", 'danger');
  }
}


buscarCredito(){
  this.httpClient.get(Urlbase.facturacion + "/billing/get_credito?cedula="+this.cedulaCred).subscribe(response=>{
    console.log(response)
    //@ts-ignore
    this.dataSource = response
  })
}

buscarCredito2(cedula){
  this.httpClient.get(Urlbase.facturacion + "/billing/get_credito?cedula="+cedula).subscribe(response=>{
    console.log(response)
    //@ts-ignore
    this.dataSource = response
    this.verDetalle(this.dataSource[0],1)
  })
}

openDialogCodeudor2(): void {
  const dialogRef = this.dialog.open(GenerateThirdComponent2Component, {
    width: '60vw',
    data: {}
  });

  dialogRef.afterClosed().subscribe(result => {
    if (result) {
      // CPrint('CREATE CLIENT SUCCESS');
      // CPrint(result);s

      let isNaturalPerson= result.data.hasOwnProperty('profile');
      let dataPerson= isNaturalPerson?result.data.profile:result.data;
      this.codeudorData.is_natural_person = isNaturalPerson;
      this.codeudorData.fullname= dataPerson.info.fullname;
      this.codeudorData.document_type = dataPerson.info.id_document_type;
      this.codeudorData.document_number = dataPerson.info.document_number;
      this.codeudorData.address = dataPerson.directory.address;
      this.codeudorData.phone = dataPerson.directory.phones[0].phone;
      this.codeudorData.email = dataPerson.directory.hasOwnProperty('mails')?dataPerson.directory.mails[0].mail:'N/A';
      this.codeudor = dataPerson.info.fullname;

    }

  });
}

  searchClient2(){
    console.log("THIS ARE HEADERS",this.headers);
    const identificacionCliente = this.ccClient;
    let aux;
    if(identificacionCliente.length>4){
    this.httpClient.get<any[]>(Urlbase.tercero + '/persons/search?doc_person='+String(identificacionCliente),{ headers: this.headers }).subscribe(data =>{
      console.log(data);
      if (data.length == 0){
        this.openDialogClient2();
        // this.searchClient(event);
      }else{
        const dialogRef = this.dialog.open(ThirdselectComponent, {
          width: '60vw',
          height: '80vh',

          data: { thirdList: data }
        });

        dialogRef.afterClosed().subscribe(result => {
          if(result){

            aux = this.locStorage.getPersonClient();
            console.log("THIS THE AUX I NEED:", aux);
            this.cliente = aux.fullname;
            this.clientData.is_natural_person = true;
            this.clientData.fullname = aux.fullname;
            this.clientData.document_type = aux.document_TYPE;
            this.clientData.document_number = aux.document_NUMBER;
            this.clientData.id_third = aux.id_PERSON;
            this.clientData.address = aux.address;
            this.clientData.email = aux.city;
            this.clientData.phone = aux.phone;
            this.id_person = aux.id_PERSON;
            console.log("THIS IS THE CLIENT",this.clientData)
          }
        });
      }
    });
  }
  else{
    this.showNotification('top', 'center', 3, "<h3>El elemento de busqueda debe ser de longitud mayor a 4.</h3> ", 'danger');
  }
}

openDialogClient2(): void {
  const dialogRef = this.dialog.open(GenerateThirdComponent2Component, {
    width: '60vw',
    data: {}
  });

  dialogRef.afterClosed().subscribe(result => {
    if (result) {
      // CPrint('CREATE CLIENT SUCCESS');
      // CPrint(result);s
      let isNaturalPerson= result.data.hasOwnProperty('profile');
      let dataPerson= isNaturalPerson?result.data.profile:result.data;
      this.clientData.is_natural_person = isNaturalPerson;
      this.clientData.fullname= dataPerson.info.fullname;
      this.clientData.document_type = dataPerson.info.id_document_type;
      this.clientData.document_number = dataPerson.info.document_number;
      this.clientData.address = dataPerson.directory.address;
      this.clientData.phone = dataPerson.directory.phones[0].phone;
      this.clientData.email = dataPerson.directory.hasOwnProperty('mails')?dataPerson.directory.mails[0].mail:'N/A';
      this.cliente = dataPerson.info.fullname;

    }

  });
}


showNotification(from, align, id_type?, msn?, typeStr?) {
  const type = ['', 'info', 'success', 'warning', 'danger'];
  const color = Math.floor((Math.random() * 4) + 1);

  $.notify({
    icon: "notifications",
    message: msn ? msn : "<b>Noficación automatica </b>"

  }, {
    type: typeStr ? typeStr : type[id_type ? id_type : 2],
    timer: 200,
    placement: {
      from: from,
      align: align
    }
  });
}

}
