import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Urlbase } from 'src/app/shared/urls';
import { createCallChain } from 'typescript';

@Component({
  selector: 'app-modal-detalles-opciones',
  templateUrl: './modal-detalles-opciones.component.html',
  styleUrls: ['./modal-detalles-opciones.component.css']
})
export class ModalDetallesOpcionesComponent implements OnInit {

  constructor(private http: HttpClient, public dialogRef: MatDialogRef < ModalDetallesOpcionesComponent > , public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) { }

  detailList:any;
  ngOnInit(): void {
    this.http.get(Urlbase.tienda+"/store/getNotesForDetailsWithOptions?idbill="+this.data.idBill).subscribe(response => {
      this.detailList = response;
      console.log(response)
    })
  }


  splitNotes(notes){
    try{
      return notes.split("=");
    }catch(e){
      return [];
    }
  }

  closeDialog(){

    this.dialogRef.close();

  }

}
