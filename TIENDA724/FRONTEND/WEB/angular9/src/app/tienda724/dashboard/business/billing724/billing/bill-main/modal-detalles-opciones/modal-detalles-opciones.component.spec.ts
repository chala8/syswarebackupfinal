import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalDetallesOpcionesComponent } from './modal-detalles-opciones.component';

describe('ModalDetallesOpcionesComponent', () => {
  let component: ModalDetallesOpcionesComponent;
  let fixture: ComponentFixture<ModalDetallesOpcionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalDetallesOpcionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDetallesOpcionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
