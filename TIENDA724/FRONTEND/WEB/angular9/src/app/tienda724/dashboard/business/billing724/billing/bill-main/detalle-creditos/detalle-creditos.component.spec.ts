import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleCreditosComponent } from './detalle-creditos.component';

describe('DetalleCreditosComponent', () => {
  let component: DetalleCreditosComponent;
  let fixture: ComponentFixture<DetalleCreditosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleCreditosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleCreditosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
