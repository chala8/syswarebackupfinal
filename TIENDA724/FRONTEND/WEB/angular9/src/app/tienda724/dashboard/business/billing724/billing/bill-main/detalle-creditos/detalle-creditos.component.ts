import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { LocalStorage } from 'src/app/services/localStorage';
import { ClientData } from '../../models/clientData';
import { Urlbase } from '../../../../../../../../app/shared/urls';
import * as jQuery from 'jquery';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { formatDate } from '@angular/common';
let $: any = jQuery;

@Component({
  selector: 'app-detalle-creditos',
  templateUrl: './detalle-creditos.component.html',
  styleUrls: ['./detalle-creditos.component.css']
})
export class DetalleCreditosComponent implements OnInit {

  fileBase64String = null;
  fileExtension = null;
  urlUploaded = "Sin Archivo";
  dataSource = [];
  dataSource2 = [];
  dataSource3 = [];
  dataSource4 = [];
  tab = 0;
  ccClient:String = "";
  ccCodeudor:String = "";
  displayedColumns: string[] = ['nombre', 'others'];
  displayedColumns2: string[] = ['fecha',  'texto'];
  displayedColumns3: string[] = ['estado_CUOTA',  'fecha_VENCIMIENTO',  'valor',  'others'];
  displayedColumns4: string[] = ['paymentmethod',  'fecha_ABONO',  'valor_ABONO'];
  fileName = "";
  filePseudonimn = "";
  abonos = false;
  newAbonos = false;
  valorCuota = "";
  valorAbono = "";
  fechaCuota = "";
  fechaAbono = "";
  paymentMethod = "1";
  selectedDocument = "-1";
  cuota;
  facturaSeleccionada = "Sin Factura";
  listBills=[];
  billToAsociate = -1;
  numDoc = "";
  public id_person;
  public cliente = "Ocasional";
  public clientData = new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ',null);
  public id_person_codeudor;
  public codeudor = "Ocasional";
  public codeudorData = new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ',null);
  private readonly headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private httpClient: HttpClient,
              private locStorage: LocalStorage,
              public dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.headers = new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization':  this.locStorage.getTokenValue(),
    });

  }


  ngOnInit(): void {
    console.log("this.data")
    console.log(this.data)
    if(this.data.value==1){
      this.tab=2;
    }
    this.httpClient.get(Urlbase.facturacion + "/billing/getDocumentoCredito?id_credito="+this.data.element.id_CREDITO).subscribe(response=>{
      //@ts-ignore
      this.dataSource = response
    })

    this.httpClient.get(Urlbase.facturacion + "/billing/getLogCredito?id_credito="+this.data.element.id_CREDITO).subscribe(response=>{
      //@ts-ignore
      this.dataSource2 = response
    })

    this.httpClient.get(Urlbase.facturacion + "/billing/getCuotas?ID_CREDITO="+this.data.element.id_CREDITO).subscribe(response=>{
      //@ts-ignore
      this.dataSource3 = response
    })


  }


  buscarFacturas(){
    this.httpClient.get(Urlbase.facturacion + "/billing/Bills4Credit?idthird="+this.locStorage.getToken().id_third_father+"&numdoc="+this.numDoc).subscribe(response=>{
      console.log(response)
      //@ts-ignore
      this.listBills = response
    })
  }


  asociarFactura(){
    this.httpClient.get(Urlbase.facturacion + "/billing/asociar_factura_credito?idcredito="+this.data.element.id_CREDITO+"&idbill="+this.billToAsociate).subscribe(response=>{
      console.log(response)
      this.data.element.id_BILL_FACTURA = this.billToAsociate
      this.httpClient.get(Urlbase.facturacion + "/billing/getDocumentoCredito?id_credito="+this.data.element.id_CREDITO).subscribe(response=>{
        //@ts-ignore
        this.dataSource = response
      })
    })
  }


  openFile(Element){
    window.open(Element.ruta, "_blank");
  }



  getAbonos(cuota){
    this.abonos = true;
    this.newAbonos = false;
    this.httpClient.get(Urlbase.facturacion + "/billing/getAbono?ID_CREDITO="+this.data.element.id_CREDITO+"&ID_CUOTA="+cuota.id_CUOTA).subscribe(response=>{
      console.log(response)
      this.abonos=true;
      //@ts-ignore
      this.dataSource4 = response
    })
  }



  createAbono(element){
    console.log(element)
    this.abonos = false;
    this.newAbonos = true;
    let credito = this.data.element.id_CREDITO;
    this.cuota = element;

  }



  postCuota(){
    //@ts-ignore
    this.httpClient.get(Urlbase.facturacion + "/billing/crear_cuota_credito?idcredito="+this.data.element.id_CREDITO+"&fechavencimiento="+formatDate(this.fechaCuota,"dd-MM-yyyy","en-US")+"&valorcuota="+this.valorCuota,{}).subscribe(response=>{
      console.log(response)
      this.httpClient.get(Urlbase.facturacion + "/billing/getCuotas?ID_CREDITO="+this.data.element.id_CREDITO).subscribe(response=>{
        console.log(response)
        //@ts-ignore
        this.dataSource3 = response
      })
    })
  }



  postAbono(){
    this.httpClient.get(Urlbase.facturacion + "/billing/crear_abono_cuota?idcuota="+this.cuota.id_CUOTA+"&fechaabono="+formatDate(this.fechaAbono,"dd-MM-yyyy","en-US")+"&valorabono="+this.valorAbono+"&idcredito="+this.data.element.id_CREDITO+"&idpaymentmethod="+this.paymentMethod+"&IDCREDITODOCUMENTO="+this.selectedDocument
    ,{}).subscribe(response=>{
      //@ts-ignore
      this.httpClient.get(Urlbase.facturacion + "/billing/getAbono?ID_CREDITO="+this.data.element.id_CREDITO+"&ID_CUOTA="+this.cuota.id_CUOTA).subscribe(response=>{
        this.abonos = true;
        this.newAbonos = false;
        this.cuota = "";
        //@ts-ignore
        this.dataSource4 = response
      })
    })
  }




  setFile(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
        console.log(reader.result);
        console.log("EXTENSION: ", event.target.files[0].name.split(".").pop());
        this.fileBase64String = reader.result;
        this.fileName =event.target.files[0].name.split(".")[0]
        this.fileExtension = event.target.files[0].name.split(".").pop();
        this.urlUploaded = event.target.files[0].name;
    };
  }


  generateRandomNumber() {
    return Math.floor(100000 + Math.random() * 900000)
  }


  uploadFile(){
    if(this.fileBase64String!=null){
      let fileName = this.data.element.id_CREDITO + "_" + this.generateRandomNumber()
      console.log({file: this.fileBase64String, fileName: fileName,format: this.fileExtension,filePseudonim: this.filePseudonimn})
      this.httpClient.post(Urlbase.facturacion+"/billing/fileUpload",{file: this.fileBase64String, fileName: fileName,format: this.fileExtension,filePseudonimn: this.filePseudonimn},{responseType:'text'}).subscribe(response =>{
        console.log("answer", response);
        this.httpClient.get(Urlbase.facturacion+"/billing/CREAR_CREDITO_DOCUMENTO?NOMBREDOC="+this.filePseudonimn+"&RUTADOC=https://tienda724.com/docscredito/"+fileName+"."+this.fileExtension+"&TIPODOC=0&IDCREDITO="+this.data.element.id_CREDITO).subscribe(response =>{
          this.showNotification('top', 'center', 3, "<h3>Producto Creado con exito.</h3> ", 'info');
          this.httpClient.get(Urlbase.facturacion + "/billing/getDocumentoCredito?id_credito="+this.data.element.id_CREDITO).subscribe(response=>{
            console.log(response)
            //@ts-ignore
            this.dataSource = response
          })
        })
      });
    }else{
      this.showNotification('top', 'center', 3, "<h3>Producto Creado con exito.</h3> ", 'info');
    }
  }



  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];
    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
      type: typeStr ? typeStr : type[id_type ? id_type : 2],
      timer: 200,
      placement: {
        from: from,
        align: align
      }
    });
  }


  aprobarCredito(){
    this.httpClient.get(Urlbase.facturacion + "/billing/aprobar_credito?IDCREDITO="+this.data.element.id_CREDITO+"&NOTE="+"Se aprueba Credito").subscribe(response=>{
      console.log(response)
      this.httpClient.get(Urlbase.facturacion + "/billing/getCreditoById?ID_CREDITO="+this.data.element.id_CREDITO).subscribe(response2=>{
        this.data.element = response2[0]
      })
    })
  }

  desembolsarCredito(){
    this.httpClient.get(Urlbase.facturacion + "/billing/desembolsar_credito?IDCREDITO="+this.data.element.id_CREDITO+"&NOTE="+"Se aprueba Credito").subscribe(response=>{
      console.log(response)
      this.httpClient.get(Urlbase.facturacion + "/billing/getCreditoById?ID_CREDITO="+this.data.element.id_CREDITO).subscribe(response2=>{
        this.data.element = response2[0]
      })
    })
  }

}

