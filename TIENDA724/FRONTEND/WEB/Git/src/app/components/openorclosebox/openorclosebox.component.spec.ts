import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenorcloseboxComponent } from './openorclosebox.component';

describe('OpenorcloseboxComponent', () => {
  let component: OpenorcloseboxComponent;
  let fixture: ComponentFixture<OpenorcloseboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenorcloseboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenorcloseboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
