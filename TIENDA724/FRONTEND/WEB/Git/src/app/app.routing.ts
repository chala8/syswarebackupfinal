import { Routes } from '@angular/router';

/***************************************************
 * Llamar el Componente que se muestra por defecto *
 ***************************************************/
//import { IndexComponent } from './welcome/index/index.component'

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/
import { WelcomeRouting }  from './welcome/welcome.routing'
import { AuthenticationRouting }  from './authentication/authentication.routing'
import { Tienda724Routing }  from './tienda724/tienda724.routing'


export const AppRouting: Routes = [
  // { path: '',          redirectTo: '', pathMatch: 'full' }
 
  ...AuthenticationRouting,
  ...Tienda724Routing,
  { path: '*', redirectTo:'/',pathMatch:'full'}
];
