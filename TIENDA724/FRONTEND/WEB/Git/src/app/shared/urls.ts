export const Urlbase = [
    //auth [0]
    'http://'+'tienda724.com'+':8445/v1',
    // tercero [1]
    'http://'+'tienda724.com'+':8446/v1',
    // tienda [2]
    'http://'+'tienda724.com'+':8447/v1',
    // billing/facturación [3]
    'http://'+'tienda724.com'+':8448/v1',
    // order [4]
    'http://'+'tienda724.com'+':8450/v1',
    // cierreCaja [5]
    'http://'+'tienda724.com'+':8451/v1',
    // remisiones [6],
    'http://'+'tienda724.com'+'/remisiones',
    // facturas [7],
    'http://'+'tienda724.com'+'/facturas',
];
