import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';

/*
*************************************************
*     Principal Routing
*************************************************
*/

/*
*************************************************
*     Principal component
*************************************************
*/
// This Module's Components
import { Tienda724Component } from './tienda724.component';

import { UserProfileComponent } from '../user-profile/user-profile.component';
import { TableListComponent } from '../table-list/table-list.component';
import { TypographyComponent } from '../typography/typography.component';
import { IconsComponent } from '../icons/icons.component';
import { MapsComponent } from '../maps/maps.component';
import { NotificationsComponent } from '../notifications/notifications.component';
import { UpgradeComponent } from '../upgrade/upgrade.component';

/*
************************************************
*     modules of  your app
*************************************************
*/
import { MaterialModule } from '../app.material';
import { ComponentsModule } from '../components/components.module';
import { BusinessModule } from './dashboard/business/business.module';
import { MyModule } from './my/my.module';
/*
*************************************************
*     services of  your app
*************************************************
*/

import { UserThirdService } from './dashboard/business/thirds724/user-third/user-third.service'
import { ThirdService } from './dashboard/business/thirds724/third/third.service'
import { PersonService } from './dashboard/business/thirds724/person/person.service';
/*
*************************************************
*     models of  your app
*************************************************
*/
/*
*************************************************
*     constant of  your app
*************************************************
*/
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    BrowserModule,
    FormsModule, ReactiveFormsModule,
    HttpModule,
    ComponentsModule,
    BusinessModule,
    MyModule

  ],
  declarations: [
      Tienda724Component,
      UserProfileComponent,
      TableListComponent,
      TypographyComponent,
      IconsComponent,
      MapsComponent,
      NotificationsComponent,
      UpgradeComponent,
  ],
  providers:[UserThirdService,ThirdService,PersonService],
  exports: [
      Tienda724Component,
  ]
})
export class Tienda724Module {
 }
