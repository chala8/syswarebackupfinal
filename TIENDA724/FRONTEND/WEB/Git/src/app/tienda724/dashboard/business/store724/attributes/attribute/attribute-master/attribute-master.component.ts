import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import * as _ from 'lodash';
/*
*    Material modules for component
*/
import {MatTabChangeEvent, VERSION} from '@angular/material';

/*
*     others component
*/
/*
*     models for  your component
*/
import { Token } from '../../../../../../../shared/token'
import { AttributeList } from '../../models/attributeList'
import { CommonStateStoreDTO } from '../../../commons/CommonStateStoreDTO'
import { CommonStoreDTO } from '../../../commons/CommonStoreDTO'

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../../shared/localStorage'
import { AttributeService } from '../../attribute.service';
/*
*     constant of  your component
*/

var categoryList:AttributeList[];
var categoryListGlobal:AttributeList[];

@Component({
  selector: 'app-attribute-master',
  templateUrl: './attribute-master.component.html',
  styleUrls: ['./attribute-master.component.scss']
})
export class AttributeMasterComponent implements OnInit {
  isMyProduct=true;
  token:Token;
  ID_THIRD_TYPE: number;
  STATE=1
  CURRENT_ID_THIRD = 0;
  
  thirdAux:AttributeList[];






    displayedColumns = ['position', 'name', 'weight', 'symbol', 'direccion', 'opciones'];
    displayedColumns_General = ['position', 'name', 'weight', 'symbol', 'direccion'];
    dataSource:ProductDataSource

  constructor(public locStorage: LocalStorage,
    private _router: Router,
    public productService: AttributeService) { }

  ngOnInit() {
    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD=this.token.id_third;
      this.ID_THIRD_TYPE = 23;

      if (this.CURRENT_ID_THIRD!==null && this.CURRENT_ID_THIRD>0){

        this.getProductList()
        

      } 
      

    }
  }

  getProductList(state?:number,id_category?:number,id_category_father?:number,id_third?:number){
    
        this.productService.getAttributeList()
        .subscribe((data: AttributeList[]) => categoryList = data,
        error => console.log(error),
        () => {
    
          this.dataSource = new ProductDataSource();
        });
    
      }
    
      Login() {
        let link = ['/auth'];
        this._router.navigate(link);
      }
    
      editCategory(category:AttributeList){ 
        this._router.navigate(['/dashboard/business/category/edit',1] ); 
     
     
     
      } 
    
      addCategory(category:AttributeList){ 
        
        this._router.navigate(['/dashboard/business/category/new'],{queryParams:{father:1}} ); 
     
      } 
    
      deleteCategory(id_product) {
        
            this.productService.DeleteAttributeList(id_product)
              .subscribe(
              result => {
                
                if (result === true) {
                  //categoryList = _.filter(categoryList, function (f) { return f.id_product !== id_product; });
                  this.dataSource = new ProductDataSource();
                
                  alert("Eliminado correctamente");
        
                  return;
                } else {
                  //this.openDialog();
                  return;
                }
              })
        
          }

}
export class ProductDataSource extends DataSource<any> {
  /** Connect function called by the table to retrieve one stream containing the data to render. */

  connect(): Observable<AttributeList[]> {

    return Observable.of(categoryList);
  }

  disconnect() {}
}
