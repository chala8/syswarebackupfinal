import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNotesCloseBoxComponent } from './add-notes-close-box.component';

describe('AddNotesCloseBoxComponent', () => {
  let component: AddNotesCloseBoxComponent;
  let fixture: ComponentFixture<AddNotesCloseBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNotesCloseBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNotesCloseBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
