import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductThirdDataComponent } from './product-third-data.component';

describe('ProductThirdDataComponent', () => {
  let component: ProductThirdDataComponent;
  let fixture: ComponentFixture<ProductThirdDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductThirdDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductThirdDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
