import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotesOnOrderComponent } from './notes-on-order.component';

describe('NotesOnOrderComponent', () => {
  let component: NotesOnOrderComponent;
  let fixture: ComponentFixture<NotesOnOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotesOnOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesOnOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
