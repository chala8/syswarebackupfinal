import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '../../../../../../../shared/localStorage';
import { DatePipe } from '@angular/common';
import { BillingService } from '../../billing.service';
import { Http, Headers } from '@angular/http';

@Component({
  selector: 'app-open-box-report',
  templateUrl: './open-box-report.component.html',
  styleUrls: ['./open-box-report.component.scss']
})
export class OpenBoxReportComponent implements OnInit {

  constructor(private categoriesService: BillingService,
              private datePipe: DatePipe,
              private http2: HttpClient,
              public locStorage: LocalStorage) { }
 

  SelectedStore = "";
  SelectedBox = "";
  Boxes;
  Stores;
  storesEmpty = true;
  efectivo = 0;
  tarjCred = 0;
  tarjDeb = 0;
  devoluciones=0;
  devolucionesCred = 0;
  devolucionesDebt = 0;
  myDetails;
  base=0;
  credits=0;
  debts=0;
  balances=0;
  cajero = "";
  fecha_apertura = "";
  caja = ""
  cajeroS = "";
  fecha_aperturaS = "";
  cajaS = ""


  ngOnInit() {
    this.getStores()
  }

  getStores() {
    this.categoriesService.getStoresByThird(this.locStorage.getToken().id_third).subscribe(data => {
        console.log(data);
        this.Stores = data
        this.SelectedStore = data[0].id_STORE
        this.storesEmpty = false;
        this.http2.get("http://tienda724.com:8451/v1/close/openBox?id_store="+data[0].id_STORE).subscribe(boxes => {
          this.Boxes = boxes;
          this.SelectedBox = boxes[0].id_CIERRE_CAJA;
          this.cajero = boxes[0].cajero;
          this.caja = boxes[0].caja;
          this.fecha_apertura = boxes[0].fecha_APERTURA;
        })
      })

  }

  getBoxes(){
    this.storesEmpty = false;
    this.http2.get("http://tienda724.com:8451/v1/close/openBox?id_store="+this.SelectedStore).subscribe(boxes => {
      this.Boxes = boxes;
      this.SelectedBox = boxes[0].id_CIERRE_CAJA;
      this.cajero = boxes[0].cajero;
      this.caja = boxes[0].caja;
      this.fecha_apertura = boxes[0].fecha_APERTURA;
    })
  }

  
  getCreds(list){

    for(let i =0; i <list.length; i++){
      if(list[i].naturaleza == "C"){
        this.credits+=list[i].valor;
      }
    }
  }

  getDevoluciones(list){

    for(let i =0; i <list.length; i++){
      if(list[i].notes.includes("Efectivo - Devolución factura")){
        this.devoluciones+=list[i].valor;
      }
      if(list[i].notes.includes("TD - Devolución factura")){
        this.devolucionesDebt+=list[i].valor;
      }
      if(list[i].notes.includes("TC - Devolución factura")){
        this.devolucionesCred+=list[i].valor;
      }
    }

  }

  getDebts(list){

    for(let i =0; i <list.length; i++){
      if(list[i].naturaleza == "D"){
        this.debts+=list[i].valor;
      }
    }

  }

  roundDecimals(number){
    return Math.round(number * 100) / 100
  }


  getData(){
    this.categoriesService.getBoxMaster(this.SelectedBox).subscribe(res4=>{
      this.balances = res4.balance;
      console.log(res4);
      this.categoriesService.getDetailsById(this.SelectedBox).subscribe((res2)=>{
        console.log(res2,"los detallitos")
        this.myDetails = res2;
        this.base = res2[0].valor;
        this.cajaS  = this.caja;
        this.cajeroS = this.cajero;
        this.fecha_aperturaS = this.fecha_apertura;
        this.getCreds(res2);
        this.getDebts(res2);
        this.getDevoluciones(res2);
        this.categoriesService.getDetailsById(this.SelectedBox).subscribe((res2)=>{
          console.log(res2,"los detallitos")
          this.myDetails = res2;
          this.http2.get("http://tienda724.com:8451/v1/close/ventasPayment?id_cierre_caja="+this.SelectedBox+"&id_payment=1").subscribe(data => {
            //@ts-ignore
            this.efectivo = data;
          })
      
          this.http2.get("http://tienda724.com:8451/v1/close/ventasPayment?id_cierre_caja="+this.SelectedBox+"&id_payment=3").subscribe(data => {
            //@ts-ignore
            this.tarjCred = data;
          })
      
      
          this.http2.get("http://tienda724.com:8451/v1/close/ventasPayment?id_cierre_caja="+this.SelectedBox+"&id_payment=2").subscribe(data => {
            //@ts-ignore
            this.tarjDeb = data;
          })
      
        })
      })
  
    })

    

  }
}
