import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import 'hammerjs';

/*
************************************************
*    Material modules for app
*************************************************
*/

import { MaterialModule } from '../../../../../app.material';
/*
************************************************
*     principal component
*************************************************
*/
import { DocumentFilterComponent } from './document-filter/document-filter.component';
/*
************************************************
*     modules of  your app
*************************************************
*/

/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { DocumentTypeService } from './document-type.service';
/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/




@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers:[DocumentTypeService],
  declarations: [DocumentFilterComponent],
  exports:[DocumentFilterComponent]
})
export class DocumentTypeModule { }
