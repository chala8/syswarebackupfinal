import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { LocalStorage } from 'app/shared/localStorage';
declare var $: any;
@Component({
  selector: 'app-editar-costo',
  templateUrl: './editar-costo.component.html',
  styleUrls: ['./editar-costo.component.scss']
})
export class EditarCostoComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,private http2: HttpClient,public locStorage: LocalStorage,public dialogRef: MatDialogRef<EditarCostoComponent>) { }
  psName = "";
  psOwn = "";
  ps;
  newCost = 0;
  costoActual = 0;
  ngOnInit() {
    console.log(this.data)
    this.psName = this.data.psName;
    this.psOwn = this.data.psOwn;
    this.http2.get("http://tienda724.com:8447/v1/store/psid?&code="+this.psOwn+"&id_store="+this.locStorage.getIdStore()).subscribe(response => {
      this.ps = response;  
      console.log(this.ps);
      this.http2.get("http://tienda724.com:8447/v1/store/stPriceByPs?id_ps="+response).subscribe(resp => {
        //@ts-ignore
        this.newCost = resp;  
        //@ts-ignore
        this.costoActual = resp;        
      });
    }); 

  }

  updateStandardPrice(){
    try{
      this.http2.put("http://tienda724.com:8447/v1/store/standardprice?standard_price="+this.newCost+"&id_ps="+this.ps,null).subscribe(response => {
        this.showNotification('top','center',3,"<h3 class = 'text-center'>Se actualizo el precio de compra Exitosamente<h3>",'info');
        this.dialogRef.close();
      })
    }catch(Exception){
      this.showNotification('top','center',3,"<h3 class = 'text-center'>No se pudo actualizar el precio de compra.<h3>",'Danger');
    }

  }

  
  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: typeStr ? typeStr : type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }


}


export interface DialogData {
  psName: any;
  psOwn: any;
}
