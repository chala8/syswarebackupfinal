import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InBillComponent } from './in-bill.component';

describe('InBillComponent', () => {
  let component: InBillComponent;
  let fixture: ComponentFixture<InBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
