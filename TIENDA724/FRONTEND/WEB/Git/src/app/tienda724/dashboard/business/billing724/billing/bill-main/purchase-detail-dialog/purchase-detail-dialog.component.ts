import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-purchase-detail-dialog',
  templateUrl: './purchase-detail-dialog.component.html',
  styleUrls: ['./purchase-detail-dialog.component.scss']
})
export class PurchaseDetailDialogComponent {
  noProduct=false;
  constructor(
    public dialogRef: MatDialogRef<PurchaseDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { 
      setTimeout(() => {
        document.getElementById('defaultAutofocus').focus();
      },1000);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  cancel(args): void {
    if(args=='noProduct'){
      this.noProduct= true;
    }else{
      this.dialogRef.close();
    }    
  }

  returnData() {
    if(this.data.purchaseTaxIncluded){
      this.data.purchaseValue = this.data.purchaseValue/(1.0 + this.data.tax);
      this.data.purchaseTaxIncluded= false;
    }

    if(this.data.saleTaxIncluded){
      this.data.saleValue = this.data.saleValue/(1.0 + this.data.tax);
      this.data.saleTaxIncluded= false;
    }
    this.dialogRef.close(this.data);
  }
}

export interface DialogData {
  quantity: number;
  tax: number;
  id_product_third: number;
  tax_product: number;
  state: number;
  description: string;
  code: string;
  id_inventory_detail: number;
  purchaseTaxIncluded: boolean;
  purchaseValue: number;
  saleTaxIncluded: boolean;
  saleValue: number;
  flagNewProduct: boolean;
}