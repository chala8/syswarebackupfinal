import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../../app.material';
/*
************************************************
*     principal component
*************************************************
*/
import { WayPayDataComponent } from './way-pay-data/way-pay-data.component';


/*
************************************************
*     modules of  your app
*************************************************
*/


/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { WayToPayService } from './way-to-pay.service'
/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
  ],
  declarations: [WayPayDataComponent],
  providers:[WayToPayService]
})
export class WayToPayModule { }
