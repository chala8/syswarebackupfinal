import { Component, OnInit, Input } from '@angular/core';
import * as Chartist from 'chartist';
@Component({
  selector: 'app-graphics',
  templateUrl: './graphics.component.html',
  styleUrls: ['./graphics.component.scss']
})
export class GraphicsComponent implements OnInit {
  @Input() idString:string= '#graphics_by_id_'+this.makeid();
  @Input() title:string="Title";
  @Input() color:string="green";
  @Input() description:string="description";
  @Input() xAxis:Array<Object>=[];
  @Input() yAxis:Array<Object>=[];

  constructor() { }
  
  ngOnInit() {
    this.initializeGraphic();
  }

  initializeGraphic(){
    let dataEmailsSubscriptionChart = {
      labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      series: [
        [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895]

      ]
    };
    let optionsEmailsSubscriptionChart = {
        axisX: {
            showGrid: false
        },
        low: 0,
        high: 1000,
        chartPadding: { top: 0, right: 5, bottom: 0, left: 0}
    };
    let responsiveOptions: any[] = [
      ['screen and (max-width: 640px)', {
        seriesBarDistance: 5,
        axisX: {
          labelInterpolationFnc: function (value) {
            return value[0];
          }
        }
      }]
    ];
    let emailsSubscriptionChart = new Chartist.Bar('#testchart', dataEmailsSubscriptionChart, optionsEmailsSubscriptionChart, responsiveOptions);

    this.startAnimationForBarChart(emailsSubscriptionChart);
  }

  startAnimationForBarChart(chart){
    let seq2: any, delays2: any, durations2: any;

    seq2 = 0;
    delays2 = 80;
    durations2 = 500;
    chart.on('draw', (data) => {
      if(data.type === 'bar'){
          seq2++;
          data.element.animate({
            opacity: {
              begin: seq2 * delays2,
              dur: durations2,
              from: 0,
              to: 1,
              easing: 'ease'
            }
          });
      }
    });

    seq2 = 0;
  };

  makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
  }
}
