import { Injectable } from '@angular/core';

import { Http, Headers, Response, URLSearchParams,RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';
import { UserThird } from '../../../../../shared/userThird'
import { Person } from '../../../../../shared/person'


@Injectable()
export class UserThirdService {
  third: UserThird;
  api_uri = Urlbase[1] + '/users';
  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

        this.headers.append('Content-Type', 'application/json');
        this.headers.append( 'Authorization', this.locStorage.getTokenValue());

        let token = localStorage.getItem('currentUser');

        this.options = new RequestOptions({headers:this.headers});
  }

  public getUserThirdList = (userThirdQ:UserThird): Observable<UserThird[]> => {
    let params: URLSearchParams = new URLSearchParams();
    params.set('id_user_third',  userThirdQ.id_user_third?""+userThirdQ.id_user_third:null);
    params.set('uuid', userThirdQ.UUID?""+userThirdQ.UUID:null);
    let myOption: RequestOptions = this.options;
    console.log("id_user_third es ");
    console.log(userThirdQ.id_user_third?""+userThirdQ.id_user_third:null)
    console.log("uuid es ");
    console.log(userThirdQ.UUID?""+userThirdQ.UUID:null)
    console.log("params es ");
    console.log(params)
    myOption.search = params;
    
    return this.http.get(this.api_uri, this.options)
      .map((response: Response) =>  <UserThird[]>response.json())
      .catch(this.handleError);
  
    }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return Observable.throw(errMsg);
  }



}
