import { Component, Inject, OnInit } from '@angular/core';
import { Http, Headers, Response, URLSearchParams,RequestOptions} from '@angular/http';
import { Urlbase } from '../../../../../../../../app/shared/urls';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { ClientData } from '../../models/clientData';
import { BillingService } from '../../billing.service';
import { PersonDialogComponent } from '../person-dialog/person-dialog.component';
import { LocalStorage } from '../../../../../../../../app/shared/localStorage';
import { GenerateThirdComponentComponent } from "../../bill-main/generate-third-component/generate-third-component.component";
import { CurrencyPipe } from '@angular/common';
import { GenerateThirdComponent2Component } from '../generate-third-component2/generate-third-component2.component';
import { ThirdselectComponent } from '../../thirdselect/thirdselect.component';
@Component({
  selector: 'app-transaction-confirm-dialog',
  templateUrl: './transaction-confirm-dialog.component.html',
  styleUrls: ['./transaction-confirm-dialog.component.scss']
})
export class TransactionConfirmDialogComponent implements OnInit{
  ccClient="";
  showedCash = ''
  private options: RequestOptions;
  public cash:number;
  public wayToPay='contado'; 
  public creditBank= '';
  public debitBank= '';
  public observations='';
  public transactionCode = ' ';
  public paymentMethod = 'efectivo';
  public cliente = "Ocasional";
  public id_person;
  private sum = true;
  public clientData = new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ',null);
  directoryList: any;
  api_uri = Urlbase[1];
  personList: any;
  private headers = new Headers();

  constructor(public currencyPipe: CurrencyPipe, private billingService: BillingService, private httpClient: Http, private locStorage: LocalStorage,
    public dialogRef: MatDialogRef<TransactionConfirmDialogComponent>,public dialog: MatDialog 
    ,@Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');
 
    this.options = new RequestOptions({headers:this.headers});

  }
  ngOnInit() {
    console.log("I NEED THIS DATA,",this.data.total)
    this.cash=this.roundnum(this.data.total);
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
  }

  add(number){
    this.cash+=number;
  }
 
  remove(number){
    this.cash-=number;
  }

  setCash(num){
    this.cash=num;
    this.sum=true;
  }

  disableButton(){
    if( this.roundnum(Number(this.data.total)) > this.cash){
      return true
    }else{
      return false
    }
  }
  
  onNoClick(): void {
    this.dialogRef.close();
  }
  
  clickedOn(method){
    this.paymentMethod = method;
  }

  roundnum(num){
    return Math.round(num);
    }
  
  returnData(){
    this.dialogRef.close({
      wayToPay:this.wayToPay,
      cash:this.roundnum(this.cash),
      creditBank:this.creditBank,
      debitBank:this.debitBank, 
      observations:this.observations,
      paymentMethod:this.paymentMethod,
      transactionCode: this.transactionCode,
      clientData : this.clientData,
      id_person : this.id_person,
      cambio : this.roundnum(this.cash - Number(this.data.total))
    });
  } 
  transformAmount(element){
    this.showedCash = this.currencyPipe.transform(this.showedCash, '$');

    element.target.value = this.roundnum(this.showedCash);
  }
  openDialogClient(): void {

  

        const dialogRef = this.dialog.open(GenerateThirdComponentComponent, {
          width: '60vw',
          data: {}
        });
        
  
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // console.log('CREATE CLIENT SUCCESS');
        // console.log(result);
        console.log("THIS IS CLIENT DATA, ",result)
        let isNaturalPerson= result.data.hasOwnProperty('profile')?true:false;
        let dataPerson= isNaturalPerson?result.data.profile:result.data;
        this.clientData.is_natural_person = isNaturalPerson;
        this.clientData.fullname= dataPerson.info.fullname;
        this.clientData.document_type = dataPerson.info.id_document_type;
        this.clientData.document_number = dataPerson.info.document_number;
        this.clientData.address = dataPerson.directory.address;
        this.clientData.phone = dataPerson.directory.phones[0].phone;
        this.clientData.email = dataPerson.directory.hasOwnProperty('mails')?dataPerson.directory.mails[0].mail:'N/A';
      }
    });
  }
    
  searchClientk(event){
    var identificacionCliente = new String(event.target.value);
    var aux;
    this.httpClient.get("http://tienda724.com:8446/v1/persons/search?doc_person="+String(identificacionCliente),{ headers: this.headers }).subscribe((res)=>{
      console.log("THIS IS DOCUMENT DATA: ",JSON.parse(res.text()))
      if (res.text() == "[]"){
        this.openDialogClient();
        // this.searchClient(event);
      }else{ 
        aux = JSON.parse(res.text())[0]; 
        console.log("THIS IS AUX",aux)
        this.cliente = aux.fullname; 
        this.clientData.is_natural_person = true;
        this.clientData.fullname = aux.fullname;
        this.clientData.document_type = aux.document_TYPE;
        this.clientData.document_number = aux.document_NUMBER;
        this.clientData.id_third = aux.id_PERSON;
        this.id_person = aux.id_PERSON;
        this.clientData.address = aux.address; 
        this.clientData.email =  aux.city;
        this.clientData.phone = aux.phone;

        
      }
    
    
    });
    

  }

  
  searchClient(event){
    var identificacionCliente = new String(event.target.value);
    var aux;
    this.httpClient.get('http://tienda724.com:8446/v1/persons/search?doc_person='+String(identificacionCliente),{ headers: this.headers }).subscribe((res)=>{
      console.log(JSON.parse(res.text()))
      if (res.text() == "[]"){
        this.openDialogClient2();
        // this.searchClient(event); 
      }else{
        const dialogRef = this.dialog.open(ThirdselectComponent, {
          width: '60vw',
          height: '80vh',
          
          data: { thirdList: JSON.parse(res.text()) }
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if(result){

            aux = this.locStorage.getPersonClient(); 
            console.log("THIS THE AUX I NEED:", aux);
            this.cliente = aux.fullname; 
            this.clientData.is_natural_person = true;
            this.clientData.fullname = aux.fullname;
            this.clientData.document_type = aux.document_TYPE;
            this.clientData.document_number = aux.document_NUMBER;
            this.clientData.id_third = aux.id_PERSON;
            this.id_person = aux.id_PERSON
            this.clientData.address = aux.address; 
            this.clientData.email = aux.city;
            this.clientData.phone = aux.phone;
            console.log("THIS IS THE CLIENT",this.clientData)
            

          }
        });
      }   
    
    
    });
  }
  
  


  searchClient2k(){
    
    console.log("THIS ARE HEADERS",this.headers)
    var identificacionCliente = this.ccClient;
    var aux;
    this.httpClient.get("http://tienda724.com:8446/v1/persons/search?doc_person="+String(identificacionCliente),{ headers: this.headers }).subscribe((res)=>{
      console.log("THIS IS DOCUMENT DATA: ",JSON.parse(res.text()))
      if (res.text() == "[]"){
        this.openDialogClient();
        // this.searchClient(event);
      }else{ 
        aux = JSON.parse(res.text())[0]; 
        console.log("THIS IS AUX",aux)
        this.cliente = aux.fullname; 
        this.clientData.is_natural_person = true;
        this.clientData.fullname = aux.fullname;
        this.clientData.document_type = aux.document_TYPE;
        this.clientData.document_number = aux.document_NUMBER;
        this.clientData.id_third = aux.id_PERSON;
        this.id_person = aux.id_PERSON;
        this.clientData.address = aux.address; 
        this.clientData.email =  aux.city;
        this.clientData.phone = aux.phone;

        
      }
    
    
    });
    

  }

  

  openDialogClient2(): void {

  

    const dialogRef = this.dialog.open(GenerateThirdComponent2Component, {
      width: '60vw',
      data: {}
    });
    

dialogRef.afterClosed().subscribe(result => {
  if (result) {
    // console.log('CREATE CLIENT SUCCESS');
    // console.log(result);s
    let isNaturalPerson= result.data.hasOwnProperty('profile')?true:false;
    let dataPerson= isNaturalPerson?result.data.profile:result.data;
    this.clientData.is_natural_person = isNaturalPerson;
    this.clientData.fullname= dataPerson.info.fullname;
    this.clientData.document_type = dataPerson.info.id_document_type;
    this.clientData.document_number = dataPerson.info.document_number;
    this.clientData.address = dataPerson.directory.address;
    this.clientData.phone = dataPerson.directory.phones[0].phone;
    this.clientData.email = dataPerson.directory.hasOwnProperty('mails')?dataPerson.directory.mails[0].mail:'N/A';

  }

});
}



  searchClient2(){
    
    console.log("THIS ARE HEADERS",this.headers)
    var identificacionCliente = this.ccClient;
    var aux;
    this.httpClient.get('http://tienda724.com:8446/v1/persons/search?doc_person='+String(identificacionCliente),{ headers: this.headers }).subscribe((res)=>{
      console.log(JSON.parse(res.text()))
      if (res.text() == "[]"){
        this.openDialogClient2();
        // this.searchClient(event);
      }else{
        const dialogRef = this.dialog.open(ThirdselectComponent, {
          width: '60vw',
          height: '80vh',
          
          data: { thirdList: JSON.parse(res.text()) }
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if(result){

            aux = this.locStorage.getPersonClient(); 
            console.log("THIS THE AUX I NEED:", aux);
            this.cliente = aux.fullname; 
            this.clientData.is_natural_person = true;
            this.clientData.fullname = aux.fullname;
            this.clientData.document_type = aux.document_TYPE;
            this.clientData.document_number = aux.document_NUMBER;
            this.clientData.id_third = aux.id_PERSON;
            this.clientData.address = aux.address; 
            this.clientData.email = aux.city;
            this.clientData.phone = aux.phone;
            this.id_person = aux.id_PERSON;
            console.log("THIS IS THE CLIENT",this.clientData)
           

          }
        });

       
      }   
    
    
    });
    

  }


}



export interface DialogData {
  total: string;
  productsQuantity: number;
}