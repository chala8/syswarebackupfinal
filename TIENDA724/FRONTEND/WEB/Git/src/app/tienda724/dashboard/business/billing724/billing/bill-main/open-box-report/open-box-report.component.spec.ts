import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenBoxReportComponent } from './open-box-report.component';

describe('OpenBoxReportComponent', () => {
  let component: OpenBoxReportComponent;
  let fixture: ComponentFixture<OpenBoxReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenBoxReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenBoxReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
