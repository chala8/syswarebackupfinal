import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { BillingService } from '../../billing.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {

  constructor(public categoriesService: BillingService ,public dialogRef: MatDialogRef<AddCategoryComponent>,public dialog: MatDialog,@Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  categoryToPut
  categoryToPost
  editing
  idCategory2
  ngOnInit() {
    this.categoryToPost = this.data.categoryToPost
    this.categoryToPut = this.data.categoryToPut
    this.editing = this.data.editing
    this.idCategory2 = this.data.idCategory2
  }

  createNewCategory(){
    if(!this.editing){
      console.log(this.categoryToPost);
      this.categoriesService.postCategory(this.categoryToPost).subscribe(res=>{
        this.dialogRef.close()
      })
    }else{
      this.categoriesService.putCategory(this.categoryToPut,this.idCategory2).subscribe(res=>{
        this.dialogRef.close()
      })
    }
    console.log(this.categoryToPost);
  }

}  



export interface DialogData {
  categoryToPut: any;
  categoryToPost: any;
  editing: any;
  idCategory2: any;
}