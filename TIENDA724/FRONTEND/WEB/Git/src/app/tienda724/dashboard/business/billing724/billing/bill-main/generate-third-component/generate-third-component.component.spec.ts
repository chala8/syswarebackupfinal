import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateThirdComponentComponent } from './generate-third-component.component';

describe('GenerateThirdComponentComponent', () => {
  let component: GenerateThirdComponentComponent;
  let fixture: ComponentFixture<GenerateThirdComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateThirdComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateThirdComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
