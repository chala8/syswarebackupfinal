import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../../app.material';

/*
************************************************
*     principal component
*************************************************
*/
import { AttributeComponent } from './attribute/attribute.component';
import { AttributeMasterComponent } from './attribute/attribute-master/attribute-master.component';
import { AttributeDetailComponent } from './attribute/attribute-detail/attribute-detail.component';
import { AttributeValueComponent } from './attribute/attribute-value/attribute-value.component';
import { AttributeNewComponent } from './attribute/attribute-new/attribute-new.component';

import { DialogAttribute } from './attribute/attribute-new/attribute-new.component';
/*
************************************************
*     modules of  your app
*************************************************
*/

//import {   } from "../products";

/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { AttributeService } from './attribute.service';
import { DialogAttributeComponent } from './attribute/attribute-new/dialog-attribute/dialog-attribute.component';

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/

@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    RouterModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule,
    
  ],
  declarations: [AttributeComponent, AttributeMasterComponent, AttributeDetailComponent, AttributeValueComponent, AttributeNewComponent,DialogAttribute, DialogAttributeComponent],
  entryComponents:[DialogAttribute,DialogAttributeComponent],
  exports:[AttributeComponent, AttributeMasterComponent, AttributeDetailComponent, AttributeValueComponent],
  providers:[AttributeService]
})
export class AttributesModule { }
