import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefundBillComponent } from './refund-bill.component';

describe('RefundBillComponent', () => {
  let component: RefundBillComponent;
  let fixture: ComponentFixture<RefundBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefundBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefundBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
