import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductOnCategoryComponent } from './product-on-category.component';

describe('ProductOnCategoryComponent', () => {
  let component: ProductOnCategoryComponent;
  let fixture: ComponentFixture<ProductOnCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductOnCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductOnCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
