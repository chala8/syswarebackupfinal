import { Routes } from '@angular/router';


import { LoginComponent }  from './login/login.component';
import { LogoutComponent }  from './logout/logout.component';
import { AuthenticationComponent } from './authentication.component';


export const AuthenticationRouting: Routes = [
  { path: '', component: AuthenticationComponent,
  children: [
      { path: '', redirectTo: 'login', pathMatch: 'full'},
      { path: 'login', component: LoginComponent },
      { path: 'register', component: LogoutComponent },
  ]
},

]
