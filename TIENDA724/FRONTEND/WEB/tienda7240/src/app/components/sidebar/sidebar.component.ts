import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import {Router} from '@angular/router';
import { Menu } from '../../shared/menu';
import { Token } from '../../shared/token';
import { Person } from '../../shared/models/person';
import { LocalStorage } from '../../shared/localStorage';
import { AuthenticationService } from '../../authentication/authentication.service'
import { Third } from '../../tienda724/dashboard/business/thirds724/third/models/third';
import { ThirdService } from '../../tienda724/dashboard/business/thirds724/third/third.service';
import { OpenBoxComponent } from '../open-box/open-box.component';
import { BillingService } from '../../tienda724/dashboard/business/billing724/billing/billing.service';
import { MatDialog } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { UserIdleService } from 'angular-user-idle';
import { OpenorcloseboxComponent } from '../openorclosebox/openorclosebox.component';
import { SelectboxComponent } from '../selectbox/selectbox.component';
import { CloseBoxComponent } from '../../tienda724/dashboard/business/billing724/billing/bill-main/close-box/close-box.component';
import { StoreSelectorService } from "../store-selector.service";

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES_START: RouteInfo[] = [
  /* { path: 'business/menu', title: 'Dashboard',  icon: 'dashboard', class: '' }, */
/*   { path: 'user-profile', title: 'User Profile',  icon:'person', class: '' } */
];
export const ROUTES: RouteInfo[] = [
    /* { path: 'table-list', title: 'Table List',  icon:'content_paste', class: '' },
    { path: 'typography', title: 'Typography',  icon:'library_books', class: '' },
    { path: 'icons', title: 'Icons',  icon:'bubble_chart', class: '' },
    { path: 'maps', title: 'Maps',  icon:'location_on', class: '' },
    { path: 'notifications', title: 'Notifications',  icon:'notifications', class: '' },
    { path: 'upgrade', title: 'Upgrade to PRO',  icon:'unarchive', class: '' }, */
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  encapsulation:ViewEncapsulation.None
  
})
export class SidebarComponent implements OnInit {
  menusLista:Menu[];
  token:Token;
  menuItems: any[];
  menuItemsStart: any[];
  person: Person;
  thirdFather: Third;
  idThird: number;
  myBox: any;
  currentBox: String = "21";
  urlBase = "/dashboard/business/movement/";
  compressed = false;
  storeName=" ";
  SelectedStore;
  Stores;

  constructor(private service: StoreSelectorService, private userIdle: UserIdleService, private http: HttpClient, private authService:AuthenticationService,public locStorage: LocalStorage,public dialog: MatDialog,
    private _router: Router,public router: Router, private thirdService : ThirdService, private billingService : BillingService ) { 

      this.service.onMainEvent.subscribe(
        (onMain) => {
          this.SelectedStore = onMain;
        }
     );


     this.service.onLogOutEvent.subscribe(
      (log) => {
        this.logout();
      }
   );



    }


    setSelectedStore(elem){
      this.SelectedStore = elem;
    }



  ngOnInit() {
    
    
    let idPerson = this.locStorage.getPerson().id_person;
    
    this.thirdService.getThirdList().subscribe(res=>{
    // @ts-ignore
    let employee = res.filter(item=> item.profile.id_person === idPerson);
    this.idThird = employee[0].id_third;
    localStorage.setItem("id_employee",String(this.idThird));
    if(this.locStorage.getRol()[0].id_rol==8888 || this.locStorage.getRol()[0].id_rol==21){
    this.billingService.getCajaByIdStatus2(String(this.idThird)).subscribe(answering=>{
      this.myBox = answering;
      localStorage.setItem("myBox",answering);
      this.currentBox = localStorage.getItem("currentBox");
      console.log("this is res",answering);
      
      
      if(answering.length > 0){
        // if(res.length === 1 ){
        //   this.unicaCaja = true;
        // }else{
        //   this.unicaCaja = false;
        // }
        var dialogRef;
            dialogRef = this.dialog.open(OpenorcloseboxComponent, {
              width: '60vw',
              data: {},
              disableClose: true
            }).afterClosed().subscribe(response=> {
              console.log(response)
              if(response){
                this.locStorage.setBoxStatus(true);
                this.locStorage.setIdCaja(Number(answering[0]));
                this.http.get("http://tienda724.com:8451/v1/close/myboxes?id_person="+this.locStorage.getPerson().id_person).subscribe(resp => {
                  console.log(resp)
                  //@ts-ignore
                  resp.forEach(element => {
                    if(element.id_CAJA==Number(answering[0])){
                      console.log("ENTRE")
                      this.locStorage.setIdStore(element.id_STORE);
                      this.getStoreType(element.id_STORE);
                    }
                  });
                  this.SelectedStore==this.locStorage.getIdStore();
                  this.router.navigateByUrl("/dashboard/business/movement/billing/main")
                  this.getStores2();
                })
                console.log("IM ON THE TRUE SIDE OF THINGS")
              }else{
                this.locStorage.setIdCaja(Number(answering[0]));
                console.log("IM ON THE FALSE SIDE OF THINGS")
                var dialogRef;
                dialogRef = this.dialog.open(CloseBoxComponent, {
                  width: '60vw',
                  data: {flag:false},
                  disableClose: true
                }).afterClosed().subscribe(response=> {
                  this.locStorage.setIdCaja(Number(answering[0]));
                  dialogRef = this.dialog.open(SelectboxComponent, {
                    width: '60vw',
                    data: {},
                    disableClose: true
                  }).afterClosed().subscribe(response2=> {
                    if(response2.open){
                    this.locStorage.setIdCaja(response2.idcaja);
                    this.http.get("http://tienda724.com:8451/v1/close/myboxes?id_person="+this.locStorage.getPerson().id_person).subscribe(resp => {
                      //@ts-ignore
                      resp.forEach(element => {
                        if(element.id_CAJA==this.locStorage.getIdCaja()){
                          this.locStorage.setIdStore(element.id_STORE);
                          this.getStoreType(element.id_STORE);
                        }
                      });
                    })
                    dialogRef = this.dialog.open(OpenBoxComponent, {
                      width: '60vw',
                      data: {flag:false},
                      disableClose: true
                    }).afterClosed().subscribe(response=> {
                      this.locStorage.setBoxStatus(true);
                      this.SelectedStore==this.locStorage.getIdStore();
                      this.getStores2();
                      console.log("ID CAJA: ",this.locStorage.getIdCaja())
                      console.log("ID STORE: ",this.locStorage.getIdStore())
                      console.log("STORE TYPE: ",this.locStorage.getTipo())
                      console.log("BOX TYPE: ",this.locStorage.getBoxStatus())
                      this.router.navigateByUrl("/dashboard/business/movement/billing/main")
                    });
                    
                  }else{
                    this.locStorage.setBoxStatus(false);
                    this.getStores2();
                  }
                });
                  
                });
              }            
            });
      }else{
        console.log("IM ON THE NO OPEN BOX SIDE OF THINGS")
          var dialogRef;
          dialogRef = this.dialog.open(SelectboxComponent, {
            width: '60vw',
            data: {},
            disableClose: true
          }).afterClosed().subscribe(response2=> {
            console.log("this is my boolean, ",response2.open)
            if(response2.open){
            this.locStorage.setIdCaja(response2.idcaja);
            this.http.get("http://tienda724.com:8451/v1/close/myboxes?id_person="+this.locStorage.getPerson().id_person).subscribe(resp => {
              //@ts-ignore
              resp.forEach(element => {
                if(element.id_CAJA==this.locStorage.getIdCaja()){
                  this.locStorage.setIdStore(element.id_STORE);
                  this.getStoreType(element.id_STORE);
                }
              });
            })
            dialogRef = this.dialog.open(OpenBoxComponent, {
              width: '60vw',
              data: {flag:false},
              disableClose: true
            }).afterClosed().subscribe(response=> {
              this.locStorage.setBoxStatus(true);
              console.log("ID CAJA: ",this.locStorage.getIdCaja())
              console.log("ID STORE: ",this.locStorage.getIdStore())
              console.log("STORE TYPE: ",this.locStorage.getTipo())
              console.log("BOX TYPE: ",this.locStorage.getBoxStatus())
              this.SelectedStore==this.locStorage.getIdStore();
              this.router.navigateByUrl("/dashboard/business/movement/billing/main")
              this.getStores2();
            });
          }else{
            this.locStorage.setIdCaja(response2.idcaja);
            this.getStores3();
            this.http.get("http://tienda724.com:8451/v1/close/myboxes?id_person="+this.locStorage.getPerson().id_person).subscribe(resp => {
              //@ts-ignore
              resp.forEach(element => {
                if(element.id_CAJA==this.locStorage.getIdCaja()){
                  this.locStorage.setIdStore(element.id_STORE);
                  this.getStoreType(element.id_STORE);
                  this.getStores2();
                }
              });
            })
            
          }
          });
          
          
      }
      // if(res.length > 0){
      //   // if(res.length === 1 ){
      //   //   this.unicaCaja = true;
      //   // }else{
      //   //   this.unicaCaja = false;
      //   // }
      // }else{
      //     var dialogRef;
      //       dialogRef = this.dialog.open(OpenBoxComponent, {
      //         width: '60vw',
      //         data: {},
      //         disableClose: true
      //       }
    // }); 
      // }
     })}
    });

    this.userIdle.startWatching();
    
    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => console.log(count));
    
    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => {
      alert('Se ha cerrado su sesion debido a Inactividad.')
      this.logout();
      
    });
    console.log(this.locStorage.getMenu(),"lo menu Xddd")
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.menuItemsStart = ROUTES_START.filter(menuItem => menuItem);

      /** @todo elimanar la asiignación de token

    */
    
    
        let session=this.locStorage.getSession();
        if(!session){
           /**
           @todo Eliminar comentario para
           */
             this.Login();
         }else{
           this.menusLista=this.locStorage.getMenu().sort(this.dynamicSort("id_menu"));
           this.token=this.locStorage.getToken();
           
           this.person=this.locStorage.getPerson();
           this.thirdFather=this.locStorage.getThird();
         }

    
    
  }

  getStoreType(id_store){
        //@ts-ignore 
        this.http.get("http://tienda724.com:8447/v1/store/tipoStore?id_store="+id_store).subscribe( response => {
          this.locStorage.setTipo(response);
          console.log("ID CAJA: ",this.locStorage.getIdCaja())
          console.log("ID STORE: ",this.locStorage.getIdStore())
          console.log("STORE TYPE: ",this.locStorage.getTipo())
          console.log("BOX TYPE: ",this.locStorage.getBoxStatus())
          this.getStores();
        })
  }


  getStores() {
    this.billingService.getStoresByThird(this.locStorage.getThird().id_third).subscribe(data => { 
      data.forEach(element => {
        if(element.id_STORE==this.locStorage.getIdStore()){
          this.storeName = element.store_NAME;
        }
      });
    })
}

setIdStore(){
  this.locStorage.setIdStore(this.SelectedStore);
  this.locStorage.setIdCaja(1);
  this.locStorage.setBoxStatus(false);
  console.log("NEW ID STORE:", this.locStorage.getIdStore());
  console.log("NEW ID CAJA:", this.locStorage.getIdCaja());
  console.log("NEW BOX STATUS:", this.locStorage.getBoxStatus());
}


getStores2() {
  this.billingService.getStoresByThird(this.locStorage.getThird().id_third).subscribe(data => { 
      console.log(data);
      this.Stores = data 
      this.SelectedStore = this.locStorage.getIdStore();

    })
}


getStores3() {
  this.billingService.getStoresByThird(this.locStorage.getThird().id_third).subscribe(data => { 
      this.getStoreType(data[0].id_STORE);
      console.log(data);
      this.locStorage.setBoxStatus(false);
      this.Stores = data 
      this.SelectedStore = data[0].id_STORE;
      this.locStorage.setIdStore(data[0].id_STORE)
    }      )
}

  dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        /* next line works with strings and numbers, 
         * and you may want to customize it to your needs
         */
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };


  logout() {
    this.locStorage.cleanSession();
    this.token=null;
    this.goIndex();
    
    this.showNotification('top','right');
 
   }

   showNotification(from, align){
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: "Usted <b>Cerro Sesión</b> de forma satisfactoria."

    },{
        type: type[2],
        timer: 200,
        placement: {
            from: from,
            align: align
        }
    });
  }
 
   goIndex() {
     let link = ['/'];
     this._router.navigate(link);
   }
 
   goDashboard() {
     let link = ['/dashboard'];
     this._router.navigate(link);
 
   }
   Perfil() {
     alert("EN DESARROLLO");
 
   }

  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }

  gotoMenu(menu){
    this._router.navigateByUrl(this.urlBase+menu.ruta)
  }
}
