import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenBoxComponent } from './open-box.component';

describe('OpenBoxComponent', () => {
  let component: OpenBoxComponent;
  let fixture: ComponentFixture<OpenBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
