import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthenticationService } from '../../authentication/authentication.service';

import { LocalStorage } from '../../shared/localStorage';
import { Token } from '../../shared/token';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  test : Date = new Date();
  user=false;
  token:Token;

  constructor(private authService:AuthenticationService,
    private router:Router,
    private locStorage:LocalStorage) { }

  ngOnInit() {
    if(this.locStorage.getThird().id_third == 244){
      this.locStorage.setIdCaja(1);
      this.locStorage.setIdStore(1);
    }
    if(this.locStorage.getThird().id_third == 541){
      this.locStorage.setIdCaja(61);
      this.locStorage.setIdStore(62);
    }
    if(this.locStorage.getThird().id_third == 542){
      this.locStorage.setIdCaja(62);
      this.locStorage.setIdStore(61);
    }

    this.user=false;
 
    let session=this.locStorage.getSession();
    if(!session){
      /**
      @todo Eliminar comentario para
      */
    }else{
      this.token=this.locStorage.getToken();
      let person= this.locStorage.getPerson();
      this.token=this.locStorage.getToken();
    }
  }


  logout() {
    this.locStorage.cleanSession();
    this.token=null;
    this.goIndex();
 
   }
 
   goIndex() {
     let link = ['/'];
     this.router.navigate(link);
   }


}
