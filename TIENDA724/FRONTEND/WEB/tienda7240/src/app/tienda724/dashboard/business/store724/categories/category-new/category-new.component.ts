import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
/*
*    Material modules for component
*/
/*
*     others component
*/
/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { CategoryDTO } from '../models/categoryDTO'
import { Category } from '../models/category'
import { CommonStateStoreDTO } from '../../commons/CommonStateStoreDTO'
import { CommonStoreDTO } from '../../commons/CommonStoreDTO'

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { CategoriesService } from '../categories.service';
/*
*     constant of  your component
*/
declare var $:any;
@Component({
  selector: 'app-category-new',
  templateUrl: './category-new.component.html',
  styleUrls: ['./category-new.component.scss']
})
export class CategoryNewComponent implements OnInit {
    form: FormGroup;
    token:Token;
    category:Category[]
    currentCategory:Category

    newCategory:CategoryDTO;
    commonStateStoreDTO:CommonStateStoreDTO;
    common:CommonStoreDTO;
  
     //attributes
     CURRENT_ID_THIRD = 0;
     CURRENT_ID_CATEGORY = 0;
     ID_CATEGORY = 0;

  
    constructor(private router: Router, private route:ActivatedRoute ,private categoriesService: CategoriesService,
                private fb: FormBuilder, private locStorage: LocalStorage) {
  
                  this.common=new CommonStoreDTO(null,null)
                  this.commonStateStoreDTO= new CommonStateStoreDTO(1,null,null)
                  this.newCategory=new CategoryDTO(null,null,null,this.common,this.commonStateStoreDTO)
                }

  ngOnInit() {
    this.createControls();

    let session=this.locStorage.getSession();
    if(!session){
        this.Login();
    }else{

      this.token=this.locStorage.getToken();
   

       
      
      this.route.queryParams
          .subscribe(params => {

            // Defaults to 0 if no query param provided.  
              this.token=this.locStorage.getToken();
      
              this.CURRENT_ID_CATEGORY=params.father;
              
            this.loadData()         
        });
    }
  }

  Login() {
    let link = ['/auth'];
    this.router.navigate(link);
  }

  
    // Notification
    showNotification(from, align,id_type?, msn?, typeStr?){
      const type = ['','info','success','warning','danger'];
  
      const color = Math.floor((Math.random() * 4) + 1);
  
      $.notify({
          icon: "notifications",
          message: msn?msn:"<b>Noficación automatica </b>"
  
      },{
          type: typeStr? typeStr:type[id_type?id_type:2],
          timer: 200,
          placement: {
              from: from,
              align: align
          }
      });
    }

  createControls() {
    this.form = this.fb.group({

      //profile
      img_url: ['', Validators.compose([
        
      ])],
      name: ['', Validators.compose([
        Validators.required
      ])],
      description: ['', Validators.compose([
        Validators.required
      ])]
    });
  }


  loadData() {
    this.form.patchValue({
    
      img_url: "Colombia",
      name: "Bogota",
      description:"Descripción"
    });
  
  }


  createNewCategory() {

    this.common.name= this.form.value["name"];
    this.common.description= this.form.value["description"];
    this.newCategory.id_category_father = this.CURRENT_ID_CATEGORY>0?this.CURRENT_ID_CATEGORY:null;
    this.newCategory.id_third_category=this.CURRENT_ID_THIRD>0?this.CURRENT_ID_THIRD:null;
    this.newCategory.img_url= this.form.value["img_url"];
    this.newCategory.common=this.common;
    this.newCategory.state=this.commonStateStoreDTO;

    console.log("CAE ",this.newCategory)
    this.categoriesService.postCategory(this.newCategory)
    .subscribe(
    result => {
     
      if (result === true) { 
        this.showNotification('top','right',2,"<h2> Se ha CREADO Correctamente</2>",'success');
         this.resetForm();
        // this.goBack();
       


        return;
      } else {
        this.showNotification('top','right',3,"<h1> PROBLEMAS AL CREAR </h1>",'warning');
        //this.openDialog();
        return;
      }
    })
  }

  resetForm() {
    
    this.form.reset();
    
  }

  goBack() {
    let link = ['/dashboard/business/category/data'];
    this.router.navigate(link);
  }

}
