import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGeneralProductComponent } from './add-general-product.component';

describe('AddGeneralProductComponent', () => {
  let component: AddGeneralProductComponent;
  let fixture: ComponentFixture<AddGeneralProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGeneralProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGeneralProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
