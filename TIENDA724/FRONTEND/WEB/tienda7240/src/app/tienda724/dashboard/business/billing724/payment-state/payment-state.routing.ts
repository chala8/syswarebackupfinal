import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { PaymentStateDataComponent } from './payment-state-data/payment-state-data.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/


export const PaymentStateRouting: Routes = [
  
    { path: 'payment-state', component: null,
      children: [
        { path: '', redirectTo: 'data', pathMatch: 'full'},
        { path: 'data', component: PaymentStateDataComponent},
         
  
      ]
    }
  
  ]