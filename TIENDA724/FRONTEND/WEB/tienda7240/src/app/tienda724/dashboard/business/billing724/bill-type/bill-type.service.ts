import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';

/** Import Model Data */
import { CommonState } from '../commons/commonState'
import { BillType } from './models/billType'
/** Import Model Data Transfer Object */
import { CommonStateDTO } from '../commons/commonStateDTO'
import { BillTypeDTO } from './models/billTypeDTO'

@Injectable()
export class BillTypeService {
  api_uri = Urlbase[3] + '/billing-type';
  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');

    this.options = new RequestOptions({ headers: this.headers });
  }


  public getBillTypeResource = (state_bill?:number,id_bill_type?:number ,id_bill?:number ): Observable<{} | BillType[]> => {

    let params: URLSearchParams = new URLSearchParams();
    params.set('id_bill_type', id_bill_type ? "" + id_bill_type : null);
    params.set('id_bill', id_bill ? "" + id_bill : null);
  
    params.set('state_bill', state_bill ? "" + state_bill : null);
    let myOption: RequestOptions = this.options;
    myOption.search = params;
    return this.http.get(this.api_uri, this.options)
    .map((response: Response) => <any[]>response.json())
    .catch(this.handleError);
    }

    public postBillTypeResource = (body:BillTypeDTO): Observable<number[] | any> => {
      
      let params: URLSearchParams = new URLSearchParams();
      let myOption: RequestOptions = this.options;
      myOption.search = params;
      
  
      return this.http.post(this.api_uri, body, this.options)
        .map((response: Response) => {
  
          
          let id=+response["_body"]
  
          //responseEntry=JSON.stringify(response["_body"]);//Object.assign(,responseEntry)
          if(id){
            return id;
          }else{
  
            return null;
  
          }
        })
        .catch(this.handleError);
    }


    private handleError(error: Response | any) {
      // In a real world app, we might use a remote logging infrastructure
      let errMsg: string;
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      alert(errMsg);
      return null;//Observable.throw(errMsg);
    }


}
