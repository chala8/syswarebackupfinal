import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateThirdComponent2Component } from './generate-third-component2.component';

describe('GenerateThirdComponent2Component', () => {
  let component: GenerateThirdComponent2Component;
  let fixture: ComponentFixture<GenerateThirdComponent2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateThirdComponent2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateThirdComponent2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
