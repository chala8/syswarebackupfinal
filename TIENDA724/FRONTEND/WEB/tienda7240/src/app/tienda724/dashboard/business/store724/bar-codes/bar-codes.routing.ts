import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { BarCodeListComponent } from './bar-code-list/bar-code-list.component';
import { BarCodeNewComponent } from './bar-code-new/bar-code-new.component';
import { BarCodeEditComponent } from './bar-code-edit/bar-code-edit.component';



/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/

export const BarCodesRouting: Routes = [
  
    { path: 'bar-code', component: null,
      children: [
          { path: '', redirectTo: 'list', pathMatch: 'full'},
          { path: 'list', component: BarCodeListComponent},
          { path: 'new', component: BarCodeNewComponent},
          { path: 'edit/:id', component: BarCodeEditComponent}
  
      ]
    }
  
  ]

