import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoDispCompComponent } from './no-disp-comp.component';

describe('NoDispCompComponent', () => {
  let component: NoDispCompComponent;
  let fixture: ComponentFixture<NoDispCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoDispCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoDispCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
