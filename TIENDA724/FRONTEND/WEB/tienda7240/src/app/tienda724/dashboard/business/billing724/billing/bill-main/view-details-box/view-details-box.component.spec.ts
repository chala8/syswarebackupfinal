import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewDetailsBoxComponent } from './view-details-box.component';

describe('ViewDetailsBoxComponent', () => {
  let component: ViewDetailsBoxComponent;
  let fixture: ComponentFixture<ViewDetailsBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewDetailsBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewDetailsBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
