import { Component, OnInit, Inject,ChangeDetectorRef  } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA, SELECT_PANEL_INDENT_PADDING_X } from '@angular/material';
import { BillingService } from '../../billing.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { totalmem } from 'os';
import { LocalStorage } from 'app/shared/localStorage';
import { ThirdService } from 'app/tienda724/dashboard/business/thirds724/third/third.service';
@Component({
  selector: 'app-statechange',
  templateUrl: './statechange.component.html',
  styleUrls: ['./statechange.component.scss']
})
export class StatechangeComponent implements OnInit {
  elem: any;
  ListReportProd;
  ListReportProd2;
  ListChecked=[];
  checked = true;
  tax=0;
  subtotal=0;
  total =0;
  alttax=0;
  altsubtotal=0;
  alttotal =0;
  pdfDatas: pdfData;
  private headers = new HttpHeaders();
  prefix = "SA";
  SelectedVehicle = "";
  ListVehicles;
  
  constructor(public locStorage: LocalStorage,private cdRef:ChangeDetectorRef,private http2: HttpClient ,public dialogRef: MatDialogRef<StatechangeComponent>,public dialog: MatDialog,@Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  
  ngOnInit() {
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());
    this.getVehicles();
    this.tax=0;
    this.subtotal=0;
    this.total=0;
    this.altsubtotal=0;
    this.alttax=0;
    this.alttotal=0;
    this.elem = this.data.elem;
    console.log("THIS IS THE DATA",this.data);
    
    this.http2.post("http://tienda724.com:8448/v1/pedidos/detalles",{listaTipos: [this.data.elem.id_BILL]},{}).subscribe(
      response => {
        this.ListReportProd = response;
        console.log(this.ListReportProd) 
        //@ts-ignore
        response.forEach(element => {
          this.ListChecked.push(true);
        });
    
        this.calcsubtotal()
      }
    )
    this.http2.post("http://tienda724.com:8448/v1/pedidos/detalles2",{listaTipos: [this.data.elem.id_BILL]},{}).subscribe(
      response => {
        this.ListReportProd2 = response;
        console.log(this.ListReportProd2) 
      }
    )
  }

  showList(){
    this.checked = false;
    this.calcsubtotal();
  }

  async changeCheked(el){
    if(el){
      for(let i=0; i< this.ListChecked.length;i++){
        this.ListChecked[i]=true;
      }
    }
  }
  allChecked(el){
    this.changeCheked(el).then(response => {
      console.log(this.ListChecked)
      this.calcsubtotal()
    }) 
  }


  getVehicles(){

    this.http2.get("http://tienda724.com:8447/v1/pedidos/vehiculos").subscribe(response => {
      this.ListVehicles = response;
      this.SelectedVehicle = response[0].id_VEHICULO;
    })

  }


  sendProducts(){
    let cont = 0;
    this.alttotal = 0;
    this.altsubtotal = 0;
    this.alttax = 0;
    let listToPost = [];
    this.http2.get("http://tienda724.com:8448/v1/pedidos/billtoupdate?idstore="+this.data.elem.id_STORE_CLIENT+"&idstoreclient="+this.data.elem.id_STORE+"&numpedido="+this.data.elem.numpedido).subscribe(element => {
      let idToUpDate = this.data.elem.id_BILL;
      //@ts-ignore
      let idToUpDate2 = element.id_bill;
      //@ts-ignore
      let idEmployeeC = element.id_third_employee;
      //@ts-ignore
      let idthirdc = element.id_third;
      let billType1 = 1;
      let billType2 = 46;
      if(idthirdc == this.locStorage.getThird().id_third){
        billType1 = 4;
        billType2 = 47;
      }

      //@ts-ignore
      console.log("I WILL USE THIS TO MY ADVANTAGE: ",this.data.elem.id_BILL, element.id_bill)
      this.http2.put("http://tienda724.com:8448/v1/pedidos/billstate?billstate=82&billid="+idToUpDate,null).subscribe(a=> {
        this.http2.put("http://tienda724.com:8448/v1/pedidos/billstate?billstate=81&billid="+idToUpDate2,null).subscribe(a=> {
   
          this.checkDetails().then(item => { 
            console.log("THIS IS JSON: ",{
              id_third_employee: this.locStorage.getPerson().id_person,
              id_third: this.locStorage.getThird().id_third,
              id_store: this.locStorage.getIdStore(),
              totalprice: Math. round(this.subtotal + this.tax),
              subtotal: this.subtotal,
              tax: this.tax,
              id_bill_state: 1,
              id_bill_type: billType1,
              id_third_destinity: idthirdc,
              id_store_cliente: this.data.elem.id_STORE_CLIENT,
              num_documento_cliente: this.elem.numpedido 
             });
          this.http2.post("http://tienda724.com:8448/v1/pedidos/create2",
          {
           id_third_employee: this.locStorage.getPerson().id_person,
           id_third: this.locStorage.getThird().id_third,
           id_store: this.locStorage.getIdStore(),
           totalprice: Math. round(this.subtotal + this.tax),
           subtotal: this.subtotal,
           tax: this.tax,
           id_bill_state: 1,
           id_bill_type: billType1,
           id_third_destinity: idthirdc,
           id_store_cliente: this.data.elem.id_STORE_CLIENT,
           num_documento_cliente: this.elem.numpedido 
          }
          ).subscribe(idBillVent => {

            console.log("LOGRE ENTRAR #1")
            this.http2.post("http://tienda724.com:8448/v1/pedidos/create2",
              {
               id_third_employee: idEmployeeC,
               id_third: idthirdc,
               id_store: this.data.elem.id_STORE_CLIENT,
               totalprice: Math.round(this.subtotal + this.tax),
               subtotal: this.subtotal,
               tax: this.tax,
               id_bill_state: 1,
               id_bill_type: billType2,
               id_third_destinity: this.locStorage.getThird().id_third,
               id_store_cliente: this.locStorage.getIdStore(),
               num_documento_cliente: this.elem.numpedido
              }
              ).subscribe(idBillVent2 => {
                console.log("THIS IDBILLVENTA: ",idBillVent+"THIS IDBILLVENTA2: ",idBillVent2);
                this.http2.post("http://tienda724.com:8448/v1/pedidos/create2",
                {
                 id_third_employee: this.locStorage.getPerson().id_person,
                 id_third: this.locStorage.getThird().id_third,
                 id_store: this.locStorage.getIdStore(),
                 totalprice: Math.round(this.subtotal + this.tax),
                 subtotal: this.subtotal,
                 tax: this.tax,
                 id_bill_state: 1,
                 id_bill_type: 88,
                 id_third_destinity: idthirdc,
                 id_store_cliente: this.data.elem.id_STORE_CLIENT,
                 num_documento_cliente: this.elem.numpedido
                }
                ).subscribe(result => {
                  console.log("ENTRE FINAL")
                  if(this.ListChecked.includes(false)){
                    console.log("PUDE ENTRAR AL IF")
                    this.http2.post("http://tienda724.com:8448/v1/pedidos/create2",
                    {
                      id_third_employee: this.locStorage.getPerson().id_person,
                      id_third: this.locStorage.getThird().id_third,
                      id_store: this.locStorage.getIdStore(),
                      totalprice: 1,
                      subtotal: 1,
                      tax: this.tax,
                      id_bill_state: 65,
                      id_bill_type: 89,
                      id_third_destinity: idthirdc,
                      id_store_cliente: this.data.elem.id_STORE_CLIENT,
                      num_documento_cliente: this.elem.numpedido
                    }
                    ).subscribe(altBillId => {
                      
                    this.http2.post("http://tienda724.com:8448/v1/pedidos/create2",
                      {
                        id_third_employee: this.locStorage.getPerson().id_person,
                        id_third: idthirdc,
                        id_store: this.data.elem.id_STORE_CLIENT,
                        totalprice: 1,
                        subtotal: 1,
                        tax: this.tax,
                        id_bill_state: 67,
                        id_bill_type: 90,
                        id_third_destinity: this.locStorage.getThird().id_third,
                        id_store_cliente: this.locStorage.getIdStore(),
                        num_documento_cliente: this.elem.numpedido
                      }
                    ).subscribe(altBillId2 => {
                      this.postAll(altBillId,altBillId2).then(thing2 => {                         
                        this.getAll(billType1,result,idBillVent,idBillVent2).then(thing => {
                          this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+idBillVent,{},{responseType: 'text'}).subscribe(a => {
                            this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+idBillVent2,{},{responseType: 'text'}).subscribe(a => {
                              this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+result,{},{responseType: 'text'}).subscribe(a => {
                                this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+altBillId,{},{responseType: 'text'}).subscribe(a => {
                                  this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+altBillId2,{},{responseType: 'text'}).subscribe(a => {
                                
      
                                    
                                    console.log("THIS IS RESULT: ",result,", and THIS IS STORE: ",this.locStorage.getIdStore()+", and THIS IS THIRD: ",this.locStorage.getThird().id_third);
                                    this.http2.get("http://tienda724.com:8448/v1/billing/master2?id_bill="+idBillVent+"&id_store="+this.locStorage.getIdStore()).subscribe(
                                      dataresult => {
                                        if(idthirdc != this.locStorage.getThird().id_third){
                                          //@ts-ignore
                                          this.prefix = dataresult.prefix_BILL;
                                        }else{
                                          this.prefix = "SA"
                                        }
                                        let clientData = new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ',null);
                                        console.log("THIS IS DATA RESULT, ", dataresult)
                                        this.http2.get("http://tienda724.com:8448/v1/pedidos/procedimiento?idbventa="+idBillVent+"&idcompra="+idBillVent2+"&idremision="+result).subscribe(flagDo =>  {

                                        if(flagDo==1){
                                        this.http2.get("http://tienda724.com:8448/v1/billing/legaldata?id_third="+this.locStorage.getThird().id_third).subscribe(legalData => {
                                          this.http2.get("http://tienda724.com:8448/v1/billing/detail?id_bill="+result).subscribe(dataset => {
                                           console.log("THIS IS MY LEGAL DATA,",legalData);
                                           
                                          console.log("THIS IS MY CONSULTA, ", "http://tienda724.com:8448/v1/billing/legaldata?id_third="+this.locStorage.getThird().id_third)
                                            //@ts-ignore
                                            this.pdfDatas =  new pdfData (this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,"Régimen Simplificado","Efectivo",dataresult.fullname,dataresult.store_NAME,dataresult.purchase_DATE,"-1",this.prefix+" - " + dataresult.consecutive,dataset,dataresult.subtotal,dataresult.tax,dataresult.totalprice,this.locStorage.getPerson().first_name+" "+this.locStorage.getPerson().first_lastname,0,legalData[0].city_NAME+", "+legalData[0].address,legalData[0].phone1,clientData.document_type+": "+clientData.document_number,clientData.fullname,clientData.address,clientData.phone,legalData[0].resolucion_DIAN,legalData[0].regimen_TRIBUTARIO,legalData[0].prefix_BILL,legalData[0].initial_RANGE,legalData[0].final_RANGE,2);       
                                          
                                            
                                            console.log("this is pdfData1: ", JSON.stringify(this.pdfDatas));
                                            
                                            
                                            this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+idBillVent,{},{responseType: 'text'}).subscribe(a => {
                                            this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+idBillVent2,{},{responseType: 'text'}).subscribe(a => {
                                            this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+result,{},{responseType: 'text'}).subscribe(a => {
                                            this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+altBillId,{},{responseType: 'text'}).subscribe(a => {
                                            this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+altBillId2,{},{responseType: 'text'}).subscribe(a => {
                                              if(billType1==4){
                                                //@ts-ignore
                                                this.http2.post("http://tienda724.com:8448/v1/billing/pdf2",new pdfData (this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,"Régimen Simplificado","Efectivo",dataresult.fullname,dataresult.store_NAME,dataresult.purchase_DATE,"-1",this.prefix+" - " + dataresult.consecutive,dataset,dataresult.subtotal,dataresult.tax,dataresult.totalprice,this.locStorage.getPerson().first_name+" "+this.locStorage.getPerson().first_lastname,0,legalData[0].city_NAME+", "+legalData[0].address,legalData[0].phone1,clientData.document_type+": "+clientData.document_number,clientData.fullname,clientData.address,clientData.phone,legalData[0].resolucion_DIAN,legalData[0].regimen_TRIBUTARIO,legalData[0].prefix_BILL,legalData[0].initial_RANGE,legalData[0].final_RANGE,1),{responseType: 'text'}).subscribe( response => {
                                                
                                                window.open("http://tienda724.com/facturas/"+response, "_blank");
                                              }); 
                                              }else{
                                                //@ts-ignore
                                                this.http2.post("http://tienda724.com:8448/v1/billing/pdf2",new pdfData (this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,"Régimen Simplificado","Efectivo",dataresult.fullname,dataresult.store_NAME,dataresult.purchase_DATE,"-1",this.prefix+" - " + dataresult.consecutive,dataset,dataresult.subtotal,dataresult.tax,dataresult.totalprice,this.locStorage.getPerson().first_name+" "+this.locStorage.getPerson().first_lastname,0,legalData[0].city_NAME+", "+legalData[0].address,legalData[0].phone1,clientData.document_type+": "+clientData.document_number,clientData.fullname,clientData.address,clientData.phone,legalData[0].resolucion_DIAN,legalData[0].regimen_TRIBUTARIO,legalData[0].prefix_BILL,legalData[0].initial_RANGE,legalData[0].final_RANGE,2),{responseType: 'text'}).subscribe( response => {
                                                
                                                window.open("http://tienda724.com/facturas/"+response, "_blank");
                                              });
                    
                                              }
                                            this.http2.get("http://tienda724.com:8448/v1/billing/master2?id_bill="+idBillVent+"&id_store="+this.locStorage.getIdStore()).subscribe(
                                                dataresult2 => {
                                            this.http2.post("http://tienda724.com:8448/v1/pedidos/procedure2?idvehiculo="+this.SelectedVehicle+"&idbventa="+idBillVent+"&idbcompra="+idBillVent2+"&idbremision="+result+"&idstorecliente="+this.data.elem.id_STORE_CLIENT+"&idstoreproveedor="+this.locStorage.getIdStore(),{}).subscribe(dat => {
                                            console.log("THIS IS DATA SET: ",dataresult);
                                            this.http2.post("http://tienda724.com:8448/v1/pedidos/pdf",{
                                              documento: this.elem.numdocumento,
                                              cliente: this.elem.cliente+"-"+this.elem.tienda,
                                              fecha: new Date(),
                                              documento_cliente: this.elem.numpedido,
                                              telefono: this.elem.telefono,
                                              correo: this.elem.mail,
                                              direccion: this.elem.address,
                                              //@ts-ignore
                                              total: dataresult2.totalprice,
                                              //@ts-ignore
                                              subtotal: dataresult2.subtotal,
                                              //@ts-ignore
                                              tax: dataresult2.tax,
                                              detail_list: this.ListReportProd2,
                                              used_list: this.ListChecked        
                                            },{responseType: 'text'}).subscribe(responsepdf => {
                                              console.log("THIS IS MY RESPONSE, ",responsepdf);
                                              //window.open("http://tienda724.com/remisiones/"+responsepdf, "_blank");
                                              })
                                            }) 
                                          }) 
                                        })
                                      })
                                    })
                                  });
                                });
                              });
                            });
                          }});
                          });
                        });
                      });
                    });
                  })   
                });            
              });  
            }); 
          });  
        }); 
          }else{

                    this.getAll(billType1,result,idBillVent,idBillVent2).then(thing => {
                      this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+idBillVent,{},{responseType: 'text'}).subscribe(a => {
                        this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+idBillVent2,{},{responseType: 'text'}).subscribe(a => {
                          this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+result,{},{responseType: 'text'}).subscribe(a => {
                      
  
                                
                                console.log("THIS IS RESULT: ",result,", and THIS IS STORE: ",this.locStorage.getIdStore()+", and THIS IS THIRD: ",this.locStorage.getThird().id_third);
                                this.http2.get("http://tienda724.com:8448/v1/billing/master2?id_bill="+idBillVent+"&id_store="+this.locStorage.getIdStore()).subscribe(
                                  dataresult => {
                                    if(idthirdc != this.locStorage.getThird().id_third){
                                      //@ts-ignore
                                      this.prefix = dataresult.prefix_BILL;
                                    }else{
                                      this.prefix = "SA"
                                    }
                                    let clientData = new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ',null);
                                    console.log("THIS IS DATA RESULT, ", dataresult)
                                    this.http2.get("http://tienda724.com:8448/v1/pedidos/procedimiento?idbventa="+idBillVent+"&idcompra="+idBillVent2+"&idremision="+result).subscribe(flagDo =>  {

                                    if(flagDo==1){
                                      this.http2.post("http://tienda724.com:8448/v1/pedidos/procedure2?idvehiculo="+this.SelectedVehicle+"&idbventa="+idBillVent+"&idbcompra="+idBillVent2+"&idbremision="+result+"&idstorecliente="+this.data.elem.id_STORE_CLIENT+"&idstoreproveedor="+this.locStorage.getIdStore(),{}).subscribe(dat => {
                                            
                                    this.http2.get("http://tienda724.com:8448/v1/billing/legaldata?id_third="+this.locStorage.getThird().id_third).subscribe(legalData => {
                                      this.http2.get("http://tienda724.com:8448/v1/billing/detail?id_bill="+result).subscribe(dataset => {
                                       console.log("THIS IS MY LEGAL DATA,",legalData);
                                       
                                      console.log("THIS IS MY CONSULTA, ", "http://tienda724.com:8448/v1/billing/legaldata?id_third="+this.locStorage.getThird().id_third)
                                        //@ts-ignore
                                        this.pdfDatas =  new pdfData (this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,"Régimen Simplificado","Efectivo",dataresult.fullname,dataresult.store_NAME,dataresult.purchase_DATE,"-1",this.prefix+" - " + dataresult.consecutive,dataset,dataresult.subtotal,dataresult.tax,dataresult.totalprice,this.locStorage.getPerson().first_name+" "+this.locStorage.getPerson().first_lastname,0,legalData[0].city_NAME+", "+legalData[0].address,legalData[0].phone1,clientData.document_type+": "+clientData.document_number,clientData.fullname,clientData.address,clientData.phone,legalData[0].resolucion_DIAN,legalData[0].regimen_TRIBUTARIO,legalData[0].prefix_BILL,legalData[0].initial_RANGE,legalData[0].final_RANGE,2);       
                                      
                                        
                                        console.log("this is pdfData1: ", JSON.stringify(this.pdfDatas));
                                       


                                        this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+idBillVent,{},{responseType: 'text'}).subscribe(a => {
                                          this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+idBillVent2,{},{responseType: 'text'}).subscribe(a => {
                                            this.http2.put("http://tienda724.com:8448/v1/pedidos/billvalues?idbill="+result,{},{responseType: 'text'}).subscribe(a => {
                                              this.http2.get("http://tienda724.com:8448/v1/billing/master2?id_bill="+idBillVent+"&id_store="+this.locStorage.getIdStore()).subscribe(
                                                dataresult => {
                                                  if(billType1==4){
                                                    //@ts-ignore
                                                    this.http2.post("http://tienda724.com:8448/v1/billing/pdf2",new pdfData (this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,"Régimen Simplificado","Efectivo",dataresult.fullname,dataresult.store_NAME,dataresult.purchase_DATE,"-1",this.prefix+" - " + dataresult.consecutive,dataset,dataresult.subtotal,dataresult.tax,dataresult.totalprice,this.locStorage.getPerson().first_name+" "+this.locStorage.getPerson().first_lastname,0,legalData[0].city_NAME+", "+legalData[0].address,legalData[0].phone1,clientData.document_type+": "+clientData.document_number,clientData.fullname,clientData.address,clientData.phone,legalData[0].resolucion_DIAN,legalData[0].regimen_TRIBUTARIO,legalData[0].prefix_BILL,legalData[0].initial_RANGE,legalData[0].final_RANGE,1),{responseType: 'text'}).subscribe( response => {
                                                    
                                                    window.open("http://tienda724.com/facturas/"+response, "_blank");
                                                  }); 
                                                  }else{
                                                    //@ts-ignore
                                                    this.http2.post("http://tienda724.com:8448/v1/billing/pdf2",new pdfData (this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,"Régimen Simplificado","Efectivo",dataresult.fullname,dataresult.store_NAME,dataresult.purchase_DATE,"-1",this.prefix+" - " + dataresult.consecutive,dataset,dataresult.subtotal,dataresult.tax,dataresult.totalprice,this.locStorage.getPerson().first_name+" "+this.locStorage.getPerson().first_lastname,0,legalData[0].city_NAME+", "+legalData[0].address,legalData[0].phone1,clientData.document_type+": "+clientData.document_number,clientData.fullname,clientData.address,clientData.phone,legalData[0].resolucion_DIAN,legalData[0].regimen_TRIBUTARIO,legalData[0].prefix_BILL,legalData[0].initial_RANGE,legalData[0].final_RANGE,2),{responseType: 'text'}).subscribe( response => {
                                                    
                                                    window.open("http://tienda724.com/facturas/"+response, "_blank");
                                                  });
                        
                                                  }
              
                                        console.log("THIS IS DATA SET: ",dataresult);
                                        this.http2.post("http://tienda724.com:8448/v1/pedidos/pdf",{
                                          documento: this.elem.numdocumento,
                                          cliente: this.elem.cliente+"-"+this.elem.tienda,
                                          fecha: new Date(), 
                                          documento_cliente: this.elem.numpedido,
                                          telefono: this.elem.telefono,
                                          correo: this.elem.mail,
                                          direccion: this.elem.address,
                                          //@ts-ignore
                                          total: dataresult.totalprice,
                                          //@ts-ignore
                                          subtotal: dataresult.subtotal,
                                          //@ts-ignore
                                          tax: dataresult.tax,
                                          detail_list: this.ListReportProd2,
                                          used_list: this.ListChecked        
                                        },{responseType: 'text'}).subscribe(responsepdf => {
                                          console.log("THIS IS MY RESPONSE, ",responsepdf);
                                          //window.open("http://tienda724.com/remisiones/"+responsepdf, "_blank");
                                        })
                                        })
                                        })
                                      })
                                    })
                                  })
                                        
                                    });
                                   });
                                  }});
                                  });
                                
              
                                });
                              });
                            });
                      })               
  
                    
                  }
                 




              })
                this.dialogRef.close();
              })

          })
        })
        })
      })
    })
    
    console.log("I NEDD THIS; ", listToPost);
    

  }



async getAlts(){
  this.ListReportProd.array.forEach((element,index) => {
    if(!this.ListChecked[index]){
      this.http2.get("http://tienda724.com:8448/v1/pedidos/price?idps="+element.id_PRODUCT_THIRD).subscribe(r => { 
        console.log("this is element 1: ",element.id_PRODUCT_THIRD);
        this.http2.get("http://tienda724.com:8448/v1/pedidos/tax?idps="+element.id_PRODUCT_THIRD).subscribe(rE => { 
          console.log("this is element 2: ",element.id_PRODUCT_THIRD);
        });
      });       
    }
  });
}




async checkDetails(){

  this.ListReportProd.forEach((element, index) => {
    //@ts-ignore
    this.http2.get("http://tienda724.com:8448/v1/pedidos/quantity?id_product_store="+element.id_PRODUCT_THIRD).subscribe( response => {
      //@ts-ignore  
      console.log("PSID: "+element.id_PRODUCT_THIRD+", INVENTORY QUANTITY: "+response+", INDEX: "+index);
      //@ts-ignore  
      if((response - element.cantidad) < 0){
        console.log("ENTRE AL IF DE CANTIDADES");
        this.ListChecked[index] = false;
      }
    });
  });

}


async postAll(result,result2){
  this.ListChecked.forEach((element,index) => {
    if(!element){
      let quantity = this.ListReportProd[index].cantidad;
      let thise = this.ListReportProd[index];
      this.http2.get("http://tienda724.com:8448/v1/pedidos/quantity?id_product_store="+this.ListReportProd[index].id_PRODUCT_THIRD).subscribe(res => { 
      //@ts-ignore
      if((quantity-res)>0 && ((res-quantity)>quantity )){ 
      this.http2.get("http://tienda724.com:8448/v1/pedidos/price?idps="+thise.id_PRODUCT_THIRD).subscribe(r => { 
          this.http2.get("http://tienda724.com:8448/v1/pedidos/tax?idps="+thise.id_PRODUCT_THIRD).subscribe(rE => { 
            
              //@ts-ignore
              this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+result+"&cantidad="+quantity+"&id_ps="+thise.id_PRODUCT_THIRD+"&price="+r+"&tax="+rE/100,null).subscribe(
                rta  => {
                  this.http2.get("http://tienda724.com:8448/v1/pedidos/ps?id_product_store="+thise.id_PRODUCT_THIRD).subscribe(answer => {
                  this.http2.get("http://tienda724.com:8448/v1/pedidos/own?code="+answer+"&id_store="+this.data.elem.id_STORE_CLIENT).subscribe(answer2 => {
                  //@ts-ignore
                  this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+result2+"&cantidad="+quantity+"&id_ps="+answer2+"&price="+r+"&tax="+rE/100,null).subscribe(
                    rta  => {
                    })
                  })
                  })
                })
          })
        })
      }else{
        //@ts-ignore
        if((quantity-res)>0 ){ 
          this.http2.get("http://tienda724.com:8448/v1/pedidos/price?idps="+thise.id_PRODUCT_THIRD).subscribe(r => { 
              this.http2.get("http://tienda724.com:8448/v1/pedidos/tax?idps="+thise.id_PRODUCT_THIRD).subscribe(rE => { 
                
                  //@ts-ignore
                  this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+result+"&cantidad="+(quantity-res)+"&id_ps="+thise.id_PRODUCT_THIRD+"&price="+r+"&tax="+rE/100,null).subscribe(
                    rta  => {
                      this.http2.get("http://tienda724.com:8448/v1/pedidos/ps?id_product_store="+thise.id_PRODUCT_THIRD).subscribe(answer => {
                      this.http2.get("http://tienda724.com:8448/v1/pedidos/own?code="+answer+"&id_store="+this.data.elem.id_STORE_CLIENT).subscribe(answer2 => {
                      //@ts-ignore
                      this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+result2+"&cantidad="+(quantity-res)+"&id_ps="+answer2+"&price="+r+"&tax="+rE/100,null).subscribe(
                        rta  => {
                      })
                    })
                  })
                })
              })
            })
          }

      }
      })
    }
  })
}






  async getAll(type,result,idBillVent,idBillVent2){
    let cont = 0;
    this.ListChecked.forEach((element,index) => {
      if(element){
        console.log("THIS IS THE LIST ILL NEED, ",this.ListReportProd)
        let quantity = this.ListReportProd[cont].cantidad;
        let thise = this.ListReportProd[cont];

        this.http2.get("http://tienda724.com:8448/v1/pedidos/price?idps="+this.ListReportProd[cont].id_PRODUCT_THIRD).subscribe(r => { 
            this.http2.get("http://tienda724.com:8448/v1/pedidos/tax?idps="+thise.id_PRODUCT_THIRD).subscribe(rE => { 
             
            if(type==1){
                //@ts-ignore
                this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+idBillVent+"&cantidad="+quantity+"&id_ps="+thise.id_PRODUCT_THIRD+"&price="+r+"&tax="+rE/100,null).subscribe(
                  rta  => {
                  }
                )
                
                //@ts-ignore
                this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+idBillVent2+"&cantidad="+quantity+"&id_ps="+thise.id_PRODUCT_THIRD+"&price="+r+"&tax="+rE/100,null).subscribe(
                  rta  => {
                  }
                )
                //@ts-ignore
                this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+result+"&cantidad="+quantity+"&id_ps="+thise.id_PRODUCT_THIRD+"&price="+r+"&tax="+rE/100,null).subscribe(
                  rta  => {
                  }
                )
              }else{
                if(type==4){
                   //@ts-ignore
                this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+idBillVent+"&cantidad="+quantity+"&id_ps="+thise.id_PRODUCT_THIRD+"&price="+thise.costo+"&tax="+rE/100,null).subscribe(
                  rta  => {
                  }
                )
                
                //@ts-ignore
                this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+idBillVent2+"&cantidad="+quantity+"&id_ps="+thise.id_PRODUCT_THIRD+"&price="+thise.costo+"&tax="+rE/100,null).subscribe(
                  rta  => {
                  }
                )
                //@ts-ignore
                this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+result+"&cantidad="+quantity+"&id_ps="+thise.id_PRODUCT_THIRD+"&price="+thise.costo+"&tax="+rE/100,null).subscribe(
                  rta  => {
                  }
                )
                }
              }
            })
          })



        this.http2.put("http://tienda724.com:8448/v1/pedidos/quantity?quantity="+quantity+"&id_product_store="+thise.id_PRODUCT_THIRD,null).subscribe(
          rta  => {
              
          }
        )
      }else{
        let quantity = this.ListReportProd[cont].cantidad;
        let thise = this.ListReportProd[cont];
        this.http2.get("http://tienda724.com:8448/v1/pedidos/quantity?id_product_store="+this.ListReportProd[cont].id_PRODUCT_THIRD).subscribe(res => { 
          //@ts-ignore
          this.ListReportProd2[index].cantidad = res;
          //@ts-ignore
          this.ListReportProd2[index].costototal = res*this.ListReportProd2[index].costo;
  
          this.http2.get("http://tienda724.com:8448/v1/pedidos/price?idps="+thise.id_PRODUCT_THIRD).subscribe(r => { 
              this.http2.get("http://tienda724.com:8448/v1/pedidos/tax?idps="+thise.id_PRODUCT_THIRD).subscribe(rE => { 
               
                  //@ts-ignore
                  this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+idBillVent+"&cantidad="+res+"&id_ps="+thise.id_PRODUCT_THIRD+"&price="+r+"&tax="+rE/100,null).subscribe(
                    rta  => {
                    }
                  )
                  
                  //@ts-ignore
                  this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+idBillVent2+"&cantidad="+res+"&id_ps="+thise.id_PRODUCT_THIRD+"&price="+r+"&tax="+rE/100,null).subscribe(
                    rta  => {
                    }
                  )
                  //@ts-ignore
                  this.http2.post("http://tienda724.com:8448/v1/pedidos/detail?id_bill="+result+"&cantidad="+res+"&id_ps="+thise.id_PRODUCT_THIRD+"&price="+r+"&tax="+rE/100,null).subscribe(
                    rta  => {
                    }
                  )
  
              })
            })
  
  
          //@ts-ignore
          this.http2.put("http://tienda724.com:8448/v1/pedidos/quantity?quantity="+res+"&id_product_store="+thise.id_PRODUCT_THIRD,null).subscribe(
            rta  => {
                
            }
          )
          



        })
      }
      cont++;
    })
  }

  async getElem(){
    let cont = 0;
    this.ListChecked.forEach(element => {
      if(element){
        let elem = this.ListReportProd[cont];
        this.http2.get("http://tienda724.com:8448/v1/pedidos/price?idps="+elem.id_PRODUCT_THIRD).subscribe(r => { 
        console.log(r)
        //@ts-ignore
        this.subtotal+=r*elem.cantidad;
          this.http2.get("http://tienda724.com:8448/v1/pedidos/tax?idps="+elem.id_PRODUCT_THIRD).subscribe(rE => { 
            console.log("IVA",rE)
            //@ts-ignore
            this.tax+=(r*elem.cantidad*rE/100);
          })
        })
        
        
      }
      cont++;
    })
  } 

  calcsubtotal(){
    
    this.subtotal = 0;
    this.tax = 0;
    this.total = 0;
    this.getElem().then(responser => {
      console.log("WTF")
      console.log(this.subtotal + this.tax)
      
    })
    this.total = this.subtotal + this.tax;
  }
}



export interface DialogData {
  elem: any
}


export class pdfData{
  nit: String;
  regimen: String;
  medio: String;
  empresa: String;
  tienda: String;
  fecha: String;
  caja: String;
  consecutivo: String;
  detalles: [String[]];
  subtotal: number;
  tax: number;
  total: number;
  nombreCajero:String;
  cambio: number;
  direccion: String;
  telefono: String;
  cedula: String;
  cliente: String;
  direccionC: String;
  telefonoC: String;
  resolucion_DIAN:String;
  regimenT : String;
  prefix : String;
  initial_RANGE : String;
  final_RANGE : String;
  pdfSize: number;
  constructor(nit: String,
    regimen: String,
    medio: String,
    empresa: String,
    tienda: String,
    fecha: String,
    caja: String,
    consecutivo: String,
    detalles: [String[]],
    subtotal: number,
    tax: number,
    total: number,
    nombreCajero: string,
    cambio: number,
    direccion: String,
    telefono: String,
    cedula: String,
    cliente: String,
    direccionC: String,
    telefonoC: String,
    resolucion_DIAN:String,
    regimenT : String,
    prefix : String,
    initial_RANGE : String,
    final_RANGE : String,
    pdfSize:number){
      this.resolucion_DIAN = resolucion_DIAN;
      this.regimenT = regimenT;
      this.prefix = prefix;
      this.initial_RANGE = initial_RANGE;
      this.final_RANGE = final_RANGE;
      this.nombreCajero = nombreCajero;
      this.cambio = cambio;
      this.nit = nit;
      this.regimen = regimen;
      this.medio = medio;
      this.empresa = empresa;
      this.tienda = tienda;
      this.fecha = fecha;
      this.caja = caja;
      this.consecutivo = consecutivo;
      this.detalles = detalles;
      this.subtotal = subtotal;
      this.tax = tax;
      this.total = total;
      this.direccion = direccion;
      this.telefono = telefono;
      this.cedula = cedula;
      this.cliente = cliente;
      this.direccionC = direccionC;
      this.telefonoC = telefonoC;
      this.pdfSize = pdfSize;

    }
}

export class ClientData {
  is_natural_person: boolean;
  fullname:string;
  document_type: string;
  document_number: string;
  address: string;
  phone: string;
  email: string;
  id_third: number;

  constructor(
    is_natural_person, fullname, document_type, document_number, address, phone, email, id_third
  ) {
    this.is_natural_person = is_natural_person;
    this.fullname= fullname;
    this.document_type = document_type;
    this.document_number = document_number;
    this.address = address;
    this.phone = phone;
    this.email = email;
    this.id_third = id_third;
  }
}