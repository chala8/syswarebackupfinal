import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdselectComponent } from './thirdselect.component';

describe('ThirdselectComponent', () => {
  let component: ThirdselectComponent;
  let fixture: ComponentFixture<ThirdselectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThirdselectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdselectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
