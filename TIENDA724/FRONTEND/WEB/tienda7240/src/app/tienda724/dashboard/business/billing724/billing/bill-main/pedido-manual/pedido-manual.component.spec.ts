import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidoManualComponent } from './pedido-manual.component';

describe('PedidoManualComponent', () => {
  let component: PedidoManualComponent;
  let fixture: ComponentFixture<PedidoManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidoManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidoManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
