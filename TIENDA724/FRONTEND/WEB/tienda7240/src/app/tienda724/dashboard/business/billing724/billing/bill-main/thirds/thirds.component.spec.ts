import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdsComponent } from './thirds.component';

describe('ThirdsComponent', () => {
  let component: ThirdsComponent;
  let fixture: ComponentFixture<ThirdsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThirdsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
