import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutBillComponent } from './out-bill.component';

describe('OutBillComponent', () => {
  let component: OutBillComponent;
  let fixture: ComponentFixture<OutBillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutBillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutBillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
