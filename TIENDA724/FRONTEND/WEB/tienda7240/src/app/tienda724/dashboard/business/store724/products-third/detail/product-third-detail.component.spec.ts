import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductThirdDetailComponent } from './product-third-detail.component';

describe('ProductThirdDetailComponent', () => {
  let component: ProductThirdDetailComponent;
  let fixture: ComponentFixture<ProductThirdDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductThirdDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductThirdDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
