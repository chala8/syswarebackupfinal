import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsOnCategory2Component } from './products-on-category2.component';

describe('ProductsOnCategory2Component', () => {
  let component: ProductsOnCategory2Component;
  let fixture: ComponentFixture<ProductsOnCategory2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsOnCategory2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsOnCategory2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
