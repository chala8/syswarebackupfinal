import { Component, OnInit } from '@angular/core';
import { BillingService } from '../../billing.service';
import { LocalStorage } from 'app/shared/localStorage';
import { ThirdService } from 'app/tienda724/dashboard/business/thirds724/third/third.service';
import { MatTabChangeEvent, MatDialog, MatDialogRef } from '@angular/material';
import { ProductsOnCategoryComponent2 } from '../products-on-category2/products-on-category2.component';
import { ProductsOnStoreComponent } from '../products-on-store/products-on-store.component';

@Component({
  selector: 'app-bill-update',
  templateUrl: './bill-update.component.html',
  styleUrls: ['./bill-update.component.scss']
})
export class BillUpdateComponent implements OnInit {
  productList;
  generalCategories;
  subcategories;
  modal1 = true;
  modal2 = false;
  modal3 = false;
  id_employee;
  subCategoryTitle;
  categories;
  products;
  allCategories;
  allProducts;
  myProducts;
  stores;
  currentStore;
  currentStorage;
  idThirdFather;
  currentStorages;
  productsOnCategory;
  categoryStack = [];
  selectedTab = 0;
  currentCategory;

  constructor( public dialog: MatDialog, private storeService: BillingService, private locStorage: LocalStorage, private thirdService: ThirdService) { }
 
  ngOnInit() {
    this.currentCategory = null;
    this.categoryStack = []
    this.productsOnCategory = [];
    this.idThirdFather = this.locStorage.getThird().id_third;
    this.storeService.getGeneralCategories().subscribe(res=>{
      this.generalCategories = res;
      console.log(this.generalCategories,"theressssssss");
        this.storeService.getStoresByThird(this.idThirdFather).subscribe(res=>{
          this.stores = res;
          console.log(this.stores)
        })
    })
    this.getIdEmployee().then(res=>{
      this.thirdService.getThirdList().subscribe(res=>{
        console.log(res,"the third")
        //this.id_employee = JSON.parse(localStorage.getItem("currentPerson")).id_person;
        var employee;
        console.log(this.id_employee)
        // @ts-ignore
        employee = res.filter(item => item.profile.id_person === this.id_employee);
        console.log(employee)     
        
       
      })
    })

    this.currentStorage = [];
  }

  tabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    console.log('tabChangeEvent => ', tabChangeEvent); 
    console.log('index => ', tabChangeEvent.index); 
    this.selectedTab = tabChangeEvent.index;
    this.currentCategory = null;
    this.categoryStack = []
    this.productsOnCategory = [];
    this.getIdEmployee().then(res=>{
      this.thirdService.getThirdList().subscribe(res=>{
        console.log(res,"the third")
        //this.id_employee = JSON.parse(localStorage.getItem("currentPerson")).id_person;
        var employee;
        console.log(this.id_employee)
        // @ts-ignore
        employee = res.filter(item => item.profile.id_person === this.id_employee);
        console.log(employee)     
        this.idThirdFather = employee[0].id_third_father;
        this.storeService.getStoresByThird(this.idThirdFather).subscribe(res=>{
          this.stores = res;
          console.log(this.stores)
        })
        this.storeService.getGeneralCategories().subscribe(res=>{
          this.generalCategories = res;
        }) 
      })
    })

    this.currentStorage = [];
  }

  gotoModal3(){
    this.modal1 = false;
    this.modal2 = false;
    this.modal3 = true;
  }

  gotoModal2(){
    this.modal1 = false;
    this.modal2 = true;
    this.modal3 = false;
  }
  gotoModal1(){
    this.modal2 = false;
    this.modal1 = true;
    this.modal3 = false;
  }
  async getIdEmployee(){
    this.id_employee = this.locStorage.getPerson().id_person;

  }

  async popCategory(){
    this.categoryStack.pop();
  }

  gotoFatherCategory(){
    this.popCategory().then(res=>{
      console.log(this.categoryStack,"categorystact")
      console.log(this.categoryStack.length)
  
      if(this.categoryStack.length > 0){
        console.log(this.categoryStack[this.categoryStack.length - 1].id_CATEGORY)
        if(this.categoryStack[this.categoryStack.length - 1].name){
          this.currentCategory = this.categoryStack[this.categoryStack.length - 1].name;
        }else{
          this.currentCategory = this.categoryStack[this.categoryStack.length - 1].category;
        }
        this.storeService.getCategoryByThirdCategory(this.idThirdFather,this.categoryStack[this.categoryStack.length - 1].id_CATEGORY).subscribe(res=>{
          this.generalCategories = res
          console.log(res)
        })
      }else{
        this.currentCategory = null;
        (console.log("is 0"))
        this.storeService.getGeneralCategories().subscribe(res=>{
          this.generalCategories = res
          console.log(res)
        })
      }
    })
   
  }

  loadStore(store){
    this.currentStore = store;
    this.storeService.getStoragesByStore(store.id_STORE).subscribe(res=>{
      var dialogRef
      dialogRef = this.dialog.open(ProductsOnStoreComponent,{
        height: '450px',
        width: '600px',
        data: {
          currentStorages: res, storeName: store.store_NAME
        }
      })
    })
  }

  gotoProductsCategory(){
    this.storeService.getProductsByCategoryThird(this.categoryStack[this.categoryStack.length - 1].id_CATEGORY,this.idThirdFather).subscribe(res=>{
      console.log("This is res, ", res)
      var dialogRef
      if(JSON.stringify(res) != "[]"){
      dialogRef = this.dialog.open(ProductsOnCategoryComponent2,{
        height: '450px',
        width: '750px',
        data: {
          productsOnCategory: res, category: this.currentCategory
        }
      })
      dialogRef.afterClosed().subscribe(response => {
        var code = this.locStorage.getCodigoBarras();
        if(code != "-1"){
          console.log("SI LA CIERRO")
        }else{
          console.log("NO LA CIERRO")
        }
      })
      }
    })
  }

  loadCategory(category){
    this.categoryStack.push(category)
    if(category.name){
      this.currentCategory = category.name;
    }else{
      this.currentCategory = category.category;
    }
    console.log(category.name)
    console.log(category)
    this.storeService.getCategoryByThirdCategory(this.idThirdFather,category.id_CATEGORY).subscribe(res=>{
      this.generalCategories = res;
      console.log(res,"categories")
    }) 
    this.storeService.getProductsByCategoryThird(category.id_CATEGORY,this.idThirdFather).subscribe(res=>{
      this.productsOnCategory = res
      this.gotoProductsCategory();
      console.log(res)
    })
  }

  // setBackButtonAction(){
  //   this.navBar.backButtonClick = () => {
  //   if(this.modal2 || this.modal3){
  //     this.gotoModal1()
  //     this.currentStorage = [];
  //   }else{
  //     this.navCtrl.pop();
  //   }
  //   }
  // }

  addProduct(product){
    this.myProducts.push(product);
    product.selected = true;
  }

  deleteProduct(product){
    var currentProduct = product;
    this.myProducts.splice(this.myProducts.indexOf(product),1);
    currentProduct.selected = true;
    this.allProducts[this.allProducts.indexOf(currentProduct)].selected = false;
  }

  addCategory(){
    this.myProducts = [];
    this.allProducts.forEach(item=>{
      item.selected = false;
    })
    this.gotoModal1();
  }

  // addByBarcode(){
  //   var alert = this.alertCtrl.create({title: "Escaneando código de barra",
  //   subTitle: "Escanea o ingresa un código de barras",
  //   buttons:["Cancelar"]});
  //   alert.present();
  // }

  showStorage(storage){
    this.storeService.getProductsByStorage(storage.id_storage).subscribe(res=>{
      console.log(res);
      this.currentStorage = res;

    })
    
  }

}
