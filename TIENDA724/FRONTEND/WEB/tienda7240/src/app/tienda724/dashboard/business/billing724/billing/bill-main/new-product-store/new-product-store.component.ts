import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LocalStorage } from 'app/shared/localStorage';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-new-product-store',
  templateUrl: './new-product-store.component.html',
  styleUrls: ['./new-product-store.component.scss']
})
export class NewProductStoreComponent implements OnInit {
  codeList;


  
  productName: String;
  muName: String;
  brandName: String;
  body;
  priceName = "";
  price = "";
  options: RequestOptions;
  headers = new Headers();
  id_ps;



  constructor(public dialogRef: MatDialogRef<NewProductStoreComponent>,public dialog: MatDialog 
    ,@Inject(MAT_DIALOG_DATA) public data: DialogData,  public locStorage: LocalStorage, private http2: HttpClient,
    private http: Http) { 
      this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');

    this.options = new RequestOptions({ headers: this.headers });
  }

  ngOnInit() {
    this.codeList = this.data.codeList
    this.http2.get("http://tienda724.com:8447/v1/products2/code/byid?id_code="+this.data.codeList).subscribe(data => {
      let body = {
        id_store: this.locStorage.getIdStore(),
        id_code: this.data.codeList,
        product_store_name: data[0].product_name + " / " + data[0].munName + " / " +data[0].brandName,
        product_store_code: "",
        standard_price: 0,
        ownbarcode: data[0].code
      }
      this.body = body;
    });
  }

 returnData(){
  this.dialogRef.close();
  console.log("THIS IS BODY: ", this.body);
  this.http.post("http://tienda724.com:8447/v1/store/ps",this.body,{headers:this.headers} ).subscribe(res => {
    console.log("THIS IS BODY: ", this.body);
    console.log(res);
    this.http.post("http://tienda724.com:8447/v1/store/pricelist",null, {headers:this.headers,params:{
      //@ts-ignore
      "id_product_store":res._body,
      "price_description":this.priceName,
      "price":this.price
    }}).subscribe(resp => {
      console.log(resp);
    
  })

  })

 }
    

}

export interface DialogData {
  codeList: number
}