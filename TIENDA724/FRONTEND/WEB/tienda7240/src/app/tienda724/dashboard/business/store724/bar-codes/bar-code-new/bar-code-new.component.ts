import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
/*
*    Material modules for component
*/
import { MatTabChangeEvent, VERSION } from '@angular/material';
/*
*     others component
*/
/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { CodeDTO } from '../models/codeDTO'
import { CommonStateStoreDTO } from '../../commons/CommonStateStoreDTO'
import { MeasureUnit } from '../../measure-unit/models/measureUnit';
import { Product } from '../../products/models/product';

import { Attribute } from '../../attributes/models/attribute';
import { AttributeComplete } from '../../attributes/models/attributeComplete'

import { AttributeListDTO } from '../../attributes/models/attributeListDTO'
import { AttributeDetailListDTO } from '../../attributes/models/attributeDetailListDTO'
import { AttributeList } from '../../attributes/models/attributeList';
import { AttributeValue } from '../../attributes/models/attributeValue';

import { Category } from '../../categories/models/category'





/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { BarCodeService } from '../bar-code.service';
import { ProductsService } from '../../products/products.service'
import { MeasureUnitService } from '../../measure-unit/measure-unit.service'
import { CategoriesService } from '../../categories/categories.service'
import { AttributeService } from '../../attributes/attribute.service'
import { CategoriesModule } from '../../categories/categories.module';



/*
*     constant of  your component
*/
declare var $:any
@Component({
  selector: 'app-bar-code-new',
  templateUrl: './bar-code-new.component.html',
  styleUrls: ['./bar-code-new.component.scss']
})
export class BarCodeNewComponent implements OnInit {

  ID_ATTRIBUTE_LIST:number=0;
  ID_MEASURE_UNIT:number=0;
  ID_PRODUCT:number=0;
  

  form: FormGroup;
  token:Token;
  newCodeDTO:CodeDTO;
  commonStateStoreDTO:CommonStateStoreDTO;
  attributeListDTO:AttributeListDTO;
  attributeDetailListDTOs:AttributeDetailListDTO[];

  measureUnitList:MeasureUnit[];
  categories:Category[]
 
  attributeList:AttributeList[];
  attributeValues:AttributeValue[]
  selectValues:AttributeValue[]
  attributes:Attribute[]
  attributeComplete:AttributeComplete[]=[]
  
  attribValhashMap:any[];
  /*********      Products     *********/
  productList:Product[];
  //product from productors
  productFromProductors:Product[];
  //product from casero or third
  productFromCasero:Product[];
  // Deploy select
  products:Product[];


   //attributes
   isEditFields=false;
   isEditCode=false
   isseenAttribute:boolean=false;
   CURRENT_ID_THIRD = 0;
   CURRENT_ID_CATEGORY = 0;
   STATE=1
   myAttributeList:any
   indexTab:number=0;


    productoCtrl: FormControl;
    filteredProducts: Observable<Product[]>;
    /**
     * @deprecated
     */
    
   stateCtrl: FormControl;
   filteredStates: Observable<any[]>;
 
   states: any[] = [
     {
       name: 'Arkansas',
       population: '2.978M',
       // https://commons.wikimedia.org/wiki/File:Flag_of_Arkansas.svg
       flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg'
     },
     {
       name: 'California',
       population: '39.14M',
       // https://commons.wikimedia.org/wiki/File:Flag_of_California.svg
       flag: 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
     },
     {
       name: 'Florida',
       population: '20.27M',
       // https://commons.wikimedia.org/wiki/File:Flag_of_Florida.svg
       flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg'
     },
     {
       name: 'Texas',
       population: '27.47M',
       // https://commons.wikimedia.org/wiki/File:Flag_of_Texas.svg
       flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Texas.svg'
     }
   ];
  
   filterStates(name: string) {
    return this.states.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }
  
  filterProducts(product){
    console.log(this.productoCtrl)
    
    return this.productList.filter(state =>{
              if(state.name_product!==null ){

                if(product.name_product!==null){
                  console.log("filtra->",state.name_product.toLowerCase())
                  state.name_product.toLowerCase().indexOf(product.toLowerCase().name_product) === 0
                }else{
                  state.name_product.indexOf(product) === 0
                }
               
                
              }
        });
    }
    
      
      
     

  
  /**
   * 
   * @param router 
   * @param route 
   * @param barCodeService 
   * @param fb 
   * @param locStorage 
   * @param productService 
   * @param measureUnitService 
   * @param attributeService 
   */
  constructor(private router: Router, private route:ActivatedRoute ,private barCodeService: BarCodeService,
              private fb: FormBuilder, private locStorage: LocalStorage,
              private productService:ProductsService,
              private categoriesService:CategoriesService,private measureUnitService:MeasureUnitService,
              private attributeService:AttributeService) {

   
                this.commonStateStoreDTO= new CommonStateStoreDTO(1,null,null)
                this.newCodeDTO=new CodeDTO(null,null,null,null,null,null,null,this.commonStateStoreDTO)
                this.attributeDetailListDTOs=[]
                this.attributeListDTO= new AttributeListDTO(this.commonStateStoreDTO,this.attributeDetailListDTOs);
                this.productList=[]
                this.attributeValues=[]
                this.selectValues=[]

                this.productFromCasero=[]
                this.productFromProductors=[]
                this.products=[]

                this.createControls();
                this.logNameChange();

                this.productoCtrl= new FormControl();
                this.attribValhashMap=[]
                this.isEditFields=false;
              
              }
              
  ngOnInit() {
  

    let session=this.locStorage.getSession();
    if(!session){
        this.Login();
    }else{
      
      this.route.queryParams
          .subscribe(params => {

            // Defaults to 0 if no query param provided.  
              this.token=this.locStorage.getToken();
            
              this.CURRENT_ID_CATEGORY=params.father;
      
              
        });

        
        this.getMeasureUnitList(this.STATE)        
        this.getProductList(this.STATE)
        //this.getAttributes()

       
    }
  }
  seenAttribute(){
    this.isseenAttribute=!this.isseenAttribute;
    this.selectValues=[];

    if(this.isseenAttribute){
    
      if(this.attributeComplete.length<=0){
        this.getAttributeComplete(this.STATE,true);
      }

    }
  }

  createControls() {
    this.form = this.fb.group({
      code: ['', Validators.compose([
        Validators.required
      ])],
      product: ['', Validators.compose([
        Validators.required
      ])],
      measure_unit: ['', Validators.compose([
        Validators.required
      ])],
      suggest_price: ['', Validators.compose([
        Validators.required
      ])]
    });
  }


  loadData() {
    this.form.patchValue({
      suggest_price: 5000
    });
  
  }
   f1=false
   f2=false
   f3=false;
  logNameChange(){
   

    const prod = this.form.get('product');
    prod.valueChanges.forEach(
            (value: string) =>{
              if(prod['value']){

               this.f1=true

               if(this.f1&&this.f2&&this.f3){
                this.loadData()
                this.isEditFields=true
                
              }
              if(this.f1&&this.f2){
                this.isEditCode=true;
              }

              }
              console.log("CHANGE -> FORM ",prod['value'])
    });     
  

    const unit = this.form.get('measure_unit');
    unit.valueChanges.forEach(
      (value: string) =>{
        if(unit['_value']){
          this.f2=true
        

          if(this.f1&&this.f2&&this.f3){
            this.loadData()
            this.isEditFields=true
            
          }
          if(this.f1&&this.f2){
            this.isEditCode=true;
          }


        }
        console.log("CHANGE -> FORM ",unit['value'])
    }); 

    const code = this.form.get('code');
    code.valueChanges.forEach(
      (value: string) =>{
        if(code['value']){
          this.f3=true

          if(this.f1&&this.f2&&this.f3){
            this.loadData()
            this.isEditFields=true
            
          }
          if(this.f1&&this.f2){
            this.isEditCode=true;
          }

        }
        console.log("CHANGE -> FORM ",code['value'])
    });

  
  }
  

  addValue(id_attribute?:any,value?:AttributeValue){
    if(!id_attribute){
      if(this.selectValues.length>0){
        
              let exit=this.selectValues.find(val=> 
                (val.id_attribute===value.id_attribute)&&(val.id_attribute_value===value.id_attribute_value)
              )
              if(!exit){
    
                this.selectValues=this.selectValues.filter(val=> 
                  (val.id_attribute!=value.id_attribute)
                )
             
                console.log("VALUE ", this.selectValues)
    
                this.selectValues.push(value)
    
              }
        
            }else{
              this.selectValues.push(value)
            }
    
            console.log("- LIS- ", this.selectValues);
    }else{
        
      this.selectValues=this.selectValues.filter(val=> 
        (val.id_attribute!=(+id_attribute))
      )
   
      console.log("VALUE ", this.selectValues)

    }
    
    
        
  }

 

  Login() {
    let link = ['/auth'];
    this.router.navigate(link);
  }


 


  save() {
    this.commonStateStoreDTO.state=1;

    this.newCodeDTO.code= this.form.value["code"];
    if(this.indexTab===1){
      this.newCodeDTO.id_third_cod=(this.token.id_third_father!=null && this.token.id_third_father>0)?this.token.id_third_father:this.token.id_third;
    }
    this.newCodeDTO.id_measure_unit= this.form.value["measure_unit"].id_measure_unit;
    this.newCodeDTO.id_product=  this.form.value["product"].id_product;
    this.newCodeDTO.suggested_price=  +this.form.value["suggest_price"];
    this.newCodeDTO.state=this.commonStateStoreDTO;
      
    // get by POST a ttribute List detail
    this.newCodeDTO.id_attribute_list= null;
    if(this.selectValues.length>0){
      this.attributeListPOST(this.newCodeDTO)
    }else{
      this.codePOST(this.newCodeDTO);
    }

    console.log("CAE ",this.newCodeDTO)
  
  }

  codePOST(newCodeDTO:CodeDTO){
    this.barCodeService.postCategory(newCodeDTO)
    .subscribe(
    result => {
     
      if (result === true) {
        this.showNotification('top','right',2,"Se ha <b>CREADO</b> Correctamente");
         this.resetForm();
         
        return;
      } else {
        this.showNotification('top','right',3,"Problemas al <b>CREAR</b>");
        //this.openDialog();
        return;
      }
    })
  }
  attributeListPOST(codeDTO:CodeDTO){
    this.attributeDetailListDTOs=[]
    this.commonStateStoreDTO.state=1;
    for( let values of this.selectValues){
      this.attributeDetailListDTOs.push(
        {"id_attribute_value":values.id_attribute_value,"state":this.commonStateStoreDTO }
      );
    }
    this.attributeListDTO.state=this.commonStateStoreDTO
    this.attributeListDTO.details=this.attributeDetailListDTOs

    this.attributeService.postAttributeList(this.attributeListDTO)
    .subscribe(
    result => {
     
      if (result) {
       console.log("ID_LIST ->>" , result);
       codeDTO.id_attribute_list=+result

         this.codePOST(codeDTO);


       
        this.showNotification('top','right',2,"Se ha <b>CREADO</b> Correctamente");
         
        return;
      } else {
        this.showNotification('top','right',3,"Problemas al <b>CREAR</b>");
        
        return;
      }
    })


  }
  resetForm() {
    this.isEditFields=false
    this.isEditCode=false;
    this.isseenAttribute=false
    this.selectValues=[]    
    this.form.reset();
  }

  goBack() {
    let link = ['/dashboard/business/bar-code'];
    this.router.navigate(link);
  }

  showNotification(from, align,id_type?, msn?){
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: msn?msn:"<b>Noficación automatica </b>"

    },{
        type: type[id_type?id_type:2],
        timer: 200,
        placement: {
            from: from,
            align: align
        }
    });
  }
  tabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    
    this.indexTab=tabChangeEvent.index;
    if(this.indexTab==1){
      this.products=this.productFromCasero;
      
    }else{
      this.products= this.productFromProductors;
    }
        console.log(tabChangeEvent)
}


      



  // services

  getMeasureUnitList(state?:number,id_category?:number,id_category_father?:number,id_third?:number){
    
        this.measureUnitService.getCategoryList(state)
        .subscribe((data: MeasureUnit[]) => this.measureUnitList = data,
        error => console.log(error),
        () => { 
        });
    
  }
  getCategory(category:Category){

    
        
    let cat:Category[]
    cat=this.categories.filter(e=>{
      return e.id_category===category.id_category_father
    });

    if(cat.length>0){
      if(cat[0].common.name.toLocaleLowerCase()==="caseros"){
       
        return true;
      }else{
      
        this.getCategory(cat[0])
      }
    }else{
    
      return false;

    }

  }
  getCategoriesService(state?:number,id_category?:number,id_category_father?:number,id_third?:number){
        let isCasero=false;
        this.categoriesService.getCategoryList(state)
        .subscribe((data: Category[]) => this.categories = data,
        error => console.log(error),
        () => { 

          if(this.productList.length>0 && this.categories.length>0){

            for(let cat of this.categories){


              if(cat.common.name.toLocaleLowerCase()==="caseros"){
              
                isCasero=true;
              }else{
                
               
                isCasero=this.getCategory(cat)
              }
              for(let product of this.productList){
             
                  if(product.id_category=== cat.id_category){
                    
                    if(isCasero){
                   
                      this.productFromCasero.push(product)
                    }else{
                      this.productFromProductors.push(product)
                    }
                  }
              }
            }
            this.products=this.productFromProductors;

         
          }
        });
    
      }
      

 

      getProductList(state?:number,id_category?:number,id_category_father?:number,id_third?:number){
        
            this.productService.getProductList(state)
            .subscribe((data: Product[]) => this.productList = data,
            error => console.log(error),
            () => {
              if(this.productList.length>0){
                this.getCategoriesService(this.STATE);
              } 
        
              
            });
        
          }

    getAttributes(state?: number,is_values?:Boolean,id_attribute?:number,id_common?:number, 
      name?: string,description?:string, id_state?:number){
          this.attributeService.getAttribute(state,id_attribute,id_common, 
            name,description, id_state)
          .subscribe((data: Attribute[]) => this.attributes = data,
          error => console.log(error),
          () => { 
          });
    }
    getAttributeComplete(state?: number,is_values?:Boolean,id_attribute?:number,id_common?:number, 
      name?: string,description?:string, id_state?:number){
      this.attributeService.getAttributeComplete(state,is_values,id_attribute,id_common, 
        name,description, id_state)
      .subscribe((data: AttributeComplete[]) => this.attributeComplete = data,
      error => console.log(error),
      () => {
        if(this.attributeComplete.length>0 ){

        }
      });
    }

}
