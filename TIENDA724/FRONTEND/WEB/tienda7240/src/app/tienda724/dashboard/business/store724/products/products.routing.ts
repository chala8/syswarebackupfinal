import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { ProductDataComponent } from './product-data/product-data.component';
import { ProductNewComponent } from './product-new/product-new.component';
import { ProductEditComponent } from './product-edit/product-edit.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/

export const ProductsRouting: Routes = [
  
    { path: 'product', component: null,
      children: [
          { path: '', redirectTo: 'list', pathMatch: 'full'},
          { path: 'list', component: ProductDataComponent},
          { path: 'new', component: ProductNewComponent},
          { path: 'edit/:id', component: ProductEditComponent}
  
      ]
    }
  
  ]
