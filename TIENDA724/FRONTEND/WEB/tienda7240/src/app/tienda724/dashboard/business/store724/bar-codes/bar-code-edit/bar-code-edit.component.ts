import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
/*
*    Material modules for component
*/
/*
*     others component
*/
/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { CodeDTO } from '../models/codeDTO'
import { Code } from '../models/code'
import { CommonStateStoreDTO } from '../../commons/CommonStateStoreDTO'
import { MeasureUnit } from '../../measure-unit/models/measureUnit';
import { Product } from '../../products/models/product';
import { AttributeList } from '../../attributes/models/attributeList';




/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { BarCodeService } from '../bar-code.service';
import { ProductsService } from '../../products/products.service'
import { MeasureUnitService } from '../../measure-unit/measure-unit.service'
import { AttributeService } from '../../attributes/attribute.service'
/*
*     constant of  your component
*/
declare var $:any

@Component({
  selector: 'app-bar-code-edit',
  templateUrl: './bar-code-edit.component.html',
  styleUrls: ['./bar-code-edit.component.scss']
})
export class BarCodeEditComponent implements OnInit {

  ID_ATTRIBUTE_LIST:number=0;
  ID_MEASURE_UNIT:number=0;
  ID_PRODUCT:number=0;
  

  form: FormGroup;
  token:Token;
  newCategory:CodeDTO;
  commonStateStoreDTO:CommonStateStoreDTO;
  
  codeList:Code[];
  currentCode:Code;
  measureUnitList:MeasureUnit[];
  productList:Product[];
  attributeLists:AttributeList[];


   //attributes
   CURRENT_ID_THIRD = 0;
   CURRENT_ID_CATEGORY = 0;
   STATE=1
   id_code:number=0;

   myAttributeList:any



  constructor(private router: Router, private route:ActivatedRoute ,private codeService: BarCodeService,
              private fb: FormBuilder, private locStorage: LocalStorage,
              private productService:ProductsService,private measureUnitService:MeasureUnitService,
              private attributeService:AttributeService) {

   
                this.commonStateStoreDTO= new CommonStateStoreDTO(1,null,null)
                this.newCategory=new CodeDTO(null,null,null,null,null,null,null,this.commonStateStoreDTO)
              }
 

  ngOnInit() {
    this.createControls();
    
        let session=this.locStorage.getSession();
        if(!session){
            this.Login();
        }else{
          this.route.params
          .subscribe( params =>{
              this.id_code=+params['id']
              // Defaults to 0 if no query param provided.
              if(this.id_code>0){
                this.getCodeList(this.STATE,this.id_code)
              }else{
                this.showNotification('top','right',3,"Indicador de código invalido!");
                this.goBack();
              }
              


          });
          
          this.route.queryParams
              .subscribe(params => {
                // Defaults to 0 if no query param provided.  
                  this.token=this.locStorage.getToken();
                  this.CURRENT_ID_CATEGORY=params.father;        
            });

            if(this.id_code<=0){
              this.showNotification('top','right',3,"Indicador de código invalido!");
              this.goBack();
            }

            this.getMeasureUnitList(this.STATE)        
            this.getProductList(this.STATE)
            this.getAttributeList(this.STATE)
        }
  }

  getCodeList(state?: number,id_code?:number,code?:string,
    id_product?: number, img_url?:string, id_measure_unit?: number, 
    id_attribute_list?:number, id_state?:number){
    
        this.codeService.getCodeList(state,id_code)
        .subscribe((data: Code[]) => this.codeList = data,
        error => console.log(error),
        () => {

          if(this.codeList.length>0){
            this.currentCode=this.codeList[0];
            this.loadData()
          }else{
            this.showNotification('top','right',3,"Inconsistencias con el Código de barras!");
            this.goBack();
            
          }
    
         
        });
    
      }

  getMeasureUnitList(state?:number,id_category?:number,id_category_father?:number,id_third?:number){
    
        this.measureUnitService.getCategoryList(state)
        .subscribe((data: MeasureUnit[]) => this.measureUnitList = data,
        error => console.log(error),
        () => { 
        });
    
      }

    getAttributeList(state?:number,id_category?:number,id_category_father?:number,id_third?:number){
      
          this.attributeService.getAttributeList(state)
          .subscribe((data: AttributeList[]) => this.attributeLists = data,
          error => console.log(error),
          () => {
      
            
          });
      
        }

      getProductList(state?:number,id_category?:number,id_category_father?:number,id_third?:number){
        
            this.productService.getProductList(state)
            .subscribe((data: Product[]) => this.productList = data,
            error => console.log(error),
            () => {
        
              
            });
        
          }

  Login() {
    let link = ['/auth'];
    this.router.navigate(link);
  }


  createControls() {
    this.form = this.fb.group({
      code: ['', Validators.compose([
        Validators.required
      ])]
    });
  }


  loadData() {
    this.form.patchValue({
      code: this.currentCode.code
    });
  
  }


  save() {
    //console.log("PROD ", this.ID_PRODUCT, " ID_MS_UNIT ",this.ID_MEASURE_UNIT," ATTR ",this.ID_ATTRIBUTE_LIST)

    this.newCategory.code= this.form.value["code"];
    this.newCategory.id_attribute_list= this.ID_ATTRIBUTE_LIST>0?this.ID_ATTRIBUTE_LIST:null;
    this.newCategory.id_measure_unit= this.ID_MEASURE_UNIT>0?this.ID_MEASURE_UNIT:null;
    this.newCategory.id_product= this.ID_PRODUCT>0?this.ID_PRODUCT:null;
    this.newCategory.state=this.commonStateStoreDTO;

    //console.log("CAE ",this.newCategory)
    this.codeService.putCode(this.id_code,this.newCategory)
    .subscribe(
    result => {
     
      if (result === true) {
        this.showNotification('top','right',2,"Se ha <b>MODIFICADO</b> Correctamente");
         this.resetForm();
         this.goBack();
        return;
      } else {
        this.showNotification('top','right',3,"Problemas al <b>MODIFICAR</b>");
       
        return;
      }
    })
  }

  resetForm() {
    
    this.form.reset();
    
  }

  goBack() {
    let link = ['/dashboard/business/bar-code'];
    this.router.navigate(link);
  }



  addID(index,ID,option){
    //console.log("CHECK ",ID)
    if(option===1){
      this.ID_PRODUCT=ID
    }
    if(option===2){
      this.ID_MEASURE_UNIT=ID
     }
    if(option===3){
      this.ID_ATTRIBUTE_LIST=ID
     }
  }

  showNotification(from, align,id_type?, msn?){
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: msn?msn:"<b>Noficación automatica </b>"

    },{
        type: type[id_type?id_type:2],
        timer: 200,
        placement: {
            from: from,
            align: align
        }
    });
  }

  test(id_menu){
    //console.log("IS OR NO ",id_menu)

    return id_menu==24?true:false;
  }

}
