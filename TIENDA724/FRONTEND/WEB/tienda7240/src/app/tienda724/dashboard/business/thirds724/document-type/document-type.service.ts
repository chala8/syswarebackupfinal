import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

/** Files for  process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';
import { DocumentType } from './models/document-type';


@Injectable()
export class DocumentTypeService {

  documentType: DocumentType;
  api_uri = Urlbase[1] + '/documents-types';
  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append( 'Authorization', this.locStorage.getTokenValue());
    this.options = new RequestOptions({headers:this.headers});
  }

  public getDocumentTypeList= (): Observable<DocumentType[]> => {
       return this.http.get(this.api_uri, this.options)
           .map((response: Response) => <DocumentType[]>response.json())
           .catch(this.handleError);
   }


   private handleError (error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
        errMsg = error.message ? error.message : error.toString();
        }
        alert(errMsg);
        return Observable.throw(errMsg);
    }

}
