import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpParams, HttpHeaders, HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
  /** Files for auth process  */
  import { Urlbase } from '../../../../../shared/urls';
  import { LocalStorage } from '../../../../../shared/localStorage';
/** Import Model Data */
import { CommonState } from '../commons/commonState'
import { Bill } from './models/bill'
import { BillComplete } from './models/billComplete'
import { DetailBill } from './models/detailBill'


/** Import Model Data Transfer Object */
import { CommonStateDTO } from '../commons/commonStateDTO'
import { BillDTO } from './models/billDTO'
import { BillCompleteDTO } from './models/billCompleteDTO'
import { DetailBillDTO } from './models/detailBillDTO'
import { DetailBillIdDTO } from './models/detailBillIdDTO'
import { BillDetailIDDTO } from './models/billDetailIdDTO'


@Injectable()
export class BillingService {

  api_uri_bill = Urlbase[3] + '/billing';
  api_uri_caja = Urlbase[5] + '/close';
  api_uri_caja_detail = Urlbase[5] + '/detail-close';
  api_uri_inv = Urlbase[2]+ "/categories2";
  //api_uri_inv = "http://tienda724.com:8447/v1" + "/categories2";
  api_uri_products = Urlbase[2] + "/products2";
  api_uri_brand = Urlbase[2] + "/mun"
  api_uri_store = Urlbase[2] + "/store";




  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage,private http2: HttpClient) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');

    this.options = new RequestOptions({ headers: this.headers });

 
  }
  public getBillResource = (state_bill?:number,id_bill?:number,id_bill_father?:number,
                                id_third_employee?:number,
                                id_third?:number,
                                id_payment_state?:number,
                                id_bill_state?:number,
                                id_bill_type?:number,
                                consecutive?:string,
                                purchase_date?:Date,
                                subtotal?:number,
                                tax?:number,
                                discount?:number,
                                totalprice?:number,
                                id_state_bill?:number,
                                
                                creation_bill?:Date,
                                update_bill?:Date ): Observable<{} | any[]> => {

    let params: URLSearchParams = new URLSearchParams();
    params.set('id_bill', id_bill ? "" + id_bill : null);
    params.set('id_bill_father', id_bill_father ? "" + id_bill_father : null);
    params.set('id_third_employee', id_third_employee ? "" + id_third_employee : null);
    params.set('id_third', id_third ? "" + id_third : null);
    params.set('id_payment_state', id_payment_state ? "" + id_payment_state : null);
    params.set('id_bill_state', id_bill_state ? "" + id_bill_state : null);
    params.set('id_bill_type', id_bill_type ? "" + id_bill_type : null);
    params.set('consecutive', consecutive ? "" + consecutive : null);
    params.set('purchase_date', purchase_date ? "" + purchase_date : null);
    params.set('subtotal', subtotal ? "" + subtotal : null);
    params.set('tax', tax ? "" + tax : null);
    params.set('discount', discount ? "" + discount : null);
    params.set('totalprice', totalprice ? "" + totalprice : null);
    params.set('id_state_bill', id_state_bill ? "" + id_state_bill : null);
    params.set('state_bill', state_bill ? "" + state_bill : null);
    params.set('creation_bill', creation_bill ? "" + creation_bill : null);
    params.set('update_bill', update_bill ? "" + update_bill : null);
   

    let myOption: RequestOptions = this.options;
    myOption.search = params;
    return this.http.get(this.api_uri_bill+"/details", this.options)
      .map((response: Response) => <any[]>response.json())
      .catch(this.handleError);
  }



  public getBillDetailsResource = (state_bill?:number,id_bill?:number,id_bill_father?:number,
                                    id_third_employee?:number,
                                    id_third?:number,
                                    id_payment_state?:number,
                                    id_bill_state?:number,
                                    id_bill_type?:number,
                                    consecutive?:string,
                                    purchase_date?:Date,
                                    subtotal?:number,
                                    tax?:number,
                                    discount?:number,
                                    totalprice?:number,
                                    id_state_bill?:number,
                                    
                                    creation_bill?:Date,
                                    update_bill?:Date ): Observable<{} | Bill[]> => {

        let params: URLSearchParams = new URLSearchParams();
        params.set('id_bill', id_bill ? "" + id_bill : null);
        params.set('id_bill_father', id_bill_father ? "" + id_bill_father : null);
        params.set('id_third_employee', id_third_employee ? "" + id_third_employee : null);
        params.set('id_third', id_third ? "" + id_third : null);
        params.set('id_payment_state', id_payment_state ? "" + id_payment_state : null);
        params.set('id_bill_state', id_bill_state ? "" + id_bill_state : null);
        params.set('id_bill_type', id_bill_type ? "" + id_bill_type : null);
        params.set('consecutive', consecutive ? "" + consecutive : null);
        params.set('purchase_date', purchase_date ? "" + purchase_date : null);
        params.set('subtotal', subtotal ? "" + subtotal : null);
        params.set('tax', tax ? "" + tax : null);
        params.set('discount', discount ? "" + discount : null);
        params.set('totalprice', totalprice ? "" + totalprice : null);
        params.set('id_state_bill', id_state_bill ? "" + id_state_bill : null);
        params.set('state_bill', state_bill ? "" + state_bill : null);
        params.set('creation_bill', creation_bill ? "" + creation_bill : null);
        params.set('update_bill', update_bill ? "" + update_bill : null);


        let myOption: RequestOptions = this.options;
        myOption.search = params;
        return this.http.get(this.api_uri_bill, this.options)
        .map((response: Response) => <Bill[]>response.json())
        .catch(this.handleError);
      }

      public getBillCompleteResource = (state_bill?:number,id_bill?:number,id_bill_father?:number,
        id_third_employee?:number,
        id_third?:number,
        id_payment_state?:number,
        id_bill_state?:number,
        id_bill_type?:number,
        consecutive?:string,
        purchase_date?:Date,
        subtotal?:number,
        tax?:number,
        discount?:number,
        totalprice?:number,
        id_state_bill?:number,
        
        creation_bill?:Date,
        update_bill?:Date ): Observable<{} | BillComplete[]> => {

      let params: URLSearchParams = new URLSearchParams();
      params.set('id_bill', id_bill ? "" + id_bill : null);
      params.set('id_bill_father', id_bill_father ? "" + id_bill_father : null);
      params.set('id_third_employee', id_third_employee ? "" + id_third_employee : null);
      params.set('id_third', id_third ? "" + id_third : null);
      params.set('id_payment_state', id_payment_state ? "" + id_payment_state : null);
      params.set('id_bill_state', id_bill_state ? "" + id_bill_state : null);
      params.set('id_bill_type', id_bill_type ? "" + id_bill_type : null);
      params.set('consecutive', consecutive ? "" + consecutive : null);
      params.set('purchase_date', purchase_date ? "" + purchase_date : null);
      params.set('subtotal', subtotal ? "" + subtotal : null);
      params.set('tax', tax ? "" + tax : null);
      params.set('discount', discount ? "" + discount : null);
      params.set('totalprice', totalprice ? "" + totalprice : null);
      params.set('id_state_bill', id_state_bill ? "" + id_state_bill : null);
      params.set('state_bill', state_bill ? "" + state_bill : null);
      params.set('creation_bill', creation_bill ? "" + creation_bill : null);
      params.set('update_bill', update_bill ? "" + update_bill : null);
      
      
      let myOption: RequestOptions = this.options;
      myOption.search = params;
      return this.http.get(this.api_uri_bill+"/completes", this.options)
      .map((response: Response) => <BillComplete[]>response.json())
      .catch(this.handleError);
}
  
public getBillCompleteDeatilsResource = (state_bill?:number,id_bill?:number,id_bill_father?:number,
      id_third_employee?:number,
      id_third?:number,
      id_payment_state?:number,
      id_bill_state?:number,
      id_bill_type?:number,
      consecutive?:string,
      purchase_date?:Date,
      subtotal?:number,
      tax?:number,
      discount?:number,
      totalprice?:number,
      id_state_bill?:number,
      
      creation_bill?:Date,
      update_bill?:Date ): Observable<{} | any[]> => {

      let params: URLSearchParams = new URLSearchParams();
      params.set('id_bill', id_bill ? "" + id_bill : null);
      params.set('id_bill_father', id_bill_father ? "" + id_bill_father : null);
      params.set('id_third_employee', id_third_employee ? "" + id_third_employee : null);
      params.set('id_third', id_third ? "" + id_third : null);
      params.set('id_payment_state', id_payment_state ? "" + id_payment_state : null);
      params.set('id_bill_state', id_bill_state ? "" + id_bill_state : null);
      params.set('id_bill_type', id_bill_type ? "" + id_bill_type : null);
      params.set('consecutive', consecutive ? "" + consecutive : null);
      params.set('purchase_date', purchase_date ? "" + purchase_date : null);
      params.set('subtotal', subtotal ? "" + subtotal : null);
      params.set('tax', tax ? "" + tax : null);
      params.set('discount', discount ? "" + discount : null);
      params.set('totalprice', totalprice ? "" + totalprice : null);
      params.set('id_state_bill', id_state_bill ? "" + id_state_bill : null);
      params.set('state_bill', state_bill ? "" + state_bill : null);
      params.set('creation_bill', creation_bill ? "" + creation_bill : null);
      params.set('update_bill', update_bill ? "" + update_bill : null);
      
      
      let myOption: RequestOptions = this.options;
      myOption.search = params;
      return this.http.get(this.api_uri_bill+"/completes/details", this.options)
      .map((response: Response) => <any[]>response.json())
      .catch(this.handleError);
      }

      public getDetailsById=(id):Observable<any> =>{
        return this.http.get(this.api_uri_caja_detail+"?id_cierre_caja="+String(id),this.options).map(
          (response) => {return response.json()})
      }
 

 
      public postPaymentDetail = (body,id):  Observable<number[] | any> => {
        this.options.params = new URLSearchParams;
        //console.log("---------------------------------------------------")
        //console.log("this is the payment detail: ",JSON.stringify(body));
        //console.log("---------------------------------------------------")
        
        this.options.params.set('id_bill', id);
        return this.http.post(Urlbase[3]+"/payments-details",body,this.options).catch(error=>{
          this.handleError(error);
          return null;
        });
      }



      public getDirectoryById = (id):  Observable<number[] | any> => {
        this.options.params.set('id_directory', id);
        return this.http.get(Urlbase[1]+"/directories",this.options)
        .map((response: Response) => <any[]>response.json())
        .catch(this.handleError);
      }
      


      public postBillResource = (body:BillDTO, disc): Observable<number[] | any> => {
        
        let params: URLSearchParams = new URLSearchParams();
        
 
        //console.log("this is body: ",body)
        let myOption: RequestOptions = this.options;
        myOption.search = params;
        //------------------------LINEA PARA LUCHO-------------------------------
        if(disc == 1){
          this.options.params.set('id_caja', String(this.locStorage.getIdCaja()) );
        }
        //------------------------LINEA PARA LUCHO-------------------------------
        return this.http.post("http://tienda724.com:8448/v1/billing", body, this.options)
          .map((response: Response) => {






            // var legalData = body.id_third
            // var prefix = legalData[0].prefixBill;
            // var initialConsecutive = legalData[0].StartConsecutive;
            // var consecutive = prefix + "-" + initialConsecutive


            let id=+response["_body"]


    
            //responseEntry=JSON.stringify(response["_body"]);//Object.assign(,responseEntry)
            if(id){
              return id;
            }else{
    
              return null;
    
            }
          })
          .catch(this.handleError);
      }



      public putInventoryDetail = (id_bill: number, body: BillDetailIDDTO): Observable<number | any> => {
        
            return this.http.put(this.api_uri_bill + "/" + id_bill, body, { headers: this.headers })
              .map((response: Response) => {
                let id=+response["_body"]
                if (id) {
                  return id;
                } else {
                  return null;
                }
              })
              .catch(this.handleError);
          }
        
          public deleteInventoryDetail = (id_bill?: number, id_inventory?: number): Observable<Response | any> => {
            let params: URLSearchParams = new URLSearchParams();
        
            let myOption: RequestOptions = this.options;
            myOption.search = params;
        
            return this.http.delete(this.api_uri_bill + '/' + id_bill, this.options)
              .map((response: Response) => {
                if (response) {
                  return true;
                } else {
        
                  return false;
                }
              })
              .catch(this.handleError);
          }

  //----------------------------------------------------------------------------------------------
  //METODOS PARA CIERRE DE CAJA
  //----------------------------------------------------------------------------------------------

  public getCajaByIdStatus = (id):Observable<any> => {
    var state="O";
    return this.http.get("http://tienda724.com:8451/v1/close"+"/boxes?id_third_employee="+id+"&status="+state,this.options).map(
      (response) => {return response.json()})
  }

  public getCajaByIdStatus2 = (id):Observable<any> => {
    var state="O";
    //console.log("idEMPLOYEE: ",id)
    return this.http.get("http://tienda724.com:8451/v1/close"+"/boxes2?id_third_employee="+id+"&status="+state,this.options).map(
      (response) => {return response.json()})
  }

  public postBox = (box):Observable<any> =>{
    //console.log("this is box: ",box)
    return this.http.post(this.api_uri_caja,box,{headers:this.headers}).map((res)=>{
      return res["_body"]
    })
  }
 
  public postBoxDetail = (boxDetail,id):Observable<any> =>{
    this.options.params= new URLSearchParams();
    this.options.params.set('id_cierre_caja',id);
    return this.http.post(this.api_uri_caja_detail,boxDetail,this.options).map((response)=>{
      return response["_body"]
    })
  }

  public closeBox = (id,body):Observable<any> =>{
    return this.http.put(this.api_uri_caja+"/state/"+id,body,this.options).map((res)=>{
      return res["_body"]
    });
  }

  public getSalesSum = (id_cierre_caja):Observable<any> =>{
    return this.http.get("http://tienda724.com:8451/v1/close/ventasCaja?id_cierre_caja="+String(id_cierre_caja),{headers:this.headers}).map((res)=>{
      return res.json()
    })
  }

  

  //----------------------------------------------------------------------------------------------
  //METODOS PARA MUN, BRAND, CATEGORIES AND PRODUCTS
  //----------------------------------------------------------------------------------------------


  public getGeneralCategories = (): Observable<any> => {    
    var lista = this.locStorage.getTipo()
    
    var body = {
      "listaTipos" : lista
     }
     //console.log("asd",body)
     return this.http.post(this.api_uri_inv+"/get",body).map(res=>{
      return res.json()
    })
  }

  public getCategoryByThird = (id_third): Observable<any> => {
    return this.http.get(this.api_uri_inv + "/byThird?id_third="+String(id_third),this.options).map(res=>{
      return res.json()
    })
  }

  public getCategoryByThirdCategory = (id_third,category): Observable<any> => {
    return this.http.get(this.api_uri_inv + "/children/byThird?id_category_father="+String(category)+"&id_third="+String(id_third),this.options).map(res=>{
      return res.json()
    })
  }

  public getCategoryByFather = (id_category): Observable<any> => {
    return this.http.get(this.api_uri_inv+"/children?id_category_father="+String(id_category),this.options).map(res=>{
      return res.json()
    })
  }

  public getProductsByCategory = (id_category): Observable<any> => {
    return this.http.get(this.api_uri_products+"?id_category="+String(id_category),this.options).map(res=>{
      //console.log(res.json(),"theresponse on product222")
      return res.json();
    })
  }


  public getSubproductsByCategory = (id_product): Observable<any> =>{
    return this.http.get(this.api_uri_products+"/code?id_product="+String(id_product),this.options).map(res=>{
      //console.log(res.json(),"the response on product")
      return res.json();
    })
  }

  public getGenericMeassureUnits = (): Observable<any> =>{
    return this.http.get(this.api_uri_brand,this.options).map(res=>{
      return res.json();
    })
  }

  public getMeassureUnitsByFather = (id_father): Observable<any> =>{
    return this.http.get(this.api_uri_brand+"/bymu?id_measure_unit_father="+String(id_father),this.options).map(res=>{
      return res.json();
    })
  }

  public getMeassureByThird = (id_third): Observable<any> => {
    return this.http.get(this.api_uri_brand+"/bythird?id_third="+String(id_third),this.options).map(res=>{
      return res.json()
    })
  }

  public getMeassureByThirdAndFater = (id_third,id_father): Observable<any> =>{
    return this.http.get(this.api_uri_brand+"/bythirdAndmu?id_measure_unit_father="+String(id_father)+"&id_third"+String(id_third)).map(res=>{
      return res.json()
    })
  }

  public getBrands = (): Observable<any> =>{
    return this.http.get(this.api_uri_brand+"/brand").map(res=>{
      return res.json();
    })
  }

  public getBrandsByFather = (id_father): Observable<any> =>{
    return this.http.get(this.api_uri_brand+"/brand/bybrand?id_brand_father="+String(id_father),this.options).map(res=>{
      return res.json();
    })
  }

  public getBrandByThirdAndFater = (id_third,id_father): Observable<any> =>{
    return this.http.get(this.api_uri_brand+"/brand/bybrandbythird?id_brand_father="+String(id_father)+"&id_third="+String(id_third)).map(res=>{
      return res.json()
    })
  }

  public getBrandByThird = (id_third): Observable<any> => {
    return this.http.get(this.api_uri_brand+"/brand/bythird?id_third="+String(id_third),this.options).map(res=>{
      return res.json()
    })
  }

  public postMeasureUnit = (body): Observable<any> => {
    return this.http.post(this.api_uri_brand,body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },error=>{
      //console.log("there was an error",error);
    })
  }

  public postBrand = (body): Observable<any> => {
    return this.http.post(this.api_uri_brand+"/brand",body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },error=>{
      //console.log("there was an error",error);
    })
  }

  public putBrand = (body,id_brand): Observable<any> => {
    return this.http.put(this.api_uri_brand+"/brand?id_brand="+String(id_brand),body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },error=>{
      //console.log("there was an error",error);
    })
  }

  public putMeasureUnit = (body,id_measure): Observable<any> =>{
    return this.http.put(this.api_uri_brand+"?id_measure_unit="+String(id_measure),body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },error=>{
      //console.log("there was an error",error);
    })
  }

  public postCategory = (body): Observable<any> =>{
    return this.http.post("http://tienda724.com:8447/v1/categories2",body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    })
  }

  public putCategory = (body,id_category): Observable<any> =>{
    return this.http.put(this.api_uri_inv+"?id_category="+String(id_category),body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    })
  }

  public postProduct = (body): Observable<any> =>{
    return this.http.post(this.api_uri_products,body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    })
  }

  public putProduct = (body,id_product): Observable<any> =>{
    return this.http.put(this.api_uri_products+"?id_product="+String(id_product),body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    })
  }

  public postCode = (body): Observable<any> =>{
    return this.http.post(this.api_uri_products+"/code",body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    }).catch(this.handleError);
  }

  public putCode = (body,id_code): Observable<any> =>{
    return this.http.put(this.api_uri_products+"/code/update?id_code="+String(id_code),body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    })
  }     
  
  getStoresByThird = (id_third): Observable<any> =>{
    return this.http.get(this.api_uri_store+"?id_third="+String(id_third), this.options).map(res=>{
      //console.log(id_third,"id third de Chaluco")
      return res.json();
    })
  }

  getStoragesByStore = (id_store): Observable<any> => {
    return this.http.get(this.api_uri_store+"/s?id_store="+String(id_store),this.options).map(res=>{
      return res.json();
    })
  }

  getProductsByStorage = (id_storage): Observable<any> => {
    return this.http.get("http://tienda724.com:8447/v1/products2"+"/productxstorage?id_storage="+String(id_storage),this.options).map(res=>{
    //console.log("this i really wnated to know: ", res);  
    return res.json();
    })
  }

  getCategoriesByThird = (id_third): Observable<any> =>{
    return this.http.get("http://tienda724.com:8447/v1/store/s/categories?id_third="+String(id_third),this.options).map(res=>{
      return res.json();
    })
  }
  

  getBoxByStore = (id_store): Observable<any> => {
    return this.http.get(this.api_uri_store+"/b?id_store="+String(id_store),this.options).map(res=>{
      return res.json();
    })
  }

  postStore = (body): Observable<any> => {
    return this.http.post(this.api_uri_store,body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    })
  }

  putStore = (body,id_store): Observable<any> => {
    return this.http.put(this.api_uri_store+"?id_store="+String(id_store),body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    })
  }

  postBoxStore = (body): Observable<any> => {
    return this.http.post(this.api_uri_store+"/b",body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    })
  }

  putBox = (body,id_caja): Observable<any> => {
    return this.http.put(this.api_uri_store+"/b?id_caja="+String(id_caja),body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    })
  }

  postStorage = (body): Observable<any> => {
    return this.http.post(this.api_uri_store+"/s",body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    })
  }

  putStorage = (body,id_storage): Observable<any> => {
    return this.http.put(this.api_uri_store+"/s?id_store="+id_storage,body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    })
  }

  getBoxDetail = (id_cierre_caja): Observable<any> =>{
    return this.http.get(this.api_uri_caja+"/boxdetail?id_cierre_caja="+String(id_cierre_caja),this.options).map(res=>{
      return res.json();
    })
  }

  getBoxMaster = (id_cierre_caja): Observable<any> =>{
    return this.http.get(this.api_uri_caja+"/boxmaster?id_cierre_caja="+String(id_cierre_caja),this.options).map(res=>{
      console.log("STUFF TO TEST,",res)
      return res.json();
    })
  }


  getProductsByCategoryThird = (id_category,id_third): Observable<any> =>{
    return this.http.get("http://tienda724.com:8447/v1/categories2"+"/productsOnStore?id_third="+String(id_third)+"&id_category="+id_category+"&id_store="+this.locStorage.getIdStore()).map(res=>{
      //console.log("THIS IS DATA I REALLY NEED: ",res)
    return res.json();
    },e=>{
      //console.log(e);
    })
  }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert("Se encontro un error de conexion. Favor haga click en el boton Recargar y vuelva a intentar.");
    return null;//Observable.throw(errMsg);
  }

}
