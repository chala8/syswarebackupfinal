import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '../../../../../../../shared/localStorage';
import { DatePipe } from '@angular/common';
import { BillingService } from '../../billing.service';
import { Http, Headers } from '@angular/http';
import { MatTabChangeEvent, MatDialog } from '@angular/material';
import { PedidosDetailComponent } from '../pedidos-detail/pedidos-detail.component'
import { PedidosDetail2Component } from '../pedidos-detail2/pedidos-detail2.component'
import { StatechangeComponent } from '../statechange/statechange.component'
import { NotesModalComponent } from '../notes-modal/notes-modal.component';
declare var $: any;

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss']
})




export class PedidosComponent implements OnInit {

  constructor(private datePipe: DatePipe, public dialog: MatDialog, public locStorage: LocalStorage,private categoriesService: BillingService,private http2: HttpClient) { }

  ngOnInit() {
    this.getVehicles();
    this.getStores();
    this.id_store = this.locStorage.getIdStore();
    console.log("THIS IS MY ID STORE: ",this.id_store)
  }

  getPlanillaList(){
    this.ListPlanilla = [];
    this.ListVehicles.forEach(element => {
      
      this.http2.get("http://tienda724.com:8447/v1/pedidos/planillas?idvehiculo="+element.id_VEHICULO+"&idstore="+this.locStorage.getIdStore()).subscribe(response => {
        
        //@ts-ignore
        response.forEach(element => {
          this.isPlanillaFull= true
          this.ListPlanilla.push(element);
        });
      })
    });
    
  }


  getVehicles(){

    this.http2.get("http://tienda724.com:8447/v1/pedidos/vehiculos").subscribe(response => {
      this.ListVehicles = response;
      this.SelectedVehicle = response[0].id_VEHICULO;
    })

  }

  exportarPdf(elem){
    this.http2.get("http://tienda724.com:8447/v1/pedidos/planillaDetail?idplanilla="+elem.id_PLANILLA).subscribe(response => {
      this.http2.post("http://tienda724.com:8447/v1/pedidos/pdf",{
        master: elem,
        detalles: response
      },{responseType: 'text'}).subscribe(answer => {
        console.log("THIS IS RESPONSE: ",answer)
        window.open("http://tienda724.com/planillas/"+answer, "_blank");
      })
    })
  }

  openClosePanilla(idplanilla){

    var dialogRef
      dialogRef = this.dialog.open(NotesModalComponent,{
        height: '275px',
        width: '850px',
        disableClose: true
      })
      dialogRef.afterClosed().subscribe(result => {

        if(result.resp){
          this.http2.put("http://tienda724.com:8447/v1/pedidos/closeplanilla?observaciones="+result.notes+"&idplanilla="+idplanilla,{}).subscribe(answer => {
            console.log("THIS IS ANSWER: ",1);
            this.getPlanillaList()
          })
        }
      });

  }
  
confirmarEnv(elem) {

  this.http2.post("http://tienda724.com:8447/v1/store/confirmarPC?numpedido="+elem.numdocumento+"&idbilltype=46&idstore="+this.locStorage.getIdStore(),{}).subscribe(
    response => {
      this.http2.post("http://tienda724.com:8447/v1/store/confirmarPC?numpedido="+elem.numdocumento+"&idbilltype=47&idstore="+this.locStorage.getIdStore(),{}).subscribe(
        response => {
           //  this.http2.put("http://tienda724.com:8448/v1/pedidos/billstate?billstate=81&billid="+elem.id_BILL,null).subscribe(a=> {
           this.getRepProdList()
           //  })
      })
    })
}
   
  id_store;
  
  type2 = "1";
  SelectedStore2;
  isListProdFull2=false;
  ListReportProd2;
  dateP12;
  dateP22;
  hours2=24;

  checked = false;
  Stores = [];
  SelectedStore = this.locStorage.getIdStore();
  SelectedBillType = '87';
  SelectedBillState = '61';
  isListProdFull = false;
  ListReportProd;
  ListChecked=[];

  SelectedVehicle = "";
  ListVehicles;
  isPlanillaFull = false;
  ListPlanilla=[];

  getStores() {
    this.categoriesService.getStoresByThird(this.locStorage.getThird().id_third).subscribe(data => { 
        console.log(data);this.Stores = data 
        this.SelectedStore =  this.locStorage.getIdStore();})
  }

  getRepProdList(){
    this.ListChecked = [];
    console.log("http://tienda724.com:8448/v1/pedidos/master?id_store="+this.SelectedStore+"&id_bill_state="+this.SelectedBillState+"&id_bill_type="+this.SelectedBillType)
    this.http2.get("http://tienda724.com:8448/v1/pedidos/master?id_store="+this.SelectedStore+"&id_bill_state="+this.SelectedBillState+"&id_bill_type="+this.SelectedBillType).subscribe(
        data => {
          console.log("THIS IS DATA: ",data)
          this.ListReportProd = data;
          this.isListProdFull = true;
          this.ListReportProd.forEach(element => {
            console.log()
            this.ListChecked.push(false);
          });
        }
    )
  }

  allChecked(el){
    if(el){
      for(let i=0; i< this.ListChecked.length;i++){
        this.ListChecked[i]=true;
      }
    }
      
  }

  cleanList(){
    this.ListReportProd = []
    this.ListChecked = []
  }

  cleanList2(){
    this.ListReportProd = []
    this.ListChecked = []
    if(this.SelectedBillType=='1' || this.SelectedBillType=='2' || this.SelectedBillType=='3' || this.SelectedBillType=='4' )
      { this.SelectedBillState = '1' }
    if(this.SelectedBillType=='86')
      { this.SelectedBillState = '62' }
    if(this.SelectedBillType=='87')
      { this.SelectedBillState = '61' }
    if(this.SelectedBillType=='88')
      { this.SelectedBillState = '1' }
    if(this.SelectedBillType=='89')
      { this.SelectedBillState = '65' }  
    if(this.SelectedBillType=='90')
      { this.SelectedBillState = '67' }
  }

  showList(){
    this.checked = false;
  }

  goToSubMenu(element){
    var dialogRef
      dialogRef = this.dialog.open(PedidosDetailComponent,{
        height: '500px',
        width: '850px',
        data: {
          elem: element
          }
      })
  }

  getPicking(){
    let billList = [];

    for(let i = 0; i < this.ListChecked.length; i++){
      if(this.ListChecked[i]){
        billList.push(this.ListReportProd[i].id_BILL);
        console.log(billList);
      }
      if(i+1==this.ListChecked.length){
        var dialogRef
      dialogRef = this.dialog.open(PedidosDetail2Component,{
        height: '500px',
        width: '900px',
        data: {
          elem: billList
          }
      })
      }
    }
    
  }


  changeState(data){
    var dialogRef
    dialogRef = this.dialog.open(StatechangeComponent,{
      height: '500px',
      width: '850px',
      data: {
        elem: data
        }
    }).afterClosed().subscribe(res=>{
      this.getRepProdList()
    })
  }




  dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        /* next line works with strings and numbers, 
         * and you may want to customize it to your needs
         */
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
  }
  
  getRepProdList2(){
    console.log("http://tienda724.com:8448/v1/reorder2?id_third="+this.locStorage.getThird().id_third+"&id_store="+this.id_store)
    this.http2.get("http://tienda724.com:8448/v1/reorder/reorder2?id_third="+this.locStorage.getThird().id_third+"&id_store="+this.id_store+"&hours="+this.hours2).subscribe(
        data => {
          console.log("THIS IS DATA: ",data)
          if(this.type2=="1"){
            //@ts-ignore
            this.ListReportProd2 = data.sort(this.dynamicSort("linea"));
          }
          if(this.type2=="2"){
            //@ts-ignore
            this.ListReportProd2 = data.sort(this.dynamicSort("categoria"));
          }
          if(this.type2=="3"){
            //@ts-ignore
            this.ListReportProd2 = data.sort(this.dynamicSort("marca"));
          }
            this.isListProdFull2 = true;
        }
    )
  }
  
  transformDate(date){
    return this.datePipe.transform(date, 'yyyy/MM/dd');
    }
  
  genPedido(){
    this.showNotification('top', 'center', 3, "<h3>El pedido se esta generando, por favor espera a la notificacion de <b>EXITO</b></h3> ", 'info')
    try{
      console.log("THIS IS JSON, ", JSON.stringify({reorder: this.ListReportProd2,idstore: this.id_store}))
    this.http2.post("http://tienda724.com:8448/v1/pedidos/detailing",{reorder: this.ListReportProd2,idstore: this.id_store},{responseType: 'text'}).subscribe(
      response => {
        this.showNotification('top', 'center', 3, "<h3>El pedido se realizo con <b>EXITO</b></h3> ", 'success')
      })

    }catch(e){
      this.showNotification('top', 'center', 3, "<h3>El pedido presento <b>PROBLEMAS</b></h3> ", 'danger')
    }
  }
  

    
  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: typeStr ? typeStr : type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }







}
