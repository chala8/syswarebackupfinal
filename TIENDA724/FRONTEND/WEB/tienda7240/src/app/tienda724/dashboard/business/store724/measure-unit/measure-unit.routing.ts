import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { MeasureUnitListComponent } from './measure-unit-list/measure-unit-list.component';
import { MeasureUnitNewComponent } from './measure-unit-new/measure-unit-new.component';
import { MeasureUnitEditComponent } from './measure-unit-edit/measure-unit-edit.component'


/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/

export const MeasureUnitRouting: Routes = [
  
    { path: 'measure-unit', component: null,
      children: [
          { path: '', redirectTo: 'list', pathMatch: 'full'},
          { path: 'list', component: MeasureUnitListComponent},
          { path: 'new', component: MeasureUnitNewComponent},
          { path: 'edit/:id', component: MeasureUnitEditComponent}
  
      ]
    }
  
  ]

