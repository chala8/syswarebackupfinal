import { Component,Inject, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormArray,FormBuilder, Validators } from '@angular/forms';

import * as _ from 'lodash';
/*
*    Material modules for component
*/
import {MatTabChangeEvent, VERSION} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

/*
*     others component
*/
/*
*     models for  your component
*/
import { Attribute} from '../../../models/attribute'
import { AttributeValue} from '../../../models/attributeValue'

// DTO's

import { AttributeDTO} from '../../../models/attributeDTO'
import { AttributeValueDTO} from '../../../models/attributeValueDTO'

import { CommonStateStoreDTO } from '../../../../commons/CommonStateStoreDTO'
import { CommonStoreDTO } from '../../../../commons/CommonStoreDTO'

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../../../shared/localStorage'
import { AttributeService } from '../../../attribute.service';

/*
*     constant of  your component
*/
declare var $ :any

@Component({
  selector: 'app-dialog-attribute',
  templateUrl: './dialog-attribute.component.html',
  styleUrls: ['./dialog-attribute.component.scss']
})

export class DialogAttributeComponent implements OnInit {
  
  form_attr: FormGroup;
  form_attr_val: FormGroup;
  isValue=false;

  attributeDTO:AttributeDTO;
  attributeValueDTO:AttributeValueDTO;
  commonStateStoreDTO:CommonStateStoreDTO;
  common:CommonStoreDTO;
  isAddOnlyAttribute:Attribute;

  constructor(
    public dialogRef: MatDialogRef<DialogAttributeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder,
    public attributeService: AttributeService) { 
      
      this.isAddOnlyAttribute=this.data.attributeTemp
      console.log("DATA -> " , this.isAddOnlyAttribute )
      this.common=new CommonStoreDTO(null,null)
      this.commonStateStoreDTO= new CommonStateStoreDTO(1,null,null)
      this.attributeDTO= new AttributeDTO(this.common,this.commonStateStoreDTO)
      this.attributeValueDTO= new AttributeValueDTO(this.common,this.commonStateStoreDTO)
    }

    addAttributeValue(){
      console.log("CREAR VALORES")
    }

    createAttributeControls() {
      this.form_attr = this.fb.group({
        name: ['', Validators.compose([
          Validators.required
        ])],
        description: ['', Validators.compose([
          
        ])]
      });
    }

  createAttributeValueControls(){
      this.form_attr_val = this.fb.group({
        name: ['', Validators.compose([
          Validators.required
        ])],
        description: ['', Validators.compose([
          
        ])]
      });

    }



  onNoClick(): void {
    this.dialogRef.close();
  }

  

  ngOnInit() {
    this.createAttributeControls()
    this.createAttributeValueControls();
  }

  save(){

    if(!this.isAddOnlyAttribute.id_attribute){

       //1. create Attribute
    this.commonStateStoreDTO.state=1;
    this.common.name=this.form_attr.value['name'];
    this.common.description=this.form_attr.value['description'];

    this.attributeDTO.common=this.common;
    this.attributeDTO.state=this.commonStateStoreDTO;

    this.attributeService.postAttribute(this.attributeDTO)
    .subscribe(
      result => {
       
        if (result) {
          console.log("RESPONSE 1 ",result)

          if(this.isValue){
            //2. create Attribute Value
            this.createValue(result);
           
          }else{
            this.dialogRef.close();
            
          }
         
        }
      })

    }else{
      this.createValue(this.isAddOnlyAttribute.id_attribute);
    }
   

      
  }

  createValue(id_attribute:number){
   
    this.commonStateStoreDTO.state=1;
    this.common.name=this.form_attr_val.value['name']
    if( this.common.name!==null && this.common.name.length>0){
      this.common.description=''+this.form_attr_val.value['description']
      this.attributeValueDTO.common=this.common;
      this.attributeValueDTO.state=this.commonStateStoreDTO;


      console.log(this.attributeValueDTO)
      let attributeVal:AttributeValueDTO[]=[];
      attributeVal.push(this.attributeValueDTO);
      this.attributeService.postAttributeValue(id_attribute,attributeVal)

      .subscribe(
        result => {
         
          if (result) {
            console.log("RESPONSE 1 ",result)
  
          
              //2. create Attribute Value
              this.dialogRef.close();
            
           
          }
        })
    }else{
      this.dialogRef.close();
    }
   
  }



}
