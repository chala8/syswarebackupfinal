import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReordenComponent } from './reorden.component';

describe('ReordenComponent', () => {
  let component: ReordenComponent;
  let fixture: ComponentFixture<ReordenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReordenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReordenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
