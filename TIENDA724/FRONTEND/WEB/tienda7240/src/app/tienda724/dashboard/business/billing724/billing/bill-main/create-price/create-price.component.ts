import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { LocalStorage } from 'app/shared/localStorage';

@Component({
  selector: 'app-create-price',
  templateUrl: './create-price.component.html',
  styleUrls: ['./create-price.component.scss']
})
export class CreatePriceComponent implements OnInit {

  psName;
  psOwn;


  priceName = "";
  price = "";
  ps = "";
  idps;
  ListPrice


  constructor(public locStorage: LocalStorage,public dialogRef: MatDialogRef<CreatePriceComponent>, private http2: HttpClient, @Inject(MAT_DIALOG_DATA) public data: DialogData) { }


  ngOnInit() {
    this.psName = this.data.psName;
    this.psOwn = this.data.psOwn;
    this.http2.get("http://tienda724.com:8447/v1/store/psid?&code="+this.psOwn+"&id_store="+this.locStorage.getIdStore()).subscribe(response => {
      console.log("this is the faqin response: ", response);
      this.idps = response;
    this.http2.get("http://tienda724.com:8447/v1/resource/pricebyps?id_ps="+response).subscribe(resp => {
      this.ListPrice = resp
    })
  })
  } 



  postPrice() {
    this.http2.get("http://tienda724.com:8447/v1/store/psid?name="+this.psName+"&code="+this.psOwn+"&id_store="+this.locStorage.getIdStore()).subscribe(resp => {
      //@ts-ignore
      this.http2.post("http://tienda724.com:8447/v1/store/pricelist",null, {params:{
      "id_product_store":resp,
      "price_description":this.priceName,
      "price":this.price
    }}).subscribe(resp => {
      this.http2.get("http://tienda724.com:8447/v1/resource/pricebyps?id_ps="+this.idps).subscribe(resp => {
      this.ListPrice = resp
    })
  }, error => {
    this.http2.get("http://tienda724.com:8447/v1/resource/pricebyps?id_ps="+this.idps).subscribe(resp => {
      this.ListPrice = resp
    })
    })
  })
  }


  deletePrice(price){
    this.http2.delete("http://tienda724.com:8447/v1/price-list/delete?idps="+this.idps+"&price="+price).subscribe(resp => {
      this.http2.get("http://tienda724.com:8447/v1/resource/pricebyps?id_ps="+this.idps).subscribe(resp => {
        this.ListPrice = resp
      })
    })

  }

}


export interface DialogData {
  psName: any;
  psOwn: any;
}