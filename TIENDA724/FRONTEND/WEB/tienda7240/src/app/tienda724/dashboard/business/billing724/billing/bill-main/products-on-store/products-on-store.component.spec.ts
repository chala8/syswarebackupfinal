import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsOnStoreComponent } from './products-on-store.component';

describe('ProductsOnStoreComponent', () => {
  let component: ProductsOnStoreComponent;
  let fixture: ComponentFixture<ProductsOnStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsOnStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsOnStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
