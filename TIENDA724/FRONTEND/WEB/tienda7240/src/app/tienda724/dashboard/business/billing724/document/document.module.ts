import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../../app.material';
/*
************************************************
*     principal component
*************************************************
*/



/*
************************************************
*     modules of  your app
*************************************************
*/



/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { DocumentService } from './document.service';
import { Document } from '../../store724/commons/document';
/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  exports:[],
  providers:[DocumentService]
})
export class DocumentModule { }
