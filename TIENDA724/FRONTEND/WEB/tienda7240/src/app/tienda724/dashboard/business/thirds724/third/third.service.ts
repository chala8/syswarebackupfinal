import { Injectable } from '@angular/core';

import { Http, Headers, Response, URLSearchParams,RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';
import { Third } from './models/third'

import { ThirdDTO } from './models/thirdDTO'

declare var $: any;

@Injectable()
export class ThirdService {

  
  third: Third;
  api_uri = Urlbase[1] + '/thirds';
  persons = Urlbase[1] + '/persons';
  api_uri_legal_data = Urlbase[2] + '/legaldata'
  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');

    this.options = new RequestOptions({headers:this.headers});


  }

  public getThirdList = (id_third?:number,id_third_father?:number,document_type?:number,document_number?:string
    ,id_doctype_person?:number,doc_person?:string,id_third_type?:number,state_third?:number,id_person?:number): Observable<{}|Third[]> => {


    let params: URLSearchParams = new URLSearchParams();
    params.set('id_third',  id_third?""+id_third:null);
    params.set('id_third_father',  id_third_father?""+id_third_father:null);
    
    params.set('id_typedoc_third', document_type?""+document_type:null);
    params.set('doc_third', document_number?""+document_number:null);
    params.set('id_third_type', id_third_type?""+id_third_type:null);
    params.set('id_doctype_person', id_doctype_person?""+id_doctype_person:null);
    params.set('state_third', state_third?""+state_third:null);
    params.set('doc_person', doc_person?""+doc_person:null);
    params.set('id_person',id_person?""+id_person:null)

    let myOption: RequestOptions = this.options;
    myOption.search = params;
    return this.http.get(this.api_uri, this.options )
      .map((response: Response) => <Third[]>response.json())
      .catch(this.handleError);
  }

  public getPersonList = (id_doctype_person?:number,doc_person?:string): Observable<{}|any[]> => {


    let params: URLSearchParams = new URLSearchParams();
    
    params.set('id_doctype_person', id_doctype_person?""+id_doctype_person:null);
    
    params.set('doc_person', doc_person?""+doc_person:null);
    

    let myOption: RequestOptions = this.options;
    myOption.search = params;
    return this.http.get(this.persons, this.options )
      .map((response: Response) => <any[]>response.json())
      .catch(this.handleError);
  }

  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: typeStr ? typeStr : type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }


  public postThird = (body: ThirdDTO): Observable<Boolean|any> => {

    //console.log('this is body',JSON.stringify(body));
    
    // return this.http.post(this.api_uri , body, { headers: this.headers })
      //console.log("this is header: ", this.headers);
      return this.http.post(this.api_uri, body, { headers: this.headers })
        .map((response: Response) => {
          //console.log("this is the new third response: ",response);
              if(response){                
                this.showNotification('top', 'center', 3, "<h3>Se ha Registrado el usuario, adecuadamente!</b></h3> ", 'info')
                return {third_id: response['_body']};
              }else{

            return false;
          }
        })
        .catch(this.handleError);
  }

  public Delete = (id_third: number): Observable<Response|any> => {
    return this.http.delete(this.api_uri +'/'+ id_third,this.options)
    .map((response: Response) => {
      if(response){
        return true;
      }else{

    return false;
      }})
        .catch(this.handleError);
}

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return null;//Observable.throw(errMsg);
  }

  public asignLegalDataToThird = (id_third,id_legal_data): Observable<any> => {
    return this.http.put(this.api_uri_legal_data+"/third?id_third="+String(id_third)+"&id_legal_data="+String(id_legal_data),null,{headers:this.headers}).map(res=>{
      return res;
    },e=>{
      //console.log(e);
    })
  }

  public postLegalData = (body): Observable<any> => {
    return this.http.post(this.api_uri_legal_data,body,{headers:this.headers}).map(res=>{
      return res;
    },e=>{
      //console.log(e);
    })
  }

  public getLegalData = (id_legal_data): Observable<any> => {
    return this.http.get(this.api_uri_legal_data+"?id_legal_data="+String(id_legal_data),this.options).map(res=>{
      return res;
    },e=>{
      //console.log(e)
    })
  }

  public getIdLegalDataByThird = (id_third): Observable<any> => {
    return this.http.get("http://10.10.11.52:8447/v1/legaldata/third?id_third="+String(id_third),this.options).map(res=>{
      return res;
    },e=>{
      //console.log(e)
    })
  }

  public putLegalData = (id_legal_data,body): Observable<any> => {
    return this.http.put(this.api_uri_legal_data+"?id_legal_data="+String(id_legal_data),body,{headers:this.headers}).map(res=>{
      //console.log(res)
    },e=>{
      //console.log(e)
    })
  }

}
