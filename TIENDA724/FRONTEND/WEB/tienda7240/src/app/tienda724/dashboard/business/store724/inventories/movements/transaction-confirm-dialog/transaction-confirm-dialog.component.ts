import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-transaction-confirm-dialog',
  templateUrl: './transaction-confirm-dialog.component.html',
  styleUrls: ['./transaction-confirm-dialog.component.scss']
})
export class TransactionConfirmDialogComponent {
  public observations='';

  constructor(
    public dialogRef: MatDialogRef<TransactionConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

    }

  onNoClick(): void {
    this.dialogRef.close();
  }
  returnData(){
    this.dialogRef.close({
      observations:this.observations
    });
  }

}
export interface DialogData {
  productsQuantity: number;
}