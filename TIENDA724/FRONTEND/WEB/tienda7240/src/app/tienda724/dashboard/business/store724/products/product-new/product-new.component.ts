import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';
/*
*    Material modules for component
*/
/*
*     others component
*/
/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { ProductDTO } from '../models/productDTO'
import { CommonStateStoreDTO } from '../../commons/CommonStateStoreDTO'
import { CommonStoreDTO } from '../../commons/CommonStoreDTO'

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { ProductsService } from '../products.service';
/*
*     constant of  your component
*/
@Component({
  selector: 'app-product-new',
  templateUrl: './product-new.component.html',
  styleUrls: ['./product-new.component.scss']
})
export class ProductNewComponent implements OnInit {

  form: FormGroup;
  token:Token;
  newProduct:ProductDTO;
  commonStateStoreDTO:CommonStateStoreDTO;
  common:CommonStoreDTO;

   //attributes
   CURRENT_ID_THIRD = 0;
   CURRENT_ID_CATEGORY = 0;

  constructor(private router: Router, private route:ActivatedRoute ,private categoriesService: ProductsService,
              private fb: FormBuilder, private locStorage: LocalStorage) {

                this.common=new CommonStoreDTO(null,null)
                this.commonStateStoreDTO= new CommonStateStoreDTO(1,null,null)
                this.newProduct=new ProductDTO(null,1,null,null,this.common,this.commonStateStoreDTO)
              }

  ngOnInit() {
    this.createControls();

    let session=this.locStorage.getSession();
    if(!session){
        this.Login();
    }else{
      
      this.route.queryParams
          .subscribe(params => {

            // Defaults to 0 if no query param provided.  
              this.token=this.locStorage.getToken();
      
              this.CURRENT_ID_CATEGORY=params.id_category;
              //this.CURRENT_ID_CATEGORY=42;
              
              
            this.loadData()         
        });
    }
  }

  Login() {
    let link = ['/auth'];
    this.router.navigate(link);
  }


  createControls() {
    this.form = this.fb.group({

      //profile
      img_url: ['', Validators.compose([
        
      ])],
      name: ['', Validators.compose([
        Validators.required
      ])],
      description: ['', Validators.compose([

      ])],

      stock: ['', Validators.compose([
       
      ])],

      stock_min: ['', Validators.compose([
        
      ])]

    });
  }


  loadData() {
    this.form.patchValue({
    
      img_url: "Colombia",
      name: "Bogota",
      description:"Descripción"
    });
  
  }


  createNewProduct() {

    this.newProduct.id_category= this.CURRENT_ID_CATEGORY >0? this.CURRENT_ID_CATEGORY:null;

    this.common.name= this.form.value["name"];
    this.common.description= this.form.value["description"];
    
    this.commonStateStoreDTO.state=1;
    this.newProduct.img_url= this.form.value["img_url"];

    this.newProduct.common=this.common;
    this.newProduct.state=this.commonStateStoreDTO;

    console.log("PRO ",this.newProduct)
    this.categoriesService.postProduct(this.newProduct)
    .subscribe(
    result => {
     
      if (result === true) {
         this.resetForm();
         this.goBack();
       
        return;
      } else {

        //this.openDialog();
        return;
      }
    })
  }

  resetForm() {
    
    this.form.reset();
    
  }

  goBack() {
    let link = ['/dashboard/business/product'];
    this.router.navigate(link);
  }


}
