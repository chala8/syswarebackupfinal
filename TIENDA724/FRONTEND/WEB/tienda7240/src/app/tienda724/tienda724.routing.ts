import { Routes } from '@angular/router';


import { Tienda724Component } from './tienda724.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { UserProfileComponent } from '../user-profile/user-profile.component';
import { TableListComponent } from '../table-list/table-list.component';
import { TypographyComponent } from '../typography/typography.component';
import { IconsComponent } from '../icons/icons.component';
import { MapsComponent } from '../maps/maps.component';
import { NotificationsComponent } from '../notifications/notifications.component';
import { UpgradeComponent } from '../upgrade/upgrade.component';


/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/
import { BusinessRouting } from './dashboard/business/business.routing';
import { MyRouting } from './my/my.routing';
import { NewThirdComponent } from './dashboard/business/thirds724/third/new-third/new-third.component';


export const Tienda724Routing: Routes= [
    ...MyRouting,
    { path: 'dashboard', component: Tienda724Component,
        children: [
          { path: '', redirectTo: 'business', pathMatch: 'full'},
          ...BusinessRouting,
          //{ path: 'dashboard',      component: DashboardComponent },
          { path: 'user-profile',   component: UserProfileComponent },
          { path: 'table-list',     component: TableListComponent },
          { path: 'typography',     component: TypographyComponent },
          { path: 'icons',          component: IconsComponent },
          { path: 'maps',           component: MapsComponent },
          { path: 'notifications',  component: NotificationsComponent },
          { path: 'upgrade',        component: UpgradeComponent },
          { path: "new-third",      component: NewThirdComponent }
        ]
    }

];
