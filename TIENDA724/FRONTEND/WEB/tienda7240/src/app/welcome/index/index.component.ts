import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthenticationService } from '../../authentication/authentication.service';

import { LocalStorage } from '../../shared/localStorage';
import { Token } from '../../shared/token';
import { Person } from '../../shared/models/person';

declare var $: any;

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  user=false;
  token:Token;
  person:Person;

  constructor(private authService:AuthenticationService,
    private router:Router,
    private locStorage:LocalStorage) { }

  ngOnInit() {
    this.user=false;
 
    let session=this.locStorage.getSession();
    if(!session){
      /**
      @todo Eliminar comentario para
      */
    }else{
      this.token=this.locStorage.getToken();
      this.person= this.locStorage.getPerson();
      this.token=this.locStorage.getToken();
      



    }
  }


  logout() {
    this.locStorage.cleanSession();
    this.token=null;
    this.goIndex();
    this.showNotification('top','right');
 
   }
 
   goIndex() {
     let link = ['/'];
     this.router.navigate(link);
   }



   showNotification(from, align){
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: "Usted <b>Cerro Sesión</b> de forma satisfactoria."

    },{
        type: type[2],
        timer: 200,
        placement: {
            from: from,
            align: align
        }
    });
  }

}
