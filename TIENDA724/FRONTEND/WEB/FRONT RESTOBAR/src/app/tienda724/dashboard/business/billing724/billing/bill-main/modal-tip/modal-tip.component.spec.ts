import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTipComponent } from './modal-tip.component';

describe('ModalTipComponent', () => {
  let component: ModalTipComponent;
  let fixture: ComponentFixture<ModalTipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
