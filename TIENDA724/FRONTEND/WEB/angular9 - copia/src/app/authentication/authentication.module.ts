import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import 'hammerjs';

import {LoginComponent} from './login/login.component';
import {AuthenticationComponent} from './authentication.component';

import {NotAuthGuard} from './not-auth.guard';

import {AuthenticationRouting} from './authentication.routing';


@NgModule({
  imports: [
    RouterModule,
    ReactiveFormsModule,
    AuthenticationRouting
  ],
  declarations: [LoginComponent, AuthenticationComponent],
  providers: [ NotAuthGuard ],
exports: [ LoginComponent ]
})
export class AuthenticationModule { }
