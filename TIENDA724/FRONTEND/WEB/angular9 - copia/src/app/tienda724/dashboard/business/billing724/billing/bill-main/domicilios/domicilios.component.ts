import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { PedidosDomiciliosCanceladosComponent } from '../pedidos-domicilios-cancelados/pedidos-domicilios-cancelados.component';
import { PedidosDomiciliosRecibidosComponent } from '../pedidos-domicilios-recibidos/pedidos-domicilios-recibidos.component';
import { PedidosDomiciliosProcesadosSComponent } from '../pedidos-domicilios-procesados-s/pedidos-domicilios-procesados-s.component';
import { PedidosDomiciliosProcesadosNComponent } from '../pedidos-domicilios-procesados-n/pedidos-domicilios-procesados-n.component';
import { PedidosDomiciliosEncaminoComponent } from '../pedidos-domicilios-encamino/pedidos-domicilios-encamino.component';
import { PedidosDomiciliosEntregadosSComponent } from '../pedidos-domicilios-entregados-s/pedidos-domicilios-entregados-s.component';
import { PedidosDomiciliosEntregadosNComponent } from '../pedidos-domicilios-entregados-n/pedidos-domicilios-entregados-n.component';
import { PedidosFacturadosComponent } from '../pedidos-facturados/pedidos-facturados.component';
import { PedidosDomiciliosFinalizadosSComponent } from '../pedidos-domicilios-finalizados-s/pedidos-domicilios-finalizados-s.component';
import { PedidosDomiciliosFinalizadosNComponent } from '../pedidos-domicilios-finalizados-n/pedidos-domicilios-finalizados-n.component';

@Component({
  selector: 'app-domicilios',
  templateUrl: './domicilios.component.html',
  styleUrls: ['./domicilios.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DomiciliosComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @ViewChild("cancelados") compUpdate: PedidosDomiciliosCanceladosComponent;
  @ViewChild("recibidos") compUpdate2: PedidosDomiciliosRecibidosComponent;
  @ViewChild("procesadosSinNovedad") compUpdate3: PedidosDomiciliosProcesadosSComponent;
  @ViewChild("procesadosNovedad") compUpdate4: PedidosDomiciliosProcesadosNComponent;
  @ViewChild("entregadoSinNovedad") compUpdate5: PedidosDomiciliosEntregadosSComponent;
  @ViewChild("entregadoConNovedad") compUpdate6: PedidosDomiciliosEntregadosNComponent;
  @ViewChild("enCamino") compUpdate7: PedidosDomiciliosEncaminoComponent;
  @ViewChild("facturados") compUpdate8: PedidosFacturadosComponent;
  @ViewChild("finalizadoSinNovedad") compUpdate9: PedidosDomiciliosFinalizadosSComponent;
  @ViewChild("finalizadoConNovedad") compUpdate10: PedidosDomiciliosFinalizadosNComponent;


  SelectedTabIndexGlobal = 0;
  SelectedTabIndexProcesados = 0;
  SelectedTabIndexEntregados = 0;
  SelectedTabIndexFinalizados=0;

  setTabIndexGlobal(index){
    this.SelectedTabIndexGlobal = index;
  }

  setTabIndexProcesados(index){
    this.SelectedTabIndexProcesados = index;
  }

  setTabIndexEntregados(index){
    this.SelectedTabIndexEntregados = index;
  }

  setTabIndexFinalizados(index){
    this.SelectedTabIndexFinalizados = index;
  }


 
}
