import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidosDomiciliosEncaminoComponent } from './pedidos-domicilios-encamino.component';

describe('PedidosDomiciliosEncaminoComponent', () => {
  let component: PedidosDomiciliosEncaminoComponent;
  let fixture: ComponentFixture<PedidosDomiciliosEncaminoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidosDomiciliosEncaminoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidosDomiciliosEncaminoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
