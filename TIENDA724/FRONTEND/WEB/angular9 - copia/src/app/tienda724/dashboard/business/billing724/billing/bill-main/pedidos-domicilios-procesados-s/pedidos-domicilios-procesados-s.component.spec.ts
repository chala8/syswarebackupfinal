import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidosDomiciliosProcesadosSComponent } from './pedidos-domicilios-procesados-s.component';

describe('PedidosDomiciliosProcesadosSComponent', () => {
  let component: PedidosDomiciliosProcesadosSComponent;
  let fixture: ComponentFixture<PedidosDomiciliosProcesadosSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidosDomiciliosProcesadosSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidosDomiciliosProcesadosSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
