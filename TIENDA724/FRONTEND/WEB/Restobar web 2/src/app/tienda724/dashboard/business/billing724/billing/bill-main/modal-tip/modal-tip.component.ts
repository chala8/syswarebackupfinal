import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogData } from '../pedidos-detail/pedidos-detail.component';

@Component({
  selector: 'app-modal-tip',
  templateUrl: './modal-tip.component.html',
  styleUrls: ['./modal-tip.component.css']
})
export class ModalTipComponent implements OnInit {

  cantidadDinero;
  porcentaje;
  tip= 10;
  total;
  tipType= 0;

  constructor(public dialogRef: MatDialogRef<ModalTipComponent>,public dialog: MatDialog
    ,@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    console.log(this.data)
    this.total = this.data.total;
  }

  sendCloseTable(){
    this.dialogRef.close({
      cantidadDinero: this.cantidadDinero,
      tip: this.tip
    })
  }

  setCerrarMesa1(val:any){
    this.cantidadDinero = (val*this.total)/100;
  }

  setCerrarMesa2(val:any){
    this.porcentaje =(val*100)/this.total;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
