import {Injectable} from '@angular/core';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';
import {from, Observable} from 'rxjs';
/** Files for auth process  */
import {Urlbase} from '../shared/urls';
import {LocalStorage} from './localStorage';
import {Auth} from '../authentication/auth';
import {UsuarioDTO} from '../shared/models/auth/UsuarioDTO';

import {Token} from '../shared/token';
import {Session} from '../shared/session';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CPrint} from '../shared/util/CustomGlobalFunctions';

//import { Http, Headers, Response, URLSearchParams } from '@angular/http';


@Injectable({
  providedIn:'root'
})
export class AuthenticationService {
  username: string;
  loggedIn: boolean;
  token: Token;
  session: Session;
  urlAuth = Urlbase[0] + '/usuarios';
  private options;
  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(private http: HttpClient, private locStorage: LocalStorage) { }

  public login = (auth: Auth): Observable<{}|Boolean> => {
    let promesa = new Promise((resolve, reject) => {
      this.http.post<any>(Urlbase[0] + '/login?id_aplicacion=' + 21 + '&dispositivo=WEB', auth, { headers: this.headers})
        .subscribe((value) => {
          this.token = value;
          this.session = value;
          if (this.session.person) {
            localStorage.setItem('currentUserPersonStore724', JSON.stringify(this.session.person));
          }

          localStorage.setItem('currentUserSessionStore724', JSON.stringify(this.session));

          localStorage.setItem('currentUserTokenStore724', JSON.stringify(this.token));
          localStorage.setItem('currentUserMenuStore724', JSON.stringify(this.session['menus']));
          localStorage.setItem('currentUserRolStore724', JSON.stringify(this.session['roles']));

          this.loggedIn = true;

          resolve(true)
        },error => {
          //if(localStorage.getItem("SesionExpirada") != "true"){ alert("Usuario o Contraseña Incorrecto");}
          this.loggedIn = false;
          reject(false);
        });
    });
    return from(promesa);
  };

  public postUserAUTH = (auth: UsuarioDTO): Observable<Number|any> => {
    let promesa = new Promise((resolve, reject) => {
      this.http.post(this.urlAuth, auth, { headers: this.headers,withCredentials:true })
        .subscribe(value => {
          resolve(value)
        },error => {
          this.handleError(error);reject(null)
        });
    });
    return from(promesa);
  };

  public register = (auth: Auth): Observable<{}|Boolean> => {
    let promesa = new Promise((resolve, reject) => {
      this.http.post(this.urlAuth, auth, { headers: this.headers })
        .subscribe(value => {resolve(true)},error => {this.handleError(error);reject(false)});
    });
    return from(promesa);
  };

  public logout() {
      return new Promise((resolve, reject) => {
        //this.http.delete(this.urlAuth, this.options)
        this.http.post(Urlbase[0]+"/logout", {},{headers:this.headers})
          .subscribe(value => {
            this.locStorage.cleanSession();
            resolve([1,value])},error => {this.handleError(error);reject([-1,error])});
      });
  };

  isLoggedIn() {
    return this.loggedIn;
  }

  private handleError(error: Response | any): Observable<Boolean> {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      // @ts-ignore
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;

    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    if(localStorage.getItem("SesionExpirada") != "true"){ alert("Usuario o Contraseña Erroneos.");}
    CPrint("errMsg");
    CPrint(errMsg);
    return Observable.throw(false);
    //return Observable.throw(errMsg);
  }
}
