import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {CPrint} from 'src/app/shared/util/CustomGlobalFunctions';
import {MatTableDataSourceWithCustomSort, PedidosComponent} from '../pedidos.component';
import {MatDatepicker, MatDialog, MatPaginator} from '@angular/material';
import {MatSort} from '@angular/material/sort';
import {LocalStorage} from '../../../../../../../../services/localStorage';
import {HttpClient} from '@angular/common/http';
import {Urlbase} from '../../../../../../../../shared/urls';


@Component({
  selector: 'app-re-orden-por-provider',
  templateUrl: './re-orden-por-provider.component.html',
  styleUrls: ['./re-orden-por-provider.component.css']
})
export class ReOrdenPorProviderComponent implements OnInit {
  @Input()
  Pedidos_Input:PedidosComponent;
  Pedidos_MainClassRef:PedidosComponent;

  Date1: Date;
  Date2: Date;

  daysForPedido = 10;
  daysInBetweenDates=0;

  reportReorderTable = [];
  storesToSend = [];

  ListadoProveedores = [];
  ProveedorSeleccionado = -1;

  detalles="";
  EstadoBusqueda_Reorden = -1;
  SubTotalReorden = 0;
  TotalIVA = 0;
  TotalReorden = 0;
  ListadoFabricantes = {};//Esto es por IDS
  ListadoNombresFabricantes = {};//Acá relaciono el ID con el nombre
  ListadoFabricantesPorProvider = [];
  ListadoMarcasTotal = {};
  ListadoMarcasFiltrado = [];
  ListadoFabricantesSeleccionados = [];
  ListadoMarcasSeleccionadas = [];
  DataSourceReporte_Reorden = new MatTableDataSourceWithCustomSort();
  @ViewChild('pickerthirds2') SelectorFechaFinal_Reorden: MatDatepicker<Date>;
  @ViewChild('SorterTablaReorden') SorterTablaReorden: MatSort;
  @ViewChild('PaginatorReorden', {static: true}) paginatorReorden: MatPaginator;
  DictColumnas_Reorden = {};

  constructor(
    public dialog: MatDialog,
    public locStorage: LocalStorage,
    private http2: HttpClient
  ) {
  }

  ngOnInit(): void {
    this.Pedidos_MainClassRef = this.Pedidos_Input;
    this.http2.get(Urlbase[3]+"/pedidos/providers?id_store="+this.Pedidos_MainClassRef.SelectedStore).subscribe(response =>{
      CPrint(response);
      // @ts-ignore
      this.ListadoProveedores = response;
    })
  }



  generatePedidosExcel(){
    CPrint(Urlbase[2] + "/resource/pedidos/Excel?days_ask="+this.daysForPedido+"&days_between="+this.daysInBetweenDates);
    this.http2.post(Urlbase[2] + "/resource/pedidos/Excel?days_ask="+this.daysForPedido+"&days_between="+this.daysInBetweenDates,this.DataSourceReporte_Reorden.filteredData, {
      responseType: 'text'
    }).subscribe(response =>{
      CPrint(response);
      window.open(Urlbase[10]+"/"+response, "_blank");
    })
  }

  generate(){
    this.EstadoBusqueda_Reorden = 1;
    this.generate2().then(response => {this.sendPost()})
  }

  async generate2(){
    this.storesToSend = [];
    if(this.Pedidos_MainClassRef.SelectedStore == -1){
      this.Pedidos_MainClassRef.Stores.forEach(store => {
        this.storesToSend.push(store.id_STORE);
      })
    }else{
      this.storesToSend.push(Number(this.Pedidos_MainClassRef.SelectedStore));
    }
  }

  sendPost(){
    this.Pedidos_MainClassRef.mostrandoCargando = true;
    this.Pedidos_MainClassRef.estadoCargando = "Consultando...";
    this.http2.get(Urlbase[3] + "/pedidos/reorderv2?id_store="+this.Pedidos_MainClassRef.SelectedStore+"&date1="+this.Date1+"&date2="+this.Date2).subscribe(response =>  {
      this.Pedidos_MainClassRef.mostrandoCargando = false;
      CPrint(Urlbase[3] + "/pedidos/reorderv2?id_store="+this.Pedidos_MainClassRef.SelectedStore+"&date1="+this.Date1+"&date2="+this.Date2);
      //@ts-ignore
      this.reportReorderTable=response;
      this.EstadoBusqueda_Reorden = 2;
      this.DataSourceReporte_Reorden = new MatTableDataSourceWithCustomSort(this.reportReorderTable);
      this.DataSourceReporte_Reorden.paginator = this.paginatorReorden;
      this.DataSourceReporte_Reorden.sort = this.SorterTablaReorden;
      this.DataSourceReporte_Reorden.filterPredicate = this.FuncionFiltro;
      //Saco providers y marcas
      this.ListadoFabricantesSeleccionados = [];
      this.ListadoMarcasSeleccionadas = [];
      this.ListadoFabricantes = {};
      this.ListadoNombresFabricantes = {};
      this.ListadoMarcasTotal = {};
      for(let n = 0; n < this.reportReorderTable.length; n++){
        if(this.ListadoFabricantes[this.reportReorderTable[n]["id_PROVIDER"]] == null){
          this.ListadoFabricantes[this.reportReorderTable[n]["id_PROVIDER"]] = {[this.reportReorderTable[n]["marca"]] : 1};
        }else{
          this.ListadoFabricantes[this.reportReorderTable[n]["id_PROVIDER"]][this.reportReorderTable[n]["marca"]] = 1;
        }
        this.ListadoMarcasTotal[this.reportReorderTable[n]["marca"]] = 1;
        this.ListadoNombresFabricantes[this.reportReorderTable[n]["id_PROVIDER"]] = this.reportReorderTable[n]["fabricante"];
      }
      this.ListadoMarcasFiltrado = Object.keys(this.ListadoMarcasTotal);
      //
      this.daysInBetweenDates =Math.ceil(Math.abs(this.Date1.getTime() - this.Date2.getTime()) / (1000 * 3600 * 24));
      this.CalcularTotales();
    })
  }

  GetListadoMarcas(){
    return this.ListadoMarcasFiltrado.sort();
  }

  GetKeysReporteReorden(){
    this.DictColumnas_Reorden = {};
    this.DictColumnas_Reorden["barcode"] = ["Codigo de Barras",0];
    this.DictColumnas_Reorden["producto"] = ["Producto",0];
    this.DictColumnas_Reorden["presentacion"] = ["Presentacion",0];
    this.DictColumnas_Reorden["fabricante"] = ["Fabricante",0];
    this.DictColumnas_Reorden["marca"] = ["Marca",0];
    this.DictColumnas_Reorden["linea"] = ["Linea",0];
    this.DictColumnas_Reorden["categoria"] = ["Categoria",0];
    this.DictColumnas_Reorden["costo"] = ["Costo",1];
    this.DictColumnas_Reorden["iva"] = ["IVA",0];
    this.DictColumnas_Reorden["cantidad_VENDIDA"] = ["Cantidad Vendida",0];
    this.DictColumnas_Reorden["cantidad_EN_INVENTARIO"] = ["Cantidad en Inventario",0];
    this.DictColumnas_Reorden["especial1"] = ["Promedio Diario",0];
    this.DictColumnas_Reorden["especial2"] = ["Cantidad a Pedir",0];
    this.DictColumnas_Reorden["especial3"] = ["Cantidad Contra Inventario",0];
    return Object.keys(this.DictColumnas_Reorden);
  }

  CalcularTotales(){
    this.SubTotalReorden = 0;
    this.TotalIVA = 0;
    this.TotalReorden = 0;
    let data = this.DataSourceReporte_Reorden.filteredData;
    for(let n = 0; n < data.length; n++){
      let Cantidad = this.round((this.daysForPedido * parseInt(data[n]["cantidad_VENDIDA"]) / this.daysInBetweenDates)-parseInt(data[n]["cantidad_EN_INVENTARIO"]),0);
      Cantidad = Cantidad < 0 ? 0 : Cantidad;
      let SubTotal = data[n]["costo"] * Cantidad;
      this.SubTotalReorden += SubTotal;
      this.TotalIVA += SubTotal*(data[n]["iva"]/100);
      this.TotalReorden += SubTotal + (SubTotal*(data[n]["iva"]/100));
    }
  }

  ActualizarFiltrado(evento,tipo){
    if(tipo == 1){
      this.ListadoFabricantesSeleccionados = evento;
      this.ListadoMarcasSeleccionadas = [];
      this.ListadoMarcasFiltrado = [];
      let ListadoAUsar = this.ListadoFabricantesSeleccionados;
      for(let n = 0;n<ListadoAUsar.length;n++){
        let MarcasFabricante = Object.keys(this.ListadoFabricantes[ListadoAUsar[n]]);
        for(let m = 0;m<MarcasFabricante.length;m++){
          this.ListadoMarcasFiltrado.push(MarcasFabricante[m]);
        }
      }
      if(this.ProveedorSeleccionado != -1 && this.ListadoFabricantesSeleccionados.length == 0){
        this.ListadoFabricantesSeleccionados = [-1];
      }
    }
    else if(tipo == 2){
      this.ListadoMarcasSeleccionadas = evento;
    }else{//tipo 3
      this.ProveedorSeleccionado = evento;
      this.Pedidos_MainClassRef.mostrandoCargando = true;
      this.Pedidos_MainClassRef.estadoCargando = "Consultando fabricantes y marcas";
      this.ListadoFabricantesSeleccionados = [];
      this.ListadoMarcasSeleccionadas = [];
      if(evento == -1){
        this.Pedidos_MainClassRef.mostrandoCargando = false;
        this.ListadoFabricantesPorProvider = [];
        this.ActualizarFiltrado([],1);
        return;
      }
      this.http2.get(Urlbase[3]+"/pedidos/fabricantes?id_third="+evento).subscribe(response =>{
        CPrint(response);
        // @ts-ignore
        this.ListadoFabricantesPorProvider = response;
        this.Pedidos_MainClassRef.mostrandoCargando = false;
        let ListadoAUsar = [];
        for(let n = 0;n<this.ListadoFabricantesPorProvider.length;n++){
          ListadoAUsar.push(this.ListadoFabricantesPorProvider[n]["id_PROVIDER"])
        }
        this.ActualizarFiltrado(ListadoAUsar,1);
      })
    }
    let Filtro = this.ListadoFabricantesSeleccionados+"|"+this.ListadoMarcasSeleccionadas.join(",");
    CPrint("Filtro es "+Filtro);
    this.DataSourceReporte_Reorden.filter = Filtro;
    this.CalcularTotales();
  }

  async getDetalles(){
    this.DataSourceReporte_Reorden.filteredData.forEach(element => {

      //@ts-ignore
      if(this.round((this.daysForPedido * element.cantidad_VENDIDA / this.daysInBetweenDates)-element.cantidad_EN_INVENTARIO,0)>0){
        //@ts-ignore
        this.detalles+="{"+element.idps+","+this.round(element.costo,0)+","+element.idt+","+this.round((this.daysForPedido * element.cantidad_VENDIDA / this.daysInBetweenDates)-element.cantidad_EN_INVENTARIO,0)+"},"
      }
    });
  }

  sendData(){
    this.getDetalles().then(elem => {
      CPrint("DETAILS TO SEND: ",this.detalles);
      this.SolicitarPedido();
    })
  }

  async SolicitarPedido(){
    this.Pedidos_MainClassRef.mostrandoCargando = true;
    this.Pedidos_MainClassRef.estadoCargando = "Consultando...";
     this.http2.post(Urlbase[3] + "/pedidos/crearPedidoV2/costo?idthirdclient="+this.locStorage.getThird().id_third+"&idstoreclient="+this.Pedidos_MainClassRef.SelectedStore+"&idthirdempclient="+this.locStorage.getToken().id_third+"&idthirdprov="+this.ListadoProveedores.filter(element => element.id_THIRD_DESTINITY == this.ProveedorSeleccionado )[0].id_THIRD_DESTINITY+"&idstoreprov="+this.ListadoProveedores.filter(element => element.id_THIRD_DESTINITY == this.ProveedorSeleccionado )[0].id_STORE_PROVIDER+"&detallepedido="+this.detalles,{}).subscribe(
       response=> {
         this.detalles="";
         this.http2.get(Urlbase[3]+"/billing/UniversalPDF?id_bill="+response+"&pdf="+false,{responseType: 'text'}).subscribe(responses =>{
           window.open(Urlbase[6]+"/"+responses, "_blank");
           this.Pedidos_MainClassRef.mostrandoCargando = false;
         })
       })
  }

  FuncionFiltro(data:{id_PROVIDER: number,marca:string}, filterValue: string){
    if(filterValue.length == 0 || filterValue.length == 1){
      return true;
    }
    let dividido = filterValue.split('|');
    if(dividido[0].length == 0 && dividido[1].length == 0){
      return true;
    }
    else if(dividido[0].length == 0 && dividido[1].length != 0){
      return dividido[1].includes(data.marca);
    }
    else if(dividido[0].length != 0 && dividido[1].length == 0){
      let divididoFabricante = dividido[0].split(',');
      let encontrado = false;
      for(let n = 0;n<divididoFabricante.length;n++){
        if(parseInt(divididoFabricante[n]) == data.id_PROVIDER){
          encontrado = true;
          break;
        }
      }
      return encontrado;
    }
    else{
      let divididoFabricante = dividido[0].split(',');
      let encontrado = false;
      for(let n = 0;n<divididoFabricante.length;n++){
        if((parseInt(divididoFabricante[n]) == data.id_PROVIDER) && dividido[1].includes(data.marca)){
          encontrado = true;
          break;
        }
      }
      return encontrado;
    }
  }

  round(value, precision) {
    const multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }
}
