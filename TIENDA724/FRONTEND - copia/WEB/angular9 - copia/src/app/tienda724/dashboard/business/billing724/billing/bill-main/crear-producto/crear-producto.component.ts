import {Component, OnInit, ViewChild} from '@angular/core';
import {CPrint} from 'src/app/shared/util/CustomGlobalFunctions';
import {HttpClient} from '@angular/common/http';
import {LocalStorage} from 'src/app/services/localStorage';
import {Router} from '@angular/router';
import {Urlbase} from 'src/app/shared/urls';
import {BillingService} from '../../../../../../../services/billing.service';
import * as jQuery from 'jquery';
import 'bootstrap-notify';
import {MatDialog} from '@angular/material/dialog';
import {UpdateNewProductComponent} from '../update-new-product/update-new-product.component';
import {MatTableDataSourceWithCustomSort} from '../pedidos/pedidos.component';
import {MatPaginator, MatSort} from '@angular/material';

let $: any = jQuery;

@Component({
  selector: 'app-crear-producto',
  templateUrl: './crear-producto.component.html',
  styleUrls: ['./crear-producto.component.css']
})

export class CrearProductoComponent implements OnInit {
  tax=0;
  price = 0;
  cost = 0;
  Stores = [];
  prodname = "";
  barcode = "";
  quantity=0;
  id_menu = 262;
  products = [];
  measureUnitList;
    SelectedMun = -1;
    SelectedLine = -1;
    SelectedSubCategory = -1;
    SelectedBrand = -1;
    SubCategoryList;
    CategoryFirstLvlList;
    productName;
    brands;


  //COSAS PARA LA TABLA
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  dataSource = new MatTableDataSourceWithCustomSort(this.products);
  expandedElement: any | null;
  GetKeys(){
    return ["product_STORE_NAME","id_CODE","code"];
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  listTable;
  storeIva ="";
  constructor(private categoriesService: BillingService, public dialog: MatDialog, private billingService : BillingService,private http: HttpClient, public locStorage: LocalStorage,public router: Router,) { }

  ngOnInit() {
    this.getIva();
    this.listTable = this.GetKeys();
    this.getMeasureUnitList();
    this.getFirstLvlCategory();
    this.getBrands();

    //PROTECCION URL INICIA
    CPrint(JSON.stringify(this.locStorage.getMenu()));
    const elem = this.locStorage.getMenu().find(item => item.id_menu == this.id_menu);

    if(!elem){
      // noinspection JSIgnoredPromiseFromCall
      this.router.navigateByUrl("/dashboard/business/movement/nopermision");
    }
    //PROTECCION URL TERMINA


    this.billingService.getStoresByThird(this.locStorage.getToken().id_third).subscribe(data => {
      CPrint(data);
      this.Stores = data;
      let storeList = "";
      data.forEach(element => {
        //@ts-ignore
        storeList = storeList + element.id_STORE + ",";
        this.http.post(Urlbase[2]+"/resource/getnewproducts?id_store="+storeList.substring(0,storeList.length-1),{}).subscribe(data => {
        // @ts-ignore
          this.products = data;
          this.dataSource = new MatTableDataSourceWithCustomSort(this.products);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
      })
      });

    });
    this.tax=0

  }

  getIva(){
    this.http.get(Urlbase[2]+"/resource/ivatienda?idstore="+this.locStorage.getIdStore(),{responseType: 'text'}).subscribe(response => {
      //@ts-ignore
      this.storeIva = response;
      console.log(response)
    })
  }

  acept(){
    if(this.cost!=0 && this.price!=0 && this.price>= this.cost && this.price>0 && this.cost>0 &&
      this.quantity>=0 && this.barcode!='' && this.prodname!=''){
        if(this.SelectedSubCategory!=-1 && this.SelectedMun != -1 && this.SelectedBrand != -1){

          this.http.post( Urlbase[2]+"/products/v2?barcode="+this.barcode+"&idstore="+this.locStorage.getIdStore()+"&producto="+this.prodname+"&costo="+this.cost+"&iva="+this.tax+"&precio="+this.price+"&cantidad="+this.quantity+"&mun="+this.SelectedMun+"&brand="+this.SelectedBrand+"&cate="+this.SelectedSubCategory,{}).subscribe(data=> {
            if(data==1){
              this.showNotification('top', 'center', 3, "<h3>Producto Creado con exito.</h3> ", 'info');
              this.cancel();
            }else{
              this.showNotification('top', 'center', 3, "<h3>Hubo un error al crear el producto. Comuniquese con el administrador</h3> ", 'danger');
              this.cancel();
            }
          });
        }else{

          if(this.SelectedSubCategory==-1 && this.SelectedMun == -1 && this.SelectedBrand == -1){

            this.http.post( Urlbase[2]+"/products/v2?barcode="+this.barcode+"&idstore="+this.locStorage.getIdStore()+"&producto="+this.prodname+"&costo="+this.cost+"&iva="+this.tax+"&precio="+this.price+"&cantidad="+this.quantity+"&mun="+this.SelectedMun+"&brand="+this.SelectedBrand+"&cate="+this.SelectedSubCategory,{}).subscribe(data=> {
              if(data==1){
                this.showNotification('top', 'center', 3, "<h3>Producto Creado con exito.</h3> ", 'info');
                this.cancel();
              }else{
                this.showNotification('top', 'center', 3, "<h3>Hubo un error al crear el producto. Comuniquese con el administrador</h3> ", 'danger');
                this.cancel();
              }
            });
          }else{
            this.showNotification('top', 'center', 3, "<h3>Debe elegir los 3 campos de Categoria, Marca y Unidad de medida.</h3> ", 'danger');
          }

        }

      CPrint("URL: ", Urlbase[2]+"/products/v2?barcode="+this.barcode+"&idstore="+this.locStorage.getIdStore()+"&producto="+this.prodname+"&costo="+this.cost+"&iva="+this.tax+"&precio="+this.price+"&cantidad="+this.quantity+"&mun="+this.SelectedMun+"&brand="+this.SelectedBrand+"&cate="+this.SelectedSubCategory)
      }else{
      this.showNotification('top', 'center', 3, "<h3>Los datos ingresados no son validos, revise que ningun dato sea vacio y que el precio sea mayor o igual al costo.</h3> ", 'danger');
    }
 }

  cancel(){
    console.log(this.locStorage.getIdStore())
    this.tax=0;
    this.price = 0;
    this.cost = 0;
    this.prodname = "";
    this.barcode = "";
    this.quantity=0;
  }


  updateButton(elem){

      const dialogRef = this.dialog.open(UpdateNewProductComponent, {
        width: '60vw',
        height: '75vh',
        data: { element: elem }
      });

      dialogRef.afterClosed().subscribe(result => {
        if(result){
          this.ngOnInit();
        }
      })


  }


  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
      type: typeStr ? typeStr : type[id_type ? id_type : 2],
      timer: 200,
      placement: {
        from: from,
        align: align
      }
    });
  }

  getSecondLvlCategory(){
    if(this.SelectedLine != -1){
      this.http.get(Urlbase[2]+"/categories2/children?id_category_father="+this.SelectedLine).subscribe(data => {
        this.SubCategoryList = data
      })
    }
  }

  getFirstLvlCategory(){
    this.categoriesService.getGeneralCategories().subscribe(data => {
      this.CategoryFirstLvlList = data
    })
  }

  getMeasureUnitList(){

    this.categoriesService.getGenericMeassureUnits().subscribe(res=>{
      CPrint(res,"meassures")
      this.measureUnitList = res
    })
  }

  getBrands(){
    this.categoriesService.getBrands().subscribe(res=>{
      CPrint("THIS ARE BRANDS",res);
      this.brands = res;
    })
  }



}
