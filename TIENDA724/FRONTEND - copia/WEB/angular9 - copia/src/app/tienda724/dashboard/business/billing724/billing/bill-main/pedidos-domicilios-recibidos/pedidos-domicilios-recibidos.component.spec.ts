import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidosDomiciliosRecibidosComponent } from './pedidos-domicilios-recibidos.component';

describe('PedidosDomiciliosRecibidosComponent', () => {
  let component: PedidosDomiciliosRecibidosComponent;
  let fixture: ComponentFixture<PedidosDomiciliosRecibidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidosDomiciliosRecibidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidosDomiciliosRecibidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
