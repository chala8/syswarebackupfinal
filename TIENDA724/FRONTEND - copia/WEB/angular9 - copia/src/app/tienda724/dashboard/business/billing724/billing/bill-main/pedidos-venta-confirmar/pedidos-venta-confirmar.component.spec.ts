import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidosVentaConfirmarComponent } from './pedidos-venta-confirmar.component';

describe('PedidosVentaConfirmarComponent', () => {
  let component: PedidosVentaConfirmarComponent;
  let fixture: ComponentFixture<PedidosVentaConfirmarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidosVentaConfirmarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidosVentaConfirmarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
