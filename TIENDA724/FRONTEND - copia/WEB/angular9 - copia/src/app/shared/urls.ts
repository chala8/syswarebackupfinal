let UsarProd = window.location.href.includes("tienda724.com");
export const Urlbase = [
  //auth [0]
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8449',
  // tercero [1]
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8446/v1',
  // tienda [2]
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8447/v1',
  // billing/facturación [3]
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8448/v1',
  // order [4]
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8450/v1',
  // cierreCaja [5]
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8451/v1',
  // remisiones [6],
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/remisiones',
  // facturas [7],
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/facturas',
  // imagenes [8],
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/imagenes',
  // logos [9],
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/logos',
  // reportes [10],
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/reportes',
  // [11],
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/',
];

//3.234.156.197
