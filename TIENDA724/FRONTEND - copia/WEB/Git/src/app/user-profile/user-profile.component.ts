import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthenticationService } from '../authentication/authentication.service';

import { LocalStorage } from '../shared/localStorage';
import { Token } from '../shared/token';
import { Person } from '../shared/models/person';

declare var $: any;

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  user=false;
  token:Token;
  person:Person;
  form: FormGroup;


  constructor(private authService:AuthenticationService,
    private router:Router,
    private locStorage:LocalStorage,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.user=false;
 
    let session=this.locStorage.getSession();
    this.person=this.locStorage.getPerson();
    if(!session){
      /**
      @todo Eliminar comentario para
      */
    }else{
      this.token=this.locStorage.getToken();
     

      
      this.createControls()
      this.loadData()
      this.token=this.locStorage.getToken();

    }
  }


  logout() {
    this.locStorage.cleanSession();
    this.token=null;
    this.goIndex();
    
    this.showNotification('top','right');
 
   }
 
   goIndex() {
     let link = ['/'];
     this.router.navigate(link);
   }


   showNotification(from, align){
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
        icon: "notifications",
        message: "Usted <b>Cerro Sesión</b> de forma satisfactoria."

    },{
        type: type[2],
        timer: 500,
        placement: {
            from: from,
            align: align
        }
    });
  }


  createControls() {
    this.form = this.fb.group({

      //profile
      first_name: ['', Validators.compose([
        Validators.required
      ])],
      second_name: ['', Validators.compose([
        Validators.required
      ])],
      first_lastname: ['', Validators.compose([
        Validators.required
      ])],
      second_lastname: ['', Validators.compose([
        Validators.required
      ])],
      birthday: ['', Validators.compose([
        Validators.required
      ])],
      //info
      fullname_prof: ['', Validators.compose([
        Validators.required
      ])],
      id_document_type_prof: ['', Validators.compose([
        Validators.required
      ])],
      document_number_prof: ['', Validators.compose([
        Validators.required
      ])],
      img_prof: ['', Validators.compose([
        Validators.required
      ])],
      //state_prof
      state_prof: ['', Validators.compose([
        Validators.required
      ])],
      //directory_prof
      address_prof: ['', Validators.compose([
        Validators.required
      ])],
      country_prof: ['', Validators.compose([
        Validators.required
      ])],
      city_prof: ['', Validators.compose([
        Validators.required
      ])],
      webpage_prof: ['', Validators.compose([
        Validators.required
      ])],
      //state_dir_prof
      state_dir_prof: ['', Validators.compose([
        Validators.required
      ])],
      //pohe_prof
      phone_prof: ['', Validators.compose([
        Validators.required
      ])],
      priority_pohe_prof: ['', Validators.compose([
        Validators.required
      ])],
      //state_pohe_prof
      state_pohe_prof: ['', Validators.compose([
        Validators.required
      ])],
      //mail_prof
      mail_prof: ['', Validators.compose([

      ])],
      priority_mail_prof: ['', Validators.compose([
        Validators.required
      ])],
      //state_mail_prof
      state_mail_prof: ['', Validators.compose([
        Validators.required
      ])],
      //employee
      salary: ['', Validators.compose([
        Validators.required
      ])],
      //state_em
      state_em: ['', Validators.compose([
        Validators.required
      ])],

      isNaturalPerson: [false, Validators.compose([

      ])],

      isEmployee: [false, Validators.compose([

      ])]
      //UUID
    });

  }

  loadData(){
    
    this.form.patchValue({
      //profile
      first_name: this.person.first_name,
      second_name: this.person.second_name,
      first_lastname: this.person.first_lastname,
      second_lastname: this.person.second_lastname,
      birthday: "",
      //info
      fullname_prof:"",
      id_document_type_prof: "",
      document_number_prof: "212eweqwewqe",
      img_prof: "es",
      //state_prof
      state_prof: "1",
      //directory_prof
      address_prof: "cra",
      country_prof: "Colombia",
      city_prof: "Bogota",
      webpage_prof: "www.personal.com",
      //state_dir_prof
      state_dir_prof: "1",
      //pohe_prof
      phone_prof: "21ewweqwe",
      priority_pohe_prof: "1",
      //state_pohe_prof
      state_pohe_prof: "1",
      //mail_prof
      mail_prof: "luis@gmail.com",
      priority_mail_prof: "1",
      //state_mail_prof
      state_mail_prof: "1",
      //employee
      salary: "1",
      //state_em
      state_em: "1"
    });
  
  }


}
