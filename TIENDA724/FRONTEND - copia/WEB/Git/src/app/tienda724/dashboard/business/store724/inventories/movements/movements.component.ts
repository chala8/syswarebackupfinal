import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SearchProductDialogComponent } from '../../../billing724/billing/bill-main/search-product-dialog/search-product-dialog.component';
import { InventoryDetail } from '../models/inventoryDetail';
import { CommonStateDTO } from '../../../billing724/commons/commonStateDTO';
import { InventoriesService } from '../../../store724/inventories/inventories.service';
import { TransactionConfirmDialogComponent } from './transaction-confirm-dialog/transaction-confirm-dialog.component';
declare var $: any;
@Component({
  selector: 'app-movements',
  templateUrl: './movements.component.html',
  styleUrls: ['./movements.component.scss']
})
export class MovementsComponent implements OnInit {

  ID_INVENTORY_TEMP = 61;
  STATE = 1;

  public flagEntry:boolean = true;
  public productsObject = {};
  public setObject = [];
  public removalArray = new Set();
  inventoryList: InventoryDetail[];
  commonStateDTO: CommonStateDTO;

  constructor(public dialog: MatDialog, public inventoriesService: InventoriesService) { }

  ngOnInit() {
    this.getInventoryList(this.STATE, null, null, this.ID_INVENTORY_TEMP);
  }

  addDetail(event) {
    var code = String(event.target.value);
    if (this.productsObject.hasOwnProperty(code)) {
      this.productsObject[code]['quantity'] += 1;
      this.setObject = Object.keys(this.productsObject);
      event.target.value = '';
    } else {
      var product = this.inventoryList.find(item => item['product']['code'] == code);
      if (product) {
        let new_product = {
          quantity: 1,
          price: product.description.standard_price,
          tax: 0.19,
          id_product_third: product.description.id_product_third,
          tax_product: product.product.id_tax,
          state: this.commonStateDTO,
          description: product.product.name_product,
          code: product.detail.code,
          id_inventory_detail: product.detail.id_inventory_detail
        };
        //this.detailBillDTOList.push(new_product);
        this.productsObject[code] = new_product;
        this.setObject = Object.keys(this.productsObject);

        event.target.blur();
        event.target.value = '';
        setTimeout(() => {
          document.getElementById('lastDetailQuantity').focus();
        });
      } else {
        alert('Product not exist');
      }
    }
  }
  toggleFlag(args) {
    this.flagEntry = args;
    this.cancel();
  }

  readNew(event: any) {
    event.target.blur();
    setTimeout(() => {
      document.getElementById('reader').focus();
    });
  }

  cancel() {
    //this.form.reset();
    //this.clientData = new ClientData(true, 'Cliente Ocacional', '--', '000', 'N/A', '000', 'N/A');
    this.productsObject = {};
    this.setObject = [];
    this.removalArray = new Set();
    //this.CUSTOMER_ID= this.token.id_third_father;
  }

  checkItem(event, code) {
    if (event.target.checked) {
      this.removalArray.add(code);
    } else {
      this.removalArray.delete(code);
    }
    console.log(this.removalArray);
  }

  checkAllItems(event) {
    if (event.target.checked) {
      this.removalArray = new Set(this.setObject);
    } else {
      this.removalArray = new Set();
    }
  }

  removeItems() {
    this.removalArray.forEach((item) => {
      if (this.productsObject.hasOwnProperty(item)) {
        delete this.productsObject[item];
      }
    });
    this.removalArray = new Set();
    this.setObject = Object.keys(this.productsObject);
  }

  openDialogSearchProduct(): void {
    const dialogRef = this.dialog.open(SearchProductDialogComponent, {
      width: '40vw'
      // data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      //this.animal = result;
    });
  }

  openDialogTransactionConfirm(): void {
    const dialogRef = this.dialog.open(TransactionConfirmDialogComponent, {
      width: '40vw',
      data: { /* total: this.calculateTotal(), */ productsQuantity: Object.keys(this.productsObject).length }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if(result){
        console.log('result:');
        console.log(result);
        this.save(result);
      }
    });
  }

  save(data){
    this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'success')
    this.cancel();
  }

  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: typeStr ? typeStr : type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }

  // services
  getInventoryList(state_inv_detail?: number, state_product?: number, id_inventory_detail?: number,
    id_inventory?: number, id_product_third?: number, location?: number,
    id_third?: number, id_category_third?: number, quantity?: number, id_state_inv_detail?: number,
    id_product?: number, id_category?: number, stock?: number,
    stock_min?: number, img_url?: string, id_tax?: number,
    id_common_product?: number, name_product?: string,
    description_product?: string, id_state_product?: number,


    id_state_prod_third?: number, state_prod_third?: number,
    id_measure_unit?: number, id_measure_unit_father?: number,
    id_common_measure_unit?: number, name_measure_unit?: string,
    description_measure_unit?: string, id_state_measure_unit?: number,
    state_measure_unit?: number, id_code?: number,
    code?: number, img?: string,
    id_attribute_list?: number,
    id_state_cod?: number, state_cod?: number,
    attribute?: number,
    attribute_value?: number) {
    return new Promise((resolve, reject) => {

      this.inventoriesService.getInventoriesDetailList(state_inv_detail, state_product, id_inventory_detail,

        id_inventory, quantity, id_state_inv_detail,
        id_product, id_category, stock,
        stock_min, img_url, id_tax,
        id_common_product, name_product,
        description_product, id_state_product,
        id_product_third, location,
        id_third, id_category_third,

        id_state_prod_third, state_prod_third,
        id_measure_unit, id_measure_unit_father,
        id_common_measure_unit, name_measure_unit,
        description_measure_unit, id_state_measure_unit,
        state_measure_unit, id_code,
        code, img,
        id_state_cod, state_cod)


        .subscribe((data: InventoryDetail[]) => {
          this.inventoryList = data;
          resolve();
        },
          (error) => {
            console.log(error);
            reject();
          },
          () => {
            if (this.inventoryList.length > 0) {

            }
          });
    });
  }
}