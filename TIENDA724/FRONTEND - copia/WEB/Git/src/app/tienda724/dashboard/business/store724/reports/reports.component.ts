import { Component, OnInit } from '@angular/core';
import { Report } from './models/report'

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  public reports:Array<Object>;

  constructor() {}

  ngOnInit() {
    this.getTabs();
  }

  public getTabs() {
    this.reports= [
      new Report('VENTAS'),
      new Report('COMPRAS')
    ]
  }

}
