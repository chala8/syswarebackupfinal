import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillInventoryComponentComponent } from './bill-inventory-component.component';

describe('BillInventoryComponentComponent', () => {
  let component: BillInventoryComponentComponent;
  let fixture: ComponentFixture<BillInventoryComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillInventoryComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillInventoryComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
