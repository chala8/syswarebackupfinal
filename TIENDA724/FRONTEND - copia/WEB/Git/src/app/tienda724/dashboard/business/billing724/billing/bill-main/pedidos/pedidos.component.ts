import {Component, OnInit, ViewChild} from '@angular/core';
import { Chart } from 'chart.js';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '../../../../../../../shared/localStorage';
import { DatePipe } from '@angular/common';
import { BillingService } from '../../billing.service';
import { Http, Headers } from '@angular/http';
import {MatTabChangeEvent, MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import { PedidosDetailComponent } from '../pedidos-detail/pedidos-detail.component'
import { PedidosDetail2Component } from '../pedidos-detail2/pedidos-detail2.component'
import { StatechangeComponent } from '../statechange/statechange.component'
import { NotesModalComponent } from '../notes-modal/notes-modal.component';
import { NotesOnOrderComponent } from '../notes-on-order/notes-on-order.component';
import {Urlbase} from 'app/shared/urls';

declare var $: any;

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss']
})
export class PedidosComponent implements OnInit {
  id_store;

  date1='';
  date2='';

  type2 = "1";
  SelectedStore2;
  isListProdFull2=false;
  ListReportProd2;
  dateP12;
  dateP22;
  hours2=24;

  checked = false;
  Stores = [];
  SelectedStore = this.locStorage.getIdStore();
  SelectedBillType = '87';
  SelectedBillState = '61';
  isListProdFull = false;
  ListReportProd;
  ListChecked=[];

  SelectedVehicle = "";
  ListVehicles;
  isPlanillaFull = false;
  ListPlanilla=[];

  CampoSorteando = -1;
  Invertido = false;

  mostrandoCargando = false;
  estadoCargando = "Procesando - Iniciando";

  //Variables de MatTable
  // @ViewChild(MatSort) sort: MatSort;
  // ELEMENT_DATA: EstructuraDatos[] = [];
  // displayedColumns: string[] = [];
  // displayedColumnsNombre: string[] = ['Fecha', '# Documento', '# Pedido Cliente', 'Cliente', 'Tienda', 'Dirección', 'E-Mail', 'Teléfono', 'Observaciones', 'Opciones'];
  // dataSource = new MatTableDataSource(this.ELEMENT_DATA);

  constructor(private datePipe: DatePipe, public dialog: MatDialog, public locStorage: LocalStorage,private categoriesService: BillingService,private http2: HttpClient) { }

  ngOnInit() {
    this.getVehicles();
    this.getStores();
    this.id_store = this.locStorage.getIdStore();
    console.log("THIS IS MY ID STORE: ",this.id_store)
  }

  getPlanillaList(){
    this.ListPlanilla = [];
    this.ListVehicles.forEach(element => {

      this.http2.get(Urlbase[2]+"/pedidos/planillas?idvehiculo="+element.id_VEHICULO+"&idstore="+this.locStorage.getIdStore()).subscribe(response => {

        //@ts-ignore
        response.forEach(element => {
          this.isPlanillaFull= true
          this.ListPlanilla.push(element);
        });
      })
    });
  } 

  getVehicles(){
    this.http2.get(Urlbase[2]+"/pedidos/vehiculos").subscribe(response => {
      this.ListVehicles = response;
      this.SelectedVehicle = response[0].id_VEHICULO;
    })
  }

  exportarPdf(elem){
    this.http2.get(Urlbase[2]+"/pedidos/planillaDetail?idplanilla="+elem.id_PLANILLA).subscribe(response => {
      this.http2.post(Urlbase[2]+"/pedidos/pdf",{
        logo: this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,
        master: elem,
        detalles: response
      },{responseType: 'text'}).subscribe(answer => {
        console.log("THIS IS RESPONSE: ",answer)
        console.log("EL BODY ES: ",JSON.stringify({
          logo: this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,
          master: elem,
          detalles: response
        }))
        window.open("http://tienda724.com/planillas/"+answer, "_blank");
      })
    })
  }

  openClosePanilla(idplanilla){
    var dialogRef
    dialogRef = this.dialog.open(NotesModalComponent,{
      height: '275px',
      width: '850px',
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {

      if(result.resp){
        this.http2.put(Urlbase[2]+"/pedidos/closeplanilla?observaciones="+result.notes+"&idplanilla="+idplanilla,{}).subscribe(answer => {
          console.log("THIS IS ANSWER: ",answer);
          this.getPlanillaList()
        })
      }
    });

  }

  confirmarEnv(elem) {
    console.log("this is element dude, ", elem)
    this.http2.post(Urlbase[2]+"/store/confirmarPC?numpedido="+elem.numdocumento+"&idbilltype=46&idstore="+this.locStorage.getIdStore(),{}).subscribe(
        response => {
          this.http2.post(Urlbase[2]+"/store/confirmarPC?numpedido="+elem.numdocumento+"&idbilltype=47&idstore="+this.locStorage.getIdStore(),{}).subscribe(
              response2 => {
                var dialogRef
                dialogRef = this.dialog.open(NotesOnOrderComponent,{
                  height: '38vh',
                  width: '80vw',
                  data: {
                    elem: elem
                  },
                  disableClose: true
                }).afterClosed().subscribe(response=> {

                  //  this.http2.put("http://tienda724.com:8448/v1/pedidos/billstate?billstate=81&billid="+elem.id_BILL,null).subscribe(a=> {
                  this.getRepProdList()
                })
                //  })
              })
        })
  }

  getStores() {
    this.categoriesService.getStoresByThird(this.locStorage.getToken().id_third).subscribe(data => {
      console.log(data);this.Stores = data
      this.SelectedStore =  this.locStorage.getIdStore();})
  }

  /*GetColumnas(){
    //Defino columnas
    console.log("this.SelectedBillType es "+this.SelectedBillType)
    console.log("this.SelectedBillState es "+this.SelectedBillState)
    this.displayedColumns = []
    this.displayedColumns.push('fecha'); this.displayedColumns.push('numdocumento');
    if(this.SelectedBillType == '86'){this.displayedColumns.push('numpedido');console.log("this.displayedColumns.push('numpedido')")}
    this.displayedColumns.push('cliente'); this.displayedColumns.push('tienda'); this.displayedColumns.push('address');
    this.displayedColumns.push('mail'); this.displayedColumns.push('phone'); this.displayedColumns.push('body');
    if((this.SelectedBillType == '86' && this.SelectedBillState == '62') || (this.SelectedBillType == '87' && this.SelectedBillState == '81')){this.displayedColumns.push('options');}
    //
    console.log("this.displayedColumns es ")
    console.log(this.displayedColumns)
    return this.displayedColumns;
  }*/

  SortearPorPropiedadMetodo(propiedad) {
    return function(n1, n2) {
      let Terminado = false;
      let i = 0;
      //let Arreglo1 = n1[propiedad].trim().split(/[ ,.-:]+/);
      let Arreglo1 = n1[propiedad].trim().split(' ').join(',').split('.').join(',').split('-').join(',').split(':').join(',').split(',');
      //let Arreglo2 = n2[propiedad].trim().split(/[ ,.-:]+/);
      let Arreglo2 = n1[propiedad].trim().split(' ').join(',').split('.').join(',').split('-').join(',').split(':').join(',').split(',');
      let retorno = 0;
      while(!Terminado){
        if(Arreglo1.length <= i && Arreglo2.length > i){
          Terminado = true;
          retorno = -1;
          break;
        }else if(Arreglo2.length <= i && Arreglo1.length > i){
          Terminado = true;
          retorno = 1;
          break;
        }else if(Arreglo1.length <= i && Arreglo2.length <= i){
          Terminado = true;
          retorno = 0;
          break;
        }
        let Parte1 = Arreglo1[i];
        let Parte2 = Arreglo2[i];
        let A = parseInt(Parte1);
        let B = parseInt(Parte2);
        if(isNaN(A)){ A = Parte1;}
        if(isNaN(B)){ B = Parte2;}
        i++;
        if (A < B){
          retorno = -1;
          Terminado = true;
          break;
        }else if (A > B){
          retorno = 1;
          Terminado = true;
          break;
        }
      }
      return retorno;
    };
  }

  SortBy(campo){
    if(this.CampoSorteando != campo){
      this.CampoSorteando = campo;
    }else{
      this.Invertido = !this.Invertido;
    }
    console.log("CAMPO A SORTEAR "+campo);
    console.log("this.CampoSorteando "+this.CampoSorteando);
    console.log("this.Invertido "+this.Invertido);
    let PropiedadASortear = "numdocumento";
    if(campo == 1){PropiedadASortear = "fecha";}
    else if(campo == 2){PropiedadASortear = "numdocumento";}
    else if(campo == 3){PropiedadASortear = "numpedido";}
    else if(campo == 4){PropiedadASortear = "cliente";}
    else if(campo == 5){PropiedadASortear = "tienda";}
    else if(campo == 6){PropiedadASortear = "address";}
    else if(campo == 7){PropiedadASortear = "mail";}
    else if(campo == 8){PropiedadASortear = "phone";}
    else if(campo == 9){PropiedadASortear = "body";}
    //Genero copia del listado
    const ListaCopia = [...this.ListReportProd];
    //Lo sorteo
    const ListaCopiaSorteada: string[][] = ListaCopia.sort(this.SortearPorPropiedadMetodo(PropiedadASortear));
    if(this.Invertido){
      let listaInvertida = [];
      for(let n = ListaCopiaSorteada.length-1;n>=0;n--){
        listaInvertida.push(ListaCopiaSorteada[n]);
      }
      this.ListReportProd = listaInvertida;
    }else{
      this.ListReportProd = ListaCopiaSorteada;
    }
    console.log("this.ListReportProd[0] es ")
    console.log(this.ListReportProd[0])
  }

  getRepProdList(){
    this.ListChecked = [];
    this.mostrandoCargando = true;
    this.estadoCargando = "Consultando Pedidos";
    console.log("http://tienda724.com:8448/v1/pedidos/master?id_store="+this.SelectedStore+"&id_bill_state="+this.SelectedBillState+"&id_bill_type="+this.SelectedBillType+"&date1="+this.date1+"&date2="+this.date2)
    this.http2.get("http://tienda724.com:8448/v1/pedidos/master?id_store="+this.SelectedStore+"&id_bill_state="+this.SelectedBillState+"&id_bill_type="+this.SelectedBillType+"&date1="+this.date1+"&date2="+this.date2).subscribe(
        data => {
          console.log("THIS IS DATA: ",data)
          this.ListReportProd = data;
          this.isListProdFull = true;
          this.ListReportProd.forEach(element => {
            console.log()
            this.ListChecked.push(false);
          });
          this.SortBy(2);
          this.SortBy(2);
          this.mostrandoCargando = false;
          this.estadoCargando = "";
          /*//Genero datasource para la tabla
          this.dataSource.sort = this.sort;
          this.ELEMENT_DATA = [];
          this.ListReportProd.forEach(element => {
            let tmp:EstructuraDatos = {
              fecha:"",
              address:"",
              body:"",
              cliente:"",
              mail:"",
              numdocumento:"",
              numpedido:"",
              phone:"",
              tienda:"",
            };
            for(let j = 0;j<this.displayedColumns.length;j++){
              tmp[this.displayedColumns[j]] = element[this.displayedColumns[j]];
            }
            this.ELEMENT_DATA.push(tmp);
          });
          this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);*/
        }
    )
  }

  allChecked(el){
    //if(el){
    for(let i=0; i< this.ListChecked.length;i++){
      this.ListChecked[i]=el;
    }
    //}
  }

  cleanList(){
    this.ListReportProd = []
    this.ListChecked = []
  }

  cleanList2(){
    this.ListReportProd = []
    this.ListChecked = []
    if(this.SelectedBillType=='1' || this.SelectedBillType=='2' || this.SelectedBillType=='3' || this.SelectedBillType=='4' )
    { this.SelectedBillState = '1' }
    if(this.SelectedBillType=='86')
    { this.SelectedBillState = '62' }
    if(this.SelectedBillType=='87')
    { this.SelectedBillState = '61' }
    if(this.SelectedBillType=='88')
    { this.SelectedBillState = '1' }
    if(this.SelectedBillType=='89')
    { this.SelectedBillState = '65' }
    if(this.SelectedBillType=='90')
    { this.SelectedBillState = '67' }
  }

  showList(){
    this.checked = false;
  }

  goToSubMenu(element){
    var dialogRef
    dialogRef = this.dialog.open(PedidosDetailComponent,{
      height: '90vh',
      width: '90vw',
      maxWidth:'90vw',
      data: {
        elem: element
      }
    })
  }

  getPicking(){
    let billList = [];

    for(let i = 0; i < this.ListChecked.length; i++){
      if(this.ListChecked[i]){
        billList.push(this.ListReportProd[i].id_BILL);
        console.log(billList);
      }
      if(i+1==this.ListChecked.length){
        var dialogRef
        dialogRef = this.dialog.open(PedidosDetail2Component,{
          height: '90vh',
          width: '90vw',
          maxWidth:'90vw',
          data: {
            elem: billList
          }
        })
      }
    }

    
  }

  pedidoInterno(){
    console.log("is null");
    return false;
  }


  

  changeState(data){
    var dialogRef
    dialogRef = this.dialog.open(StatechangeComponent,{
      height: '90vh',
      width: '90vw',
      maxWidth: '90vw',
      data: {
        elem: data
      }
    }).afterClosed().subscribe(res=>{
      this.getRepProdList()
    })
  }

  dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function (a,b) {
      /* next line works with strings and numbers,
       * and you may want to customize it to your needs
       */
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
    }
  }

  getRepProdList2(){
    console.log("http://tienda724.com:8448/v1/reorder2?id_third="+this.locStorage.getThird().id_third+"&id_store="+this.id_store)
    this.http2.get("http://tienda724.com:8448/v1/reorder/reorder2?id_third="+this.locStorage.getThird().id_third+"&id_store="+this.id_store+"&hours="+this.hours2).subscribe(
        data => {
          console.log("THIS IS DATA: ",data)
          if(this.type2=="1"){
            //@ts-ignore
            this.ListReportProd2 = data.sort(this.dynamicSort("linea"));
          }
          if(this.type2=="2"){
            //@ts-ignore
            this.ListReportProd2 = data.sort(this.dynamicSort("categoria"));
          }
          if(this.type2=="3"){
            //@ts-ignore
            this.ListReportProd2 = data.sort(this.dynamicSort("marca"));
          }
          this.isListProdFull2 = true;
        }
    )
  }

  transformDate(date){
    return this.datePipe.transform(date, 'yyyy/MM/dd');
  }

  genPedido(){
    this.showNotification('top', 'center', 3, "<h3>El pedido se esta generando, por favor espera a la notificacion de <b>EXITO</b></h3> ", 'info')
    try{
      console.log("THIS IS JSON, ", JSON.stringify({reorder: this.ListReportProd2,idstore: this.id_store}))
      this.http2.post("http://tienda724.com:8448/v1/pedidos/detailing",{reorder: this.ListReportProd2,idstore: this.id_store},{responseType: 'text'}).subscribe(
          response => {
            this.showNotification('top', 'center', 3, "<h3>El pedido se realizo con <b>EXITO</b></h3> ", 'success')
          })

    }catch(e){
      this.showNotification('top', 'center', 3, "<h3>El pedido presento <b>PROBLEMAS</b></h3> ", 'danger')
    }
  }

  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
      type: typeStr ? typeStr : type[id_type ? id_type : 2],
      timer: 200,
      placement: {
        from: from,
        align: align
      }
    });
  }
}

export interface EstructuraDatos {
  fecha: string;
  numdocumento: string;
  numpedido: string;
  cliente: string;
  tienda: string;
  address: string;
  mail: string;
  phone: string;
  body: string;
}

