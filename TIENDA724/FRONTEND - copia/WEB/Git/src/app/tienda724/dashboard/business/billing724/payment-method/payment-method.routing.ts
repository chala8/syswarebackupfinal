import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { PaymentDataComponent } from './payment-data/payment-data.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/


export const PaymentMethodRouting: Routes = [
  
    { path: 'payment', component: null,
      children: [
        { path: '', redirectTo: 'data', pathMatch: 'full'},
        { path: 'data', component: PaymentDataComponent},
         
  
      ]
    }
  
  ]