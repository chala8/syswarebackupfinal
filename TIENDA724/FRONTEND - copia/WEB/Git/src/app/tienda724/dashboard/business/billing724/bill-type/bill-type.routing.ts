import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { BillTypeDataComponent } from './bill-type-data/bill-type-data.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/


export const BillTypeRouting: Routes = [
  
    { path: 'bill-type', component: null,
      children: [
        { path: '', redirectTo: 'data', pathMatch: 'full'},
        { path: 'data', component: BillTypeDataComponent},
         
  
      ]
    }
  
  ]