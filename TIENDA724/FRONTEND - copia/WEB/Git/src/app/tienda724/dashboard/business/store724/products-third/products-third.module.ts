import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../../app.material';

/*
************************************************
*     principal component
*************************************************
*/
import { ProductThirdListComponent } from './product-third-list/product-third-list.component';
import { ProductThirdDataComponent } from './product-third-data/product-third-data.component';
import { ProductThirdNewComponent } from './product-third-new/product-third-new.component';
import { ProductThirdEditComponent } from './product-third-edit/product-third-edit.component';
import { ProductThirdDetailComponent } from './detail/product-third-detail.component'



/*
************************************************
*     modules of  your app
*************************************************
*/
import { ProductsModule } from '../products/products.module' 
/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { ProductThirdService } from './product-third.service';

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/
@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    RouterModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule,
    ProductsModule

  ],
  declarations: [ProductThirdListComponent, ProductThirdDataComponent, ProductThirdNewComponent, ProductThirdEditComponent, ProductThirdDetailComponent],
  providers:[ProductThirdService],
  exports:[ProductThirdNewComponent]
})
export class ProductsThirdModule { }
