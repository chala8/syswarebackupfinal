import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { TransactionConfirmDialogComponent } from '../transaction-confirm-dialog/transaction-confirm-dialog.component';
import { DialogData } from '../quantity-dialog/quantity-dialog.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-notes-on-order',
  templateUrl: './notes-on-order.component.html',
  styleUrls: ['./notes-on-order.component.scss']
})
export class NotesOnOrderComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TransactionConfirmDialogComponent>,public dialog: MatDialog 
    ,@Inject(MAT_DIALOG_DATA) public data: DialogData, public http2: HttpClient) { }

  ngOnInit() {
  }

  notes="";

  accept(){
    if(this.notes==""){
      //@ts-ignore
      this.http2.post("http://tienda724.com:8448/v1/pedidos/addNotesBill?numpedido="+this.data.elem.numdocumento+"&notas="+this.notes+"&idbc="+this.data.elem.id_BILL+"&state=INTERNO-OK&idstore="+this.data.elem.id_STORE_CLIENT,{},{}).subscribe(item => {
        this.dialogRef.close();
      })
    }else{
      //@ts-ignore
      this.http2.post("http://tienda724.com:8448/v1/pedidos/addNotesBill?numpedido="+this.data.elem.numdocumento+"&notas="+this.notes+"&idbc="+this.data.elem.id_BILL+"&state=INTERNO-NOVEDAD&idstore="+this.data.elem.id_STORE_CLIENT,{},{}).subscribe(item => {
        this.dialogRef.close();
      })
    }
     //@ts-ignore
    console.log(this.data.elem.numdocumento);
  }


}

export interface DialogData {
  elem: any;
}