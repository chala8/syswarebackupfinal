import { Component, OnInit, ViewChild, ElementRef, SystemJsNgModuleLoader } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router'
import { LocalStorage } from '../../../../../../shared/localStorage';
import {FormControl} from '@angular/forms';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';

import {MatDialog, MatSnackBar} from '@angular/material';
/* import components */
import { ReportesComponent } from '../../billing/bill-main/reportes/reportes.component';
import { QuantityDialogComponent } from '../bill-main/quantity-dialog/quantity-dialog.component';
import { ThirdDialogComponent } from '../bill-main/third-dialog/third-dialog.component';
import { CloseBoxComponent } from '../bill-main/close-box/close-box.component';
import { CategoriesComponent } from '../bill-main/categories/categories.component';
import { EmployeeDialogComponent } from '../bill-main/employee-dialog/employee-dialog.component';
import { PersonDialogComponent } from '../bill-main/person-dialog/person-dialog.component';
import { SearchClientDialogComponent } from '../bill-main/search-client-dialog/search-client-dialog.component';
import { SearchProductDialogComponent } from '../bill-main/search-product-dialog/search-product-dialog.component';
import { InventoryDetail } from '../../../store724/inventories/models/inventoryDetail';
import { InventoryQuantityDTO } from '../../../store724/inventories/models/inventoryQuantityDTO';
import { CommonStateDTO } from '../../commons/commonStateDTO';
import { DocumentDTO } from '../../commons/documentDTO'; 
import { DetailBillDTO } from '../models/detailBillDTO';
import { BillDTO } from '../models/billDTO';
import { BillingService } from '../billing.service';
import { InventoriesService } from '../../../store724/inventories/inventories.service';
import { Token } from '../../../../../../shared/token';
import { PurchaseDetailDialogComponent } from '../bill-main/purchase-detail-dialog/purchase-detail-dialog.component';
import { Urlbase } from '../../../../../../shared/urls';
import { HttpClient } from '@angular/common/http';
import { ClientData } from '../models/clientData';
import { InventoryComponent } from '../bill-main/inventory/inventory.component';
import { StoresComponent } from '../bill-main/stores/stores.component';
import { UpdateLegalDataComponent } from '../bill-main/update-legal-data/update-legal-data.component';
import { NewProductStoreComponent } from '../bill-main/new-product-store/new-product-store.component';
import { ThirdService } from '../../../thirds724/third/third.service';
import { BillComponent } from '../bill/bill.component'
import { RefundBillComponent } from '../../billing/refund-bill/refund-bill.component'
import { BillInventoryComponentComponent } from '../bill-main/bill-inventory-component/bill-inventory-component.component';
import {DatePipe} from '@angular/common';

declare var $: any;
@Component({
  selector: 'in-bill-main',
  templateUrl: './in-bill.component.html',
  styleUrls: ['./in-bill.component.scss']
})
export class InBillComponent implements OnInit {
  api_uri = Urlbase[2];

  id_store = this.locStorage.getIdStore();
  CUSTOMER_ID
  // venta =1
  // venta con remision 107

  //list
  itemLoadBilling: InventoryDetail;
  // inventoryList: InventoryDetail[];
  inventoryList: InventoryName[];
  codeList: any;
  categoryList = [] as any[];
  notCategoryList: any;
  productList: any;
  categoryIDboolean = false;
  categoryID: number;
  usableProducts = [] as any[];
  productsByCategoryList = [] as any [];
  codesByProductList = [] as any [];
  codesByCategoryList = [] as any [];
  tab: number = 0;
  selected = new FormControl(0);
  CategoryName: string = "";
  taxesList: any;
  state: any;
  paymentDetail: string = '[{';
  paymentDetailObservable = this.httpClient;
  tipoFactura: string = "entrada";
  isTotalDev: boolean = false;
  idBillOriginal: String = "";
  storageList;
  priceList = []; 
  pdfDatas: pdfData;


  storageActual: string = "1";

  flag = false;

  FechaDevolucion: String="--/--/--";
  NotasDevolucion: String="--Sin notas--";
  TotalDevolucion: String="--No se Cargo el Total--";
  IdBillDevolucion: String="";
  IdDocumentDevolucion: String="";
  IdBillState;
  botonDevolucionActive: Boolean = true;

  // DTO's
  inventoryQuantityDTO: InventoryQuantityDTO;
  inventoryQuantityDTOList: InventoryQuantityDTO[];
  detailBillingDTOList: any[]
  commonStateDTO: CommonStateDTO;
  documentDTO: DocumentDTO;
  detailBillDTO: DetailBillDTO;
  detailBillDTOList: DetailBillDTO[];
  billDTO: BillDTO;

  public productsObject = [];
  public setObject = [];
  public removalArray = new Set();

  public flagVenta = true;
  public vendedor = {};
  public animal;
  public name;
  public form;
  public token:Token;
  public clientData: ClientData;

  public searchData;
  public searchInput : string ="";
  public ObservationsInOrOut: string ="";

  private headers = new Headers();
  private options: RequestOptions;
  @ViewChild('nameit') private elementRef: ElementRef;
  currentBox;
  idThird;
  myBox;
  inputForCode = "";

  SelectedBillState="1";

  listaElem = [];
  disableDoubleClickSearch = false;
  mostrandoCargando = false;


  constructor(private datePipe: DatePipe, public thirdService: ThirdService,private http2: HttpClient,private http: Http, public locStorage: LocalStorage,private fb: FormBuilder, private billingService: BillingService,
    public inventoriesService: InventoriesService, public dialog: MatDialog, private httpClient: HttpClient) {

    this.clientData = new ClientData(true, 'Cliente Ocacional', '--', '000', 'N/A', '000', 'N/A', null);
    this.detailBillingDTOList = []
    this.inventoryList = []
    this.inventoryQuantityDTOList = [];
    this.detailBillDTOList = [];

    this.commonStateDTO = new CommonStateDTO(1, null, null);
    this.documentDTO = new DocumentDTO(null, null);
    this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null,null)
    this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO, null);
    this.billDTO = new BillDTO(this.locStorage.getIdStore(),null, null, null, null, null, null, null, null, null, null, null, null, this.commonStateDTO, null, this.detailBillDTOList, this.documentDTO);
    this.createControls();
    this.logChange();
  }

  openCompra(){
    var dialogRef;
            dialogRef = this.dialog.open(BillComponent, {
              width: '60vw',
              data: {},
              disableClose: false
            });

  }

  getPriceList2(product,code,id){
    console.log("id is: ",id)
    this.http2.get("http://tienda724.com:8447/v1/store/pricelist?id_product_store="+id).subscribe(response => {
      console.log("This is picked price list: ",response);
      var datos = response;
      console.log("this is datos: ",datos)
        if (product) {
          let new_product = {
            quantity: 1,
            id_storage: this.storageList[0].id_storage,
            price: product.standard_PRICE,
            tax: this.getPercentTax(product.id_TAX),
            id_product_third: product.id_PRODUCT_STORE,
            tax_product: product.id_TAX,
            state: this.commonStateDTO,
            description: product.product_STORE_NAME,
            code: product.code,
            id_inventory_detail: product.id_INVENTORY_DETAIL,
            ownbarcode: product.ownbarcode,
            product_store_code: String(product.product_STORE_CODE),
            pricelist: response,
            priceGen: response[0].price,
            standarPrice: product.standard_PRICE,
            productStoreId: id
          };
          new_product.price = product.standard_PRICE

          //this.detailBillDTOList.push(new_product);
          this.productsObject.unshift(new_product);
          this.setObject = Object.keys(this.productsObject);
  
          // event.target.blur();
          // event.target.value = '';
          // setTimeout(() => {
          //   document.getElementById('lastDetailQuantity');
          // });
        } else {
          var codeList;
          this.http2.get("http://tienda724.com:8447/v1/products2/code/general?code="+String(code)).subscribe(data => {
            codeList = data;
            console.log("this is codeList: ", codeList);
          //@ts-ignore
          if( data.length == 0 ){
            alert('El codigo no esta asociado a un producto');
          }else{
            var dialogRef;
            dialogRef = this.dialog.open(NewProductStoreComponent, {
              width: '30vw',
              data: {codeList: codeList[0]}
            });
            dialogRef.afterClosed().subscribe(res=>{
              this.ngOnInit()
            })
          // let body = {
          //   id_store: this.locStorage.getIdStore(),
          //   id_code: codeList[0],
          //   product_store_name: "new",
          //   product_store_code: 123456,
          //   ownbarcode: "av12312312"
          // }
          // console.log(JSON.stringify(body));
          // this.http.post("http://tienda724.com:8447/v1/store/ps",body,this.options).subscribe(data=>
          // {
          //   console.log("Post hecho");});
          }
          });
          
        } 

  });
}

  addDetail2(code) {
    this.disableDoubleClickSearch = true;
    // console.log(code,"da code :3")
    
    // var codeFilter = this.productsObject.filter(item => (String(item.code) === code || String(item.ownbarcode) === code || String(item.product_store_code) === code ));
    
    // console.log(codeFilter);
    var key: any = null;
    this.productsObject.forEach(item => {
      // console.log("//-----------------------------------");
      // console.log(item.code);
      // console.log("//-----------------------------------");
      // console.log(item.ownbarcode);
      // console.log("//-----------------------------------");
      // console.log(item.product_store_code);
      // console.log("//-----------------------------------");
      if(item.code == code || item.ownbarcode == code || String(item.product_store_code) == code ){
        key = item;
      }
      // else{
      // }
    });
      // console.log("this is array papu: ",this.productsObject)
      // console.log("this is key", key)
    if (key != null) {
      // console.log("entro")
      this.productsObject[this.productsObject.indexOf(key)].quantity += 1;
      this.disableDoubleClickSearch = false;
    } else {
      
      var product = this.inventoryList.find(item => (item.code == code || item.ownbarcode == code || String(item.product_STORE_CODE) == code));
          //@ts-ignore
          if(product){
            this.disableDoubleClickSearch = false;
            this.getPriceList(product,code,product.id_PRODUCT_STORE)
          }else{
            this.disableDoubleClickSearch = false;
            alert('Ese codigo no esta asociado a un producto.');
          }
    }
  }

  openCategories2(){
    console.log("THIS IS ROLES: ", this.locStorage.getTipo())

    var modal;
    modal = this.dialog.open(BillInventoryComponentComponent, {
      width: '675px',
      height: '450px',
      data: {}
    }); 

    modal.afterClosed().subscribe(response => {
      console.log("ESTE ES EL CODIGO DE BARRAS QUE QUIERO EJECUTAR: ",this.locStorage.getCodigoBarras())  
      if(this.locStorage.getCodigoBarras() != "-1"){
        this.addDetail2(this.locStorage.getCodigoBarras())
        this.locStorage.setCodigoBarras("-1");
      }
    });
  }

  openDevolucion(){
    var dialogRef;
            dialogRef = this.dialog.open(RefundBillComponent, {
              width: '60vw',
              data: {},
              disableClose: false
            });

  }

  ngOnInit() {

    setTimeout(() => {

      this.elementRef.nativeElement.focus();
    this.elementRef.nativeElement.select();

  }, 100);

    console.log(this.locStorage.getMenu(),"lo menu Xddd")

    let idPerson = this.locStorage.getPerson().id_person;
    
    this.thirdService.getThirdList().subscribe(res=>{
    // @ts-ignore
    let employee = res.filter(item=> item.profile.id_person === idPerson);
    this.idThird = employee[0].id_third;
    localStorage.setItem("id_employee",String(this.idThird));
    this.billingService.getCajaByIdStatus(String(this.idThird)).subscribe(res=>{
      this.myBox = res;
      localStorage.setItem("myBox",res);
      this.currentBox = localStorage.getItem("currentBox");
      console.log("this is res",res);

     
     })
    });
    this.state = {
      state:1
    };
    this.loadData();
    this.token = this.locStorage.getToken();
    this.CUSTOMER_ID= this.token.id_third_father;
    this.vendedor['fullname'] = this.locStorage.getPerson().info.fullname;
    this.vendedor['cargo'] = this.locStorage.getRol().map((item) => {
      return item.rol;
    }).join(" / ");
    // this.getInventoryList(this.STATE, null, null, this.ID_INVENTORY_TEMP);
    this.getInventoryList(this.locStorage.getIdStore());
    this.getCodes();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());
    let token = localStorage.getItem('currentUser');
    this.options = new RequestOptions({ headers: this.headers });
    this.getStorages(); 
  } 

  getStorages() {
    this.http2.get("http://tienda724.com:8447/v1/store/s?id_store="+this.id_store).subscribe((res)=>{
      this.storageList = res;
    });

  }

  getBillData(event){
    this.httpClient.get("http://tienda724.com:8448/v1/billing-state/billData?consecutive="+event.target.value).subscribe((res)=>{
      var refund = res;
      try{
        this.FechaDevolucion = refund[0].purchase_DATE;
        this.NotasDevolucion = refund[0].body;
        this.TotalDevolucion = refund[0].totalprice;
        this.IdBillDevolucion = refund[0].id_BILL;
        this.IdDocumentDevolucion = refund[0].id_DOCUMENT;
        this.IdBillState = refund[0].id_BILL_STATE;
        this.botonDevolucionActive = false;
        this.showNotification('top', 'center', 3, "<h3>LA FACTURA FUE CARGADA CORRECTAMENTE</h3> ", 'info');
      }catch(e){
        this.showNotification('top', 'center', 3, "<h3>NO SE ENCONTRO EL CONSECUTIVO DE FACTURA SOLICITADO</h3> ", 'danger');
      }
  });

  }

  anularFacturaPorDevolucion(){
    if(this.IdBillState != 41){
      var state= 41;
      this.http.put("http://tienda724.com:8448/v1/billing-state/billState/"+this.IdBillDevolucion,null,{params: {'state': state}}).subscribe((res)=>{
        var notas = this.NotasDevolucion + " / Factura Anulada Por Motivo de Devolucion";
        this.http.put("http://tienda724.com:8448/v1/billing-state/documentBody/"+this.IdDocumentDevolucion,null,{params: {'body': notas}}).subscribe((resp)=>{ 
          this.showNotification('top', 'center', 3, "<h3>LA FACTURA FUE ANULADA CORRECTAMENTE</h3> ", 'success');
      });  
    });
    }else{
      this.showNotification('top', 'center', 3, "<h3>LA FACTURA YA SE ENCUENTRABA ANULADA POR DEVOLUCION</h3> ", 'danger');
    }
    
    this.FechaDevolucion="--/--/--";
    this.NotasDevolucion="--Sin notas--";
    this.TotalDevolucion="--No se Cargo el Total--";
    this.IdBillDevolucion="";
    this.IdDocumentDevolucion="";
    this.IdBillState = 0;
    this.botonDevolucionActive = true;

  }

  resetCategoryMenu(){
    this.categoryIDboolean = false;
    this.categoryID = null;
    this.productsByCategoryList = [] as any[];
    this.codesByProductList = [] as any[];
    this.codesByCategoryList = [] as any[];
    this.CategoryName = "";
  }  

  getTaxes(){
    this.httpClient.get(this.api_uri+'/tax-tariff').subscribe((res)=>{
        this.taxesList = res;
        this.getCategoryList();
    });  
     
  }

  getCodes(){
    this.httpClient.get(this.api_uri+'/codes').subscribe((res)=>{
        this.codeList = res;
        this.getCategories();
    });   
  }

  openCierreCaja(){

      var dialogRef;
              dialogRef = this.dialog.open(CloseBoxComponent, {
                width: '60vw',
                data: {}
              });

  }

  openCategories(){
    var dialogRef;
            dialogRef = this.dialog.open(CategoriesComponent, {
              width: '60vw',
              data: {}
            });

  }

  openInventories(){
    var dialogRef;
            dialogRef = this.dialog.open(InventoryComponent, {
              width: '60vw',
              data: {}
            });

  }

  openStores(){
    var dialogRef;
            dialogRef = this.dialog.open(StoresComponent, {
              width: '60vw',
              data: {}
            });

  }

  openUpdateLegalData(){
    var dialogRef;
    dialogRef = this.dialog.open(UpdateLegalDataComponent, {
      width: '60vw',
      data: {}
    });
  }

  getProducts(){
    this.httpClient.get(this.api_uri+'/products').subscribe((res)=>{
      this.productList = res;
      this.getTaxes();
    });
  }

  getCategories(){
    this.httpClient.get(this.api_uri+'/categories').subscribe((res)=>{
      this.notCategoryList = res;
      this.getProducts();
    });
  }

  getPercentTax(idTaxProd){
      var thisTax;
      var taxToUse;
      
      for (thisTax in this.taxesList){
          if(idTaxProd == this.taxesList[thisTax].id_tax_tariff){
            taxToUse = this.taxesList[thisTax].percent/100;
          }
      }

      return taxToUse;
  }

  getCodesByCategory(){
    var product: any;
    for (product in this.usableProducts){
      var compProduct = this.usableProducts[product]
        if(compProduct.id_category == this.categoryID){
          if(!this.productsByCategoryList.includes(compProduct)){
            this.productsByCategoryList.push(compProduct);
          }
        }

    }
    var code: any;
    for (code in this.codeList){
      var compCode = this.codeList[code]
      var product:any;
      for (product in this.productList){
        var compProduct = this.productList[product]
        if(compCode.id_product == compProduct.id_product){
          compCode.img_url = compProduct.img_url; 
          compCode.id_category = compProduct.id_category;
          compCode.description_product = compProduct.description_product;
          compCode.name_product = compProduct.name_product;
          var elem = this.inventoryList.find(item => item.code == compCode.code);
          compCode.price = elem.standard_PRICE;
          if(!this.codesByProductList.includes(compCode)){
            this.codesByProductList.push(compCode);
          }
        }
      }
    }
    var codeKey: any;
    for (codeKey in this.codeList){
     var compCode = this.codesByProductList[codeKey]
       if(compCode.id_category == this.categoryID){
         if(!this.codesByCategoryList.includes(compCode)){
           this.codesByCategoryList.push(compCode);
         }
       }
    }
    return this.codesByCategoryList;
  }

  getCategoryList(){
    this.codeList.forEach(code => {

       var aux = this.productList.filter(
        product => {
          return code.id_product == product.id_product             
        }
      )[0];

      if(!this.usableProducts.includes(aux)){
        this.usableProducts.push(aux)
      }
      
    });


    this.usableProducts.forEach(prod => {

      var aux = this.notCategoryList.filter(
       cat => {
         return prod.id_category == cat.id_category;             
       }
     )[0];

     if(!this.categoryList.includes(aux)){
       this.categoryList.push(aux)
     }
     
   });


    // this.codeList.forEach(code => {
    //   this.usableProducts.push( this.productList.filter(
    //     product => {
    //       return code.id_product == product.id_product             
    //     }
    //   )[0]);
    // });

    
    // console.log(this.usableProducts)
    // console.log(this.categoryList)
    
    
    


    // for (code in this.codeList){
    //   var compCode = this.codeList[code]
    //   var product:any;
    //   for (product in this.productList){
    //     var compProduct = this.productList[product]
    //     if(compCode.id_product == compProduct.id_product){
          
    //       if(!this.usableProducts.includes(compProduct)){
    //         this.usableProducts.push(compProduct);
    //       }
    //     }
    //   }
    // }
    
    var useProduct: any;
    var category: any;


    // for(useProduct in this.usableProducts){
    //   var compUseProduct = this.usableProducts[useProduct]
    //   for (category in this.notCategoryList){
    //     var compCategory = this.notCategoryList[category];
    //     if(compUseProduct.id_category == compCategory.id_category){
    //       if(!this.categoryList.includes(compCategory)){
    //         this.categoryList.push(compCategory);
    //       }
    //     }
    //   }     
    // } 
    
    

  }

  setCategoryID(id,name){
    this.categoryID = id;
    this.categoryIDboolean = true;
    this.CategoryName = name;
  }

  getPriceList(product,code,id){
    console.log("id is: ",id)
    this.http2.get("http://tienda724.com:8447/v1/store/pricelist?id_product_store="+id).subscribe(response => {
      console.log("This is picked price list: ",response);
      var datos = response;
      console.log("this is datos: ",datos)
        if (product) {
          let new_product = {
            quantity: 1,
            id_storage: this.storageList[0].id_storage,
            price: product.standard_PRICE,
            tax: this.getPercentTax(product.id_TAX),
            id_product_third: product.id_PRODUCT_STORE,
            tax_product: product.id_TAX,
            state: this.commonStateDTO,
            description: product.product_STORE_NAME,
            code: product.code,
            id_inventory_detail: product.id_INVENTORY_DETAIL,
            ownbarcode: product.ownbarcode,
            product_store_code: String(product.product_STORE_CODE),
            pricelist: response,
            priceGen: response[0].price,
            standarPrice: product.standard_PRICE,
            productStoreId: id
          };
          new_product.price = product.standard_PRICE

          //this.detailBillDTOList.push(new_product);
          this.productsObject.unshift(new_product);
          this.setObject = Object.keys(this.productsObject);
  
          // event.target.blur();
          // event.target.value = '';
          // setTimeout(() => {
          //   document.getElementById('lastDetailQuantity');
          // });
        } else {
          var codeList;
          this.http2.get("http://tienda724.com:8447/v1/products2/code/general?code="+String(code)).subscribe(data => {
            codeList = data;
            console.log("this is codeList: ", codeList);
          //@ts-ignore
          if( data.length == 0 ){
            alert('El codigo no esta asociado a un producto');
          }else{
            var dialogRef;
            dialogRef = this.dialog.open(NewProductStoreComponent, {
              width: '30vw',
              data: {codeList: codeList[0]}
            });
            dialogRef.afterClosed().subscribe(res=>{
              this.ngOnInit()
            })
          // let body = {
          //   id_store: this.locStorage.getIdStore(),
          //   id_code: codeList[0],
          //   product_store_name: "new",
          //   product_store_code: 123456,
          //   ownbarcode: "av12312312"
          // }
          // console.log(JSON.stringify(body));
          // this.http.post("http://tienda724.com:8447/v1/store/ps",body,this.options).subscribe(data=>
          // {
          //   console.log("Post hecho");});
          }
          });
          
        } 

  });
}

  addDetail(event) {
    console.log("THIS IS DATA: ",this.productsObject )
    var code = String(event.target.value);
    console.log(code);
    // console.log(code,"da code :3")

    // var codeFilter = this.productsObject.filter(item => (String(item.code) === code || String(item.ownbarcode) === code || String(item.product_store_code) === code ));

    // console.log(codeFilter);
    var key: any = null;
    this.productsObject.forEach(item => {
      // console.log("//-----------------------------------");
      // console.log(item.code);
      // console.log("//-----------------------------------");
      // console.log(item.ownbarcode);
      // console.log("//-----------------------------------");
      // console.log(item.product_store_code);
      // console.log("//-----------------------------------");
      if(item.code == code || item.ownbarcode == code || String(item.product_store_code) == code ){
        key = item;
        console.log("THIS IS ITEM, ", item)
      }
      // else{
      // }
    });
      // console.log("this is array papu: ",this.productsObject)
      // console.log("this is key", key)
    if (key != null) {
      // console.log("entro")
      this.productsObject[this.productsObject.indexOf(key)].quantity += 1;
      event.target.value = '';
    } else {
      var product = this.inventoryList.find(item => (item.code == code || item.ownbarcode == code || String(item.product_STORE_CODE) == code));
      console.log("this is inventory List: ", JSON.stringify(this.inventoryList))
       //@ts-ignore
       if(product){
        this.getPriceList(product,code,product.id_PRODUCT_STORE)
      }else{
        this.getPriceList(product,code,-1)
      }
    }
  }

  addDetailFromCat(code) {
    if (this.productsObject.hasOwnProperty(code)) {
      this.productsObject[code]['quantity'] += 1;
      this.setObject = Object.keys(this.productsObject);
      this.resetCategoryMenu();
      this.tab = 0;
    } else {
      var product = this.inventoryList.find(item => item['code'] == code);
      if (product) {
        let new_product = {
          quantity: 1,
          id_storage: this.storageList[0].id_storage,
          price: product.standard_PRICE,
          tax: this.getPercentTax(product.id_TAX),
          id_product_third: product.id_PRODUCT_STORE,
          tax_product: product.id_TAX,
          state: this.commonStateDTO,
          description: product.product_STORE_NAME,
          code: product.code,
          id_inventory_detail: product.id_INVENTORY_DETAIL
        };
        //this.detailBillDTOList.push(new_product);
        this.productsObject[code] = new_product;
        this.setObject = Object.keys(this.productsObject);

        setTimeout(() => {
          document.getElementById('lastDetailQuantity');
        });
        this.resetCategoryMenu()
        this.tab = 0;
      } else {
        this.resetCategoryMenu();
        this.showNotification('top','center',3,"<h3 class = 'text-center'>El producto solicitado no existe<h3>",'warning');
        this.tab = 0;
      }
    }
  }

  addDetailBuy(event) {
    var code = String(event.target.value);
    if (this.productsObject.hasOwnProperty(code)) {
      //this.productsObject[code]['quantity'] += 1;
      //this.setObject = Object.keys(this.productsObject);
      event.target.value = '';
      this.openDialogPurchaseDetail(this.productsObject[code]);
    } else {
      var product = this.inventoryList.find(item => item['code'] == code);
      if (product) {
        let new_product = {
          quantity: 1,
          id_storage: this.storageList[0].id_storage,
          tax: 0,
          id_product_third: product.id_PRODUCT_STORE,
          tax_product: product.id_TAX,
          state: this.commonStateDTO,
          description: product.product_STORE_NAME,
          code: product.code,
          id_inventory_detail: product.id_INVENTORY_DETAIL,
          purchaseTaxIncluded:true,
          purchaseValue:0,
          saleTaxIncluded:false,
          saleValue:product.standard_PRICE,
          flagNewProduct:false
        };

        event.target.value = '';
        this.openDialogPurchaseDetail(new_product);
        // event.target.blur();
        // setTimeout(() => {
        //   document.getElementById('lastDetailQuantity').focus();
        // });
      } else {
        let new_product = {
          quantity: 1,
          id_storage: this.storageList[0].id_storage,
          tax: 0.19,
          id_product_third: 0,
          tax_product: 0,
          state: this.commonStateDTO,
          description: '',
          code: code,
          id_inventory_detail: 0,
          purchaseTaxIncluded:true,
          purchaseValue:0,
          saleTaxIncluded:false,
          saleValue:0,
          flagNewProduct:true
        };
        event.target.value = '';
        this.openDialogPurchaseDetail(new_product);
      }
    }
  }

  calculateSubtotal() {
    var subtotal = 0;
    var attr= this.flagVenta?'price':'purchaseValue';
    for (let code in this.productsObject) {      
      subtotal += this.productsObject[code][attr] * this.productsObject[code]['quantity'];
    }
    return subtotal;
  }

  calculateTax() {
    var tax = 0;
    // if (!this.form.value['isRemission']) {
      for (let code in this.productsObject) {
        if(this.flagVenta){
          tax += this.productsObject[code]['price'] * this.productsObject[code]['quantity'] * this.productsObject[code]['tax'];
        }else{
          tax += this.productsObject[code]['purchaseValue'] * this.productsObject[code]['quantity'] * this.productsObject[code]['tax'];
        }
      }
    // }
    return tax;
  }

  calculateTotal() {
    return this.calculateSubtotal() + this.calculateTax();
  }

  checkItem(event, code) {
    if (event.target.checked) {
      this.removalArray.add(code);
    } else {
      this.removalArray.delete(code);
    }
  }

  checkAllItems(event) {
    if (event.target.checked) {
      this.removalArray = new Set(this.setObject);
    } else {
      this.removalArray = new Set();
    }
  }

  listLoad(){
    this.listaElem = [];
    console.log("THIS IS LISTA, ", this.listaElem)
    this.getElemsIfOwn().then(e => {
      console.log("1")
      this.getElemsIfcode().then(a => {
        console.log("2")
        //inserto elementos si name
        this.inventoryList.forEach(element => {
          if(element.product_STORE_NAME.toLowerCase( ).includes(this.inputForCode.toLowerCase())){
            this.listaElem.push(element);
          }
        })
      })
    })  
  }

  async getElemsIfOwn(){
    this.inventoryList.forEach(element => {
      if(element.ownbarcode==this.inputForCode){
        console.log("PUDE ENTRAR 1")
        this.listaElem.push(element);
      }
    })
  }

  async getElemsIfcode(){
    this.inventoryList.forEach(ele => {
      //@ts-ignore
      if(ele.product_STORE_CODE==this.inputForCode){
        console.log("PUDE ENTRAR 2")
        this.listaElem.push(ele);
      }
    })
  }

  removeItems() {
    this.removalArray.forEach((item) => {
      if (this.productsObject.hasOwnProperty(item)) {
        delete this.productsObject[item];
      }
    });
    this.removalArray = new Set();
    this.setObject = Object.keys(this.productsObject);
  }

  editPurchaseValue(event: any) {
    event.target.blur();
    setTimeout(() => {
      document.getElementById('lastDetailSaleValue').focus();
    });
  }

  readNew(event: any) {
    event.target.blur();
    setTimeout(() => {
      document.getElementById('reader').focus();
    });
  }

  cancel() {
    this.form.reset();
    this.clientData = new ClientData(true, 'Cliente Ocacional', '--', '000', 'N/A', '000', 'N/A',null);
    this.productsObject = []
    this.setObject = [];
    this.removalArray = new Set();
    this.CUSTOMER_ID= this.token.id_third_father;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(QuantityDialogComponent, {
      width: '40vw',
      data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
    });
  }

  openDialogClient(tipo): void {
    var dialogRef;
    if(tipo == 0){
    dialogRef = this.dialog.open(ThirdDialogComponent, {
      width: '60vw',
      data: {}
    });
    }

    if(tipo == 1){
      dialogRef = this.dialog.open(PersonDialogComponent, {
        width: '60vw',
        data: {}
      });
      }

    if(tipo == 2){
      dialogRef = this.dialog.open(EmployeeDialogComponent, {
        width: '60vw',
        data: {}
      });
      }

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.CUSTOMER_ID= result.id;
        // console.log('CREATE CLIENT SUCCESS');
        // console.log(result);
        let isNaturalPerson= result.data.hasOwnProperty('profile')?true:false;
        let dataPerson= isNaturalPerson?result.data.profile:result.data;
        this.clientData.is_natural_person = isNaturalPerson;
        this.clientData.fullname= dataPerson.info.fullname;
        this.clientData.document_type = dataPerson.info.id_document_type;
        this.clientData.document_number = dataPerson.info.document_number;
        this.clientData.address = dataPerson.directory.address;
        this.clientData.phone = dataPerson.directory.phones[0].phone;
        this.clientData.email = dataPerson.directory.hasOwnProperty('mails')?dataPerson.directory.mails[0].mail:'N/A';
      }
    });
  }

  openDialogSearchClient(): void {
    const dialogRef = this.dialog.open(SearchClientDialogComponent, {
      width: '40vw',
      data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
    });
  }

  openDialogSearchProduct(): void {
    const dialogRef = this.dialog.open(SearchProductDialogComponent, {
      width: '40vw',
      data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
    });
  }

  openDialogPurchaseDetail(product): void {
    const dialogRef = this.dialog.open(PurchaseDetailDialogComponent, {
      width: '60vw',
      data: product
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        if(result.flagNewProduct){
          // this.getInventoryList(this.STATE, null, null, this.ID_INVENTORY_TEMP).then(()=>{
          this.getInventoryList(this.id_store).then(()=>{
            var product = this.inventoryList.find(item => item['code'] == result.code);
            if (product) {
              let new_product = {
                quantity: 1,
                id_storage: this.storageList[0].id_storage,
                tax: this.getPercentTax(product.id_TAX),
                id_product_third: product.id_PRODUCT_STORE,
                tax_product: product.id_TAX,
                state: this.commonStateDTO,
                description: product.product_STORE_NAME,
                code: product.code,
                id_inventory_detail: product.id_INVENTORY_DETAIL,
                purchaseTaxIncluded:true,
                purchaseValue:0,
                saleTaxIncluded:false,
                saleValue:product.standard_PRICE,
                flagNewProduct:false
              };
              this.openDialogPurchaseDetail(new_product);
            }
          });
        }else{
          this.productsObject[result.code]= result;
          this.setObject = Object.keys(this.productsObject);
        }
      }
    });
  }

  openDialogTransactionConfirm(disc,isCompra): void {
    var result = 0;
    this.mostrandoCargando = true;
    //this.save(result,disc,isCompra);
    this.sendData(result,disc,isCompra)
  }

  // start controls
  createControls() {
    this.form = this.fb.group({

      isRemission: [true, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-z0-9_-]{8,15}$")
      ])],

      number_order: ['', Validators.compose([
        Validators.required
      ])],

      date: [{ value: '', disabled: true }, Validators.compose([
        Validators.required
      ])],

    });
  }

  loadData() {
    this.form.patchValue({
      isRemission: true,
      number_order: '',
      date: new Date()
    });
  }

  logChange() {
    const isRemission = this.form.get('isRemission');
    isRemission.valueChanges.forEach((value: boolean) => {
    });

    const listenerNumberOrder = this.form.get('number_order');
    listenerNumberOrder.valueChanges.forEach((value: string) => {
    });
  }// end controls

  async getInventoryList(store){
    this.inventoriesService.getInventory(store).subscribe((data: InventoryName[]) => {
      this.inventoryList = data;
        console.log("this is InventoryList: "+data)
      },
      (error) =>{
        console.log(error);
      },
      () => {
      if (this.inventoryList.length > 0) {

      }
    });
  }

  save(data,disc,isCompra) {
    let Temp_setObject = [...this.setObject]
    let Temp_productsObject = [...this.productsObject]
    this.billDTO.id_store = this.locStorage.getIdStore();
    if(data.id_person){
      this.billDTO.id_third_destiny = data.id_person;
    }
    this.detailBillDTOList = []
    this.commonStateDTO.state = 1
    this.inventoryQuantityDTOList = [];
    let titleString= this.form.value['isRemission']?' Por Remision':'';
    //
    this.documentDTO.title = 'Movimiento De Entrada';
    //
    this.documentDTO.body = this.ObservationsInOrOut;
    /**
     * building detailBill and Quantity
     */
    for (let code in this.productsObject) {
      let element = this.productsObject[code];

      this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null,null);
      this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO,null);

      //building detailBill
      this.detailBillDTO.id_storage = element.id_storage;
      this.detailBillDTO.price = (+element.price)
      this.detailBillDTO.tax = this.getPercentTax(element.tax_product)*100;
      this.detailBillDTO.id_product_third = element.id_product_third;
      this.detailBillDTO.tax_product = element.tax_product;
      this.detailBillDTO.state = this.commonStateDTO;
      this.detailBillDTO.quantity = Math.floor(element.quantity);

      if(element.quantity > 0) {
        this.detailBillDTOList.push(this.detailBillDTO);
      }
 
      // Inv Quantity
      this.inventoryQuantityDTO.id_inventory_detail = element.id_inventory_detail;
      this.inventoryQuantityDTO.id_product_third = element.id_product_third;
      this.inventoryQuantityDTO.code = element.code;
      this.inventoryQuantityDTO.id_storage = element.id_storage;

      // Inv Quantity for discount tienda
      this.inventoryQuantityDTO.quantity = Math.floor(element.quantity);

      this.inventoryQuantityDTOList.push(
        this.inventoryQuantityDTO
      );
    }

    if (this.detailBillDTOList.length > 0) {
      let ID_BILL_TYPE= this.form.value['isRemission']?107:1;
      this.billDTO.id_third_employee = this.token.id_third_father;
      this.billDTO.id_third = this.token.id_third;
      this.billDTO.id_bill_type = 3;
      this.billDTO.id_store = this.locStorage.getIdStore();
      //instanciar de acuerdo a por remision
      this.billDTO.id_bill_state = Number(this.SelectedBillState);
      this.billDTO.purchase_date = new Date();
      this.billDTO.subtotal = this.calculateSubtotal();
      this.billDTO.tax = this.calculateTax();
      this.billDTO.totalprice = this.calculateTotal();

      this.billDTO.discount = 0;
      this.billDTO.documentDTO = this.documentDTO;
      this.billDTO.state = null;

      this.billDTO.details = this.detailBillDTOList;

      console.log(JSON.stringify(this.billDTO),"This is Bill DTO");
      this.billingService.postBillResource(this.billDTO, disc)
        .subscribe( 
          result => {
            /*this.http2.get("http://tienda724.com:8448/v1/billing/master?id_bill="+result+"&id_store="+this.locStorage.getIdStore()).subscribe(
              dataresult => {
                this.http2.get("http://tienda724.com:8448/v1/billing/detail?id_bill="+result).subscribe(dataset => {
                  //@ts-ignore
                  //this.pdfDatas =  new pdfData (dataresult.fullname,dataresult.store_NAME,dataresult.purchase_DATE,this.locStorage.getIdCaja(),dataresult.prefix_BILL + " - " + dataresult.consecutive,dataset,dataresult.subtotal,dataresult.tax,dataresult.totalprice);

                });
              });*/
            if (result) {
              console.log("this is bull DTO",JSON.stringify(this.billDTO))
              // alert(this.ID_INVENTORY_TEMP)
              this.beginPlusOrDiscount(this.inventoryQuantityDTOList,disc)
              this.mostrandoCargando = false;
              this.paymentDetail = '[{';
              //GENERO EL PDF
              console.log("this.billDTO es ")
              console.log(this.billDTO)
              console.log("result es ")
              console.log(result)
              console.log("SERGIO: ",this.locStorage.getThird())
              console.log("SERGIO 2: ", this.locStorage.getPerson())
              //
              let detalles : String[][] = []
              for(let j = 0;j<Temp_setObject.length;j++){
                //Storage name
                let NombreStorage = "";
                this.storageList.forEach(storage => {
                  if(storage.id_storage == Temp_productsObject[Temp_setObject[j]].id_storage){
                    NombreStorage = storage.storage_name;
                  }
                })
                //codigo,producto,cantidad,ubicacion
                detalles.push([
                  Temp_productsObject[Temp_setObject[j]].code,
                  Temp_productsObject[Temp_setObject[j]].description,
                  Temp_productsObject[Temp_setObject[j]].quantity,
                  NombreStorage,
                ]);
              }
              //Razón
              let razon = "Otros";
              if(this.SelectedBillState == "1"){razon = "Otros";}
              else if(this.SelectedBillState == "301"){razon = "Entrada por vencimiento";}
              else if(this.SelectedBillState == "302"){razon = "Entrada por daño o averia";}
              else if(this.SelectedBillState == "303"){razon = "Entrada por devolucion a proveedor";}
              else if(this.SelectedBillState == "304"){razon = "Entrada por traslado de mercancia";}
              else if(this.SelectedBillState == "305"){razon = "Entrada por robo";}
              //
              var DataAEnviar = new pdfData_inOutbill(
                  detalles,
                  this.ObservationsInOrOut,
                  razon,
                  this.transformDateWithHour(this.billDTO.purchase_date),
                  this.locStorage.getThird().info.fullname,
                  this.locStorage.getThird().info.type_document + " "+ this.locStorage.getThird().info.document_number,
                  this.locStorage.getThird().id_third+"",
                  "1"//caso 1 es entrada
              );
              //
              console.log("this is DataAEnviar: ");
              console.log(DataAEnviar);
              console.log(JSON.stringify(DataAEnviar));
              this.http2.post(Urlbase[3]+"/billing/createPaymentDetail?idbill=" + result, {}).subscribe(resp => {
                                                                                                                                                      
                this.http2.put(Urlbase[3]+"/billing/domiciliario?id_bill="+result+"&id_third=-1",{}).subscribe(responseItem => {
                     
              this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
              this.http2.get(Urlbase[3]+"/billing/UniversalPDF?id_bill="+result,{responseType: 'text'}).subscribe(response =>{
                //-----------------------------------------------------------------------------------------------------------------
                window.open(Urlbase[7]+"/"+response, "_blank");
                this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
                this.mostrandoCargando = false;
                setTimeout(() => {
                
                  this.elementRef.nativeElement.focus();
                  this.elementRef.nativeElement.select();
                
                }, 100);
                //-----------------------------------------------------------------------------------------------------------------
              })
            })
          })
        })
            }
          });
    }
  }

  sendData(data,disc,isCompra){

    let detailList = [];
    //GENERO LA LISTA DE DTOs DE DETALLES
    this.productsObject.forEach(item => {
      detailList.push({cantidad:item.quantity,
        valor:item.price,
        idproductstore:item.id_product_third,
        id_impuesto:item.tax_product})
    }) 
    
    let Temp_setObject = [...this.setObject]
    let Temp_productsObject = [...this.productsObject]
    this.billDTO.id_store = this.locStorage.getIdStore();
    if(data.id_person){
      this.billDTO.id_third_destiny = data.id_person;
    }
    this.detailBillDTOList = []
    this.commonStateDTO.state = 1
    this.inventoryQuantityDTOList = [];
    let titleString= this.form.value['isRemission']?' Por Remision':'';
    //
    this.documentDTO.title = 'Movimiento De Entrada';
    //
    this.documentDTO.body = this.ObservationsInOrOut;
    /**
     * building detailBill and Quantity
     */
    for (let code in this.productsObject) {
      let element = this.productsObject[code];

      this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null,null);
      this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO,null);

      //building detailBill
      this.detailBillDTO.id_storage = element.id_storage;
      this.detailBillDTO.price = (+element.price)
      this.detailBillDTO.tax = this.getPercentTax(element.tax_product)*100;
      this.detailBillDTO.id_product_third = element.id_product_third;
      this.detailBillDTO.tax_product = element.tax_product;
      this.detailBillDTO.state = this.commonStateDTO;
      this.detailBillDTO.quantity = Math.floor(element.quantity);

      if(element.quantity > 0) {
        this.detailBillDTOList.push(this.detailBillDTO);
      }
 
      // Inv Quantity
      this.inventoryQuantityDTO.id_inventory_detail = element.id_inventory_detail;
      this.inventoryQuantityDTO.id_product_third = element.id_product_third;
      this.inventoryQuantityDTO.code = element.code;
      this.inventoryQuantityDTO.id_storage = element.id_storage;

      // Inv Quantity for discount tienda
      this.inventoryQuantityDTO.quantity = Math.floor(element.quantity);

      this.inventoryQuantityDTOList.push(
        this.inventoryQuantityDTO
      );
    }

    if (this.detailBillDTOList.length > 0) {
      let ID_BILL_TYPE= this.form.value['isRemission']?107:1;
      this.billDTO.id_third_employee = this.token.id_third_father;
      this.billDTO.id_third = this.token.id_third;
      this.billDTO.id_bill_type = 3;
      this.billDTO.id_store = this.locStorage.getIdStore();
      //instanciar de acuerdo a por remision
      this.billDTO.id_bill_state = Number(this.SelectedBillState);
      this.billDTO.purchase_date = new Date();
      this.billDTO.subtotal = this.calculateSubtotal();
      this.billDTO.tax = this.calculateTax();
      this.billDTO.totalprice = this.calculateTotal();

      this.billDTO.discount = 0;
      this.billDTO.documentDTO = this.documentDTO;
      this.billDTO.state = null;

      this.billDTO.details = this.detailBillDTOList;

      console.log(JSON.stringify(this.billDTO),"This is Bill DTO");
      
      console.log("THIS IS BILL DTO V2: ", JSON.stringify({idthirdemployee:this.billDTO.id_third,
        idthird:this.billDTO.id_third_employee,
        idbilltype:this.billDTO.id_bill_type,
        notes:this.billDTO.documentDTO.body,
        idthirddestinity:this.billDTO.id_third_destiny,
        idcaja:this.locStorage.getIdCaja(),
        idstore:this.locStorage.getIdStore(),
        idbankentity:1,
        idbillstate:this.SelectedBillState,
        details:detailList}))

        this.http2.post(Urlbase[3]+"/billing/v2",{idthirdemployee:this.billDTO.id_third,
        idthird:this.billDTO.id_third_employee,
        idbilltype:this.billDTO.id_bill_type,
        notes:this.billDTO.documentDTO.body,
        idthirddestinity:this.billDTO.id_third_destiny,
        idcaja:this.locStorage.getIdCaja(),
        idstore:this.locStorage.getIdStore(),
        idbankentity:1,
        idbillstate:this.SelectedBillState,
        details:detailList}).subscribe(idBill => {
            this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+idBill,{}).subscribe(resp => {
                
            this.http2.get(Urlbase[3]+"/billing/UniversalPDF?id_bill="+idBill,{responseType: 'text'}).subscribe(response =>{
              //-----------------------------------------------------------------------------------------------------------------
              window.open(Urlbase[7]+"/"+response, "_blank");
              this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
             this.http2.get(Urlbase[5]+"/close/alertForEmail?id_caja="+this.locStorage.getIdCaja()+"&id_third="+this.locStorage.getThird().id_third).subscribe(data => {
                  this.mostrandoCargando = false;
                  //@ts-ignore
                  console.log(data)
                  setTimeout(() => {
                    this.cancel();
                  
                    this.elementRef.nativeElement.focus();
                    this.elementRef.nativeElement.select();
                  
                  }, 100);
                })
              });
              
            });
         
        })

  
    }
  }

  transformDate(date){
    return this.datePipe.transform(date, 'yyyy/MM/dd');
  }

  transformDateWithHour(date){
    return this.datePipe.transform(date, 'yyyy/MM/dd, h:mm a');
  }

  beginPlusOrDiscount(inventoryQuantityDTOList,isDisc) {
    try{
      inventoryQuantityDTOList.forEach(element =>{
        console.log(element);
        this.inventoriesService.putQuantity(this.locStorage.getIdStore(),((-1)*element.quantity),element.code,inventoryQuantityDTOList,isDisc,element.id_storage).subscribe(result =>{
          if(this.SelectedBillState=="301"){
            this.http2.post("http://tienda724.com:8447/v1/pedidos/procedure?idbill=-4001&idbillstate="+this.SelectedBillState+"&code="+element.code+"&idstore="+this.locStorage.getIdStore()+"&cantidad="+element.quantity+"&tipo=E",{},{}).subscribe(item =>{
          });
          }
          if(this.SelectedBillState=="302"){
            this.http2.post("http://tienda724.com:8447/v1/pedidos/procedure?idbill=-4002&idbillstate="+this.SelectedBillState+"&code="+element.code+"&idstore="+this.locStorage.getIdStore()+"&cantidad="+element.quantity+"&tipo=E",{},{}).subscribe(item =>{
          });
          }
          if(this.SelectedBillState=="303"){
            this.http2.post("http://tienda724.com:8447/v1/pedidos/procedure?idbill=-4003&idbillstate="+this.SelectedBillState+"&code="+element.code+"&idstore="+this.locStorage.getIdStore()+"&cantidad="+element.quantity+"&tipo=E",{},{}).subscribe(item =>{
          });
          }
          if(this.SelectedBillState=="304"){
            this.http2.post("http://tienda724.com:8447/v1/pedidos/procedure?idbill=-4004&idbillstate="+this.SelectedBillState+"&code="+element.code+"&idstore="+this.locStorage.getIdStore()+"&cantidad="+element.quantity+"&tipo=E",{},{}).subscribe(item =>{
          });
          }
          if(this.SelectedBillState=="305"){
            this.http2.post("http://tienda724.com:8447/v1/pedidos/procedure?idbill=-4005&idbillstate="+this.SelectedBillState+"&code="+element.code+"&idstore="+this.locStorage.getIdStore()+"&cantidad="+element.quantity+"&tipo=E",{},{}).subscribe(item =>{
          });
        }
        });
      });
      this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
      this.cancel();
    }catch{
      this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'info')
    }
  }

  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: typeStr ? typeStr : type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }

  individualDelete(code){
  
    this.removalArray.add(code)
    this.removeItems()

  }

  openReportes(): void {
    const dialogRef = this.dialog.open(ReportesComponent, {
      width: '40vw',
      height: '60vh',
      data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
    });
  }

  upQuantity(code){
    this.productsObject[code].quantity+=1;
  }

  downQuantity(code){
    if(1<this.productsObject[code].quantity){
      this.productsObject[code].quantity-=1;
    }
  }
}

export class pdfData_inOutbill{
  detalles: String[][];
  obs: String;
  razon: String;
  fecha: String;
  nombreEmpresa: String;
  NIT: String;
  id_third: String;
  caso: String;
  constructor(
      detalles: String[][],
      obs: String,
      razon: String,
      fecha: String,
      nombreEmpresa: String,
      NIT: String,
      id_third: String,
      caso: String,
      ){
    this.detalles = detalles;
    this.obs = obs;
    this.razon = razon;
    this.fecha = fecha;
    this.nombreEmpresa = nombreEmpresa;
    this.NIT = NIT;
    this.id_third = id_third;
    this.caso = caso;
  }
}

export class pdfData{
  empresa: String;
  tienda: String;
  fecha: String;
  caja: String;
  consecutivo: String;
  detalles: [String[]];
  subtotal: number;
  tax: number;
  total: number;
  constructor(empresa: String,
    tienda: String,
    fecha: String,
    caja: String,
    consecutivo: String,
    detalles: [String[]],
    subtotal: number,
    tax: number,
    total: number){
      this.empresa = empresa;
      this.tienda = tienda;
      this.fecha = fecha;
      this.caja = caja;
      this.consecutivo = consecutivo;
      this.detalles = detalles;
      this.subtotal = subtotal;
      this.tax = tax;
      this.total = total;
    }
}

export class InventoryName{
  id_TAX: number;
  standard_PRICE: number;
  id_PRODUCT_STORE: number;
  product_STORE_NAME: string;
  code: string;
  id_INVENTORY_DETAIL: number;
  ownbarcode: string;
  product_STORE_CODE: number;
  constructor(ID_TAX: number,
    STANDARD_PRICE: number,
    ID_PRODUCT_STORE: number,
    PRODUCT_STORE_NAME: string,
    CODE: string,
    ID_INVENTORY_DETAIL: number,
    OWNBARCODE: string,
    PRODUCT_STORE_CODE: number){
      this.id_TAX = ID_TAX;
      this.standard_PRICE = STANDARD_PRICE;
      this.id_PRODUCT_STORE = ID_PRODUCT_STORE;
      this.product_STORE_NAME = PRODUCT_STORE_NAME;
      this.code = CODE;
      this.id_INVENTORY_DETAIL = ID_INVENTORY_DETAIL;
      this.ownbarcode = OWNBARCODE;
      this.product_STORE_CODE = PRODUCT_STORE_CODE;
    }
}
