import { Routes } from '@angular/router';



import {  DashboardComponent } from "../../../dashboard/dashboard.component";
import { StoreAdminComponent } from './store-admin/store-admin.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/
import { ThirdRouting }  from './thirds724/third/third.routing'
import { CategoriesRouting }  from './store724/categories/categories.routing'
import { BarCodesRouting }  from './store724/bar-codes/bar-codes.routing'
import { MeasureUnitRouting }  from './store724/measure-unit/measure-unit.routing'
import { ProductsRouting }  from './store724/products/products.routing'
import { ProductsThirdRouting }  from './store724/products-third/products-third.routing'
import { AttributesRouting }  from './store724/attributes/attributes.routing'
import { InventoryRouting }  from './store724/inventories/inventories.routing'
import {  Billing724Routing }  from './billing724/Billing724.routing'
import {  ReportsRouting }  from './store724/reports/reports.routing'





export const BusinessRouting: Routes = [


  { path: 'business', component: null,
    children: [
        { path: '', redirectTo: 'menu', pathMatch: 'full'},
        { path: 'menu', component: DashboardComponent,pathMatch: 'full'},
        { path: 'admin', component: StoreAdminComponent,pathMatch: 'full'},
        ...ThirdRouting,
        ...CategoriesRouting,
        ...BarCodesRouting,
        ...MeasureUnitRouting,
        ...ProductsRouting,
        ...ProductsThirdRouting,
        ...AttributesRouting,
        ...InventoryRouting,
        ...Billing724Routing,
        ...ReportsRouting
    ]
  },

]