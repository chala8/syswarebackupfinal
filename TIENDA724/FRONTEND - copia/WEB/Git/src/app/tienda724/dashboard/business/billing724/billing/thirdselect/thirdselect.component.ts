import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { LocalStorage } from '../../../../../../../app/shared/localStorage';

@Component({
  selector: 'app-thirdselect',
  templateUrl: './thirdselect.component.html',
  styleUrls: ['./thirdselect.component.scss']
})
export class ThirdselectComponent implements OnInit {

  constructor(private locStorage: LocalStorage,
    public dialogRef: MatDialogRef<ThirdselectComponent>,public dialog: MatDialog 
    ,@Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  listPerson;

  ngOnInit() {
    console.log("THIS IS THIRD LIST, ",this.data.thirdList)
    this.listPerson = this.data.thirdList;
  }

  returnData(elem){
    this.locStorage.setPersonClient(elem);
    this.dialogRef.close({
      didDo: true
    });
  }

  closeWin(){
    this.dialogRef.close({
      didDo:false
    });
  }

}

export interface DialogData {
  thirdList: any;
}