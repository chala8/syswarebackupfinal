import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { BillingService } from '../../billing.service';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from 'app/shared/localStorage';
import { Urlbase } from 'app/shared/urls'
@Component({
  selector: 'app-pedidos-detail',
  templateUrl: './pedidos-detail.component.html',
  styleUrls: ['./pedidos-detail.component.scss']
})
export class PedidosDetailComponent implements OnInit {
  elem: any;
  ListReportProd;

  CampoSorteando = "";
  Invertido = false;
  
  constructor(
      public locStorage: LocalStorage,
      private http2: HttpClient,
      public dialogRef: MatDialogRef<PedidosDetailComponent>,
      public dialog: MatDialog,
      @Inject(MAT_DIALOG_DATA) public data: DialogData,
  )
  { }


  SortearPorPropiedadMetodo(propiedad,invertido) {
    return function(n1, n2) {
      let Terminado = false;
      let i = 0;
      let TempN1 = n1[propiedad];
      let TempN2 = n2[propiedad];
      TempN1 = TempN1 + "";
      TempN2 = TempN2 + "";
      let Arreglo1 = [];
      let Arreglo2 = [];
      try {
        Arreglo1 = TempN1.trim().split(/[ ,.]+/)
        Arreglo2 = TempN2.trim().split(/[ ,.]+/)
      }
      catch(error) {
        console.error(error);
      }
      let retorno = -1;
      while(!Terminado){
        if(Arreglo1.length <= i){
          Terminado = true;
          retorno = -1;
          break;
        }
        if(Arreglo2.length <= i){
          Terminado = true;
          retorno = 1;
          break;
        }
        let Parte1 = Arreglo1[i]
        let Parte2 = Arreglo2[i]
        var A = parseInt(Parte1);
        var B = parseInt(Parte2);
        if(isNaN(A)){ A = Parte1;}
        if(isNaN(B)){ B = Parte2;}
        i++;
        if (A < B){
          retorno = -1;
          Terminado = true;
          break;
        }else if (A > B){
          retorno = 1;
          Terminado = true;
          break;
        }else{
          continue;
        }
      }
      return invertido ? -retorno:retorno;
    };
  }

  SortBy(campo){
    if(this.CampoSorteando != campo){
      this.CampoSorteando = campo;
    }else{
      this.Invertido = !this.Invertido;
    }
    //Genero copia del listado
    var ListaCopia = [...this.ListReportProd];
    //Lo sorteo
    var ListaCopiaSorteada: string[][] = ListaCopia.sort(this.SortearPorPropiedadMetodo(campo,this.Invertido));
    this.ListReportProd = ListaCopiaSorteada;
  }

  ngOnInit() {
    this.elem = this.data.elem;
    console.log(this.data.elem);
    console.log(JSON.stringify({listaTipos: [this.data.elem.id_BILL]}))
    this.http2.post("http://tienda724.com:8448/v1/pedidos/detalles",{listaTipos: [this.data.elem.id_BILL]},{}).subscribe(
      response => {  
        this.ListReportProd = response;
        console.log(response)
      }
    )
  }





  
genPdf(){
  console.log("THIS I NEED: ",{
    documento: this.elem.numdocumento,
    cliente: this.elem.cliente+"-"+this.elem.tienda,
    fecha: new Date(),
    documento_cliente: this.elem.numpedido,
    telefono: this.elem.telefono,
    correo: this.elem.mail,
    direccion: this.elem.address,
    total: 0 + 0,
    subtotal: 0,
    tax: 0,
    percent: this.elem.tax,
    detail_list: this.ListReportProd,
    used_list: [],
    observaciones: this.elem.body,
    logo: this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number
  })
  console.log(JSON.stringify({
    documento: this.elem.numdocumento,
    cliente: this.elem.cliente+"-"+this.elem.tienda,
    fecha: new Date(),
    documento_cliente: this.elem.numpedido,
    telefono: this.elem.telefono,
    correo: this.elem.mail,
    direccion: this.elem.address,
    total: 0 + 0,
    subtotal: 0,
    tax: 0,
    detail_list: this.ListReportProd,
    used_list: [],
    observaciones: this.elem.body  ,
    logo: this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number
  }));
  this.http2.post(Urlbase[3]+"/pedidos/pdf2",{
    documento: this.elem.numdocumento,
    cliente: this.elem.cliente+"-"+this.elem.tienda,
    fecha: new Date(),
    documento_cliente: this.elem.numpedido,
    telefono: this.elem.telefono,
    correo: this.elem.mail,
    direccion: this.elem.address,
    total: 0 + 0,
    subtotal: 0,
    tax: 0,
    detail_list: this.ListReportProd,
    used_list: [],
    observaciones: this.elem.body  ,
    logo: this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number      
  },{responseType: 'text'}).subscribe(responsepdf => {
    console.log("THIS IS MY RESPONSE, ",responsepdf);
    window.open(Urlbase[6]+"/"+responsepdf, "_blank");
  })
}


confirmarEnv() {
  this.http2.post("http://tienda724.com:8447/v1/store/confirmarPC?numpedido="+this.elem.numdocumento+"&idstore="+this.locStorage.getIdStore()+"&idthird="+this.locStorage.getIdThird(),{}).subscribe(
    response => {
    }
  )
}






}



export interface DialogData {
  elem: any
}
