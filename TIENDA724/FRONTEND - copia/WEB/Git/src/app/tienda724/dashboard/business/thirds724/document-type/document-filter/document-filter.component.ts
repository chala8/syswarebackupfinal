import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


/*
*    Material modules for component
*/

/*
*     others component
*/

/*
*     models for  your component
*/
import { CommonThird } from '../../../commons/CommonThird'
import { DocumentType } from '../models/document-type'

/*
*     services of  your component
*/
import { DocumentTypeService } from '../document-type.service';

/*
*     constant of  your component
*/

@Component({
  selector: 'app-document-filter',
  templateUrl: './document-filter.component.html',
  styleUrls: ['./document-filter.component.css']
})
export class DocumentFilterComponent implements OnInit {

  documentTypes: DocumentType[];
  form: FormGroup;

  // Usamos el decorador Output
  @Output() EmitDocumentType = new EventEmitter();
  public nombre: string;
  public document_type:number;
  public document_number:String;

  checked = false;
 indeterminate = false;
 align = 'start';
 disabled = true;

  constructor(    public docTypeService: DocumentTypeService,
    private fb: FormBuilder ) { }

  ngOnInit() {
    this.getDocumentType();
    this.nombre = "Pueblo de la Toscana";
    this.createControls();
   
  }

  isChecked(){
    this.disabled=!this.disabled;
  }


  // GET /documents-types
  getDocumentType(): void {

    this.docTypeService.getDocumentTypeList()
      .subscribe((data: DocumentType[]) => this.documentTypes = data,
      error => console.log(error),
      () => {
       
      });
  }

  filterThird(event) {
    console.log("ENTRANDO AL BUSCAR LUPA")
    this.EmitDocumentType.emit({ document_type: this.form.value['document_type'], document_number: this.form.value['document_number'] });
    console.log("EL EMIT --> ", this.EmitDocumentType)    
  }

  createControls() {
    this.form = this.fb.group({
      document_type: ['', Validators.compose([
        Validators.required
      ])],
      document_number: ['', Validators.compose([
        
      ])],
    });
  }

}
