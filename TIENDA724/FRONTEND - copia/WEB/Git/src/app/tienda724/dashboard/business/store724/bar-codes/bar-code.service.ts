import { Injectable } from '@angular/core';

import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';


/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';
import { Code } from './models/code'
import { CodeDTO } from './models/codeDTO'

@Injectable()
export class BarCodeService {

  api_uri = Urlbase[2] + '/codes';
  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');
    
        this.options = new RequestOptions({headers:this.headers});
  }

  public getCodeList = (state?: number,id_code?:number, id_third_cod?: number,code?:string,
    id_product?: number, img_url?:string, id_measure_unit?: number, 
    id_attribute_list?:number, id_state?:number, modify_attribute?:Date): Observable<{}|Code[]> => {

      let params: URLSearchParams = new URLSearchParams();
      params.set('id_code',  id_code?""+id_code:null);
      params.set('code',  code?""+code:null);
    
      params.set('img', img_url?""+img_url:null);
      params.set('id_product', id_product?""+id_product:null);
      params.set('id_measure_unit', id_measure_unit?""+id_measure_unit:null);
      params.set('id_attribute_list', id_attribute_list?""+id_attribute_list:null);
      params.set('id_state', id_state?""+id_state:null);
      params.set('state', state?""+state:null);
      params.set('id_third_cod', id_third_cod?""+id_third_cod:null);
      params.set('modify_code', modify_attribute?""+modify_attribute:null);
        
      let myOption: RequestOptions = this.options;
      myOption.search = params;
      return this.http.get(this.api_uri, this.options )
        .map((response: Response) => <Code[]>response.json())
        .catch(this.handleError);
    }

    public Delete = (id_category: number): Observable<Response|any> => {
      return this.http.delete(this.api_uri +'/'+ id_category,this.options)
      .map((response: Response) => {
        if(response){
          return true;
        }else{
  
      return false;
        }})
          .catch(this.handleError);
    }

    public postCategory = (body: CodeDTO): Observable<Boolean|any> => {
      
        return this.http.post(this.api_uri , body, { headers: this.headers })
          .map((response: Response) => {
                if(response){
                  return true;
                }else{
  
              return false;
            }
          })
          .catch(this.handleError);
    }


    public putCode = (id:number,body: CodeDTO): Observable<Boolean|any> => {
      
      return this.http.put(this.api_uri +"/"+ id, body, { headers: this.headers })
          .map((response: Response) => {
                if(response){
                  return true;
                }else{
  
              return false;
            }
          })
          .catch(this.handleError);
    }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return null;//Observable.throw(errMsg);
  }

}
