import { Component, OnInit, ViewChild, ElementRef, SystemJsNgModuleLoader } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router'
import { LocalStorage } from '../../../../../../shared/localStorage';
import { FormControl } from '@angular/forms';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { MatDialog } from '@angular/material';
/* import components */
import { ReportesComponent } from '../../billing/bill-main/reportes/reportes.component';
import { QuantityDialogComponent } from './quantity-dialog/quantity-dialog.component';
import { ThirdDialogComponent } from './third-dialog/third-dialog.component';
import { CloseBoxComponent } from './close-box/close-box.component';
import { CategoriesComponent } from './categories/categories.component';
import { EmployeeDialogComponent } from './employee-dialog/employee-dialog.component';
import { PersonDialogComponent } from './person-dialog/person-dialog.component';
import { SearchClientDialogComponent } from './search-client-dialog/search-client-dialog.component';
import { SearchProductDialogComponent } from './search-product-dialog/search-product-dialog.component';
import { TransactionConfirmDialogComponent } from './transaction-confirm-dialog/transaction-confirm-dialog.component'
import { InventoryDetail } from '../../../store724/inventories/models/inventoryDetail';
import { InventoryQuantityDTO } from '../../../store724/inventories/models/inventoryQuantityDTO';
import { CommonStateDTO } from '../../commons/commonStateDTO';
import { DocumentDTO } from '../../commons/documentDTO';
import { DetailBillDTO } from '../models/detailBillDTO';
import { BillDTO } from '../models/billDTO';
import { BillingService } from '../billing.service';
import { InventoriesService } from '../../../store724/inventories/inventories.service';
import { Token } from '../../../../../../shared/token';
import { PurchaseDetailDialogComponent } from './purchase-detail-dialog/purchase-detail-dialog.component';
import { Urlbase } from '../../../../../../shared/urls';
import { HttpClient } from '@angular/common/http';
import { ClientData } from '../models/clientData';
import { MatSelectModule } from '@angular/material/select';
import { NewPersonComponent } from '../../../thirds724/third/new-person/new-person.component';
import { s } from '@angular/core/src/render3';
import { forEach } from '@angular/router/src/utils/collection';
import { InventoryComponent } from './inventory/inventory.component';
import { StoresComponent } from './stores/stores.component';
import { Observable } from 'rxjs/Observable';
import { UpdateLegalDataComponent } from './update-legal-data/update-legal-data.component';
import { NewProductStoreComponent } from './new-product-store/new-product-store.component';
import { ThirdService } from '../../../thirds724/third/third.service';
import { OpenBoxComponent } from 'app/components/open-box/open-box.component';
import { BillComponent } from '../bill/bill.component'
import { InBillComponent } from '../../billing/in-bill/in-bill.component'
import { OutBillComponent } from '../../billing/out-bill/out-bill.component'
import { RefundBillComponent } from '../../billing/refund-bill/refund-bill.component'
import { BillInventoryComponentComponent } from '../bill-main/bill-inventory-component/bill-inventory-component.component';
import { OpenorcloseboxComponent } from 'app/components/openorclosebox/openorclosebox.component';
import { SelectboxComponent } from 'app/components/selectbox/selectbox.component';
import { StoreSelectorService } from "../../../../../../components/store-selector.service";
import { TopProductsComponent } from './top-products/top-products.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { paymentDetailDTO } from "./../models/paymentDetailDTO";
import { stateDTO } from "./../models/stateDTO";

declare var $: any;
@Component({
  selector: 'app-bill-main',
  templateUrl: './bill-main.component.html',
  styleUrls: ['./bill-main.component.scss']
})
export class BillMainComponent implements OnInit {
  api_uri = Urlbase[2];

  id_store = this.locStorage.getIdStore();
  CUSTOMER_ID;
  // venta =1
  // venta con remision 107

  //list
  itemLoadBilling: InventoryDetail;
  // inventoryList: InventoryDetail[];
  inventoryList: InventoryName[];
  codeList: any;
  categoryList = [] as any[];
  notCategoryList: any;
  productList: any;
  categoryIDboolean = false;
  categoryID: number;
  usableProducts = [] as any[];
  productsByCategoryList = [] as any [];
  codesByProductList = [] as any []; 
  codesByCategoryList = [] as any [];
  tab: number = 0;
  selected = new FormControl(0);
  CategoryName: string = "";
  taxesList: any;
  state: any;
  paymentDetailFinal: paymentDetailDTO = new paymentDetailDTO(1,1,0," ",new stateDTO(1));
  paymentDetail: string = '[{';
  paymentDetailObservable = this.httpClient;
  tipoFactura: string = "venta";
  isTotalDev: boolean = false;
  idBillOriginal: String = "";
  storageList;
  priceList = [];
  pdfDatas: pdfData;


  selectedDomi = "-1";
  domiName = "";


  openBox = false;

  storageActual: string = "1";


  FechaDevolucion: String="--/--/--";
  NotasDevolucion: String="--Sin notas--";
  TotalDevolucion: String="--No se Cargo el Total--";
  IdBillDevolucion: String="";
  IdDocumentDevolucion: String="";
  IdBillState;
  botonDevolucionActive: Boolean = true;

  // DTO's
  inventoryQuantityDTO: InventoryQuantityDTO;
  inventoryQuantityDTOList: InventoryQuantityDTO[];
  detailBillingDTOList: any[];
  commonStateDTO: CommonStateDTO;
  documentDTO: DocumentDTO;
  detailBillDTO: DetailBillDTO;
  detailBillDTOList: DetailBillDTO[];
  billDTO: BillDTO;
  storeName=" ";
  public flag = "0";
  public productsObject = [];
  public setObject = [];
  public removalArray = new Set();

  public flagVenta = true;
  public vendedor = {};
  public animal;
  public name;
  public form;
  public token:Token;
  public clientData: ClientData;

  public searchData;
  public searchInput : string ="";
  public ObservationsInOrOut: string ="";

  private headers = new Headers();
  private options: RequestOptions;

  public pickedPriceList: any;

  public doIAddItems = false;
  public idBillToAdd = 0;
  public topProds = [];

  public busqueda="";

  listaElem = [];

  quickconfirm = false;

  checker = true;


  constructor(private service: StoreSelectorService, public thirdService: ThirdService,private http2: HttpClient,private http: Http, public locStorage: LocalStorage,private fb: FormBuilder, private billingService: BillingService,
              public inventoriesService: InventoriesService, public dialog: MatDialog, private httpClient: HttpClient) {

    this.clientData = new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ', null);
    this.detailBillingDTOList = [];
    this.inventoryList = [];
    this.inventoryQuantityDTOList = [];
    this.detailBillDTOList = [];

    this.commonStateDTO = new CommonStateDTO(1, null, null);
    this.documentDTO = new DocumentDTO(null, null);
    this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null,null);
    this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO, null);
    this.billDTO = new BillDTO(this.locStorage.getIdStore(),null, null, null, null, null, null, null, null, null, null, null, null, this.commonStateDTO, null, this.detailBillDTOList, this.documentDTO);
    this.createControls();
    this.logChange();
    this.service.onDataSendEvent.subscribe(log => {
      console.log("THIS CODE DID RUN, ", log)
    });
  }
  currentBox;
  idThird;
  myBox;
  SelectedStore;
  Stores;
  domiList;
  @ViewChild('nameit') private elementRef: ElementRef;

  getDomiName(){
    var elem = this.domiList.find(item => item.id_THIRD_DOMICILIARIO == Number(this.selectedDomi));

    if(elem){
      this.domiName = elem.domiciliario;
    }else{
      this.domiName = "Sin Domicilio"
    }
  }

  getStores2() {
    this.billingService.getStoresByThird(this.locStorage.getToken().id_third).subscribe(data => {
      console.log("DATO: ", this.Stores);
      this.Stores = data;
      this.SelectedStore = this.locStorage.getIdStore();
      this.service.onMainEvent.emit(this.locStorage.getIdStore());
    }      )
    
  }

  listElem = "";

  listLoad(){

    this.listaElem = [];
    console.log("THIS IS LISTA, ", this.listaElem);
    this.getElemsIfOwn().then(e => {
      console.log("1");
      this.getElemsIfcode().then(a => {
        console.log("2");
        //inserto elementos si name
        this.inventoryList.forEach(element => {
          if(element.product_STORE_NAME.toLowerCase( ).includes(this.listElem.toLowerCase())){
            this.listaElem.push(element);
          }
        })
      })
    })
  }

  async getElemsIfOwn(){
    this.inventoryList.forEach(element => {
      if(element.ownbarcode==this.listElem){
        console.log("PUDE ENTRAR 1");
        this.listaElem.push(element);
      }
    })
  }

  async getElemsIfcode(){
    this.inventoryList.forEach(element => {
      if(element.product_STORE_CODE==this.listElem){
        console.log("PUDE ENTRAR 2");
        this.listaElem.push(element);
      }
    })
  }

  getStoreType(id_store){
    //@ts-ignore 
    this.http2.get(Urlbase[2]+"/store/tipoStore?id_store="+id_store).subscribe( response => {
      this.locStorage.setTipo(response);
      console.log("ID CAJA: ",this.locStorage.getIdCaja());
      console.log("ID STORE: ",this.locStorage.getIdStore());
      console.log("STORE TYPE: ",this.locStorage.getTipo());
      console.log("BOX TYPE: ",this.locStorage.getBoxStatus());
      console.log("AM I?");
      this.getStores();
    })
  }


  getStores() {
    this.billingService.getStoresByThird(this.locStorage.getToken().id_third).subscribe(data => {
      this.Stores = data;
      console.log("DATO: ", this.Stores);
      data.forEach(element => {
        if(element.id_STORE==this.locStorage.getIdStore()){
          this.storeName = element.store_NAME;
        }
      });
    })
  }


  getTopProds(idstore){
    this.topProds = [];
    this.http2.get(Urlbase[3]+"/billing/top20?idstore="+idstore).subscribe(res=> {
      for(let i=0;i<20;i++){
        this.topProds.push(res[i]);
      }

      console.log("THIS IS LIST, ",this.topProds)
    })
  }


  reload(){
    this.ngOnInit();
    this.getTopProds(this.locStorage.getIdStore())
  }


  ngOnInit() {
    
    console.log("ON VENTA");
    if (this.billingService.subsVar==undefined) {
      this.billingService.subsVar = this.billingService.
      invokeFirstComponentFunction.subscribe((name:string) => {
        this.reload();
      });
    }

    if(!this.locStorage.getBoxStatus()){

      var idPerson = this.locStorage.getPerson().id_person;

      this.thirdService.getThirdList().subscribe(res=>{
        // @ts-ignore
        let employee = res.filter(item=> item.profile.id_person === this.locStorage.getPerson().id_person);
        this.idThird = employee[0].id_third;
        localStorage.setItem("id_employee",String(this.idThird));
        if(this.locStorage.getRol()[0].id_rol==8888 || this.locStorage.getRol()[0].id_rol==21){
          this.billingService.getCajaByIdStatus2(String(this.idThird)).subscribe(answering=>{
            this.myBox = answering;
            localStorage.setItem("myBox",answering);
            this.currentBox = localStorage.getItem("currentBox");
            console.log("this is res",answering);


            if(answering.length > 0 && this.locStorage.getDoINav()==false){
              // if(res.length === 1 ){
              //   this.unicaCaja = true;
              // }else{
              //   this.unicaCaja = false;
              // }
              var dialogRef;
              dialogRef = this.dialog.open(OpenorcloseboxComponent, {
                width: '60vw',
                data: {},
                disableClose: true
              }).afterClosed().subscribe(response=> {
                console.log(response);
                if(response){
                  this.locStorage.setBoxStatus(true);
                  this.locStorage.setIdCaja(Number(answering[0]));

                  this.http2.get(Urlbase[5]+"/close/myboxes?id_person="+this.locStorage.getPerson().id_person).subscribe(resp => {
                    console.log(resp);
                    //@ts-ignore
                    resp.forEach(element => {
                      if(element.id_CAJA==Number(answering[0])){
                        console.log("ENTRE");
                        this.locStorage.setIdStore(element.id_STORE);
                        this.getStoreType(element.id_STORE);
                        this.getTopProds(element.id_STORE);
                      }
                    });
                    this.SelectedStore=this.locStorage.getIdStore();
                    this.service.onMainEvent.emit(Number(answering[0]));
                    this.getStores2();
                  });
                  console.log("IM ON THE TRUE SIDE OF THINGS")
                }else{
                  this.locStorage.setIdCaja(Number(answering[0]));
                  console.log("IM ON THE FALSE SIDE OF THINGS");
                  var dialogRef;
                  dialogRef = this.dialog.open(CloseBoxComponent, {
                    width: '60vw',
                    data: {flag:false},
                    disableClose: true
                  }).afterClosed().subscribe(response=> {
                    dialogRef = this.dialog.open(SelectboxComponent, {
                      width: '60vw',
                      data: {},
                      disableClose: true
                    }).afterClosed().subscribe(response2=> {
                      this.locStorage.setIdCaja(response2.idcaja);
                      this.http2.get(Urlbase[5]+"/close/myboxes?id_person="+this.locStorage.getPerson().id_person).subscribe(resp => {
                        //@ts-ignore
                        resp.forEach(element => {
                          if(element.id_CAJA==this.locStorage.getIdCaja()){
                            this.locStorage.setIdStore(element.id_STORE);
                            this.getStoreType(element.id_STORE);
                            this.getTopProds(element.id_STORE);
                          }
                        });
                      });
                      dialogRef = this.dialog.open(OpenBoxComponent, {
                        width: '60vw',
                        data: {},
                        disableClose: true
                      }).afterClosed().subscribe(response=> {
                        this.locStorage.setBoxStatus(true);
                        this.SelectedStore==this.locStorage.getIdStore();
                        this.getTopProds(this.locStorage.getIdStore());
                        this.getStores2();
                        console.log("ID CAJA: ",this.locStorage.getIdCaja());
                        console.log("ID STORE: ",this.locStorage.getIdStore());
                        console.log("STORE TYPE: ",this.locStorage.getTipo());
                        console.log("BOX TYPE: ",this.locStorage.getBoxStatus());
                        console.log("OR ITS ME?");
                      });

                    });

                  });
                }
              });

            }else{
              if( this.locStorage.getDoINav()==false){

                console.log("IM ON THE NO OPEN BOX SIDE OF THINGS");
                var dialogRef;
                dialogRef = this.dialog.open(SelectboxComponent, {
                  width: '60vw',
                  data: {},
                  disableClose: true
                }).afterClosed().subscribe(response2=> {
                  if(response2.open){
                    this.locStorage.setIdCaja(response2.idcaja);
                    console.log("-------------------------------------------------");
                    console.log("THIS IS 1-quickconfirm: ", this.quickconfirm);
                    console.log("THIS IS 2-isfull: ", this.isfull());
                    console.log("THIS IS 3-getBoxStatus: ", !this.locStorage.getBoxStatus());
                    console.log("-------------------------------------------------");
                    this.locStorage.setBoxStatus(false);
                    this.http2.get(Urlbase[5]+"/close/myboxes?id_person="+this.locStorage.getPerson().id_person).subscribe(asd => {
                      //@ts-ignore
                      asd.forEach(element => {
                        if(element.id_CAJA==this.locStorage.getIdCaja()){
                          this.locStorage.setIdStore(element.id_STORE);
                          this.getStoreType(element.id_STORE);
                          this.getTopProds(element.id_STORE);
                        }
                      });
                    });
                    dialogRef = this.dialog.open(OpenBoxComponent, {
                      width: '60vw',
                      data: {},
                      disableClose: true
                    }).afterClosed().subscribe(response=> {
                      this.service.onMainEvent.emit(this.locStorage.getIdStore());
                      this.locStorage.setBoxStatus(true);
                      console.log("ID CAJA: ",this.locStorage.getIdCaja());
                      console.log("ID STORE: ",this.locStorage.getIdStore());
                      console.log("STORE TYPE: ",this.locStorage.getTipo());
                      console.log("BOX TYPE: ",this.locStorage.getBoxStatus());
                      console.log("OR AM I?");

                      this.SelectedStore==this.locStorage.getIdStore();
                      this.getTopProds(this.locStorage.getIdStore());
                      this.getStores2();
                    });
                  }else{
                    this.locStorage.setIdCaja(response2.idcaja);
                    this.getStores3();
                    this.http2.get(Urlbase[5]+"/close/myboxes?id_person="+this.locStorage.getPerson().id_person).subscribe(resp => {
                      //@ts-ignore
                      resp.forEach(element => {
                        if(element.id_CAJA==this.locStorage.getIdCaja()){
                          this.locStorage.setIdStore(element.id_STORE);
                          this.getTopProds(element.id_STORE);
                          this.getStoreType(element.id_STORE);
                          this.getStores2();
                        }
                      });
                    })
                  }
                });

              }
            }
            // if(res.length > 0){
            //   // if(res.length === 1 ){
            //   //   this.unicaCaja = true;
            //   // }else{
            //   //   this.unicaCaja = false;
            //   // }
            // }else{
            //     var dialogRef;
            //       dialogRef = this.dialog.open(OpenBoxComponent, {
            //         width: '60vw',
            //         data: {},
            //         disableClose: true
            //       }
            // }); 
            // }
          })}
      });






    }else{this.getStoreType(this.locStorage.getIdStore());}

    // this.http2.get(Urlbase[2]+"/store/cajaitems?id_person="+this.locStorage.getIdPersonApp()).subscribe(response => {
    // //@ts-ignore  
    // this.locStorage.setIdCaja(response.id_CAJA);
    // //@ts-ignore 
    // this.locStorage.setIdCaja(response.id_store);
    // })
    // var input = document.getElementById('reader');
    // input.focus();

    this.openBox = this.locStorage.getBoxStatus();
    console.log("ID CAJAAAAA: ", this.locStorage.getIdCaja());
    console.log("ID STOREEEEE: ", this.locStorage.getIdStore());
    console.log("Box STATUS: ", this.locStorage.getBoxStatus());

    //-----machete para telefono y direccion
    if(this.locStorage.getThird().id_third == 542){
      this.locStorage.setDireccion("Cali, Cra 42C # 52-05");
      this.locStorage.setTelefono("2345678 / 3222345674");
    }

    if(this.locStorage.getThird().id_third == 601){
      this.locStorage.setDireccion("Cali, Calle 3 Oeste # 1-80");
      this.locStorage.setTelefono("3209807254");
    }
    //-----machete para telefono y direccion

    setTimeout(() => {

      this.elementRef.nativeElement.focus();
      this.elementRef.nativeElement.select();

    }, 100);


    this.thirdService.getThirdList().subscribe(res=>{
      // @ts-ignore
      let employee = res.filter(item=> item.profile.id_person === this.locStorage.getPerson().id_person);
      this.idThird = employee[0].id_third;
      console.log(this.idThird,"I NEEDED THIS THIRD ID :V");
      console.log(this.locStorage.getToken().id_third);
      localStorage.setItem("id_employee",String(this.idThird));
    });
    this.state = {
      state:1
    };
    this.loadData();
    this.token = this.locStorage.getToken();
    this.CUSTOMER_ID= this.token.id_third_father;
    this.vendedor['fullname'] = this.locStorage.getPerson().info.fullname;
    this.vendedor['cargo'] = this.locStorage.getRol().map((item) => {
      return item.rol;
    }).join(" / ");
    // this.getInventoryList(this.STATE, null, null, this.ID_INVENTORY_TEMP);
    this.getInventoryList(this.locStorage.getIdStore());
    this.getCodes();
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());
    let token = localStorage.getItem('currentUser');
    this.options = new RequestOptions({ headers: this.headers });
    this.getStorages();
  }

  //   getPriceList(id){
  //   this.inventoryList.forEach(item => {
  //     this.http2.get(Urlbase[2]+"/store/pricelist?id_product_store="+id).subscribe((data) => {
  //       this.priceList[item.id_PRODUCT_STORE] = data;
  //      })
  //   });
  // }

  setCode(code){

    this.addDetail2(code);

  }

  roundnum(num){
    return Math.round(num / 50)*50;
  }

  getStores3() {
    this.billingService.getStoresByThird(this.locStorage.getToken().id_third).subscribe(data => {
      console.log(data);
      this.Stores = data;
      this.SelectedStore = data[0].id_STORE;
      this.locStorage.setIdStore(data[0].id_STORE)
    })
  }

  getPriceList3(product,code,id,quantity){
    console.log("id is: ",id);
    this.http2.get(Urlbase[2]+"/store/pricelist?id_product_store="+id).subscribe(response => {
      console.log("This is picked price list: ",response);
      var datos = response;
      console.log("this is datos: ",datos);
      if (product) {
        let new_product = {
          quantity: quantity,
          id_storage: this.storageList[0].id_storage,
          price: datos[0].price,
          tax: this.getPercentTax(product.id_TAX),
          id_product_third: product.id_PRODUCT_STORE,
          tax_product: product.id_TAX,
          state: this.commonStateDTO,
          description: product.product_STORE_NAME,
          code: product.code,
          id_inventory_detail: product.id_INVENTORY_DETAIL,
          ownbarcode: product.ownbarcode,
          product_store_code: String(product.product_STORE_CODE),
          pricelist: response
        };
        new_product.price = response[0].price;
        //this.detailBillDTOList.push(new_product);
        this.productsObject.unshift(new_product);
        this.setObject = Object.keys(this.productsObject);

        // event.target.blur();
        // event.target.value = '';
        // setTimeout(() => {
        //   document.getElementById('lastDetailQuantity');
        // });
      } else {
        var codeList;
        this.http2.get(Urlbase[2]+"/products2/code/general?code="+String(code)).subscribe(data => {
          codeList = data;
          console.log("this is codeList: ", codeList);
          //@ts-ignore
          if( data.length == 0 ){
            alert('Product not exist');
          }else{
            var dialogRef;
            dialogRef = this.dialog.open(NewProductStoreComponent, {
              width: '30vw',
              data: {codeList: codeList[0]}
            });
            dialogRef.afterClosed().subscribe(res=>{
              this.ngOnInit()
            })
            // let body = {
            //   id_store: this.locStorage.getIdStore(),
            //   id_code: codeList[0],
            //   product_store_name: "new",
            //   product_store_code: 123456,
            //   ownbarcode: "av12312312"
            // }
            // console.log(JSON.stringify(body));
            // this.http.post(Urlbase[2]+"/store/ps",body,this.options).subscribe(data=>
            // {
            //   console.log("Post hecho");});
          }
        });

      }

    });
  }

  getPriceList(product,code,id){
    console.log("id is: ",id);
    this.http2.get(Urlbase[2]+"/store/pricelist?id_product_store="+id).subscribe(response => {
      console.log("This is picked price list: ",response);
      var datos = response;
      console.log("this is datos: ",datos);
      if (product) {
        let new_product = {
          quantity: 1,
          id_storage: this.storageList[0].id_storage,
          price: datos[0].price,
          tax: this.getPercentTax(product.id_TAX),
          id_product_third: product.id_PRODUCT_STORE,
          tax_product: product.id_TAX,
          state: this.commonStateDTO,
          description: product.product_STORE_NAME,
          code: product.code,
          id_inventory_detail: product.id_INVENTORY_DETAIL,
          ownbarcode: product.ownbarcode,
          product_store_code: String(product.product_STORE_CODE),
          pricelist: response
        };
        new_product.price = response[0].price;
        //this.detailBillDTOList.push(new_product);
        this.productsObject.unshift(new_product);
        this.setObject = Object.keys(this.productsObject);

        // event.target.blur();
        // event.target.value = '';
        // setTimeout(() => {
        //   document.getElementById('lastDetailQuantity');
        // });
      } else {
        var codeList;
        this.http2.get(Urlbase[2]+"/products2/code/general?code="+String(code)).subscribe(data => {
          codeList = data;
          console.log("this is codeList: ", codeList);
          //@ts-ignore
          if( data.length == 0 ){
            alert('Product not exist');
          }else{
            var dialogRef;
            dialogRef = this.dialog.open(NewProductStoreComponent, {
              width: '30vw',
              data: {codeList: codeList[0]}
            });
            dialogRef.afterClosed().subscribe(res=>{
              this.ngOnInit()
            })
            // let body = {
            //   id_store: this.locStorage.getIdStore(),
            //   id_code: codeList[0],
            //   product_store_name: "new",
            //   product_store_code: 123456,
            //   ownbarcode: "av12312312"
            // }
            // console.log(JSON.stringify(body));
            // this.http.post(Urlbase[2]+"/store/ps",body,this.options).subscribe(data=>
            // {
            //   console.log("Post hecho");});
          }
        });

      }

    });
  }

  getStorages() {
    this.http2.get(Urlbase[2]+"/store/s?id_store="+this.id_store).subscribe((res)=>{
      this.storageList = res;
    });

  }

  getBillData(event){
    this.httpClient.get(Urlbase[3]+"/billing-state/billData?consecutive="+event.target.value).subscribe((res)=>{
      var refund = res;
      try{
        this.FechaDevolucion = refund[0].purchase_DATE;
        this.NotasDevolucion = refund[0].body;
        this.TotalDevolucion = refund[0].totalprice;
        this.IdBillDevolucion = refund[0].id_BILL;
        this.IdDocumentDevolucion = refund[0].id_DOCUMENT;
        this.IdBillState = refund[0].id_BILL_STATE;
        this.botonDevolucionActive = false;
        this.showNotification('top', 'center', 3, "<h3>LA FACTURA FUE CARGADA CORRECTAMENTE</h3> ", 'info');
      }catch(e){
        this.showNotification('top', 'center', 3, "<h3>NO SE ENCONTRO EL CONSECUTIVO DE FACTURA SOLICITADO</h3> ", 'danger');
      }
    });

  }

  anularFacturaPorDevolucion(){
    if(this.IdBillState != 41){
      var state= 41;
      this.http.put(Urlbase[3]+"/billing-state/billState/"+this.IdBillDevolucion,null,{params: {'state': state}}).subscribe((res)=>{
        var notas = this.NotasDevolucion + " / Factura Anulada Por Motivo de Devolucion";
        this.http.put(Urlbase[3]+"/billing-state/documentBody/"+this.IdDocumentDevolucion,null,{params: {'body': notas}}).subscribe((resp)=>{
          this.showNotification('top', 'center', 3, "<h3>LA FACTURA FUE ANULADA CORRECTAMENTE</h3> ", 'success');
        });
      });
    }else{
      this.showNotification('top', 'center', 3, "<h3>LA FACTURA YA SE ENCUENTRABA ANULADA POR DEVOLUCION</h3> ", 'danger');
    }

    this.FechaDevolucion="--/--/--";
    this.NotasDevolucion="--Sin notas--";
    this.TotalDevolucion="--No se Cargo el Total--";
    this.IdBillDevolucion="";
    this.IdDocumentDevolucion="";
    this.IdBillState = 0;
    this.botonDevolucionActive = true;

  }


  resetCategoryMenu(){
    this.categoryIDboolean = false;
    this.categoryID = null;
    this.productsByCategoryList = [] as any[];
    this.codesByProductList = [] as any[];
    this.codesByCategoryList = [] as any[];
    this.CategoryName = "";
  }

  getTaxes(){
    this.httpClient.get(this.api_uri+'/tax-tariff').subscribe((res)=>{
      this.taxesList = res;
      this.getCategoryList();
    });

  }

  getCodes(){
    this.httpClient.get(this.api_uri+'/codes').subscribe((res)=>{
      this.codeList = res;
      this.getCategories();
    });
  }

  openCierreCaja(){

    var dialogRef;
    dialogRef = this.dialog.open(CloseBoxComponent, {
      width: '60vw',
      data: {flag:true}
    });

  }




  openTop(){
    console.log("THIS IS ROLES: ", this.locStorage.getTipo());

    var modal;
    modal = this.dialog.open(TopProductsComponent, {
      width: '1040px',
      height: '680px',
      data: {}
    });

    modal.afterClosed().subscribe(response => {
      console.log("ESTE ES EL CODIGO DE BARRAS QUE QUIERO EJECUTAR: ",this.locStorage.getCodigoBarras());
      if(this.locStorage.getCodigoBarras() != "-1"){
        this.addDetail2(this.locStorage.getCodigoBarras());
        this.locStorage.setCodigoBarras("-1");
      }
    });
  }




  openCategories(){
    console.log("THIS IS ROLES: ", this.locStorage.getTipo());

    var modal;
    modal = this.dialog.open(BillInventoryComponentComponent, {
      width: '675px',
      height: '450px',
      data: {}
    });

    modal.afterClosed().subscribe(response => {
      console.log("ESTE ES EL CODIGO DE BARRAS QUE QUIERO EJECUTAR: ",this.locStorage.getCodigoBarras());
      if(this.locStorage.getCodigoBarras() != "-1"){
        this.addDetail2(this.locStorage.getCodigoBarras());
        this.locStorage.setCodigoBarras("-1");
      }
    });
  }

  openInventories(){

    var dialogRef;
    dialogRef = this.dialog.open(InventoryComponent, {
      width: '60vw',
      data: {}
    });


  }

  openStores(){

    var dialogRef;
    dialogRef = this.dialog.open(StoresComponent, {
      width: '60vw',
      data: {}
    });

  }
  openUpdateLegalData(){
    var dialogRef;
    dialogRef = this.dialog.open(UpdateLegalDataComponent, {
      width: '60vw',
      data: {}
    });
  }

  getProducts(){
    this.httpClient.get(this.api_uri+'/products').subscribe((res)=>{
      this.productList = res;
      this.getTaxes();
    });
  }

  getCategories(){
    this.httpClient.get(this.api_uri+'/categories').subscribe((res)=>{
      this.notCategoryList = res;
      this.getProducts();
    });
  }

  getPercentTax(idTaxProd){

    var thisTax;
    var taxToUse;

    for (thisTax in this.taxesList){
      if(idTaxProd == this.taxesList[thisTax].id_tax_tariff){
        taxToUse = this.taxesList[thisTax].percent/100;
      }
    }

    return taxToUse;
  }


  getCodesByCategory(){
    var product: any;
    for (product in this.usableProducts){
      var compProduct = this.usableProducts[product];
      if(compProduct.id_category == this.categoryID){
        if(!this.productsByCategoryList.includes(compProduct)){
          this.productsByCategoryList.push(compProduct);
        }
      }

    }
    var code: any;

    for (code in this.codeList){
      var compCode = this.codeList[code];
      var product:any;
      for (product in this.productList){
        var compProduct = this.productList[product];
        if(compCode.id_product == compProduct.id_product){
          compCode.img_url = compProduct.img_url;
          compCode.id_category = compProduct.id_category;
          compCode.description_product = compProduct.description_product;
          compCode.name_product = compProduct.name_product;
          var elem = this.inventoryList.find(item => item.code == compCode.code);
          compCode.price = elem.standard_PRICE;
          if(!this.codesByProductList.includes(compCode)){
            this.codesByProductList.push(compCode);
          }
        }
      }
    }


    var codeKey: any;

    for (codeKey in this.codeList){
      var compCode = this.codesByProductList[codeKey];
      if(compCode.id_category == this.categoryID){
        if(!this.codesByCategoryList.includes(compCode)){
          this.codesByCategoryList.push(compCode);
        }
      }
    }
    return this.codesByCategoryList;
  }


  getCategoryList(){
    this.codeList.forEach(code => {

      var aux = this.productList.filter(
          product => {
            return code.id_product == product.id_product
          }
      )[0];

      if(!this.usableProducts.includes(aux)){
        this.usableProducts.push(aux)
      }

    });


    this.usableProducts.forEach(prod => {

      var aux = this.notCategoryList.filter(
          cat => {
            return prod.id_category == cat.id_category;
          }
      )[0];

      if(!this.categoryList.includes(aux)){
        this.categoryList.push(aux)
      }

    });


    // this.codeList.forEach(code => {
    //   this.usableProducts.push( this.productList.filter(
    //     product => {
    //       return code.id_product == product.id_product             
    //     }
    //   )[0]);
    // });


    // console.log(this.usableProducts)
    // console.log(this.categoryList)





    // for (code in this.codeList){
    //   var compCode = this.codeList[code]
    //   var product:any;
    //   for (product in this.productList){
    //     var compProduct = this.productList[product]
    //     if(compCode.id_product == compProduct.id_product){

    //       if(!this.usableProducts.includes(compProduct)){
    //         this.usableProducts.push(compProduct);
    //       }
    //     }
    //   }
    // }

    var useProduct: any;
    var category: any;


    // for(useProduct in this.usableProducts){
    //   var compUseProduct = this.usableProducts[useProduct]
    //   for (category in this.notCategoryList){
    //     var compCategory = this.notCategoryList[category];
    //     if(compUseProduct.id_category == compCategory.id_category){
    //       if(!this.categoryList.includes(compCategory)){
    //         this.categoryList.push(compCategory);
    //       }
    //     }
    //   }     
    // } 



  }


  setCategoryID(id,name){
    this.categoryID = id;
    this.categoryIDboolean = true;
    this.CategoryName = name;
  }




  // addDetail(event) {
  //   var code = String(event.target.value);
  //   // console.log(code,"da code :3")

  //   // var codeFilter = this.productsObject.filter(item => (String(item.code) === code || String(item.ownbarcode) === code || String(item.product_store_code) === code ));

  //   // console.log(codeFilter);
  //   var key: any = null;
  //   this.productsObject.forEach(item => {
  //     // console.log("//-----------------------------------");
  //     // console.log(item.code);
  //     // console.log("//-----------------------------------");
  //     // console.log(item.ownbarcode);
  //     // console.log("//-----------------------------------");
  //     // console.log(item.product_store_code);
  //     // console.log("//-----------------------------------");
  //     if(item.code == code || item.ownbarcode == code || String(item.product_store_code) == code ){
  //       key = item;
  //     }
  //     // else{
  //     // }
  //   });
  //     // console.log("this is array papu: ",this.productsObject)
  //     // console.log("this is key", key)
  //   if (key != null) {
  //     // console.log("entro")
  //     this.productsObject[this.productsObject.indexOf(key)].quantity += 1;
  //     event.target.value = '';
  //   } else {
  //     var product = this.inventoryList.find(item => (item.code == code || item.ownbarcode == code || String(item.product_STORE_CODE) == code));
  //     console.log("this is price list: ",this.priceList)
  //     if (product) {
  //         let new_product = {
  //           quantity: 1,
  //           id_storage: -1,
  //           price: this.priceList[product.id_PRODUCT_STORE][0].price,
  //           tax: this.getPercentTax(product.id_TAX),
  //           id_product_third: product.id_PRODUCT_STORE,
  //           tax_product: product.id_TAX,
  //           state: this.commonStateDTO,
  //           description: product.product_STORE_NAME,
  //           code: product.code,
  //           id_inventory_detail: product.id_INVENTORY_DETAIL,
  //           ownbarcode: product.ownbarcode,
  //           product_store_code: String(product.product_STORE_CODE),
  //           pricelist: this.priceList[product.id_PRODUCT_STORE]
  //         };
  //         //this.detailBillDTOList.push(new_product);
  //         this.productsObject.push(new_product);
  //         this.setObject = Object.keys(this.productsObject);

  //         event.target.blur();
  //         event.target.value = '';
  //         setTimeout(() => {
  //           document.getElementById('lastDetailQuantity');
  //         });
  //       } else {
  //         alert('Product not exist');
  //       }      
  //   }
  // }

  addDetail3(code,quantity) {
    // console.log(code,"da code :3")

    // var codeFilter = this.productsObject.filter(item => (String(item.code) === code || String(item.ownbarcode) === code || String(item.product_store_code) === code ));

    // console.log(codeFilter);
    var key: any = null;
    this.productsObject.forEach(item => {
      // console.log("//-----------------------------------");
      // console.log(item.code);
      // console.log("//-----------------------------------");
      // console.log(item.ownbarcode);
      // console.log("//-----------------------------------");
      // console.log(item.product_store_code);
      // console.log("//-----------------------------------");
      if(item.code == code || item.ownbarcode == code || String(item.product_store_code) == code ){
        key = item;
      }
      // else{
      // }
    });
    // console.log("this is array papu: ",this.productsObject)
    // console.log("this is key", key)
    if (key != null) {
      // console.log("entro")
      this.productsObject[this.productsObject.indexOf(key)].quantity += 1;
    } else {

      // var product = this.inventoryList.find(item => (item.ownbarcode == code || item.code == code || String(item.product_STORE_CODE) == code));
      var product = this.inventoryList.find(item => this.findCode(code,item));
      //@ts-ignore
      if(product){
        this.getPriceList3(product,code,product.id_PRODUCT_STORE,quantity)
      }else{
        alert('Ese codigo no esta asociado a un producto.');
      }
    }
  }


  findCode(code,item){

    if(item.ownbarcode == code){
      return item;
    }else{
      if(String(item.product_STORE_CODE) == code){
        return item;
      }
    }

  }



  addDetail2(code) {
    this.disableDoubleClickSearch = true;
    // console.log(code,"da code :3")

    // var codeFilter = this.productsObject.filter(item => (String(item.code) === code || String(item.ownbarcode) === code || String(item.product_store_code) === code ));

    // console.log(codeFilter);
    var key: any = null;
    this.productsObject.forEach(item => {
      // console.log("//-----------------------------------");
      // console.log(item.code);
      // console.log("//-----------------------------------");
      // console.log(item.ownbarcode);
      // console.log("//-----------------------------------");
      // console.log(item.product_store_code);
      // console.log("//-----------------------------------");
      if(item.code == code || item.ownbarcode == code || String(item.product_store_code) == code ){
        key = item;
      }
      // else{
      // }
    });
    // console.log("this is array papu: ",this.productsObject)
    // console.log("this is key", key)
    if (key != null) {
      // console.log("entro")
      this.productsObject[this.productsObject.indexOf(key)].quantity += 1;
      this.disableDoubleClickSearch = false;
    } else {

      var product = this.inventoryList.find(item => this.findCode(code,item));
      // var product = this.inventoryList.find(item => (item.code == code || item.ownbarcode == code || String(item.product_STORE_CODE) == code));
      //@ts-ignore
      if(product){
        this.getPriceList(product,code,product.id_PRODUCT_STORE);
        setTimeout(() => {
          this.disableDoubleClickSearch = false;
          this.elementRef.nativeElement.focus();
          this.elementRef.nativeElement.select();

        }, 100);
      }else{
        alert('Ese codigo no esta asociado a un producto.');
        this.disableDoubleClickSearch = false;
      }
    }
  }

  addDetail(event) {
    console.log("STORE NAME, "+this.storeName);
    var code = String(event.target.value);
    // console.log(code,"da code :3")

    // var codeFilter = this.productsObject.filter(item => (String(item.code) === code || String(item.ownbarcode) === code || String(item.product_store_code) === code ));

    // console.log(codeFilter);
    var key: any = null;
    this.productsObject.forEach(item => {
      // console.log("//-----------------------------------");
      // console.log(item.code);
      // console.log("//-----------------------------------");
      // console.log(item.ownbarcode);
      // console.log("//-----------------------------------");
      // console.log(item.product_store_code);
      // console.log("//-----------------------------------");
      if(item.code == code || item.ownbarcode == code || String(item.product_store_code) == code ){
        key = item;
      }
      // else{
      // }
    });
    // console.log("this is array papu: ",this.productsObject)
    // console.log("this is key", key)
    if (key != null) {
      // console.log("entro")
      this.productsObject[this.productsObject.indexOf(key)].quantity += 1;
      event.target.value = '';
    } else {
      event.target.value = '';
      // var product = this.inventoryList.find(item => (item.code == code || item.ownbarcode == code || String(item.product_STORE_CODE) == code));
      var product = this.inventoryList.find(item => this.findCode(code,item));
      //@ts-ignore
      if(product){
        this.getPriceList(product,code,product.id_PRODUCT_STORE)
      }else{
        alert('Ese codigo no esta asociado a un producto.');
      }
    }
  }
  addDetailFromCat(code) {
    if (this.productsObject.hasOwnProperty(code)) {
      this.productsObject[code]['quantity'] += 1;
      this.setObject = Object.keys(this.productsObject);
      this.resetCategoryMenu();
      this.tab = 0;
    } else {
      var product = this.inventoryList.find(item => item['code'] == code);
      if (product) {
        let new_product = {
          quantity: 1,
          id_storage: this.storageList[0].id_storage,
          price: product.standard_PRICE,
          tax: this.getPercentTax(product.id_TAX),
          id_product_third: product.id_PRODUCT_STORE,
          tax_product: product.id_TAX,
          state: this.commonStateDTO,
          description: product.product_STORE_NAME,
          code: product.code,
          id_inventory_detail: product.id_INVENTORY_DETAIL
        };
        //this.detailBillDTOList.push(new_product);
        this.productsObject[code] = new_product;
        this.setObject = Object.keys(this.productsObject);

        setTimeout(() => {
          document.getElementById('lastDetailQuantity');
        });
        this.resetCategoryMenu();
        this.tab = 0;
      } else {
        this.resetCategoryMenu();
        this.showNotification('top','center',3,"<h3 class = 'text-center'>El producto solicitado no existe<h3>",'danger');
        this.tab = 0;
      }
    }
  }

  addDetailBuy(event) {
    var code = String(event.target.value);
    if (this.productsObject.hasOwnProperty(code)) {
      //this.productsObject[code]['quantity'] += 1;
      //this.setObject = Object.keys(this.productsObject);
      event.target.value = '';
      this.openDialogPurchaseDetail(this.productsObject[code]);
    } else {
      var product = this.inventoryList.find(item => item['code'] == code);
      if (product) {
        let new_product = {
          quantity: 1,
          id_storage: this.storageList[0].id_storage,
          tax: 0,
          id_product_third: product.id_PRODUCT_STORE,
          tax_product: product.id_TAX,
          state: this.commonStateDTO,
          description: product.product_STORE_NAME,
          code: product.code,
          id_inventory_detail: product.id_INVENTORY_DETAIL,
          purchaseTaxIncluded:true,
          purchaseValue:0,
          saleTaxIncluded:false,
          saleValue:product.standard_PRICE,
          flagNewProduct:false
        };

        event.target.value = '';
        this.openDialogPurchaseDetail(new_product);
        // event.target.blur();
        // setTimeout(() => {
        //   document.getElementById('lastDetailQuantity').focus();
        // });
      } else {
        let new_product = {
          quantity: 1,
          id_storage: this.storageList[0].id_storage,
          tax: 0.19,
          id_product_third: 0,
          tax_product: 0,
          state: this.commonStateDTO,
          description: '',
          code: code,
          id_inventory_detail: 0,
          purchaseTaxIncluded:true,
          purchaseValue:0,
          saleTaxIncluded:false,
          saleValue:0,
          flagNewProduct:true
        };
        event.target.value = '';
        this.openDialogPurchaseDetail(new_product);
      }
    }
  }

  calculateTotalPrice(key){
    return (this.productsObject[key].price+(this.productsObject[key].tax*this.productsObject[key].price)) * Math.floor(this.productsObject[key].quantity)
  }

  calculateSubtotal() {
    var subtotal = 0;
    var attr= this.flagVenta?'price':'purchaseValue';
    for (let code in this.productsObject) {
      subtotal += this.productsObject[code][attr] * Math.floor(this.productsObject[code]['quantity']);
    }
    return subtotal;
  }

  calculateTax() {
    var tax = 0;
    // if (!this.form.value['isRemission']) {
    for (let code in this.productsObject) {
      if(this.flagVenta){
        tax += this.productsObject[code]['price'] * Math.floor(this.productsObject[code]['quantity']) * this.productsObject[code]['tax'];
      }else{
        tax += this.productsObject[code]['price'] * Math.floor(this.productsObject[code]['quantity']) * this.productsObject[code]['tax'];
      }
    }
    // }
    return tax;
  }

  calculateTotal() {
    return this.calculateSubtotal() + this.calculateTax();
  }

  checkItem(event, code) {
    if (event.target.checked) {
      this.removalArray.add(code);
    } else {
      this.removalArray.delete(code);
    }
  }

  checkAllItems(event) {
    if (event.target.checked) {
      this.removalArray = new Set(this.setObject);
    } else {
      this.removalArray = new Set();
    }
  }

  removeItems() {
    this.removalArray.forEach((item) => {
      //@ts-ignore
      if (this.productsObject.hasOwnProperty(item)) {
        //@ts-ignore
        delete this.productsObject[item];
      }
    });
    this.removalArray = new Set();
    this.setObject = Object.keys(this.productsObject);
  }

  editPurchaseValue(event: any) {
    event.target.blur();
    setTimeout(() => {
      document.getElementById('lastDetailSaleValue').focus();
    });
  }

  readNew(event: any) {
    event.target.blur();
    setTimeout(() => {
      document.getElementById('reader').focus();
    });
  }

  cancel() {
    this.billDTO.id_third_destiny = null;
    this.listElem = "";
    this.listaElem = [];
    this.form.reset();
    this.clientData = new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ', null);
    this.productsObject = [];
    this.setObject = [];
    this.removalArray = new Set();

    this.CUSTOMER_ID= this.token.id_third_father;
    setTimeout(() => {

      this.elementRef.nativeElement.focus();
      this.elementRef.nativeElement.select();

    }, 100);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(QuantityDialogComponent, {
      width: '40vw',
      data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
    });
  }

  openDialogClient(tipo): void {
    var dialogRef;
    if(tipo == 0){
      dialogRef = this.dialog.open(ThirdDialogComponent, {
        width: '60vw',
        data: {}
      });
    }

    if(tipo == 1){
      dialogRef = this.dialog.open(PersonDialogComponent, {
        width: '60vw',
        data: {}
      });
    }

    if(tipo == 2){
      dialogRef = this.dialog.open(EmployeeDialogComponent, {
        width: '60vw',
        data: {}
      });
    }

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.CUSTOMER_ID= result.id;
        // console.log('CREATE CLIENT SUCCESS');
        // console.log(result);
        let isNaturalPerson= result.data.hasOwnProperty('profile')?true:false;
        let dataPerson= isNaturalPerson?result.data.profile:result.data;
        this.clientData.is_natural_person = isNaturalPerson;
        this.clientData.fullname= dataPerson.info.fullname;
        this.clientData.document_type = dataPerson.info.id_document_type;
        this.clientData.document_number = dataPerson.info.document_number;
        this.clientData.address = dataPerson.directory.address;
        this.clientData.phone = dataPerson.directory.phones[0].phone;
        this.clientData.email = dataPerson.directory.hasOwnProperty('mails')?dataPerson.directory.mails[0].mail:'N/A';
      }
    });
  }

  openDialogSearchClient(): void {
    const dialogRef = this.dialog.open(SearchClientDialogComponent, {
      width: '40vw',
      data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
    });
  }

  openDialogSearchProduct(): void {
    const dialogRef = this.dialog.open(SearchProductDialogComponent, {
      width: '40vw',
      data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
    });
  }

  openDialogPurchaseDetail(product): void {
    const dialogRef = this.dialog.open(PurchaseDetailDialogComponent, {
      width: '60vw',
      data: product
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        if(result.flagNewProduct){
          // this.getInventoryList(this.STATE, null, null, this.ID_INVENTORY_TEMP).then(()=>{
          this.getInventoryList(this.id_store).then(()=>{
            var product = this.inventoryList.find(item => item['code'] == result.code);
            if (product) {
              let new_product = {
                quantity: 1,
                id_storage: this.storageList[0].id_storage,
                tax: this.getPercentTax(product.id_TAX),
                id_product_third: product.id_PRODUCT_STORE,
                tax_product: product.id_TAX,
                state: this.commonStateDTO,
                description: product.product_STORE_NAME,
                code: product.code,
                id_inventory_detail: product.id_INVENTORY_DETAIL,
                purchaseTaxIncluded:true,
                purchaseValue:0,
                saleTaxIncluded:false,
                saleValue:product.standard_PRICE,
                flagNewProduct:false
              };
              this.openDialogPurchaseDetail(new_product);
            }
          });
        }else{
          this.productsObject[result.code]= result;
          this.setObject = Object.keys(this.productsObject);
        }
      }
    });
  }

  openDialogTransactionConfirm(disc,isCompra): void {
    
    this.quickconfirm = true;
    if( disc==1 || disc==2 ){
      const dialogRef = this.dialog.open(TransactionConfirmDialogComponent, {
        width: '60vw',
        height: '80vh',
        data: { total: this.calculateTotal(), productsQuantity: Object.keys(this.productsObject).length }
      });

      dialogRef.afterClosed().subscribe(result => {
        this.quickconfirm=false;
        if(result){
          this.clientData = result.clientData;
          console.log("this is result: ",JSON.stringify(result));
          //this.save(result,disc,isCompra);
          this.sendData(result,disc,true);
        }else{

        }
      });
    }else{

      //this.save(this.clientData,disc,isCompra);
      this.sendData(this.clientData,disc,true);
    }

  }

  sendData(data,disc,pdf){

    let detailList = [];
    //GENERO LA LISTA DE DTOs DE DETALLES
    this.productsObject.forEach(item => {
      detailList.push({cantidad:item.quantity,
        valor:item.price,
        idproductstore:item.id_product_third,
        id_impuesto:item.tax_product})
    }) 
    

    //GENERO EL DTO DE FACTURA
    this.getDomiName();
    if(this.locStorage.getIdStore() == 81){
      this.flag = "1"
    }
    let datos = "";
    if(data.paymentMethod == "efectivo"){
      datos = "Efectivo"
    }
    if(data.paymentMethod == "debito"){
      datos = "Débito"
    }
    if(data.paymentMethod == "credito"){
      datos = "Crédito"
    }
    console.log("this is id person: ",data.id_person);
    if(data.id_person){
      console.log("ENTRE");
      //@ts-ignore
      this.billDTO.id_third_destiny = data.id_person;

      this.detailBillDTOList = [];
      this.commonStateDTO.state = 1;
      this.inventoryQuantityDTOList = [];
      let titleString= this.form.value['isRemission']?' Por Remision':'';

      if(disc==1){
        this.documentDTO.title = 'Movimiento De Venta';
      }
      if(disc==2){
        this.documentDTO.title = 'Movimiento De Compra';
      }
      if(disc==3){
        this.documentDTO.title = 'Movimiento De Entrada';
      }
      if(disc==4){
        this.documentDTO.title = 'Movimiento De Salida';
      }
      if(disc==5){
        this.documentDTO.title = 'Movimiento De Devolucion';
      }

      if(disc==1 || disc==2){
        this.documentDTO.body = data.observations||'';
      }

      if(disc==4 || disc==3){
        this.documentDTO.body = this.ObservationsInOrOut;
      }

      this.billDTO.id_store = this.locStorage.getIdStore();
      if (detailList.length > 0 && this.checker) {
        let ID_BILL_TYPE= this.form.value['isRemission']?107:1;
        this.billDTO.id_third_employee = this.token.id_third_father;
        this.billDTO.id_third = this.token.id_third;
        this.billDTO.id_bill_type = 1;
        //instanciar de acuerdo a por remision
        this.billDTO.id_bill_state = 1;
        this.billDTO.purchase_date = new Date();
        this.billDTO.subtotal = this.roundnum(this.calculateSubtotal());
        this.billDTO.tax = this.roundnum(this.calculateTax());
        this.billDTO.totalprice = this.roundnum(this.calculateTotal());
        if(disc==3 || disc==4){
          this.billDTO.subtotal = 0;
          this.billDTO.tax = 0;
          this.billDTO.totalprice = 0;
        }
        this.billDTO.discount = 0;
        this.billDTO.documentDTO = this.documentDTO;
        this.billDTO.state = null;

        this.billDTO.details = this.detailBillDTOList;
 
        if(disc==1 || disc==2){

          if(data.paymentMethod == "efectivo"){
            this.paymentDetail+='"id_payment_method": 1, ';
            this.paymentDetail+='"aprobation_code": "", ';
            this.paymentDetailFinal.aprobation_code= "";
            this.paymentDetailFinal.id_payment_method=1;
          }
          if(data.paymentMethod == "debito"){
            this.paymentDetail+='"id_payment_method": 2, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.aprobation_code= data.transactionCode;
            this.paymentDetailFinal.id_payment_method=2;
          }
          if(data.paymentMethod == "credito"){
            this.paymentDetail+='"id_payment_method": 3, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.aprobation_code= data.transactionCode;
            this.paymentDetailFinal.id_payment_method=3;
          }


          if(data.wayToPay == "contado"){
            this.paymentDetail+='"id_way_to_pay": 1, ';
            this.paymentDetailFinal.id_way_to_pay = 1;
          }

          if(data.wayToPay == "credito"){
            this.paymentDetail+='"id_way_to_pay": 2, ';
            this.paymentDetailFinal.id_way_to_pay = 2;
          }



          this.paymentDetail+='"payment_value": '+this.roundnum(this.calculateTotal())+', ';
          this.paymentDetail+='"state": {"state": 1}}]';
          this.paymentDetailFinal.state = new stateDTO(1);
          this.paymentDetailFinal.payment_value = this.roundnum(this.calculateTotal());

        }


        console.log("THIS IS BILL DTO V2: ", JSON.stringify({idthirdemployee:this.billDTO.id_third,
          idthird:this.billDTO.id_third_employee,
          idbilltype:this.billDTO.id_bill_type,
          notes:this.billDTO.documentDTO.body,
          idthirddestinity:this.billDTO.id_third_destiny,
          idcaja:this.locStorage.getIdCaja(),
          idstore:this.locStorage.getIdStore(),
          idthirddomiciliario:this.selectedDomi,
          idpaymentmethod:this.paymentDetailFinal.id_payment_method,
          idwaytopay:this.paymentDetailFinal.id_way_to_pay,
          approvalcode:this.paymentDetailFinal.aprobation_code,
          idbankentity:1,
          idbillstate:1,
          details:detailList}))

          this.http2.post(Urlbase[3]+"/billing/v2",{idthirdemployee:this.billDTO.id_third,
          idthird:this.billDTO.id_third_employee,
          idbilltype:this.billDTO.id_bill_type,
          notes:this.billDTO.documentDTO.body,
          idthirddestinity:this.billDTO.id_third_destiny,
          idcaja:this.locStorage.getIdCaja(),
          idstore:this.locStorage.getIdStore(),
          idthirddomiciliario:this.selectedDomi,
          idpaymentmethod:this.paymentDetailFinal.id_payment_method,
          idwaytopay:this.paymentDetailFinal.id_way_to_pay,
          approvalcode:this.paymentDetailFinal.aprobation_code,
          idbankentity:1,
          idbillstate:1,
          details:detailList}).subscribe(idBill => {
            if(pdf){
              this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+idBill,{}).subscribe(resp => {
                  
              this.http2.get(Urlbase[3]+"/billing/UniversalPDF?id_bill="+idBill,{responseType: 'text'}).subscribe(response =>{
                //-----------------------------------------------------------------------------------------------------------------
                window.open(Urlbase[7]+"/"+response, "_blank");
                this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
               this.http2.get(Urlbase[5]+"/close/alertForEmail?id_caja="+this.locStorage.getIdCaja()+"&id_third="+this.locStorage.getThird().id_third).subscribe(data => {
                    //@ts-ignore
                    console.log(data)
                    setTimeout(() => {
                      this.quickconfirm=false;
                      this.cancel();
                    
                      this.elementRef.nativeElement.focus();
                      this.elementRef.nativeElement.select();
                    
                    }, 100);
                  })
                });
                
              });
            }else{
              setTimeout(() => {
                this.quickconfirm=false;
                this.cancel();
              
                this.elementRef.nativeElement.focus();
                this.elementRef.nativeElement.select();
                this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
            
              
              }, 100);
            }
          })

      }else{
        
        this.checker = true;
        this.quickconfirm=false;
        this.cancel();
        this.showNotification('top', 'center', 3, "<h3> Intentaste agregar un producto con cantidad en <b>CERO</b> a la factura, para agregarlo al carrito, tiene que tener cantidad en 1</h3> ", 'danger');
      }
    }else{
      console.log("ENTRE - NO PERSON")
      this.billDTO.id_store = this.locStorage.getIdStore();
      this.detailBillDTOList = [];
      this.commonStateDTO.state = 1;
      this.inventoryQuantityDTOList = [];
      let titleString= this.form.value['isRemission']?' Por Remision':'';

      if(disc==1){
        this.documentDTO.title = 'Movimiento De Venta';
      }
      if(disc==2){
        this.documentDTO.title = 'Movimiento De Compra';
      }
      if(disc==3){
        this.documentDTO.title = 'Movimiento De Entrada';
      }
      if(disc==4){
        this.documentDTO.title = 'Movimiento De Salida';
      }
      if(disc==5){
        this.documentDTO.title = 'Movimiento De Devolucion';
      }

      if(disc==1 || disc==2){
        this.documentDTO.body = data.observations||'';
      }

      if(disc==4 || disc==3){
        this.documentDTO.body = this.ObservationsInOrOut;
      }

      if (detailList.length > 0 && this.checker) {
        let ID_BILL_TYPE= this.form.value['isRemission']?107:1;
        this.billDTO.id_third_employee = this.token.id_third_father;
        this.billDTO.id_third = this.token.id_third;
        this.billDTO.id_bill_type = 1;
        //instanciar de acuerdo a por remision
        this.billDTO.id_bill_state = 1;
        this.billDTO.purchase_date = new Date();
        this.billDTO.subtotal = Math.floor(this.calculateSubtotal()* 100) / 100;
        this.billDTO.tax = Math.floor(this.calculateTax()* 100) / 100;
        this.billDTO.totalprice = Math.floor(this.calculateTotal()* 100) / 100;
        if(disc==3 || disc==4){
          this.billDTO.subtotal = 0;
          this.billDTO.tax = 0;
          this.billDTO.totalprice = 0;
        }
        this.billDTO.discount = 0;
        this.billDTO.documentDTO = this.documentDTO;
        this.billDTO.state = null;

        this.billDTO.details = this.detailBillDTOList;

        if(disc==1 || disc==2){

          if(data.paymentMethod == "efectivo"){
            this.paymentDetail+='"id_payment_method": 1, ';
            this.paymentDetail+='"aprobation_code": "", ';
            this.paymentDetailFinal.aprobation_code = "";
            this.paymentDetailFinal.id_payment_method = 1;
          }
          if(data.paymentMethod == "debito"){
            this.paymentDetail+='"id_payment_method": 2, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.aprobation_code = data.transactionCode;
            this.paymentDetailFinal.id_payment_method = 2;
          }
          if(data.paymentMethod == "credito"){
            this.paymentDetail+='"id_payment_method": 3, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.aprobation_code = data.transactionCode;
            this.paymentDetailFinal.id_payment_method = 3;
          }


          if(data.wayToPay == "contado"){
            this.paymentDetail+='"id_way_to_pay": 1, ';
            this.paymentDetailFinal.id_way_to_pay=1;
          }

          if(data.wayToPay == "credito"){
            this.paymentDetail+='"id_way_to_pay": 2, ';
            this.paymentDetailFinal.id_way_to_pay=2;
          }


          this.paymentDetailFinal.payment_value = this.calculateTotal();
          this.paymentDetailFinal.state = new stateDTO(1);
          this.paymentDetail+='"payment_value": '+this.calculateTotal()+', ';
          this.paymentDetail+='"state": {"state": 1}}]';

        }
        console.log("THIS IS BILL DTO V2: ", JSON.stringify({idthirdemployee:this.billDTO.id_third,
          idthird:this.billDTO.id_third_employee,
          idbilltype:this.billDTO.id_bill_type,
          notes:this.billDTO.documentDTO.body,
          idthirddestinity:this.billDTO.id_third_destiny,
          idcaja:this.locStorage.getIdCaja(),
          idstore:this.locStorage.getIdStore(),
          idthirddomiciliario:this.selectedDomi,
          idpaymentmethod:this.paymentDetailFinal.id_payment_method,
          idwaytopay:this.paymentDetailFinal.id_way_to_pay,
          approvalcode:this.paymentDetailFinal.aprobation_code,
          idbankentity:1,
          idbillstate:1,
          details:detailList}))

          this.http2.post('http://'+'tienda724.com'+':8448/v1'+"/billing/v2",{idthirdemployee:this.billDTO.id_third,
          idthird:this.billDTO.id_third_employee,
          idbilltype:this.billDTO.id_bill_type,
          notes:this.billDTO.documentDTO.body,
          idthirddestinity:this.billDTO.id_third_destiny,
          idcaja:this.locStorage.getIdCaja(),
          idstore:this.locStorage.getIdStore(),
          idthirddomiciliario:this.selectedDomi,
          idpaymentmethod:this.paymentDetailFinal.id_payment_method,
          idwaytopay:this.paymentDetailFinal.id_way_to_pay,
          approvalcode:this.paymentDetailFinal.aprobation_code,
          idbankentity:1,
          idbillstate:1,
          details:detailList}).subscribe(idBill => {
            if(pdf){
              this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+idBill,{}).subscribe(resp => {
                  
              this.http2.get(Urlbase[3]+"/billing/UniversalPDF?id_bill="+idBill,{responseType: 'text'}).subscribe(response =>{
                //-----------------------------------------------------------------------------------------------------------------
                window.open(Urlbase[7]+"/"+response, "_blank");
                this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
                this.http2.get(Urlbase[5]+"/close/alertForEmail?id_caja="+this.locStorage.getIdCaja()+"&id_third="+this.locStorage.getThird().id_third).subscribe(data => {
                    //@ts-ignore
                    console.log(data)
                    setTimeout(() => {
                      this.quickconfirm=false;
                      this.cancel();
                    
                      this.elementRef.nativeElement.focus();
                      this.elementRef.nativeElement.select();
                    
                    }, 100);
                  })
                });
              });
            }else{
              setTimeout(() => {
                this.quickconfirm=false;
                this.cancel();
              
                this.elementRef.nativeElement.focus();
                this.elementRef.nativeElement.select();
                this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
                
              
              }, 100);
            } })
  
      }else{
        this.cancel();
        this.checker = true;
        this.quickconfirm=false;
        this.showNotification('top', 'center', 3, "<h3> Intentaste agregar un producto con cantidad en <b>CERO</b> a la factura, para agregarlo al carrito, tiene que tener cantidad en 1</h3> ", 'danger');
      }
    }

    
    

  }

  quickConfirm(){
    this.quickconfirm = true;
    //  this.save2({clientData:new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ', null),
    //    cambio:0,
    //    wayToPay: "contado",
    //    cash: this.roundnum(this.calculateTotal()),
    //    creditBank: "",
    //    debitBank: "",
    //    observations: "PAGO RAPIDO",
    //    paymentMethod: "efectivo",
    //    transactionCode: " ",},1,1);
     this.sendData({clientData:new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ', null),
       cambio:0,
       wayToPay: "contado",
       cash: this.roundnum(this.calculateTotal()),
       creditBank: "",
       debitBank: "",
       observations: "PAGO RAPIDO",
       paymentMethod: "efectivo",
       transactionCode: " ",},1, false
     )
  }


  quickConfirmPDF(){
    this.quickconfirm = true;
    //  this.save3({clientData:new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ', null),
    //    cambio:0,
    //    wayToPay: "contado",
    //    cash: this.roundnum(this.calculateTotal()),
    //    creditBank: "",
    //    debitBank: "",
    //    observations: "PAGO RAPIDO",
    //    paymentMethod: "efectivo",
    //    transactionCode: " ",},1,1);
     this.sendData({clientData:new ClientData(true, 'Cliente Ocasional', ' ', ' ', ' ', ' ', ' ', null),
       cambio:0,
       wayToPay: "contado",
       cash: this.roundnum(this.calculateTotal()),
       creditBank: "",
       debitBank: "",
       observations: "PAGO RAPIDO",
       paymentMethod: "efectivo",
       transactionCode: " ",},1, true
     )
  }





  // start controls
  createControls() {
    this.form = this.fb.group({

      isRemission: [true, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-z0-9_-]{8,15}$")
      ])],

      number_order: ['', Validators.compose([
        Validators.required
      ])],

      date: [{ value: '', disabled: true }, Validators.compose([
        Validators.required
      ])],

    });
  }

  loadData() {
    this.form.patchValue({
      isRemission: true,
      number_order: '',
      date: new Date()
    });
  }

  logChange() {
    const isRemission = this.form.get('isRemission');
    isRemission.valueChanges.forEach((value: boolean) => {
    });

    const listenerNumberOrder = this.form.get('number_order');
    listenerNumberOrder.valueChanges.forEach((value: string) => {
    });
  }// end controls


  // services

  async getInventoryList(store){
    this.http2.get("http://tienda724.com:8448/v1/billing/domiciliarios?id_store="+this.locStorage.getIdStore()).subscribe(data => {
      this.domiList = data;
      console.log("DOMI DOMI, "+data)
    });
    this.inventoriesService.getInventory(store).subscribe((data: InventoryName[]) => {
          console.log("This is InventoryList: ",data);
          this.inventoryList = data;
          //  this.getPriceList();
          setTimeout(() => {

            this.elementRef.nativeElement.focus();
            this.elementRef.nativeElement.select();

          }, 150);
          console.log("THIS IS SOME DATA I HAVE TO CHECK, do i add stuff? ", this.doIAddItems,", and do i have a id bill? ",this.idBillToAdd);
          if(this.locStorage.getDoIMakeRefund()){
            console.log("ENTRE A LA LISTA REFUND");
            this.http2.get(Urlbase[3]+"/billing/detail?id_bill="+this.locStorage.getIdRefund()).subscribe(response => {
              this.locStorage.setDoIMakeRefund(false);
              this.locStorage.setIdRefund(0);
              //@ts-ignore
              response.forEach(element => {
                this.addDetail3(Number(element[3]),Number(element[0]))
              });
              // //@ts-ignore
              // for(let i=0;i<response.length;i++){
              //   for(let j=0; j < Number(response[i][0]);j++){
              //     this.addDetail2(response[i][3]); 
              //   }
              // }
            })
          }
        },
        (error) =>{
          console.log(error);
        },
        () => {
          if (this.inventoryList.length > 0) {

          }
        });

  }





  // getInventoryList(state_inv_detail?: number, state_product?: number, id_inventory_detail?: number,
  //   id_inventory?: number, id_product_third?: number, location?: number,
  //   id_third?: number, id_category_third?: number, quantity?: number, id_state_inv_detail?: number,
  //   id_product?: number, id_category?: number, stock?: number,
  //   stock_min?: number, img_url?: string, id_tax?: number,
  //   id_common_product?: number, name_product?: string,
  //   description_product?: string, id_state_product?: number,


  //   id_state_prod_third?: number, state_prod_third?: number,
  //   id_measure_unit?: number, id_measure_unit_father?: number,
  //   id_common_measure_unit?: number, name_measure_unit?: string,
  //   description_measure_unit?: string, id_state_measure_unit?: number,
  //   state_measure_unit?: number, id_code?: number,
  //   code?: number, img?: string,
  //   id_attribute_list?: number,
  //   id_state_cod?: number, state_cod?: number,
  //   attribute?: number,
  //   attribute_value?: number) {
  //   return new Promise((resolve,reject)=>{

  //     this.inventoriesService.getInventoriesDetailList(state_inv_detail, state_product, id_inventory_detail,

  //       id_inventory, quantity, id_state_inv_detail,
  //       id_product, id_category, stock,
  //       stock_min, img_url, id_tax,
  //       id_common_product, name_product,
  //       description_product, id_state_product,
  //       id_product_third, location,
  //       id_third, id_category_third,

  //       id_state_prod_third, state_prod_third,
  //       id_measure_unit, id_measure_unit_father,
  //       id_common_measure_unit, name_measure_unit,
  //       description_measure_unit, id_state_measure_unit,
  //       state_measure_unit, id_code,
  //       code, img,
  //       id_state_cod, state_cod)


  //       .subscribe((data: InventoryDetail[]) => {
  //           this.inventoryList = data;
  //           resolve();
  //         },
  //         (error) =>{ 
  //           console.log(error);
  //           reject();
  //         },
  //         () => {
  //           if (this.inventoryList.length > 0) {

  //           }
  //         });
  //   });
  // }
  // end process

  // cashback(disc): void {    
  //   console.log("put para actualizar el state del bill original", this.idBillOriginal)
  //   for (let code in this.productsObject) {
  //     let element = this.productsObject[code];

  //     this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null);

  //     // Inv Quantity
  //     this.inventoryQuantityDTO.id_inventory_detail = element.id_inventory_detail;
  //     this.inventoryQuantityDTO.id_product_third = element.id_product_third;
  //     this.inventoryQuantityDTO.code = element.code;

  //     // Inv Quantity for discount tienda
  //     this.inventoryQuantityDTO.quantity = element.quantity;

  //     this.inventoryQuantityDTOList.push(
  //       this.inventoryQuantityDTO
  //     );
  //   }

  //   this.beginPlusOrDiscount(this.ID_INVENTORY_TEMP, this.inventoryQuantityDTOList,disc);

  //   console.log("se crea un nuevo Detalle de caja");

  //   if(this.isTotalDev==true){
  //     console.log("es devolucion total")
  //   }else{
  //     console.log("no es devolucion total")
  //   }


  // }

  disableDoubleClickSearch = false;



  save(data,disc,isCompra) {
    this.getDomiName();
    if(this.locStorage.getIdStore() == 81){
      this.flag = "1"
    }
    let datos = "";
    if(data.paymentMethod == "efectivo"){
      datos = "Efectivo"
    }
    if(data.paymentMethod == "debito"){
      datos = "Débito"
    }
    if(data.paymentMethod == "credito"){
      datos = "Crédito"
    }
    console.log("this is id person: ",data.id_person);
    if(data.id_person){
      console.log("ENTRE");
      //@ts-ignore
      this.billDTO.id_third_destiny = data.id_person;

      this.detailBillDTOList = [];
      this.commonStateDTO.state = 1;
      this.inventoryQuantityDTOList = [];
      let titleString= this.form.value['isRemission']?' Por Remision':'';

      if(disc==1){
        this.documentDTO.title = 'Movimiento De Venta';
      }
      if(disc==2){
        this.documentDTO.title = 'Movimiento De Compra';
      }
      if(disc==3){
        this.documentDTO.title = 'Movimiento De Entrada';
      }
      if(disc==4){
        this.documentDTO.title = 'Movimiento De Salida';
      }
      if(disc==5){
        this.documentDTO.title = 'Movimiento De Devolucion';
      }

      if(disc==1 || disc==2){
        this.documentDTO.body = data.observations||'';
      }

      if(disc==4 || disc==3){
        this.documentDTO.body = this.ObservationsInOrOut;
      }

      /**
       * building detailBill and Quantity
       */
      for (let code in this.productsObject) {
        let element = this.productsObject[code];

        this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null,null);
        this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO,null);

        //building detailBill
        console.log;
        this.detailBillDTO.id_storage = element.id_storage;
        this.detailBillDTO.price = element.price;
        this.detailBillDTO.tax = this.getPercentTax(element.tax_product)*100;
        this.detailBillDTO.id_product_third = element.id_product_third;
        this.detailBillDTO.tax_product = element.tax_product;
        this.detailBillDTO.state = this.commonStateDTO;
        this.detailBillDTO.quantity = Math.floor(element.quantity);

        if(element.quantity > 0) {
          this.detailBillDTOList.push(this.detailBillDTO);
        }else{
          this.checker = false;
        }

        // Inv Quantity
        this.inventoryQuantityDTO.id_inventory_detail = element.id_inventory_detail;
        this.inventoryQuantityDTO.id_product_third = element.id_product_third;
        this.inventoryQuantityDTO.code = element.code;
        this.inventoryQuantityDTO.id_storage = element.id_storage;

        // Inv Quantity for discount tienda
        this.inventoryQuantityDTO.quantity = Math.floor(element.quantity);

        this.inventoryQuantityDTOList.push(
            this.inventoryQuantityDTO
        );
      }
      this.billDTO.id_store = this.locStorage.getIdStore();
      if (this.detailBillDTOList.length > 0 && this.checker) {
        let ID_BILL_TYPE= this.form.value['isRemission']?107:1;
        this.billDTO.id_third_employee = this.token.id_third_father;
        this.billDTO.id_third = this.token.id_third;
        this.billDTO.id_bill_type = 1;
        //instanciar de acuerdo a por remision
        this.billDTO.id_bill_state = 1;
        this.billDTO.purchase_date = new Date();
        this.billDTO.subtotal = this.roundnum(this.calculateSubtotal());
        this.billDTO.tax = this.roundnum(this.calculateTax());
        this.billDTO.totalprice = this.roundnum(this.calculateTotal());
        if(disc==3 || disc==4){
          this.billDTO.subtotal = 0;
          this.billDTO.tax = 0;
          this.billDTO.totalprice = 0;
        }
        this.billDTO.discount = 0;
        this.billDTO.documentDTO = this.documentDTO;
        this.billDTO.state = null;

        this.billDTO.details = this.detailBillDTOList;

        if(disc==1 || disc==2){

          if(data.paymentMethod == "efectivo"){
            this.paymentDetail+='"id_payment_method": 1, ';
            this.paymentDetail+='"aprobation_code": "", ';
            this.paymentDetailFinal.aprobation_code= "";
            this.paymentDetailFinal.id_payment_method=1;
          }
          if(data.paymentMethod == "debito"){
            this.paymentDetail+='"id_payment_method": 2, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.aprobation_code= data.transactionCode;
            this.paymentDetailFinal.id_payment_method=2;
          }
          if(data.paymentMethod == "credito"){
            this.paymentDetail+='"id_payment_method": 3, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.aprobation_code= data.transactionCode;
            this.paymentDetailFinal.id_payment_method=3;
          }


          if(data.wayToPay == "contado"){
            this.paymentDetail+='"id_way_to_pay": 1, ';
            this.paymentDetailFinal.id_way_to_pay = 1;
          }

          if(data.wayToPay == "credito"){
            this.paymentDetail+='"id_way_to_pay": 2, ';
            this.paymentDetailFinal.id_way_to_pay = 2;
          }



          this.paymentDetail+='"payment_value": '+this.roundnum(this.calculateTotal())+', ';
          this.paymentDetail+='"state": {"state": 1}}]';
          this.paymentDetailFinal.state = new stateDTO(1);
          this.paymentDetailFinal.payment_value = this.roundnum(this.calculateTotal());

        }
        console.log(JSON.stringify(this.billDTO),"This is Bill DTO");
        this.billingService.postBillResource(this.billDTO, disc)
            .subscribe(
                result => {

                  this.http2.put("http://tienda724.com:8448/v1/billing/domiciliario?id_bill="+result+"&id_third="+this.selectedDomi,{}).subscribe(responseItem => {
                    console.log("THIS IS ITEM, "+responseItem);
                    this.selectedDomi = "-1";
                   
                  


                  console.log("THIS IS RESULT", result);






                  // console.log("THIS IS first dir: http://tienda724.com:8448/v1/billing/master?id_bill="+result+"&id_store="+this.locStorage.getIdStore());
                  // this.http2.get(Urlbase[3]+"/billing/master?id_bill="+result+"&id_store="+this.locStorage.getIdStore()).subscribe(
                  //   dataresult => {
                  //     console.log("THIS IS DATA RESULT, ", dataresult)
                  //     console.log("THIS IS second dir: http://tienda724.com:8448/v1/billing/legaldata?id_third="+this.locStorage.getThird().id_third );
                  //     this.http2.get(Urlbase[3]+"/billing/legaldata?id_third="+this.locStorage.getThird().id_third).subscribe(legalData => {
                  //       console.log("THIS IS first dir: http://tienda724.com:8448/v1/billing/detail?id_bill="+result );
                  //       this.http2.get(Urlbase[3]+"/billing/detail?id_bill="+result).subscribe(dataset => {
                  //        console.log("THIS IS MY LEGAL DATA,",legalData);

                  //       console.log("THIS IS MY CONSULTA, ", Urlbase[3]+"/billing/legaldata?id_third="+this.locStorage.getThird().id_third)
                  //       console.log("THIS IS DATOS", datos)
                  //       if(this.locStorage.getIdStore()==141){
                  //         //@ts-ignore
                  //         this.pdfDatas =  new pdfData (this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,"Régimen Simplificado",datos,dataresult.fullname,dataresult.store_NAME,dataresult.purchase_DATE,dataresult.caja,dataresult.num_DOCUMENTO,dataset,dataresult.subtotal,dataresult.tax,dataresult.totalprice,this.locStorage.getPerson().first_name+" "+this.locStorage.getPerson().first_lastname,data.cambio,legalData[0].city_NAME+", "+legalData[0].address,legalData[0].phone1,data.clientData.document_type+": "+data.clientData.document_number,data.clientData.fullname,data.clientData.address,data.clientData.phone,"18763000168739 Autorización: Desde CD1 a CD99999",legalData[0].regimen_TRIBUTARIO,legalData[0].prefix_BILL,legalData[0].initial_RANGE,legalData[0].final_RANGE,2);
                  //       }else{
                  //         if(this.locStorage.getIdStore()==142){
                  //           //@ts-ignore
                  //           this.pdfDatas =  new pdfData (this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,"Régimen Simplificado",datos,dataresult.fullname,dataresult.store_NAME,dataresult.purchase_DATE,dataresult.caja,dataresult.num_DOCUMENTO,dataset,dataresult.subtotal,dataresult.tax,dataresult.totalprice,this.locStorage.getPerson().first_name+" "+this.locStorage.getPerson().first_lastname,data.cambio,legalData[0].city_NAME+", "+legalData[0].address,legalData[0].phone1,data.clientData.document_type+": "+data.clientData.document_number,data.clientData.fullname,data.clientData.address,data.clientData.phone,"18763000168739 Autorización: Desde C11 a C199999",legalData[0].regimen_TRIBUTARIO,legalData[0].prefix_BILL,legalData[0].initial_RANGE,legalData[0].final_RANGE,2);
                  //         }else{
                  //           if(this.locStorage.getIdStore()==143){
                  //             //@ts-ignore
                  //             this.pdfDatas =  new pdfData (this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,"Régimen Simplificado",datos,dataresult.fullname,dataresult.store_NAME,dataresult.purchase_DATE,dataresult.caja,dataresult.num_DOCUMENTO,dataset,dataresult.subtotal,dataresult.tax,dataresult.totalprice,this.locStorage.getPerson().first_name+" "+this.locStorage.getPerson().first_lastname,data.cambio,legalData[0].city_NAME+", "+legalData[0].address,legalData[0].phone1,data.clientData.document_type+": "+data.clientData.document_number,data.clientData.fullname,data.clientData.address,data.clientData.phone,"18763000168739 Autorización: Desde VL1 a VL99999",legalData[0].regimen_TRIBUTARIO,legalData[0].prefix_BILL,legalData[0].initial_RANGE,legalData[0].final_RANGE,2);
                  //           }else{
                  //             //@ts-ignore
                  //             this.pdfDatas =  new pdfData (this.locStorage.getThird().info.type_document+" "+this.locStorage.getThird().info.document_number,"Régimen Simplificado",datos,dataresult.fullname,dataresult.store_NAME,dataresult.purchase_DATE,dataresult.caja,dataresult.num_DOCUMENTO,dataset,dataresult.subtotal,dataresult.tax,dataresult.totalprice,this.locStorage.getPerson().first_name+" "+this.locStorage.getPerson().first_lastname,data.cambio,legalData[0].city_NAME+", "+legalData[0].address,legalData[0].phone1,data.clientData.document_type+": "+data.clientData.document_number,data.clientData.fullname,data.clientData.address,data.clientData.phone,legalData[0].resolucion_DIAN,legalData[0].regimen_TRIBUTARIO,legalData[0].prefix_BILL,legalData[0].initial_RANGE,legalData[0].final_RANGE,2);
                  //           }
                  //         }
                  //       }

                  //         console.log("this is client data dude: ",data)
                  //         console.log("this is pdfData1: ", JSON.stringify(this.pdfDatas));
                  //         this.http2.post(Urlbase[3]+"/billing/pdf",this.pdfDatas,{responseType: 'text'}).subscribe( response => {
                  //           window.open(Urlbase[7]+"/"+response, "_blank");
                  //         }
                  //       );
                  //     });
                  //    });
                  //   });
                  if (result) {
                    this.http2.post(Urlbase[3]+"/billing/procedureup2?idbill="+result,{}).subscribe(resp => {
                      console.log("THIS IS MY RESP: ",result);
                      // alert(this.ID_INVENTORY_TEMP)
                      this.beginPlusOrDiscount(this.inventoryQuantityDTOList,disc);

                      if(disc==1 || disc==2){

                        try{
                          console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE, ", JSON.parse(this.paymentDetail));
                          console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE 2, ", this.paymentDetailFinal);
                          this.billingService.postPaymentDetail([this.paymentDetailFinal],Number(result)).subscribe(answer =>{
                                this.paymentDetailFinal = new paymentDetailDTO(1,1,this.calculateTotal()," ",new stateDTO(1));
                                this.http2.get(Urlbase[3]+"/billing/UniversalPDF?id_bill="+result,{responseType: 'text'}).subscribe(response =>{
                                  //-----------------------------------------------------------------------------------------------------------------
                                  window.open(Urlbase[7]+"/"+response, "_blank");
                                  this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
                                  this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                                    this.http2.get(Urlbase[5]+"/close/alertForEmail?id_caja="+this.locStorage.getIdCaja()+"&id_third="+this.locStorage.getThird().id_third).subscribe(data => {
                                      //@ts-ignore
                                      console.log(data)
                                    })
                                  });
                                  setTimeout(() => {
                                  
                                    this.elementRef.nativeElement.focus();
                                    this.elementRef.nativeElement.select();
                                  
                                  }, 100);
                                  //-----------------------------------------------------------------------------------------------------------------
                                });

                                this.paymentDetail = '[{';
                                var currentSells = localStorage.getItem("sells");
                                var mySells = '';
                                var total = this.calculateTotal();
                                if(currentSells !== 'null'){
                                  mySells = currentSells + ',' + String(total)
                                }else{
                                  mySells = String(total)
                                }
                                localStorage.setItem("sells",mySells);
                                console.log(localStorage.getItem("sells"))
                              },
                              error => {
                                this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                                });
                                this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')

                              });

                        }catch{
                          this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                          });
                          this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')
                        }



                      }
                      this.paymentDetail = '[{';
                      this.paymentDetailFinal = new paymentDetailDTO(1,1,this.calculateTotal()," ",new stateDTO(1));
                    })
                  }
                })
                });


      }else{
        
        this.checker = true;
        this.quickconfirm=false;
        this.showNotification('top', 'center', 3, "<h3> Intentaste agregar un producto con cantidad en <b>CERO</b> a la factura, para agregarlo al carrito, tiene que tener cantidad en 1</h3> ", 'danger');
      }
    }else{
      this.billDTO.id_store = this.locStorage.getIdStore();
      this.detailBillDTOList = [];
      this.commonStateDTO.state = 1;
      this.inventoryQuantityDTOList = [];
      let titleString= this.form.value['isRemission']?' Por Remision':'';

      if(disc==1){
        this.documentDTO.title = 'Movimiento De Venta';
      }
      if(disc==2){
        this.documentDTO.title = 'Movimiento De Compra';
      }
      if(disc==3){
        this.documentDTO.title = 'Movimiento De Entrada';
      }
      if(disc==4){
        this.documentDTO.title = 'Movimiento De Salida';
      }
      if(disc==5){
        this.documentDTO.title = 'Movimiento De Devolucion';
      }

      if(disc==1 || disc==2){
        this.documentDTO.body = data.observations||'';
      }

      if(disc==4 || disc==3){
        this.documentDTO.body = this.ObservationsInOrOut;
      }

      /**
       * building detailBill and Quantity
       */
      for (let code in this.productsObject) {
        let element = this.productsObject[code];

        this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null,null);
        this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO,null);

        //building detailBill
        this.detailBillDTO.id_storage = element.id_storage;
        this.detailBillDTO.price = element.price;
        this.detailBillDTO.tax = this.getPercentTax(element.tax_product)*100;
        this.detailBillDTO.id_product_third = element.id_product_third;
        this.detailBillDTO.tax_product = element.tax_product;
        this.detailBillDTO.state = this.commonStateDTO;
        this.detailBillDTO.quantity = Math.floor(element.quantity);

        if(element.quantity > 0) {
          this.detailBillDTOList.push(this.detailBillDTO);
        }else{
         this.checker = false;
        }


        // Inv Quantity
        this.inventoryQuantityDTO.id_inventory_detail = element.id_inventory_detail;
        this.inventoryQuantityDTO.id_product_third = element.id_product_third;
        this.inventoryQuantityDTO.code = element.code;
        this.inventoryQuantityDTO.id_storage = element.id_storage;

        // Inv Quantity for discount tienda
        this.inventoryQuantityDTO.quantity = Math.floor(element.quantity);

        this.inventoryQuantityDTOList.push(
            this.inventoryQuantityDTO
        );
      }

      if (this.detailBillDTOList.length > 0 && this.checker) {
        let ID_BILL_TYPE= this.form.value['isRemission']?107:1;
        this.billDTO.id_third_employee = this.token.id_third_father;
        this.billDTO.id_third = this.token.id_third;
        this.billDTO.id_bill_type = 1;
        //instanciar de acuerdo a por remision
        this.billDTO.id_bill_state = 1;
        this.billDTO.purchase_date = new Date();
        this.billDTO.subtotal = Math.floor(this.calculateSubtotal()* 100) / 100;
        this.billDTO.tax = Math.floor(this.calculateTax()* 100) / 100;
        this.billDTO.totalprice = Math.floor(this.calculateTotal()* 100) / 100;
        if(disc==3 || disc==4){
          this.billDTO.subtotal = 0;
          this.billDTO.tax = 0;
          this.billDTO.totalprice = 0;
        }
        this.billDTO.discount = 0;
        this.billDTO.documentDTO = this.documentDTO;
        this.billDTO.state = null;

        this.billDTO.details = this.detailBillDTOList;

        if(disc==1 || disc==2){

          if(data.paymentMethod == "efectivo"){
            this.paymentDetail+='"id_payment_method": 1, ';
            this.paymentDetail+='"aprobation_code": "", ';
            this.paymentDetailFinal.aprobation_code = "";
            this.paymentDetailFinal.id_payment_method = 1;
          }
          if(data.paymentMethod == "debito"){
            this.paymentDetail+='"id_payment_method": 2, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.aprobation_code = data.transactionCode;
            this.paymentDetailFinal.id_payment_method = 2;
          }
          if(data.paymentMethod == "credito"){
            this.paymentDetail+='"id_payment_method": 3, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.aprobation_code = data.transactionCode;
            this.paymentDetailFinal.id_payment_method = 3;
          }


          if(data.wayToPay == "contado"){
            this.paymentDetail+='"id_way_to_pay": 1, ';
            this.paymentDetailFinal.id_way_to_pay=1;
          }

          if(data.wayToPay == "credito"){
            this.paymentDetail+='"id_way_to_pay": 2, ';
            this.paymentDetailFinal.id_way_to_pay=2;
          }


          this.paymentDetailFinal.payment_value = this.calculateTotal();
          this.paymentDetailFinal.state = new stateDTO(1);
          this.paymentDetail+='"payment_value": '+this.calculateTotal()+', ';
          this.paymentDetail+='"state": {"state": 1}}]';

        }
        console.log(JSON.stringify(this.billDTO),"This is Bill DTO");
        this.billingService.postBillResource(this.billDTO, disc)
            .subscribe(
                result => {
                  this.http2.put("http://tienda724.com:8448/v1/billing/domiciliario?id_bill="+result+"&id_third="+this.selectedDomi,{}).subscribe(responseItem => {
                    console.log("THIS IS ITEM, "+responseItem);
                    this.selectedDomi = "-1";
                  
                  console.log("THIS IS RESULT", result);

                  if (result) {
                    this.http2.post(Urlbase[3]+"/billing/procedureup2?idbill="+result,{}).subscribe(resp => {
                      console.log("THIS IS MY RESP: ",result);
                      console.log("this is bull DTO",JSON.stringify(this.billDTO));
                      // alert(this.ID_INVENTORY_TEMP)
                      this.beginPlusOrDiscount(this.inventoryQuantityDTOList,disc);
                      console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE, ", JSON.parse(this.paymentDetail));
                      console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE2, ", this.paymentDetailFinal);


                      if(disc==1 || disc==2){

                        try{
                          console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE, ", JSON.parse(this.paymentDetail));
                          console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE 2, ", this.paymentDetailFinal);
                          this.billingService.postPaymentDetail([this.paymentDetailFinal],Number(result)).subscribe(answer =>{
                                this.paymentDetailFinal = new paymentDetailDTO(1,1,this.calculateTotal()," ",new stateDTO(1));
                                this.http2.get(Urlbase[3]+"/billing/UniversalPDF?id_bill="+result,{responseType: 'text'}).subscribe(response =>{
                                  //-----------------------------------------------------------------------------------------------------------------
                                  window.open(Urlbase[7]+"/"+response, "_blank");
                                  this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
                                  this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                                    this.http2.get(Urlbase[5]+"/close/alertForEmail?id_caja="+this.locStorage.getIdCaja()+"&id_third="+this.locStorage.getThird().id_third).subscribe(data => {
                                      //@ts-ignore
                                      console.log(data)
                                    })
                                  });
                                  setTimeout(() => {
                                  
                                    this.elementRef.nativeElement.focus();
                                    this.elementRef.nativeElement.select();
                                  
                                  }, 100);
                                  //-----------------------------------------------------------------------------------------------------------------
                                });
                               
                                this.paymentDetail = '[{';
                                var currentSells = localStorage.getItem("sells");
                                var mySells = '';
                                var total = this.calculateTotal();
                                if(currentSells !== 'null'){
                                  mySells = currentSells + ',' + String(total)
                                }else{
                                  mySells = String(total)
                                }
                                localStorage.setItem("sells",mySells);
                                console.log(localStorage.getItem("sells"))
                              },
                              error => {
                                this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                                });
                                this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')

                              }
                          );

                        }catch{
                          this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                          });
                          this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')
                        }



                      }
                      this.paymentDetail = '[{';

                      this.paymentDetailFinal = new paymentDetailDTO(1,1,this.calculateTotal()," ",new stateDTO(1));
                    })
                  }
                })
                },
                error => {
                  this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')

                });


      }else{
        
        this.checker = true;
        this.quickconfirm=false;
        this.showNotification('top', 'center', 3, "<h3> Intentaste agregar un producto con cantidad en <b>CERO</b> a la factura, para agregarlo al carrito, tiene que tener cantidad en 1</h3> ", 'danger');
      }
    }

  }


  save2(data,disc,isCompra) {
    this.getDomiName();
    if(this.locStorage.getIdStore() == 81){
      this.flag = "1"
    }
    let datos = "";
    if(data.paymentMethod == "efectivo"){
      datos = "Efectivo"
    }
    if(data.paymentMethod == "debito"){
      datos = "Débito"
    }
    if(data.paymentMethod == "credito"){
      datos = "Crédito"
    }
    console.log("this is id person: ",data.id_person);
    if(data.id_person){
      console.log("ENTRE");
      //@ts-ignore
      this.billDTO.id_third_destiny = data.id_person;

      this.detailBillDTOList = [];
      this.commonStateDTO.state = 1;
      this.inventoryQuantityDTOList = [];
      let titleString= this.form.value['isRemission']?' Por Remision':'';

      if(disc==1){
        this.documentDTO.title = 'Movimiento De Venta';
      }
      if(disc==2){
        this.documentDTO.title = 'Movimiento De Compra';
      }
      if(disc==3){
        this.documentDTO.title = 'Movimiento De Entrada';
      }
      if(disc==4){
        this.documentDTO.title = 'Movimiento De Salida';
      }
      if(disc==5){
        this.documentDTO.title = 'Movimiento De Devolucion';
      }

      if(disc==1 || disc==2){
        this.documentDTO.body = data.observations||'';
      }

      if(disc==4 || disc==3){
        this.documentDTO.body = this.ObservationsInOrOut;
      }

      /**
       * building detailBill and Quantity
       */
      for (let code in this.productsObject) {
        let element = this.productsObject[code];

        this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null,null);
        this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO,null);

        //building detailBill
        console.log;
        this.detailBillDTO.id_storage = element.id_storage;
        this.detailBillDTO.price = element.price;
        this.detailBillDTO.tax = this.getPercentTax(element.tax_product)*100;
        this.detailBillDTO.id_product_third = element.id_product_third;
        this.detailBillDTO.tax_product = element.tax_product;
        this.detailBillDTO.state = this.commonStateDTO;
        this.detailBillDTO.quantity = Math.floor(element.quantity);

        if(element.quantity > 0) {
          this.detailBillDTOList.push(this.detailBillDTO);
        }else{
          this.checker = false;
        }

        // Inv Quantity
        this.inventoryQuantityDTO.id_inventory_detail = element.id_inventory_detail;
        this.inventoryQuantityDTO.id_product_third = element.id_product_third;
        this.inventoryQuantityDTO.code = element.code;
        this.inventoryQuantityDTO.id_storage = element.id_storage;

        // Inv Quantity for discount tienda
        this.inventoryQuantityDTO.quantity = Math.floor(element.quantity);

        this.inventoryQuantityDTOList.push(
            this.inventoryQuantityDTO
        );
      }
      this.billDTO.id_store = this.locStorage.getIdStore();
      if (this.detailBillDTOList.length > 0 && this.checker) {
        let ID_BILL_TYPE= this.form.value['isRemission']?107:1;
        this.billDTO.id_third_employee = this.token.id_third_father;
        this.billDTO.id_third = this.token.id_third;
        this.billDTO.id_bill_type = 1;
        //instanciar de acuerdo a por remision
        this.billDTO.id_bill_state = 1;
        this.billDTO.purchase_date = new Date();
        this.billDTO.subtotal = this.roundnum(this.calculateSubtotal());
        this.billDTO.tax = this.roundnum(this.calculateTax());
        this.billDTO.totalprice = this.roundnum(this.calculateTotal());
        if(disc==3 || disc==4){
          this.billDTO.subtotal = 0;
          this.billDTO.tax = 0;
          this.billDTO.totalprice = 0;
        }
        this.billDTO.discount = 0;
        this.billDTO.documentDTO = this.documentDTO;
        this.billDTO.state = null;

        this.billDTO.details = this.detailBillDTOList;

        if(disc==1 || disc==2){

          if(data.paymentMethod == "efectivo"){
            this.paymentDetail+='"id_payment_method": 1, ';
            this.paymentDetail+='"aprobation_code": "", ';
            this.paymentDetailFinal.id_payment_method =1;
            this.paymentDetailFinal.aprobation_code = "";
          }
          if(data.paymentMethod == "debito"){
            this.paymentDetail+='"id_payment_method": 2, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.id_payment_method =2;
            this.paymentDetailFinal.aprobation_code = data.transactionCode;
          }
          if(data.paymentMethod == "credito"){
            this.paymentDetail+='"id_payment_method": 3, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.id_payment_method =3;
            this.paymentDetailFinal.aprobation_code = data.transactionCode;
          }


          if(data.wayToPay == "contado"){
            this.paymentDetail+='"id_way_to_pay": 1, ';
            this.paymentDetailFinal.id_way_to_pay=1;

          }

          if(data.wayToPay == "credito"){
            this.paymentDetail+='"id_way_to_pay": 2, ';
            this.paymentDetailFinal.id_way_to_pay=2;
          }


          this.paymentDetailFinal.payment_value=this.roundnum(this.calculateTotal());
          this.paymentDetailFinal.state = new stateDTO(1);
          this.paymentDetail+='"payment_value": '+this.roundnum(this.calculateTotal())+', ';
          this.paymentDetail+='"state": {"state": 1}}]';

        }
        console.log(JSON.stringify(this.billDTO),"This is Bill DTO");
        this.billingService.postBillResource(this.billDTO, disc)
            .subscribe(
                result => {


                  this.http2.put("http://tienda724.com:8448/v1/billing/domiciliario?id_bill="+result+"&id_third="+this.selectedDomi,{}).subscribe(responseItem => {
                    console.log("THIS IS ITEM, "+responseItem);
                    this.selectedDomi = "-1";
                               
                  console.log("THIS IS RESULT", result);

                  if (result) {
                    this.http2.post(Urlbase[3]+"/billing/procedureup2?idbill="+result,{}).subscribe(resp => {
                      console.log("THIS IS MY RESP: ",result);
                      // alert(this.ID_INVENTORY_TEMP)
                      this.beginPlusOrDiscount(this.inventoryQuantityDTOList,disc);

                      if(disc==1 || disc==2){

                        try{
                          console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE, ", JSON.parse(this.paymentDetail));
                          console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE 2, ", this.paymentDetailFinal);
                          this.billingService.postPaymentDetail([this.paymentDetailFinal],Number(result)).subscribe(answer =>{
                                this.paymentDetailFinal = new paymentDetailDTO(1,1,this.calculateTotal()," ",new stateDTO(1));
                                this.http2.get(Urlbase[3]+"/billing/UniversalPDF?id_bill="+result,{responseType: 'text'}).subscribe(response =>{
                                  //-----------------------------------------------------------------------------------------------------------------
                                  this.quickconfirm = false;
                                  //window.open(Urlbase[7]+"/"+response, "_blank");
                                  this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
                                  this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                                    this.http2.get(Urlbase[5]+"/close/alertForEmail?id_caja="+this.locStorage.getIdCaja()+"&id_third="+this.locStorage.getThird().id_third).subscribe(data => {
                                      //@ts-ignore
                                      console.log(data)
                                    })
                                  });
                                  setTimeout(() => {
                                  
                                    this.elementRef.nativeElement.focus();
                                    this.elementRef.nativeElement.select();
                                  
                                  }, 100);
                                  //-----------------------------------------------------------------------------------------------------------------
                                });
                               
                                this.paymentDetail = '[{';
                                var currentSells = localStorage.getItem("sells");
                                var mySells = '';
                                var total = this.calculateTotal();
                                if(currentSells !== 'null'){
                                  mySells = currentSells + ',' + String(total)
                                }else{
                                  mySells = String(total)
                                }
                                localStorage.setItem("sells",mySells);
                                console.log(localStorage.getItem("sells"))
                              },
                              error => {
                                this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                                });
                                this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')

                              });

                        }catch{
                          this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                          });
                          this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')
                        }



                      }

                      this.paymentDetailFinal = new paymentDetailDTO(1,1,this.calculateTotal()," ", new stateDTO(1));
                      this.paymentDetail = '[{';
                    });
                  }
                })
                });


      }else{
        this.checker = true;
        this.quickconfirm=false;
        this.showNotification('top', 'center', 3, "<h3> Intentaste agregar un producto con cantidad en <b>CERO</b> a la factura, para agregarlo al carrito, tiene que tener cantidad en 1</h3> ", 'danger');
      
      }
    }else{
      this.billDTO.id_store = this.locStorage.getIdStore();
      this.detailBillDTOList = [];
      this.commonStateDTO.state = 1;
      this.inventoryQuantityDTOList = [];
      let titleString= this.form.value['isRemission']?' Por Remision':'';

      if(disc==1){
        this.documentDTO.title = 'Movimiento De Venta';
      }
      if(disc==2){
        this.documentDTO.title = 'Movimiento De Compra';
      }
      if(disc==3){
        this.documentDTO.title = 'Movimiento De Entrada';
      }
      if(disc==4){
        this.documentDTO.title = 'Movimiento De Salida';
      }
      if(disc==5){
        this.documentDTO.title = 'Movimiento De Devolucion';
      }

      if(disc==1 || disc==2){
        this.documentDTO.body = data.observations||'';
      }

      if(disc==4 || disc==3){
        this.documentDTO.body = this.ObservationsInOrOut;
      }

      /**
       * building detailBill and Quantity
       */
      for (let code in this.productsObject) {
        let element = this.productsObject[code];

        this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null,null);
        this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO,null);

        //building detailBill
        this.detailBillDTO.id_storage = element.id_storage;
        this.detailBillDTO.price = element.price;
        this.detailBillDTO.tax = this.getPercentTax(element.tax_product)*100;
        this.detailBillDTO.id_product_third = element.id_product_third;
        this.detailBillDTO.tax_product = element.tax_product;
        this.detailBillDTO.state = this.commonStateDTO;
        this.detailBillDTO.quantity = Math.floor(element.quantity);

        if(element.quantity > 0) {
          this.detailBillDTOList.push(this.detailBillDTO);
        }else{
          this.checker = false;
        }


        // Inv Quantity
        this.inventoryQuantityDTO.id_inventory_detail = element.id_inventory_detail;
        this.inventoryQuantityDTO.id_product_third = element.id_product_third;
        this.inventoryQuantityDTO.code = element.code;
        this.inventoryQuantityDTO.id_storage = element.id_storage;

        // Inv Quantity for discount tienda
        this.inventoryQuantityDTO.quantity = Math.floor(element.quantity);

        this.inventoryQuantityDTOList.push(
            this.inventoryQuantityDTO
        );
      }

      if (this.detailBillDTOList.length > 0 && this.checker) {
        let ID_BILL_TYPE= this.form.value['isRemission']?107:1;
        this.billDTO.id_third_employee = this.token.id_third_father;
        this.billDTO.id_third = this.token.id_third;
        this.billDTO.id_bill_type = 1;
        //instanciar de acuerdo a por remision
        this.billDTO.id_bill_state = 1;
        this.billDTO.purchase_date = new Date();
        this.billDTO.subtotal = Math.floor(this.calculateSubtotal()* 100) / 100;
        this.billDTO.tax = Math.floor(this.calculateTax()* 100) / 100;
        this.billDTO.totalprice = Math.floor(this.calculateTotal()* 100) / 100;
        if(disc==3 || disc==4){
          this.billDTO.subtotal = 0;
          this.billDTO.tax = 0;
          this.billDTO.totalprice = 0;
        }
        this.billDTO.discount = 0;
        this.billDTO.documentDTO = this.documentDTO;
        this.billDTO.state = null;

        this.billDTO.details = this.detailBillDTOList;

        if(disc==1 || disc==2){

          if(data.paymentMethod == "efectivo"){
            this.paymentDetail+='"id_payment_method": 1, ';
            this.paymentDetail+='"aprobation_code": "", ';
            this.paymentDetailFinal.id_payment_method = 1;
            this.paymentDetailFinal.aprobation_code = "";
          }
          if(data.paymentMethod == "debito"){
            this.paymentDetail+='"id_payment_method": 2, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.id_payment_method = 2;
            this.paymentDetailFinal.aprobation_code = data.transactionCode
          }
          if(data.paymentMethod == "credito"){
            this.paymentDetail+='"id_payment_method": 3, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.id_payment_method = 3;
            this.paymentDetailFinal.aprobation_code = data.transactionCode
          }


          if(data.wayToPay == "contado"){
            this.paymentDetail+='"id_way_to_pay": 1, ';
            this.paymentDetailFinal.id_way_to_pay = 1;
          }

          if(data.wayToPay == "credito"){
            this.paymentDetail+='"id_way_to_pay": 2, ';
            this.paymentDetailFinal.id_way_to_pay = 2;
          }


          this.paymentDetailFinal.payment_value = this.calculateTotal();
          this.paymentDetailFinal.state = new stateDTO(1);
          this.paymentDetail+='"payment_value": '+this.calculateTotal()+', ';
          this.paymentDetail+='"state": {"state": 1}}]';

        }
        console.log(JSON.stringify(this.billDTO),"This is Bill DTO");
        this.billingService.postBillResource(this.billDTO, disc)
            .subscribe(
                result => {
                  this.http2.put("http://tienda724.com:8448/v1/billing/domiciliario?id_bill="+result+"&id_third="+this.selectedDomi,{}).subscribe(responseItem => {
                    console.log("THIS IS ITEM, "+responseItem);
                    this.selectedDomi = "-1";
                  
                  console.log("THIS IS RESULT", result);
                  if (result) {
                    this.http2.post(Urlbase[3]+"/billing/procedureup2?idbill="+result,{}).subscribe(resp => {
                      console.log("THIS IS MY RESP: ",result);
                      console.log("this is bull DTO",JSON.stringify(this.billDTO));
                      // alert(this.ID_INVENTORY_TEMP)
                      this.beginPlusOrDiscount(this.inventoryQuantityDTOList,disc);


                      if(disc==1 || disc==2){

                        try{
                          console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE, ", JSON.parse(this.paymentDetail));
                          console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE 2, ", this.paymentDetailFinal);
                          this.billingService.postPaymentDetail([this.paymentDetailFinal],Number(result)).subscribe(answer =>{
                                this.paymentDetailFinal = new paymentDetailDTO(1,1,this.calculateTotal()," ",new stateDTO(1));
                                this.http2.get(Urlbase[3]+"/billing/UniversalPDF?id_bill="+result,{responseType: 'text'}).subscribe(response =>{
                                  //-----------------------------------------------------------------------------------------------------------------
                                  this.quickconfirm = false;
                                  //window.open(Urlbase[7]+"/"+response, "_blank");
                                  this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
                                  this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                                    this.http2.get(Urlbase[5]+"/close/alertForEmail?id_caja="+this.locStorage.getIdCaja()+"&id_third="+this.locStorage.getThird().id_third).subscribe(data => {
                                      //@ts-ignore
                                      console.log(data)
                                      console.log()
                                    })
                                  });
                                  setTimeout(() => {
                                  
                                    this.elementRef.nativeElement.focus();
                                    this.elementRef.nativeElement.select();
                                  
                                  }, 100);
                                  //-----------------------------------------------------------------------------------------------------------------
                                });
                               
                                this.paymentDetail = '[{';
                                var currentSells = localStorage.getItem("sells");
                                var mySells = '';
                                var total = this.calculateTotal();
                                if(currentSells !== 'null'){
                                  mySells = currentSells + ',' + String(total)
                                }else{
                                  mySells = String(total)
                                }
                                localStorage.setItem("sells",mySells);
                                console.log(localStorage.getItem("sells"))
                              },
                              error => {
                                this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                                });
                                this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')

                              });

                        }catch{
                          this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                          });
                          this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')
                        }



                      }
                      this.paymentDetail = '[{';
                      this.paymentDetailFinal = new paymentDetailDTO(1,1,this.calculateTotal()," ", new stateDTO(1));
                    });
                  }
                })
                });


      }else{
        this.checker = true;
        this.quickconfirm=false;
        this.showNotification('top', 'center', 3, "<h3> Intentaste agregar un producto con cantidad en <b>CERO</b> a la factura, para agregarlo al carrito, tiene que tener cantidad en 1</h3> ", 'danger');
      
      }
    }

  }



  save3(data,disc,isCompra) {
    this.getDomiName();
    if(this.locStorage.getIdStore() == 81){
      this.flag = "1"
    }
    let datos = "";
    if(data.paymentMethod == "efectivo"){
      datos = "Efectivo"
    }
    if(data.paymentMethod == "debito"){
      datos = "Débito"
    }
    if(data.paymentMethod == "credito"){
      datos = "Crédito"
    }
    console.log("this is id person: ",data.id_person);
    if(data.id_person){
      console.log("ENTRE");
      //@ts-ignore
      this.billDTO.id_third_destiny = data.id_person;

      this.detailBillDTOList = [];
      this.commonStateDTO.state = 1;
      this.inventoryQuantityDTOList = [];
      let titleString= this.form.value['isRemission']?' Por Remision':'';

      if(disc==1){
        this.documentDTO.title = 'Movimiento De Venta';
      }
      if(disc==2){
        this.documentDTO.title = 'Movimiento De Compra';
      }
      if(disc==3){
        this.documentDTO.title = 'Movimiento De Entrada';
      }
      if(disc==4){
        this.documentDTO.title = 'Movimiento De Salida';
      }
      if(disc==5){
        this.documentDTO.title = 'Movimiento De Devolucion';
      }

      if(disc==1 || disc==2){
        this.documentDTO.body = data.observations||'';
      }

      if(disc==4 || disc==3){
        this.documentDTO.body = this.ObservationsInOrOut;
      }

      /**
       * building detailBill and Quantity
       */
      for (let code in this.productsObject) {
        let element = this.productsObject[code];

        this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null,null);
        this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO,null);

        //building detailBill
        console.log;
        this.detailBillDTO.id_storage = element.id_storage;
        this.detailBillDTO.price = element.price;
        this.detailBillDTO.tax = this.getPercentTax(element.tax_product)*100;
        this.detailBillDTO.id_product_third = element.id_product_third;
        this.detailBillDTO.tax_product = element.tax_product;
        this.detailBillDTO.state = this.commonStateDTO;
        this.detailBillDTO.quantity = Math.floor(element.quantity);

        if(element.quantity > 0) {
          this.detailBillDTOList.push(this.detailBillDTO);
        }else{
          this.checker = false;
        }

        // Inv Quantity
        this.inventoryQuantityDTO.id_inventory_detail = element.id_inventory_detail;
        this.inventoryQuantityDTO.id_product_third = element.id_product_third;
        this.inventoryQuantityDTO.code = element.code;
        this.inventoryQuantityDTO.id_storage = element.id_storage;

        // Inv Quantity for discount tienda
        this.inventoryQuantityDTO.quantity = Math.floor(element.quantity);

        this.inventoryQuantityDTOList.push(
            this.inventoryQuantityDTO
        );
      }
      this.billDTO.id_store = this.locStorage.getIdStore();
      if (this.detailBillDTOList.length > 0 && this.checker) {
        let ID_BILL_TYPE= this.form.value['isRemission']?107:1;
        this.billDTO.id_third_employee = this.token.id_third_father;
        this.billDTO.id_third = this.token.id_third;
        this.billDTO.id_bill_type = 1;
        //instanciar de acuerdo a por remision
        this.billDTO.id_bill_state = 1;
        this.billDTO.purchase_date = new Date();
        this.billDTO.subtotal = this.roundnum(this.calculateSubtotal());
        this.billDTO.tax = this.roundnum(this.calculateTax());
        this.billDTO.totalprice = this.roundnum(this.calculateTotal());
        if(disc==3 || disc==4){
          this.billDTO.subtotal = 0;
          this.billDTO.tax = 0;
          this.billDTO.totalprice = 0;
        }
        this.billDTO.discount = 0;
        this.billDTO.documentDTO = this.documentDTO;
        this.billDTO.state = null;

        this.billDTO.details = this.detailBillDTOList;

        if(disc==1 || disc==2){

          if(data.paymentMethod == "efectivo"){
            this.paymentDetail+='"id_payment_method": 1, ';
            this.paymentDetail+='"aprobation_code": "", ';
            this.paymentDetailFinal.id_payment_method =1;
            this.paymentDetailFinal.aprobation_code = "";
          }
          if(data.paymentMethod == "debito"){
            this.paymentDetail+='"id_payment_method": 2, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.id_payment_method =2;
            this.paymentDetailFinal.aprobation_code = data.transactionCode;
          }
          if(data.paymentMethod == "credito"){
            this.paymentDetail+='"id_payment_method": 3, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.id_payment_method =3;
            this.paymentDetailFinal.aprobation_code = data.transactionCode;
          }


          if(data.wayToPay == "contado"){
            this.paymentDetail+='"id_way_to_pay": 1, ';
            this.paymentDetailFinal.id_way_to_pay=1;

          }

          if(data.wayToPay == "credito"){
            this.paymentDetail+='"id_way_to_pay": 2, ';
            this.paymentDetailFinal.id_way_to_pay=2;
          }


          this.paymentDetailFinal.payment_value=this.roundnum(this.calculateTotal());
          this.paymentDetailFinal.state = new stateDTO(1);
          this.paymentDetail+='"payment_value": '+this.roundnum(this.calculateTotal())+', ';
          this.paymentDetail+='"state": {"state": 1}}]';

        }
        console.log(JSON.stringify(this.billDTO),"This is Bill DTO");
        this.billingService.postBillResource(this.billDTO, disc)
            .subscribe(
                result => {
                  this.http2.put("http://tienda724.com:8448/v1/billing/domiciliario?id_bill="+result+"&id_third="+this.selectedDomi,{}).subscribe(responseItem => {
                  console.log("THIS IS ITEM, "+responseItem);
                  this.selectedDomi = "-1";
                
                  console.log("THIS IS RESULT", result);
                  if (result) {
                    this.http2.post(Urlbase[3]+"/billing/procedureup2?idbill="+result,{}).subscribe(resp => {
                      console.log("THIS IS MY RESP: ",result);
                      // alert(this.ID_INVENTORY_TEMP)
                      this.beginPlusOrDiscount(this.inventoryQuantityDTOList,disc);

                      if(disc==1 || disc==2){

                        try{
                          console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE, ", JSON.parse(this.paymentDetail));
                          console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE 2, ", this.paymentDetailFinal);
                          this.billingService.postPaymentDetail([this.paymentDetailFinal],Number(result)).subscribe(answer =>{
                                this.paymentDetailFinal = new paymentDetailDTO(1,1,this.calculateTotal()," ",new stateDTO(1));
                                this.http2.get(Urlbase[3]+"/billing/UniversalPDF?id_bill="+result,{responseType: 'text'}).subscribe(response =>{
                                  //-----------------------------------------------------------------------------------------------------------------
                                  this.quickconfirm=false
                                  window.open(Urlbase[7]+"/"+response, "_blank");
                                  this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
                                  this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                                    this.http2.get(Urlbase[5]+"/close/alertForEmail?id_caja="+this.locStorage.getIdCaja()+"&id_third="+this.locStorage.getThird().id_third).subscribe(data => {
                                      //@ts-ignore
                                      console.log(data)
                                    })
                                  });
                                  setTimeout(() => {
                                  
                                    this.elementRef.nativeElement.focus();
                                    this.elementRef.nativeElement.select();
                                  
                                  }, 100);
                                  //-----------------------------------------------------------------------------------------------------------------
                                });
                                this.paymentDetail = '[{';
                                var currentSells = localStorage.getItem("sells");
                                var mySells = '';
                                var total = this.calculateTotal();
                                if(currentSells !== 'null'){
                                  mySells = currentSells + ',' + String(total)
                                }else{
                                  mySells = String(total)
                                }
                                localStorage.setItem("sells",mySells);
                                console.log(localStorage.getItem("sells"))
                              },
                              error => {
                                this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                                });
                                this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')

                              });

                        }catch{
                          this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                          });
                          this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')
                        }



                      }

                      this.paymentDetailFinal = new paymentDetailDTO(1,1,this.calculateTotal()," ", new stateDTO(1));
                      this.paymentDetail = '[{';
                    });
                  }
                })
                });


      }else{
        this.checker = true;
        this.quickconfirm=false;
        this.showNotification('top', 'center', 3, "<h3> Intentaste agregar un producto con cantidad en <b>CERO</b> a la factura, para agregarlo al carrito, tiene que tener cantidad en 1</h3> ", 'danger');
      
      }
    }else{
      this.billDTO.id_store = this.locStorage.getIdStore();
      this.detailBillDTOList = [];
      this.commonStateDTO.state = 1;
      this.inventoryQuantityDTOList = [];
      let titleString= this.form.value['isRemission']?' Por Remision':'';

      if(disc==1){
        this.documentDTO.title = 'Movimiento De Venta';
      }
      if(disc==2){
        this.documentDTO.title = 'Movimiento De Compra';
      }
      if(disc==3){
        this.documentDTO.title = 'Movimiento De Entrada';
      }
      if(disc==4){
        this.documentDTO.title = 'Movimiento De Salida';
      }
      if(disc==5){
        this.documentDTO.title = 'Movimiento De Devolucion';
      }

      if(disc==1 || disc==2){
        this.documentDTO.body = data.observations||'';
      }

      if(disc==4 || disc==3){
        this.documentDTO.body = this.ObservationsInOrOut;
      }

      /**
       * building detailBill and Quantity
       */
      for (let code in this.productsObject) {
        let element = this.productsObject[code];

        this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null,null);
        this.detailBillDTO = new DetailBillDTO(null, null, null, null, null, this.commonStateDTO,null);

        //building detailBill
        this.detailBillDTO.id_storage = element.id_storage;
        this.detailBillDTO.price = element.price;
        this.detailBillDTO.tax = this.getPercentTax(element.tax_product)*100;
        this.detailBillDTO.id_product_third = element.id_product_third;
        this.detailBillDTO.tax_product = element.tax_product;
        this.detailBillDTO.state = this.commonStateDTO;
        this.detailBillDTO.quantity = Math.floor(element.quantity);

        if(element.quantity > 0) {
          this.detailBillDTOList.push(this.detailBillDTO);
        }else{
          this.checker = false;
        }


        // Inv Quantity
        this.inventoryQuantityDTO.id_inventory_detail = element.id_inventory_detail;
        this.inventoryQuantityDTO.id_product_third = element.id_product_third;
        this.inventoryQuantityDTO.code = element.code;
        this.inventoryQuantityDTO.id_storage = element.id_storage;

        // Inv Quantity for discount tienda
        this.inventoryQuantityDTO.quantity = Math.floor(element.quantity);

        this.inventoryQuantityDTOList.push(
            this.inventoryQuantityDTO
        );
      }

      if (this.detailBillDTOList.length > 0 && this.checker) {
        let ID_BILL_TYPE= this.form.value['isRemission']?107:1;
        this.billDTO.id_third_employee = this.token.id_third_father;
        this.billDTO.id_third = this.token.id_third;
        this.billDTO.id_bill_type = 1;
        //instanciar de acuerdo a por remision
        this.billDTO.id_bill_state = 1;
        this.billDTO.purchase_date = new Date();
        this.billDTO.subtotal = Math.floor(this.calculateSubtotal()* 100) / 100;
        this.billDTO.tax = Math.floor(this.calculateTax()* 100) / 100;
        this.billDTO.totalprice = Math.floor(this.calculateTotal()* 100) / 100;
        if(disc==3 || disc==4){
          this.billDTO.subtotal = 0;
          this.billDTO.tax = 0;
          this.billDTO.totalprice = 0;
        }
        this.billDTO.discount = 0;
        this.billDTO.documentDTO = this.documentDTO;
        this.billDTO.state = null;

        this.billDTO.details = this.detailBillDTOList;

        if(disc==1 || disc==2){

          if(data.paymentMethod == "efectivo"){
            this.paymentDetail+='"id_payment_method": 1, ';
            this.paymentDetail+='"aprobation_code": "", ';
            this.paymentDetailFinal.id_payment_method = 1;
            this.paymentDetailFinal.aprobation_code = "";
          }
          if(data.paymentMethod == "debito"){
            this.paymentDetail+='"id_payment_method": 2, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.id_payment_method = 2;
            this.paymentDetailFinal.aprobation_code = data.transactionCode
          }
          if(data.paymentMethod == "credito"){
            this.paymentDetail+='"id_payment_method": 3, ';
            this.paymentDetail+='"aprobation_code": "'+data.transactionCode+'", ';
            // this.paymentDetail+='"id_bank_entity": '+Number(data.creditBank)+', ';
            this.paymentDetailFinal.id_payment_method = 3;
            this.paymentDetailFinal.aprobation_code = data.transactionCode
          }


          if(data.wayToPay == "contado"){
            this.paymentDetail+='"id_way_to_pay": 1, ';
            this.paymentDetailFinal.id_way_to_pay = 1;
          }

          if(data.wayToPay == "credito"){
            this.paymentDetail+='"id_way_to_pay": 2, ';
            this.paymentDetailFinal.id_way_to_pay = 2;
          }


          this.paymentDetailFinal.payment_value = this.calculateTotal();
          this.paymentDetailFinal.state = new stateDTO(1);
          this.paymentDetail+='"payment_value": '+this.calculateTotal()+', ';
          this.paymentDetail+='"state": {"state": 1}}]';

        }
        console.log(JSON.stringify(this.billDTO),"This is Bill DTO");
        this.billingService.postBillResource(this.billDTO, disc)
            .subscribe(
                result => {
                  this.http2.put("http://tienda724.com:8448/v1/billing/domiciliario?id_bill="+result+"&id_third="+this.selectedDomi,{}).subscribe(responseItem => {
                    console.log("THIS IS ITEM, "+responseItem);
                    this.selectedDomi = "-1";
                  
                  console.log("THIS IS RESULT", result);
                  if (result) {
                    this.http2.post(Urlbase[3]+"/billing/procedureup2?idbill="+result,{}).subscribe(resp => {
                      console.log("THIS IS MY RESP: ",result);
                      console.log("this is bull DTO",JSON.stringify(this.billDTO));
                      // alert(this.ID_INVENTORY_TEMP)
                      this.beginPlusOrDiscount(this.inventoryQuantityDTOList,disc);


                      if(disc==1 || disc==2){

                        try{
                          console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE, ", JSON.parse(this.paymentDetail));
                          console.log("THIS IS THE PAYMENT DETAIL ILL TRY TO EMULATE 2, ", this.paymentDetailFinal);
                          this.billingService.postPaymentDetail([this.paymentDetailFinal],Number(result)).subscribe(answer =>{
                                this.paymentDetailFinal = new paymentDetailDTO(1,1,this.calculateTotal()," ",new stateDTO(1));
                                this.http2.get(Urlbase[3]+"/billing/UniversalPDF?id_bill="+result,{responseType: 'text'}).subscribe(response =>{
                                  //-----------------------------------------------------------------------------------------------------------------
                                  this.quickconfirm=false
                                  window.open(Urlbase[7]+"/"+response, "_blank");
                                  this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
                                  this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                                    this.http2.get(Urlbase[5]+"/close/alertForEmail?id_caja="+this.locStorage.getIdCaja()+"&id_third="+this.locStorage.getThird().id_third).subscribe(data => {
                                      //@ts-ignore
                                      console.log(data)
                                    })
                                  });
                                  setTimeout(() => {
                                  
                                    this.elementRef.nativeElement.focus();
                                    this.elementRef.nativeElement.select();
                                  
                                  }, 100);
                                  //-----------------------------------------------------------------------------------------------------------------
                                });
                               
                                this.paymentDetail = '[{';
                                var currentSells = localStorage.getItem("sells");
                                var mySells = '';
                                var total = this.calculateTotal();
                                if(currentSells !== 'null'){
                                  mySells = currentSells + ',' + String(total)
                                }else{
                                  mySells = String(total)
                                }
                                localStorage.setItem("sells",mySells);
                                console.log(localStorage.getItem("sells"))
                              },
                              error => {
                                this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                                });
                                this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')

                              });

                        }catch{
                          this.http2.post(Urlbase[3]+"/billing/validateBill?idbill="+result,{}).subscribe(resp => {
                          });
                          this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')
                        }



                      }
                      this.paymentDetail = '[{';
                      this.paymentDetailFinal = new paymentDetailDTO(1,1,this.calculateTotal()," ", new stateDTO(1));
                    });
                  }
                })
                }
                );


      }else{ 
        this.checker = true;
        this.quickconfirm=false;
        this.showNotification('top', 'center', 3, "<h3> Intentaste agregar un producto con cantidad en <b>CERO</b> a la factura, para agregarlo al carrito, tiene que tener cantidad en 1</h3> ", 'danger');
         
      }
    }

  }

  beginPlusOrDiscount(inventoryQuantityDTOList,isDisc) {
    if(isDisc == 1 || isDisc == 4){



      // this.inventoriesService.putQuantity(id_Store,quantity,code,inventoryQuantityDTOList);


      try{
        inventoryQuantityDTOList.forEach(element =>{
          this.inventoriesService.putQuantity(this.locStorage.getIdStore(),element.quantity,element.code,inventoryQuantityDTOList,isDisc,element.id_storage).subscribe(result =>{
          });
        });
        this.cancel();
      }catch{
        this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')
      }



      // this.inventoriesService.putDiscountInventory(id_inventory, this.inventoryQuantityDTOList)
      // .subscribe(result => {
      //   if (result) {
      //     this.itemLoadBilling = null;
      //     this.detailBillingDTOList = [];
      //     this.form.reset();
      //     this.getInventoryList(this.STATE, null, null, this.ID_INVENTORY_TEMP)

      //     this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
      //     this.cancel();

      //   } else {
      //     this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'info')

      //   }
      // })
    }
    if(isDisc == 2 || isDisc == 3){


      try{
        inventoryQuantityDTOList.forEach(element =>{
          console.log(element);
          this.inventoriesService.putQuantity(this.locStorage.getIdStore(),((-1)*element.quantity),element.code,inventoryQuantityDTOList,isDisc,element.id_storage).subscribe(result =>{
          });
        });
        this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info');
        this.cancel();
      }catch{
        this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'danger')
      }

      //   this.inventoriesService.putPlusInventory(id_inventory, this.inventoryQuantityDTOList)
      // .subscribe(result => {
      //   if (result) {
      //     this.itemLoadBilling = null;
      //     this.detailBillingDTOList = [];
      //     this.form.reset();S

      //     this.getInventoryList(this.STATE, null, null, this.ID_INVENTORY_TEMP)

      //     this.showNotification('top', 'center', 3, "<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b></h3> ", 'info')
      //     this.cancel();

      //   } else {
      //     this.showNotification('top', 'center', 3, "<h3>El movimento presento <b>PROBLEMAS</b></h3> ", 'info')

      //   }
      // })

    }

  }
  isfull(){
    if(this.inventoryList.length==0){
      return true;
    }
    else{
      return false;
    }
  }

  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
      type: typeStr ? typeStr : type[id_type ? id_type : 2],
      timer: 200,
      placement: {
        from: from,
        align: align
      }
    });
  }

  individualDelete(code){

    this.removalArray.add(code);
    this.removeItems()

  }

  openReportes(): void {
    const dialogRef = this.dialog.open(ReportesComponent, {
      width: '40vw',
      height: '60vh',
      data: { name: this.name, animal: this.animal }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.animal = result;
    });
  }
  upQuantity(code){
    this.productsObject[code].quantity+=1;
  }

  downQuantity(code){
    if(1<this.productsObject[code].quantity){
      this.productsObject[code].quantity-=1;
    }
  }


}


export class pdfData{
  nit: String;
  regimen: String;
  medio: String;
  empresa: String;
  tienda: String;
  fecha: String;
  caja: String;
  consecutivo: String;
  detalles: [String[]];
  subtotal: number;
  tax: number;
  total: number;
  nombreCajero:String;
  cambio: number;
  direccion: String;
  telefono: String;
  cedula: String;
  cliente: String;
  direccionC: String;
  telefonoC: String;
  resolucion_DIAN:String;
  regimenT : String;
  prefix : String;
  initial_RANGE : String;
  final_RANGE : String;
  pdfSize: number;
  constructor(nit: String,
              regimen: String,
              medio: String,
              empresa: String,
              tienda: String,
              fecha: String,
              caja: String,
              consecutivo: String,
              detalles: [String[]],
              subtotal: number,
              tax: number,
              total: number,
              nombreCajero: string,
              cambio: number,
              direccion: String,
              telefono: String,
              cedula: String,
              cliente: String,
              direccionC: String,
              telefonoC: String,
              resolucion_DIAN:String,
              regimenT : String,
              prefix : String,
              initial_RANGE : String,
              final_RANGE : String,
              pdfSize:number){
    this.resolucion_DIAN = resolucion_DIAN;
    this.regimenT = regimenT;
    this.prefix = prefix;
    this.initial_RANGE = initial_RANGE;
    this.final_RANGE = final_RANGE;
    this.nombreCajero = nombreCajero;
    this.cambio = cambio;
    this.nit = nit;
    this.regimen = regimen;
    this.medio = medio;
    this.empresa = empresa;
    this.tienda = tienda;
    this.fecha = fecha;
    this.caja = caja;
    this.consecutivo = consecutivo;
    this.detalles = detalles;
    this.subtotal = subtotal;
    this.tax = tax;
    this.total = total;
    this.direccion = direccion;
    this.telefono = telefono;
    this.cedula = cedula;
    this.cliente = cliente;
    this.direccionC = direccionC;
    this.telefonoC = telefonoC;
    this.pdfSize = pdfSize;

  }
}




export class InventoryName{
  id_TAX: number;
  standard_PRICE: number;
  id_PRODUCT_STORE: number;
  product_STORE_NAME: string;
  code: string;
  id_INVENTORY_DETAIL: number;
  ownbarcode: string;
  product_STORE_CODE: string;
  constructor(ID_TAX: number,
              STANDARD_PRICE: number,
              ID_PRODUCT_STORE: number,
              PRODUCT_STORE_NAME: string,
              CODE: string,
              ID_INVENTORY_DETAIL: number,
              OWNBARCODE: string,
              PRODUCT_STORE_CODE: string){
    this.id_TAX = ID_TAX;
    this.standard_PRICE = STANDARD_PRICE;
    this.id_PRODUCT_STORE = ID_PRODUCT_STORE;
    this.product_STORE_NAME = PRODUCT_STORE_NAME;
    this.code = CODE;
    this.id_INVENTORY_DETAIL = ID_INVENTORY_DETAIL;
    this.ownbarcode = OWNBARCODE;
    this.product_STORE_CODE = PRODUCT_STORE_CODE;
  }
}
