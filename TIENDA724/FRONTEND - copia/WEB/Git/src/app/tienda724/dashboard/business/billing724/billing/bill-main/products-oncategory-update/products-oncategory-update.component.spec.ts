import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsOncategoryUpdateComponent } from './products-oncategory-update.component';

describe('ProductsOncategoryUpdateComponent', () => {
  let component: ProductsOncategoryUpdateComponent;
  let fixture: ComponentFixture<ProductsOncategoryUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsOncategoryUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsOncategoryUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
