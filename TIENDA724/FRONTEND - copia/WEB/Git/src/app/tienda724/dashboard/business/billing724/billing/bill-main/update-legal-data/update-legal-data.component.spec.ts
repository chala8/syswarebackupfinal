import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateLegalDataComponent } from './update-legal-data.component';

describe('UpdateLegalDataComponent', () => {
  let component: UpdateLegalDataComponent;
  let fixture: ComponentFixture<UpdateLegalDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateLegalDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateLegalDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
