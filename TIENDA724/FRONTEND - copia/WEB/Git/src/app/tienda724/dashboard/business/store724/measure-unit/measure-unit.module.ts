import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../../app.material';

/*
************************************************
*     principal component
*************************************************
*/
import { MeasureUnitListComponent } from './measure-unit-list/measure-unit-list.component';
import { MeasureUnitNewComponent } from './measure-unit-new/measure-unit-new.component';
import { MeasureUnitEditComponent } from './measure-unit-edit/measure-unit-edit.component';

/*
************************************************
*     modules of  your app
*************************************************
*/

//import {   } from "../products";

/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { MeasureUnitService } from './measure-unit.service'

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/




@NgModule({
  imports: [
    CommonModule,
    CommonModule,
    RouterModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MaterialModule,

  ],
  declarations: [MeasureUnitListComponent, MeasureUnitNewComponent, MeasureUnitEditComponent],
  providers:[MeasureUnitService]
})
export class MeasureUnitModule { }
