import { Component, OnInit } from '@angular/core';
import { LocalStorage } from 'app/shared/localStorage';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MatDialog } from '@angular/material';

@Component({
  selector: 'app-top-products',
  templateUrl: './top-products.component.html',
  styleUrls: ['./top-products.component.scss']
})
export class TopProductsComponent implements OnInit {

  constructor(private http2: HttpClient,
              public locStorage: LocalStorage,
              public dialogRef: MatDialogRef<TopProductsComponent>,
              public dialog: MatDialog) { }
  topProds = [];
  ngOnInit() { 

    this.http2.get("http://tienda724.com:8448/v1/billing/top20?idstore="+this.locStorage.getIdStore()).subscribe(res=> {
    for(let i=0;i<20;i++){
      this.topProds.push(res[i]);
      console.log(this.topProds)
    }
    

  })
  }
  setCode(code){
    this.locStorage.setCodigoBarras(code);
    this.dialogRef.close();
  }
  showProds(){
   console.log(this.topProds) 
  }

}
