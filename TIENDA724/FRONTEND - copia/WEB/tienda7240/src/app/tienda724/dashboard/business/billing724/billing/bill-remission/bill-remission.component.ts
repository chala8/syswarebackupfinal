import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// materials
import { MatTabChangeEvent, VERSION, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

/*
*     others component
*/
import { BillDocumentComponent } from '../bill-document/bill-document.component'


/*
*     models for  your component
*/
import { Token } from '../../../../../../shared/token'
import { InventoryDetail } from '../../../store724/inventories/models/inventoryDetail'
import { Inventory } from '../../../store724/inventories/models/inventory';
import { Third } from '../../../thirds724/third/models/third';
import { Document } from '../../commons/document'


// DTO
import { CommonStateDTO } from '../../commons/commonStateDTO'
import { DocumentDTO } from '../../commons/documentDTO'
import { BillDTO } from '../models/billDTO';
import { DetailBillDTO } from '../models/detailBillDTO';

// This Store724 
import { CommonStateStoreDTO } from '../../../store724/commons/CommonStateStoreDTO';
import { CommonStoreDTO } from '../../../store724/commons/CommonStoreDTO';
import { ProductThirdDTO } from '../../../store724/products-third/models/productThirdDTO';
import { InventoryDTO } from '../../../store724/inventories/models/inventoryDTO';
import { InventoryDetailDTO } from '../../../store724/inventories/models/inventoryDetailDTO';
import { InventoryDetailSimple } from '../../../store724/inventories/models/inventoryDetailSimple'

import { InventoryQuantityDTO } from '../../../store724/inventories/models/inventoryQuantityDTO';

/*
*     services of  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage'
import { InventoriesService } from '../../../store724/inventories/inventories.service';
import { BillingService } from '../billing.service';
import { ThirdService } from '../../../thirds724/third/third.service'
import { ProductThirdService } from '../../../store724/products-third/product-third.service';
import { delay } from 'q';


/*
*     constant of  your component
*/
declare var $: any;

@Component({
  selector: 'app-bill-remission',
  templateUrl: './bill-remission.component.html',
  styleUrls: ['./bill-remission.component.scss']
})
export class BillRemissionComponent implements OnInit {
  // Flags
  isExistVenue = false;

  // attribute
  CURRENT_ID_THIRD = 0;
  CURRENT_ID_THIRD_PATHER = 0;
  ID_INVENTORY_TEMP = 0;
  STATE = 1
  ID_BILL_TYPE = 0;
  TOTAL_PRICE = 0;

  TYPE_NAME: string;

  // models
  token: Token;
  form: FormGroup;
  inventoryQuantityDTO: InventoryQuantityDTO;
  inventoryQuantityDTO_origen: InventoryQuantityDTO;
  inventories: Inventory[];
  inventoriesTemp: Inventory[];
  documentDTO:DocumentDTO;
  

  //list
  itemLoadBilling: InventoryDetail;
  inventoryList: InventoryDetail[];

  thirdListList: Third[];
  thirdStores: Third[];
  thirdStoresVenue: Third[];
  thirdStoreVenueOrigen: Third[];
  thirdStoreVenueDestinity: Third[];
  inventoryDetailSimpleList: InventoryDetailSimple[];
  inventoryQuantityDTOOrigenList: InventoryQuantityDTO[];
  inventoryQuantityDTODestinityList: InventoryQuantityDTO[];


  //specials
  idsInventoryList: any[];
  idsInvWithDetails: any[];
  idsInventoryDic = new Array();
 
  
  // DTO's
  inventorieDTO: InventoryDTO;
  commonDTO: CommonStoreDTO;
  commonStateStoreDTO: CommonStateStoreDTO;
  detailBillingDTOList: any[]
  newProductThird: ProductThirdDTO;
  inventoryDetailDTO: InventoryDetailDTO;
  inventoryDetailDTOList: InventoryDetailDTO[];

  constructor(public locStorage: LocalStorage, private _router: Router,
    private fb: FormBuilder, public thirdService: ThirdService,
    public inventoriesService: InventoriesService, public productThirdService: ProductThirdService,
    private route: ActivatedRoute, public dialog: MatDialog) {

    this.detailBillingDTOList = []
    this.inventoryList = []
    this.createControls();
    this.logNameChange();

    this.thirdListList = [];
    this.thirdStores = [];
    this.thirdStoresVenue = [];
    this.thirdStoreVenueOrigen = [];
    this.thirdStoreVenueDestinity = [];
    this.idsInventoryList = []
    this.idsInvWithDetails = [];
    this.inventoriesTemp = [];
    this.inventoryDetailSimpleList = []
    this.inventoryDetailDTOList = []
    this.inventoryQuantityDTODestinityList = [];
    this.inventoryQuantityDTOOrigenList = [];

    this.commonDTO = new CommonStoreDTO(null, null);
    this.commonStateStoreDTO = new CommonStateStoreDTO(1, null, null)
    this.newProductThird = new ProductThirdDTO(null, null, null, null, null, this.commonStateStoreDTO, null, null, null, null)
    this.inventorieDTO = new InventoryDTO(null, this.commonDTO, this.commonStateStoreDTO);
    this.inventoryDetailDTO = new InventoryDetailDTO(null, null, null, null, this.commonStateStoreDTO)
    this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null,null)
    this.inventoryQuantityDTO_origen== new InventoryQuantityDTO(null, null, null,null,null)
    this.documentDTO= new DocumentDTO(null,null);





  }

  ngOnInit() {

    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD = this.token.id_third;
      this.CURRENT_ID_THIRD_PATHER = this.token.id_third_father;

      if (this.CURRENT_ID_THIRD !== null && this.CURRENT_ID_THIRD > 0) {

        this.route.queryParams
          .subscribe(params => {
            // get id _bill_type
            this.ID_BILL_TYPE = +params['type'] || 0;
          });

        //this.getInventoryList(this.STATE, null, null, null, null, null, this.CURRENT_ID_THIRD_PATHER)
        this.getThird(true, this.STATE, null, this.CURRENT_ID_THIRD_PATHER)
      }
    }

  }


  //There is to receive DETAILS
  getDetails(event) {
    this.detailBillingDTOList = event;
    console.log("Nueva <lista ", this.detailBillingDTOList)
  }


  showType(event) {

    this.TYPE_NAME = event.type;
    this.ID_INVENTORY_TEMP = event.inventory;
    this.getInventoryList(this.STATE, null, null, this.ID_INVENTORY_TEMP)


  }

  changeVenuePair() {

    let orig = this.form.value['origen'];
    let destinity = this.form.value['destinity'];
    let is_orig = this.form.value['is_principal_origen'];
    let is_destinity = this.form.value['is_principal_destinity'];
    if (is_orig) {

      this.form.patchValue({
        is_principal_origen: false,
        is_principal_destinity: true,

      });

    } else if (is_destinity) {
      this.form.patchValue({
        is_principal_origen: true,
        is_principal_destinity: false,
        destinity: orig
      });
    } else {
      this.form.patchValue({
        origen: destinity,
        destinity: orig
      });

    }


  }

  //The user auth is not valid 
  Login() {
    let link = ['/auth'];
    this._router.navigate(link);
  }
  volver() {
    let link = ['/dashboard/business/movement/options'];
    this._router.navigate(link);
  }

  removeVenue(venue?, is_origen?) {
    if (venue) {
      if (is_origen) {
        this.thirdStoreVenueDestinity = this.thirdStoresVenue.filter(
          element => element.id_third !== venue.id_third);
      } else {
        this.thirdStoreVenueOrigen = this.thirdStoresVenue.filter(
          element => element.id_third !== venue.id_third);
      }
    } else {
      if (is_origen) {
        this.thirdStoreVenueOrigen = this.thirdStoresVenue;
      } else {
        this.thirdStoreVenueDestinity = this.thirdStoresVenue;
      }


    }
  }



  // start controls
  createControls() {
    this.form = this.fb.group({
      origen: ['', Validators.compose([
        Validators.required
      ])],
      destinity: ['', Validators.compose([
        Validators.required
      ])],
      is_principal_origen: [true, Validators.compose([

      ])],
      is_principal_destinity: [false, Validators.compose([

      ])],
      codeProd: ['', Validators.compose([
        Validators.required
      ])],
      title: ['', Validators.compose([
        Validators.required
      ])],
      body: ['', Validators.compose([
        Validators.required
      ])],
    });
  }

  loadData() {
    this.form.patchValue({
      codeProd: ''
    });
  }

  logNameChange() {
    const codeProdControl = this.form.get('codeProd');
    codeProdControl.valueChanges.forEach((value: string) => {
      this.itemLoadBilling = codeProdControl['value'];


      if (this.itemLoadBilling) { // IF Active all
        console.log(" NUEVO PRODUCT ", this.itemLoadBilling);
        // call function  loadDatailBillin()
        this.loadDatailBilling(this.itemLoadBilling)
      }
    });

    const is_principal_destinityControl = this.form.get('is_principal_destinity');
    is_principal_destinityControl.valueChanges.forEach((value: boolean) => {
      if (value) {
        this.form.patchValue({
          is_principal_origen: false,
          destinity: ''
        });
        this.removeVenue(null, true)
      } else {

      }

    });

    const is_principal_origenControl = this.form.get('is_principal_origen');
    is_principal_origenControl.valueChanges.forEach((value: boolean) => {
      this.detailBillingDTOList = []
      if (value) {
        this.form.patchValue({
          is_principal_destinity: false,
          origen: ''
        });
        this.removeVenue(null, false)
      } else {

      }
    });
    // Select an venue
    const destinityControl = this.form.get('destinity');
    destinityControl.valueChanges.forEach((value: string) => {
      if (value) {

        console.log("SEDE DESTINO,->", value)
        //Eliminar esta sede de la lista origen
        this.removeVenue(value, false)
        // Cargar el inventario de la sede seleccionada
        this.loadInventoryVenue(value)
      } else {

      }

    });

    const origenControl = this.form.get('origen');
    origenControl.valueChanges.forEach((value: boolean) => {
      if (value) {
        console.log("SEDE ORIGEN,->", value)
        this.detailBillingDTOList = []
        //Eliminar esta sede de la lista destino
        this.removeVenue(value, true)
        // Cargar el inventario de la sede seleccionada
        this.loadInventoryVenue(value)
      } else {

      }
    });
  }

  loadDatailBilling(element) {
    let flag = false;
    //element.detail.quantity=1;
    if (element) {
      if (element.detail.quantity > 0) {

        if (this.detailBillingDTOList.length > 0) {

          for (let i = 0; i < this.detailBillingDTOList.length; i++) {


            if (this.detailBillingDTOList[i].item.detail.id_inventory_detail === element.detail.id_inventory_detail) {
              flag = true;
            }
          }

          if (!flag) {
            this.TOTAL_PRICE = this.TOTAL_PRICE + (+element.description.standard_price)
            this.detailBillingDTOList.push(
              {
                'is_exit': 2, 'OldQuantity': element.detail.quantity, 'quantity': 1,
                'item': {
                  "detail": {
                    "id_inventory_detail": element.detail.id_inventory_detail
                  },
                  "id_code": element.description.id_code,
                  "id_product_third": element.description.id_prod_third,
                  'product': {
                    "name_product": element.product.name_product,
                    "min_price": element.description.min_price,
                    "standard_price": element.description.standard_price
                  }
                }
              }
            );
          }

        } else {
          this.TOTAL_PRICE = this.TOTAL_PRICE + (+element.description.standard_price)
          this.detailBillingDTOList.push(
            {
              'is_exit': 2, 'OldQuantity': element.detail.quantity, 'quantity': 1,
              'item': {
                "detail": {
                  "id_inventory_detail": element.detail.id_inventory_detail
                },
                "id_code": element.description.id_code,
                "id_product_third": element.description.id_prod_third,
                'product': {
                  "name_product": element.product.name_product,
                  "min_price": element.description.min_price,
                  "standard_price": element.description.standard_price
                }
              }
            }
          );
        }

      } else {
        this.showNotification('top', 'center', 3, "<h3>El Producto esta agotado</h3> ", 'warning')
      }

      this.loadData();


    }
  }// end controls

  

  // Notifications
  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: typeStr ? typeStr : type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }
  falg = false;
  endPlus=false;
  endDiscount=false;
  save() {
    console.log("BEGIN SAVE");
    let detailBillingDTOListCase_3: any[] = []
    let ID_TH_DESTINITY_TEMP=0;
    let inv_destinity=0
    let inv_origen=0



    let invDet: InventoryDetail[] = [];
    if (!this.form.value['is_principal_destinity']) {

      console.log("TIENE INV", this.idsInventoryDic[this.form.value['destinity'].id_third])
      if (this.form.value['destinity'] == null || this.form.value['destinity'].id_third < 1) {
        this.showNotification('top', 'center', 1, "<h3> <b>Debe seleccionar un destino</b> </h3> ", "warning")
        return;
      }
      // Ask for the inv of an any venue
      if ((this.idsInventoryDic[this.form.value['destinity'].id_third] == null) || this.idsInventoryDic[this.form.value['destinity'].id_third].length < 1) {
        console.log("BEGIN CREATE INV DEST CH");
        this.falg = false;
        //this.createInveForVenue(+this.form.value['destinity'].id_third);

        for (let i = 0; i < this.detailBillingDTOList.length; i++) {
          console.log("CASO 3: \n \t Si NO EXISTE \n ", this.detailBillingDTOList[i]);
          
          // fill list for create product third to destinity
          detailBillingDTOListCase_3.push(this.detailBillingDTOList[i])
          ID_TH_DESTINITY_TEMP=this.form.value['destinity'].id_third

          // create a model for do discount to origen
          this.inventoryQuantityDTO_origen = new InventoryQuantityDTO(null, null, null, null, null);              
          this.inventoryQuantityDTO_origen = new InventoryQuantityDTO(null, null, null, null, null);
          this.inventoryQuantityDTO_origen.code = this.detailBillingDTOList[i].item.id_code
          this.inventoryQuantityDTO_origen.id_inventory_detail = this.detailBillingDTOList[i].item.detail.id_inventory_detail
          this.inventoryQuantityDTO_origen.quantity = this.detailBillingDTOList[i].quantity

          // list for origen
          this.inventoryQuantityDTOOrigenList.push(this.inventoryQuantityDTO_origen)
        }


      } else {

        // You must verify the id_code of this inv, for get either id_prod_third, id_detail_inv.        
        
          console.log("CASO 1: \n \t Revisar los códigos si existen en el inventario destino");
          for (let i = 0; i < this.detailBillingDTOList.length; i++) {
            let swap: any = null;
            for (let element of this.idsInventoryDic[this.form.value['destinity'].id_third]) {

              console.log("ele: ", element);
              console.log("Obj: ", this.detailBillingDTOList[i]);
              // this in the list of destinity
              if (this.detailBillingDTOList[i].item.id_code === element.description.id_code) {
                swap = element
                
                console.log("CASO 2: \n \t Si existen obtener el id_inv_det_det y el id_prod_th \n");

              }

            }
            if (swap) {
              console.log("CASO 2.1: \n \t Guardar en una Lista QualityDTO \n ", swap);
              // create a model for do plus to destinity 
              this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null, null);
              this.inventoryQuantityDTO.code = this.detailBillingDTOList[i].item.id_code
              this.inventoryQuantityDTO.id_inventory_detail = swap.detail.id_inventory_detail
              this.inventoryQuantityDTO.quantity = this.detailBillingDTOList[i].quantity
              console.log("inventoryQuantityDTO DEST \n ", this.inventoryQuantityDTO);
              // list for destinity
              this.inventoryQuantityDTODestinityList.push(this.inventoryQuantityDTO)

              // create a model for do discount to origen
              this.inventoryQuantityDTO_origen = new InventoryQuantityDTO(null, null, null, null, null);
              this.inventoryQuantityDTO_origen.code = this.detailBillingDTOList[i].item.id_code
              this.inventoryQuantityDTO_origen.quantity = this.detailBillingDTOList[i].quantity
              this.inventoryQuantityDTO_origen.id_inventory_detail = this.detailBillingDTOList[i].item.detail.id_inventory_detail
              console.log("inventoryQuantityDTO ORG \n ", this.inventoryQuantityDTO_origen);
              // list for origen
              this.inventoryQuantityDTOOrigenList.push(this.inventoryQuantityDTO_origen)

            } else {
              console.log("CASO 3: \n \t Si NO EXISTE \n ", this.detailBillingDTOList[i]);

              // fill list for create product third to destinity
              detailBillingDTOListCase_3.push(this.detailBillingDTOList[i])
              ID_TH_DESTINITY_TEMP=this.form.value['destinity'].id_third

              // create a model for do discount to origen
              this.inventoryQuantityDTO_origen = new InventoryQuantityDTO(null, null, null, null, null);              
              this.inventoryQuantityDTO_origen = new InventoryQuantityDTO(null, null, null, null, null);
              this.inventoryQuantityDTO_origen.code = this.detailBillingDTOList[i].item.id_code
              this.inventoryQuantityDTO_origen.id_inventory_detail = this.detailBillingDTOList[i].item.detail.id_inventory_detail
              this.inventoryQuantityDTO_origen.quantity = this.detailBillingDTOList[i].quantity

              // list for origen
              this.inventoryQuantityDTOOrigenList.push(this.inventoryQuantityDTO_origen)

            }
          }
        

      }

    } else {
      // Ask for the inv of principal venue
      if ((this.idsInventoryDic[this.CURRENT_ID_THIRD_PATHER] == null) || this.idsInventoryDic[this.CURRENT_ID_THIRD_PATHER].length < 1) {
         console.log("BEGIN CREATE INV DEST FA");
         this.createInveForVenue(this.CURRENT_ID_THIRD_PATHER);
      }else{
        // built list for plus destinity (principal)
        console.log("CASO 1: \n \t Revisar los códigos si existen en el inventario destino");
        for (let i = 0; i < this.detailBillingDTOList.length; i++) {
          let swap: any = null;
          for (let element of this.idsInventoryDic[this.CURRENT_ID_THIRD_PATHER]) {

            console.log("ele: ", element);
            console.log("Obj: ", this.detailBillingDTOList[i]);
            // this in the list of destinity
            if (this.detailBillingDTOList[i].item.id_code === element.description.id_code) {
              swap = element
              
              console.log("CASO 2: \n \t Si existen obtener el id_inv_det_det y el id_prod_th \n");

            }

          }
          if (swap) {
            console.log("CASO 2.1: \n \t Guardar en una Lista QualityDTO \n ", swap);
            // create a model for do plus to destinity 
            this.inventoryQuantityDTO = new InventoryQuantityDTO(null, null, null, null, null);
            this.inventoryQuantityDTO.code = this.detailBillingDTOList[i].item.id_code
            this.inventoryQuantityDTO.id_inventory_detail = swap.detail.id_inventory_detail
            this.inventoryQuantityDTO.quantity = this.detailBillingDTOList[i].quantity
            console.log("inventoryQuantityDTO DEST \n ", this.inventoryQuantityDTO);
            // list for destinity
            this.inventoryQuantityDTODestinityList.push(this.inventoryQuantityDTO)
          }else{
            // fill list for create product third to destinity
            detailBillingDTOListCase_3.push(this.detailBillingDTOList[i])
            ID_TH_DESTINITY_TEMP=this.CURRENT_ID_THIRD_PATHER
            
          }

           // create a model for do discount to origen
           this.inventoryQuantityDTO_origen = new InventoryQuantityDTO(null, null, null, null, null);
           this.inventoryQuantityDTO_origen.code = this.detailBillingDTOList[i].item.id_code
           this.inventoryQuantityDTO_origen.quantity = this.detailBillingDTOList[i].quantity
           this.inventoryQuantityDTO_origen.id_inventory_detail = this.detailBillingDTOList[i].item.detail.id_inventory_detail
           console.log("inventoryQuantityDTO ORG \n ", this.inventoryQuantityDTO_origen);
           // list for origen
           this.inventoryQuantityDTOOrigenList.push(this.inventoryQuantityDTO_origen)
        
        }
      }
      //built list for for discount origen
      
    }
    if(this.inventoryQuantityDTODestinityList.length>0){
      let ob_1
      if (!this.form.value['is_principal_destinity']){
        ob_1=this.idsInventoryDic[this.form.value['destinity'].id_third]
        inv_destinity=ob_1[0].detail.id_inventory

      }else{
        ob_1=this.idsInventoryDic[this.CURRENT_ID_THIRD_PATHER]
      }
      inv_destinity=ob_1[0].detail.id_inventory      
      console.log("ENVAR DESTINO  \n",inv_destinity);
      console.log("LISTAS PARA ENVIAR DEST ", this.inventoryQuantityDTODestinityList)
      this.beginPlus(inv_destinity,this.inventoryQuantityDTODestinityList,true,detailBillingDTOListCase_3.length>0)
    }else{
      if(this.inventoryQuantityDTOOrigenList.length>0){
        let ob_2
        if (!this.form.value['is_principal_origen']){
          ob_2=this.idsInventoryDic[this.form.value['origen'].id_third]
         
        }else{
          ob_2=this.idsInventoryDic[this.CURRENT_ID_THIRD_PATHER]
          
        }
        inv_origen=ob_2[0].detail.id_inventory
        console.log("ENVAR ORIGEN \n",inv_origen);
        console.log("LISTAS PARA ENVIAR ORIGEN ", this.inventoryQuantityDTOOrigenList)
        this.beginDiscount(inv_origen,this.inventoryQuantityDTOOrigenList,true,detailBillingDTOListCase_3.length>0)
  
      }

    }



  
    
    if (detailBillingDTOListCase_3.length > 0) {
      // Registre as product third
      console.log("LISTAS PARA CREAR PROD_TH FALTANTE \n ", detailBillingDTOListCase_3)
      this.beginProductThirdForVenue(ID_TH_DESTINITY_TEMP,detailBillingDTOListCase_3)
    }
  }
  beginProductThirdForVenue(id_third?: number,detailBillingDTOListCase_3?) {
        for (let element of detailBillingDTOListCase_3) {
          console.log("BEGIN DATA", id_third, element['item'].id_code, element['item'].product.min_price, element['item'].product.standard_price, element['quantity'])
          this.create_product_third(id_third, element['item'].id_code, element['item'].product.min_price, element['item'].product.standard_price, element['quantity'])
          delay(this,500)
        }
      }

  createInveForVenue(id_third?: number) {

    for (let element of this.detailBillingDTOList) {
      console.log("BEGIN DATA", id_third, element['item'].id_code, element['item'].product.min_price, element['item'].product.standard_price, element['quantity'])
      this.create_product_third(id_third, element['item'].id_code, element['item'].product.min_price, element['item'].product.standard_price, element['quantity'])
    }
  }

  // services
  getInventoryList(state_inv_detail?: number, state_product?: number, id_inventory_detail?: number,
    id_inventory?: number, id_product_third?: number, location?: number,
    id_third?: number, id_category_third?: number, quantity?: number, id_state_inv_detail?: number,
    id_product?: number, id_category?: number, stock?: number,
    stock_min?: number, img_url?: string, id_tax?: number,
    id_common_product?: number, name_product?: string,
    description_product?: string, id_state_product?: number,


    id_state_prod_third?: number, state_prod_third?: number,
    id_measure_unit?: number, id_measure_unit_father?: number,
    id_common_measure_unit?: number, name_measure_unit?: string,
    description_measure_unit?: string, id_state_measure_unit?: number,
    state_measure_unit?: number, id_code?: number,
    code?: number, img?: string,
    id_attribute_list?: number,
    id_state_cod?: number, state_cod?: number,
    attribute?: number,
    attribute_value?: number) {

    if ((id_inventory === this.ID_INVENTORY_TEMP) && (this.inventoryList.length > 0)) {

      return;
    }

    let detailListTemp: any[];

    this.inventoriesService.getInventoriesDetailList(state_inv_detail, state_product, id_inventory_detail,
      id_inventory, quantity, id_state_inv_detail,
      id_product, id_category, stock,
      stock_min, img_url, id_tax,
      id_common_product, name_product,
      description_product, id_state_product,
      id_product_third, location,
      id_third, id_category_third,

      id_state_prod_third, state_prod_third,
      id_measure_unit, id_measure_unit_father,
      id_common_measure_unit, name_measure_unit,
      description_measure_unit, id_state_measure_unit,
      state_measure_unit, id_code,
      code, img,
      id_state_cod, state_cod)
      .subscribe((data: InventoryDetail[]) => detailListTemp = data,
      error => console.log(error),
      () => {
        if (detailListTemp.length > 0) {


          if (id_inventory === this.ID_INVENTORY_TEMP) {
            this.inventoryList = detailListTemp;
            this.idsInventoryDic[this.CURRENT_ID_THIRD_PATHER] = detailListTemp;
            if (this.falg) {
              this.falg = false;
              // begin analysis
              this.getDetailInventoryById(null, null, id_third)
            }
          } else {
            this.idsInventoryDic[id_third] = detailListTemp;
            if (this.falg) {
              this.falg = false;
              // begin analysis
              this.getDetailInventoryById(null, null, id_third)
            }
          }
        }
      });
  }

  getThird(is_venue: boolean, state?: number, id_third?: number, id_third_father?: number, document_type?: number, document_number?: string, id_doctype_person?: number, doc_person?: string, id_third_type?: number, id_person?: number): void {

    this.thirdService.getThirdList(id_third, id_third_father, document_type, document_number,
      id_doctype_person, doc_person, id_third_type, state, id_person)
      .subscribe((data: Third[]) => this.thirdListList = data,
      error => console.log(error),
      () => {
        if (is_venue && this.thirdListList.length > 0) {
          this.prosessingDataThird(is_venue);
        }

      });
  }
  prosessingDataThird(is_venue?: boolean) {
    let flag = true;
    if (is_venue) {
      for (let element of this.thirdListList) {
        flag = true;
        if ((element.profile === null || element.profile === 0)) {
          //  
          if (this.thirdStoresVenue.length > 0) {
            for (let objet of this.thirdStoresVenue) {
              if (element.id_third !== objet.id_third) {
                flag = false;
              }
            }
            if (!flag) {
              this.thirdStoresVenue.push(element);
            }
          } else {
            this.thirdStoresVenue.push(element);
          }
        }
      }
      if (this.thirdStoresVenue.length > 0) {
        this.isExistVenue = true;
        this.thirdStoreVenueDestinity = this.thirdStoresVenue;
        this.thirdStoreVenueOrigen = this.thirdStoresVenue;
        
      } else {
        this.isExistVenue = false;
        this.showNotification('top','center',1," <h1>  NO TIENE SEDES  </h1> ",'danger')
      }
    } else {

    }

    
  }

  loadInventoryVenue(origen) {
    if (origen && !this.idsInventoryDic[origen.id_third]) {
      let inv: Inventory[] = [];
      this.inventoriesService.getInventoriesList(this.STATE, null, origen.id_third)
        .subscribe((response: Inventory[]) => inv = response,
        error => console.log(error),
        () => {
          if (inv.length > 0) {

            this.getInventoryList(this.STATE, null, null, inv[0].id_inventory, null,
              null, origen.id_third);
          } else {
            this.idsInventoryDic[origen.id_third] = [];
            this.showNotInvOrDetails();
          }
        }
        );

      // Importante, Como obtner la longitud del diccionario en JAVASCRIPT
      console.log("LONG ", Object.keys(this.idsInventoryDic).length)

      this.idsInventoryDic.forEach(function (valor, indice, array) {
        console.log("En el índice " + indice + " hay este valor: ", valor);
      });
    } else {

    }

  }

  create_product_third(id_third?: number, id_code?: null, min_price?: number, standard_price?: number, quantity?: number) {

    //
    this.commonStateStoreDTO.state = 1;
    this.newProductThird.min_price = min_price ? min_price : 0.0;
    this.newProductThird.standard_price = standard_price ? standard_price : 0.0;
    this.newProductThird.location = "sin definir";
    this.newProductThird.state = this.commonStateStoreDTO;
    this.newProductThird.id_third = id_third;

    console.log("PROD TH -> ", this.newProductThird)
    alert("id_third " + id_third)

    this.productThirdService.postCategory(this.newProductThird, id_code)
      .subscribe(
      result => {

        if (result > 0) {
          // this.resetForm();
          // this.goBack();
          this.getInventoriesResource(this.STATE, null, id_third, this.newProductThird, result, quantity);

          return;
        } else {

          //this.openDialog();
          return;
        }
      })
  }
  getInventoriesResource(state?: number, id_inventory?: number, id_third?: number,
    newProductThird?: ProductThirdDTO, id_prod_third?: number, quantity?: number) {
    alert("id_third 2" + id_third)

    this.inventoriesService.getInventoriesList(state, id_inventory, id_third)
      .subscribe((data: Inventory[]) => this.inventories = data,
      error => console.log(error),
      () => {
        if (this.inventories.length > 0) {
          let id_inv = this.inventories[0].id_inventory
          if (id_inv > 0) {
            this.managementDetailInventory(id_inv, newProductThird, id_prod_third, quantity, id_third)
          } else {
            this.createInventory(newProductThird, id_prod_third, quantity, id_third)
          }
        } else {
          this.createInventory(newProductThird, id_prod_third, quantity, id_third)
        }
      });
  }
 
  createInventory(newProductThird?: ProductThirdDTO, id_prod_third?: number, quantity?: number, id_third?: number) {
    alert("id_third 3" + id_third)
    this.commonDTO.name = "Inventario 1"
    this.commonDTO.name = "Inventario para contabilidad de su establecimiento"
    this.commonStateStoreDTO.state = 1

    this.inventorieDTO.id_third = id_third
    this.inventorieDTO.common = this.commonDTO
    this.inventorieDTO.state = this.commonStateStoreDTO

    this.inventoryDetailDTO.quantity = quantity ? quantity : 0;
    this.inventoryDetailDTO.id_product_third = id_prod_third
    this.inventoryDetailDTO.state = this.commonStateStoreDTO
    this.inventoryDetailDTOList.push(this.inventoryDetailDTO)

    this.inventorieDTO.details = this.inventoryDetailDTOList
    console.log("INVENTORY ", this.inventorieDTO)
    this.inventoriesService.postInventory(this.inventorieDTO)
      .subscribe(
      result => {
        // call create billing
        if (result > 0) {
          this.falg = true
          this.getInventoryList(null, null, null, result, null, null, id_third)
          this.showNotification('top', 'center', 1, "<h3> <b>PROCESO INTERNO DE LA SEDE</b> </h3> <br> <h2>CORRECTO!</h2>", "info")

        }
      })
  }

  managementDetailInventory(id_inventory: number, newProductThird?: ProductThirdDTO, id_prod_third?: number, quantity?: number, id_third?: number) {

    this.commonDTO.name = "Inventario 1"
    this.commonDTO.name = "Inventario para contabilidad de su establecimiento"
    this.commonStateStoreDTO.state = 1

    this.inventoryDetailDTO.quantity = quantity
    this.inventoryDetailDTO.id_product_third = id_prod_third
    this.inventoryDetailDTO.state = this.commonStateStoreDTO
    this.inventoryDetailDTOList.push(this.inventoryDetailDTO)

    this.inventoriesService.postInventoryDetail(id_inventory, this.inventoryDetailDTOList)
      .subscribe(
      result => {
        if (result > 0) {
          // call create billing
          this.falg = true;
          this.getInventoryList(null, null, null, id_inventory, null, null, id_third)
          this.showNotification('top', 'center', 1, "<h3> <b>PROCESO INTERNO DE LA SEDE</b> </h3> <br> <h2>CORRECTO!</h2>", "info")

        }
      })
  }

  showNotInvOrDetails() {
    this.showNotification('top', 'center', 1, "<h3> <b>NO TIENE ITEM/INVENTARIO PARA LA REMISION</b> </h3>", "warning")
  }

  proccessBiling() {
    //registarr la feactura del movimeinto.  Paso final
    for (let element of this.detailBillingDTOList) {
      alert("POCESS")
      console.log("Elementtos ",element )
    }
  }

  getDetailInventoryById(state?: number, id_inventory?: number, id_third?: number,
    newProductThird?: ProductThirdDTO, id_prod_third?: number, quantity?: number) {
    let objectList = this.idsInventoryDic[id_third]

    console.log("Ver detalles nuevos ", objectList)
    this.proccessBiling();
  }

  beginPlus(id_inventory,inventoryQuantityDTOList,is_discount?,is_prod_th?){      
    console.log("enviar... ",this.inventoryQuantityDTODestinityList);
    let inv_origen
    this.inventoriesService.putPlusInventory(id_inventory, this.inventoryQuantityDTODestinityList)
    .subscribe(result=>{
      if(result){
        console.log("CORRECT")
        this.showNotification('top','center',3,"<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b> </h3> ",'info')

        if(is_discount){
          if(this.inventoryQuantityDTOOrigenList.length>0){
            let ob_2
            if (!this.form.value['is_principal_origen']){
              ob_2=this.idsInventoryDic[this.form.value['origen'].id_third]
             
            }else{
              ob_2=this.idsInventoryDic[this.CURRENT_ID_THIRD_PATHER]
              
            }
            inv_origen=ob_2[0].detail.id_inventory
            console.log("ENVAR ORIGEN \n",inv_origen);
            console.log("LISTAS PARA ENVIAR ORIGEN ", this.inventoryQuantityDTOOrigenList)
            this.beginDiscount(inv_origen,this.inventoryQuantityDTOOrigenList,false,is_prod_th)
          }else{
            if(!is_prod_th){
              this.showNotification('top','center',3,"<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b> </h3> ",'info')  
              this.volver()
            }
          }
        }else{
          if(!is_prod_th){
            this.showNotification('top','center',3,"<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b> </h3> ",'info')  
            this.volver()
          }
        }
        
 
      }else{
       this.showNotification('top','center',3,"<h3>El movimento presento <b>PROBLEMAS</b></h3> ",'info')
       this.volver()
        
      }
    });   
  }

  beginDiscount(id_inventory,inventoryQuantityDTOList,is_plus?,is_prod_th?){      
    console.log("enviar... ",inventoryQuantityDTOList);
    let inv_destinity
    this.inventoriesService.putDiscountInventory(id_inventory, inventoryQuantityDTOList)
    .subscribe(result=>{
      if(result){
        console.log("CORRECT")
        if(is_plus){
          if(this.inventoryQuantityDTODestinityList.length>0){
            let ob_1
            if (!this.form.value['is_principal_destinity']){
              ob_1=this.idsInventoryDic[this.form.value['destinity'].id_third]
              inv_destinity=ob_1[0].detail.id_inventory
      
            }else{
              ob_1=this.idsInventoryDic[this.CURRENT_ID_THIRD_PATHER]
            }
            inv_destinity=ob_1[0].detail.id_inventory      
            console.log("ENVAR DESTINO  \n",inv_destinity);
            console.log("LISTAS PARA ENVIAR DEST ", this.inventoryQuantityDTODestinityList)
            this.beginPlus(inv_destinity,this.inventoryQuantityDTODestinityList,true,is_prod_th)
          }else{
            if(!is_prod_th){
              this.showNotification('top','center',3,"<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b> </h3> ",'info')  
              this.volver()
            }
          }

        }else{
          if(!is_prod_th){
            this.showNotification('top','center',3,"<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b> </h3> ",'info')  
            this.volver()
          }
        }
       // this.showNotification('top','center',3,"<h3>Se ha realizado el movimento <b>CORRECTAMENTE</b> </h3> ",'info')
 
      }else{
       this.showNotification('top','center',3,"<h3>El movimento presento <b>PROBLEMAS</b></h3> ",'info')
       this.volver()
        
      }
    });   
  }

  addDocument(element) {
    let oldQuantity = element.quantity;
    let inventoryDetail = null;
    let flag = true;

    let dialogRef = this.dialog.open(BillDocumentComponent, {
      height: '450px',
      width: '600px',
      data: {
        type_name:this.TYPE_NAME,   
        doc: this.documentDTO
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        console.log('DOC ->> ',this.documentDTO);
        console.log('RESULT ->> ',result);
      }
    });

  }

}
