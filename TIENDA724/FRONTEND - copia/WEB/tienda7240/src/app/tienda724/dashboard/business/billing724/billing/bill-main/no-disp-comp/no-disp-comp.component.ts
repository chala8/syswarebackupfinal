import { Component, OnInit,Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-no-disp-comp',
  templateUrl: './no-disp-comp.component.html',
  styleUrls: ['./no-disp-comp.component.scss']
})
export class NoDispCompComponent implements OnInit {
 
  constructor(private http2: HttpClient, public dialogRef: MatDialogRef<NoDispCompComponent>,public dialog: MatDialog 
    ,@Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  vencimiento =0;
  averia =0;
  devolucion =0;
  traslado =0;
  robo =0;
  ngOnInit() {
    console.log(this.data.stores, this.data.code)
    this.http2.get("http://tienda724.com:8447/v1/resource/nodisponibles?idbill=-4001&listStore="+this.data.stores+"&code="+this.data.code).subscribe(res => {
      //@ts-ignore
      this.vencimiento = res;
    })
    this.http2.get("http://tienda724.com:8447/v1/resource/nodisponibles?idbill=-4002&listStore="+this.data.stores+"&code="+this.data.code).subscribe(res => {
      //@ts-ignore
      this.averia = res;
    })
    this.http2.get("http://tienda724.com:8447/v1/resource/nodisponibles?idbill=-4003&listStore="+this.data.stores+"&code="+this.data.code).subscribe(res => {
      //@ts-ignore
      this.devolucion = res;
    })
    this.http2.get("http://tienda724.com:8447/v1/resource/nodisponibles?idbill=-4004&listStore="+this.data.stores+"&code="+this.data.code).subscribe(res => {
      //@ts-ignore
      this.traslado = res;
    })
    this.http2.get("http://tienda724.com:8447/v1/resource/nodisponibles?idbill=-4005&listStore="+this.data.stores+"&code="+this.data.code).subscribe(res => {
      //@ts-ignore
      this.robo = res;
    })
  }

}

export interface DialogData {
  stores: any, code: any, producto: any
}
