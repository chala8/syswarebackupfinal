import { Routes } from '@angular/router';

/***************************************************************
 * Import components for your module *
 ***************************************************************/

 import {  ReportsComponent } from "./reports.component";
/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/


export const ReportsRouting: Routes = [


  { path: 'reports', component: null,
    children: [
        { path: '', redirectTo: 'menu', pathMatch: 'full'},
        { path: 'menu', component: ReportsComponent ,pathMatch: 'full'},
        
    ]
  },

]