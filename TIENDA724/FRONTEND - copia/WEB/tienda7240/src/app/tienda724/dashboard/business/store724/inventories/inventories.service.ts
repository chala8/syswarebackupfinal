import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';


/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';
import { Inventory } from './models/inventory'
import { InventoryDetail } from './models/inventoryDetail'
import { InventoryDetailDTO } from './models/inventoryDetailDTO'
import { InventoryDetailSimple } from './models/inventoryDetailSimple'
import { InventoryDTO } from './models/inventoryDTO';
import { InventoryParameters } from './models/inventaryParameter';
import { InventoryQuantityDTO } from './models/inventoryQuantityDTO'

import { FilterInventory } from './models/filters'
import { InventoryName } from '../../billing724/billing/bill-main/bill-main.component';

@Injectable()
export class InventoriesService {


  api_uri_inv = Urlbase[2] + '/inventories';
  api_uri_inv_det = Urlbase[2] + '/inventories-details';
  api_uri_inv_det_spl = Urlbase[2] + 'inventories-details/simple';
  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');

    this.options = new RequestOptions({ headers: this.headers });
  }

  public getInventoriesList = (state_inventory?: number, id_inventory?: number,
    id_third?: number, id_common_inventory?: number,
    name_inventory?: string, description_inventory?: string,
    id_state_inventory?: number): Observable<{} | Inventory[]> => {

    let params: URLSearchParams = new URLSearchParams();
    params.set('id_inventory', id_inventory ? "" + id_inventory : null);
    params.set('id_third', id_third ? "" + id_third : null);
    params.set('id_common_inventory', id_common_inventory ? "" + id_common_inventory : null);
    params.set('name_inventory', name_inventory ? "" + name_inventory : null);
    params.set('description_inventory', description_inventory ? "" + description_inventory : null);
    params.set('id_state_inventory', id_state_inventory ? "" + id_state_inventory : null);
    params.set('state_inventory', state_inventory ? "" + state_inventory : null);

    let myOption: RequestOptions = this.options;
    myOption.search = params;
    return this.http.get(this.api_uri_inv, this.options)
      .map((response: Response) => <Inventory[]>response.json())
      .catch(this.handleError);
  }


  public getInventoriesParamters = (id_inventory?: number,
    id_third?: number): Observable<{} | InventoryParameters> => {

    let params: URLSearchParams = new URLSearchParams();
    params.set('id_inventory', id_inventory ? "" + id_inventory : null);
    params.set('id_third', id_third ? "" + id_third : null);

    let myOption: RequestOptions = this.options;
    myOption.search = params;
    return this.http.get(this.api_uri_inv+"/parameters", this.options)
      .map((response: Response) => <InventoryParameters>response.json())
      .catch(this.handleError);
  }



  public postInventory = (body: InventoryDTO): Observable<Number | any> => {

    let params: URLSearchParams = new URLSearchParams();


    let myOption: RequestOptions = this.options;
    myOption.search = params;

    return this.http.post(this.api_uri_inv, body, this.options)
      .map((response: Response) => {
        if (response) {
          let id = +response["_body"]
          return id;
        } else {

          return null;
        }
      })
      .catch(this.handleError);
  }

  public putInventory = (id: number, body: InventoryDTO): Observable<Boolean | any> => {

    return this.http.put(this.api_uri_inv + "/" + id, body, { headers: this.headers })
      .map((response: Response) => {
        if (response) {
          return true;
        } else {

          return false;
        }
      })
      .catch(this.handleError);
  }

//----------------------------------------------------------------------------------------------------------------------------
  public getInventory = (id: number): Observable<Boolean | any> => {

    return this.http.get("http://tienda724.com:8447/v1/products2/inventoryList?id_store=" + id, { headers: this.headers })
    .map((response: Response) => <InventoryName[]>response.json())
    .catch(this.handleError);
    
  }
//----------------------------------------------------------------------------------------------------------------------------

  public deleteInventory = (id_inventory: number): Observable<Response | any> => {
    return this.http.delete(this.api_uri_inv + '/' + id_inventory, this.options)
      .map((response: Response) => {
        if (response) {
          return true;
        } else {

          return false;
        }
      })
      .catch(this.handleError);
  }

  /**
   * 
   * @param error 
   */
  public getInventoriesDetailSimpleList = (state_inv_detail?: number, id_inventory_detail?: number,
    id_inventory?: number,
    id_product_third?: number, quantity?: number,
    code?: string, description_inventory?: string,
    id_state_inv_detail?: number): Observable<{} | InventoryDetailSimple[]> => {

    let params: URLSearchParams = new URLSearchParams();
    params.set('id_inventory_detail', id_inventory_detail ? "" + id_inventory_detail : null);
    params.set('id_inventory', id_inventory ? "" + id_inventory : null);
    params.set('id_product_third', id_product_third ? "" + id_product_third : null);
    params.set('quantity', quantity ? "" + quantity : null);
    params.set('code', code ? "" + code : null);
    params.set('id_state_inv_detail', id_state_inv_detail ? "" + id_state_inv_detail : null);
    params.set('state_inv_detail', state_inv_detail ? "" + state_inv_detail : null);

    let myOption: RequestOptions = this.options;
    myOption.search = params;
    return this.http.get(this.api_uri_inv_det_spl, this.options)
      .map((response: Response) => <InventoryDetailSimple[]>response.json())
      .catch(this.handleError);

  }

      /**
   * 
   * @param error 
   */
  public getInventoriesDetailList = (state_inv_detail?:number,state_product?:number,id_inventory_detail?:number, 
              id_inventory?:number, quantity?:number,id_state_inv_detail?:number,
              id_product?:number,id_category?:number,stock?:number,
              stock_min?:number,img_url?:string,id_tax?:number,
              id_common_product?:number,name_product?:string,
              description_product?:string,id_state_product?:number,
              id_product_third?:number,location?:number,
              id_third?:number,id_category_third?:number,

              id_state_prod_third?:number,state_prod_third?:number,
              id_measure_unit?:number,id_measure_unit_father?:number,
              id_common_measure_unit?:number,name_measure_unit?:string,
              description_measure_unit?:string,id_state_measure_unit?:number,
              state_measure_unit?:number,id_code?:number,
              code?:number,img?:string,
              id_attribute_list?:number,
              id_state_cod?:number,state_cod?:number): Observable<{} | InventoryDetail[]> => {

    let params: URLSearchParams = new URLSearchParams();
    params.set('state_inv_detail', state_inv_detail ? "" + state_inv_detail : null);

    params.set('id_inventory_detail', id_inventory_detail ? "" + id_inventory_detail : null);
    params.set('id_inventory', id_inventory ? "" + id_inventory : null);
    params.set('quantity', quantity ? "" + quantity : null);
    params.set('id_state_inv_detail', id_state_inv_detail? "" + id_state_inv_detail : null);

    params.set('id_product',  id_product? "" + id_product : null);
    params.set('id_category', id_category? "" + id_category: null);
    params.set('stock', stock? "" + stock : null);
    params.set('stock_min', stock_min? "" + stock_min : null);
    params.set('img_url', img_url? "" + img_url: null);
    params.set('id_tax',  id_tax? "" + id_tax: null);
    params.set('id_common_product', id_common_product? "" + id_common_product: null);
    params.set('name_product', name_product? "" + name_product: null);
    params.set('description_product', description_product? "" + description_product: null);
    params.set('id_state_product', id_state_product? "" + id_state_product : null);
    params.set('state_product', state_product? "" + state_product : null);
    
    params.set('id_product_third', id_product_third ? "" + id_product_third : null);
    
    params.set('id_third', id_third ? "" + id_third : null);
    params.set('id_category_third', id_category_third ? "" + id_category_third : null);
    params.set('location', location ? "" + location : null);
    params.set('id_state_prod_third', id_state_prod_third ? "" + id_state_prod_third : null);
    params.set('state_prod_third', state_prod_third ? "" + state_prod_third : null);

    params.set('id_measure_unit', id_measure_unit ? "" + id_measure_unit : null);
    params.set('id_measure_unit_father', id_measure_unit_father ? "" + id_measure_unit_father : null);
    params.set('id_common_measure_unit', id_common_measure_unit ? "" + id_common_measure_unit : null);
    params.set('name_measure_unit', name_measure_unit ? "" + name_measure_unit : null);
    params.set('description_measure_unit', description_measure_unit ? "" + description_measure_unit : null);
    params.set('id_state_measure_unit', id_state_measure_unit ? "" + id_state_measure_unit : null);
    params.set('state_measure_unit', state_measure_unit ? "" + state_measure_unit : null);

    params.set('id_code', id_code ? "" + id_code : null);
    params.set('code', code ? "" + code : null);
    params.set('id_attribute_list', id_attribute_list ? "" + id_attribute_list : null);
    
    params.set('id_state_cod', id_state_cod ? "" + id_state_cod : null);
    params.set('state_cod', state_cod ? "" + state_cod : null);




    let myOption: RequestOptions = this.options;
    myOption.search = params;
    return this.http.get(this.api_uri_inv_det, this.options)
      .map((response: Response) => <InventoryDetail[]>response.json())
      .catch(this.handleError);

  }
  public postInventoryDetail = (id_inventory:number,body: InventoryDetailDTO[]): Observable<Boolean | any> => {
    
    let params: URLSearchParams = new URLSearchParams();
    
    params.set('id_inventory', id_inventory ? "" + id_inventory : null);
    
    let myOption: RequestOptions = this.options;
    myOption.search = params;

    return this.http.post(this.api_uri_inv_det, body, this.options)
      .map((response: Response) => {
        if (response) {
        
          return true;
        } else {

          return false;
        }
      })
      .catch(this.handleError);
  }
  
  public postInventoriesDetailListFilters = (CURRENT_ID_THIRD_PATHER,ID_INVENTORY,body: FilterInventory): Observable<InventoryDetail[] | any> => {
    
    let params: URLSearchParams = new URLSearchParams();
    
    params.set('id_inventory', ID_INVENTORY ? "" + ID_INVENTORY : null);
    params.set('id_third', CURRENT_ID_THIRD_PATHER ? "" + CURRENT_ID_THIRD_PATHER : null);
    
    let myOption: RequestOptions = this.options;
    myOption.search = params;
    let InvDetTemp:InventoryDetail[]

    return this.http.post(this.api_uri_inv+"/filters", body, this.options)
      .map((response: Response) => {

        let responseEntry :InventoryDetail[]=[];


        //responseEntry=JSON.stringify(response["_body"]);//Object.assign(,responseEntry)
        if(responseEntry){
          responseEntry=response.json();
          //console.log("INMEDIATE-->",responseEntry)

          return responseEntry;
        }else{

          return null;

        }
      })
      .catch(this.handleError);
  }





  public putInventoryDetail = (id_inventory: number, body: InventoryDTO[]): Observable<Boolean | any> => {

    return this.http.put(this.api_uri_inv_det + "/" + id_inventory, body, { headers: this.headers })
      .map((response: Response) => {
        if (response) {
          return true;
        } else {

          return false;
        }
      })
      .catch(this.handleError);
  }

  public deleteInventoryDetail = (id_inventory_detail?: number, id_inventory?: number): Observable<Response | any> => {
    let params: URLSearchParams = new URLSearchParams();

    params.set('id_inventory', id_inventory ? "" + id_inventory : null);

    let myOption: RequestOptions = this.options;
    myOption.search = params;

    return this.http.delete(this.api_uri_inv_det + '/' + id_inventory_detail, this.options)
      .map((response: Response) => {
        if (response) {
          return true;
        } else {

          return false;
        }
      })
      .catch(this.handleError);
  }

  public putPlusInventory = (id_inventory: number, body: InventoryQuantityDTO[]): Observable<Boolean | any> => {

    return this.http.put(this.api_uri_inv + "/plus/" + id_inventory, body, { headers: this.headers })
      .map((response: Response) => {
        if (response) {
          return true;
        } else {

          return false;
        }
      })
      .catch(this.handleError);
  }

  public putQuantity = (id_Store: number, quantity: number, code: string ,body: InventoryQuantityDTO[], disc, id_storage): Observable<Boolean | any> => {
    //console.log("this is disc", disc)
  if(disc == 1){
    return this.http.put("http://tienda724.com:8448/v1/billing-state/quantity", body, { 
      params: {"quantity":quantity,"id_store":id_Store,"code":code }
   }).map((response: Response) => {
        if (response) {
          return true;
        } else {

          return false;
        }
      })
      .catch(this.handleError);

  }else{
    return this.http.put("http://tienda724.com:8448/v1/billing-state/quantity", body, { 
      params: {"quantity":quantity,"id_store":id_Store,"code":code, "id_storage":id_storage}
   }).map((response: Response) => {
        if (response) {
          return true;
        } else {

          return false;
        }
      })
      .catch(this.handleError);

  }
    
  }

  public putDiscountInventory = (id_inventory: number, body: InventoryQuantityDTO[]): Observable<Boolean | any> => {

    return this.http.put(this.api_uri_inv + "/discount/" + id_inventory, body, { headers: this.headers })
      .map((response: Response) => {
        if (response) {
          return true;
        } else {

          return false;
        }
      })
      .catch(this.handleError);
  }


  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return null;//Observable.throw(errMsg);
  }


}
