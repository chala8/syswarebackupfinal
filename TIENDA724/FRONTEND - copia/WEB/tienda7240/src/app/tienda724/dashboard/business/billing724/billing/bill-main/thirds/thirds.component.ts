import { Http, Headers } from '@angular/http';
import { LocalStorage } from 'app/shared/localStorage';
import { Component, OnInit, ViewChild, ElementRef, SystemJsNgModuleLoader } from '@angular/core';
import { ClientData } from '../../models/clientData';
import { MatDialog } from '@angular/material';
import { GenerateThirdComponent2Component } from '../generate-third-component2/generate-third-component2.component';
import { ThirdselectComponent } from '../../thirdselect/thirdselect.component';
declare var $: any;
@Component({
  selector: 'app-thirds',
  templateUrl: './thirds.component.html',
  styleUrls: ['./thirds.component.scss']
})
export class ThirdsComponent implements OnInit {

  constructor(private httpClient: Http, private locStorage: LocalStorage, public dialog: MatDialog) { 
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());
   }
   @ViewChild('nameit') private elementRef: ElementRef;
  document = "";
  private headers = new Headers();
  address = "";
  city = "";
  doc = "";
  docType = "";
  fullname = "";
  phones = [];
  mails = [];
  cliente="";
  id_person=0;
  ccClient = "";
  id_directory=0;
 clientData = new ClientData(true, 'Cliente Ocacional', '--', '000', 'N/A', '000', 'N/A', null);
 @ViewChild('nameot') private elementRef2: ElementRef;



  searchClient2(){
    
    console.log("THIS ARE HEADERS",this.headers)
    var identificacionCliente = this.ccClient;
    var aux;
    this.httpClient.get('http://tienda724.com:8446/v1/persons/search?doc_person='+String(identificacionCliente),{ headers: this.headers }).subscribe((res)=>{
      console.log(JSON.parse(res.text()))
      if (res.text() == "[]"){
        this.openDialogClient2();
        // this.searchClient(event);
      }else{
        const dialogRef = this.dialog.open(ThirdselectComponent, {
          width: '60vw',
          height: '80vh',
          
          data: { thirdList: JSON.parse(res.text()) }
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if(result){

            aux = this.locStorage.getPersonClient(); 
            console.log("THIS THE AUX I NEED:", aux);
            this.cliente = aux.fullname; 
            this.clientData.is_natural_person = true;
            this.clientData.fullname = aux.fullname;
            this.clientData.document_type = aux.document_TYPE;
            this.clientData.document_number = aux.document_NUMBER;
            this.clientData.id_third = aux.id_PERSON;
            this.id_person = aux.id_PERSON;
            this.clientData.address = aux.address; 
            this.clientData.email = aux.city;
            this.clientData.phone = aux.phone;
            this.id_directory = aux.id_DIRECTORY;
            console.log("THIS IS THE CLIENT",this.clientData)
            setTimeout(() => {
  
              this.elementRef2.nativeElement.focus();
              this.elementRef2.nativeElement.select();
            
              }, 100);

          }
        });

       
      }   
    
    
    });
    

  }


  update(){
    console.log("http://tienda724.com:8446/v1/directories/phoneAndMail?id_directory="+this.id_directory+"&phone="+this.clientData.phone+"&mail="+this.clientData.email+"&address="+this.clientData.address)
    this.httpClient.put("http://tienda724.com:8446/v1/directories/phoneAndMail?id_directory="+this.id_directory+"&phone="+this.clientData.phone+"&mail="+this.clientData.email+"&address="+this.clientData.address, {} ,{ headers: this.headers }).subscribe(resp => {
      console.log("THIS IS MY RTESP: ",resp)
      this.clientData = new ClientData(true, 'Cliente Ocacional', '--', '000', 'N/A', '000', 'N/A', null);
      this.ccClient= "";
      this.cliente = "";
    })
  }


  searchClient(event){
    var identificacionCliente = new String(event.target.value);
    var aux;
    this.httpClient.get('http://tienda724.com:8446/v1/persons/search?doc_person='+String(identificacionCliente),{ headers: this.headers }).subscribe((res)=>{
      console.log(JSON.parse(res.text()))
      if (res.text() == "[]"){
        this.openDialogClient2();
        // this.searchClient(event); 
      }else{
        const dialogRef = this.dialog.open(ThirdselectComponent, {
          width: '60vw',
          height: '80vh',
          
          data: { thirdList: JSON.parse(res.text()) }
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if(result){

            aux = this.locStorage.getPersonClient(); 
            console.log("THIS THE AUX I NEED:", aux);
            this.cliente = aux.fullname; 
            this.clientData.is_natural_person = true;
            this.clientData.fullname = aux.fullname;
            this.clientData.document_type = aux.document_TYPE;
            this.clientData.document_number = aux.document_NUMBER;
            this.clientData.id_third = aux.id_PERSON;
            this.id_person = aux.id_PERSON;
            this.clientData.address = aux.address; 
            this.clientData.email = aux.city;
            this.clientData.phone = aux.phone;
            this.id_directory = aux.id_DIRECTORY;
            console.log("THIS IS THE CLIENT",this.clientData)
            

          }
          setTimeout(() => {
  
            this.elementRef.nativeElement.focus();
            this.elementRef.nativeElement.select();
          
            }, 100);
        });
      }   
    
    
    });
  }
  
  openDialogClient2(): void {

  

    const dialogRef = this.dialog.open(GenerateThirdComponent2Component, {
      width: '60vw',
      data: {}
    });
    

dialogRef.afterClosed().subscribe(result => {
  if (result) {
    // console.log('CREATE CLIENT SUCCESS');
    // console.log(result);s
    let isNaturalPerson= result.data.hasOwnProperty('profile')?true:false;
    let dataPerson= isNaturalPerson?result.data.profile:result.data;
    this.clientData.is_natural_person = isNaturalPerson;
    this.clientData.fullname= dataPerson.info.fullname;
    this.clientData.document_type = dataPerson.info.id_document_type;
    this.clientData.document_number = dataPerson.info.document_number;
    this.clientData.address = dataPerson.directory.address;
    this.clientData.phone = dataPerson.directory.phones[0].phone;
    this.clientData.email = dataPerson.directory.hasOwnProperty('mails')?dataPerson.directory.mails[0].mail:'N/A';
    setTimeout(() => {
  
      this.elementRef2.nativeElement.focus();
      this.elementRef2.nativeElement.select();
    
      }, 100);
  }
  setTimeout(() => {
  
    this.elementRef2.nativeElement.focus();
    this.elementRef2.nativeElement.select();
  
    }, 100);
});
}

  




  ngOnInit() { 


    
  }

  showNotification(from, align, id_type?, msn?, typeStr?) {
    const type = ['', 'info', 'success', 'warning', 'danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn ? msn : "<b>Noficación automatica </b>"

    }, {
        type: typeStr ? typeStr : type[id_type ? id_type : 2],
        timer: 200,
        placement: {
          from: from,
          align: align
        }
      });
  }


  buscarTercero(){
    var aux;
    this.httpClient.get('http://tienda724.com:8446/v1/persons/search?doc_person='+this.document,{ headers: this.headers }).subscribe(res=>{
    this.phones = []
    this.mails = []  
    if (res.text() == "[]"){
        this.showNotification('top', 'center', 3, "<h3>NO SE ENCONTRO EL DOCUMENTO DE FACTURA SOLICITADO</h3> ", 'danger');
        // this.searchClient(event);
      }else{
        aux = JSON.parse(res.text())[0]; 
        this.httpClient.get("http://tienda724.com:8446/v1/directories/generalDir?id_directory="+aux.id_DIRECTORY,{ headers: this.headers }).subscribe(response =>{
          this.httpClient.get("http://tienda724.com:8446/v1/directories/phone?id_directory="+aux.id_DIRECTORY,{ headers: this.headers }).subscribe(responsePhone =>{
            this.httpClient.get("http://tienda724.com:8446/v1/directories/mail?id_directory="+aux.id_DIRECTORY,{ headers: this.headers }).subscribe(responseMail=>{
              this.address = aux.address;
              this.city = JSON.parse(response.text()).city
              this.doc = aux.document_NUMBER;
              this.docType = aux.document_TYPE;
              this.fullname = aux.fullname;
              JSON.parse(responsePhone.text()).forEach(element => {
                this.phones.push(element)
              });
              JSON.parse(responseMail.text()).forEach(element => {
                this.mails.push(element)
              });

              console.log(this.address,this.city,this.doc,this.docType,this.fullname,this.phones,this.mails)

            })
          })
        })
      }   
    
    
    });
  }

}
