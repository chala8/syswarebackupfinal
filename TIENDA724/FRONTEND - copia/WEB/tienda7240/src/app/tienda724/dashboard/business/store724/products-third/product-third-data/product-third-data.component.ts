import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import * as _ from 'lodash';

/*
*    Material modules for component
*/
import {MatTabChangeEvent, VERSION} from '@angular/material';
/*
*     others component
*/
/*
*     models for  your component
*/
import { LocalStorage } from '../../../../../../shared/localStorage';
import { ProductThird } from '../models/productThird';

/*
*     services of  your component
*/
import { ProductThirdService } from '../product-third.service';
import { Token } from '../../../../../../shared/token';



var categoryList:ProductThird[];
var categoryListGlobal:ProductThird[];
/*
*     constant of  your component
*/
@Component({
  selector: 'app-product-third-data',
  templateUrl: './product-third-data.component.html',
  styleUrls: ['./product-third-data.component.scss']
})
export class ProductThirdDataComponent implements OnInit {

  isMyProduct=true;
  token:Token;
  ID_THIRD_TYPE: number;
  STATE=1
  CURRENT_ID_THIRD = 0;
  
  thirdAux:ProductThird[];






    displayedColumns = ['position', 'name', 'weight', 'symbol', 'direccion', 'opciones'];
    displayedColumns_General = ['position', 'name', 'weight', 'symbol', 'direccion'];
    dataSource:ProductDataSource

  constructor(public locStorage: LocalStorage,
    private _router: Router,
    public productService: ProductThirdService) { }

  ngOnInit() {
    let session = this.locStorage.getSession();
    if (!session) {
      this.Login();
    } else {
      this.token = this.locStorage.getToken();
      this.CURRENT_ID_THIRD=this.token.id_third;
      this.ID_THIRD_TYPE = 23;

      if (this.CURRENT_ID_THIRD!==null && this.CURRENT_ID_THIRD>0){

        this.getProductList()
        

      } 
      

    }
  }

  getProductList(state?:number,id_category?:number,id_category_father?:number,id_third?:number){
    
        this.productService.getProductThirdList()
        .subscribe((data: ProductThird[]) => categoryList = data,
        error => console.log(error),
        () => {
    
          this.dataSource = new ProductDataSource();
        });
    
      }
    
      Login() {
        let link = ['/auth'];
        this._router.navigate(link);
      }
    
      editCategory(category:ProductThird){ 
        this._router.navigate(['/dashboard/business/category/edit',category.product.id_product] ); 
     
     
     
      } 
    
      addCategory(category:ProductThird){ 
        
        this._router.navigate(['/dashboard/business/category/new'],{queryParams:{father:category.product.id_product}} ); 
     
      } 
    
      deleteCategory(id_product) {
        
            this.productService.Delete(id_product)
              .subscribe(
              result => {
                
                if (result === true) {
                  categoryList = _.filter(categoryList, function (f) { return f.id_product !== id_product; });
                  this.dataSource = new ProductDataSource();
                
                  alert("Eliminado correctamente");
        
                  return;
                } else {
                  //this.openDialog();
                  return;
                }
              })
        
          }

}
export class ProductDataSource extends DataSource<any> {
  /** Connect function called by the table to retrieve one stream containing the data to render. */

  connect(): Observable<ProductThird[]> {

    return Observable.of(categoryList);
  }

  disconnect() {}
}
