import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewProductStoreComponent } from './new-product-store.component';

describe('NewProductStoreComponent', () => {
  let component: NewProductStoreComponent;
  let fixture: ComponentFixture<NewProductStoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewProductStoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewProductStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
