import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { WayPayDataComponent } from './way-pay-data/way-pay-data.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/


export const WayToPayRouting: Routes = [
  
    { path: 'waypay', component: null,
      children: [
        { path: '', redirectTo: 'data', pathMatch: 'full'},
        { path: 'data', component: WayPayDataComponent},
         
  
      ]
    }
  
  ]