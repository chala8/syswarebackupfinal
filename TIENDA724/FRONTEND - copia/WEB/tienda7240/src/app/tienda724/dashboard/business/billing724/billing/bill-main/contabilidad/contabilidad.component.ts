import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '../../../../../../../shared/localStorage';
import { BillingService } from '../../billing.service';

@Component({
  selector: 'app-contabilidad',
  templateUrl: './contabilidad.component.html',
  styleUrls: ['./contabilidad.component.scss']
})
export class ContabilidadComponent implements OnInit {

  constructor(private http2: HttpClient,
              public locStorage: LocalStorage,
              private categoriesService: BillingService) { }

  selectedTypeDoc=3;
  docTypeList;
  notes = "";
  selectedStore;
  Stores;

  //nivel 1
  selectedFirstCC = "";
  firstLvlCC = [];
  //nivel 2
  selectedCC2 = "";
  lvlCC2 = [];
  //nivel 3
  selectedCC3 = "";
  lvlCC3 = [];
  //nivel 4
  selectedCC4 = "";
  lvlCC4 = [];
  //nivel 5
  selectedCC5 = "";
  lvlCC5 = [];
  //naturaleza
  selectedNat = "C"
  //valorCuenta
  cuenta = "0";
  //notasDetalle
  notesD= "";

  //Tabla Detalles
  tablaDetalles = []

  ngOnInit() {

    this.getDocTypeList();
    this.getStores();
    this.getFirstlvlCC();
  
  }

  addDetail(){

    if(this.selectedCC2==""){
      this.tablaDetalles.push({
        cuenta: this.selectedFirstCC,
        naturaleza: this.selectedNat,
        valor: Number(this.cuenta),
        nota: this.notesD
      })
    }else{
      if(this.selectedCC3==""){
        this.tablaDetalles.push({
          cuenta: this.selectedCC2,
          naturaleza: this.selectedNat,
          valor: Number(this.cuenta),
          nota: this.notesD
        })
      }else{
        if(this.selectedCC4==""){
          this.tablaDetalles.push({
            cuenta: this.selectedCC3,
            naturaleza: this.selectedNat,
            valor: Number(this.cuenta),
            nota: this.notesD
          })
        }else{
          if(this.selectedCC5==""){
            this.tablaDetalles.push({
              cuenta: this.selectedCC4,
              naturaleza: this.selectedNat,
              valor: Number(this.cuenta),
              nota: this.notesD
            })
          }else{
            this.tablaDetalles.push({
              cuenta: this.selectedCC5,
              naturaleza: this.selectedNat,
              valor: Number(this.cuenta),
              nota: this.notesD
            })
          }
        }

      }
    }

    
    this.selectedFirstCC="";
    this.selectedCC2="";
    this.selectedCC3="";
    this.selectedCC4="";
    this.selectedCC5="";
    this.selectedNat="";
    this.cuenta="";
    this.notesD="";
  }

  getFirstlvlCC(){
    this.http2.get("http://tienda724.com:8447/v1/kazu724/getcodcuentagen").subscribe(list => {
      //@ts-ignore
      this.firstLvlCC = list;
      this.selectedCC2 = "";
      this.selectedCC3 = "";
      this.selectedCC4 = "";
      this.selectedCC5 = "";
    })
  }

  getlvlCC2(){
    this.http2.get("http://tienda724.com:8447/v1/kazu724/getcodcuenta?cp="+this.selectedFirstCC).subscribe(list => {
      //@ts-ignore
      this.lvlCC2 = list;
      this.selectedCC3 = "";
      this.selectedCC4 = "";
      this.selectedCC5 = "";
    })
  }

  getlvlCC3(){
    this.http2.get("http://tienda724.com:8447/v1/kazu724/getcodcuenta?cp="+this.selectedCC2).subscribe(list => {
      //@ts-ignore
      this.lvlCC3 = list;
      this.selectedCC4 = "";
      this.selectedCC5 = "";
    })
  }

  getlvlCC4(){
    this.http2.get("http://tienda724.com:8447/v1/kazu724/getcodcuenta?cp="+this.selectedCC3).subscribe(list => {
      //@ts-ignore
      this.lvlCC4 = list;
      this.selectedCC5 = "";
    })
  }

  getlvlCC5(){
    this.http2.get("http://tienda724.com:8447/v1/kazu724/getcodcuenta?cp="+this.selectedCC4).subscribe(list => {
      //@ts-ignore
      this.lvlCC5 = list;
    })
  }

  getStores() {
    this.categoriesService.getStoresByThird(this.locStorage.getThird().id_third).subscribe(data => { 
        console.log(data);this.Stores = data 
        this.selectedStore = data[0].id_STORE})
}

  getDocTypeList(){
    this.http2.get("http://tienda724.com:8447/v1/kazu724/getdoctype").subscribe(list => {
      this.docTypeList = list;
    })
  }

  postMaster(){
    this.http2.post("http://tienda724.com:8447/v1/kazu724/doc",{
    id_document_status: 1,
    id_document_type: this.selectedTypeDoc,
    notes: this.notes,
    id_store: this.selectedStore,
    id_third_user: this.locStorage.getThird().id_third}).subscribe(response => { 
      this.tablaDetalles.forEach(element => {
        console.log("THIS IS IT, ",element)
        let body ={
          cc: Number(element.cuenta),
          valor: Number(element.valor),
          naturaleza: element.naturaleza,
          notes: element.nota,
          id_document: Number(response),
          id_country: 169
        }
        console.log("THIS IS BODY", body);
        this.http2.post("http://tienda724.com:8447/v1/kazu724/detail",body).subscribe(element2 => {})
      })
      console.log("THIS IS MY RESPONSE: ",response);
      this.notes = "";
      this.tablaDetalles = [];
    })
  }
}
