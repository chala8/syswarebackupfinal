import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { AttributeComponent } from './attribute/attribute.component';
import { AttributeMasterComponent } from './attribute/attribute-master/attribute-master.component';
import { AttributeDetailComponent } from './attribute/attribute-detail/attribute-detail.component';
import { AttributeValueComponent } from './attribute/attribute-value/attribute-value.component';
import { AttributeNewComponent } from './attribute/attribute-new/attribute-new.component';


/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/

export const AttributesRouting: Routes = [
  
    { path: 'attribute', component: null,
      children: [
          { path: '', redirectTo: 'list', pathMatch: 'full'},
          { path: 'list', component: AttributeComponent},
          { path: 'new', component: AttributeNewComponent},
          { path: 'master', component: AttributeMasterComponent},
          { path: 'detail/:id', component: AttributeDetailComponent},
          { path: 'value', component: AttributeValueComponent}
  
      ]
    }
  
  ]
