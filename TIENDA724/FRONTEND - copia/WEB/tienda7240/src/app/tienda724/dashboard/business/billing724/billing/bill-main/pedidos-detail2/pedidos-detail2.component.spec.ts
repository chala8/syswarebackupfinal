import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidosDetail2Component } from './pedidos-detail2.component';

describe('PedidosDetail2Component', () => {
  let component: PedidosDetail2Component;
  let fixture: ComponentFixture<PedidosDetail2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidosDetail2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidosDetail2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
