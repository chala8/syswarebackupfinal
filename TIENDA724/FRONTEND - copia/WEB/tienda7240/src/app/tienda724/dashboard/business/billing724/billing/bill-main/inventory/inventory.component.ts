import { Component, OnInit, ViewChild, ElementRef, SystemJsNgModuleLoader } from '@angular/core';
import { BillingService } from '../../billing.service';
import { LocalStorage } from 'app/shared/localStorage';
import { ThirdService } from 'app/tienda724/dashboard/business/thirds724/third/third.service';
import { MatTabChangeEvent, MatDialog } from '@angular/material';
import { ProductsOnCategoryComponent } from '../products-on-category/products-on-category.component';
import { ProductsOnStoreComponent } from '../products-on-store/products-on-store.component';
import { HttpClient } from '@angular/common/http';
import { NoDispCompComponent } from "./../no-disp-comp/no-disp-comp.component";

@Component({
  selector: 'app-inventarios-tienda',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {
//--------------
CantidadA = 0;
CantidadV = 0;
rotacion = 0;
Cantidad = 0;
Costo = 0;
CostoTotal = 0;
PCT = 0;
size = 1;
days = 1;
dataTable = [];
dataTable2 = [];
storesToSend = [];
linesToSend = [];
categoriesToSend = [];
brandsToSend = [];
SelectedStoreProd = "-1";
Stores;
CategoryFirstLvlList;
SelectedLine =-1;
SelectedSubCategory = -1;
SubCategoryList;
Brands;
SelectedBrand = -1
list = [];
//--------------
  productList;
  generalCategories;
  subcategories;
  modal1 = true;
  modal2 = false;
  modal3 = false;
  id_employee;
  subCategoryTitle;
  categories;
  products;
  allCategories;
  allProducts;
  myProducts;
  stores;
  currentStore;
  currentStorage;
  idThirdFather;
  currentStorages;
  productsOnCategory;
  categoryStack = [];
  selectedTab = 0;
  currentCategory;
  @ViewChild('nameit') private elementRef: ElementRef;

  constructor(private http2: HttpClient,private categoriesService: BillingService,public dialog: MatDialog, private storeService: BillingService, private locStorage: LocalStorage, private thirdService: ThirdService) { }


  cancel(){

  }


  getFirstLvlCategory(){ 
    this.categoriesService.getGeneralCategories().subscribe(data => { 
      this.CategoryFirstLvlList = data
      this.SelectedSubCategory=-1
    })
}

getSonCat(){

  this.http2.post("http://tienda724.com:8447/v1/resource/getsons",{listStore:this.locStorage.getTipo()}).subscribe(data => {
      console.log("THIS IS SONS:",data);
      //@ts-ignore
      this.list = data })

}

getSecondLvlCategory(){

  if(this.SelectedLine != -1){
    this.http2.get("http://tienda724.com:8447/v1/categories2/children?id_category_father="+this.SelectedLine).subscribe(data => {
      this.SelectedSubCategory = -1;
      this.SubCategoryList = data })
}
}


 
getBrands(){
    this.http2.get("http://tienda724.com:8447/v1/resource/brands").subscribe(data => {
      this.Brands = data })

}

getStores() {
  this.categoriesService.getStoresByThird(this.locStorage.getThird().id_third).subscribe(data => { 
      console.log(data);this.Stores = data })
}


getNoDisponibles(code){
  this.http2.get("http://tienda724.com:8447/v1/resource/nodisponibles?idbill=-4001&idstore="+this.locStorage.getIdStore()+"&code="+code).subscribe(
    response => {
      return response;    
    }    
  )
}



  ngOnInit() {
    //-----------------------------------------------
    this.getSonCat();
    this.getBrands();
    this.getFirstLvlCategory();
    this.getStores();
    //-----------------------------------------------
    this.currentCategory = null;
    this.categoryStack = []
    this.productsOnCategory = [];
    
    this.idThirdFather = this.locStorage.getThird().id_third;
    this.storeService.getGeneralCategories().subscribe(res=>{
      this.generalCategories = res;
      console.log(this.generalCategories,"theressssssss");
    })
    this.getIdEmployee().then(res=>{
      this.thirdService.getThirdList().subscribe(res=>{
        console.log(res,"the third")
        //this.id_employee = JSON.parse(localStorage.getItem("currentPerson")).id_person;
        var employee;
        console.log(this.id_employee)
        // @ts-ignore
        employee = res.filter(item => item.profile.id_person === this.id_employee);
        console.log(employee)     
        this.idThirdFather = employee[0].id_third_father;
        this.storeService.getStoresByThird(this.idThirdFather).subscribe(res=>{
          this.stores = res;
          console.log(this.stores)
        })
       
      })
    })

    this.currentStorage = [];
  }

  tabChanged = (tabChangeEvent: MatTabChangeEvent): void => {
    console.log('tabChangeEvent => ', tabChangeEvent); 
    console.log('index => ', tabChangeEvent.index); 
    this.selectedTab = tabChangeEvent.index;
    this.currentCategory = null;
    this.categoryStack = []
    this.productsOnCategory = [];
    this.getIdEmployee().then(res=>{
      this.thirdService.getThirdList().subscribe(res=>{
        console.log(res,"the third")
        //this.id_employee = JSON.parse(localStorage.getItem("currentPerson")).id_person;
        var employee;
        console.log(this.id_employee)
        // @ts-ignore
        employee = res.filter(item => item.profile.id_person === this.id_employee);
        console.log(employee)     
        this.idThirdFather = employee[0].id_third_father;
        this.storeService.getStoresByThird(this.idThirdFather).subscribe(res=>{
          this.stores = res;
          console.log(this.stores)
        })
        this.storeService.getGeneralCategories().subscribe(res=>{
          this.generalCategories = res;
        }) 
      })
    })

    this.currentStorage = [];
  }

  gotoModal3(){
    this.modal1 = false;
    this.modal2 = false;
    this.modal3 = true;
  }

  gotoModal2(){
    this.modal1 = false;
    this.modal2 = true;
    this.modal3 = false;
  }
  gotoModal1(){
    this.modal2 = false;
    this.modal1 = true;
    this.modal3 = false;
  }
  async getIdEmployee(){
    this.id_employee = this.locStorage.getPerson().id_person;

  }

  async popCategory(){
    this.categoryStack.pop();
  }

  gotoFatherCategory(){
    this.popCategory().then(res=>{
      console.log(this.categoryStack,"categorystact")
      console.log(this.categoryStack.length)
  
      if(this.categoryStack.length > 0){
        console.log(this.categoryStack[this.categoryStack.length - 1].id_CATEGORY)
        if(this.categoryStack[this.categoryStack.length - 1].name){
          this.currentCategory = this.categoryStack[this.categoryStack.length - 1].name;
        }else{
          this.currentCategory = this.categoryStack[this.categoryStack.length - 1].category;
        }
        this.storeService.getCategoryByThirdCategory(this.idThirdFather,this.categoryStack[this.categoryStack.length - 1].id_CATEGORY).subscribe(res=>{
          this.generalCategories = res
          console.log(res)
        })
      }else{
        this.currentCategory = null;
        (console.log("is 0"))
        this.storeService.getGeneralCategories().subscribe(res=>{
          this.generalCategories = res
          console.log(res)
        })
      }
    })
   
  }

  loadStore(store){
    this.currentStore = store;
    this.storeService.getStoragesByStore(store.id_STORE).subscribe(res=>{
      var dialogRef
      dialogRef = this.dialog.open(ProductsOnStoreComponent,{
        height: '450px',
        width: '600px',
        data: {
          currentStorages: res, storeName: store.store_NAME
        }
      })
    })
  }

  gotoProductsCategory(){
    this.storeService.getProductsByCategoryThird(this.categoryStack[this.categoryStack.length - 1].id_CATEGORY,this.idThirdFather).subscribe(res=>{
      var dialogRef
      dialogRef = this.dialog.open(ProductsOnCategoryComponent,{
        height: '450px',
        width: '750px',
        data: {
          productsOnCategory: res, category: this.currentCategory
        }
      })
    })
  }

  loadCategory(category){
    this.categoryStack.push(category)
    if(category.name){
      this.currentCategory = category.name;
    }else{
      this.currentCategory = category.category;
    }
    console.log(category.name)
    console.log(category)
    this.storeService.getCategoryByThirdCategory(this.idThirdFather,category.id_CATEGORY).subscribe(res=>{
      this.generalCategories = res;
      console.log(res,"categories")
    })
    this.storeService.getProductsByCategoryThird(category.id_CATEGORY,this.idThirdFather).subscribe(res=>{
      this.productsOnCategory = res
      console.log(res)
    })
  }

  // setBackButtonAction(){
  //   this.navBar.backButtonClick = () => {
  //   if(this.modal2 || this.modal3){
  //     this.gotoModal1()
  //     this.currentStorage = [];
  //   }else{
  //     this.navCtrl.pop();
  //   }
  //   }
  // }

  addProduct(product){
    this.myProducts.push(product);
    product.selected = true;
  }

  deleteProduct(product){
    var currentProduct = product;
    this.myProducts.splice(this.myProducts.indexOf(product),1);
    currentProduct.selected = true;
    this.allProducts[this.allProducts.indexOf(currentProduct)].selected = false;
  }

  addCategory(){
    this.myProducts = [];
    this.allProducts.forEach(item=>{
      item.selected = false;
    })
    this.gotoModal1();
  }

  // addByBarcode(){
  //   var alert = this.alertCtrl.create({title: "Escaneando código de barra",
  //   subTitle: "Escanea o ingresa un código de barras",
  //   buttons:["Cancelar"]});
  //   alert.present();
  // }

  showStorage(storage){
    this.storeService.getProductsByStorage(storage.id_storage).subscribe(res=>{
      console.log(res);
      this.currentStorage = res;

    })
    
  }

  async generate2(){
    
    this.brandsToSend = []
    if(this.SelectedBrand == -1){
      this.Brands.forEach(brand => {
        this.brandsToSend.push(brand.id_BRAND);
      })
    }else{
      this.brandsToSend.push(this.SelectedBrand);
    }
    
    this.storesToSend = []
    if(this.SelectedStoreProd == "-1"){
      this.Stores.forEach(store => {
        this.storesToSend.push(store.id_STORE);
      })
    }else{
      this.storesToSend.push(Number(this.SelectedStoreProd));
    }

    this.linesToSend = []
    if(this.SelectedLine == -1){
      this.CategoryFirstLvlList.forEach(line => {
        this.linesToSend.push(line.id_CATEGORY);
      })
    }else{
      this.linesToSend.push(this.SelectedLine);
    }

    this.categoriesToSend = []
    if(this.SelectedLine == -1){
        this.categoriesToSend = this.list;
    }else{
      if(this.SelectedSubCategory==-1){
        this.SubCategoryList.forEach(element2 => {
          this.categoriesToSend.push(element2.id_CATEGORY)
        });
      }else{
        this.categoriesToSend.push(this.SelectedSubCategory)
      }
    }



    


  }

  openDialog(code, product){
    const dialogRef = this.dialog.open(NoDispCompComponent, {
      width: '60vw',
      height: '80vh',
      data: { stores: this.storesToSend, code: code, producto: product }
    })
  }

  sendPost(){
    this.Cantidad = 0;
    this.Costo = 0;
    this.CostoTotal = 0;
    this.PCT = 0;
    console.log(JSON.stringify({listStore: this.storesToSend,
      listLine: this.linesToSend,
      listCategory: this.categoriesToSend,
      listBrand: this.brandsToSend}))
    this.http2.post("http://tienda724.com:8447/v1/resource/inventory",{listStore: this.storesToSend,
    listLine: this.linesToSend,
    listCategory: this.categoriesToSend,
    listBrand: this.brandsToSend}).subscribe(response =>  {
      console.log("THIS IS RESPONSE, ",response);
      this.http2.post("http://localhost:8447/v1/resource/sumnodisponibles",{listStore: this.storesToSend}).subscribe(responses =>  {
        console.log(responses)
        //@ts-ignore
        response.forEach(element => {
          let item = element;
          //@ts-ignore
          if(responses.find(items => items.ownbarcode == element.barcode)){
            //@ts-ignore
            item.nodisp=responses.find(items => items.ownbarcode == element.barcode).no_DISPONIBLE;
          }else{
  
            item.nodisp=0;
          }
          this.dataTable.push(item)
          this.Cantidad += element.cantidad;
          this.Costo += element.costo;
          this.CostoTotal += element.costototal;
          this.PCT += element.pct_INVENTARIO;
        });
        //@ts-ignore
        this.size = response.lenght
        
        
        console.log(response)
        
      } )
      //@ts-ignore
      
    })
  }

  sendPost2(){
    
    this.CantidadA = 0;
    this.CantidadV = 0;
    this.rotacion = 0;
    console.log(JSON.stringify({listStore: this.storesToSend,
      listLine: this.linesToSend,
      listCategory: this.categoriesToSend,
      listBrand: this.brandsToSend,
      days: this.days}))
    this.http2.post("http://tienda724.com:8447/v1/resource/rotacion",{listStore: this.storesToSend,
    listLine: this.linesToSend,
    listCategory: this.categoriesToSend,
    listBrand: this.brandsToSend,
    days: this.days}).subscribe(response =>  {
      //@ts-ignore
      this.dataTable2 = response;
      console.log(response);
      //@ts-ignore
      response.forEach(element => {
        this.CantidadA += element.cantidadactual;
        this.CantidadV += element.cantidadvendida;
        this.rotacion += element.dias_ROTACION;
      })
      
    })
  }

  roundRot(){
    return Math.round((this.rotacion/this.dataTable2.length) * 100) / 100
  }


  generate(){
    this.dataTable = [];
    this.generate2().then(response => {this.sendPost()})
  }

  gen(){
    this.generate2().then(response => {this.sendPost2()})
  }

  excel(){
    console.log("THIS IS ROWS: ",JSON.stringify(this.dataTable));
    this.http2.post("http://tienda724.com:8447/v1/resource/inventory/Excel",this.dataTable,{ responseType: 'text'}).subscribe(response => {
      console.log(response);
      window.open("http://tienda724.com/reportes/"+response);
    })
  }

  excelrot(){
    console.log("THIS IS ROWS: ",JSON.stringify(this.dataTable2));
    this.http2.post("http://tienda724.com:8447/v1/resource/rotacion/Excel",this.dataTable2,{ responseType: 'text'}).subscribe(response => {
      console.log(response);
      window.open("http://tienda724.com/reportes/"+response);
    })
  }

  
}
