import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsOnStoragesComponent } from './products-on-storages.component';

describe('ProductsOnStoragesComponent', () => {
  let component: ProductsOnStoragesComponent;
  let fixture: ComponentFixture<ProductsOnStoragesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsOnStoragesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsOnStoragesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
