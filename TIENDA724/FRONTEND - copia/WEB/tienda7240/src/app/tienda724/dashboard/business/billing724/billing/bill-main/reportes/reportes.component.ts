import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '../../../../../../../shared/localStorage';
import { DatePipe } from '@angular/common';
import { BillingService } from '../../billing.service';
import { Http, Headers } from '@angular/http';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.scss']
})
export class ReportesComponent implements OnInit {
 
  constructor(private categoriesService: BillingService,
              private datePipe: DatePipe, 
              private http2: HttpClient, 
              public locStorage: LocalStorage,
              private httpClient: Http) {
                this.headers.append('Content-Type', 'application/json');
                this.headers.append('Authorization', this.locStorage.getTokenValue());
               }
  private headers = new Headers();
  SelectedStore = "";
  SelectedStore2 = "";
  SelectedStore3 = "";
  SelectedStoreProd = "";
  SelectedStoreCat = "";
  SelectedStorerep = "";
  picker;
  picker2;
  LineChart=null;
  PieChart=[];
  datos = [];
  title = "This is Title";
  labels = ["Tax", "Subtotal", "Total"];
  chartHtml = "<div></div>";
  date1: Date;
  date2: Date;
  date3: Date;
  date4: Date;
  dateP1: Date;
  dateP2: Date;
  dateC1: Date;
  dateC2: Date; 
  dateF1: Date;
  dateF2: Date;     
  dateCC1: Date;
  dateCC2: Date;   
  ThirdsDate1: Date;  
  ThirdsDate2: Date;  
  LineChartTotal = [];
  LineChartSubtotal = [];
  LineChartTax = [];
  Labels = [];
  Labels2 = [];
  datosTotal = [];
  datosReport = [];
  datosSubtotal = [];
  datosTax = [];
  tipoFactura = 1;
  tipoFactura2 = 1;
  tipoIntervalo = 1;
  colorLabels = [];
  chartLabel = "";
  BarChart = null;
  CategoryFirstLvlList
  Stores;
  SubCategoryList;
  ProductList;
  SelectedLine = -1;
  SelectedSubCategory = -1;
  SelectedProduct = -1;
  dataTablaThirds = " ";



  ListReportProd = [];
  isListProdFull = false;



  ListReportCat;
  isListCatFull = false;


 
  ListReportBill;
  isListBillFull = false;



  ListReportCC;
  isListCCFull = false;



  StoreNameForConsec: String;

  devolucionesVentas = 0;
  ventasTotales = 0;
  costosTotales = 0;
  utilidadesTotales = 0;
  margenPromedio = 0;
  margenPromedio2 = 0;


  storeId;


async generateMeans(){

let cont = 0;

this.ListReportBill.forEach(element => {
    if(element.id_BILL_STATE == 1){
        this.ventasTotales+=element.venta;
        this.costosTotales+=element.costo;
        this.utilidadesTotales+=element.utilidad;
        this.margenPromedio+=element.pct_MARGEN_VENTA;
        this.margenPromedio+=element.pct_MARGEN_COSTO;
    }else{
        if(element.id_BILL_STATE == 41){
            console.log("WEBBB: ",this.devolucionesVentas)
            this.devolucionesVentas+=element.venta;
        }
    }
        cont++;
    })


this.margenPromedio = this.utilidadesTotales*100/this.ventasTotales;
this.margenPromedio2 = this.utilidadesTotales*100/this.costosTotales;

}

 


generatePdfRoute(fullname:String,num_DOCUMENTO:String){
    let name = fullname.split(" ").join("_");
    return "http://tienda724.com/facturas/"+name+"_"+num_DOCUMENTO+".pdf";
}




generatePdfRoute2(fullname:String,consecutive:number){
    let name = fullname.split(" ").join("_");
    return "http://tienda724.com/docscaja/"+name+consecutive+".pdf";
    }


getRepBillList(){
    this.devolucionesVentas  = 0;
    this.ventasTotales = 0;
    this.costosTotales = 0;
    this.utilidadesTotales = 0;
    this.margenPromedio = 0;
    this.margenPromedio2 = 0;
      console.log("http://tienda724.com:8447/v1/resource/reportbill?id_third="+this.locStorage.getThird().id_third+"&id_store="+this.SelectedStore+"&date1="+this.transformDate(this.dateF1)+"&date2="+this.transformDate(this.dateF2)+"&typemove="+this.tipoFactura2)
      this.http2.get("http://tienda724.com:8447/v1/resource/reportbill?id_third="+this.locStorage.getThird().id_third+"&id_store="+this.SelectedStore+"&date1="+this.transformDate(this.dateF1)+"&date2="+this.transformDate(this.dateF2)+"&typemove="+this.tipoFactura2).subscribe(
        data => {
            console.log("I NEED THIS DATA DUDE: ",data);
            this.ListReportBill = data;
            this.isListBillFull = true;
            this.generateMeans().then(data => {
            });
        }
    )
}


getRepCCList(){
    this.devolucionesVentas  = 0;
    this.ventasTotales = 0;
    this.costosTotales = 0;
    this.utilidadesTotales = 0;
    this.margenPromedio = 0;
    this.margenPromedio2 = 0;
      console.log("http://tienda724.com:8451/v1/close/report?id_store="+this.SelectedStore2+"&date1="+this.transformDate(this.dateCC1)+"&date2="+this.transformDate(this.dateCC2))
      this.http2.get("http://tienda724.com:8451/v1/close/report?id_store="+this.SelectedStore2+"&date1="+this.transformDate(this.dateCC1)+"&date2="+this.transformDate(this.dateCC2)).subscribe(
        data => {
            console.log("THIS IS THE CC LIST: ",data)
            this.ListReportCC = data;    
            this.isListCCFull = true;
            this.generateMeans().then(data => {
            });
        }
    )
}



  getRepProdList(){
      this.ListReportProd = [];
      console.log("http://tienda724.com:8447/v1/resource/reportproduct?id_third="+this.locStorage.getThird().id_third+"&id_store="+this.SelectedStoreProd+"&date1="+this.transformDate(this.dateP1)+"&date2="+this.transformDate(this.dateP2))
      this.http2.get("http://tienda724.com:8447/v1/resource/reportproduct?id_third="+this.locStorage.getThird().id_third+"&id_store="+this.SelectedStoreProd+"&date1="+this.transformDate(this.dateP1)+"&date2="+this.transformDate(this.dateP2)).subscribe(
          data => {
              console.log("THIS I WILL USE HEE HEE: ",data);
              //@ts-ignore
              this.ListReportProd = data;
              this.isListProdFull = true;
          }
      )
  }



  getRepCatList(){
      console.log("http://tienda724.com:8447/v1/resource/reportcategory?id_third="+this.locStorage.getThird().id_third+"&id_store="+this.SelectedStoreCat+"&date1="+this.transformDate(this.dateC1)+"&date2="+this.transformDate(this.dateC2))
      this.http2.get("http://tienda724.com:8447/v1/resource/reportcategory?id_third="+this.locStorage.getThird().id_third+"&id_store="+this.SelectedStoreCat+"&date1="+this.transformDate(this.dateC1)+"&date2="+this.transformDate(this.dateC2)).subscribe(
        data => {
            this.ListReportCat = data;
            this.isListCatFull = true;
        }
    )
}




  generateReport(){
      if(this.SelectedLine == -1) {
          console.log("Is all")
          this.getChartData3()
      }else{
          if(this.SelectedSubCategory == -1 && this.SelectedLine != -1) {
              console.log("Is a Line")
              this.getChartData(0)
          }else{
              if(this.SelectedSubCategory != -1 && this.SelectedProduct == -1 && this.SelectedLine != -1){
                  console.log("Is a SubCategory")
                  this.getChartData(1)
              }else {
                  if(this.SelectedSubCategory !=-1 && this.SelectedLine !=-1 && this.SelectedProduct!= -1){
                      console.log("Is a Product")
                      this.getChartData4();
                  }
              }
          }
      }
  }

  getChartData4(){
    console.log("http://tienda724.com:8447/v1/resource/byproduct?id_bill_type="+this.tipoFactura+"&id_product_store="+this.SelectedProduct+"&id_period="+this.tipoIntervalo+"&date1="+this.transformDate(this.date1)+"&date2="+this.transformDate(this.date2))
    this.http2.get("http://tienda724.com:8447/v1/resource/byproduct?id_bill_type="+this.tipoFactura+"&id_product_store="+this.SelectedProduct+"&id_period="+this.tipoIntervalo+"&date1="+this.transformDate(this.date1)+"&date2="+this.transformDate(this.date2)+"&id_store="+[this.SelectedStorerep]).subscribe(data => {
    if(this.BarChart == null){
    }else{
        //@ts-ignore
        this.BarChart.destroy();
    }
    
    this.loadData(data).then(a => {
      this.loadChart3().then(a =>{
          this.datosReport = [];
          this.Labels = [];
          this.colorLabels = [];}
      );
    })

     });
    }

  getSecondLvlCategory(){
      if(this.SelectedLine != -1){
        this.http2.get("http://tienda724.com:8447/v1/categories2/children?id_category_father="+this.SelectedLine).subscribe(data => { this.SubCategoryList = data })
    }
  }


  getFirstLvlCategory(){ 
      this.categoriesService.getGeneralCategories().subscribe(data => { this.CategoryFirstLvlList = data })
  }


  getProductList(){
    this.http2.get("http://tienda724.com:8447/v1/categories2/productStore?id_ps="+this.SelectedSubCategory+"&id_store="+this.locStorage.getIdStore()).subscribe(data => { this.ProductList = data })
}



  async loadData(data){
        data.forEach(element => {
            this.Labels.push(element.label);
            this.datosReport.push(element.total);
            this.colorLabels.push('rgba(132, 255, 132, 0.6)')
        });
        if(this.tipoFactura == 1){
            this.chartLabel = "Venta" 
        }else{
            this.chartLabel = "Compra"
        }
      
  }

 getChartData(iscat){
     let storeList = [];
     storeList.push(this.SelectedStorerep);
     if(iscat == 0){

        console.log("http://tienda724.com:8447/v1/resource/bycategory?id_third="+this.locStorage.getThird().id_third+"&id_bill_type="+this.tipoFactura+"&id_category="+this.SelectedLine+"&id_period="+this.tipoIntervalo+"&date1="+this.transformDate(this.date1)+"&date2="+this.transformDate(this.date2)+"&id_store="+storeList);
        this.http2.post("http://tienda724.com:8447/v1/resource/bycategory?id_third="+this.locStorage.getThird().id_third+"&id_bill_type="+this.tipoFactura+"&id_category="+this.SelectedLine+"&id_period="+this.tipoIntervalo+"&date1="+this.transformDate(this.date1)+"&date2="+this.transformDate(this.date2)+"&id_store="+storeList,{}).subscribe(data => {
        if(this.BarChart == null){
        }else{
            //@ts-ignore
            this.BarChart.destroy();
        }
        
        this.loadData(data).then(a => {
          this.loadChart3().then(a =>{
              this.datosReport = [];
              this.Labels = [];
              this.colorLabels = [];}
          );
        })
    
         });}
        if(iscat == 1){
        console.log("http://tienda724.com:8447/v1/resource/bycategory?id_bill_type="+this.tipoFactura+"&id_category="+this.SelectedSubCategory+"&id_period="+this.tipoIntervalo+"&date1="+this.transformDate(this.date1)+"&date2="+this.transformDate(this.date2)+"&id_third="+this.locStorage.getThird().id_third);
        this.http2.post("http://tienda724.com:8447/v1/resource/bycategory?id_bill_type="+this.tipoFactura+"&id_category="+this.SelectedSubCategory+"&id_period="+this.tipoIntervalo+"&date1="+this.transformDate(this.date1)+"&date2="+this.transformDate(this.date2)+"&id_third="+this.locStorage.getThird().id_third+"&id_store="+storeList,{}).subscribe(data => {
        if(this.BarChart == null){
        }else{
            //@ts-ignore
            this.BarChart.destroy();
        }
        
        this.loadData(data).then(a => {
            this.loadChart3().then(a =>{
                this.datosReport = [];
                this.Labels = [];
                this.colorLabels = [];} 
            );
        })
    
            });}
  }

  getChartData3(){ 
      console.log("http://tienda724.com:8447/v1/resource?id_store="+[this.SelectedStorerep]+"&id_bill_type="+this.tipoFactura+"&id_period="+this.tipoIntervalo+"&date1="+this.transformDate(this.date1)+"&date2="+this.transformDate(this.date2)+"&id_third="+this.locStorage.getThird().id_third);
    this.http2.get("http://tienda724.com:8447/v1/resource?id_store="+[this.SelectedStorerep]+"&id_bill_type="+this.tipoFactura+"&id_period="+this.tipoIntervalo+"&date1="+this.transformDate(this.date1)+"&date2="+this.transformDate(this.date2)+"&id_third="+this.locStorage.getThird().id_third).subscribe(data => {
    if(this.BarChart == null){
    }else{
        //@ts-ignore
        this.BarChart.destroy();
    }
    
    this.loadData3(data).then(a => {
      this.loadChart3().then(a =>{
          this.datosReport = [];
          this.Labels = [];
          this.colorLabels = [];}
      );
    })

     });
    }

    async loadData3(data){
        data.forEach(element => {
            this.Labels.push(element.labelDate + "/" + element.counter);
            this.datosReport.push(element.total);
            this.colorLabels.push('rgba(132, 255, 132, 0.6)')
        });
        if(this.tipoFactura == 1){
            this.chartLabel = "Venta" 
        }else{
            this.chartLabel = "Compra"
        }
      }

transformDate(date){
return this.datePipe.transform(date, 'yyyy/MM/dd');
}

async loadChart3(){
  // Bar chart:

  
  
  this.BarChart = new Chart('barChart', {
    type: 'bar',
  data: {
   labels: this.Labels,
   datasets: [{
       label: this.chartLabel,
       data: this.datosReport,
       backgroundColor: this.colorLabels,
       borderColor: this.colorLabels,
       borderWidth: 1
   }]
  }, 
  options: {
   title:{
       text: this.chartLabel,
       display:true
   },
   scales: {
       yAxes: [{
           ticks: {
               beginAtZero:true
           }
       }]
   }
  }
  });
}

  async loadChart(){

             console.log("this is datos",this.datos)
this.LineChart = new Chart('lineChart', {
    type: 'line',
  data: {
   labels: ["Subtotal", "Tax", "Total"],
   datasets: [{
       label: 'Ventas',
       data: this.datos,
       fill:false,
       lineTension:0.2,
       borderColor:"Green",
       borderWidth: 2
   }]
  }, 
  options: {
   title:{
       text:"Ventas",
       display:true
   },
   scales: {
       yAxes: [{
           ticks: {
               beginAtZero:true
           }
       }]
   }
  }
  });
  
  
  // Bar chart:
  this.BarChart = new Chart('barChart', {
    type: 'bar',
  data: {
   labels: ["Subtotal", "Tax", "Total"],
   datasets: [{
       label: 'Ventas',
       data: this.datos,
       backgroundColor: [
           'rgba(255, 99, 132, 0.2)',
           'rgba(54, 162, 235, 0.2)',
           'rgba(255, 206, 86, 0.2)'
       ],
       borderColor: [
           'rgba(255,99,132,1)',
           'rgba(54, 162, 235, 1)',
           'rgba(255, 206, 86, 1)'
       ],
       borderWidth: 1
   }]
  }, 
  options: {
   title:{
       text:"Ventas",
       display:true
   },
   scales: {
       yAxes: [{
           ticks: {
               beginAtZero:true
           }
       }]
   }
  }
  });
  
  
  
  
  // pie chart:
  this.PieChart = new Chart('pieChart', {
    type: 'pie',
  data: {
   labels: ["Subtotal", "Tax", "Total"],
   datasets: [{
       label: 'Ventas',
       data: this.datos,
       backgroundColor: [
           'rgba(255, 99, 132, 0.2)',
           'rgba(54, 162, 235, 0.2)',
           'rgba(255, 206, 86, 0.2)'
       ],
       borderColor: [
           'rgba(255,99,132,1)',
           'rgba(54, 162, 235, 1)',
           'rgba(255, 206, 86, 1)'
       ],
       borderWidth: 1
   }]
  }, 
  options: {
   title:{
       text:"Ventas",
       display:true
   },
   scales: {
       yAxes: [{
           ticks: {
               beginAtZero:true
           }
       }]
   }
  }
  });
  
        
             
    }

    

  ngOnInit(){
      //@ts-ignore
      this.storeId = this.locStorage.getIdStore();
    this.StoreNameForConsec = (this.locStorage.getThird().info.fullname.replace(" ","_"))+"_cc_"
    this.getFirstLvlCategory();
    this.getStores();

}

getStores() {
    this.categoriesService.getStoresByThird(this.locStorage.getThird().id_third).subscribe(data => { 
        console.log(data);this.Stores = data 
        this.SelectedStore = data[0].id_STORE
        this.SelectedStore2 = data[0].id_STORE
        this.SelectedStore3 = data[0].id_STORE
        this.SelectedStoreProd = data[0].id_STORE
        this.SelectedStoreCat = data[0].id_STORE})
}
 
getChartData2(){
    console.log("THIS IS URL: ","http://tienda724.com:8447/v1/resource/product?id_bill_type=1&date1="+this.transformDate(this.date3)+"&date2="+this.transformDate(this.date4)+"&id_store="+this.SelectedStore3);
    this.http2.get("http://tienda724.com:8447/v1/resource/product?id_bill_type=1&date1="+this.transformDate(this.date3)+"&date2="+this.transformDate(this.date4)+"&id_store="+this.SelectedStore3).subscribe(data => {
    console.log(data);
    this.loadData2(data).then(a => {
        console.log("this is labels", this.Labels2)
        console.log("this is subtotal", this.datosSubtotal)
        console.log("this is total", this.datosTotal)
        console.log("this is tax", this.datosTax)
        this.loadChart2().then(a =>{
            this.datos = null;
            this.Labels2 = [];
            this.datosSubtotal = [];
            this.datosTax = [];
            this.datosTotal = [];}
        );
    })

     });
}


async loadData2(data){
    data.forEach(elem =>{
        this.Labels2.push(elem.field);
        this.datosSubtotal.push(elem.subtotal);
        this.datosTotal.push(elem.total);
        this.datosTax.push(elem.tax);
    });
  }

  
  async loadChart2(){

    this.LineChartTotal = new Chart('lineChartTotal', {
type: 'line',
data: {
labels: this.Labels2,
datasets: [{
label: 'Total',
data: this.datosTotal,
fill:false,
lineTension:0.2,
borderColor:"Green",
borderWidth: 2
},{
    label: 'Subtotal',
    data: this.datosSubtotal,
    fill:false,
    lineTension:0.2,
    borderColor:"Blue",
    borderWidth: 2
    },{
        label: 'Tax',
        data: this.datosTax,
        fill:false,
        lineTension:0.2,
        borderColor:"Red",
        borderWidth: 2
        }]
}, 
options: {
title:{
text:"Ventas",
display:true
},
scales: {
yAxes: [{
  ticks: {
      beginAtZero:true
  }
}]
}
}
});


    
}

excelPersona(){
    console.log("THIS IS ROWS: ",this.dataTablaThirds);
    this.http2.post("http://tienda724.com:8447/v1/resource/personas/Excel",this.dataTablaThirds,{ responseType: 'text'}).subscribe(response => {
      console.log(response);
       window.open("http://tienda724.com/reportes/"+response);
    })
  }



genReport() {
    console.log("this is url", "http://tienda724.com:8446/v1/thirds/listThird?id_third="+this.locStorage.getThird().id_third+"&date1="+this.transformDate(this.ThirdsDate1)+"&date2="+this.transformDate(this.ThirdsDate2)+"&bill_type="+this.tipoFactura2)
    this.httpClient.get("http://tienda724.com:8446/v1/thirds/listThird?id_third="+this.locStorage.getThird().id_third+"&date1="+this.transformDate(this.ThirdsDate1)+"&date2="+this.transformDate(this.ThirdsDate2)+"&bill_type="+this.tipoFactura2,{ headers: this.headers }).subscribe(elem =>{
        this.dataTablaThirds = JSON.parse(elem.text());
        console.log("ELEMENTO: "+elem.text())
    })

}

excelProductos(){
    console.log("THIS IS ROWS: ",this.ListReportProd);
    this.http2.post("http://tienda724.com:8447/v1/resource/productos/Excel",this.ListReportProd,{ responseType: 'text'}).subscribe(response => {
      console.log(response);
      window.open("http://tienda724.com/reportes/"+response);
    })
  }

excelCategorias(){
console.log("THIS IS ROWS: ",this.ListReportCat);
this.http2.post("http://tienda724.com:8447/v1/resource/categorias/Excel",this.ListReportCat,{ responseType: 'text'}).subscribe(response => {
    console.log(response);
    window.open("http://tienda724.com/reportes/"+response);
})
}

excelFacturas(){
    console.log("THIS IS ROWS: ",this.ListReportBill);
    this.http2.post("http://tienda724.com:8447/v1/resource/facturas/Excel",this.ListReportBill,{ responseType: 'text'}).subscribe(response => {
        console.log(response);
        window.open("http://tienda724.com/reportes/"+response);
    })
    }

excelCaja(){
    console.log("THIS IS ROWS: ",this.ListReportCC);
    this.http2.post("http://tienda724.com:8447/v1/resource/cajas/Excel",this.ListReportCC,{ responseType: 'text'}).subscribe(response => {
        console.log(response);
        window.open("http://tienda724.com/reportes/"+response);
    })
} 



}
