import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';
  
/** Import Model Data */
import { CommonState } from '../commons/commonState'
import { PaymentMethod } from './models/paymentMethod'
import { DetailPaymentBill } from './models/detailPaymentBill'
import { DetailPaymentBillComplete } from './models/detailPaymentBillComplete'

/** Import Model Data Transfer Object */
import { CommonStateDTO } from '../commons/commonStateDTO'
import { PaymentMethodDTO } from './models/paymentMethodDTO'
import { DetailPaymentBillIdDTO } from './models/detailPaymentBillIdDTO'
import { DetailPaymentBillDTO } from './models/detailPaymentBillDTO'
import { DetailPaymentBillCompleteDTO } from './models/detailPaymentBillCompleteDTO'

@Injectable()
export class PaymentMethodService {
  api_uri_pay = Urlbase[3] + '/payments-methods';
  api_uri_pay_detail = Urlbase[3] + '/payments-details';
  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');

    this.options = new RequestOptions({ headers: this.headers });
  }


}
