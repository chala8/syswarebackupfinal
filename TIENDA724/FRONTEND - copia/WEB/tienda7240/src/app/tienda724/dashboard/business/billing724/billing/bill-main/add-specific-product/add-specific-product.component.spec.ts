import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSpecificProductComponent } from './add-specific-product.component';

describe('AddSpecificProductComponent', () => {
  let component: AddSpecificProductComponent;
  let fixture: ComponentFixture<AddSpecificProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSpecificProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSpecificProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
