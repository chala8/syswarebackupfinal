import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { BillingService } from '../../billing.service';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from 'app/shared/localStorage';
@Component({
  selector: 'app-pedidos-detail',
  templateUrl: './pedidos-detail.component.html',
  styleUrls: ['./pedidos-detail.component.scss']
})
export class PedidosDetailComponent implements OnInit {
  elem: any;
  ListReportProd;
  
  constructor(public locStorage: LocalStorage,private http2: HttpClient ,public dialogRef: MatDialogRef<PedidosDetailComponent>,public dialog: MatDialog,@Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  
  ngOnInit() {
    this.elem = this.data.elem;
    console.log(this.data)
    this.http2.post("http://tienda724.com:8448/v1/pedidos/detalles",{listaTipos: [this.data.elem.id_BILL]},{}).subscribe(
      response => {
        this.ListReportProd = response;
        console.log(response)
      }
    )
  }





  
genPdf(){
  console.log("THIS I NEED: ",{
    documento: this.elem.numdocumento,
    cliente: this.elem.cliente+"-"+this.elem.tienda,
    fecha: new Date(),
    documento_cliente: this.elem.numpedido,
    telefono: this.elem.telefono,
    correo: this.elem.mail,
    direccion: this.elem.address,
    total: 0 + 0,
    subtotal: 0,
    tax: 0,
    detail_list: this.ListReportProd,
    used_list: []        
  })
  this.http2.post("http://tienda724.com:8448/v1/pedidos/pdf2",{
    documento: this.elem.numdocumento,
    cliente: this.elem.cliente+"-"+this.elem.tienda,
    fecha: new Date(),
    documento_cliente: this.elem.numpedido,
    telefono: this.elem.telefono,
    correo: this.elem.mail,
    direccion: this.elem.address,
    total: 0 + 0,
    subtotal: 0,
    tax: 0,
    detail_list: this.ListReportProd,
    used_list: []        
  },{responseType: 'text'}).subscribe(responsepdf => {
    console.log("THIS IS MY RESPONSE, ",responsepdf);
    window.open("http://tienda724.com/remisiones/"+responsepdf, "_blank");
  })
}


confirmarEnv() {
  this.http2.post("http://tienda724.com:8447/v1/store/confirmarPC?numpedido="+this.elem.numdocumento+"&idstore="+this.locStorage.getIdStore()+"&idthird="+this.locStorage.getIdThird(),{}).subscribe(
    response => {
    }
  )
}






}



export interface DialogData {
  elem: any
}
