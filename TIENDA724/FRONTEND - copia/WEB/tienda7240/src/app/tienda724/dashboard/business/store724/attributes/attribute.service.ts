import { Injectable } from '@angular/core';

import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';
import { HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';


/** Files for auth process  */
import { Urlbase } from '../../../../../shared/urls';
import { LocalStorage } from '../../../../../shared/localStorage';
import { Attribute } from './models/attribute'
import { AttributeComplete } from './models/attributeComplete'

import { AttributeDTO } from './models/attributeDTO'

import { AttributeList } from './models/attributeList'
import { AttributeListDTO } from './models/attributeListDTO'

import { AttributeDetailList } from './models/attributeDetailList'
import { AttributeDetailListDTO } from './models/attributeDetailListDTO'

import { AttributeValue } from './models/attributeValue'
import { AttributeValueDTO } from './models/attributeValueDTO'

@Injectable()
export class AttributeService {

  api_att = Urlbase[2] + '/attributes';
  api_att_list = this.api_att + '/list';
  api_att_list_det = this.api_att_list + '/deatils';
  api_att_value= this.api_att +'/values';
  private options: RequestOptions;
  private headers = new Headers();

  constructor(private http: Http, private locStorage: LocalStorage) {

    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Authorization', this.locStorage.getTokenValue());

    let token = localStorage.getItem('currentUser');
    
        this.options = new RequestOptions({headers:this.headers});
  }
  public getAttribute = (state?: number,id_attribute?:number,id_common?:number, 
    name?: string,description?:string, id_state?:number): Observable<{}|Attribute[]> => {

      let params: URLSearchParams = new URLSearchParams();
      params.set('id_attribute',  id_attribute?""+id_attribute:null);
      params.set('id_common',  id_common?""+id_common:null);
      params.set('name', name?""+name:null);
      params.set('description', description?""+description:null);
      params.set('id_state', id_state?""+id_state:null);
      params.set('state', state?""+state:null);        
      let myOption: RequestOptions = this.options;
      myOption.search = params;
      return this.http.get(this.api_att, this.options )
        .map((response: Response) => <Attribute[]>response.json())
        .catch(this.handleError);
    }

    public getAttributeComplete = (state?: number,is_values?:Boolean,id_attribute?:number,id_common?:number, 
       name?: string,description?:string, id_state?:number): Observable<{}|AttributeComplete[]> => {
  
        let params: URLSearchParams = new URLSearchParams();
        params.set('id_attribute',  id_attribute?""+id_attribute:null);
        params.set('id_common',  id_common?""+id_common:null);
        params.set('name', name?""+name:null);
        params.set('description', description?""+description:null);
        params.set('id_state', id_state?""+id_state:null);
        params.set('state', state?""+state:null);
        params.set('is_values', is_values?""+is_values:null);
          
        let myOption: RequestOptions = this.options;
        myOption.search = params;
        return this.http.get(this.api_att, this.options )
          .map((response: Response) => <AttributeComplete[]>response.json())
          .catch(this.handleError);
      }

    public DeleteAttribute = (id_category: number): Observable<Response|any> => {
      return this.http.delete(this.api_att +'/'+ id_category,this.options)
      .map((response: Response) => {
        if(response){
          return true;
        }else{
  
      return false;
        }})
          .catch(this.handleError);
    }

    public postAttribute = (body: AttributeDTO): Observable<Number|any> => {
      
        return this.http.post(this.api_att , body, { headers: this.headers })
          .map((response: Response) => {

            let id=+response["_body"]

            if(id){

              return id;
            }else{

              return null;
            }
          })
          .catch(this.handleError);
    }


    public getAttributeDetailList = (id_attribute_detail_list?:number,id_attribute_list?:number,
      id_state_attrib_detail_list?: number, state_attrib_detail_list?:number, id_attribute_value?: number,
      id_attribute?: number, id_common_attrib_value?:number, name_attrib_value?: string,
      description_attrib_value?: string, id_state_attrib_value?:number, state_attrib_value?: number): Observable<{}|AttributeDetailList[]> => {

        let params: URLSearchParams = new URLSearchParams();

        params.set('id_attribute_detail_list',  id_attribute_detail_list?""+id_attribute_detail_list:null);
        params.set('id_attribute_list',  id_attribute_list?""+id_attribute_list:null);
        params.set('id_state_attrib_detail_list', id_state_attrib_detail_list?""+id_state_attrib_detail_list:null);
        params.set('state_attrib_detail_list', state_attrib_detail_list?""+state_attrib_detail_list:null);
        params.set('id_attribute_value', id_attribute_value?""+id_attribute_value:null);
        params.set('id_attribute', id_attribute?""+id_attribute:null);
        params.set('id_common_attrib_value', id_common_attrib_value?""+id_common_attrib_value:null);
        params.set('name_attrib_value', name_attrib_value?""+name_attrib_value:null);
        params.set('description_attrib_value', description_attrib_value?""+description_attrib_value:null);
        params.set('id_state_attrib_value', id_state_attrib_value?""+id_state_attrib_value:null);
        params.set('state_attrib_value', state_attrib_value?""+state_attrib_value:null);
        
          
        let myOption: RequestOptions = this.options;
        myOption.search = params;
        return this.http.get(this.api_att_list_det, this.options )
          .map((response: Response) => <AttributeDetailList[]>response.json())
          .catch(this.handleError);
      }
  
      public DeleteAttributeDetailList = (id_category: number): Observable<Response|any> => {
        return this.http.delete(this.api_att_list_det +'/'+ id_category,this.options)
        .map((response: Response) => {
          if(response){
            return true;
          }else{
    
        return false;
          }})
            .catch(this.handleError);
      }
  
      public postAttributeDetailList = (id_attribute_list:number,body: AttributeDetailListDTO[]): Observable<Boolean|any> => {
        let params: URLSearchParams = new URLSearchParams();
        params.set('id_attribute_list',  id_attribute_list?""+id_attribute_list:null);

        let myOption: RequestOptions = this.options;
        myOption.search = params;
        
          return this.http.post(this.api_att_list_det , body,  this.options)
            .map((response: Response) => {
                  if(response){
                    return true;
                  }else{
    
                return false;
              }
            })
            .catch(this.handleError);
      }



      public getAttributeList = (state?: number,id_category?:number,id_common?:number, id_category_father?: number, img_url?:string, name?: string, 
        description?:string, id_state?:number, creation_attribute?: Date, modify_attribute?:Date): Observable<{}|AttributeList[]> => {
    
          //console.log("id_category")
          let params: URLSearchParams = new URLSearchParams();
          params.set('id_category',  id_category?""+id_category:null);
          params.set('id_common',  id_common?""+id_common:null);
          params.set('id_category_father', id_category_father?""+id_category_father:null);
          params.set('img_url', img_url?""+img_url:null);
          params.set('name', name?""+name:null);
          params.set('description', description?""+description:null);
          params.set('id_state', id_state?""+id_state:null);
          params.set('state', state?""+state:null);
          params.set('creation_attribute', creation_attribute?""+creation_attribute:null);
          params.set('modify_attribute', modify_attribute?""+modify_attribute:null);
            
          let myOption: RequestOptions = this.options;
          myOption.search = params;
          return this.http.get(this.api_att_list, this.options )
            .map((response: Response) => <AttributeList[]>response.json())
            .catch(this.handleError);
        }
    
        public DeleteAttributeList = (id_category: number): Observable<Response|any> => {
          return this.http.delete(this.api_att_list +'/'+ id_category,this.options)
          .map((response: Response) => {
            if(response){
              return true;
            }else{
      
          return false;
            }})
              .catch(this.handleError);
        }
    
        public postAttributeList = (body: AttributeListDTO): Observable<Number|any> => {
          
            return this.http.post(this.api_att_list , body, { headers: this.headers })
              .map((response: Response) => {
              
                let id=+response["_body"]
                    if(id){

                      return id;
                    }else{

                      return null;
                }
              })
              .catch(this.handleError);
        }


        public getAttributeValue = (state?: number,id_attribute_value?:number,id_attribute?:number,
          id_common?:number, id_category_father?: number, img_url?:string, name?: string, 
          description?:string, id_state?:number, creation_attribute?: Date, modify_attribute?:Date): Observable<{}|AttributeValue[]> => {
      
            //console.log("id_category")
            let params: URLSearchParams = new URLSearchParams();
            params.set('id_attribute_value',  id_attribute_value?""+id_attribute_value:null);
            params.set('id_attribute',  id_attribute?""+id_attribute:null);
            params.set('id_category_father', id_category_father?""+id_category_father:null);
            params.set('img_url', img_url?""+img_url:null);
            params.set('name', name?""+name:null);
            params.set('description', description?""+description:null);
            params.set('id_state', id_state?""+id_state:null);
            params.set('state', state?""+state:null);
            params.set('creation_attribute', creation_attribute?""+creation_attribute:null);
            params.set('modify_attribute', modify_attribute?""+modify_attribute:null);
              
            let myOption: RequestOptions = this.options;
            myOption.search = params;
            return this.http.get(this.api_att_value, this.options )
              .map((response: Response) => <AttributeValue[]>response.json())
              .catch(this.handleError);
          }
      
          public DeleteAttributeValue = (id_category: number): Observable<Response|any> => {
            return this.http.delete(this.api_att_value +'/'+ id_category,this.options)
            .map((response: Response) => {
              if(response){
                return true;
              }else{
        
            return false;
              }})
                .catch(this.handleError);
          }
      
          public postAttributeValue= (id_attribute:number,body: AttributeValueDTO[]): Observable<Boolean|any> => {

            let params: URLSearchParams = new URLSearchParams();
            params.set('id_attribute',  id_attribute?""+id_attribute:null);
            let myOption: RequestOptions = this.options;
            myOption.search = params;
              return this.http.post(this.api_att_value, body, this.options)
                .map((response: Response) => {
                      if(response){
                        return true;
                      }else{
        
                    return false;
                  }
                })
                .catch(this.handleError);
          }
  



  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert(errMsg);
    return null;//Observable.throw(errMsg);
  }

}
