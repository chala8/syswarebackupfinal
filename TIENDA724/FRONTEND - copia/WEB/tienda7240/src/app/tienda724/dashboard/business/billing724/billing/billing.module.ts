import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatePipe } from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../../../../../app.material';
/*
************************************************
*     principal component
*************************************************
*/

import { BillGeneralComponent } from './bill-data/bill-general/bill-general.component';
import { BillComponent } from './bill/bill.component';
import { DetailBillComponent } from './detail-bill/detail-bill.component';
import { BillDataComponent } from './bill-data/bill-data.component';
import { DialogQuantityBillComponent } from './dialog-quantity-bill/dialog-quantity-bill.component'
import { CurrencyMaskModule } from "ng2-currency-mask";

/*
************************************************
*     modules of  your app
*************************************************
*/

import { InventoriesModule } from '../../store724/inventories/inventories.module';


/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { BillingService} from './billing.service'


import { InventoriesService } from '../../store724/inventories/inventories.service';
import { BarCodeService } from '../../store724/bar-codes/bar-code.service';
import { AttributeService } from '../../store724/attributes/attribute.service';
import { ThirdService } from '../../thirds724/third/third.service';
import { DocumentTypeService } from '../../thirds724/document-type/document-type.service';



import { BillShopComponent } from './bill-shop/bill-shop.component';
import { BillSaleComponent } from './bill-sale/bill-sale.component';
import { BillInputComponent } from './bill-input/bill-input.component';
import { BillOutputComponent } from './bill-output/bill-output.component';
import { BillReturnComponent } from './bill-return/bill-return.component';
import { BillRemissionComponent } from './bill-remission/bill-remission.component';
import { BillHeaderComponent } from './bill-header/bill-header.component';
import { BillDetailComponent } from './bill-detail/bill-detail.component';
import { BillDialogQuantityComponent } from './bill-dialog-quantity/bill-dialog-quantity.component';
import { BillThirdComponent } from './bill-third/bill-third.component';
import { BillDialogThirdComponent } from './bill-dialog-third/bill-dialog-third.component';
import { BillDocumentComponent } from './bill-document/bill-document.component';
import { BillMainComponent } from './bill-main/bill-main.component';
import { QuantityDialogComponent } from './bill-main/quantity-dialog/quantity-dialog.component';
import { ClientDialogComponent } from './bill-main/client-dialog/client-dialog.component';
import { PersonDialogComponent } from './bill-main/person-dialog/person-dialog.component';
import { ThirdDialogComponent } from './bill-main/third-dialog/third-dialog.component';
import { EmployeeDialogComponent } from './bill-main/employee-dialog/employee-dialog.component';
import { SearchClientDialogComponent } from './bill-main/search-client-dialog/search-client-dialog.component';
import { SearchProductDialogComponent } from './bill-main/search-product-dialog/search-product-dialog.component';
import { TransactionConfirmDialogComponent } from './bill-main/transaction-confirm-dialog/transaction-confirm-dialog.component';
import { ThirdModule } from '../../thirds724/third/third.module';
import { PurchaseDetailDialogComponent } from './bill-main/purchase-detail-dialog/purchase-detail-dialog.component';
import { ProductsThirdModule } from '../../store724/products-third/products-third.module';
import { CloseBoxComponent } from './bill-main/close-box/close-box.component';
import { ViewDetailsBoxComponent } from './bill-main/view-details-box/view-details-box.component';
import { CategoriesComponent } from './bill-main/categories/categories.component';
import { InventoryComponent } from './bill-main/inventory/inventory.component';
import { StoresComponent } from './bill-main/stores/stores.component';
import { UpdateLegalDataComponent } from './bill-main/update-legal-data/update-legal-data.component';
import { NewProductStoreComponent } from './bill-main/new-product-store/new-product-store.component';
import { ReportesComponent } from './bill-main/reportes/reportes.component';
import { ProductsOnCategoryComponent } from './bill-main/products-on-category/products-on-category.component';
import { ProductsOnStoreComponent } from './bill-main/products-on-store/products-on-store.component';
import { ProductsOnStoragesComponent } from './bill-main/products-on-storages/products-on-storages.component';
import { AddCategoryComponent } from './bill-main/add-category/add-category.component';
import { AddBrandComponent } from './bill-main/add-brand/add-brand.component';
import { AddMeasureComponent } from './bill-main/add-measure/add-measure.component';
import { AddProductComponent } from './bill-main/add-product/add-product.component';
import { AddCodeComponent } from './bill-main/add-code/add-code.component';
import { AddProductModalComponent } from './bill-main/add-product-modal/add-product-modal.component';
import { InBillComponent } from './in-bill/in-bill.component';
import { OutBillComponent } from './out-bill/out-bill.component';
import { RefundBillComponent } from './refund-bill/refund-bill.component';
import { TransactionConfirmProvDialogComponent } from './transaction-confirm-prov-dialog/transaction-confirm-prov-dialog.component';
import { AddNotesCloseBoxComponent } from './bill-main/add-notes-close-box/add-notes-close-box.component';
import { GenerateThirdComponentComponent } from './bill-main/generate-third-component/generate-third-component.component';
import { GenerateThirdComponent2Component } from './bill-main/generate-third-component2/generate-third-component2.component';
import { BillInventoryComponentComponent } from './bill-main/bill-inventory-component/bill-inventory-component.component';
import { CreatePriceComponent } from './bill-main/create-price/create-price.component';
import { ProductsOnCategoryComponent2 } from './bill-main/products-on-category2/products-on-category2.component';
import { AddGeneralProduct } from './bill-main/add-general-product/add-general-product.component';
import { AddSpecificProductComponent } from './bill-main/add-specific-product/add-specific-product.component';
import { ThirdsComponent } from './bill-main/thirds/thirds.component';
import { EditarCostoComponent } from './bill-main/editar-costo/editar-costo.component';
import { ContabilidadComponent } from './bill-main/contabilidad/contabilidad.component';
import { ReordenComponent } from './bill-main/reorden/reorden.component';
import { PedidosComponent } from './bill-main/pedidos/pedidos.component';
import { PedidosDetailComponent } from './bill-main/pedidos-detail/pedidos-detail.component';
import { PedidosDetail2Component } from './bill-main/pedidos-detail2/pedidos-detail2.component';
import { StatechangeComponent } from './bill-main/statechange/statechange.component';
import { PedidoManualComponent } from './bill-main/pedido-manual/pedido-manual.component';
import { BillUpdateComponent } from './bill-main/bill-update/bill-update.component';
import { ProductsOncategoryUpdateComponent } from './bill-main/products-oncategory-update/products-oncategory-update.component';
import { TopProductsComponent } from './bill-main/top-products/top-products.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { NotesModalComponent } from './bill-main/notes-modal/notes-modal.component';
import { ThirdselectComponent } from './thirdselect/thirdselect.component';
import { NoDispCompComponent } from './bill-main/no-disp-comp/no-disp-comp.component';


/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    InventoriesModule,
    ThirdModule,
    ProductsThirdModule,
    HttpClientModule,  
    CurrencyMaskModule,
    FilterPipeModule,
    Ng2SearchPipeModule
  ],
  declarations: [CloseBoxComponent, EmployeeDialogComponent, PersonDialogComponent, ThirdDialogComponent, BillGeneralComponent, BillComponent, DetailBillComponent, BillDataComponent, DialogQuantityBillComponent, BillShopComponent, BillSaleComponent, BillInputComponent, BillOutputComponent, BillReturnComponent, BillRemissionComponent, BillHeaderComponent, BillDetailComponent, BillDialogQuantityComponent, BillThirdComponent, BillDialogThirdComponent, BillDocumentComponent, BillMainComponent, QuantityDialogComponent, ClientDialogComponent, SearchClientDialogComponent, SearchProductDialogComponent, TransactionConfirmDialogComponent, PurchaseDetailDialogComponent, ViewDetailsBoxComponent, CategoriesComponent, InventoryComponent, StoresComponent, UpdateLegalDataComponent, NewProductStoreComponent, ReportesComponent, ProductsOnCategoryComponent, ProductsOnStoreComponent, ProductsOnStoragesComponent, AddCategoryComponent, AddBrandComponent, AddMeasureComponent, AddProductComponent, AddCodeComponent, AddProductModalComponent, InBillComponent, OutBillComponent, RefundBillComponent, TransactionConfirmProvDialogComponent, AddNotesCloseBoxComponent, GenerateThirdComponentComponent, GenerateThirdComponent2Component, BillInventoryComponentComponent, CreatePriceComponent, ProductsOnCategoryComponent2, AddGeneralProduct, AddSpecificProductComponent, ThirdsComponent, EditarCostoComponent, ContabilidadComponent, ReordenComponent, PedidosComponent, PedidosDetailComponent, PedidosDetail2Component, StatechangeComponent, PedidoManualComponent, BillUpdateComponent, ProductsOncategoryUpdateComponent, TopProductsComponent, NotesModalComponent, ThirdselectComponent, NoDispCompComponent],
  entryComponents: [NoDispCompComponent, ThirdselectComponent,StatechangeComponent,PedidosDetail2Component,PedidosDetailComponent,AddSpecificProductComponent, AddGeneralProduct,ProductsOnCategoryComponent2, BillInventoryComponentComponent, CreatePriceComponent, GenerateThirdComponent2Component, GenerateThirdComponentComponent, AddNotesCloseBoxComponent, TransactionConfirmProvDialogComponent, RefundBillComponent, OutBillComponent, InBillComponent, ReportesComponent, CloseBoxComponent,EmployeeDialogComponent, PersonDialogComponent ,ThirdDialogComponent ,DialogQuantityBillComponent, BillDialogQuantityComponent, BillDialogThirdComponent, BillDocumentComponent,QuantityDialogComponent,ClientDialogComponent,SearchClientDialogComponent,SearchProductDialogComponent,TransactionConfirmDialogComponent,PurchaseDetailDialogComponent, ViewDetailsBoxComponent,CategoriesComponent, InventoryComponent, StoresComponent, UpdateLegalDataComponent, NewProductStoreComponent, ProductsOnCategoryComponent,ProductsOnStoreComponent, ProductsOnStoragesComponent, AddCategoryComponent, AddBrandComponent,AddMeasureComponent, AddProductComponent, AddCodeComponent, AddProductModalComponent,EditarCostoComponent,TopProductsComponent,NotesModalComponent],
  providers:[DatePipe,BillingService,InventoriesService, BarCodeService, AttributeService, ThirdService, DocumentTypeService]

})
export class BillingModule { }