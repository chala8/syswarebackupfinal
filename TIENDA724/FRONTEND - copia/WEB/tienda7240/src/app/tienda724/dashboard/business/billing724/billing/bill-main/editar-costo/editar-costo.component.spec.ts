import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarCostoComponent } from './editar-costo.component';

describe('EditarCostoComponent', () => {
  let component: EditarCostoComponent;
  let fixture: ComponentFixture<EditarCostoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarCostoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarCostoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
