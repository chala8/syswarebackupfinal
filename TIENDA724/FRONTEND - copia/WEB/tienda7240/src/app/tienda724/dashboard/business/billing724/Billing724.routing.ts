import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { OptionsComponent } from './options/options.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/

import { WayToPayRouting } from './way-to-pay/way-to-pay.routing';
import { PaymentStateRouting } from './payment-state/payment-state.routing';
import { PaymentMethodRouting } from './payment-method/payment-method.routing';
import { BillingRouting } from './billing/billing.routing';
import { BillTypeRouting } from './bill-type/bill-type.routing';
import { BillStateRouting } from './bill-state/bill-state.routing';


export const Billing724Routing: Routes = [
  
    { path: 'movement', component: null,
      children: [
          { path: '', redirectTo: 'options', pathMatch: 'full'},
          { path: 'options', component: OptionsComponent},
          ...WayToPayRouting,
          ...PaymentStateRouting,
          ...PaymentMethodRouting,
          ...BillingRouting,
          ...BillTypeRouting,
          ...BillStateRouting,
  
      ]
    }
  
  ]