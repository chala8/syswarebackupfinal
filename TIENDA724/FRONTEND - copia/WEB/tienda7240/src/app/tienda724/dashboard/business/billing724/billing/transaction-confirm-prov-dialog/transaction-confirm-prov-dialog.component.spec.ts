import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionConfirmProvDialogComponent } from './transaction-confirm-prov-dialog.component';

describe('TransactionConfirmProvDialogComponent', () => {
  let component: TransactionConfirmProvDialogComponent;
  let fixture: ComponentFixture<TransactionConfirmProvDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionConfirmProvDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionConfirmProvDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
