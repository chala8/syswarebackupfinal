import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


/*
************************************************
*    Material modules for app
*************************************************
*/
import {  MaterialModule } from '../../../../../app.material';

/*
************************************************
*     principal component
*************************************************
*/


/*
************************************************
*     modules of  your app
*************************************************
*/




/*
************************************************
*     routing of  your app
*************************************************
*/

/*
************************************************
*     services of  your app
*************************************************
*/
import { LocalStorage } from '../../../../../shared/localStorage';
import { ReportsComponent } from './reports.component';
import { GraphicsComponent } from './graphics/graphics.component';
import { TableComponent } from './table/table.component';
import { FiltersComponent } from './filters/filters.component';
import { MenuComponent } from './menu/menu.component';

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/

@NgModule({
  
    imports: [
      CommonModule,
      RouterModule,
      FormsModule,
      ReactiveFormsModule,
      MaterialModule,  
    ],
    providers:[LocalStorage],
    declarations: [ReportsComponent,GraphicsComponent, TableComponent, FiltersComponent, MenuComponent],
    exports:[]
})
export class ReportsModule { }
