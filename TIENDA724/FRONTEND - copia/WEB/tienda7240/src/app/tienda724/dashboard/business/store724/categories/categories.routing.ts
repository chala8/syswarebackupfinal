import { Routes } from '@angular/router';

/*
************************************************
*     principal component
*************************************************
*/
import { CategoryListComponent } from './category-list/category-list.component';

import { CategoryDataComponent } from './category-data/category-data.component';
import { CategoryNewComponent } from './category-new/category-new.component';
import { CategoryEditComponent } from './category-edit/category-edit.component';

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/

export const CategoriesRouting: Routes = [
  
    { path: 'category', component: null,
      children: [
          { path: '', redirectTo: 'list', pathMatch: 'full'},
          { path: 'list', component: CategoryListComponent},
          { path: 'data', component: CategoryDataComponent},
          { path: 'new', component: CategoryNewComponent},
          { path: 'edit/:id', component: CategoryEditComponent}
  
      ]
    }
  
  ]
