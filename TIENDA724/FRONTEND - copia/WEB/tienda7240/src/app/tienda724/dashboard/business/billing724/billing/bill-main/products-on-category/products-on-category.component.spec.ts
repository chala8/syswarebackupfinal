import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsOnCategoryComponent } from './products-on-category.component';

describe('ProductsOnCategoryComponent', () => {
  let component: ProductsOnCategoryComponent;
  let fixture: ComponentFixture<ProductsOnCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsOnCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsOnCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
