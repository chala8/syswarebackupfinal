import { Component, OnInit, ElementRef } from '@angular/core';
import { ROUTES } from '../../../components/sidebar/sidebar.component';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../authentication/authentication.service';

import { LocalStorage } from '../../../shared/localStorage';
import { Token } from '../../../shared/token';
import { Person } from '../../../shared/models/person';

import { Third } from '../../dashboard/business/thirds724/third/models/third';


declare var $: any;

@Component({
  selector: 'app-my',
  templateUrl: './my.component.html',
  styleUrls: ['./my.component.scss']
})
export class MyComponent implements OnInit {
  private toggleButton: any;
  user=false;
  private sidebarVisible: boolean;    
  thirdFather:Third
  person:Person;
  
  token:Token;
  location: Location;

constructor(location: Location,  private element: ElementRef,
          private authService:AuthenticationService,
          private router:Router,
          private locStorage:LocalStorage) {
this.location = location;
    this.sidebarVisible = false;
}

  ngOnInit() {

    let session=this.locStorage.getSession();
    if(!session){
      /**
      @todo Eliminar comentario para
      */
     this.goIndex();
    }else{

        let link = ['/dashboard/business/movement/billing/main'];
        this.router.navigate(link);

    //  const navbar: HTMLElement = this.element.nativeElement;
    //  this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];

    //   this.token=this.locStorage.getToken();
    //   this.person= this.locStorage.getPerson();

    //   this.token=this.locStorage.getToken();
    //   this.thirdFather=this.locStorage.getThird();

    } 
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
        return true;
    }
    return false;
};

sidebarToggle2() {
  // const toggleButton = this.toggleButton;
  // const body = document.getElementsByTagName('body')[0];
  
  this.sidebarClose();
};

sidebarClose() {
  const body = document.getElementsByTagName('body')[0];
  this.toggleButton.classList.remove('toggled');
  this.sidebarVisible = false;
  body.classList.remove('nav-open');
};

goIndex() {
  let link = ['/'];
  this.router.navigate(link);
}
logout() {
  this.locStorage.cleanSession();
  this.token=null;
  this.goIndex();
  
  this.showNotification('top','right');

 }

 showNotification(from, align){
  const type = ['','info','success','warning','danger'];

  const color = Math.floor((Math.random() * 4) + 1);

  $.notify({
      icon: "notifications",
      message: "Usted <b>Cerro Sesión</b> de forma satisfactoria."

  },{
      type: type[2],
      timer: 200,
      placement: {
          from: from,
          align: align
      }
  });
}

}
