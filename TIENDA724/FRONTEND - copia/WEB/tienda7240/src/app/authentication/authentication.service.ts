import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

/** Files for auth process  */
import { Urlbase } from '../shared/urls';
import { LocalStorage } from '../shared/localStorage';
import { Auth } from './auth';
import { UsuarioDTO } from '../shared/models/auth/UsuarioDTO';

import { Token } from '../shared/token';
import { Session } from '../shared/session';


@Injectable()
export class AuthenticationService {
  username: string;
  loggedIn: boolean;
  token: Token;
  session: Session;
  urlAuth = Urlbase[0] + '/usuarios';
  private options: RequestOptions;
  private headers = new Headers({ 'Content-Type': 'application/json' });

 
  constructor(private http: Http, private locStorage: LocalStorage) { }

  public login = (auth: Auth): Observable<Boolean> => {
  

    return this.http.post(this.urlAuth + '/login?id_aplicacion=' + 21 + '&dispositivo=WEB', auth, { headers: this.headers })
      .map((response: Response) => {

        let session = response.json();




        if (session) {
          this.token = session;
          this.session = session;
          if (this.session.person) {
            localStorage.setItem('currentUserPersonStore724', JSON.stringify(this.session.person));
          }
       


          localStorage.setItem('currentUserSessionStore724', JSON.stringify(this.session));

          localStorage.setItem('currentUserTokenStore724', JSON.stringify(this.token));
          localStorage.setItem('currentUserMenuStore724', JSON.stringify(this.session['menus']));
          localStorage.setItem('currentUserRolStore724', JSON.stringify(this.session['roles']));

          this.loggedIn = true;


          return true;
        } else {

          alert("Usuario o Contraseña Incorrecto")
          this.loggedIn = false;
          return false;
        }
      })
      .catch(this.handleError);
  }

  public postUserAUTH = (auth: UsuarioDTO): Observable<Number|any> => {
    return this.http.post(this.urlAuth, auth, { headers: this.headers })
      .map((response: Response) => {

        let UUID = response.json();
        if (UUID) {
        

          return UUID;
        } else {

          return null;
        }
      })
      .catch(this.handleError);
  }

  public register = (auth: Auth): Observable<Boolean> => {
    return this.http.post(this.urlAuth, auth, { headers: this.headers })
      .map((response: Response) => {

        let token = response.json();
        if (token) {

          return true;
        } else {

          return false;
        }
      })
      .catch(this.handleError);
  }

  public logout = (): Observable<Boolean> => {
    if (this.locStorage.isSession()) {
      let headers = new Headers({
        'Content-Type': 'application/json'
        // 'Authorization' :  this.locStorage.getTokenValue()
      });
      this.options = new RequestOptions({ headers });
      let params: URLSearchParams = new URLSearchParams();
      // params.set('id_user', '' + this.locStorage.getIdUsuarioApp());
      //params.set('key_token', this.locStorage.getTokenValue());
      this.options.search = params;
      return this.http.delete(this.urlAuth, this.options)
        .map((response: Response) => {
          this.locStorage.cleanSession();
          return true;
        })
        .catch(this.handleError);
    }

  }

  isLoggedIn() {
    return this.loggedIn;
  }

  private handleError(error: Response | any): Observable<Boolean> {
    //alert("Usuario o Contraseña Innnnncorrecto")
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;

    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    alert("Usuario o Contraseña Erroneos.");
    return Observable.throw(false);
    //return Observable.throw(errMsg);
  }

}
