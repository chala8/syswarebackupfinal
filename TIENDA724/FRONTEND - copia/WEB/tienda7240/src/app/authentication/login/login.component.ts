import { Component, OnInit, ViewEncapsulation,Pipe } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import 'rxjs/add/operator/filter';

/*
*     others component
*/

/*
*     models for  your component
*/

import { UserThird } from '../../shared/userThird'
import { Token } from '../../shared/token';
import { Person } from '../../shared/models/person'
import { Employee } from '../../shared/models/employee'
import { Third } from '../../tienda724/dashboard/business/thirds724/third/models/third';
import { CommonThird } from '../../tienda724/dashboard/business/commons/CommonThird';

import { Auth } from '../auth';


/*
*     services of  your component
*/
import { LocalStorage } from '../../shared/localStorage';
import { AuthenticationService } from '../authentication.service';
import { PersonService } from '../../tienda724/dashboard/business/thirds724/person/person.service'
import { UserThirdService } from '../../tienda724/dashboard/business/thirds724/user-third/user-third.service'
import { ThirdService } from '../../tienda724/dashboard/business/thirds724/third/third.service';


/********************************
 *          Constantes
 ********************************/
declare var $:any

@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  title: string = 'Iniciar Session';
  isLoggedIn: boolean = false;
  error: string = '';
  form: FormGroup;
  action: string = 'Iniciar Sesion'
  auth: Auth;

  token:Token;
  UUID:string;
  
  userThird:UserThird[];
  person:Person;
  userThirdQ:UserThird;
  userThirdAux:UserThird;
  thirdList:Third[];
  thirdFather:Third;
  commonThird:CommonThird;
  employee:Employee;



  constructor(public location: Location,  
              private _router: Router,
              private userThirdService: UserThirdService,
              private personService: PersonService,
              public thirdService:ThirdService,
              private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              public locStorage: LocalStorage,
              private authService: AuthenticationService) {

    this.auth = new Auth(null, null);
    this.commonThird= new CommonThird(null,null,null,null);
    this.userThirdQ= new UserThird(null,null,null,this.commonThird);
    this.userThirdAux=this.userThirdQ;
    this.person= new Person(null,null,null,null,null,null,null,this.commonThird,null,null,this.userThirdQ)
    this.employee=new Employee(null,null,this.commonThird)

  }

  ngOnInit() {
  
    this.controlsCreate();

    let session=this.locStorage.getSession();
    if(!session){
      /**
      @todo Eliminar comentario para
      */
        
    }else{
      
      this.router.navigate(['/my']);
      

    }

      
        
  }

  goReg() {
    let link = ['/auth/register'];
    this.router.navigate(link);
  }

  // start controls from from
  controlsCreate() {
    this.form = this.fb.group({
      usuario: ['', Validators.required],
      clave: ['', Validators.required],
    });
  }
  loadData(){

  }

  login() {
    this.locStorage.cleanSession();
    this.auth.usuario = this.form.value['usuario'];
    this.auth.clave = this.form.value['clave'];

    this.authService.login(this.auth)
      .subscribe(
      result => {
        if (result === true) {
          this.showNotification('top','center',3,"<p> Procesando datos, por favor espere  <p> <br> ",'info');
          
          this.token=this.locStorage.getToken();
          this.UUID=""+this.token.id_usuario;
          
        
          
          if(!this.locStorage.getPerson()){
            this.userThirdQ.UUID=this.UUID;
            this.loadUserThird(this.userThirdQ);
          }
        } else {
          this.showNotification('top','right',3,"<h1>USUARIO Y/O CLAVE INCORRECTO</h1>",'warning');

          //this.openDialog();
          return;
        }
      })




  }

  cargarPerfil() {

    if(this.person['info']['fullname']!=null && this.person['info']['fullname']!=undefined && this.person['info']['fullname']!=""){
      this.showNotification('top','center',3,"<h3> Bienvenido!  <h3> "+this.person['info']['fullname'],'success');
    }else{
      this.showNotification('top','center',3,"<h3> Bienvenido!  <h3> "+this.person['first_name']+" "+this.person['first_lastname'],'success');
    }

    
    

    
  
    this.router.navigate(['/dashboard/business']);

  }
  loadUserThird(user:UserThird){
    
    this.userThirdService.getUserThirdList(user)
    .subscribe((data: UserThird[]) => this.userThird = data,
      error => console.log(error),
            () => {
              if(this.userThird.length>0){

                if(this.userThird.length>0){
                  this.getThird(null,null,null,null,null,null,this.userThird[0].id_person)
                }
              
              }
          });
  }

  volver() {
    let link = ['/'];
    this.router.navigate(link);

  }

      // Notification
      showNotification(from, align,id_type?, msn?, typeStr?){
        const type = ['','info','success','warning','danger'];
    
        const color = Math.floor((Math.random() * 4) + 1);
    
        $.notify({
            icon: "notifications",
            message: msn?msn:"<b>Noficación automatica </b>"
    
        },{
            type: typeStr? typeStr:type[id_type?id_type:2],
            timer: 200,
            placement: {
                from: from,
                align: align
            }
        });
      }
  // services
     // GET /Thirds
     getThird(id_third?: number, id_third_father?: number, document_type?: number, document_number?: string,
      
          id_doctype_person?: number, doc_person?: string,id_person?:number): void {
      
      
          this.thirdService.getThirdList(id_third, id_third_father, document_type, document_number, id_doctype_person, doc_person, null, null,id_person)
            .subscribe((data: Third[]) => this.thirdList = data,
            error => console.log(error),
            () => {
              if(this.thirdList.length>0){
                let father:Third;
                father=this.thirdList[0];
                localStorage.setItem('currentThirdPersonStore724',JSON.stringify(father));
             
                Object.assign(this.person,this.thirdList[0].profile );
                Object.assign(this.userThirdAux,this.thirdList[0].profile['uuid'][0] );
                Object.assign(this.employee,this.thirdList[0].profile['employee'][0] );

                this.person.uuid=this.userThirdAux;
                this.person.employee=this.employee;
                this.token.id_third=this.thirdList[0].id_third?this.thirdList[0].id_third:null;
                this.token.id_third_father=this.thirdList[0].id_third_father?this.thirdList[0].id_third_father:null;
                localStorage.setItem('currentUserPersonStore724', JSON.stringify(this.person));
                localStorage.setItem('currentUserTokenStore724',JSON.stringify(this.token))
                
                if(this.token.id_third_father!=null && this.token.id_third_father>0){
                  this.getThirdStore(this.token.id_third_father)

                }else{
                  this.cargarPerfil();
                }
                

              }
            });
        }
        getThirdStore(id_third?: number, id_third_father?: number, document_type?: number, document_number?: string,
          
              id_doctype_person?: number, doc_person?: string,id_person?:number): void {
          
          
              this.thirdService.getThirdList(id_third, id_third_father, document_type, document_number, id_doctype_person, doc_person, null, null,id_person)
                .subscribe((data: Third[]) => this.thirdList = data,
                error => console.log(error),
                () => {
                  if(this.thirdList.length>0){
                    let father:Third;
                    father=this.thirdList[0];
                    localStorage.setItem('currentThirdFatherStore724',JSON.stringify(father));
                    this.cargarPerfil();    
                  }
                });
            }

        
        

}
