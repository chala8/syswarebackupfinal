import { TestBed, inject } from '@angular/core/testing';

import { StoreSelectorService } from './store-selector.service';

describe('StoreSelectorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StoreSelectorService]
    });
  });

  it('should be created', inject([StoreSelectorService], (service: StoreSelectorService) => {
    expect(service).toBeTruthy();
  }));
});
