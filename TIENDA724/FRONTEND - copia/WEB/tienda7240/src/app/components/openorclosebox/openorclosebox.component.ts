import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-openorclosebox',
  templateUrl: './openorclosebox.component.html',
  styleUrls: ['./openorclosebox.component.scss']
})
export class OpenorcloseboxComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<OpenorcloseboxComponent>) { }

  ngOnInit() {
  }

  close(resp){
    this.dialogRef.close(
      resp
    )

  }

}
