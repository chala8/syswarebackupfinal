import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CurrencyMaskModule } from "ng2-currency-mask";

import {  MaterialModule } from '../app.material';


import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { OpenBoxComponent } from './open-box/open-box.component';
import { FormsModule } from '@angular/forms';
import { OpenorcloseboxComponent } from './openorclosebox/openorclosebox.component';
import { SelectboxComponent } from './selectbox/selectbox.component';
import { StoreSelectorService } from "../components/store-selector.service";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FormsModule,
    CurrencyMaskModule
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    OpenBoxComponent,
    OpenorcloseboxComponent,
    SelectboxComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    OpenBoxComponent
  ],
  providers:[
    StoreSelectorService,
  ],
  entryComponents:[OpenBoxComponent,OpenorcloseboxComponent,SelectboxComponent]
})
export class ComponentsModule { }
