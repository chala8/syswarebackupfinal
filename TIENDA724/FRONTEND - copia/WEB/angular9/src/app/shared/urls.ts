let UsarProd = window.location.href.includes("tienda724.com")||true;

export const Urlbase = {
  //auth [0]
  auth:'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8449',
  // tercero [1]
  tercero:'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8446/v1',
  // tienda [2]
  tienda:'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8447/v1',
  // billing/facturación [3]
  facturacion:'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8448/v1',
  // order [4]
  order:'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8450/v1',
  // cierreCaja [5]
  cierreCaja:'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8451/v1',
  // remisiones [6],
  remisiones:'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/remisiones',
  // facturas [7],
  facturas:'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/facturas',
  // imagenes [8],
  imagenes:'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/imagenes',
  // logos [9],
  logos:'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/logos',
  // reportes [10],
  reportes:'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/reportes',
  // [11],
  root:'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/',
}

export const Urlbase_Old = [
  //auth [0]
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8449',
  // tercero [1]
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8446/v1',
  // tienda [2]
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8447/v1',
  // billing/facturación [3]
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8448/v1',
  // order [4]
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8450/v1',
  // cierreCaja [5]
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+':8451/v1',
  // remisiones [6],
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/remisiones',
  // facturas [7],
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/facturas',
  // imagenes [8],
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/imagenes',
  // logos [9],
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/logos',
  // reportes [10],
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/reportes',
  // [11],
  'http://'+(UsarProd ? 'tienda724.com':'3.234.156.197')+'/',
];

//3.234.156.197
