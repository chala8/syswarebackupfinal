import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { PedidosDomiciliosCanceladosComponent } from '../pedidos-domicilios-cancelados/pedidos-domicilios-cancelados.component';
import { PedidosDomiciliosRecibidosComponent } from '../pedidos-domicilios-recibidos/pedidos-domicilios-recibidos.component';
import { PedidosDomiciliosProcesadosSComponent } from '../pedidos-domicilios-procesados-s/pedidos-domicilios-procesados-s.component';
import { PedidosDomiciliosProcesadosNComponent } from '../pedidos-domicilios-procesados-n/pedidos-domicilios-procesados-n.component';
import { PedidosDomiciliosEncaminoComponent } from '../pedidos-domicilios-encamino/pedidos-domicilios-encamino.component';
import { PedidosDomiciliosEntregadosSComponent } from '../pedidos-domicilios-entregados-s/pedidos-domicilios-entregados-s.component';
import { PedidosDomiciliosEntregadosNComponent } from '../pedidos-domicilios-entregados-n/pedidos-domicilios-entregados-n.component';
import { PedidosFacturadosComponent } from '../pedidos-facturados/pedidos-facturados.component';
import { PedidosDomiciliosFinalizadosSComponent } from '../pedidos-domicilios-finalizados-s/pedidos-domicilios-finalizados-s.component';
import { PedidosDomiciliosFinalizadosNComponent } from '../pedidos-domicilios-finalizados-n/pedidos-domicilios-finalizados-n.component';
import {FCMservice} from '../../../../../../../services/fcmservice.service';
import {NgxCoolDialogsService} from 'ngx-cool-dialogs';
import {valueReferenceToExpression} from '@angular/compiler-cli/src/ngtsc/annotations/src/util';
import {CPrint} from '../../../../../../../shared/util/CustomGlobalFunctions';
import * as jQuery from 'jquery';

import 'bootstrap-notify';
let $: any = jQuery;

@Component({
  selector: 'app-domicilios',
  templateUrl: './domicilios.component.html',
  styleUrls: ['./domicilios.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DomiciliosComponent implements OnInit {

  VariableSuscriptor:any = null;

  constructor(private fcmService: FCMservice,private coolDialogs: NgxCoolDialogsService) { }

  ngOnInit(): void {
    localStorage.setItem("currentBox",String(2));
    this.VariableSuscriptor = this.fcmService.currentMessage.subscribe((message:any) => {
      console.log("Mensaje FCM entrante:");
      console.log(message);
      this.showNotification('top','right',3,"Notificación entrante <p>"+message.message+"</p>",'info');
    });
  }
  ngOnDestroy() {
    console.log("Se elimina el suscribe")
    //prevent memory leak when component destroyed
    this.VariableSuscriptor.unsubscribe();
  }

  @ViewChild("cancelados") compUpdate: PedidosDomiciliosCanceladosComponent;
  @ViewChild("recibidos") compUpdate2: PedidosDomiciliosRecibidosComponent;
  @ViewChild("procesadosSinNovedad") compUpdate3: PedidosDomiciliosProcesadosSComponent;
  @ViewChild("procesadosNovedad") compUpdate4: PedidosDomiciliosProcesadosNComponent;
  @ViewChild("entregadoSinNovedad") compUpdate5: PedidosDomiciliosEntregadosSComponent;
  @ViewChild("entregadoConNovedad") compUpdate6: PedidosDomiciliosEntregadosNComponent;
  @ViewChild("enCamino") compUpdate7: PedidosDomiciliosEncaminoComponent;
  @ViewChild("facturados") compUpdate8: PedidosFacturadosComponent;
  @ViewChild("finalizadoSinNovedad") compUpdate9: PedidosDomiciliosFinalizadosSComponent;
  @ViewChild("finalizadoConNovedad") compUpdate10: PedidosDomiciliosFinalizadosNComponent;


  SelectedTabIndexGlobal = 0;
  SelectedTabIndexProcesados = 0;
  SelectedTabIndexEntregados = 0;
  SelectedTabIndexFinalizados=0;

  setTabIndexGlobal(index){
    this.SelectedTabIndexGlobal = index;
  }

  setTabIndexProcesados(index){
    this.SelectedTabIndexProcesados = index;
  }

  setTabIndexEntregados(index){
    this.SelectedTabIndexEntregados = index;
  }

  setTabIndexFinalizados(index){
    this.SelectedTabIndexFinalizados = index;
  }


  async SendFCMpush(title,body,data,id_third,nombre){
    try {
      //Ajuste
      let Llaves = Object.keys(data);
      for(let n = 0;n<Llaves.length;n++){
        data[Llaves[n]] += "";//esto es para estar seguros de que todo se vaya como string
      }
      //
      let Resultado = await this.fcmService.SendMessageToThird(title,body,data,id_third);
      if(Resultado != "Empty"){
        this.showNotification('top','right',3,"Notificación enviada a <p>"+nombre+"</p>",'success');
      }
    }catch (e) {
      CPrint(e);
      this.showNotification('top','right',3,"No se pudo enviar la notificación a  <p>"+nombre+"</p>",'danger');
    }
  }

  // Notification
  showNotification(from, align,id_type?, msn?, typeStr?){
    const type = ['','info','success','warning','danger'];

    const color = Math.floor((Math.random() * 4) + 1);

    $.notify({
      icon: "notifications",
      message: msn?msn:"<b>Noficación automatica </b>"

    },{
      type: typeStr? typeStr:type[id_type?id_type:2],
      timer: 200,
      placement: {
        from: from,
        align: align
      }
    });
  }
}
