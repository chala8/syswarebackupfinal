import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlbase } from 'src/app/classes/urls';
import { SessionManagerService } from 'src/app/services/sessionManager/session-manager.service';

@Component({
  selector: 'app-confirm-loggin',
  templateUrl: './confirm-loggin.component.html',
  styleUrls: ['./confirm-loggin.component.scss']
})
export class ConfirmLogginComponent implements OnInit {

  idmesa: string = "";
  idstore: string = "";
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Validate' : 'Validate'
  });

  constructor(private http: HttpClient,
              private router: Router,
              private route: ActivatedRoute,private spinner: NgxSpinnerService,
              private sessionManager : SessionManagerService,) {
                this.route.queryParams.subscribe(params => {
                  this.idstore = params['idstore'];
                });

  }

  ngOnInit() {
    this.spinner.show();
    this.http.post(Urlbase.auth+"/login?id_aplicacion=21&dispositivo=WEB",{'usuario':"73214195",'clave':"123"},{ headers: this.headers,withCredentials:true }).subscribe(loginResult =>{
      console.log("LOGIN RESULT")
      console.log(loginResult)
      localStorage.setItem('ID', btoa(JSON.stringify(loginResult)));
      localStorage.setItem("user","73214195")
      //@ts-ignore
      localStorage.setItem('at', btoa(loginResult.token));
      this.sessionManager.setIdStore(this.idstore);
      this.goToLoader();
      // const result = await this.http.get(Urlbase.tienda+"/store/getStoreNameById?idStore="+idstore,{ headers: this.headers,withCredentials:true, responseType: 'text' }).toPromise();
      // this.setStoreName(result);
    })
  }

  goToLoader(){
    this.router.navigate(['loader']);

  }

  goToCatalog(){
    this.sessionManager.setIdStore(this.idstore);

    this.router.navigate(['catalog']);
  }

  returnStoreName(){
    return this.sessionManager.returnStoreName();
  }

}
