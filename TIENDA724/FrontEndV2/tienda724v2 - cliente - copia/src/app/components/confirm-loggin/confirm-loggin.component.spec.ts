import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmLogginComponent } from './confirm-loggin.component';

describe('ConfirmLogginComponent', () => {
  let component: ConfirmLogginComponent;
  let fixture: ComponentFixture<ConfirmLogginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmLogginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmLogginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
