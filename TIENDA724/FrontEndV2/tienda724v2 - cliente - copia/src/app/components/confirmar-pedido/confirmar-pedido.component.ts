import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { SessionManagerService } from 'src/app/services/sessionManager/session-manager.service';

@Component({
  selector: 'app-confirmar-pedido',
  templateUrl: './confirmar-pedido.component.html',
  styleUrls: ['./confirmar-pedido.component.scss']
})
export class ConfirmarPedidoComponent implements OnInit {

  constructor(private httpClient: HttpClient,
    private sessionManager : SessionManagerService,) { }

  inventory:any = "";
  inventory2:any = "";
  at:any = "";
  envio = 10000;
  total = 0;

  ngOnInit(): void {
    //@ts-ignore
    this.inventory = JSON.parse(localStorage.getItem("inventory")).item
    console.log(this.inventory);
    //@ts-ignore
    this.inventory2 = JSON.parse(localStorage.getItem("inventory2")).item
    console.log(this.inventory2);
    this.at = this.sessionManager.returnIdObject();
    console.log("MY OBJECT");
    console.log(this.at);

    for(let a =0; a<this.inventory.length;a++){
      let tmpElem = this.inventory[a];
      this.total+=(tmpElem.quantity*tmpElem.priceGen)
    }


  }


  confirm(){
    let itemString = "";
    for(let i = 0; i<this.inventory.length;i++){
      let invItem = this.inventory[i];
      itemString+="{"+invItem.id_product_third+","+invItem.pricelist[0].price+","+invItem.quantity+","+invItem.tax_product+"},"
    }


    let detailList = '';
    //GENERO LA LISTA DE DTOs DE DETALLES
    let notes = "";
    let detailListOpciones = '';
    this.inventory2.forEach((item:any) => {

      if(item.opciones != [] ||  item.opciones.length!=0){
        item.opciones.forEach((elem:any) => {
          console.log(elem.check)
          if(elem.check){
            notes = notes +  "opciones=" + elem.opcion+";";
          }
        })
      }

      notes=notes+"opciones="+item.text+";";


      if(notes==""){
        detailListOpciones = detailListOpciones+ "{"+item.id_product_third+",0},"
      }else{
        detailListOpciones = detailListOpciones+ "{"+item.id_product_third+","+notes.substring(0,notes.length-1)+"},"
      }


      notes=""

    });

    console.log(detailListOpciones.substring(0,detailListOpciones.length-1))

    console.log(itemString.substring(0,itemString.length-1))

    this.httpClient.post("https://tienda724.com:8448/v1/pedidos/crearPedidoAppOpciones?idapp=26&idstoreclient=10&idthirduseraapp="+this.at.id_third+"&idstoreprov="+this.sessionManager.returnIdStore()+"&detallepedido="+itemString.substring(0,itemString.length-1)+"&descuento=0&detallepedidomesa="+detailListOpciones.substring(0,detailListOpciones.length-1),{},{}).subscribe(
      response =>{

      }
    )
  }


  calculateTotal(){

  }

}
