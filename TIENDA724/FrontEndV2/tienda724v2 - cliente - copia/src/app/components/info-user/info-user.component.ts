import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionManagerService } from 'src/app/services/sessionManager/session-manager.service';

@Component({
  selector: 'app-info-user',
  templateUrl: './info-user.component.html',
  styleUrls: ['./info-user.component.scss']
})
export class InfoUserComponent implements OnInit {

  user : string | null = "";
  token : any = "";

  constructor(private sessionManager : SessionManagerService,
              private router: Router,) { }

  ngOnInit(): void {
    this.user = localStorage.getItem("user");
    console.log(this.sessionManager.returnIdObject());
    this.token = this.sessionManager.returnIdObject();
  }

  goToLogin(){}

  goToMenu(){
    this.router.navigate(['addproductsmesa']);
  }

  goToPedidos(){
    this.router.navigate(['pedidos']);
  }

  goToPedidosActivos(){
    this.router.navigate(['pedidosActivos']);
  }

  returnRoles(){
    let roles = "";
    this.token.roles.forEach((element:any) => {
      roles+=element.rol+",";
    });
    return roles.substring(0,roles.length-1);
  }

  returnDocument(){
    return this.token.third.document_type +" - "+this.token.third.document;
  }
}
