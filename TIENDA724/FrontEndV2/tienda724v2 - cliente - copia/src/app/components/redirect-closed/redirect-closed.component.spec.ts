import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedirectClosedComponent } from './redirect-closed.component';

describe('RedirectClosedComponent', () => {
  let component: RedirectClosedComponent;
  let fixture: ComponentFixture<RedirectClosedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedirectClosedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RedirectClosedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
