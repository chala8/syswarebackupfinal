import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { SessionManagerService } from 'src/app/services/sessionManager/session-manager.service';

@Component({
  selector: 'app-redirect-closed',
  templateUrl: './redirect-closed.component.html',
  styleUrls: ['./redirect-closed.component.scss']
})
export class RedirectClosedComponent implements OnInit {

  constructor(public router: Router, private sessionManager: SessionManagerService,) { }

  ngOnInit(): void {
  }

  next(){
    window.location.href = "https://restobar724.com";
  }
}
