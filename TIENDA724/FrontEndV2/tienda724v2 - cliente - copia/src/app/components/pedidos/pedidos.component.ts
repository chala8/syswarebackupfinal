import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Urlbase } from 'src/app/classes/urls';
import { SessionManagerService } from 'src/app/services/sessionManager/session-manager.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss']
})
export class PedidosComponent implements OnInit {

  user : string | null = "";
  token : any = "";
  pedidoList: any = "";

  constructor(private sessionManager : SessionManagerService,
              private router: Router,
              private httpClient : HttpClient,) { }

  ngOnInit(): void {
    this.httpClient.get(Urlbase.facturacion+"/pedidos/getPedidos/third?idapp=26&idthirdclient="+this.sessionManager.returnIdObject().id_third+"&id_bill_type=86&id_bill_state=808|804|806|705|99").subscribe(
      element=>{
        this.pedidoList = element;
        console.log(element)
      }
    );
    this.user = localStorage.getItem("user");
    console.log(this.sessionManager.returnIdObject());
    this.token = this.sessionManager.returnIdObject();
  }

  goToLogin(){}

  goToMenu(){
    this.router.navigate(['addproductsmesa']);
  }

  goToInfoUser(){
    this.router.navigate(['userInfo']);
  }

  goToPedidosActivos(){
    this.router.navigate(['pedidosActivos']);
  }


}
