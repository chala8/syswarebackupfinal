import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Urlbase } from 'src/app/classes/urls';
import { SessionManagerService } from 'src/app/services/sessionManager/session-manager.service';
import { environment } from 'src/environments/environment';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {

  constructor(private router: Router,private httpClient: HttpClient, private sessionManager : SessionManagerService,) { }

  headers = new HttpHeaders({
    'Content-Type':  'application/json',
  });

  headers2 = new HttpHeaders({
    'skip':  'skip',
  });
  fnombre: String = "";
  snombre: String = "";
  fapellido: String = "";
  sapellido: String = "";
  docType: String = "3";
  docList: any [] = [];
  numDoc: String = "";
  password: string = "";
  cel: String = "";
  email: string = "";
  dir1: String = "Avenida";
  dir2: String = "";
  dir3: String = "";
  dir4: String = "";
  dirOpcional: String = "";
  city: String = "";
  cityList: any [] = [];
  mapbox = (mapboxgl as typeof mapboxgl);
  map: any;
  cityName: any;
  userLat:any;
  userLong:any;

  ngOnInit(): void {
    this.httpClient.get<any[]>(Urlbase.tercero+"/thirds/cities",{ headers: new HttpHeaders({'Content-Type': 'application/json','Authorization': '3020D4:0DD-2F413E82B-A1EF04559-78CA', 'Key_Server': 'FE651467B48D552C2EFBC8B13EBA9',})}).subscribe(response => {
      this.cityList = response;
      this.cityName = response[0].city_NAME;
      this.city = response[0].id_CITY;
    });
    this.getPosition().then(pos=>{},)
    console.log("INVENTORY")
    console.log(localStorage.getItem("inventory"));
    this.mapbox.accessToken = environment.mapBoxKey;
    this.map= new mapboxgl.Map({
      container: 'mapa-mapbox', // container ID
      style: 'mapbox://styles/mapbox/streets-v11', // style URL
      center: [-74.082777777778, 4.6355555555556], // starting position
      zoom: 16 // starting zoom
    });
    this.map.addControl(new mapboxgl.NavigationControl());
    // Add zoom and rotation controls to the map.
    //this.mapa.addControl(new Mapboxgl.NavigationControl());
    this.httpClient.get(Urlbase.tercero +"/documents-types",{headers:this.headers,withCredentials:true}).subscribe(data => {
      console.log("docList")
      console.log(data)
      //@ts-ignore
      this.docList = data;
    })
    this.httpClient.get<any[]>(Urlbase.tercero+"/thirds/cities",{ headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': '3020D4:0DD-2F413E82B-A1EF04559-78CA',
          'Key_Server': 'FE651467B48D552C2EFBC8B13EBA9',
        }) }).subscribe(response => {
        this.cityList = response;
        this.cityName = response[0].city_NAME;
        this.city = response[0].id_CITY;
        });
  }

  removeAccents(str:any) {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }


  getPosition(): Promise<any>{
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resp => {
        this.httpClient.get<any[]>(Urlbase.tercero+"/thirds/cities",{ headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': '3020D4:0DD-2F413E82B-A1EF04559-78CA',
          'Key_Server': 'FE651467B48D552C2EFBC8B13EBA9',
        }) }).subscribe(response => {
          this.cityList = response;
          this.httpClient.get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+resp.coords.latitude+","+resp.coords.longitude+"&key="+environment.googlekey,{headers:this.headers2}).subscribe(response=>{
            console.log("data: ");
            //@ts-ignore
            console.log(response);
            //@ts-ignore
            console.log(this.removeAccents(response.results[0].formatted_address.split(", ")[1].toUpperCase()));
            //@ts-ignore
            let find = this.cityList.find(x => x?.city_NAME.indexOf((this.removeAccents(response.results[0].formatted_address.split(", ")[1].toUpperCase())))>-1);
            console.log(find)
            this.cityName = find.city_NAME;
            this.city = find.id_CITY;

            this.lat = resp.coords.latitude;
            this.lng = resp.coords.longitude;
            this.crearMarcador(this.lng, this.lat);
            this.map.flyTo({
              //@ts-ignore
              center: {lng: this.lng, lat: this.lat}
            });
            //@ts-ignore
            this.direccion = response.results[0].formatted_address;
          })
        })
        resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
      },err => {
        this.httpClient.get<any[]>(Urlbase.tercero+"/thirds/cities",{ headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': '3020D4:0DD-2F413E82B-A1EF04559-78CA',
          'Key_Server': 'FE651467B48D552C2EFBC8B13EBA9',
        }) }).subscribe(response => {
        this.cityList = response;
        this.cityName = response[0].city_NAME;
        this.city = response[0].id_CITY;
        });
        reject(err);
      },{timeout:10000});
    });
  }

  crearMarcadorForm(){
    try{
      this.marker.remove();
      console.log("ADDRESS:");
      console.log("https://maps.googleapis.com/maps/api/geocode/json?key="+environment.googlekey+"&address="+this.ReturnFullAddress(false))
      this.httpClient.get("https://maps.googleapis.com/maps/api/geocode/json?key="+environment.googlekey+"&address="+this.ReturnFullAddress(false),{headers:this.headers2}).subscribe(response=>{
        //@ts-ignore
        if(response.status == "OK"){
          //@ts-ignore
          this.crearMarcador(response.results[0].geometry.location.lng, response.results[0].geometry.location.lat);
          this.map.flyTo({
            //@ts-ignore
            center: {lng: response.results[0].geometry.location.lng, lat: response.results[0].geometry.location.lat}
          });
          //@ts-ignore
          this.lng = response.results[0].geometry.location.lng;
          //@ts-ignore
          this.lat = response.results[0].geometry.location.lat;
          this.httpClient.get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+this.lat+","+this.lng+"&key="+environment.googlekey,{headers:this.headers2}).subscribe(response=>{
            console.log("data: ");

            //@ts-ignore
            console.log(response.results[0].formatted_address);
            //@ts-ignore
            if(response.status == "OK"){
              //@ts-ignore
              this.direccion = response.results[0].formatted_address;
            }else{

            }

          })
        }else{

        }

      })
    }catch(e){
      console.log("ADDRESS:");
      console.log("https://maps.googleapis.com/maps/api/geocode/json?key="+environment.googlekey+"&address="+this.ReturnFullAddress(false))
      this.httpClient.get("https://maps.googleapis.com/maps/api/geocode/json?key="+environment.googlekey+"&address="+this.ReturnFullAddress(false),{headers:this.headers2}).subscribe(response=>{
        //@ts-ignore
        if(response.status == "OK"){
          //@ts-ignore
          this.crearMarcador(response.results[0].geometry.location.lng, response.results[0].geometry.location.lat);
          this.map.flyTo({
            //@ts-ignore
            center: {lng: response.results[0].geometry.location.lng, lat: response.results[0].geometry.location.lat}
          });
          //@ts-ignore
          this.lng = response.results[0].geometry.location.lng;
          //@ts-ignore
          this.lat = response.results[0].geometry.location.lat;
          this.httpClient.get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+this.lat+","+this.lng+"&key="+environment.googlekey,{headers:this.headers2}).subscribe(response=>{
            console.log("data: ");

            //@ts-ignore
            console.log(response.results[0].formatted_address);
            //@ts-ignore
            if(response.status == "OK"){
              //@ts-ignore
              this.direccion = response.results[0].formatted_address;
            }else{

            }

          })
        }else{

        }

      })
    }

    //this.crearMarcador(-74.082777777778, 4.6355555555556);
  }


  async createUser(){
    await console.log({
      usuario:this.email,
      clave:this.password,
      appid:40,
      third:{
        firstname: this.fnombre,
        secondname: this.snombre,
        firstlastname: this.fapellido,
        secondlastname: this.sapellido,
        iddocumenttype:this.docType,
        docnumber:this.numDoc,
        direccion:this.direccion,//le concatenos las coordenadas
        idcity:this.city,
        telefono:this.cel,
        email:this.email,
        // birthDate:this.signinForm.controls.birthDate.value,
        birthDate: new Date(),
        // gender:this.signinForm.controls.gender.value,
        gender:1,
        lat:this.lat,
        lng:this.lng
      }
    })
    await this.postUser({
      usuario:this.email,
      clave:this.password,
      appid:40,
      third:{
        firstname: this.fnombre,
        secondname: this.snombre,
        firstlastname: this.fapellido,
        secondlastname: this.sapellido,
        iddocumenttype:this.docType,
        docnumber:this.numDoc,
        direccion:this.direccion,//le concatenos las coordenadas
        idcity:this.city,
        telefono:this.cel,
        email:this.email,
        // birthDate:this.signinForm.controls.birthDate.value,
        birthDate: new Date(),
        // gender:this.signinForm.controls.gender.value,
        gender:1,
        lat:this.lat,
        lng:this.lng
      }
    });
    await this.sessionManager.logout();
    await this.sessionManager.login2(this.email,this.password);
    await this.router.navigate(["pedidosActivos"]);
  }

  skip(){
    this.router.navigate(["pedidosActivos"]);
  }


  postUser(bodyPosteo:{}):Promise<any>{
    return new Promise<any>((resolve,reject) => {
        this.httpClient.post(Urlbase.auth+"/CreateClient",bodyPosteo).subscribe(value => {
          console.log("IT IS CREATED!")
            resolve(value);
        }, error => {
          reject(error);
        })
    });
}

  obtenerDireccion(){
    this.httpClient.get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+this.lat+","+this.lng+"&key="+environment.googlekey,{headers:this.headers2}).subscribe(response=>{
      //@ts-ignore
      if(response.status == "OK"){
        //@ts-ignore
        this.direccion = response.results[0].formatted_address;
      }else{

      }

    })
  }

  direccion = "";
  lng = 0;
  lat = 0;
  marker: any = "";
  crearMarcador(lng: number, lat: number){
    this.marker = new mapboxgl.Marker({
      draggable:true
    }).setLngLat([lng,lat]).addTo(this.map)
    this.marker.on("drag",()=>{
      this.lng = this.marker.getLngLat().lng;
      this.lat = this.marker.getLngLat().lat;
    })
  }


  ReturnFullAddress(withCity:any,withComplement = true){
    let city = "";
    if(withCity){
      for(let n = 0;n<this.cityList.length;n++){
        if(this.city == this.cityList[n].id_CITY){
          city = this.cityList[n].city_NAME;
          break;
        }
      }
    }
    let ajusteOpcional = "";
    if(this.dirOpcional != "" && withComplement){
      ajusteOpcional = "| "+this.dirOpcional;
    }
    return (this.cityName+" "+this.dir1+" "+
      this.dir2+" # "+
      this.dir3+" "+
      this.dir4 +" " + ajusteOpcional );
  }

  citychanged(e:any){
    let find = this.cityList.find(x => x?.city_NAME === e.target.value);
    this.city = find.id_CITY;
    console.log(this.city);
  }

  regresarAAgregar(){
    //this.router.navigate(['detailmesa']);
    this.router.navigate(['addproductsmesa']);
  }

}
