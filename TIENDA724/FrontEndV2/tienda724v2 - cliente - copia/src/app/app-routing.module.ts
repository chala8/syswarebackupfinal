import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailMesaComponent } from './components/detail-mesa/detail-mesa.component';
import { AddProductsMesaComponent } from './components/add-products-mesa/add-products-mesa.component';
import { SelectClientComponent } from './components/select-client/select-client.component';
import { CreateClientComponent } from './components/create-client/create-client.component';
import { LoaderComponent } from './components/loader/loader.component';
import { ConfirmLogginComponent } from './components/confirm-loggin/confirm-loggin.component';
import { CatalogoComponent } from './components/catalogo/catalogo.component';
import { RedirectClosedComponent } from './components/redirect-closed/redirect-closed.component';
import { LoginComponent } from './components/login/login.component';
import { NewUserComponent } from './components/new-user/new-user.component';
import { ConfirmarPedidoComponent } from './components/confirmar-pedido/confirmar-pedido.component';
import { SalidaComponent } from './components/salida/salida.component';
import { InfoUserComponent } from './components/info-user/info-user.component';
import { PedidosComponent } from './components/pedidos/pedidos.component';
import { PedidosActivosComponent } from './components/pedidos-activos/pedidos-activos.component';

const routes: Routes = [
  { path: 'detailmesa', component: DetailMesaComponent, canActivate:[] },
  { path: 'addproductsmesa', component: AddProductsMesaComponent, canActivate:[] },
  { path: 'selectclient', component: SelectClientComponent, canActivate:[] },
  { path: 'createclient', component: CreateClientComponent, canActivate:[] },
  { path: 'loader', component: LoaderComponent, canActivate:[] },
  { path: 'catalog', component: CatalogoComponent, canActivate:[] },
  { path: '', component: ConfirmLogginComponent, canActivate:[] },
  { path: 'redirect', component: RedirectClosedComponent, canActivate:[] },
  { path: 'login', component: LoginComponent, canActivate:[] },
  { path: 'newUser', component: NewUserComponent, canActivate:[] },
  { path: 'confirmPedido', component: ConfirmarPedidoComponent, canActivate:[] },
  { path: 'exit', component: SalidaComponent, canActivate:[] },
  { path: 'userInfo', component: InfoUserComponent, canActivate:[] },
  { path: 'pedidos', component: PedidosComponent, canActivate:[] },
  { path: 'pedidosActivos', component: PedidosActivosComponent, canActivate:[] }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
