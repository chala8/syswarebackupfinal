import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { DetailMesaComponent } from './components/detail-mesa/detail-mesa.component';
import { AddProductsMesaComponent } from './components/add-products-mesa/add-products-mesa.component';
import { DatePipe, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { NgxCurrencyModule } from "ngx-currency";
import {CustomHttpInterceptor} from "./services/sessionManager/custom-http.interceptor";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxSpinnerModule } from "ngx-spinner";
import { SelectClientComponent } from './components/select-client/select-client.component';
import { CreateClientComponent } from './components/create-client/create-client.component'
import { CookieService } from 'ngx-cookie-service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoaderComponent } from './components/loader/loader.component';
import { ConfirmLogginComponent } from './components/confirm-loggin/confirm-loggin.component';
import { CatalogoComponent } from './components/catalogo/catalogo.component';
import { RedirectClosedComponent } from './components/redirect-closed/redirect-closed.component';
import { LoginComponent } from './components/login/login.component';
import { NewUserComponent } from './components/new-user/new-user.component';
import { ConfirmarPedidoComponent } from './components/confirmar-pedido/confirmar-pedido.component';
import { SalidaComponent } from './components/salida/salida.component';
import { InfoUserComponent } from './components/info-user/info-user.component';
import { PedidosComponent } from './components/pedidos/pedidos.component';
import { PedidosActivosComponent } from './components/pedidos-activos/pedidos-activos.component';

@NgModule({
  declarations: [
    AppComponent,
    DetailMesaComponent,
    AddProductsMesaComponent,
    SelectClientComponent,
    CreateClientComponent,
    LoaderComponent,
    ConfirmLogginComponent,
    CatalogoComponent,
    RedirectClosedComponent,
    LoginComponent,
    NewUserComponent,
    ConfirmarPedidoComponent,
    SalidaComponent,
    InfoUserComponent,
    PedidosComponent,
    PedidosActivosComponent,
  ],
  imports: [
    FormsModule,
    NgxCurrencyModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    NgbModule,
  ],
  providers: [
    CookieService,
    DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CustomHttpInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
