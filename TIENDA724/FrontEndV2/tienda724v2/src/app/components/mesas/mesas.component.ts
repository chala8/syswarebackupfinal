import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, OnDestroy  } from '@angular/core';
import { Router } from '@angular/router';
import { interval, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SessionManagerService } from 'src/app/services/sessionManager/session-manager.service';
import { Urlbase } from '../../classes/urls';

@Component({
  selector: 'app-mesas',
  templateUrl: './mesas.component.html',
  styleUrls: ['./mesas.component.scss']
})
export class MesasComponent implements OnInit, OnDestroy {

  constructor(private httpClient : HttpClient,
              private sessionManager : SessionManagerService,
              private router: Router,) { }
  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  tableList:any;
  tableToOpen:any;
  inventoryList:any;
  idObject:any;
  tableName:any;
  rolList:any;
  username:any;
  clientName:any;
  obj: any;
  canItOpenTable:any = false;
  private unsubscribe$ = new Subject();

  ngOnInit(): void {

    this.httpClient.get(Urlbase.tienda + "/products2/inventoryList?id_store="+this.sessionManager.returnIdStore(),{ headers: this.headers,withCredentials:true }).subscribe(data => {
      console.log(data)
      this.inventoryList = data;
      //CPrint("this is InventoryList: "+JSON.stringify(data))
      this.canItOpenTable = true;
    });

    this.idObject = this.sessionManager.returnIdObject();
    this.obj = this.sessionManager.returnIdObject();
    this.rolList = this.obj.roles;
    this.clientName = this.obj.third.fullname;
    this.username = this.obj.usuario;
    this.httpClient.get(Urlbase.facturacion + "/billing/getPedidosMesa?id_store="+this.sessionManager.returnIdStore()+"&id_bill_type="+86+"&id_bill_state="+801,{ headers: this.headers,withCredentials:true }).subscribe(response => {
      console.log(response)
      //@ts-ignore
      this.tableList = response

    })


    this.getPedidosMesa();
    interval(10000)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((val) => {
        this.getPedidosMesa();
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getPedidosMesa(){
    this.httpClient.get(Urlbase.facturacion + "/billing/getPedidosMesa?id_store="+this.sessionManager.returnIdStore()+"&id_bill_type="+86+"&id_bill_state="+801,{ headers: this.headers,withCredentials:true }).subscribe(response => {
      console.log(response)
      //@ts-ignore
      this.tableList = response

    })
  }

  getRolString(){
    let response = "";
    this.rolList.forEach((i: { rol: string; }) => {
      response = response + i.rol+"/ "
    });
    return response;
  }

  getDateInMinutes(date:any){
    if(date == null){
      return null;
    }
    let today = new Date();
    let dif = today.getTime() - (new Date(date)).getTime();
    return Math.round((Math.round(dif/1000)/60))
  }

  goToDetail(mesa:any){
    console.log("ENTRE A ABRIR 7");
    let table = this.tableList.find((element:any) => mesa.id_MESA == element.id_MESA )
    this.sessionManager.setMesa(table);
    this.router.navigate(['detailmesa']);
  }

  goMenu(){
    this.router.navigate(['menu']);
  }

  disableTableClickDouble = false;
  tableClick(table:any){
    if(this.disableTableClickDouble){
      return;
    }else{
      this.disableTableClickDouble = true;
      this.tableToOpen = table
      this.tableName = table.mesa
      this.disableTableClickDouble = false;
    }

  }

  logOut(){
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    localStorage.removeItem("ID");
    localStorage.removeItem("idStore");
    localStorage.removeItem("mesa");
    localStorage.removeItem("storeName");
    localStorage.removeItem("ID_CAJA");
    this.router.navigate(['login']);
  }


  noDecimal(number:any){
    return Math.ceil(number);
  }

  getMesero(mesero:any){
    try{
      return mesero.split(' ')[0]
    }catch(exception ){
      return mesero
    }
  }


  openTable(item:any){

    console.log("ENTRE A ABRIR");

    if(!this.canItOpenTable){
      alert("Espera 30 segundos mientras se terminan de cargar los productos");
      return;
    }

    console.log("ENTRE A ABRIR 2");
    let detailList = "";
    let detailListNew = "";
    let object = this.inventoryList.find((element:any) => element.code === "45000198" || element.ownbarcode === "45000198" );
    detailList = detailList+ "{"+object.id_PRODUCT_STORE+","+object.standard_PRICE+","+object.id_TAX+","+1+"},"
    detailListNew = detailListNew+ "{"+object.id_PRODUCT_STORE+",0},"
    console.log("ENTRE A ABRIR 3");

    //GENERO LA LISTA DE DTOs DE DETALLES
    this.httpClient.post(Urlbase.facturacion+ "/billing/crearPedidoMesa?idstoreclient=11&idthirduseraapp="+this.idObject.id_third+"&idstoreprov="+this.sessionManager.returnIdStore()+"&detallepedido="+detailList.substring(0, detailList.length - 1)+"&descuento="+0+"&idpaymentmethod="+1+"&idapplication="+40+"&idthirdemp="+this.idObject.id_third+"&detallepedidomesa="+detailListNew.substring(0, detailListNew.length - 1)+"&idmesa="+item.id_MESA,{},{ headers: this.headers,withCredentials:true }).subscribe(itemr => {
      console.log("ENTRE A ABRIR 4");
      console.log(itemr)
      if(itemr==1){
        console.log("ENTRE A ABRIR 5");
        //this.showNotification('top', 'center', 3, "<h3>Mesa abierta con <b>EXITO.</b></h3> ", 'info');
        this.httpClient.get(Urlbase.facturacion + "/billing/getPedidosMesa?id_store="+this.sessionManager.returnIdStore()+"&id_bill_type="+86+"&id_bill_state="+801,{ headers: this.headers,withCredentials:true }).subscribe(response => {
          //@ts-ignore
          this.tableList = response;
          console.log("ENTRE A ABRIR 6");
          this.goToDetail(item);
        })
      }
    })
  }

  getStoreName(){
    return this.sessionManager.returnStoreName();
  }
}
