import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Urlbase } from 'src/app/classes/urls';
import { SessionManagerService } from 'src/app/services/sessionManager/session-manager.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  idstore: string = "";
  inventoryList:any;
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': this.sessionManager.returnToken(),
    'skip':'skip'
  });

  private headers2 = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(private spinner: NgxSpinnerService,
              private sessionManager : SessionManagerService,
              private router: Router,
              private httpClient: HttpClient
  ) {

  }

  ngOnInit(): void {
    this.initData();
  }

  initData(){
    this.spinner.show();
    // this.httpClient.get(Urlbase.facturacion + "/billing/getMesaAbierta?idmesa="+this.sessionManager.returnIdMesa(),{ headers: this.headers,withCredentials:true }).subscribe(response=>{
    //   console.log("GET MESA ABIERTA:" + response);
    //   if(response==0){
    this.httpClient.get(Urlbase.tienda + "/products2/inventoryList?id_store="+this.sessionManager.returnIdStore(),{ headers: this.headers,withCredentials:true }).subscribe(data =>  {
      console.log(data)
      this.inventoryList =  data;
      this.openTable(this.sessionManager.returnIdMesa());
      this.httpClient.get(Urlbase.facturacion + "/billing/registrarMesaToken?idmesa="+this.sessionManager.returnIdMesa()+"&tok="+this.sessionManager.returnToken(),{ headers: this.headers,withCredentials:true }).subscribe(data2 =>  {
        console.log("data2: "+data2)
      });
    });
    //   }else{
    //     this.router.navigateByUrl('/main?idmesa='+this.sessionManager.returnIdMesa()+'&idstore='+this.sessionManager.returnIdStore());
    //   }
    // })

  }

  openTable(id:any){

    let detailList =  "";
    let detailListNew =  "";
    let object =  this.inventoryList.find((element:any) => element.code === "45000198" || element.ownbarcode === "45000198" );
    detailList =  detailList+ "{"+object.id_PRODUCT_STORE+","+object.standard_PRICE+","+object.id_TAX+","+1+"},"
    detailListNew =  detailListNew+ "{"+object.id_PRODUCT_STORE+",0},"

    //GENERO LA LISTA DE DTOs DE DETALLES
    this.httpClient.post(Urlbase.facturacion+ "/billing/crearPedidoMesa?idstoreclient=11&idthirduseraapp="+803+"&idstoreprov="+this.sessionManager.returnIdStore()+"&detallepedido="+detailList.substring(0, detailList.length - 1)+"&descuento="+0+"&idpaymentmethod="+1+"&idapplication="+40+"&idthirdemp="+803+"&detallepedidomesa="+detailListNew.substring(0, detailListNew.length - 1)+"&idmesa="+id,{},{ headers: this.headers,withCredentials:true }).subscribe(itemr => {
      this.httpClient.get(Urlbase.facturacion + "/billing/getPedidosMesaByIdMesa?id_store="+id+"&id_bill_type="+86+"&id_bill_state="+801,{ headers: this.headers2,withCredentials:true }).subscribe(response => {
        console.log(response)
        this.sessionManager.setMesa(response);

        this.spinner.hide();
        this.router.navigate(['addproductsmesa']);
      })
    })
  }

}
