import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Urlbase } from 'src/app/classes/urls';
import { SessionManagerService } from 'src/app/services/sessionManager/session-manager.service';

@Component({
  selector: 'app-confirm-loggin',
  templateUrl: './confirm-loggin.component.html',
  styleUrls: ['./confirm-loggin.component.scss']
})
export class ConfirmLogginComponent implements OnInit {

  idmesa: string = "";
  idstore: string = "";
  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Validate' : 'Validate'
  });

  constructor(private router: Router,
              private route: ActivatedRoute,
              private httpClient: HttpClient,
              private sessionManager : SessionManagerService,) {
                this.route.queryParams.subscribe(params => {
                  this.idmesa = params['idmesa'];
                  this.idstore = params['idstore'];
                });

  }

  ngOnInit(): void {
    let at = this.sessionManager.returnToken();
    this.httpClient.get(Urlbase.facturacion + "/billing/getValidacionMesa?id_store="+this.sessionManager.returnIdMesa(),{ headers: this.headers,withCredentials:true }).subscribe(response => {
      try{
        //@ts-ignore
        if(response.token != this.sessionManager.returnToken()){
          this.sessionManager.login("73214195","123",this.idstore);
          this.sessionManager.setIdStore(this.idstore);
          this.sessionManager.setIdMesa(this.idmesa);
        }else{
          this.router.navigate(['loader']);
        }
      }catch(e){
        this.sessionManager.login("73214195","123",this.idstore);
        this.sessionManager.setIdStore(this.idstore);
        this.sessionManager.setIdMesa(this.idmesa);
      }

    },error=>{
      this.sessionManager.login("73214195","123",this.idstore);
      this.sessionManager.setIdStore(this.idstore);
      this.sessionManager.setIdMesa(this.idmesa);
    })



  }

  goToLoader(){
    this.router.navigate(['loader']);

  }

  goToCatalog(){
    this.sessionManager.setIdStore(this.idstore);

    this.router.navigate(['catalog']);
  }

  returnStoreName(){
    return this.sessionManager.returnStoreName();
  }

}
