import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, OnDestroy, NgZone  } from '@angular/core';
import { Router } from '@angular/router';
import { interval, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SessionManagerService } from 'src/app/services/sessionManager/session-manager.service';
import { Urlbase } from '../../classes/urls';
import { PaginationInstance } from 'ngx-pagination';

@Component({
  selector: 'app-mesas',
  templateUrl: './mesas.component.html',
  styleUrls: ['./mesas.component.scss']
})
export class MesasComponent implements OnInit, OnDestroy {
  // pages: number = 1;
  // itemsPerPage:number = 30;

  constructor(private httpClient : HttpClient,
              private sessionManager : SessionManagerService,
              private router: Router,
              private zone: NgZone) { }
  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  tableList:any;
  tableListShow : any = [];
  tableListGreen:any = [];
  tableListPurple:any;
  tableListBlue:any;
  tableListOrange:any;
  tableListYellow:any;
  tableListRed:any;
  tableToOpen:any;
  inventoryList:any;
  idObject:any;
  tableName:any;
  rolList:any;
  username:any;
  clientName:any;
  obj: any;
  canItOpenTable:any = false;
  private unsubscribe$?: NodeJS.Timeout;
  getPedidosMesaUrl = Urlbase.facturacion + "/billing/getPedidosMesa?id_store="+this.sessionManager.returnIdStore()+"&id_bill_type="+86+"&id_bill_state="+801;
  public filter: string = '';
  public maxSize: number = 7;
  public directionLinks: boolean = true;
  public autoHide: boolean = false;
  public responsive: boolean = true;
  public configLandscape: PaginationInstance = {
      itemsPerPage: 45,
      currentPage: 1
  };
  public configPortrait: PaginationInstance = {
    itemsPerPage: 18,
    currentPage: 1
};
  idLandscapeAll : string = 'AllLandscape';
  idPortraitAll : string = 'AllPortrait';
  idLandscapeRed : string = 'RedLandscape';
  idPortraitRed : string = 'RedPortrait';
  idLandscapeYellow : string = 'YellowLandscape';
  idPortraitYellow : string = 'YellowPortrait';
  idLandscapeOrange : string = 'OrangeLandscape';
  idPortraitOrange : string = 'OrangePortrait';
  idLandscapeBlue : string = 'BlueLandscape';
  idPortraitBlue : string = 'BluePortrait';
  idLandscapePurple : string = 'PurpleLandscape';
  idPortraitPurple : string = 'PurplePortrait';
  idLandscapeGreen : string = 'GreenLandscape';
  idPortraitGreen : string = 'GreenPortrait';

  public labels: any = {
      previousLabel: 'Atrás',
      nextLabel: 'Siguiente',
      screenReaderPaginationLabel: 'Pagination',
      screenReaderPageLabel: 'page',
      screenReaderCurrentLabel: `You're on page`
  };
  public eventLog: string[] = [];

  public tabTitleObj: any = {
    'All' : {
      'Title': 'Todas',
      'bgColor': '#1E1E1E'
    },
    'Red' : {
      'Title': 'Pendiente',
      'bgColor': "#E9A6A1"
    },
    'Yellow' : {
      'Title': 'En preparación',
      'bgColor': "#E4E49C"
    },
    'Orange' : {
      'Title': 'Entregado a mesero',
      'bgColor': "#E2BC96"
    },
    'Blue' : {
      'Title': 'Entregado en mesa',
      'bgColor': "#AFCADE"
    },
    'Purple' : {
      'Title': 'Mesa cerrada',
      'bgColor': "#AF98DF"
    },
    'Green' : {
      'Title': 'Sin Abrir',
      'bgColor': "#A7DE9F"
    },
  };

  public tabTitle: any;
  public tabTitleBgColor: any;
  public hamburguerMenuOpened : boolean = false;
  public responsiveToggler : boolean = false;
  public paginatorTablet : boolean = false;
  public titleTablet : boolean = false;

  paginationControlsId: any;

  public pagControlerIdsObj : any = {
    'landscape' : {
      'All' : 'AllLandscape',
      'Red': 'RedLandscape',
      'Yellow': 'YellowLandscape',
      'Orange' : 'OrangeLandscape',
      'Blue' : 'BlueLandscape',
      'Purple' : 'PurpleLandscape',
      'Green' : 'GreenLandscape'
    },
    'portrait' : {
      'All' : 'AllPortrait',
      'Red': 'RedPortrait',
      'Yellow': 'YellowPortrait',
      'Orange' : 'OrangePortrait',
      'Blue' : 'BluePortrait',
      'Purple' : 'PurplePortrait',
      'Green' : 'GreenPortrait'
    }
  }


  ngOnInit(): void {

    // Inicio listener para slide-in menu responsivo
    document.querySelector(".ham-menu")?.addEventListener("click",() => {
        document.querySelector(".tab-container-responsive")?.classList.add("tab-container-opened")
        document.querySelector(".tab-container-responsive")?.classList.remove("tab-container-closed")
        this.hamburguerMenuOpened = true;
        this.responsiveToggler = true;

        document.querySelector(".close-menu")?.addEventListener("click",() => {
          document.querySelector(".responsive-header")?.classList.remove("fixed-position")
          document.querySelector(".tab-container-responsive")?.classList.remove("tab-container-opened")
          document.querySelector(".tab-container-responsive")?.classList.add("tab-container-closed")
          this.hamburguerMenuOpened = false;
      })
    })
    //fin listeners para slide-in menu responsivo

    console.log("ID_OBJECT")
    console.log(this.sessionManager.returnIdObject())
    if(this.sessionManager.returnIdObject().roles[0].id_rol==51){
      this.getPedidosMesaUrl = Urlbase.facturacion + "/billing/getPedidosMesaADR?id_store="+this.sessionManager.returnIdStore()+"&id_bill_type="+86+"&id_bill_state="+801+"&id_third="+this.sessionManager.returnIdObject().id_third;
    }
    //@ts-ignore
    document.getElementById("defaultOpen").click();

    this.httpClient.get(Urlbase.tienda + "/products2/inventoryList?id_store="+this.sessionManager.returnIdStore(),{ headers: this.headers,withCredentials:true }).subscribe(data => {
      console.log(data)
      this.inventoryList = data;
      //CPrint("this is InventoryList: "+JSON.stringify(data))
      this.canItOpenTable = true;
    });

    this.idObject = this.sessionManager.returnIdObject();
    this.obj = this.sessionManager.returnIdObject();
    this.rolList = this.obj.roles;
    this.clientName = this.obj.third.fullname;
    this.username = this.obj.usuario;

    this.getPedidosMesa();

    this.unsubscribe$ = setInterval(() => {
      this.getPedidosMesa();
    }, 15000)
  }


//CONTROLADORES DE PAGINADOR

  // landscape
  onPageChange(number: number) {
    console.log(window.screen.availWidth)
    if(window.screen.availWidth <= 800){
      this.logEvent(`pageChange(${number})`);
      this.configPortrait.currentPage = number;
    }
    else{
      this.logEvent(`pageChange(${number})`);
      this.configLandscape.currentPage = number;
    }
  }

  // onPageBoundsCorrectionLandscape(number: number) {
  //     this.logEvent(`pageBoundsCorrection(${number})`);  MISMO METODO QUE OCASIONA BUG EN EL PAGINADOR
  //     this.configLandscape.currentPage = number;
  // }

  // pushItem() {
  //     let item = this.popped.pop() || 'A newly-created meal!';
  //     this.tableListShow.push(item);
  // }

  // popItem() {
  //     this.popped.push(this.tableListShow.pop());
  // }

  // Portrait

  // onPageBoundsCorrectionPortrait(number: number) {
  //     this.logEvent(`pageBoundsCorrection(${number})`);
  //     this.configPortrait.currentPage = number;          ESTE METODO OCASIONA UN ERROR EN EL PAGINADOR
  // }

  private logEvent(message: string) {
      this.eventLog.unshift(`${new Date().toISOString()}: ${message}`)
  }

//FIN CONTROLADORES DE PAGINADOR


//Funcion que setea el color del fondo del titulo de la página
  getTabTitleBackgroundColor(){
    return this.tabTitleBgColor
  }

//Función que setea el color del titulo de la página
  getTabTitleTextColor(){
    if(this.tabTitle == "Todas"){
      return "white"
    }
    else{
      return "black"
    }
  }


  ngOnDestroy(): void {
    clearInterval(this.unsubscribe$);
  }

  getPedidosMesa(){
    this.httpClient.get(this.getPedidosMesaUrl,{ headers: this.headers,withCredentials:true }).subscribe(response => {
      this.zone.run(() => {
        console.log(response)
        if(this.obj.roles[0].id_rol!=51){
          this.tableList = response;
          this.tableListShow = response;
          this.tableListGreen = this.tableList.filter((item:any) => item.purchase_DATE == null)
          this.tableListPurple = this.tableList.filter((item:any) => item.violeta > 0)
          this.tableListBlue = this.tableList.filter((item:any) => item.naranja <= 0 && item.amarillo <= 0 && item.azul && item.rojo == 0)
          this.tableListOrange = this.tableList.filter((item:any) => item.naranja > 0)
          this.tableListYellow = this.tableList.filter((item:any) => item.naranja <= 0 && item.amarillo > 0)
          this.tableListRed = this.tableList.filter((item:any) => item.rojo > 0)
        }else{
          this.tableList = response;
          this.tableListGreen = this.tableList.filter((item:any) => item.purchase_DATE == null )
          this.tableListPurple = this.tableList.filter((item:any) => item.violeta > 0)
          this.tableListBlue = this.tableList.filter((item:any) => item.naranja <= 0 && item.amarillo <= 0 && item.azul && item.rojo == 0)
          this.tableListOrange = this.tableList.filter((item:any) => item.naranja > 0)
          this.tableListYellow = this.tableList.filter((item:any) => item.naranja <= 0 && item.amarillo > 0)
          this.tableListRed = this.tableList.filter((item:any) => item.rojo > 0)
          this.tableListShow = this.tableList.filter((item:any) => { return !(item.naranja == 0 && item.amarillo == 0 && item.azul == 0 && item.rojo == 0 && item.violeta == 0) })
        }
      });
    })
  }

  getRolString(){
    let response = "";
    this.rolList.forEach((i: { rol: string; }) => {
      response = response + i.rol+"/ "
    });
    return response;
  }

  getDateInMinutes(date:any){
    if(date == null){
      return null;
    }
    let today = new Date();
    let dif = today.getTime() - (new Date(date)).getTime();
    return Math.round((Math.round(dif/1000)/60))
  }

  goToDetail(mesa:any){
    let table = this.tableList.find((element:any) => mesa.id_MESA == element.id_MESA )
    this.sessionManager.setMesa(table);
    localStorage.setItem('checkReload', "1");
    this.router.navigate(['detailmesa']);
  }

  goMenu(){
    this.router.navigate(['menu']);
  }

  disableTableClickDouble = false;
  tableClick(table:any){
    if(this.disableTableClickDouble){
      return;
    }else{
      this.disableTableClickDouble = true;
      this.tableToOpen = table
      this.tableName = table.mesa
      this.disableTableClickDouble = false;
    }

  }

  logOut(){
    clearInterval(this.unsubscribe$);
    localStorage.removeItem("ID");
    localStorage.removeItem("idStore");
    localStorage.removeItem("mesa");
    localStorage.removeItem("storeName");
    localStorage.removeItem("ID_CAJA");
    this.router.navigate(['login']);
  }


  noDecimal(number:any){
    return Math.ceil(number);
  }

  getMesero(mesero:any){
    try{
      return mesero.split(' ')[0]
    }catch(exception ){
      return mesero
    }
  }


  openTable(item:any){

    console.log("ENTRE A ABRIR");

    if(!this.canItOpenTable){
      alert("Espera 30 segundos mientras se terminan de cargar los productos");
      return;
    }

    console.log("ENTRE A ABRIR 2");
    let detailList = "";
    let detailListNew = "";
    let object = this.inventoryList.find((element:any) => element.code === "45000198" || element.ownbarcode === "45000198" );
    detailList = detailList+ "{"+object.id_PRODUCT_STORE+","+object.standard_PRICE+","+object.id_TAX+","+1+"},"
    detailListNew = detailListNew+ "{"+object.id_PRODUCT_STORE+",0},"
    console.log("ENTRE A ABRIR 3");

    //GENERO LA LISTA DE DTOs DE DETALLES
    this.httpClient.post(Urlbase.facturacion+ "/billing/crearPedidoMesa?idstoreclient=11&idthirduseraapp="+this.idObject.id_third+"&idstoreprov="+this.sessionManager.returnIdStore()+"&detallepedido="+detailList.substring(0, detailList.length - 1)+"&descuento="+0+"&idpaymentmethod="+1+"&idapplication="+40+"&idthirdemp="+this.idObject.id_third+"&detallepedidomesa="+detailListNew.substring(0, detailListNew.length - 1)+"&idmesa="+item.id_MESA,{},{ headers: this.headers,withCredentials:true }).subscribe(itemr => {
      console.log("ENTRE A ABRIR 4");
      console.log(itemr)
      if(itemr==1){
        console.log("ENTRE A ABRIR 5");
        //this.showNotification('top', 'center', 3, "<h3>Mesa abierta con <b>EXITO.</b></h3> ", 'info');
        this.httpClient.get(this.getPedidosMesaUrl,{ headers: this.headers,withCredentials:true }).subscribe(response => {
          if(this.obj.roles[0].id_rol!=51){
            this.tableList = response;
            this.tableListShow = response;
            this.tableListGreen = this.tableList.filter((item:any) => item.purchase_DATE == null)
            this.tableListPurple = this.tableList.filter((item:any) => item.violeta > 0)
            this.tableListBlue = this.tableList.filter((item:any) => item.naranja <= 0 && item.amarillo <= 0 && item.azul && item.rojo == 0)
            this.tableListOrange = this.tableList.filter((item:any) => item.naranja > 0)
            this.tableListYellow = this.tableList.filter((item:any) => item.naranja <= 0 && item.amarillo > 0)
            this.tableListRed = this.tableList.filter((item:any) => item.rojo > 0)
          }else{
            this.tableList = response;
            this.tableListGreen = this.tableList.filter((item:any) => item.purchase_DATE == null )
            this.tableListPurple = this.tableList.filter((item:any) => item.violeta > 0)
            this.tableListBlue = this.tableList.filter((item:any) => item.naranja <= 0 && item.amarillo <= 0 && item.azul && item.rojo == 0)
            this.tableListOrange = this.tableList.filter((item:any) => item.naranja > 0)
            this.tableListYellow = this.tableList.filter((item:any) => item.naranja <= 0 && item.amarillo > 0)
            this.tableListRed = this.tableList.filter((item:any) => item.rojo > 0)
            this.tableListShow = this.tableList.filter((item:any) => { return !(item.naranja == 0 && item.amarillo == 0 && item.azul == 0 && item.rojo == 0 && item.violeta == 0) })
          }
          this.goToDetail(item);
        })
      }
    })
  }



  getStoreName(){
    return this.sessionManager.returnStoreName();
  }

  setPaginatorControlsId(tabName:any){
    if(window.screen.availWidth <= 800){
      this.paginationControlsId = this.pagControlerIdsObj.portrait[tabName]
      console.log(this.paginationControlsId)
    }
    else if(window.screen.availWidth >= 600){
      this.paginationControlsId = this.pagControlerIdsObj.landscape[tabName]
      console.log(this.paginationControlsId)
    }
  }

  conditionalsNumMesas(){
    if(window.screen.availWidth > 1745 ){
      this.configLandscape.itemsPerPage = 45;
    }

    if(window.screen.availWidth <= 1745 && window.screen.availWidth >= 1550){
      this.configLandscape.itemsPerPage = 35;
    }
    else if(window.screen.availWidth <= 1550 && window.screen.availWidth >= 1340){
      this.configLandscape.itemsPerPage = 55;
    }
    else if(window.screen.availWidth <= 1340 && window.screen.availWidth >= 900){
      this.configLandscape.itemsPerPage = 40;
    }
  }

  setPaginatorType(){
    if(window.screen.availWidth <= 800){
      this.paginatorTablet = true
    }
    else{
      this.paginatorTablet = false
    }
  }

  setTabletTitle(){
    if(window.screen.availWidth <= 800 && window.screen.availWidth >= 600){
      this.titleTablet = true
    }
    else{
      this.titleTablet = false
    }
  }

  openTab(tabName:any,evt:any) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      //@ts-ignore
      tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    //@ts-ignore
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";

    this.tabTitle = this.tabTitleObj[tabName].Title
    this.tabTitleBgColor = this.tabTitleObj[tabName].bgColor
    this.configLandscape.currentPage = 1;
    this.configPortrait.currentPage = 1;

    if(this.responsive){
      document.querySelector(".responsive-header")?.classList.remove("fixed-position")
        document.querySelector(".tab-container-responsive")?.classList.remove("tab-container-opened")
        document.querySelector(".tab-container-responsive")?.classList.add("tab-container-closed")
        this.hamburguerMenuOpened = false;
    }

    this.setPaginatorControlsId(tabName)
    this.conditionalsNumMesas()
    this.setPaginatorType()
    this.setTabletTitle()

    window.addEventListener('resize', () => {
      this.setPaginatorControlsId(tabName)
      this.conditionalsNumMesas()
      this.setPaginatorType()
      this.setTabletTitle()
      console.log(this.configLandscape.itemsPerPage)
    });
  }


}
