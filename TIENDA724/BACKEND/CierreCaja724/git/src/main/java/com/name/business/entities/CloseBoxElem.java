package com.name.business.entities;

import java.util.Date;

public class CloseBoxElem {
    private Long ID_CIERRE_CAJA;
    private Long ID_CAJA;
    private String CAJA;
    private String FECHA_APERTURA;
    private String CAJERO;

    public CloseBoxElem(
                      Long ID_CIERRE_CAJA,
                      Long ID_CAJA,
                      String CAJA,
                      String FECHA_APERTURA,
                      String CAJERO) {
        this.ID_CIERRE_CAJA = ID_CIERRE_CAJA;
        this.ID_CAJA = ID_CAJA;
        this.CAJA = CAJA;
        this.FECHA_APERTURA = FECHA_APERTURA;
        this.CAJERO = CAJERO;
    }


    public Long getID_CIERRE_CAJA() {
        return ID_CIERRE_CAJA;
    }

    public void setID_CIERRE_CAJA(Long ID_CIERRE_CAJA) {
        this.ID_CIERRE_CAJA = ID_CIERRE_CAJA;
    }

    public Long getID_CAJA() {
        return ID_CAJA;
    }

    public void setID_CAJA(Long ID_CAJA) {
        this.ID_CAJA = ID_CAJA;
    }

    public String getCAJA() {
        return CAJA;
    }

    public void setCAJA(String CAJA) {
        this.CAJA = CAJA;
    }

    public String getFECHA_APERTURA() {
        return FECHA_APERTURA;
    }

    public void setFECHA_APERTURA(String FECHA_APERTURA) {
        this.FECHA_APERTURA = FECHA_APERTURA;
    }

    public String getCAJERO() {
        return CAJERO;
    }

    public void setCAJERO(String CAJERO) {
        this.CAJERO = CAJERO;
    }


}
