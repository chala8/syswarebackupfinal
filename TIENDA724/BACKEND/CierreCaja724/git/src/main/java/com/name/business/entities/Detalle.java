package com.name.business.entities;

import java.util.Date;

public class Detalle {
    private String FECHA;
    private Long VALOR;
    private String NATURALEZA;
    private String NOTES;

    public Detalle(String FECHA,
                   Long VALOR,
                   String NATURALEZA,
                   String NOTES) {
        this.FECHA = FECHA;
        this.VALOR = VALOR;
        this.NATURALEZA = NATURALEZA;
        this.NOTES = NOTES;
    }

    public String getFECHA() {
        return FECHA;
    }

    public void setFECHA(String FECHA) {
        this.FECHA = FECHA;
    }

    public Long getVALOR() {
        return VALOR;
    }

    public void setVALOR(Long VALOR) {
        this.VALOR = VALOR;
    }

    public String getNATURALEZA() {
        return NATURALEZA;
    }

    public void setNATURALEZA(String NATURALEZA) {
        this.NATURALEZA = NATURALEZA;
    }

    public String getNOTES() {
        return NOTES;
    }

    public void setNOTES(String NOTES) {
        this.NOTES = NOTES;
    }


}
