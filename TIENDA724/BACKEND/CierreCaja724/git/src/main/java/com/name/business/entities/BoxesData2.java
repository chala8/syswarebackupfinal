package com.name.business.entities;

import java.util.Date;

public class BoxesData2 {

    private String CAJA_NUMBER;
    private String CAJA;
    private String TIENDA;
    private Long ID_STORE;
    private Long ID_CAJA;

    public BoxesData2(String CAJA_NUMBER,
                     String CAJA,
                     String TIENDA,
                     Long ID_STORE,
                     Long ID_CAJA) {
        this.CAJA_NUMBER = CAJA_NUMBER;
        this.CAJA = CAJA;
        this.TIENDA = TIENDA;
        this.ID_STORE = ID_STORE;
        this.ID_CAJA = ID_CAJA;
    }

    public String getCAJA_NUMBER() {
        return CAJA_NUMBER;
    }

    public void setCAJA_NUMBER(String CAJA_NUMBER) {
        this.CAJA_NUMBER = CAJA_NUMBER;
    }

    public String getCAJA() {
        return CAJA;
    }

    public void setCAJA(String CAJA) {
        this.CAJA = CAJA;
    }

    public String getTIENDA() {
        return TIENDA;
    }

    public void setTIENDA(String TIENDA) {
        this.TIENDA = TIENDA;
    }

    public Long getID_STORE() {
        return ID_STORE;
    }

    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    public Long getID_CAJA() {
        return ID_CAJA;
    }

    public void setID_CAJA(Long ID_CAJA) {
        this.ID_CAJA = ID_CAJA;
    }


}
