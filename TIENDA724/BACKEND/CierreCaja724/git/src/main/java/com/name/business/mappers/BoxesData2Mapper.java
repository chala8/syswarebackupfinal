package com.name.business.mappers;

import com.name.business.entities.BoxesData;
import com.name.business.entities.BoxesData2;
import com.name.business.entities.CajaReport;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BoxesData2Mapper implements ResultSetMapper<BoxesData2> {

    @Override
    public BoxesData2 map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BoxesData2(
                resultSet.getString("CAJA_NUMBER"),
                resultSet.getString("CAJA"),
                resultSet.getString("TIENDA"),
                resultSet.getLong("ID_STORE"),
                resultSet.getLong("ID_CAJA")

        );
    }
}
