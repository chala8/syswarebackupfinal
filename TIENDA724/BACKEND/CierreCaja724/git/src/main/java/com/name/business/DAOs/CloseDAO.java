package com.name.business.DAOs;
import com.name.business.entities.Detalle;
import com.name.business.entities.*;
import com.name.business.entities.CierreCaja;
import com.name.business.mappers.*;
import com.name.business.representations.CloseDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(CloseMapper.class)
public interface CloseDAO {
    @SqlUpdate("update CIERRE_CAJA " +
            " set NOTES=NOTES||' - '||sysdate||'   -  '||:notes "+
            " where ID_CAJA = :id_caja and consecutive=:consecutive")
    void putNotesCaja(@Bind("notes") String notes,
                      @Bind("id_caja") Long id_cierre_caja,
                      @Bind("consecutive") String consecutive);

    @SqlUpdate("update CIERRE_CAJA " +
            " set STATUS= :closeDTO.STATUS , NOTES = :closeDTO.NOTES , CLOSING_DATE = sysdate " +
            " where ID_CIERRE_CAJA = :id_cierre_caja")
    void updateCierreCaja(@Bind("id_cierre_caja") int id_cierre_caja, @BindBean("closeDTO") CloseDTO closeDTO);


    @SqlUpdate(" insert into URL_DOCUMENTOS(ID_DOCUMENTO, ID_TIPO_DOCUMENTO, URL)" +
               " values (:iddoc, 2, :url) ")
    void postURL(@Bind("iddoc") Long iddoc, @Bind("url") String url);


    @SqlQuery("select id_cierre_caja from CIERRE_CAJA where id_third_employee= :id_third_employee and status= :Status")
    List<String> Boxes(@Bind("id_third_employee") Long id_third_employee, @Bind("Status") String Status);

    @SqlQuery("select id_caja from CIERRE_CAJA where id_third_employee= :id_third_employee and status= :Status")
    List<String> Boxes2(@Bind("id_third_employee") Long id_third_employee, @Bind("Status") String Status);

    @SqlQuery("select URL from facturacion724.URL_DOCUMENTOS where ID_DOCUMENTO= :iddoc and ID_TIPO_DOCUMENTO= :idtype")
    String getdocurl(@Bind("iddoc") Long iddoc, @Bind("idtype") Long idtype);

    @SqlQuery("SELECT ID_CIERRE_CAJA FROM FACTURACION724.CIERRE_CAJA WHERE ID_CAJA = :idcaja AND CONSECUTIVE = :consecutive")
    Long getidcc(@Bind("idcaja") Long idcaja, @Bind("consecutive") Long consecutive);

    @SqlQuery("SELECT ID_CIERRE_CAJA FROM CIERRE_CAJA WHERE ID_CIERRE_CAJA IN (SELECT MAX( ID_CIERRE_CAJA ) FROM CIERRE_CAJA )\n")
    Long getPkLast();


    @SqlQuery("SELECT COUNT(ID_CIERRE_CAJA) FROM CIERRE_CAJA WHERE ID_CIERRE_CAJA = :id")
    Integer getValidatorID(@Bind("id") Long id);


    @SqlQuery("SELECT * FROM CIERRE_CAJA WHERE( ID_CIERRE_CAJA = :ID_CIERRE_CAJA OR :ID_CIERRE_CAJA IS NULL) AND ( ID_THIRD_EMPLOYEE = :ID_THIRD_EMPLOYEE OR :ID_THIRD_EMPLOYEE IS NULL) \n ")
    List<Close> readCommons(@Bind("ID_CIERRE_CAJA") Long ID_CIERRE_CAJA, @Bind("ID_THIRD_EMPLOYEE") Long ID_THIRD_EMPLOYEE);
    

    @SqlUpdate("INSERT INTO CIERRE_CAJA(ID_THIRD_EMPLOYEE,STARTING_DATE,CLOSING_DATE,ID_COMMON_STATE,ID_CAJA,STATUS,NOTES) " +
        "            VALUES ( :caja.ID_THIRD_EMPLOYEE ,SYSDATE ,:caja.CLOSING_DATE ,:id_common_state,:caja.ID_CAJA ,:caja.STATUS,:caja.NOTES )  ")
    void create(@Bind("id_common_state") Long id_common_state, @BindBean("caja") CloseDTO closeDTO);


    @RegisterMapper(CierreCajaMapper.class)
    @SqlQuery(" select starting_date,ca.description caja,cb.fullname Cajero,balance,notes,s.description store\n" +
            " from facturacion724.cierre_caja c,tercero724.third t, tercero724.common_basicinfo cb,tienda724.caja ca,tienda724.store s\n" +
            " where c.id_third_employee=t.id_third and t.id_common_basicinfo=cb.id_common_basicinfo and c.id_caja=ca.id_caja\n" +
            "  and s.id_store=ca.id_store and id_cierre_caja=:id_cierre_caja ")
    CierreCaja getMaestroCaja(@Bind("id_cierre_caja") Long id_cierre_caja);


    @RegisterMapper(CajaStoreMapper.class)
    @SqlQuery(" select a.CAJA_NUMBER as caja, b.DESCRIPTION as tienda from tienda724.caja a, tienda724.store b \n" +
            " where a.id_caja = :id_caja \n" +
            "  and a.id_store = b.id_store ")
    CajaStore getBoxDetails(@Bind("id_caja") Long id_caja);

    @RegisterMapper(DetalleMapper.class)
    @SqlQuery(" select movement_date fecha,valor,naturaleza,notes\n" +
            " from facturacion724.caja_movimientos\n" +
            " where id_cierre_caja=:id_cierre_caja order by fecha ")
    List<Detalle> getDetalleCaja(@Bind("id_cierre_caja") Long id_cierre_caja);



    @SqlQuery(" select sum(totalprice)\n" +
            " from facturacion724.bill b\n" +
            " where id_bill_type=1 and purchase_date between (select starting_date from facturacion724.cierre_caja where id_cierre_caja=:id_cierre_caja) and sysdate\n" +
            "  and b.id_caja=(select id_caja from facturacion724.cierre_caja where id_cierre_caja=:id_cierre_caja) and b.id_bill_state in (1,41) ")
    Long getSalesCaja(@Bind("id_cierre_caja") Long id_cierre_caja);



    @SqlQuery(" select ID_CIERRE_CAJA from FACTURACION724.cierre_caja where id_caja=:id_caja and STATUS='O' ")
    Long getIdCierreCaja(@Bind("id_caja") Long id_caja);



    @SqlQuery(" select CASH_UMBRAL from TIENDA724.caja where id_caja = :id_caja ")
    Double getCashUmbral(@Bind("id_caja") Long id_caja);


    @SqlQuery(" select m.MAIL from TERCERO724.THIRD t, TERCERO724.MAIL m, TERCERO724.DIRECTORY d\n" +
            "    where t.ID_THIRD = :id_third and t.ID_DIRECTORY  = d.ID_DIRECTORY and m.ID_DIRECTORY = d.ID_DIRECTORY ")
    String getMail(@Bind("id_third") Long id_third);


    @SqlQuery(" select s.DESCRIPTION from TIENDA724.STORE s, TIENDA724.CAJA c where s.ID_STORE = c.ID_STORE and c.ID_CAJA=:id_caja")
    String getStoreName(@Bind("id_caja") Long id_caja);


    @SqlQuery(" select CAJA_NUMBER from TIENDA724.CAJA where ID_CAJA = :id_caja ")
    String getCajaName(@Bind("id_caja") Long id_caja);


    @RegisterMapper(CloseBoxMapper.class)
    @SqlQuery(" SELECT ID_CIERRE_CAJA,C.ID_CAJA,C.DESCRIPTION CAJA,starting_date fecha_apertura,FULLNAME CAJERO \n" +
            "FROM FACTURACION724.CIERRE_CAJA CC,TIENDA724.CAJA C,TERCERO724.THIRD T,TERCERO724.COMMON_BASICINFO CBI\n" +
            "WHERE CC.ID_CAJA=C.ID_CAJA AND CC.ID_THIRD_EMPLOYEE=T.ID_THIRD AND T.ID_COMMON_BASICINFO=CBI.ID_COMMON_BASICINFO\n" +
            "AND STATUS='O' \n" +
            "AND C.ID_STORE=:id_store ")
    List<CloseBoxElem> getOpenBoxesByStore(@Bind("id_store") Long id_store);

    @SqlQuery("select nvl(sum(payment_value),0) venta\n" +
            "from facturacion724.bill b,facturacion724.detail_payment_bill d \n" +
            " where b.id_bill=d.id_bill\n" +
            "and d.id_payment_method = :id_payment\n" +
            " and id_bill_type = 1 and id_bill_state in (1,41)\n" +
            " and purchase_date between (select starting_date from facturacion724.cierre_caja where id_cierre_caja=:id_cierre_caja) and sysdate \n" +
            " and b.id_caja=(select id_caja from facturacion724.cierre_caja where id_cierre_caja=:id_cierre_caja)")
    Double getPayments(@Bind("id_cierre_caja") Long id_cierre_caja, @Bind("id_payment") Long id_payment);

    @SqlQuery(" select consecutive from facturacion724.cierre_caja where id_cierre_caja = :id_cierre_caja")
    Long getConsecutiveCaja(@Bind("id_cierre_caja") Long id_cierre_caja);

    @SqlQuery(" select facturacion724.getBalanceCaja2(:id_cierre_caja) from dual")
    Double getBalance(@Bind("id_cierre_caja") Long id_cierre_caja);


    @SqlQuery(" select facturacion724.getbalancajaabierta(:id_cierre_caja) from dual")
    Double getBalanceAbierta(@Bind("id_cierre_caja") Long id_cierre_caja);

    @SqlQuery(" Select CLOSING_DATE from FACTURACION724.CIERRE_CAJA where ID_CIERRE_CAJA = :id")
    String getDateCloseCaja(@Bind("id") Long id);

    @RegisterMapper(CajaReportMapper.class)
    @SqlQuery(" select cc.id_caja, cc.balance, c.caja_number, cb.FULLNAME, cc.STARTING_DATE, cc.CLOSING_DATE,cc.NOTES, cc.CONSECUTIVE,movement_date fecha,valor,naturaleza,cm.notes notas\n" +
            "             from FACTURACION724.cierre_caja cc,\n" +
            "                  FACTURACION724.caja_movimientos cm,\n" +
            "                 TIENDA724.CAJA c,\n" +
            "                 TERCERO724.THIRD t,\n" +
            "                 TERCERO724.COMMON_BASICINFO cb\n" +
            "             where cc.id_caja = c.id_caja and cc.id_cierre_caja=cm.id_cierre_caja\n" +
            "              and c.id_store = :id_store\n" +
            "              and cc.STATUS = 'C'\n" +
            "              and t.id_third = cc.ID_THIRD_EMPLOYEE\n" +
            "              and cb.id_common_basicinfo = t.id_common_basicinfo\n" +
            "              and cc.closing_date between :date1 and :date2+1\n" +
            "              order by 8 desc ")
    List<CajaReport> getReportCaja(@Bind("id_store") Long id_store,
                            @Bind("date1") Date date1,
                            @Bind("date2") Date date2);



    @RegisterMapper(CajaSelectMapper.class)
    @SqlQuery(" SELECT s.id_store,s.description store,ca.id_caja,ca.description caja,caja_number\n" +
            "from tienda724.caja_person cp,tienda724.caja ca,tienda724.store s\n" +
            "where cp.id_caja=ca.id_caja and ca.id_store=s.id_store\n" +
            "  and id_person=:id_person")
    List<CajaSelect> getBoxesToOpen(@Bind("id_person") Long id_person);




    @SqlQuery("select count(*) from facturacion724.cierre_caja where id_caja=:idcaja and status='O'")
    Long isOpen(@Bind("idcaja") Long idcaja);


    @RegisterMapper(BoxesDataMapper.class)
    @SqlQuery(" select caja_number,c.description caja,s.description tienda,s.id_store,cc.id_cierre_caja,cc.id_caja\n" +
            "from facturacion724.cierre_caja cc,tienda724.caja c,tienda724.store s\n" +
            "where cc.id_caja=c.id_caja and c.id_store=s.id_store\n" +
            "  and cc.id_third_employee=:id_third and status='O' ")
    List<BoxesData> getBoxes2(@Bind("id_third") Long id_third);


    @RegisterMapper(BoxesData2Mapper.class)
    @SqlQuery(" select s.description tienda,s.id_store,cp.id_caja,c.caja_number,c.description caja\n" +
            "from tienda724.caja_person cp,tercero724.person p,tercero724.third t, tienda724.caja c,tienda724.store s\n" +
            "where cp.id_person=p.id_person and cp.id_caja=c.id_caja and c.id_store=s.id_store and p.id_person=t.id_person\n" +
            "  and t.id_third=:id_third and cp.id_caja not in\n" +
            "                         (select id_caja from facturacion724.cierre_caja where status='O')\n" +
            "order by tienda,c.caja_number ")
    List<BoxesData2> getBoxes2P2(@Bind("id_third") Long id_third);


    @SqlCall(" call facturacion724.cierre_de_caja(:idCaja, :billetes, :monedas, :notas ) ")
    void cierre_de_caja(@Bind("idCaja") Long idCaja,
                        @Bind("billetes") String billetes,
                        @Bind("monedas") String monedas,
                        @Bind("notas") String notas);


}
