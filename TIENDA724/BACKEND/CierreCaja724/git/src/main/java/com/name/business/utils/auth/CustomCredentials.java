package com.name.business.utils.auth;

public class CustomCredentials {
    private String Token;

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        this.Token = token;
    }
}
