package com.name.business.businesses;

import com.name.business.DAOs.DetailCloseDAO;
import com.name.business.entities.DetailClose;
import com.name.business.representations.DetailCloseDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;

public class DetailCloseBussiness {
	private DetailCloseDAO detailCloseDAO;

	public DetailCloseBussiness(DetailCloseDAO detailCloseDAO) {
		this.detailCloseDAO = detailCloseDAO;
	}

	public Either<IException, List<DetailClose>> getDetailClose(Long ID_CIERRE_CAJA) {
		List<String> msn = new ArrayList<>();
		try {
			if(ID_CIERRE_CAJA!=null && ID_CIERRE_CAJA<=0){
				return Either.left(new BussinessException(messages_error("El cierre de caja no existe")));
			}else{
				//System.out.println("|||||||||||| Starting consults Detail Close ||||||||||||||||| ");
				return Either.right(detailCloseDAO.readCommons(ID_CIERRE_CAJA));
			}

		} catch (Exception e) {
			e.printStackTrace();
			return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
		}
	}

	public Either<IException, Long> createDetailClose(List<DetailCloseDTO> detailCloseDTOList, Long ID_CIERRE_CAJA) {
		try {
			//System.out.println("|||||||||||| Starting consults Close ||||||||||||||||| ");
			for (DetailCloseDTO detailCloseDTO:detailCloseDTOList) {
				detailCloseDAO.create(detailCloseDTO,ID_CIERRE_CAJA);
			}
			detailCloseDAO.updateBalance(ID_CIERRE_CAJA);
			return Either.right(Long.valueOf(detailCloseDTOList.size()));
		} catch (Exception e) {
			e.printStackTrace();
			return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
		}
	}
}
