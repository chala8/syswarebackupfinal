package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;


public class CloseDTO {


    private Long ID_THIRD_EMPLOYEE;
    private Date STARTING_DATE;
    private Date CLOSING_DATE;
    private Long ID_COMMON_STATE;
    private Long ID_CAJA;
    private String STATUS;
    private String NOTES;
    private Double BALANCE;

    @JsonCreator
    public CloseDTO(@JsonProperty("ID_THIRD_EMPLOYEE")  Long ID_THIRD_EMPLOYEE,@JsonProperty("STARTING_DATE") Date STARTING_DATE,@JsonProperty("CLOSING_DATE") Date CLOSING_DATE,
                    @JsonProperty("ID_COMMON_STATE") Long ID_COMMON_STATE,@JsonProperty("ID_CAJA") Long ID_CAJA, @JsonProperty("STATUS")  String STATUS,
                    @JsonProperty("NOTES") String NOTES, @JsonProperty("BALANCE") Double BALANCE) {
        this.ID_THIRD_EMPLOYEE = ID_THIRD_EMPLOYEE;
        this.STARTING_DATE = STARTING_DATE;
        this.CLOSING_DATE = CLOSING_DATE;
        this.ID_COMMON_STATE = ID_COMMON_STATE;
        this.ID_CAJA = ID_CAJA;
        this.STATUS = STATUS;
        this.NOTES = NOTES;
        this.BALANCE = BALANCE;
    }



    public Long getID_THIRD_EMPLOYEE() {
        return ID_THIRD_EMPLOYEE;
    }

    public void setID_THIRD_EMPLOYEE(Long ID_THIRD_EMPLOYEE) {
        this.ID_THIRD_EMPLOYEE = ID_THIRD_EMPLOYEE;
    }

    public Date getSTARTING_DATE() {
        return STARTING_DATE;
    }

    public void setSTARTING_DATE(Date STARTING_DATE) {
        this.STARTING_DATE = STARTING_DATE;
    }

    public Date getCLOSING_DATE() {
        return CLOSING_DATE;
    }

    public void setCLOSING_DATE(Date CLOSING_DATE) {
        this.CLOSING_DATE = CLOSING_DATE;
    }

    public Long getID_COMMON_STATE() {
        return ID_COMMON_STATE;
    }

    public void setID_COMMON_STATE(Long ID_COMMON_STATE) {
        this.ID_COMMON_STATE = ID_COMMON_STATE;
    }

    public Long getID_CAJA() {
        return ID_CAJA;
    }

    public void setID_CAJA(Long ID_CAJA) {
        this.ID_CAJA = ID_CAJA;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getNOTES() {
        return NOTES;
    }

    public void setNOTES(String NOTES) {
        this.NOTES = NOTES;
    }

    public Double getBALANCE() {
        return BALANCE;
    }

    public void setBALANCE(Double BALANCE) {
        this.BALANCE = BALANCE;
    }
}
