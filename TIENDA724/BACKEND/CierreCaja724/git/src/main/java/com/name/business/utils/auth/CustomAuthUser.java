package com.name.business.utils.auth;

import java.security.Principal;
import java.util.List;

public class CustomAuthUser implements Principal {
    private final String name;
    private final List<String> roles;

    public CustomAuthUser(String name, List<String> role) {
        this.name = name;
        this.roles = role;
    }

    @Override
    public String getName() {
        return name;
    }

    public List<String> getRoles() {
        return roles;
    }
}