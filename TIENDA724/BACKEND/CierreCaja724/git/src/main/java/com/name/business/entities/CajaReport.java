package com.name.business.entities;

import java.util.Date;

public class CajaReport {
    private Long idcaja;
    private String CAJA_NUMBER;
    private String FULLNAME;
    private String STARTING_DATE;
    private String CLOSING_DATE;
    private String NOTES;
    private Long CONSECUTIVE;
    private String MOVEMENT_DATE;
    private Double VALOR;
    private String NATURALEZA;
    private String NOTAS;
    private Double BALANCE;


    public CajaReport(Long idcaja,
                      Double BALANCE,
                      String CAJA_NUMBER,
                      String FULLNAME,
                      String STARTING_DATE,
                      String CLOSING_DATE,
                      String NOTES,
                      Long CONSECUTIVE,
                      String MOVEMENT_DATE,
                      Double VALOR,
                      String NATURALEZA,
                      String NOTAS) {
        this.idcaja = idcaja;
        this.BALANCE = BALANCE;
        this.CAJA_NUMBER = CAJA_NUMBER;
        this.FULLNAME = FULLNAME;
        this.STARTING_DATE = STARTING_DATE;
        this.CLOSING_DATE = CLOSING_DATE;
        this.NOTES = NOTES;
        this.CONSECUTIVE = CONSECUTIVE;
        this.MOVEMENT_DATE = MOVEMENT_DATE;
        this.VALOR = VALOR;
        this.NATURALEZA = NATURALEZA;
        this.NOTAS = NOTAS;
    }
    public Long getidcaja() {
        return idcaja;
    }

    public void setidcaja(Long idcaja) {
        this.idcaja = idcaja;
    }

    public Double getBALANCE() {
        return BALANCE;
    }

    public void setBALANCE(Double BALANCE) {
        this.BALANCE = BALANCE;
    }

    public String getMOVEMENT_DATE() {
        return MOVEMENT_DATE;
    }

    public void setMOVEMENT_DATE(String MOVEMENT_DATE) {
        this.MOVEMENT_DATE = MOVEMENT_DATE;
    }

    public Double getVALOR() {
        return VALOR;
    }

    public void setVALOR(Double VALOR) {
        this.VALOR = VALOR;
    }

    public String getNATURALEZA() {
        return NATURALEZA;
    }

    public void setNATURALEZA(String NATURALEZA) {
        this.NATURALEZA = NATURALEZA;
    }

    public String getNOTAS() {
        return NOTAS;
    }

    public void setNOTAS(String NOTAS) {
        this.NOTAS = NOTAS;
    }

    public String getCAJA_NUMBER() {
        return CAJA_NUMBER;
    }

    public void setCAJA_NUMBER(String CAJA_NUMBER) {
        this.CAJA_NUMBER = CAJA_NUMBER;
    }

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }

    public String getSTARTING_DATE() {
        return STARTING_DATE;
    }

    public void setSTARTING_DATE(String STARTING_DATE) {
        this.STARTING_DATE = STARTING_DATE;
    }

    public String getCLOSING_DATE() {
        return CLOSING_DATE;
    }

    public void setCLOSING_DATE(String CLOSING_DATE) {
        this.CLOSING_DATE = CLOSING_DATE;
    }

    public String getNOTES() {
        return NOTES;
    }

    public void setNOTES(String NOTES) {
        this.NOTES = NOTES;
    }

    public Long getCONSECUTIVE() {
        return CONSECUTIVE;
    }

    public void setCONSECUTIVE(Long CONSECUTIVE) {
        this.CONSECUTIVE = CONSECUTIVE;
    }
}
