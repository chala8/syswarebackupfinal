package com.name.business.DAOs;
import com.name.business.entities.Store;
import com.name.business.mappers.StoreMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(StoreMapper.class)
public interface StoreDAO {


    @SqlQuery("select s.id_store,s.description store,CA.caja_number,s.id_third " +
            " from tienda724.caja CA,tienda724.store S " +
            " where CA.id_store=s.id_store " +
            " and CA.id_caja= :id_caja")
    List<Store> dataBox(@Bind("id_caja") int id_caja);


}
