package com.name.business.businesses;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.DAOs.*;
import com.name.business.entities.*;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import com.name.business.representations.CajaPdfDTO;
import com.name.business.representations.CloseCompleteDTO;
import com.name.business.representations.CloseDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import org.skife.jdbi.v2.sqlobject.Bind;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileOutputStream;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;

public class CloseBusiness {

    private CloseDAO billDAO;
    private StoreDAO storeDAO;
    private DetailCloseBussiness detailCloseBussiness;
    //VARIABLES PARA CONEXIÓN CORREO
    static final String FROM = "soporte@tienda724.com";
    static final String FROMNAME = "Soporte Tienda 724";
    static final String SMTP_USERNAME = "soporte@tienda724.com";
    static final String SMTP_PASSWORD = "Lx3BPR5j8pdt2xmm";
    static final String HOST = "smtp.mail.us-east-1.awsapps.com";
    static final int PORT = 465;
    ///////////////////////////////////

    public Either<IException, String> updateCierreCaja(int id_cierre_caja, CloseDTO closeDTO) {
        //System.out.println(id_cierre_caja);
        try {
            billDAO.updateCierreCaja(id_cierre_caja, closeDTO);
            return Either.right("OK!");
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, String> putNotesCaja(String notes, Long id_caja, String consecutive) {
        try {
            billDAO.putNotesCaja(notes, id_caja, consecutive);
            return Either.right("OK!");
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, List<Store>> dataBox(int id_caja) {
        try {
            return Either.right(this.storeDAO.dataBox(id_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, List <String>> Boxes(Long id_third_employee, String status) {
        try {
           return Either.right(billDAO.Boxes(id_third_employee, status));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }

    }

    public void postURL(Long iddoc, String url) {

        billDAO.postURL(iddoc,url);

    }

    public Either<IException, List <String>> Boxes2(Long id_third_employee, String status) {
        try {
            return Either.right(billDAO.Boxes2(id_third_employee, status));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, String> getdocurl(Long iddoc, Long idtype) {
        try {
            return Either.right(billDAO.getdocurl(iddoc, idtype));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, Long> getidcc(Long idcaja, Long consecutive) {
        try {
            return Either.right(billDAO.getidcc(idcaja, consecutive));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public CloseBusiness(CloseDAO billDAO, DetailCloseBussiness detailCloseBussiness, StoreDAO storeDAO) {
        this.billDAO = billDAO;
        this.detailCloseBussiness = detailCloseBussiness;
        this.storeDAO = storeDAO;
    }

    public Either<IException, List<Close>> getBill(Long ID_CIERRE_CAJA, Long id_employee) {
        List<String> msn = new ArrayList<>();
        try {
            //System.out.println("|||||||||||| Starting consults Close ||||||||||||||||| ");
            return Either.right(billDAO.readCommons(ID_CIERRE_CAJA,id_employee));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, Long> createClose(CloseDTO detailCloseDTO) {
    try {
      //System.out.println("|||||||||||| Starting consults Close ||||||||||||||||| ");

      billDAO.create(new Long(301),detailCloseDTO);
      return Either.right(Long.valueOf(billDAO.getPkLast()));
    } catch (Exception e) {
      e.printStackTrace();
      return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
    }
  }

    public Either<IException, Long> createCloseBulk(CloseCompleteDTO closeCompleteDTO) {
    try {
      if(closeCompleteDTO.getCloseDTO()!=null){
        //System.out.println("|||||||||||| Starting consults Close ||||||||||||||||| ");
        CloseDTO detailCloseDTO = closeCompleteDTO.getCloseDTO();
        billDAO.create(new Long(301),detailCloseDTO);

        if(closeCompleteDTO.getDetailCloseDTOS()!=null && closeCompleteDTO.getDetailCloseDTOS().size()>0){
          return  detailCloseBussiness.createDetailClose(closeCompleteDTO.getDetailCloseDTOS(),billDAO.getPkLast());

        }else{
          return Either.right(Long.valueOf(billDAO.getPkLast()));
        }

      }else{
        return Either.left(new BussinessException(messages_error("Requiere el formato JSON para cierre de caja")));
      }



    } catch (Exception e) {
      e.printStackTrace();
      return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
    }
  }

    public Either<IException, CierreCaja> getMaestroCaja(Long id_cierre_caja) {
        try {
            return Either.right(billDAO.getMaestroCaja(id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, List<Detalle>> getDetalleCaja(Long id_cierre_caja) {
        try {
            return Either.right(billDAO.getDetalleCaja(id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, CajaStore> getBoxDetails(Long id_caja) {
        try {
            return Either.right(billDAO.getBoxDetails(id_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, Long> getSalesCaja(Long id_cierre_caja) {
        try {
            return Either.right(billDAO.getSalesCaja(id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, List<CloseBoxElem>> getOpenBoxesByStore(Long id_store) {
        try {
            return Either.right(billDAO.getOpenBoxesByStore(id_store));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, Double> getPayments(Long id_cierre_caja, Long id_payment) {
        try {
            return Either.right(billDAO.getPayments(id_cierre_caja, id_payment));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Boolean SendEmail(String Destinatario,String Asunto,String CuerpoCorreo) throws UnsupportedEncodingException, MessagingException {
        String[] CorreoEnFormatoLista = {Destinatario};
        return SendEmail(CorreoEnFormatoLista,Asunto,CuerpoCorreo);
    }

    public Boolean SendEmail(String[] Destinatario,String Asunto,String CuerpoCorreo) throws MessagingException, UnsupportedEncodingException {
        Boolean Resultado = true;//De ser falso significaria que ocurrió algun error
        // Create a Properties object to contain connection configuration information.
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtps");
        props.put("mail.smtp.port", PORT);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");

        // Create a Session object to represent a mail session with the specified properties.
        Session session = Session.getDefaultInstance(props);

        // Create a message with the specified information.
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(FROM,FROMNAME));
        //Agrego los receptores del correo
        for(String cc:Destinatario){
            msg.addRecipients(Message.RecipientType.CC,InternetAddress.parse(cc));
        }
        ////////////////
        msg.setSubject(Asunto);
        msg.setContent(CuerpoCorreo,"text/html");
        //Metodo que envia el correo, SINCRONICO
        Transport transport = session.getTransport();
        try
        {
            //System.out.println("Sending...");
            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            //System.out.println("Email sent!");
            Resultado = true;
        }
        catch (Exception ex) {
            //System.out.println("The email was not sent.");
            //System.out.println("Error message: " + ex.getMessage());
            Resultado = false;
        }
        finally
        {
            // Close and terminate the connection.
            transport.close();
        }
        return Resultado;
    }

    public  Boolean alertForEmail(Long id_caja, Long id_third) {
        try {
            Long id_cierre_caja = billDAO.getIdCierreCaja(id_caja);
            Double umbral_cash = billDAO.getCashUmbral(id_caja);
            Double ventas = billDAO.getBalance(id_cierre_caja);

            if(ventas>=umbral_cash){
                //ACA SE REALIZA EL ENVIO DEL EMAIL DE ALERTA
                //String[] correos = {"Correo1","Correo2","Correo3","etc"};//este seria el formato para varios destinatarios
                String correo = billDAO.getMail(id_third);//este seria para uno solo
                String Asunto = "Correo Soporte Tienda 724 (No Responder)";
                String cajaNombre = billDAO.getCajaName(id_caja);
                String tiendaNombre = billDAO.getStoreName(id_caja);
                String Cuerpo = String.join(
                        System.getProperty("line.separator"),
                        "<h1>Correo Soporte Tienda 724</h1>",
                        "<p>Este correo se envia para informar que en la caja"+cajaNombre+", ubicada en la tienda "+tiendaNombre+
                                ", se excedio la cantidad de dinero de umbral de $"+umbral_cash+
                                ", al momento del envio de este correo hay en caja un total de $"+ventas+".</p>." +
                                "<p>Atentamente Soporte Sysware Ingenieria.</p>"
                );
                Boolean Resultado = SendEmail(correo,Asunto,Cuerpo);//Retorna falso si no pudo enviar el correo, de lo contrario verdadero
                return Resultado;
            }

            return false;
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }


    public  Double balancev2(Long id_cierre_caja) {
        try {

            Double ventas = billDAO.getBalance(id_cierre_caja);

               return ventas;
        } catch (Exception e) {
            e.printStackTrace();

            return new Double(-1);
        }
    }


    public  Double balanceAbierta(Long id_cierre_caja) {
        try {

            Double ventas = billDAO.getBalanceAbierta(id_cierre_caja);

            return ventas;
        } catch (Exception e) {
            e.printStackTrace();

            return new Double(-1);
        }
    }




    public Either<IException, String> printPDF(CajaPdfDTO cajaPdfDTO){
        // Se crea el documento
        Rectangle envelope = new Rectangle(137, 598);
        Document documento = new Document(envelope, 0f, 0f, 10f, 0f);
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        format.setMaximumFractionDigits(0);
        String response = "Bad!!!";
        try { // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
            String consecutivo = cajaPdfDTO.getnumero_documento();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 5, Font.NORMAL);

            BaseFont HELVETICA2 = BaseFont.createFont(
                    BaseFont.HELVETICA,
                    BaseFont.WINANSI, false);
            Font fontText = new Font(HELVETICA2, 6, Font.BOLD);

            BaseFont TABLETITLE = BaseFont.createFont(
                    BaseFont.HELVETICA,
                    BaseFont.WINANSI, false);
            Font fontTitle = new Font(TABLETITLE, 7, Font.BOLD);

            BaseFont bigBold = BaseFont.createFont(
                    BaseFont.HELVETICA,
                    BaseFont.WINANSI, false);
            Font bigBoldFont = new Font(bigBold, 8, Font.BOLD);

            String nombrePdf = cajaPdfDTO.getfecha_inicio().replaceAll("/","_") + "-" + cajaPdfDTO.getfecha_finalizacion().replaceAll("/","_") + "-" + cajaPdfDTO.getcodigo() + ".pdf";
            //String nombrePdf = consecutivo + ".pdf";
            FileOutputStream ficheroPdf = new FileOutputStream("/usr/share/nginx/html/docscaja/" + nombrePdf);
            //FileOutputStream ficheroPdf = new FileOutputStream("./" + nombrePdf);
            ///usr/share/nginx/html/docscaja
            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento, ficheroPdf).setInitialLeading(20);
            documento.open();

            documento.newPage();
            documento.add(new Chunk(""));


            PdfPCell saltoLinea = new PdfPCell(new Phrase("    "));
            saltoLinea.setBorder(PdfPCell.NO_BORDER);
            saltoLinea.setHorizontalAlignment(Element.ALIGN_CENTER);


            // TABLA DE TIENDA
            PdfPTable tablaTienda = new PdfPTable(1);
            PdfPCell tienda = new PdfPCell(new Phrase(cajaPdfDTO.getnombre_empresa(), fontText));
            PdfPCell nit = new PdfPCell(new Phrase(cajaPdfDTO.getnit(), fontText));
            tienda.setBorder(PdfPCell.NO_BORDER);
            tienda.setHorizontalAlignment(Element.ALIGN_CENTER);
            nit.setBorder(PdfPCell.NO_BORDER);
            nit.setHorizontalAlignment(Element.ALIGN_CENTER);
            //tablaTienda.addCell(tienda);
            //tablaTienda.addCell(nit);

            // TABLA DE TITULO
            PdfPTable tablaTitulo = new PdfPTable(1);
            PdfPCell cell1 = new PdfPCell(new Phrase("REPORTE DE CONTROL DE CAJA", bigBoldFont));
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            try {
                Image img = Image.getInstance("/usr/share/nginx/html/logos/" + cajaPdfDTO.getnit() + ".jpg");
                //Image img = Image.getInstance("./"+billpdfDTO.getnit()+".jpg");
                PdfPCell imagen = new PdfPCell(img, true);
                imagen.setBorder(PdfPCell.NO_BORDER);
                imagen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaTitulo.addCell(imagen);
            } catch (Exception e) {
                //System.out.println(e.toString());
            }
            tablaTitulo.addCell(cell1);
            tablaTitulo.addCell(tienda);
            tablaTitulo.addCell(nit);


            //TABLA DE FECHAS
            PdfPTable tablaFechas = new PdfPTable(1);
            //String cellFechainicialS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cajaPdfDTO.getfecha_inicio());
            PdfPCell cellFechainicial = new PdfPCell(new Phrase("Fecha apertura: " + cajaPdfDTO.getfecha_inicio(), fontText));
            //String cellFechafinalizacionS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cajaPdfDTO.getfecha_finalizacion());
            PdfPCell cellFechaFinal = new PdfPCell(new Phrase("Fecha cierre: " + cajaPdfDTO.getfecha_finalizacion(), fontText));
            cellFechainicial.setBorder(PdfPCell.NO_BORDER);
            cellFechainicial.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellFechaFinal.setBorder(PdfPCell.NO_BORDER);
            cellFechaFinal.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaFechas.addCell(cellFechainicial);
            tablaFechas.addCell(cellFechaFinal);


            //TABLA DE DATOS
            PdfPTable tablaDatos = new PdfPTable(2);
            PdfPCell cellDocumento = new PdfPCell(new Phrase("Cierre # " + cajaPdfDTO.getnumero_documento().split("_")[cajaPdfDTO.getnumero_documento().split("_").length - 1], fontText));
            PdfPCell cellCajero = new PdfPCell(new Phrase("Cajero: " + cajaPdfDTO.getnombre_cajero().replaceAll("null", ""), fontText));
            PdfPCell cellCaja = new PdfPCell(new Phrase("Caja numero: " + cajaPdfDTO.getnombre_caja(), fontText));
            PdfPCell cellTienda = new PdfPCell(new Phrase("Tienda: " + cajaPdfDTO.getnombre_tienda(), fontText));

            cellDocumento.setBorder(PdfPCell.NO_BORDER);
            cellDocumento.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaDatos.addCell(cellDocumento);

            cellCajero.setBorder(PdfPCell.NO_BORDER);
            cellCajero.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDatos.addCell(cellCajero);

            cellCaja.setBorder(PdfPCell.NO_BORDER);
            cellCaja.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaDatos.addCell(cellCaja);

            cellTienda.setBorder(PdfPCell.NO_BORDER);
            cellTienda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDatos.addCell(cellTienda);


            //TABLA BALANCE
            PdfPTable tablaBalance;
            tablaBalance = new PdfPTable(1);

            //TABLA DETALLES INGRESOS
            int k = 0;
            PdfPTable tablaDetalles = new PdfPTable(1);


            PdfPTable tablaDetalles2 = new PdfPTable(1);


            PdfPTable tablaDetalle3 = new PdfPTable(3);


            PdfPTable tablaDetalles4 = new PdfPTable(1);


            Double ingresos = new Double(0);
            Double egresos = new Double(0);
            PdfPCell cellFecha = new PdfPCell(new Phrase("Fecha", fontTitle));
            cellFecha.setBorder(PdfPCell.NO_BORDER);
            cellFecha.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cellTipo = new PdfPCell(new Phrase("Tipo", fontTitle));
            cellTipo.setBorder(PdfPCell.NO_BORDER);
            cellTipo.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cellNotas = new PdfPCell(new Phrase("Notas", fontTitle));
            cellNotas.setBorder(PdfPCell.NO_BORDER);
            cellNotas.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cellValor = new PdfPCell(new Phrase("Valor", fontTitle));
            cellValor.setBorder(PdfPCell.NO_BORDER);
            cellValor.setHorizontalAlignment(Element.ALIGN_CENTER);

            tablaDetalle3.addCell(cellFecha);
            tablaDetalle3.addCell(cellTipo);
            tablaDetalle3.addCell(cellValor);
            tablaDetalles4.addCell(cellNotas);


            PdfPCell cellData1 = new PdfPCell(tablaDetalle3);
            cellData1.setBorder(PdfPCell.NO_BORDER);
            cellData1.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cellData2 = new PdfPCell(tablaDetalles4);
            cellData2.setBorder(PdfPCell.NO_BORDER);
            cellData2.setHorizontalAlignment(Element.ALIGN_CENTER);


            tablaDetalles2.addCell(cellData1);
            tablaDetalles2.addCell(cellData2);


            PdfPCell cellData3 = new PdfPCell(tablaDetalles2);
            cellData3.setBorder(PdfPCell.BOTTOM);
            cellData3.setHorizontalAlignment(Element.ALIGN_CENTER);


            tablaDetalles.addCell(cellData3);


            List<List<String>> theList = cajaPdfDTO.getdetalles();
            for (int i = 0; i < cajaPdfDTO.getdetalles().size(); i++) {
                System.out.println(theList.get(i).toString());
            }


            System.out.println("-------------------------------INGRESOS-------------------------------------");
            for (int i = 0; i < cajaPdfDTO.getdetalles().size(); i++) {
                if (theList.get(i).get(1).equals("D")) {

                    System.out.println(theList.get(i).toString());
                    tablaDetalles2 = new PdfPTable(1);


                    tablaDetalle3 = new PdfPTable(3);


                    tablaDetalles4 = new PdfPTable(1);

                    PdfPCell cellF = new PdfPCell(new Phrase(theList.get(i).get(0), fontH1));
                    cellF.setBorder(PdfPCell.NO_BORDER);
                    cellF.setHorizontalAlignment(Element.ALIGN_CENTER);
                    PdfPCell cellT = new PdfPCell();


                    cellT = new PdfPCell(new Phrase("Ingreso", fontH1));
                    cellT.setBorder(PdfPCell.NO_BORDER);
                    cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
                    ingresos += Double.parseDouble(theList.get(i).get(3));

                    PdfPCell cellN;

                    if (theList.get(i).get(2).length() <= 60) {
                        cellN = new PdfPCell(new Phrase(theList.get(i).get(2), fontH1));
                        cellN.setBorder(PdfPCell.NO_BORDER);
                        cellN.setHorizontalAlignment(Element.ALIGN_CENTER);
                    } else {
                        cellN = new PdfPCell(new Phrase(theList.get(i).get(2).substring(0, 60) + "...", fontH1));
                        cellN.setBorder(PdfPCell.NO_BORDER);
                        cellN.setHorizontalAlignment(Element.ALIGN_CENTER);
                    }

                    Double var2 = Double.parseDouble(theList.get(i).get(3));
            /*if(var!=0){
                var = var*-1;
            }*/
                    String result2 = String.format("%.0f", var2);
                    PdfPCell cellV = new PdfPCell(new Phrase(format.format(Long.parseLong(result2)), fontH1));
                    cellV.setBorder(PdfPCell.NO_BORDER);
                    cellV.setHorizontalAlignment(Element.ALIGN_CENTER);

                    if (var2 != 0) {


                        tablaDetalle3.addCell(cellF);
                        tablaDetalle3.addCell(cellT);
                        tablaDetalle3.addCell(cellV);

                        tablaDetalles4.addCell(cellN);


                        cellData1 = new PdfPCell(tablaDetalle3);
                        cellData1.setBorder(PdfPCell.NO_BORDER);
                        cellData1.setHorizontalAlignment(Element.ALIGN_CENTER);

                        cellData2 = new PdfPCell(tablaDetalles4);
                        cellData2.setBorder(PdfPCell.NO_BORDER);
                        cellData2.setHorizontalAlignment(Element.ALIGN_CENTER);


                        tablaDetalles2.addCell(cellData1);
                        tablaDetalles2.addCell(cellData2);


                        cellData3 = new PdfPCell(tablaDetalles2);
                        cellData3.setBorder(PdfPCell.BOTTOM);
                        cellData3.setHorizontalAlignment(Element.ALIGN_CENTER);


                        tablaDetalles.addCell(cellData3);

                    }
                    k++;
                    System.out.println(k);
                }

            }

            PdfPTable tablaDIngresos = new PdfPTable(2);
            PdfPCell ingresos1 = new PdfPCell(new Phrase("Total Ingresos: ", fontTitle));
            ingresos1.setBorder(PdfPCell.NO_BORDER);
            ingresos1.setHorizontalAlignment(Element.ALIGN_CENTER);
            PdfPCell ingresos2 = new PdfPCell(new Phrase(format.format(Long.parseLong(String.format("%.0f", ingresos))), fontTitle));
            ingresos2.setBorder(PdfPCell.NO_BORDER);
            ingresos2.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDIngresos.addCell(ingresos1);
            tablaDIngresos.addCell(ingresos2);
            PdfPCell blank = new PdfPCell(new Phrase(" "));
            blank.setBorder(PdfPCell.NO_BORDER);
            blank.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDIngresos.addCell(blank);
            tablaDIngresos.addCell(blank);


            //TABLA DETALLES EGRESOS


            PdfPTable tablaDetallese = new PdfPTable(1);


            PdfPTable tablaDetalles2e = new PdfPTable(1);


            PdfPTable tablaDetalle3e = new PdfPTable(3);


            PdfPTable tablaDetalles4e = new PdfPTable(1);

            System.out.println("-------------------------------EGRESOS-------------------------------------");
            for (int i = 0; i < cajaPdfDTO.getdetalles().size(); i++)
            {

                if(theList.get(i).get(1).equals("C") && !theList.get(i).get(2).contains("Monedas") && !theList.get(i).get(2).contains("Billetes") && !theList.get(i).get(2).equals("Total de ventas")){

                    System.out.println(theList.get(i).toString());
                    tablaDetalles2e = new PdfPTable(1);


                    tablaDetalle3e = new PdfPTable(3);


                    tablaDetalles4e = new PdfPTable(1);

                    PdfPCell cellF = new PdfPCell(new Phrase(theList.get(i).get(0),fontH1));
                    cellF.setBorder(PdfPCell.NO_BORDER);
                    cellF.setHorizontalAlignment(Element.ALIGN_CENTER);
                    PdfPCell cellT = new PdfPCell();


                    cellT = new PdfPCell(new Phrase("Egreso",fontH1));
                    cellT.setBorder(PdfPCell.NO_BORDER);
                    cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
                    egresos += Double.parseDouble(theList.get(i).get(3));

                    PdfPCell cellN;

                    if(theList.get(i).get(2).length()<=60){
                        cellN = new PdfPCell(new Phrase(theList.get(i).get(2),fontH1));
                        cellN.setBorder(PdfPCell.NO_BORDER);
                        cellN.setHorizontalAlignment(Element.ALIGN_CENTER);
                    }else{
                        cellN = new PdfPCell(new Phrase(theList.get(i).get(2).substring(0,60)+"...",fontH1));
                        cellN.setBorder(PdfPCell.NO_BORDER);
                        cellN.setHorizontalAlignment(Element.ALIGN_CENTER);
                    }

                    Double var2 = Double.parseDouble(theList.get(i).get(3));
            /*if(var!=0){
                var = var*-1;
            }*/
                    String result2 = String.format("%.0f", var2);
                    PdfPCell cellV = new PdfPCell(new Phrase(format.format(Long.parseLong(result2)),fontH1));
                    cellV.setBorder(PdfPCell.NO_BORDER);
                    cellV.setHorizontalAlignment(Element.ALIGN_CENTER);

                    if(var2!=0){


                        tablaDetalle3e.addCell(cellF);
                        tablaDetalle3e.addCell(cellT);
                        tablaDetalle3e.addCell(cellV);

                        tablaDetalles4e.addCell(cellN);


                        cellData1 =new PdfPCell(tablaDetalle3e);
                        cellData1.setBorder(PdfPCell.NO_BORDER);
                        cellData1.setHorizontalAlignment(Element.ALIGN_CENTER);

                        cellData2 =new PdfPCell(tablaDetalles4e);
                        cellData2.setBorder(PdfPCell.NO_BORDER);
                        cellData2.setHorizontalAlignment(Element.ALIGN_CENTER);


                        tablaDetalles2e.addCell(cellData1);
                        tablaDetalles2e.addCell(cellData2);


                        cellData3 =new PdfPCell(tablaDetalles2e);
                        cellData3.setBorder(PdfPCell.BOTTOM);
                        cellData3.setHorizontalAlignment(Element.ALIGN_CENTER);


                        tablaDetallese.addCell(cellData3);

                    }

                }

            }

            PdfPTable tablaDEgresos = new PdfPTable(2);
            PdfPCell ingresos1X = new PdfPCell(new Phrase("Total Egresos: ", fontTitle));
            ingresos1X.setBorder(PdfPCell.NO_BORDER);
            ingresos1X.setHorizontalAlignment(Element.ALIGN_CENTER);
            PdfPCell ingresos2X = new PdfPCell(new Phrase(format.format(Long.parseLong(String.format("%.0f", egresos))),fontTitle));
            ingresos2X.setBorder(PdfPCell.NO_BORDER);
            ingresos2X.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDEgresos.addCell(ingresos1X);
            tablaDEgresos.addCell(ingresos2X);
            tablaDEgresos.addCell(blank);
            tablaDEgresos.addCell(blank);




            PdfPTable tablaDEgresoster = new PdfPTable(2);
            PdfPCell ingresos1Xter = new PdfPCell(new Phrase("Total a Recaudar: ", fontTitle));
            ingresos1Xter.setBorder(PdfPCell.TOP);
            ingresos1Xter.setHorizontalAlignment(Element.ALIGN_CENTER);
            PdfPCell ingresos2Xter = new PdfPCell(new Phrase(format.format(Long.parseLong(String.format("%.0f", ingresos-egresos))),fontTitle));
            ingresos2Xter.setBorder(PdfPCell.TOP);
            ingresos2Xter.setHorizontalAlignment(Element.ALIGN_CENTER);
            PdfPCell blank2 = new PdfPCell(new Phrase(" "));
            blank2.setBorder(PdfPCell.TOP);
            blank2.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDEgresoster.addCell(ingresos1Xter);
            tablaDEgresoster.addCell(ingresos2Xter);
            tablaDEgresoster.addCell(blank2);
            tablaDEgresoster.addCell(blank2);




            //TABLA DETALLES EGRESOS ESPECIFICOS

            PdfPTable tablaDetalleseX = new PdfPTable(1);


            PdfPTable tablaDetalles2eX = new PdfPTable(1);


            PdfPTable tablaDetalle3eX = new PdfPTable(3);





            System.out.println("-------------------------------EGRESO ESPECIAL-------------------------------------");
            Double cuadre = new Double(0);
            for (int i = 0; i < cajaPdfDTO.getdetalles().size(); i++)
            {

                if(theList.get(i).get(1).equals("C") && (theList.get(i).get(2).contains("Monedas") || theList.get(i).get(2).contains("Billetes"))){
                    System.out.println(theList.get(i).toString());
                    tablaDetalles2eX = new PdfPTable(1);


                    tablaDetalle3eX = new PdfPTable(3);



                    PdfPCell cellF = new PdfPCell(new Phrase(theList.get(i).get(0),fontH1));
                    cellF.setBorder(PdfPCell.NO_BORDER);
                    cellF.setHorizontalAlignment(Element.ALIGN_CENTER);


                    PdfPCell cellT;

                    if(theList.get(i).get(2).length()<=60){
                        cellT = new PdfPCell(new Phrase(theList.get(i).get(2),fontH1));
                        cellT.setBorder(PdfPCell.NO_BORDER);
                        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
                    }else{
                        cellT = new PdfPCell(new Phrase(theList.get(i).get(2).substring(0,60)+"...",fontH1));
                        cellT.setBorder(PdfPCell.NO_BORDER);
                        cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
                    }

                    Double var2 = Double.parseDouble(theList.get(i).get(3));
            /*if(var!=0){
                var = var*-1;
            }*/
                    String result2 = String.format("%.0f", var2);
                    PdfPCell cellV = new PdfPCell(new Phrase(format.format(Long.parseLong(result2)),fontH1));
                    cellV.setBorder(PdfPCell.NO_BORDER);
                    cellV.setHorizontalAlignment(Element.ALIGN_CENTER);

                    cuadre += Double.parseDouble(theList.get(i).get(3));

                    if(var2!=0){


                        tablaDetalle3eX.addCell(cellF);
                        tablaDetalle3eX.addCell(cellT);
                        tablaDetalle3eX.addCell(cellV);



                        cellData1 =new PdfPCell(tablaDetalle3eX);
                        cellData1.setBorder(PdfPCell.NO_BORDER);
                        cellData1.setHorizontalAlignment(Element.ALIGN_CENTER);



                        tablaDetalles2eX.addCell(cellData1);



                        cellData3 =new PdfPCell(tablaDetalles2eX);
                        cellData3.setBorder(PdfPCell.BOTTOM);
                        cellData3.setHorizontalAlignment(Element.ALIGN_CENTER);


                        tablaDetalleseX.addCell(cellData3);

                    }

                }

            }

            PdfPTable tablaDEgresoster2 = new PdfPTable(2);
            PdfPCell ingresos1Xter2 = new PdfPCell(new Phrase("Total Recaudo: ", fontTitle));
            ingresos1Xter2.setBorder(PdfPCell.TOP);
            ingresos1Xter2.setHorizontalAlignment(Element.ALIGN_CENTER);
            PdfPCell ingresos2Xter2 = new PdfPCell(new Phrase(format.format(Long.parseLong(String.format("%.0f", cuadre))),fontTitle));
            ingresos2Xter2.setBorder(PdfPCell.TOP);
            ingresos2Xter2.setHorizontalAlignment(Element.ALIGN_CENTER);
            blank2.setBorder(PdfPCell.TOP);
            blank2.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDEgresoster2.addCell(ingresos1Xter2);
            tablaDEgresoster2.addCell(ingresos2Xter2);
            tablaDEgresoster2.addCell(blank2);
            tablaDEgresoster2.addCell(blank2);




            PdfPCell tituloBalance =new PdfPCell(new Phrase("BALANCE AL CIERRE",fontTitle));
            String Phrase = "";
            if(cajaPdfDTO.getbalance()<0){

                Phrase = "Faltó Dinero en Caja";
            }
            if(cajaPdfDTO.getbalance()>0){
                Phrase = "Sobró Dinero en Caja";
            }

            Double var = cajaPdfDTO.getbalance();
            /*if(var!=0){
                var = var*-1;
            }*/
            String result = String.format("%.0f", var);
            PdfPCell balance =new PdfPCell(new Phrase(format.format(Long.parseLong(result))));


            tituloBalance.setBorder(PdfPCell.NO_BORDER);
            tituloBalance.setHorizontalAlignment(Element.ALIGN_CENTER);
            balance.setBorder(PdfPCell.NO_BORDER);
            balance.setHorizontalAlignment(Element.ALIGN_CENTER);





           //PdfPTable tablaIngresosEgresos = new PdfPTable(2);
            PdfPCell cellIngresosEgresos1 = new PdfPCell(new Phrase("Ingresos",fontTitle));
            PdfPCell cellIngresosEgresos2 = new PdfPCell(new Phrase("Egresos",fontTitle));
            cellIngresosEgresos1.setBorder(PdfPCell.NO_BORDER);
            cellIngresosEgresos2.setBorder(PdfPCell.NO_BORDER);
            cellIngresosEgresos1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellIngresosEgresos2.setHorizontalAlignment(Element.ALIGN_CENTER);
            PdfPCell cellIngresosEgresos12 = new PdfPCell(new Phrase(format.format(Long.parseLong(String.format("%.0f", ingresos))),fontTitle));
            PdfPCell cellIngresosEgresos22 = new PdfPCell(new Phrase(format.format(Long.parseLong(String.format("%.0f", egresos))),fontTitle));
            cellIngresosEgresos12.setBorder(PdfPCell.NO_BORDER);
            cellIngresosEgresos22.setBorder(PdfPCell.NO_BORDER);
            cellIngresosEgresos12.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellIngresosEgresos22.setHorizontalAlignment(Element.ALIGN_CENTER);
/*
            tablaIngresosEgresos.addCell(cellIngresosEgresos1);
            tablaIngresosEgresos.addCell(cellIngresosEgresos12);
            tablaIngresosEgresos.addCell(cellIngresosEgresos2);
            tablaIngresosEgresos.addCell(cellIngresosEgresos22);

            PdfPCell tablatest =new PdfPCell(tablaIngresosEgresos);
            tablatest.setBorder(PdfPCell.NO_BORDER);
            tablatest.setHorizontalAlignment(Element.ALIGN_CENTER);

            tablaBalance.addCell(tablatest);
*/

            tablaBalance.addCell(tituloBalance);
            tablaBalance.addCell(balance);

            if(cajaPdfDTO.getbalance()!=0){
                PdfPCell frase =new PdfPCell(new Phrase(Phrase));
                frase.setBorder(PdfPCell.NO_BORDER);
                frase.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaBalance.addCell(frase);
            }

            PdfPTable tabla = new PdfPTable(1);
            PdfPCell space = new PdfPCell(new Phrase(" "));
            PdfPCell cell18 = new PdfPCell(tablaTitulo);
            PdfPCell cell19 = new PdfPCell(tablaFechas);
            PdfPCell cell20 = new PdfPCell(tablaDatos);
            PdfPCell cell21 = new PdfPCell(tablaBalance);
            PdfPCell cell22 = new PdfPCell(tablaDetalles);
            PdfPCell cell22T = new PdfPCell(tablaDIngresos);
            PdfPCell cell22e = new PdfPCell(tablaDetallese);
            PdfPCell cellRecaudo = new PdfPCell(tablaDEgresoster);
            PdfPCell cell23e = new PdfPCell(tablaDEgresos);
            PdfPCell cell23eX = new PdfPCell(tablaDetalleseX);
            PdfPCell cellRec = new PdfPCell(tablaDEgresoster2);
            PdfPCell cell23 = new PdfPCell(tablaTienda);
            cell23.setBorder(PdfPCell.NO_BORDER);
            cell18.setBorder(PdfPCell.NO_BORDER);
            cell19.setBorder(PdfPCell.NO_BORDER);
            cell20.setBorder(PdfPCell.NO_BORDER);
            cell21.setBorder(PdfPCell.NO_BORDER);
            cell22.setBorder(PdfPCell.NO_BORDER);
            cell22T.setBorder(PdfPCell.NO_BORDER);
            cell22e.setBorder(PdfPCell.NO_BORDER);
            cellRecaudo.setBorder(PdfPCell.NO_BORDER);
            cellRec.setBorder(PdfPCell.NO_BORDER);
            cell23e.setBorder(PdfPCell.NO_BORDER);
            cell23eX.setBorder(PdfPCell.NO_BORDER);
            space.setBorder(PdfPCell.NO_BORDER);
            tabla.addCell(cell23);
            tabla.addCell(cell18);
            tabla.addCell(cell20);
            tabla.addCell(cell19);
            tabla.addCell(cell22);
            tabla.addCell(cell22T);
            tabla.addCell(cell22e);
            tabla.addCell(cell23e);
            tabla.addCell(cellRecaudo);
            tabla.addCell(cell23eX);
            tabla.addCell(cellRec);
            tabla.addCell(cell21);
            documento.add(tabla);
            response = nombrePdf;

        }catch(Exception e){
            e.printStackTrace();
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> getConsecutiveCaja(Long id_cierre_caja) {
        try {
            return Either.right(billDAO.getConsecutiveCaja(id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }



    public Either<IException, String> getDateCloseCaja(Long id_cierre_caja) {
        try {
            //System.out.println(id_cierre_caja);
            return Either.right(billDAO.getDateCloseCaja(id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, List<CajaReport>> getReportCaja(Long id_store,
                                                                   Date date1,
                                                                   Date date2) {
        try {
            return Either.right(billDAO.getReportCaja(id_store,date1,date2));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, List<CajaSelect>> getBoxesToOpen(Long id_person) {
        try {
            return Either.right(billDAO.getBoxesToOpen(id_person));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, Long> isOpen(Long idcaja) {
        try {
            return Either.right(billDAO.isOpen(idcaja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, List<BoxesData>> getBoxes2(Long id_third) {
        try {
            return Either.right(billDAO.getBoxes2(id_third));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, List<BoxesData2>> getBoxes2P2(Long id_third) {
        try {
            return Either.right(billDAO.getBoxes2P2(id_third));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public  Long cierre_de_caja(Long idCaja,
                                String billetes,
                                String monedas,
                                String notas) {
        try {
            billDAO.cierre_de_caja(idCaja,billetes,monedas,notas);
            return new Long (1);
        }catch (Exception e){
            e.printStackTrace();
            return new Long (0);
        }
    }

}