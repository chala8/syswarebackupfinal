package com.name.business.mappers;

import com.name.business.entities.CajaReport;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CajaReportMapper implements ResultSetMapper<CajaReport> {

    @Override
    public CajaReport map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CajaReport(
                resultSet.getLong("ID_CAJA"),
                resultSet.getDouble("BALANCE"),
                resultSet.getString("CAJA_NUMBER"),
                resultSet.getString("FULLNAME"),
                resultSet.getString("STARTING_DATE"),
                resultSet.getString("CLOSING_DATE"),
                resultSet.getString("NOTES"),
                resultSet.getLong("CONSECUTIVE"),
                resultSet.getString("FECHA"),
                resultSet.getDouble("VALOR"),
                resultSet.getString("NATURALEZA"),
                resultSet.getString("NOTAS")

                );
    }
}
