package com.name.business.mappers;

import com.name.business.entities.BoxesData;
import com.name.business.entities.CajaReport;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BoxesDataMapper implements ResultSetMapper<BoxesData> {

    @Override
    public BoxesData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BoxesData(
                resultSet.getString("CAJA_NUMBER"),
                resultSet.getString("CAJA"),
                resultSet.getString("TIENDA"),
                resultSet.getLong("ID_STORE"),
                resultSet.getLong("ID_CIERRE_CAJA"),
                resultSet.getLong("ID_CAJA")

        );
    }
}
