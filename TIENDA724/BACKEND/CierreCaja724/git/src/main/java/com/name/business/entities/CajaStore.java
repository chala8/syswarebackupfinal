package com.name.business.entities;

import java.util.Date;

public class CajaStore {
    private String CAJA;
    private String STORE;

    public CajaStore( String CAJA, String STORE) {
        this.CAJA = CAJA;
        this.STORE = STORE;
    }

    public String getCAJA() {
        return CAJA;
    }

    public void setCAJA(String CAJA) {
        this.CAJA = CAJA;
    }

    public String getSTORE() {
        return STORE;
    }

    public void setSTORE(String STORE) {
        this.STORE = STORE;
    }






    }


