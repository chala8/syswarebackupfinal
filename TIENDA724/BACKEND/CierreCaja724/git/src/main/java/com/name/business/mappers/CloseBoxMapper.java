package com.name.business.mappers;

import com.name.business.entities.CloseBoxElem;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CloseBoxMapper implements ResultSetMapper<CloseBoxElem> {

    @Override
    public CloseBoxElem map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CloseBoxElem(
                resultSet.getLong("ID_CIERRE_CAJA"),
                resultSet.getLong("ID_CAJA"),
                resultSet.getString("CAJA"),
                resultSet.getString("FECHA_APERTURA"),
                resultSet.getString("CAJERO")
        );
    }
}
