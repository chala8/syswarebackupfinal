package com.name.business.mappers;

import com.name.business.entities.CierreCaja;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CierreCajaMapper implements ResultSetMapper<CierreCaja> {

    @Override
    public CierreCaja map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CierreCaja(
                resultSet.getString("STARTING_DATE"),
                resultSet.getString("CAJA"),
                resultSet.getString("CAJERO"),
                resultSet.getDouble("BALANCE"),
                resultSet.getString("NOTES"),
                resultSet.getString("STORE")
        );
    }
}
