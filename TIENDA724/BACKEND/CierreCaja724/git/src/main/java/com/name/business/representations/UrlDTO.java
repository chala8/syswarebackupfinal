package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;


public class UrlDTO {


    private Long ID_DOCUMENTO;
    private String URL;


    @JsonCreator
    public UrlDTO(@JsonProperty("iddoc")  Long ID_DOCUMENTO,
                  @JsonProperty("url") String URL) {
        this.ID_DOCUMENTO = ID_DOCUMENTO;
        this.URL = URL;
    }



    public Long getID_DOCUMENTO() {
        return ID_DOCUMENTO;
    }

    public void setID_DOCUMENTO(Long ID_DOCUMENTO) {
        this.ID_DOCUMENTO = ID_DOCUMENTO;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

}
