package com.name.business.mappers;

import com.name.business.entities.Detalle;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetalleMapper implements ResultSetMapper<Detalle> {

    @Override
    public Detalle map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Detalle(
                resultSet.getString("FECHA"),
                resultSet.getLong("VALOR"),
                resultSet.getString("NATURALEZA"),
                resultSet.getString("NOTES")
        );
    }
}
