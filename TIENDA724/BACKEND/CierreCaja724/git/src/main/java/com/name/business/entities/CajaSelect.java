package com.name.business.entities;

import java.util.Date;

public class CajaSelect {
    private String CAJA_NUMBER;
    private String STORE;
    private String CAJA;
    private Long ID_STORE;
    private Long ID_CAJA;

    public CajaSelect(String CAJA_NUMBER,
                      String STORE,
                      String CAJA,
                      Long ID_STORE,
                      Long ID_CAJA) {
        this.CAJA_NUMBER = CAJA_NUMBER;
        this.STORE = STORE;
        this.CAJA = CAJA;
        this.ID_STORE = ID_STORE;
        this.ID_CAJA = ID_CAJA;
    }

    public String getCAJA_NUMBER() {
        return CAJA_NUMBER;
    }

    public void setCAJA_NUMBER(String CAJA_NUMBER) {
        this.CAJA_NUMBER = CAJA_NUMBER;
    }



    public String getSTORE() {
        return STORE;
    }

    public void setSTORE(String STORE) {
        this.STORE = STORE;
    }



    public String getCAJA() {
        return CAJA;
    }

    public void setCAJA(String CAJA) {
        this.CAJA = CAJA;
    }



    public Long getID_STORE() {
        return ID_STORE;
    }

    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }



    public Long getID_CAJA() {
        return ID_CAJA;
    }

    public void setID_CAJA(Long ID_CAJA) {
        this.ID_CAJA = ID_CAJA;
    }


}
