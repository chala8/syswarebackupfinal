package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import java.util.List;


public class CajaPdfDTO {
    private String nombre_empresa;
    private String nit;
    private String numero_documento;
    private String nombre_cajero;
    private String nombre_caja;
    private String nombre_tienda;
    private String fecha_inicio;
    private String fecha_finalizacion;
    private Double balance;
    private List<List<String>> detalles;
    private String codigo;

    @JsonCreator
    public CajaPdfDTO(
                      @JsonProperty("codigo")  String codigo,
                      @JsonProperty("nombre_empresa")  String nombre_empresa,
                      @JsonProperty("nit")  String nit,
                      @JsonProperty("numero_documento")  String numero_documento,
                      @JsonProperty("nombre_cajero") String nombre_cajero,
                      @JsonProperty("nombre_caja") String nombre_caja,
                      @JsonProperty("nombre_tienda") String nombre_tienda,
                      @JsonProperty("fecha_inicio") String fecha_inicio,
                      @JsonProperty("fecha_finalizacion")  String fecha_finalizacion,
                      @JsonProperty("balance") Double balance,
                      @JsonProperty("detalles") List<List<String>> detalles) {
        this.codigo = codigo;
        this.nombre_empresa = nombre_empresa;
        this.nit = nit;
        this.numero_documento = numero_documento;
        this.nombre_cajero = nombre_cajero;
        this.nombre_caja = nombre_caja;
        this.nombre_tienda = nombre_tienda;
        this.fecha_inicio = fecha_inicio;
        this.fecha_finalizacion = fecha_finalizacion;
        this.balance = balance;
        this.detalles = detalles;
    }


    public String getcodigo() {
        return codigo;
    }

    public void setcodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getnumero_documento() {
        return numero_documento;
    }

    public void setnumero_documento(String numero_documento) {
        this.numero_documento = numero_documento;
    }

    public String getnombre_cajero() {
        return nombre_cajero;
    }

    public void setnombre_cajero(String nombre_cajero) {
        this.nombre_cajero = nombre_cajero;
    }

    public String getnombre_caja() {
        return nombre_caja;
    }

    public void setnombre_caja(String nombre_caja) {
        this.nombre_caja = nombre_caja;
    }

    public String getnombre_tienda() {
        return nombre_tienda;
    }

    public void setnombre_tienda(String nombre_tienda) {
        this.nombre_tienda = nombre_tienda;
    }

    public String getfecha_inicio() {
        return fecha_inicio;
    }

    public void setfecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public String getfecha_finalizacion() {
        return fecha_finalizacion;
    }

    public void setfecha_finalizacion(String fecha_finalizacion) {
        this.fecha_finalizacion = fecha_finalizacion;
    }

    public Double getbalance() {
        return balance;
    }

    public void setbalance(Double balance) {
        this.balance = balance;
    }

    public List<List<String>> getdetalles() {
        return detalles;
    }

    public void setdetalles(List<List<String>> detalles) {
        this.detalles = detalles;
    }

    public String getnombre_empresa() {
        return nombre_empresa;
    }

    public void setnombre_empresa(String nombre_empresa) {
        this.nombre_empresa = nombre_empresa;
    }

    public String getnit() {
        return nit;
    }

    public void setnit(String nit) {
        this.nit = nit;
    }

}
