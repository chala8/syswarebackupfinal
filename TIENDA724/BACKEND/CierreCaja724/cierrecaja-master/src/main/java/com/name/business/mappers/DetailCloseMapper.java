package com.name.business.mappers;

import com.name.business.entities.DetailClose;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailCloseMapper implements ResultSetMapper<DetailClose> {

	@Override
	public DetailClose map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
		return new DetailClose(
				resultSet.getLong("ID_CAJA_MOVIMIENTOS"),
				resultSet.getString("MOVEMENT_DATE"),
				resultSet.getDouble("VALOR"),
				resultSet.getString("NATURALEZA"),
				resultSet.getString("NOTES"),
				resultSet.getLong("ID_CIERRE_CAJA")
		);
	}
}
