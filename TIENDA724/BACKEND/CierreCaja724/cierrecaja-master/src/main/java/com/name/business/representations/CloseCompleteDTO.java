package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CloseCompleteDTO {
	private CloseDTO closeDTO;
	private List<DetailCloseDTO> detailCloseDTOS;

	@JsonCreator
	public CloseCompleteDTO(@JsonProperty("header") CloseDTO closeDTO,@JsonProperty("details") List<DetailCloseDTO> detailCloseDTOS) {
		this.closeDTO = closeDTO;
		this.detailCloseDTOS = detailCloseDTOS;
	}

	public CloseDTO getCloseDTO() {
		return closeDTO;
	}

	public void setCloseDTO(CloseDTO closeDTO) {
		this.closeDTO = closeDTO;
	}

	public List<DetailCloseDTO> getDetailCloseDTOS() {
		return detailCloseDTOS;
	}

	public void setDetailCloseDTOS(List<DetailCloseDTO> detailCloseDTOS) {
		this.detailCloseDTOS = detailCloseDTOS;
	}
}
