package com.name.business.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class Store {
    private Long ID_STORE;
    private String STORE;
    private Long CAJA_NUMBER;
    private Long ID_THIRD;

    @JsonCreator
    public Store(@JsonProperty("ID_STORE") Long ID_STORE,@JsonProperty("STORE") String STORE,@JsonProperty("CAJA_NUMBER") Long CAJA_NUMBER,@JsonProperty("ID_THIRD") Long ID_THIRD) {
        this.ID_STORE = ID_STORE;
        this.STORE = STORE;
        this.CAJA_NUMBER = CAJA_NUMBER;
        this.ID_THIRD = ID_THIRD;
    }

    public Long getID_STORE() {
        return ID_STORE;
    }

    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    public String getSTORE() {
        return STORE;
    }

    public void setSTORE(String STORE) {
        this.STORE = STORE;
    }

    public Long getCAJA_NUMBER() {
        return CAJA_NUMBER;
    }

    public void setCAJA_NUMBER(Long CAJA_NUMBER) {
        this.CAJA_NUMBER = CAJA_NUMBER;
    }

    public Long getID_THIRD() {
        return ID_THIRD;
    }

    public void setID_THIRD(Long ID_THIRD) {
        this.ID_THIRD = ID_THIRD;
    }




}
