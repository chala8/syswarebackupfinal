package com.name.business.mappers;

import com.name.business.entities.CajaSelect;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CajaSelectMapper implements ResultSetMapper<CajaSelect> {

    @Override
    public CajaSelect map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CajaSelect(
                resultSet.getString("CAJA_NUMBER"),
                resultSet.getString("STORE"),
                resultSet.getString("CAJA"),
                resultSet.getLong("ID_STORE"),
                resultSet.getLong("ID_CAJA")
        );
    }
}
