package com.name.business.resources;


import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.DetailCloseBussiness;
import com.name.business.entities.DetailClose;
import com.name.business.representations.DetailCloseDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/detail-close")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DetailCloseResource {

	private DetailCloseBussiness detailCloseBussiness;


	public DetailCloseResource(DetailCloseBussiness detailCloseBussiness) {
		this.detailCloseBussiness = detailCloseBussiness;
	}

	@GET
	@Timed
	public Response getDetailCloseResource(@QueryParam("id_cierre_caja") Long ID_CIERRE_CAJA){

		Response response;

		Either<IException, List<DetailClose>> getResponseGeneric = detailCloseBussiness.getDetailClose(ID_CIERRE_CAJA);
		if (getResponseGeneric.isRight()){
			System.out.println(getResponseGeneric.right().value().size());
			response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
		}else {
			response= ExceptionResponse.createErrorResponse(getResponseGeneric);
		}
		return response;
	}

	/**
	 *
	 * @param detailCloseDTOList
	 * @return
	 */
	@POST
	@Timed
	public Response postResource(List<DetailCloseDTO> detailCloseDTOList, @QueryParam("id_cierre_caja") Long ID_CIERRE_CAJA){
		Response response;
		Either<IException, Long> allViewOffertsEither =detailCloseBussiness.createDetailClose(detailCloseDTOList, ID_CIERRE_CAJA);

		if (allViewOffertsEither.isRight()){
			response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
		}else {
			response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
		}

		return response;
	}



}
