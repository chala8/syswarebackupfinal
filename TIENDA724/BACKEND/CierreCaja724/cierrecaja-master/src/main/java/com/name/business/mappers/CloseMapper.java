package com.name.business.mappers;

import com.name.business.entities.Close;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CloseMapper implements ResultSetMapper<Close> {

    @Override
    public Close map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Close(
                resultSet.getLong("ID_CIERRE_CAJA"),
                resultSet.getLong("ID_THIRD_EMPLOYEE"),
                resultSet.getDate("STARTING_DATE"),
                resultSet.getDate("CLOSING_DATE"),
                resultSet.getLong("ID_COMMON_STATE"),
                resultSet.getLong("ID_CAJA"),
                resultSet.getString("STATUS"),
                resultSet.getString("NOTES"),
                resultSet.getDouble("BALANCE")

        );
    }
}
