package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class DetailCloseDTO {

	private Date MOVEMENT_DATE;
	private Double VALOR;
	private String NATURALEZA;
	private String NOTES;


	@JsonCreator
	public DetailCloseDTO(@JsonProperty("MOVEMENT_DATE") Date MOVEMENT_DATE,
						   @JsonProperty("VALOR") Double VALOR,
						   @JsonProperty("NATURALEZA")  String NATURALEZA,
						   @JsonProperty("NOTES") String NOTES) {
		this.MOVEMENT_DATE = MOVEMENT_DATE;
		this.VALOR = VALOR;
		this.NATURALEZA = NATURALEZA;
		this.NOTES = NOTES;
	}

	public Date getMOVEMENT_DATE() {
		return MOVEMENT_DATE;
	}

	public void setMOVEMENT_DATE(Date MOVEMENT_DATE) {
		this.MOVEMENT_DATE = MOVEMENT_DATE;
	}

	public Double getVALOR() {
		return VALOR;
	}

	public void setVALOR(Double VALOR) {
		this.VALOR = VALOR;
	}

	public String getNATURALEZA() {
		return NATURALEZA;
	}

	public void setNATURALEZA(String NATURALEZA) {
		this.NATURALEZA = NATURALEZA;
	}

	public String getNOTES() {
		return NOTES;
	}

	public void setNOTES(String NOTES) {
		this.NOTES = NOTES;
	}
}
