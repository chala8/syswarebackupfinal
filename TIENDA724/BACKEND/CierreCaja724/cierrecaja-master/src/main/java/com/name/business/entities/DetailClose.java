package com.name.business.entities;

import java.util.Date;

public class DetailClose {

	private Long ID_CAJA_MOVIMIENTOS;
	private String MOVEMENT_DATE;
	private Double VALOR;
	private String NATURALEZA;
	private String NOTES;
	private Long ID_CIERRE_CAJA;

	public DetailClose(Long ID_CAJA_MOVIMIENTOS, String MOVEMENT_DATE, Double VALOR, String NATURALEZA, String NOTES, Long ID_CIERRE_CAJA) {
		this.ID_CAJA_MOVIMIENTOS = ID_CAJA_MOVIMIENTOS;
		this.MOVEMENT_DATE = MOVEMENT_DATE;
		this.VALOR = VALOR;
		this.NATURALEZA = NATURALEZA;
		this.NOTES = NOTES;
		this.ID_CIERRE_CAJA = ID_CIERRE_CAJA;
	}

	public Long getID_CAJA_MOVIMIENTOS() {
		return ID_CAJA_MOVIMIENTOS;
	}

	public void setID_CAJA_MOVIMIENTOS(Long ID_CAJA_MOVIMIENTOS) {
		this.ID_CAJA_MOVIMIENTOS = ID_CAJA_MOVIMIENTOS;
	}

	public String getMOVEMENT_DATE() {
		return MOVEMENT_DATE;
	}

	public void setMOVEMENT_DATE(String MOVEMENT_DATE) {
		this.MOVEMENT_DATE = MOVEMENT_DATE;
	}

	public Double getVALOR() {
		return VALOR;
	}

	public void setVALOR(Double VALOR) {
		this.VALOR = VALOR;
	}

	public String getNATURALEZA() {
		return NATURALEZA;
	}

	public void setNATURALEZA(String NATURALEZA) {
		this.NATURALEZA = NATURALEZA;
	}

	public String getNOTES() {
		return NOTES;
	}

	public void setNOTES(String NOTES) {
		this.NOTES = NOTES;
	}

	public Long getID_CIERRE_CAJA() {
		return ID_CIERRE_CAJA;
	}

	public void setID_CIERRE_CAJA(Long ID_CIERRE_CAJA) {
		this.ID_CIERRE_CAJA = ID_CIERRE_CAJA;
	}
}
