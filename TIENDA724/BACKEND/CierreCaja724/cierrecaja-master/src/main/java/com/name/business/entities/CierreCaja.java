package com.name.business.entities;

import java.util.Date;

public class CierreCaja {
    private String STARTING_DATE;
    private String CAJA;
    private String CAJERO;
    private Long BALANCE;
    private String NOTES;
    private String STORE;

    public CierreCaja(String STARTING_DATE,
                      String CAJA,
                      String CAJERO,
                      Long BALANCE,
                      String NOTES,
                      String STORE) {
        this.STARTING_DATE = STARTING_DATE;
        this.CAJA = CAJA;
        this.CAJERO = CAJERO;
        this.BALANCE = BALANCE;
        this.NOTES = NOTES;
        this.STORE = STORE;
    }

    public String getSTARTING_DATE() {
        return STARTING_DATE;
    }

    public void setSTARTING_DATE(String STARTING_DATE) {
        this.STARTING_DATE = STARTING_DATE;
    }

    public String getCAJA() {
        return CAJA;
    }

    public void setCAJA(String CAJA) {
        this.CAJA = CAJA;
    }

    public String getCAJERO() {
        return CAJERO;
    }

    public void setCAJERO(String CAJERO) {
        this.CAJERO = CAJERO;
    }

    public Long getBALANCE() {
        return BALANCE;
    }

    public void setBALANCE(Long BALANCE) {
        this.BALANCE = BALANCE;
    }

    public String getNOTES() {
        return NOTES;
    }

    public void setNOTES(String NOTES) {
        this.NOTES = NOTES;
    }

    public String getSTORE() {
        return STORE;
    }

    public void setSTORE(String STORE) {
        this.STORE = STORE;
    }


}
