package com.name.business;


import com.name.business.DAOs.CloseDAO;
import com.name.business.DAOs.DetailCloseDAO;
import com.name.business.DAOs.StoreDAO;
import com.name.business.businesses.CloseBusiness;
import com.name.business.businesses.DetailCloseBussiness;
import com.name.business.resources.CloseResource;
import com.name.business.resources.DetailCloseResource;
import io.dropwizard.Application;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.java8.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.commons.dbcp2.BasicDataSource;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

import static com.name.business.utils.constans.K.URI_BASE;


public class Close724 extends Application<CloseAppConfiguration> {



    public static void main(String[] args) throws Exception  {
        new Close724().run(args);
    }





    @Override
    public void initialize(Bootstrap<CloseAppConfiguration> bootstrap) {

        bootstrap.addBundle(new Java8Bundle());
        bootstrap.addBundle(new MultiPartBundle());


    }
    @Override
    public void run(CloseAppConfiguration configuration, Environment environment) throws Exception {
        environment.jersey().setUrlPattern(URI_BASE+"/*");

        // Enable CORS headers
        final FilterRegistration.Dynamic corsFilter =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        corsFilter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        corsFilter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        corsFilter.setInitParameter("allowCredentials", "true");

        // Add URL mapping
        corsFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");


        // Se estable el ambiente de coneccion con JDBI con la base de datos de oracle
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "oracle");




        final DetailCloseDAO detailCloseDAO = jdbi.onDemand(DetailCloseDAO.class);
        final DetailCloseBussiness detailCloseBussiness = new DetailCloseBussiness(detailCloseDAO);

        final CloseDAO closeDAO= jdbi.onDemand(CloseDAO.class);
        final StoreDAO storeDAO= jdbi.onDemand(StoreDAO.class);
        final CloseBusiness closeBusiness= new CloseBusiness(closeDAO,detailCloseBussiness,storeDAO);

        CloseDatabaseConfiguration routesDatabaseConfiguration = configuration.getDatabaseConfiguration();
        BasicDataSource datasource = createDataSource(routesDatabaseConfiguration);
        environment.jersey().register(new CloseResource(closeBusiness));
        environment.jersey().register(new DetailCloseResource(detailCloseBussiness));

    }
    /**
     * @Método:  Hace  la configuración para establecer conexión con las bases de datos que se
     * va a utilizar como persistencia del servidor
     * */
    private BasicDataSource createDataSource(CloseDatabaseConfiguration closeDatabaseConfiguration){

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(closeDatabaseConfiguration.getDriverClassName());
        basicDataSource.setUrl(closeDatabaseConfiguration.getUrl());
        basicDataSource.setUsername(closeDatabaseConfiguration.getUsername());
        basicDataSource.setPassword(closeDatabaseConfiguration.getPassword());
        basicDataSource.addConnectionProperty("characterEncoding", "UTF-8");

        basicDataSource.setRemoveAbandonedOnMaintenance(true);
        basicDataSource.setRemoveAbandonedOnBorrow(true);
        basicDataSource.setRemoveAbandonedTimeout(30000);

        return basicDataSource;

    }




}
