package com.name.business.mappers;

import com.name.business.entities.CajaStore;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CajaStoreMapper implements ResultSetMapper<CajaStore> {

    @Override
    public CajaStore map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CajaStore(
                resultSet.getString("CAJA"),
                resultSet.getString("TIENDA")

        );
    }
}
