package com.name.business.businesses;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.DAOs.*;
import com.name.business.entities.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.name.business.representations.CajaPdfDTO;
import com.name.business.representations.CloseCompleteDTO;
import com.name.business.representations.CloseDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;

public class CloseBusiness {

   private CloseDAO billDAO;
   private StoreDAO storeDAO;
  private DetailCloseBussiness detailCloseBussiness;

    public Either<IException, String> updateCierreCaja(int id_cierre_caja, CloseDTO closeDTO) {
        System.out.println(id_cierre_caja);
        try {
            billDAO.updateCierreCaja(id_cierre_caja, closeDTO);
            return Either.right("OK!");
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, List<Store>> dataBox(int id_caja) {
        try {
            return Either.right(this.storeDAO.dataBox(id_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, List <String>> Boxes(Long id_third_employee, String status) {
        try {
           return Either.right(billDAO.Boxes(id_third_employee, status));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }

    }

    public void postURL(Long iddoc, String url) {

        billDAO.postURL(iddoc,url);

    }


    public Either<IException, List <String>> Boxes2(Long id_third_employee, String status) {
        try {
            return Either.right(billDAO.Boxes2(id_third_employee, status));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, String> getdocurl(Long iddoc, Long idtype) {
        try {
            return Either.right(billDAO.getdocurl(iddoc, idtype));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, Long> getidcc(Long idcaja, Long consecutive) {
        try {
            return Either.right(billDAO.getidcc(idcaja, consecutive));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public CloseBusiness(CloseDAO billDAO, DetailCloseBussiness detailCloseBussiness, StoreDAO storeDAO) {
    this.billDAO = billDAO;
    this.detailCloseBussiness = detailCloseBussiness;
    this.storeDAO = storeDAO;
  }


  public Either<IException, List<Close>> getBill(Long ID_CIERRE_CAJA, Long id_employee) {
        List<String> msn = new ArrayList<>();
        try {
            System.out.println("|||||||||||| Starting consults Close ||||||||||||||||| ");
            return Either.right(billDAO.readCommons(ID_CIERRE_CAJA,id_employee));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

  public Either<IException, Long> createClose(CloseDTO detailCloseDTO) {
    try {
      System.out.println("|||||||||||| Starting consults Close ||||||||||||||||| ");

      billDAO.create(new Long(301),detailCloseDTO);
      return Either.right(Long.valueOf(billDAO.getPkLast()));
    } catch (Exception e) {
      e.printStackTrace();
      return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
    }
  }

  public Either<IException, Long> createCloseBulk(CloseCompleteDTO closeCompleteDTO) {
    try {
      if(closeCompleteDTO.getCloseDTO()!=null){
        System.out.println("|||||||||||| Starting consults Close ||||||||||||||||| ");
        CloseDTO detailCloseDTO = closeCompleteDTO.getCloseDTO();
        billDAO.create(new Long(301),detailCloseDTO);

        if(closeCompleteDTO.getDetailCloseDTOS()!=null && closeCompleteDTO.getDetailCloseDTOS().size()>0){
          return  detailCloseBussiness.createDetailClose(closeCompleteDTO.getDetailCloseDTOS(),billDAO.getPkLast());

        }else{
          return Either.right(Long.valueOf(billDAO.getPkLast()));
        }

      }else{
        return Either.left(new BussinessException(messages_error("Requiere el formato JSON para cierre de caja")));
      }



    } catch (Exception e) {
      e.printStackTrace();
      return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
    }
  }

    public Either<IException, CierreCaja> getMaestroCaja(Long id_cierre_caja) {
        try {
            return Either.right(billDAO.getMaestroCaja(id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, List<Detalle>> getDetalleCaja(Long id_cierre_caja) {
        try {
            return Either.right(billDAO.getDetalleCaja(id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, CajaStore> getBoxDetails(Long id_caja) {
        try {
            return Either.right(billDAO.getBoxDetails(id_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, Long> getSalesCaja(Long id_cierre_caja) {
        try {
            return Either.right(billDAO.getSalesCaja(id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, Long> getPayments(Long id_cierre_caja, Long id_payment) {
        try {
            return Either.right(billDAO.getPayments(id_cierre_caja, id_payment));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, String> printPDF(CajaPdfDTO cajaPdfDTO){
        // Se crea el documento
        Rectangle envelope = new Rectangle(137, 598);
        Document documento = new Document(envelope, 0f, 0f, 10f, 0f);

        String response = "Bad!!!";
        try { // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
            String consecutivo = cajaPdfDTO.getnumero_documento();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 5, Font.NORMAL);

            BaseFont HELVETICA2 = BaseFont.createFont(
                    BaseFont.HELVETICA,
                    BaseFont.WINANSI, false);
            Font fontText = new Font(HELVETICA2, 6, Font.BOLD);

            BaseFont TABLETITLE = BaseFont.createFont(
                    BaseFont.HELVETICA,
                    BaseFont.WINANSI, false);
            Font fontTitle = new Font(TABLETITLE, 7, Font.BOLD);

            BaseFont bigBold = BaseFont.createFont(
                    BaseFont.HELVETICA,
                    BaseFont.WINANSI, false);
            Font bigBoldFont = new Font(bigBold, 8, Font.BOLD);

            String nombrePdf = consecutivo + cajaPdfDTO.getcodigo() + ".pdf";
            //String nombrePdf = consecutivo + ".pdf";
            FileOutputStream ficheroPdf = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/docscaja/" + nombrePdf);
            //FileOutputStream ficheroPdf  = new FileOutputStream("./"+nombrePdf);
            ///u01/tomcat/apache-tomcat-9.0.0.M21/webapps/docscaja
            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento, ficheroPdf).setInitialLeading(20);
            documento.open();

            documento.newPage();
            documento.add(new Chunk(""));


            PdfPCell saltoLinea = new PdfPCell(new Phrase("    "));
            saltoLinea.setBorder(PdfPCell.NO_BORDER);
            saltoLinea.setHorizontalAlignment(Element.ALIGN_CENTER);


            // TABLA DE TIENDA
            PdfPTable tablaTienda = new PdfPTable(1);
            PdfPCell tienda = new PdfPCell(new Phrase(cajaPdfDTO.getnombre_empresa(), fontText));
            PdfPCell nit = new PdfPCell(new Phrase(cajaPdfDTO.getnit(),fontText));
            tienda.setBorder(PdfPCell.NO_BORDER);
            tienda.setHorizontalAlignment(Element.ALIGN_CENTER);
            nit.setBorder(PdfPCell.NO_BORDER);
            nit.setHorizontalAlignment(Element.ALIGN_CENTER);
            //tablaTienda.addCell(tienda);
            //tablaTienda.addCell(nit);

            // TABLA DE TITULO
            PdfPTable tablaTitulo = new PdfPTable(1);
            PdfPCell cell1 =new PdfPCell(new Phrase("REPORTE DE CONTROL DE CAJA",bigBoldFont));
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            try {
                Image img = Image.getInstance("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/logos/"+cajaPdfDTO.getnit()+".jpg");
                //Image img = Image.getInstance("./"+billpdfDTO.getnit()+".jpg");
                PdfPCell imagen = new PdfPCell(img, true);
                imagen.setBorder(PdfPCell.NO_BORDER);
                imagen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaTitulo.addCell(imagen);
            }catch(Exception e){
                System.out.println(e.toString());
            }
            tablaTitulo.addCell(cell1);
            tablaTitulo.addCell(tienda);
            tablaTitulo.addCell(nit);


            //TABLA DE FECHAS
            PdfPTable tablaFechas = new PdfPTable(1);
            //String cellFechainicialS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cajaPdfDTO.getfecha_inicio());
            PdfPCell cellFechainicial =new PdfPCell(new Phrase("Fecha apertura: "+cajaPdfDTO.getfecha_inicio(),fontText));
            //String cellFechafinalizacionS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cajaPdfDTO.getfecha_finalizacion());
            PdfPCell cellFechaFinal =new PdfPCell(new Phrase("Fecha cierre: "+cajaPdfDTO.getfecha_finalizacion(),fontText));
            cellFechainicial.setBorder(PdfPCell.NO_BORDER);
            cellFechainicial.setHorizontalAlignment(Element.ALIGN_LEFT);
            cellFechaFinal.setBorder(PdfPCell.NO_BORDER);
            cellFechaFinal.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaFechas.addCell(cellFechainicial);
            tablaFechas.addCell(cellFechaFinal);


            //TABLA DE DATOS
            PdfPTable tablaDatos = new PdfPTable(2);
            PdfPCell cellDocumento =new PdfPCell(new Phrase(cajaPdfDTO.getnumero_documento(),fontText));
            PdfPCell cellCajero =new PdfPCell(new Phrase("Cajero: "+cajaPdfDTO.getnombre_cajero().replaceAll("null",""),fontText));
            PdfPCell cellCaja =new PdfPCell(new Phrase("Caja numero: "+cajaPdfDTO.getnombre_caja(),fontText));
            PdfPCell cellTienda =new PdfPCell(new Phrase("Tienda: "+cajaPdfDTO.getnombre_tienda(),fontText));

            cellDocumento.setBorder(PdfPCell.NO_BORDER);
            cellDocumento.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaDatos.addCell(cellDocumento);

            cellCajero.setBorder(PdfPCell.NO_BORDER);
            cellCajero.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDatos.addCell(cellCajero);

            cellCaja.setBorder(PdfPCell.NO_BORDER);
            cellCaja.setHorizontalAlignment(Element.ALIGN_LEFT);
            tablaDatos.addCell(cellCaja);

            cellTienda.setBorder(PdfPCell.NO_BORDER);
            cellTienda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDatos.addCell(cellTienda);


            //TABLA BALANCE
            PdfPTable tablaBalance = new PdfPTable(1);
            PdfPCell tituloBalance =new PdfPCell(new Phrase("BALANCE AL CIERRE",fontTitle));
            String Phrase = "";
            if(cajaPdfDTO.getbalance()<0){

                Phrase = "Faltó Dinero en Caja";
            }
            if(cajaPdfDTO.getbalance()>0){
                Phrase = "Sobró Dinero en Caja";
            }

            Double var = cajaPdfDTO.getbalance();
            /*if(var!=0){
                var = var*-1;
            }*/
            PdfPCell balance =new PdfPCell(new Phrase(var+""));


            tituloBalance.setBorder(PdfPCell.NO_BORDER);
            tituloBalance.setHorizontalAlignment(Element.ALIGN_CENTER);
            balance.setBorder(PdfPCell.NO_BORDER);
            balance.setHorizontalAlignment(Element.ALIGN_CENTER);

            tablaBalance.addCell(tituloBalance);
            tablaBalance.addCell(balance);
            if(cajaPdfDTO.getbalance()!=0){
                PdfPCell frase =new PdfPCell(new Phrase(Phrase));
                frase.setBorder(PdfPCell.NO_BORDER);
                frase.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaBalance.addCell(frase);
            }

            //TABLA DETALLES

            PdfPTable tablaDetalles = new PdfPTable(4);

            PdfPCell cellFecha =new PdfPCell(new Phrase("Fecha",fontTitle));
            cellFecha.setBorder(PdfPCell.NO_BORDER);
            cellFecha.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cellTipo =new PdfPCell(new Phrase("Tipo",fontTitle));
            cellTipo.setBorder(PdfPCell.NO_BORDER);
            cellTipo.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cellNotas =new PdfPCell(new Phrase("Notas",fontTitle));
            cellNotas.setBorder(PdfPCell.NO_BORDER);
            cellNotas.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell cellValor =new PdfPCell(new Phrase("Valor",fontTitle));
            cellValor.setBorder(PdfPCell.NO_BORDER);
            cellValor.setHorizontalAlignment(Element.ALIGN_CENTER);

            tablaDetalles.addCell(cellFecha);
            tablaDetalles.addCell(cellTipo);
            tablaDetalles.addCell(cellNotas);
            tablaDetalles.addCell(cellValor);




            List<List<String>> theList = cajaPdfDTO.getdetalles();

            for (int i = 0; i < cajaPdfDTO.getdetalles().size(); i++)
            {
                PdfPCell cellF = new PdfPCell(new Phrase(theList.get(i).get(0),fontH1));
                cellF.setBorder(PdfPCell.TOP);
                cellF.setHorizontalAlignment(Element.ALIGN_CENTER);
                PdfPCell cellT = new PdfPCell();

                if(theList.get(i).get(1).equals("C")){
                    cellT = new PdfPCell(new Phrase("Egreso a la Caja",fontH1));
                    cellT.setBorder(PdfPCell.TOP);
                    cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
                }
                if(theList.get(i).get(1).equals("D")){
                    cellT = new PdfPCell(new Phrase("Ingreso a la Caja",fontH1));
                    cellT.setBorder(PdfPCell.TOP);
                    cellT.setHorizontalAlignment(Element.ALIGN_CENTER);
                }

                PdfPCell cellN = new PdfPCell(new Phrase(theList.get(i).get(2),fontH1));
                cellN.setBorder(PdfPCell.TOP);
                cellN.setHorizontalAlignment(Element.ALIGN_CENTER);

                PdfPCell cellV = new PdfPCell(new Phrase(theList.get(i).get(3),fontH1));
                cellV.setBorder(PdfPCell.TOP);
                cellV.setHorizontalAlignment(Element.ALIGN_CENTER);


                tablaDetalles.addCell(cellF);
                tablaDetalles.addCell(cellT);
                tablaDetalles.addCell(cellN);
                tablaDetalles.addCell(cellV);
            }



            PdfPTable tabla = new PdfPTable(1);
            PdfPCell space = new PdfPCell(new Phrase(" "));
            PdfPCell cell18 = new PdfPCell(tablaTitulo);
            PdfPCell cell19 = new PdfPCell(tablaFechas);
            PdfPCell cell20 = new PdfPCell(tablaDatos);
            PdfPCell cell21 = new PdfPCell(tablaBalance);
            PdfPCell cell22 = new PdfPCell(tablaDetalles);
            PdfPCell cell23 = new PdfPCell(tablaTienda);
            cell23.setBorder(PdfPCell.NO_BORDER);
            cell18.setBorder(PdfPCell.NO_BORDER);
            cell19.setBorder(PdfPCell.NO_BORDER);
            cell20.setBorder(PdfPCell.NO_BORDER);
            cell21.setBorder(PdfPCell.NO_BORDER);
            cell22.setBorder(PdfPCell.NO_BORDER);
            space.setBorder(PdfPCell.NO_BORDER);
            tabla.addCell(cell23);
            tabla.addCell(cell18);
            tabla.addCell(cell20);
            tabla.addCell(cell19);
            tabla.addCell(cell22);
            tabla.addCell(cell21);
            documento.add(tabla);
            response = nombrePdf;

        }catch(Exception e){
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> getConsecutiveCaja(Long id_cierre_caja) {
        try {
            return Either.right(billDAO.getConsecutiveCaja(id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }



    public Either<IException, String> getDateCloseCaja(Long id_cierre_caja) {
        try {
            System.out.println(id_cierre_caja);
            return Either.right(billDAO.getDateCloseCaja(id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, List<CajaReport>> getReportCaja(Long id_store,
                                                                   Date date1,
                                                                   Date date2) {
        try {
            return Either.right(billDAO.getReportCaja(id_store,date1,date2));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, List<CajaSelect>> getBoxesToOpen(Long id_person) {
        try {
            return Either.right(billDAO.getBoxesToOpen(id_person));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, Long> isOpen(Long idcaja) {
        try {
            return Either.right(billDAO.isOpen(idcaja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }
}