package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.CloseBusiness;
import com.name.business.entities.*;
import com.name.business.representations.CloseCompleteDTO;
import com.name.business.representations.CloseDTO;
import com.name.business.representations.CajaPdfDTO;
import com.name.business.representations.UrlDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Date;


@Path("/close")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CloseResource {
    private CloseBusiness billBusiness;

    public CloseResource(CloseBusiness billBusiness) {
        this.billBusiness = billBusiness;
    }

    @GET
    @Timed
    public Response getBillResource(@QueryParam("id_third_employee") Long id_third_employee,
                    @QueryParam("id_cierre_caja") Long ID_CIERRE_CAJA){

        Response response;

        Either<IException, List<Close>> getResponseGeneric = billBusiness.getBill(ID_CIERRE_CAJA,id_third_employee);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/boxes")
    @Timed
    public Response getOpenBoxes(@QueryParam("id_third_employee") Long id_third_employee,
                                 @QueryParam("status") String status){

        Response response;

        Either<IException, List<String>> getResponseGeneric = billBusiness.Boxes(id_third_employee,status);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/boxes2")
    @Timed
    public Response getOpenBoxes2(@QueryParam("id_third_employee") Long id_third_employee,
                                 @QueryParam("status") String status){

        Response response;

        Either<IException, List<String>> getResponseGeneric = billBusiness.Boxes2(id_third_employee,status);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




    @GET
    @Path("/dataBox")
    @Timed
    public Response dataBox (@QueryParam("id_caja") int id_caja){

        Response response;

        Either<IException, List<Store>> getResponseGeneric = billBusiness.dataBox(id_caja);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @PUT
    @Path("/state/{id_cierre_caja}")
    @Timed
    public Response updateCierreCaja(CloseDTO close, @PathParam("id_cierre_caja") int id_cierre_caja){

        Response response;
        System.out.println("Starting Consult");
        Either<IException, String> allViewOffertsEither =billBusiness.updateCierreCaja(id_cierre_caja,close);
        System.out.println("----------------------------");
        System.out.println(allViewOffertsEither);
        System.out.println("----------------------------");
        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }

        return response;
    }

    /**
     *
     * @param closeDTO
     * @return
     */
    @POST
    @Timed
    public Response postResource(CloseDTO closeDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither =billBusiness.createClose(closeDTO);

        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }

        return response;
    }


    @POST
    @Path("/url")
    @Timed
    public Response postURL(UrlDTO urlDTO){
        Response response;
        try{
            billBusiness.postURL(urlDTO.getID_DOCUMENTO(), urlDTO.getURL());
            response = Response.status(Response.Status.OK).entity(1).build();
        }catch(Exception e){
            System.out.println(e.toString());
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(0).build();
        }
        return response;
    }

    @GET
    @Path("/docurl")
    @Timed
    public Response getdocurl (@QueryParam("iddoc") Long iddoc, @QueryParam("idtype") Long idtype){

        Response response;

        Either<IException, String> getResponseGeneric = billBusiness.getdocurl(iddoc,idtype);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/id_cierre_caja")
    @Timed
    public Response getidcc (@QueryParam("idcaja") Long idcaja, @QueryParam("consecutive") Long consecutive){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.getidcc(idcaja,consecutive);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    /**
     *
     * @param closeCompleteDTO
     * @return
     */
    @POST
    @Path("bulk")
    @Timed
    public Response postResource(CloseCompleteDTO closeCompleteDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = billBusiness.createCloseBulk(closeCompleteDTO);

        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @GET
    @Path("/boxmaster")
    @Timed
    public Response getMaestroCaja(@QueryParam("id_cierre_caja") Long id_cierre_caja){

        Response response;

        Either<IException, CierreCaja> getResponseGeneric = billBusiness.getMaestroCaja(id_cierre_caja);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/boxdetail")
    @Timed
    public Response getDetalleCaja(@QueryParam("id_cierre_caja") Long id_cierre_caja){

        Response response;

        Either<IException, List<Detalle>> getResponseGeneric = billBusiness.getDetalleCaja(id_cierre_caja);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/details")
    @Timed
    public Response getBoxDetails(@QueryParam("id_caja") Long id_cierre_caja){

        Response response;

        Either<IException, CajaStore> getResponseGeneric = billBusiness.getBoxDetails(id_cierre_caja);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/ventasCaja")
    @Timed
    public Response getSalesCaja(@QueryParam("id_cierre_caja") Long id_cierre_caja){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.getSalesCaja(id_cierre_caja);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/ventasPayment")
    @Timed
    public Response getPayments(@QueryParam("id_cierre_caja") Long id_cierre_caja,@QueryParam("id_payment") Long id_payment){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.getPayments(id_cierre_caja,id_payment);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @POST
    @Path("/pdf")
    @Timed
    public Response printPdf(CajaPdfDTO cajaPdfDTO) {
        Response response;

        Either<IException, String> mailEither = billBusiness.printPDF(cajaPdfDTO);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @GET
    @Path("/consecutive")
    @Timed
    public Response getConsecutiveCaja(@QueryParam("id_cierre_caja") Long id_cierre_caja){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.getConsecutiveCaja(id_cierre_caja);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/consecutive2")
    @Timed
    public Response getDateCloseCaja(@QueryParam("id") Long id_cierre_caja){

        Response response;

        Either<IException, String> getResponseGeneric = billBusiness.getDateCloseCaja(id_cierre_caja);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/report")
    @Timed
    public Response getReportCaja(@QueryParam("id_store") Long id_store,
                                  @QueryParam("date1") String date1,
                                  @QueryParam("date2") String date2){

        Response response;

        Either<IException, List<CajaReport>> getResponseGeneric = billBusiness.getReportCaja(id_store,
                                                                                 new Date(date1),
                                                                                 new Date(date2));

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/myboxes")
    @Timed
    public Response getBoxesToOpen(@QueryParam("id_person") Long id_third_employee){

        Response response;

        Either<IException, List<CajaSelect>> getResponseGeneric = billBusiness.getBoxesToOpen(id_third_employee);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/isopen")
    @Timed
    public Response isOpen(@QueryParam("idcaja") Long id_third_employee){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.isOpen(id_third_employee);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }







}