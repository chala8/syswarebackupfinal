package com.name.business.DAOs;

import com.name.business.entities.DetailClose;
import com.name.business.mappers.DetailCloseMapper;
import com.name.business.representations.DetailCloseDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(DetailCloseMapper.class)
public interface DetailCloseDAO {


	@SqlQuery("SELECT * FROM CAJA_MOVIMIENTOS WHERE( ID_CIERRE_CAJA = :ID_CIERRE_CAJA ) order by movement_date")
	List<DetailClose> readCommons(@Bind("ID_CIERRE_CAJA") Long ID_CIERRE_CAJA);

	@SqlUpdate("INSERT INTO CAJA_MOVIMIENTOS(MOVEMENT_DATE,VALOR,NATURALEZA,NOTES,ID_CIERRE_CAJA)" +
			"            VALUES ( SYSDATE ,:dCaja.VALOR ,:dCaja.NATURALEZA ,:dCaja.NOTES ,:ID_CIERRE_CAJA )")
	void create(@BindBean("dCaja") DetailCloseDTO closeDTO, @Bind("ID_CIERRE_CAJA") Long id_common_state);

	@SqlUpdate(" CALL update_balance(:id_cierre_caja) ")
	void updateBalance(@Bind("id_cierre_caja") Long id_cierre_caja);
}
