package com.name.business.mappers;

import com.name.business.entities.Store;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StoreMapper implements ResultSetMapper<Store> {

    @Override
    public Store map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Store(
                resultSet.getLong("ID_STORE"),
                resultSet.getString("STORE"),
                resultSet.getLong("CAJA_NUMBER"),
                resultSet.getLong("ID_THIRD")
        );
    }
}
