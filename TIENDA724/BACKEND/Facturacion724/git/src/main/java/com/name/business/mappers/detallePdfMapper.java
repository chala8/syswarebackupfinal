package com.name.business.mappers;


import com.name.business.entities.CommonState;
import com.name.business.entities.DetailPaymentBill;
import com.name.business.entities.detallePdf;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class detallePdfMapper implements ResultSetMapper<detallePdf> {

    @Override
    public detallePdf map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new detallePdf(

                resultSet.getString("CODIGO"),
                resultSet.getString("CODIGOPROV"),
                resultSet.getString("ID_STORE"),
                resultSet.getString("PRODUCTO"),
                resultSet.getString("CANTIDAD"),
                resultSet.getString("VALORCOMPRA"),
                resultSet.getString("DESCUENTO"),
                resultSet.getString("IVA"),
                resultSet.getString("TOTAL"),
                resultSet.getString("TOTALCONIVA")
        );
    }
}
