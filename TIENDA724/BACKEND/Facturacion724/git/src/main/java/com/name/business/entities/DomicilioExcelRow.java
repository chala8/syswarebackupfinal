package com.name.business.entities;

import java.util.Date;

public class DomicilioExcelRow {
    private String VENDEDOR;
    private String CLIENTE;
    private String ESTADO;
    private String BODEGA;
    private String ORDEN_DE_COMPRA;
    private String FECHA;
    private String FECHA_ENTREGA;
    private String OBSERVACION;
    private String CODIGO_DEL_PRODUCTO;
    private String CANTIDAD;
    private String DESCUENTO;
    private String DESCUENT;
    private String TIPO;
    private String VALOR;
    private String UNIDAD;
    private String LISTA;


    public DomicilioExcelRow(String VENDEDOR,
             String CLIENTE,
             String ESTADO,
             String BODEGA,
             String ORDEN_DE_COMPRA,
             String FECHA,
             String FECHA_ENTREGA,
             String OBSERVACION,
             String CODIGO_DEL_PRODUCTO,
             String CANTIDAD,
             String DESCUENTO,
             String DESCUENT,
             String TIPO,
             String VALOR,
             String UNIDAD,
             String LISTA) {
        this.VENDEDOR = VENDEDOR;
        this.CLIENTE = CLIENTE;
        this.ESTADO = ESTADO;
        this.BODEGA = BODEGA;
        this.ORDEN_DE_COMPRA = ORDEN_DE_COMPRA;
        this.FECHA = FECHA;
        this.FECHA_ENTREGA = FECHA_ENTREGA;
        this.OBSERVACION = OBSERVACION;
        this.CODIGO_DEL_PRODUCTO = CODIGO_DEL_PRODUCTO;
        this.CANTIDAD = CANTIDAD;
        this.DESCUENTO = DESCUENTO;
        this.DESCUENT = DESCUENT;
        this.TIPO = TIPO;
        this.VALOR = VALOR;
        this.UNIDAD = UNIDAD;
        this.LISTA = LISTA;
    }


    public String getVENDEDOR() {
        return VENDEDOR;
    }

    public void setVENDEDOR(String VENDEDOR) {
        this.VENDEDOR = VENDEDOR;
    }

    public String getCLIENTE() {
        return CLIENTE;
    }

    public void setCLIENTE(String CLIENTE) {
        this.CLIENTE = CLIENTE;
    }

    public String getESTADO() {
        return ESTADO;
    }

    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }

    public String getBODEGA() {
        return BODEGA;
    }

    public void setBODEGA(String BODEGA) {
        this.BODEGA = BODEGA;
    }

    public String getORDEN_DE_COMPRA() {
        return ORDEN_DE_COMPRA;
    }

    public void setORDEN_DE_COMPRA(String ORDEN_DE_COMPRA) {
        this.ORDEN_DE_COMPRA = ORDEN_DE_COMPRA;
    }

    public String getFECHA() {
        return FECHA;
    }

    public void setFECHA(String FECHA) {
        this.FECHA = FECHA;
    }

    public String getFECHA_ENTREGA() {
        return FECHA_ENTREGA;
    }

    public void setFECHA_ENTREGA(String FECHA_ENTREGA) {
        this.FECHA_ENTREGA = FECHA_ENTREGA;
    }

    public String getOBSERVACION() {
        return OBSERVACION;
    }

    public void setOBSERVACION(String OBSERVACION) {
        this.OBSERVACION = OBSERVACION;
    }

    public String getCODIGO_DEL_PRODUCTO() {
        return CODIGO_DEL_PRODUCTO;
    }

    public void setCODIGO_DEL_PRODUCTO(String CODIGO_DEL_PRODUCTO) {
        this.CODIGO_DEL_PRODUCTO = CODIGO_DEL_PRODUCTO;
    }

    public String getCANTIDAD() {
        return CANTIDAD;
    }

    public void setCANTIDAD(String CANTIDAD) {
        this.CANTIDAD = CANTIDAD;
    }

    public String getDESCUENTO() {
        return DESCUENTO;
    }

    public void setDESCUENTO(String DESCUENTO) {
        this.DESCUENTO = DESCUENTO;
    }

    public String getDESCUENT() {
        return DESCUENT;
    }

    public void setDESCUENT(String DESCUENT) {
        this.DESCUENT = DESCUENT;
    }

    public String getTIPO() {
        return TIPO;
    }

    public void setTIPO(String TIPO) {
        this.TIPO = TIPO;
    }

    public String getVALOR() {
        return VALOR;
    }

    public void setVALOR(String VALOR) {
        this.VALOR = VALOR;
    }

    public String getUNIDAD() {
        return UNIDAD;
    }

    public void setUNIDAD(String UNIDAD) {
        this.UNIDAD = UNIDAD;
    }

    public String getLISTA() {
        return LISTA;
    }

    public void setLISTA(String LISTA) {
        this.LISTA = LISTA;
    }



}
