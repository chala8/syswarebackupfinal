package com.name.business.mappers;


import com.name.business.entities.DetallePedido;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetallePedidoMapper implements ResultSetMapper<DetallePedido> {

    @Override
    public DetallePedido map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DetallePedido(
                resultSet.getString("OWNBARCODE"),
                resultSet.getString("FABRICANTE"),
                resultSet.getString("MARCA"),
                resultSet.getString("LINEA"),
                resultSet.getString("CATEGORIA"),
                resultSet.getString("PRESENTACION"),
                resultSet.getString("PRODUCTO"),
                resultSet.getLong("CANTIDAD"),
                resultSet.getDouble("COSTO"),
                resultSet.getDouble("COSTOTOTAL"),
                resultSet.getLong("ID_PRODUCT_THIRD"),
                resultSet.getDouble("TAX"),
                resultSet.getDouble("TAX_IND")
        );
    }
}
