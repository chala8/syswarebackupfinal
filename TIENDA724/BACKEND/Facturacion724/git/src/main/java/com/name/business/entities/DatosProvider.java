package com.name.business.entities;

import java.util.Date;

public class DatosProvider {

    private Long ID_STORE_PROVIDER;
    private String ID_CITY;
    private String CITY_NAME;
    private String ADDRESS;
    private Double LATITUD;
    private Double LONGITUD;
    private String DOCUMENT_TYPE;
    private String DOCUMENT_NUMBER;
    private String DESCRIPTION;
    private String EMAILPROV;
    private String PHONEPROV;
    private String URL_LOGO;
    private String ID_COUNTRY;

    public DatosProvider(Long ID_STORE_PROVIDER,
                         String ID_CITY,
             String CITY_NAME,
             String ADDRESS,
             Double LATITUD,
             Double LONGITUD,
             String DOCUMENT_TYPE,
             String DOCUMENT_NUMBER,
             String DESCRIPTION,
             String EMAILPROV,
             String PHONEPROV,
             String URL_LOGO,
                         String ID_COUNTRY) {
        this.ID_COUNTRY = ID_COUNTRY;
        this.ID_STORE_PROVIDER = ID_STORE_PROVIDER;
        this.ID_CITY = ID_CITY;
        this.CITY_NAME = CITY_NAME;
        this.LATITUD = LATITUD;
        this.LONGITUD = LONGITUD;
        this.ADDRESS = ADDRESS;
        this.DOCUMENT_TYPE = DOCUMENT_TYPE;
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
        this.DESCRIPTION = DESCRIPTION;
        this.EMAILPROV = EMAILPROV;
        this.PHONEPROV = PHONEPROV;
        this.URL_LOGO = URL_LOGO;
    }

    public String getID_COUNTRY() { return ID_COUNTRY; }

    public void setID_COUNTRY(String ID_COUNTRY) { this.ID_COUNTRY = ID_COUNTRY; }

    public String getURL_LOGO() {
        return URL_LOGO;
    }

    public void setURL_LOGO(String URL_LOGO) {
        this.URL_LOGO = URL_LOGO;
    }

    public String getEMAILPROV() {
        return EMAILPROV;
    }

    public void setEMAILPROV(String EMAILPROV) {
        this.EMAILPROV = EMAILPROV;
    }

    public String getPHONEPROV() {
        return PHONEPROV;
    }

    public void setPHONEPROV(String PHONEPROV) {
        this.PHONEPROV = PHONEPROV;
    }

    public String getDOCUMENT_TYPE() {
        return DOCUMENT_TYPE;
    }

    public void setDOCUMENT_TYPE(String DOCUMENT_TYPE) {
        this.DOCUMENT_TYPE = DOCUMENT_TYPE;
    }

    public String getDOCUMENT_NUMBER() {
        return DOCUMENT_NUMBER;
    }

    public void setDOCUMENT_NUMBER(String DOCUMENT_NUMBER) {
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public Long getID_STORE_PROVIDER() {
        return ID_STORE_PROVIDER;
    }

    public void setID_STORE_PROVIDER(Long ID_STORE_PROVIDER) {
        this.ID_STORE_PROVIDER = ID_STORE_PROVIDER;
    }

    public String getID_CITY() {
        return ID_CITY;
    }

    public void setID_CITY(String ID_CITY) {
        this.ID_CITY = ID_CITY;
    }

    public String getCITY_NAME() {
        return CITY_NAME;
    }

    public void setCITY_NAME(String CITY_NAME) {
        this.CITY_NAME = CITY_NAME;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public Double getLATITUD() {
        return LATITUD;
    }

    public void setLATITUD(Double LATITUD) {
        this.LATITUD = LATITUD;
    }

    public Double getLONGITUD() {
        return LONGITUD;
    }

    public void setLONGITUD(Double LONGITUD) {
        this.LONGITUD = LONGITUD;
    }


}