package com.name.business.mappers;

import com.name.business.entities.BillDetail;
import com.name.business.entities.CommonState;
import com.name.business.entities.BillData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillDetailMapper  implements ResultSetMapper<BillDetail> {

    @Override
    public BillDetail map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BillDetail(
                resultSet.getString("PRODUCT_STORE_NAME"),
                resultSet.getLong("QUANTITY"),
                resultSet.getDouble("VALOR"),
                resultSet.getString("CODE"),
                resultSet.getDouble("PERCENT"),
                resultSet.getDouble("PRICE_NO_IVA"),
                resultSet.getDouble("PRICE"),
                resultSet.getLong("IDTAX")
        );
    }
}
