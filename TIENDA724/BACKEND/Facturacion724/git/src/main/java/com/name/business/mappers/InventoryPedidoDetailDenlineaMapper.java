package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.InventoryPedidoDetail;
import com.name.business.entities.InventoryPedidoDetailDenlinea;
import com.name.business.entities.LegalData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InventoryPedidoDetailDenlineaMapper implements ResultSetMapper<InventoryPedidoDetailDenlinea> {
    @Override
    public InventoryPedidoDetailDenlinea map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new InventoryPedidoDetailDenlinea(
                resultSet.getString("PCT_DESCUENTO"),
                resultSet.getLong("ID_TAX"),
                resultSet.getDouble("PRICE"),
                resultSet.getLong("ID_PRODUCT_STORE"),
                resultSet.getString("PRODUCT_STORE_NAME"),
                resultSet.getString("OWNBARCODE"),
                resultSet.getString("PRODUCT_STORE_CODE"),
                resultSet.getDouble("TAX"),
                resultSet.getLong("ID_PROVIDER"),
                resultSet.getString("PROVIDER"),
                resultSet.getLong("IDLINEA"),
                resultSet.getString("LINEA"),
                resultSet.getLong("IDCAT"),
                resultSet.getString("CATEGORIA"),
                resultSet.getLong("CANTIDADVENDIDA"),
                resultSet.getString("IMG"),
                resultSet.getString("IMGLINEA"),
                resultSet.getString("IMGCAT"),
                resultSet.getString("NOMBREPRODUCTO"),
                resultSet.getLong("ID_ATTRIBUTE"),
                resultSet.getString("ATRIBUTO"),
                resultSet.getString("VALORATRIBUTO"),
                resultSet.getLong("ID_ATTRIBUTE_VALUE"),
                resultSet.getString("IMG_URL"),
                resultSet.getString("ORDEN"),
                resultSet.getString("DESCRIPTION")
        );
    }
}
