package com.name.business.mappers;


import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailPaymentBillCompleteMapper implements ResultSetMapper<DetailPaymentBillComplete> {

    @Override
    public DetailPaymentBillComplete map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DetailPaymentBillComplete(
                resultSet.getLong("ID_DETAIL_PAYMENT_BILL"),
                resultSet.getDouble("PAYMENT_VALUE"),
                resultSet.getLong("ID_BILL"),
                resultSet.getString("APPROBATION_CODE"),
                new PaymentMethod(
                        resultSet.getLong("ID_PAYMENT_METHOD"),
                        resultSet.getString("NAME_PAY_ME"),
                        new CommonState(
                                resultSet.getLong("ID_CM_PAY_MET"),
                                resultSet.getInt("STATE_PAY_ME"),
                                resultSet.getDate("CREATION_PAY_ME"),
                                resultSet.getDate("UPDATE_PAY_ME")
                        )
                ),
                new WayToPay(
                        resultSet.getLong("ID_WAY_TO_PAY"),
                        resultSet.getString("NAME_WAY_PAY"),
                        new CommonState(
                                resultSet.getLong("ID_CM_WAY_PAY"),
                                resultSet.getInt("STATE_CM_WAY_PAY"),
                                resultSet.getDate("CREATION_WAY_PAY"),
                                resultSet.getDate("UPDATE_WAY_PAY")
                        )
                ),
                new CommonState(
                        resultSet.getLong("ID_CM_DET_PAY_BIL"),
                        resultSet.getInt("STATE_DET_PAY_BIL"),
                        resultSet.getDate("CREATION_DET_PAY_BIL"),
                        resultSet.getDate("UPDATE_DET_PAY_BIL")
                )

        );
    }
}
