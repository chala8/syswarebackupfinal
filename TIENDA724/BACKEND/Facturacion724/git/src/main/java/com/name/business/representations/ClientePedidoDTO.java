package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class ClientePedidoDTO {

    private String NUM_DOCUMENT;
    private String PURCHASE_DATE;
    private String DESCRIPTION;
    private String PHONE;
    private String MAIL;
    private String address;

    @JsonCreator
    public ClientePedidoDTO(@JsonProperty("num_DOCUMENT") String NUM_DOCUMENT,
                            @JsonProperty("purchase_DATE") String PURCHASE_DATE,
                            @JsonProperty("description") String DESCRIPTION,
                            @JsonProperty("phone") String PHONE,
                            @JsonProperty("mail") String MAIL,
                            @JsonProperty("address") String address) {
        this.NUM_DOCUMENT = NUM_DOCUMENT;
        this.DESCRIPTION = DESCRIPTION;
        this.PHONE = PHONE;
        this.PURCHASE_DATE = PURCHASE_DATE;
        this.MAIL = MAIL;
        this.address = address;
    }
    //--------------------------------------------------------------------------------------------------

    public String getaddress() {
        return address;
    }

    public void setaddress(String address) { this.address = address; }

    //--------------------------------------------------------------------------------------------------

    public String getMAIL() {
        return MAIL;
    }

    public void setMAIL(String MAIL) {
        this.MAIL = MAIL;
    }


    //--------------------------------------------------------------------------------------------------

    public String getPURCHASE_DATE() {
        return PURCHASE_DATE;
    }

    public void setPURCHASE_DATE(String PURCHASE_DATE) {
        this.PURCHASE_DATE = PURCHASE_DATE;
    }

    //--------------------------------------------------------------------------------------------------

    public String getPHONE() {
        return PHONE;
    }

    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    //--------------------------------------------------------------------------------------------------

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    //--------------------------------------------------------------------------------------------------

    public String getNUM_DOCUMENT() {
        return NUM_DOCUMENT;
    }

    public void setNUM_DOCUMENT(String NUM_DOCUMENT) {
        this.NUM_DOCUMENT = NUM_DOCUMENT;
    }

    //--------------------------------------------------------------------------------------------------
}
