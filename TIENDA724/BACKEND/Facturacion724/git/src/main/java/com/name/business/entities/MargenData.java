package com.name.business.entities;

public class MargenData {
    private Double price;
    private Double standard_price;

    public MargenData(Double price, Double standard_price) {
        this.price = price;
        this.standard_price = standard_price;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getStandard_price() {
        return standard_price;
    }

    public void setStandard_price(Double standard_price) {
        this.standard_price = standard_price;
    }
}
