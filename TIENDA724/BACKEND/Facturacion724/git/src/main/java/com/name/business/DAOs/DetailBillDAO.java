package com.name.business.DAOs;

import com.name.business.entities.CommonSimple;
import com.name.business.entities.DetailBill;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.DetailBillMapper;
import com.name.business.representations.DetailBillDTO;
import com.name.business.representations.DetailBillIdDTO;
import com.name.business.representations.DetailPaymentBillDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(DetailBillMapper.class)
public interface DetailBillDAO {


    @SqlQuery("SELECT ID_DETAIL_BILL FROM DETAIL_BILL WHERE ID_DETAIL_BILL IN (SELECT MAX( ID_DETAIL_BILL ) FROM DETAIL_BILL )\n")
    Long getPkLast();
    @SqlQuery("SELECT COUNT(ID_DETAIL_BILL) FROM DETAIL_BILL WHERE ID_DETAIL_BILL = :id\n")
    Integer getValidatorID(@Bind("id") Long id);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_DETAIL_BILL  ID, ID_COMMON_STATE FROM DETAIL_BILL WHERE( ID_DETAIL_BILL = :id_det_bil OR :id_det_bil IS NULL) AND ( ID_BILL = :id_bil OR :id_bil IS NULL) ")
    List<CommonSimple> readCommons(@Bind("id_det_bil") Long id_det_bil,@Bind("id_bil") Long id_bil);

    @SqlQuery("SELECT * FROM V_DETAILBILL V_DET_BIL " +
            " WHERE (V_DET_BIL.ID_DETAIL_BILL=:det_bil.id_detail_bill OR :det_bil.id_detail_bill IS NULL ) AND\n" +
            "      (V_DET_BIL.ID_BILL=:det_bil.id_bill OR :det_bil.id_bill IS NULL ) AND\n" +
            "      (V_DET_BIL.ID_PRODUCT_THIRD=:det_bil.id_product_third OR :det_bil.id_product_third IS NULL ) AND\n" +
            "      (V_DET_BIL.QUANTITY=:det_bil.quantity OR :det_bil.quantity IS NULL ) AND\n" +
            "      (V_DET_BIL.PRICE=:det_bil.price OR :det_bil.price IS NULL ) AND\n" +
            "      (V_DET_BIL.TAX_PRODUCT=:det_bil.tax_product OR :det_bil.tax_product IS NULL ) AND\n" +
            "      (V_DET_BIL.TAX=:det_bil.tax OR :det_bil.tax IS NULL ) AND\n" +
            "      (V_DET_BIL.ID_CM_DET_BIL=:det_bil.id_state_detail_bill OR :det_bil.id_state_detail_bill IS NULL ) AND\n" +
            "      (V_DET_BIL.STATE_DET_BIL=:det_bil.state_detail_bill OR :det_bil.state_detail_bill IS NULL ) AND\n" +
            "      (V_DET_BIL.CREATION_DET_BIL=:det_bil.creation_detail_bill OR :det_bil.creation_detail_bill IS NULL ) AND\n" +
            "      (V_DET_BIL.UPDATE_DET_BIL=:det_bil.update_detail_bill OR :det_bil.update_detail_bill IS NULL)")
    List<DetailBill> read(@BindBean("det_bil") DetailBill detailBill);


    @SqlUpdate("INSERT INTO DETAIL_BILL ( ID_BILL,ID_COMMON_STATE, QUANTITY, PRICE, TAX, ID_PRODUCT_THIRD,TAX_PRODUCT, ID_STORAGE) " +
            "   VALUES (:id_bill,:id_common_state,:det_bil.quantity,:det_bil.price, " +
            "        :det_bil.tax,:det_bil.id_product_third,:det_bil.tax_product, :det_bil.id_storage) ")
    void create(@Bind("id_common_state") Long id_common_state,@Bind("id_bill") Long id_bill,@BindBean("det_bil") DetailBillDTO detailBillDTO);

    @SqlUpdate("INSERT INTO DETAIL_BILL ( ID_BILL,ID_COMMON_STATE, QUANTITY, PRICE, TAX, ID_PRODUCT_THIRD,TAX_PRODUCT, ID_STORAGE) " +
            "   VALUES (:id_bill,:id_common_state,:det_bil.quantity,:det_bil.price, " +
            "        :det_bil.tax,:det_bil.id_product_third,:det_bil.tax_product, null) ")
    void create2(@Bind("id_common_state") Long id_common_state,@Bind("id_bill") Long id_bill,@BindBean("det_bil") DetailBillDTO detailBillDTO);


    @SqlUpdate("UPDATE DETAIL_BILL SET \n" +
            "    QUANTITY=:det_bil.quantity,\n" +
            "    PRICE=:det_bil.price,\n" +
            "    TAX=:det_bil.tax,\n" +
            "    ID_PRODUCT_THIRD=:det_bil.id_product_third,\n" +
            "    TAX_PRODUCT=:det_bil.tax_product\n" +
            "    WHERE (ID_DETAIL_BILL =  :id_det_bill OR :id_det_bill IS NULL ) AND\n" +
            "          (ID_BILL =  :id_bill OR :id_bill IS NULL )")
    int update(@Bind("id_det_bill")Long id_detail_bill,@Bind("id_bill") Long id_bill,@BindBean("det_bil") DetailBillIdDTO detailBillIdDTO);


    int delete();


    int deletePermanent();

    @SqlQuery("SELECT COUNT(ID_BILL) FROM BILL WHERE ID_BILL = :id")
    Integer getValidatorIDBill(@Bind("id") Long id_bill);
}
