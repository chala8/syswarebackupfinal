package com.name.business.entities;

import javax.smartcardio.ATR;
import java.util.Date;

public class InventoryPedidoDetailDenlinea {
    private String PCT_DESCUENTO;
    private Long ID_TAX;
    private Double PRICE;
    private Long ID_PRODUCT_STORE;
    private String PRODUCT_STORE_NAME;
    private String OWNBARCODE;
    private String PRODUCT_STORE_CODE;
    private Double TAX;
    private Long ID_PROVIDER;
    private String PROVIDER;
    private Long ID_LINEA;
    private String LINEA;
    private Long ID_CAT;
    private String CATEGORIA;
    private Long CANTIDADVENDIDA;
    private String IMG;
    private String IMGLINEA;
    private String IMGCAT;
    private String NOMBREPRODUCTO;
    private Long ID_ATRIBUTO;
    private String ATRIBUTO;
    private String VALORATRIBUTO;
    private Long ID_ATTRIBUTE_VALUE;
    private String IMG_URL;
    private String ORDEN;
    private String DESCRIPTION;

    public InventoryPedidoDetailDenlinea(
                                 String PCT_DESCUENTO,
                                 Long ID_TAX,
                                 Double PRICE,
                                 Long ID_PRODUCT_STORE,
                                 String PRODUCT_STORE_NAME,
                                 String OWNBARCODE,
                                 String PRODUCT_STORE_CODE,
                                 Double TAX,
                                 Long ID_PROVIDER,
                                 String PROVIDER,
                                 Long ID_LINEA,
                                 String LINEA,
                                 Long ID_CAT,
                                 String CATEGORIA,
                                 Long CANTIDADVENDIDA,
                                 String IMG,
                                 String IMGLINEA,
                                 String IMGCAT,
                                 String NOMBREPRODUCTO,
                                 Long ID_ATRIBUTO,
                                 String ATRIBUTO,
                                 String VALORATRIBUTO,
                                 Long ID_ATTRIBUTE_VALUE,
                                 String IMG_URL,
                                 String ORDEN,
                                 String DESCRIPTION) {
        this.PCT_DESCUENTO = PCT_DESCUENTO;
        this.DESCRIPTION = DESCRIPTION;
        this.NOMBREPRODUCTO = NOMBREPRODUCTO;
        this.ID_ATRIBUTO = ID_ATRIBUTO;
        this.ATRIBUTO = ATRIBUTO;
        this.VALORATRIBUTO = VALORATRIBUTO;
        this.ID_ATTRIBUTE_VALUE = ID_ATTRIBUTE_VALUE;
        this.IMG_URL = IMG_URL;
        this.ORDEN = ORDEN;
        this.ID_TAX = ID_TAX;
        this.PRICE = PRICE;
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
        this.OWNBARCODE = OWNBARCODE;
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
        this.TAX = TAX;
        this.ID_PROVIDER = ID_PROVIDER;
        this.PROVIDER = PROVIDER;
        this.ID_LINEA = ID_LINEA;
        this.LINEA = LINEA;
        this.ID_CAT = ID_CAT;
        this.CATEGORIA = CATEGORIA;
        this.CANTIDADVENDIDA = CANTIDADVENDIDA;
        this.IMG = IMG;
        this.IMGLINEA = IMGLINEA;
        this.IMGCAT = IMGCAT;
    }

    public String getPCT_DESCUENTO() {
        return PCT_DESCUENTO;
    }

    public void setPCT_DESCUENTO(String PCT_DESCUENTO) {
        this.PCT_DESCUENTO = PCT_DESCUENTO;
    }

    public String getDESCRIPTION() { return DESCRIPTION; }

    public void setDESCRIPTION(String DESCRIPTION) { this.DESCRIPTION = DESCRIPTION; }

    public String getNOMBREPRODUCTO() {
        return NOMBREPRODUCTO;
    }

    public void setNOMBREPRODUCTO(String NOMBREPRODUCTO) {
        this.NOMBREPRODUCTO = NOMBREPRODUCTO;
    }

    public Long getID_ATRIBUTO() {
        return ID_ATRIBUTO;
    }

    public void setID_ATRIBUTO(Long ID_ATRIBUTO) {
        this.ID_ATRIBUTO = ID_ATRIBUTO;
    }

    public String getATRIBUTO() {
        return ATRIBUTO;
    }

    public void setATRIBUTO(String ATRIBUTO) {
        this.ATRIBUTO = ATRIBUTO;
    }

    public String getVALORATRIBUTO() {
        return VALORATRIBUTO;
    }

    public void setVALORATRIBUTO(String VALORATRIBUTO) {
        this.VALORATRIBUTO = VALORATRIBUTO;
    }

    public Long getID_ATTRIBUTE_VALUE() {
        return ID_ATTRIBUTE_VALUE;
    }

    public void setID_ATTRIBUTE_VALUE(Long ID_ATTRIBUTE_VALUE) {
        this.ID_ATTRIBUTE_VALUE = ID_ATTRIBUTE_VALUE;
    }

    public String getIMG_URL() {
        return IMG_URL;
    }

    public void setIMG_URL(String IMG_URL) {
        this.IMG_URL = IMG_URL;
    }

    public String getORDEN() {
        return ORDEN;
    }

    public void setORDEN(String ORDEN) {
        this.ORDEN = ORDEN;
    }


    public String getIMGLINEA() {
        return IMGLINEA;
    }

    public void setIMGLINEA(String IMGLINEA) {
        this.IMGLINEA = IMGLINEA;
    }

    public String getIMGCAT() {
        return IMGCAT;
    }

    public void setIMGCAT(String IMGCAT) {
        this.IMGCAT = IMGCAT;
    }

    public String getIMG() {
        return IMG;
    }

    public void setIMG(String IMG) {
        this.IMG = IMG;
    }

    public Long getID_LINEA() {
        return ID_LINEA;
    }

    public void setID_LINEA(Long ID_LINEA) {
        this.ID_LINEA = ID_LINEA;
    }

    public String getLINEA() {
        return LINEA;
    }

    public void setLINEA(String LINEA) {
        this.LINEA = LINEA;
    }

    public Long getID_CAT() {
        return ID_CAT;
    }

    public void setID_CAT(Long ID_CAT) {
        this.ID_CAT = ID_CAT;
    }

    public String getCATEGORIA() {
        return CATEGORIA;
    }

    public void setCATEGORIA(String CATEGORIA) {
        this.CATEGORIA = CATEGORIA;
    }

    public Long getCANTIDADVENDIDA() {
        return CANTIDADVENDIDA;
    }

    public void setCANTIDADVENDIDA(Long CANTIDADVENDIDA) {
        this.CANTIDADVENDIDA = CANTIDADVENDIDA;
    }

    public Long getID_TAX() {
        return ID_TAX;
    }
    public void setID_TAX(Long ID_TAX) {
        this.ID_TAX = ID_TAX;
    }

    public Double getPRICE() {
        return PRICE;
    }
    public void setPRICE(Double PRICE) { this.PRICE = PRICE; }

    public Long getID_PRODUCT_STORE() {
        return ID_PRODUCT_STORE;
    }
    public void setID_PRODUCT_STORE(Long ID_PRODUCT_STORE) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
    }

    public String getPRODUCT_STORE_NAME() {
        return PRODUCT_STORE_NAME;
    }
    public void setPRODUCT_STORE_NAME(String PRODUCT_STORE_NAME) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
    }

    public String getOWNBARCODE() {
        return OWNBARCODE;
    }
    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }

    public String getPRODUCT_STORE_CODE() {
        return PRODUCT_STORE_CODE;
    }
    public void setPRODUCT_STORE_CODE(String PRODUCT_STORE_CODE) {
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
    }

    public Double getTAX() {
        return TAX;
    }
    public void setTAX(Double TAX) { this.TAX = TAX; }

    public Long getID_PROVIDER() {
        return ID_PROVIDER;
    }
    public void setID_PROVIDER(Long ID_PROVIDER) {
        this.ID_PROVIDER = ID_PROVIDER;
    }

    public String getPROVIDER() {
        return PROVIDER;
    }
    public void setPROVIDER(String PROVIDER) {
        this.PROVIDER = PROVIDER;
    }


}
