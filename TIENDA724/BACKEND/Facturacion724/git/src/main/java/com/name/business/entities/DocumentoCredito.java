package com.name.business.entities;

public class DocumentoCredito {

    private String NOMBRE;
    private String RUTA;
    private String TIPO_DOCUMENTO;

    private String id_credito_documento;

    public DocumentoCredito(String NOMBRE, String RUTA, String TIPO_DOCUMENTO, String id_credito_documento) {
        this.NOMBRE = NOMBRE;
        this.RUTA = RUTA;
        this.TIPO_DOCUMENTO = TIPO_DOCUMENTO;
        this.id_credito_documento = id_credito_documento;
    }


    public String getId_credito_documento() { return id_credito_documento; }


    public void setId_credito_documento(String id_credito_documento) { this.id_credito_documento = id_credito_documento; }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }

    public String getRUTA() {
        return RUTA;
    }

    public void setRUTA(String RUTA) {
        this.RUTA = RUTA;
    }

    public String getTIPO_DOCUMENTO() {
        return TIPO_DOCUMENTO;
    }

    public void setTIPO_DOCUMENTO(String TIPO_DOCUMENTO) {
        this.TIPO_DOCUMENTO = TIPO_DOCUMENTO;
    }

}
