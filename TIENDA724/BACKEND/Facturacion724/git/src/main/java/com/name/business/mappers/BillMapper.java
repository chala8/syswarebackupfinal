package com.name.business.mappers;

import com.name.business.entities.Bill;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillMapper  implements ResultSetMapper<Bill> {

    @Override
    public Bill map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Bill(
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("ID_BILL_FATHER"),
                resultSet.getLong("ID_THIRD_EMPLOYEE"),
                resultSet.getLong("ID_THIRD"),
                resultSet.getString("CONSECUTIVE"),
                resultSet.getDate("PURCHASE_DATE"),
                resultSet.getDouble("SUBTOTAL"),
                resultSet.getDouble("TOTALPRICE"),
                resultSet.getDouble("TAX"),
                resultSet.getDouble("DISCOUNT"),

                resultSet.getLong("ID_PAYMENT_STATE"),
                resultSet.getLong("ID_BILL_STATE"),
                resultSet.getLong("ID_BILL_TYPE"),

                new CommonState(
                        resultSet.getLong("ID_CM_BIL"),
                        resultSet.getInt("STATE_BIL"),
                        resultSet.getDate("CREATION_BIL"),
                        resultSet.getDate("UPDATE_BIL")
                )



        );
    }
}
