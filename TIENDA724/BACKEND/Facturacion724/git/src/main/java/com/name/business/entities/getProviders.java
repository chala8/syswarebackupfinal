package com.name.business.entities;

public class getProviders {

    private Long ID_THIRD_DESTINITY;
    private String CBI_FULLNAME;
    private Long ID_STORE_PROVIDER;

    public getProviders(  Long ID_THIRD_DESTINITY,
                       String CBI_FULLNAME,
                       Long ID_STORE_PROVIDER) {
        this.ID_THIRD_DESTINITY = ID_THIRD_DESTINITY;
        this.CBI_FULLNAME = CBI_FULLNAME;
        this.ID_STORE_PROVIDER = ID_STORE_PROVIDER;
    }

    public Long getID_THIRD_DESTINITY() {
        return ID_THIRD_DESTINITY;
    }

    public void setID_THIRD_DESTINITY(Long ID_THIRD_DESTINITY) {
        this.ID_THIRD_DESTINITY = ID_THIRD_DESTINITY;
    }

    public String getCBI_FULLNAME() {
        return CBI_FULLNAME;
    }

    public void setCBI_FULLNAME(String CBI_FULLNAME) {
        this.CBI_FULLNAME = CBI_FULLNAME;
    }

    public Long getID_STORE_PROVIDER() {
        return ID_STORE_PROVIDER;
    }

    public void setID_STORE_PROVIDER(Long ID_STORE_PROVIDER) {
        this.ID_STORE_PROVIDER = ID_STORE_PROVIDER;
    }
}
