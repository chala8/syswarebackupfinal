package com.name.business.entities;

import java.util.Date;

public class BillComplete {
    private Long id_bill;
    private Long id_bill_father;
    private Long id_third_employee;
    private Long id_third;

    private String consecutive;
    private Date purchase_date;
    private Double subtotal;
    private Double totalprice;
    private Double tax;
    private Double discount;

    private PaymentState payment_state;
    private BillState bill_state;
    private BillType bill_type;

    private Long id_state_bill;
    private Integer state_bill;
    private Date creation_bill;
    private Date update_bill;

    public BillComplete(Long id_bill, Long id_bill_father, Long id_third_employee, Long id_third, String consecutive,
                        Date purchase_date, Double subtotal, Double totalprice, Double tax,Double discount,
                        PaymentState payment_state, BillState bill_state, BillType bill_type, CommonState state) {
        this.id_bill = id_bill;
        this.id_bill_father = id_bill_father;
        this.id_third_employee = id_third_employee;
        this.id_third = id_third;
        this.consecutive = consecutive;
        this.purchase_date = purchase_date;
        this.subtotal = subtotal;
        this.totalprice = totalprice;
        this.tax = tax;
        this.discount=discount;
        this.payment_state = payment_state;
        this.bill_state = bill_state;
        this.bill_type = bill_type;
        this.id_state_bill =state.getId_common_state();
        this.state_bill  =state.getState();
        this.creation_bill =state.getCreation_date();
        this.update_bill =state.getUpdate_date();
    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_bill(),
                this.getState_bill(),
                this.getCreation_bill(),
                this.getUpdate_bill()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_bill =state.getId_common_state();
        this.state_bill  =state.getState();
        this.creation_bill =state.getCreation_date();
        this.update_bill =state.getUpdate_date();
    }


    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Long getId_bill() {
        return id_bill;
    }

    public void setId_bill(Long id_bill) {
        this.id_bill = id_bill;
    }

    public Long getId_bill_father() {
        return id_bill_father;
    }

    public void setId_bill_father(Long id_bill_father) {
        this.id_bill_father = id_bill_father;
    }

    public Long getId_third_employee() {
        return id_third_employee;
    }

    public void setId_third_employee(Long id_third_employee) {
        this.id_third_employee = id_third_employee;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public String getConsecutive() {
        return consecutive;
    }

    public void setConsecutive(String consecutive) {
        this.consecutive = consecutive;
    }

    public Date getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(Date purchase_date) {
        this.purchase_date = purchase_date;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(Double totalprice) {
        this.totalprice = totalprice;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public PaymentState getPayment_state() {
        return payment_state;
    }

    public void setPayment_state(PaymentState payment_state) {
        this.payment_state = payment_state;
    }

    public BillState getBill_state() {
        return bill_state;
    }

    public void setBill_state(BillState bill_state) {
        this.bill_state = bill_state;
    }

    public BillType getBill_type() {
        return bill_type;
    }

    public void setBill_type(BillType bill_type) {
        this.bill_type = bill_type;
    }

    public Long getId_state_bill() {
        return id_state_bill;
    }

    public void setId_state_bill(Long id_state_bill) {
        this.id_state_bill = id_state_bill;
    }

    public Integer getState_bill() {
        return state_bill;
    }

    public void setState_bill(Integer state_bill) {
        this.state_bill = state_bill;
    }

    public Date getCreation_bill() {
        return creation_bill;
    }

    public void setCreation_bill(Date creation_bill) {
        this.creation_bill = creation_bill;
    }

    public Date getUpdate_bill() {
        return update_bill;
    }

    public void setUpdate_bill(Date update_bill) {
        this.update_bill = update_bill;
    }
}
