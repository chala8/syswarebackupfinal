package com.name.business.mappers;

import com.name.business.entities.BillState;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillStateMapper  implements ResultSetMapper<BillState> {
    @Override
    public BillState map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BillState(
                resultSet.getLong("ID_BILL_STATE"),
                resultSet.getString("NAME_BIL_ST"),
                new CommonState(
                        resultSet.getLong("ID_CM_BIL_ST"),
                        resultSet.getInt("STATE_CM_BIL_ST"),
                        resultSet.getDate("CREATION_BIL_ST"),
                        resultSet.getDate("UPDATE_BIL_ST")
                )
        );
    }
}
