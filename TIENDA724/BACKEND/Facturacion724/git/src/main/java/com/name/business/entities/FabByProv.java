package com.name.business.entities;

import java.util.Date;

public class FabByProv {
    private String PROVIDER;
    private Long ID_PROVIDER;


    public FabByProv( String PROVIDER,
                      Long ID_PROVIDER) {
        this.PROVIDER = PROVIDER;
        this.ID_PROVIDER = ID_PROVIDER;

    }
    public Long getID_PROVIDER() {
        return ID_PROVIDER;
    }

    public void setID_PROVIDER(Long ID_PROVIDER) {
        this.ID_PROVIDER = ID_PROVIDER;
    }


    public String getPROVIDER() {
        return PROVIDER;
    }

    public void setPROVIDER(String PROVIDER) {
        this.PROVIDER = PROVIDER;
    }

}
