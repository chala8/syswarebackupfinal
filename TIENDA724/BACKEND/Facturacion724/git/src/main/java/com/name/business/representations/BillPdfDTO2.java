package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.ClientePedido;

import java.util.List;

public class BillPdfDTO2 {
    private ClientePedido clientData;
    private String empresa;
    private String tienda;
    private String fecha;
    private String caja;
    private String consecutivo;
    private List<List<String>> detalles;
    private Double subtotal;
    private Double tax;
    private Double totalprice;
    private String nit;
    private String medioPago;
    private String regimen;
    private String nombreCajero;
    private Long cambio;
    private String direccion;
    private String telefono;
    private String cedula;
    private String cliente;
    private String direccionC;
    private String telefonoC;
    private Long pdfSize;
    private String resolucion_DIAN;
    private String regimenT;
    private String prefix;
    private String initial_RANGE;
    private String final_RANGE;

    @JsonCreator
    public BillPdfDTO2(@JsonProperty("clientData") ClientePedido clientData,
                      @JsonProperty("empresa") String empresa,
                      @JsonProperty("tienda") String tienda,
                      @JsonProperty("fecha") String fecha,
                      @JsonProperty("caja") String caja,
                      @JsonProperty("consecutivo") String consecutivo,
                      @JsonProperty("detalles") List<List<String>> detalles,
                      @JsonProperty("subtotal") Double subtotal,
                      @JsonProperty("tax") Double tax,
                      @JsonProperty("total") Double totalprice,
                      @JsonProperty("nit") String nit,
                      @JsonProperty("medio") String medioPago,
                      @JsonProperty("regimen") String regimen,
                      @JsonProperty("nombreCajero") String nombreCajero,
                      @JsonProperty("cambio") Long cambio,
                      @JsonProperty("direccion") String direccion,
                      @JsonProperty("telefono") String telefono,
                      @JsonProperty("cedula") String cedula,
                      @JsonProperty("cliente") String cliente,
                      @JsonProperty("direccionC") String direccionC,
                      @JsonProperty("telefonoC") String telefonoC,
                      @JsonProperty("flag") String flag,
                      @JsonProperty("pdfSize") Long pdfSize,
                      @JsonProperty("resolucion_DIAN") String resolucion_DIAN,
                      @JsonProperty("regimenT") String regimenT,
                      @JsonProperty("prefix") String prefix,
                      @JsonProperty("initial_RANGE") String initial_RANGE,
                      @JsonProperty("final_RANGE") String final_RANGE) {

        this.clientData = clientData;
        this.resolucion_DIAN = resolucion_DIAN;
        this.regimenT = regimenT;
        this.prefix = prefix;
        this.initial_RANGE = initial_RANGE;
        this.final_RANGE = final_RANGE;
        this.empresa = empresa;
        this.tienda = tienda;
        this.fecha = fecha;
        this.caja = caja;
        this.consecutivo = consecutivo;
        this.detalles = detalles;
        this.subtotal = subtotal;
        this.tax = tax;
        this.totalprice = totalprice;
        this.nit = nit;
        this.medioPago = medioPago;
        this.regimen = regimen;
        this.nombreCajero = nombreCajero;
        this.cambio = cambio;
        this.direccion = direccion;
        this.telefono = telefono;
        this.cedula = cedula;
        this.cliente = cliente;
        this.direccionC = direccionC;
        this.telefonoC = telefonoC;
        this.pdfSize = pdfSize;
    }

    public ClientePedido getclientData() {
        return clientData;
    }

    public void setclientData(ClientePedido clientData) { this.clientData = clientData; }

    public String getfinal_RANGE() {
        return final_RANGE;
    }

    public void seetfinal_RANGE(String final_RANGE) { this.final_RANGE = final_RANGE; }

    public String getinitial_RANGE() {
        return initial_RANGE;
    }

    public void setinitial_RANGE(String initial_RANGE) { this.initial_RANGE = initial_RANGE; }

    public String getprefix() {
        return prefix;
    }

    public void setprefix(String prefix) { this.prefix = prefix; }

    public String getregimenT() {
        return regimenT;
    }

    public void setregimenT(String regimenT) {
        this.regimenT = regimenT;
    }

    public String getresolucion_DIAN() {
        return resolucion_DIAN;
    }

    public void setresolucion_DIAN(String resolucion_DIAN) {
        this.resolucion_DIAN = resolucion_DIAN;
    }

    public Long getpdfSize() {
        return this.pdfSize;
    }

    public void setpdfSize(Long pdfSize) {
        this.pdfSize = pdfSize;
    }

    public String getcedula() {
        return this.cedula;
    }

    public void setcedula(String cedula) {
        this.cedula = cedula;
    }

    public String getcliente() {
        return this.cliente;
    }

    public void setcliente(String cliente) {
        this.cliente = cliente;
    }

    public String getempresa() {
        return this.empresa;
    }

    public void setempresa(String empresa) {
        this.empresa = empresa;
    }

    public String gettienda() {
        return tienda;
    }

    public void settienda(String tienda) {
        this.tienda = tienda;
    }

    public String getfecha() {
        return fecha;
    }

    public void setfecha(String fecha) {
        this.fecha = fecha;
    }

    public String getcaja() {
        return caja;
    }

    public void setcaja(String caja) {
        this.caja = caja;
    }

    public String getconsecutivo() {
        return consecutivo;
    }

    public void setconsecutivo(String consecutivo) {
        this.consecutivo = consecutivo;
    }

    public List<List<String>> getdetalles() {
        return detalles;
    }

    public void setdetalles(List<List<String>> detalles) {
        this.detalles = detalles;
    }

    public Double getsubtotal() {
        return subtotal;
    }

    public void setsubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double gettax() {
        return tax;
    }

    public void settax(Double tax) {
        this.tax = tax;
    }

    public Double gettotalprice() {
        return totalprice;
    }

    public void settotalprice(Double totalprice) {
        this.totalprice = totalprice;
    }

    public String getnit() {
        return nit;
    }

    public void setnit(String nit) {
        this.nit = nit;
    }

    public String getmedioPago() {
        return medioPago;
    }

    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    public String getregimen() {
        return regimen;
    }

    public void setregimen(String regimen) {
        this.regimen = regimen;
    }

    public String getnombreCajero() {
        return nombreCajero;
    }

    public void setnombreCajero(String nombreCajero) {
        this.nombreCajero = nombreCajero;
    }

    public Long getcambio() {
        return cambio;
    }

    public void setcambio(long cambio) {
        this.cambio = cambio;
    }

    public String getdireccion() {
        return direccion;
    }

    public void setdireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getdireccionC() {
        return direccionC;
    }

    public void setdireccionC(String direccionC) {
        this.direccionC = direccionC;
    }

    public String gettelefonoC() {
        return telefonoC;
    }

    public void settelefonoC(String telefonoC) {
        this.telefonoC = telefonoC;
    }

    public String gettelefono() {
        return telefono;
    }

    public void settelefono(String telefono) {
        this.telefono = telefono;
    }


}
