package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.WayToPay;
import com.name.business.entities.reorderProd;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class reorderMapper2 implements ResultSetMapper<reorderProd> {

    @Override
    public reorderProd map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new reorderProd(
                new Double (0),
                resultSet.getString("BARCODE"),
                resultSet.getString("PRODUCTO"),
                resultSet.getString("LINEA"),
                resultSet.getString("CATEGORIA"),
                resultSet.getString("MARCA"),
                resultSet.getString("PRESENTACION"),
                resultSet.getLong("COSTO"),
                resultSet.getLong("CANTIDAD")
        );
    }
}
