package com.name.business.sanitations;

import com.name.business.entities.*;

import java.util.Date;

import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class EntitySanitation {

    public static CommonState commonStateSanitation(CommonState commonState){
        CommonState commonStateSanitation=null;
        commonStateSanitation= new CommonState(
                formatoLongSql(commonState.getId_common_state()),
                formatoIntegerSql(commonState.getState()),
                formatoDateSql(commonState.getCreation_date()),
                formatoDateSql(commonState.getUpdate_date())
        );

        return commonStateSanitation;
    }
    public static PaymentState paymentStateSanitation(PaymentState paymentState){
        PaymentState paymentStateSanitation=null;
        paymentStateSanitation= new PaymentState(
                formatoLongSql(paymentState.getId_payment_state()),
                formatoLIKESql(paymentState.getName_payment_state()),
                commonStateSanitation(paymentState.loadingCommonState())
        );

        return paymentStateSanitation;
    }

    public static BillState billStateSanitation(BillState billState){
        BillState billStateSanitation=null;
        billStateSanitation= new BillState(
                formatoLongSql(billState.getId_bill_state()),
                formatoLIKESql(billState.getName_bill_state()),
                commonStateSanitation(billState.loadingCommonState())
        );

        return billStateSanitation;
    }
    public static BillType billTypeSanitation(BillType billType){
        BillType billTypeSanitation=null;
        billTypeSanitation= new BillType(
                formatoLongSql(billType.getId_bill_type()),
                formatoLIKESql(billType.getName_bill_type()),
                commonStateSanitation(billType.loadingCommonState())
        );

        return billTypeSanitation;
    }
    public static WayToPay wayToPaySanitation(WayToPay paymentMethod){
        WayToPay wayToPaySanitation=null;
        wayToPaySanitation= new WayToPay(
                formatoLongSql(paymentMethod.getId_way_pay()),
                formatoLIKESql(paymentMethod.getName_way_pay()),
                commonStateSanitation(paymentMethod.loadingCommonState())
        );

        return wayToPaySanitation;
    }
    public static PaymentMethod paymentMethodSanitation(PaymentMethod paymentMethod){
        PaymentMethod paymentMethodSanitation=null;
        paymentMethodSanitation= new PaymentMethod(
                formatoLongSql(paymentMethod.getId_payment_method()),
                formatoLIKESql(paymentMethod.getName_payment_method()),
                commonStateSanitation(paymentMethod.loadingCommonState())
        );

        return paymentMethodSanitation;
    }


    public static Bill billSanitation(Bill bill){
        Bill billSanitation=null;
        billSanitation= new Bill(
                formatoLongSql(bill.getId_bill()),
                formatoLongSql(bill.getId_bill_father()),
                formatoLongSql(bill.getId_third_employee()),
                formatoLongSql(bill.getId_third()),
                formatoStringSql(bill.getConsecutive()),
                formatoDateSql(bill.getPurchase_date()),
                formatoDoubleSql(bill.getSubtotal()),
                formatoDoubleSql(bill.getTotalprice()),
                formatoDoubleSql(bill.getTax()),
                formatoDoubleSql(bill.getDiscount()),
                formatoLongSql(bill.getId_payment_state()),
                formatoLongSql(bill.getId_bill_state()),
                formatoLongSql(bill.getId_bill_type()),
                commonStateSanitation(bill.loadingCommonState())
        );

        return billSanitation;
    }



    public static BillComplete billCompleteSanitation(BillComplete billComplete){
        BillComplete billCompleteSanitation=null;
        billCompleteSanitation= new BillComplete(
                formatoLongSql(billComplete.getId_bill()),
                formatoLongSql(billComplete.getId_bill_father()),
                formatoLongSql(billComplete.getId_third_employee()),
                formatoLongSql(billComplete.getId_third()),
                formatoStringSql(billComplete.getConsecutive()),
                formatoDateSql(billComplete.getPurchase_date()),
                formatoDoubleSql(billComplete.getSubtotal()),
                formatoDoubleSql(billComplete.getTotalprice()),
                formatoDoubleSql(billComplete.getTax()),
                formatoDoubleSql(billComplete.getDiscount()),
                paymentStateSanitation(billComplete.getPayment_state()),
                billStateSanitation(billComplete.getBill_state()),
                billTypeSanitation(billComplete.getBill_type()),
                commonStateSanitation(billComplete.loadingCommonState())
        );
        return billCompleteSanitation;
    }


    Long detail_bill;
    private Long id_bill; /// It can be null, if The Bill does not exist, or It can given the id Biling exist
    private Integer id_product_third;
    private Integer quantity;
    private Double price;
    private Double tax_product;
    private Double tax;


    public static DetailBill detailBillSanitation(DetailBill detailBill){
        DetailBill detailBillSanitation=null;
        detailBillSanitation= new DetailBill(
                formatoLongSql(detailBill.getId_detail_bill()),
                formatoLongSql(detailBill.getId_bill()),
                formatoLongSql(detailBill.getId_product_third()),
                formatoIntegerSql(detailBill.getQuantity()),
                formatoDoubleSql(detailBill.getPrice()),
                formatoDoubleSql(detailBill.getTax_product()),
                formatoDoubleSql(detailBill.getTax()),
                commonStateSanitation(detailBill.loadingCommonState())
        );

        return detailBillSanitation;
    }

    public static DetailPaymentBill detailPaymentBillSanitation(DetailPaymentBill detailPaymentBill){
        DetailPaymentBill detailPaymentBillSanitation=null;
        detailPaymentBillSanitation= new DetailPaymentBill(
                formatoLongSql(detailPaymentBill.getId_detail_payment_bill()),
                formatoLongSql(detailPaymentBill.getId_bill()),
                formatoLongSql(detailPaymentBill.getId_way_to_pay()),
                formatoLongSql(detailPaymentBill.getId_payment_method()),
                formatoDoubleSql(detailPaymentBill.getPayment_value()),
                formatoStringSql(detailPaymentBill.getAprobation_code()),
                commonStateSanitation(detailPaymentBill.loadingCommonState())
        );

        return detailPaymentBillSanitation;
    }



    public static DetailPaymentBillComplete detailPaymentBillCompleteSanitation(DetailPaymentBillComplete detailPaymentBillComplete){
        DetailPaymentBillComplete detailPaymentBillCompleteSanitation=null;
        detailPaymentBillCompleteSanitation= new DetailPaymentBillComplete(
                formatoLongSql(detailPaymentBillComplete.getId_detail_payment_bill()),
                formatoDoubleSql(detailPaymentBillComplete.getPayment_value()),
                formatoLongSql(detailPaymentBillComplete.getId_bill()),
                formatoStringSql(detailPaymentBillComplete.getAprobation_code()),
                paymentMethodSanitation(detailPaymentBillComplete.getPayment_method()),
                wayToPaySanitation(detailPaymentBillComplete.getWay_pay()),
                commonStateSanitation(detailPaymentBillComplete.loadingCommonState())
        );

        return detailPaymentBillCompleteSanitation;
    }




}

