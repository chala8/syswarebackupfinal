package com.name.business.mappers;


import com.name.business.entities.Pedido;
import com.name.business.entities.PedidosSelect;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PedidosSelectMapper implements ResultSetMapper<PedidosSelect> {

    @Override
    public PedidosSelect map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PedidosSelect(
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("ID_THIRD"),
                resultSet.getString("NUM_DOCUMENTO")
        );
    }
}
