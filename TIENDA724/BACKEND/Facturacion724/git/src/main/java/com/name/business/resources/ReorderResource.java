package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.ReorderBusiness;
import com.name.business.entities.CommonState;
import com.name.business.entities.PaymentState;
import com.name.business.representations.PaymentStateDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;
import com.name.business.entities.*;
@Path("/reorder")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ReorderResource {
    private ReorderBusiness reorderBusiness;

    public ReorderResource(ReorderBusiness reorderBusiness) {
        this.reorderBusiness = reorderBusiness;
    }

    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getReorderData(@QueryParam("id_third") Long id_third,
                                          @QueryParam("id_store")Long id_store,
                                          @QueryParam("date1") String date1,
                                          @QueryParam("date2") String date2) {
        Response response;

        Either<IException, List<reorderProd>> getCategoriesEither = reorderBusiness.getReorderData(id_third,
                id_store,
                new Date(date1),
                new Date(date2));
        if (getCategoriesEither.isRight()) {
            //System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }


    @GET
    @Path("/reorder2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getReorderData2(@QueryParam("id_third") Long id_third,
                                   @QueryParam("id_store")Long id_store,
                                   @QueryParam("hours") Double hours) {
        Response response;

        Either<IException, List<reorderProd>> getCategoriesEither = reorderBusiness.getReorderData2(id_third,
                id_store,
                hours);
        if (getCategoriesEither.isRight()) {
            //System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }

    @GET
    @Path("/manual")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getReorderDataManual(@QueryParam("id_third") Long id_third,
                                    @QueryParam("id_store")Long id_store,
                                    @QueryParam("days") Long hours) {
        Response response;

        Either<IException, List<reorderProd>> getCategoriesEither = reorderBusiness.getReorderDataManual(id_third,
                id_store,
                hours);
        if (getCategoriesEither.isRight()) {
            //System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }

}
