package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.WayToPayBusiness;
import com.name.business.entities.BillType;
import com.name.business.entities.CommonState;
import com.name.business.entities.WayToPay;
import com.name.business.representations.BillTypeDTO;
import com.name.business.representations.WayToPayDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;


@Path("/way-pay")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class WayToPayResource {
   private WayToPayBusiness wayToPayBusiness;

    public WayToPayResource(WayToPayBusiness wayToPayBusiness) {
        this.wayToPayBusiness = wayToPayBusiness;
    }


    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getWayToPayResource(
            @QueryParam("id_way_pay") Long id_bill_type,
            @QueryParam("name_way_pay") String name_bill_type,

            @QueryParam("id_state_way_pay") Long id_state_bill_type,
            @QueryParam("state_way_pay") Integer state_bill_type,
            @QueryParam("creation_way_pay") Date creation_bill_type,
            @QueryParam("update_way_pay") Date update_bill_type){

        Response response;

        Either<IException, List<WayToPay>> getResponseGeneric = wayToPayBusiness.getWayToPay(
                new WayToPay(id_bill_type, name_bill_type,
                        new CommonState(id_state_bill_type, state_bill_type,
                                creation_bill_type, update_bill_type)
                )
        );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postWayToPayResource(WayToPayDTO wayToPayDTO){
        Response response;

        Either<IException, Long> getResponseGeneric = wayToPayBusiness.creatWayToPay(wayToPayDTO);


        if (getResponseGeneric.isRight()){

            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateWayToPayResource(@PathParam("id") Long id_way_pay, WayToPayDTO wayToPayDTO){
        Response response;
        Either<IException, Long> getResponseGeneric = wayToPayBusiness.updateWayToPay(id_way_pay, wayToPayDTO);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deleteWayToPayResource(@PathParam("id") Long id_way_pay){
        Response response;
        Either<IException, Long> getResponseGeneric = wayToPayBusiness.deleteWayToPay(id_way_pay);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }
}
