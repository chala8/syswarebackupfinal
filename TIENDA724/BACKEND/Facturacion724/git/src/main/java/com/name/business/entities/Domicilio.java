package com.name.business.entities;

import java.util.Date;

public class Domicilio {
    private Long ID_PERSON;
    private Long ID_THIRD_DOMICILIARIO;
    private String DOMICILIARIO;

    public Domicilio(Long ID_PERSON, Long ID_THIRD_DOMICILIARIO, String DOMICILIARIO) {
        this.ID_PERSON = ID_PERSON;
        this.ID_THIRD_DOMICILIARIO = ID_THIRD_DOMICILIARIO;
        this.DOMICILIARIO = DOMICILIARIO;
    }


    public Long getID_PERSON() {
        return ID_PERSON;
    }

    public void setID_PERSON(Long ID_PERSON) {
        this.ID_PERSON = ID_PERSON;
    }

    public Long getID_THIRD_DOMICILIARIO() {
        return ID_THIRD_DOMICILIARIO;
    }

    public void setID_THIRD_DOMICILIARIO(Long ID_THIRD_DOMICILIARIO) {
        this.ID_THIRD_DOMICILIARIO = ID_THIRD_DOMICILIARIO;
    }

    public String getDOMICILIARIO() {
        return DOMICILIARIO;
    }

    public void setDOMICILIARIO(String DOMICILIARIO) {
        this.DOMICILIARIO = DOMICILIARIO;
    }


}
