package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.*;
import com.name.business.representations.BillDTO;
import com.name.business.representations.BillDetailIDDTO;
import com.name.business.representations.DocumentDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(BillMapper.class)
public interface ReorderDAO  {

    @RegisterMapper(reorderMapper.class)
    @SqlQuery(" select tt.percent por, ps.ownbarcode ,ps.product_store_name producto,com.name linea,co.name categoria,br.brand marca,comu.name Presentacion,sum(quantity*standard_price) costo,sum(quantity) Cantidad\n" +
            "               from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps,\n" +
            "               tienda724.codes c,tienda724.product p,tienda724.category cat,tienda724.common co,tienda724.brand br,tienda724.measure_unit mu,\n" +
            "               tienda724.common comu,tienda724.category linea,tienda724.common com, tienda724.tax_tariff tt\n" +
            "               where p.id_tax = tt.id_tax_tariff and d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code and c.id_product=p.id_product and\n" +
            "                     p.id_category=cat.id_category and cat.id_common=co.id_common and c.id_brand=br.id_brand and c.id_measure_unit=mu.id_measure_unit\n" +
            "                     and mu.id_common=comu.id_common  and cat.id_category_father=linea.id_category and linea.id_common=com.id_common\n" +
            "                 and id_bill_type=1 and id_bill_state=1 and b.id_third=:id_third and ps.id_store=:id_store and cat.id_category_father is not null\n" +
            "                 and purchase_date between trunc(sysdate) - 1 and trunc(sysdate)\n" +
            "               group by tt.percent,ps.ownbarcode,ps.product_store_name,com.name,co.name,br.brand,comu.name\n ")
    List<reorderProd> getReorderData(@Bind("id_third") Long id_third,
                                     @Bind("id_store")Long id_store,
                                     @Bind("date1") Date date1,
                                     @Bind("date2") Date date2);


    @RegisterMapper(reorderMapper.class)
    @SqlQuery(" select tt.percent por, ps.ownbarcode ,ps.product_store_name producto,com.name linea,co.name categoria,br.brand marca,comu.name Presentacion,sum(quantity*standard_price) costo,sum(quantity) Cantidad\n" +
            "               from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps,\n" +
            "               tienda724.codes c,tienda724.product p,tienda724.category cat,tienda724.common co,tienda724.brand br,tienda724.measure_unit mu,\n" +
            "               tienda724.common comu,tienda724.category linea,tienda724.common com, tienda724.tax_tariff tt\n" +
            "               where p.id_tax = tt.id_tax_tariff and d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code and c.id_product=p.id_product and\n" +
            "                     p.id_category=cat.id_category and cat.id_common=co.id_common and c.id_brand=br.id_brand and c.id_measure_unit=mu.id_measure_unit\n" +
            "                     and mu.id_common=comu.id_common  and cat.id_category_father=linea.id_category and linea.id_common=com.id_common\n" +
            "                 and id_bill_type=1 and id_bill_state=1 and b.id_third=:id_third and ps.id_store=:id_store and cat.id_category_father is not null\n" +
            "                 and purchase_date between trunc(:date1) + :hours and trunc(:date2) + 1 + :hours2\n" +
            "               group by tt.percent,ps.ownbarcode,ps.product_store_name,com.name,co.name,br.brand,comu.name\n ")
    List<reorderProd> getReorderData2(@Bind("id_third") Long id_third,
                                      @Bind("id_store")Long id_store,
                                      @Bind("date1") Date date1,
                                      @Bind("date2") Date date2,
                                      @Bind("hours") Double hours,
                                      @Bind("hours2") Double hours2);



    @RegisterMapper(reorderMapper.class)
    @SqlQuery(" select tt.percent por, ps.ownbarcode ,ps.product_store_name producto,com.name linea,co.name categoria,br.brand marca,comu.name Presentacion,sum(quantity*standard_price) costo,sum(quantity) Cantidad\n" +
            "        from tienda724.tax_tariff tt, facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps,\n" +
            "        tienda724.codes c,tienda724.product p,tienda724.category cat,tienda724.common co,tienda724.brand br,tienda724.measure_unit mu,\n" +
            "        tienda724.common comu\n" +
            "        ,tienda724.category linea,tienda724.common com\n" +
            "        where p.id_tax = tt.id_tax_tariff and d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code and c.id_product=p.id_product and\n" +
            "              p.id_category=cat.id_category and cat.id_common=co.id_common and c.id_brand=br.id_brand and c.id_measure_unit=mu.id_measure_unit\n" +
            "              and mu.id_common=comu.id_common  and cat.id_category_father=linea.id_category and linea.id_common=com.id_common \n" +
            "          and id_bill_type=1 and id_bill_state=1 and b.id_third=:id_third and ps.id_store=:id_store and cat.id_category_father is not null\n" +
            "          and purchase_date between sysdate-(1/24*:hours) and sysdate\n" +
            "        group by tt.percent,ps.ownbarcode,ps.product_store_name,com.name,co.name,br.brand,comu.name\n")
    List<reorderProd> getReorderData2(@Bind("id_third") Long id_third,
                                     @Bind("id_store")Long id_store,
                                     @Bind("hours") Double hours);



    @RegisterMapper(reorderMapper2.class)
    @SqlQuery(" select ps.ownbarcode BARCODE ,ps.product_store_name producto,com.name linea,co.name categoria,br.brand marca,comu.name Presentacion\n" +
            "     ,sum(d.quantity*ps.standard_price) costo,sum(d.quantity) Cantidad\n" +
            " from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps,\n" +
            "     tienda724.codes c,tienda724.product p,tienda724.category cat,tienda724.common co,tienda724.brand br,tienda724.measure_unit mu,\n" +
            "     tienda724.common comu,tienda724.category linea,tienda724.common com\n" +
            " where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code and c.id_product=p.id_product and\n" +
            "        p.id_category=cat.id_category and cat.id_common=co.id_common and c.id_brand=br.id_brand and c.id_measure_unit=mu.id_measure_unit\n" +
            "  and mu.id_common=comu.id_common  and cat.id_category_father=linea.id_category and linea.id_common=com.id_common\n" +
            "  and b.id_bill_type=1 and b.id_bill_state=1\n" +
            "  and ps.id_store=:idstore and cat.id_category_father is not null\n" +
            "  and b.purchase_date between SYSDATE-:dias and SYSDATE\n" +
            "  and d.id_product_third in (SELECT distinct id_product_third\n" +
            "                             from FACTURACION724.bill pedido,FACTURACION724.detail_bill detalle\n" +
            "                             where pedido.id_bill=detalle.id_bill\n" +
            "                               and pedido.id_bill_type=2 and pedido.id_third_destinity=:idthird and pedido.id_store=:idstore)\n" +
            " group by ps.ownbarcode,ps.product_store_name,com.name,co.name,br.brand,comu.name\n" +
            " order by cantidad desc ")
    List<reorderProd> getReorderDataManual(@Bind("idthird") Long id_third, //tercero proveedor
                                     @Bind("idstore")Long id_store, //mi tienda de cliente
                                     @Bind("dias") Long date1);

}


