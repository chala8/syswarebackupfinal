package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.BillBusiness;
import com.name.business.businesses.PedidosBusiness;
import com.name.business.entities.*;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.EmailHelpers.SendEmail;
import static com.name.business.utils.constans.K.messages_error;

@Path("/pedidos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PedidosResource {
    private PedidosBusiness pedidosBusiness;
    private BillBusiness billBusiness;

    public PedidosResource(PedidosBusiness pedidosBusiness, BillBusiness billBusiness) {
        this.pedidosBusiness = pedidosBusiness;
        this.billBusiness = billBusiness;
        this.billBusiness.pedidosBusiness = pedidosBusiness;
    }



    @GET
    @Path("/ps")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getOwnBarCode(
            @QueryParam("id_product_store") Long id_product_store){
        Response response;

        Either<IException, String> getProducts = pedidosBusiness.getOwnBarCode(id_product_store);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/own")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPsId(
            @QueryParam("code") String code,
            @QueryParam("id_store") Long id_store){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.getPsId(code,id_store);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/tax")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTax(
            @QueryParam("idps") Long idps){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.getTax(idps);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/billtoupdate")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getBillIdByIdStore(
            @QueryParam("idstore") Long idstore,
            @QueryParam("idstoreclient") Long idstoreclient,
            @QueryParam("numpedido") String numpedido){
        Response response;

        Either<IException, BillToUpdate> getProducts = pedidosBusiness.getBillIdByIdStore(idstore,idstoreclient,numpedido);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @PUT
    @Path("/billstate")
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateBillState(
            @QueryParam("billstate") Long billstate,
            @QueryParam("billid") Long billid){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.updateBillState(billstate,billid);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @GET
    @Path("/postPedidos")
    @Timed
    @RolesAllowed({"Auth"})
    public Response insertBillv2(@QueryParam("id_store_client") Long id_store_client,
                                 @QueryParam("id_store_provider") Long id_store_provider,
                                 @QueryParam("date1") String date1,
                                 @QueryParam("date2") String date2,
                                 @QueryParam("hours") Double hours,
                                 @QueryParam("hours2") Double hours2){

        // mes,dia,año

        Response response;

        Long getResponseGeneric = pedidosBusiness.detailint(id_store_client,
                id_store_provider,date1,date2,hours,hours2);


            response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;
    }



    @GET
    @Path("/master")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPedidos(
            @QueryParam("id_store") Long id_store,
            @QueryParam("id_bill_state") Long id_bill_state,
            @QueryParam("id_bill_type") Long id_bill_type,
            @QueryParam("date1") String date1,
            @QueryParam("date2") String date2){
        Response response;

        Either<IException, List<Pedido>> getProducts = pedidosBusiness.getPedidos(id_store,id_bill_state,id_bill_type,date1,date2);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/reorderv2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getReorderV2(
            @QueryParam("list") String listStore,
            @QueryParam("date1") String date1,
            @QueryParam("date2") String date2,
            @QueryParam("id_store") Long id_store){
        Response response;
        Either<IException, List<ReorderV2>> getProducts = pedidosBusiness.getReorderV2(listStore,date1,date2,id_store);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/reorderv2new")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getReorderV2New(
            @QueryParam("list") String listStore,
            @QueryParam("listProv") String listProv,
            @QueryParam("date1") String date1,
            @QueryParam("date2") String date2,
            @QueryParam("id_store") Long id_store){
        Response response;
        Either<IException, List<ReorderV2New>> getProducts = pedidosBusiness.getReorderV2New(listStore,listProv,date1,date2,id_store);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/price")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPrice(
            @QueryParam("idps") Long idps){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.getPrice(idps);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




    @GET
    @Path("/datosClientePedido")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDatosClientePedido(
            @QueryParam("idbill") Long idps){
        Response response;

        Either<IException, ClientePedido> getProducts = pedidosBusiness.getDatosClientePedido(idps);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @GET
    @Path("/datosCliente")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDatosCliente(
            @QueryParam("idbill") Long idps){
        Response response;

        Either<IException, ClientePedido2> getProducts = pedidosBusiness.getDatosCliente(idps);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @POST
    @Path("/detalles")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDetallesPedidos(PedidoDTO priceListDTO){
        Response response;

        Either<IException, List<DetallePedido>> mailEither = pedidosBusiness.getDetallesPedidos(priceListDTO.getlistaTipos());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



    @GET
    @Path("/detalles/v2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getProviders(@QueryParam("id_store") Long id_store,
                                 @QueryParam("id_prov") Long id_prov){
        Response response;

        Either<IException, List<getProviders>> mailEither = pedidosBusiness.getProviders(id_store,id_prov);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



    @GET
    @Path("/providers")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getProviders(@QueryParam("id_store") Long id_store){
        Response response;

        Either<IException, List<getProviders>> mailEither = pedidosBusiness.getProviders2(id_store);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }


    @GET
    @Path("/fabricantes")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getFabByProv(@QueryParam("id_third") Long id_third){
        Response response;

        Either<IException, List<FabByProv>> mailEither = pedidosBusiness.getFabByProv(id_third);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }




    @POST
    @Path("/bill4prods")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getBills4Products(@QueryParam("id_ps") Long id_ps,
                                      @QueryParam("date1") String date1,
                                      @QueryParam("date2") String date2){
        Response response;

        Either<IException, List<Bill4Prod>> mailEither = pedidosBusiness.getBills4Products(id_ps, new Date(date1), new Date(date2));


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



    @POST
    @Path("/detalles2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDetallesPedidos2(PedidoDTO priceListDTO){
        Response response;

        Either<IException, List<DetallePedido>> mailEither = pedidosBusiness.getDetallesPedidos2(priceListDTO.getlistaTipos());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }




    @POST
    @Path("/detailing")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPedidoAuts(ReorderDTO reorder){
        Response response;

        Either<IException, String> mailEither = pedidosBusiness.detailintaut(reorder.getreorder(),reorder.getidstore());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



    @POST
    @Path("/create")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postBill(BillPDTO billPDTO){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.create(billPDTO.getid_third_employee(),
                billPDTO.getid_third(),
                billPDTO.getid_store(),
                billPDTO.gettotalprice(),
                billPDTO.getsubtotal(),
                billPDTO.gettax(),
                billPDTO.getid_bill_state(),
                billPDTO.getid_bill_type(),
                billPDTO.getid_third_destinity(),
                billPDTO.getid_store_cliente());

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/create2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postBill2(BillPDTO2 billPDTO){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.create2(billPDTO.getid_third_employee(),
                billPDTO.getid_third(),
                billPDTO.getid_store(),
                billPDTO.gettotalprice(),
                billPDTO.getsubtotal(),
                billPDTO.gettax(),
                billPDTO.getid_bill_state(),
                billPDTO.getid_bill_type(),
                billPDTO.getid_third_destinity(),
                billPDTO.getid_store_cliente(),
                billPDTO.getnum_documento_cliente());

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @POST
    @Path("/detail")
    @Timed
    @RolesAllowed({"Auth"})
    public Response createDetail(
            @QueryParam("id_bill") Long id_bill,
            @QueryParam("cantidad") Long cantidad,
            @QueryParam("id_ps") Long id_ps,
            @QueryParam("price") Double price,
            @QueryParam("tax") Double tax){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.createDetail(id_bill,cantidad,id_ps,price,tax);
        //System.out.println("tax");
        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @PUT
    @Path("/quantity")
    @Timed
    @RolesAllowed({"Auth"})
    public Response putInventoryBill(
            @QueryParam("quantity") Long quantity,
            @QueryParam("id_product_store") Long id_product_store){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.putInventoryBill(quantity,id_product_store);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @PUT
    @Path("/completar")
    @Timed
    @RolesAllowed({"Auth"})
    public Response completeManualBill(
            @QueryParam("idstore") Long idstore,
            @QueryParam("idbill") Long idbill){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.completeManualBill(idstore,idbill);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @POST
    @Path("/cloneBill")
    @Timed
    @RolesAllowed({"Auth"})
    public Response callProcedure2(
            @QueryParam("idbill") Long idbill){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.callProcedure2(idbill);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }
    @PUT
    @Path("/billtotal")
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateBillTotal(
            @QueryParam("total") Long total,
            @QueryParam("idbill") Long idbill){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.updateBillTotal(total,idbill);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }









    @PUT
    @Path("/billvalues")
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateBillTotal21(
            @QueryParam("idbill") Long idbill){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.updateBillTotal21(idbill);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @PUT
    @Path("/billvalues2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateBillTotal22(
            @QueryParam("idbill") Long idbill){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.updateBillTotal22(idbill);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/stores")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getStoreList(
            @QueryParam("idthird") Long idthird,
                                     @QueryParam("id_store_client") Long id_store_client){
        Response response;

        Either<IException,  List<StoresPedidos>> getProducts = pedidosBusiness.getStoreList(idthird,id_store_client);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }






    @GET
    @Path("/numdoc")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getNumDoc(
            @QueryParam("idbill") Long idbill){
        Response response;

        Either<IException,  String> getProducts = pedidosBusiness.getNumDoc(idbill);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




    @PUT
    @Path("/quantitysum")
    @Timed
    @RolesAllowed({"Auth"})
    public Response putInventoryBill2(
            @QueryParam("quantity") Long quantity,
            @QueryParam("id_product_store") Long id_product_store){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.putInventoryBill2(quantity,id_product_store);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




    @POST
    @Path("/pdf")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postPdf(PdfPedidosDTO pedido){
        Response response;

        //Either<IException, Long> getProducts = pedidosBusiness.putInventoryBill2(quantity,id_product_store);



        /*if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;*/
        Either<IException, String> mailEither =Either.right("NOPDF!!!");


        mailEither = pedidosBusiness.printPDF(pedido);
        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }










    @POST
    @Path("/pdf2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postPdf2(PdfPedidosDTO2 pedido){
        Response response;

        //Either<IException, Long> getProducts = pedidosBusiness.putInventoryBill2(quantity,id_product_store);



        /*if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;*/
        Either<IException, String> mailEither =Either.right("NOPDF!!!");


        mailEither = pedidosBusiness.printPDF2(pedido);
        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @POST
    @Path("/pdf3")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postPdf3(PdfPedidosDTO pedido){
        Response response;

        //Either<IException, Long> getProducts = pedidosBusiness.putInventoryBill2(quantity,id_product_store);



        /*if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;*/
        Either<IException, String> mailEither =Either.right("NOPDF!!!");


        mailEither = pedidosBusiness.printPDF3(pedido);
        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }




    @POST
    @Path("/addNotesBill")
    @Timed
    @RolesAllowed({"Auth"})
    public Response addNotesBill(@QueryParam("notas") String notas,
                                 @QueryParam("idbc") Long idbc,
                                 @QueryParam("numpedido") String numpedido,
                                 @QueryParam("state") String state,
                                 @QueryParam("idstore") Long idstore){
        Response response;

        //Either<IException, Long> getProducts = pedidosBusiness.putInventoryBill2(quantity,id_product_store);



        /*if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;*/
        Either<IException, Long> mailEither = pedidosBusiness.addNotesBill(notas,idbc,numpedido,state,idstore);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @POST
    @Path("/CrearPlanillaPedidos")
    @Timed
    @RolesAllowed({"Auth"})
    public Response CREAR_PLANILLA_PEDIDOS(@QueryParam("IDVEHICULO") Long IDVEHICULO,
                                @QueryParam("IDSTORE") Long IDSTORE,
                                @QueryParam("IDCIERRECAJA") Long IDCIERRECAJA){
        Response response;

        Either<IException, Long> mailEither = pedidosBusiness.CREAR_PLANILLA_PEDIDOS(IDVEHICULO,IDSTORE,IDCIERRECAJA);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }


    @POST
    @Path("/CerrarPlanillaPedidos")
    @Timed
    @RolesAllowed({"Auth"})
    public Response CERRAR_PLANILLA_PEDIDOS(@QueryParam("IDPLANILLA") Long IDPLANILLA,
                                            @QueryParam("NOTAS") String NOTAS,
                                            @QueryParam("valorbilletes") Double valorbilletes,
                                            @QueryParam("valormonedas") Double valormonedas,
                                            @QueryParam("valorotros") Double valorotros){
        Response response;

        Either<IException, Long> mailEither = pedidosBusiness.CERRAR_PLANILLA_PEDIDOS(IDPLANILLA,NOTAS,valorbilletes,valormonedas,valorotros);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



    @POST
    @Path("/sendPedidosV2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response procedureV2(@QueryParam("idbpedido") Long idbpedido,
                                @QueryParam("idstoreprov") Long idstoreprov,
                                @QueryParam("idstoreclient") Long idstoreclient,
                                @QueryParam("idplanilla") Long idplanilla){
        Response response;

        Either<IException, Long> mailEither = pedidosBusiness.procedureV2(idbpedido,idstoreprov,idstoreclient,idplanilla);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }


    @POST
    @Path("/crearPedidoV2/costo")
    @Timed
    @RolesAllowed({"Auth"})
    public Response crearPedidoV2Costo(@QueryParam("idthirdclient") Long idthirdclient,
                                  @QueryParam("idstoreclient") Long idstoreclient,
                                  @QueryParam("idthirdempclient") Long idthirdempclient,
                                  @QueryParam("idthirdprov") Long idthirdprov,
                                  @QueryParam("idstoreprov") Long idstoreprov,
                                  CostoPedidoDTO costoDTO){
        Response response;

        Either<IException, Long> mailEither = pedidosBusiness.crear_pedido_costo(idthirdclient,
                idstoreclient,
                idthirdempclient,
                idthirdprov,
                idstoreprov,
                costoDTO.getDetalles());

        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



    @POST
    @Path("/generarPedidosPorConsumo")
    @Timed
    @RolesAllowed({"Auth"})
    public Response generar_pedidos_por_consumo(@QueryParam("fechainicio") String fechainicio,
                                                @QueryParam("horainicio") Long horainicio,
                                                @QueryParam("fechafin") String fechafin,
                                                @QueryParam("horafin") Long horafin,
                                                @QueryParam("idstoreprov") Long idstoreprov){
        Response response;

        Either<IException, Long> mailEither = pedidosBusiness.generar_pedidos_por_consumo(fechainicio,
                horainicio,
                fechafin,
                horafin,
                idstoreprov);

        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }





    @POST
    @Path("/crearPedidoV2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response crearPedidoV2(@QueryParam("idthirdclient") Long idthirdclient,
                                  @QueryParam("idstoreclient") Long idstoreclient,
                                  @QueryParam("idthirdempclient") Long idthirdempclient,
                                  @QueryParam("idthirdprov") Long idthirdprov,
                                  @QueryParam("idstoreprov") Long idstoreprov,
                                  @QueryParam("detallepedido") String detallepedido){
        Response response;

        Either<IException, Long> mailEither = pedidosBusiness.crear_pedido(idthirdclient,
                idstoreclient,
                idthirdempclient,
                idthirdprov,
                idstoreprov,
                detallepedido);

        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }





    @POST
    @Path("/pickingPdf")
    @Timed
    @RolesAllowed({"Auth"})
    public Response pickingPdf(PickingConsolidadoPdfDTO pedido){
        Response response;

        response=Response.status(Response.Status.OK).entity("OK!!!").build();

        //System.out.println("THIS IS TOTAL COST: "+pedido.getcostoTotal());
        //System.out.println("THIS IS TOTAL PRICE: "+pedido.getprecioTotal());

        //System.out.println("--LETS CHECK THE LIST OF BRANDS NOW--");

        List<FabricanteDTO> brandList = pedido.getlist();

        for(FabricanteDTO brand: brandList){

            //System.out.println("WE ARE ON BRAND: "+brand.getmarca());
            //System.out.println("ITS TOTAL COST WAS: "+brand.getcostoTotal());
            //System.out.println("ITS TOTAL PRICE WAS: "+brand.getprecioTotal());

            //System.out.println("**LETS CHECK THIS BRANDS DETAILS**");

            List<DetallePedidoDTO> details = brand.getdetalles();

            for(DetallePedidoDTO detail: details) {

                /*System.out.println(detail.getPRODUCTO()+", "+
                                    detail.getCATEGORIA()+", "+
                                    detail.getLINEA()+", "+
                                    detail.getFABRICANTE()+", "+
                                    detail.getMARCA()+", "+
                                    detail.getPRESENTACION()+", "+
                                    detail.getCANTIDAD()+", "+
                                    detail.getCOSTO()+", "+
                                    detail.getCOSTOTOTAL()+", "+
                                    detail.getID_PRODUCT_THIRD());*/

            }

            //System.out.println("**THIS IS THE END OF THE BRAND DETAIL CHECK**");

        }

        //System.out.println("--THIS IS THE END OF THE WHOLE THING CHECK--");
        Either<IException, String> mailEither =Either.right("NOPDF!!!");


        mailEither = pedidosBusiness.printPickingPDF(pedido);
        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }


        return response;
    }




    @POST
    @Path("/pickingExcel")
    @Timed
    @RolesAllowed({"Auth"})
    public Response pickingExcel(PickingConsolidadoPdfDTO pedido){
        Response response;

        response=Response.status(Response.Status.OK).entity("OK!!!").build();

        //System.out.println("THIS IS TOTAL COST: "+pedido.getcostoTotal());
        //System.out.println("THIS IS TOTAL PRICE: "+pedido.getprecioTotal());

        //System.out.println("--LETS CHECK THE LIST OF BRANDS NOW--");

        List<FabricanteDTO> brandList = pedido.getlist();

        for(FabricanteDTO brand: brandList){

            //System.out.println("WE ARE ON BRAND: "+brand.getmarca());
            //System.out.println("ITS TOTAL COST WAS: "+brand.getcostoTotal());
            //System.out.println("ITS TOTAL PRICE WAS: "+brand.getprecioTotal());

            //System.out.println("**LETS CHECK THIS BRANDS DETAILS**");

            List<DetallePedidoDTO> details = brand.getdetalles();

            for(DetallePedidoDTO detail: details) {

                /*System.out.println(detail.getPRODUCTO()+", "+
                        detail.getCATEGORIA()+", "+
                        detail.getLINEA()+", "+
                        detail.getFABRICANTE()+", "+
                        detail.getMARCA()+", "+
                        detail.getPRESENTACION()+", "+
                        detail.getCANTIDAD()+", "+
                        detail.getCOSTO()+", "+
                        detail.getCOSTOTOTAL()+", "+
                        detail.getID_PRODUCT_THIRD());*/

            }

            //System.out.println("**THIS IS THE END OF THE BRAND DETAIL CHECK**");

        }

        //System.out.println("--THIS IS THE END OF THE WHOLE THING CHECK--");
        Either<IException, String> mailEither =Either.right("NOPDF!!!");


        mailEither = pedidosBusiness.postExcel(pedido);
        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }


        return response;
    }





    @GET
    @Path("/quantity")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getQuantity(@QueryParam("id_product_store") Long id_product_store){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.getQuantity(id_product_store);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @GET
    @Path("/InventoryPedidoDistriJass")
    @Timed
    @RolesAllowed({"Auth","PublicAPI"})
    public Response getInventoryPedidoDistriJass(@QueryParam("id_store") Long id_ps,@QueryParam("id_store_prov") Long id_ps_prov){
        Response response;

        Either<IException, List<InventoryPedidoDetail>> getProducts = pedidosBusiness.getInventoryPedidoDistriJass(id_ps,id_ps_prov);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/inventoryPedido")
    @Timed
    @RolesAllowed({"Auth","PublicAPI"})
    public Response getInventoryPedido(@QueryParam("id_store") Long id_ps,@QueryParam("id_store_prov") Long id_ps_prov){
        Response response;

        Either<IException, List<InventoryPedidoDetailDenlinea>> getProducts = pedidosBusiness.getInventoryPedido(id_ps,id_ps_prov);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/inventoryPedido/v2")
    @Timed
    @RolesAllowed({"Auth","PublicAPI"})
    public Response getInventoryPedido2(@QueryParam("id_store") Long id_ps,@QueryParam("id_store_prov") Long id_ps_prov){
        Response response;

        Either<IException, List<InventoryPedidoDetail2>> getProducts = pedidosBusiness.getInventoryPedido2(id_ps,id_ps_prov);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @GET
    @Path("/pedidoManual")
    @Timed
    @RolesAllowed({"Auth"})
    public Response pedidoManual(@QueryParam("id_store") Long id_ps,@QueryParam("id_store_prov") Long id_ps_prov){
        Response response;

        Either<IException, List<PedidosManuales>> getProducts = pedidosBusiness.pedidoManual(id_ps,id_ps_prov);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }








    @POST
    @Path("/procedure2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response procedure2(
                               @QueryParam("idvehiculo") Long idvehiculo,
                               @QueryParam("idbventa") Long idbventa,
                               @QueryParam("idbcompra") Long idbcompra,
                               @QueryParam("idbremision") Long idbremision,
                               @QueryParam("idstorecliente") Long idstorecliente,
                               @QueryParam("idstoreproveedor") Long idstoreproveedor,
                               @QueryParam("idbpedidoproveedor") Long idbpedidoproveedor){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.procedure2(
                idvehiculo,
                idbventa,
                idbcompra,
                idbremision,
                idstorecliente,
                idstoreproveedor,
                idbpedidoproveedor);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/procedimiento")
    @Timed
    @RolesAllowed({"Auth"})
    public Response procedimiento(@QueryParam("idbventa") Long idbventa,@QueryParam("idcompra") Long idcompra,@QueryParam("idremision") Long idremision){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.procedimiento(idbventa,idcompra,idremision);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




    @POST
    @Path("/actualizarEstadoPedidoApp")
    @Timed
    @RolesAllowed({"Auth"})
    public Response ACTUALIZAR_ESTDO_PEDIDO_APP(
            @QueryParam("idbillpedidoprov") Long idbillpedidoprov,
            @QueryParam("idbillstateclient") Long idbillstateclient,
            @QueryParam("idbillstateprov") Long idbillstateprov,
            @QueryParam("notas") String notas,
            @QueryParam("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @QueryParam("IDTHIRDUSER") Long IDTHIRDUSER,
            @QueryParam("ACTOR") String ACTOR){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.ACTUALIZAR_ESTDO_PEDIDO_APP(idbillpedidoprov,
                idbillstateclient,
                idbillstateprov,
                notas,ESTADOORIGENPROV,IDTHIRDUSER,ACTOR);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




    @POST
    @Path("/crearPedidoApp")
    @Timed
    @RolesAllowed({"Auth"})
    public Response CREAR_PEDIDO_APP(
            @QueryParam("idstoreclient") Long idstoreclient,
            @QueryParam("idthirduseraapp") Long idthirduseraapp,
            @QueryParam("idstoreprov") Long idstoreprov,
            @QueryParam("detallepedido") String detallepedido,
            @QueryParam("descuento") Long descuento,
            @QueryParam("idpaymentmethod") Long idpaymentmethod,
            @QueryParam("idapp") Long idapp){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.CREAR_PEDIDO_APP(
                idstoreclient,
                idthirduseraapp,
                idstoreprov,
                detallepedido,
                descuento,
                idpaymentmethod,
                idapp);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/crearPedidoAppEmp")
    @Timed
    @RolesAllowed({"Auth"})
    public Response CREAR_PEDIDO_APP_EMP(
            @QueryParam("id_employee") Long id_employee,
            @QueryParam("idstoreclient") Long idstoreclient,
            @QueryParam("idthirduseraapp") Long idthirduseraapp,
            @QueryParam("idstoreprov") Long idstoreprov,
            @QueryParam("detallepedido") String detallepedido,
            @QueryParam("descuento") Long descuento,
            @QueryParam("idpaymentmethod") Long idpaymentmethod,
            @QueryParam("idapp") Long idapp){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.CREAR_PEDIDO_APP_EMP(
                id_employee,
                idstoreclient,
                idthirduseraapp,
                idstoreprov,
                detallepedido,
                descuento,
                idpaymentmethod,
                idapp);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/contadorAppInstaladas")
    @Timed
    @RolesAllowed({"Auth","PublicAPI"})
    public Response CONTADOR_APP_INSTALADAS(
            @QueryParam("deviceid") String deviceid,
            @QueryParam("idapp") Long idapp,
            @QueryParam("appversion") Long appversion){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.CONTADOR_APP_INSTALADAS(deviceid,idapp,
                appversion);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @POST
    @Path("/postExcelDomicilio")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postExcelDomicilio(
            @QueryParam("date1") String date1,
            @QueryParam("date2") String date2,
            @QueryParam("id_store") Long idstore){
        Response response;

        Either<IException, String> getProducts = pedidosBusiness.postExcelDomicilio(date1,date2,idstore);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @POST
    @Path("/actualizarPedidoNovedadApp")
    @Timed
    @RolesAllowed({"Auth"})
    public Response ACTUALIZAR_PEDIDO_NOVEDAD_APP(
            @QueryParam("idbillpedidoprov") Long idbillpedidoprov,
            @QueryParam("nuevosdetalles") String nuevosdetalles,
            @QueryParam("notas") String notas,
            @QueryParam("idpaymentmethod") Long idpaymentmethod,
            @QueryParam("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @QueryParam("IDTHIRDUSER") Long IDTHIRDUSER,
            @QueryParam("ACTOR") String ACTOR){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.ACTUALIZAR_PEDIDO_NOVEDAD_APP(
                idbillpedidoprov,
                nuevosdetalles,
                notas,
                idpaymentmethod,
                ESTADOORIGENPROV,
                IDTHIRDUSER,
                ACTOR);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/getPlanillas")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPlanillas(
            @QueryParam("id_store") Long id_store, @QueryParam("status") String status){
        Response response;

        Either<IException, List<PlanillaB>> getProducts = pedidosBusiness.getPlanillas(id_store, status);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @GET
    @Path("/getPlanillas/cierreCaja")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPlanillas2(
            @QueryParam("id_cierre_caja") Long id_cierre_caja){
        Response response;

        Either<IException, List<PlanillaB>> getProducts = pedidosBusiness.getPlanillas2(id_cierre_caja);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/getPlanillasData/cierreCaja")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPlanillaData2(
            @QueryParam("id_cierre_caja") Long id_cierre_caja,@QueryParam("status") String status){
        Response response;

        Either<IException, List<PlanillaB2>> getProducts = pedidosBusiness.getPlanillaData2(id_cierre_caja,status);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/getThirDataPedidos")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getThirDataPedidos(
            @QueryParam("id_store") Long id_store){
        Response response;

        Either<IException, List<ThirdDataPedidos>> getProducts = pedidosBusiness.getThirDataPedidos(id_store);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




    @GET
    @Path("/getDatosPlanilla")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDatosPlanilla(
            @QueryParam("id_planilla") Long id_planilla){
        Response response;

        Either<IException, List<PlanillaData>> getProducts = pedidosBusiness.getDatosPlanilla(id_planilla);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/getDatosDomiciliario")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDatosDomiciliario(
            @QueryParam("id_store") Long id_store,
            @QueryParam("status") String status){
        Response response;

        Either<IException, List<DatosDomiciliario>> getProducts = pedidosBusiness.getDatosDomiciliario(id_store,status);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/getPedidos/third")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPedidosVerThird(
            @QueryParam("id_bill_state") String id_bill_state,
            @QueryParam("idthirdclient") Long idthirdclient,
            @QueryParam("id_bill_type") Long id_bill_type,
            @QueryParam("idapp") Long idapp){
        Response response;

        String[] SplittedList = id_bill_state.split("\\|");
        List<Long> CastedList = new ArrayList<>();
        for(int n = 0;n<SplittedList.length;n++){
            CastedList.add(Long.parseLong(SplittedList[n]));
        }

        Either<IException, List<Pedido2>> getProducts = pedidosBusiness.getPedidosVerThird(CastedList,idthirdclient,id_bill_type,idapp);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/getIdBillPlanilla")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getIdBillPlanilla(
            @QueryParam("id_store") Long id_store,
            @QueryParam("id_planilla") Long id_planilla){
        Response response;


        Either<IException, List<Long>> getProducts = pedidosBusiness.getIdBillPlanilla(id_store,id_planilla);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/getDetailForPedido")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDetailForPedido(
            @QueryParam("id_bill") Long id_bill){
        Response response;


        Either<IException, List<DetailForPedido>> getProducts = pedidosBusiness.getDetailForPedido(id_bill);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/getKardex")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getKardex(
            @QueryParam("id_store") Long id_store,
            @QueryParam("ownbarcode") String ownbarcode,
            @QueryParam("date1") String date1,
            @QueryParam("date2") String date2){
        Response response;


        Either<IException, List<Kardex>> getProducts = pedidosBusiness.getKardex(id_store,ownbarcode,date1,date2);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/enviarPedidoApp")
    @Timed
    @RolesAllowed({"Auth"})
    public Response ENVIAR_PEDIDO_APP(
            @QueryParam("idbillpedidoprov") Long idbillpedidoprov,
            @QueryParam("idbillventa") Long idbillventa,
            @QueryParam("notas") String notas,
            @QueryParam("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @QueryParam("IDTHIRDUSER") Long IDTHIRDUSER,
            @QueryParam("ACTOR") String ACTOR){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.ENVIAR_PEDIDO_APP(
                idbillpedidoprov,
                idbillventa,
                notas,
                ESTADOORIGENPROV,
                IDTHIRDUSER,
                ACTOR);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/enviarPedidosFacturados")
    @Timed
    @RolesAllowed({"Auth"})
    public Response ENVIAR_PEDIDOS_FACTURADOS(
            @QueryParam("idplanilla") Long IDPLANILLA,
            @QueryParam("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @QueryParam("IDTHIRDUSER") Long IDTHIRDUSER,
            @QueryParam("ACTOR") String ACTOR){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.ENVIAR_PEDIDOS_FACTURADOS(
                IDPLANILLA,
                ESTADOORIGENPROV,
                IDTHIRDUSER,
                ACTOR);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @POST
    @Path("/crearMovimientoCaja")
    @Timed
    @RolesAllowed({"Auth"})
    public Response CREAR_MOVIMIENTO_CAJA(
            @QueryParam("IDPLANILLA") Long IDPLANILLA,
            @QueryParam("VALOR") Long VALOR,
            @QueryParam("NATURALEZA") String NATURALEZA,
            @QueryParam("NOTAS") String NOTAS){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.CREAR_MOVIMIENTO_CAJA(IDPLANILLA,
                VALOR,
                NATURALEZA,
                NOTAS);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @POST
    @Path("/cerrarPlanilla")
    @Timed
    @RolesAllowed({"Auth"})
    public Response CERRAR_PLANILLA(
            @QueryParam("IDPLANILLA") Long IDPLANILLA,
            @QueryParam("NOTAS") String NOTAS,
            @QueryParam("valorbilletes") Long valorbilletes,
            @QueryParam("valormonedas") String valormonedas,
            @QueryParam("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @QueryParam("IDTHIRDUSER") Long IDTHIRDUSER,
            @QueryParam("ACTOR") String ACTOR,
            @QueryParam("valorotros") String valorotros){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.CERRAR_PLANILLA(IDPLANILLA,
                NOTAS,
                valorbilletes,
                valormonedas,
                ESTADOORIGENPROV,
                IDTHIRDUSER,
                ACTOR,valorotros
                );

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/cerrarPlanillaNovedad")
    @Timed
    @RolesAllowed({"Auth"})
    public Response CERRAR_PLANILLA_NOVEDAD(
            @QueryParam("IDPLANILLA") Long IDPLANILLA,
            @QueryParam("NOTAS") String NOTAS,
            @QueryParam("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @QueryParam("IDTHIRDUSER") Long IDTHIRDUSER,
            @QueryParam("ACTOR") String ACTOR){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.CERRAR_PLANILLA_NOVEDAD(IDPLANILLA,
                NOTAS,
                ESTADOORIGENPROV,
                IDTHIRDUSER,
                ACTOR);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




    @POST
    @Path("/cierreCajaPlanilla")
    @Timed
    @RolesAllowed({"Auth"})
    public Response CIERRE_CAJA_PLANILLA(
            @QueryParam("IDCIERRECAJA") Long IDCIERRECAJA){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.CIERRE_CAJA_PLANILLA(IDCIERRECAJA);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/abrirCajaPlanilla")
    @Timed
    @RolesAllowed({"Auth"})
    public Response ABRIR_CAJA_PLANILLA(
            @QueryParam("IDCIERRECAJA") Long IDCIERRECAJA){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.ABRIR_CAJA_PLANILLA(IDCIERRECAJA);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/cancelarPedidoApp")
    @Timed
    @RolesAllowed({"Auth"})
    public Response CANCELAR_PEDIDO_APP(
            @QueryParam("idbillpedidoprov") Long idbillpedidoprov,
            @QueryParam("notes") String notes,
            @QueryParam("idcaja") Long idcaja,
            @QueryParam("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @QueryParam("IDTHIRDUSER") Long IDTHIRDUSER,
            @QueryParam("ACTOR") String ACTOR){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.CANCELAR_PEDIDO_APP(idbillpedidoprov,notes,idcaja,ESTADOORIGENPROV,IDTHIRDUSER,ACTOR);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/copiaProducto")
    @Timed
    @RolesAllowed({"Auth"})
    public Response copia_producto(
            @QueryParam("ownbarcode") String ownbarcode,
            @QueryParam("idstore") Long idstore,
            @QueryParam("idstoredestiny") Long idstoredestiny){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.copia_producto(ownbarcode, idstore, idstoredestiny);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @GET
    @Path("/GetStoreProvs")
    @Timed
    @RolesAllowed({"Auth","PublicAPI"})
    public Response GetStoreProvs(
            @QueryParam("id_store") Long id_store){
        Response response;

        Either<IException, List<DatosProvider>> closestProv = pedidosBusiness.GetStoreProvs(id_store);

        if (closestProv.isRight()){
            //System.out.println(closestProv.right().value());
            response=Response.status(Response.Status.OK).entity(closestProv.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(closestProv);
        }
        return response;
    }

    @GET
    @Path("/CheckClosestProv")
    @Timed
    @RolesAllowed({"Auth"})
    public Response CheckClosestProv(
            @QueryParam("id_store") Long id_store,
            @QueryParam("lat") Double lat,
            @QueryParam("lng") Double lng){
        Response response;

        Either<IException, String> closestProv = pedidosBusiness.CheckClosestProv(
                id_store,
                lat,
                lng);

        if (closestProv.isRight()){
            //System.out.println(closestProv.right().value());
            response=Response.status(Response.Status.OK).entity(closestProv.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(closestProv);
        }
        return response;
    }

    @POST
    @Path("/SendEmail")
    @Timed
    @RolesAllowed({"Auth"})
    public Response SendEmailRequest(SendEmailBody pedido){
        Response response;
        try{
            if(pedido.getDestinatario() == null || pedido.getDestinatario().length == 0){throw new Error();}
            List<String> ListadoArchivos = new ArrayList<>();
            SendEmail(pedido.getDestinatario(),pedido.getAsunto(),pedido.getCuerpoCorreo(),ListadoArchivos);
            response=Response.status(Response.Status.OK).build();
        }catch (Exception e){
            //e.printStackTrace();
            //System.out.println("NO SE PUDO ENVIAR CORREO");
            response= ExceptionResponse.createErrorResponse(Either.left(new TechnicalException(messages_error(e.toString()))));
        }
        return response;
    }



    @POST
    @Path("/cancelarPedidoTrad")
    @Timed
    @RolesAllowed({"Auth"})
    public Response cancelar_pedido_trad(
            @QueryParam("IDBILL") Long IDBILL){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.cancelar_pedido_trad(IDBILL);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @POST
    @Path("/eliminarFaltantes")
    @Timed
    @RolesAllowed({"Auth"})
    public Response eliminar_faltantes(
            @QueryParam("idstore") Long idstore){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.eliminar_faltantes(idstore);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @POST
    @Path("/actualizarPedidoTrad")
    @Timed
    @RolesAllowed({"Auth"})
    public Response actualizar_pedido_trad(
            @QueryParam("idbillpedidocliente") Long idbillpedidocliente,
            @QueryParam("idpaymentmethod") Long idpaymentmethod,
            CostoPedidoDTO costoDTO){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.actualizar_pedido_trad( idbillpedidocliente,
                costoDTO.getDetalles(),
                idpaymentmethod);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




    @POST
    @Path("/crearPedidoAppOpciones")
    @Timed
    @RolesAllowed({"Auth"})
    public Response CREAR_PEDIDO_APP_OPCIONES(
            @QueryParam("idstoreclient") Long idstoreclient,
            @QueryParam("idthirduseraapp") Long idthirduseraapp,
            @QueryParam("idstoreprov") Long idstoreprov,
            @QueryParam("detallepedido") String detallepedido,
            @QueryParam("descuento") Long descuento,
            @QueryParam("idpaymentmethod") Long idpaymentmethod,
            @QueryParam("idapp") Long idapp,
                @QueryParam("detallepedidomesa") String detallepedidomesa){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.CREAR_PEDIDO_APP_OPCIONES(
                idstoreclient,
                idthirduseraapp,
                idstoreprov,
                detallepedido,
                descuento,
                idpaymentmethod,
                idapp,
                detallepedidomesa);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

}