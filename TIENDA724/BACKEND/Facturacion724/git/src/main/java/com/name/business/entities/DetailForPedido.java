package com.name.business.entities;

public class DetailForPedido {

    private String FECHA_EVENTO;
    private String ACTOR;
    private Long ID_THIRD_USER;
    private Long IDESTADOORIGEN;
    private Long IDESTADODESTINO;
    private String NOTAS;

    public DetailForPedido(String FECHA_EVENTO,
             String ACTOR,
             Long ID_THIRD_USER,
             Long IDESTADOORIGEN,
             Long IDESTADODESTINO,
             String NOTAS) {
        this.FECHA_EVENTO = FECHA_EVENTO;
        this.ACTOR = ACTOR;
        this.ID_THIRD_USER =ID_THIRD_USER;
        this.IDESTADOORIGEN = IDESTADOORIGEN;
        this.IDESTADODESTINO = IDESTADODESTINO;
        this.NOTAS = NOTAS;
    }



    public String getFECHA_EVENTO() {
        return FECHA_EVENTO;
    }

    public void setFECHA_EVENTO(String FECHA_EVENTO) {
        this.FECHA_EVENTO = FECHA_EVENTO;
    }

    public String getACTOR() {
        return ACTOR;
    }

    public void setACTOR(String ACTOR) {
        this.ACTOR = ACTOR;
    }

    public Long getID_THIRD_USER() {
        return ID_THIRD_USER;
    }

    public void setID_THIRD_USER(Long ID_THIRD_USER) {
        this.ID_THIRD_USER = ID_THIRD_USER;
    }

    public Long getIDESTADOORIGEN() {
        return IDESTADOORIGEN;
    }

    public void setIDESTADOORIGEN(Long IDESTADOORIGEN) {
        this.IDESTADOORIGEN = IDESTADOORIGEN;
    }

    public Long getIDESTADODESTINO() {
        return IDESTADODESTINO;
    }

    public void setIDESTADODESTINO(Long IDESTADODESTINO) {
        this.IDESTADODESTINO = IDESTADODESTINO;
    }

    public String getNOTAS() {
        return NOTAS;
    }

    public void setNOTAS(String NOTAS) {
        this.NOTAS = NOTAS;
    }


}
