package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.*;
import com.name.business.representations.BillDTO;
import com.name.business.representations.BillDetailIDDTO;
import com.name.business.representations.DocumentDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.unstable.BindIn;

import java.util.Date;
import java.util.List;

@UseStringTemplate3StatementLocator
@RegisterMapper(BillMapper.class)
public interface BillDAO  {

    @RegisterMapper(VendorMapper.class)
    @SqlQuery(" select t.id_third,fullname vendedor \n" +
            " from tienda724.store_vendedor sv,tercero724.third t,TERCERO724.COMMON_BASICINFO cbi \n" +
            " where sv.ID_THIRD_VENDEDOR=t.ID_THIRD and t.ID_COMMON_BASICINFO=cbi.ID_COMMON_BASICINFO \n" +
            " and sv.id_store=:idstore ")
    List<Vendor> getVendorList(@Bind("idstore") Long idstore);



    @RegisterMapper(detallePdfMapper.class)
    @SqlQuery("select ownbarcode codigo,null codigoprov,\n" +
            "b.id_store,\n" +
            "product_store_name producto,quantity cantidad,price valorcompra,null descuento,\n" +
            "db.tax iva,quantity*price total,quantity*price*(1 + db.tax/100)  totalconiva  \n" +
            "from facturacion724.bill b,facturacion724.detail_bill db,tienda724.product_store ps\n" +
            "where b.id_bill=db.id_bill and db.id_product_third=ps.id_product_store\n" +
            "and b.id_bill=:idbill")
    List<detallePdf> getDetallesPdf(@Bind("idbill") Long idbill);


    @RegisterMapper(MargenDataMapper.class)
    @SqlQuery("select price,standard_price\n" +
            "from tienda724.price pr,tienda724.product_store p \n" +
            "where pr.id_product_store=p.id_product_store and p.ownbarcode=:code and p.id_store=:id_store \n" +
            "and pr.PRICE_DESCRIPTION='General'")
    MargenData getMargenData(@Bind("code") String code,
                                   @Bind("id_store") String id_store);



    @RegisterMapper(ProductosPedidosMesaMapper.class)
    @SqlQuery("select product_store_name producto,id_detalle_detail_bill,ddb.id_bill,ddb.id_product_store,ddb.id_bill_state,ddb.notas,fecha_evento,\n" +
            "                                                            decode(ddb.id_bill_state,1500,'PENDIENTE',1501,'EN PROCESO',1502,'ENTREGADO A MESERO',1503,'ENTREGADO EN MESA',1599,'CANCELADO',1504,'MESA CERRADA') ESTADO,\n" +
            "                                                          decode(ddb.id_bill_state,1500,'ROJO',1501,'AMARILLO',1502,'NARANJA',1503,'AZUL',1599,'NEGRO',1504,'VIOLETA') COLOR\n" +
            "                                                            ,ps.ownbarcode\n" +
            "                                     ,b.totalprice - facturacion724.get_total_prods_cancelados(ddb.id_bill) totalprice\n" +
            "                                    ,c.img\n" +
            "                                                          from facturacion724.detalle_detail_bill ddb,tienda724.product_store ps,\n" +
            "                                                          facturacion724.bill b,tienda724.codes c\n" +
            "                                                            where ddb.id_bill=b.id_bill and ddb.id_product_store=ps.id_product_store\n" +
            "                                                            and ps.id_code=c.id_code\n" +
            "                                                            and ddb.id_bill=:id_bill order by FECHA_EVENTO")
    List<ProductosPedidosMesa> getProductosPedidoMesa(@Bind("id_bill") Long id_bill);




    @RegisterMapper(ProductosPedidosMesaMapper.class)
    @SqlQuery("select product_store_name producto,id_detalle_detail_bill,ddb.id_bill,ddb.id_product_store,ddb.id_bill_state,ddb.notas,fecha_evento,\n" +
            "                                                            decode(ddb.id_bill_state,1500,'PENDIENTE',1501,'EN PROCESO',1502,'ENTREGADO A MESERO',1503,'ENTREGADO EN MESA',1599,'CANCELADO',1504,'MESA CERRADA') ESTADO,\n" +
            "                                                          decode(ddb.id_bill_state,1500,'ROJO',1501,'AMARILLO',1502,'NARANJA',1503,'AZUL',1599,'NEGRO',1504,'VIOLETA') COLOR\n" +
            "                                                            ,ps.ownbarcode\n" +
            "                                     ,b.totalprice - facturacion724.get_total_prods_cancelados(ddb.id_bill) totalprice\n" +
            "                                     ,c.img\n" +
            "                                                          from facturacion724.detalle_detail_bill ddb,tienda724.product_store ps,\n" +
            "                                                          facturacion724.bill b,tienda724.codes c\n" +
            "            ,tienda724.PS_ADR PSADR, tienda724.ADR_EMPLEADO ADREMP\n" +
            "                                                            where ddb.id_bill=b.id_bill and ddb.id_product_store=ps.id_product_store\n" +
            "                                                            and ddb.id_bill=:id_bill\n" +
            "             and DDB.ID_PRODUCT_STORE=PS.ID_PRODUCT_STORE AND PS.ID_PRODUCT_STORE=PSADR.ID_PRODUCT_STORE\n" +
            "             and ps.id_code=c.id_code\n" +
            "             AND PSADR.ID_ADR=ADREMP.ID_ADR\n" +
            "             AND ADREMP.ID_THIRD=:idthird                  \n" +
            "            order by FECHA_EVENTO")
    List<ProductosPedidosMesa> getProductosPedidoMesaADR(@Bind("id_bill") Long id_bill,
                                                         @Bind("idthird") Long idthird);



    @RegisterMapper(PedidosMesaMapper.class)
    @SqlQuery("select distinct id_bill,purchase_date,m.id_mesa,m.DESCRIPTION mesa,id_third_domiciliario,\n" +
            "                                                totalprice - facturacion724.get_total_prods_cancelados(b.id_bill) totalprice,cbi.fullname mesero,idapp,\n" +
            "                                                                                        (select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill and ID_BILL_STATE=1501) AMARILLO,\n" +
            "                                                                                        (select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill and ID_BILL_STATE=1502) NARANJA,\n" +
            "                                                                                        (select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill and ID_BILL_STATE=1503) AZUL,\n" +
            "                                                                                        (select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill and ID_BILL_STATE=1500) ROJO,\n" +
            "                                                                                        (select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill and ID_BILL_STATE=1504) VIOLETA,\n" +
            "                                                                                        (select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill) Total_productos,mesa_number,m.id_store\n" +
            "                                                                       ,cbicliente.fullname cliente\n" +
            "                                                                       ,(select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill and ID_BILL_STATE=1599) NEGRO\n" +
            "                                             ,(select facturacion724.get_total_prods_cancelados(b.id_bill) from dual) cancelados                          \n" +
            "                                                                       from facturacion724.bill b, TIENDA724.MESA m,tercero724.third t,tercero724.common_basicinfo cbi\n" +
            "                                                                     ,tercero724.third tcliente,tercero724.common_basicinfo cbicliente\n" +
            "                                                                       where b.id_mesa=m.id_mesa and b.id_third_domiciliario=t.id_third\n" +
            "                                                                          and t.ID_COMMON_BASICINFO=cbi.ID_COMMON_BASICINFO\n" +
            "                                                                         and b.id_third_destinity=tcliente.id_third\n" +
            "                                                                          and tcliente.ID_COMMON_BASICINFO=cbicliente.ID_COMMON_BASICINFO                \n" +
            "                                                                         and b.id_store in (<storeList>)\n" +
            "                                                                      and id_bill_type=:id_bill_type and b.id_bill_state=:id_bill_state\n" +
            "                                                                      UNION\n" +
            "                                                                        select null id_bill,null purchase_date,m.id_mesa,description mesa, null id_third_domiciliario,\n" +
            "                                                                               null totalprice,null mesero,null idapp,null amarillo,null naranaja,null azul,null rojo,null violeta,null total_productos,mesa_number,m.id_store\n" +
            "                                                                        ,null cliente,null negro,null cancelados\n" +
            "                                                                        from tienda724.mesa m\n" +
            "                                                                        where  m.id_store in (<storeList>)\n" +
            "                                                                      and estado_mesa='DISPONIBLE'\n" +
            "                                                                       order by id_bill desc nulls last,mesa_number")
    List<PedidosMesa> getPedidosMesa(@BindIn("storeList") List<String> storeList,
                                     @Bind("id_bill_type") Long id_bill_type,
                                     @Bind("id_bill_state") Long id_bill_state);





    @RegisterMapper(PedidosMesaMapper.class)
    @SqlQuery("select distinct b.id_bill,purchase_date,m.id_mesa,m.DESCRIPTION mesa,id_third_domiciliario,\n" +
            "totalprice - facturacion724.get_total_prods_cancelados(b.id_bill) totalprice,cbi.fullname mesero,idapp,\n" +
            "(select count(*) \n" +
            "from facturacion724.detalle_detail_bill ddb\n" +
            ",tienda724.PS_ADR PSADR, tienda724.ADR_EMPLEADO ADREMP\n" +
            "where ddb.id_bill=b.id_bill \n" +
            "and ddb.id_product_store=psadr.id_product_store and PSADR.ID_ADR=ADREMP.ID_ADR\n" +
            "and adremp.id_third=:idt\n" +
            "and ID_BILL_STATE=1501\n" +
            ") AMARILLO,\n" +
            "(select count(*) from facturacion724.detalle_detail_bill ddb\n" +
            ",tienda724.PS_ADR PSADR, tienda724.ADR_EMPLEADO ADREMP\n" +
            "where ddb.id_bill=b.id_bill \n" +
            "and ddb.id_product_store=psadr.id_product_store and PSADR.ID_ADR=ADREMP.ID_ADR\n" +
            "and adremp.id_third=:idt\n" +
            "and ID_BILL_STATE=1502) NARANJA,\n" +
            "(select count(*) from facturacion724.detalle_detail_bill ddb\n" +
            ",tienda724.PS_ADR PSADR, tienda724.ADR_EMPLEADO ADREMP\n" +
            "where ddb.id_bill=b.id_bill \n" +
            "and ddb.id_product_store=psadr.id_product_store and PSADR.ID_ADR=ADREMP.ID_ADR\n" +
            "and adremp.id_third=:idt\n" +
            "and ID_BILL_STATE=1503) AZUL,\n" +
            "(select count(*) from facturacion724.detalle_detail_bill ddb\n" +
            ",tienda724.PS_ADR PSADR, tienda724.ADR_EMPLEADO ADREMP\n" +
            "where ddb.id_bill=b.id_bill \n" +
            "and ddb.id_product_store=psadr.id_product_store and PSADR.ID_ADR=ADREMP.ID_ADR\n" +
            "and adremp.id_third=:idt\n" +
            "and ID_BILL_STATE=1500) ROJO,\n" +
            "(select count(*) from facturacion724.detalle_detail_bill ddb\n" +
            ",tienda724.PS_ADR PSADR, tienda724.ADR_EMPLEADO ADREMP\n" +
            "where ddb.id_bill=b.id_bill \n" +
            "and ddb.id_product_store=psadr.id_product_store and PSADR.ID_ADR=ADREMP.ID_ADR\n" +
            "and adremp.id_third=:idt\n" +
            "and ID_BILL_STATE=1504) VIOLETA,\n" +
            "(select count(*) from facturacion724.detalle_detail_bill ddb\n" +
            ",tienda724.PS_ADR PSADR, tienda724.ADR_EMPLEADO ADREMP\n" +
            "where ddb.id_bill=b.id_bill \n" +
            "and ddb.id_product_store=psadr.id_product_store and PSADR.ID_ADR=ADREMP.ID_ADR\n" +
            "and adremp.id_third=:idt\n" +
            ") Total_productos,mesa_number,m.id_store\n" +
            ",cbicliente.fullname cliente,\n" +
            "(select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill and ID_BILL_STATE=1599) NEGRO\n" +
            ",(select facturacion724.get_total_prods_cancelados(b.id_bill) from dual) cancelados\n" +
            "from facturacion724.bill b, TIENDA724.MESA m,tercero724.third t,tercero724.common_basicinfo cbi\n" +
            ",tercero724.third tcliente,tercero724.common_basicinfo cbicliente\n" +
            ", facturacion724.DETAIL_BILL DB, tienda724.PRODUCT_STORE PS\n" +
            ",tienda724.PS_ADR PSADR, tienda724.ADR_EMPLEADO ADREMP\n" +
            "where b.id_mesa=m.id_mesa and b.id_third_domiciliario=t.id_third\n" +
            " and t.ID_COMMON_BASICINFO=cbi.ID_COMMON_BASICINFO and b.id_third_destinity=tcliente.id_third and tcliente.ID_COMMON_BASICINFO=cbicliente.ID_COMMON_BASICINFO                 \n" +
            " and b.id_bill=db.id_bill\n" +
            " and DB.ID_PRODUCT_THIRD=PS.ID_PRODUCT_STORE AND PS.ID_PRODUCT_STORE=PSADR.ID_PRODUCT_STORE\n" +
            " AND PSADR.ID_ADR=ADREMP.ID_ADR\n" +
            " AND ADREMP.ID_THIRD=:idt\n" +
            " and b.id_store in (<storeList>)\n" +
            " and b.id_bill_type=:id_bill_type and b.id_bill_state=:id_bill_state\n" +
            " order by id_bill desc nulls last,mesa_number")
    List<PedidosMesa> getPedidosMesaADR(@BindIn("storeList") List<String> storeList,
                                        @Bind("id_bill_type") Long id_bill_type,
                                        @Bind("id_bill_state") Long id_bill_state,
                                        @Bind("idt") Long idthird);






    @SqlQuery("SELECT ID_BILL FROM BILL WHERE ID_BILL IN (SELECT MAX( ID_BILL ) FROM BILL )\n")
    Long getPkLast();

    @SqlQuery("SELECT ID_BILL FROM FACTURACION724.BILL WHERE ID_BILL IN (SELECT MAX( ID_BILL ) FROM BILL ) AND ID_STORE = :idstore\n")
    Long getPkLastV2(@Bind("idstore") Long idstore);

    @SqlQuery("SELECT ID_BILL FROM FACTURACION724.BILL WHERE ID_BILL IN (SELECT MAX( ID_BILL ) FROM BILL ) AND ID_STORE = :idstore")
    Long getPkLastV23(@Bind("idstore") Long idstore);

    @SqlUpdate("INSERT INTO DOCUMENT (ID_DOCUMENT, TITLE, BODY) VALUES (:ID_DOCUMENT,:title,:body)")
    void insertDocument(@Bind("ID_DOCUMENT") Long ID_DOCUMENT, @Bind("title") String title,@Bind("body") String body);

    @SqlUpdate("UPDATE BILL SET ID_DOCUMENT = :ID_DOCUMENT where ID_BILL =:ID_DOCUMENT")
    void updateIdDocument(@Bind("ID_DOCUMENT") Long ID_DOCUMENT);


    @SqlUpdate(" UPDATE FACTURACION724.bill\n" +
            "                    SET PURCHASE_DATE_DOCUMENT = :date1\n" +
            "                    WHERE id_bill = :idbill ")
    void updateBillDateCont(@Bind("idbill") Long idbill, @Bind("date1") Date date2);

    @SqlQuery("SELECT COUNT(ID_BILL) FROM BILL WHERE ID_BILL = :id")
    Integer getValidatorID(@Bind("id") Long id);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_BILL  ID, ID_COMMON_STATE FROM BILL WHERE( ID_BILL = :id OR :id IS NULL)\n ")
    List<CommonSimple> readCommons(@Bind("id") Long id);


    @RegisterMapper(BillRawMapper.class)
    @SqlQuery("SELECT ID_BILL,"+
            "ID_THIRD_EMPLOYEE,"+
            "ID_THIRD,"+
            "CONSECUTIVE,"+
            "TOTALPRICE,"+
            "PURCHASE_DATE,"+
            "ID_PAYMENT_STATE,"+
            "ID_BILL_STATE,"+
            "SUBTOTAL,"+
            "TAX,"+
            "ID_BILL_TYPE,"+
            "ID_BILL_FATHER,"+
            "ID_COMMON_STATE,"+
            "DISCOUNT,"+
            "ID_DOCUMENT,"+
            "ID_THIRD_DESTINITY,"+
            "ID_CAJA,"+
            "ID_STORE,"+
            "NUM_DOCUMENTO,"+
            "ID_STORE_CLIENT,"+
            "NUM_DOCUMENTO_CLIENTE,"+
            "ID_THIRD_DOMICILIARIO,"+
            "NUM_DOCUMENTO_ADICIONAL,"+
            "PURCHASE_DATE_DOCUMENT FROM Facturacion724.bill WHERE id_bill = :id_bill")
    BillRaw readRawBill(@Bind("id_bill") Long id_bill);

    @SqlQuery(" select body from FACTURACION724.document where id_document = :id_bill ")
    String getBodyDocumentBill( @Bind("id_bill") long id_bill);

    @SqlQuery(" select name from FACTURACION724.BILL_STATE where ID_BILL_STATE = :id_bill_state ")
    String getNameBillState( @Bind("id_bill_state") long id_bill_state);

    @SqlQuery("select name from TERCERO724.DOCUMENT_TYPE where ID_DOCUMENT_TYPE = :ID_DOCUMENT_TYPE ")
    String getDocumentTypeName( @Bind("ID_DOCUMENT_TYPE") long ID_DOCUMENT_TYPE);
    
    @SqlQuery("SELECT * FROM V_BILL V_BIL \n" +
            " WHERE (V_BIL.ID_BILL=:bil.id_bill OR :bil.id_bill IS NULL ) AND \n" +
            "      (V_BIL.ID_BILL_FATHER=:bil.id_bill_father OR :bil.id_bill_father IS NULL ) AND\n" +
            "      (V_BIL.ID_THIRD_EMPLOYEE=:bil.id_third_employee OR :bil.id_third_employee IS NULL ) AND\n" +
            "      (V_BIL.CONSECUTIVE=:bil.consecutive OR :bil.consecutive IS NULL ) AND\n" +
            "      (V_BIL.PURCHASE_DATE=:bil.purchase_date OR :bil.purchase_date IS NULL ) AND\n" +
            "      (V_BIL.SUBTOTAL=:bil.subtotal OR :bil.subtotal IS NULL ) AND\n" +
            "      (V_BIL.TOTALPRICE=:bil.totalprice OR :bil.totalprice IS NULL ) AND\n" +
            "      (V_BIL.TAX=:bil.tax OR :bil.tax IS NULL ) AND\n" +
            "      (V_BIL.ID_PAYMENT_STATE=:bil.id_payment_state OR :bil.id_payment_state IS NULL ) AND\n" +
            "      (V_BIL.ID_BILL_STATE=:bil.id_bill_state OR :bil.id_bill_state IS NULL ) AND\n" +
            "      (V_BIL.ID_BILL_TYPE=:bil.id_bill_type OR :bil.id_bill_type IS NULL ) AND\n" +
            "      (V_BIL.ID_CM_BIL=:bil.id_state_bill OR :bil.id_state_bill IS NULL ) AND\n" +
            "      (V_BIL.STATE_BIL=:bil.state_bill OR :bil.state_bill IS NULL ) AND\n" +
            "      (V_BIL.CREATION_BIL=:bil.creation_bill OR :bil.creation_bill IS NULL ) AND\n" +
            "      (V_BIL.UPDATE_BIL=:bil.update_bill OR :bil.update_bill IS NULL ) ")
    List<Bill> read(@BindBean("bil") Bill bill);

    @RegisterMapper(BillCompleteMapper.class)
    @SqlQuery("SELECT * FROM V_BILLCOMPLETE V_BIL \n " +
            "WHERE (V_BIL.ID_BILL =:bil.id_bill OR  :bil.id_bill IS NULL) AND\n" +
            "      (V_BIL.ID_BILL_FATHER =:bil.id_bill_father OR  :bil.id_bill_father IS NULL) AND\n" +
            "      (V_BIL.ID_THIRD_EMPLOYEE =:bil.id_third_employee OR  :bil.id_third_employee IS NULL) AND\n" +
            "      (V_BIL.ID_THIRD =:bil.id_third OR  :bil.id_third IS NULL) AND\n" +
            "      (V_BIL.CONSECUTIVE =:bil.consecutive OR  :bil.consecutive IS NULL) AND\n" +
            "      (V_BIL.PURCHASE_DATE =:bil.purchase_date OR  :bil.purchase_date IS NULL) AND\n" +
            "      (V_BIL.SUBTOTAL =:bil.subtotal OR  :bil.subtotal IS NULL) AND\n" +
            "      (V_BIL.TOTALPRICE =:bil.totalprice OR  :bil.totalprice IS NULL) AND\n" +
            "      (V_BIL.TAX =:bil.tax OR  :bil.tax IS NULL) AND\n" +
            "\n " +
            "      (V_BIL.ID_PAYMENT_STATE =:pay_st.id_payment_state OR  :pay_st.id_payment_state IS NULL) AND\n" +
            "      (V_BIL.NAME_PAY_ST =:pay_st.name_payment_state OR  :pay_st.name_payment_state IS NULL) AND\n" +
            "      (V_BIL.ID_CM_PAY_ST =:pay_st.id_state_payment_state OR  :pay_st.id_state_payment_state IS NULL) AND\n" +
            "      (V_BIL.STATE_CM_PAY_ST =:pay_st.state_payment_state OR  :pay_st.state_payment_state IS NULL) AND\n" +
            "      (V_BIL.CREATION_PAY_ST =:pay_st.creation_payment_state OR  :pay_st.creation_payment_state IS NULL) AND\n" +
            "      (V_BIL.UPDATE_PAY_ST =:pay_st.update_payment_state OR  :pay_st.update_payment_state IS NULL) AND\n" +
            "\n " +
            "      (V_BIL.ID_BILL_STATE =:bil_st.id_bill_state OR  :bil_st.id_bill_state IS NULL) AND\n" +
            "      (V_BIL.NAME_BIL_ST =:bil_st.name_bill_state OR  :bil_st.name_bill_state IS NULL) AND\n" +
            "      (V_BIL.ID_CM_BIL_ST =:bil_st.id_state_bill_state OR  :bil_st.id_state_bill_state IS NULL) AND\n" +
            "      (V_BIL.STATE_CM_BIL_ST =:bil_st.state_bill_state OR  :bil_st.state_bill_state IS NULL) AND\n" +
            "      (V_BIL.CREATION_BIL_ST =:bil_st.creation_bill_state OR  :bil_st.creation_bill_state IS NULL) AND\n" +
            "      (V_BIL.UPDATE_BIL_ST =:bil_st.update_bill_state OR  :bil_st.update_bill_state IS NULL) AND\n" +
            "\n " +
            "      (V_BIL.ID_BILL_TYPE =:bil_ty.id_bill_type OR  :bil_ty.id_bill_type IS NULL) AND\n" +
            "      (V_BIL.NAME_BIL_TY =:bil_ty.name_bill_type OR  :bil_ty.name_bill_type IS NULL) AND\n" +
            "      (V_BIL.ID_CM_BIL_TY =:bil_ty.id_state_bill_type OR  :bil_ty.id_state_bill_type IS NULL) AND\n" +
            "      (V_BIL.STATE_CM_BIL_TY =:bil_ty.state_bill_type OR  :bil_ty.state_bill_type IS NULL) AND\n" +
            "      (V_BIL.CREATION_BIL_TY =:bil_ty.creation_bill_type OR  :bil_ty.creation_bill_type IS NULL) AND\n" +
            "      (V_BIL.UPDATE_BIL_TY =:bil_ty.update_bill_type OR  :bil_ty.update_bill_type IS NULL) AND\n" +

            "      (V_BIL.ID_CM_BIL =:bil.id_state_bill OR  :bil.id_state_bill IS NULL) AND\n" +
            "      (V_BIL.STATE_BIL =:bil.state_bill OR  :bil.state_bill IS NULL) AND\n" +
            "      (V_BIL.CREATION_BIL =:bil.creation_bill OR  :bil.creation_bill IS NULL) AND\n" +
            "      (V_BIL.UPDATE_BIL =:bil.update_bill OR  :bil.update_bill IS NULL) ")
    List<BillComplete> readComplete(@BindBean("bil")BillComplete billComplete, @BindBean("pay_st")PaymentState paymentState,
                                    @BindBean("bil_st")BillState billState, @BindBean("bil_ty") BillType billType);


    @SqlUpdate("INSERT INTO BILL (ID_STORE, ID_COMMON_STATE,ID_THIRD_EMPLOYEE,ID_THIRD,TOTALPRICE,PURCHASE_DATE,ID_PAYMENT_STATE,ID_BILL_STATE,SUBTOTAL,TAX,DISCOUNT,ID_BILL_TYPE,ID_BILL_FATHER, ID_CAJA, ID_THIRD_DESTINITY)\n" +
        "            VALUES (:bil.id_store, :id_common_state,:bil.id_third,:bil.id_third_employee,\n" +
            "        :bil.totalprice,SYSDATE,:bil.id_payment_state,:bil.id_bill_state,:bil.subtotal,:bil.tax,:bil.discount,:bil.id_bill_type,:bil.id_bill_father, :id_caja, :bil.id_third_destiny) \n")
    void create(@Bind("consecutive") String consecutive_final,@Bind("id_common_state")Long id_common_state,@BindBean("bil") BillDTO billDTO, @Bind("id_caja") Long id_caja);



    @SqlUpdate(" UPDATE BILL SET \n" +
            "    ID_THIRD_EMPLOYEE = :bil.id_third_employee, \n" +
            "    ID_THIRD = :bil.id_third, \n" +
            "    TOTALPRICE = :bil.totalprice, \n" +
            "    PURCHASE_DATE = :bil.purchase_date,\n" +
            "    ID_PAYMENT_STATE=:bil.id_payment_state,\n" +
            "    ID_BILL_STATE=:bil.id_bill_state,\n" +
            "    SUBTOTAL=:bil.subtotal,\n" +
            "    TAX=:bil.tax,\n" +
            "    DISCOUNT=:bil.discount,\n" +
            "    ID_BILL_TYPE=:bil.id_bill_type,\n" +
            "    ID_BILL_FATHER=:bil.id_bill_father\n" +
            "    WHERE (ID_BILL =  :id_bill) \n ")
    int update(@Bind("id_bill")Long id_bill, @BindBean("bil") BillDetailIDDTO billDTO);

    @SqlQuery(" select id_third from TERCERO724.third where id_person = :id_person ")
    long getIdThird( @Bind("id_person") long id_person);

    @SqlQuery(" select id_third from FACTURACION724.bill where id_bill = :id_bill ")
    long getIdThirdFromIdBill( @Bind("id_bill") long id_bill);

    @SqlQuery(" select id_store from FACTURACION724.bill where id_bill = :id_bill ")
    long getIdStoreFromIdBill( @Bind("id_bill") long id_bill);

    @RegisterMapper(BillMasterMapper.class)
//    @SqlQuery("  Select b.NUM_DOCUMENTO,b.purchase_date,l.prefix_bill,b.consecutive, cb.fullname, st.description store_name,ca.caja_number caja,doc.name,cb.document_number, b.subtotal,b.tax,b.totalprice\n" +
//            "      from tercero724.document_type doc,tercero724.common_basicinfo cb,tercero724.third t,tercero724.legal_data l,facturacion724.bill b,\n" +
//            "        tienda724.caja ca,tienda724.store st\n" +
//            "      where doc.id_document_type=cb.id_document_type and cb.id_common_basicinfo=t.id_common_basicinfo and  t.id_legal_data=l.id_legal_data and t.id_third=b.id_third\n" +
//            "      and (b.id_caja=ca.id_caja or b.id_caja is null) and ca.id_store=st.id_store\n" +
//            "      and b.id_bill=:id_bill and st.id_store=:id_store  ")
    @SqlQuery("Select b.NUM_DOCUMENTO,b.purchase_date,l.prefix_bill,b.consecutive, cb.fullname, st.description store_name\n" +
            ",stprov.description storeprov\n" +
            ",ca.caja_number caja,doc.name,cb.document_number, b.subtotal,b.tax,b.totalprice\n" +
            "                 from tercero724.document_type doc,tercero724.common_basicinfo cb,tercero724.third t,tercero724.legal_data l,facturacion724.bill b,\n" +
            "                      tienda724.caja ca,tienda724.store st,tienda724.store stprov\n" +
            "                  where doc.id_document_type=cb.id_document_type and cb.id_common_basicinfo=t.id_common_basicinfo \n" +
            "                       and t.id_legal_data=l.id_legal_data(+) and t.id_third=b.id_third \n" +
            "                       and (b.id_caja=ca.id_caja or b.id_caja is null) and ca.id_store=st.id_store\n" +
            "                       and b.id_store_client=stprov.id_store(+)\n" +
            "                       and b.id_bill=:id_bill and st.id_store=:id_store")
    BillMaster getMaestroFactura(@Bind("id_bill") Long id_bill,
                                 @Bind("id_store") Long id_store);

    @RegisterMapper(BillMasterMapper2.class)
    @SqlQuery("  Select b.purchase_date,l.prefix_bill,b.consecutive, cb.fullname, st.description store_name,doc.name,cb.document_number, b.subtotal,b.tax,b.totalprice\n" +
            "      from tercero724.document_type doc,tercero724.common_basicinfo cb,tercero724.third t,tercero724.legal_data l,facturacion724.bill b,\n" +
            "      tienda724.store st\n" +
            "      where doc.id_document_type=cb.id_document_type and cb.id_common_basicinfo=t.id_common_basicinfo and  t.id_legal_data=l.id_legal_data and t.id_third=b.id_third\n" +
            "      and b.id_bill=:id_bill and st.id_store=:id_store  ")
    BillMaster2 getMaestroFactura2(@Bind("id_bill") Long id_bill,
                                 @Bind("id_store") Long id_store);

    @RegisterMapper(TopProdsMapper.class)
    @SqlQuery("  select ownbarcode,product_store_code ,id_product_third,product_store_name,co.img, count(*) as ventas\n" +
            "from facturacion724.detail_bill db,facturacion724.bill b,tienda724.product_store ps,tienda724.codes co\n" +
            "where db.id_bill=b.id_bill and db.id_product_third=ps.id_product_store and ps.id_code=co.id_code\n" +
            "  and purchase_date between sysdate - 15 and sysdate and ps.id_store in (:idstore) and ownbarcode like '99999%' \n" +
            "group by ownbarcode,product_store_code,id_product_third,product_store_name,co.img\n" +
            "order by 6 desc ")
    List<TopProds> getTopProducts(@Bind("idstore") Long idstore);


    @SqlQuery(" select count(*) from facturacion724.bill where id_bill_type=1 and id_caja=:id_caja\n" +
            "         and purchase_date between (select STARTING_DATE from facturacion724.cierre_caja where id_cierre_caja=:id_cierre_caja) and\n" +
            "         nvl((select closing_DATE from facturacion724.cierre_caja where id_cierre_caja=:id_cierre_caja),sysdate) ")
    Long getCantFacturas(@Bind("id_caja") Long id_caja,@Bind("id_cierre_caja") Long id_cierre_caja);

//    @SqlQuery("select ps.product_store_name,d.quantity,d.quantity*(d.price*(1+(t.percent/100))) valor, t.percent , d.quantity*d.price price_no_iva,c.code\n" +
//            "                       from facturacion724.detail_bill d,tienda724.product_store ps, tienda724.codes c,\n" +
//            "                                TIENDA724.tax_tariff t\n" +
//            "                           where d.id_product_third=ps.id_product_store\n" +
//            "                            and id_bill=:id_bill and ps.id_code = c.id_code\n" +
//            "                      and t.id_tax_tariff = ps.id_tax\n")
//    List<BillDetail> getDetalleFactura(@Bind("id_bill") Long id_bill);

    @RegisterMapper(BillDetailMapper.class)
    @SqlQuery("select ps.product_store_name,d.quantity,d.quantity*(d.price*(1+(t.percent/100))) valor, t.percent , d.quantity*d.price price_no_iva,c.code, d.price, t.id_tax_tariff idtax\n" +
            "from facturacion724.detail_bill d,tienda724.product_store ps, tienda724.codes c,\n" +
            "\t\tTIENDA724.tax_tariff t\n" +
            "   where d.id_product_third=ps.id_product_store\n" +
            "\tand id_bill=:id_bill and ps.id_code = c.id_code\n" +
            "and t.id_tax_tariff = d.tax_product\n")
    List<BillDetail> getDetalleFactura(@Bind("id_bill") Long id_bill);

    @SqlQuery("select sum(totalprice)  \n" +
            " from facturacion724.bill \n" +
            " where id_bill_type=1 and purchase_date between (select starting_date from facturacion724.cierre_caja where id_cierre_caja=:id_cierre_caja) and sysdate ")
    Double getSalesCaja(@Bind("id_cierre_caja") Long id_cierre_caja);


    @SqlUpdate("UPDATE FACTURACION724.BILL SET PURCHASE_DATE_DOCUMENT=:fecha, NUM_DOCUMENTO_CLIENTE=:dato  WHERE ID_BILL=:id_bill ")
    void putCompraBill(@Bind("id_bill")Long id_bill, @Bind("dato") String dato, @Bind("fecha") Date fecha);


    @SqlUpdate("UPDATE FACTURACION724.BILL SET ID_THIRD_DOMICILIARIO=:id_third  WHERE ID_BILL=:id_bill ")
    void putDomiciliario(@Bind("id_bill")Long id_bill, @Bind("id_third") Long id_third);




    int delete();


    int deletePermanent();


    @RegisterMapper(LegalDataMapper.class)
    @SqlQuery(" select RESOLUCION_DIAN,REGIMEN_TRIBUTARIO,AUTORETENEDOR,URL_LOGO,city_name,co.descripcion country,address,phone1,email,webpage,\n" +
            "    prefix_bill,initial_range,final_range,start_consecutive,notes\n" +
            "    from tercero724.legal_data ld, tercero724.third t,TERCERO724.CITY CI,TERCERO724.COUNTRY CO \n" +
            "    where ld.id_legal_data=t.id_legal_data AND ld.id_city=ci.id_city and ld.id_country=co.id_country\n" +
            "    and id_third=:id_third ")
    List<LegalData> getLegalData(@Bind("id_third") Long id_third);



    @RegisterMapper(storesByThirdMapper.class)
    @SqlQuery("select s.id_third,s.id_store,s.description,d.address,city_name,latitud,longitud,url_logo,phone,mail\n" +
            "from tercero724.third t,tercero724.third tf,tienda724.store s,tercero724.directory d\n" +
            "   ,tercero724.city c,tercero724.legal_data ld,tercero724.mail m,tercero724.phone p\n" +
            "where t.id_third_father=tf.id_third and tf.id_third=s.id_third and s.id_directory=d.id_directory\n" +
            "  and d.id_city=c.id_city and tf.ID_LEGAL_DATA=ld.ID_LEGAL_DATA\n" +
            "  and m.id_directory=d.id_directory and p.id_directory=d.id_directory and\n" +
            "        m.priority=1 and p.PRIORITY=1\n" +
            "  and t.id_third=:id_third ")
    List<storesByThird> getTiendasByThird(@Bind("id_third") Long id_third);




    @RegisterMapper(DomicilioMapper.class)
    @SqlQuery(" SELECT P.ID_PERSON,FULLNAME DOMICILIARIO,T.ID_THIRD ID_THIRD_DOMICILIARIO \n" +
            "   FROM TIENDA724.STORE_DOMICILIARIO SD,TERCERO724.PERSON P,TERCERO724.COMMON_BASICINFO CBI,TERCERO724.THIRD T\n" +
            "   WHERE SD.ID_PERSON=P.ID_PERSON AND P.ID_COMMON_BASICINFO=CBI.ID_COMMON_BASICINFO AND P.ID_PERSON=T.ID_PERSON\n" +
            "   AND ID_STORE=:id_store ")
    List<Domicilio> getDomi(@Bind("id_store") Long id_store);


    @RegisterMapper(PedidosSelectMapper.class)
    @SqlQuery(" SELECT ID_BILL,NUM_DOCUMENTO,ID_THIRD FROM FACTURACION724.BILL B WHERE ID_BILL_TYPE=87 AND ID_BILL_STATE=61 AND ID_STORE=:id_store\n" +
            " and id_store_client in (select id_store_provider from tienda724.param_pedidos pp where pp.id_store_client=b.id_store and pp.id_third_destinity=:id_third)  \n" +
            " AND NUM_DOCUMENTO NOT IN (SELECT NUM_DOCUMENTO_CLIENTE FROM FACTURACION724.BILL WHERE ID_BILL_TYPE=2 AND ID_BILL_STATE=1 AND ID_STORE=:id_store and num_documento_cliente is not null) ORDER BY ID_BILL ")
    List<PedidosSelect> getPedidosSelect(@Bind("id_store") Long id_store,
                                         @Bind("id_third") Long id_third);


    @SqlCall(" call facturacion724.actualizar_bill_sin_iva(:idbill,:disccount) ")
    void procedure2(@Bind("idbill") Long idbill,@Bind("disccount") Long disccount);


    @SqlCall(" call facturacion724.anular_factura_error(idcaja)")
    void deleteBillPro(@Bind("idcaja") Long idcaja);


    @SqlCall(" call facturacion724.validar_bill_final(:idbill) ")
    void procedureValidarBill(@Bind("idbill") Long idbill);


    @SqlCall(" call facturacion724.crear_pedido_mesa(:idstoreclient ,:idthirduseraapp ," +
            " :idstoreprov ,:detallepedido ,:descuento ,:idpaymentmethod ," +
            " :idapplication ,:idthirdemp ,:detallepedidomesa ,:idmesa ) ")
    void crearPedidoMesa(@Bind("idstoreclient") Long idstoreclient,
                                   @Bind("idthirduseraapp") Long idthirduseraapp,
                                   @Bind("idstoreprov") Long idstoreprov,
                                   @Bind("detallepedido") String detallepedido,
                                   @Bind("descuento") Long descuento,
                                   @Bind("idpaymentmethod") Long idpaymentmethod,
                                   @Bind("idapplication") Long idapplication,
                                   @Bind("idthirdemp") Long idthirdemp,
                                   @Bind("detallepedidomesa") String detallepedidomesa,
                                   @Bind("idmesa") Long idmesa);

    @SqlCall(" call facturacion724.crear_pedido_mesa(:idstoreclient ,:idthirduseraapp ," +
            " :idstoreprov ,:detallepedido ,:descuento ,:idpaymentmethod ," +
            " :idapplication ,:idthirdemp ,:detallepedidomesa ,:idmesa ) ")
    void crearPedidoMesaV2(@Bind("idstoreclient") Long idstoreclient,
                           @Bind("idthirduseraapp") Long idthirduseraapp,
                           @Bind("idstoreprov") Long idstoreprov,
                           @Bind("detallepedido") String detallepedido,
                           @Bind("descuento") Long descuento,
                           @Bind("idpaymentmethod") Long idpaymentmethod,
                           @Bind("idapplication") Long idapplication,
                           @Bind("idthirdemp") Long idthirdemp,
                           @Bind("detallepedidomesa") String detallepedidomesa,
                           @Bind("idmesa") Long idmesa);


    @SqlCall(" call facturacion724.actualizar_estado_mesa(:IDBILL,\n" +
            "    :IDESTADODESTINO, :nota, :IDESTADOORIGEN ,:IDTHIRDUSER , :ACTOR ) ")
    void actualizarEstadoMesa(@Bind("IDBILL") Long IDBILL,
                           @Bind("IDESTADODESTINO") Long IDESTADODESTINO,
                           @Bind("nota") String nota,
                           @Bind("IDESTADOORIGEN") String IDESTADOORIGEN,
                           @Bind("IDTHIRDUSER") Long IDTHIRDUSER,
                           @Bind("ACTOR") String ACTOR);


    @SqlCall(" call FACTURACION724.AGREGAR_PRODUCTOS_MESA(:idbill ,:detallepedido) ")
    void agregarProductosMesa(@Bind("idbill") Long idbill,
                              @Bind("detallepedido") String detallepedido);



    @SqlCall(" call facturacion724.AGREGAR_PRODUCTOS_MESA_OPCIONES(:idbill,:detallepedido,:detallepedidomesa) ")
    void agregarProductosMesaOpciones(@Bind("idbill") Long idbill,
                              @Bind("detallepedido") String detallepedido,
                              @Bind("detallepedidomesa") String detallepedidomesa);


    @SqlCall(" call facturacion724.crear_detalle_pedido_mesa(:idbprov,:detallepedidomesa)")
    void crearDetallePedidoMesa(@Bind("idbprov") Long idbprov,
                         @Bind("detallepedidomesa") String detallepedidomesa);


    @SqlCall(" call facturacion724.actualizar_estado_pedido_mesa(:ID_DETALLE_DETAIL_BILL,\n" +
            "    :IDESTADODESTINO, :notas, :IDESTADOORIGEN, :IDTHIRDUSER, :ACTOR)")
    void actualizar_estado_pedido_mesa(@Bind("ID_DETALLE_DETAIL_BILL") Long ID_DETALLE_DETAIL_BILL,
                                       @Bind("IDESTADODESTINO") Long IDESTADODESTINO,
                                       @Bind("notas") String notas,
                                       @Bind("IDESTADOORIGEN") Long IDESTADOORIGEN,
                                       @Bind("IDTHIRDUSER") Long IDTHIRDUSER,
                                       @Bind("ACTOR") String ACTOR);

    @SqlCall(" call facturacion724.asignar_mesero_pedido(:idbprov, :id_third_mesero)")
    void asignar_mesero_pedido(@Bind("idbprov") Long idbprov,
                               @Bind("id_third_mesero") Long id_third_mesero);


    @SqlCall(" call facturacion724.crear_payment_detail(:idbill)")
    void procedureCrearPaymentDetail(@Bind("idbill") Long idbill);


    @SqlCall(" call facturacion724.facturar_pedido_mesa(:idthirdemployee,:idthird,:idbilltype," +
            ":notes,:idthirddestinity,\n" +
            ":idcaja,:idstore,:idthirddomiciliario,:idpaymentmethod,:idwaytopay,\n" +
            ":approvalcode,:idbankentity,:idbillstate,:detallesbill,:descuento ,\n" +
            ":numdocumentoadicional,:idthirdvendedor,:detallespago,:idbillpedido,:nota,:idthirduser,:actor)")
    void facturarPedidoMesa(@Bind("idthirdemployee") Long idthirdemployee,
                            @Bind("idthird") Long idthird,
                            @Bind("idbilltype") Long idbilltype,
                            @Bind("notes") String notes,
                            @Bind("idthirddestinity") Long idthirddestinity,
                            @Bind("idcaja") Long idcaja,
                            @Bind("idstore") Long idstore,
                            @Bind("idthirddomiciliario") Long idthirddomiciliario,
                            @Bind("idpaymentmethod") Long idpaymentmethod,
                            @Bind("idwaytopay") Long idwaytopay,
                            @Bind("approvalcode") String approvalcode,
                            @Bind("idbankentity") Long idbankentity,
                            @Bind("idbillstate") Long idbillstate,
                            @Bind("detallesbill") String detallesbill,
                            @Bind("descuento") Long descuento,
                            @Bind("numdocumentoadicional") String numdocumentoadicional,
                            @Bind("idthirdvendedor") Long idthirdvendedor,
                            @Bind("detallespago") String detallespago,
                            @Bind("idbillpedido") Long idbillpedido,
                            @Bind("nota") String nota,
                            @Bind("idthirduser") Long idthirduser,@Bind("actor") String actor);



    @RegisterMapper(mesaStateMapper.class)
    @SqlQuery("select estado_mesa,token from tienda724.mesa m,tienda724.mesa_token mt\n" +
            "where m.id_mesa=mt.id_mesa(+) and m.id_mesa=:idStore")
    mesaState getValidacionMesa(@Bind("idStore") Long idStore);


    @SqlCall(" begin FACTURACION724.CREATE_BILL(:idthirdemployee,:idthird ,:idbilltype , \n" +
            "                                :notes ,:idthirddestinity ,:idcaja , \n" +
            "                                :idstore ,:idthirddomiciliario , \n" +
            "                                :idpaymentmethod ,:idwaytopay , \n" +
            "                                :approvalcode ,:idbankentity, :idbillstate, :details, :disccount, :num_documento_factura, :idthirdvendedor ); end; ")
    void procedureInsertBill(@Bind("idthirdemployee") Long idthirdemployee,
                             @Bind("idthird") Long idthird,
                             @Bind("idbilltype") Long idbilltype,
                             @Bind("notes") String notes,
                             @Bind("idthirddestinity") Long idthirddestinity,
                             @Bind("idcaja") Long idcaja,
                             @Bind("idstore") Long idstore,
                             @Bind("idthirddomiciliario") Long idthirddomiciliario,
                             @Bind("idpaymentmethod") Long idpaymentmethod,
                             @Bind("idwaytopay") Long idwaytopay,
                             @Bind("approvalcode") String approvalcode,
                             @Bind("idbankentity") Long idbankentity,
                             @Bind("idbillstate") Long idbillstate,
                             @Bind("details") String details,
                             @Bind("disccount") Long disccount,
                             @Bind("num_documento_factura") String num_documento_factura,
                             @Bind("idthirdvendedor") Long idthirdvendedor);


    @SqlCall(" call facturacion724.CREATE_DETAIL_BILL(:idbill ,:cantidad ,:valor ,:idproductstore ,:id_impuesto, :idbilltype, :idbillstate ) ")
    void procedureInsertDetailBill(@Bind("idbill") Long idbill,
                                   @Bind("cantidad") Long cantidad,
                                   @Bind("valor") Double valor,
                                   @Bind("idproductstore") Long idproductstore,
                                   @Bind("id_impuesto") Long id_impuesto,
                                   @Bind("idbilltype") Long idbilltype,
                                   @Bind("idbillstate") Long idbillstate);



    @RegisterMapper(comandaDetailsMapper.class)
    @SqlQuery(" select product_store_name,count(*) cantidad \n" +
            "from FACTURACION724.DETALLE_DETAIL_BILL db,tienda724.product_store ps \n" +
            "where db.id_product_store=ps.id_product_store and \n" +
            "id_bill=:id_bill and ID_BILL_STATE=1501 and ownbarcode!='45000198' \n" +
            "group by product_store_name ")
    List<comandaDetails> getComandaDetails(@Bind("id_bill") Long id_bill);



    @SqlQuery("SELECT SUM(QUANTITY) FROM TIENDA724.PRODUCT_STORE_INVENTORY WHERE ID_PRODUCT_STORE=:idps\n")
    Long validateProductQuantity(@Bind("idps") Long idps);


    @SqlCall("call facturacion724.validar_detalles_venta(:idbill)")
    void validatePdfDrawing(@Bind("idbill") Long idbill);


    @SqlQuery("select max(id_bill) from facturacion724.bill " +
            "where id_store=:idstore and id_third_employee=:idemp")
    Long getMaxBillPedidos(@Bind("idstore") Long idstore,@Bind("idemp") Long idemp);


    @SqlUpdate("insert into facturacion724.detail_payment_bill(PAYMENT_VALUE,ID_BILL,ID_PAYMENT_METHOD,APPROBATION_CODE,ID_WAY_TO_PAY,ID_BANK_ENTITY) \n" +
            "values(:PAYMENTVALUE,:IDBILL,:IDPAYMENTMETHOD,:APPROBATIONCODE,:IDWAYTOPAY,:IDBANKENTITY)")
    void insertPaymentDetail(@Bind("PAYMENTVALUE") String PAYMENTVALUE,
                             @Bind("IDBILL") String IDBILL,
                             @Bind("IDPAYMENTMETHOD") String IDPAYMENTMETHOD,
                             @Bind("APPROBATIONCODE") String APPROBATIONCODE,
                             @Bind("IDWAYTOPAY") String IDWAYTOPAY,
                             @Bind("IDBANKENTITY") String IDBANKENTITY);



    @SqlCall("call facturacion724.registrar_primer_pago(:idbill,:valor)")
    void registrar_primer_pago(@Bind("idbill") Long idbill,@Bind("valor") String valor);



    @SqlQuery("select nvl(sum(price),0) total_cancelados from facturacion724.detalle_detail_bill ddb,facturacion724.detail_bill db\n" +
            "    where ddb.id_bill=db.id_bill and ddb.id_product_store=db.id_product_third\n" +
            "    and ddb.id_bill=:idBill\n" +
            "    and ddb.id_bill_state=1599")
    Long getCanceledTable(@Bind("idBill") Long idBill);



    @SqlCall(" call facturacion724.agregar_servicio_mesa(:idbill,:barcode,:valor,:idstore) ")
    void agregar_servicio_mesa(@Bind("idbill") Long idbill,
                               @Bind("barcode") Long barcode,
                               @Bind("valor") Long valor,
                               @Bind("idstore") Long idstore);

    @SqlCall(" call facturacion724.eliminar_servicio_detailbill(:idbill) ")
    void eliminar_servicio_detailbill(@Bind("idbill") Long idbill);



    @SqlCall(" call facturacion724.cerrar_mesa(:idbill) ")
    void cerrar_mesa(@Bind("idbill") Long idbill);


    @SqlCall(" call facturacion724.asociar_tercero_bill(:idthird, :idbill)")
    void asociar_tercero_bill(@Bind("idthird") Long idthird,@Bind("idbill") Long idbill);


    @SqlQuery("select c.id_city from tercero724.city c, tienda724.store s,tercero724.directory d\n" +
            " where c.ID_CITY=d.ID_CITY and s.ID_DIRECTORY=d.ID_DIRECTORY\n" +
            " and s.id_store=:id_store")
    String getIdCityByStore(@Bind("id_store") Long id_store);


    @RegisterMapper(PedidosMesa2Mapper.class)
    @SqlQuery("select distinct id_bill,purchase_date,m.id_mesa,m.DESCRIPTION mesa,id_third_domiciliario,\n" +
            " totalprice - facturacion724.get_total_prods_cancelados(b.id_bill) totalprice,cbi.fullname mesero,idapp,\n" +
            " (select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill and ID_BILL_STATE=1501) AMARILLO,\n" +
            " (select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill and ID_BILL_STATE=1502) NARANJA,\n" +
            " (select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill and ID_BILL_STATE=1503) AZUL,\n" +
            " (select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill and ID_BILL_STATE=1599) ROJO,\n" +
            " (select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill and ID_BILL_STATE=1504) VIOLETA,\n" +
            " (select count(*) from facturacion724.detalle_detail_bill where id_bill=b.id_bill) Total_productos,mesa_number,m.id_store\n" +
            " ,cbicliente.fullname cliente,m.ESTADO_MESA\n" +
            " from facturacion724.bill b, TIENDA724.MESA m,tercero724.third t,tercero724.common_basicinfo cbi\n" +
            " ,tercero724.third tcliente,tercero724.common_basicinfo cbicliente\n" +
            " where b.id_mesa=m.id_mesa and b.id_third_domiciliario=t.id_third\n" +
            " and t.ID_COMMON_BASICINFO=cbi.ID_COMMON_BASICINFO\n" +
            " and b.id_third_destinity=tcliente.id_third\n" +
            " and tcliente.ID_COMMON_BASICINFO=cbicliente.ID_COMMON_BASICINFO\n" +
            " and b.id_mesa=:idmesa\n" +
            " and id_bill_type=:id_bill_type and b.id_bill_state=:id_bill_state")
    PedidosMesa2 getPedidosMesaByIdMesa(@Bind("idmesa") Long idmesa,
                                        @Bind("id_bill_type") Long id_bill_type,
                                        @Bind("id_bill_state") Long id_bill_state);

    @SqlCall(" call tienda724.registrar_mesa_token(:idmesa,:tok) ")
    void registrar_mesa_token(@Bind("idmesa") Long idmesa,
                              @Bind("tok") String tok);


    @SqlQuery("select count(*) from tienda724.mesa_token where id_mesa=:idmesa")
    Long getMesaAbierta(@Bind("idmesa") Long idmesa);



    @SqlQuery("select facturacion724.get_valor_servicio(:idbill,:pct) from dual")
    String get_valor_servicio(@Bind("idbill") String idbill,@Bind("pct") String pct);



    @SqlQuery("select count(*) from tienda724.product_store ps,facturacion724.detail_bill db \n" +
            "where ps.id_product_store=db.id_product_third and ownbarcode='10101010101011' and id_bill=:idbill")
    Long getIsServicioInBill(@Bind("idbill") Long idbill);



    @SqlQuery("select facturacion724.get_valores_bill(:idbill) from dual")
    String get_valores_bill(@Bind("idbill") String idbill);


    @SqlCall(" call facturacion724.crear_credito(:fechacreacion,:valor,:numcuotas,:interes,:interesmora,:periodicidad,:cuotainicial,:idthirdcliente,:idthirdprov,:idthirdcodeudor)")
    void crear_credito(@Bind("fechacreacion") String fechacreacion,
                              @Bind("valor") Long valor,
                              @Bind("numcuotas") Long numcuotas,
                              @Bind("interes") Long interes,
                              @Bind("interesmora") Long interesmora,
                              @Bind("periodicidad") Long periodicidad,
                              @Bind("cuotainicial") Long cuotainicial,
                              @Bind("idthirdcliente") Long idthirdcliente,
                              @Bind("idthirdprov") Long idthirdprov,
                              @Bind("idthirdcodeudor") Long idthirdcodeudor);


    @SqlQuery("select max(:id_credito) from facturacion724.credito")
    String get_max_credito(@Bind("id_credito") String id_credito);

    @RegisterMapper(CreditoMapper.class)
    @SqlQuery("select\n" +
            "            ID_CREDITO,\n" +
            "            FECHA_CREACION,\n" +
            "            c.ID_ESTADO_CREDITO,\n" +
            "            MONTO,\n" +
            "            NUM_CUOTAS,\n" +
            "            INTERES,\n" +
            "            INTERES_MORA,\n" +
            "            PERDIODICIDAD_DIAS,\n" +
            "            FECHA_APROBACION,\n" +
            "            FECHA_DESEMBOLSO,\n" +
            "            CUOTA_INICIAL,\n" +
            "            ID_BILL_FACTURA,\n" +
            "            ID_THIRD_CLIENT,\n" +
            "            ID_THIRD_PROVIDER,\n" +
            "            ID_THIRD_CODEUDOR,\n" +
            "            cbi.fullname cliente,\n" +
            "            cbi.DOCUMENT_NUMBER doccliente,\n" +
            "            dircliente.ADDRESS dircliente,\n" +
            "            phcliente.PHONE phonecliente,\n" +
            "            citycliente.CITY_NAME ciudadCliente,\n" +
            "            cbicodeudor.fullname codeudor,\n" +
            "            cbi.DOCUMENT_NUMBER doccodeudor,\n" +
            "            dircodeudor.ADDRESS dircodeudor,\n" +
            "            phcodeudor.PHONE phonecodeudor,\n" +
            "            citycodeudor.CITY_NAME ciudadCodeudor\n" +
            "            ,ec.estado_credito\n" +
            "            from facturacion724.credito c,tercero724.third t,tercero724.common_basicinfo cbi\n" +
            "            ,tercero724.common_basicinfo cbicodeudor,tercero724.directory dircliente,\n" +
            "            tercero724.directory dircodeudor,tercero724.third codeudor\n" +
            "            ,tercero724.phone phcliente,tercero724.phone phcodeudor\n" +
            "            ,tercero724.city citycliente,tercero724.city citycodeudor\n" +
            "            ,facturacion724.estado_credito ec\n" +
            "            where c.id_third_client=t.id_third and t.ID_COMMON_BASICINFO=cbi.ID_COMMON_BASICINFO \n" +
            "            and t.id_directory=dircliente.id_directory \n" +
            "            and c.id_third_codeudor=codeudor.id_third and codeudor.id_common_basicinfo=cbicodeudor.id_common_basicinfo\n" +
            "            and codeudor.id_directory=dircodeudor.id_directory\n" +
            "            and dircliente.ID_DIRECTORY=phcliente.ID_DIRECTORY and dircodeudor.ID_DIRECTORY=phcodeudor.ID_DIRECTORY\n" +
            "            and dircliente.ID_CITY=citycliente.ID_CITY and dircodeudor.ID_CITY=citycodeudor.ID_CITY\n" +
            "            and ec.ID_ESTADO_CREDITO=c.ID_ESTADO_CREDITO\n" +
            "            and cbi.document_number like :cedula order by ID_CREDITO DESC")
    List<credito> get_credito(@Bind("cedula") String cedula);


    @RegisterMapper(DocumentoCreditoMapper.class)
    @SqlQuery("select id_credito_documento,NOMBRE,RUTA,TIPO_DOCUMENTO from facturacion724.credito_documento where id_credito=:id_credito")
    List<DocumentoCredito> getDocumentoCredito(@Bind("id_credito") Long id_credito);


    @SqlCall(" call facturacion724.CREAR_CREDITO_DOCUMENTO(:NOMBREDOC,:RUTADOC,:TIPODOC,:IDCREDITO)")
    void CREAR_CREDITO_DOCUMENTO(@Bind("NOMBREDOC") String NOMBREDOC,
                       @Bind("RUTADOC") String RUTADOC,
                       @Bind("TIPODOC") String TIPODOC,
                       @Bind("IDCREDITO") Long IDCREDITO);


    @RegisterMapper(LogCreditoMapper.class)
    @SqlQuery("select ID_CREDITO_LOG,TEXTO,FECHA from facturacion724.credito_log where id_credito=:id_credito")
    List<LogCredito> getLogCredito(@Bind("id_credito") Long id_credito);



    @SqlCall(" call facturacion724.aprobar_credito(:IDCREDITO ,:NOTE)")
    void aprobar_credito(@Bind("NOTE") String NOMBREDOC,
                         @Bind("IDCREDITO") Long IDCREDITO);



    @SqlCall(" call facturacion724.desembolsar_credito(:IDCREDITO ,:NOTE)")
    void desembolsar_credito(@Bind("NOTE") String NOTE,
                                 @Bind("IDCREDITO") Long IDCREDITO);



    @SqlCall(" call facturacion724.generar_cuotas(:IDCREDITO)")
    void generar_cuotas(@Bind("IDCREDITO") Long IDCREDITO);




    @RegisterMapper(CreditoMapper.class)
    @SqlQuery("select\n" +
            "                        ID_CREDITO,\n" +
            "                        FECHA_CREACION,\n" +
            "                        c.ID_ESTADO_CREDITO,\n" +
            "                        MONTO,\n" +
            "                        NUM_CUOTAS,\n" +
            "                        INTERES,\n" +
            "                        INTERES_MORA,\n" +
            "                        PERDIODICIDAD_DIAS,\n" +
            "                        FECHA_APROBACION,\n" +
            "                        FECHA_DESEMBOLSO,\n" +
            "                        CUOTA_INICIAL,\n" +
            "                        ID_BILL_FACTURA,\n" +
            "                        ID_THIRD_CLIENT,\n" +
            "                        ID_THIRD_PROVIDER,\n" +
            "                        ID_THIRD_CODEUDOR,\n" +
            "                        cbi.fullname cliente,\n" +
            "                        cbi.DOCUMENT_NUMBER doccliente,          \n" +
            "                        dircliente.ADDRESS dircliente,\n" +
            "                        phcliente.PHONE phonecliente,\n" +
            "                        citycliente.CITY_NAME ciudadCliente,\n" +
            "                        cbicodeudor.fullname codeudor,\n" +
            "                        cbi.DOCUMENT_NUMBER doccodeudor,\n" +
            "                        dircodeudor.ADDRESS dircodeudor,\n" +
            "                        phcodeudor.PHONE phonecodeudor,\n" +
            "                        citycodeudor.CITY_NAME ciudadCodeudor\n" +
            "                        ,ec.estado_credito\n" +
            "                        from facturacion724.credito c,tercero724.third t,tercero724.common_basicinfo cbi\n" +
            "                      ,tercero724.common_basicinfo cbicodeudor,tercero724.directory dircliente\n" +
            "                        ,tercero724.directory dircodeudor,tercero724.third codeudor\n" +
            "                        ,tercero724.phone phcliente,tercero724.phone phcodeudor \n" +
            "                        ,tercero724.city citycliente,tercero724.city citycodeudor\n" +
            "                        ,facturacion724.estado_credito ec\n" +
            "                        where c.id_third_client=t.id_third and t.ID_COMMON_BASICINFO=cbi.ID_COMMON_BASICINFO\n" +
            "                        and t.id_directory=dircliente.id_directory \n" +
            "                      and c.id_third_codeudor=codeudor.id_third and codeudor.id_common_basicinfo=cbicodeudor.id_common_basicinfo\n" +
            "                        and codeudor.id_directory=dircodeudor.id_directory\n" +
            "                        and dircliente.ID_DIRECTORY=phcliente.ID_DIRECTORY and dircodeudor.ID_DIRECTORY=phcodeudor.ID_DIRECTORY\n" +
            "                        and dircliente.ID_CITY=citycliente.ID_CITY and dircodeudor.ID_CITY=citycodeudor.ID_CITY\n" +
            "                        and ec.ID_ESTADO_CREDITO=c.ID_ESTADO_CREDITO\n" +
            "                        and c.ID_CREDITO= :ID_CREDITO")
    List<credito> getCreditoById(@Bind("ID_CREDITO") Long ID_CREDITO);




    @SqlCall("call facturacion724.crear_cuota_credito(:idcredito, :fechavencimiento, :valorcuota)")
    void crear_cuota_credito(@Bind("idcredito") Long idcredito,
                             @Bind("fechavencimiento") String fechavencimiento,
                             @Bind("valorcuota") Long valorcuota);




    @SqlCall("call facturacion724.crear_abono_cuota(:idcuota ,:idcredito ,:fechaabono ,:valorabono ,:idpaymentmethod ,:IDCREDITODOCUMENTO )")
    void crear_abono_cuota(@Bind("idcuota") Long idcuota,
                           @Bind("idcredito") Long idcredito,
                           @Bind("fechaabono") String fechaabono,
                           @Bind("valorabono") Long valorabono,
                           @Bind("idpaymentmethod") Long idpaymentmethod,
                           @Bind("IDCREDITODOCUMENTO") Long IDCREDITODOCUMENTO);




    @RegisterMapper(CuotaMapper.class)
    @SqlQuery("select ID_CUOTA,ID_CREDITO,FECHA_VENCIMIENTO,VALOR,ID_ESTADO_CUOTA,e.ESTADO_CREDITO ESTADO_CUOTA\n" +
            "    from facturacion724.cuotas_credito c,facturacion724.estado_credito e\n" +
            "    where c.ID_ESTADO_CUOTA=e.ID_ESTADO_CREDITO\n" +
            "    and  id_credito=:idcredito")
    List<Cuota> getCuotas(@Bind("idcredito") String idcredito);



    @RegisterMapper(AbonoMapper.class)
    @SqlQuery("select ID_ABONO_CUOTA_CREDITO,ID_CUOTA,FECHA_ABONO,VALOR_ABONO,ID_CREDITO,pm.name paymentmethod\n" +
            "from facturacion724.abono_cuota_credito a,facturacion724.payment_method pm\n" +
            "where a.id_payment_method=pm.id_payment_method\n" +
            "and id_cuota=:idcuota and id_credito=:idcredito\n" +
            "order by id_cuota")
    List<Abono> getAbono(@Bind("idcredito") Long idcredito,
                         @Bind("idcuota") Long idcuota);



    @SqlCall("call facturacion724.asociar_factura_credito(:idcredito, :idbill )")
    void asociar_factura_credito(@Bind("idcredito") Long idcredito,
                             @Bind("idbill") Long idbill);


    @RegisterMapper(Bills4CreditMapper.class)
    @SqlQuery("select id_bill,num_documento,s.description tienda from facturacion724.bill b,tienda724.store s \n" +
            "where b.id_store=s.id_store and b.ID_THIRD=:idthird and b.num_documento like :numdoc\n" +
            "and  id_bill_type=1 and id_bill_state=1 \n" +
            "order by 1 desc")
    List<Bills4Credit> Bills4Credit(@Bind("idthird") Long idthird,
                                    @Bind("numdoc") String numdoc);







    @SqlQuery("select id_bill_factura from facturacion724.credito where id_third_provider=:idthird and id_bill_factura is not null")
    List<Long> getBillsNotToUse(@Bind("idthird") Long idthird);

}
