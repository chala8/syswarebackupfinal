package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class PaymentDetailListDTO {
    private List<PaymentDetailDTO> detalles;

    @JsonCreator
    public PaymentDetailListDTO(@JsonProperty("detalles") List<PaymentDetailDTO> detalles) {
        this.detalles = detalles;
    }

    public List<PaymentDetailDTO> getdetalles() {
        return detalles;
    }

    public void setdetalles(List<PaymentDetailDTO> detalles) {
        this.detalles = detalles;
    }


}
