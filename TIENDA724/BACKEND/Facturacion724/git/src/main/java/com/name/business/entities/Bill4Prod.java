package com.name.business.entities;

import java.util.Date;

public class Bill4Prod {

    private Long ID_BILL;
    private String PURCHASE_DATE;
    private String DOCUMENT_NUMBER;
    private Double TOTALPRICE;
    private Long QUANTITY;

    public Bill4Prod(  Long ID_BILL,
                       String PURCHASE_DATE,
                       String DOCUMENT_NUMBER,
                       Double TOTALPRICE,
                       Long QUANTITY) {
        this.PURCHASE_DATE = PURCHASE_DATE;
        this.ID_BILL = ID_BILL;
        this.QUANTITY = QUANTITY;
        this.DOCUMENT_NUMBER =DOCUMENT_NUMBER;
        this.TOTALPRICE = TOTALPRICE;
    }

    //--------------------------------------------------------------------------------------------------

    public String getPURCHASE_DATE() {
        return PURCHASE_DATE;
    }

    public void setPURCHASE_DATE(String PURCHASE_DATE) {
        this.PURCHASE_DATE = PURCHASE_DATE;
    }

    //--------------------------------------------------------------------------------------------------

    public Long getID_BILL() {
        return ID_BILL;
    }

    public void setID_BILL(Long ID_BILL) {
        this.ID_BILL = ID_BILL;
    }

    //--------------------------------------------------------------------------------------------------

    public Long getQUANTITY() {
        return QUANTITY;
    }

    public void setQUANTITY(Long QUANTITY) {
        this.QUANTITY = QUANTITY;
    }

    //--------------------------------------------------------------------------------------------------

    public String getDOCUMENT_NUMBER() {
        return DOCUMENT_NUMBER;
    }

    public void setDOCUMENT_NUMBER(String DOCUMENT_NUMBER) {
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
    }

    //--------------------------------------------------------------------------------------------------
    public Double getTOTALPRICE() {
        return TOTALPRICE;
    }

    public void setTOTALPRICE(Double TOTALPRICE) {
        this.TOTALPRICE = TOTALPRICE;
    }

    //--------------------------------------------------------------------------------------------------






}
