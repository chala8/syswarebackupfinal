package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.ReorderV2;
import com.name.business.entities.WayToPay;
import com.name.business.entities.reorderProd;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ReorderV2Mapper implements ResultSetMapper<ReorderV2> {

    @Override
    public ReorderV2 map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ReorderV2(
                resultSet.getString("BARCODE"),
                resultSet.getString("PRODUCTO"),
                resultSet.getString("LINEA"),
                resultSet.getString("CATEGORIA"),
                resultSet.getString("FABRICANTE"),
                resultSet.getString("MARCA"),
                resultSet.getString("PRESENTACION"),
                resultSet.getLong("Cantidad Vendida"),
                resultSet.getLong("Cantidad en Inventario"),
                resultSet.getDouble("COSTO"),
                resultSet.getDouble("IVA"),
                resultSet.getLong("IDPS"),
                resultSet.getLong("IDT"),
                resultSet.getLong("ID_PROVIDER")
        );
    }
}
