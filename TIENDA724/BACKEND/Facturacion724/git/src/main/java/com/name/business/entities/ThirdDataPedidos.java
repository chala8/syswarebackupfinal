package com.name.business.entities;

public class ThirdDataPedidos {




    private Long ID_THIRD;
    private String FULLNAME;



    public ThirdDataPedidos(Long ID_THIRD,
                         String FULLNAME ) {
        this.ID_THIRD = ID_THIRD;
        this.FULLNAME  = FULLNAME;
    }



    public Long getID_THIRD() {
        return ID_THIRD;
    }

    public void setID_THIRD(Long ID_THIRD) {
        this.ID_THIRD = ID_THIRD;
    }

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }
}
