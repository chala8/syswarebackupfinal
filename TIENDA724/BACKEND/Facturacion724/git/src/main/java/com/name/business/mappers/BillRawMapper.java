package com.name.business.mappers;

import com.name.business.entities.BillRaw;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillRawMapper implements ResultSetMapper<BillRaw> {
    @Override
    public BillRaw map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BillRaw(
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("ID_THIRD_EMPLOYEE"),
                resultSet.getLong("ID_THIRD"),
                resultSet.getLong("CONSECUTIVE"),
                resultSet.getDouble("TOTALPRICE"),
                resultSet.getString("PURCHASE_DATE"),
                resultSet.getLong("ID_PAYMENT_STATE"),
                resultSet.getLong("ID_BILL_STATE"),
                resultSet.getDouble("SUBTOTAL"),
                resultSet.getDouble("TAX"),
                resultSet.getLong("ID_BILL_TYPE"),
                resultSet.getLong("ID_BILL_FATHER"),
                resultSet.getLong("ID_COMMON_STATE"),
                resultSet.getDouble("DISCOUNT"),
                resultSet.getLong("ID_DOCUMENT"),
                resultSet.getLong("ID_THIRD_DESTINITY"),
                resultSet.getLong("ID_CAJA"),
                resultSet.getLong("ID_STORE"),
                resultSet.getString("NUM_DOCUMENTO"),
                resultSet.getLong("ID_STORE_CLIENT"),
                resultSet.getString("NUM_DOCUMENTO_CLIENTE"),
                resultSet.getLong("ID_THIRD_DOMICILIARIO"),
                resultSet.getString("PURCHASE_DATE_DOCUMENT"),
                resultSet.getString("NUM_DOCUMENTO_ADICIONAL")
        );
    }
}
