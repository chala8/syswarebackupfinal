package com.name.business.businesses;

import com.name.business.DAOs.DetailBillDAO;
import com.name.business.entities.*;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.billTypeSanitation;
import static com.name.business.sanitations.EntitySanitation.detailBillSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;
import static com.name.business.utils.security.CodeGenerator.generatorConsecutive;

public class DetailBillBusiness {
   private DetailBillDAO detailBillDAO;
   private CommonStateBusiness commonStateBusiness;

    public DetailBillBusiness(DetailBillDAO detailBillDAO, CommonStateBusiness commonStateBusiness) {
        this.detailBillDAO = detailBillDAO;
        this.commonStateBusiness = commonStateBusiness;
    }

    public Either<IException, List<DetailBill>> getDetailBill(DetailBill detailBill) {
        List<String> msn = new ArrayList<>();
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting consults Detail Bill ||||||||||||||||| ");
            return Either.right(detailBillDAO.read(detailBillSanitation(detailBill)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param detailBillDTOList
     * @return
     */
    public Either<IException,Long> creatDetailBill(Long  id_bill_type ,Long id_bill, List<DetailBillDTO> detailBillDTOList){
        List<String> msn = new ArrayList<>();
        Long id_detail_bill = null;
        Long id_common_state = null;


        try {
            if (formatoLongSql(id_bill) > 0 && !validatorIDBill(id_bill)) {  /// It can be null, if The Bill does not exist, or It can given the id Biling exist

                //msn.add("It ID Bill does not exist register on  Bill, probably it has bad ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if (detailBillDTOList==null && detailBillDTOList.size()<=0){

                //msn.add("It Detail Bill does not exist for register on  Data Base, probably it has bad ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            for (DetailBillDTO detailBillDTO:detailBillDTOList ) {
                //System.out.println("|||||||||||| Starting creation Detail Bill   ||||||||||||||||| ");
                if (detailBillDTO != null) {

                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(detailBillDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                    //System.out.println("THIS IS DETAIL BILL PRICE: "+detailBillDTO.getPrice()+", AND TAX: "+detailBillDTO.getTax());
                    if(id_bill_type == 1) {

                        detailBillDAO.create2(id_common_state, id_bill, detailBillDTO);
                        id_detail_bill = detailBillDAO.getPkLast();

                    }else{

                    detailBillDAO.create(id_common_state, id_bill, detailBillDTO);
                    id_detail_bill = detailBillDAO.getPkLast();
                    }


                } else {
                    //msn.add("It does not recognize Detail Bill, probably it has bad format");
                }
            }
            if (msn.size()>0){
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            return Either.right(id_bill);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = detailBillDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }

    private Boolean validatorIDBill(Long id_bill) {
        Integer validator = 0;
        if (id_bill==null){
            return false;
        }
        try {
            validator = detailBillDAO.getValidatorIDBill(id_bill);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }

    /**
     * @param id_bill
     * @param id_detail_bill
     * @return
     */
    public Either<IException, Long> deleteDetailBill(Long id_detail_bill,Long id_bill) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        List<CommonSimple> readCommons= new ArrayList<>();
        try {

            //System.out.println("|||||||||||| Starting delete Detail  Bill ||||||||||||||||| ");

            if ((id_detail_bill != null && id_detail_bill > 0) || (id_bill != null && id_bill > 0)) {

                if ((id_detail_bill != null && id_detail_bill > 0) && (id_bill != null && id_bill > 0) ){

                    readCommons = detailBillDAO.readCommons(formatoLongSql(id_detail_bill),formatoLongSql(id_bill));

                }else  if (id_detail_bill != null && id_detail_bill > 0){
                    readCommons = detailBillDAO.readCommons(formatoLongSql(id_detail_bill),formatoLongSql(null));
                }else  if (id_bill != null && id_bill > 0){
                    readCommons = detailBillDAO.readCommons(formatoLongSql(null),formatoLongSql(id_bill));

                }else {
                    //msn.add("It does not recognize ID Detail Bill or ID Bill, probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                if (readCommons.size()>0){
                    for (CommonSimple cmSim: readCommons){
                        Either<IException, Long> deleteCommonState = commonStateBusiness.deleteCommonState(cmSim.getId_common_state());
                    }

                }

                return Either.right(id_detail_bill!=null?id_detail_bill:id_bill);


            } else {
                //msn.add("It does not recognize ID Detail Bill, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param detailBillDTOList
     * @param id_bill
     * @return
     */
    public Either<IException, Long> updateDetailBill(Long id_bill, List<DetailBillIdDTO> detailBillDTOList) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        DetailBill actualRegister=null;
        List<DetailBill> read= new ArrayList<>();

        try {
            //System.out.println("|||||||||||| Starting update Deatil Bill ||||||||||||||||| ");
            if ((id_bill != null && id_bill > 0)) {



                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorIDBill(id_bill).equals(false)){
                    //msn.add("It ID Bill State  does not exist register on  Bill , probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                for (DetailBillIdDTO detailBillIdDTO:detailBillDTOList){
                    //TODO validate the  id attribute if exist on database a register with this id
                    if (validatorID(detailBillIdDTO.getId_detail_bill()).equals(false)){
                        //msn.add("It ID Detail Bill   does not exist register on  Detail Bill, probably it has bad ");

                    }else {
                        read= detailBillDAO.read(
                                new DetailBill(detailBillIdDTO.getId_detail_bill(), id_bill,null,null,null,null,null,
                                        new CommonState(null,null,null,null))
                        );



                        if (read.size()<=0){
                            //msn.add("It does not recognize ID Bill , probably it has bad format");
                           continue;
                        }else {
                            actualRegister=read.get(0);
                        }


                        List<CommonSimple> readCommons = detailBillDAO.readCommons(formatoLongSql(detailBillIdDTO.getId_detail_bill()),null);
                        if (readCommons.size()>0){
                            idCommons = readCommons.get(0);
                            // Common  State business update
                            commonStateBusiness.updateCommonState(idCommons.getId_common_state(), detailBillIdDTO.getStateDTO());

                        }

                        if (detailBillIdDTO.getQuantity() == null || detailBillIdDTO.getQuantity()<0) {
                            detailBillIdDTO.setQuantity(actualRegister.getQuantity());
                        } else {
                            detailBillIdDTO.setQuantity(formatoIntegerSql(detailBillIdDTO.getQuantity()));
                        }

                        if (detailBillIdDTO.getPrice() == null) {
                            detailBillIdDTO.setPrice(actualRegister.getPrice());
                        } else {
                            detailBillIdDTO.setPrice(formatoDoubleSql(detailBillIdDTO.getPrice()));
                        }

                        if (detailBillIdDTO.getTax() == null) {
                            detailBillIdDTO.setTax(actualRegister.getTax());
                        } else {
                            detailBillIdDTO.setTax(formatoDoubleSql(detailBillIdDTO.getTax()));
                        }

                        if (detailBillIdDTO.getId_product_third()== null || detailBillIdDTO.getId_product_third()<0) {
                            detailBillIdDTO.setId_product_third(actualRegister.getId_product_third());
                        } else {
                            detailBillIdDTO.setId_product_third(formatoLongSql(detailBillIdDTO.getId_product_third()));
                        }

                        if (detailBillIdDTO.getTax_product() == null) {
                            detailBillIdDTO.setTax_product(actualRegister.getTax_product());
                        } else {
                            detailBillIdDTO.setTax_product(formatoDoubleSql(detailBillIdDTO.getTax_product()));
                        }



                        // Attribute update
                        detailBillDAO.update(detailBillIdDTO.getId_detail_bill(),id_bill,detailBillIdDTO);
                    }

                }
                return Either.right(id_bill);

            } else {
                //msn.add("It does not recognize ID Bill, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

}
