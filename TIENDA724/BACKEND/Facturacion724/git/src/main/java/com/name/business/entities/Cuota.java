package com.name.business.entities;

public class Cuota {

    private Long ID_CUOTA;
    private Long ID_CREDITO;
    private String FECHA_VENCIMIENTO;
    private Long VALOR;
    private Long ID_ESTADO_CUOTA;
    private String ESTADO_CUOTA;


    public Cuota(Long ID_CUOTA, Long ID_CREDITO, String FECHA_VENCIMIENTO, Long VALOR, Long ID_ESTADO_CUOTA, String ESTADO_CUOTA) {
        this.ID_CUOTA = ID_CUOTA;
        this.ID_CREDITO = ID_CREDITO;
        this.FECHA_VENCIMIENTO = FECHA_VENCIMIENTO;
        this.VALOR = VALOR;
        this.ID_ESTADO_CUOTA = ID_ESTADO_CUOTA;
        this.ESTADO_CUOTA = ESTADO_CUOTA;
    }

    public Long getID_CUOTA() {
        return ID_CUOTA;
    }

    public void setID_CUOTA(Long ID_CUOTA) {
        this.ID_CUOTA = ID_CUOTA;
    }

    public Long getID_CREDITO() {
        return ID_CREDITO;
    }

    public void setID_CREDITO(Long ID_CREDITO) {
        this.ID_CREDITO = ID_CREDITO;
    }

    public String getFECHA_VENCIMIENTO() {
        return FECHA_VENCIMIENTO;
    }

    public void setFECHA_VENCIMIENTO(String FECHA_VENCIMIENTO) {
        this.FECHA_VENCIMIENTO = FECHA_VENCIMIENTO;
    }

    public Long getVALOR() {
        return VALOR;
    }

    public void setVALOR(Long VALOR) {
        this.VALOR = VALOR;
    }

    public Long getID_ESTADO_CUOTA() {
        return ID_ESTADO_CUOTA;
    }

    public void setID_ESTADO_CUOTA(Long ID_ESTADO_CUOTA) {
        this.ID_ESTADO_CUOTA = ID_ESTADO_CUOTA;
    }

    public String getESTADO_CUOTA() {
        return ESTADO_CUOTA;
    }

    public void setESTADO_CUOTA(String ESTADO_CUOTA) {
        this.ESTADO_CUOTA = ESTADO_CUOTA;
    }

}
