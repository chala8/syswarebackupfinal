package com.name.business.entities;

public class PedidosSelect {

    private Long ID_BILL;
    private Long ID_THIRD;
    private String NUMDOCUMENTO;


    public PedidosSelect(Long ID_BILL,
                  Long ID_THIRD,
                  String NUMDOCUMENTO) {
        this.NUMDOCUMENTO = NUMDOCUMENTO;
        this.ID_BILL = ID_BILL;
        this.ID_THIRD = ID_THIRD;
    }
    //----------------------------------------------------------------------------
    public String getNUMDOCUMENTO() {
        return NUMDOCUMENTO;
    }
    public void setNUMDOCUMENTO(String NUMDOCUMENTO) {
        this.NUMDOCUMENTO = NUMDOCUMENTO;
    }
    //----------------------------------------------------------------------------
    public Long getID_BILL() {
        return ID_BILL;
    }
    public void setID_BILL(Long ID_BILL) {
        this.ID_BILL = ID_BILL;
    }
    //----------------------------------------------------------------------------
    public Long getID_THIRD() { return ID_THIRD; }
    public void setID_THIRD(Long ID_THIRD) { this.ID_THIRD = ID_THIRD; }
    //----------------------------------------------------------------------------

    }
