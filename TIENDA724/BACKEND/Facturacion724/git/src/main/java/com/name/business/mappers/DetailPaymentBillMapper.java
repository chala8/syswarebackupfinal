package com.name.business.mappers;


import com.name.business.entities.CommonState;
import com.name.business.entities.DetailPaymentBill;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailPaymentBillMapper implements ResultSetMapper<DetailPaymentBill> {

    @Override
    public DetailPaymentBill map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DetailPaymentBill(
                resultSet.getLong("ID_DETAIL_PAYMENT_BILL"),
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("ID_WAY_TO_PAY"),
                resultSet.getLong("ID_PAYMENT_METHOD"),
                resultSet.getDouble("PAYMENT_VALUE"),
                resultSet.getString("APPROBATION_CODE"),
                new CommonState(
                        resultSet.getLong("ID_CM_DET_PAY_BIL"),
                        resultSet.getInt("STATE_DET_PAY_BIL"),
                        resultSet.getDate("CREATION_DET_PAY_BIL"),
                        resultSet.getDate("UPDATE_DET_PAY_BIL")
                )
        );
    }
}
