package com.name.business.mappers;


import com.name.business.entities.PedidosMesa2;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PedidosMesa2Mapper implements ResultSetMapper<PedidosMesa2> {

    @Override
    public PedidosMesa2 map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PedidosMesa2(
                resultSet.getLong("ID_BILL"),
                resultSet.getString("PURCHASE_DATE"),
                resultSet.getLong("ID_MESA"),
                resultSet.getString("MESA"),
                resultSet.getLong("ID_THIRD_DOMICILIARIO"),
                resultSet.getDouble("TOTALPRICE"),
                resultSet.getString("MESERO"),
                resultSet.getLong("IDAPP"),
                resultSet.getLong("AMARILLO"),
                resultSet.getLong("NARANJA"),
                resultSet.getLong("AZUL"),
                resultSet.getLong("ROJO"),
                resultSet.getLong("VIOLETA"),
                resultSet.getLong("TOTAL_PRODUCTOS"),
                resultSet.getString("MESA_NUMBER"),
                resultSet.getLong("ID_STORE"),
                resultSet.getString("CLIENTE"),
                resultSet.getString("ESTADO_MESA")
        );
    }
}
