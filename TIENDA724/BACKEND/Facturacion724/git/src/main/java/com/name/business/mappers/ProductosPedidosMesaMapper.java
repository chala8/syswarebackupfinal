package com.name.business.mappers;


import com.name.business.entities.Pedido;
import com.name.business.entities.PedidosMesa;
import com.name.business.entities.ProductosPedidosMesa;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductosPedidosMesaMapper implements ResultSetMapper<ProductosPedidosMesa> {

    @Override
    public ProductosPedidosMesa map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ProductosPedidosMesa(
                resultSet.getString("PRODUCTO"),
                resultSet.getLong("ID_DETALLE_DETAIL_BILL"),
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("ID_PRODUCT_STORE"),
                resultSet.getLong("ID_BILL_STATE"),
                resultSet.getString("NOTAS"),
                resultSet.getString("FECHA_EVENTO"),
                resultSet.getString("ESTADO"),
                resultSet.getString("COLOR"),
                resultSet.getString("OWNBARCODE"),
                resultSet.getString("totalprice"),
                false,
                resultSet.getString("IMG")
        );
    }
}
