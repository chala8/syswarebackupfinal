package com.name.business.entities;

import java.util.Date;

public class DetailBill {
    private Long id_detail_bill;
    private Long id_bill; /// It can be null, if The Bill does not exist, or It can given the id Biling exist
    private Long id_product_third;
    private Integer quantity;
    private Double price;
    private Double tax_product;
    private Double tax;

    private Long id_state_detail_bill;
    private Integer state_detail_bill;
    private Date creation_detail_bill;
    private Date update_detail_bill;

    public DetailBill(Long id_detail_bill, Long id_bill, Long id_product_third, Integer quantity, Double price,
                      Double tax_product, Double tax, CommonState state) {
        this.id_detail_bill = id_detail_bill;
        this.id_bill = id_bill;
        this.id_product_third = id_product_third;
        this.quantity = quantity;
        this.price = price;
        this.tax_product = tax_product;
        this.tax = tax;


        this.id_state_detail_bill = state.getId_common_state();
        this.state_detail_bill=state.getState();
        this.creation_detail_bill = state.getCreation_date();
        this.update_detail_bill = state.getUpdate_date();
    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_detail_bill(),
                this.getState_detail_bill(),
                this.getCreation_detail_bill(),
                this.getUpdate_detail_bill()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_detail_bill =state.getId_common_state();
        this.state_detail_bill  =state.getState();
        this.creation_detail_bill =state.getCreation_date();
        this.update_detail_bill =state.getUpdate_date();
    }

    public Integer getState_detail_bill() {
        return state_detail_bill;
    }

    public void setState_detail_bill(Integer state_detail_bill) {
        this.state_detail_bill = state_detail_bill;
    }

    public Long getId_detail_bill() {
        return id_detail_bill;
    }

    public Long getId_state_detail_bill() {
        return id_state_detail_bill;
    }

    public void setId_state_detail_bill(Long id_state_detail_bill) {
        this.id_state_detail_bill = id_state_detail_bill;
    }

    public Date getCreation_detail_bill() {
        return creation_detail_bill;
    }

    public void setCreation_detail_bill(Date creation_detail_bill) {
        this.creation_detail_bill = creation_detail_bill;
    }

    public Date getUpdate_detail_bill() {
        return update_detail_bill;
    }

    public void setUpdate_detail_bill(Date update_detail_bill) {
        this.update_detail_bill = update_detail_bill;
    }

    public void setId_detail_bill(Long id_detail_bill) {
        this.id_detail_bill = id_detail_bill;
    }



    public Long getId_bill() {
        return id_bill;
    }

    public void setId_bill(Long id_bill) {
        this.id_bill = id_bill;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Double getTax_product() {
        return tax_product;
    }

    public void setTax_product(Double tax_product) {
        this.tax_product = tax_product;
    }


}
