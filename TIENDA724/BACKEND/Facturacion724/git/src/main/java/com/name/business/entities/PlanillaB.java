package com.name.business.entities;

public class PlanillaB {




    private Long ID_PLANILLA;
    private String NUM_PLANILLA;
    private String DOMICILIARIO;



    public PlanillaB    (Long ID_PLANILLA,
                         String NUM_PLANILLA,
                         String DOMICILIARIO) {
        this.DOMICILIARIO = DOMICILIARIO;
        this.ID_PLANILLA = ID_PLANILLA;
        this.NUM_PLANILLA  = NUM_PLANILLA;
    }

    public String getDOMICILIARIO() { return DOMICILIARIO; }

    public void setDOMICILIARIO(String DOMICILIARIO) { this.DOMICILIARIO = DOMICILIARIO; }

    public Long getID_PLANILLA() {
        return ID_PLANILLA;
    }

    public void setID_PLANILLA(Long ID_PLANILLA) {
        this.ID_PLANILLA = ID_PLANILLA;
    }

    public String getNUM_PLANILLA() {
        return NUM_PLANILLA;
    }

    public void setNUM_PLANILLA(String NUM_PLANILLA) {
        this.NUM_PLANILLA = NUM_PLANILLA;
    }


}
