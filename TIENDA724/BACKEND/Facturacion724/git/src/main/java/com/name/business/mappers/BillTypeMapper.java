package com.name.business.mappers;

import com.name.business.entities.BillType;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillTypeMapper implements ResultSetMapper<BillType> {
    @Override
    public BillType map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BillType(
                resultSet.getLong("ID_BILL_TYPE"),
                resultSet.getString("NAME_BIL_TY"),
                new CommonState(
                        resultSet.getLong("ID_CM_BIL_TY"),
                        resultSet.getInt("STATE_CM_BIL_TY"),
                        resultSet.getDate("CREATION_BIL_TY"),
                        resultSet.getDate("UPDATE_BIL_TY")
                )
        );
    }
}
