package com.name.business.entities;

import java.util.Date;

public class DetailData {
    private Long QUANTITY;
    private Long ID_PRODUCT_STORE;

    public DetailData(Long QUANTITY, Long ID_PRODUCT_STORE) {
        this.QUANTITY = QUANTITY;
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;    }

    public Long getQUANTITY() {
        return QUANTITY;
    }

    public void setQUANTITY( Long QUANTITY) {
        this.QUANTITY = QUANTITY;
    }

    public Long getID_PRODUCT_STORE() {
        return ID_PRODUCT_STORE;
    }

    public void setID_PRODUCT_STORE( Long ID_PRODUCT_STORE) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
    }
}
