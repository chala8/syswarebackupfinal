package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.skife.jdbi.v2.sqlobject.Bind;

import java.util.Date;
import java.util.List;

public class PaymentDetailDTO {
    private String PAYMENTVALUE;
    private String IDBILL;
    private String IDPAYMENTMETHOD;
    private String APPROBATIONCODE;
    private String IDWAYTOPAY;
    private String IDBANKENTITY;

    @JsonCreator
    public PaymentDetailDTO(@JsonProperty("PAYMENTVALUE") String PAYMENTVALUE,
                            @JsonProperty("IDBILL") String IDBILL,
                            @JsonProperty("IDPAYMENTMETHOD") String IDPAYMENTMETHOD,
                            @JsonProperty("APPROBATIONCODE") String APPROBATIONCODE,
                            @JsonProperty("IDWAYTOPAY") String IDWAYTOPAY,
                            @JsonProperty("IDBANKENTITY") String IDBANKENTITY) {
        this.PAYMENTVALUE = PAYMENTVALUE;
        this.IDBILL = IDBILL;
        this.IDPAYMENTMETHOD = IDPAYMENTMETHOD;
        this.APPROBATIONCODE = APPROBATIONCODE;
        this.IDWAYTOPAY = IDWAYTOPAY;
        this.IDBANKENTITY = IDBANKENTITY;
    }


    public String getPAYMENTVALUE() {
        return PAYMENTVALUE;
    }

    public void setPAYMENTVALUE(String PAYMENTVALUE) {
        this.PAYMENTVALUE = PAYMENTVALUE;
    }

    public String getIDBILL() {
        return IDBILL;
    }

    public void setIDBILL(String IDBILL) {
        this.IDBILL = IDBILL;
    }

    public String getIDPAYMENTMETHOD() {
        return IDPAYMENTMETHOD;
    }

    public void setIDPAYMENTMETHOD(String IDPAYMENTMETHOD) {
        this.IDPAYMENTMETHOD = IDPAYMENTMETHOD;
    }

    public String getAPPROBATIONCODE() {
        return APPROBATIONCODE;
    }

    public void setAPPROBATIONCODE(String APPROBATIONCODE) {
        this.APPROBATIONCODE = APPROBATIONCODE;
    }

    public String getIDWAYTOPAY() {
        return IDWAYTOPAY;
    }

    public void setIDWAYTOPAY(String IDWAYTOPAY) {
        this.IDWAYTOPAY = IDWAYTOPAY;
    }

    public String getIDBANKENTITY() {
        return IDBANKENTITY;
    }

    public void setIDBANKENTITY(String IDBANKENTITY) {
        this.IDBANKENTITY = IDBANKENTITY;
    }
}
