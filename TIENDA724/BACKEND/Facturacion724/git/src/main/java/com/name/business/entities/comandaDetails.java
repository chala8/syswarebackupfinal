package com.name.business.entities;

public class comandaDetails {

    private String product_store_name;
    private String cantidad;

    public comandaDetails(String product_store_name, String cantidad) {
        this.product_store_name = product_store_name;
        this.cantidad = cantidad;
    }

    public String getProduct_store_name() {
        return product_store_name;
    }

    public void setProduct_store_name(String product_store_name) {
        this.product_store_name = product_store_name;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }
}
