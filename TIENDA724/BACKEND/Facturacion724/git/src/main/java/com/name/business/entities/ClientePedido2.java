package com.name.business.entities;

import java.util.Date;

public class ClientePedido2 {
    private String NAME;
    private String DOCUMENT_NUMBER;
    private String NUM_DOCUMENT;
    private String PURCHASE_DATE;
    private String DESCRIPTION;
    private String PHONE;
    private String MAIL;
    private String ADDRESS;

    public ClientePedido2(
             String NAME,
             String DOCUMENT_NUMBER,
             String NUM_DOCUMENT,
             String PURCHASE_DATE,
             String DESCRIPTION,
             String PHONE,
             String MAIL,
             String ADDRESS) {
        this.NAME = NAME;
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
        this.NUM_DOCUMENT = NUM_DOCUMENT;
        this.DESCRIPTION = DESCRIPTION;
        this.PHONE = PHONE;
        this.PURCHASE_DATE = PURCHASE_DATE;
        this.MAIL = MAIL;
        this.ADDRESS = ADDRESS;
    }
    //--------------------------------------------------------------------------------------------------

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }
    //--------------------------------------------------------------------------------------------------

    public String getDOCUMENT_NUMBER() {
        return DOCUMENT_NUMBER;
    }

    public void setDOCUMENT_NUMBER(String DOCUMENT_NUMBER) {
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
    }
    //--------------------------------------------------------------------------------------------------

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }
    //--------------------------------------------------------------------------------------------------

    public String getMAIL() {
        return MAIL;
    }

    public void setMAIL(String MAIL) {
        this.MAIL = MAIL;
    }


    //--------------------------------------------------------------------------------------------------

    public String getPURCHASE_DATE() {
        return PURCHASE_DATE;
    }

    public void setPURCHASE_DATE(String PURCHASE_DATE) {
        this.PURCHASE_DATE = PURCHASE_DATE;
    }

    //--------------------------------------------------------------------------------------------------

    public String getPHONE() {
        return PHONE;
    }

    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    //--------------------------------------------------------------------------------------------------

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    //--------------------------------------------------------------------------------------------------

    public String getNUM_DOCUMENT() {
        return NUM_DOCUMENT;
    }

    public void setNUM_DOCUMENT(String NUM_DOCUMENT) {
        this.NUM_DOCUMENT = NUM_DOCUMENT;
    }

    //--------------------------------------------------------------------------------------------------

}