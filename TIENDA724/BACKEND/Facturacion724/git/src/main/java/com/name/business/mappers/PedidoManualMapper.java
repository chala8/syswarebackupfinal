package com.name.business.mappers;


import com.name.business.entities.Pedido;
import com.name.business.entities.PedidosManuales;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PedidoManualMapper implements ResultSetMapper<PedidosManuales> {

    @Override
    public PedidosManuales map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PedidosManuales(
                resultSet.getLong("ID_TAX"),
                resultSet.getDouble("PRICE"),
                resultSet.getLong("ID_PRODUCT_STORE"),
                resultSet.getString("PRODUCT_STORE_NAME"),
                resultSet.getString("OWNBARCODE"),
                resultSet.getString("PRODUCT_STORE_CODE"),
                resultSet.getDouble("TAX"),
                resultSet.getLong("ID_PROVIDER"),
                resultSet.getString("PROVIDER"),
                resultSet.getLong("QUANTITY")
        );
    }
}
