package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.LegalData;
import com.name.business.entities.MargenData;
import com.name.business.entities.mesaState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class mesaStateMapper implements ResultSetMapper<mesaState> {
    @Override
    public mesaState map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new mesaState(
                resultSet.getString("estado_mesa"),
                resultSet.getString("token")
        );
    }
}
