package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.BillTypeBusiness;
import com.name.business.entities.BillType;
import com.name.business.entities.CommonState;
import com.name.business.representations.BillTypeDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/billing-type")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BillTypeResource {
  private BillTypeBusiness billTypeBusiness;

    public BillTypeResource(BillTypeBusiness billTypeBusiness) {
        this.billTypeBusiness = billTypeBusiness;
    }

    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getBillTypeResource(
            @QueryParam("id_bill_type") Long id_bill_type,
            @QueryParam("name_bill_type") String name_bill_type,

            @QueryParam("id_state_bill_type") Long id_state_bill_type,
            @QueryParam("state_bill_type") Integer state_bill_type,
            @QueryParam("creation_bill_type") Date creation_bill_type,
            @QueryParam("update_bill_type") Date update_bill_type){

        Response response;

        Either<IException, List<BillType>> getResponseGeneric = billTypeBusiness.getBillType(
                new BillType(id_bill_type, name_bill_type,
                        new CommonState(id_state_bill_type, state_bill_type,
                                creation_bill_type, update_bill_type)
                )
        );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postBillTypeResource(BillTypeDTO billTypeDTO){
        Response response;

        Either<IException, Long> getResponseGeneric = billTypeBusiness.createBillType(billTypeDTO);


        if (getResponseGeneric.isRight()){

            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateProductResource(@PathParam("id") Long id_bill_type, BillTypeDTO billTypeDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = billTypeBusiness.updateBillType(id_bill_type, billTypeDTO);

        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deleteBillTypeResource(@PathParam("id") Long id_bill_type){
        Response response;
        Either<IException, Long> getResponseGeneric = billTypeBusiness.deleteBillType(id_bill_type);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }
}
