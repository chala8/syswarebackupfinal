package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.credito;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CreditoMapper implements ResultSetMapper<credito> {

    @Override
    public credito map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new credito(
                resultSet.getLong("ID_CREDITO"),
                resultSet.getString("FECHA_CREACION"),
                resultSet.getLong("ID_ESTADO_CREDITO"),
                resultSet.getLong("MONTO"),
                resultSet.getString("NUM_CUOTAS"),
                resultSet.getString("INTERES"),
                resultSet.getString("INTERES_MORA"),
                resultSet.getString("PERDIODICIDAD_DIAS"),
                resultSet.getString("FECHA_APROBACION"),
                resultSet.getString("FECHA_DESEMBOLSO"),
                resultSet.getString("CUOTA_INICIAL"),
                resultSet.getLong("ID_BILL_FACTURA"),
                resultSet.getLong("ID_THIRD_CLIENT"),
                resultSet.getLong("ID_THIRD_PROVIDER"),
                resultSet.getLong("ID_THIRD_CODEUDOR"),
                resultSet.getString("cliente"),
                resultSet.getString("doccliente"),
                resultSet.getString("dircliente"),
                resultSet.getString("phonecliente"),
                resultSet.getString("ciudadCliente"),
                resultSet.getString("codeudor"),
                resultSet.getString("doccodeudor"),
                resultSet.getString("dircodeudor"),
                resultSet.getString("phonecodeudor"),
                resultSet.getString("ciudadCodeudor"),
                resultSet.getString("estado_credito")
        );
    }
}
