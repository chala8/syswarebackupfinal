package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.reorderProd;
import org.glassfish.jersey.server.JSONP;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReorderDTO {
    private List<reorderProd>  reorder = new ArrayList<>();
    private Long idstore;

    @JsonCreator
    public ReorderDTO(@JsonProperty("reorder") List<reorderProdDTO> reordere,
                      @JsonProperty("idstore") Long idstore) {

        for(reorderProdDTO element: reordere){
            reorderProd e = new reorderProd(element.getPERCENT(),element.getOWNBARCODE(),element.getPRODUCTO(), element.getLINEA(), element.getCATEGORIA(), element.getMARCA(),
                                            element.getPRESENTACION(),element.getCOSTO(), element.getCANTIDAD());
            //System.out.println(e.getCOSTO());
            //System.out.println(e.getCANTIDAD());
            //System.out.println(e.getPRESENTACION());
            //System.out.println(e.getMARCA());
            //System.out.println(e.getLINEA());
            //System.out.println(e.getCATEGORIA());
            //System.out.println(e.getPRODUCTO());
            //System.out.println(e.getOWNBARCODE());
            //System.out.println("***********************************************************");
            reorder.add(e);
        }
        this.idstore = idstore;
    }

    public List<reorderProd> getreorder() {
        return reorder;
    }

    public void setreorder(List<reorderProd> reorder) { this.reorder = reorder; }

    public Long getidstore() {
        return idstore;
    }

    public void setidstore(Long idstore) { this.idstore = idstore; }

}
