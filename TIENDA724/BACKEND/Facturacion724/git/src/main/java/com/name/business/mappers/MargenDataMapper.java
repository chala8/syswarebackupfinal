package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.LegalData;
import com.name.business.entities.MargenData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MargenDataMapper implements ResultSetMapper<MargenData> {
    @Override
    public MargenData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new MargenData(
                resultSet.getDouble("PRICE"),
                resultSet.getDouble("STANDARD_PRICE")
        );
    }
}
