package com.name.business.entities;

public class detallePdf {
    private String CODIGO;
    private String CODIGOPROV;
    private String ID_STORE;
    private String PRODUCTO;
    private String CANTIDAD;
    private String VALORCOMPRA;
    private String DESCUENTO;
    private String IVA;
    private String TOTAL;
    private String TOTALCONIVA;

    public detallePdf(String CODIGO, String CODIGOPROV, String ID_STORE, String PRODUCTO, String CANTIDAD, String VALORCOMPRA, String DESCUENTO, String IVA, String TOTAL, String TOTALCONIVA) {
        this.CODIGO = CODIGO;
        this.CODIGOPROV = CODIGOPROV;
        this.ID_STORE = ID_STORE;
        this.PRODUCTO = PRODUCTO;
        this.CANTIDAD = CANTIDAD;
        this.VALORCOMPRA = VALORCOMPRA;
        this.DESCUENTO = DESCUENTO;
        this.IVA = IVA;
        this.TOTAL = TOTAL;
        this.TOTALCONIVA = TOTALCONIVA;
    }

    public String getCODIGO() {
        return CODIGO;
    }

    public void setCODIGO(String CODIGO) {
        this.CODIGO = CODIGO;
    }

    public String getCODIGOPROV() {
        return CODIGOPROV;
    }

    public void setCODIGOPROV(String CODIGOPROV) {
        this.CODIGOPROV = CODIGOPROV;
    }

    public String getID_STORE() {
        return ID_STORE;
    }

    public void setID_STORE(String ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    public String getPRODUCTO() {
        return PRODUCTO;
    }

    public void setPRODUCTO(String PRODUCTO) {
        this.PRODUCTO = PRODUCTO;
    }

    public String getCANTIDAD() {
        return CANTIDAD;
    }

    public void setCANTIDAD(String CANTIDAD) {
        this.CANTIDAD = CANTIDAD;
    }

    public String getVALORCOMPRA() {
        return VALORCOMPRA;
    }

    public void setVALORCOMPRA(String VALORCOMPRA) {
        this.VALORCOMPRA = VALORCOMPRA;
    }

    public String getDESCUENTO() {
        return DESCUENTO;
    }

    public void setDESCUENTO(String DESCUENTO) {
        this.DESCUENTO = DESCUENTO;
    }

    public String getIVA() {
        return IVA;
    }

    public void setIVA(String IVA) {
        this.IVA = IVA;
    }

    public String getTOTAL() {
        return TOTAL;
    }

    public void setTOTAL(String TOTAL) {
        this.TOTAL = TOTAL;
    }

    public String getTOTALCONIVA() {
        return TOTALCONIVA;
    }

    public void setTOTALCONIVA(String TOTALCONIVA) {
        this.TOTALCONIVA = TOTALCONIVA;
    }
}
