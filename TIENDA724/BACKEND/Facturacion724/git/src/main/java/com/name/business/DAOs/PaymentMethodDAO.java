package com.name.business.DAOs;

import com.name.business.entities.CommonSimple;
import com.name.business.entities.PaymentMethod;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.PaymentMethodMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(PaymentMethodMapper.class)
public interface PaymentMethodDAO {

    @SqlQuery("SELECT ID_PAYMENT_METHOD FROM PAYMENT_METHOD WHERE ID_PAYMENT_METHOD IN (SELECT MAX( ID_PAYMENT_METHOD ) FROM PAYMENT_METHOD )\n")
    Long getPkLast();

    @SqlQuery("SELECT COUNT(ID_PAYMENT_METHOD) FROM PAYMENT_METHOD WHERE ID_PAYMENT_METHOD = :id\n")
    Integer getValidatorID(@Bind("id") Long id);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_PAYMENT_METHOD  ID, ID_COMMON_STATE FROM PAYMENT_METHOD WHERE( ID_PAYMENT_METHOD = :id OR :id IS NULL)\n ")
    List<CommonSimple> readCommons(@Bind("id") Long id);


    @SqlQuery("SELECT * FROM V_PAYMENTMETHOD V_PAY_MET \n" +
            "WHERE (V_PAY_MET.ID_PAYMENT_METHOD=:pay_met.id_payment_method OR :pay_met.id_payment_method IS NULL ) AND\n" +
            "      (V_PAY_MET.NAME_PAY_ME LIKE :pay_met.name_payment_method OR :pay_met.name_payment_method IS NULL ) AND\n" +
            "      (V_PAY_MET.ID_CM_PAY_MET=:pay_met.id_state_payment_method OR :pay_met.id_state_payment_method IS NULL ) AND\n" +
            "      (V_PAY_MET.STATE_PAY_ME =:pay_met.state_payment_method OR :pay_met.state_payment_method IS NULL ) AND\n" +
            "      (V_PAY_MET.CREATION_PAY_ME =:pay_met.creation_payment_method OR :pay_met.creation_payment_method IS NULL ) AND\n" +
            "      (V_PAY_MET.UPDATE_PAY_ME= :pay_met.update_payment_method OR :pay_met.update_payment_method IS NULL )")
    List<PaymentMethod> read(@BindBean("pay_met") PaymentMethod paymentMethod);


    @SqlUpdate("INSERT INTO PAYMENT_METHOD ( ID_COMMON_STATE,NAME) \n " +
            "VALUES (:id_cm_st,:name)")
    void create(@Bind("id_cm_st") Long id_common_state,@Bind("name") String s);


    @SqlUpdate("UPDATE PAYMENT_METHOD SET\n " +
            "    NAME = :name\n " +
            "    WHERE (ID_PAYMENT_METHOD =  :id_pay_met) ")
    int update(@Bind("id_pay_met") Long id_pay_met,@Bind("name") String s);


    int delete();


    int deletePermanent();
}
