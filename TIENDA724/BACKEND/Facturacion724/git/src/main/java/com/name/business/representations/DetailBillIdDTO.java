package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DetailBillIdDTO {

    private Long id_detail_bill;
    private Integer quantity;
    private Double price;
    private Double tax;
    private Long id_product_third;
    private Double tax_product;
    private CommonStateDTO stateDTO;


    @JsonCreator
    public DetailBillIdDTO(@JsonProperty("id_detail_bill") Long id_detail_bill,@JsonProperty("quantity") Integer quantity,
                           @JsonProperty("price") Double price, @JsonProperty("tax") Double tax,
                           @JsonProperty("id_product_third") Long id_product_third,
                           @JsonProperty("tax_product") Double tax_product, @JsonProperty("state") CommonStateDTO stateDTO) {

        this.id_detail_bill=id_detail_bill;
        this.quantity = quantity;
        this.price = price;
        this.tax = tax;
        this.id_product_third = id_product_third;
        this.tax_product = tax_product;
        this.stateDTO = stateDTO;
    }

    public Long getId_detail_bill() {
        return id_detail_bill;
    }

    public void setId_detail_bill(Long id_detail_bill) {
        this.id_detail_bill = id_detail_bill;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Long getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Long id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Double getTax_product() {
        return tax_product;
    }

    public void setTax_product(Double tax_product) {
        this.tax_product = tax_product;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
