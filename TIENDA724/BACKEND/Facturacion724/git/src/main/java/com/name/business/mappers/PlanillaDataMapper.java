package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PlanillaDataMapper  implements ResultSetMapper<PlanillaData> {

    @Override
    public PlanillaData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PlanillaData(
                resultSet.getString("FECHA_INICIO"),
                resultSet.getString("NOTES"),
                resultSet.getString("DOMICILIARIO"),
                resultSet.getLong("ID_PLANILLA"),
                resultSet.getString("NUMPLANILLA"),
                resultSet.getString("STATUS"),
                resultSet.getString("NUMFACTURA"),
                resultSet.getString("NUMPEDIDO"),
                resultSet.getDouble("TOTALPRICE"),
                resultSet.getLong("ID_BILL")
        );
    }
}
