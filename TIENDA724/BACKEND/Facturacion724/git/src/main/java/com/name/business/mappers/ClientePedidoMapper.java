package com.name.business.mappers;

import com.name.business.entities.ClientePedido;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientePedidoMapper implements ResultSetMapper<ClientePedido> {
    @Override
    public ClientePedido map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ClientePedido(
                resultSet.getString("NUM_DOCUMENTO"),
                resultSet.getString("PURCHASE_DATE"),
                resultSet.getString("DESCRIPTION"),
                resultSet.getString("PHONE"),
                resultSet.getString("MAIL"),
                resultSet.getString("ADDRESS")
        );
    }
}
