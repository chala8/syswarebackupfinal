package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.DocumentoCredito;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DocumentoCreditoMapper implements ResultSetMapper<DocumentoCredito> {

    @Override
    public DocumentoCredito map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DocumentoCredito(
                resultSet.getString("NOMBRE"),
                resultSet.getString("RUTA"),
                resultSet.getString("TIPO_DOCUMENTO"),
                resultSet.getString("id_credito_documento")

        );
    }
}
