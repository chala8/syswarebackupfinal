package com.name.business.mappers;

import com.name.business.entities.Abono;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AbonoMapper implements ResultSetMapper<Abono> {

    @Override
    public Abono map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Abono(
                resultSet.getLong("ID_ABONO_CUOTA_CREDITO"),
                resultSet.getLong("ID_CUOTA"),
                resultSet.getDate("FECHA_ABONO"),
                resultSet.getLong("VALOR_ABONO"),
                resultSet.getLong("ID_CREDITO"),
                resultSet.getString("paymentmethod")
        );
    }
}
