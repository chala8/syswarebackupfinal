package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.BillCompleteMapper;
import com.name.business.mappers.PricePerProdMapper;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.representations.BillDTO;
import com.name.business.representations.BillDetailIDDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(PricePerProdMapper.class)
public interface RefundDAO {

    @SqlQuery(" select id_product_third,p.standard_price,c.description" +
            " from tienda724.price_list p,tienda724.common c" +
            " where p.id_common=c.id_common " +
            " and p.id_product_third= :id_product_third")
    List <Refund> getPricePerProd(@Bind("id_product_third") int id_product_third);



    @SqlUpdate("UPDATE DOCUMENT " +
            " SET  BODY = :body " +
            " WHERE ID_DOCUMENT = :id_document ")
    void updateDocument(@Bind("id_document") int id_document, @Bind("body") String body);



    @SqlUpdate(" update bill " +
            " set id_bill_state=:id_bill_state " +
            " where id_bill=:id_bill ")
    void updateBill(@Bind("id_bill") int id_bill, @Bind("id_bill_state") int id_bill_state);



    @SqlQuery(" Select id_product_store from tienda724.product_store " +
            " Where id_code=:code and id_store=:id_store ")
    Long getProductStoreByCodeStore(@Bind("code") Long code,
                                    @Bind("id_store") Long id_store);


    @SqlQuery("Select id_storage from tienda724.product_store_inventory " +
            " Where id_product_store=:id_product_store and rownum<2 ")
    Long getStorageByProductStore(@Bind("id_product_store") Long id_product_store);


    /*@SqlQuery("Select quantity from tienda724.product_store_inventory " +
            " Where id_product_store=:id_product_store and rownum<2 ")
    Long getQuantityByProductStore(@Bind("id_product_store") Long id_product_store);*/

    @SqlQuery("Select quantity from tienda724.product_store_inventory " +
                     " Where id_product_store=:id_product_store and id_storage = :id_storage ")
    Long getQuantityByProductStorage(@Bind("id_product_store") Long id_product_store,@Bind("id_storage") Long id_storage);

    @SqlQuery("Select ID_CODE from tienda724.codes " +
            " Where code=:code")
    Long getIDCODEBycode(@Bind("code") String code);



    @SqlUpdate(" update tienda724.product_store_inventory " +
            " set quantity =:quantity " +
            " where id_product_store=:id_product_store and id_storage=:id_storage")
    void putInventoryBill(@Bind("quantity") Long quantity,
                          @Bind("id_product_store") Long id_product_store,
                          @Bind("id_storage") Long id_storage);




    int delete();
    int deletePermanent();
}
