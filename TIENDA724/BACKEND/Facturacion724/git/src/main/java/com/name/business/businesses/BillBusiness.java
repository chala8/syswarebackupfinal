package com.name.business.businesses;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.name.business.DAOs.*;
import com.name.business.entities.*;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Array;
import fj.data.Either;
import org.skife.jdbi.v2.sqlobject.Bind;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.*;
import java.text.NumberFormat;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.billCompleteSanitation;
import static com.name.business.sanitations.EntitySanitation.billSanitation;
import static com.name.business.utils.PDFHelpers.*;
import static com.name.business.utils.EmailHelpers.*;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;
import static com.name.business.utils.security.CodeGenerator.generatorConsecutive;
import static java.lang.Math.round;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Base64;

public class BillBusiness {
    private BillDAO billDAO;
    private CommonStateBusiness commonStateBusiness;
    private PaymentStateBusiness paymentStateBusiness;
    private BillStateBusiness billStateBusiness;
    private BillTypeBusiness billTypeBusiness;
    private DetailBillBusiness detailBillBusiness;
    private DetailPaymentBillBusiness detailPaymentBillBusiness;
    public PedidosBusiness pedidosBusiness;

    private boolean PruebasLocales = false;

    @SuppressWarnings("FieldCanBeLocal")
    private String URI_Terceros = System.getenv("SYSWARE_SERVER").equals("prod") ? "https://tienda724.com:8446/v1/":"https://pruebas.tienda724.com:8446/v1/";
    //private String URI_Terceros = "http://localhost:8446/v1/";

    public BillBusiness(BillDAO billDAO, CommonStateBusiness commonStateBusiness, PaymentStateBusiness paymentStateBusiness,
                        BillStateBusiness billStateBusiness, BillTypeBusiness billTypeBusiness,
                        DetailBillBusiness detailBillBusiness,
                        DetailPaymentBillBusiness detailPaymentBillBusiness) {
        this.billDAO = billDAO;
        this.commonStateBusiness = commonStateBusiness;
        this.paymentStateBusiness = paymentStateBusiness;
        this.billStateBusiness = billStateBusiness;
        this.billTypeBusiness = billTypeBusiness;
        this.detailBillBusiness = detailBillBusiness;
        this.detailPaymentBillBusiness = detailPaymentBillBusiness;
        try{
            if(System.getenv("SYSWARE_DEVPC").equals("yes")){this.PruebasLocales = true;}
        }catch (Exception e){
            this.PruebasLocales = false;
            ////System.out.println("Can't Get SYSWARE_DEVPC envr var");
        }
    }

    public Either<IException, Long> insertBillv2(BillDTOv2 billdto) {
        try {
            //System.out.println("|||||||||||| SE INSERTA EL HEADER DEL BILL ||||||||||||||||| ");
            if(billdto.getNum_documento_factura().equals("0")){
                //System.out.println("ENTRE 1");
                billDAO.procedureInsertBill(billdto.getidthirdemployee(),billdto.getidthird(),billdto.getidbilltype(),billdto.getnotes(),billdto.getidthirddestinity(),billdto.getidcaja(),billdto.getidstore(),billdto.getidthirddomiciliario(),billdto.getidpaymentmethod(),billdto.getidwaytopay(),billdto.getapprovalcode(),billdto.getidbankentity(),billdto.getidbillstate(), billdto.getDetails(),billdto.getdisccount(),null,billdto.getIdthirdvendedor());

            }else{
                //System.out.println("ENTRE 2");
                billDAO.procedureInsertBill(billdto.getidthirdemployee(),billdto.getidthird(),billdto.getidbilltype(),billdto.getnotes(),billdto.getidthirddestinity(),billdto.getidcaja(),billdto.getidstore(),billdto.getidthirddomiciliario(),billdto.getidpaymentmethod(),billdto.getidwaytopay(),billdto.getapprovalcode(),billdto.getidbankentity(),billdto.getidbillstate(), billdto.getDetails(),billdto.getdisccount(),billdto.getNum_documento_factura(),billdto.getIdthirdvendedor());

            }//System.out.println("|||||||||||| SE OBTIENE EL ID CON EL QUE SE INSERTO EL BILL ||||||||||||||||| ");
            Long id_bill = billDAO.getPkLastV23(billdto.getidstore());
            //System.out.println();
            /*//System.out.println("|||||||||||| SE RECORREN LOS DETALLES Y SE VAN INSERTANDO ||||||||||||||||| ");
            for(int i = 0; i < details.size();i++){

                DetailBillDTOv2 thisDetail = details.get(i);
                billDAO.procedureInsertDetailBill(id_bill,thisDetail.getcantidad(),thisDetail.getvalor(),thisDetail.getidproductstore(),thisDetail.getid_impuesto(),billdto.getidbilltype(),billdto.getidbillstate());

            }
            //System.out.println("|||||||||||| SE ACTUALIZAN LOS VALORES DEL IDBILL PARA QUE FUNCIONE CORRECTAMENTE ||||||||||||||||| ");
            billDAO.procedure2(id_bill,billdto.getdisccount());*/
            //System.out.println("|||||||||||| SE RETORNA EL ID_BILL EN CASO DE QUE TODO FUNCIONE CORRECTAMENTE ||||||||||||||||| ");
            return Either.right(id_bill);
        } catch (Exception e) {
            String exception = getStackTrace(e);
            if(exception.contains("ORA-20594")){
                e.printStackTrace();
                return Either.right(new Long(-2));
            }else{
                e.printStackTrace();
                return Either.right(new Long(0));
            }
        }
    }


    public static String getStackTrace(final Throwable throwable) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);
        return sw.getBuffer().toString();
    }


    public Either<IException, List<BillDTOv2>> insertBillsv2(billsV2DTO facts ) {


        //noinspection unchecked
        @SuppressWarnings("rawtypes") List<BillDTOv2> failedBills = new ArrayList();
        List<BillDTOv2> bills = facts.getbills();
        List<Date> dateList = facts.getdateList();


            int cont = 0;
            for (BillDTOv2 billdto:bills) {
                try {
                    //System.out.println("|||||||||||| SE INSERTA EL HEADER DEL BILL ||||||||||||||||| ");
                    //noinspection NewObjectEquality,NumberEquality
                    if(new Long(billdto.getNum_documento_factura()) == new Long(0)){
                        //System.out.println("ENTRE 1");
                        billDAO.procedureInsertBill(billdto.getidthirdemployee(),billdto.getidthird(),billdto.getidbilltype(),billdto.getnotes(),billdto.getidthirddestinity(),billdto.getidcaja(),billdto.getidstore(),billdto.getidthirddomiciliario(),billdto.getidpaymentmethod(),billdto.getidwaytopay(),billdto.getapprovalcode(),billdto.getidbankentity(),billdto.getidbillstate(), billdto.getDetails(),billdto.getdisccount(),billdto.getNum_documento_factura(),new Long(-1));

                    }else{
                        //System.out.println("ENTRE 1");
                        billDAO.procedureInsertBill(billdto.getidthirdemployee(),billdto.getidthird(),billdto.getidbilltype(),billdto.getnotes(),billdto.getidthirddestinity(),billdto.getidcaja(),billdto.getidstore(),billdto.getidthirddomiciliario(),billdto.getidpaymentmethod(),billdto.getidwaytopay(),billdto.getapprovalcode(),billdto.getidbankentity(),billdto.getidbillstate(), billdto.getDetails(),billdto.getdisccount(),billdto.getNum_documento_factura(),new Long(-1));

                    }
                    //System.out.println("|||||||||||| SE OBTIENE EL ID CON EL QUE SE INSERTO EL BILL ||||||||||||||||| ");
                    Long id_bill = billDAO.getPkLastV2(billdto.getidstore());
                    //System.out.println();
                    /*//System.out.println("|||||||||||| SE RECORREN LOS DETALLES Y SE VAN INSERTANDO ||||||||||||||||| ");
                    for(int i = 0; i < details.size();i++){

                        DetailBillDTOv2 thisDetail = details.get(i);
                        billDAO.procedureInsertDetailBill(id_bill,thisDetail.getcantidad(),thisDetail.getvalor(),thisDetail.getidproductstore(),thisDetail.getid_impuesto(),billdto.getidbilltype(),billdto.getidbillstate());

                    }
                    //System.out.println("|||||||||||| SE ACTUALIZAN LOS VALORES DEL IDBILL PARA QUE FUNCIONE CORRECTAMENTE ||||||||||||||||| ");
                    billDAO.procedure2(id_bill,billdto.getdisccount());*/
                    //System.out.println("|||||||||||| SE IMPRIME EL ID_BILL EN CASO DE QUE TODO FUNCIONE CORRECTAMENTE ||||||||||||||||| ");
                    //System.out.println(id_bill);

                    billDAO.updateBillDateCont(id_bill, dateList.get(cont));
                    billDAO.validatePdfDrawing(id_bill);
                } catch (Exception e) {
                    e.printStackTrace();
                    failedBills.add(billdto);
                }
            }

        return Either.right(failedBills);
    }



    public Either<IException, Double> getSalesCaja(Long id_cierre_caja) {
        try {
            return Either.right(billDAO.getSalesCaja(id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Long procedure2(Long idbill)
    {

        // Check if the word is present in string
        // If found, remove it using removeAll()


        billDAO.procedure2(idbill,new Long(0));


        // Return the resultant string
        return new Long(1);
    }




    public Long deleteBillPro(Long id_caja)
    {

        // Check if the word is present in string
        // If found, remove it using removeAll()


        billDAO.deleteBillPro(id_caja);


        // Return the resultant string
        return new Long(1);
    }


    public Long procedureCrearPaymentDetail(Long idbill)
    {
        Long response = new Long(0);
        // Check if the word is present in string
        // If found, remove it using removeAll()
        try{
            billDAO.procedureCrearPaymentDetail(idbill);


            // Return the resultant string

            response = new Long(1);

        }catch(Exception e){
            response = new Long(0);
        }

        return response;

    }


    public Long facturarPedidoMesa(Long idthirdemployee,
                                   Long idthird,
                                   Long idbilltype,
                                   String notes,
                                   Long idthirddestinity,
                                   Long idcaja,
                                   Long idstore,
                                   Long idthirddomiciliario,
                                   Long idpaymentmethod,
                                   Long idwaytopay,
                                   String approvalcode,
                                   Long idbankentity,
                                   Long idbillstate,
                                   String detallesbill,
                                   Long descuento,
                                   String numdocumentoadicional,
                                   Long idthirdvendedor,
                                   String detallespago,
                                   Long idbillpedido,
                                   String nota,
                                   Long idthirduser,
                                   String actor)
    {
        try{
            billDAO.facturarPedidoMesa(  idthirdemployee,
                                         idthird,
                                         idbilltype,
                                         notes,
                                         idthirddestinity,
                                         idcaja,
                                         idstore,
                                         idthirddomiciliario,
                                         idpaymentmethod,
                                         idwaytopay,
                                         approvalcode,
                                         idbankentity,
                                         idbillstate,
                                         detallesbill,
                                         descuento,
                                         numdocumentoadicional,
                                         idthirdvendedor,
                                         detallespago,
                                         idbillpedido,
                                         nota,
                                         idthirduser,
                                         actor);


            // Return the resultant string

            return billDAO.getPkLastV2(idstore);

        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }


    }


    public Long procedureValidarBill(Long idbill)
    {
        Long response = new Long(0);
        // Check if the word is present in string
        // If found, remove it using removeAll()
        try{
            billDAO.procedureValidarBill(idbill);


            // Return the resultant string

            response = new Long(1);

        }catch(Exception e){
           response = new Long(0);
        }

        return response;

    }




    public Long crearPedidoMesaV2(Long idstoreclient,
                                  Long idthirduseraapp,
                                  Long idstoreprov,
                                  String detallepedido,
                                  Long descuento,
                                  Long idpaymentmethod,
                                  Long idapplication,
                                  Long idthirdemp,
                                  String detallepedidomesa,
                                  Long idmesa)
    {
        // Check if the word is present in string
        // If found, remove it using removeAll()
        try{
            billDAO.crearPedidoMesaV2( idstoreclient,
                     idthirduseraapp,
                     idstoreprov,
                     detallepedido,
                     descuento,
                     idpaymentmethod,
                     idapplication,
                     idthirdemp,
                     detallepedidomesa,
                     idmesa);



            // Return the resultant string

            return new Long(1);

        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }

    }


    public Long crearPedidoMesa(Long idstoreclient,
                                Long idthirduseraapp,
                                Long idstoreprov,
                                String detallepedido,
                                Long descuento,
                                Long idpaymentmethod,
                                Long idapplication,
                                Long idthirdemp,
                                String detallepedidomesa,
                                Long idmesa)
    {
        Long response;
        // Check if the word is present in string
        // If found, remove it using removeAll()
        try{
            billDAO.crearPedidoMesa(idstoreclient,
                     idthirduseraapp,
                     idstoreprov,
                     detallepedido,
                     descuento,
                     idpaymentmethod,
                     idapplication,
                     idthirdemp,
                     detallepedidomesa,
                     idmesa);
            // Return the resultant string
            response = new Long(1);
        }catch(Exception e){
            response = new Long(0);
            e.printStackTrace();
        }
        return response;

    }


    public Long crearDetallePedidoMesa( Long idbprov,
                                        String detallepedidomesa)
    {
        Long response = new Long(0);
        // Check if the word is present in string
        // If found, remove it using removeAll()
        try{
            billDAO.crearDetallePedidoMesa(idbprov,
                    detallepedidomesa);
            // Return the resultant string
            response = new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            response = new Long(0);
        }
        return response;
    }


    public Long actualizarEstadoMesa( Long IDBILL,
                                      Long IDESTADODESTINO,
                                      String nota,
                                      String IDESTADOORIGEN,
                                      Long IDTHIRDUSER,
                                      String ACTOR)
    {

        // Check if the word is present in string
        // If found, remove it using removeAll()
        try{
            billDAO.actualizarEstadoMesa(IDBILL,
                     IDESTADODESTINO,
                     nota,
                     IDESTADOORIGEN,
                     IDTHIRDUSER,
                     ACTOR);
            // Return the resultant string
            return new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }


    public Long agregarProductosMesa( Long idbill,
                                      String detallepedido)
    {

        // Check if the word is present in string
        // If found, remove it using removeAll()
        try{
            billDAO.agregarProductosMesa(idbill,
                    detallepedido);

            // Return the resultant string
            return new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }





    public Long agregarProductosMesaOpciones( Long idbill,
                                      String detallepedido,
                                              String detallepedidomesa)
    {

        // Check if the word is present in string
        // If found, remove it using removeAll()
        try{
            billDAO.agregarProductosMesaOpciones(idbill,
                    detallepedido,detallepedidomesa);

            // Return the resultant string
            return new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }






    public Long actualizar_estado_pedido_mesa(Long ID_DETALLE_DETAIL_BILL,
                                              Long IDESTADODESTINO,
                                              String notas,
                                              Long IDESTADOORIGEN,
                                              Long IDTHIRDUSER,
                                              String ACTOR)
    {
        Long response = new Long(0);
        // Check if the word is present in string
        // If found, remove it using removeAll()
        try{
            billDAO.actualizar_estado_pedido_mesa(ID_DETALLE_DETAIL_BILL,
                    IDESTADODESTINO,
                    notas,
                    IDESTADOORIGEN,
                    IDTHIRDUSER,
                    ACTOR);
            // Return the resultant string
            response = new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            response = new Long(0);
        }
        return response;
    }







    public Long asignar_mesero_pedido(Long idbprov,
                                              Long id_third_mesero)
    {
        Long response = new Long(0);
        // Check if the word is present in string
        // If found, remove it using removeAll()
        try{
            billDAO.asignar_mesero_pedido(idbprov,
                    id_third_mesero);
            // Return the resultant string
            response = new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            response = new Long(0);
        }
        return response;
    }






    /** @TODO for get Bill create json as
     * header{
     *     bill entity
     * }body{
     *
     *     array detail bill entity
     * }
     * for get Bill
     *
     */
    public static String removeWord(String string, String word)
    {

        // Check if the word is present in string
        // If found, remove it using removeAll()
        if (string.contains(word)) {

            // To cover the case
            // if the word is at the
            // beginning of the string
            // or anywhere in the middle
            String tempWord = word + "";
            string = string.replaceAll(tempWord, "");

            // To cover the edge case
            // if the word is at the
            // end of the string
            tempWord = "" + word;
            string = string.replaceAll(tempWord, "");
        }

        // Return the resultant string
        return string;
    }

    private String Id_payment_method_To_Name(Long id){
        if(id == 1){
            return "Efectivo";
        }else if(id == 2){
            return "Tarjeta Débito";
        }else if(id == 3){
            return "Tarjeta Crédito";
        }else{
            return "Transferencia";
        }
    }

    public Either<IException, String> TestPDF(ReporteFinanciero1DTO elDTO){
        // Se crea el documento
        Document documento = new Document(new Rectangle(596, 842), 0f, 0f, 20f, 20f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.CANADA);
        String response = "Bad!!!";
        String ruta = "";
        FileOutputStream ficheroPdf = null;
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
            ////////////////////////////////////////////////////////////////////////
            ////////// DEFINICIONES FORMALES PARA INICIALIZAR EL DOCMENTO///////////
            ////////////////////////////////////////////////////////////////////////
            String empresa = elDTO.getEmpresa();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 12, Font.BOLD);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);
            Font fontH4 = new Font(HELVETICA, 12, Font.NORMAL);
            Font fontH5 = new Font(HELVETICA, 13, Font.BOLD);
            empresa = empresa.replaceAll(" ","_");
            String nombrePdf = empresa+"_"+elDTO.getFecha().replace(" ","_")+".pdf_tmp.pdf";
            if(PruebasLocales){
                ruta = new File(".").getCanonicalPath()+"/"+ nombrePdf;
            }else{
                ruta = "/usr/share/nginx/html/facturas/"+ nombrePdf;
            }
            ficheroPdf = new FileOutputStream(ruta);

            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));

            ///////////////////////////////////////////////////////////////
            ////////// ACÁ ESTA TODO EL CONTENIDO DEL DOCUMENTO ///////////
            ///////////////////////////////////////////////////////////////
            //DEFINICIÓN TABLA PRINCIPAL
            float AnchoTablas = 80;
            PdfPTable tablaDatos = new PdfPTable(1);
            tablaDatos.setWidthPercentage(AnchoTablas);
            //NOMBRE EMPRESA
            CellMaker(elDTO.getEmpresa(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //NIT EMPRESA
            CellMaker(elDTO.getNIT(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //TITULO 1
            CellMaker("ESTADO EN LA SITUACIÓN FINANCIERA (Preliminares)",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //TITULO 2
            CellMaker("(Cifras expresadas en pesos colombianos)",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);

            try {
                Image img;
                if(PruebasLocales){
                    img = Image.getInstance("./"+elDTO.getNIT().replace(".","")+".jpg");
                }else{
                    img = Image.getInstance("/usr/share/nginx/html/logos/"+elDTO.getNIT().replace(".","")+".jpg");
                }
                img.scalePercent(50);
                Rectangle pageSize = documento.getPageSize();
                img.setAbsolutePosition(pageSize.getRight(0) - (img.getWidth()/2), pageSize.getTop(0) - (img.getHeight()/2));
                documento.add(img);
            }catch(Exception e){
                //System.out.println(e.toString());
            }

            PdfPTable tablaData = new PdfPTable(2);
            tablaData.setWidthPercentage(AnchoTablas);
            float[] AnchosTabla = new float[]{3f,1.5f};
            tablaData.setWidths(AnchosTabla);
            //
            String Tab = "      ";
            //Fecha
            EmptyCellMaker(5,PdfPCell.NO_BORDER,tablaData);
            CellMaker(elDTO.getFecha(),fontH5,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            //
            int LastSize = 0,LastRootIndex = 0;
            //////////////////////////////
            // ITERO Y GENERO LAS FILAS //
            //////////////////////////////
            for(int n = 0;n<elDTO.getFilas().size();n++){
                //Obtengo el item
                Map<String,String> Item = elDTO.getFilas().get(n);
                //Calculo el tabulado
                int Tabulados = Item.get("codigo_CUENTA").length()/2;
                //Obtengo el indice de inicio de una sección grande
                if(Item.get("codigo_CUENTA").length() == 1){LastRootIndex = n;}
                //En caso de que la nueva fila sea de menor jerarquia que la anterior, agrego una linea en blanco antes
                if(Tabulados < LastSize){EmptyCellMaker(2,PdfPCell.NO_BORDER,tablaData);}
                //
                StringBuilder Tabs = new StringBuilder();
                for(int m = 0;m<Tabulados;m++){ Tabs.append(Tab);}
                CellMaker(Tabs+Item.get("descripcion"),Item.get("codigo_CUENTA").length()==1?fontH5:fontH4,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaData,3);
                if(Item.get("codigo_CUENTA").length() != 1){
                    CellMaker(format.format(Double.parseDouble(Item.get("saldo"))),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaData);
                }else{
                    EmptyCellMaker(1,PdfPCell.NO_BORDER,tablaData);
                }
                LastSize = Tabulados;
                //Verifico si es una nueva sección
                boolean MostrarTotal = false;
                if((n+1) < elDTO.getFilas().size()){
                    if(elDTO.getFilas().get(n+1).get("codigo_CUENTA").length() == 1){
                        MostrarTotal = true;
                    }
                }else{
                    MostrarTotal = true;
                }
                if(MostrarTotal){
                    EmptyCellMaker(2,PdfPCell.NO_BORDER,tablaData);
                    CellMaker("Total "+elDTO.getFilas().get(LastRootIndex).get("descripcion"),fontH5,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaData,3);
                    CellMaker(format.format(Double.parseDouble(elDTO.getFilas().get(LastRootIndex).get("saldo"))),fontH4,PdfPCell.TOP,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaData);
                }
            }
            ////////////////////////////////////////////////////////////////////////////////
            ////////// DEFINICIONES FORMALES PARA COMPLETAR TABLAS EN EL DOCMENTO///////////
            ////////////////////////////////////////////////////////////////////////////////
            PdfPTable tabla = new PdfPTable(1);
            tabla.setWidthPercentage(AnchoTablas);

            tablaDatos.setKeepTogether(false); tablaData.setKeepTogether(false);

            tablaDatos.setSplitLate(false); tablaData.setSplitLate(false);
            //
            PdfPCell tablaDatosC = new PdfPCell(tablaDatos);
            PdfPCell tablaDataC = new PdfPCell(tablaData);
            //
            tablaDatosC.setBorder(PdfPCell.NO_BORDER);
            tablaDataC.setBorder(PdfPCell.NO_BORDER);
            //
            tabla.addCell(tablaDatosC); tabla.addCell(tablaDataC);
            tabla.setKeepTogether(false); tabla.setSplitLate(false);
            documento.add(tabla);
            //
            response = nombrePdf.substring(0,nombrePdf.length()-8);
        }catch(Exception e){
            e.printStackTrace();
            response = e.toString();
        }
        try {
            documento.close();
            assert ficheroPdf != null;
            ficheroPdf.close();
            PaginarPDF(ruta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> UniversalPDF(BillMaster billMaster,BillRaw billRaw, LegalData billLegalData, List<List<String>> billDetails,String bodyDocument,String autorizacion,RawDetailPaymentBill detailPaymentBill,Map<String, Object> DatosCajero,Map<String, Object> DatosCliente,Long pdf, Long cash, Long restflag,Boolean size) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        ////////////////////////////////////////////////////////////////////
        //////// VERIFICO EL TIPO DE FACTURA PARA SABER COMO ACTUAR ////////
        ////////////////////////////////////////////////////////////////////
        int TipoFactura = (int) billRaw.getID_BILL_TYPE();
        //System.out.println("TipoFactura es "+TipoFactura);
        ////////////////////////////////////////////////////////////
        //////// ESTO ES PARA CUANDO ES DESDE ENVIAR PEDIDO ////////
        ////////////////////////////////////////////////////////////
        //System.out.println("billRaw.getNUM_DOCUMENTO_CLIENTE() es "+billRaw.getNUM_DOCUMENTO_CLIENTE());
        //System.out.println("billRaw.getID_STORE_CLIENT() es "+billRaw.getID_STORE_CLIENT());
        if(size){

            System.out.println("VENTA DESDE ENVIAR PEDIDO");
            String doc = "";

                BillPdfDTO2 billpdfDTO = new BillPdfDTO2(
                        new ClientePedido(" ",billRaw.getPURCHASE_DATE()," ","NA","NA"," "),
                        billMaster.getFULLNAME(),
                        billMaster.getSTORE_NAME(),
                        billMaster.getPURCHASE_DATE(),
                        "-1",
                        billMaster.getNUM_DOCUMENTO(),
                        billDetails,
                        billMaster.getSUBTOTAL(),
                        billMaster.getTAX(),
                        billMaster.getTOTALPRICE(),
                        billMaster.getNAME()+" "+billMaster.getDOCUMENT_NUMBER(),
                        Id_payment_method_To_Name(detailPaymentBill.getId_payment_method()),
                        "Régimen Simplificado",
                        "",
                        (long)(detailPaymentBill.getPayment_value()-billMaster.getTOTALPRICE()),
                        billLegalData.getCITY_NAME()+", "+billLegalData.getADDRESS(),
                        billLegalData.getPHONE1(),
                        DatosCliente.get("document_TYPE").toString() + " " + DatosCliente.get("document_NUMBER").toString(),
                        DatosCliente.get("fullname") == null ? "":DatosCliente.get("fullname").toString(),
                        DatosCliente.get("address") == null ? "":DatosCliente.get("address").toString(),
                        DatosCliente.get("phone") == null ? "":DatosCliente.get("phone").toString(),
                        "",
                        new Long(TipoFactura == 4 ? 1:2),
                        autorizacion,
                        billLegalData.getREGIMEN_TRIBUTARIO(),
                        billLegalData.getPREFIX_BILL(),
                        billLegalData.getINITIAL_RANGE(),
                        billLegalData.getFINAL_RANGE()

                );


                return printPDF4(billpdfDTO,billpdfDTO.getpdfSize(), billRaw.getID_THIRD());


        }else{
        if(billRaw.getNUM_DOCUMENTO_CLIENTE() != null && (TipoFactura == 1 || TipoFactura == 4) && billRaw.getID_STORE_CLIENT() > 0){
            switch (TipoFactura){
                case 1:
                case 4:{
                    //VENTA DESDE ENVIAR PEDIDO
                    //SALIDA DESDE ENVIAR PEDIDO
                    System.out.println("VENTA DESDE ENVIAR PEDIDO");
                    if(billRaw.getNUM_DOCUMENTO_CLIENTE().equals(" ")){
                        BillPdfDTO2 billpdfDTO = new BillPdfDTO2(
                                new ClientePedido(" ",billRaw.getPURCHASE_DATE()," ","NA","NA"," "),
                                billMaster.getFULLNAME(),
                                billMaster.getSTORE_NAME(),
                                billMaster.getPURCHASE_DATE(),
                                "-1",
                                billMaster.getNUM_DOCUMENTO(),
                                billDetails,
                                billMaster.getSUBTOTAL(),
                                billMaster.getTAX(),
                                billMaster.getTOTALPRICE(),
                                billMaster.getNAME()+" "+billMaster.getDOCUMENT_NUMBER(),
                                Id_payment_method_To_Name(detailPaymentBill.getId_payment_method()),
                                "Régimen Simplificado",
                                "",
                                (long)(detailPaymentBill.getPayment_value()-billMaster.getTOTALPRICE()),
                                billLegalData.getCITY_NAME()+", "+billLegalData.getADDRESS(),
                                billLegalData.getPHONE1(),
                                DatosCliente.get("document_TYPE").toString() + " " + DatosCliente.get("document_NUMBER").toString(),
                                DatosCliente.get("fullname") == null ? "":DatosCliente.get("fullname").toString(),
                                DatosCliente.get("address") == null ? "":DatosCliente.get("address").toString(),
                                DatosCliente.get("phone") == null ? "":DatosCliente.get("phone").toString(),
                                "",
                                new Long(TipoFactura == 4 ? 1:2),
                                autorizacion,
                                billLegalData.getREGIMEN_TRIBUTARIO(),
                                billLegalData.getPREFIX_BILL(),
                                billLegalData.getINITIAL_RANGE(),
                                billLegalData.getFINAL_RANGE()
                        );
                        return printPDF4(billpdfDTO,billpdfDTO.getpdfSize(), billRaw.getID_THIRD());
                    }else{
                        BillPdfDTO2 billpdfDTO = new BillPdfDTO2(
                                pedidosBusiness.getDatosClientePedido(billRaw.getID_BILL()).right().value(),
                                billMaster.getFULLNAME(),
                                billMaster.getSTORE_NAME(),
                                billMaster.getPURCHASE_DATE(),
                                "-1",
                                billMaster.getNUM_DOCUMENTO(),
                                billDetails,
                                billMaster.getSUBTOTAL(),
                                billMaster.getTAX(),
                                billMaster.getTOTALPRICE(),
                                billMaster.getNAME()+" "+billMaster.getDOCUMENT_NUMBER(),
                                Id_payment_method_To_Name(detailPaymentBill.getId_payment_method()),
                                "Régimen Simplificado",
                                "",
                                (long)(detailPaymentBill.getPayment_value()-billMaster.getTOTALPRICE()),
                                billLegalData.getCITY_NAME()+", "+billLegalData.getADDRESS(),
                                billLegalData.getPHONE1(),
                                DatosCliente.get("document_TYPE").toString() + " " + DatosCliente.get("document_NUMBER").toString(),
                                DatosCliente.get("fullname") == null ? "":DatosCliente.get("fullname").toString(),
                                DatosCliente.get("address") == null ? "":DatosCliente.get("address").toString(),
                                DatosCliente.get("phone") == null ? "":DatosCliente.get("phone").toString(),
                                "",
                                new Long(TipoFactura == 4 ? 1:2),
                                autorizacion,
                                billLegalData.getREGIMEN_TRIBUTARIO(),
                                billLegalData.getPREFIX_BILL(),
                                billLegalData.getINITIAL_RANGE(),
                                billLegalData.getFINAL_RANGE()

                        );

                        return printPDF4(billpdfDTO,billpdfDTO.getpdfSize(),billRaw.getID_THIRD());
                    }

                }
            }
        }
        /////////////////////////////////////////////////
        //////// ESTO ES PARA UNA FACTURA NORMAL ////////
        /////////////////////////////////////////////////
        else{
            switch (TipoFactura){
                case 1:{
                        //caso de VENTA
                        /////Obtengo el domicilario
                        //Datos del cliente
                        boolean fallo = false;
                        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
                        headers.add("Key_Server", "3020D4:1FG-2U113E822-A1TE04529-68CF");
                        headers.add("Authorization", "3020D4:0DD-2F413E82B-A1EF04559-78CA");
                        //
                        KeyStore trustStore = KeyStore.getInstance("jks");
                        FileInputStream fin;
                        if (System.getenv("SYSWARE_SERVER").equals("prod")) {
                            fin = new FileInputStream(new File(".").getCanonicalPath() + "/ssl.jks");
                            trustStore.load(fin, "Jkbotero2020$$".toCharArray());
                        } else {
                            try {
                                if (System.getenv("SYSWARE_DEVPC").equals("yes")) {
                                    fin = new FileInputStream(new File(".").getCanonicalPath() + "/localhost.jks");
                                    trustStore.load(fin, "lel123".toCharArray());
                                }
                            } catch (Exception e) {
                                fin = new FileInputStream(new File(".").getCanonicalPath() + "/localhost.jks");
                                trustStore.load(fin, "Jkbotero2020$$".toCharArray());
                            }
                        }
                        //
                        String json = ClientBuilder.newBuilder().trustStore(trustStore).build().target(URI_Terceros + "thirds/getBasicThirdInfo?id_third=" + billRaw.getID_THIRD_DOMICILIARIO()).request().headers(headers).get(String.class);
                        Map<String, Object> DatosDomicilario = null;
                        try {
                            DatosDomicilario = new ObjectMapper().readValue(json, new TypeReference<Map<String, Object>>() {
                            });
                        } catch (JsonMappingException e) {
                            System.out.print("[JsonMappingException] ");
                            e.printStackTrace();
                            fallo = true;
                        } catch (IOException e) {
                            System.out.print("[IOException] ");
                            e.printStackTrace();
                            fallo = true;
                        }
                        if (fallo) {
                            DatosDomicilario = new HashMap<>();
                            DatosDomicilario.put("fullname", "Domicilario -1");
                        }
                        ///////
                        BillPdfDTO billpdfdto = new BillPdfDTO(
                                DatosDomicilario.get("fullname").toString(),
                                billMaster.getFULLNAME(),
                                billMaster.getSTORE_NAME(),
                                billMaster.getPURCHASE_DATE(),
                                billMaster.getCAJA(),
                                billMaster.getNUM_DOCUMENTO(),
                                billDetails,
                                billMaster.getSUBTOTAL(),
                                billMaster.getTAX(),
                                billMaster.getTOTALPRICE(),
                                billMaster.getNAME() + " " + billMaster.getDOCUMENT_NUMBER(),
                                Id_payment_method_To_Name(detailPaymentBill.getId_payment_method()),
                                "Régimen Simplificado",
                                DatosCajero.get("fullname").toString(),
                                (long) (detailPaymentBill.getPayment_value() - billMaster.getTOTALPRICE()),
                                billLegalData.getCITY_NAME() + ", " + billLegalData.getADDRESS(),
                                billLegalData.getPHONE1(),
                                DatosCliente.get("document_TYPE").toString() + " " + DatosCliente.get("document_NUMBER").toString(),
                                DatosCliente.get("fullname") == null ? "" : DatosCliente.get("fullname").toString(),
                                DatosCliente.get("address") == null ? "" : DatosCliente.get("address").toString(),
                                DatosCliente.get("phone") == null ? "" : DatosCliente.get("phone").toString(),
                                "",
                                new Long(2),
                                autorizacion,
                                billLegalData.getREGIMEN_TRIBUTARIO(),
                                billLegalData.getPREFIX_BILL(),
                                billLegalData.getINITIAL_RANGE(),
                                billLegalData.getFINAL_RANGE(),
                                bodyDocument,
                                billRaw.getPURCHASE_DATE_DOCUMENT()
                        );
                        Either<IException, String> retorno =  printPDF2(billpdfdto, billRaw.getDISCOUNT(),billRaw.getID_THIRD(),cash,restflag);
                        //System.out.println("DatosCliente.get(\"fullname\").toString()");
                        //System.out.println(DatosCliente.get("fullname").toString());
                        if(pdf == 0 && DatosCliente.get("fullname").toString() != null){
                            try{
                                String Destinatario = DatosCliente.get("mail").toString();
                                if(Destinatario != null){if(Destinatario.equals("")){return retorno;}}
                                String NumDocCliente = "";
                                if(billRaw.getNUM_DOCUMENTO_CLIENTE() != null && !billRaw.getNUM_DOCUMENTO_CLIENTE().equals("")){NumDocCliente = " - Pedido: "+billRaw.getNUM_DOCUMENTO_CLIENTE();}
                                String Asunto = "Factura de venta: "+billRaw.getNUM_DOCUMENTO()+NumDocCliente;
                                String Cuerpo = "Se adjunta en el correo la factura de compra correspondiente al dia de hoy.";
                                List<String> ListadoArchivos = new ArrayList<>();
                                if(PruebasLocales){
                                    ListadoArchivos.add(new File(".").getCanonicalPath()+"/"+retorno.right().value());
                                }else {
                                    ListadoArchivos.add("/usr/share/nginx/html/facturas/" + retorno.right().value());
                                }
                                SendEmail(Destinatario,Asunto,Cuerpo,ListadoArchivos);
                            }catch (Exception e){
                                //e.printStackTrace();
                                //System.out.println("NO SE PUDO ENVIAR CORREO");
                            }
                        }
                        return retorno;
                }
                case 2:{
                    //caso de COMPRA
                    //Obtengo el domicilario
                    //Datos del cliente
                    boolean fallo = false;
                    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
                    headers.add("Key_Server", "3020D4:1FG-2U113E822-A1TE04529-68CF");
                    headers.add("Authorization", "3020D4:0DD-2F413E82B-A1EF04559-78CA");
                    //
                    KeyStore trustStore = KeyStore.getInstance("jks");
                    FileInputStream fin;
                    if(System.getenv("SYSWARE_SERVER").equals("prod")){
                        fin = new FileInputStream(new File(".").getCanonicalPath()+"/ssl.jks");
                        trustStore.load(fin, "Jkbotero2020$$".toCharArray());
                    }else{
                        try{
                            if(System.getenv("SYSWARE_DEVPC").equals("yes")){
                                fin = new FileInputStream(new File(".").getCanonicalPath()+"/localhost.jks");
                                trustStore.load(fin, "lel123".toCharArray());
                            }
                        }catch (Exception e){
                            fin = new FileInputStream(new File(".").getCanonicalPath()+"/localhost.jks");
                            trustStore.load(fin, "Jkbotero2020$$".toCharArray());
                        }
                    }
                    //
                    String json = ClientBuilder.newBuilder().trustStore(trustStore).build().target(URI_Terceros+"thirds/getBasicThirdInfo?id_third="+billRaw.getID_THIRD_DOMICILIARIO()).request().headers(headers).get(String.class);
                    Map<String, Object> DatosDomicilario = null;
                    try { DatosDomicilario = new ObjectMapper().readValue(json , new TypeReference<Map<String, Object>>(){}); }
                    catch (JsonMappingException e) { System.out.print("[JsonMappingException] "); e.printStackTrace();  fallo = true;}
                    catch (IOException e) { System.out.print("[IOException] "); e.printStackTrace();  fallo = true;}
                    if(fallo){
                        DatosDomicilario = new HashMap<>();
                        DatosDomicilario.put("fullname","Domicilario -1");
                    }
                    ///////
                    BillPdfDTO billpdfdto = new BillPdfDTO(
                            DatosDomicilario.get("fullname").toString(),
                            billMaster.getFULLNAME(),
                            billMaster.getSTORE_NAME(),
                            billMaster.getPURCHASE_DATE(),
                            billMaster.getCAJA(),
                            billMaster.getNUM_DOCUMENTO(),
                            billDetails,
                            billMaster.getSUBTOTAL(),
                            billMaster.getTAX(),
                            billMaster.getTOTALPRICE(),
                            billMaster.getNAME()+" "+billMaster.getDOCUMENT_NUMBER(),
                            Id_payment_method_To_Name(detailPaymentBill.getId_payment_method()),
                            "Régimen Simplificado",
                            DatosCajero.get("fullname").toString(),
                            (long)(detailPaymentBill.getPayment_value()-billMaster.getTOTALPRICE()),
                            billLegalData.getCITY_NAME()+", "+billLegalData.getADDRESS(),
                            billLegalData.getPHONE1(),
                            DatosCliente.get("document_TYPE").toString() + " " + DatosCliente.get("document_NUMBER").toString(),
                            DatosCliente.get("fullname") == null ? "":DatosCliente.get("fullname").toString(),
                            DatosCliente.get("address") == null ? "":DatosCliente.get("address").toString(),
                            DatosCliente.get("phone") == null ? "":DatosCliente.get("phone").toString(),
                            "",
                            new Long(2),
                            autorizacion,
                            billLegalData.getREGIMEN_TRIBUTARIO(),
                            billLegalData.getPREFIX_BILL(),
                            billLegalData.getINITIAL_RANGE(),
                            billLegalData.getFINAL_RANGE(),
                            bodyDocument,
                            billRaw.getPURCHASE_DATE_DOCUMENT()
                    );
                    return printPDF2compra(billpdfdto, billRaw.getPURCHASE_DATE(),billRaw.getNUM_DOCUMENTO_ADICIONAL(),billRaw.getNUM_DOCUMENTO_CLIENTE(),billRaw.getID_THIRD(),billRaw.getID_BILL());
                }
                case 3:
                case 4:{
                    //3 es caso de ENTRADA, 4 es caso de SALIDA
                    //obtengo la razon
                    String razon = billDAO.getNameBillState(billRaw.getID_BILL_STATE());
                    if(billRaw.getID_BILL_STATE() == 1){razon = "Otros";}
                    //obtengo datos empresa
                    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
                    headers.add("Key_Server", "3020D4:1FG-2U113E822-A1TE04529-68CF");
                    headers.add("Authorization", "3020D4:0DD-2F413E82B-A1EF04559-78CA");
                    //
                    KeyStore trustStore = KeyStore.getInstance("jks");
                    FileInputStream fin;
                    if(System.getenv("SYSWARE_SERVER").equals("prod")){
                        fin = new FileInputStream(new File(".").getCanonicalPath()+"/ssl.jks");
                        trustStore.load(fin, "Jkbotero2020$$".toCharArray());
                    }else{
                        try{
                            if(System.getenv("SYSWARE_DEVPC").equals("yes")){
                                fin = new FileInputStream(new File(".").getCanonicalPath()+"/localhost.jks");
                                trustStore.load(fin, "lel123".toCharArray());
                            }
                        }catch (Exception e){
                            fin = new FileInputStream(new File(".").getCanonicalPath()+"/localhost.jks");
                            trustStore.load(fin, "Jkbotero2020$$".toCharArray());
                        }
                    }
                    //
                    String json = ClientBuilder.newBuilder().trustStore(trustStore).build().target(URI_Terceros+"thirds/getBasicThirdInfo?id_third="+billRaw.getID_THIRD()).request().headers(headers).get(String.class);
                    Map<String, Object> DatosEmpresa = null;
                    boolean fallo = false;
                    try { DatosEmpresa = new ObjectMapper().readValue(json , new TypeReference<Map<String, Object>>(){}); }
                    catch (JsonMappingException e) { System.out.print("[JsonMappingException] "); e.printStackTrace(); fallo = true;}
                    catch (IOException e) { System.out.print("[IOException] "); e.printStackTrace();  fallo = true;}
                    if(fallo){//En caso de fallar, datos temporales
                        DatosEmpresa = new HashMap<>();
                        DatosEmpresa.put("document_TYPE","NIT");
                        DatosEmpresa.put("document_NUMBER","-1");
                        DatosEmpresa.put("fullname","-1");
                    }
                    //System.out.println("billMaster.getSTORE_NAME() es "+billMaster.getSTORE_NAME());
                    //System.out.println("billMaster.getNUM_DOCUMENTO() es "+billMaster.getNUM_DOCUMENTO());
                    ///////////////////
                    BillinPDF billinPDF = new BillinPDF(
                            billDetails,
                            bodyDocument,
                            razon,
                            billRaw.getPURCHASE_DATE(),
                            DatosEmpresa.get("fullname").toString(),
                            DatosEmpresa.get("document_TYPE").toString()+" "+DatosEmpresa.get("document_NUMBER").toString(),
                            billRaw.getID_THIRD()+"",
                            TipoFactura == 3 ? "1":"2",
                            billMaster.getSTORE_NAME(),
                            billMaster.getNUM_DOCUMENTO()
                    );
                    return printInBillPDF(billinPDF,billLegalData,billRaw.getID_THIRD());
                }
                //GENERAR PEDIDO
                case 86:
                case 87:{
                    //Datos del empleado cajero
                    //System.out.println("Consulto Datos del tercero");
                    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
                    headers.add("Key_Server", "3020D4:1FG-2U113E822-A1TE04529-68CF");
                    headers.add("Authorization", "3020D4:0DD-2F413E82B-A1EF04559-78CA");
                    //
                    KeyStore trustStore = KeyStore.getInstance("jks");
                    FileInputStream fin;
                    if(System.getenv("SYSWARE_SERVER").equals("prod")){
                        fin = new FileInputStream(new File(".").getCanonicalPath()+"/ssl.jks");
                        trustStore.load(fin, "Jkbotero2020$$".toCharArray());
                    }else{
                        try{
                            if(System.getenv("SYSWARE_DEVPC").equals("yes")){
                                fin = new FileInputStream(new File(".").getCanonicalPath()+"/localhost.jks");
                                trustStore.load(fin, "lel123".toCharArray());
                            }
                        }catch (Exception e){
                            fin = new FileInputStream(new File(".").getCanonicalPath()+"/localhost.jks");
                            trustStore.load(fin, "Jkbotero2020$$".toCharArray());
                        }
                    }
                    //
                    String json = ClientBuilder.newBuilder().trustStore(trustStore).build().target(URI_Terceros+"thirds/getBasicThirdInfo?id_third="+billRaw.getID_THIRD()).request().headers(headers).get(String.class);
                    Map<String, Object> DatosTercero = null;
                    boolean fallo = false;
                    try { DatosTercero = new ObjectMapper().readValue(json , new TypeReference<Map<String, Object>>(){}); }
                    catch (JsonMappingException e) { System.out.print("[JsonMappingException-Error Controlado] No pudo mapear el json"); fallo = true;}
                    catch (IOException e) { System.out.print("[IOException] "); e.printStackTrace();  fallo = true;}
                    if(fallo){//En caso de fallar, POSIBLEMENTE, es cliente ocasional
                        DatosTercero = new HashMap<>();
                        DatosTercero.put("document_NUMBER","-1");
                        DatosTercero.put("fullname","-1");
                        DatosTercero.put("phone","-1");
                        DatosTercero.put("document_TYPE","-1");
                        DatosTercero.put("totalventas","-1");
                        DatosTercero.put("mail","-1");
                        DatosTercero.put("address","-1");
                    }
                    //////
                    PdfPedidosOrderDTO pedido = new PdfPedidosOrderDTO(
                            null,
                            billRaw.getNUM_DOCUMENTO(),
                            DatosCliente.get("fullname") == null ? "":DatosCliente.get("fullname").toString(),
                            billRaw.getPURCHASE_DATE(),
                            DatosCliente.get("document_TYPE").toString() + " " + DatosCliente.get("document_NUMBER").toString(),
                            DatosCliente.get("mail") == null ? "":DatosCliente.get("mail").toString(),
                            DatosCliente.get("address") == null ? "":DatosCliente.get("address").toString(),
                            billRaw.getTOTALPRICE(),
                            billRaw.getSUBTOTAL(),
                            billRaw.getTAX(),
                            billDetails,
                            null,
                            bodyDocument == null ? "N/A":(bodyDocument.equals("") ? "N/A":bodyDocument),
                            DatosTercero,
                            DatosCliente.get("phone") == null ? "":DatosCliente.get("phone").toString(),
                            billRaw.getNUM_DOCUMENTO_CLIENTE(),
                            billMaster.getSTORE_NAME(),
                            billMaster.getSTORE_NAME_CLIENT()
                    );
                    Either<IException, String> retorno =  pedidosBusiness.printPDF2order(pedido,TipoFactura, billRaw.getID_THIRD(),billRaw.getID_BILL());
                    if(pdf >= 0){
                        try{
                            if(pdf == 1){
                                //Ajustes para el correo prov
                                String DestinatarioProv = DatosTercero.get("mail").toString();
                                String AsuntoProv = "Tienda724 - Pedido "+billRaw.getNUM_DOCUMENTO();
                                String CuerpoProv = "El cliente "+(DatosCliente.get("fullname").toString() == null ? "":DatosCliente.get("fullname").toString())+" creó el pedido "+billRaw.getNUM_DOCUMENTO()+" en la tienda: "+DatosTercero.get("fullname").toString()+" - "+billMaster.getSTORE_NAME()+"<br>" +
                                        "<br>Se adjunta pdf con el<br>Detalle de la orden <br><br>Cordialmente,<br><br>Soporte Tienda724";
                                List<String> ListadoArchivosProv = new ArrayList<>();
                                //Ajustes para el correo cliente
                                String DestinatarioCliente = DatosCliente.get("mail").toString();
                                String AsuntoCliente = "Tienda724 - Pedido "+billRaw.getNUM_DOCUMENTO();
                                String CuerpoCliente = "El cliente "+(DatosCliente.get("fullname").toString() == null ? "":DatosCliente.get("fullname").toString())+" creó el pedido "+billRaw.getNUM_DOCUMENTO()+" en la tienda: "+DatosTercero.get("fullname").toString()+" - "+billMaster.getSTORE_NAME()+"<br>" +
                                        "<br>Se adjunta pdf con el<br>Detalle de la orden <br><br>Cordialmente,<br><br>Soporte Tienda724";
                                List<String> ListadoArchivosCliente = new ArrayList<>();
                                //Agrego archivos
                                if(PruebasLocales){
                                    ListadoArchivosProv.add(new File(".").getCanonicalPath()+"/"+retorno.right().value());
                                    ListadoArchivosCliente.add(new File(".").getCanonicalPath()+"/"+retorno.right().value());
                                }else {
                                    ListadoArchivosProv.add("/usr/share/nginx/html/remisiones/" + retorno.right().value());
                                    ListadoArchivosCliente.add("/usr/share/nginx/html/remisiones/" + retorno.right().value());
                                }
                                //Envio el del prov
                                SendEmail(DestinatarioProv,AsuntoProv,CuerpoProv,ListadoArchivosProv);
                                //Envio el del cliente
                                SendEmail(DestinatarioCliente,AsuntoCliente,CuerpoCliente,ListadoArchivosCliente);
                            }else{
                                String Destinatario = DatosCliente.get("mail").toString();
                                if(DatosTercero.get("mail") != null){
                                    Destinatario += ","+DatosTercero.get("mail").toString();
                                }
                                //Destinatario = "stpremiun@gmail.com";
                                String Asunto = "Pedido Cliente: "+billRaw.getNUM_DOCUMENTO();
                                String Cuerpo = "El cliente <b>"+(DatosTercero.get("fullname") == null ? "":DatosTercero.get("fullname").toString())+" - "+billMaster.getSTORE_NAME()+"</b> ha generado un pedido.<br><br>Ver el pdf adjunto para más información.";
                                List<String> ListadoArchivos = new ArrayList<>();
                                if(PruebasLocales){
                                    ListadoArchivos.add(new File(".").getCanonicalPath()+"/"+retorno.right().value());
                                }else {
                                    ListadoArchivos.add("/usr/share/nginx/html/remisiones/" + retorno.right().value());
                                }
                                SendEmail(Destinatario,Asunto,Cuerpo,ListadoArchivos);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    return retorno;
                }
            }
            }
        }
        return Either.right("Not Match");
    }




    public Either<IException, String> PrintPdfTable(BillMaster billMaster,BillRaw billRaw, LegalData billLegalData, List<List<String>> billDetails,String bodyDocument,String autorizacion,RawDetailPaymentBill detailPaymentBill,Map<String, Object> DatosCajero,Map<String, Object> DatosCliente,Long pdf, Long cash, Long restflag, String mesa) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        ////////////////////////////////////////////////////////////////////
        //////// VERIFICO EL TIPO DE FACTURA PARA SABER COMO ACTUAR ////////
        ////////////////////////////////////////////////////////////////////
        int TipoFactura = (int) billRaw.getID_BILL_TYPE();
        //System.out.println("TipoFactura es "+TipoFactura);
        ////////////////////////////////////////////////////////////
        //////// ESTO ES PARA CUANDO ES DESDE ENVIAR PEDIDO ////////
        ////////////////////////////////////////////////////////////
        //System.out.println("billRaw.getNUM_DOCUMENTO_CLIENTE() es "+billRaw.getNUM_DOCUMENTO_CLIENTE());
        //System.out.println("billRaw.getID_STORE_CLIENT() es "+billRaw.getID_STORE_CLIENT());



                    //caso de VENTA
                    /////Obtengo el domicilario
                    //Datos del cliente
                    boolean fallo = false;
                    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
                    headers.add("Key_Server", "3020D4:1FG-2U113E822-A1TE04529-68CF");
                    headers.add("Authorization", "3020D4:0DD-2F413E82B-A1EF04559-78CA");
                    //
                    KeyStore trustStore = KeyStore.getInstance("jks");
                    FileInputStream fin;
                    if(System.getenv("SYSWARE_SERVER").equals("prod")){
                        fin = new FileInputStream(new File(".").getCanonicalPath()+"/ssl.jks");
                        trustStore.load(fin, "Jkbotero2020$$".toCharArray());
                    }else{
                        try{
                            if(System.getenv("SYSWARE_DEVPC").equals("yes")){
                                fin = new FileInputStream(new File(".").getCanonicalPath()+"/localhost.jks");
                                trustStore.load(fin, "lel123".toCharArray());
                            }
                        }catch (Exception e){
                            fin = new FileInputStream(new File(".").getCanonicalPath()+"/localhost.jks");
                            trustStore.load(fin, "Jkbotero2020$$".toCharArray());
                        }
                    }
                    //
                    String json = ClientBuilder.newBuilder().trustStore(trustStore).build().target(URI_Terceros+"thirds/getBasicThirdInfo?id_third="+billRaw.getID_THIRD_DOMICILIARIO()).request().headers(headers).get(String.class);
                    Map<String, Object> DatosDomicilario = null;
                    try { DatosDomicilario = new ObjectMapper().readValue(json , new TypeReference<Map<String, Object>>(){}); }
                    catch (JsonMappingException e) { System.out.print("[JsonMappingException] "); e.printStackTrace();  fallo = true;}
                    catch (IOException e) { System.out.print("[IOException] "); e.printStackTrace();  fallo = true;}
                    if(fallo){
                        DatosDomicilario = new HashMap<>();
                        DatosDomicilario.put("fullname","Domicilario -1");
                    }
                    ///////
                    BillPdfDTO billpdfdto = new BillPdfDTO(
                            DatosDomicilario.get("fullname").toString(),
                            billMaster.getFULLNAME(),
                            billMaster.getSTORE_NAME(),
                            billMaster.getPURCHASE_DATE(),
                            billMaster.getCAJA(),
                            billMaster.getNUM_DOCUMENTO(),
                            billDetails,
                            billMaster.getSUBTOTAL(),
                            billMaster.getTAX(),
                            billMaster.getTOTALPRICE(),
                            billMaster.getNAME()+" "+billMaster.getDOCUMENT_NUMBER(),
                            Id_payment_method_To_Name(detailPaymentBill.getId_payment_method()),
                            "Régimen Simplificado",
                            DatosCajero.get("fullname").toString(),
                            (long)(detailPaymentBill.getPayment_value()-billMaster.getTOTALPRICE()),
                            billLegalData.getCITY_NAME()+", "+billLegalData.getADDRESS(),
                            billLegalData.getPHONE1(),
                            DatosCliente.get("document_TYPE").toString() + " " + DatosCliente.get("document_NUMBER").toString(),
                            DatosCliente.get("fullname") == null ? "":DatosCliente.get("fullname").toString(),
                            DatosCliente.get("address") == null ? "":DatosCliente.get("address").toString(),
                            DatosCliente.get("phone") == null ? "":DatosCliente.get("phone").toString(),
                            "",
                            new Long(2),
                            autorizacion,
                            billLegalData.getREGIMEN_TRIBUTARIO(),
                            billLegalData.getPREFIX_BILL(),
                            billLegalData.getINITIAL_RANGE(),
                            billLegalData.getFINAL_RANGE(),
                            bodyDocument,
                            billRaw.getPURCHASE_DATE_DOCUMENT()
                    );
                    Either<IException, String> retorno =  PrintPdfTablePrint(billpdfdto, billRaw.getDISCOUNT(),billRaw.getID_THIRD(),cash,restflag, billRaw.getID_BILL(),mesa);
                    //System.out.println("DatosCliente.get(\"fullname\").toString()");
                    //System.out.println(DatosCliente.get("fullname").toString());

                    return retorno;

    }



    public Either<IException, String> PrintPdfTablePrint(BillPdfDTO billpdfDTO, double discount, Long id_third, Long cash, Long restflag, Long idBill, String mesa){
        // Se crea el documento
        Rectangle envelope = new Rectangle(137, 598);
        Document documento = new Document(envelope, 0f, 0f, 10f, 0f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        format.setMaximumFractionDigits(0);
        String response = "Bad!!!";
        String ruta = "";
        FileOutputStream ficheroPdf = null;
        try{
            // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
            String empresa = billpdfDTO.getempresa();
            //DEFINO FUENTE BASE
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            //DEFINO LAS FUENTES
            Font fontH1 = new Font(HELVETICA, 6, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 5, Font.NORMAL);
            Font fontH3 = new Font(HELVETICA, 7, Font.BOLD);
            Font fontH4 = new Font(HELVETICA, 7, Font.NORMAL);
            //
            empresa = empresa.replaceAll(" ","_");
            //INICIALIZO NOMBRE Y VARIABLE DEL FICHERO, DEPENDIENDO DEL CASO
            String nombrePdf = id_third+"_"+billpdfDTO.getconsecutivo().replace(" ","_").replaceAll("_-_","-")+".pdf_tmp.pdf";
            if(PruebasLocales){
                ruta = new File(".").getCanonicalPath()+"/"+ nombrePdf;
            }else{
                ruta = "/usr/share/nginx/html/facturas/"+ nombrePdf;
            }
            ficheroPdf = new FileOutputStream(ruta);
            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));
            //DEFINO TABLA
            PdfPTable tablaDatos = new PdfPTable(1);

            PdfPCell cella =new PdfPCell(new Phrase("  ",fontH4));
            cella.setBorder(PdfPCell.NO_BORDER);

            try {
                Image img;
                if(PruebasLocales){
                    img = Image.getInstance(new File(".").getCanonicalPath()+"/"+billpdfDTO.getnit()+".jpg");
                }else{
                    img = Image.getInstance("/usr/share/nginx/html/logos/"+billpdfDTO.getnit()+".jpg");
                }
                PdfPCell imagen = new PdfPCell(img, true);
                imagen.setBorder(PdfPCell.NO_BORDER);
                imagen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos.addCell(imagen);
            }catch(Exception e){
                //System.out.println(e.toString());
                Image img = Image.getInstance("/usr/share/nginx/html/logos/tienda724.jpg");
                PdfPCell imagen = new PdfPCell(img, true);
                imagen.setBorder(PdfPCell.NO_BORDER);
                imagen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos.addCell(imagen);
            }

            //NOMBRE EMPRESA
            if(billpdfDTO.getempresa().equals("SUPERTIENDAS MERCAFULL")){
                CellMaker("SIEMPRE PENSANDO EN TI",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            }else{
                CellMaker(billpdfDTO.getempresa(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            }
            //NIT
            CellMaker(billpdfDTO.getnit(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //DIRECCIÓN
            CellMaker(billpdfDTO.getdireccion(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //TELEFONO
            CellMaker("Tel: "+billpdfDTO.gettelefono(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            if(restflag == null){
                restflag = new Long(0) ;
            }
            if(billpdfDTO.getdomiciliario().equals("Sin Asignación")){
                CellMaker(" ",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            }else{
                String frase = "Domiciliario: ";
                if(restflag == 1){
                    frase = "Mesero: ";
                }
                if((frase+billpdfDTO.getdomiciliario()).length()<=18) {
                    CellMaker(frase+billpdfDTO.getdomiciliario(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
                }else{
                    CellMaker(frase+billpdfDTO.getdomiciliario().substring(0, 18),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
                }
            }
            tablaDatos.addCell(cella);

            PdfPTable tablaData = new PdfPTable(1);

            //CLIENTE
            CellMaker("Cliente",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            if(removeWord(billpdfDTO.getcliente(),"null").length()<=18) {
                CellMaker(removeWord(billpdfDTO.getcliente(),"null"),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            }else{
                CellMaker(removeWord(billpdfDTO.getcliente(),"null").substring(0, 18),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            }
            //DOCUMENTO CC
            CellMaker(billpdfDTO.getcedula(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablaData);
            //getdireccionC
            CellMaker(billpdfDTO.getdireccionC(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            //gettelefonoC
            CellMaker("Tel: "+billpdfDTO.gettelefonoC(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);

            //DATOS CLIENTE
            if(billpdfDTO.getcliente().equals("Cliente Ocasional")){
                tablaData = new PdfPTable(1);
                CellMaker("Cliente",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);

                CellMaker(removeWord(billpdfDTO.getcliente(),"null"),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            }

            PdfPTable TablaObservaciones = new PdfPTable(1);
            //TablaObservaciones.addCell(cella);
            //TITULO OBSERVACIONES
            //CellMaker("Observaciones:",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,TablaObservaciones);
            String observaciones = billpdfDTO.getobservaciones();

            if(restflag == 1){
                String[] observacionesList = billpdfDTO.getobservaciones().split("-");
                observaciones = mesa;
            }

            //LAS OBSERVACIONES
            CellMaker(observaciones,fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,TablaObservaciones);

            //TABLA DEBAJO DE OBSERVACIONES
            PdfPTable tablaFecha = new PdfPTable(1);
            tablaFecha.addCell(cella);
            //FECHA
            CellMaker(billpdfDTO.getfecha(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            //FACTURA
            CellMaker("Comanda # "+billpdfDTO.getconsecutivo().replaceAll(" - ",""),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            //CAJERO
            //CellMaker("Cajero: "+removeWord(billpdfDTO.getnombreCajero(),"null"),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            tablaFecha.addCell(cella);

            //TABLA TIENDA
            PdfPTable tablaDatos2 = new PdfPTable(2);

            //CAJERO
            CellMaker("Tienda: ",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos2);

            if(billpdfDTO.getcaja().equals("-1")){
                //PEDIDO
                CellMaker("PEDIDO",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos2);

            }else{
                //Caja
                //CellMaker("Caja: "+billpdfDTO.getcaja(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos2);
                CellMaker(billpdfDTO.gettienda(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos2);
            }
            tablaDatos2.addCell(cella);
            tablaDatos2.addCell(cella);
            tablaDatos2.addCell(cella);
            tablaDatos2.addCell(cella);


            //PdfPTable tablaDetalles = new PdfPTable(new float[] { new Float(0.75),new Float(2.25), 1 });

            PdfPTable tablaDetalles = new PdfPTable(1);

            //Caja
            CellMaker("Productos",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            CellMaker(" ",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            //Caja
            //CellMaker("Precio",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaDetalles);

            List<List<String>> theList = billpdfDTO.getdetalles();

            boolean containsService = false;
            Long serviceValue = new Long(0);

            List<ProductosPedidosMesa> comandaDetailss = billDAO.getProductosPedidoMesa(idBill);

            for (int i = 0; i < comandaDetailss.size(); i++) {

                ProductosPedidosMesa temp = comandaDetailss.get(i);

                if(!temp.getPRODUCTO().equals("INICIO DE CONTEO")){
                    //PRODUCTO
                    if(temp.getPRODUCTO().length()<=35) {
                        CellMaker(i+"- "+temp.getPRODUCTO(),fontH2,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                    }else{
                        CellMaker(i+"- "+temp.getPRODUCTO().substring(0, 35),fontH2,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                    }


                    try{

                        String notas[]  = temp.getNOTAS().split(";");
                        for(String nota : notas){
                            //DESCRIPCION
                            CellMaker(" * "+nota.split("=")[1],fontH2,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                        }
                    }catch(Exception e){

                        e.printStackTrace();

                    }



                    //CANTIDAD
                    //CellMaker(temp.getCantidad(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);

                    //PRECIO
                    //CellMaker(format.format(number),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaDetalles);
                    CellMaker(" ",fontH2,PdfPCell.BOTTOM,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                }
            }

            if(containsService){
                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);

                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                CellMaker("Servicio",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                CellMaker(format.format(serviceValue),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaDetalles);

                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
            }

            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);

            Long cambio = Math.round(cash-billpdfDTO.gettotalprice());
            String medioPago = billpdfDTO.getmedioPago();
            Long cantRecibida = billpdfDTO.getcambio() + round(billpdfDTO.gettotalprice());

            if(cambio<0){
                cambio = new Long(0);
                medioPago = "Múltiples Medios";
                cantRecibida = round(billpdfDTO.gettotalprice());
            }
            //
            PdfPTable tablaTotales = new PdfPTable(2);
            //
            if(discount != 0){
                //DESCUENTO
                CellMaker("Descuento",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
                CellMaker(format.format(new Long(Math.round(discount)))+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            }
           //SUBTOTAL
            CellMaker("Subtotal",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(new Long(Math.round(billpdfDTO.getsubtotal())))+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //IVA
            CellMaker("IVA",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(new Long(Math.round(billpdfDTO.gettax())))+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            if(discount != 0){
                //TOTAL SIN DESCUENTO
                CellMaker("Total sin Descuento",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
                CellMaker(format.format(new Long(Math.round(billpdfDTO.gettotalprice()+discount)))+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            }
            //TOTAL
            CellMaker("Total",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(new Long(Math.round(billpdfDTO.gettotalprice())))+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //CANTIDAD RECIBIDA
            CellMaker("Cantidad Recibida",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(cantRecibida)+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //CAMBIO
            CellMaker("Cambio",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(cambio)+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //MEDIO PAGO
            CellMaker("Medio pago",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(medioPago+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);

            PdfPTable tablaLegal = new PdfPTable(1);
            tablaLegal.addCell(cella);
            if(billpdfDTO.getregimenT()!= null && !billpdfDTO.getregimenT().isEmpty()){
                String regimenTemp;
                if(billpdfDTO.getregimenT().equals("C")){
                    regimenTemp = "Común";
                }else{
                    regimenTemp = "Simplificado";
                }
                CellMaker("Régimen "+regimenTemp,fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaLegal);
            }

            if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                CellMaker("Resolución Dian # "+billpdfDTO.getresolucion_DIAN(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaLegal);
            }

            CellMaker("Modalidad: POS",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaLegal);

            tablaLegal.addCell(cella);

            PdfPTable tabla = new PdfPTable(1);

            tablaDatos.setKeepTogether(false);
            tablaData.setKeepTogether(false);
            tablaFecha.setKeepTogether(false);
            tablaDatos2.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaTotales.setKeepTogether(false);
            tablaLegal.setKeepTogether(false);
            TablaObservaciones.setKeepTogether(false);

            tablaDatos.setSplitLate(false);
            tablaData.setSplitLate(false);
            tablaFecha.setSplitLate(false);
            tablaDatos2.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaTotales.setSplitLate(false);
            TablaObservaciones.setSplitLate(false);

            tablaLegal.setSplitLate(false);

            PdfPCell tablaDatosC = new PdfPCell(tablaDatos);
            PdfPCell tablaDataC = new PdfPCell(tablaData);
            PdfPCell tablaFechaC = new PdfPCell(tablaFecha);
            PdfPCell tablaDatos2C = new PdfPCell(tablaDatos2);
            PdfPCell tablaDetallesC = new PdfPCell(tablaDetalles);
            //PdfPCell tablaTotalesC = new PdfPCell(tablaTotales);
            //PdfPCell tablaLegalC = new PdfPCell(tablaLegal);
            PdfPCell tablaobs = new PdfPCell(TablaObservaciones);

            tablaDatosC.setBorder(PdfPCell.NO_BORDER);
            tablaDataC.setBorder(PdfPCell.NO_BORDER);
            tablaobs.setBorder(PdfPCell.NO_BORDER);
            tablaFechaC.setBorder(PdfPCell.NO_BORDER);
            tablaDatos2C.setBorder(PdfPCell.NO_BORDER);
            tablaDetallesC.setBorder(PdfPCell.NO_BORDER);
            //tablaTotalesC.setBorder(PdfPCell.NO_BORDER);
            //tablaLegalC.setBorder(PdfPCell.NO_BORDER);

            tabla.addCell(tablaDatosC);
            tabla.addCell(tablaDataC);
            tabla.addCell(tablaFechaC);
            tabla.addCell(tablaDatos2C);
            tabla.addCell(tablaDetallesC);
            //tabla.addCell(tablaDetalles2C);
            //tabla.addCell(tablaTotalesC);
            //tabla.addCell(tablaLegalC);
            tabla.addCell(tablaobs);

            tabla.setKeepTogether(false);
            tabla.setSplitLate(false);

            documento.add(tabla);
            response = nombrePdf.substring(0,nombrePdf.length()-8);

        }catch(Exception e){
            response = e.toString();
            //System.out.println("[Error en printPDF2] "+e.toString());
            e.printStackTrace();
        }
        try {
            documento.close();
            assert ficheroPdf != null;
            ficheroPdf.close();
            PaginarPDF(ruta,2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String> printPDF2(BillPdfDTO billpdfDTO, double discount, Long id_third, Long cash, Long restflag){
        // Se crea el documento
        Rectangle envelope = new Rectangle(137, 598);
        Document documento = new Document(envelope, 0f, 0f, 10f, 0f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        format.setMaximumFractionDigits(0);
        String response = "Bad!!!";
        String ruta = "";
        FileOutputStream ficheroPdf = null;
        try{
            // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
            String empresa = billpdfDTO.getempresa();
            //DEFINO FUENTE BASE
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            //DEFINO LAS FUENTES
            Font fontH1 = new Font(HELVETICA, 6, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 5, Font.NORMAL);
            Font fontH3 = new Font(HELVETICA, 7, Font.BOLD);
            Font fontH4 = new Font(HELVETICA, 7, Font.NORMAL);
            //
            empresa = empresa.replaceAll(" ","_");
            //INICIALIZO NOMBRE Y VARIABLE DEL FICHERO, DEPENDIENDO DEL CASO
            String nombrePdf = id_third+"_"+billpdfDTO.getconsecutivo().replace(" ","_").replaceAll("_-_","-")+".pdf_tmp.pdf";
            if(PruebasLocales){
                ruta = new File(".").getCanonicalPath()+"/"+ nombrePdf;
            }else{
                ruta = "/usr/share/nginx/html/facturas/"+ nombrePdf;
            }
            ficheroPdf = new FileOutputStream(ruta);
            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));
            //DEFINO TABLA
            PdfPTable tablaDatos = new PdfPTable(1);

            PdfPCell cella =new PdfPCell(new Phrase("  ",fontH4));
            cella.setBorder(PdfPCell.NO_BORDER);

            try {
                Image img;
                if(PruebasLocales){
                    img = Image.getInstance(new File(".").getCanonicalPath()+"/"+billpdfDTO.getnit()+".jpg");
                }else{
                    img = Image.getInstance("/usr/share/nginx/html/logos/"+billpdfDTO.getnit()+".jpg");
                }
                PdfPCell imagen = new PdfPCell(img, true);
                imagen.setBorder(PdfPCell.NO_BORDER);
                imagen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos.addCell(imagen);
            }catch(Exception e){
                //System.out.println(e.toString());
                Image img = Image.getInstance("/usr/share/nginx/html/logos/tienda724.jpg");
                PdfPCell imagen = new PdfPCell(img, true);
                imagen.setBorder(PdfPCell.NO_BORDER);
                imagen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos.addCell(imagen);
            }

            //NOMBRE EMPRESA
            if(billpdfDTO.getempresa().equals("SUPERTIENDAS MERCAFULL")){
                CellMaker("SIEMPRE PENSANDO EN TI",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            }else{
                CellMaker(billpdfDTO.getempresa(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            }
            //NIT
            CellMaker(billpdfDTO.getnit(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //DIRECCIÓN
            CellMaker(billpdfDTO.getdireccion(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //TELEFONO
            CellMaker("Tel: "+billpdfDTO.gettelefono(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            if(restflag == null){
                restflag = new Long(0) ;
            }
            if(billpdfDTO.getdomiciliario().equals("Sin Asignación")){
                CellMaker(" ",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            }else{
                String frase = "Domiciliario: ";
                if(restflag == 1){
                    frase = "Mesero: ";
                }
                CellMaker(frase+billpdfDTO.getdomiciliario(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            }
            tablaDatos.addCell(cella);

            PdfPTable tablaData = new PdfPTable(2);

            //CLIENTE
            CellMaker("Cliente: "+removeWord(billpdfDTO.getcliente(),"null"),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            //DOCUMENTO CC
            CellMaker(billpdfDTO.getcedula(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablaData);
            //getdireccionC
            CellMaker(billpdfDTO.getdireccionC(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            //gettelefonoC
            CellMaker("Tel: "+billpdfDTO.gettelefonoC(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);

            //DATOS CLIENTE
            if(billpdfDTO.getcliente().equals("Cliente Ocasional")){
                tablaData = new PdfPTable(1);
                CellMaker("Cliente: "+removeWord(billpdfDTO.getcliente(),"null"),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            }

            PdfPTable TablaObservaciones = new PdfPTable(1);
            //TablaObservaciones.addCell(cella);
            //TITULO OBSERVACIONES
            CellMaker("Observaciones:",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,TablaObservaciones);
            String observaciones = billpdfDTO.getobservaciones();

            if(restflag == 1){
                String[] observacionesList = billpdfDTO.getobservaciones().split("-");
                observaciones = "";
                for(String a: observacionesList){
                    observaciones = observaciones + a + "\n";
                }
            }

            //LAS OBSERVACIONES
            CellMaker(observaciones,fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,TablaObservaciones);

            //TABLA DEBAJO DE OBSERVACIONES
            PdfPTable tablaFecha = new PdfPTable(1);
            tablaFecha.addCell(cella);
            //FECHA
            CellMaker(billpdfDTO.getfecha(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            //FACTURA
            CellMaker("Factura # "+billpdfDTO.getconsecutivo().replaceAll(" - ",""),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            //CAJERO
            CellMaker("Cajero: "+removeWord(billpdfDTO.getnombreCajero(),"null"),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            tablaFecha.addCell(cella);

            //TABLA TIENDA
            PdfPTable tablaDatos2 = new PdfPTable(2);

            //CAJERO
            CellMaker("Tienda: "+billpdfDTO.gettienda(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos2);

            if(billpdfDTO.getcaja().equals("-1")){
                //PEDIDO
                CellMaker("PEDIDO",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos2);

            }else{
                //Caja
                CellMaker("Caja: "+billpdfDTO.getcaja(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos2);
            }
            tablaDatos2.addCell(cella);
            tablaDatos2.addCell(cella);


            PdfPTable tablaDetalles = new PdfPTable(new float[] { new Float(0.75),new Float(2.25), 1 });

            //Caja
            CellMaker("Cant",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
            //Caja
            CellMaker("Producto",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
            //Caja
            CellMaker("Precio",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaDetalles);

            List<List<String>> theList = billpdfDTO.getdetalles();

            boolean containsService = false;
            Long serviceValue = new Long(0);

            for (int i = 0; i < billpdfDTO.getdetalles().size(); i++) {
                Long number = Math.round(new Double(theList.get(i).get(2)));
                if(theList.get(i).get(3).contains("SERVICIOVOLUNTARIO")){
                    serviceValue = number;
                    containsService = true;
                }else{
                    //CANTIDAD
                    CellMaker(theList.get(i).get(0),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);

                    //PRODUCTO
                    if(theList.get(i).get(1).length()<=23) {
                        CellMaker(theList.get(i).get(1),fontH2,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                    }else{
                        CellMaker(theList.get(i).get(1).substring(0, 23),fontH2,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                    }
                    //PRECIO
                    CellMaker(format.format(number),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaDetalles);
                }
            }

            if(containsService){
                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);

                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                CellMaker("Servicio",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                CellMaker(format.format(serviceValue),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaDetalles);

                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
                CellMaker(" ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaDetalles);
            }

            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            //
            PdfPTable tablaTotales = new PdfPTable(2);


            Long cambio = Math.round(cash-billpdfDTO.gettotalprice());
            String medioPago = billpdfDTO.getmedioPago();
            Long cantRecibida = billpdfDTO.getcambio() + round(billpdfDTO.gettotalprice());

            if(cambio<0){
                cambio = new Long(0);
                medioPago = "Múltiples Medios";
                cantRecibida = round(billpdfDTO.gettotalprice());
            }

            //
            if(discount != 0){
                //DESCUENTO
                CellMaker("Descuento",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
                CellMaker(format.format(new Long(Math.round(discount)))+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            }
            //SUBTOTAL
            CellMaker("Subtotal",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(new Long(Math.round(billpdfDTO.getsubtotal())))+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //IVA
            CellMaker("IVA",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(new Long(Math.round(billpdfDTO.gettax())))+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            if(discount != 0){
                //TOTAL SIN DESCUENTO
                CellMaker("Total sin Descuento",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
                CellMaker(format.format(new Long(Math.round(billpdfDTO.gettotalprice()+discount)))+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            }
            //TOTAL
            CellMaker("Total",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(new Long(Math.round(billpdfDTO.gettotalprice())))+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //CANTIDAD RECIBIDA
            CellMaker("Cantidad Recibida",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(cantRecibida)+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //CAMBIO
            CellMaker("Cambio",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(cambio)+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //MEDIO PAGO
            CellMaker("Medio pago",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(medioPago+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);

            PdfPTable tablaLegal = new PdfPTable(1);
            tablaLegal.addCell(cella);
            if(billpdfDTO.getregimenT()!= null && !billpdfDTO.getregimenT().isEmpty()){
                String regimenTemp;
                if(billpdfDTO.getregimenT().equals("C")){
                    regimenTemp = "Común";
                }else{
                    regimenTemp = "Simplificado";
                }
                CellMaker("Régimen "+regimenTemp,fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaLegal);
            }

            if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                CellMaker("Resolución Dian # "+billpdfDTO.getresolucion_DIAN(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaLegal);
            }

            CellMaker("Modalidad: POS",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaLegal);

            tablaLegal.addCell(cella);

            PdfPTable tabla = new PdfPTable(1);

            tablaDatos.setKeepTogether(false);
            tablaData.setKeepTogether(false);
            tablaFecha.setKeepTogether(false);
            tablaDatos2.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaTotales.setKeepTogether(false);
            tablaLegal.setKeepTogether(false);
            TablaObservaciones.setKeepTogether(false);

            tablaDatos.setSplitLate(false);
            tablaData.setSplitLate(false);
            tablaFecha.setSplitLate(false);
            tablaDatos2.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaTotales.setSplitLate(false);
            TablaObservaciones.setSplitLate(false);

            tablaLegal.setSplitLate(false);

            PdfPCell tablaDatosC = new PdfPCell(tablaDatos);
            PdfPCell tablaDataC = new PdfPCell(tablaData);
            PdfPCell tablaFechaC = new PdfPCell(tablaFecha);
            PdfPCell tablaDatos2C = new PdfPCell(tablaDatos2);
            PdfPCell tablaDetallesC = new PdfPCell(tablaDetalles);
            PdfPCell tablaTotalesC = new PdfPCell(tablaTotales);
            PdfPCell tablaLegalC = new PdfPCell(tablaLegal);
            PdfPCell tablaobs = new PdfPCell(TablaObservaciones);

            tablaDatosC.setBorder(PdfPCell.NO_BORDER);
            tablaDataC.setBorder(PdfPCell.NO_BORDER);
            tablaobs.setBorder(PdfPCell.NO_BORDER);
            tablaFechaC.setBorder(PdfPCell.NO_BORDER);
            tablaDatos2C.setBorder(PdfPCell.NO_BORDER);
            tablaDetallesC.setBorder(PdfPCell.NO_BORDER);
            tablaTotalesC.setBorder(PdfPCell.NO_BORDER);
            tablaLegalC.setBorder(PdfPCell.NO_BORDER);

            tabla.addCell(tablaDatosC);
            tabla.addCell(tablaDataC);
            tabla.addCell(tablaFechaC);
            tabla.addCell(tablaDatos2C);
            tabla.addCell(tablaDetallesC);
            //tabla.addCell(tablaDetalles2C);
            tabla.addCell(tablaTotalesC);
            tabla.addCell(tablaLegalC);
            tabla.addCell(tablaobs);

            tabla.setKeepTogether(false);
            tabla.setSplitLate(false);

            documento.add(tabla);
            response = nombrePdf.substring(0,nombrePdf.length()-8);

        }catch(Exception e){
            response = e.toString();
            //System.out.println("[Error en printPDF2] "+e.toString());
            e.printStackTrace();
        }
        try {
            documento.close();
            assert ficheroPdf != null;
            ficheroPdf.close();
            PaginarPDF(ruta,2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String> printPDF2compra(BillPdfDTO billpdfDTO,String fechaCompra,String numFactura,String numPedidoAsociado, Long id_third, Long idBill){
        // Se crea el documento
        Rectangle envelope = new Rectangle(596, 842);
        Document documento = new Document(envelope, 0f, 0f, 20f, 20f);
        //Document documento = new Document();
        documento.setPageSize(PageSize.LETTER.rotate());
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        String ruta = "";
        FileOutputStream ficheroPdf = null;
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
            //
            String empresa = billpdfDTO.getempresa();
            //DEFINO LAS FUENTES
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 8, Font.NORMAL);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);
            Font fontH4 = new Font(HELVETICA, 12, Font.NORMAL);
            //
            empresa = empresa.replaceAll(" ","_");
            //DEFINO NOMBRE Y RUTAS
            String nombrePdf = id_third+"_"+billpdfDTO.getconsecutivo().replace(" ","_").replaceAll("_-_","-")+".pdf_tmp.pdf";
            if(PruebasLocales){
                ruta = new File(".").getCanonicalPath()+"/"+ nombrePdf;
            }else{
                ruta = "/usr/share/nginx/html/facturas/"+ nombrePdf;
            }
            ficheroPdf = new FileOutputStream(ruta);
            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));


            //DEFINO TABLA
            float AnchoTablas = 90;
            PdfPTable tablaDatos = new PdfPTable(1);
            tablaDatos.setKeepTogether(false);
            tablaDatos.setSplitLate(false);
            tablaDatos.setWidthPercentage(AnchoTablas);

            PdfPCell cella =new PdfPCell(new Phrase("  ",fontH4));cella.setBorder(PdfPCell.NO_BORDER);

            //NOMBRE EMPRESA
            CellMaker(billpdfDTO.getempresa(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //NIT
            CellMaker(billpdfDTO.getnit(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //DIRECCIÓN
            CellMaker(billpdfDTO.getdireccion(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //TELEFONO
            CellMaker("Tel: "+billpdfDTO.gettelefono(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //TITULO FACTURA
            tablaDatos.addCell(cella);
            CellMaker("FACTURA DE COMPRA # "+billpdfDTO.getconsecutivo().replaceAll(" - ",""),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);

            try {
                Image img;
                if(PruebasLocales){
                    img = Image.getInstance(new File(".").getCanonicalPath()+"/"+billpdfDTO.getnit()+".jpg");
                }else{
                    img = Image.getInstance("/usr/share/nginx/html/logos/"+billpdfDTO.getnit()+".jpg");
                }
                img.scalePercent(50);
                Rectangle pageSize = documento.getPageSize();
                img.setAbsolutePosition(pageSize.getRight(0) - (img.getWidth()/2), pageSize.getTop(0) - (img.getHeight()/2));
                documento.add(img);
            }catch(Exception e){
                e.printStackTrace();
                Image img;
                img = Image.getInstance("/usr/share/nginx/html/logos/tienda724.jpg");
                img.scalePercent(50);
                Rectangle pageSize = documento.getPageSize();
                img.setAbsolutePosition(pageSize.getRight(0) - (img.getWidth()/2), pageSize.getTop(0) - (img.getHeight()/2));
                documento.add(img);
            }

            tablaDatos.addCell(cella);

            PdfPTable tablaData = new PdfPTable(3);
            tablaData.setWidthPercentage(AnchoTablas);
            float[] AnchosTabla = new float[]{1.9f,1f,1f};
            tablaData.setWidths(AnchosTabla);
            tablaData.setKeepTogether(false);
            tablaData.setSplitLate(false);

            //CLIENTE
            CellMaker("Proveedor:\n"+removeWord(billpdfDTO.getcliente(),"null"),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaData);
            //DOCUMENTO CC
            CellMaker(billpdfDTO.getcedula(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_BOTTOM,tablaData);
            //Fecha Compra PURCHASE_DATE_DOCUMENT
            CellMaker("Fecha Compra:\n" +(billpdfDTO.getPURCHASE_DATE_DOCUMENT() == null ? "\n-":billpdfDTO.getPURCHASE_DATE_DOCUMENT()),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            //getdireccionC
            CellMaker(billpdfDTO.getdireccionC(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaData);
            //gettelefonoC
            CellMaker("Tel: "+billpdfDTO.gettelefonoC(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaData);
            //Numero Factura
            CellMaker("N° Factura: " +numFactura,fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);

            if(billpdfDTO.getcliente().equals("Cliente Ocasional") ){
                tablaData = new PdfPTable(1);
                tablaData.setWidthPercentage(AnchoTablas);
                CellMaker("Proveedor: "+removeWord(billpdfDTO.getcliente(),"null"),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            }
            PdfPTable tablaFecha = new PdfPTable(4);
            tablaFecha.setWidthPercentage(AnchoTablas);
            float[] AnchosTabla2 = new float[]{1.1f,1.2f,2f,1f};
            tablaFecha.setWidths(AnchosTabla2);
            tablaFecha.setKeepTogether(false);
            tablaFecha.setSplitLate(false);
            EmptyCellMaker(4,PdfPCell.NO_BORDER,tablaFecha);

            //TITULO PEDIDO ASOCIADO
            CellMaker("Pedido asociado # ",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            //TITULO FECHA
            CellMaker("Fecha:",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            //TITULO CAJERO
            CellMaker("Cajero:",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            //TITULO OBSERVACIONES
            CellMaker("Observaciones:",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            //FACTURA
            CellMaker(numPedidoAsociado,fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            //Fecha
            CellMaker(fechaCompra,fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            //CAJERO
            CellMaker(removeWord(billpdfDTO.getnombreCajero(),"null"),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            //OBSERVACIONES
            CellMaker(billpdfDTO.getobservaciones(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaFecha);
            tablaFecha.addCell(cella);

            PdfPTable tablaDatos2 = new PdfPTable(2);
            tablaDatos2.setWidthPercentage(AnchoTablas);
            tablaDatos2.addCell(cella);
            tablaDatos2.addCell(cella);


            //Tienda
            CellMaker("Tienda: "+billpdfDTO.gettienda(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos2);

            if(billpdfDTO.getcaja().equals("-1")){
                //PEDIDO
                CellMaker("PEDIDO",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos2);

            }else{
                //Caja
                CellMaker("Caja: "+billpdfDTO.getcaja(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos2);
            }

            tablaDatos2.addCell(cella);
            tablaDatos2.addCell(cella);


            PdfPTable tablaDetalles = new PdfPTable(10);
            tablaDetalles.setWidthPercentage(95);

            tablaDetalles.setWidths(new int[]{2,1,1,4,1,2,1,1,2,2});

            //Codigo de barras
            CellMaker("Cod",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            //Codigo de Proveedor
            CellMaker("Cod Prov",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            //Margen
            CellMaker("Marg",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            //Producto
            CellMaker("Prod",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            //Cantidad
            CellMaker("Cant",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            //Precio
            CellMaker("Val Comp",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            //Descuento
            CellMaker("Desc",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            //IVA
            CellMaker("IVA",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            //SubTot
            CellMaker("Total",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            //Total
            CellMaker("Total Iva",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);


            List<detallePdf> theList = billDAO.getDetallesPdf(idBill);
            boolean LeTocaGris = false;
            int R = 180;
            int G = 180;
            int B = 180;
            for (detallePdf strings : theList) {

                System.out.println("-------------------CODIGOS A USAR---------------");
                System.out.println("CODIGO: "+strings.getCODIGO());
                System.out.println("ID_STORE: "+strings.getID_STORE());

                MargenData data = billDAO.getMargenData(strings.getCODIGO(), strings.getID_STORE());

                Double precio = data.getPrice();
                if(precio.toString().equals("0")){
                    System.out.println("YIN");
                    precio = new Double (1);
                }

                Double Margen = rounder((100*(precio-new Double(strings.getVALORCOMPRA()))/precio),1) ;

                //Double Margen = rounder(((1-(new Double (strings.getVALORCOMPRA()) / precio))*100),1);
                Font fontH1TMP = new Font(fontH1.getBaseFont(), fontH1.getSize(), fontH1.getStyle());
                if (LeTocaGris) {
                    fontH1TMP.setColor(BaseColor.WHITE);
                } else {
                    fontH1TMP.setColor(BaseColor.BLACK);
                }
                BaseColor fondo = LeTocaGris ? new BaseColor(R, G, B) : BaseColor.WHITE;


                //Código
                CellMaker(strings.getCODIGO(), fontH1TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);
                //Código Proveedor
                CellMaker(strings.getCODIGOPROV(), fontH1TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);
                //Margen
                CellMaker(Margen.toString(), fontH1TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);
                //Producto
                CellMaker(strings.getPRODUCTO(), fontH1TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);
                //Cantidad
                CellMaker(strings.getCANTIDAD(), fontH1TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);
                //Precio
                CellMaker(format.format(new Double(strings.getVALORCOMPRA())), fontH1TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);
                String text = strings.getDESCUENTO();
                if(strings.getDESCUENTO()==null){
                    text = "0";
                }
                //Descuento
                CellMaker(text+"%", fontH1TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);
                //Iva
                CellMaker(strings.getIVA(), fontH1TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);
                //Subtotal
                CellMaker(format.format(new Double(strings.getTOTAL())), fontH1TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);
                //Total
                CellMaker(format.format(new Double(strings.getTOTALCONIVA())), fontH1TMP, PdfPCell.NO_BORDER, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);


                /*//Ubicación

                CellMaker("Pendiente",fontH1TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles,fondo);*/

                LeTocaGris = !LeTocaGris;
            }

            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);


            Long cambio = billpdfDTO.getcambio();
            String medioPago = billpdfDTO.getmedioPago();
            Long cantRecibida = billpdfDTO.getcambio() + round(billpdfDTO.gettotalprice());

            if(billpdfDTO.getcambio()<0){
                cambio = new Long(0);
                medioPago = "Múltiples Medios";
                cantRecibida = round(billpdfDTO.gettotalprice());
            }


            PdfPTable tablaTotales = new PdfPTable(2);
            tablaTotales.setWidthPercentage(AnchoTablas);
            tablaTotales.addCell(cella);
            tablaTotales.addCell(cella);
            //
            //SUBTOTAL
            CellMaker("Subtotal",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(billpdfDTO.getsubtotal())+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //IVA
            CellMaker("IVA",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(billpdfDTO.gettax())+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //TOTAL
            CellMaker("Total",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(billpdfDTO.gettotalprice())+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //CANTIDAD RECIBIDA
            CellMaker("Cantidad Recibida",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(cantRecibida)+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //CAMBIO
            CellMaker("Cambio",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(cambio)+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //MEDIO PAGO
            CellMaker("Medio pago",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(medioPago+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);

            PdfPTable tablaLegal = new PdfPTable(1);
            tablaLegal.setWidthPercentage(AnchoTablas);
            tablaLegal.addCell(cella);
            if(billpdfDTO.getregimenT()!= null && !billpdfDTO.getregimenT().isEmpty()){
                String regimenTemp;
                if(billpdfDTO.getregimenT().equals("C")){
                    regimenTemp = "Común";
                }else{
                    regimenTemp = "Simplificado";
                }
                CellMaker("Régimen "+regimenTemp,fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaLegal);
            }

            if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                CellMaker("Resolución Dian # "+billpdfDTO.getresolucion_DIAN(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaLegal);
            }

            PdfPCell cellPos = new PdfPCell(new Phrase("Modalidad: POS",fontH4));
            cellPos.setBorder(PdfPCell.NO_BORDER);
            cellPos.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaLegal.addCell(cellPos);

            tablaLegal.addCell(cella);

            PdfPTable tabla = new PdfPTable(1);
            tabla.setWidthPercentage(AnchoTablas);

            tablaDatos.setKeepTogether(false);
            tablaData.setKeepTogether(false);
            tablaFecha.setKeepTogether(false);
            tablaDatos2.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaTotales.setKeepTogether(false);
            tablaLegal.setKeepTogether(false);

            tablaDatos.setSplitLate(false);
            tablaData.setSplitLate(false);
            tablaFecha.setSplitLate(false);
            tablaDatos2.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaTotales.setSplitLate(false);
            tablaLegal.setSplitLate(false);

            PdfPCell tablaDatosC = new PdfPCell(tablaDatos);
            PdfPCell tablaDataC = new PdfPCell(tablaData);
            PdfPCell tablaFechaC = new PdfPCell(tablaFecha);
            PdfPCell tablaDatos2C = new PdfPCell(tablaDatos2);
            PdfPCell tablaDetallesC = new PdfPCell(tablaDetalles);
            PdfPCell tablaTotalesC = new PdfPCell(tablaTotales);
            PdfPCell tablaLegalC = new PdfPCell(tablaLegal);

            tablaDatosC.setBorder(PdfPCell.NO_BORDER);
            tablaDataC.setBorder(PdfPCell.NO_BORDER);
            tablaFechaC.setBorder(PdfPCell.NO_BORDER);
            tablaDatos2C.setBorder(PdfPCell.NO_BORDER);
            tablaDetallesC.setBorder(PdfPCell.NO_BORDER);
            tablaTotalesC.setBorder(PdfPCell.NO_BORDER);
            tablaLegalC.setBorder(PdfPCell.NO_BORDER);

            tabla.addCell(tablaDatosC);
            tabla.addCell(tablaDataC);
            tabla.addCell(tablaFechaC);
            tabla.addCell(tablaDatos2C);
            tabla.addCell(tablaDetallesC);
            tabla.addCell(tablaTotalesC);
            //tabla.addCell(tablaLegalC);

            tabla.setKeepTogether(false);
            tabla.setSplitLate(false);

            documento.add(tabla);

            response = nombrePdf.substring(0,nombrePdf.length()-8);

        }catch(Exception e){
            e.printStackTrace();
            response = e.toString();
        }
        try {
            documento.close();
            assert ficheroPdf != null;
            ficheroPdf.close();
            PaginarPDF(ruta,1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }

    private static double rounder (double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    public Either<IException, String> printInBillPDF(BillinPDF billinPDF,LegalData billlegaldata, Long id_third){
        // Se crea el documento
        Rectangle envelope = new Rectangle(596, 842);
        Document documento = new Document(envelope, 0f, 0f, 20f, 20f);
        String response = "Bad!!!";
        String ruta = "";
        FileOutputStream ficheroPdf = null;
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
            String empresa = billinPDF.getnombreEmpresa().replace(" ","_");
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 12, Font.NORMAL);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);
            Font fontH4 = new Font(HELVETICA, 12, Font.NORMAL);
            empresa = empresa.replaceAll(" ","_");
            String nombrePdf = id_third+"_"+billinPDF.getfecha().replaceAll(" ","_").replaceAll(":","_").replaceAll("/","_").replaceAll(",","_")+new Random().nextInt(10000)+".pdf_tmp.pdf";
            if(PruebasLocales){
                ruta = new File(".").getCanonicalPath()+"/"+ nombrePdf;
            }else{
                ruta = "/usr/share/nginx/html/facturas/"+ nombrePdf;
            }
            ficheroPdf = new FileOutputStream(ruta);

            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));

            float AnchoTablas = 95;
            PdfPTable tablaDatos = new PdfPTable(1);
            tablaDatos.setKeepTogether(false);
            tablaDatos.setSplitLate(false);
            tablaDatos.setWidthPercentage(AnchoTablas);
            //NOMBRE EMPRESA
            CellMaker(empresa.replaceAll("_"," "),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //NOMBRE TIENDA
            //CellMaker(billinPDF.getnombreEmpresa(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //NIT
            CellMaker(billinPDF.getNIT(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //DIRECCIÓN
            CellMaker(billlegaldata.getCITY_NAME()+", "+billlegaldata.getADDRESS(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //TELEFONO
            CellMaker("Tel: "+billlegaldata.getPHONE1(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //espacio vacio
            CellMaker(" ",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //TITULO DEL CASO
            CellMaker(billinPDF.getcaso().equals("1") ? "DOCUMENTO DE ENTRADA DE MERCANCÍA":"DOCUMENTO DE SALIDA DE MERCANCÍA",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //espacio vacio
            CellMaker(" ",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            try {
                Image img;
                if(PruebasLocales){
                    img = Image.getInstance("./"+billinPDF.getNIT()+".jpg");
                }else{
                    img = Image.getInstance("/usr/share/nginx/html/logos/"+billinPDF.getNIT()+".jpg");
                }
                img.scalePercent(45);
                Rectangle pageSize = documento.getPageSize();
                img.setAbsolutePosition(pageSize.getRight(5) - (img.getWidth()/2), pageSize.getTop(5) - (img.getHeight()/2));
                documento.add(img);
            }catch(Exception e){
                //System.out.println(e.toString());
                Image img;
                img = Image.getInstance("/usr/share/nginx/html/logos/tienda724.jpg");
                img.scalePercent(45);
                Rectangle pageSize = documento.getPageSize();
                img.setAbsolutePosition(pageSize.getRight(5) - (img.getWidth()/2), pageSize.getTop(5) - (img.getHeight()/2));
                documento.add(img);
            }
            //TABLA RAZÓN Y FECHA
            PdfPTable tablafecha = new PdfPTable(4);
            tablafecha.setWidths(new float[]{1f,1f,1f,1f});
            tablafecha.setKeepTogether(false);
            tablafecha.setSplitLate(false);
            tablafecha.setWidthPercentage(AnchoTablas);
            //Tienda
            CellMaker("Tienda",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablafecha,7);
            //Consecutivo
            CellMaker("Consecutivo",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablafecha,7);
            //Fecha
            CellMaker("Fecha",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablafecha,7);
            //Razón
            CellMaker("Razón",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablafecha,7);
            //La Tienda
            CellMaker(billinPDF.gettienda(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablafecha);
            //El Consecutivo
            CellMaker(billinPDF.getnum_documento(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablafecha);
            //La Fecha
            CellMaker(billinPDF.getfecha(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablafecha);
            //La Razón
            CellMaker(billinPDF.getrazon(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablafecha);
            //Agrego espacios vacios
            EmptyCellMaker(8,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablafecha,-1);
            //TABLA OBSERVACIONES
            PdfPTable tablaOBS = new PdfPTable(2);
            tablaOBS.setWidths(new float[]{1f,3f});
            tablaOBS.setKeepTogether(false);
            tablaOBS.setSplitLate(false);
            tablaOBS.setWidthPercentage(AnchoTablas);
            //Observaciones
            CellMaker("Observaciones",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_TOP,tablaOBS);
            //las Observaciones
            CellMaker(billinPDF.getobs(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_JUSTIFIED,Element.ALIGN_MIDDLE,tablaOBS);
            //Agrego espacios vacios
            EmptyCellMaker(4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablaOBS,-1);
            //TODO TABLA
            PdfPTable tablaDetalles = new PdfPTable(3);
            float[] AnchosTabla = new float[]{1f,2.9f,0.7f};
            tablaDetalles.setWidths(AnchosTabla);
            tablaDetalles.setKeepTogether(false);
            tablaDetalles.setSplitLate(false);
            //Código
            CellMaker("Código",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            //Producto
            CellMaker("Producto",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            //Cantidad
            CellMaker("Cantidad",fontH4,PdfPCell.NO_BORDER | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
            /*//Ubicación
            CellMaker("Ubicación",fontH4,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);*/

            List<List<String>> theList = billinPDF.getdetalles();
            boolean LeTocaGris = false;
            int R = 180;
            int G = 180;
            int B = 180;
            for (List<String> strings : theList) {
                Font fontH1TMP = new Font(fontH1.getBaseFont(), fontH1.getSize(), fontH1.getStyle());
                if (LeTocaGris) {
                    fontH1TMP.setColor(BaseColor.WHITE);
                } else {
                    fontH1TMP.setColor(BaseColor.BLACK);
                }
                BaseColor fondo = LeTocaGris ? new BaseColor(R, G, B) : BaseColor.WHITE;

                //Código
                CellMaker(strings.get(3), fontH1TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);
                //Producto
                CellMaker(strings.get(1), fontH1TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);
                //Cantidad
                CellMaker(strings.get(0), fontH1TMP, PdfPCell.NO_BORDER, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, fondo);
                /*//Ubicación
                CellMaker("Pendiente",fontH1TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles,fondo);*/

                LeTocaGris = !LeTocaGris;
            }
            //
            PdfPTable tabla = new PdfPTable(1);
            tabla.setKeepTogether(false);
            tabla.setSplitLate(false);
            //agrego tablas a la tabla principal
            CellMaker(tablaDatos,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tabla);
            CellMaker(tablafecha,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tabla);
            CellMaker(tablaOBS,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tabla);
            CellMaker(tablaDetalles,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tabla);
            //
            documento.add(tabla);
            response = nombrePdf.substring(0,nombrePdf.length()-8);

        }catch(Exception e){
            e.printStackTrace();
            response = e.toString();
        }
        try {
            documento.close();
            assert ficheroPdf != null;
            ficheroPdf.close();
            PaginarPDF(ruta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> printPDF4(BillPdfDTO2 billpdfDTO, long caso, Long id_third){
        // Se crea el documento
        Rectangle envelope = new Rectangle(596, 842);
        Document documento = new Document(envelope, 0f, 0f, 20f, 20f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        String ruta = "";
        FileOutputStream ficheroPdf = null;
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
            String empresa = billpdfDTO.getempresa();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH2 = new Font(HELVETICA, 10, Font.NORMAL);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);
            Font fontH4 = new Font(HELVETICA, 12, Font.NORMAL);
            empresa = empresa.replaceAll(" ","_");
            String nombrePdf = "";
            if(caso == 1){
                nombrePdf = id_third+"_"+billpdfDTO.getconsecutivo().replace(" ","_").replaceAll("_-_","-")+"SALIDA.pdf_tmp.pdf";
            }else if(caso == 2){
                nombrePdf = id_third+"_"+billpdfDTO.getconsecutivo().replace(" ","_").replaceAll("_-_","-")+".pdf_tmp.pdf";
            }
            if(PruebasLocales){
                ruta = new File(".").getCanonicalPath()+"/"+ nombrePdf;
            }else{
                ruta = "/usr/share/nginx/html/facturas/"+ nombrePdf;
            }
            ficheroPdf = new FileOutputStream(ruta);

            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));

            float AnchoTablas = 92;
            PdfPTable tablaDatos = new PdfPTable(1);
            tablaDatos.setWidthPercentage(AnchoTablas);
            //NOMBRE EMPRESA
            CellMaker(billpdfDTO.getempresa(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //NOMBRE TIENDA
            CellMaker(billpdfDTO.gettienda(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //NIT
            CellMaker(billpdfDTO.getnit(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //DIRECCIÓN
            CellMaker(billpdfDTO.getdireccion(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //TELEFONO
            CellMaker("Tel: "+billpdfDTO.gettelefono(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //
            PdfPCell cell2 = new PdfPCell();
            if(caso == 1){
                cell2 =new PdfPCell(new Phrase("Remision # "+billpdfDTO.getconsecutivo().replaceAll(" - ",""),fontH4));
            }else if(caso == 2){
                cell2 =new PdfPCell(new Phrase("Factura de Venta # "+billpdfDTO.getconsecutivo().replaceAll(" - ",""),fontH4));
            }
            //
            PdfPCell cella =new PdfPCell(new Phrase("  ",fontH4));
            PdfPCell regimen =new PdfPCell(new Phrase(billpdfDTO.getregimen(),fontH4));
            PdfPCell nombreCajero =new PdfPCell(new Phrase("Cajero: "+removeWord(billpdfDTO.getnombreCajero(),"null"),fontH4));
            PdfPCell documentoCC = new PdfPCell(new Phrase(billpdfDTO.getcedula(),fontH4));



            documentoCC.setHorizontalAlignment(Element.ALIGN_CENTER);
            documentoCC.setBorder(PdfPCell.NO_BORDER);
            cell2.setBorder(PdfPCell.NO_BORDER);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cella.setBorder(PdfPCell.NO_BORDER);
            regimen.setHorizontalAlignment(Element.ALIGN_CENTER);
            regimen.setBorder(PdfPCell.NO_BORDER);
            nombreCajero.setHorizontalAlignment(Element.ALIGN_CENTER);
            nombreCajero.setBorder(PdfPCell.NO_BORDER);
            try {
                Image img;
                if(PruebasLocales){
                    img = Image.getInstance("./"+billpdfDTO.getnit()+".jpg");
                }else{
                    img = Image.getInstance("/usr/share/nginx/html/logos/"+billpdfDTO.getnit()+".jpg");
                }
                img.scalePercent(50);
                Rectangle pageSize = documento.getPageSize();
                img.setAbsolutePosition(pageSize.getRight(0) - (img.getWidth()/2), pageSize.getTop(0) - (img.getHeight()/2));
                //img.setAbsolutePosition(500f, 500f);
                documento.add(img);
                /*PdfPCell imagen = new PdfPCell(img, false);
                imagen.setBorder(PdfPCell.NO_BORDER);
                imagen.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tablaDatos.addCell(imagen);*/
            }catch(Exception e){
                //System.out.println(e.toString());
                Image img;
                    img = Image.getInstance("/usr/share/nginx/html/logos/tienda724.jpg");
                img.scalePercent(50);
                Rectangle pageSize = documento.getPageSize();
                img.setAbsolutePosition(pageSize.getRight(0) - (img.getWidth()/2), pageSize.getTop(0) - (img.getHeight()/2));
                //img.setAbsolutePosition(500f, 500f);
                documento.add(img);
            }
            tablaDatos.addCell(cella);

            ClientePedido client = billpdfDTO.getclientData();

            String tmp =  client.getNUM_DOCUMENT();
            try{
                if(tmp == null){
                    tmp = "";
                }else if(tmp.contains("null")){
                    tmp = client.getNUM_DOCUMENT().replace("null","");
                }
            }catch (Exception e){
                tmp = "";
            }

            PdfPCell numdoc =new PdfPCell(new Phrase("Documento pedido: "+tmp,fontH4));
            numdoc.setHorizontalAlignment(Element.ALIGN_CENTER);
            numdoc.setBorder(PdfPCell.NO_BORDER);

            PdfPTable tablaData = new PdfPTable(2);
            tablaData.setWidthPercentage(AnchoTablas);

            //tablaData.addCell(direccion);
            //tablaData.addCell(telefono);
            //tablaData.addCell(cell2);
            //tablaData.addCell(nombreCajero);
            if(caso == 2){
                CellMaker("Empresa: "+billpdfDTO.getcliente().replace("null",""),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            }
            else{
                tablaData.addCell(cella);
            }
            //Direccion cliente
            CellMaker("Dirección: "+client.getADDRESS().replace("null",""),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            //Nombre cliente
            CellMaker("Tienda: "+client.getDESCRIPTION().replace("null",""),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            //Mail cliente
            CellMaker("Correo: "+client.getMAIL().replace("null",""),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            //Phone cliente
            CellMaker("Telefono: "+client.getPHONE().replace("null",""),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            //Purchdate cliente
            CellMaker("Fecha de pedido:"+client.getPURCHASE_DATE().replace("null","").split(" ")[0],fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaData);
            tablaData.addCell(cella);
            //System.out.println("THIS I NEED" + billpdfDTO.getcliente());
            //tablaData.addCell(documentoCC);
            PdfPCell direCl =new PdfPCell(new Phrase(billpdfDTO.getdireccionC(),fontH4));
            PdfPCell telCl =new PdfPCell(new Phrase("Tel: "+billpdfDTO.gettelefonoC(),fontH4));
            direCl.setHorizontalAlignment(Element.ALIGN_CENTER);
            direCl.setBorder(PdfPCell.NO_BORDER);
            telCl.setHorizontalAlignment(Element.ALIGN_CENTER);
            telCl.setBorder(PdfPCell.NO_BORDER);
            //tablaData.addCell(direCl);
            //tablaData.addCell(telCl);

            PdfPTable tablaFecha = new PdfPTable(1);
            tablaFecha.setWidthPercentage(AnchoTablas);
            PdfPCell cell3 =new PdfPCell(new Phrase(billpdfDTO.getfecha(),fontH4));
            cell3.setBorder(PdfPCell.NO_BORDER);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaFecha.addCell(cella);
            tablaFecha.addCell(cell3);
            tablaFecha.addCell(cell2);

            PdfPTable tablaDatos2 = new PdfPTable(1);
            tablaDatos2.setWidthPercentage(AnchoTablas);

            if(caso == 1 || caso == 2){
                PdfPCell cell4 =new PdfPCell(new Phrase("Documento pedido: "+client.getNUM_DOCUMENT().replace("null",""),fontH4));
                cell4.setBorder(PdfPCell.NO_BORDER);
                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos2.addCell(cell4);
            }
            tablaDatos2.addCell(cella);
            tablaDatos2.addCell(cella);

            PdfPTable tablaDetalles = new PdfPTable(7);
            tablaDetalles.setWidthPercentage(AnchoTablas);
            float[] AnchosTabla = new float[]{3.3f,1.4f,0.6f,0.7f,1,1,1};
            tablaDetalles.setWidths(AnchosTabla);

            //TITULOS COLUMNAS
            float Padding = 5f;

            //Producto
            CellMaker("Producto",fontH4,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles,Padding);
            //Codigo Barras
            CellMaker("Codigo Barras",fontH4,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles,Padding);
            //CANTIDAD
            CellMaker("Cant",fontH4,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles,0);
            //IVA
            CellMaker("IVA",fontH4,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles,Padding);
            //Precio Unitario Sin IVA
            CellMaker("Precio Unitario Sin IVA",fontH4,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles,Padding);
            //Precio Sin IVA
            CellMaker("Precio Sin IVA",fontH4,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles,Padding);
            //Precio Con IVA
            CellMaker("Precio Con IVA",fontH4,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles,Padding);

            boolean LeTocaGris = false;
            int R = 180;
            int G = 180;
            int B = 180;

            List<List<String>> theList = billpdfDTO.getdetalles();

            for (List<String> strings : theList) {
                //theList.get(i).GET 0 -> CANTIDAD
                //theList.get(i).GET 1 -> NOMBRE
                //theList.get(i).GET 2 -> PRECIO SIN IVA
                //theList.get(i).GET 3 -> CODIGO BARRAS
                //theList.get(i).GET 4 -> IVA
                //theList.get(i).GET 5 -> PRECIO CON IVA

                Font fontH2TMP = new Font(fontH2.getBaseFont(), fontH2.getSize(), fontH2.getStyle());

                if (LeTocaGris) {
                    fontH2TMP.setColor(BaseColor.WHITE);
                } else {
                    fontH2TMP.setColor(BaseColor.BLACK);
                }

                //NOMBRE
                String Contenido = strings.get(1);
                if (Contenido.length() > 45) {
                    Contenido = Contenido.substring(0, 45);
                }
                CellMaker(Contenido, fontH2TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, LeTocaGris ? new BaseColor(R, G, B) : BaseColor.WHITE, Padding);

                //CODIGO BARRAS
                CellMaker(strings.get(3), fontH2TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, LeTocaGris ? new BaseColor(R, G, B) : BaseColor.WHITE, Padding);

                //CANTIDAD
                CellMaker(strings.get(0), fontH2TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, LeTocaGris ? new BaseColor(R, G, B) : BaseColor.WHITE, Padding);

                //IVA
                CellMaker(strings.get(4) + " %", fontH2TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, LeTocaGris ? new BaseColor(R, G, B) : BaseColor.WHITE, Padding);

                //Precio Unitario Sin IVA
                CellMaker(format.format(roundAvoid(Double.parseDouble(strings.get(5)) / Double.parseDouble(strings.get(0)), 2)), fontH2TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, LeTocaGris ? new BaseColor(R, G, B) : BaseColor.WHITE, Padding);

                //PRECIO SIN IVA
                CellMaker(format.format(roundAvoid(Double.parseDouble(strings.get(5)), 2)), fontH2TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, LeTocaGris ? new BaseColor(R, G, B) : BaseColor.WHITE, Padding);

                //PRECIO CON IVA
                CellMaker(format.format(roundAvoid(Double.parseDouble(strings.get(2)), 2)), fontH2TMP, PdfPCell.RIGHT, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, tablaDetalles, LeTocaGris ? new BaseColor(R, G, B) : BaseColor.WHITE, Padding);
                //
                LeTocaGris = !LeTocaGris;
            }

            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);

            Long cambio = billpdfDTO.getcambio();
            String medioPago = billpdfDTO.getmedioPago();
            Long cantRecibida = billpdfDTO.getcambio() + round(billpdfDTO.gettotalprice());

            if(billpdfDTO.getcambio()<0){
                cambio = new Long(0);
                medioPago = "Múltiples Medios";
                cantRecibida = round(billpdfDTO.gettotalprice());
            }

            PdfPTable tablaTotales = new PdfPTable(2);
            tablaTotales.setWidthPercentage(AnchoTablas);
            PdfPCell cell12 = new PdfPCell(new Phrase("Subtotal",fontH4));
            PdfPCell cell13 = new PdfPCell(new Phrase(format.format(billpdfDTO.getsubtotal())+"",fontH4));
            PdfPCell cellPago = new PdfPCell(new Phrase("Cantidad Recibida",fontH4));
            PdfPCell pago = new PdfPCell(new Phrase(format.format(cantRecibida)+"",fontH4));
            PdfPCell cell14 = new PdfPCell(new Phrase("IVA",fontH4));
            PdfPCell cell15 = new PdfPCell(new Phrase(format.format(billpdfDTO.gettax())+"",fontH4));
            PdfPCell cellcambio = new PdfPCell(new Phrase("Cambio",fontH4));
            PdfPCell celcambio2 = new PdfPCell(new Phrase(format.format(cambio)+"",fontH4));

            PdfPCell cell16 = new PdfPCell(new Phrase("Total",fontH4));
            PdfPCell cell17 = new PdfPCell(new Phrase(format.format(billpdfDTO.gettotalprice())+"",fontH4));
            PdfPCell cell16medio = new PdfPCell(new Phrase("Medio pago",fontH4));
            PdfPCell cell17medio = new PdfPCell(new Phrase(medioPago+"",fontH4));
            cell12.setBorder(PdfPCell.NO_BORDER);
            cell13.setBorder(PdfPCell.NO_BORDER);
            cell14.setBorder(PdfPCell.NO_BORDER);
            cell15.setBorder(PdfPCell.NO_BORDER);
            cell16.setBorder(PdfPCell.NO_BORDER);
            cell17.setBorder(PdfPCell.NO_BORDER);
            cellcambio.setBorder(PdfPCell.NO_BORDER);
            celcambio2.setBorder(PdfPCell.NO_BORDER);
            cell16medio.setBorder(PdfPCell.NO_BORDER);
            cell17medio.setBorder(PdfPCell.NO_BORDER);
            cellPago.setBorder(PdfPCell.NO_BORDER);
            pago.setBorder(PdfPCell.NO_BORDER);
            cellPago.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pago.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell13.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell14.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell15.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell16.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell17.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cellcambio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celcambio2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell16medio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell17medio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaTotales.addCell(cell12);
            tablaTotales.addCell(cell13);
            tablaTotales.addCell(cell14);
            tablaTotales.addCell(cell15);
            tablaTotales.addCell(cell16);
            tablaTotales.addCell(cell17);
            tablaTotales.addCell(cellPago);
            tablaTotales.addCell(pago);
            tablaTotales.addCell(cellcambio);
            tablaTotales.addCell(celcambio2);
            tablaTotales.addCell(cell16medio);
            tablaTotales.addCell(cell17medio);



            PdfPTable tablaLegal = new PdfPTable(1);
            tablaLegal.setWidthPercentage(AnchoTablas);
            tablaLegal.addCell(cella);
            if(billpdfDTO.getregimenT()!= null && !billpdfDTO.getregimenT().isEmpty()){
                String regimenTemp;
                if(billpdfDTO.getregimenT().equals("C")){
                    regimenTemp = "Común";
                }else{
                    regimenTemp = "Simplificado";
                }
                PdfPCell cellRegimen = new PdfPCell(new Phrase("Régimen "+regimenTemp,fontH4));
                cellRegimen.setBorder(PdfPCell.NO_BORDER);
                cellRegimen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellRegimen);
            }

            if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                PdfPCell cellAuth = new PdfPCell(new Phrase("Resolución Dian # "+billpdfDTO.getresolucion_DIAN(),fontH4));
                cellAuth.setBorder(PdfPCell.NO_BORDER);
                cellAuth.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellAuth);
            }

           /* if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                PdfPCell cellDatos = new PdfPCell(new Phrase("Fecha Formalización: 13 de junio de 2019",fontH2));
                cellDatos.setBorder(PdfPCell.NO_BORDER);
                cellDatos.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellDatos);
            }    */

            PdfPCell cellPos = new PdfPCell(new Phrase("Modalidad: POS",fontH4));
            cellPos.setBorder(PdfPCell.NO_BORDER);
            cellPos.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaLegal.addCell(cellPos);

            //if(billpdfDTO.getinitial_RANGE()!= null && !billpdfDTO.getinitial_RANGE().isEmpty() &&
            //        billpdfDTO.getfinal_RANGE()!= null && !billpdfDTO.getfinal_RANGE().isEmpty() &&
            //        billpdfDTO.getprefix()!= null && !billpdfDTO.getprefix().isEmpty()){
            //    PdfPCell cellAutor = new PdfPCell(new Phrase("Autorización: Desde "+billpdfDTO.getprefix()+billpdfDTO.getinitial_RANGE()+" a "+billpdfDTO.getprefix()+billpdfDTO.getfinal_RANGE(),fontH4));
            //    cellAutor.setBorder(PdfPCell.NO_BORDER);
            //    cellAutor.setHorizontalAlignment(Element.ALIGN_CENTER);
            //    tablaLegal.addCell(cellAutor);
            //}



            tablaLegal.addCell(cella);

            PdfPTable tabla = new PdfPTable(1);
            tabla.setWidthPercentage(AnchoTablas);


            tablaDatos.setKeepTogether(false);
            tablaData.setKeepTogether(false);
            tablaFecha.setKeepTogether(false);
            tablaDatos2.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaTotales.setKeepTogether(false);
            tablaLegal.setKeepTogether(false);

            tablaDatos.setSplitLate(false);
            tablaData.setSplitLate(false);
            tablaFecha.setSplitLate(false);
            tablaDatos2.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaLegal.setSplitLate(false);
            tablaTotales.setSplitLate(false);



            PdfPCell tablaDatosC = new PdfPCell(tablaDatos);
            PdfPCell tablaDataC = new PdfPCell(tablaData);
            PdfPCell tablaFechaC = new PdfPCell(tablaFecha);
            PdfPCell tablaDatos2C = new PdfPCell(tablaDatos2);
            PdfPCell tablaDetallesC = new PdfPCell(tablaDetalles);
            PdfPCell tablaLegalC = new PdfPCell(tablaLegal);
            PdfPCell tablaTotalesC = new PdfPCell(tablaTotales);


            tablaTotalesC.setBorder(PdfPCell.NO_BORDER);
            tablaDatosC.setBorder(PdfPCell.NO_BORDER);
            tablaDataC.setBorder(PdfPCell.NO_BORDER);
            tablaFechaC.setBorder(PdfPCell.NO_BORDER);
            tablaDatos2C.setBorder(PdfPCell.NO_BORDER);
            tablaDetallesC.setBorder(PdfPCell.NO_BORDER);
            tablaLegalC.setBorder(PdfPCell.NO_BORDER);


            tabla.addCell(tablaDatosC);
            tabla.addCell(tablaDataC);
            tabla.addCell(tablaFechaC);
            tabla.addCell(tablaDatos2C);
            tabla.addCell(tablaDetallesC);
            tabla.addCell(tablaTotalesC);
            tabla.addCell(tablaLegalC);

            tabla.setKeepTogether(false);
            tabla.setSplitLate(false);

            documento.add(tabla);
            response = nombrePdf.substring(0,nombrePdf.length()-8);

        }catch(Exception e){
            e.printStackTrace();
            response = e.toString();
        }
        try {
            documento.close();
            assert ficheroPdf != null;
            ficheroPdf.close();
            PaginarPDF(ruta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> printPDF(BillPdfDTO billpdfDTO){
        // Se crea el documento
        Rectangle envelope = new Rectangle(227, 842);
        Document documento = new Document(envelope, 0f, 0f, 10f, 0f);
        //Document documento = new Document();0

        String response = "Bad!!!";
        try { // Se crea el OutputStream para el fichero donde queremos dejar el pdf.


            String empresa = billpdfDTO.getempresa();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 8, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 8, Font.NORMAL);
            empresa = empresa.replaceAll(" ", "_");
            String nombrePdf = empresa + "_" + billpdfDTO.getconsecutivo().replace(" ", "_").replaceAll("_-_","-") + ".pdf";
            FileOutputStream ficheroPdf;
            if(PruebasLocales){
                ficheroPdf = new FileOutputStream("./" + nombrePdf);
            }else{
                ficheroPdf = new FileOutputStream("/usr/share/nginx/html/facturas/"+ nombrePdf);
            }

            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento, ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));


            PdfPTable tablaDatos = new PdfPTable(1);
            PdfPCell cell1 = new PdfPCell(new Phrase(billpdfDTO.getempresa(), fontH1));
            PdfPCell nit = new PdfPCell(new Phrase(billpdfDTO.getnit(), fontH1));
            PdfPCell direccion = new PdfPCell(new Phrase(billpdfDTO.getdireccion(), fontH2));
            PdfPCell telefono = new PdfPCell(new Phrase(billpdfDTO.gettelefono(), fontH2));
            PdfPCell cell2 = new PdfPCell(new Phrase("Factura # " + billpdfDTO.getconsecutivo().replaceAll(" - ", ""), fontH2));
            PdfPCell cella = new PdfPCell(new Phrase("  ", fontH2));
            PdfPCell regimen = new PdfPCell(new Phrase(billpdfDTO.getregimen(), fontH2));
            PdfPCell nombreCajero = new PdfPCell(new Phrase("Cajero: " + removeWord(billpdfDTO.getnombreCajero(),"null"), fontH2));
            PdfPCell cliente = new PdfPCell(new Phrase("Cliente: " + removeWord(billpdfDTO.getcliente(),"null"), fontH2));
            PdfPCell documentoCC = new PdfPCell(new Phrase(billpdfDTO.getcedula(), fontH2));

            documentoCC.setHorizontalAlignment(Element.ALIGN_CENTER);
            documentoCC.setBorder(PdfPCell.NO_BORDER);
            cliente.setHorizontalAlignment(Element.ALIGN_CENTER);
            cliente.setBorder(PdfPCell.NO_BORDER);
            direccion.setBorder(PdfPCell.NO_BORDER);
            telefono.setBorder(PdfPCell.NO_BORDER);
            direccion.setHorizontalAlignment(Element.ALIGN_CENTER);
            telefono.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBorder(PdfPCell.NO_BORDER);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cella.setBorder(PdfPCell.NO_BORDER);
            nit.setHorizontalAlignment(Element.ALIGN_CENTER);
            nit.setBorder(PdfPCell.NO_BORDER);
            regimen.setHorizontalAlignment(Element.ALIGN_CENTER);
            regimen.setBorder(PdfPCell.NO_BORDER);
            nombreCajero.setHorizontalAlignment(Element.ALIGN_CENTER);
            nombreCajero.setBorder(PdfPCell.NO_BORDER);


            try {
                Image img;
                if(PruebasLocales){
                    img = Image.getInstance("./"+billpdfDTO.getnit()+".jpg");
                }else{
                    img = Image.getInstance("/usr/share/nginx/html/logos/" + billpdfDTO.getnit() + ".jpg");
                }
                PdfPCell imagen = new PdfPCell(img, true);
                imagen.setBorder(PdfPCell.NO_BORDER);
                imagen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos.addCell(imagen);
            } catch (Exception e) {
                //System.out.println(e.toString());
            }
            tablaDatos.addCell(cell1);
            tablaDatos.addCell(nit);
            tablaDatos.addCell(direccion);
            tablaDatos.addCell(telefono);
            tablaDatos.addCell(cella);

            PdfPTable tablaData = new PdfPTable(2);

            //tablaData.addCell(direccion);
            //tablaData.addCell(telefono);
            //tablaData.addCell(cell2);
            //tablaData.addCell(nombreCajero);


            tablaData.addCell(cliente);
            tablaData.addCell(documentoCC);
            PdfPCell direCl = new PdfPCell(new Phrase("Dir: " + billpdfDTO.getdireccionC(), fontH2));
            PdfPCell telCl = new PdfPCell(new Phrase("Tel: " + billpdfDTO.gettelefonoC(), fontH2));
            direCl.setHorizontalAlignment(Element.ALIGN_CENTER);
            direCl.setBorder(PdfPCell.NO_BORDER);
            telCl.setHorizontalAlignment(Element.ALIGN_CENTER);
            telCl.setBorder(PdfPCell.NO_BORDER);
            tablaData.addCell(direCl);
            tablaData.addCell(telCl);
            if (billpdfDTO.getcliente().equals("Cliente Ocasional")) {
                tablaData = new PdfPTable(1);
                cliente.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaData.addCell(cliente);
            }

            PdfPTable tablaFecha = new PdfPTable(1);
            PdfPCell cell3 = new PdfPCell(new Phrase(billpdfDTO.getfecha(), fontH1));
            cell3.setBorder(PdfPCell.NO_BORDER);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaFecha.addCell(cella);
            tablaFecha.addCell(cell3);
            tablaFecha.addCell(cell2);
            tablaFecha.addCell(nombreCajero);
            tablaFecha.addCell(cella);

            PdfPTable tablaDatos2 = new PdfPTable(2);
            PdfPCell cell5 = new PdfPCell(new Phrase("Tienda: " + billpdfDTO.gettienda(), fontH2));
            cell5.setBorder(PdfPCell.NO_BORDER);
            cell5.setHorizontalAlignment(Element.ALIGN_CENTER);


            tablaDatos2.addCell(cell5);
            if (billpdfDTO.getcaja().equals("-1")) {
                PdfPCell cell4 = new PdfPCell(new Phrase("REMISIÓN", fontH2));
                cell4.setBorder(PdfPCell.NO_BORDER);
                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos2.addCell(cell4);

            } else {
                PdfPCell cell4 = new PdfPCell(new Phrase("Caja: " + billpdfDTO.getcaja(), fontH2));
                cell4.setBorder(PdfPCell.NO_BORDER);
                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos2.addCell(cell4);
            }
            tablaDatos2.addCell(cella);
            tablaDatos2.addCell(cella);


            PdfPTable tablaDetalles = new PdfPTable(3);
            PdfPTable tablaDetalles2 = new PdfPTable(2);

            PdfPCell cell6 = new PdfPCell(new Phrase("Prod", fontH1));
            PdfPCell cell7 = new PdfPCell(new Phrase("Cant", fontH1));
            PdfPCell cell8 = new PdfPCell(new Phrase("Precio", fontH1));

            cell6.setBorder(PdfPCell.NO_BORDER);
            cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell7.setBorder(PdfPCell.NO_BORDER);
            cell7.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell8.setBorder(PdfPCell.NO_BORDER);
            cell8.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDetalles2.addCell(cell7);
            tablaDetalles2.addCell(cell8);
            PdfPCell table = new PdfPCell(tablaDetalles2);
            table.setBorder(PdfPCell.NO_BORDER);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetalles.addCell(cell6);
            tablaDetalles.addCell(cell7);
            tablaDetalles.addCell(cell8);

            PdfPCell saltoLinea = new PdfPCell(new Phrase("    "));

            List<List<String>> theList = billpdfDTO.getdetalles();

            for (int i = 0; i < billpdfDTO.getdetalles().size(); i++) {
                PdfPCell cell9 = new PdfPCell();
                if (theList.get(i).get(1).length() <= 30) {
                    cell9 = new PdfPCell(new Phrase(theList.get(i).get(3)+" - "+theList.get(i).get(1), fontH2));
                } else {
                    cell9 = new PdfPCell(new Phrase(theList.get(i).get(3)+" - "+(theList.get(i).get(1).substring(0, 30)), fontH2));
                }


                cell9.setBorder(PdfPCell.NO_BORDER);
                cell9.setHorizontalAlignment(Element.ALIGN_CENTER);

                PdfPTable tablaDetalles3 = new PdfPTable(3);
                PdfPCell cell10 = new PdfPCell(new Phrase(theList.get(i).get(0), fontH2));
                PdfPCell cell11 = new PdfPCell(new Phrase("$" + theList.get(i).get(2), fontH2));
                cell10.setBorder(PdfPCell.NO_BORDER);
                cell10.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell11.setBorder(PdfPCell.NO_BORDER);
                cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
                saltoLinea.setBorder(PdfPCell.NO_BORDER);
                tablaDetalles.addCell(cell9);
                tablaDetalles.addCell(cell10);
                tablaDetalles.addCell(cell11);
                PdfPCell tablas = new PdfPCell(tablaDetalles3);
                tablas.setBorder(PdfPCell.NO_BORDER);
                tablas.setHorizontalAlignment(Element.ALIGN_CENTER);
                //tablaDetalles.addCell(tablas);
            }

            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);

            Long cambio = billpdfDTO.getcambio();
            String medioPago = billpdfDTO.getmedioPago();
            Long cantRecibida = billpdfDTO.getcambio() + round(billpdfDTO.gettotalprice());

            if(billpdfDTO.getcambio()<0){
                cambio = new Long(0);
                medioPago = "Múltiples Medios";
                cantRecibida = round(billpdfDTO.gettotalprice());
            }


            PdfPTable tablaTotales = new PdfPTable(2);
            PdfPCell cell12 = new PdfPCell(new Phrase("Subtotal", fontH2));
            PdfPCell cell13 = new PdfPCell(new Phrase("$" + billpdfDTO.getsubtotal() + "", fontH2));
            PdfPCell cellPago = new PdfPCell(new Phrase("Cantidad Recibida", fontH2));
            PdfPCell pago = new PdfPCell(new Phrase("$" + cantRecibida, fontH2));
            PdfPCell cell14 = new PdfPCell(new Phrase("IVA", fontH2));
            PdfPCell cell15 = new PdfPCell(new Phrase("$" + billpdfDTO.gettax(), fontH2));
            PdfPCell cellcambio = new PdfPCell(new Phrase("Cambio", fontH2));
            PdfPCell celcambio2 = new PdfPCell(new Phrase("$" + cambio + ".0", fontH2));
            PdfPCell cell16 = new PdfPCell(new Phrase("Total", fontH2));
            PdfPCell cell17 = new PdfPCell(new Phrase("$" + round(billpdfDTO.gettotalprice()) + "", fontH2));
            PdfPCell cell16medio = new PdfPCell(new Phrase("Medio pago", fontH2));
            PdfPCell cell17medio = new PdfPCell(new Phrase(medioPago + "", fontH2));
            cell12.setBorder(PdfPCell.NO_BORDER);
            cell13.setBorder(PdfPCell.NO_BORDER);
            cell14.setBorder(PdfPCell.NO_BORDER);
            cell15.setBorder(PdfPCell.NO_BORDER);
            cell16.setBorder(PdfPCell.NO_BORDER);
            cell17.setBorder(PdfPCell.NO_BORDER);
            cellcambio.setBorder(PdfPCell.NO_BORDER);
            celcambio2.setBorder(PdfPCell.NO_BORDER);
            cell16medio.setBorder(PdfPCell.NO_BORDER);
            cell17medio.setBorder(PdfPCell.NO_BORDER);
            cellPago.setBorder(PdfPCell.NO_BORDER);
            pago.setBorder(PdfPCell.NO_BORDER);
            cellPago.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pago.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell13.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell14.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell15.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell16.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell17.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cellcambio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celcambio2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell16medio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell17medio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaTotales.addCell(cell12);
            tablaTotales.addCell(cell13);
            tablaTotales.addCell(cell14);
            tablaTotales.addCell(cell15);
            tablaTotales.addCell(cell16);
            tablaTotales.addCell(cell17);
            tablaTotales.addCell(cellPago);
            tablaTotales.addCell(pago);
            tablaTotales.addCell(cellcambio);
            tablaTotales.addCell(celcambio2);
            tablaTotales.addCell(cell16medio);
            tablaTotales.addCell(cell17medio);


            PdfPTable tablaLegal = new PdfPTable(1);
            tablaLegal.addCell(cella);
            if (billpdfDTO.getregimenT() != null && !billpdfDTO.getregimenT().isEmpty()) {
                String regimenTemp;
                if (billpdfDTO.getregimenT().equals("C")) {
                    regimenTemp = "Común";
                } else {
                    regimenTemp = "Simplificado";
                }
                PdfPCell cellRegimen = new PdfPCell(new Phrase("Régimen " + regimenTemp, fontH2));
                cellRegimen.setBorder(PdfPCell.NO_BORDER);
                cellRegimen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellRegimen);
            }

            if (billpdfDTO.getresolucion_DIAN() != null && !billpdfDTO.getresolucion_DIAN().isEmpty()) {
                PdfPCell cellAuth = new PdfPCell(new Phrase("Resolución Dian # " + billpdfDTO.getresolucion_DIAN(), fontH2));
                cellAuth.setBorder(PdfPCell.NO_BORDER);
                cellAuth.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellAuth);
            }

           /* if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                PdfPCell cellDatos = new PdfPCell(new Phrase("Fecha Formalización: 13 de junio de 2019",fontH2));
                cellDatos.setBorder(PdfPCell.NO_BORDER);
                cellDatos.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellDatos);
            }    */

            PdfPCell cellPos = new PdfPCell(new Phrase("Modalidad: POS", fontH2));
            cellPos.setBorder(PdfPCell.NO_BORDER);
            cellPos.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaLegal.addCell(cellPos);

            //if (billpdfDTO.getinitial_RANGE() != null && !billpdfDTO.getinitial_RANGE().isEmpty() &&
            //        billpdfDTO.getfinal_RANGE() != null && !billpdfDTO.getfinal_RANGE().isEmpty() &&
            //        billpdfDTO.getprefix() != null && !billpdfDTO.getprefix().isEmpty()) {
            //    PdfPCell cellAutor = new PdfPCell(new Phrase("Autorización: Desde " + billpdfDTO.getprefix() + billpdfDTO.getinitial_RANGE() + " a " + billpdfDTO.getprefix() + billpdfDTO.getfinal_RANGE(), fontH2));
            //    cellAutor.setBorder(PdfPCell.NO_BORDER);
            //    cellAutor.setHorizontalAlignment(Element.ALIGN_CENTER);
            //    tablaLegal.addCell(cellAutor);
            //}


            tablaLegal.addCell(cella);


            tablaDatos.setKeepTogether(false);
            tablaData.setKeepTogether(false);
            tablaFecha.setKeepTogether(false);
            tablaDatos2.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaTotales.setKeepTogether(false);
            tablaLegal.setKeepTogether(false);


            tablaDatos.setSplitLate(false);
            tablaData.setSplitLate(false);
            tablaFecha.setSplitLate(false);
            tablaDatos2.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaTotales.setSplitLate(false);
            tablaLegal.setSplitLate(false);

            PdfPTable tabla = new PdfPTable(1);
            PdfPCell tablaDatosC = new PdfPCell(tablaDatos);
            PdfPCell tablaDataC = new PdfPCell(tablaData);
            PdfPCell tablaFechaC = new PdfPCell(tablaFecha);
            PdfPCell tablaDatos2C = new PdfPCell(tablaDatos2);
            PdfPCell tablaDetallesC = new PdfPCell(tablaDetalles);
            PdfPCell tablaDetalles2C = new PdfPCell(tablaDetalles2);
            PdfPCell tablaTotalesC = new PdfPCell(tablaTotales);
            PdfPCell tablaLegalC = new PdfPCell(tablaLegal);
            tablaDatosC.setBorder(PdfPCell.NO_BORDER);
            tablaDataC.setBorder(PdfPCell.NO_BORDER);
            tablaFechaC.setBorder(PdfPCell.NO_BORDER);
            tablaDatos2C.setBorder(PdfPCell.NO_BORDER);
            tablaDetallesC.setBorder(PdfPCell.NO_BORDER);
            tablaDetalles2C.setBorder(PdfPCell.NO_BORDER);
            tablaTotalesC.setBorder(PdfPCell.NO_BORDER);
            tablaLegalC.setBorder(PdfPCell.NO_BORDER);

            tabla.setKeepTogether(false);
            tabla.setSplitLate(false);
            tabla.addCell(tablaDatosC);
            tabla.addCell(tablaDataC);
            tabla.addCell(tablaFechaC);
            tabla.addCell(tablaDatos2C);
            tabla.addCell(tablaDetallesC);
            //tabla.addCell(tablaDetalles2C);
            tabla.addCell(tablaTotalesC);
            tabla.addCell(tablaLegalC);
            documento.add(tabla);
            response = nombrePdf;

        }catch(Exception e){
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Domicilio>> getDomi(Long id_store) {
        try {
            //System.out.println("|||||||||||| Starting consults Bill ||||||||||||||||| ");
            return Either.right(billDAO.getDomi(id_store));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, List<PedidosSelect>> getPedidosSelect(Long id_store,Long id_third) {
        try {
            //System.out.println("|||||||||||| Starting consults Bill ||||||||||||||||| ");
            return Either.right(billDAO.getPedidosSelect(id_store,id_third));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * Get Billing with a filter of bill, so The response will be with  or without this filter.
     * @param bill
     * @return
     */


    public Either<IException, List<Bill>> getBill(Bill bill) {
        try {
            //System.out.println("|||||||||||| Starting consults Bill ||||||||||||||||| ");
            return Either.right(billDAO.read(billSanitation(bill)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * Get raw bill.
     * @param id_bill
     * @return
     */


    public Either<IException, BillRaw> getRawBill(long id_bill) {
        try {
            //System.out.println("|||||||||||| Starting consults raw Bill ||||||||||||||||| ");
            return Either.right(billDAO.readRawBill(id_bill));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     *
     * Get Billing with details as detail bill and detail payment, using a filter of bill, of detail bill, of detail payment. So The response will be with  or without this filter.
     * @param bill
     * @param detailBill
     * @param detailPaymentBill
     * @return
     */
    public Either<IException, List<HashMap<String,Object>>> getBillDetail(Bill bill,DetailBill detailBill,DetailPaymentBill detailPaymentBill) {
        List<HashMap<String,Object>> response= new ArrayList<>();
        List<Bill> billList= new ArrayList<>();
        List<DetailBill> detailBillList= new ArrayList<>();
        List<DetailPaymentBill> detailPaymentBillList= new ArrayList<>();
        try {
            //System.out.println("|||||||||||| Starting consults Bill Detail ||||||||||||||||| ");
            billList=billDAO.read(billSanitation(bill));
            for (Bill billT: billList){
                HashMap<String,Object> response_aux= new HashMap<>();


                Either<IException, List<DetailPaymentBill>> detailPaymentEither = detailPaymentBillBusiness.getDetailPaymentBill(detailPaymentBill);
                if (detailPaymentEither.isRight()){
                    detailPaymentBillList = detailPaymentEither.right().value();
                    if (detailPaymentBillList.size()>0){

                        response_aux.put("detail_payments",detailPaymentBillList);
                    }
                }

                Either<IException, List<DetailBill>> detailBillEither = detailBillBusiness.getDetailBill(detailBill);
                if (detailBillEither.isRight()){
                    detailBillList = detailBillEither.right().value();
                    if (detailBillList.size()>0){

                        response_aux.put("details",detailBillList);
                    }
                }
                response_aux.put("billing",billT);
                response.add(response_aux);
            }

            return Either.right(response);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     *
     * Get Billing with a description more depth using bill complete. So The response will be with  or without this filter.
     * @param billComplete
     * @return
     */
    public Either<IException, List<BillComplete>> getBillComplete(BillComplete billComplete) {
        try {

            //System.out.println("|||||||||||| Starting consults Bill ||||||||||||||||| ");

            BillComplete billCompleteSanitation = billCompleteSanitation(billComplete);
            return Either.right(billDAO.readComplete(billCompleteSanitation,billCompleteSanitation.getPayment_state(),billCompleteSanitation.getBill_state(),
                    billCompleteSanitation.getBill_type()));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<DetailPaymentBillComplete>> getDetailPaymentBillComplete(DetailPaymentBillComplete detailPaymentBillComplete){
        return detailPaymentBillBusiness.getDetailPaymentBillComplete(detailPaymentBillComplete);
    }

    public Either<IException, RawDetailPaymentBill> getRawDetailPaymentFromIdBill(long id_bill){
        return detailPaymentBillBusiness.getRawDetailPaymentFromIdBill(id_bill);
    }
    /**
     *
     * Get Billing with a description more depth. With details as detail bill and detail payment complete, using a filter of bill complete, of detail bill, of detail payment  complete. So The response will be with  or without this filter.
     * @param billComplete
     * @param detailBill
     * @param detailPaymentBillComplete
     * @return
     */
    public Either<IException, List<HashMap<String,Object>>> getBillCompleteDetail(BillComplete billComplete,DetailBill detailBill,DetailPaymentBillComplete detailPaymentBillComplete) {
        List<HashMap<String,Object>> response= new ArrayList<>();
        List<BillComplete> billCompleteList= new ArrayList<>();
        List<DetailBill> detailBillList= new ArrayList<>();
        List<DetailPaymentBillComplete> detailPaymentBillList= new ArrayList<>();
        try {
            //System.out.println("|||||||||||| Starting consults Bill Detail ||||||||||||||||| ");
            BillComplete billCompleteSanitation = billCompleteSanitation(billComplete);
            billCompleteList=billDAO.readComplete(billCompleteSanitation,billCompleteSanitation.getPayment_state(),billCompleteSanitation.getBill_state(),
                    billCompleteSanitation.getBill_type());


            for (BillComplete billT: billCompleteList){
                HashMap<String,Object> response_aux= new HashMap<>();


                Either<IException, List<DetailPaymentBillComplete>> detailPaymentEither = getDetailPaymentBillComplete(detailPaymentBillComplete);
                if (detailPaymentEither.isRight()){
                    detailPaymentBillList = detailPaymentEither.right().value();
                    if (detailPaymentBillList.size()>0){

                        response_aux.put("detail_payments",detailPaymentBillList);
                    }
                }

                Either<IException, List<DetailBill>> detailBillEither = detailBillBusiness.getDetailBill(detailBill);
                if (detailBillEither.isRight()){
                    detailBillList = detailBillEither.right().value();
                    if (detailBillList.size()>0){

                        response_aux.put("details",detailBillList);
                    }
                }

                response_aux.put("billing",billT);
                response.add(response_aux);

            }

            return Either.right(response);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param billDTO
     * @return
     */
    public Either<IException,Long> creatBill(BillDTO billDTO, Long id_caja){
        List<String> msn = new ArrayList<>();
        Long id_bill = null;
        Long id_common_state = null;

        try {
            //System.out.println("|||||||||||| Starting creation Bill   ||||||||||||||||| ");
            if (billDTO!=null){

                if (billDTO.getId_bill_father()!=null && formatoLongSql(billDTO.getId_bill_father())>0 && !validatorID(billDTO.getId_bill_father())){

                    //msn.add("It ID Bill does not exist register on  Bill, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));

                }


                if (formatoLongSql(billDTO.getId_bill_state())>0 && !billStateBusiness.validatorID(billDTO.getId_bill_state()) ){

                    //msn.add("It ID Bill State does not exist register on  Bill State, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                if (formatoLongSql(billDTO.getId_bill_type())>0 && !billTypeBusiness.validatorID(billDTO.getId_bill_type())){

                    //msn.add("It ID Bill Type does not exist register on  Bill Type, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));

                }


                if (billDTO.getStateDTO() == null) {
                    billDTO.setStateDTO(new CommonStateDTO(1,null,null));
                }

                Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(billDTO.getStateDTO());
                if (commonThirdEither.isRight()) {
                    id_common_state = commonThirdEither.right().value();
                }
                Integer consecutive = generatorConsecutive();
                String consecutive_final=consecutive+"";
                if (billDTO.getId_third_employee()!=null){
                    consecutive_final=billDTO.getId_third_employee()+":"+consecutive;
                }
                billDAO.create(consecutive_final,id_common_state,billDTO, id_caja);
                id_bill = billDAO.getPkLast();

                billDAO.insertDocument(id_bill,billDTO.getdocumentDTO().gettitle(),billDTO.getdocumentDTO().getbody());

                billDAO.updateIdDocument(id_bill);



                // @TODO begin creation of detail payment bill
                if (billDTO.getPayments()!=null && billDTO.getPayments().size()>0){
                    detailPaymentBillBusiness.createDetailPaymentBill(id_bill,billDTO.getPayments());

                }
                // @TODO begin creation of detail bill

                if (billDTO.getDetails()!=null && billDTO.getDetails().size()>0){
                    detailBillBusiness.creatDetailBill(billDTO.getId_bill_type(),id_bill,billDTO.getDetails());
                }

                return Either.right(id_bill);
            }else{
                //msn.add("It does not recognize Bill, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param billDetailIDDTO
     * @param id_bill
     * @return
     */
    public Either<IException, Long> updateBill(Long id_bill, BillDetailIDDTO billDetailIDDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        Bill actualRegister=null;
        List<Bill> read= new ArrayList<>();

        try {
            //System.out.println("|||||||||||| Starting update Bill ||||||||||||||||| ");
            if ((id_bill != null && id_bill > 0)) {



                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorID(id_bill).equals(false)){
                    //msn.add("It ID Bill  does not exist register on  Bill, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (billDetailIDDTO.getId_bill_father()!=null && formatoLongSql(billDetailIDDTO.getId_bill_father())>0 && !validatorID(billDetailIDDTO.getId_bill_father())){

                    //msn.add("It ID Bill does not exist register on  Bill, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));

                }

                if (formatoLongSql(billDetailIDDTO.getId_bill_state())>0 && !billStateBusiness.validatorID(billDetailIDDTO.getId_bill_state()) ){

                    //msn.add("It ID Bill State does not exist register on  Bill State, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                if (formatoLongSql(billDetailIDDTO.getId_bill_type())>0 && !billTypeBusiness.validatorID(billDetailIDDTO.getId_bill_type())){

                    //msn.add("It ID Bill Type does not exist register on  Bill Type, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));

                }
                if (formatoLongSql(billDetailIDDTO.getId_payment_state())>0 && !paymentStateBusiness.validatorID(billDetailIDDTO.getId_payment_state())){

                    //msn.add("It ID Payment State does not exist register on  Payment State, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                read= billDAO.read(
                        new Bill(id_bill,null,null,null,null,null,null,null,null,null,null,null,null,
                                new CommonState(null,null,null,null)));




                if (read.size()<=0){
                    //msn.add("It does not recognize ID Bill  , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }else {
                    actualRegister=read.get(0);
                }


                List<CommonSimple> readCommons = billDAO.readCommons(formatoLongSql(id_bill));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                    // Common  State business update
                    commonStateBusiness.updateCommonState(idCommons.getId_common_state(), billDetailIDDTO.getStateDTO());
                }



                if (billDetailIDDTO.getId_third_employee() == null || billDetailIDDTO.getId_third_employee()<0) {
                    billDetailIDDTO.setId_third_employee(actualRegister.getId_third_employee());
                } else {
                    billDetailIDDTO.setId_third_employee(formatoLongSql(billDetailIDDTO.getId_third_employee()));
                }

                if (billDetailIDDTO.getId_third() == null || billDetailIDDTO.getId_third()<0) {
                    billDetailIDDTO.setId_third(actualRegister.getId_third());
                } else {
                    billDetailIDDTO.setId_third_employee(formatoLongSql(billDetailIDDTO.getId_third_employee()));
                }


                if (billDetailIDDTO.getPurchase_date()== null ) {
                    billDetailIDDTO.setPurchase_date(actualRegister.getPurchase_date());
                } else {
                    billDetailIDDTO.setPurchase_date(formatoDateSql(billDetailIDDTO.getPurchase_date()));
                }

                if (billDetailIDDTO.getSubtotal() == null) {
                    billDetailIDDTO.setSubtotal(actualRegister.getSubtotal());
                } else {
                    billDetailIDDTO.setSubtotal(formatoDoubleSql(billDetailIDDTO.getSubtotal()));
                }

                if (billDetailIDDTO.getTax() == null) {
                    billDetailIDDTO.setTax(actualRegister.getTax());
                } else {
                    billDetailIDDTO.setTax(formatoDoubleSql(billDetailIDDTO.getTax()));
                }

                if (billDetailIDDTO.getTotalprice() == null) {
                    billDetailIDDTO.setTotalprice(actualRegister.getTotalprice());
                } else {
                    billDetailIDDTO.setTotalprice(formatoDoubleSql(billDetailIDDTO.getTotalprice()));
                }

                // Attribute update
                billDAO.update(id_bill,billDetailIDDTO);

                if (billDetailIDDTO.getPayments()!=null && billDetailIDDTO.getPayments().size()>0){
                    detailPaymentBillBusiness.updateDetailPaymentBill(id_bill,billDetailIDDTO.getPayments());
                }
                if (billDetailIDDTO.getDetails()!=null && billDetailIDDTO.getDetails().size()>0){
                    detailBillBusiness.updateDetailBill(id_bill,billDetailIDDTO.getDetails());
                }
                return Either.right(id_bill);
            } else {
                //msn.add("It does not recognize ID Bill, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> deleteBill(Long id_bill) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            if ( (id_bill != null && id_bill > 0)) {

                detailBillBusiness.deleteDetailBill(null,id_bill);
                detailPaymentBillBusiness.deleteDetailPaymentBill(null,id_bill);

                List<CommonSimple> readCommons = billDAO.readCommons(formatoLongSql(id_bill));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                assert idCommons != null;
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());

                return Either.right(id_bill);





            }else{
                //msn.add("It does not recognize ID  Bill, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = billDAO.getValidatorID(id);
            return validator != 0;
        }catch (Exception e){
            return false;
        }
    }

    public Long getIdStoreFromIdBill(long id_bill) {
        long validator = 0;
        try {
            validator = billDAO.getIdStoreFromIdBill(id_bill);
            if (validator != 0)
                return validator;
            else
                return new Long(0);
        }catch (Exception e){
            return new Long(0);
        }
    }

    public String getBodyDocumentBill(long id_bill) {
        String validator = "-1";
        try {
            validator = billDAO.getBodyDocumentBill(id_bill);
            if (!validator.equals("-1"))
                return validator;
            else
                return "";
        }catch (Exception e){
            return "";
        }
    }

    public Either<IException, BillMaster> getMaestroFactura(Long id_bill,Long id_store) {
        try {
            return Either.right(billDAO.getMaestroFactura(id_bill,id_store));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, BillMaster2> getMaestroFactura2(Long id_bill,Long id_store) {
        try {
            return Either.right(billDAO.getMaestroFactura2(id_bill,id_store));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<TopProds>> getTopProducts(Long idstore) {
        try {
            return Either.right(billDAO.getTopProducts(idstore));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, List<Vendor>> getVendorList(Long idstore) {
        try {
            return Either.right(billDAO.getVendorList(idstore));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, List<ProductosPedidosMesa>> getProductosPedidoMesa(Long id_bill) {
        try {
            return Either.right(billDAO.getProductosPedidoMesa(id_bill));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, List<PedidosMesa>> getPedidosMesa(List<String> id_store,
                                                                Long id_bill_type,
                                                                Long id_bill_state) {
        try {
            return Either.right(billDAO.getPedidosMesa(id_store,id_bill_type,id_bill_state));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, List<ProductosPedidosMesa>> getProductosPedidoMesaADR(Long id_bill,Long idthird) {
        try {
            return Either.right(billDAO.getProductosPedidoMesaADR(id_bill,idthird));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, List<PedidosMesa>> getPedidosMesaADR(List<String> id_store,
                                                                Long id_bill_type,
                                                                Long id_bill_state,Long idthird) {
        try {
            return Either.right(billDAO.getPedidosMesaADR(id_store,id_bill_type,id_bill_state,idthird));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> getCantFacturas(Long id_caja, Long id_cierre_caja) {
        try {
            return Either.right(billDAO.getCantFacturas(id_caja,id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Double round2(Double a){
        return Math.round(a * 100.0) / 100.0;
    }

    public Either<IException,List<List<String>>> getDetalleFactura(Long id_bill) {
        try {
            List<BillDetail> list = billDAO.getDetalleFactura(id_bill);
            List<List<String>> response = new ArrayList<>();
            for (BillDetail billDetail : list) {
                List<String> tmp = new ArrayList<>();
                tmp.add(billDetail.getQUANTITY() + "");
                tmp.add(billDetail.getPRODUCT_STORE_NAME());
                tmp.add(round2(billDetail.getVALOR()) + "");
                tmp.add(billDetail.getCODE() + "");
                tmp.add(billDetail.getPERCENT() + "");
                tmp.add(billDetail.getPRICE_NO_IVA() + "");
                tmp.add(round2(billDetail.getPRICE()) + "");
                tmp.add(billDetail.getIDTAX() + "");
                response.add(tmp);
            }
            return Either.right(response);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> putDomiciliario(Long id_bill, Long id_third) {

        try {
            //System.out.println(id_bill);
            //System.out.println(id_third);
            billDAO.putDomiciliario(id_bill,id_third);
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }



    public Either<IException, String> putCompraBill(Long id_bill, String dato, Date fecha) {

        try {
            billDAO.putCompraBill(id_bill,dato,fecha);
            return Either.right("OK!!!");
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    public Either<IException, List<LegalData>> getLegalData(Long id_third) {

        try {
            //System.out.println("///////START GET LEGAL DATA////////");
            List<LegalData> a = billDAO.getLegalData(id_third);
            return Either.right(a);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }


    public Either<IException, List<storesByThird>> getTiendasByThird(Long id_third) {

        try {
            //System.out.println("///////START GET LEGAL DATA////////");
            List<storesByThird> a = billDAO.getTiendasByThird(id_third);
            return Either.right(a);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }


    public Either<IException, Long> validateProductQuantity(Long idps) {
        try {
            //System.out.println("|||||||||||| Starting consults Bill ||||||||||||||||| ");
            return Either.right(billDAO.validateProductQuantity(idps));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> validatePdfDrawing(Long idbill) {
        try {
            billDAO.validatePdfDrawing(idbill);
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.right(new Long(0));
        }
    }


    public Either<IException, Long> getMaxBillPedidos(Long idstore,Long idemp) {
        try {
            //System.out.println("|||||||||||| Starting consults Bill ||||||||||||||||| ");
            return Either.right(billDAO.getMaxBillPedidos(idstore,idemp));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> insertPaymentDetail(PaymentDetailListDTO paymentDTOList) {
        try {
            List<PaymentDetailDTO> list = paymentDTOList.getdetalles();
            for (PaymentDetailDTO p: list) {
                billDAO.insertPaymentDetail(p.getPAYMENTVALUE(),p.getIDBILL(),p.getIDPAYMENTMETHOD(),p.getAPPROBATIONCODE(),p.getIDWAYTOPAY(),p.getIDBANKENTITY());
            }

            return Either.right(new Long(1));

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> registrar_primer_pago(Long idBill,String valor ) {
        try {
            billDAO.registrar_primer_pago(idBill,valor);
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> getCanceledTable(Long idBill) {
        try {
            //System.out.println("|||||||||||| Starting consults Bill ||||||||||||||||| ");
            return Either.right(billDAO.getCanceledTable(idBill));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, Long> agregar_servicio_mesa(Long idbill,
                                                          Long barcode,
                                                          Long valor,
                                                          Long idstore) {
        try {
            billDAO.agregar_servicio_mesa( idbill,
                     barcode,
                     valor,
                     idstore);
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> eliminar_servicio_detailbill(Long idbill) {
        try {
            billDAO.eliminar_servicio_detailbill( idbill);
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> cerrar_mesa(Long idbill ) {
        try {
            billDAO.cerrar_mesa( idbill );
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> asociar_tercero_bill(Long idthird, Long idbill ) {
        try {
            billDAO.asociar_tercero_bill( idthird, idbill );
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, String> getIdCityByStore(Long idstore ) {
        try {

            return Either.right(billDAO.getIdCityByStore( idstore ));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, PedidosMesa2> getPedidosMesaByIdMesa(Long id_store,
                                                                Long id_bill_type,
                                                                Long id_bill_state) {
        try {
            return Either.right(billDAO.getPedidosMesaByIdMesa(id_store,id_bill_type,id_bill_state));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, mesaState> getValidacionMesa(Long id_store) {
        try {
            return Either.right(billDAO.getValidacionMesa(id_store));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> registrar_mesa_token(Long idmesa, String tok ) {
        try {
            billDAO.registrar_mesa_token( idmesa, tok );
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, Long> getMesaAbierta(Long idmesa) {
        try {
            return Either.right(billDAO.getMesaAbierta(idmesa));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, String> get_valor_servicio(String idbill,String pct) {
        try {
            return Either.right(billDAO.get_valor_servicio(idbill,pct));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> getIsServicioInBill(Long idbill) {
        try {
            return Either.right(billDAO.getIsServicioInBill(idbill));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, String> get_valores_bill(String idbill) {
        try {
            return Either.right(billDAO.get_valores_bill(idbill));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> crear_credito(String fechacreacion,
                                                  Long valor,
                                                  Long numcuotas,
                                                  Long interes,
                                                  Long interesmora,
                                                  Long periodicidad,
                                                  Long cuotainicial,
                                                  Long idthirdcliente,
                                                  Long idthirdprov,
                                                  Long idthirdcodeudor ) {
        try {
            billDAO.crear_credito( fechacreacion,
                    valor,
                    numcuotas,
                    interes,
                    interesmora,
                    periodicidad,
                    cuotainicial,
                    idthirdcliente,
                    idthirdprov,
                    idthirdcodeudor );
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, String> get_max_credito(String id_credito) {
        try {
            return Either.right(billDAO.get_max_credito(id_credito));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<credito>> get_credito(String cedula) {
        try {
            return Either.right(billDAO.get_credito("%"+cedula+"%"));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<DocumentoCredito>> getDocumentoCredito(Long id_credito) {
        try {
            return Either.right(billDAO.getDocumentoCredito(id_credito));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> CREAR_CREDITO_DOCUMENTO(String NOMBREDOC,
                                                            String RUTADOC,
                                                            String TIPODOC,
                                                            Long IDCREDITO ) {
        try {
            billDAO.CREAR_CREDITO_DOCUMENTO( NOMBREDOC,
                     RUTADOC,
                     TIPODOC,
                     IDCREDITO );
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, List<LogCredito>> getLogCredito(Long id_credito) {
        try {
            return Either.right(billDAO.getLogCredito(id_credito));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException,Long> postFile(base64FileDTO dto) {
        try {

            String Base64File = dto.getFile();
            String extension = dto.getformat();
            String fileName = dto.getFileName();


            File file = new File("/usr/share/nginx/html/docscredito/"+fileName+"."+extension+"");


            try ( FileOutputStream fos = new FileOutputStream(file); ) {
                // To be short I use a corrupted PDF string, so make sure to use a valid one if you want to preview the PDF file
                String b64 = Base64File.split(",")[1];
                byte[] decoder = Base64.getDecoder().decode(b64);

                fos.write(decoder);
                System.out.println("File File Saved");
            } catch (Exception e) {
                e.printStackTrace();
            }



            // create a buffered image
            /*
            BufferedImage image = null;
            byte[] imageByte;

            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(Base64File.split(",")[1]);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();

            // write the image to a file
            File outputfile = new File("/usr/share/nginx/html/docscredito/"+fileName+"."+extension);
            ImageIO.write(image, extension, outputfile);

            */
            //System.out.println("/imagenes/productos/"+fileName+"."+extension+" , "+fileName);

            return Either.right(new Long(1));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage())));
        }
    }



    public Either<IException, Long> aprobar_credito(Long IDCREDITO,
                                                            String NOTE ) {
        try {
            billDAO.aprobar_credito(
                    NOTE,
                    IDCREDITO );
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }




    public Either<IException, Long> desembolsar_credito(Long IDCREDITO,
                                                        String NOTE ) {
        try {
            billDAO.desembolsar_credito(NOTE,
                    IDCREDITO);
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, Long> generar_cuotas(
                                                            Long IDCREDITO ) {
        try {
            billDAO.generar_cuotas(
                    IDCREDITO );
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, List<credito>> getCreditoById(Long ID_CREDITO) {
        try {
            return Either.right(billDAO.getCreditoById(ID_CREDITO));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, Long> crear_cuota_credito(Long idcredito,
                                                        String fechavencimiento,
                                                        Long valorcuota ) {
        try {
            billDAO.crear_cuota_credito(idcredito, fechavencimiento, valorcuota);
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }




    public Either<IException, Long> crear_abono_cuota(Long idcuota,
                                                        Long idcredito,
                                                        String fechaabono,
                                                        Long valorabono,
                                                        Long idpaymentmethod,
                                                        Long IDCREDITODOCUMENTO) {
        try {
            billDAO.crear_abono_cuota(idcuota,idcredito,fechaabono,valorabono,idpaymentmethod,IDCREDITODOCUMENTO);
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }




    public Either<IException, List<Cuota>> getCuotas(String ID_CREDITO) {
        try {
            return Either.right(billDAO.getCuotas(ID_CREDITO));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }




    public Either<IException, List<Abono>> getAbono( Long idcredito,Long idcuota) {
        try {
            return Either.right(billDAO.getAbono(idcredito, idcuota));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> asociar_factura_credito(Long idcredito,
                                                         Long idbill) {
        try {
            billDAO.asociar_factura_credito(idcredito,
                     idbill );
            return Either.right(new Long(1));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, List<Bills4Credit>> Bills4Credit( Long idthird,String numdoc) {
        try {

            List<Bills4Credit> facturasFinal = new ArrayList<>();
            List<Bills4Credit> facturas = billDAO.Bills4Credit(idthird, "%"+numdoc+"%");
            List<Long> billsNotUsable = billDAO.getBillsNotToUse(idthird);


            for(Bills4Credit bill :facturas) {
                if(!billsNotUsable.contains(bill.getId_bill())){
                    facturasFinal.add(bill);
                }
            }


            return Either.right(facturasFinal);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



}
