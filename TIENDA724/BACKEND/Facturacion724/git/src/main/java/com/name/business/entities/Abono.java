package com.name.business.entities;

import java.util.Date;

public class Abono {

    private Long ID_ABONO_CUOTA_CREDITO;
    private Long ID_CUOTA;
    private Date FECHA_ABONO;
    private Long VALOR_ABONO;
    private Long ID_CREDITO;
    private String paymentmethod;

    public Abono(Long ID_ABONO_CUOTA_CREDITO, Long ID_CUOTA, Date FECHA_ABONO, Long VALOR_ABONO, Long ID_CREDITO, String paymentmethod) {
        this.ID_ABONO_CUOTA_CREDITO = ID_ABONO_CUOTA_CREDITO;
        this.ID_CUOTA = ID_CUOTA;
        this.FECHA_ABONO = FECHA_ABONO;
        this.VALOR_ABONO = VALOR_ABONO;
        this.ID_CREDITO = ID_CREDITO;
        this.paymentmethod = paymentmethod;
    }

    public Long getID_ABONO_CUOTA_CREDITO() {
        return ID_ABONO_CUOTA_CREDITO;
    }

    public void setID_ABONO_CUOTA_CREDITO(Long ID_ABONO_CUOTA_CREDITO) {
        this.ID_ABONO_CUOTA_CREDITO = ID_ABONO_CUOTA_CREDITO;
    }

    public Long getID_CUOTA() {
        return ID_CUOTA;
    }

    public void setID_CUOTA(Long ID_CUOTA) {
        this.ID_CUOTA = ID_CUOTA;
    }

    public Date getFECHA_ABONO() {
        return FECHA_ABONO;
    }

    public void setFECHA_ABONO(Date FECHA_ABONO) {
        this.FECHA_ABONO = FECHA_ABONO;
    }

    public Long getVALOR_ABONO() {
        return VALOR_ABONO;
    }

    public void setVALOR_ABONO(Long VALOR_ABONO) {
        this.VALOR_ABONO = VALOR_ABONO;
    }

    public Long getID_CREDITO() {
        return ID_CREDITO;
    }

    public void setID_CREDITO(Long ID_CREDITO) {
        this.ID_CREDITO = ID_CREDITO;
    }

    public String getPaymentmethod() {
        return paymentmethod;
    }

    public void setPaymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }

}
