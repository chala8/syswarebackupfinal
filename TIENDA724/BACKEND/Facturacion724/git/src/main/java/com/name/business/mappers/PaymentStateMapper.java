package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.PaymentState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PaymentStateMapper implements ResultSetMapper<PaymentState> {

    @Override
    public PaymentState map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PaymentState(
                resultSet.getLong("ID_PAYMENT_STATE"),
                resultSet.getString("NAME_PAY_ST"),
                new CommonState(
                        resultSet.getLong("ID_CM_PAY_ST"),
                        resultSet.getInt("STATE_CM_PAY_ST"),
                        resultSet.getDate("CREATION_PAY_ST"),
                        resultSet.getDate("UPDATE_PAY_ST")
                )
        );
    }
}
