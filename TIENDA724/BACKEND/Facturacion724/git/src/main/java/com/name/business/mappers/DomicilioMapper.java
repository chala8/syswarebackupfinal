package com.name.business.mappers;


import com.name.business.entities.Domicilio;
import com.name.business.entities.Pedido;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DomicilioMapper implements ResultSetMapper<Domicilio> {

    @Override
    public Domicilio map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Domicilio(
                resultSet.getLong("ID_PERSON"),
                resultSet.getLong("ID_THIRD_DOMICILIARIO"),
                resultSet.getString("DOMICILIARIO")
        );
    }
}
