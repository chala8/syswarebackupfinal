package com.name.business;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
/**
 * La clase BillAppConfiguration Se establece los parámetros de  configuración que se requieren
 * con la base de datos(MySql)
 * */
public class BillAppConfiguration extends Configuration {


    @Valid
    @NotNull
    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();

    @Valid
    @NotNull
    private BillDatabaseConfiguration databaseConfiguration;


    public DataSourceFactory getDataSourceFactory(){
        return database;
    }
    public BillDatabaseConfiguration getDatabaseConfiguration(){
        return databaseConfiguration;
    }

}
