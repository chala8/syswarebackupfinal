package com.name.business.entities;

public class mesaState {
    private String estado_mesa;
    private String token;

    public mesaState(String estado_mesa, String token) {
        this.estado_mesa = estado_mesa;
        this.token = token;
    }

    public String getEstado_mesa() {
        return estado_mesa;
    }

    public void setEstado_mesa(String estado_mesa) {
        this.estado_mesa = estado_mesa;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
