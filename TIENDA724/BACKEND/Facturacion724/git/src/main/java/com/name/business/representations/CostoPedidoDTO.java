package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CostoPedidoDTO {
    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    private String detalles;

    @JsonCreator
    public CostoPedidoDTO(@JsonProperty("detalles") String detalles){
        this.detalles = detalles;
    }

}
