package com.name.business.mappers;

import com.name.business.entities.Bill;
import com.name.business.entities.CommonState;
import com.name.business.entities.PlanillaB;
import com.name.business.entities.Refund;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PlanillaBMapper  implements ResultSetMapper<PlanillaB> {

    @Override
    public PlanillaB map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PlanillaB(
                resultSet.getLong("ID_PLANILLA"),
                resultSet.getString("NUM_PLANILLA"),
                resultSet.getString("FULLNAME")
        );
    }
}
