package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SendEmailBody {
    String[] Destinatario;
    String Asunto;
    String CuerpoCorreo;

    @JsonCreator
    public SendEmailBody(@JsonProperty("Destinatario")  String[] Destinatario,
                         @JsonProperty("Asunto")  String Asunto,
                         @JsonProperty("CuerpoCorreo")  String CuerpoCorreo) {
        this.Destinatario = Destinatario;
        this.Asunto = Asunto;
        this.CuerpoCorreo = CuerpoCorreo;
    }

    public String[] getDestinatario() {
        return Destinatario;
    }

    public void setDestinatario(String[] destinatario) {
        Destinatario = destinatario;
    }

    public String getAsunto() {
        return Asunto;
    }

    public void setAsunto(String asunto) {
        Asunto = asunto;
    }

    public String getCuerpoCorreo() {
        return CuerpoCorreo;
    }

    public void setCuerpoCorreo(String cuerpoCorreo) {
        CuerpoCorreo = cuerpoCorreo;
    }
}
