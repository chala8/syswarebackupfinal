package com.name.business.DAOs;
import com.name.business.entities.*;
import com.name.business.mappers.*;
import com.name.business.representations.BillDTO;
import com.name.business.representations.DetailBillDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.unstable.BindIn;

import java.sql.CallableStatement;
import java.util.Date;
import java.util.List;

@RegisterMapper(PedidoMapper.class)
@UseStringTemplate3StatementLocator
public interface PedidosDAO {


    @SqlQuery(" select ownbarcode from tienda724.product_store where id_product_store=:id_product_store ")
    String getOwnBarCode(@Bind("id_product_store") Long id_product_store);


    @SqlQuery(" select id_product_store from tienda724.product_store where ownbarcode=:code and id_store = :id_store ")
    Long getPsId(@Bind("code") String code, @Bind("id_store") Long id_store);


    @RegisterMapper(PedidoMapper.class)
    @SqlQuery(" select b.id_store, b.id_store_client, b.id_bill,c.fullname cliente,s.description tienda,num_documento,num_documento_cliente numpedido\n" +
            "                                        ,to_char(purchase_date,'yyyy-mon-dd hh24:mi:ss') fecha\n" +
            "                                         ,dircliente.address,pcliente.phone,ecliente.mail\n" +
            "                                         ,dircliente.latitud,dircliente.longitud, doc.body,b.id_third_destinity idthirdcliente,nvl(dp.id_payment_method,1) id_payment\n" +
            "                                         ,d.address addressp,pprov.phone phonep,eprov.mail mailp,d.latitud latitudp,d.longitud longitudp\n" +
            "                                         ,sc.description tiendacliente,dirtiendacliente.address dirtiendacliente,cstcliente.city_name ciudadtiendacliente\n" +
            "                                         ,b.totalprice,b.idapp\n" +
            "                                    from facturacion724.bill b,tienda724.store s,tercero724.third t,tercero724.common_basicinfo c,tercero724.directory d,tercero724.phone p\n" +
            "                                        ,tercero724.mail e,facturacion724.document doc,facturacion724.detail_payment_bill dp,tercero724.directory dircliente\n" +
            "                                        ,tercero724.phone pcliente,tercero724.mail ecliente\n" +
            "                                        ,tercero724.phone pprov,tercero724.mail eprov\n" +
            "                                        ,tienda724.store sc,tercero724.directory dirtiendacliente,tercero724.city cstcliente\n" +
            "                                    where b.id_store=s.id_store and s.id_directory=d.id_directory and d.id_directory=p.id_directory and d.id_directory=e.id_directory\n" +
            "                                    and b.id_third_destinity=t.id_third and t.id_common_basicinfo=c.id_common_basicinfo and b.ID_DOCUMENT = doc.ID_DOCUMENT\n" +
            "                                            and b.id_bill=dp.id_bill(+) and t.id_directory=dircliente.id_directory\n" +
            "                                            and dircliente.id_directory=pcliente.id_directory and dircliente.id_directory=ecliente.id_directory\n" +
            "                                            and d.id_directory=pprov.id_directory and d.id_directory=eprov.id_directory\n" +
            "                                            and b.id_store_client=sc.id_store and sc.id_directory=dirtiendacliente.id_directory and dirtiendacliente.id_city=cstcliente.id_city\n" +
            "                                    and b.id_bill_type=:id_bill_type and b.id_bill_state=:id_bill_state and b.id_store=:id_store \n" +
            "                                           and pcliente.priority=1 and ecliente.priority=1\n" +
            "                                            and pprov.priority=1 and eprov.priority=1\n" +
            "                                            and b.purchase_date between trunc(sysdate)-15 and trunc(sysdate)+1 and b.id_mesa is null\n" +
            "                                     union\n" +
            "                                    select b.id_store, b.id_store_client, b.id_bill,c.fullname cliente,s.description tienda,num_documento,num_documento_cliente numpedido\n" +
            "                                    ,to_char(purchase_date,'yyyy-mon-dd hh24:mi:ss') fecha\n" +
            "                                        ,dircliente.address,pcliente.phone,ecliente.mail,dircliente.latitud,dircliente.longitud, 'N/A' body,b.id_third_destinity idthirdcliente,nvl(dp.id_payment_method,1) id_payment\n" +
            "                                    ,d.address addressp,pprov.phone phonep,eprov.mail mailp,d.latitud latitudp,d.longitud longitudp\n" +
            "                                    ,sc.description tiendacliente,dirtiendacliente.address dirtiendacliente,cstcliente.city_name ciudadtiendacliente\n" +
            "                                    ,b.totalprice,b.idapp\n" +
            "                                        from facturacion724.bill b,tienda724.store s,tercero724.third t,tercero724.common_basicinfo c,tercero724.directory d,tercero724.phone p,tercero724.mail e\n" +
            "                                        ,facturacion724.document doc,facturacion724.detail_payment_bill dp,tercero724.directory dircliente\n" +
            "                                        ,tercero724.phone pcliente,tercero724.mail ecliente\n" +
            "                                        ,tercero724.phone pprov,tercero724.mail eprov\n" +
            "                                        ,tienda724.store sc,tercero724.directory dirtiendacliente,tercero724.city cstcliente\n" +
            "                                    where b.id_store=s.id_store and s.id_directory=d.id_directory and d.id_directory=p.id_directory and d.id_directory=e.id_directory\n" +
            "                                    and b.id_third_destinity=t.id_third and t.id_common_basicinfo=c.id_common_basicinfo and b.ID_DOCUMENT is null \n" +
            "                                            and b.id_bill=dp.id_bill(+) and t.id_directory=dircliente.id_directory\n" +
            "                                            and dircliente.id_directory=pcliente.id_directory and dircliente.id_directory=ecliente.id_directory\n" +
            "                                            and d.id_directory=pprov.id_directory and d.id_directory=eprov.id_directory\n" +
            "                                            and b.id_store_client=sc.id_store and sc.id_directory=dirtiendacliente.id_directory and dirtiendacliente.id_city=cstcliente.id_city\n" +
            "                                    and b.id_bill_type=:id_bill_type and b.id_bill_state=:id_bill_state and b.id_store=:id_store \n" +
            "                                            and pcliente.priority=1 and ecliente.priority=1\n" +
            "                                            and pprov.priority=1 and eprov.priority=1\n" +
            "                                            and b.purchase_date between trunc(sysdate)-15 and trunc(sysdate)+1 and b.id_mesa is null\n" +
            "                                    order by fecha DESC " )
    List<Pedido> getPedidos(@Bind("id_store") Long id_store,
                            @Bind("id_bill_state") Long id_bill_state,
                            @Bind("id_bill_type") Long id_bill_type);

    @RegisterMapper(PedidoMapper.class)
    @SqlQuery("select b.id_store, b.id_store_client, b.id_bill,c.fullname cliente,s.description tienda,num_documento,num_documento_cliente numpedido\n" +
            "                                        ,to_char(purchase_date,'yyyy-mon-dd hh24:mi:ss') fecha\n" +
            "                                         ,dircliente.address,pcliente.phone,ecliente.mail\n" +
            "                                         ,dircliente.latitud,dircliente.longitud, doc.body,b.id_third_destinity idthirdcliente,nvl(dp.id_payment_method,1) id_payment\n" +
            "                                         ,d.address addressp,pprov.phone phonep,eprov.mail mailp,d.latitud latitudp,d.longitud longitudp\n" +
            "                                         ,sc.description tiendacliente,dirtiendacliente.address dirtiendacliente,cstcliente.city_name ciudadtiendacliente\n" +
            "                                         ,b.totalprice,b.idapp\n" +
            "                                    from facturacion724.bill b,tienda724.store s,tercero724.third t,tercero724.common_basicinfo c,tercero724.directory d,tercero724.phone p\n" +
            "                                        ,tercero724.mail e,facturacion724.document doc,facturacion724.detail_payment_bill dp,tercero724.directory dircliente\n" +
            "                                        ,tercero724.phone pcliente,tercero724.mail ecliente\n" +
            "                                        ,tercero724.phone pprov,tercero724.mail eprov\n" +
            "                                        ,tienda724.store sc,tercero724.directory dirtiendacliente,tercero724.city cstcliente\n" +
            "                                    where b.id_store=s.id_store and s.id_directory=d.id_directory and d.id_directory=p.id_directory and d.id_directory=e.id_directory\n" +
            "                                    and b.id_third_destinity=t.id_third and t.id_common_basicinfo=c.id_common_basicinfo and b.ID_DOCUMENT = doc.ID_DOCUMENT\n" +
            "                                            and b.id_bill=dp.id_bill(+) and t.id_directory=dircliente.id_directory\n" +
            "                                            and dircliente.id_directory=pcliente.id_directory and dircliente.id_directory=ecliente.id_directory\n" +
            "                                            and d.id_directory=pprov.id_directory and d.id_directory=eprov.id_directory\n" +
            "                                            and b.id_store_client=sc.id_store and sc.id_directory=dirtiendacliente.id_directory and dirtiendacliente.id_city=cstcliente.id_city\n" +
            "                                    and b.id_bill_type=:id_bill_type and b.id_bill_state=:id_bill_state and b.id_store=:id_store \n" +
            "                                           and pcliente.priority=1 and ecliente.priority=1\n" +
            "                                            and pprov.priority=1 and eprov.priority=1\n" +
            "                                            and b.purchase_date between trunc(:date1) and trunc(:date2)+1 and b.id_mesa is null\n" +
            "                                     union\n" +
            "                                    select b.id_store, b.id_store_client, b.id_bill,c.fullname cliente,s.description tienda,num_documento,num_documento_cliente numpedido\n" +
            "                                    ,to_char(purchase_date,'yyyy-mon-dd hh24:mi:ss') fecha\n" +
            "                                        ,dircliente.address,pcliente.phone,ecliente.mail,dircliente.latitud,dircliente.longitud, 'N/A' body,b.id_third_destinity idthirdcliente,nvl(dp.id_payment_method,1) id_payment\n" +
            "                                    ,d.address addressp,pprov.phone phonep,eprov.mail mailp,d.latitud latitudp,d.longitud longitudp\n" +
            "                                    ,sc.description tiendacliente,dirtiendacliente.address dirtiendacliente,cstcliente.city_name ciudadtiendacliente\n" +
            "                                    ,b.totalprice,b.idapp\n" +
            "                                        from facturacion724.bill b,tienda724.store s,tercero724.third t,tercero724.common_basicinfo c,tercero724.directory d,tercero724.phone p,tercero724.mail e\n" +
            "                                        ,facturacion724.document doc,facturacion724.detail_payment_bill dp,tercero724.directory dircliente\n" +
            "                                        ,tercero724.phone pcliente,tercero724.mail ecliente\n" +
            "                                        ,tercero724.phone pprov,tercero724.mail eprov\n" +
            "                                        ,tienda724.store sc,tercero724.directory dirtiendacliente,tercero724.city cstcliente\n" +
            "                                    where b.id_store=s.id_store and s.id_directory=d.id_directory and d.id_directory=p.id_directory and d.id_directory=e.id_directory\n" +
            "                                    and b.id_third_destinity=t.id_third and t.id_common_basicinfo=c.id_common_basicinfo and b.ID_DOCUMENT is null \n" +
            "                                            and b.id_bill=dp.id_bill(+) and t.id_directory=dircliente.id_directory\n" +
            "                                            and dircliente.id_directory=pcliente.id_directory and dircliente.id_directory=ecliente.id_directory\n" +
            "                                            and d.id_directory=pprov.id_directory and d.id_directory=eprov.id_directory\n" +
            "                                            and b.id_store_client=sc.id_store and sc.id_directory=dirtiendacliente.id_directory and dirtiendacliente.id_city=cstcliente.id_city\n" +
            "                                    and b.id_bill_type=:id_bill_type and b.id_bill_state=:id_bill_state and b.id_store=:id_store \n" +
            "                                            and pcliente.priority=1 and ecliente.priority=1\n" +
            "                                            and pprov.priority=1 and eprov.priority=1\n" +
            "                                            and b.purchase_date between trunc(:date1) and trunc(:date2)+1 and b.id_mesa is null\n" +
            "                                    order by fecha DESC" )
    List<Pedido> getPedidos2(@Bind("id_store") Long id_store,
                             @Bind("id_bill_state") Long id_bill_state,
                             @Bind("id_bill_type") Long id_bill_type,
                             @Bind("date1") Date date1,
                             @Bind("date2") Date date2);


    @RegisterMapper(DetallePedidoMapper.class)
    @SqlQuery(" select ownbarcode,pr.provider fabricante,b.brand marca,com.name linea,co.name categoria,comu.name presentacion,ps.product_store_name producto,\n" +
            "       sum(db.quantity) cantidad,MIN(db.price) costo,(sum(db.quantity)*MIN(db.price))+((sum(db.quantity)*min(db.price))*tax.percent/100) costototal,\n" +
            "       db.id_product_third, tax.percent tax, tax.percent*MIN(db.price)/100 tax_ind\n" +
            "from tienda724.common co,tienda724.category cat,tienda724.common com,tienda724.category linea,tienda724.product p,tienda724.provider pr,\n" +
            "     tienda724.measure_unit mu,tienda724.common comu,tienda724.brand b,tienda724.codes c,tienda724.product_store ps,\n" +
            "     facturacion724.detail_bill db, tienda724.tax_tariff tax\n" +
            "where\n" +
            "        db.id_product_third = ps.id_product_store and ps.ID_TAX = tax.id_tax_tariff and co.id_common=cat.id_common and cat.id_category=p.id_category\n" +
            "  and com.id_common=linea.id_common and linea.id_category=cat.id_category_father\n" +
            "  and p.id_product=c.id_product and c.id_code=ps.id_code and pr.id_provider=b.id_provider\n" +
            "  and c.id_brand=b.id_brand and c.id_measure_unit=mu.id_measure_unit and mu.id_common=comu.id_common\n" +
            "  and id_bill in (<id_bill>)\n" +
            "group by ownbarcode,pr.provider,b.brand,com.name,co.name,comu.name,ps.product_store_name,db.id_product_third,tax.percent\n" +
            "order by 2,3,4,5")
    List<DetallePedido> getDetallesPedidos(@BindIn("id_bill") List<Long> id_store);


    @RegisterMapper(ClientePedidoMapper.class)
    @SqlQuery(" select b.NUM_DOCUMENTO, b.PURCHASE_DATE, s.DESCRIPTION, p.PHONE, m.MAIL, d.ADDRESS\n" +
            "   from facturacion724.bill b, TIENDA724.store s, TERCERO724.directory d, TERCERO724.mail m, TERCERO724.phone p\n" +
            "   where b.NUM_DOCUMENTO=(select NUM_DOCUMENTO_CLIENTE from FACTURACION724.bill where ID_BILL = :idBillVent)\n" +
            "   and b.ID_STORE= (select ID_STORE_CLIENT from FACTURACION724.bill where ID_BILL = :idBillVent)\n" +
            "   and b.ID_STORE = s.ID_STORE and s.ID_DIRECTORY = d.ID_DIRECTORY\n" +
            "   and p.ID_DIRECTORY = d.ID_DIRECTORY and m.ID_DIRECTORY = d.ID_DIRECTORY ")
    ClientePedido getDatosClientePedido(@Bind("idBillVent") Long idBillVent);


    @RegisterMapper(ClientePedidoMapper2.class)
    @SqlQuery(" select dt.NAME, cbi.DOCUMENT_NUMBER, b.NUM_DOCUMENTO, b.PURCHASE_DATE, s.DESCRIPTION, p.PHONE, m.MAIL, d.ADDRESS\n" +
            "       from facturacion724.bill b, TIENDA724.store s, TERCERO724.directory d,\n" +
            "            TERCERO724.mail m, TERCERO724.phone p, TERCERO724.COMMON_BASICINFO cbi,\n" +
            "            TERCERO724.THIRD t, TERCERO724.DOCUMENT_TYPE dt\n" +
            "where b.ID_BILL = :idbill and b.ID_STORE_CLIENT= s.ID_STORE and s.ID_DIRECTORY = d.ID_DIRECTORY\n" +
            "       and p.ID_DIRECTORY = d.ID_DIRECTORY and m.ID_DIRECTORY = d.ID_DIRECTORY and b.ID_THIRD_DESTINITY =  t.ID_THIRD\n" +
            "  and t.ID_COMMON_BASICINFO = cbi.ID_COMMON_BASICINFO and cbi.ID_DOCUMENT_TYPE = dt.ID_DOCUMENT_TYPE ")
    ClientePedido2 getDatosCliente(@Bind("idbill") Long idBillVent);


    @RegisterMapper(DetallePedidoMapper.class)
    @SqlQuery(" select OWNBARCODE,pr.provider fabricante,b.brand marca,com.name linea,co.name categoria,comu.name presentacion,ps.product_store_name producto,\n" +
            "                   sum(db.quantity) cantidad,min(pr.price) costo,sum(db.quantity)*min(pr.price) costototal\n" +
            "                    ,db.id_product_third, tt.PERCENT tax, sum(db.quantity)*min(pr.price) - (tt.PERCENT*sum(db.quantity)*min(pr.price))/100 TAX_IND\n" +
            "             from tienda724.common co,tienda724.category cat,tienda724.common com,tienda724.category linea,tienda724.product p,tienda724.provider pr,\n" +
            "                 tienda724.measure_unit mu,tienda724.common comu,tienda724.brand b,tienda724.codes c,tienda724.product_store ps,\n" +
            "                 facturacion724.detail_bill db,tienda724.price pr, tienda724.tax_tariff tt\n" +
            "             where co.id_common=cat.id_common and cat.id_category=p.id_category and com.id_common=linea.id_common and linea.id_category=cat.id_category_father\n" +
            "              and p.id_product=c.id_product and c.id_code=ps.id_code and pr.id_provider=b.id_provider\n" +
            "              and c.id_brand=b.id_brand and c.id_measure_unit=mu.id_measure_unit and mu.id_common=comu.id_common\n" +
            "              and ps.id_product_store=db.id_product_third and ps.id_product_store=pr.id_product_store\n" +
            "          and id_bill in (<id_bill>) and ps.ID_TAX = tt.ID_TAX_TARIFF\n" +
            "             group by OWNBARCODE,db.id_product_third, pr.provider,b.brand,com.name,co.name,comu.name,ps.product_store_name,ps.standard_price, tt.percent\n" +
            "             order by 2,3,4,5 ")
    List<DetallePedido> getDetallesPedidos2(@BindIn("id_bill") List<Long> id_bill);

    @RegisterMapper(TablaPedidosMapper.class)
    @SqlQuery(" select * from tienda724.PARAM_PEDIDOS ")
    List<TablaPedidos> getPedidosT();

    @RegisterMapper(TablaPedidosMapper.class)
    @SqlQuery(" select * from tienda724.PARAM_PEDIDOS where ID_STORE_CLIENT = :idstoreclient and ID_STORE_PROVIDER = :idstoreprov ")
    List<TablaPedidos> getPedidosT2(@Bind("idstoreclient") Long idstoreclient,
                                    @Bind("idstoreprov") Long idstoreprov);



    @RegisterMapper(ReorderV2Mapper.class)
    @SqlQuery(" select\n" +
            "ps.ownbarcode BARCODE\n" +
            ",ps2.product_store_name producto\n" +
            ",com.name linea,co.name categoria\n" +
            ",pr.provider FABRICANTE\n" +
            ",br.brand marca\n" +
            ",comu.name Presentacion\n" +
            ",sum(d.quantity) \"Cantidad Vendida\",psi.quantity \"Cantidad en Inventario\"\n" +
            ",round(ps2.standard_price ,2) costo,tt.percent iva,PS2.ID_PRODUCT_STORE IDPS,D.TAX_PRODUCT IDT,PR.ID_PROVIDER\n" +
            "from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps\n" +
            "   ,tienda724.codes c,tienda724.product p,tienda724.category cat,tienda724.common co,tienda724.brand br,tienda724.measure_unit mu,\n" +
            "tienda724.common comu,tienda724.category linea,tienda724.common com\n" +
            "   ,TIENDA724.STORE ST,tienda724.product_store ps2,tienda724.tax_tariff tt\n" +
            "   ,TIENDA724.PRODUCT_STORE_INVENTORY PSI,tienda724.provider pr\n" +
            "where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code and c.id_product=p.id_product and\n" +
            "p.id_category=cat.id_category and cat.id_common=co.id_common and c.id_brand=br.id_brand and c.id_measure_unit=mu.id_measure_unit\n" +
            "  and mu.id_common=comu.id_common  and cat.id_category_father=linea.id_category and linea.id_common=com.id_common\n" +
            "  and b.id_store=st.id_store and c.id_code=ps2.id_code and ps2.id_product_store=psi.id_product_store  and ps2.id_tax=tt.ID_TAX_TARIFF\n" +
            "  and br.id_provider=pr.id_provider\n" +
            "  and ps2.id_store=:id_store --and linea.id_category!=675\n" +
            "  and b.id_bill_type=1 and b.id_bill_state=1\n" +
            "  and ps.id_store IN (<listStore>)\n" +
            "  and cat.id_category_father is not null\n" +
            "  and b.purchase_date between trunc(:date1) and trunc(:date2)+1\n" +
            "group by\n" +
            "ps.ownbarcode\n" +
            "  ,ps2.product_store_name\n" +
            "  ,com.name,co.name\n" +
            "  ,pr.provider\n" +
            "  ,br.brand\n" +
            "  ,comu.name\n" +
            "  ,psi.quantity\n" +
            "  ,ps2.standard_price,tt.percent,PS2.ID_PRODUCT_STORE,D.TAX_PRODUCT,PR.ID_PROVIDER\n" +
            "order by provider,marca ")
    List<ReorderV2> getReorderV2(@BindIn("listStore") List<String> listStore,
                                 @Bind("date1") Date date1,
                                 @Bind("date2") Date date2,
                                 @Bind("id_store") Long id_store);





    @RegisterMapper(ReorderV2NewMapper.class)
    @SqlQuery(
            "select  nvl(CBIPROV.FULLNAME,' ') PROVEEDOR ,TPROV.ID_THIRD,PSPROV.ID_STORE,\n" +
            "        ps.ownbarcode BARCODE\n" +
            "      ,ps2.product_store_name producto\n" +
            "        ,com.name linea,co.name categoria\n" +
            "        ,pr.provider FABRICANTE\n" +
            "        ,br.brand marca\n" +
            "       ,comu.name Presentacion\n" +
            "        ,sum(d.quantity) \"Cantidad Vendida\",psi.quantity \"Cantidad en Inventario\"\n" +
            "        ,round(ps2.standard_price ,2) costo,tt.percent iva,PS2.ID_PRODUCT_STORE IDPS,D.TAX_PRODUCT IDT,PR.ID_PROVIDER\n" +
            "        from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps\n" +
            "           ,tienda724.codes c,tienda724.product p,tienda724.category cat,tienda724.common co,tienda724.brand br,tienda724.measure_unit mu,\n" +
            "        tienda724.common comu,tienda724.category linea,tienda724.common com\n" +
            "           ,TIENDA724.STORE ST,tienda724.product_store ps2,tienda724.tax_tariff tt\n" +
            "           ,TIENDA724.PRODUCT_STORE_INVENTORY PSI,tienda724.provider pr\n" +
            "           ,TIENDA724.PRODUCT_STORE PSPROV,TIENDA724.STORE STPROV,\n" +
            "           TERCERO724.THIRD TPROV,TERCERO724.COMMON_BASICINFO CBIPROV\n" +
            "        where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code and c.id_product=p.id_product and\n" +
            "        p.id_category=cat.id_category and cat.id_common=co.id_common and c.id_brand=br.id_brand and c.id_measure_unit=mu.id_measure_unit\n" +
            "          and mu.id_common=comu.id_common  and cat.id_category_father=linea.id_category and linea.id_common=com.id_common\n" +
            "          and b.id_store=st.id_store and c.id_code=ps2.id_code and ps2.id_product_store=psi.id_product_store  and ps2.id_tax=tt.ID_TAX_TARIFF\n" +
            "          and br.id_provider=pr.id_provider\n" +
            "          AND PS.OWNBARCODE=PSPROV.OWNBARCODE(+)\n" +
            "          AND PSPROV.ID_STORE(+) IN(<listStoreProveedora>)--lista tiendas proveedoras\n" +
            "          AND PSPROV.ID_STORE=STPROV.ID_STORE(+) AND STPROV.ID_THIRD=TPROV.ID_THIRD(+)\n" +
            "          AND TPROV.ID_COMMON_BASICINFO=CBIPROV.ID_COMMON_BASICINFO(+)\n" +
            "          and ps2.id_store=:id_store\n" +
            "          and b.id_bill_type=1 and b.id_bill_state=1\n" +
            "          and ps.id_store IN (<listStoreCliente>) -- lista tiendas cliente\n" +
            "          and cat.id_category_father is not null\n" +
            "          and b.purchase_date between trunc(:date1) and trunc(:date2)+1\n" +
            "        group by CBIPROV.FULLNAME,TPROV.ID_THIRD,PSPROV.ID_STORE,\n" +
            "        ps.ownbarcode\n" +
            "          ,ps2.product_store_name\n" +
            "          ,com.name,co.name\n" +
            "          ,pr.provider\n" +
            "          ,br.brand\n" +
            "          ,comu.name\n" +
            "          ,psi.quantity\n" +
            "          ,ps2.standard_price,tt.percent,PS2.ID_PRODUCT_STORE,D.TAX_PRODUCT,PR.ID_PROVIDER\n" +
            "        --order by CBIPROV.FULLNAME NULLS LAST,provider,marca--)\n" +
            "        ORDER BY 8,5 ")
    List<ReorderV2New> getReorderV2New( @BindIn("listStoreCliente") List<String> listStoreCliente,
                                        @BindIn("listStoreProveedora") List<String> listStoreProveedora,
                                        @Bind("date1") Date date1,
                                        @Bind("date2") Date date2,
                                        @Bind("id_store") Long id_store);





    @SqlQuery(" select id_store_client from tienda724.param_pedidos where id_store_provider = :id_store AND STATUS='AUTO' ")
    List<Long> getClients(@Bind("id_store") Long id_store);


    @RegisterMapper(getProvidersMapper.class)
    @SqlQuery("SELECT ID_THIRD_DESTINITY,CBI.FULLNAME,ID_STORE_PROVIDER\n" +
            "FROM TIENDA724.PARAM_PEDIDOS P,TERCERO724.THIRD TH,TERCERO724.common_basicinfo CBI,TIENDA724.THIRD_PROVIDER TP\n" +
            "WHERE P.ID_THIRD_DESTINITY=TH.ID_THIRD AND th.id_common_basicinfo=CBI.id_common_basicinfo AND TP.ID_THIRD=P.ID_THIRD_DESTINITY\n" +
            "  AND TP.ID_PROVIDER=:id_prov AND P.ID_STORE_CLIENT=:id_store")
    List<getProviders> getProviders(@Bind("id_store") Long id_store,
                                    @Bind("id_prov") Long id_prov);


    @RegisterMapper(getProvidersMapper.class)
    @SqlQuery("SELECT ID_THIRD_DESTINITY,CBI.FULLNAME,ID_STORE_PROVIDER \n" +
            "FROM TIENDA724.PARAM_PEDIDOS P,TERCERO724.THIRD TH,TERCERO724.common_basicinfo CBI\n" +
            "WHERE P.ID_THIRD_DESTINITY=TH.ID_THIRD AND th.id_common_basicinfo=CBI.id_common_basicinfo\n" +
            "AND P.ID_STORE_CLIENT=:id_store")
    List<getProviders> getProviders2(@Bind("id_store") Long id_store);


    @RegisterMapper(FabByProvMapper.class)
    @SqlQuery("SELECT P.ID_PROVIDER,PROVIDER\n" +
            "FROM TIENDA724.THIRD_PROVIDER TP, TIENDA724.PROVIDER P\n" +
            "WHERE TP.ID_PROVIDER=P.ID_PROVIDER\n" +
            "AND ID_THIRD=:id_third")
    List<FabByProv> getFabByProv(@Bind("id_third") Long id_third);


    @RegisterMapper(Bill4ProdMapper.class)
    @SqlQuery("SELECT b.ID_BILL, purchase_date, num_documento,price,quantity FROM FACTURACION724.BILL B,FACTURACION724.DETAIL_BILL DB\n" +
            "WHERE B.ID_BILL=DB.ID_BILL AND PURCHASE_DATE BETWEEN trunc(:date1) and trunc(:date2)+1  and id_product_third =:id_ps and id_bill_type=1 and id_bill_state=1\n" +
            "order by purchase_date")
    List<Bill4Prod> getBills4Products(@Bind("id_ps") Long id_ps,
                                      @Bind("date1") Date date1,
                                      @Bind("date2") Date date2);






    @SqlUpdate(" insert into FACTURACION724.BILL ( ID_COMMON_STATE,ID_THIRD_EMPLOYEE,ID_THIRD,\n" +
            "                                  TOTALPRICE,PURCHASE_DATE,ID_STORE\n" +
            "                                  ,ID_BILL_STATE,SUBTOTAL,TAX,DISCOUNT,ID_BILL_TYPE, ID_THIRD_DESTINITY, ID_STORE_CLIENT)\n" +
            "                                 values ( 540, :id_third_employee,\n" +
            "                                         :id_third,:totalprice,sysdate,:id_store,  \n" +
            "                                         :id_bill_state, :subtotal, :tax, 0,\n" +
            "                                         :id_bill_type,\n" +
            "                                         :id_third_destinity," +
            " :id_store_cliente) ")
    void create(@Bind("id_third_employee") Long id_third_employee,
                @Bind("id_third") Long id_third,
                @Bind("id_store") Long id_store,
                @Bind("totalprice") Double totalprice,
                @Bind("subtotal") Double subtotal,
                @Bind("tax") Double tax,
                @Bind("id_bill_state") Long id_bill_state,
                @Bind("id_bill_type") Long id_bill_type,
                @Bind("id_third_destinity") Long id_third_destinity,
                @Bind("id_store_cliente") Long id_store_cliente
    );

    @SqlUpdate(" insert into FACTURACION724.BILL ( ID_COMMON_STATE,ID_THIRD_EMPLOYEE,ID_THIRD,\n" +
            "                                  TOTALPRICE,PURCHASE_DATE,ID_STORE\n" +
            "                                  ,ID_BILL_STATE,SUBTOTAL,TAX,DISCOUNT,ID_BILL_TYPE, ID_THIRD_DESTINITY, ID_STORE_CLIENT," +
            "                                   NUM_DOCUMENTO_CLIENTE)\n" +
            "                                 values ( 540, :id_third_employee,\n" +
            "                                         :id_third,:totalprice,sysdate,:id_store,  \n" +
            "                                         :id_bill_state, :subtotal, :tax, 0,\n" +
            "                                         :id_bill_type,\n" +
            "                                         :id_third_destinity," +
            "                                         :id_store_cliente," +
            "                                         :num_documento_cliente) ")
    void create2(@Bind("id_third_employee") Long id_third_employee,
                 @Bind("id_third") Long id_third,
                 @Bind("id_store") Long id_store,
                 @Bind("totalprice") Double totalprice,
                 @Bind("subtotal") Double subtotal,
                 @Bind("tax") Double tax,
                 @Bind("id_bill_state") Long id_bill_state,
                 @Bind("id_bill_type") Long id_bill_type,
                 @Bind("id_third_destinity") Long id_third_destinity,
                 @Bind("id_store_cliente") Long id_store_cliente,
                 @Bind("num_documento_cliente") String num_documento_cliente);

    @SqlQuery("SELECT ID_BILL FROM BILL WHERE ID_BILL IN (SELECT MAX( ID_BILL ) FROM BILL )\n")
    Long getPkLast();

    @SqlQuery("SELECT ID_BILL FROM BILL WHERE ID_BILL IN (SELECT MAX( ID_BILL ) FROM BILL WHERE ID_BILL_TYPE = 87 AND ID_BILL_STATE = 31) \n")
    Long getLastClient();

    @SqlUpdate(" INSERT INTO DETAIL_BILL ( ID_BILL,ID_COMMON_STATE, QUANTITY, PRICE, TAX, ID_PRODUCT_THIRD,TAX_PRODUCT)\n" +
            "      VALUES (:id_bill,540,:cantidad,:price,\n" +
            "           :tax,:id_ps,1) ")
    void createDetail(@Bind("id_bill") Long id_bill,
                      @Bind("cantidad") Long cantidad,
                      @Bind("id_ps") Long id_ps,
                      @Bind("price") Double price,
                      @Bind("tax") Double tax);


    @SqlUpdate(" update facturacion724.bill set ID_STORE_CLIENT = :idstore where ID_BILL =:idbill ")
    void completeManualBill(@Bind("idstore") Long idstore,
                            @Bind("idbill") Long idbill);


    @SqlQuery("SELECT price FROM tienda724.price WHERE id_product_store = :idps\n")
    List<Long> getPrice(@Bind("idps") Long idps);



    @SqlQuery(" select percent from tienda724.tax_tariff, tienda724.PRODUCT_STORE where id_tax_tariff=id_tax and ID_PRODUCT_STORE = :idps ")
    Long getTax(@Bind("idps") Long idps);

    @RegisterMapper(BillToUpdateMapper.class)
    @SqlQuery("  select id_bill, id_third, id_third_employee from facturacion724.bill where id_store=:idstore and id_store_client=:idstoreclient and num_documento=:numpedido\n ")
    BillToUpdate getBillIdByIdStore(@Bind("idstore") Long idstore,
                                    @Bind("idstoreclient") Long idstoreclient,
                                    @Bind("numpedido") String numpedido);

    @SqlUpdate(" update facturacion724.bill set ID_BILL_STATE=:billstate where id_bill = :billid ")
    void updateBillState(@Bind("billstate") Long billstate,
                         @Bind("billid") Long billid);

    @SqlUpdate(" update tienda724.product_store_inventory " +
            " set quantity = quantity - :quantity " +
            " where id_product_store=:id_product_store and id_storage=:id_storage")
    void putInventoryBill(@Bind("quantity") Long quantity,
                          @Bind("id_product_store") Long id_product_store,
                          @Bind("id_storage") Long id_storage);

    @SqlQuery(" select ID_STORAGE from tienda724.PRODUCT_STORE_INVENTORY where ID_PRODUCT_STORE = :id_product_store")
    Long getStorage(@Bind("id_product_store") Long id_product_store);

    @SqlUpdate(" update tienda724.product_store_inventory " +
            " set quantity = quantity + :quantity " +
            " where id_product_store=:id_product_store and id_storage=:id_storage")
    void putInventoryBill2(@Bind("quantity") Long quantity,
                           @Bind("id_product_store") Long id_product_store,
                           @Bind("id_storage") Long id_storage);

    @SqlQuery("SELECT ID_DETAIL_BILL FROM DETAIL_BILL WHERE ID_DETAIL_BILL IN (SELECT MAX( ID_DETAIL_BILL ) FROM DETAIL_BILL )\n")
    Long getPkLastDetail();

    @RegisterMapper(DetailDataMapper.class)
    @SqlQuery("SELECT QUANTITY, ID_PRODUCT_THIRD from facturacion724.detail_bill where ID_DETAIL_BILL = :iddbill\n")
    DetailData detailData(@Bind("iddbill") Long iddbill);

    @SqlQuery("select NUM_DOCUMENTO from facturacion724.BILL where ID_BILL=:idbill\n")
    String getNumDoc(@Bind("idbill") Long idbill);


    //    @RegisterMapper(InventoryPedidoDetailMapper.class)
//    @SqlQuery(" select\n" +
//            "    psp.id_tax,min(p.price) price, b.id_product_store, b.product_store_name,\n" +
//            "    b.OWNBARCODE , b.PRODUCT_STORE_CODE, tt.percent tax,pr.id_provider,provider\n" +
//            " from tienda724.PRODUCT_STORE b, tienda724.codes c, tienda724.brand br, tienda724.provider pr, tienda724.tax_tariff tt\n" +
//            "   ,tienda724.product_store psp,tienda724.price p\n" +
//            " where c.ID_CODE = b.ID_CODE and c.id_brand=br.id_brand and pr.id_provider=br.id_provider\n" +
//            "  and b.ownbarcode=psp.ownbarcode\n" +
//            "  and psp.id_tax=tt.id_tax_tariff\n" +
//            "  and p.id_product_store=psp.id_product_store\n" +
//            "  and b.ID_STORE = :id_store and psp.id_store=:id_store_prov\n" +
//            " group by psp.id_tax, b.id_product_store, b.product_store_name,\n" +
//            "         b.OWNBARCODE , b.PRODUCT_STORE_CODE, tt.percent,pr.id_provider,provider\n" +
//            " order by product_store_name ")

    @RegisterMapper(InventoryPedidoDetailDenlineaMapper.class)
    @SqlQuery("select psp.id_tax,p.price price,b.id_product_store, psp.product_store_name\n" +
            "                             ,b.OWNBARCODE,b.PRODUCT_STORE_CODE,tt.percent tax,pr.id_provider,provider\n" +
            "                             ,linea.id_category idlinea,colinea.name linea,cat.id_category idcat,cocat.name categoria\n" +
            "                             ,0 cantidadvendida,c.img,linea.img_url imglinea,cat.img_url imgcat\n" +
            "                             ,cp.name nombreproducto,at.id_attribute,coat.name atributo, cav.name valoratributo,av.ID_ATTRIBUTE_VALUE\n" +
            "                             ,i.img_url,i.orden,cp.description,p.PCT_DESCUENTO\n" +
            "                        from tienda724.PRODUCT_STORE b, tienda724.codes c, tienda724.brand br, tienda724.provider pr, tienda724.tax_tariff tt\n" +
            "                           ,tienda724.product_store psp,tienda724.price p, tienda724.product prod\n" +
            "                           ,tienda724.category linea,tienda724.category cat,tienda724.common colinea,tienda724.common cocat\n" +
            "                           ,tienda724.common cp\n" +
            "                           ,TIENDA724.ATTRIBUTE_LIST al\n" +
            "                           ,TIENDA724.ATTRIBUTE_DETAIL_LIST adl\n" +
            "                           ,tienda724.attribute at,tienda724.common coat,TIENDA724.ATTRIBUTE_VALUE av,tienda724.common cav\n" +
            "                         ,TIENDA724.IMAGEN_PRODUCTO i\n" +
            "                        where c.ID_CODE = b.ID_CODE and c.id_brand=br.id_brand and pr.id_provider=br.id_provider\n" +
            "                          and b.ownbarcode=psp.ownbarcode\n" +
            "                          and psp.id_tax=tt.id_tax_tariff\n" +
            "                          and p.id_product_store=psp.id_product_store\n" +
            "                          and c.id_product=prod.id_product\n" +
            "                          and prod.id_category=cat.id_category and cat.id_category_father=linea.id_category and linea.id_common=colinea.id_common\n" +
            "                          and cat.id_common=cocat.id_common\n" +
            "                          and prod.id_common=cp.id_common\n" +
            "                          and c.id_attribute_list=al.id_attribute_list(+)\n" +
            "                          and al.ID_ATTRIBUTE_LIST=adl.ID_ATTRIBUTE_LIST(+)\n" +
            "                          and av.ID_ATTRIBUTE=at.ID_ATTRIBUTE(+) and at.ID_COMMON=coat.ID_COMMON(+)\n" +
            "                          and adl.ID_ATTRIBUTE_VALUE=av.ID_ATTRIBUTE_VALUE(+) and av.ID_COMMON=cav.ID_COMMON(+)\n" +
            "                          and c.id_code=i.id_code(+)\n" +
            "                          and b.ID_STORE = :id_store and psp.id_store=:id_store_prov\n" +
            "                          and psp.DISPONIBLE_VENTA='S' and psp.status='ACTIVO'\n" +
            "                       order by psp.product_store_name,atributo")
    List<InventoryPedidoDetailDenlinea> getInventoryPedido(@Bind("id_store") Long id_store,
                                                   @Bind("id_store_prov") Long id_store_prov);


    @RegisterMapper(InventoryPedidoDetailMapper.class)
    @SqlQuery("select psp.id_tax,p.price price,b.id_product_store, psp.product_store_name,b.OWNBARCODE,b.PRODUCT_STORE_CODE,tt.percent tax,pr.id_provider,provider\n" +
            "                                              ,linea.id_category idlinea,colinea.name linea,cat.id_category idcat,cocat.name categoria\n" +
            "                                              ,0 cantidadvendida,c.img,linea.img_url imglinea,cat.img_url imgcat\n" +
            "                                                    from tienda724.PRODUCT_STORE b, tienda724.codes c, tienda724.brand br, tienda724.provider pr, tienda724.tax_tariff tt\n" +
            "                                                      ,tienda724.product_store psp,tienda724.price p, tienda724.product prod\n" +
            "                                                      ,tienda724.category linea,tienda724.category cat,tienda724.common colinea,tienda724.common cocat\n" +
            "                                                  ,otros.homologacion_ecom he\n" +
            "                                                where c.ID_CODE = b.ID_CODE and c.id_brand=br.id_brand and pr.id_provider=br.id_provider\n" +
            "                                                     and b.ownbarcode=psp.ownbarcode\n" +
            "                                                    and psp.id_tax=tt.id_tax_tariff\n" +
            "                                                      and p.id_product_store=psp.id_product_store\n" +
            "                                                      and c.id_product=prod.id_product\n" +
            "                                                      and prod.id_category=cat.id_category and cat.id_category_father=linea.id_category and linea.id_common=colinea.id_common\n" +
            "                                                      and cat.id_common=cocat.id_common\n" +
            "                                                      and he.barcode=psp.ownbarcode\n" +
            "                                                      and b.ID_STORE = :id_store and psp.id_store=:id_store_prov\n" +
            "                                                  and psp.ownbarcode != '999999993421'\n" +
            "                                                  order by psp.product_store_name")
    List<InventoryPedidoDetail> getInventoryPedidoDistriJass(@Bind("id_store") Long id_store,
                                                             @Bind("id_store_prov") Long id_store_prov);



    @RegisterMapper(InventoryPedidoDetail2Mapper.class)
    @SqlQuery("select psp.id_tax,p.price price,b.id_product_store, psp.product_store_name,b.OWNBARCODE,b.PRODUCT_STORE_CODE,tt.percent tax,pr.id_provider,provider\n" +
            "                                              ,linea.id_category idlinea,colinea.name linea,cat.id_category idcat,cocat.name categoria\n" +
            "                                              ,0 cantidadvendida,c.img,linea.img_url imglinea,cat.img_url imgcat,pr.IMG_URL IMGPROVIDER\n" +
            "                                                    from tienda724.PRODUCT_STORE b, tienda724.codes c, tienda724.brand br, tienda724.provider pr, tienda724.tax_tariff tt\n" +
            "                                                      ,tienda724.product_store psp,tienda724.price p, tienda724.product prod\n" +
            "                                                      ,tienda724.category linea,tienda724.category cat,tienda724.common colinea,tienda724.common cocat                                            \n" +
            "                                                where c.ID_CODE = b.ID_CODE and c.id_brand=br.id_brand and pr.id_provider=br.id_provider\n" +
            "                                                     and b.ownbarcode=psp.ownbarcode\n" +
            "                                                      and psp.id_tax=tt.id_tax_tariff\n" +
            "                                                      and p.id_product_store=psp.id_product_store\n" +
            "                                                      and c.id_product=prod.id_product\n" +
            "                                                      and prod.id_category=cat.id_category and cat.id_category_father=linea.id_category and linea.id_common=colinea.id_common\n" +
            "                                                      and cat.id_common=cocat.id_common\n" +
            "                                                      and b.ID_STORE = :id_store and psp.id_store=:id_store_prov\n" +
            "                                                  and psp.ownbarcode != '999999993421'\n" +
            "                                                  order by psp.product_store_name")
    List<InventoryPedidoDetail2> getInventoryPedido2(@Bind("id_store") Long id_store,
                                                   @Bind("id_store_prov") Long id_store_prov);




    @RegisterMapper(PedidoManualMapper.class)
    @SqlQuery(" select\n" +
            " psp.id_tax,p.price price,b.id_product_store, b.product_store_name,\n" +
            " b.OWNBARCODE , b.PRODUCT_STORE_CODE, tt.percent tax,pr.id_provider,provider, psi.quantity\n" +
            " from tienda724.PRODUCT_STORE b, tienda724.codes c, tienda724.brand br, tienda724.provider pr, tienda724.tax_tariff tt\n" +
            " ,tienda724.product_store psp,tienda724.price p,tienda724.product_store_inventory psi\n" +
            " where c.ID_CODE = b.ID_CODE and c.id_brand=br.id_brand and pr.id_provider=br.id_provider\n" +
            " and b.ownbarcode=psp.ownbarcode\n" +
            " and psp.id_tax=tt.id_tax_tariff\n" +
            " and p.id_product_store=psp.id_product_store\n" +
            " and psi.id_product_store=psp.id_product_store\n" +
            " and b.ID_STORE = :id_store and psp.id_store=:id_store_prov\n" +
            " order by product_store_name ")
    List<PedidosManuales> pedidoManual(@Bind("id_store") Long id_store,
                                       @Bind("id_store_prov") Long id_store_prov);






    @SqlCall(" begin facturacion724.crear_pedido(:idthirdclient,:idstoreclient,:idthirdempclient,:idthirdprov,:idstoreprov,:detallepedido); end; ")
    void crear_pedido(@Bind("idthirdclient") Long idthirdclient,
                      @Bind("idstoreclient") Long idstoreclient,
                      @Bind("idthirdempclient") Long idthirdempclient,
                      @Bind("idthirdprov") Long idthirdprov,
                      @Bind("idstoreprov") Long idstoreprov,
                      @Bind("detallepedido") String detallepedido );



    @SqlCall(" begin facturacion724.crear_pedido_costo(:idthirdclient,:idstoreclient,:idthirdempclient,:idthirdprov,:idstoreprov,:detallepedido); end; ")
    void crear_pedido_costo(@Bind("idthirdclient") Long idthirdclient,
                            @Bind("idstoreclient") Long idstoreclient,
                            @Bind("idthirdempclient") Long idthirdempclient,
                            @Bind("idthirdprov") Long idthirdprov,
                            @Bind("idstoreprov") Long idstoreprov,
                            @Bind("detallepedido") String detallepedido );

    @SqlCall(" begin FACTURACION724.generar_pedidos_por_consumo(:fechainicio,:horainicio,:fechafin,:horafin,:idstoreprov); end; ")
    void generar_pedidos_por_consumo(@Bind("fechainicio") Date fechainicio,
                            @Bind("horainicio") Long horainicio,
                            @Bind("fechafin") Date fechafin,
                            @Bind("horafin") Long horafin,
                            @Bind("idstoreprov") Long idstoreprov );



    @SqlCall(" call facturacion724.procesar_pedidos_recibidos(:idbpedido,:idstoreprov,:idstoreclient,:idplanilla) ")
    void procedureV2(@Bind("idbpedido") Long idbpedido,
                     @Bind("idstoreprov") Long idstoreprov,
                     @Bind("idstoreclient") Long idstoreclient,
                     @Bind("idplanilla") Long idplanilla);

    @SqlCall(" call tienda724.conversion_pedidos(:idbill,:idps) ")
    void procedure(@Bind("idbill") Long idbill,
                   @Bind("idps") Long idps);

    @SqlCall(" call facturacion724.gestion_pedidos.agregar_notes_pedidos(:nota, :idbc, :idbp, :state) ")
    void addNotesBill(@Bind("nota") String nota,
                      @Bind("idbc") Long idbc,
                      @Bind("idbp") Long idbp,
                      @Bind("state") String state);

    @SqlCall(" call facturacion724.gestion_pedidos.filtro_pedidos_cliente(:idbill) ")
    void procedure2(@Bind("idbill") Long idbill);


    @SqlUpdate(" update facturacion724.bill set TOTALPRICE=:total where id_bill = :billid ")
    void updateBillTotal(@Bind("total") Long total,
                         @Bind("billid") Long billid);



    @SqlUpdate(" call facturacion724.actualizar_valores_bill(:billid) ")
    void updateBillTotalandSubtotal21(@Bind("billid") Long billid);



    @SqlUpdate(" call facturacion724.actualizar_bill_sin_IVA(:billid,0) ")
    void updateBillTotalandSubtotal22(@Bind("billid") Long billid);



    @SqlQuery(" Select sum(quantity) from tienda724.product_store_inventory where id_product_store = :idps ")
    Long getQuantity(@Bind("idps") Long idps);



    @SqlQuery(" select facturacion724.getIDBillVentaPedido(:idbpedido, :idstoreprov, :idstoreclient) from dual")
    Long getBillVentPedido(@Bind("idbpedido") Long idbpedido,
                           @Bind("idstoreprov") Long idstoreprov,
                           @Bind("idstoreclient") Long idstoreclient);

    @SqlCall(" call facturacion724.gestion_pedidos.completar_pedidos_pendientes(:idbventa, :idbcompra, :idbremision, :idstorecliente, :idstoreproveedor, :idvehiculo, :idbpedidoproveedor) ")
    void procedureVe(
            @Bind("idvehiculo") Long idvehiculo,
            @Bind("idbventa") Long idbventa,
            @Bind("idbcompra") Long idbcompra,
            @Bind("idbremision") Long idbremision,
            @Bind("idstorecliente") Long idstorecliente,
            @Bind("idstoreproveedor") Long idstoreproveedor,
            @Bind("idbpedidoproveedor") Long idbpedidoproveedor);

    @SqlCall(" call facturacion724.anular_bill_con_detail_cero(:idBill) ")
    void updateBillIf0(@Bind("idBill") Long idBill);

    @SqlQuery("select ID_BILL from facturacion724.BILL where ID_STORE=:idstore and NUM_DOCUMENTO_CLIENTE=:cons and ID_BILL_TYPE=86")
    Long getIdC(@Bind("idstore") Long idstore,@Bind("cons") String cons);

    @RegisterMapper(StoresPedidosMapper.class)
    @SqlQuery(" select distinct id_store_provider, s.DESCRIPTION\n" +
            " from tienda724.param_pedidos p, tienda724.store s\n" +
            " where id_third_destinity=:idthird and s.ID_STORE= p.id_store_provider and id_store_client=:id_store_client")
    List<StoresPedidos> getStoreList(@Bind("idthird") Long idthird,
                                     @Bind("id_store_client") Long id_store_client);


    @RegisterMapper(DomicilioExcelRowMapper.class)
    @SqlQuery(" select v.CODIGO_VENDEDOR_ECOM vendedor,het.codigoclienteecom cliente,0 estado,'01' bodega,b.num_documento ORDENDECOMPRA,to_char(purchase_date,'dd/mm/yyyy') fecha,null FECHAENTREGA\n" +
            ",NULL OBSERVACION,he.codigoecom CODIGODELPRODUCTO, db.quantity CANTIDAD, B.DISCOUNT DESCUENTO,0 DESCUENT,'P' tipo,db.price valor, 'Und' Unidad,'L1' LISTA\n" +
            "from facturacion724.bill b,facturacion724.detail_bill db, tienda724.product_store ps,otros.homologacion_ecom he\n" +
            ",otros.homologacion_ecom_terceros het, OTROS.HOMOLOGACION_ECOM_VENDEDOR v\n" +
            "where b.id_bill=db.id_bill and db.id_product_third=ps.id_product_store and ps.ownbarcode=he.barcode\n" +
            "and het.id_third=b.id_third_destinity and b.ID_THIRD_EMPLOYEE=v.id_third(+)\n" +
            "and ps.id_store=:id_store and id_bill_type=86 and id_bill_state=801\n" +
            "and b.PURCHASE_DATE between trunc(:date1) and trunc(:date2)+1\n" +
            "ORDER BY B.PURCHASE_DATE DESC ")
    List<DomicilioExcelRow> getExcelDomicilios(@Bind("date1") Date date1,
                                           @Bind("date2") Date date2,
                                           @Bind("id_store") Long id_store);



    @SqlCall(" call FACTURACION724.ACTUALIZAR_ESTADO_PEDIDO_APP(:idbillpedidoprov, :idbillstateclient, :idbillstateprov, :notas, " +
            " :ESTADOORIGENPROV, :IDTHIRDUSER, :ACTOR) ")
    void ACTUALIZAR_ESTDO_PEDIDO_APP(
            @Bind("idbillpedidoprov") Long idbillpedidoprov,
            @Bind("idbillstateclient") Long idbillstateclient,
            @Bind("idbillstateprov") Long idbillstateprov,
            @Bind("notas") String notas,
            @Bind("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @Bind("IDTHIRDUSER") Long IDTHIRDUSER,
            @Bind("ACTOR") String ACTOR);

    @SqlCall(" begin FACTURACION724.CREAR_PEDIDO_APP(:idstoreclient, :idthirduseraapp, :idstoreprov, :detallepedido, :descuento, :idpaymentmethod, :idapp); end; ")
    void CREAR_PEDIDO_APP(
            @Bind("idstoreclient") Long idstoreclient,
            @Bind("idthirduseraapp") Long idthirduseraapp,
            @Bind("idstoreprov") Long idstoreprov,
            @Bind("detallepedido") String detallepedido,
            @Bind("descuento") Long descuento,
            @Bind("idpaymentmethod") Long idpaymentmethod,
            @Bind("idapp") Long idapp);

    @SqlCall(" begin FACTURACION724.crear_pedido_app_opciones(" +
            ":idstoreclient ," +
            ":idthirduseraapp ," +
            ":idstoreprov ," +
            ":detallepedido ," +
            ":descuento ," +
            ":idpaymentmethod ," +
            ":idapp ," +
            ":detallepedidomesa ); end; ")
    void CREAR_PEDIDO_APP_OPCIONES(
            @Bind("idstoreclient") Long idstoreclient,
            @Bind("idthirduseraapp") Long idthirduseraapp,
            @Bind("idstoreprov") Long idstoreprov,
            @Bind("detallepedido") String detallepedido,
            @Bind("descuento") Long descuento,
            @Bind("idpaymentmethod") Long idpaymentmethod,
            @Bind("idapp") Long idapp,
            @Bind("detallepedidomesa") String detallepedidomesa);

    @SqlCall(" begin FACTURACION724.crear_pedido_app_emp(:idstoreclient, :idthirduseraapp, :idstoreprov, :detallepedido, :descuento, :idpaymentmethod, :idapp, :id_emp); end; ")
    void CREAR_PEDIDO_APP_EMP(
            @Bind("id_emp") Long id_emp,
            @Bind("idstoreclient") Long idstoreclient,
            @Bind("idthirduseraapp") Long idthirduseraapp,
            @Bind("idstoreprov") Long idstoreprov,
            @Bind("detallepedido") String detallepedido,
            @Bind("descuento") Long descuento,
            @Bind("idpaymentmethod") Long idpaymentmethod,
            @Bind("idapp") Long idapp);

    @SqlCall(" begin tienda724.contador_app_instaladas(:deviceid ,:idapp ,:appversion ); end; ")
    void CONTADOR_APP_INSTALADAS(
            @Bind("deviceid") String deviceid,
            @Bind("idapp") Long idapp,
            @Bind("appversion") Long appversion);

    @SqlCall(" begin FACTURACION724.ACTUALIZAR_PEDIDO_NOVEDAD_APP(:idbillpedidoprov, :nuevosdetalles, :notas, :idpaymentmethod, :ESTADOORIGENPROV, :IDTHIRDUSER, :ACTOR); end; ")
    void ACTUALIZAR_PEDIDO_NOVEDAD_APP(
            @Bind("idbillpedidoprov") Long idbillpedidoprov,
            @Bind("nuevosdetalles") String nuevosdetalles,
            @Bind("notas") String notas,
            @Bind("idpaymentmethod") Long idpaymentmethod,
            @Bind("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @Bind("IDTHIRDUSER") Long IDTHIRDUSER,
            @Bind("ACTOR") String ACTOR);

    @SqlCall(" call FACTURACION724.ENVIAR_PEDIDO_APP(:idbillpedidoprov, :idbillventa, :notas, :ESTADOORIGENPROV, :IDTHIRDUSER, :ACTOR) ")
    void ENVIAR_PEDIDO_APP(
            @Bind("idbillpedidoprov") Long idbillpedidoprov,
            @Bind("idbillventa") Long idbillventa,
            @Bind("notas") String notas,
            @Bind("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @Bind("IDTHIRDUSER") Long IDTHIRDUSER,
            @Bind("ACTOR") String ACTOR);



    @RegisterMapper(DatosProviderMapper.class)
    @SqlQuery("SELECT ID_STORE_PROVIDER,DPROV.ID_CITY,C.CITY_NAME,DPROV.ADDRESS,DPROV.LATITUD,DPROV.LONGITUD,DT.NAME DOCUMENT_TYPE,\n" +
            "                                  DOCUMENT_NUMBER,PROV.DESCRIPTION,MPROV.MAIL EMAILPROV,PHPROV.PHONE PHONEPROV,LD.URL_LOGO,re.ID_COUNTRY\n" +
            "                            FROM TIENDA724.PARAM_PEDIDOS PP, TIENDA724.STORE PROV, TERCERO724.DIRECTORY DPROV, TERCERO724.CITY C\n" +
            "                              ,TERCERO724.THIRD T,TERCERO724.COMMON_BASICINFO CBI,TERCERO724.DOCUMENT_TYPE DT\n" +
            "                              ,TERCERO724.MAIL MPROV,TERCERO724.PHONE PHPROV\n" +
            "                              ,tercero724.legal_data LD\n" +
            "                              ,tercero724.state st,tercero724.region re\n" +
            "                            WHERE PP.ID_STORE_PROVIDER=PROV.ID_STORE AND PROV.ID_DIRECTORY=DPROV.ID_DIRECTORY AND DPROV.ID_CITY=C.ID_CITY\n" +
            "                              AND PROV.ID_THIRD=T.ID_THIRD AND T.ID_COMMON_BASICINFO=CBI.ID_COMMON_BASICINFO AND CBI.ID_DOCUMENT_TYPE=DT.ID_DOCUMENT_TYPE\n" +
            "                              AND DPROV.ID_DIRECTORY=MPROV.ID_DIRECTORY AND DPROV.ID_DIRECTORY=PHPROV.ID_DIRECTORY\n" +
            "                              AND T.ID_LEGAL_DATA=LD.ID_LEGAL_DATA\n" +
            "                              AND C.ID_STATE=st.ID_STATE AND st.ID_REGION=re.ID_REGION\n" +
            "                              AND MPROV.PRIORITY=1 AND PHPROV.PRIORITY=1\n" +
            "                              AND ID_STORE_CLIENT=:id_store AND DISPONIBLE='S'\n" +
            "                              order by id_store_provider desc")
    List<DatosProvider> getDatosProveedor(@Bind("id_store") Long id_store);



    @RegisterMapper(PlanillaBMapper.class)
    @SqlQuery(" select ID_PLANILLA,NUM_PLANILLA,cbi.fullname\n" +
            "from FACTURACION724.PLANILLA pl, tercero724.third th, tercero724.common_basicinfo cbi\n" +
            "where pl.id_third_domiciliario=th.id_third and th.id_common_basicinfo=cbi.id_common_basicinfo \n" +
            "and pl.id_store=:id_store and status=:status and pl.id_third_domiciliario!=-1 \n" )
    List<PlanillaB> getPlanillas(@Bind("id_store") Long id_store, @Bind("status") String status);

    @RegisterMapper(ThirdDataPedidosMapper.class)
    @SqlQuery(" select th.id_third,cbi.fullname\n" +
            "FROM  tercero724.third th,tienda724.store_domiciliario sd, tercero724.common_basicinfo cbi\n" +
            "WHERE sd.id_person=th.id_person and th.id_common_basicinfo=cbi.id_common_basicinfo\n" +
            "and sd.id_store=:id_store and th.id_third not in (\n" +
            "select pl.id_third_domiciliario \n" +
            "from tienda724.store_domiciliario sd, tercero724.third th,FACTURACION724.PLANILLA pl\n" +
            "where sd.id_person=th.id_person and th.id_third=pl.id_third_domiciliario \n" +
            "and sd.id_store=:id_store and pl.status IN ('R','N')) " )
    List<ThirdDataPedidos> getThirDataPedidos(@Bind("id_store") Long id_store);


    @RegisterMapper(PlanillaDataMapper.class)
    @SqlQuery(" select TO_CHAR(P.FECHA_INICIO,'DD-MON-YYYY HH24:MI:SS') FECHA_INICIO,P.NOTES,cb.fullname DOMICILIARIO,P.ID_PLANILLA,NUM_PLANILLA NUMPLANILLA,STATUS,NUM_DOCUMENTO NUMFACTURA,NUM_DOCUMENTO_CLIENTE NUMPEDIDO,b.TOTALPRICE,b.id_bill  \n" +
            "from FACTURACION724.PLANILLA p,FACTURACION724.DETALLE_PLANILLA dp, FACTURACION724.BILL b,TERCERO724.THIRD t,TERCERO724.COMMON_BASICINFO cb\n" +
            "where P.ID_PLANILLA=dp.ID_PLANILLA AND dp.ID_BILL_FACTURA=b.ID_BILL AND p.id_third_domiciliario=t.id_third and t.id_common_basicinfo=cb.id_common_basicinfo\n" +
            "AND P.ID_PLANILLA=:id_planilla " )
    List<PlanillaData> getDatosPlanilla(@Bind("id_planilla") Long id_planilla);

    @RegisterMapper(DatosDomiciliarioMapper.class)
    @SqlQuery(" select cb.fullname DOMICILIARIO,P.ID_PLANILLA,NUM_PLANILLA NUMPLANILLA,P.STATUS,ROUND(SUM(b.TOTALPRICE)) TOTAL,p.id_cierre_caja \n" +
            "from FACTURACION724.PLANILLA p,FACTURACION724.DETALLE_PLANILLA dp, FACTURACION724.BILL b\n" +
            "     ,TERCERO724.THIRD t,TERCERO724.COMMON_BASICINFO cb\n" +
            "where P.ID_PLANILLA=dp.ID_PLANILLA AND dp.ID_BILL_FACTURA=b.ID_BILL AND p.id_third_domiciliario=t.id_third and t.id_common_basicinfo=cb.id_common_basicinfo\n" +
            "AND p.id_store=:id_store and p.status=:status \n" +
            "GROUP BY cb.fullname,P.ID_PLANILLA,NUM_PLANILLA,P.STATUS,p.id_cierre_caja \n" +
            "ORDER BY 1 " )
    List<DatosDomiciliario> getDatosDomiciliario(@Bind("id_store") Long id_store,
                                                 @Bind("status") String status);


    @RegisterMapper(PlanillaBMapper.class)
    @SqlQuery(" select ID_PLANILLA,NUM_PLANILLA  \n" +
            "from FACTURACION724.PLANILLA pl\n" +
            "where id_cierre_caja=:id_cierre_caja and status!='C'" )
    List<PlanillaB> getPlanillas2(@Bind("id_cierre_caja") Long id_cierre_caja);


    @RegisterMapper(PlanillaB2Mapper.class)
    @SqlQuery(" select pl.id_planilla,TO_CHAR(PL.FECHA_INICIO,'DD-MON-YYYY HH24:MI:SS') FECHA_INICIO,num_planilla,status,cbi.fullname domiciliario \n" +
            "from FACTURACION724.PLANILLA PL,TERCERO724.THIRD TH, TERCERO724.COMMON_BASICINFO CBI \n" +
            "WHERE th.id_third=pl.id_third_domiciliario and th.id_common_basicinfo=cbi.id_common_basicinfo and id_cierre_caja=:id_cierre_caja AND STATUS != :status \n" +
            "       and facturacion724.sepuedecerrarplanilla(pl.id_planilla)=1 ")
    List<PlanillaB2> getPlanillaData2(@Bind("id_cierre_caja") Long id_cierre_caja,@Bind("status") String status);


    @SqlCall(" call FACTURACION724.ENVIAR_PEDIDOS_FACTURADOS(:IDPLANILLA,:ESTADOORIGENPROV, :IDTHIRDUSER, :ACTOR) ")
    void ENVIAR_PEDIDOS_FACTURADOS(
            @Bind("IDPLANILLA") Long IDPLANILLA,
            @Bind("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @Bind("IDTHIRDUSER") Long IDTHIRDUSER,
            @Bind("ACTOR") String ACTOR);

    @SqlCall(" call FACTURACION724.CREAR_MOVIMIENTO_CAJA(:IDCIERRECAJA, :VALOR, :NATURALEZA, :NOTAS) ")
    void CREAR_MOVIMIENTO_CAJA(
            @Bind("IDPLANILLA") Long IDPLANILLA,
            @Bind("VALOR") Long VALOR,
            @Bind("NATURALEZA") String NATURALEZA,
            @Bind("NOTAS") String NOTAS);

    @SqlCall(" call FACTURACION724.CERRAR_PLANILLA(:IDPLANILLA, :NOTAS, :valorbilletes, :valormonedas, :ESTADOORIGENPROV, :IDTHIRDUSER, :ACTOR,:valorotros) ")
    void CERRAR_PLANILLA(
            @Bind("IDPLANILLA") Long IDPLANILLA,
            @Bind("NOTAS") String NOTAS,
            @Bind("valorbilletes") Long valorbilletes,
            @Bind("valormonedas") String valormonedas,
            @Bind("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @Bind("IDTHIRDUSER") Long IDTHIRDUSER,
            @Bind("ACTOR") String ACTOR,
            @Bind("valorotros") String valorotros
    );



    @SqlCall(" call FACTURACION724.CERRAR_PLANILLA_NOVEDAD(:IDPLANILLA,:NOTAS,:ESTADOORIGENPROV, :IDTHIRDUSER, :ACTOR) ")
    void CERRAR_PLANILLA_NOVEDAD(
            @Bind("IDPLANILLA") Long IDPLANILLA,
            @Bind("NOTAS") String NOTAS,
            @Bind("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
            @Bind("IDTHIRDUSER") Long IDTHIRDUSER,
            @Bind("ACTOR") String ACTOR
    );



    @SqlCall(" call FACTURACION724.CIERRE_CAJA_PLANILLA(:IDCIERRECAJA) ")
    void CIERRE_CAJA_PLANILLA(
            @Bind("IDCIERRECAJA") Long IDCIERRECAJA
    );


    @SqlCall(" call tienda724.copia_producto(:ownbarcode,:idstore,:idstoredestiny) ")
    void copia_producto(@Bind("ownbarcode") String ownbarcode,
                        @Bind("idstore") Long idstore,
                        @Bind("idstoredestiny") Long idstoredestiny);

    @SqlCall(" call facturacion724.ABRIR_CAJA_PLANILLA(:IDCIERRECAJA) ")
    void ABRIR_CAJA_PLANILLA(@Bind("IDCIERRECAJA") Long IDCIERRECAJA);


    @SqlCall(" call facturacion724.CANCELAR_PEDIDO_APP(:idbillpedidoprov, :notes, :idcaja, :ESTADOORIGENPROV, :IDTHIRDUSER, :ACTOR) ")
    void CANCELAR_PEDIDO_APP(@Bind("idbillpedidoprov") Long idbillpedidoprov,
                             @Bind("notes") String notes,
                             @Bind("idcaja") Long idcaja,
                             @Bind("ESTADOORIGENPROV") Long ESTADOORIGENPROV,
                             @Bind("IDTHIRDUSER") Long IDTHIRDUSER,
                             @Bind("ACTOR") String ACTOR);



    @RegisterMapper(Pedido2Mapper.class)
    @SqlQuery("select b.id_store, b.id_store_client, b.id_bill,c.fullname cliente,s.description tienda,num_documento,num_documento_cliente numpedido,purchase_date fecha\n" +
            "                        ,dircliente.address,pcliente.phone,ecliente.mail\n" +
            "                        ,dircliente.latitud,dircliente.longitud, doc.body,b.id_third_destinity idthirdcliente,nvl(dp.id_payment_method,1) id_payment\n" +
            "                        ,d.address addressp,pprov.phone phonep,eprov.mail mailp,d.latitud latitudp,d.longitud longitudp\n" +
            "                        ,sc.description tiendacliente,dirtiendacliente.address dirtiendacliente,cstcliente.city_name ciudadtiendacliente,b.totalprice,B.ID_BILL_STATE\n" +
            "                        ,cityprov.CITY_NAME ciudadprov\n" +
            "                        from facturacion724.bill b,tienda724.store s,tercero724.third t,tercero724.common_basicinfo c,tercero724.directory d,tercero724.phone p\n" +
            "                        ,tercero724.mail e,facturacion724.document doc,facturacion724.detail_payment_bill dp,tercero724.directory dircliente\n" +
            "                        ,tercero724.phone pcliente,tercero724.mail ecliente\n" +
            "                        ,tercero724.phone pprov,tercero724.mail eprov\n" +
            "                        ,tienda724.store sc,tercero724.directory dirtiendacliente,tercero724.city cstcliente\n" +
            "                        ,tercero724.city cityprov\n" +
            "                        where b.id_store=s.id_store and s.id_directory=d.id_directory and d.id_directory=p.id_directory and d.id_directory=e.id_directory\n" +
            "                        and b.id_third_destinity=t.id_third and t.id_common_basicinfo=c.id_common_basicinfo and b.ID_DOCUMENT = doc.ID_DOCUMENT\n" +
            "                        and b.id_bill=dp.id_bill(+) and t.id_directory=dircliente.id_directory\n" +
            "                        and dircliente.id_directory=pcliente.id_directory and dircliente.id_directory=ecliente.id_directory\n" +
            "                        and d.id_directory=pprov.id_directory and d.id_directory=eprov.id_directory\n" +
            "                        and b.id_store_client=sc.id_store and sc.id_directory=dirtiendacliente.id_directory and dirtiendacliente.id_city=cstcliente.id_city\n" +
            "                        and b.id_bill_type=:id_bill_type\n" +
            "                        and pcliente.priority=1 and ecliente.priority=1\n" +
            "                        and pprov.priority=1 and eprov.priority=1\n" +
            "                        and cityprov.id_city=d.id_city\n" +
            "                        and b.id_third_destinity=:idthirdclient\n" +
            "                        and b.id_bill_state in (<id_bill_state>)\n" +
            "                        and b.idapp=:idapp\n" +
            "                        order by fecha DESC")
    List<Pedido2> getPedidosVerThird(@BindIn("id_bill_state") List<Long> id_bill_state,
                                     @Bind("idthirdclient") Long idthirdclient,
                                     @Bind("id_bill_type") Long id_bill_type,
                                     @Bind("idapp") Long idapp);



    @SqlQuery(" select pedido.id_bill \n" +
            "from facturacion724.detalle_planilla dp,facturacion724.bill b,facturacion724.bill pedido\n" +
            "where dp.id_bill_factura=b.id_bill and b.num_documento_cliente=pedido.num_documento and pedido.id_bill_type=86 and pedido.id_store=:id_store\n" +
            "and dp.id_planilla=:id_planilla ")
    List<Long> getIdBillPlanilla(@Bind("id_store") Long id_store,
                                     @Bind("id_planilla") Long id_planilla);



    @RegisterMapper(DetailForPedidoMapper.class)
    @SqlQuery(" select fecha_evento,actor,id_third_user,idestadoorigen,idestadodestino,notas from facturacion724.log_estados_pedido where id_bill_pedido_proveedor=:id_bill order by fecha_evento desc ")
    List<DetailForPedido> getDetailForPedido(@Bind("id_bill") Long id_bill);


    @RegisterMapper(KardexMapper.class)
    @SqlQuery("select k.id_bill,fecha_movimiento,product_store_name producto\n" +
            ",nvl(b.num_documento,'VACIO') DOCUMENTO\n" +
            ",DECODE(ID_BILL_TYPE,1,'VENTA ',4,'SALIDA ',2,'COMPRA ',3,'ENTRADA ',-501,\n" +
            "'ACUALIZAR INVENTARIO ',\n" +
            "-502,'DEVOLUCION VENTA/SALIDA #'||k.num_documento,\n" +
            "-503,'DEVOLUCION COMPRA/ENTRADA #'||k.num_documento) TIPO_MOVIMIENTO\n" +
            ",k.id_product_store,cantidad_actual INVENTARIO_INICIAL,cantidad_movimiento\n" +
            ",decode(DECODE(ID_BILL_TYPE,1,0,4,0,2,1,3,1,-501,2,-502,3,-503,4),0,cantidad_actual-cantidad_movimiento,1\n" +
            ",cantidad_actual+cantidad_movimiento,2,cantidad_movimiento,3,cantidad_actual+cantidad_movimiento,4,cantidad_actual-cantidad_movimiento)\n" +
            " Inventario_final\n" +
            ",(select cbi.fullname from TERCERO724.THIRD T,TERCERO724.common_basicinfo cbi\n" +
            "where t.id_third=k.id_third_employee and t.id_common_basicinfo=cbi.id_common_basicinfo) usuario \n" +
            "from tienda724.kardex k,tienda724.product_store ps,facturacion724.bill b\n" +
            "where k.id_product_store=ps.ID_PRODUCT_STORE and k.id_bill=b.id_bill\n" +
            "and ps.ownbarcode=:ownbarcode and ps.id_store=:id_store\n" +
            "and fecha_movimiento between TRUNC(:date1) and TRUNC(:date2)+1\n" +
            "order by fecha_movimiento")
    List<Kardex> getKardex(@Bind("id_store") Long id_store,
                           @Bind("ownbarcode") String ownbarcode,
                           @Bind("date1") Date date1,
                           @Bind("date2") Date date2);



    @SqlCall(" call FACTURACION724.CREAR_PLANILLA_PEDIDOS(:IDVEHICULO,:IDSTORE,:IDCIERRECAJA)\n ")
    void CREAR_PLANILLA_PEDIDOS(
            @Bind("IDVEHICULO") Long IDVEHICULO,
            @Bind("IDSTORE") Long IDSTORE,
            @Bind("IDCIERRECAJA") Long IDCIERRECAJA);

    @SqlCall(" call CERRAR_PLANILLA_PEDIDOS(:IDPLANILLA,:NOTAS,:valorbilletes,:valormonedas, :valorotros) ")
    void CERRAR_PLANILLA_PEDIDOS(
            @Bind("IDPLANILLA") Long IDPLANILLA,
            @Bind("NOTAS") String NOTAS,
            @Bind("valorbilletes") Double valorbilletes,
            @Bind("valormonedas") Double valormonedas,
            @Bind("valorotros") Double valorotros);


    @SqlCall(" begin facturacion724.ACTUALIZAR_PEDIDO(:idbillpedidocliente,:nuevosdetalles,:idstate ); end; ")
    void actualizar_pedido_trad(@Bind("idbillpedidocliente") Long idbillpedidocliente,
                                @Bind("nuevosdetalles") String nuevosdetalles,
                                @Bind("idstate") Long idstate);



    @SqlCall(" call facturacion724.CANCELAR_PEDIDO(:IDBILL) ")
    void cancelar_pedido_trad(@Bind("IDBILL") Long IDBILL);


    @SqlCall(" call facturacion724.eliminar_faltantes(:idstore) ")
    void eliminar_faltantes(@Bind("idstore") Long idstore );





}
