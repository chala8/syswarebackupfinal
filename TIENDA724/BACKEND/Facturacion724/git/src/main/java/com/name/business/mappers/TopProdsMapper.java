package com.name.business.mappers;


import com.name.business.entities.Pedido;
import com.name.business.entities.TopProds;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TopProdsMapper implements ResultSetMapper<TopProds> {

    @Override
    public TopProds map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new TopProds(
                resultSet.getString("OWNBARCODE"),
                resultSet.getString("PRODUCT_STORE_CODE"),
                resultSet.getLong("ID_PRODUCT_THIRD"),
                resultSet.getString("PRODUCT_STORE_NAME"),
                resultSet.getString("IMG"),
                resultSet.getLong("VENTAS")
        );
    }
}
