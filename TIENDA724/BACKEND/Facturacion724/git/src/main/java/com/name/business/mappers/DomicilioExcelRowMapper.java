package com.name.business.mappers;


import com.name.business.entities.Domicilio;
import com.name.business.entities.DomicilioExcelRow;
import com.name.business.entities.Pedido;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DomicilioExcelRowMapper implements ResultSetMapper<DomicilioExcelRow> {

    @Override
    public DomicilioExcelRow map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DomicilioExcelRow(
                resultSet.getString("VENDEDOR"),
                resultSet.getString("CLIENTE"),
                resultSet.getString("ESTADO"),
                resultSet.getString("BODEGA"),
                resultSet.getString("ORDENDECOMPRA"),
                resultSet.getString("FECHA"),
                resultSet.getString("FECHAENTREGA"),
                resultSet.getString("OBSERVACION"),
                resultSet.getString("CODIGODELPRODUCTO"),
                resultSet.getString("CANTIDAD"),
                resultSet.getString("DESCUENTO"),
                resultSet.getString("DESCUENT"),
                resultSet.getString("TIPO"),
                resultSet.getString("VALOR"),
                resultSet.getString("UNIDAD"),
                resultSet.getString("LISTA")
        );
    }
}
