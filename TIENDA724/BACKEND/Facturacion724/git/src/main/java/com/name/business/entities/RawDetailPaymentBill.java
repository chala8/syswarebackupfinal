package com.name.business.entities;

import java.util.Date;

public class RawDetailPaymentBill {

    private Long id_detail_payment_bill;
    private Double payment_value;
    private Long id_bill;
    private Long id_payment_method;
    private String aprobation_code;
    private Long id_common_state;
    private Long id_way_to_pay;
    private Long id_bank_entity;

    public RawDetailPaymentBill(
            Long id_detail_payment_bill,
            Double payment_value,
            Long id_bill,
            Long id_payment_method,
            String aprobation_code,
            Long id_common_state,
            Long id_way_to_pay,
            Long id_bank_entity
    ) {
        this.id_detail_payment_bill = id_detail_payment_bill;
        this.payment_value = payment_value;
        this.id_bill = id_bill;
        this.id_payment_method = id_payment_method;
        this.aprobation_code = aprobation_code;
        this.id_common_state = id_common_state;
        this.id_way_to_pay = id_way_to_pay;
        this.id_bank_entity = id_bank_entity;
    }

    public Long getId_detail_payment_bill() {
        return id_detail_payment_bill;
    }

    public void setId_detail_payment_bill(Long id_detail_payment_bill) {
        this.id_detail_payment_bill = id_detail_payment_bill;
    }

    public Double getPayment_value() {
        return payment_value;
    }

    public void setPayment_value(Double payment_value) {
        this.payment_value = payment_value;
    }

    public Long getId_bill() {
        return id_bill;
    }

    public void setId_bill(Long id_bill) {
        this.id_bill = id_bill;
    }

    public Long getId_payment_method() {
        return id_payment_method;
    }

    public void setId_payment_method(Long id_payment_method) {
        this.id_payment_method = id_payment_method;
    }

    public String getAprobation_code() {
        return aprobation_code;
    }

    public void setAprobation_code(String aprobation_code) {
        this.aprobation_code = aprobation_code;
    }

    public Long getId_common_state() {
        return id_common_state;
    }

    public void setId_common_state(Long id_common_state) {
        this.id_common_state = id_common_state;
    }

    public Long getId_way_to_pay() {
        return id_way_to_pay;
    }

    public void setId_way_to_pay(Long id_way_to_pay) {
        this.id_way_to_pay = id_way_to_pay;
    }

    public Long getId_bank_entity() {
        return id_bank_entity;
    }

    public void setId_bank_entity(Long id_bank_entity) {
        this.id_bank_entity = id_bank_entity;
    }

}
