package com.name.business.entities;

public class PlanillaB2 {




    private Long ID_PLANILLA;
    private String FECHA_INICIO;
    private String NUM_PLANILLA;
    private String DOMICILIARIO;
    private String STATUS;


    public PlanillaB2    (Long ID_PLANILLA,
                         String NUM_PLANILLA,
                         String DOMICILIARIO,
                         String FECHA_INICIO,
                         String STATUS) {
        this.FECHA_INICIO = FECHA_INICIO;
        this.STATUS = STATUS;
        this.DOMICILIARIO = DOMICILIARIO;
        this.ID_PLANILLA = ID_PLANILLA;
        this.NUM_PLANILLA  = NUM_PLANILLA;
    }


    public String getFECHA_INICIO() {
        return FECHA_INICIO;
    }

    public void setFECHA_INICIO(String FECHA_INICIO) {
        this.FECHA_INICIO = FECHA_INICIO;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }



    public String getDOMICILIARIO() { return DOMICILIARIO; }

    public void setDOMICILIARIO(String DOMICILIARIO) { this.DOMICILIARIO = DOMICILIARIO; }

    public Long getID_PLANILLA() {
        return ID_PLANILLA;
    }

    public void setID_PLANILLA(Long ID_PLANILLA) {
        this.ID_PLANILLA = ID_PLANILLA;
    }

    public String getNUM_PLANILLA() {
        return NUM_PLANILLA;
    }

    public void setNUM_PLANILLA(String NUM_PLANILLA) {
        this.NUM_PLANILLA = NUM_PLANILLA;
    }


}
