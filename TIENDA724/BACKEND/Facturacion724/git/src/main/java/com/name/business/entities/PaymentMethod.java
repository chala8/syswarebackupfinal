package com.name.business.entities;

import java.util.Date;

public class PaymentMethod {
    private Long id_payment_method;
    private String name_payment_method;

    private Long id_state_payment_method;
    private Integer state_payment_method;
    private Date creation_payment_method;
    private Date update_payment_method;

    public PaymentMethod(Long id_payment_method, String name_payment_method, CommonState state) {
        this.id_payment_method = id_payment_method;
        this.name_payment_method = name_payment_method;

        this.id_state_payment_method = state.getId_common_state();
        this.state_payment_method=state.getState();
        this.creation_payment_method = state.getCreation_date();
        this.update_payment_method = state.getUpdate_date();
    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_payment_method(),
                this.getState_payment_method(),
                this.getCreation_payment_method(),
                this.getUpdate_payment_method()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_payment_method =state.getId_common_state();
        this.state_payment_method  =state.getState();
        this.creation_payment_method =state.getCreation_date();
        this.update_payment_method =state.getUpdate_date();
    }

    public Integer getState_payment_method() {
        return state_payment_method;
    }

    public void setState_payment_method(Integer state_payment_method) {
        this.state_payment_method = state_payment_method;
    }

    public Long getId_payment_method() {
        return id_payment_method;
    }

    public void setId_payment_method(Long id_payment_method) {
        this.id_payment_method = id_payment_method;
    }

    public String getName_payment_method() {
        return name_payment_method;
    }

    public void setName_payment_method(String name_payment_method) {
        this.name_payment_method = name_payment_method;
    }

    public Long getId_state_payment_method() {
        return id_state_payment_method;
    }

    public void setId_state_payment_method(Long id_state_payment_method) {
        this.id_state_payment_method = id_state_payment_method;
    }

    public Date getCreation_payment_method() {
        return creation_payment_method;
    }

    public void setCreation_payment_method(Date creation_payment_method) {
        this.creation_payment_method = creation_payment_method;
    }

    public Date getUpdate_payment_method() {
        return update_payment_method;
    }

    public void setUpdate_payment_method(Date update_payment_method) {
        this.update_payment_method = update_payment_method;
    }
}
