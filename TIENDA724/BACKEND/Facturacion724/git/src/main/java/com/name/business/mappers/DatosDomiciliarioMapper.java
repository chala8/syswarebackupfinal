package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DatosDomiciliarioMapper  implements ResultSetMapper<DatosDomiciliario> {

    @Override
    public DatosDomiciliario map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DatosDomiciliario(

                resultSet.getString("DOMICILIARIO"),
                resultSet.getLong("ID_PLANILLA"),
                resultSet.getString("NUMPLANILLA"),
                resultSet.getString("STATUS"),
                resultSet.getDouble("TOTAL"),
                resultSet.getLong("ID_CIERRE_CAJA")
        );
    }
}
