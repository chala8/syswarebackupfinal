package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DetailPaymentBillCompleteDTO {
    private Long id_bill; /// It can be null, if The Bill does not exist, or It can given the id Biling exist
    private Long id_way_to_pay;
    private Long id_payment_method;
    private Double payment_value;
    private String aprobation_code;
    private CommonStateDTO stateDTO;

    @JsonCreator
    public DetailPaymentBillCompleteDTO(@JsonProperty("id_bill") Long id_bill, @JsonProperty("id_way_to_pay") Long id_way_to_pay,
                                        @JsonProperty("id_payment_method") Long id_payment_method, @JsonProperty("payment_value") Double payment_value,
                                        @JsonProperty("aprobation_code") String aprobation_code, @JsonProperty("state") CommonStateDTO stateDTO) {
        this.id_bill = id_bill;
        this.id_way_to_pay = id_way_to_pay;
        this.id_payment_method = id_payment_method;
        this.payment_value = payment_value;
        this.aprobation_code = aprobation_code;
        this.stateDTO = stateDTO;
    }

    public Long getId_bill() {
        return id_bill;
    }

    public void setId_bill(Long id_bill) {
        this.id_bill = id_bill;
    }

    public Long getId_way_to_pay() {
        return id_way_to_pay;
    }

    public void setId_way_to_pay(Long id_way_to_pay) {
        this.id_way_to_pay = id_way_to_pay;
    }

    public Long getId_payment_method() {
        return id_payment_method;
    }

    public void setId_payment_method(Long id_payment_method) {
        this.id_payment_method = id_payment_method;
    }

    public Double getPayment_value() {
        return payment_value;
    }

    public void setPayment_value(Double payment_value) {
        this.payment_value = payment_value;
    }

    public String getAprobation_code() {
        return aprobation_code;
    }

    public void setAprobation_code(String aprobation_code) {
        this.aprobation_code = aprobation_code;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
