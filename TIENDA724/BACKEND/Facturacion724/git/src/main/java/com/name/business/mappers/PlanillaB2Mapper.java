package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PlanillaB2Mapper  implements ResultSetMapper<PlanillaB2> {

    @Override
    public PlanillaB2 map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PlanillaB2(
                resultSet.getLong("ID_PLANILLA"),
                resultSet.getString("NUM_PLANILLA"),
                resultSet.getString("DOMICILIARIO"),
                resultSet.getString("FECHA_INICIO"),
                resultSet.getString("STATUS")
        );
    }
}
