package com.name.business.DAOs;

import com.name.business.entities.CommonSimple;
import com.name.business.entities.PaymentState;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.PaymentStateMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(PaymentStateMapper.class)
public interface PaymentStateDAO  {

    @SqlQuery("SELECT ID_PAYMENT_STATE FROM PAYMENT_STATE WHERE ID_PAYMENT_STATE IN (SELECT MAX( ID_PAYMENT_STATE ) FROM PAYMENT_STATE )\n")
    Long getPkLast();

    @SqlQuery("SELECT COUNT(ID_PAYMENT_STATE) FROM PAYMENT_STATE WHERE ID_PAYMENT_STATE = :id\n")
    Integer getValidatorID(@Bind("id") Long id);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_PAYMENT_STATE  ID, ID_COMMON_STATE FROM PAYMENT_STATE WHERE( ID_PAYMENT_STATE = :id OR :id IS NULL)\n ")
    List<CommonSimple> readCommons(@Bind("id") Long id);

    @SqlQuery("SELECT * FROM V_PAYMENTSTATE PAY_ST\n" +
            "WHERE (PAY_ST.ID_PAYMENT_STATE=:pay_st.id_payment_state OR :pay_st.id_payment_state IS NULL ) AND\n" +
            "      (PAY_ST.NAME_PAY_ST LIKE :pay_st.name_payment_state OR :pay_st.name_payment_state IS NULL ) AND\n" +
            "      (PAY_ST.ID_CM_PAY_ST=:pay_st.id_state_payment_state OR :pay_st.id_state_payment_state IS NULL ) AND\n" +
            "      (PAY_ST.STATE_CM_PAY_ST =:pay_st.state_payment_state OR :pay_st.state_payment_state IS NULL ) AND\n" +
            "      (PAY_ST.CREATION_PAY_ST =:pay_st.creation_payment_state OR :pay_st.creation_payment_state IS NULL ) AND\n" +
            "      (PAY_ST.UPDATE_PAY_ST= :pay_st.update_payment_state OR :pay_st.update_payment_state IS NULL )")
    List<PaymentState> read(@BindBean("pay_st")PaymentState paymentState);

    @SqlUpdate("INSERT INTO PAYMENT_STATE ( ID_COMMON_STATE,NAME)\n" +
            "VALUES (:id_cm_st,:name_pay_st)")
    void create(@Bind("id_cm_st") Long id_cm_st,@Bind("name_pay_st")String name_cm_st);



    @SqlUpdate("UPDATE PAYMENT_STATE SET \n" +
            "    NAME = :name\n" +
            "    WHERE (ID_PAYMENT_STATE =  :id_payment_state)")
    int update(@Bind("id_payment_state") Long id_payment_state,@Bind("name") String name);


    int delete();


    int deletePermanent();
}
