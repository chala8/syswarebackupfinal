package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.glassfish.jersey.server.JSONP;

import java.util.Date;
import java.util.List;

public class PdfPedidoDetailDTO {
    private Long cantidad;
    private String marca;
    private String linea;
    private String categoria;
    private String presentacion;
    private String producto;
    private Long costo;
    private Long costototal;
    private Long id_product_store;
    private String fabricante;
    private Double tax;
    private Double tax_ind;
    private String ownbarcode;

    @JsonCreator
    public PdfPedidoDetailDTO(@JsonProperty("ownbarcode") String ownbarcode,
                              @JsonProperty("cantidad") Long cantidad,
                              @JsonProperty("marca") String marca,
                              @JsonProperty("linea") String linea,
                              @JsonProperty("categoria") String categoria,
                              @JsonProperty("presentacion") String presentacion,
                              @JsonProperty("producto") String producto,
                              @JsonProperty("costo") Long costo,
                              @JsonProperty("costototal") Long costototal,
                              @JsonProperty("id_PRODUCT_THIRD") Long id_product_store,
                              @JsonProperty("fabricante") String fabricante,
                              @JsonProperty("tax") Double tax,
                              @JsonProperty("tax_IND") Double tax_IND) {
        this.ownbarcode = ownbarcode;
        this.tax = tax;
        this.tax_ind = tax_IND;
        this.cantidad = cantidad;
        this.marca = marca;
        this.linea = linea;
        this.categoria = categoria;
        this.presentacion = presentacion;
        this.producto = producto;
        this.costo = costo;
        this.costototal = costototal;
        this.id_product_store = id_product_store;
        this.fabricante = fabricante;
    }

    public String getownbarcode() {
        return ownbarcode;
    }
    public void setownbarcode(String ownbarcode) {
        this.ownbarcode = ownbarcode;
    }
    public String getfabricante() {
        return fabricante;
    }

    public void setfabricante(String fabricante) {
        this.fabricante = fabricante;
    }


    public Long getid_product_store() {
        return id_product_store;
    }

    public void setid_product_store(Long id_product_store) {
        this.id_product_store = id_product_store;
    }


    public Long getcostototal() {
        return costototal;
    }

    public void setcostototal(Long costototal) {
        this.costototal = costototal;
    }

    public Long getcosto() {
        return costo;
    }

    public void setcosto(Long costo) {
        this.costo = costo;
    }


    public String getproducto() {
        return producto;
    }

    public void setproducto(String producto) {
        this.producto = producto;
    }


    public String getpresentacion() {
        return presentacion;
    }

    public void setpresentacion(String presentacion) {
        this.presentacion = presentacion;
    }


    public String getcategoria() {
        return categoria;
    }

    public void setcategoria(String categoria) {
        this.categoria = categoria;
    }


    public String getlinea() {
        return linea;
    }

    public void setlinea(String linea) {
        this.linea = linea;
    }


    public String getmarca() {
        return marca;
    }

    public void setmarca(String marca) {
        this.marca = marca;
    }


    public Long getcantidad() {
        return cantidad;
    }

    public void setcantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public Double gettax() {
        return tax;
    }

    public void settax(Double tax) {
        this.tax = tax;
    }

    public Double gettax_ind() {
        return tax_ind;
    }

    public void settax_ind(Double tax_ind) {
        this.tax_ind = tax_ind;
    }

}
