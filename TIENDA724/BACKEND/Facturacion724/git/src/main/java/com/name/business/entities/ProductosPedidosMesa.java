package com.name.business.entities;

import org.skife.jdbi.v2.sqlobject.customizers.Mapper;

public class ProductosPedidosMesa {

    public String getIMG() {        return IMG;    }
    public void setIMG(String IMG) {        this.IMG = IMG;    }
    public Boolean getCHECK() {        return CHECK;    }
    public void setCHECK(Boolean CHECK) {        this.CHECK = CHECK;    }
    public String getTOTALPRICE() {
        return TOTALPRICE;
    }
    public void setTOTALPRICE(String TOTALPRICE) {
        this.TOTALPRICE = TOTALPRICE;
    }
    public String getPRODUCTO() {
        return PRODUCTO;
    }
    public void setPRODUCTO(String PRODUCTO) {
        this.PRODUCTO = PRODUCTO;
    }
    public Long getID_DETALLE_DETAIL_BILL() {
        return ID_DETALLE_DETAIL_BILL;
    }
    public void setID_DETALLE_DETAIL_BILL(Long ID_DETALLE_DETAIL_BILL) {        this.ID_DETALLE_DETAIL_BILL = ID_DETALLE_DETAIL_BILL;    }
    public Long getID_BILL() {
        return ID_BILL;
    }
    public void setID_BILL(Long ID_BILL) {
        this.ID_BILL = ID_BILL;
    }
    public Long getID_PRODUCT_STORE() {
        return ID_PRODUCT_STORE;
    }
    public void setID_PRODUCT_STORE(Long ID_PRODUCT_STORE) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
    }
    public Long getID_BILL_STATE() {
        return ID_BILL_STATE;
    }
    public void setID_BILL_STATE(Long ID_BILL_STATE) {
        this.ID_BILL_STATE = ID_BILL_STATE;
    }
    public String getNOTAS() {
        return NOTAS;
    }
    public void setNOTAS(String NOTAS) {
        this.NOTAS = NOTAS;
    }
    public String getFECHA_EVENTO() {
        return FECHA_EVENTO;
    }
    public void setFECHA_EVENTO(String FECHA_EVENTO) {
        this.FECHA_EVENTO = FECHA_EVENTO;
    }
    public String getESTADO() {
        return ESTADO;
    }
    public void setESTADO(String ESTADO) {
        this.ESTADO = ESTADO;
    }
    public String getCOLOR() {
        return COLOR;
    }
    public void setCOLOR(String COLOR) {
        this.COLOR = COLOR;
    }
    public String getOWNBARCODE() {
        return OWNBARCODE;
    }
    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }



    private String PRODUCTO;
    private Long ID_DETALLE_DETAIL_BILL;
    private Long ID_BILL;
    private Long ID_PRODUCT_STORE;
    private Long ID_BILL_STATE;
    private String NOTAS;
    private String FECHA_EVENTO;
    private String ESTADO;
    private String COLOR;
    private String OWNBARCODE;
    private String TOTALPRICE;

    private String IMG;
    private Boolean CHECK;



    public ProductosPedidosMesa(String PRODUCTO,
                                Long ID_DETALLE_DETAIL_BILL,
                                Long ID_BILL,
                                Long ID_PRODUCT_STORE,
                                Long ID_BILL_STATE,
                                String NOTAS,
                                String FECHA_EVENTO,
                                String ESTADO,
                                String COLOR,
                                String OWNBARCODE,
                                String TOTALPRICE,
                                Boolean CHECK,
                                String IMG
    ) {
        this.IMG = IMG;
        this.CHECK = CHECK;
        this.OWNBARCODE = OWNBARCODE;
        this.PRODUCTO = PRODUCTO;
        this.ID_BILL = ID_BILL;
        this.ID_DETALLE_DETAIL_BILL = ID_DETALLE_DETAIL_BILL;
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
        this.ID_BILL_STATE = ID_BILL_STATE;
        this.NOTAS = NOTAS;
        this.FECHA_EVENTO = FECHA_EVENTO;
        this.ESTADO = ESTADO;
        this.COLOR = COLOR;
        this.TOTALPRICE = TOTALPRICE;
    }


}
