package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.DatosProvider;
import com.name.business.entities.DetailBill;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DatosProviderMapper implements ResultSetMapper<DatosProvider> {

    @Override
    public DatosProvider map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DatosProvider(
                resultSet.getLong("ID_STORE_PROVIDER"),
                resultSet.getString("ID_CITY"),
                resultSet.getString("CITY_NAME"),
                resultSet.getString("ADDRESS"),
                resultSet.getDouble("LATITUD"),
                resultSet.getDouble("LONGITUD"),
                resultSet.getString("DOCUMENT_TYPE"),
                resultSet.getString("DOCUMENT_NUMBER"),
                resultSet.getString("DESCRIPTION"),
                resultSet.getString("EMAILPROV"),
                resultSet.getString("PHONEPROV"),
                resultSet.getString("URL_LOGO"),
                resultSet.getString("ID_COUNTRY")
        );
    }
}
