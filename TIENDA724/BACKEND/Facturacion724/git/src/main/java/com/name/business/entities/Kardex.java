package com.name.business.entities;

import java.util.Date;

public class Kardex {
    private Long ID_BILL;
    private String FECHA_MOVIMIENTO;
    private String PRODUCTO;
    private String DOCUMENTO;
    private String TIPO_MOVIMIENTO;
    private Long ID_PRODUCT_STORE;
    private Long INVENTARIO_INICIAL;
    private Long CANTIDAD_MOVIMIENTO;
    private Long INVENTARIO_FINAL;
    private String USUARIO;

    public Kardex(Long ID_BILL,
             String FECHA_MOVIMIENTO,
             String PRODUCTO,
             String DOCUMENTO,
             String TIPO_MOVIMIENTO,
             Long ID_PRODUCT_STORE,
             Long INVENTARIO_INICIAL,
             Long CANTIDAD_MOVIMIENTO,
             Long INVENTARIO_FINAL,
             String USUARIO) {
        this.ID_BILL = ID_BILL;
        this.FECHA_MOVIMIENTO = FECHA_MOVIMIENTO;
        this.PRODUCTO = PRODUCTO;
        this.DOCUMENTO = DOCUMENTO;
        this.TIPO_MOVIMIENTO = TIPO_MOVIMIENTO;
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
        this.INVENTARIO_INICIAL = INVENTARIO_INICIAL;
        this.CANTIDAD_MOVIMIENTO = CANTIDAD_MOVIMIENTO;
        this.INVENTARIO_FINAL = INVENTARIO_FINAL;
        this.USUARIO = USUARIO;
    }


    public Long getID_BILL() {
        return ID_BILL;
    }

    public void setID_BILL(Long ID_BILL) {
        this.ID_BILL = ID_BILL;
    }

    public String getFECHA_MOVIMIENTO() {
        return FECHA_MOVIMIENTO;
    }

    public void setFECHA_MOVIMIENTO(String FECHA_MOVIMIENTO) {
        this.FECHA_MOVIMIENTO = FECHA_MOVIMIENTO;
    }

    public String getPRODUCTO() {
        return PRODUCTO;
    }

    public void setPRODUCTO(String PRODUCTO) {
        this.PRODUCTO = PRODUCTO;
    }

    public String getDOCUMENTO() {
        return DOCUMENTO;
    }

    public void setDOCUMENTO(String DOCUMENTO) {
        this.DOCUMENTO = DOCUMENTO;
    }

    public String getTIPO_MOVIMIENTO() {
        return TIPO_MOVIMIENTO;
    }

    public void setTIPO_MOVIMIENTO(String TIPO_MOVIMIENTO) {
        this.TIPO_MOVIMIENTO = TIPO_MOVIMIENTO;
    }

    public Long getID_PRODUCT_STORE() {
        return ID_PRODUCT_STORE;
    }

    public void setID_PRODUCT_STORE(Long ID_PRODUCT_STORE) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
    }

    public Long getINVENTARIO_INICIAL() {
        return INVENTARIO_INICIAL;
    }

    public void setINVENTARIO_INICIAL(Long INVENTARIO_INICIAL) {
        this.INVENTARIO_INICIAL = INVENTARIO_INICIAL;
    }

    public Long getCANTIDAD_MOVIMIENTO() {
        return CANTIDAD_MOVIMIENTO;
    }

    public void setCANTIDAD_MOVIMIENTO(Long CANTIDAD_MOVIMIENTO) {
        this.CANTIDAD_MOVIMIENTO = CANTIDAD_MOVIMIENTO;
    }

    public Long getINVENTARIO_FINAL() {
        return INVENTARIO_FINAL;
    }

    public void setINVENTARIO_FINAL(Long INVENTARIO_FINAL) {
        this.INVENTARIO_FINAL = INVENTARIO_FINAL;
    }

    public String getUSUARIO() {
        return USUARIO;
    }

    public void setUSUARIO(String USUARIO) {
        this.USUARIO = USUARIO;
    }




}
