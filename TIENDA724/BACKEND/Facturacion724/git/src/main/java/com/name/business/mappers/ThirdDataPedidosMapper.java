package com.name.business.mappers;

import com.name.business.entities.Bill;
import com.name.business.entities.CommonState;
import com.name.business.entities.Refund;
import com.name.business.entities.ThirdDataPedidos;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ThirdDataPedidosMapper  implements ResultSetMapper<ThirdDataPedidos> {

    @Override
    public ThirdDataPedidos map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ThirdDataPedidos(
                resultSet.getLong("ID_THIRD"),
                resultSet.getString("FULLNAME")
        );
    }
}
