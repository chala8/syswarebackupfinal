package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.DetailData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailDataMapper implements ResultSetMapper<DetailData> {

    @Override
    public DetailData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DetailData(
                resultSet.getLong("QUANTITY"),
                resultSet.getLong("ID_PRODUCT_THIRD")
        );
    }
}
