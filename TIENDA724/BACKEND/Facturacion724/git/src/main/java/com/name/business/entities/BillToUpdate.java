package com.name.business.entities;

import java.util.Date;

public class BillToUpdate {
    private Long id_bill;
    private Long id_third;
    private Long id_third_employee;

    public BillToUpdate(Long id_bill,
                        Long id_third,
                        Long id_third_employee) {
        this.id_bill = id_bill;
        this.id_third = id_third;
        this.id_third_employee = id_third_employee;
    }


    public Long getid_bill() {
        return id_bill;
    }

    public void setid_bill(Long id_bill) {
        this.id_bill = id_bill;
    }

    public Long getid_third() {
        return id_third;
    }

    public void setid_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getid_third_employee() {
        return id_third_employee;
    }

    public void setid_third_employee(Long id_third_employee) {
        this.id_third_employee = id_third_employee;
    }


}
