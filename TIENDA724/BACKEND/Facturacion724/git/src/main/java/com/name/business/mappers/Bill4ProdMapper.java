package com.name.business.mappers;

import com.name.business.entities.Bill;
import com.name.business.entities.Bill4Prod;
import com.name.business.entities.CommonState;
import com.name.business.entities.BillData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Bill4ProdMapper  implements ResultSetMapper<Bill4Prod> {

    @Override
    public Bill4Prod map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Bill4Prod(
                resultSet.getLong("ID_BILL"),
                resultSet.getString("PURCHASE_DATE"),
                resultSet.getString("NUM_DOCUMENTO"),
                resultSet.getDouble("PRICE"),
                resultSet.getLong("QUANTITY")
        );
    }
}
