package com.name.business.mappers;

        import com.name.business.entities.*;
        import org.skife.jdbi.v2.StatementContext;
        import org.skife.jdbi.v2.tweak.ResultSetMapper;

        import java.sql.ResultSet;
        import java.sql.SQLException;


public class storesByThirdMapper implements ResultSetMapper<storesByThird> {

    @Override
    public storesByThird map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new storesByThird(
                resultSet.getLong("ID_THIRD"),
                resultSet.getLong("ID_STORE"),
                resultSet.getString("DESCRIPTION"),
                resultSet.getString("ADDRESS"),
                resultSet.getString("CITY_NAME"),
                resultSet.getString("LATITUD"),
                resultSet.getString("LONGITUD"),
                resultSet.getString("URL_LOGO"),
                resultSet.getString("PHONE"),
                resultSet.getString("MAIL")
        );
    }
}
