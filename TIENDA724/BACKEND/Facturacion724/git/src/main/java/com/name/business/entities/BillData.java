package com.name.business.entities;

import java.util.Date;

public class BillData {
    private Long ID_BILL_STATE;
    private Long ID_BILL;
    private Long ID_DOCUMENT;
    private String BODY;
    private Date PURCHASE_DATE;
    private Long TOTALPRICE;

    public BillData(Long ID_BILL_STATE, Long ID_BILL, Long ID_DOCUMENT, String BODY, Date PURCHASE_DATE, Long TOTALPRICE) {
        this.ID_BILL_STATE = ID_BILL_STATE;
        this.ID_BILL = ID_BILL;
        this.ID_DOCUMENT = ID_DOCUMENT;
        this.BODY = BODY;
        this.PURCHASE_DATE = PURCHASE_DATE;
        this.TOTALPRICE = TOTALPRICE;
    }

    //--------------------------------------------------------------------------------------------------

    public Long getID_BILL_STATE() {
        return ID_BILL_STATE;
    }

    public void setID_BILL_STATE(Long ID_BILL_STATE) {
        this.ID_BILL_STATE = ID_BILL_STATE;
    }

    //--------------------------------------------------------------------------------------------------

    public Long getID_BILL() {
        return ID_BILL;
    }

    public void setID_BILL(Long ID_BILL) {
        this.ID_BILL = ID_BILL;
    }

    //--------------------------------------------------------------------------------------------------

    public Long getID_DOCUMENT() {
        return ID_DOCUMENT;
    }

    public void setID_DOCUMENT(Long ID_DOCUMENT) {
        this.ID_DOCUMENT = ID_DOCUMENT;
    }

    //--------------------------------------------------------------------------------------------------

    public String getBODY() {
        return BODY;
    }

    public void setBODY(String BODY) {
        this.BODY = BODY;
    }

    //--------------------------------------------------------------------------------------------------

    public Date getPURCHASE_DATE() {
        return PURCHASE_DATE;
    }

    public void setPURCHASE_DATE(Date PURCHASE_DATE) {
        this.PURCHASE_DATE = PURCHASE_DATE;
    }

    //--------------------------------------------------------------------------------------------------

    public Long getTOTALPRICE() {
        return TOTALPRICE;
    }

    public void setTOTALPRICE(Long TOTALPRICE) {
        this.TOTALPRICE = TOTALPRICE;
    }

    //--------------------------------------------------------------------------------------------------






}
