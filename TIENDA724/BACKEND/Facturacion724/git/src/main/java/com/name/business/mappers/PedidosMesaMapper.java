package com.name.business.mappers;


import com.name.business.entities.Pedido;
import com.name.business.entities.PedidosMesa;
import com.name.business.entities.ProductosPedidosMesa;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PedidosMesaMapper implements ResultSetMapper<PedidosMesa> {

    @Override
    public PedidosMesa map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PedidosMesa(
                resultSet.getLong("ID_BILL"),
                resultSet.getString("PURCHASE_DATE"),
                resultSet.getLong("ID_MESA"),
                resultSet.getString("MESA"),
                resultSet.getLong("ID_THIRD_DOMICILIARIO"),
                resultSet.getDouble("TOTALPRICE"),
                resultSet.getString("MESERO"),
                resultSet.getLong("IDAPP"),
                resultSet.getLong("AMARILLO"),
                resultSet.getLong("NARANJA"),
                resultSet.getLong("AZUL"),
                resultSet.getLong("ROJO"),
                resultSet.getLong("VIOLETA"),
                resultSet.getLong("TOTAL_PRODUCTOS"),
                resultSet.getString("MESA_NUMBER"),
                resultSet.getLong("ID_STORE"),
                resultSet.getString("CLIENTE"),
                resultSet.getLong("NEGRO"),
                resultSet.getString("CANCELADOS"),
                false
        );
    }
}
