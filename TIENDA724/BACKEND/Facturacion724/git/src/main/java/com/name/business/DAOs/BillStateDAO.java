package com.name.business.DAOs;

import com.name.business.entities.BillState;
import com.name.business.entities.CommonSimple;
import com.name.business.mappers.BillStateMapper;
import com.name.business.mappers.CommonSimpleMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(BillStateMapper.class)
public interface BillStateDAO {

    @SqlQuery("SELECT ID_BILL_STATE FROM BILL_STATE WHERE ID_BILL_STATE IN (SELECT MAX( ID_BILL_STATE ) FROM BILL_STATE )\n")
    Long getPkLast();

    @SqlQuery("SELECT COUNT(ID_BILL_STATE) FROM BILL_STATE WHERE ID_BILL_STATE = :id")
    Integer getValidatorID(@Bind("id") Long id);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_BILL_STATE  ID, ID_COMMON_STATE FROM BILL_STATE WHERE( ID_BILL_STATE = :id OR :id IS NULL)\n ")
    List<CommonSimple> readCommons(@Bind("id") Long id);

    @SqlQuery("SELECT * FROM V_BILLSTATE V_BIL_ST\n" +
            "WHERE (V_BIL_ST.ID_BILL_STATE=:bil_st.id_bill_state OR :bil_st.id_bill_state IS NULL ) AND\n" +
            "      (V_BIL_ST.NAME_BIL_ST LIKE :bil_st.name_bill_state OR :bil_st.name_bill_state IS NULL ) AND\n" +
            "      (V_BIL_ST.ID_CM_BIL_ST=:bil_st.id_state_bill_state OR :bil_st.id_state_bill_state IS NULL ) AND\n" +
            "      (V_BIL_ST.STATE_CM_BIL_ST =:bil_st.state_bill_state OR :bil_st.state_bill_state IS NULL ) AND\n" +
            "      (V_BIL_ST.CREATION_BIL_ST =:bil_st.creation_bill_state OR :bil_st.creation_bill_state IS NULL ) AND\n" +
            "      (V_BIL_ST.UPDATE_BIL_ST= :bil_st.update_bill_state OR :bil_st.update_bill_state IS NULL )")
    List<BillState> read(@BindBean("bil_st") BillState billState);


    @SqlUpdate("INSERT INTO BILL_STATE ( ID_COMMON_STATE,NAME)\n" +
            "VALUES (:id_cm_st,:name_pay_st)\n")
    void create(@Bind("id_cm_st") Long id_common_state,@Bind("name_pay_st") String s);


    @SqlUpdate("UPDATE BILL_STATE SET\n" +
            "    NAME = :name\n" +
            "    WHERE (ID_BILL_STATE =  :id_bill_state)")
    int update(@Bind("id_bill_state") Long id_bill_state,@Bind("name") String name);


    int delete();


    int deletePermanent();
}
