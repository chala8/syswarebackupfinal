package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.name.business.businesses.BillBusiness;
import com.name.business.entities.*;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;
import org.skife.jdbi.v2.sqlobject.Bind;

import javax.annotation.security.RolesAllowed;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

@Path("/billing")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BillResource {
    private BillBusiness billBusiness;

    public BillResource(BillBusiness billBusiness) {
        this.billBusiness = billBusiness;
    }

    @POST
    @Path("/v2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response insertBillv2(BillDTOv2 billdto){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.insertBillv2(billdto);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @POST
    @Path("/v2/bills")
    @Timed
    @RolesAllowed({"Auth"})
    public Response insertBillsv2(billsV2DTO facts){

        Response response;

        Either<IException, List<BillDTOv2>> getResponseGeneric = billBusiness.insertBillsv2(facts);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getBillResource(@QueryParam("id_bill") Long id_bill,
                    @QueryParam("id_bill_father") Long id_bill_father,
                    @QueryParam("id_third_employee") Long id_third_employee,
                    @QueryParam("id_third") Long id_third,
                    @QueryParam("id_payment_state") Long id_payment_state,
                    @QueryParam("id_bill_state") Long id_bill_state,
                    @QueryParam("id_bill_type") Long id_bill_type,
                    @QueryParam("consecutive") String consecutive,
                    @QueryParam("purchase_date") Date purchase_date,
                    @QueryParam("subtotal") Double subtotal,
                    @QueryParam("tax") Double tax,
                    @QueryParam("discount") Double discount,
                    @QueryParam("totalprice") Double totalprice,

                    @QueryParam("id_state_bill") Long id_state_bill,
                    @QueryParam("state_bill") Integer state_bill,
                    @QueryParam("creation_bill") Date creation_bill,
                    @QueryParam("update_bill") Date update_bill
    ){

        Response response;

        Either<IException, List<Bill>> getResponseGeneric = billBusiness.getBill(
                new Bill(id_bill,  id_bill_father, id_third_employee, id_third,consecutive,
                        purchase_date,subtotal,totalprice,tax,discount,id_payment_state,id_bill_state,id_bill_type,
                        new CommonState(id_state_bill, state_bill,
                                creation_bill, update_bill)
                )
        );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/details")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getBillDetailsResource(@QueryParam("id_bill") Long id_bill,
                                            @QueryParam("id_bill_father") Long id_bill_father,
                                            @QueryParam("id_third_employee") Long id_third_employee,
                                            @QueryParam("id_third") Long id_third,
                                            @QueryParam("id_payment_state") Long id_payment_state,
                                            @QueryParam("id_bill_state") Long id_bill_state,
                                            @QueryParam("id_bill_type") Long id_bill_type,
                                            @QueryParam("consecutive") String consecutive,
                                            @QueryParam("purchase_date") Date purchase_date,
                                            @QueryParam("subtotal") Double subtotal,
                                            @QueryParam("tax") Double tax,
                                            @QueryParam("totalprice") Double totalprice,
                                           @QueryParam("discount") Double discount,

                                            @QueryParam("id_state_bill") Long id_state_bill,
                                            @QueryParam("state_bill") Integer state_bill,
                                            @QueryParam("creation_bill") Date creation_bill,
                                            @QueryParam("update_bill") Date update_bill,


                                           @QueryParam("id_detail_bill") Long id_detail_bill,
                                           @QueryParam("id_product_third") Long id_product_third,
                                           @QueryParam("quantity") Integer quantity,
                                           @QueryParam("price") Double price,
                                           @QueryParam("tax_product") Double tax_product,


                                           @QueryParam("id_state_detail_bill") Long id_state_detail_bill,
                                           @QueryParam("state_detail_bill") Integer state_detail_bill,
                                           @QueryParam("creation_detail_bill") Date creation_detail_bill,
                                           @QueryParam("update_detail_bill") Date update_detail_bill,


                                           @QueryParam("id_detail_payment_bill") Long id_detail_payment_bill,
                                           @QueryParam("id_way_to_pay") Long id_way_to_pay,
                                           @QueryParam("id_payment_method") Long id_payment_method,

                                           @QueryParam("payment_value") Double payment_value,
                                           @QueryParam("approbation_code") String approbation_code,



                                           @QueryParam("id_state_detail_pay") Long id_state_det_pay,
                                           @QueryParam("state_detail_pay") Integer state_det_pay,
                                           @QueryParam("creation_detail_pay") Date creation_det_pay,
                                           @QueryParam("update_detail_pay") Date update_det_pay
    ){

        Response response;

        Either<IException, List<HashMap<String,Object>>> getResponseGeneric = billBusiness.getBillDetail(
                new Bill(id_bill,  id_bill_father, id_third_employee, id_third,consecutive,
                        purchase_date,subtotal,totalprice,tax,discount,id_payment_state,id_bill_state,id_bill_type,
                        new CommonState(id_state_bill, state_bill,
                                creation_bill, update_bill)
                ),
                new DetailBill(id_detail_bill,id_bill,id_product_third,quantity,price,tax_product,tax,
                        new CommonState(id_state_detail_bill, state_detail_bill,
                                creation_detail_bill, update_detail_bill)
                ),
                new DetailPaymentBill(id_detail_payment_bill,id_bill,id_way_to_pay,id_payment_method,payment_value,approbation_code,
                        new CommonState(id_state_det_pay, state_det_pay,
                                creation_det_pay, update_det_pay)
                )
        );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/completes")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getBillCompleteResource(@QueryParam("id_bill") Long id_bill,
                                            @QueryParam("id_bill_father") Long id_bill_father,
                                            @QueryParam("id_third_employee") Long id_third_employee,
                                            @QueryParam("id_third") Long id_third,
                                            @QueryParam("consecutive") String consecutive,
                                            @QueryParam("purchase_date") Date purchase_date,
                                            @QueryParam("subtotal") Double subtotal,
                                            @QueryParam("tax") Double tax,
                                            @QueryParam("totalprice") Double totalprice,
                                            @QueryParam("discount") Double discount,


                                            @QueryParam("id_payment_state") Long id_payment_state,
                                            @QueryParam("name_payment_state") String name_payment_state,
                                            @QueryParam("id_state_payment_state") Long id_state_payment_state,
                                            @QueryParam("state_payment_state") Integer state_product,
                                            @QueryParam("creation_payment_state") Date creation_payment_state,
                                            @QueryParam("update_payment_state") Date update_payment_state,

                                            @QueryParam("id_bill_state") Long id_bill_state,
                                            @QueryParam("name_bill_state") String name_bill_state,

                                            @QueryParam("id_state_bill_state") Long id_state_bill_state,
                                            @QueryParam("state_bill_state") Integer state_bill_state,
                                            @QueryParam("creation_bill_state") Date creation_bill_state,
                                            @QueryParam("update_bill_state") Date update_bill_state,

                                            @QueryParam("id_bill_type") Long id_bill_type,
                                            @QueryParam("name_bill_type") String name_bill_type,

                                            @QueryParam("id_state_bill_type") Long id_state_bill_type,
                                            @QueryParam("state_bill_type") Integer state_bill_type,
                                            @QueryParam("creation_bill_type") Date creation_bill_type,
                                            @QueryParam("update_bill_type") Date update_bill_type,


                                            @QueryParam("id_state_bill") Long id_state_bill,
                                            @QueryParam("state_bill") Integer state_bill,
                                            @QueryParam("creation_bill") Date creation_bill,
                                            @QueryParam("creation_bill") Date update_bill){

        Response response;

        Either<IException, List<BillComplete>> getResponseGeneric = billBusiness.getBillComplete(
                new BillComplete(
                        id_bill,id_bill_father,id_third_employee,id_third,consecutive,purchase_date,subtotal,totalprice,tax,discount,
                        new PaymentState(id_payment_state, name_payment_state,
                                new CommonState(id_state_payment_state, state_product,
                                        creation_payment_state, update_payment_state)
                        ),
                        new BillState(id_bill_state, name_bill_state,
                                new CommonState(id_state_bill_state, state_bill_state,
                                        creation_bill_state, update_bill_state)
                        ),
                        new BillType(id_bill_type, name_bill_type,
                                new CommonState(id_state_bill_type, state_bill_type,
                                        creation_bill_type, update_bill_type)
                        ),
                        new CommonState(id_state_bill, state_bill,
                                creation_bill, update_bill)
                        )

                );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/completes/details")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getBillCompleteDeatilsResource(@QueryParam("id_bill") Long id_bill,
                                            @QueryParam("id_bill_father") Long id_bill_father,
                                            @QueryParam("id_third_employee") Long id_third_employee,
                                            @QueryParam("id_third") Long id_third,
                                            @QueryParam("consecutive") String consecutive,
                                            @QueryParam("purchase_date") Date purchase_date,
                                            @QueryParam("subtotal") Double subtotal,
                                            @QueryParam("tax") Double tax,
                                            @QueryParam("totalprice") Double totalprice,
                                            @QueryParam("discount") Double discount,


                                            @QueryParam("id_payment_state") Long id_payment_state,
                                            @QueryParam("name_payment_state") String name_payment_state,
                                            @QueryParam("id_state_payment_state") Long id_state_payment_state,
                                            @QueryParam("state_payment_state") Integer state_product,
                                            @QueryParam("creation_payment_state") Date creation_payment_state,
                                            @QueryParam("update_payment_state") Date update_payment_state,

                                            @QueryParam("id_bill_state") Long id_bill_state,
                                            @QueryParam("name_bill_state") String name_bill_state,

                                            @QueryParam("id_state_bill_state") Long id_state_bill_state,
                                            @QueryParam("state_bill_state") Integer state_bill_state,
                                            @QueryParam("creation_bill_state") Date creation_bill_state,
                                            @QueryParam("update_bill_state") Date update_bill_state,

                                            @QueryParam("id_bill_type") Long id_bill_type,
                                            @QueryParam("name_bill_type") String name_bill_type,

                                            @QueryParam("id_state_bill_type") Long id_state_bill_type,
                                            @QueryParam("state_bill_type") Integer state_bill_type,
                                            @QueryParam("creation_bill_type") Date creation_bill_type,
                                            @QueryParam("update_bill_type") Date update_bill_type,


                                            @QueryParam("id_state_bill") Long id_state_bill,
                                            @QueryParam("state_bill") Integer state_bill,
                                            @QueryParam("creation_bill") Date creation_bill,
                                            @QueryParam("creation_bill") Date update_bill,





                                           @QueryParam("id_detail_bill") Long id_detail_bill,
                                           @QueryParam("id_product_third") Long id_product_third,
                                           @QueryParam("quantity") Integer quantity,
                                           @QueryParam("price") Double price,
                                           @QueryParam("tax_product") Double tax_product,


                                           @QueryParam("id_state_detail_bill") Long id_state_detail_bill,
                                           @QueryParam("state_detail_bill") Integer state_detail_bill,
                                           @QueryParam("creation_detail_bill") Date creation_detail_bill,
                                           @QueryParam("update_detail_bill") Date update_detail_bill,



                                           @QueryParam("id_detail_payment_bill") Long id_detail_payment_bill,
                                           @QueryParam("payment_value") Double payment_value,
                                           @QueryParam("aprobation_code") String aprobation_code,

                                           @QueryParam("id_payment_method") Long id_payment_method,
                                           @QueryParam("name_payment_method") String name_payment_method,
                                           @QueryParam("id_state_payment_method") Long id_state_payment_method,
                                           @QueryParam("state_payment_method") Integer state_payment_method,
                                           @QueryParam("creation_payment_method") Date creation_payment_method,
                                           @QueryParam("update_payment_method") Date update_payment_method,


                                           @QueryParam("id_way_pay") Long id_way_pay,
                                           @QueryParam("name_way_pay") String name_way_pay,

                                           @QueryParam("id_state_way_pay") Long id_state_way_pay,
                                           @QueryParam("state_way_pay") Integer state_way_pay,
                                           @QueryParam("creation_way_pay") Date creation_way_pay,
                                           @QueryParam("update_way_pay") Date update_way_pay,



                                           @QueryParam("id_state_det_pay") Long id_state_det_pay,
                                           @QueryParam("state_det_pay") Integer state_det_pay,
                                           @QueryParam("creation_det_pay") Date creation_det_pay,
                                           @QueryParam("update_det_pay") Date update_det_pay


                                                   ){

        Response response;

        Either<IException, List<HashMap<String,Object>>> getResponseGeneric = billBusiness.getBillCompleteDetail(
                new BillComplete(
                        id_bill,id_bill_father,id_third_employee,id_third,consecutive,purchase_date,subtotal,totalprice,tax,discount,
                        new PaymentState(id_payment_state, name_payment_state,
                                new CommonState(id_state_payment_state, state_product,
                                        creation_payment_state, update_payment_state)
                        ),
                        new BillState(id_bill_state, name_bill_state,
                                new CommonState(id_state_bill_state, state_bill_state,
                                        creation_bill_state, update_bill_state)
                        ),
                        new BillType(id_bill_type, name_bill_type,
                                new CommonState(id_state_bill_type, state_bill_type,
                                        creation_bill_type, update_bill_type)
                        ),
                        new CommonState(id_state_bill, state_bill,
                                creation_bill, update_bill)
                ),

                new DetailBill(id_detail_bill,id_bill,id_product_third,quantity,price,tax_product,tax,
                        new CommonState(id_state_detail_bill, state_detail_bill,
                                creation_detail_bill, update_detail_bill)
                ),
                new DetailPaymentBillComplete(id_detail_payment_bill,payment_value,id_bill,aprobation_code,
                        new PaymentMethod(id_payment_method, name_payment_method,
                                new CommonState(id_state_payment_method, state_payment_method,
                                        creation_payment_method, update_payment_method)
                        ),
                        new WayToPay(id_way_pay, name_way_pay,
                                new CommonState(id_state_way_pay, state_way_pay,
                                        creation_way_pay, update_way_pay)
                        ),
                        new CommonState(id_state_det_pay, state_det_pay,
                                creation_det_pay, update_det_pay)
                )
        );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postBillResource(BillDTO billDTO, @QueryParam("id_caja") Long id_caja){
        Response response;
        //System.out.println("console testing: "+billDTO.getid_third_destiny());
        Either<IException, Long> getResponseGeneric = billBusiness.creatBill(billDTO, id_caja);


        if (getResponseGeneric.isRight()){

            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @POST
    @Path("/procedureup2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response procedureup2(@QueryParam("idbill") Long idbill){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.procedure2(idbill)).build();

        return response;

    }


    @POST
    @Path("/deleteBill")
    @Timed
    @RolesAllowed({"Auth"})
    public Response deleteBillPro(@QueryParam("id_caja") Long id_caja){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.deleteBillPro(id_caja)).build();

        return response;

    }



    @POST
    @Path("/validateBill")
    @Timed
    @RolesAllowed({"Auth"})
    public Response procedureValidarBill(@QueryParam("idbill") Long idbill){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.procedureValidarBill(idbill)).build();

        return response;

    }


    @POST
    @Path("/crearPedidoMesa")
    @Timed
    @RolesAllowed({"Auth"})
    public Response crearPedidoMesaV2(@QueryParam("idstoreclient") Long idstoreclient,
                                      @QueryParam("idthirduseraapp") Long idthirduseraapp,
                                      @QueryParam("idstoreprov") Long idstoreprov,
                                      @QueryParam("detallepedido") String detallepedido,
                                      @QueryParam("descuento") Long descuento,
                                      @QueryParam("idpaymentmethod") Long idpaymentmethod,
                                      @QueryParam("idapplication") Long idapplication,
                                      @QueryParam("idthirdemp") Long idthirdemp,
                                      @QueryParam("detallepedidomesa") String detallepedidomesa,
                                      @QueryParam("idmesa") Long idmesa){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.crearPedidoMesaV2(idstoreclient,
                idthirduseraapp,
                idstoreprov,
                detallepedido,
                descuento,
                idpaymentmethod,
                idapplication,
                idthirdemp,
                detallepedidomesa,
                idmesa)).build();

        return response;

    }



    @POST
    @Path("/crearDetallePedidoMesa")
    @Timed
    @RolesAllowed({"Auth"})
    public Response crearDetallePedidoMesa(@QueryParam("idbprov") Long idbprov,
                                           @QueryParam("detallepedidomesa") String detallepedidomesa){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.crearDetallePedidoMesa(idbprov,
                detallepedidomesa)).build();

        return response;

    }


    @POST
    @Path("/actualizarEstadoMesa")
    @Timed
    @RolesAllowed({"Auth"})
    public Response actualizarEstadoMesa(@QueryParam("IDBILL") Long IDBILL,
                                         @QueryParam("IDESTADODESTINO") Long IDESTADODESTINO,
                                         @QueryParam("nota") String nota,
                                         @QueryParam("IDESTADOORIGEN") String IDESTADOORIGEN,
                                         @QueryParam("IDTHIRDUSER") Long IDTHIRDUSER,
                                         @QueryParam("ACTOR") String ACTOR){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.actualizarEstadoMesa(IDBILL,
                IDESTADODESTINO,
                nota,
                IDESTADOORIGEN,
                IDTHIRDUSER,
                ACTOR)).build();

        return response;

    }



    @POST
    @Path("/agregarProductosMesa")
    @Timed
    @RolesAllowed({"Auth"})
    public Response agregarProductosMesa(@QueryParam("idbill") Long idbill,
                                         @QueryParam("detallepedido") String detallepedido){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.agregarProductosMesa(idbill,
                detallepedido)).build();

        return response;

    }


    @POST
    @Path("/agregarProductosMesaOpciones")
    @Timed
    @RolesAllowed({"Auth"})
    public Response agregarProductosMesaOpciones(@QueryParam("idbill") Long idbill,
                                         @QueryParam("detallepedido") String detallepedido,
                                                 @QueryParam("detallepedidomesa") String detallepedidomesa){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.agregarProductosMesaOpciones(idbill,
                detallepedido,detallepedidomesa)).build();

        return response;

    }


    @POST
    @Path("/actualizarEstadoPedidoMesa")
    @Timed
    @RolesAllowed({"Auth"})
    public Response actualizar_estado_pedido_mesa(@QueryParam("ID_DETALLE_DETAIL_BILL") Long ID_DETALLE_DETAIL_BILL,
                                                  @QueryParam("IDESTADODESTINO") Long IDESTADODESTINO,
                                                  @QueryParam("notas") String notas,
                                                  @QueryParam("IDESTADOORIGEN") Long IDESTADOORIGEN,
                                                  @QueryParam("IDTHIRDUSER") Long IDTHIRDUSER,
                                                  @QueryParam("ACTOR") String ACTOR){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.actualizar_estado_pedido_mesa(ID_DETALLE_DETAIL_BILL,
                IDESTADODESTINO,
                notas,
                IDESTADOORIGEN,
                IDTHIRDUSER,
                ACTOR)).build();

        return response;

    }


    @POST
    @Path("/actualizarEstadoPedidoMesa/list")
    @Timed
    @RolesAllowed({"Auth"})
    public Response actualizar_estado_pedido_mesa_list(listStateDTO listDTO){
        Response response;

        List<StateChangeDTO> lista = listDTO.getList();

        for(StateChangeDTO item : lista){
            billBusiness.actualizar_estado_pedido_mesa(item.getID_DETALLE_DETAIL_BILL(),item.getIDESTADODESTINO(),item.getNOTAS(),item.getIDESTADOORIGEN(),item.getIDTHIRDUSER(),item.getACTOR());
        }

        response=Response.status(Response.Status.OK).entity(true).build();

        return response;

    }


    @POST
    @Path("/asignarMeseroPedido")
    @Timed
    @RolesAllowed({"Auth"})
    public Response asignar_mesero_pedido(@QueryParam("idbprov") Long idbprov,
                                           @QueryParam("id_third_mesero") Long id_third_mesero){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.asignar_mesero_pedido(idbprov,
                id_third_mesero)).build();

        return response;

    }


    @POST
    @Path("/createPaymentDetail")
    @Timed
    @RolesAllowed({"Auth"})
    public Response procedureCrearPaymentDetail(@QueryParam("idbill") Long idbill){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.procedureCrearPaymentDetail(idbill)).build();

        return response;

    }


    @POST
    @Path("/facturarPedidoMesa")
    @Timed
    @RolesAllowed({"Auth"})
    public Response facturarPedidoMesa(@QueryParam("idthirdemployee") Long idthirdemployee,
                                       @QueryParam("idthird") Long idthird,
                                       @QueryParam("idbilltype") Long idbilltype,
                                       @QueryParam("notes") String notes,
                                       @QueryParam("idthirddestinity") Long idthirddestinity,
                                       @QueryParam("idcaja") Long idcaja,
                                       @QueryParam("idstore") Long idstore,
                                       @QueryParam("idthirddomiciliario") Long idthirddomiciliario,
                                       @QueryParam("idpaymentmethod") Long idpaymentmethod,
                                       @QueryParam("idwaytopay") Long idwaytopay,
                                       @QueryParam("approvalcode") String approvalcode,
                                       @QueryParam("idbankentity") Long idbankentity,
                                       @QueryParam("idbillstate") Long idbillstate,
                                       @QueryParam("detallesbill") String detallesbill,
                                       @QueryParam("descuento") Long descuento,
                                       @QueryParam("numdocumentoadicional") String numdocumentoadicional,
                                       @QueryParam("idthirdvendedor") Long idthirdvendedor,
                                       @QueryParam("detallespago") String detallespago,
                                       @QueryParam("idbillpedido") Long idbillpedido,
                                       @QueryParam("nota") String nota,
                                       @QueryParam("idthirduser") Long idthirduser,
                                       @QueryParam("actor") String actor){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.facturarPedidoMesa(idthirdemployee,
                idthird,
                idbilltype,
                notes,
                idthirddestinity,
                idcaja,
                idstore,
                idthirddomiciliario,
                idpaymentmethod,
                idwaytopay,
                approvalcode,
                idbankentity,
                idbillstate,
                detallesbill,
                descuento,
                numdocumentoadicional,
                idthirdvendedor,
                detallespago,
                idbillpedido,
                nota,
                idthirduser,
                actor)).build();

        return response;

    }


    @Path("/{id}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateBillResource(@PathParam("id") Long id_bill, BillDetailIDDTO billDetailIDDTO){
        Response response;
        Either<IException, Long> getResponseGeneric = billBusiness.updateBill(id_bill, billDetailIDDTO);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deleteBillResource(@PathParam("id") Long id_bill){
        Response response;
        Either<IException, Long> getResponseGeneric = billBusiness.deleteBill(id_bill);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @POST
    @Path("/pdf")
    @Timed
    @RolesAllowed({"Auth"})
    public Response printPDF(BillPdfDTO billpdfDTO) {
        Response response;
        Either<IException, String> mailEither =Either.right("NOPDF!!!");
        if(billpdfDTO.getpdfSize()==1){
            mailEither = billBusiness.printPDF(billpdfDTO);}
        if(billpdfDTO.getpdfSize()==2){
            mailEither = billBusiness.printPDF2(billpdfDTO,0,new Long(0), new Long(0),new Long(0));}



        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @POST
    @Path("/Test")
    @Timed
    @RolesAllowed({"Auth"})
    public Response TestPDF(ReporteFinanciero1DTO elDTO){
        Response response;
        Either<IException, String> mailEither;
        //System.out.println("Ejecuto el TestPDF");
        ///Elimino cuentas que no empiezan por 1 o 2 o 3
        List<Map<String,String>> FilasTemp = new ArrayList<>();
        for(int n = 0;n<elDTO.getFilas().size();n++){
            Map<String,String> Item = elDTO.getFilas().get(n);
            if(Item.get("codigo_CUENTA").charAt(0) == '1' || Item.get("codigo_CUENTA").charAt(0) == '2' || Item.get("codigo_CUENTA").charAt(0) == '3'){
                FilasTemp.add(Item);
            }
        }
        elDTO.setFilas(FilasTemp);
        ///
        mailEither = billBusiness.TestPDF(elDTO);
        //ANALIZO EL RESULTADO
        if (mailEither.isRight()){ response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build(); }
        else { response= ExceptionResponse.createErrorResponse(mailEither); }
        return response;
    }

    @GET
    @Path("/UniversalPDF")
    @Timed
    @RolesAllowed({"Auth"})
    public Response UniversalPDF(@QueryParam("id_bill") Long id_bill, @QueryParam("pdf") Long pdf,@QueryParam("cash") Long cash, @QueryParam("restflag") Long restflag, @QueryParam("size") Boolean size
    ) throws NoSuchAlgorithmException, KeyManagementException, IOException, KeyStoreException, CertificateException {
        System.out.println(size);
        //System.out.println("Iniciando UniversalPDF");
        /*Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().create();
        //System.out.println("billmaster es");
        //System.out.println(gson.toJson(billmaster));*/
        Response response;
        Either<IException, String> mailEither = Either.right("NOPDF!!!");

        //////////////////////////////////////////////
        ///////// OBTENGO EL BILL MASTER /////////////
        //////////////////////////////////////////////
        //System.out.println("OBTENGO EL BILL MASTER");
        long id_store = billBusiness.getIdStoreFromIdBill(id_bill);
        BillMaster billmaster = billBusiness.getMaestroFactura(id_bill, id_store).right().value();

        Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().create();
        //System.out.println("billmaster es");
        //System.out.println(gson.toJson(billmaster));

        ///////////////////////////////////////////////////
        ///////// OBTENGO LA FACTURA COMPLETA /////////////
        ///////////////////////////////////////////////////
        //System.out.println("OBTENGO LA FACTURA COMPLETA");
        BillRaw La_Bill = billBusiness.getRawBill(id_bill).right().value();
        if(id_store==161 && La_Bill.getID_BILL_TYPE() == 1 && !(La_Bill.getNUM_DOCUMENTO_CLIENTE() != null) && !(La_Bill.getID_STORE_CLIENT()>0)){
            La_Bill.setNUM_DOCUMENTO_CLIENTE(" ");
            La_Bill.setID_STORE_CLIENT(La_Bill.getID_STORE());
        }

        ///////////////////////////////////////////////////
        //////// OBTENGO LA INFO LEGAL DEL TERCERO ////////
        ///////////////////////////////////////////////////
        //System.out.println("OBTENGO LA INFO LEGAL DEL TERCERO");
        List<LegalData> billlegaldata = billBusiness.getLegalData(La_Bill.getID_THIRD()).right().value();

        ////////////////////////////////////////////////////
        //////// OBTENGO LOS DETALLES DE LA FACTURA ////////
        ////////////////////////////////////////////////////
        //System.out.println("OBTENGO LOS DETALLES DE LA FACTURA");
        List<List<String>> billdetails = billBusiness.getDetalleFactura(id_bill).right().value();

        /////////////////////////////////////////////////////////////
        //////// OBTENGO EL BODY DEL DOCUMENTO DE LA FACTURA ////////
        /////////////////////////////////////////////////////////////
        //System.out.println("OBTENGO EL BODY DEL DOCUMENTO DE LA FACTURA");
        String body_Document = billBusiness.getBodyDocumentBill(id_bill);

        /////////////////////////////////////////////////
        //////// OBTENGO LOS DATOS SOBRE EL PAGO ////////
        /////////////////////////////////////////////////
        //System.out.println("OBTENGO LOS DATOS SOBRE EL PAGO");
        RawDetailPaymentBill detailPaymentRaw = billBusiness.getRawDetailPaymentFromIdBill(id_bill).right().value();

        ////////////////////////////////////
        //////// LO DE AUTORIZACIÓN ////////
        ////////////////////////////////////
        String autorizacion = "";
        try {
            if(id_store==141){ autorizacion = "18763000168739 Autorización: Desde CD1 a CD99999"; }
            else if(id_store==142){ autorizacion = ""; }
            else if(id_store==143){ autorizacion = "18763000168739 Autorización: Desde VL1 a VL99999"; }
            else if(id_store==221){ autorizacion = "Autorización: Desde SJ2 a SJ99999"; }
            else{ autorizacion = billlegaldata.get(0).getRESOLUCION_DIAN(); }
            //System.out.println("Consulto autorizacion "+autorizacion);
        }catch (Exception e){
            e.printStackTrace();
            autorizacion = "";
            billlegaldata = new ArrayList<>();
            billlegaldata.add(new LegalData("","","","","","","","","","","","","","",""));
        }

        /////////////////////////////////////////
        //////// OBTENGO TODO LO TERCERO ////////
        ////////////////////////////////////////

        //Datos del empleado cajero
        //System.out.println("Consulto Datos del empleado cajero");
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.add("Key_Server", "3020D4:1FG-2U113E822-A1TE04529-68CF");
        headers.add("Authorization", "3020D4:0DD-2F413E82B-A1EF04559-78CA");
        //
        KeyStore trustStore = KeyStore.getInstance("jks");
        FileInputStream fin;
        String urlbase;
        if(System.getenv("SYSWARE_SERVER").equals("prod")){
            fin = new FileInputStream(new File(".").getCanonicalPath()+"/ssl.jks");
            trustStore.load(fin, "Jkbotero2020$$".toCharArray());
            urlbase = "https://tienda724.com:8446/v1/";
        }else{
            try{
                if(System.getenv("SYSWARE_DEVPC").equals("yes")){
                    fin = new FileInputStream(new File(".").getCanonicalPath()+"/localhost.jks");
                    trustStore.load(fin, "lel123".toCharArray());
                }
            }catch (Exception e){
                fin = new FileInputStream(new File(".").getCanonicalPath()+"/localhost.jks");
                trustStore.load(fin, "Jkbotero2020$$".toCharArray());
            }
            urlbase = "https://pruebas.tienda724.com:8446/v1/";
        }
        //
        String json = ClientBuilder.newBuilder().trustStore(trustStore).build().target(urlbase+"thirds/getBasicThirdInfo?id_third="+La_Bill.getID_THIRD_EMPLOYEE()).request().headers(headers).get(String.class);
        //System.out.println("json es ");
        //System.out.println(json);
        Map<String, Object> DatosCajero = null;
        boolean fallo = false;
        try { DatosCajero = new ObjectMapper().readValue(json , new TypeReference<Map<String, Object>>(){}); }
        catch (JsonMappingException e) { System.out.print("[JsonMappingException-Error Controlado] No pudo mapear el json"); fallo = true;}
        catch (IOException e) { System.out.print("[IOException] "); e.printStackTrace();  fallo = true;}
        if(fallo){//En caso de fallar, POSIBLEMENTE, es cliente ocasional
            DatosCajero = new HashMap<>();
            DatosCajero.put("document_NUMBER","-1");
            DatosCajero.put("fullname","-1");
            DatosCajero.put("phone","-1");
            DatosCajero.put("document_TYPE","-1");
            DatosCajero.put("totalventas","-1");
            DatosCajero.put("mail","-1");
            DatosCajero.put("address","-1");
        }

        //Datos del cliente
        fallo = false;
        //System.out.println("Consulto Datos del cliente");
        json = ClientBuilder.newBuilder().trustStore(trustStore).build().target(urlbase+"thirds/getBasicThirdInfo?id_third="+La_Bill.getID_THIRD_DESTINITY()).request().headers(headers).get(String.class);
        Map<String, Object> DatosCliente = null;
        try { DatosCliente = new ObjectMapper().readValue(json , new TypeReference<Map<String, Object>>(){}); }
        catch (JsonMappingException e) { System.out.print("[JsonMappingException-Error Controlado] No pudo mapear el json");fallo = true;}
        catch (IOException e) { System.out.print("[IOException] "); e.printStackTrace();  fallo = true;}
        //System.out.println(json);
        if(fallo){
            DatosCliente = new HashMap<>();
            DatosCliente.put("document_NUMBER","");
            DatosCliente.put("fullname","Cliente Ocasional");
            DatosCliente.put("phone","");
            DatosCliente.put("document_TYPE","");
            DatosCliente.put("totalventas","");
            DatosCliente.put("mail","");
            DatosCliente.put("address","");
        }

        ///////////////////////////////////
        //////// EJECUTO EL METODO ////////
        ///////////////////////////////////
        //System.out.println("Ejecuto el UniversalPDF");
        if(pdf == null){pdf = -1L;}
        mailEither = billBusiness.UniversalPDF(billmaster,La_Bill, billlegaldata.get(0), billdetails,body_Document,autorizacion,detailPaymentRaw,DatosCajero,DatosCliente,pdf,cash,restflag,size);
        //ANALIZO EL RESULTADO
        if (mailEither.isRight()){ response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build(); }
        else { response= ExceptionResponse.createErrorResponse(mailEither); }
        return response;
    }


    @GET
    @Path("/PrintPdfTable")
    @Timed
    @RolesAllowed({"Auth"})
    public Response PrintPdfTable(@QueryParam("id_bill") Long id_bill, @QueryParam("pdf") Long pdf,@QueryParam("cash") Long cash, @QueryParam("restflag") Long restflag, @QueryParam("mesa") String mesa
    ) throws NoSuchAlgorithmException, KeyManagementException, IOException, KeyStoreException, CertificateException {
        //System.out.println("Iniciando UniversalPDF");
        /*Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().create();
        //System.out.println("billmaster es");
        //System.out.println(gson.toJson(billmaster));*/
        Response response;
        Either<IException, String> mailEither = Either.right("NOPDF!!!");

        //////////////////////////////////////////////
        ///////// OBTENGO EL BILL MASTER /////////////
        //////////////////////////////////////////////
        //System.out.println("OBTENGO EL BILL MASTER");
        long id_store = billBusiness.getIdStoreFromIdBill(id_bill);
        BillMaster billmaster = billBusiness.getMaestroFactura(id_bill, id_store).right().value();

        Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().create();
        //System.out.println("billmaster es");
        //System.out.println(gson.toJson(billmaster));

        ///////////////////////////////////////////////////
        ///////// OBTENGO LA FACTURA COMPLETA /////////////
        ///////////////////////////////////////////////////
        //System.out.println("OBTENGO LA FACTURA COMPLETA");
        BillRaw La_Bill = billBusiness.getRawBill(id_bill).right().value();
        if(id_store==161 && La_Bill.getID_BILL_TYPE() == 1 && !(La_Bill.getNUM_DOCUMENTO_CLIENTE() != null) && !(La_Bill.getID_STORE_CLIENT()>0)){
            La_Bill.setNUM_DOCUMENTO_CLIENTE(" ");
            La_Bill.setID_STORE_CLIENT(La_Bill.getID_STORE());
        }

        ///////////////////////////////////////////////////
        //////// OBTENGO LA INFO LEGAL DEL TERCERO ////////
        ///////////////////////////////////////////////////
        //System.out.println("OBTENGO LA INFO LEGAL DEL TERCERO");
        List<LegalData> billlegaldata = billBusiness.getLegalData(La_Bill.getID_THIRD()).right().value();

        ////////////////////////////////////////////////////
        //////// OBTENGO LOS DETALLES DE LA FACTURA ////////
        ////////////////////////////////////////////////////
        //System.out.println("OBTENGO LOS DETALLES DE LA FACTURA");
        List<List<String>> billdetails = billBusiness.getDetalleFactura(id_bill).right().value();

        /////////////////////////////////////////////////////////////
        //////// OBTENGO EL BODY DEL DOCUMENTO DE LA FACTURA ////////
        /////////////////////////////////////////////////////////////
        //System.out.println("OBTENGO EL BODY DEL DOCUMENTO DE LA FACTURA");
        String body_Document = billBusiness.getBodyDocumentBill(id_bill);

        /////////////////////////////////////////////////
        //////// OBTENGO LOS DATOS SOBRE EL PAGO ////////
        /////////////////////////////////////////////////
        //System.out.println("OBTENGO LOS DATOS SOBRE EL PAGO");
        RawDetailPaymentBill detailPaymentRaw = billBusiness.getRawDetailPaymentFromIdBill(id_bill).right().value();

        ////////////////////////////////////
        //////// LO DE AUTORIZACIÓN ////////
        ////////////////////////////////////
        String autorizacion = "";
        try {
            if(id_store==141){ autorizacion = "18763000168739 Autorización: Desde CD1 a CD99999"; }
            else if(id_store==142){ autorizacion = ""; }
            else if(id_store==143){ autorizacion = "18763000168739 Autorización: Desde VL1 a VL99999"; }
            else if(id_store==221){ autorizacion = "Autorización: Desde SJ2 a SJ99999"; }
            else{ autorizacion = billlegaldata.get(0).getRESOLUCION_DIAN(); }
            //System.out.println("Consulto autorizacion "+autorizacion);
        }catch (Exception e){
            e.printStackTrace();
            autorizacion = "";
            billlegaldata = new ArrayList<>();
            billlegaldata.add(new LegalData("","","","","","","","","","","","","","",""));
        }

        /////////////////////////////////////////
        //////// OBTENGO TODO LO TERCERO ////////
        ////////////////////////////////////////

        //Datos del empleado cajero
        //System.out.println("Consulto Datos del empleado cajero");
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
        headers.add("Key_Server", "3020D4:1FG-2U113E822-A1TE04529-68CF");
        headers.add("Authorization", "3020D4:0DD-2F413E82B-A1EF04559-78CA");
        //
        KeyStore trustStore = KeyStore.getInstance("jks");
        FileInputStream fin;
        String urlbase;
        if(System.getenv("SYSWARE_SERVER").equals("prod")){
            fin = new FileInputStream(new File(".").getCanonicalPath()+"/ssl.jks");
            trustStore.load(fin, "Jkbotero2020$$".toCharArray());
            urlbase = "https://tienda724.com:8446/v1/";
        }else{
            try{
                if(System.getenv("SYSWARE_DEVPC").equals("yes")){
                    fin = new FileInputStream(new File(".").getCanonicalPath()+"/localhost.jks");
                    trustStore.load(fin, "lel123".toCharArray());
                }
            }catch (Exception e){
                fin = new FileInputStream(new File(".").getCanonicalPath()+"/localhost.jks");
                trustStore.load(fin, "Jkbotero2020$$".toCharArray());
            }
            urlbase = "https://pruebas.tienda724.com:8446/v1/";
        }
        //
        String json = ClientBuilder.newBuilder().trustStore(trustStore).build().target(urlbase+"thirds/getBasicThirdInfo?id_third="+La_Bill.getID_THIRD_EMPLOYEE()).request().headers(headers).get(String.class);
        //System.out.println("json es ");
        //System.out.println(json);
        Map<String, Object> DatosCajero = null;
        boolean fallo = false;
        try { DatosCajero = new ObjectMapper().readValue(json , new TypeReference<Map<String, Object>>(){}); }
        catch (JsonMappingException e) { System.out.print("[JsonMappingException-Error Controlado] No pudo mapear el json"); fallo = true;}
        catch (IOException e) { System.out.print("[IOException] "); e.printStackTrace();  fallo = true;}
        if(fallo){//En caso de fallar, POSIBLEMENTE, es cliente ocasional
            DatosCajero = new HashMap<>();
            DatosCajero.put("document_NUMBER","-1");
            DatosCajero.put("fullname","-1");
            DatosCajero.put("phone","-1");
            DatosCajero.put("document_TYPE","-1");
            DatosCajero.put("totalventas","-1");
            DatosCajero.put("mail","-1");
            DatosCajero.put("address","-1");
        }

        //Datos del cliente
        fallo = false;
        //System.out.println("Consulto Datos del cliente");
        json = ClientBuilder.newBuilder().trustStore(trustStore).build().target(urlbase+"thirds/getBasicThirdInfo?id_third="+La_Bill.getID_THIRD_DESTINITY()).request().headers(headers).get(String.class);
        Map<String, Object> DatosCliente = null;
        try { DatosCliente = new ObjectMapper().readValue(json , new TypeReference<Map<String, Object>>(){}); }
        catch (JsonMappingException e) { System.out.print("[JsonMappingException-Error Controlado] No pudo mapear el json");fallo = true;}
        catch (IOException e) { System.out.print("[IOException] "); e.printStackTrace();  fallo = true;}
        //System.out.println(json);
        if(fallo){
            DatosCliente = new HashMap<>();
            DatosCliente.put("document_NUMBER","");
            DatosCliente.put("fullname","Cliente Ocasional");
            DatosCliente.put("phone","");
            DatosCliente.put("document_TYPE","");
            DatosCliente.put("totalventas","");
            DatosCliente.put("mail","");
            DatosCliente.put("address","");
        }

        ///////////////////////////////////
        //////// EJECUTO EL METODO ////////
        ///////////////////////////////////
        //System.out.println("Ejecuto el UniversalPDF");
        if(pdf == null){pdf = -1L;}
        mailEither = billBusiness.PrintPdfTable(billmaster,La_Bill, billlegaldata.get(0), billdetails,body_Document,autorizacion,detailPaymentRaw,DatosCajero,DatosCliente,pdf,cash,restflag,mesa);
        //ANALIZO EL RESULTADO
        if (mailEither.isRight()){ response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build(); }
        else { response= ExceptionResponse.createErrorResponse(mailEither); }
        return response;
    }



    @GET
    @Path("/master")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getMaestroFactura(@QueryParam("id_bill") Long id_bill,
                                    @QueryParam("id_store") Long id_store){

        Response response;

        Either<IException, BillMaster> getResponseGeneric = billBusiness.getMaestroFactura(id_bill,id_store);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/domiciliarios")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDomi(@QueryParam("id_store") Long id_store){

        Response response;

        Either<IException, List<Domicilio>> getResponseGeneric = billBusiness.getDomi(id_store);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/pedidos")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPedidosSelect(@QueryParam("id_store") Long id_store,
                                     @QueryParam("id_third") Long id_third){

        Response response;

        Either<IException, List<PedidosSelect>> getResponseGeneric = billBusiness.getPedidosSelect(id_store,id_third);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/master2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getMaestroFactura2(@QueryParam("id_bill") Long id_bill,
                                      @QueryParam("id_store") Long id_store){

        Response response;

        Either<IException, BillMaster2> getResponseGeneric = billBusiness.getMaestroFactura2(id_bill,id_store);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




    @GET
    @Path("/detail")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDetalleFactura(@QueryParam("id_bill") Long id_bill){

        Response response;

        Either<IException, List<List<String>>> getResponseGeneric = billBusiness.getDetalleFactura(id_bill);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/venta")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getSalesCaja(@QueryParam("id_cierre_caja") Long id_cierre_caja){

        Response response;

        Either<IException, Double> getResponseGeneric = billBusiness.getSalesCaja(id_cierre_caja);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @PUT
    @Path("/billCompra")
    @Timed
    @RolesAllowed({"Auth"})
    public Response putCompraBill(@QueryParam("id_bill") Long id_bill,
                                  @QueryParam("dato") String dato,
                                  @QueryParam("fecha") String fecha){

        Response response;

        @SuppressWarnings( "deprecation" )
        Either<IException, String> getResponseGeneric = billBusiness.putCompraBill(id_bill,dato,new Date(fecha));

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @PUT
    @Path("/domiciliario")
    @Timed
    @RolesAllowed({"Auth"})
    public Response putDomiciliario(@QueryParam("id_bill") Long id_bill,
                                  @QueryParam("id_third") Long id_third){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.putDomiciliario(id_bill,id_third);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/getTiendasByThird")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTiendasByThird(@QueryParam("id_third") Long id_third){

        Response response;

        Either<IException, List<storesByThird>> getResponseGeneric = billBusiness.getTiendasByThird(id_third);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/legaldata")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getLegalData(@QueryParam("id_third") Long id_third){

        Response response;

        Either<IException, List<LegalData>> getResponseGeneric = billBusiness.getLegalData(id_third);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/top20")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTopProducts(@QueryParam("idstore") Long idstore){

        Response response;

        Either<IException, List<TopProds>> getResponseGeneric = billBusiness.getTopProducts(idstore);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/Vendors")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getVendorList(@QueryParam("idstore") Long idstore){

        Response response;

        Either<IException, List<Vendor>> getResponseGeneric = billBusiness.getVendorList(idstore);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/getProductosPedidoMesa")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getProductosPedidoMesa(@QueryParam("id_bill") Long id_bill){

        Response response;

        Either<IException, List<ProductosPedidosMesa>> getResponseGeneric = billBusiness.getProductosPedidoMesa(id_bill);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/getPedidosMesa")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPedidosMesa(@QueryParam("id_store") String id_store,
                                   @QueryParam("id_bill_type") Long id_bill_type,
                                   @QueryParam("id_bill_state") Long id_bill_state){

        Response response;

        Either<IException, List<PedidosMesa>> getResponseGeneric = billBusiness.getPedidosMesa(Arrays.asList(id_store.split(",")),id_bill_type,id_bill_state);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




    @GET
    @Path("/getProductosPedidoMesaADR")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getProductosPedidoMesaADR(@QueryParam("id_bill") Long id_bill,
                                              @QueryParam("id_third") Long idthird){

        Response response;

        Either<IException, List<ProductosPedidosMesa>> getResponseGeneric = billBusiness.getProductosPedidoMesaADR(id_bill,idthird);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/getPedidosMesaADR")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPedidosMesaADR(@QueryParam("id_store") String id_store,
                                   @QueryParam("id_bill_type") Long id_bill_type,
                                   @QueryParam("id_bill_state") Long id_bill_state,
                                      @QueryParam("id_third") Long idthird){

        Response response;

        Either<IException, List<PedidosMesa>> getResponseGeneric = billBusiness.getPedidosMesaADR(Arrays.asList(id_store.split(",")),id_bill_type,id_bill_state,idthird);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/cantidadFacturas")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCantFacturas(@QueryParam("id_caja") Long id_caja, @QueryParam("id_cierre_caja") Long id_cierre_caja){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.getCantFacturas(id_caja, id_cierre_caja);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }







    @GET
    @Path("/validateProductQuantity")
    @Timed
    @RolesAllowed({"Auth"})
    public Response validateProductQuantity(@QueryParam("idps") Long idps){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.validateProductQuantity(idps);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/validatePdfDrawing")
    @Timed
    @RolesAllowed({"Auth"})
    public Response validatePdfDrawing(@QueryParam("idbill") Long idbill){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.validatePdfDrawing(idbill);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/getMaxBillPedidos")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getMaxBillPedidos(@QueryParam("idstore") Long idstore,@QueryParam("idemp") Long idemp){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.getMaxBillPedidos(idstore,idemp);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @POST
    @Path("/insertPaymentDetail")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getMaxBillPedidos(PaymentDetailListDTO lista){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.insertPaymentDetail(lista);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @POST
    @Path("/registrar_primer_pago")
    @Timed
    @RolesAllowed({"Auth"})
    public Response registrar_primer_pago(@QueryParam("idBill") Long idBill,@QueryParam("valor") String valor){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.registrar_primer_pago(idBill,valor);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/getCanceledTable")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCanceledTable(@QueryParam("idBill") Long idBill){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.getCanceledTable(idBill);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @POST
    @Path("/agregar_servicio_mesa")
    @Timed
    @RolesAllowed({"Auth"})
    public Response agregar_servicio_mesa(@QueryParam("idbill") Long idbill,
                                          @QueryParam("barcode") Long barcode,
                                          @QueryParam("valor") Long valor,
                                          @QueryParam("idstore") Long idstore){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.agregar_servicio_mesa( idbill,
                barcode,
                valor,
                idstore);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @POST
    @Path("/eliminar_servicio_detailbill")
    @Timed
    @RolesAllowed({"Auth"})
    public Response eliminar_servicio_detailbill(@QueryParam("idbill") Long idbill){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.eliminar_servicio_detailbill( idbill);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @POST
    @Path("/cerrar_mesa")
    @Timed
    @RolesAllowed({"Auth"})
    public Response cerrar_mesa(@QueryParam("idbill") Long idbill){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.cerrar_mesa( idbill );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @POST
    @Path("/asociar_tercero_bill")
    @Timed
    @RolesAllowed({"Auth"})
    public Response agregar_servicio_mesa(@QueryParam("idthird") Long idthird,@QueryParam("idbill") Long idbill){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.asociar_tercero_bill( idthird, idbill);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/getIdCityByStore")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getIdCityByStore(@QueryParam("id_store") Long id_store){

        Response response;

        Either<IException, String> getResponseGeneric = billBusiness.getIdCityByStore( id_store );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/getPedidosMesaByIdMesa")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPedidosMesaByIdMesa(@QueryParam("id_store") Long id_store,
                                   @QueryParam("id_bill_type") Long id_bill_type,
                                   @QueryParam("id_bill_state") Long id_bill_state){

        Response response;

        Either<IException, PedidosMesa2> getResponseGeneric = billBusiness.getPedidosMesaByIdMesa(id_store,id_bill_type,id_bill_state);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/getValidacionMesa")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getValidacionMesa(@QueryParam("id_store") Long id_store){

        Response response;

        Either<IException, mesaState> getResponseGeneric = billBusiness.getValidacionMesa(id_store);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/registrarMesaToken")
    @Timed
    @RolesAllowed({"Auth"})
    public Response registrar_mesa_token(@QueryParam("idmesa") Long idmesa,@QueryParam("tok") String tok){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.registrar_mesa_token(idmesa,tok);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/getMesaAbierta")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getMesaAbierta(@QueryParam("idmesa") Long idmesa){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.getMesaAbierta(idmesa);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/get_valor_servicio")
    @Timed
    @RolesAllowed({"Auth"})
    public Response get_valor_servicio(@QueryParam("idbill") String idbill,@QueryParam("pct") String pct){

        Response response;

        Either<IException, String> getResponseGeneric = billBusiness.get_valor_servicio(idbill,pct);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/getIsServicioInBill")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getIsServicioInBill(@QueryParam("idbill") Long idbill){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.getIsServicioInBill(idbill);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/get_valores_bill")
    @Timed
    @RolesAllowed({"Auth"})
    public Response get_valores_bill(@QueryParam("idbill") String idbill){

        Response response;

        Either<IException, String> getResponseGeneric = billBusiness.get_valores_bill(idbill);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/crear_credito")
    @Timed
    @RolesAllowed({"Auth"})
    public Response crear_credito(@QueryParam("fechacreacion") String fechacreacion,
                                  @QueryParam("valor") Long valor,
                                  @QueryParam("numcuotas") Long numcuotas,
                                  @QueryParam("interes") Long interes,
                                  @QueryParam("interesmora") Long interesmora,
                                  @QueryParam("periodicidad") Long periodicidad,
                                  @QueryParam("cuotainicial") Long cuotainicial,
                                  @QueryParam("idthirdcliente") Long idthirdcliente,
                                  @QueryParam("idthirdprov") Long idthirdprov,
                                  @QueryParam("idthirdcodeudor") Long idthirdcodeudor){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.crear_credito(fechacreacion,
                valor,
                numcuotas,
                interes,
                interesmora,
                periodicidad,
                cuotainicial,
                idthirdcliente,
                idthirdprov,
                idthirdcodeudor);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/get_max_credito")
    @Timed
    @RolesAllowed({"Auth"})
    public Response get_max_credito(@QueryParam("id_credito") String id_credito){

        Response response;

        Either<IException, String> getResponseGeneric = billBusiness.get_max_credito(id_credito);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/get_credito")
    @Timed
    @RolesAllowed({"Auth"})
    public Response get_credito(@QueryParam("cedula") String cedula){

        Response response;

        Either<IException, List<credito>> getResponseGeneric = billBusiness.get_credito(cedula);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/getDocumentoCredito")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDocumentoCredito(@QueryParam("id_credito") Long id_credito){

        Response response;

        Either<IException, List<DocumentoCredito>> getResponseGeneric = billBusiness.getDocumentoCredito(id_credito);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/CREAR_CREDITO_DOCUMENTO")
    @Timed
    @RolesAllowed({"Auth"})
    public Response CREAR_CREDITO_DOCUMENTO(@QueryParam("NOMBREDOC") String NOMBREDOC,
                                  @QueryParam("RUTADOC") String RUTADOC,
                                  @QueryParam("TIPODOC") String TIPODOC,
                                  @QueryParam("IDCREDITO") Long IDCREDITO){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.CREAR_CREDITO_DOCUMENTO( NOMBREDOC,
                RUTADOC,
                TIPODOC,
                IDCREDITO );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/getLogCredito")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getLogCredito(@QueryParam("id_credito") Long id_credito){

        Response response;

        Either<IException, List<LogCredito>> getResponseGeneric = billBusiness.getLogCredito(id_credito);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @POST
    @Timed
    @Path("/fileUpload")
    @RolesAllowed({"Auth"})
    public Response postImageProduct(base64FileDTO dto){

        Response response;

        Either<IException, Long> mailEither = billBusiness.postFile(dto);

        /*Either<IException, Long> mailEither = inventoryBusiness.createInventory(inventoryDTO);
         */

        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



    @GET
    @Path("/aprobar_credito")
    @Timed
    @RolesAllowed({"Auth"})
        public Response aprobar_credito(@QueryParam("IDCREDITO") Long IDCREDITO,
                                    @QueryParam("NOTE") String NOTE){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.aprobar_credito( IDCREDITO, NOTE );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/desembolsar_credito")
    @Timed
    @RolesAllowed({"Auth"})
    public Response desembolsar_credito(@QueryParam("IDCREDITO") Long IDCREDITO,
                                        @QueryParam("NOTE") String NOTE){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.desembolsar_credito( IDCREDITO, NOTE  );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/generar_cuotas")
    @Timed
    @RolesAllowed({"Auth"})
    public Response generar_cuotas(@QueryParam("IDCREDITO") Long IDCREDITO){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.generar_cuotas( IDCREDITO );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/getCreditoById")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCreditoById(@QueryParam("ID_CREDITO") Long ID_CREDITO){

        Response response;

        Either<IException, List<credito>> getResponseGeneric = billBusiness.getCreditoById(ID_CREDITO);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/crear_cuota_credito")
    @Timed
    @RolesAllowed({"Auth"})
    public Response crear_cuota_credito(@QueryParam("idcredito") Long idcredito,
                                        @QueryParam("fechavencimiento") String fechavencimiento,
                                        @QueryParam("valorcuota") Long valorcuota){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.crear_cuota_credito( idcredito, fechavencimiento, valorcuota );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/crear_abono_cuota")
    @Timed
    @RolesAllowed({"Auth"})
    public Response crear_abono_cuota(@QueryParam("idcuota") Long idcuota,
                                      @QueryParam("idcredito") Long idcredito,
                                      @QueryParam("fechaabono") String fechaabono,
                                      @QueryParam("valorabono") Long valorabono,
                                      @QueryParam("idpaymentmethod") Long idpaymentmethod,
                                      @QueryParam("IDCREDITODOCUMENTO") Long IDCREDITODOCUMENTO){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.crear_abono_cuota( idcuota, idcredito, fechaabono, valorabono, idpaymentmethod, IDCREDITODOCUMENTO);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/getCuotas")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCuotas(@QueryParam("ID_CREDITO") String ID_CREDITO){

        Response response;

        Either<IException, List<Cuota>> getResponseGeneric = billBusiness.getCuotas(ID_CREDITO);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




    @GET
    @Path("/getAbono")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getAbono(@QueryParam("ID_CREDITO") Long ID_CREDITO,
                             @QueryParam("ID_CUOTA") Long ID_CUOTA){

        Response response;

        Either<IException, List<Abono>> getResponseGeneric = billBusiness.getAbono(ID_CREDITO,ID_CUOTA);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/asociar_factura_credito")
    @Timed
    @RolesAllowed({"Auth"})
    public Response asociar_factura_credito(@QueryParam("idcredito") Long idcredito,
                                            @QueryParam("idbill") Long idbill){

        Response response;

        Either<IException, Long> getResponseGeneric = billBusiness.asociar_factura_credito( idcredito, idbill);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/Bills4Credit")
    @Timed
    @RolesAllowed({"Auth"})
    public Response Bills4Credit(@QueryParam("idthird") Long idthird,
                                 @QueryParam("numdoc") String numdoc){

        Response response;

        Either<IException, List<Bills4Credit>> getResponseGeneric = billBusiness.Bills4Credit(idthird,numdoc);

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

}
