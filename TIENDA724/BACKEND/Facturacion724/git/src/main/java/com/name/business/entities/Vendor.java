package com.name.business.entities;

import java.util.Date;

public class Vendor {

    public Long getID_THIRD() {
        return ID_THIRD;
    }

    public void setID_THIRD(Long ID_THIRD) {
        this.ID_THIRD = ID_THIRD;
    }

    public String getVENDEDOR() {
        return VENDEDOR;
    }

    public void setVENDEDOR(String VENDEDOR) {
        this.VENDEDOR = VENDEDOR;
    }

    private Long ID_THIRD;
    private String VENDEDOR;

    public Vendor(Long ID_THIRD, String VENDEDOR) {
        this.ID_THIRD = ID_THIRD;
        this.VENDEDOR = VENDEDOR;
    }

}
