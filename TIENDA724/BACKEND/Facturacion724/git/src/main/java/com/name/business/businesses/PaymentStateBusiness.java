package com.name.business.businesses;

import com.name.business.DAOs.PaymentStateDAO;
import com.name.business.entities.CommonSimple;
import com.name.business.entities.CommonState;
import com.name.business.entities.PaymentState;
import com.name.business.representations.CommonStateDTO;
import com.name.business.representations.PaymentStateDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.paymentMethodSanitation;
import static com.name.business.sanitations.EntitySanitation.paymentStateSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

public class PaymentStateBusiness {
    private PaymentStateDAO paymentStateDAO;
    private CommonStateBusiness commonStateBusiness;

    public PaymentStateBusiness(PaymentStateDAO paymentStateDAO, CommonStateBusiness commonStateBusiness) {
        this.paymentStateDAO = paymentStateDAO;
        this.commonStateBusiness = commonStateBusiness;
    }

    public Either<IException, List<PaymentState>> getPaymentState(PaymentState paymentState) {
        List<String> msn = new ArrayList<>();
        try {
            //System.out.println("|||||||||||| Starting consults Payment State ||||||||||||||||| ");
            return Either.right(paymentStateDAO.read(paymentStateSanitation(paymentState)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     *
     * @param paymentStateDTO
     * @return
     */
    public Either<IException,Long> createPaymentState(PaymentStateDTO paymentStateDTO){
        List<String> msn = new ArrayList<>();
        Long id_payment_state = null;
        Long id_common_state = null;

        try {

            //System.out.println("|||||||||||| Starting creation Payment State  ||||||||||||||||| ");
            if (paymentStateDTO!=null){

                if (paymentStateDTO.getStateDTO() == null) {
                    paymentStateDTO.setStateDTO(new CommonStateDTO(1,null,null));
                }
                Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(paymentStateDTO.getStateDTO());
                if (commonThirdEither.isRight()) {
                    id_common_state = commonThirdEither.right().value();
                }

                paymentStateDAO.create(id_common_state,formatoStringSql(paymentStateDTO.getName()));
                id_payment_state = paymentStateDAO.getPkLast();

                return Either.right(id_payment_state);
            }else{
                //msn.add("It does not recognize Product, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * @param id_payment_state
     * @param paymentStateDTO
     * @return
     */
    public Either<IException, Long> updatePaymentState(Long id_payment_state, PaymentStateDTO paymentStateDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        PaymentState actualRegister=null;

        try {
            //System.out.println("|||||||||||| Starting update PaymentState ||||||||||||||||| ");
            if ((id_payment_state != null && id_payment_state > 0)) {



                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorID(id_payment_state).equals(false)){
                    //msn.add("It ID Payment State  does not exist register on  Payment State, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<PaymentState> read = paymentStateDAO.read(
                        new PaymentState(id_payment_state, null,
                                new CommonState(null,null,null,null))
                );

                if (read.size()<=0){
                    //msn.add("It does not recognize ID Payment State , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = paymentStateDAO.readCommons(formatoLongSql(id_payment_state));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);

                    // Common  State business update
                    commonStateBusiness.updateCommonState(idCommons.getId_common_state(), paymentStateDTO.getStateDTO());

                }

                if (paymentStateDTO.getName() == null || paymentStateDTO.getName().isEmpty()) {
                    paymentStateDTO.setName(actualRegister.getName_payment_state());
                } else {
                    paymentStateDTO.setName(formatoStringSql(paymentStateDTO.getName()));
                }



                // Attribute update
                paymentStateDAO.update(id_payment_state,paymentStateDTO.getName());

                return Either.right(id_payment_state);

            } else {
                //msn.add("It does not recognize ID Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id_payment_state
     * @return
     */
    public Either<IException, Long> deletePaymentState(Long id_payment_state) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting delete Payment  State ||||||||||||||||| ");
            if (id_payment_state != null && id_payment_state > 0) {
                List<CommonSimple> readCommons = paymentStateDAO.readCommons(formatoLongSql(id_payment_state));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common
                return Either.right(id_payment_state);
            } else {
                //msn.add("It does not recognize ID Payment State, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = paymentStateDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }
}
