package com.name.business.mappers;

import com.name.business.entities.BillRaw;
import com.name.business.entities.Bills4Credit;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Bills4CreditMapper implements ResultSetMapper<Bills4Credit> {
    @Override
    public Bills4Credit map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Bills4Credit(
                resultSet.getLong("id_bill"),
                resultSet.getString("num_documento"),
                resultSet.getString("tienda")
        );
    }
}
