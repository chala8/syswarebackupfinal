package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.DetailData;
import com.name.business.entities.DetailForPedido;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailForPedidoMapper implements ResultSetMapper<DetailForPedido> {

    @Override
    public DetailForPedido map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DetailForPedido(
                resultSet.getString("FECHA_EVENTO"),
                resultSet.getString("ACTOR"),
                resultSet.getLong("ID_THIRD_USER"),
                resultSet.getLong("IDESTADOORIGEN"),
                resultSet.getLong("IDESTADODESTINO"),
                resultSet.getString("NOTAS")
        );
    }
}
