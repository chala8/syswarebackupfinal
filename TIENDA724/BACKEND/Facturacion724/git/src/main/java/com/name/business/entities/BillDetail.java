package com.name.business.entities;

import java.util.Date;

public class BillDetail {

    private String PRODUCT_STORE_NAME;
    private Long QUANTITY;
    private Double VALOR;
    private String CODE;
    private Double PERCENT;
    private Double PRICE_NO_IVA;
    private Double PRICE;
    private Long IDTAX;


    public BillDetail(String PRODUCT_STORE_NAME,
                      Long QUANTITY,
                      Double VALOR,
                      String CODE,
                      Double PERCENT,
                      Double PRICE_NO_IVA,
                      Double PRICE,
                      Long IDTAX) {
        this.PRICE = PRICE;
        this.IDTAX = IDTAX;
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
        this.QUANTITY = QUANTITY;
        this.VALOR = VALOR;
        this.CODE = CODE;
        this.PERCENT = PERCENT;
        this.PRICE_NO_IVA = PRICE_NO_IVA;
    }
    public Double getPRICE() { return PRICE; }

    public void setPRICE(Double PRICE) { this.PRICE = PRICE; }

    public Long getIDTAX() { return IDTAX; }

    public void setIDTAX(Long IDTAX) { this.IDTAX = IDTAX; }
    //--------------------------------------------------------------------------------------------------

    public Double getPRICE_NO_IVA() {
        return PRICE_NO_IVA;
    }

    public void setPRICE_NO_IVA(Double PRICE_NO_IVA) {
        this.PRICE_NO_IVA = PRICE_NO_IVA;
    }

    //--------------------------------------------------------------------------------------------------

    public Double getPERCENT() {
        return PERCENT;
    }

    public void setPERCENT(Double PERCENT) {
        this.PERCENT = PERCENT;
    }

    //--------------------------------------------------------------------------------------------------

    public String getPRODUCT_STORE_NAME() {
        return PRODUCT_STORE_NAME;
    }

    public void setPRODUCT_STORE_NAME(String PRODUCT_STORE_NAME) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
    }

    //--------------------------------------------------------------------------------------------------

    public Long getQUANTITY() {
        return QUANTITY;
    }

    public void setQUANTITY(Long QUANTITY) {
        this.QUANTITY = QUANTITY;
    }

    //--------------------------------------------------------------------------------------------------

    public Double getVALOR() {
        return VALOR;
    }

    public void setVALOR(Double VALOR) {
        this.VALOR = VALOR;
    }

    //--------------------------------------------------------------------------------------------------

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    //--------------------------------------------------------------------------------------------------




}
