package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.Kardex;
import com.name.business.entities.LegalData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class KardexMapper implements ResultSetMapper<Kardex> {
    @Override
    public Kardex map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Kardex(
                resultSet.getLong("ID_BILL"),
                resultSet.getString("FECHA_MOVIMIENTO"),
                resultSet.getString("PRODUCTO"),
                resultSet.getString("DOCUMENTO"),
                resultSet.getString("TIPO_MOVIMIENTO"),
                resultSet.getLong("ID_PRODUCT_STORE"),
                resultSet.getLong("INVENTARIO_INICIAL"),
                resultSet.getLong("CANTIDAD_MOVIMIENTO"),
                resultSet.getLong("INVENTARIO_FINAL"),
                resultSet.getString("USUARIO")

        );
    }
}
