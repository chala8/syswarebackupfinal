package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.Cuota;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CuotaMapper implements ResultSetMapper<Cuota> {

    @Override
    public Cuota map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Cuota(
                resultSet.getLong("ID_CUOTA"),
                resultSet.getLong("ID_CREDITO"),
                resultSet.getString("FECHA_VENCIMIENTO"),
                resultSet.getLong("VALOR"),
                resultSet.getLong("ID_ESTADO_CUOTA"),
                resultSet.getString("ESTADO_CUOTA")
        );
    }
}
