package com.name.business.entities;

import java.util.Date;

public class ReorderV2 {
    private String BARCODE;
    private String PRODUCTO;
    private String LINEA;
    private String CATEGORIA;
    private String FABRICANTE;
    private String MARCA;
    private String PRESENTACION;
    private Long CANTIDAD_VENDIDA;
    private Long CANTIDAD_EN_INVENTARIO;
    private Double COSTO;
    private Double IVA;
    private Long IDPS;
    private Long IDT;
    private Long ID_PROVIDER;


    public ReorderV2( String BARCODE,
                        String PRODUCTO,
                        String LINEA,
                        String CATEGORIA,
                        String FABRICANTE,
                        String MARCA,
                        String PRESENTACION,
                        Long CANTIDAD_VENDIDA,
                        Long CANTIDAD_EN_INVENTARIO,
                        Double COSTO,
                        Double IVA,
                        Long IDPS,
                        Long IDT,
                        Long ID_PROVIDER) {
        this.FABRICANTE = FABRICANTE;
        this.BARCODE = BARCODE;
        this.PRODUCTO = PRODUCTO;
        this.LINEA = LINEA;
        this.CATEGORIA = CATEGORIA;
        this.MARCA = MARCA;
        this.PRESENTACION = PRESENTACION;
        this.CANTIDAD_VENDIDA = CANTIDAD_VENDIDA;
        this.CANTIDAD_EN_INVENTARIO = CANTIDAD_EN_INVENTARIO;
        this.COSTO = COSTO;
        this.IVA = IVA;
        this.IDPS = IDPS;
        this.IDT = IDT;
        this.ID_PROVIDER = ID_PROVIDER;

    }
    public Long getID_PROVIDER() {
        return ID_PROVIDER;
    }

    public void setID_PROVIDER(Long ID_PROVIDER) {
        this.ID_PROVIDER = ID_PROVIDER;
    }



    public Long getIDT() {
        return IDT;
    }

    public void setIDT(Long IDT) {
        this.IDT = IDT;
    }



    public Long getIDPS() {
        return IDPS;
    }

    public void setIDPS(Long IDPS) {
        this.IDPS = IDPS;
    }

    public Double getIVA() {
        return IVA;
    }

    public void setIVA(Double IVA) {
        this.IVA = IVA;
    }


    public Double getCOSTO() {
        return COSTO;
    }

    public void setCOSTO(Double FABRICANTE) {
        this.COSTO = COSTO;
    }

    public String getFABRICANTE() {
        return FABRICANTE;
    }

    public void setFABRICANTE(String FABRICANTE) {
        this.FABRICANTE = FABRICANTE;
    }

    public String getBARCODE() {
        return BARCODE;
    }

    public void setBARCODE(String BARCODE) {
        this.BARCODE = BARCODE;
    }

    public String getPRODUCTO() {
        return PRODUCTO;
    }

    public void setPRODUCTO(String PRODUCTO) {
        this.PRODUCTO = PRODUCTO;
    }

    public String getLINEA() {
        return LINEA;
    }

    public void setLINEA(String LINEA) {
        this.LINEA = LINEA;
    }

    public String getCATEGORIA() {
        return CATEGORIA;
    }

    public void setCATEGORIA(String CATEGORIA) {
        this.CATEGORIA = CATEGORIA;
    }

    public String getMARCA() {
        return MARCA;
    }

    public void setMARCA(String MARCA) {
        this.MARCA = MARCA;
    }

    public String getPRESENTACION() {
        return PRESENTACION;
    }

    public void setPRESENTACION(String PRESENTACION) {
        this.PRESENTACION = PRESENTACION;
    }

    public Long getCANTIDAD_VENDIDA() {
        return CANTIDAD_VENDIDA;
    }

    public void setCANTIDAD_VENDIDA(Long CANTIDAD_VENDIDA) {
        this.CANTIDAD_VENDIDA = CANTIDAD_VENDIDA;
    }

    public Long getCANTIDAD_EN_INVENTARIO() {
        return CANTIDAD_EN_INVENTARIO;
    }

    public void setCANTIDAD_EN_INVENTARIO(Long CANTIDAD_EN_INVENTARIO) {
        this.CANTIDAD_EN_INVENTARIO = CANTIDAD_EN_INVENTARIO;
    }


}
