package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.glassfish.jersey.server.JSONP;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class PdfPedidosOrderDTO {
    private ProviderDataDTO datosProv;
    private String documento;
    private String cliente;
    private String fecha;
    private String documento_cliente;
    private String correo;
    private String direccion;
    private double total;
    private double subtotal;
    private double tax;
    private List<List<String>> detail_list;
    private List<Boolean> used_list;
    private String observaciones;
    private Map<String, Object> DatosTercero;
    private String telefono;
    private String num_documento_cliente;
    private String tienda;
    private String tienda_client;

    @JsonCreator
    public PdfPedidosOrderDTO(@JsonProperty("datosProv") ProviderDataDTO datosProv,
                          @JsonProperty("documento") String documento,
                          @JsonProperty("cliente") String cliente,
                          @JsonProperty("fecha") String fecha,
                          @JsonProperty("documento_cliente") String documento_cliente,
                          @JsonProperty("correo") String correo,
                          @JsonProperty("direccion") String direccion,
                          @JsonProperty("total") double total,
                          @JsonProperty("subtotal") double subtotal,
                          @JsonProperty("tax") double tax,
                          @JsonProperty("detail_list") List<List<String>> detail_list,
                          @JsonProperty("used_list") List<Boolean> used_list,
                          @JsonProperty("observaciones") String observaciones,
                          @JsonProperty("DatosTercero") Map<String, Object> DatosTercero,
                          @JsonProperty("telefono") String telefono,
                          @JsonProperty("num_documento_cliente") String num_documento_cliente,
                          @JsonProperty("num_documento_cliente") String tienda,
                          @JsonProperty("num_documento_cliente") String tienda_client
                              ) {
        this.datosProv = datosProv;
        this.DatosTercero = DatosTercero;
        this.documento = documento;
        this.cliente = cliente;
        this.fecha = fecha;
        this.documento_cliente = documento_cliente;
        this.correo = correo;
        this.direccion = direccion;
        this.total = total;
        this.subtotal = subtotal;
        this.tax = tax;
        this.detail_list = detail_list;
        this.used_list = used_list;
        this.observaciones = observaciones;
        this.telefono = telefono;
        this.num_documento_cliente = num_documento_cliente;
        this.tienda = tienda;
        this.tienda_client = tienda_client;
    }

    public String gettienda_client() {
        return tienda_client;
    }

    public void settienda_client(String tienda_client) {
        this.tienda_client = tienda_client;
    }

    public ProviderDataDTO getdatosProv() {
        return datosProv;
    }

    public void setdatosProv(ProviderDataDTO datosProv) {
        this.datosProv = datosProv;
    }

    public String getnum_documento_cliente() {
        return num_documento_cliente;
    }

    public void setnum_documento_cliente(String num_documento_cliente) {
        this.num_documento_cliente = num_documento_cliente;
    }

    public String gettienda() {
        return tienda;
    }

    public void settienda(String tienda) {
        this.tienda = tienda;
    }

    public String gettelefono() {
        return telefono;
    }

    public void settelefono(String telefono) {
        this.telefono = telefono;
    }

    public Map<String, Object> getDatosTercero() {
        return DatosTercero;
    }

    public void setDatosTercero(Map<String, Object> DatosTercero) {
        this.DatosTercero = DatosTercero;
    }

    public List<Boolean> getused_list() {
        return used_list;
    }

    public void setused_list(List<Boolean> used_list) {
        this.used_list = used_list;
    }

    public List<List<String>> getdetail_list() {
        return detail_list;
    }

    public void setdetail_list(List<List<String>> detail_list) {
        this.detail_list = detail_list;
    }

    public double gettax() {
        return tax;
    }

    public void settax(double tax) {
        this.tax = tax;
    }

    public double getsubtotal() {
        return subtotal;
    }

    public void setsubtotal(double subtotal) {
        this.subtotal = subtotal;
    }


    public double gettotal() {
        return total;
    }

    public void settotal(double total) {
        this.total = total;
    }


    public String getdireccion() {
        return direccion;
    }

    public void setdireccion(String direccion) {
        this.direccion = direccion;
    }


    public String getobservaciones() {
        return observaciones;
    }

    public void setobservaciones(String observaciones) {
        this.observaciones = observaciones;
    }


    public String getcorreo() {
        return correo;
    }

    public void setcorreo(String correo) {
        this.correo = correo;
    }





    public String getdocumento_cliente() {
        return documento_cliente;
    }

    public void setdocumento_cliente(String documento_cliente) {
        this.documento_cliente = documento_cliente;
    }


    public String getfecha() {
        return fecha;
    }

    public void setfecha(String fecha) {
        this.fecha = fecha;
    }



    public String getcliente() {
        return cliente;
    }

    public void setcliente(String cliente) {
        this.cliente = cliente;
    }

    public String getdocumento() {
        return documento;
    }

    public void setdocumento(String documento) {
        this.documento = documento;
    }

}
