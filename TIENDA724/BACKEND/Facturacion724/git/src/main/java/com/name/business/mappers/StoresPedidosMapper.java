package com.name.business.mappers;

import com.name.business.entities.Bill;
import com.name.business.entities.CommonState;
import com.name.business.entities.Refund;
import com.name.business.entities.StoresPedidos;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StoresPedidosMapper  implements ResultSetMapper<StoresPedidos> {

    @Override
    public StoresPedidos map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new StoresPedidos(
                resultSet.getLong("ID_STORE_PROVIDER"),
                resultSet.getString("DESCRIPTION")
        );
    }
}
