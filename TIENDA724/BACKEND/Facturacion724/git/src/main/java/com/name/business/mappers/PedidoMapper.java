package com.name.business.mappers;


import com.name.business.entities.Pedido;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PedidoMapper implements ResultSetMapper<Pedido> {

    @Override
    public Pedido map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Pedido(
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("ID_STORE_CLIENT"),
                resultSet.getLong("ID_STORE"),
                resultSet.getString("CLIENTE"),
                resultSet.getString("TIENDA"),
                resultSet.getString("NUMPEDIDO"),
                resultSet.getString("FECHA"),
                resultSet.getString("ADDRESS"),
                resultSet.getString("PHONE"),
                resultSet.getString("MAIL"),
                resultSet.getString("LATITUD"),
                resultSet.getString("LONGITUD"),
                resultSet.getString("NUM_DOCUMENTO"),
                resultSet.getString("BODY"),
                resultSet.getLong("IDTHIRDCLIENTE"),
                resultSet.getLong("ID_PAYMENT"),
                resultSet.getString("ADDRESSP"),
                resultSet.getString("PHONEP"),
                resultSet.getString("MAILP"),
                resultSet.getDouble("LATITUDP"),
                resultSet.getDouble("LONGITUDP"),
                resultSet.getString("TIENDACLIENTE"),
                resultSet.getString("DIRTIENDACLIENTE"),
                resultSet.getString("CIUDADTIENDACLIENTE"),
                resultSet.getDouble("TOTALPRICE"),
                resultSet.getLong("IDAPP")
                );
    }
}
