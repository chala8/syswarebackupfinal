package com.name.business.entities;

import java.util.Date;

public class DatosDomiciliario {



    private String DOMICILIARIO;
    private Long ID_PLANILLA;
    private String NUMPLANILLA;
    private String STATUS;
    private Double TOTAL;
    private Long ID_CIERRE_CAJA;



    public DatosDomiciliario  (
                          String DOMICILIARIO,
                          Long ID_PLANILLA,
                          String NUMPLANILLA,
                          String STATUS,
                          Double TOTAL,
                          Long ID_CIERRE_CAJA) {
        this.ID_CIERRE_CAJA = ID_CIERRE_CAJA;
        this.TOTAL = TOTAL;
        this.STATUS  = STATUS;
        this.NUMPLANILLA = NUMPLANILLA;
        this.ID_PLANILLA  = ID_PLANILLA;
        this.DOMICILIARIO = DOMICILIARIO;
    }


    public Long getID_CIERRE_CAJA() {
        return ID_CIERRE_CAJA;
    }

    public void setID_CIERRE_CAJA(Long ID_CIERRE_CAJA) {
        this.ID_CIERRE_CAJA = ID_CIERRE_CAJA;
    }


    public String getDOMICILIARIO() {
        return DOMICILIARIO;
    }

    public void setDOMICILIARIO(String DOMICILIARIO) {
        this.DOMICILIARIO = DOMICILIARIO;
    }

    public Long getID_PLANILLA() {
        return ID_PLANILLA;
    }

    public void setID_PLANILLA(Long ID_PLANILLA) {
        this.ID_PLANILLA = ID_PLANILLA;
    }

    public String getNUMPLANILLA() {
        return NUMPLANILLA;
    }

    public void setNUMPLANILLA(String NUMPLANILLA) {
        this.NUMPLANILLA = NUMPLANILLA;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public Double getTOTAL() {
        return TOTAL;
    }

    public void setTOTAL(Double TOTAL) {
        this.TOTAL = TOTAL;
    }



}
