package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.LogCredito;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LogCreditoMapper implements ResultSetMapper<LogCredito> {
    @Override
    public LogCredito map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new LogCredito(
                resultSet.getLong("ID_CREDITO_LOG"),
                resultSet.getString("TEXTO"),
                resultSet.getString("FECHA")


        );
    }
}
