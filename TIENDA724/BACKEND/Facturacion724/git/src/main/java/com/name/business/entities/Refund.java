package com.name.business.entities;

import java.util.Date;

public class Refund {
    private Long ID_PRODUCT_THIRD;
    private Long STANDARD_PRICE;
    private String DESCRIPTION;

    public Refund(Long ID_PRODUCT_THIRD, Long STANDARD_PRICE, String DESCRIPTION) {
        this.ID_PRODUCT_THIRD = ID_PRODUCT_THIRD;
        this.STANDARD_PRICE = STANDARD_PRICE;
        this.DESCRIPTION = DESCRIPTION;
    }


    public Long getID_PRODUCT_THIRD() {
        return ID_PRODUCT_THIRD;
    }

    public void setID_PRODUCT_THIRD(Long ID_PRODUCT_THIRD) {
        this.ID_PRODUCT_THIRD = ID_PRODUCT_THIRD;
    }

    public Long getSTANDARD_PRICE() {
        return STANDARD_PRICE;
    }

    public void setSTANDARD_PRICE(Long STANDARD_PRICE) {
        this.STANDARD_PRICE = STANDARD_PRICE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }


}
