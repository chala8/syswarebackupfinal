package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;
import java.util.List;

public class ReporteFinanciero1DTO {
    private String empresa;
    private String NIT;
    private String Fecha;
    //Listado
    private List<Map<String,String>> Filas;

    public ReporteFinanciero1DTO(
            @JsonProperty("empresa") String empresa,
            @JsonProperty("NIT") String NIT,
            @JsonProperty("Fecha") String fecha,
            @JsonProperty("Filas") List<Map<String,String>> Filas
    ) {
        this.empresa = empresa;
        this.NIT = NIT;
        this.Fecha = fecha;
        this.Filas = Filas;
    }

    //TODOS LOS METODOS GET Y SET

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getNIT() {
        return NIT;
    }

    public void setNIT(String NIT) {
        this.NIT = NIT;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        this.Fecha = fecha;
    }

    public List<Map<String,String>> getFilas() {
        return Filas;
    }

    public void setFilas(List<Map<String,String>> Filas) {
        this.Filas = Filas;
    }
}
