package com.name.business.entities;

import java.util.Date;

public class PlanillaData {



    private String FECHA_INICIO;
    private String NOTES;
    private String DOMICILIARIO;
    private Long ID_PLANILLA;
    private String NUMPLANILLA;
    private String STATUS;
    private String NUMFACTURA;
    private String NUMPEDIDO;
    private Double TOTALPRICE;
    private Long ID_BILL;



    public PlanillaData  (String FECHA_INICIO,
             String NOTES,
             String DOMICILIARIO,
             Long ID_PLANILLA,
             String NUMPLANILLA,
             String STATUS,
             String NUMFACTURA,
             String NUMPEDIDO,
             Double TOTALPRICE,
             Long ID_BILL) {
        this.ID_BILL = ID_BILL;
        this.TOTALPRICE = TOTALPRICE;
        this.NUMPEDIDO  = NUMPEDIDO;
        this.NUMFACTURA = NUMFACTURA;
        this.STATUS  = STATUS;
        this.NUMPLANILLA = NUMPLANILLA;
        this.ID_PLANILLA  = ID_PLANILLA;
        this.DOMICILIARIO = DOMICILIARIO;
        this.FECHA_INICIO = FECHA_INICIO;
        this.NOTES  = NOTES;
    }


    public Long getID_BILL() { return ID_BILL; }

    public void setID_BILL(Long ID_BILL) { this.ID_BILL = ID_BILL; }

    public String getFECHA_INICIO() {
        return FECHA_INICIO;
    }

    public void setFECHA_INICIO(String FECHA_INICIO) {
        this.FECHA_INICIO = FECHA_INICIO;
    }

    public String getNOTES() {
        return NOTES;
    }

    public void setNOTES(String NOTES) {
        this.NOTES = NOTES;
    }

    public String getDOMICILIARIO() {
        return DOMICILIARIO;
    }

    public void setDOMICILIARIO(String DOMICILIARIO) {
        this.DOMICILIARIO = DOMICILIARIO;
    }

    public Long getID_PLANILLA() {
        return ID_PLANILLA;
    }

    public void setID_PLANILLA(Long ID_PLANILLA) {
        this.ID_PLANILLA = ID_PLANILLA;
    }

    public String getNUMPLANILLA() {
        return NUMPLANILLA;
    }

    public void setNUMPLANILLA(String NUMPLANILLA) {
        this.NUMPLANILLA = NUMPLANILLA;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getNUMFACTURA() {
        return NUMFACTURA;
    }

    public void setNUMFACTURA(String NUMFACTURA) {
        this.NUMFACTURA = NUMFACTURA;
    }

    public String getNUMPEDIDO() {
        return NUMPEDIDO;
    }

    public void setNUMPEDIDO(String NUMPEDIDO) {
        this.NUMPEDIDO = NUMPEDIDO;
    }

    public Double getTOTALPRICE() {
        return TOTALPRICE;
    }

    public void setTOTALPRICE(Double TOTALPRICE) {
        this.TOTALPRICE = TOTALPRICE;
    }


}
