package com.name.business.businesses;

import com.name.business.DAOs.PaymentMethodDAO;
import com.name.business.entities.*;
import com.name.business.representations.CommonStateDTO;
import com.name.business.representations.PaymentMethodDTO;
import com.name.business.representations.WayToPayDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.detailPaymentBillSanitation;
import static com.name.business.sanitations.EntitySanitation.paymentMethodSanitation;
import static com.name.business.sanitations.EntitySanitation.wayToPaySanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

public class PaymentMethodBusiness {
    private PaymentMethodDAO paymentMethodDAO;
    private CommonStateBusiness commonStateBusiness;

    public PaymentMethodBusiness(PaymentMethodDAO paymentMethodDAO, CommonStateBusiness commonStateBusiness) {
        this.paymentMethodDAO = paymentMethodDAO;
        this.commonStateBusiness = commonStateBusiness;
    }


    public Either<IException, List<PaymentMethod>> getPaymentMethod(PaymentMethod paymentMethod) {
        List<String> msn = new ArrayList<>();
        try {
            ////msn.add("OK");
            //System.out.println("|||||||||||| Starting consults Payment Method ||||||||||||||||| ");
            return Either.right(paymentMethodDAO.read(paymentMethodSanitation(paymentMethod)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     *
     * @param paymentMethodDTO
     * @return
     */
    public Either<IException,Long> creatPaymentMethod(PaymentMethodDTO paymentMethodDTO){
        List<String> msn = new ArrayList<>();
        Long id_payment_method = null;
        Long id_common_state = null;

        try {
            //System.out.println("|||||||||||| Starting creation Payment Method  ||||||||||||||||| ");
            if (paymentMethodDTO!=null){

                if (paymentMethodDTO.getStateDTO() == null) {
                    paymentMethodDTO.setStateDTO(new CommonStateDTO(1,null,null));
                }
                Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(paymentMethodDTO.getStateDTO());
                if (commonThirdEither.isRight()) {
                    id_common_state = commonThirdEither.right().value();
                }

                paymentMethodDAO.create(id_common_state,formatoStringSql(paymentMethodDTO.getName()));
                id_payment_method = paymentMethodDAO.getPkLast();

                return Either.right(id_payment_method);
            }else{
                //msn.add("It does not recognize Payment Method, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * @param id_payment_state
     * @param paymentMethodDTO
     * @return
     */
    public Either<IException, Long> updatePaymentMethod(Long id_payment_state, PaymentMethodDTO paymentMethodDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        PaymentState actualRegister=null;

        try {
            //System.out.println("|||||||||||| Starting update Payment Method ||||||||||||||||| ");
            if ((id_payment_state != null && id_payment_state > 0)) {



                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorID(id_payment_state).equals(false)){
                    //msn.add("It ID Payment Method  does not exist register on  Payment Method, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<PaymentMethod> read = paymentMethodDAO.read(
                        new PaymentMethod(id_payment_state, null,
                                new CommonState(null,null,null,null))
                );

                if (read.size()<=0){
                    //msn.add("It does not recognize ID Payment Method , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = paymentMethodDAO.readCommons(formatoLongSql(id_payment_state));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);

                    // Common  State business update
                    commonStateBusiness.updateCommonState(idCommons.getId_common_state(), paymentMethodDTO.getStateDTO());

                }

                if (paymentMethodDTO.getName() == null || paymentMethodDTO.getName().isEmpty()) {
                    paymentMethodDTO.setName(actualRegister.getName_payment_state());
                } else {
                    paymentMethodDTO.setName(formatoStringSql(paymentMethodDTO.getName()));
                }



                // Attribute update
                paymentMethodDAO.update(id_payment_state,paymentMethodDTO.getName());

                return Either.right(id_payment_state);

            } else {
                //msn.add("It does not recognize Payment Method, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id_payment_mathod
     * @return
     */
    public Either<IException, Long> deletePaymentMethod(Long id_payment_mathod) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            //msn.add("OK");

            //System.out.println("|||||||||||| Starting delete Payment  State ||||||||||||||||| ");

            if (id_payment_mathod != null && id_payment_mathod > 0) {

                List<CommonSimple> readCommons = paymentMethodDAO.readCommons(formatoLongSql(id_payment_mathod));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());

                return Either.right(id_payment_mathod);
            } else {
                //msn.add("It does not recognize ID Payment State, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = paymentMethodDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }
}
