package com.name.business.businesses;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.name.business.DAOs.PedidosDAO;
import com.name.business.DAOs.ReorderDAO;
import com.name.business.entities.*;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import static com.name.business.utils.PDFHelpers.*;
import static com.name.business.utils.constans.K.messages_error;


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

public class PedidosBusiness {
    private final ReorderDAO reorderDAO;
    private final PedidosDAO pedidosDAO;

    private boolean PruebasLocales = false;

    public PedidosBusiness(PedidosDAO pedidosDAO, ReorderDAO reorderDAO) {
        this.pedidosDAO = pedidosDAO;
        this.reorderDAO = reorderDAO;
        try{
            if(System.getenv("SYSWARE_DEVPC").equals("yes")){this.PruebasLocales = true;}
        }catch (Exception e){
            this.PruebasLocales = false;
            ////System.out.println("Can't Get SYSWARE_DEVPC envr var");
        }
    }

    public Either<IException, List<StoresPedidos>> getStoreList(Long idthird,Long id_store_client) {
        try {
            List<StoresPedidos> validator = pedidosDAO.getStoreList(idthird, id_store_client);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<InventoryPedidoDetailDenlinea>> getInventoryPedido(Long id_store,Long id_store_prov) {
        try {
            List<InventoryPedidoDetailDenlinea> validator = pedidosDAO.getInventoryPedido(id_store,id_store_prov);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<InventoryPedidoDetail>> getInventoryPedidoDistriJass(Long id_store,Long id_store_prov) {
        try {
            List<InventoryPedidoDetail> validator = pedidosDAO.getInventoryPedidoDistriJass(id_store,id_store_prov);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<InventoryPedidoDetail2>> getInventoryPedido2(Long id_store,Long id_store_prov) {
        try {
            List<InventoryPedidoDetail2> validator = pedidosDAO.getInventoryPedido2(id_store,id_store_prov);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<PedidosManuales>> pedidoManual(Long id_store,Long id_store_prov) {
        try {
            List<PedidosManuales> validator = pedidosDAO.pedidoManual(id_store,id_store_prov);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, ClientePedido> getDatosClientePedido(Long idbill) {
        try {
            ClientePedido validator = pedidosDAO.getDatosClientePedido(idbill);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }
    public Either<IException, ClientePedido2> getDatosCliente(Long idbill) {
        try {
            ClientePedido2 validator = pedidosDAO.getDatosCliente(idbill);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }
    public Either<IException, String> getOwnBarCode(Long id_product_store) {
        try {
            String validator = pedidosDAO.getOwnBarCode(id_product_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> getPsId(String code, Long id_store) {
        try {
            Long validator = pedidosDAO.getPsId(code,id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Pedido>> getPedidos(Long id_store, Long id_bill_state, Long id_bill_type, String date1, String date2) {
        try {
            //System.out.println("DATE 1");
            //System.out.println(date1);
            //System.out.println("DATE 2");
            //System.out.println(date2);
            //List<Pedido> validator = pedidosDAO.getPedidos(id_store,id_bill_state,id_bill_type);
            List<Pedido> validator;
            if((date1==null || date1.equals("null")) && (date2 == null || date2.equals("null"))){
                validator = pedidosDAO.getPedidos(id_store,id_bill_state,id_bill_type);
            }
            else{
                //noinspection ConstantConditions
                if((date1==null || date1.equals("null")) && (date2 != null || !date2.equals("null"))){
                    validator = pedidosDAO.getPedidos2(id_store,id_bill_state,id_bill_type,new Date(date2),new Date(date2));
                }else{
                    //noinspection ConstantConditions
                    if((date2==null || date2.equals("null")) && (date1!=null || !date1.equals("null"))){
                        validator = pedidosDAO.getPedidos2(id_store,id_bill_state,id_bill_type,new Date(date1),new Date(date1));
                    }else{
                        validator = pedidosDAO.getPedidos2(id_store,id_bill_state,id_bill_type,new Date(date1),new Date(date2));
                    }
                }
            }
            return Either.right(validator);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<ReorderV2>> getReorderV2( String listStoreString, String date1, String date2, Long id_store) {
        try {
            List<ReorderV2> myEmptyList = Collections.<ReorderV2>emptyList();
            List<String> listStore = new ArrayList<String>(Arrays.asList(listStoreString.split(",")));
            //System.out.println(listStore.toString());
            if(listStore.size()==0){
                return Either.right(myEmptyList);
            }
            List<ReorderV2> validator = pedidosDAO.getReorderV2(listStore,new Date(date1),new Date(date2),id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<ReorderV2New>> getReorderV2New( String listStoreString, String listStoreStringProv, String date1, String date2, Long id_store) {
        try {
            List<ReorderV2New> myEmptyList = Collections.<ReorderV2New>emptyList();
            List<String> listStore = new ArrayList<String>(Arrays.asList(listStoreString.split(",")));
            List<String> listStoreStringProvm = new ArrayList<String>(Arrays.asList(listStoreStringProv.split(",")));
            //System.out.println(listStore.toString());
            if(listStore.size()==0){
                return Either.right(myEmptyList);
            }
            System.out.println("-----------------------WORD-------------------------");
            System.out.println(listStore.toString());
            System.out.println(listStoreStringProvm.toString());
            System.out.println("-----------------------WORD-------------------------");
            List<ReorderV2New> validator = pedidosDAO.getReorderV2New(listStore,listStoreStringProvm,new Date(date1),new Date(date2),id_store);



            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<DetallePedido>> getDetallesPedidos(List<Long> id_store) {
        try {
            List<DetallePedido> validator = pedidosDAO.getDetallesPedidos(id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<getProviders>> getProviders(Long id_store, Long id_prov) {
        try {
            List<getProviders> validator = pedidosDAO.getProviders(id_store, id_prov);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<getProviders>> getProviders2(Long id_store) {
        try {
            List<getProviders> validator = pedidosDAO.getProviders2(id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<FabByProv>> getFabByProv(Long id_third) {
        try {
            List<FabByProv> validator = pedidosDAO.getFabByProv(id_third);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Bill4Prod>> getBills4Products(Long id_ps, Date date1, Date date2) {
        try {
            List<Bill4Prod> validator = pedidosDAO.getBills4Products(id_ps, date1, date2);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<DetallePedido>> getDetallesPedidos2(List<Long> id_bill) {
        try {
            List<DetallePedido> validator = pedidosDAO.getDetallesPedidos2(id_bill);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> postExcel(PickingConsolidadoPdfDTO pedido){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);


        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            HSSFRichTextString textoT7 = new HSSFRichTextString(" ");
            celdaprodT.setCellValue(textoT7);


            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            HSSFRichTextString textoT4 = new HSSFRichTextString(" ");
            celdalineaT.setCellValue(textoT4);


            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Documento de picking Consolidado");
            celdacateT.setCellValue(textoT5);


            HSSFCell celdamarcaT = filaTitulo.createCell((short)3);
            HSSFRichTextString textoT6 = new HSSFRichTextString(" ");
            celdamarcaT.setCellValue(textoT6);



            HSSFCell celdaCantT = filaTitulo.createCell((short)4);
            HSSFRichTextString textoT = new HSSFRichTextString(" ");
            celdaCantT.setCellValue(textoT);

            HSSFRow filaTitulo2 = hoja.createRow(1);

            HSSFCell celdaprodT2 = filaTitulo2.createCell((short)0);
            HSSFRichTextString textoT72 = new HSSFRichTextString(" ");
            celdaprodT2.setCellValue(textoT72);


            HSSFCell celdalineaT2 = filaTitulo2.createCell((short)1);
            HSSFRichTextString textoT42 = new HSSFRichTextString("Costo Total: "+pedido.getcostoTotal());
            celdalineaT2.setCellValue(textoT42);


            HSSFCell celdacateT2 = filaTitulo2.createCell((short)2);
            HSSFRichTextString textoT52 = new HSSFRichTextString(" ");
            celdacateT2.setCellValue(textoT52);


            HSSFCell celdamarcaT2 = filaTitulo2.createCell((short)3);
            HSSFRichTextString textoT62 = new HSSFRichTextString("Precio Total: "+pedido.getprecioTotal());
            celdamarcaT2.setCellValue(textoT62);



            HSSFCell celdaCantT2 = filaTitulo2.createCell((short)4);
            HSSFRichTextString textoT2 = new HSSFRichTextString(" ");
            celdaCantT2.setCellValue(textoT2);




            HSSFRow filaTitulo3 = hoja.createRow(2);

            HSSFCell celdaprodT23 = filaTitulo3.createCell((short)0);
            HSSFRichTextString textoT723 = new HSSFRichTextString("PRODUCTO");
            celdaprodT23.setCellValue(textoT723);


            HSSFCell celdalineaT23 = filaTitulo3.createCell((short)1);
            HSSFRichTextString textoT423 = new HSSFRichTextString("PRESENTACION");
            celdalineaT23.setCellValue(textoT423);


            HSSFCell celdacateT23 = filaTitulo3.createCell((short)2);
            HSSFRichTextString textoT523 = new HSSFRichTextString("CANTIDAD");
            celdacateT23.setCellValue(textoT523);


            HSSFCell celdamarcaT23 = filaTitulo3.createCell((short)3);
            HSSFRichTextString textoT623 = new HSSFRichTextString("COSTO");
            celdamarcaT23.setCellValue(textoT623);



            HSSFCell celdaCantT23 = filaTitulo3.createCell((short)4);
            HSSFRichTextString textoT23 = new HSSFRichTextString("COSTO TOTAL");
            celdaCantT23.setCellValue(textoT23);


            int cont = 3;

            for(FabricanteDTO element : pedido.getlist()){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(" ");
                celdaprod.setCellValue(texto7);


                HSSFCell celdalinea = fila.createCell((short)1);
                HSSFRichTextString texto4 = new HSSFRichTextString(" ");
                celdalinea.setCellValue(texto4);


                HSSFCell celdacate = fila.createCell((short)2);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getmarca().toUpperCase());
                celdacate.setCellValue(texto5);


                HSSFCell celdamarca = fila.createCell((short)3);
                HSSFRichTextString texto6 = new HSSFRichTextString(" ");
                celdamarca.setCellValue(texto6);



                HSSFCell celdaCant = fila.createCell((short)4);
                HSSFRichTextString texto = new HSSFRichTextString(" ");
                celdaCant.setCellValue(texto);

                cont++;


                for(DetallePedidoDTO item : element.getdetalles()){
                    HSSFRow filaTmp = hoja.createRow(cont);

                    HSSFCell celdaprodTmp = filaTmp.createCell((short)0);
                    HSSFRichTextString texto7Tmp = new HSSFRichTextString(item.getPRODUCTO());
                    celdaprodTmp.setCellValue(texto7Tmp);


                    HSSFCell celdalineaTmp = filaTmp.createCell((short)1);
                    HSSFRichTextString texto4Tmp = new HSSFRichTextString(item.getPRESENTACION());
                    celdalineaTmp.setCellValue(texto4Tmp);


                    HSSFCell celdacateTmp = filaTmp.createCell((short)2);
                    HSSFRichTextString texto5Tmp = new HSSFRichTextString(item.getCANTIDAD()+"");
                    celdacateTmp.setCellValue(texto5Tmp);


                    HSSFCell celdamarcaTmp = filaTmp.createCell((short)3);
                    HSSFRichTextString texto6Tmp = new HSSFRichTextString(item.getCOSTO()+"");
                    celdamarcaTmp.setCellValue(texto6Tmp);


                    HSSFCell celdaCantTmp = filaTmp.createCell((short)4);
                    HSSFRichTextString textoTmp = new HSSFRichTextString(item.getCOSTOTOTAL()+"");
                    celdaCantTmp.setCellValue(textoTmp);

                    cont++;

                }

                HSSFRow filaTmp = hoja.createRow(cont);

                HSSFCell celdaprodTmp = filaTmp.createCell((short)0);
                HSSFRichTextString texto7Tmp = new HSSFRichTextString(" ");
                celdaprodTmp.setCellValue(texto7Tmp);


                HSSFCell celdalineaTmp = filaTmp.createCell((short)1);
                HSSFRichTextString texto4Tmp = new HSSFRichTextString("Precio Marca: "+element.getprecioTotal());
                celdalineaTmp.setCellValue(texto4Tmp);


                HSSFCell celdacateTmp = filaTmp.createCell((short)2);
                HSSFRichTextString texto5Tmp = new HSSFRichTextString(" ");
                celdacateTmp.setCellValue(texto5Tmp);


                HSSFCell celdamarcaTmp = filaTmp.createCell((short)3);
                HSSFRichTextString texto6Tmp = new HSSFRichTextString("Costo Marca: "+element.getcostoTotal());
                celdamarcaTmp.setCellValue(texto6Tmp);


                HSSFCell celdaCantTmp = filaTmp.createCell((short)4);
                HSSFRichTextString textoTmp = new HSSFRichTextString(" ");
                celdaCantTmp.setCellValue(textoTmp);

                cont++;


                HSSFRow filaTmp2 = hoja.createRow(cont);

                HSSFCell celdaprodTmp2 = filaTmp2.createCell((short)0);
                HSSFRichTextString texto7Tmp2 = new HSSFRichTextString(" ");
                celdaprodTmp2.setCellValue(texto7Tmp2);


                HSSFCell celdalineaTmp2 = filaTmp2.createCell((short)1);
                HSSFRichTextString texto4Tmp2 = new HSSFRichTextString(" ");
                celdalineaTmp2.setCellValue(texto4Tmp2);


                HSSFCell celdacateTmp2 = filaTmp2.createCell((short)2);
                HSSFRichTextString texto5Tmp2 = new HSSFRichTextString(" ");
                celdacateTmp2.setCellValue(texto5Tmp2);


                HSSFCell celdamarcaTmp2 = filaTmp2.createCell((short)3);
                HSSFRichTextString texto6Tmp2 = new HSSFRichTextString(" ");
                celdamarcaTmp2.setCellValue(texto6Tmp2);


                HSSFCell celdaCantTmp2 = filaTmp2.createCell((short)4);
                HSSFRichTextString textoTmp2 = new HSSFRichTextString(" ");
                celdaCantTmp2.setCellValue(textoTmp2);

                cont++;
            }

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero;
            if(PruebasLocales){
                elFichero = new FileOutputStream("./"+Name+"pickingConsolidado.xls");
            }else{
                elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"pickingConsolidado.xls");
            }
            libro.write(elFichero);
            elFichero.close();
            response = Name+"pickingConsolidado.xls";
        }catch (Exception e){
            //System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, String> postExcelDomicilio(String date1, String date2, Long id_store){
        List<DomicilioExcelRow> listaElementos = this.pedidosDAO.getExcelDomicilios(new Date(date1), new Date(date2), id_store);

        System.out.println("SIZE LIST: "+listaElementos.size());

        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);


        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT23 = filaTitulo.createCell((short)0);
            HSSFRichTextString textoT723 = new HSSFRichTextString("VENDEDOR");
            celdaprodT23.setCellValue(textoT723);

            HSSFCell celdalineaT23 = filaTitulo.createCell((short)1);
            HSSFRichTextString textoT423 = new HSSFRichTextString("CLIENTE");
            celdalineaT23.setCellValue(textoT423);

            HSSFCell celdacateT23 = filaTitulo.createCell((short)2);
            HSSFRichTextString textoT523 = new HSSFRichTextString("ESTADO");
            celdacateT23.setCellValue(textoT523);

            HSSFCell celdamarcaT23 = filaTitulo.createCell((short)3);
            HSSFRichTextString textoT623 = new HSSFRichTextString("BODEGA");
            celdamarcaT23.setCellValue(textoT623);

            HSSFCell celdaCantT23 = filaTitulo.createCell((short)4);
            HSSFRichTextString textoT23 = new HSSFRichTextString("ORDEN_DE_COMPRA");
            celdaCantT23.setCellValue(textoT23);

            HSSFCell celdaCantT231 = filaTitulo.createCell((short)5);
            HSSFRichTextString textoT231 = new HSSFRichTextString("FECHA");
            celdaCantT231.setCellValue(textoT231);

            HSSFCell celdaCantT232 = filaTitulo.createCell((short)6);
            HSSFRichTextString textoT232 = new HSSFRichTextString("FECHA_ENTREGA");
            celdaCantT232.setCellValue(textoT232);

            HSSFCell celdaCantT233 = filaTitulo.createCell((short)7);
            HSSFRichTextString textoT233 = new HSSFRichTextString("OBSERVACION");
            celdaCantT233.setCellValue(textoT233);

            HSSFCell celdaCantT234 = filaTitulo.createCell((short)8);
            HSSFRichTextString textoT234 = new HSSFRichTextString("CODIGO_DEL_PRODUCTO");
            celdaCantT234.setCellValue(textoT234);

            HSSFCell celdaCantT235 = filaTitulo.createCell((short)9);
            HSSFRichTextString textoT235 = new HSSFRichTextString("CANTIDAD");
            celdaCantT235.setCellValue(textoT235);

            HSSFCell celdaCantT236 = filaTitulo.createCell((short)10);
            HSSFRichTextString textoT236 = new HSSFRichTextString("DESCUENTO");
            celdaCantT236.setCellValue(textoT236);

            HSSFCell celdaCantT237 = filaTitulo.createCell((short)11);
            HSSFRichTextString textoT237 = new HSSFRichTextString("DESCUENT");
            celdaCantT237.setCellValue(textoT237);

            HSSFCell celdaCantT238 = filaTitulo.createCell((short)12);
            HSSFRichTextString textoT238 = new HSSFRichTextString("TIPO");
            celdaCantT238.setCellValue(textoT238);

            HSSFCell celdaCantT239 = filaTitulo.createCell((short)13);
            HSSFRichTextString textoT239 = new HSSFRichTextString("VALOR");
            celdaCantT239.setCellValue(textoT239);

            HSSFCell celdaCantT2311 = filaTitulo.createCell((short)14);
            HSSFRichTextString textoT2311 = new HSSFRichTextString("UNIDAD");
            celdaCantT2311.setCellValue(textoT2311);

            HSSFCell celdaCantT2322 = filaTitulo.createCell((short)15);
            HSSFRichTextString textoT2322 = new HSSFRichTextString("LISTA");
            celdaCantT2322.setCellValue(textoT2322);

            int cont = 1;

            for(DomicilioExcelRow element : listaElementos){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getVENDEDOR());
                celdaprod.setCellValue(texto7);

                HSSFCell celdalinea = fila.createCell((short)1);
                HSSFRichTextString texto4 = new HSSFRichTextString(element.getCLIENTE());
                celdalinea.setCellValue(texto4);

                HSSFCell celdacate = fila.createCell((short)2);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getESTADO());
                celdacate.setCellValue(texto5);

                HSSFCell celdamarca = fila.createCell((short)3);
                HSSFRichTextString texto6 = new HSSFRichTextString(element.getBODEGA());
                celdamarca.setCellValue(texto6);

                HSSFCell celdaCant = fila.createCell((short)4);
                HSSFRichTextString texto = new HSSFRichTextString(element.getORDEN_DE_COMPRA());
                celdaCant.setCellValue(texto);

                HSSFCell celdaCantc = fila.createCell((short)5);
                HSSFRichTextString textoc = new HSSFRichTextString(element.getFECHA());
                celdaCantc.setCellValue(textoc);

                HSSFCell celdaCants = fila.createCell((short)6);
                HSSFRichTextString textos = new HSSFRichTextString(element.getFECHA_ENTREGA());
                celdaCants.setCellValue(textos);

                HSSFCell celdaCantq = fila.createCell((short)7);
                HSSFRichTextString textoq = new HSSFRichTextString(element.getOBSERVACION());
                celdaCantq.setCellValue(textoq);

                HSSFCell celdaCantw = fila.createCell((short)8);
                HSSFRichTextString textow = new HSSFRichTextString(element.getCODIGO_DEL_PRODUCTO());
                celdaCantw.setCellValue(textow);

                HSSFCell celdaCantr = fila.createCell((short)9);
                HSSFRichTextString textor = new HSSFRichTextString(element.getCANTIDAD());
                celdaCantr.setCellValue(textor);

                HSSFCell celdaCantt = fila.createCell((short)10);
                HSSFRichTextString textot = new HSSFRichTextString(element.getDESCUENTO());
                celdaCantt.setCellValue(textot);

                HSSFCell celdaCantqw = fila.createCell((short)11);
                HSSFRichTextString textoqw = new HSSFRichTextString(element.getDESCUENT());
                celdaCantqw.setCellValue(textoqw);

                HSSFCell celdaCantwe = fila.createCell((short)12);
                HSSFRichTextString textowe = new HSSFRichTextString(element.getTIPO());
                celdaCantwe.setCellValue(textowe);

                HSSFCell celdaCantqe = fila.createCell((short)13);
                HSSFRichTextString textoqe = new HSSFRichTextString(element.getVALOR());
                celdaCantqe.setCellValue(textoqe);

                HSSFCell celdaCantqwq = fila.createCell((short)14);
                HSSFRichTextString textoqwq = new HSSFRichTextString(element.getUNIDAD());
                celdaCantqwq.setCellValue(textoqwq);

                HSSFCell celdaCantg = fila.createCell((short)15);
                HSSFRichTextString textog = new HSSFRichTextString(element.getLISTA());
                celdaCantg.setCellValue(textog);

                cont++;



                }

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero;
            if(PruebasLocales){
                elFichero = new FileOutputStream("./"+Name+"pedidoDomicilio.xls");
            }else{
                elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"pedidoDomicilio.xls");
            }
            libro.write(elFichero);
            elFichero.close();
            response = Name+"pedidoDomicilio.xls";
        }catch (Exception e){
            //System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, String> detailintaut(List<reorderProd> reorder,Long idstore) {

        String Response = "";
        try {
            List<TablaPedidos> validator = pedidosDAO.getPedidosT();
            TablaPedidos tp = validator.get(0);
            for (TablaPedidos ele:validator) {
                if(ele.getID_STORE_CLIENT() == idstore){
                    tp = ele;
                }
            }

            List<reorderProd> list = reorder;
            if(!list.isEmpty()){
                pedidosDAO.create(tp.getID_THIRD_EMPLOYEE(),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),new Double(0),new Double(0),new Double(0),new Long(61),new Long(87),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER());
                Long billClientId = pedidosDAO.getPkLast();
                //String numdoc = pedidosDAO.getNumDoc(billClientId);
                //pedidosDAO.create2(tp.getID_THIRD_EMP_PROVIDER(),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER(),new Double(0),new Double(0),new Double(0),new Long(62),new Long(86),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),numdoc);
                //Long billProviderID = pedidosDAO.getPkLast();
                //System.out.println("this is id: "+billClientId);
                for(reorderProd rp: list){
                    try{

                        Long idpsC = pedidosDAO.getPsId(rp.getOWNBARCODE(),tp.getID_STORE_CLIENT());
                        pedidosDAO.createDetail(billClientId,rp.getCANTIDAD(),idpsC,new Double(rp.getCOSTO()/rp.getCANTIDAD()),rp.getPERCENT());
                        Long idDetail = pedidosDAO.getPkLastDetail();
                        //pedidosDAO.procedure(billClientId,idpsC);
                        //Long quantity = pedidosDAO.detailData(idDetail).getQUANTITY();
                        //Long idpsP = pedidosDAO.getPsId(pedidosDAO.getOwnBarCode(pedidosDAO.detailData(idDetail).getID_PRODUCT_STORE()),tp.getID_STORE_PROVIDER());
                        //pedidosDAO.createDetail(billProviderID,quantity,idpsP,new Double(0),new Double(0));

                    }catch(Exception e2){
                        Response+=rp.getOWNBARCODE()+",";
                    }
                }
                pedidosDAO.procedure2(billClientId);

            }


            return Either.right("ok!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public String detailintaut2() {

        String Response = "";
        try {
            List<TablaPedidos> validator = pedidosDAO.getPedidosT();

            for (TablaPedidos tp:validator) {

                //System.out.println("--------------------------------------");
                //System.out.println(tp.getID_STORE_CLIENT());
                //System.out.println(tp.getID_THIRD_CLIENT());
                //System.out.println(tp.getID_STORE_PROVIDER());
                //System.out.println(tp.getID_THIRD_DESTINITY());
                //System.out.println(tp.getID_THIRD_EMPLOYEE());
                //System.out.println(tp.getID_THIRD_EMP_PROVIDER());
                //System.out.println("--------------------------------------");

                List<reorderProd> list = reorderDAO.getReorderData(tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(), new Date(), new Date());

                //System.out.println("THE LIST IS EMPTY?"+list.isEmpty());
                //System.out.println("************************************");
                for(reorderProd elem : list){
                    //System.out.println(elem.getOWNBARCODE());
                }
                //System.out.println("************************************");
                /*if(!list.isEmpty()){
                    pedidosDAO.create(tp.getID_THIRD_EMPLOYEE(),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),new Double(0),new Double(0),new Double(0),new Long(61),new Long(87),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER());
                    Long billClientId = pedidosDAO.getPkLast();
                    String numdoc = pedidosDAO.getNumDoc(billClientId);
                    pedidosDAO.create2(tp.getID_THIRD_EMP_PROVIDER(),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER(),new Double(0),new Double(0),new Double(0),new Long(62),new Long(86),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),numdoc);
                    Long billProviderID = pedidosDAO.getPkLast();
                    for(reorderProd rp: list){
                        try{
                            Long idpsC = pedidosDAO.getPsId(rp.getOWNBARCODE(),tp.getID_STORE_CLIENT());
                            pedidosDAO.createDetail(billClientId,rp.getCANTIDAD(),idpsC,new Double(0),new Double(0));

                            Long idDetail = pedidosDAO.getPkLastDetail();
                            //System.out.println("------------------------------------");
                            //System.out.println(pedidosDAO.detailData(idDetail).getID_PRODUCT_STORE());
                            pedidosDAO.procedure(billClientId,idpsC);
                            Long quantity = pedidosDAO.detailData(idDetail).getQUANTITY();
                            Long idpsP = pedidosDAO.getPsId(pedidosDAO.getOwnBarCode(pedidosDAO.detailData(idDetail).getID_PRODUCT_STORE()),tp.getID_STORE_PROVIDER());
                            pedidosDAO.createDetail(billProviderID,quantity,idpsP,new Double(0),new Double(0));
                        }catch(Exception e2){
                               Response+=rp.getOWNBARCODE()+",";
                        }
                    }
                }
*/
                if(!list.isEmpty()){
                    pedidosDAO.create(tp.getID_THIRD_EMPLOYEE(),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),new Double(0),new Double(0),new Double(0),new Long(61),new Long(87),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER());
                    Long billClientId = pedidosDAO.getPkLast();
                    //String numdoc = pedidosDAO.getNumDoc(billClientId);
                    //pedidosDAO.create2(tp.getID_THIRD_EMP_PROVIDER(),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER(),new Double(0),new Double(0),new Double(0),new Long(62),new Long(86),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),numdoc);
                    //Long billProviderID = pedidosDAO.getPkLast();
                    //System.out.println("this is id: "+billClientId);
                    for(reorderProd rp: list){
                        try{

                            Long idpsC = pedidosDAO.getPsId(rp.getOWNBARCODE(),tp.getID_STORE_CLIENT());
                            pedidosDAO.createDetail(billClientId,rp.getCANTIDAD(),idpsC,new Double(rp.getCOSTO()/rp.getCANTIDAD()),rp.getPERCENT());
                            Long idDetail = pedidosDAO.getPkLastDetail();
                            //pedidosDAO.procedure(billClientId,idpsC);
                            //Long quantity = pedidosDAO.detailData(idDetail).getQUANTITY();
                            //Long idpsP = pedidosDAO.getPsId(pedidosDAO.getOwnBarCode(pedidosDAO.detailData(idDetail).getID_PRODUCT_STORE()),tp.getID_STORE_PROVIDER());
                            //pedidosDAO.createDetail(billProviderID,quantity,idpsP,new Double(0),new Double(0));

                        }catch(Exception e2){
                            Response+=rp.getOWNBARCODE()+",";
                        }
                    }
                    pedidosDAO.procedure2(billClientId);

                }

            }
            //System.out.println("ENVIO DE PEDIDOS TERMINADO");
            return Response;

        }catch (Exception e){
            //System.out.println(new TechnicalException(messages_error(e.toString())));
            Response = e.toString();
            return Response;
        }
    }


    public Long detailint(Long id_store_client, Long id_store_provider, String date1, String date2, Double hours, Double hours2) {

        Long Response = new Long(1);
        try {
            List<TablaPedidos> validator = pedidosDAO.getPedidosT2(id_store_client, id_store_provider);

            for (TablaPedidos tp:validator) {

                //System.out.println("--------------------------------------");
                //System.out.println(tp.getID_STORE_CLIENT());
                //System.out.println(tp.getID_THIRD_CLIENT());
                //System.out.println(tp.getID_STORE_PROVIDER());
                //System.out.println(tp.getID_THIRD_DESTINITY());
                //System.out.println(tp.getID_THIRD_EMPLOYEE());
                //System.out.println(tp.getID_THIRD_EMP_PROVIDER());
                //System.out.println("--------------------------------------");
                //System.out.println(new Date(date1));
                //System.out.println(new Date(date2));

                List<reorderProd> list = reorderDAO.getReorderData2(tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(), new Date(date1), new Date(date2),hours,hours2);

                //System.out.println("THE LIST IS EMPTY?"+list.isEmpty());
                //System.out.println("************************************");
                for(reorderProd elem : list){
                    //System.out.println(elem.getOWNBARCODE());
                }
                //System.out.println("************************************");
                /*if(!list.isEmpty()){
                    pedidosDAO.create(tp.getID_THIRD_EMPLOYEE(),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),new Double(0),new Double(0),new Double(0),new Long(61),new Long(87),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER());
                    Long billClientId = pedidosDAO.getPkLast();
                    String numdoc = pedidosDAO.getNumDoc(billClientId);
                    pedidosDAO.create2(tp.getID_THIRD_EMP_PROVIDER(),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER(),new Double(0),new Double(0),new Double(0),new Long(62),new Long(86),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),numdoc);
                    Long billProviderID = pedidosDAO.getPkLast();
                    for(reorderProd rp: list){
                        try{
                            Long idpsC = pedidosDAO.getPsId(rp.getOWNBARCODE(),tp.getID_STORE_CLIENT());
                            pedidosDAO.createDetail(billClientId,rp.getCANTIDAD(),idpsC,new Double(0),new Double(0));

                            Long idDetail = pedidosDAO.getPkLastDetail();
                            //System.out.println("------------------------------------");
                            //System.out.println(pedidosDAO.detailData(idDetail).getID_PRODUCT_STORE());
                            pedidosDAO.procedure(billClientId,idpsC);
                            Long quantity = pedidosDAO.detailData(idDetail).getQUANTITY();
                            Long idpsP = pedidosDAO.getPsId(pedidosDAO.getOwnBarCode(pedidosDAO.detailData(idDetail).getID_PRODUCT_STORE()),tp.getID_STORE_PROVIDER());
                            pedidosDAO.createDetail(billProviderID,quantity,idpsP,new Double(0),new Double(0));
                        }catch(Exception e2){
                               Response+=rp.getOWNBARCODE()+",";
                        }
                    }
                }
*/
                if(!list.isEmpty()){
                    pedidosDAO.create(tp.getID_THIRD_EMPLOYEE(),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),new Double(0),new Double(0),new Double(0),new Long(61),new Long(87),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER());
                    Long billClientId = pedidosDAO.getPkLast();
                    //String numdoc = pedidosDAO.getNumDoc(billClientId);
                    //pedidosDAO.create2(tp.getID_THIRD_EMP_PROVIDER(),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER(),new Double(0),new Double(0),new Double(0),new Long(62),new Long(86),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),numdoc);
                    //Long billProviderID = pedidosDAO.getPkLast();
                    //System.out.println("this is id: "+billClientId);
                    for(reorderProd rp: list){
                        try{

                            Long idpsC = pedidosDAO.getPsId(rp.getOWNBARCODE(),tp.getID_STORE_CLIENT());
                            pedidosDAO.createDetail(billClientId,rp.getCANTIDAD(),idpsC,new Double(rp.getCOSTO()/rp.getCANTIDAD()),rp.getPERCENT());
                            Long idDetail = pedidosDAO.getPkLastDetail();
                            //pedidosDAO.procedure(billClientId,idpsC);
                            //Long quantity = pedidosDAO.detailData(idDetail).getQUANTITY();
                            //Long idpsP = pedidosDAO.getPsId(pedidosDAO.getOwnBarCode(pedidosDAO.detailData(idDetail).getID_PRODUCT_STORE()),tp.getID_STORE_PROVIDER());
                            //pedidosDAO.createDetail(billProviderID,quantity,idpsP,new Double(0),new Double(0));

                        }catch(Exception e2){
                            e2.printStackTrace();
                        }
                    }
                    pedidosDAO.procedure2(billClientId);

                }

            }
            //System.out.println("ENVIO DE PEDIDOS TERMINADO");
            return Response;

        }catch (Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }

    public Either<IException, Long> create(Long id_third_employee,
                                           Long id_third,
                                           Long id_store,
                                           Double totalprice,
                                           Double subtotal,
                                           Double tax,
                                           Long id_bill_state,
                                           Long id_bill_type,
                                           Long id_third_destinity,
                                           Long id_store_cliente) {
        try {
            pedidosDAO.create( id_third_employee,
                    id_third,
                    id_store,
                    totalprice,
                    subtotal,
                    tax,
                    id_bill_state,
                    id_bill_type,
                    id_third_destinity,
                    id_store_cliente);
            Long validator = pedidosDAO.getPkLast();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> create2(Long id_third_employee,
                                            Long id_third,
                                            Long id_store,
                                            Double totalprice,
                                            Double subtotal,
                                            Double tax,
                                            Long id_bill_state,
                                            Long id_bill_type,
                                            Long id_third_destinity,
                                            Long id_store_cliente,
                                            String num_doc) {
        try {
            pedidosDAO.create2( id_third_employee,
                    id_third,
                    id_store,
                    totalprice,
                    subtotal,
                    tax,
                    id_bill_state,
                    id_bill_type,
                    id_third_destinity,
                    id_store_cliente,
                    num_doc);
            Long validator = pedidosDAO.getPkLast();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> createDetail(Long id_bill, Long cantidad, Long id_ps, Double price, Double tax) {
        try {
            pedidosDAO.createDetail( id_bill,
                    cantidad,
                    id_ps,
                    price,
                    tax);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> getPrice(Long id_ps) {
        try {
            //System.out.println("THIS IS THE PS I NEED: "+id_ps);
            List<Long> listPrices = pedidosDAO.getPrice(id_ps);
            Long smallest = listPrices.get(0);
            //System.out.println(smallest);
            for(Long elem: listPrices){
                if(elem<=smallest){
                    smallest = elem;
                }
            }
            Long validator = smallest;
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> getNumDoc(Long idbill) {
        try {


            String validator = pedidosDAO.getNumDoc(idbill);;
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long>  addNotesBill(String notas,Long idbc,String numpedido, String state, Long idstore) {
        try {
            Long idprov = pedidosDAO.getIdC(idstore,numpedido);

            //System.out.println("NOTAS: " + notas);
            //System.out.println("IDBC: " + idbc);
            //System.out.println("IDPROV: " + idprov);
            //System.out.println("STATE: " + state);
            //System.out.println("IDSTORE: " + idstore);

            pedidosDAO.addNotesBill( notas, idbc, idprov, state);

            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long>  procedureV2(Long idbpedido,
                                                 Long idstoreprov,
                                                 Long idstoreclient,
                                                 Long idplanilla) {
        try {
            pedidosDAO.procedureV2( idbpedido,
                    idstoreprov,
                    idstoreclient,
                    idplanilla);

            //System.out.println("idbpedido: " + idbpedido);
            //System.out.println("idstoreprov: " + idstoreprov);
            //System.out.println("idstoreclient: " + idstoreclient);
            //System.out.println("idplanilla: " + idplanilla);


            Long idBillResp = pedidosDAO.getBillVentPedido(idbpedido, idstoreprov, idstoreclient);

            //System.out.println("idBillResp: "+idBillResp);

            Long validator = new Long (idBillResp);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long>  crear_pedido(Long idthirdclient,
                                                  Long idstoreclient,
                                                  Long idthirdempclient,
                                                  Long idthirdprov,
                                                  Long idstoreprov,
                                                  String detallepedido) {
        try { pedidosDAO.crear_pedido( idthirdclient,
                idstoreclient,
                idthirdempclient,
                idthirdprov,
                idstoreprov,
                detallepedido);
            Long response = pedidosDAO.getLastClient();
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long>  crear_pedido_costo(Long idthirdclient,
                                                        Long idstoreclient,
                                                        Long idthirdempclient,
                                                        Long idthirdprov,
                                                        Long idstoreprov,
                                                        String detallepedido) {
        try { pedidosDAO.crear_pedido_costo( idthirdclient,
                idstoreclient,
                idthirdempclient,
                idthirdprov,
                idstoreprov,
                detallepedido);
            Long response = pedidosDAO.getLastClient();
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long>  generar_pedidos_por_consumo(String fechainicio,
                                                                 Long horainicio,
                                                                 String fechafin,
                                                                 Long horafin,
                                                                 Long idstoreprov) {
        try { pedidosDAO.generar_pedidos_por_consumo(new Date(fechainicio),
                 horainicio,
                 new Date(fechafin),
                 horafin,
                 idstoreprov);
            return Either.right(new Long(1));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> putInventoryBill(Long quantity, Long id_product_store) {
        try {
            pedidosDAO.putInventoryBill( quantity,
                    id_product_store,
                    pedidosDAO.getStorage(id_product_store));
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> callProcedure2(Long idbill) {
        try {
            pedidosDAO.procedure2(idbill);
            Long validator = new Long (1);
            validator = pedidosDAO.getPkLast();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> completeManualBill(Long idstore, Long idbill) {
        try {
            pedidosDAO.completeManualBill( idstore, idbill );
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> getpsid(String code, Long id_store) {
        try {
            return Either.right(pedidosDAO.getPsId(code, id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> putInventoryBill2(Long quantity, Long id_product_store) {
        try {
            pedidosDAO.putInventoryBill2( quantity,
                    id_product_store,
                    pedidosDAO.getStorage(id_product_store));
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> getTax(Long id_product_store) {
        try {
            Long validator = pedidosDAO.getTax(id_product_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, BillToUpdate> getBillIdByIdStore(Long idstore, Long idstoreclient, String numpedido) {
        try {
            BillToUpdate validator = pedidosDAO.getBillIdByIdStore(idstore,idstoreclient,numpedido);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> updateBillState(Long billstate, Long billid) {
        try {
            pedidosDAO.updateBillState(billstate,billid);
            return Either.right(new Long(1));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> updateBillTotal(Long total, Long idbil) {
        try {
            pedidosDAO.updateBillTotal(total, idbil);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> updateBillTotal21(Long idbil) {
        try {

            List<Long> billList = new ArrayList<>();
            billList.add(idbil);
            Double total = new Double(0);
            Double subtotal = new Double(0);
            int tax = 0;

            List<DetallePedido> details = pedidosDAO.getDetallesPedidos2(billList);

            for(DetallePedido detail: details){
                total += detail.getCOSTO()*detail.getCANTIDAD();
                subtotal+= detail.getCOSTO()*detail.getCANTIDAD();
            }

            pedidosDAO.updateBillTotalandSubtotal21(idbil);
            Long validator = new Long (idbil);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> updateBillTotal22(Long idbil) {
        try {

            List<Long> billList = new ArrayList<>();
            billList.add(idbil);
            Double total = new Double(0);
            Double subtotal = new Double(0);
            int tax = 0;

            List<DetallePedido> details = pedidosDAO.getDetallesPedidos2(billList);

            for(DetallePedido detail: details){
                total += detail.getCOSTO()*detail.getCANTIDAD();
                subtotal+= detail.getCOSTO()*detail.getCANTIDAD();
            }

            pedidosDAO.updateBillTotalandSubtotal22(idbil);
            Long validator = new Long (idbil);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }

    //PICKING CONSOLIDADO
    public Either<IException, String> printPickingPDF(PickingConsolidadoPdfDTO pedido) {
        Document documento = new Document(PageSize.A4, 0f, 0f, 30f, 37f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        DecimalFormat df = new DecimalFormat("###.##");
        String ruta = "";
        FileOutputStream ficheroPdf = null;
        try{
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);

            Font fontH1 = new Font(HELVETICA, 7, Font.NORMAL);
            Font fontH3 = new Font(HELVETICA, 6, Font.BOLD);
            Font fontH4 = new Font(HELVETICA, 8, Font.NORMAL);
            Font fontH5 = new Font(HELVETICA,  10,Font.BOLD);

            String nombrePdf = "picking.pdf_tmp.pdf";

            if(PruebasLocales){
                ruta = new File(".").getCanonicalPath()+"/"+ nombrePdf;
            }else{
                ruta = "/usr/share/nginx/html/remisiones/"+ nombrePdf;
            }
            ficheroPdf = new FileOutputStream(ruta);

            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));



            //DEFINO UNA CELDA VACIA PARA USO DE SALTO DE LINEA
            PdfPCell cellblanks =new PdfPCell(new Phrase(" "));
            cellblanks.setBorder(PdfPCell.NO_BORDER);
            cellblanks.setHorizontalAlignment(Element.ALIGN_CENTER);



            //DEFINO LA TABLA PRINCIPAL
            float AnchoTablas = 90;
            PdfPTable table = new PdfPTable(1);
            table.setWidthPercentage(AnchoTablas);


            //INGRESO EL ICONO
            try {
                Image img;
                if(PruebasLocales){
                    img = Image.getInstance(new File(".").getCanonicalPath()+"/"+pedido.getLogo() +".jpg");
                }else{
                    img = Image.getInstance("/usr/share/nginx/html/logos/" + pedido.getLogo() + ".jpg");
                }
                img.scalePercent(50);
                Rectangle pageSize = documento.getPageSize();
                img.setAbsolutePosition(pageSize.getRight(0) - (img.getWidth()/2), pageSize.getTop(0) - (img.getHeight()/2));
                //img.setAbsolutePosition(500f, 500f);
                documento.add(img);
            } catch (Exception e) {
                //System.out.println(e.toString());

                Image img;
                img = Image.getInstance("/usr/share/nginx/html/logos/tienda724.jpg");
                img.scalePercent(50);
                Rectangle pageSize = documento.getPageSize();
                img.setAbsolutePosition(pageSize.getRight(0) - (img.getWidth()/2), pageSize.getTop(0) - (img.getHeight()/2));
                //img.setAbsolutePosition(500f, 500f);
                documento.add(img);
            }



            //DEFINO LA TABLA QUE CONTIENE EL TITULO DEL DOCUMENTO
            PdfPTable tablaTitulo = new PdfPTable(1);
            tablaTitulo.setWidthPercentage(AnchoTablas);
            PdfPCell cellTitulo =new PdfPCell(new Phrase("Documento de picking Consolidado",fontH3));
            cellTitulo.setBorder(PdfPCell.NO_BORDER);
            cellTitulo.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaTitulo.addCell(cellTitulo);
            tablaTitulo.addCell(cellblanks);
            tablaTitulo.setKeepTogether(false);
            tablaTitulo.setSplitLate(false);
            PdfPCell titleCell = new PdfPCell(tablaTitulo);
            titleCell.setBorder(PdfPCell.NO_BORDER);
            titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(titleCell);


            //DEFINO LA TABLA QUE CONTIENE LOS COSTOS Y PRECIOS TOTALES DEL DOCUMENTO
            PdfPTable tablaTotales = new PdfPTable(1);
            tablaTotales.setWidthPercentage(AnchoTablas);
            PdfPCell cellTotalesPreciosTitulo =new PdfPCell(new Phrase("Precio Total",fontH5));
            cellTotalesPreciosTitulo.setBorder(PdfPCell.NO_BORDER);
            cellTotalesPreciosTitulo.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaTotales.addCell(cellTotalesPreciosTitulo);
            PdfPCell cellTotalesCostos =new PdfPCell(new Phrase(format.format(roundAvoid(pedido.getcostoTotal(),2))+"",fontH4));
            cellTotalesCostos.setBorder(PdfPCell.NO_BORDER);
            cellTotalesCostos.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaTotales.addCell(cellTotalesCostos);
            tablaTotales.addCell(cellblanks);
            tablaTotales.addCell(cellblanks);
            tablaTotales.setKeepTogether(false);
            tablaTotales.setSplitLate(false);
            PdfPCell titleTotals = new PdfPCell(tablaTotales);
            titleTotals.setBorder(PdfPCell.NO_BORDER);
            titleTotals.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(titleTotals);

            //DEFINO LA TABLA QUE CONTIENE YA LOS DATOS DEL PDF
            PdfPTable tablaDetalles2 = new PdfPTable(9);
            tablaDetalles2.setWidthPercentage(AnchoTablas);
            float[] AnchosTabla = new float[]{1.7f,2f,1,1.2f,1,1,2,1.5f,1.7f};
            tablaDetalles2.setWidths(AnchosTabla);

            //Columnas

            CellMaker("C. BARRAS",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles2);
            CellMaker("PRODUCTO",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles2);
            CellMaker("MARCA",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles2);
            CellMaker("CATEGORIA",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles2);
            CellMaker("LINEA",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles2);
            CellMaker("CANTIDAD",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles2);
            CellMaker("FABRICANTE",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles2);
            CellMaker("PRESENTACIÓN",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles2);
            CellMaker("PRECIO TOTAL",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles2);

            tablaDetalles2.setKeepTogether(false);
            tablaDetalles2.setSplitLate(false);
            PdfPCell cellDetailTable2 = new PdfPCell(tablaDetalles2);
            cellDetailTable2.setBorder(PdfPCell.NO_BORDER);
            cellDetailTable2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cellDetailTable2);


            //DEFINO EL CICLO PARA RECORRER LOS FABRICANTES
            int R = 180;
            int G = 180;
            int B = 180;
            int TotalUnidades = 0;
            for(FabricanteDTO temporal : pedido.getlist()){
                PdfPTable tablaFabTemp = new PdfPTable(1);
                tablaFabTemp.setWidthPercentage(AnchoTablas);

                Font fontH3TMP = new Font(fontH3.getBaseFont(), fontH3.getSize(), Font.NORMAL);

                //DEFINO LA CELDA QUE INCLUIRA EL TITULO DE LA MARCA
                CellMaker("FABRICANTE: "+temporal.getmarca().toUpperCase(),fontH3TMP,PdfPCell.TOP | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_CENTER,tablaFabTemp,new BaseColor(R,G,B));

                PdfPTable tablaDetalles = new PdfPTable(9);
                tablaDetalles.setWidthPercentage(AnchoTablas);
                tablaDetalles.setWidths(AnchosTabla);

                //DEFINO EL CICLO PARA LOS DETALLES DE LA MARCA
                for(DetallePedidoDTO detail: temporal.getdetalles()) {

                    //C. Barras
                    CellMaker(detail.getownbarcode(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
                    //Producto
                    CellMaker(detail.getPRODUCTO(),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
                    //Marca
                    CellMaker(detail.getMARCA(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
                    //Categoria
                    CellMaker(detail.getCATEGORIA(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
                    //Linea
                    CellMaker(detail.getLINEA(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
                    //Cantidad
                    CellMaker(detail.getCANTIDAD()+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
                    //Fabricante
                    CellMaker(detail.getFABRICANTE(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
                    //Presentación
                    CellMaker(detail.getPRESENTACION(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
                    //Precio total
                    CellMaker(format.format(roundAvoid(detail.getCOSTOTOTAL(),2))+"",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetalles);
                    //
                    TotalUnidades += detail.getCANTIDAD();
                }
                tablaDetalles.setKeepTogether(false);
                tablaDetalles.setSplitLate(false);
                PdfPCell cellDetailTable = new PdfPCell(tablaDetalles);
                cellDetailTable.setBorder(PdfPCell.NO_BORDER);
                cellDetailTable.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaFabTemp.addCell(cellDetailTable);

                //DEFINO LA TABLA QUE CONTIENE LOS VALORES DE COSTO Y PRECIO DE LA MARCA
                PdfPTable tablaCostoPrecioTemp = new PdfPTable(1);
                tablaCostoPrecioTemp.setWidthPercentage(AnchoTablas);
                PdfPCell titlePrecioMarca =new PdfPCell(new Phrase("PRECIO DE "+temporal.getmarca().toUpperCase()+":",fontH3));
                titlePrecioMarca.setBorder(PdfPCell.BOTTOM);
                titlePrecioMarca.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaCostoPrecioTemp.addCell(titlePrecioMarca);
                PdfPCell CostoMarca =new PdfPCell(new Phrase(format.format(roundAvoid(temporal.getcostoTotal(),2))+"",fontH1));
                CostoMarca.setBorder(PdfPCell.BOTTOM);
                CostoMarca.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaCostoPrecioTemp.addCell(CostoMarca);
                tablaCostoPrecioTemp.setKeepTogether(false);
                tablaCostoPrecioTemp.setSplitLate(false);
                PdfPCell cellCosto = new PdfPCell(tablaCostoPrecioTemp);
                cellCosto.setBorder(PdfPCell.NO_BORDER);
                cellCosto.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaFabTemp.addCell(cellCosto);
                tablaFabTemp.setKeepTogether(false);
                tablaFabTemp.setSplitLate(false);
                PdfPCell tempCell = new PdfPCell(tablaFabTemp);
                tempCell.setBorder(PdfPCell.NO_BORDER);
                tempCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(tempCell);
            }
            //TOTAL CANTIDAD
            PdfPTable tablaTotalesFinal = new PdfPTable(3);
            tablaTotalesFinal.setWidthPercentage(AnchoTablas);

            tablaTotalesFinal.addCell(cellblanks); tablaTotalesFinal.addCell(cellblanks); tablaTotalesFinal.addCell(cellblanks); tablaTotalesFinal.addCell(cellblanks); tablaTotalesFinal.addCell(cellblanks); tablaTotalesFinal.addCell(cellblanks);

            CellMaker("",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_CENTER,tablaTotalesFinal);
            CellMaker("Total Unidades",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_CENTER,tablaTotalesFinal);
            CellMaker(TotalUnidades+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_CENTER,tablaTotalesFinal);

            tablaTotalesFinal.setKeepTogether(false);
            tablaTotalesFinal.setSplitLate(false);
            PdfPCell tablaTotalesC = new PdfPCell(tablaTotalesFinal);
            tablaTotalesC.setBorder(PdfPCell.NO_BORDER);
            table.addCell(tablaTotalesC);
            ////////
            table.setKeepTogether(false);
            table.setSplitLate(false);
            documento.add(table);
            response = nombrePdf.substring(0,nombrePdf.length()-8);

        }catch(Exception e){
            response = e.toString();
        }
        try {
            documento.close();
            ficheroPdf.close();
            PaginarPDF(ruta);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }

    }

    public Either<IException, String> printPDF3(PdfPedidosDTO pedidosDTO){
        Document documento = new Document(PageSize.A4, 0f, 0f, 35f, 0f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.


            String docNum = pedidosDTO.getdocumento();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 12, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 12, Font.BOLD);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);
            String cliente = pedidosDTO.getcliente().replaceAll(" ","_");
            String nombrePdf = "pickingg.pdf";
            FileOutputStream ficheroPdf;
            if(PruebasLocales){
                ficheroPdf = new FileOutputStream("./"+ nombrePdf);
            }else{
                ficheroPdf = new FileOutputStream("/usr/share/nginx/html/remisiones/"+ nombrePdf);
            }
            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));



            PdfPCell cellblanks =new PdfPCell(new Phrase(" "));
            cellblanks.setBorder(PdfPCell.NO_BORDER);
            cellblanks.setHorizontalAlignment(Element.ALIGN_CENTER);


            PdfPTable tablaDatos = new PdfPTable(1);
            PdfPCell cell1 =new PdfPCell(new Phrase("Documento de picking Consolidado",fontH3));
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDatos.addCell(cell1);
            tablaDatos.addCell(cellblanks);




            PdfPTable tablaDetails = new PdfPTable(5);

            PdfPCell cellprod = new PdfPCell(new Phrase("Producto",fontH2));
            cellprod.setBorder(PdfPCell.BOTTOM);
            cellprod.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(cellprod);

            PdfPCell celcat = new PdfPCell(new Phrase("Categoria",fontH2));
            celcat.setBorder(PdfPCell.BOTTOM);
            celcat.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celcat);

            PdfPCell celcant = new PdfPCell(new Phrase("Cantidad",fontH2));
            celcant.setBorder(PdfPCell.BOTTOM);
            celcant.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celcant);

            PdfPCell cellcost = new PdfPCell(new Phrase("Costo",fontH2));
            cellcost.setBorder(PdfPCell.BOTTOM);
            cellcost.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(cellcost);

            PdfPCell celltotcost = new PdfPCell(new Phrase("Costo Total",fontH2));
            celltotcost.setBorder(PdfPCell.BOTTOM);
            celltotcost.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celltotcost);

            List<PdfPedidoDetailDTO> detail_list = pedidosDTO.getdetail_list();

            for(int i=0; i<detail_list.size(); i++){

                PdfPedidoDetailDTO element = detail_list.get(i);


                PdfPCell cellprod2 = new PdfPCell(new Phrase(element.getproducto(),fontH1));
                cellprod2.setBorder(PdfPCell.NO_BORDER);
                cellprod2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(cellprod2);

                PdfPCell celcat2 = new PdfPCell(new Phrase(element.getcategoria(),fontH1));
                celcat2.setBorder(PdfPCell.NO_BORDER);
                celcat2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celcat2);

                PdfPCell celcant2 = new PdfPCell(new Phrase(element.getcantidad()+"",fontH1));
                celcant2.setBorder(PdfPCell.NO_BORDER);
                celcant2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celcant2);

                PdfPCell cellcost2 = new PdfPCell(new Phrase(element.getcosto()+"",fontH1));
                cellcost2.setBorder(PdfPCell.NO_BORDER);
                cellcost2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(cellcost2);

                PdfPCell celltotcost2 = new PdfPCell(new Phrase(element.getcostototal()+"",fontH1));
                celltotcost2.setBorder(PdfPCell.NO_BORDER);
                celltotcost2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celltotcost2);

            }


            PdfPTable table = new PdfPTable(1);
            tablaDatos.setKeepTogether(false);
            tablaDatos.setSplitLate(false);
            PdfPCell hed = new PdfPCell(tablaDatos);
            hed.setBorder(PdfPCell.NO_BORDER);
            hed.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed);

            tablaDetails.setSplitLate(false);
            tablaDetails.setKeepTogether(false);
            PdfPCell hed3 = new PdfPCell(tablaDetails);
            hed3.setBorder(PdfPCell.NO_BORDER);
            hed3.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed3);

            table.setKeepTogether(false);
            table.setSplitLate(false);

            documento.add(table);





            response = nombrePdf;

        }catch(Exception e){
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    //PICKING INDIVIDUAL
    public Either<IException, String> printPDF2(PdfPedidosDTO2 pedidosDTO){
        Document documento = new Document(PageSize.A4, 0f, 0f, 30f, 35f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        String ruta = "";
        FileOutputStream ficheroPdf = null;
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
            String docNum = pedidosDTO.getdocumento();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 7, Font.NORMAL);
            Font fontH3 = new Font(HELVETICA, 8, Font.NORMAL);
            Font fontH4 = new Font(HELVETICA, 10, Font.NORMAL);
            String cliente = pedidosDTO.getcliente().replaceAll(" ","_");
            String nombrePdf = docNum+pedidosDTO.getdocumento_cliente()+cliente+"pickingi.pdf_tmp.pdf";
            nombrePdf.replace("null","");
            if(PruebasLocales){
                ruta = new File(".").getCanonicalPath()+"/"+ nombrePdf;
            }else{
                ruta = "/usr/share/nginx/html/remisiones/"+ nombrePdf;
            }
            ficheroPdf = new FileOutputStream(ruta);
            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter writer = PdfWriter.getInstance(documento,ficheroPdf);
            writer.setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));

            //DEFINO UNA CELDA VACIA PARA USO DE SALTO DE LINEA
            PdfPCell cellblanks =new PdfPCell(new Phrase(" "));
            cellblanks.setBorder(PdfPCell.NO_BORDER);
            cellblanks.setHorizontalAlignment(Element.ALIGN_CENTER);

            //DEFINO LA TABLA PRINCIPAL
            float AnchoTablas = 90;
            PdfPTable tablaDatos = new PdfPTable(1);
            tablaDatos.setWidthPercentage(AnchoTablas);

            //Defino LA TABLA QUE CONTIENE EL TITULO DEL DOCUMENTO
            PdfPCell cell1 =new PdfPCell(new Phrase("Documento de picking individual: "+pedidosDTO.getdocumento(),fontH1));
            cell1.setBorder(PdfPCell.NO_BORDER); cell1.setHorizontalAlignment(Element.ALIGN_CENTER); tablaDatos.addCell(cell1); tablaDatos.addCell(cellblanks);

            //tabla que contiene correo y dirección
            PdfPTable tablaHeader1 = new PdfPTable(3);
            tablaHeader1.setWidthPercentage(AnchoTablas);
            tablaHeader1.setWidths(new float[]{1,1,4});
            //DOCUMENTO CLIENTE
            CellMaker("Pedido del cliente: ",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader1);
            //DOCUMENTO CLIENTE VAL
            CellMaker(pedidosDTO.getdocumento_cliente(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader1);
            //OBSERVACIONES
            CellMaker("Observaciones: "+pedidosDTO.getobservaciones(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader1);

            tablaHeader1.addCell(cellblanks);tablaHeader1.addCell(cellblanks);
            tablaHeader1.addCell(cellblanks);tablaHeader1.addCell(cellblanks);

            //tabla 2
            PdfPTable tablaHeader2 = new PdfPTable(4);
            tablaHeader2.setWidthPercentage(AnchoTablas);
            //NOMBRE CLIENTE
            CellMaker("Nombre Cliente",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader2);
            //FECHA
            CellMaker("Fecha",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader2);
            //CORREO
            CellMaker("Correo",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader2);
            //DIRECCIÓN
            CellMaker("Dirección",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader2);
            //NOMBRE CLIENTE ALIGN_LEFT
            CellMaker(pedidosDTO.getcliente(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader2);
            //FECHA VAL
            CellMaker(pedidosDTO.getfecha()+"",fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader2);
            //CORREO VAL
            CellMaker(pedidosDTO.getcorreo(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader2);
            //DIRECCIÓN VAL
            CellMaker(pedidosDTO.getdireccion(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader2);

            tablaHeader2.addCell(cellblanks);tablaHeader2.addCell(cellblanks);tablaHeader2.addCell(cellblanks);tablaHeader2.addCell(cellblanks);

            //INGRESO EL ICONO
            try {
                Image img;
                if(PruebasLocales){
                    img = Image.getInstance(new File(".").getCanonicalPath()+"/"+pedidosDTO.getLogo() +".jpg");
                }else{
                    img = Image.getInstance("/usr/share/nginx/html/logos/" + pedidosDTO.getLogo() + ".jpg");
                }
                img.scalePercent(50);
                Rectangle pageSize = documento.getPageSize();
                img.setAbsolutePosition(pageSize.getRight(0) - (img.getWidth()/2), pageSize.getTop(0) - (img.getHeight()/2));
                //img.setAbsolutePosition(500f, 500f);
                documento.add(img);
            } catch (Exception e) {
                //System.out.println(e.toString());
            }

            //Espaciado
            //tablaHeader2.addCell(cellblanks);
            //tablaHeader2.addCell(cellblanks);
            //tablaHeader2.addCell(cellblanks);

            //Tabla que contiene ya los productos
            PdfPTable tablaDetails = new PdfPTable(8);
            tablaDetails.setWidthPercentage(AnchoTablas);
            float[] AnchosTabla = new float[]{2f,3.5f,1,1,1.2f,0.8f,1.8f,1.2f};
            tablaDetails.setWidths(AnchosTabla);

            //Defino los encabezados
            float Padding = 3f;
            CellMaker("C. Barras",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_CENTER,tablaDetails,Padding);
            CellMaker("Producto",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_CENTER,tablaDetails,Padding);
            CellMaker("Marca",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_CENTER,tablaDetails,Padding);
            CellMaker("Categoria",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_CENTER,tablaDetails,Padding);
            CellMaker("Linea",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_CENTER,tablaDetails,Padding);
            CellMaker("Cantidad",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_CENTER,tablaDetails,Padding);
            CellMaker("Fabricante",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_CENTER,tablaDetails,Padding);
            CellMaker("Presentación",fontH1,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_CENTER,tablaDetails,Padding);
            //CellMaker("Precio Total",fontH2,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_CENTER,tablaDetails,Padding);

            List<PdfPedidoDetailDTO> detail_list = pedidosDTO.getdetail_list();

            boolean LeTocaGris = false;
            int R = 180;
            int G = 180;
            int B = 180;
            Locale locale = new Locale("en", "US");
            int TotalUnidades = 0;
            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
            for(int i=0; i<detail_list.size(); i++){
                PdfPedidoDetailDTO element = detail_list.get(i);

                Font fontH1TMP = new Font(fontH1.getBaseFont(), fontH1.getSize(), fontH1.getStyle());
                Font fontH3TMP = new Font(fontH3.getBaseFont(), fontH3.getSize(), fontH3.getStyle());

                if(LeTocaGris){ fontH1TMP.setColor(BaseColor.WHITE); fontH3TMP.setColor(BaseColor.WHITE); }
                else{ fontH1TMP.setColor(BaseColor.BLACK); fontH3TMP.setColor(BaseColor.BLACK); }

                BaseColor fondo = LeTocaGris ? new BaseColor(R,G,B):BaseColor.WHITE;
                //Codigo barras
                CellMaker(element.getownbarcode(),fontH3TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //Producto
                CellMaker(element.getproducto(),fontH3TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //Marca
                CellMaker(element.getmarca(),fontH1TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //Categoria
                CellMaker(element.getcategoria(),fontH1TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //Linea
                CellMaker(element.getlinea(),fontH1TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //Cantidad
                CellMaker(element.getcantidad()+"",fontH3TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //Fabricante
                CellMaker(element.getfabricante(),fontH1TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //Presentación
                CellMaker(element.getpresentacion(),fontH1TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //Costo total
                //CellMaker(currencyFormatter.format(element.getcostototal()),fontH1TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_CENTER,tablaDetails,fondo,Padding);
                //
                TotalUnidades += element.getcantidad();
                LeTocaGris = !LeTocaGris;
            }
            //TOTAL CANTIDAD
            PdfPTable tablaTotales = new PdfPTable(3);
            tablaTotales.setWidthPercentage(AnchoTablas);

            tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks);

            CellMaker("",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker("Total Unidades",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(TotalUnidades+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaTotales);

            tablaTotales.setKeepTogether(false);
            tablaTotales.setSplitLate(false);
            PdfPCell tablaTotalesC = new PdfPCell(tablaTotales);
            tablaTotalesC.setBorder(PdfPCell.NO_BORDER);
            ////////

            PdfPTable table = new PdfPTable(1); tablaDatos.setKeepTogether(false); tablaDatos.setSplitLate(false);
            table.setWidthPercentage(AnchoTablas);
            PdfPCell hed = new PdfPCell(tablaDatos); hed.setBorder(PdfPCell.NO_BORDER); hed.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed);

            tablaHeader1.setKeepTogether(false); tablaHeader1.setSplitLate(false);
            PdfPCell hed1 = new PdfPCell(tablaHeader1); hed1.setBorder(PdfPCell.NO_BORDER); hed1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed1);

            tablaHeader2.setSplitLate(false); tablaHeader2.setKeepTogether(false);
            PdfPCell hed2 = new PdfPCell(tablaHeader2); hed2.setBorder(PdfPCell.NO_BORDER); hed2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed2);

            tablaDetails.setSplitLate(false); tablaDetails.setKeepTogether(false);
            PdfPCell hed3 = new PdfPCell(tablaDetails); hed3.setBorder(PdfPCell.NO_BORDER); hed3.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed3);

            table.setKeepTogether(false);
            table.setSplitLate(false);

            table.addCell(tablaTotalesC);

            documento.add(table);
            response = nombrePdf.substring(0,nombrePdf.length()-8);
        }catch(Exception e){
            response = e.toString();
        }
        try {
            documento.close();
            ficheroPdf.close();
            PaginarPDF(ruta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    //PICKING INDIVIDUAL
    public Either<IException, String> printPDF2order(PdfPedidosOrderDTO pedidosDTO,int tipoFactura, Long id_third, Long id_bill){
        Document documento = new Document(PageSize.A4, 0f, 0f, 30f, 35f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        String ruta = "";
        FileOutputStream ficheroPdf = null;
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
            String docNum = pedidosDTO.getdocumento();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 9, Font.NORMAL);
            Font fontH3 = new Font(HELVETICA, 11, Font.BOLD);
            Font fontH4 = new Font(HELVETICA, 10, Font.NORMAL);
            //fuentes para proveedor y fecha
            Font Font1 = new Font(HELVETICA, 11, Font.BOLD);
            Font Font2 = new Font(HELVETICA, 10, Font.NORMAL);
            //
            String cliente = pedidosDTO.getcliente().replaceAll(" ","_");
            //String nombrePdf = docNum+pedidosDTO.getdocumento_cliente()+cliente+"pedido.pdf_tmp.pdf";
            String nombrePdf = docNum+"-"+id_third+id_bill+".pdf_tmp.pdf";
            nombrePdf.replace("null","");
            if(PruebasLocales){
                ruta = new File(".").getCanonicalPath()+"/"+ nombrePdf;
            }else{
                ruta = "/usr/share/nginx/html/remisiones/"+ nombrePdf;
            }
            ficheroPdf = new FileOutputStream(ruta);
            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter writer = PdfWriter.getInstance(documento,ficheroPdf);
            writer.setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));

            //DEFINO UNA CELDA VACIA PARA USO DE SALTO DE LINEA
            PdfPCell cellblanks =new PdfPCell(new Phrase(" "));
            cellblanks.setBorder(PdfPCell.NO_BORDER);
            cellblanks.setHorizontalAlignment(Element.ALIGN_CENTER);

            //DEFINO LA TABLA PRINCIPAL
            float AnchoTablas = 90;
            PdfPTable tablaDatos = new PdfPTable(1);
            tablaDatos.setWidthPercentage(AnchoTablas);

            //NOMBRE EMPRESA
            CellMaker(pedidosDTO.getDatosTercero().get("fullname").toString(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //NOMBRE TIENDA
            //CellMaker(billinPDF.getnombreEmpresa(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //NIT
            CellMaker(pedidosDTO.getDatosTercero().get("document_TYPE").toString() + " " + pedidosDTO.getDatosTercero().get("document_NUMBER").toString(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //DIRECCIÓN
            Object Direccion = pedidosDTO.getDatosTercero().get("address");
            if(Direccion != null){
                if(!Direccion.toString().equals("")){
                    CellMaker(Direccion.toString(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
                }
            }
            //TELEFONO
            Object Telefono = pedidosDTO.getDatosTercero().get("phone");
            if(Telefono != null){
                if(!Telefono.toString().equals("")){
                    CellMaker("Tel: "+Telefono.toString(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
                }
            }
            //TIENDA
            String tienda = pedidosDTO.gettienda();
            if(tienda != null){
                if(!tienda.toString().equals("")){
                    CellMaker("Tienda: "+tienda.toString(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
                }
            }
            //espacio vacio
            CellMaker(" ",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            //TITULO
            if(tipoFactura == 87){
                CellMaker("PEDIDO: "+pedidosDTO.getdocumento(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            }else{//tipo 86
                CellMaker("PEDIDO PROVEEDOR: "+pedidosDTO.getdocumento() + "\nPEDIDO CLIENTE: "+pedidosDTO.getnum_documento_cliente(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            }
            //espacio vacio
            CellMaker(" ",fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);

            //tabla que contiene correo y dirección
            PdfPTable tablaHeader1 = new PdfPTable(3);

            //tabla 2
            PdfPTable tablaHeader2 = new PdfPTable(2);
            /*float[] AnchosTabla2 = new float[]{1.5f,1f,1f,1.5f};
            tablaHeader2.setWidths(AnchosTabla2);*/
            tablaHeader2.setWidthPercentage(50);
            //NOMBRE CLIENTE
            if(tipoFactura == 87){
                CellMaker("PROVEEDOR",Font1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader2);
            }else{//tipo 86
                CellMaker("CLIENTE",Font1,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader2);
            }
            //FECHA
            CellMaker("FECHA",Font1,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaHeader2);
            //NOMBRE CLIENTE y DOCUMENTO VAL
            CellMaker(pedidosDTO.getcliente()+" - "+pedidosDTO.gettienda_client()+"\n"+pedidosDTO.getdocumento_cliente(),Font2,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader2);
            //FECHA VAL y fin linea
            CellMaker(pedidosDTO.getfecha()+"\n",Font2,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaHeader2);
            //TELEFONO VAL
            CellMaker(pedidosDTO.gettelefono(),Font2,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_MIDDLE,tablaHeader2);
            //OBSERVACIONES
            CellMaker(pedidosDTO.getobservaciones().equals("N/A") ?"":"OBSERVACIONES",Font1,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaHeader2);
            //DIRECCIÓN VAL y CORREO VAL
            CellMaker(pedidosDTO.getdireccion() + "\n" + pedidosDTO.getcorreo(),Font2,PdfPCell.NO_BORDER,Element.ALIGN_LEFT,Element.ALIGN_TOP,tablaHeader2);
            //OBSERVACIONES VAL
            CellMaker(pedidosDTO.getobservaciones().equals("N/A") ?"":pedidosDTO.getobservaciones(),Font2,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaHeader2);

            tablaHeader2.addCell(cellblanks);tablaHeader2.addCell(cellblanks);tablaHeader2.addCell(cellblanks);tablaHeader2.addCell(cellblanks);

            //INGRESO EL ICONO
            try {
                Image img;
                String logo = pedidosDTO.getDatosTercero().get("document_TYPE").toString() + " " + pedidosDTO.getDatosTercero().get("document_NUMBER");
                if(PruebasLocales){
                    img = Image.getInstance(new File(".").getCanonicalPath()+"/"+logo +".jpg");
                }else{
                    img = Image.getInstance("/usr/share/nginx/html/logos/" + logo + ".jpg");
                }
                img.scalePercent(45);
                Rectangle pageSize = documento.getPageSize();
                img.setAbsolutePosition(pageSize.getRight(5) - (img.getWidth()/2), pageSize.getTop(5) - (img.getHeight()/2));
                //img.setAbsolutePosition(500f, 500f);
                documento.add(img);
            } catch (Exception e) {
                //System.out.println(e.toString());
            }

            //Espaciado
            //tablaHeader2.addCell(cellblanks);
            //tablaHeader2.addCell(cellblanks);
            //tablaHeader2.addCell(cellblanks);

            //Tabla que contiene ya los productos
            PdfPTable tablaDetails = new PdfPTable(7);
            tablaDetails.setWidthPercentage(AnchoTablas);
            float[] AnchosTabla = new float[]{2f,3.5f,1.2f,2f,2f,1f,2f};
            tablaDetails.setWidths(AnchosTabla);

            //Defino los encabezados
            float Padding = 3f;
            //Código
            CellMaker("Código",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,Padding);
            //Producto
            CellMaker("Producto",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,Padding);
            //Cantidad
            CellMaker("Cantidad",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,Padding);
            //Valor U.
            CellMaker("Valor U.",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,Padding);
            //SubTotal
            CellMaker("Subtotal",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,Padding);
            //IVA
            CellMaker("IVA",fontH4,PdfPCell.RIGHT | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,Padding);
            //Total
            CellMaker("Total",fontH4,PdfPCell.NO_BORDER | PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,Padding);

            List<List<String>> detail_list = pedidosDTO.getdetail_list();

            boolean LeTocaGris = false;
            int R = 180;
            int G = 180;
            int B = 180;
            Locale locale = new Locale("en", "US");
            int TotalUnidades = 0;
            double Subtotal = 0, Impuesto = 0, Total = 0;
            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
            for(int i=0; i<detail_list.size(); i++){
                List<String> element = detail_list.get(i);
                //System.out.println(element);

                Font fontH1TMP = new Font(fontH1.getBaseFont(), fontH1.getSize(), fontH1.getStyle());
                Font fontH3TMP = new Font(fontH3.getBaseFont(), fontH3.getSize(), fontH3.getStyle());

                if(LeTocaGris){ fontH1TMP.setColor(BaseColor.WHITE); fontH3TMP.setColor(BaseColor.WHITE); }
                else{ fontH1TMP.setColor(BaseColor.BLACK); fontH3TMP.setColor(BaseColor.BLACK); }

                BaseColor fondo = LeTocaGris ? new BaseColor(R,G,B):BaseColor.WHITE;
                //AJUSTES TEMPORALES DEBIDO A LOS DETALLES CON EL IVA MAL
//                double TotalProducto = Double.parseDouble(element.get(5));
//                double SubTotalProducto = TotalProducto / (1.0 + (Double.parseDouble(element.get(4))/100));
                ///////////////////////////////////////////////////////////
                double SubTotalProducto = Double.parseDouble(element.get(5));
                int cantidad = Integer.parseInt(element.get(0));
                double valorUnitario = SubTotalProducto/cantidad;
                double TotalProducto = SubTotalProducto*(1.0+(Double.parseDouble(element.get(4))/100));

                //Código
                CellMaker(element.get(3),fontH1TMP,PdfPCell.RIGHT,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //Producto
                CellMaker(element.get(1),fontH1TMP,PdfPCell.RIGHT,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //Cantidad
                CellMaker(element.get(0),fontH1TMP,PdfPCell.RIGHT,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //Valor U.
                CellMaker(format.format(valorUnitario),fontH1TMP,PdfPCell.RIGHT,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //SubTotal
                CellMaker(format.format(SubTotalProducto),fontH1TMP,PdfPCell.RIGHT,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //IVA
                CellMaker(element.get(4),fontH1TMP,PdfPCell.RIGHT,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //Total
                CellMaker(format.format(TotalProducto),fontH1TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,fondo,Padding);
                //
                TotalUnidades += Integer.parseInt(element.get(0));
                Subtotal += SubTotalProducto;
                Impuesto += SubTotalProducto * (Double.parseDouble(element.get(4))/100);
                Total += TotalProducto;
                LeTocaGris = !LeTocaGris;
            }
            //TOTAL CANTIDAD
            PdfPTable tablaTotales = new PdfPTable(3);
            float[] AnchosTabla3 = new float[]{3f,1f,1f};
            tablaTotales.setWidths(AnchosTabla3);
            tablaTotales.setWidthPercentage(AnchoTablas);
            //Redondeo a 50 el subtotal e iva
            Subtotal = Math.round(Subtotal);
            Impuesto = Math.round(Impuesto);
            Total = Subtotal + Impuesto;
            ///////////////////////////////////

            tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks);

            CellMaker("",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker("Total Unidades",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(TotalUnidades+"",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker("",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker("Subtotal",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(Subtotal),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker("",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker("IVA",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(Impuesto),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker("",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker("Total",fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(Total),fontH4,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);

            tablaTotales.setKeepTogether(false);
            tablaTotales.setSplitLate(false);
            PdfPCell tablaTotalesC = new PdfPCell(tablaTotales);
            tablaTotalesC.setBorder(PdfPCell.NO_BORDER);
            ////////

            PdfPTable table = new PdfPTable(1); tablaDatos.setKeepTogether(false); tablaDatos.setSplitLate(false);
            table.setWidthPercentage(AnchoTablas);
            PdfPCell hed = new PdfPCell(tablaDatos); hed.setBorder(PdfPCell.NO_BORDER); hed.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed);

            tablaHeader1.setKeepTogether(false); tablaHeader1.setSplitLate(false);
            PdfPCell hed1 = new PdfPCell(tablaHeader1); hed1.setBorder(PdfPCell.NO_BORDER); hed1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed1);

            tablaHeader2.setSplitLate(false); tablaHeader2.setKeepTogether(false);
            PdfPCell hed2 = new PdfPCell(tablaHeader2); hed2.setBorder(PdfPCell.NO_BORDER); hed2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed2);

            tablaDetails.setSplitLate(false); tablaDetails.setKeepTogether(false);
            PdfPCell hed3 = new PdfPCell(tablaDetails); hed3.setBorder(PdfPCell.NO_BORDER); hed3.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed3);

            table.setKeepTogether(false);
            table.setSplitLate(false);

            table.addCell(tablaTotalesC);

            documento.add(table);
            response = nombrePdf.substring(0,nombrePdf.length()-8);
        }catch(Exception e){
            e.printStackTrace();
            response = e.toString();
        }
        try {
            documento.close();
            ficheroPdf.close();
            PaginarPDF(ruta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String> printPDF(PdfPedidosDTO pedidosDTO){
        Document documento = new Document(PageSize.A4, 0f, 0f, 35f, 0f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.


            String docNum = pedidosDTO.getdocumento();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 12, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 12, Font.BOLD);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);
            String cliente = pedidosDTO.getcliente().replaceAll(" ","_");
            String nombrePdf = docNum+pedidosDTO.getdocumento_cliente()+cliente+".pdf";
            FileOutputStream ficheroPdf;
            if(PruebasLocales){
                ficheroPdf = new FileOutputStream("./"+ nombrePdf);
            }else{
                ficheroPdf = new FileOutputStream("/usr/share/nginx/html/remisiones/"+ nombrePdf);
            }
            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));



            PdfPCell cellblanks =new PdfPCell(new Phrase(" "));
            cellblanks.setBorder(PdfPCell.NO_BORDER);
            cellblanks.setHorizontalAlignment(Element.ALIGN_CENTER);


            PdfPTable tablaDatos = new PdfPTable(1);
            PdfPCell cell1 =new PdfPCell(new Phrase("Documento de remision: "+pedidosDTO.getdocumento(),fontH3));
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDatos.addCell(cell1);
            tablaDatos.addCell(cellblanks);


            PdfPTable tablaHeader1 = new PdfPTable(2);
            PdfPCell cellTel = new PdfPCell(new Phrase(pedidosDTO.getcorreo(),fontH2));
            cellTel.setBorder(PdfPCell.NO_BORDER);
            cellTel.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader1.addCell(cellTel);
            PdfPCell celldir = new PdfPCell(new Phrase(pedidosDTO.getdireccion(),fontH2));
            celldir.setBorder(PdfPCell.NO_BORDER);
            celldir.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader1.addCell(celldir);
            tablaHeader1.addCell(cellblanks);
            tablaHeader1.addCell(cellblanks);


            PdfPTable tablaHeader2 = new PdfPTable(3);
            PdfPCell cellcliente = new PdfPCell(new Phrase(pedidosDTO.getcliente(),fontH1));
            cellcliente.setBorder(PdfPCell.NO_BORDER);
            cellcliente.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(cellcliente);
            PdfPCell cellFecha = new PdfPCell(new Phrase(""+pedidosDTO.getfecha(),fontH1));
            cellFecha.setBorder(PdfPCell.NO_BORDER);
            cellFecha.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(cellFecha);
            PdfPCell cellDocCliente = new PdfPCell(new Phrase(pedidosDTO.getdocumento_cliente(),fontH1));
            cellDocCliente.setBorder(PdfPCell.NO_BORDER);
            cellDocCliente.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(cellDocCliente);
            PdfPCell celltotal = new PdfPCell(new Phrase("Total: "+pedidosDTO.gettotal(),fontH1));
            celltotal.setBorder(PdfPCell.NO_BORDER);
            celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(celltotal);
            PdfPCell cellsubtotal = new PdfPCell(new Phrase("Subtotal: "+pedidosDTO.getsubtotal(),fontH1));
            cellsubtotal.setBorder(PdfPCell.NO_BORDER);
            cellsubtotal.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(cellsubtotal);
            PdfPCell celltax = new PdfPCell(new Phrase("Tax: "+pedidosDTO.gettax(),fontH1));
            celltax.setBorder(PdfPCell.NO_BORDER);
            celltax.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(celltax);
            tablaHeader2.addCell(cellblanks);
            tablaHeader2.addCell(cellblanks);
            tablaHeader2.addCell(cellblanks);


            PdfPTable tablaDetails = new PdfPTable(5);

            PdfPCell cellprod = new PdfPCell(new Phrase("Producto",fontH2));
            cellprod.setBorder(PdfPCell.BOTTOM);
            cellprod.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(cellprod);

            PdfPCell celcat = new PdfPCell(new Phrase("Categoria",fontH2));
            celcat.setBorder(PdfPCell.BOTTOM);
            celcat.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celcat);

            PdfPCell celcant = new PdfPCell(new Phrase("Cantidad",fontH2));
            celcant.setBorder(PdfPCell.BOTTOM);
            celcant.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celcant);

            PdfPCell cellcost = new PdfPCell(new Phrase("Costo",fontH2));
            cellcost.setBorder(PdfPCell.BOTTOM);
            cellcost.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(cellcost);

            PdfPCell celltotcost = new PdfPCell(new Phrase("Costo Total",fontH2));
            celltotcost.setBorder(PdfPCell.BOTTOM);
            celltotcost.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celltotcost);

            List<PdfPedidoDetailDTO> detail_list = pedidosDTO.getdetail_list();

            for(int i=0; i<detail_list.size(); i++){

                PdfPedidoDetailDTO element = detail_list.get(i);

                PdfPCell cellprod2 = new PdfPCell(new Phrase(element.getproducto(),fontH1));
                cellprod2.setBorder(PdfPCell.NO_BORDER);
                cellprod2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(cellprod2);

                PdfPCell celcat2 = new PdfPCell(new Phrase(element.getcategoria(),fontH1));
                celcat2.setBorder(PdfPCell.NO_BORDER);
                celcat2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celcat2);

                PdfPCell celcant2 = new PdfPCell(new Phrase(element.getcantidad()+"",fontH1));
                celcant2.setBorder(PdfPCell.NO_BORDER);
                celcant2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celcant2);

                PdfPCell cellcost2 = new PdfPCell(new Phrase(element.getcosto()+"",fontH1));
                cellcost2.setBorder(PdfPCell.NO_BORDER);
                cellcost2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(cellcost2);

                PdfPCell celltotcost2 = new PdfPCell(new Phrase(element.getcostototal()+"",fontH1));
                celltotcost2.setBorder(PdfPCell.NO_BORDER);
                celltotcost2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celltotcost2);

            }


            PdfPTable table = new PdfPTable(1);
            tablaDatos.setKeepTogether(false);
            tablaDatos.setSplitLate(false);
            PdfPCell hed = new PdfPCell(tablaDatos);
            hed.setBorder(PdfPCell.NO_BORDER);
            hed.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed);

            tablaHeader1.setKeepTogether(false);
            tablaHeader1.setSplitLate(false);
            PdfPCell hed1 = new PdfPCell(tablaHeader1);
            hed1.setBorder(PdfPCell.NO_BORDER);
            hed1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed1);

            tablaHeader2.setSplitLate(false);
            tablaHeader2.setKeepTogether(false);
            PdfPCell hed2 = new PdfPCell(tablaHeader2);
            hed2.setBorder(PdfPCell.NO_BORDER);
            hed2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed2);

            tablaDetails.setSplitLate(false);
            tablaDetails.setKeepTogether(false);
            PdfPCell hed3 = new PdfPCell(tablaDetails);
            hed3.setBorder(PdfPCell.NO_BORDER);
            hed3.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed3);

            table.setKeepTogether(false);
            table.setSplitLate(false);

            documento.add(table);





            response = nombrePdf;

        }catch(Exception e){
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> getQuantity(Long idps) {
        try {
            return Either.right(pedidosDAO.getQuantity(idps));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> procedure2(Long idvehiculo,
                                               Long idbventa,
                                               Long idbcompra,
                                               Long idbremision,
                                               Long idstorecliente,
                                               Long idstoreproveedor,
                                               Long idbpedidoproveedor) {
        try {
            pedidosDAO.procedureVe(
                    idvehiculo,
                    idbventa,
                    idbcompra,
                    idbremision,
                    idstorecliente,
                    idstoreproveedor,
                    idbpedidoproveedor);
            Long validator = new Long (1);
            //System.out.println("IDVENTA: "+idbventa);
            //System.out.println("IDCOMPRA: "+idbcompra);
            //System.out.println("IDREMISION: "+idbremision);
            //System.out.println("IDPEDIDO: "+idbpedidoproveedor);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> procedimiento(Long idbventa,Long idcompra,Long idremision) {
        try {
            CallableStatement stmt;
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@sysware.cw2rwsqphepo.us-east-1.rds.amazonaws.com:1521:ORCL","facturacion724","tienda724");
            stmt = con.prepareCall("select facturacion724.GET_VALIDAR_DETAIL_BILL_ZERO(?) from dual");
            stmt.setLong(1,idbventa );
            //stmt.setString(2, bb);
            ResultSet rs = stmt.executeQuery();
            Long validator = new Long(0);

            while (rs.next()) {
                validator = rs.getLong(1);
            }

            rs.close();
            stmt.close();
            con.close();
            if(validator==0){
                pedidosDAO.updateBillIf0(idbventa);
                pedidosDAO.updateBillIf0(idcompra);
                pedidosDAO.updateBillIf0(idremision);
            }
            return Either.right(validator);

        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long> ACTUALIZAR_ESTDO_PEDIDO_APP(Long idbillpedidoprov,
                                                                Long idbillstateclient,
                                                                Long idbillstateprov,
                                                                String notas,
                                                                Long ESTADOORIGENPROV,
                                                                Long IDTHIRDUSER,
                                                                String ACTOR) {
        try {
            //System.out.println(notas);
            pedidosDAO.ACTUALIZAR_ESTDO_PEDIDO_APP(idbillpedidoprov,
                    idbillstateclient,
                    idbillstateprov,
                    notas,ESTADOORIGENPROV,IDTHIRDUSER,ACTOR);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long> CREAR_PEDIDO_APP(Long idstoreclient,
                                                     Long idthirduseraapp,
                                                     Long idstoreprov,
                                                     String detallepedido,
                                                     Long descuento,
                                                     Long idpaymentmethod,
                                                     Long idapp) {
        try {
            pedidosDAO.CREAR_PEDIDO_APP(idstoreclient,
                    idthirduseraapp,
                    idstoreprov,
                    detallepedido,
                    descuento,
                    idpaymentmethod,
                    idapp);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> CREAR_PEDIDO_APP_EMP(
                                                     Long id_emp,
                                                     Long idstoreclient,
                                                     Long idthirduseraapp,
                                                     Long idstoreprov,
                                                     String detallepedido,
                                                     Long descuento,
                                                     Long idpaymentmethod,
                                                     Long idapp) {
        try {
            pedidosDAO.CREAR_PEDIDO_APP_EMP(id_emp,idstoreclient,
                    idthirduseraapp,
                    idstoreprov,
                    detallepedido,
                    descuento,
                    idpaymentmethod,
                    idapp);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> CONTADOR_APP_INSTALADAS(
            String deviceid,Long idapp, Long appversion) {
        try {
            pedidosDAO.CONTADOR_APP_INSTALADAS(deviceid,idapp,
                    appversion);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long> ACTUALIZAR_PEDIDO_NOVEDAD_APP(Long idbillpedidoprov,
                                                                  String nuevosdetalles,
                                                                  String notas,
                                                                  Long idpaymentmethod,
                                                                  Long ESTADOORIGENPROV,
                                                                  Long IDTHIRDUSER,
                                                                  String ACTOR) {
        try {
            pedidosDAO.ACTUALIZAR_PEDIDO_NOVEDAD_APP(idbillpedidoprov,
                    nuevosdetalles,
                    notas,
                    idpaymentmethod,
                    ESTADOORIGENPROV,
                    IDTHIRDUSER,
                    ACTOR);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<PlanillaB>> getPlanillas2(Long id_cierre_caja) {
        try {
            List<PlanillaB> response = pedidosDAO.getPlanillas2(id_cierre_caja);
            return Either.right(response);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<PlanillaB2>> getPlanillaData2(Long id_cierre_caja, String status) {
        try {
            List<PlanillaB2> response = pedidosDAO.getPlanillaData2(id_cierre_caja, status);
            return Either.right(response);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<PlanillaB>> getPlanillas(Long id_store,String status) {
        try {
            List<PlanillaB> response = pedidosDAO.getPlanillas(id_store,
                    status);
            return Either.right(response);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<ThirdDataPedidos>> getThirDataPedidos(Long id_store) {
        try {
            List<ThirdDataPedidos> response = pedidosDAO.getThirDataPedidos(id_store);
            return Either.right(response);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<PlanillaData>> getDatosPlanilla(Long id_planilla) {
        try {
            List<PlanillaData> response = pedidosDAO.getDatosPlanilla(id_planilla);
            return Either.right(response);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List<DatosDomiciliario>> getDatosDomiciliario(Long id_store,
                                                                            String status) {
        try {
            List<DatosDomiciliario> response = pedidosDAO.getDatosDomiciliario(id_store,status);
            return Either.right(response);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List<Pedido2>> getPedidosVerThird(List<Long> id_bill_state,
                                                                Long idthirdclient,
                                                                Long id_bill_type,
                                                                Long idapp) {
        try {
            List<Pedido2> response = pedidosDAO.getPedidosVerThird(id_bill_state,idthirdclient,id_bill_type,idapp);
            return Either.right(response);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Long>> getIdBillPlanilla(    Long id_store,
                                                                Long id_planilla) {
        try {
            List<Long> response = pedidosDAO.getIdBillPlanilla(id_store,id_planilla);
            return Either.right(response);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<DetailForPedido>> getDetailForPedido(Long id_bill) {
        try {
            List<DetailForPedido> response = pedidosDAO.getDetailForPedido(id_bill);
            return Either.right(response);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Kardex>> getKardex(Long id_store, String ownbarcode, String date1, String date2) {
        try {
            List<Kardex> response = pedidosDAO.getKardex(id_store,ownbarcode,new Date(date1),new Date(date2));
            System.out.println("BACK 4 DE ENERO 2");
            return Either.right(response);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<DatosProvider>> GetStoreProvs(Long id_store) {
        try {
            List<DatosProvider> provDisponibles = pedidosDAO.getDatosProveedor(id_store);
            return Either.right(provDisponibles);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String> CheckClosestProv(Long id_store,
                                                     Double lat,
                                                     Double lng) {
        try {
            List<DatosProvider> provDisponibles = pedidosDAO.getDatosProveedor(id_store);
            //String Salida = "idstore|doctype|docnumber|name|address|lat|lng|cityname|cityid|email|phone";
            String Salida = "-1";
            double lastDistance = Double.MAX_VALUE;
            //System.out.println("llega a CheckClosestProv 2");
            Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().serializeNulls().create();
            for(int n = 0;n<provDisponibles.size();n++){
                DatosProvider prov = provDisponibles.get(n);
                //verifico distancia
                try {
                    //System.out.println("https://route.ls.hereapi.com/routing/7.2/calculateroute.json?" +
                     //       "apiKey=UIoqjswUAwxj-SQfmmLsfc_un9p_oynEKo5dU0co-7Q&" +
                       //     "waypoint0=geo!"+lat+","+lng+"&" +
                         //    "waypoint1=geo!"+prov.getLATITUD()+","+prov.getLONGITUD()+"&" +
                          //  "routeattributes=wp,sm,sh,sc&mode=fastest;bicycle;traffic:disabled");
                    MultivaluedMap<String, Object> headers = new MultivaluedHashMap<>();
                    Object Consulta = ClientBuilder.newBuilder().build().target(
                            "https://route.ls.hereapi.com/routing/7.2/calculateroute.json?" +
                                    "apiKey=UIoqjswUAwxj-SQfmmLsfc_un9p_oynEKo5dU0co-7Q&" +
                                    "waypoint0=geo!"+lat+","+lng+"&" +
                                    "waypoint1=geo!"+prov.getLATITUD()+","+prov.getLONGITUD()+"&" +
                                    "routeattributes=wp,sm,sh,sc&mode=fastest;bicycle;traffic:disabled"
                    ).request(MediaType.APPLICATION_JSON).headers(headers).get(Object.class);

                    ObjectMapper m = new ObjectMapper();

                    Map<String,Object> mappedObject = m.convertValue(Consulta, new TypeReference<Map<String, Object>>() {});
                    mappedObject = m.convertValue(mappedObject.get("response"), new TypeReference<Map<String, Object>>() {});
                    List<Object> mappedObject2 = m.convertValue(mappedObject.get("route"), new TypeReference<List<Object>>() {});
                    mappedObject = m.convertValue(mappedObject2.get(0), new TypeReference<Map<String, Object>>() {});
                    mappedObject2 = m.convertValue(mappedObject.get("summaryByCountry"), new TypeReference<List<Object>>() {});
                    mappedObject = m.convertValue(mappedObject2.get(0), new TypeReference<Map<String, Object>>() {});
                    //
                    double Distancia = Double.parseDouble(mappedObject.get("distance").toString());
                    //System.out.println(prov.getID_STORE_PROVIDER()+"|"+
                      //      prov.getDOCUMENT_TYPE()+"|"+
                        //    prov.getDOCUMENT_NUMBER()+"|"+
                          //  prov.getDESCRIPTION()+"|"+Distancia);
                    if(Distancia < lastDistance){
                        lastDistance = Distancia;
                        //Salida = "idstore|doctype|docnumber|name|address|lat|lng";
                        Salida =
                                prov.getID_STORE_PROVIDER()+"|"+
                                prov.getDOCUMENT_TYPE()+"|"+
                                prov.getDOCUMENT_NUMBER()+"|"+
                                prov.getDESCRIPTION()+"|"+
                                prov.getADDRESS()+"|"+
                                prov.getLATITUD()+"|"+
                                prov.getLONGITUD()+"|"+
                                prov.getCITY_NAME()+"|"+
                                prov.getID_CITY()+"|"+
                                prov.getEMAILPROV()+"|"+
                                prov.getPHONEPROV();
                    }
                }catch (Exception e){
                    //System.out.println("e.getMessage() es ");
                    //System.out.println(e.getMessage());
                }
            }
            return Either.right(Salida);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> ENVIAR_PEDIDO_APP(Long idbillpedidoprov,
                                                      Long idbillventa,
                                                      String notas,
                                                      Long ESTADOORIGENPROV,
                                                      Long IDTHIRDUSER,
                                                      String ACTOR) {
        try {
            pedidosDAO.ENVIAR_PEDIDO_APP(idbillpedidoprov,
                    idbillventa,
                    notas,
                    ESTADOORIGENPROV,
                    IDTHIRDUSER,
                    ACTOR);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long> CERRAR_PLANILLA_PEDIDOS(Long IDPLANILLA,
                                                            String NOTAS,
                                                            Double valorbilletes,
                                                            Double valormonedas,
                                                            Double valorotros) {
        try {
            pedidosDAO.CERRAR_PLANILLA_PEDIDOS(IDPLANILLA,NOTAS,
                    valorbilletes,
                    valormonedas,valorotros);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> CREAR_PLANILLA_PEDIDOS(Long IDVEHICULO,
                                                      Long IDSTORE,
                                                      Long IDCIERRECAJA) {
        try {
            pedidosDAO.CREAR_PLANILLA_PEDIDOS(IDVEHICULO,
                    IDSTORE,
                    IDCIERRECAJA);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> ENVIAR_PEDIDOS_FACTURADOS(Long IDPLANILLA,
                                                              Long ESTADOORIGENPROV,
                                                              Long IDTHIRDUSER,
                                                              String ACTOR) {
        try {
            pedidosDAO.ENVIAR_PEDIDOS_FACTURADOS(IDPLANILLA,
                    ESTADOORIGENPROV,
                    IDTHIRDUSER,
                    ACTOR);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> CREAR_MOVIMIENTO_CAJA(Long IDPLANILLA,
                                                          Long VALOR,
                                                          String NATURALEZA,
                                                          String NOTAS) {
        try {
            pedidosDAO.CREAR_MOVIMIENTO_CAJA(IDPLANILLA,
                    VALOR,
                    NATURALEZA,
                    NOTAS);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> CERRAR_PLANILLA(Long IDPLANILLA,
                                                    String NOTAS,
                                                    Long valorbilletes,
                                                    String valormonedas,
                                                    Long ESTADOORIGENPROV,
                                                    Long IDTHIRDUSER,
                                                    String ACTOR,
                                                    String valorotros) {
        try {
            pedidosDAO.CERRAR_PLANILLA(IDPLANILLA,
                    NOTAS,
                    valorbilletes,
                    valormonedas,
                    ESTADOORIGENPROV,
                    IDTHIRDUSER,
                    ACTOR,valorotros);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long> CERRAR_PLANILLA_NOVEDAD(Long IDPLANILLA,
                                                            String NOTAS,
                                                            Long ESTADOORIGENPROV,
                                                            Long IDTHIRDUSER,
                                                            String ACTOR) {
        try {
            pedidosDAO.CERRAR_PLANILLA_NOVEDAD(IDPLANILLA,
                    NOTAS,
                    ESTADOORIGENPROV,
                    IDTHIRDUSER,
                    ACTOR);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long> CIERRE_CAJA_PLANILLA(Long IDCIERRECAJA) {
        try {
            pedidosDAO.CIERRE_CAJA_PLANILLA(IDCIERRECAJA);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> ABRIR_CAJA_PLANILLA(Long IDCIERRECAJA) {
        try {
            pedidosDAO.ABRIR_CAJA_PLANILLA(IDCIERRECAJA);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> CANCELAR_PEDIDO_APP(Long idbillpedidoprov, String notes, Long idcaja,
                                                        Long ESTADOORIGENPROV,
                                                        Long IDTHIRDUSER,
                                                        String ACTOR) {
        try {
            pedidosDAO.CANCELAR_PEDIDO_APP(idbillpedidoprov,notes,idcaja,ESTADOORIGENPROV,IDTHIRDUSER,ACTOR);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> copia_producto(String ownbarcode,
                                                   Long idstore,
                                                   Long idstoredestiny) {
        try {
            pedidosDAO.copia_producto(ownbarcode, idstore, idstoredestiny);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long>  actualizar_pedido_trad(Long idbillpedidocliente,
                                                          String nuevosdetalles,
                                                          Long idpaymentmethod ) {
        try { //System.out.println("IDBILLPEDIDOCLIENTE: "+idbillpedidocliente);
            //System.out.println("DETAILS: "+nuevosdetalles);
                pedidosDAO.actualizar_pedido_trad( idbillpedidocliente,
                 nuevosdetalles,idpaymentmethod);
            Long response = new Long(1);
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long>  cancelar_pedido_trad(Long IDBILL) {
        try { pedidosDAO.cancelar_pedido_trad( IDBILL);
            Long response = new Long(1);
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long>  eliminar_faltantes(Long idstore) {
        try { pedidosDAO.eliminar_faltantes(idstore);
            Long response = new Long(1);
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> CREAR_PEDIDO_APP_OPCIONES(Long idstoreclient,
                                                     Long idthirduseraapp,
                                                     Long idstoreprov,
                                                     String detallepedido,
                                                     Long descuento,
                                                     Long idpaymentmethod,
                                                     Long idapp,
                                                     String detallepedidomesa) {
        try {
            pedidosDAO.CREAR_PEDIDO_APP_OPCIONES(idstoreclient,
                    idthirduseraapp,
                    idstoreprov,
                    detallepedido,
                    descuento,
                    idpaymentmethod,
                    idapp,
                    detallepedidomesa);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

}
