package com.name.business.entities;

import java.util.Date;

public class TopProds {
    private String OWNBARCODE;
    private String PRODUCT_STORE_CODE;
    private Long ID_PRODUCT_THIRD;
    private String PRODUCT_STORE_NAME;
    private String IMG;
    private Long VENTAS;

    public TopProds(String OWNBARCODE,
                    String PRODUCT_STORE_CODE,
             Long ID_PRODUCT_THIRD,
             String PRODUCT_STORE_NAME,
             String IMG,
             Long VENTAS) {
        this.ID_PRODUCT_THIRD = ID_PRODUCT_THIRD;
        this.OWNBARCODE = OWNBARCODE;
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
        this.IMG = IMG;
        this.VENTAS = VENTAS;
    }


    public Long getID_PRODUCT_THIRD() {
        return ID_PRODUCT_THIRD;
    }

    public void setID_PRODUCT_THIRD(Long ID_PRODUCT_THIRD) {
        this.ID_PRODUCT_THIRD = ID_PRODUCT_THIRD;
    }

    public String getPRODUCT_STORE_CODE() {
        return PRODUCT_STORE_CODE;
    }

    public void setPRODUCT_STORE_CODE(String PRODUCT_STORE_CODE) {
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
    }

    public Long getVENTAS() {
        return VENTAS;
    }

    public void setVENTAS(Long VENTAS) {
        this.VENTAS = VENTAS;
    }

    public String getOWNBARCODE() {
        return OWNBARCODE;
    }

    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }

    public String getPRODUCT_STORE_NAME() {
        return PRODUCT_STORE_NAME;
    }

    public void setPRODUCT_STORE_NAME(String PRODUCT_STORE_NAME) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
    }

    public String getIMG() {
        return IMG;
    }

    public void setIMG(String IMG) {
        this.IMG = IMG;
    }


}
