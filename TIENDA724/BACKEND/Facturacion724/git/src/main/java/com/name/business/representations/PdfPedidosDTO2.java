package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.glassfish.jersey.server.JSONP;

import java.util.Date;
import java.util.List;

public class PdfPedidosDTO2 {
    private String documento;
    private String cliente;
    private Date fecha;
    private String documento_cliente;
    private String correo;
    private String direccion;
    private Long total;
    private Long subtotal;
    private Long tax;
    private List<PdfPedidoDetailDTO> detail_list;
    private List<Boolean> used_list;
    private String observaciones;
    private String logo;

    @JsonCreator
    public PdfPedidosDTO2(@JsonProperty("documento") String documento,
                         @JsonProperty("cliente") String cliente,
                         @JsonProperty("fecha") Date fecha,
                         @JsonProperty("documento_cliente") String documento_cliente,
                         @JsonProperty("correo") String correo,
                         @JsonProperty("direccion") String direccion,
                         @JsonProperty("total") Long total,
                         @JsonProperty("subtotal") Long subtotal,
                         @JsonProperty("tax") Long tax,
                         @JsonProperty("detail_list") List<PdfPedidoDetailDTO> detail_list,
                         @JsonProperty("used_list") List<Boolean> used_list,
                         @JsonProperty("observaciones") String observaciones,
                          @JsonProperty("logo") String logo) {
        this.logo = logo;
        this.documento = documento;
        this.cliente = cliente;
        this.fecha = fecha;
        this.documento_cliente = documento_cliente;
        this.correo = correo;
        this.direccion = direccion;
        this.total = total;
        this.subtotal = subtotal;
        this.tax = tax;
        this.detail_list = detail_list;
        this.used_list = used_list;
        this.observaciones = observaciones;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public List<Boolean> getused_list() {
        return used_list;
    }

    public void setused_list(List<Boolean> used_list) {
        this.used_list = used_list;
    }

    public List<PdfPedidoDetailDTO> getdetail_list() {
        return detail_list;
    }

    public void setdetail_list(List<PdfPedidoDetailDTO> detail_list) {
        this.detail_list = detail_list;
    }

    public Long gettax() {
        return tax;
    }

    public void settax(Long tax) {
        this.tax = tax;
    }

    public Long getsubtotal() {
        return subtotal;
    }

    public void setsubtotal(Long subtotal) {
        this.subtotal = subtotal;
    }


    public Long gettotal() {
        return total;
    }

    public void settotal(Long total) {
        this.total = total;
    }


    public String getdireccion() {
        return direccion;
    }

    public void setdireccion(String direccion) {
        this.direccion = direccion;
    }


    public String getobservaciones() {
        return observaciones;
    }

    public void setobservaciones(String observaciones) {
        this.observaciones = observaciones;
    }


    public String getcorreo() {
        return correo;
    }

    public void setcorreo(String correo) {
        this.correo = correo;
    }





    public String getdocumento_cliente() {
        return documento_cliente;
    }

    public void setdocumento_cliente(String documento_cliente) {
        this.documento_cliente = documento_cliente;
    }


    public Date getfecha() {
        return fecha;
    }

    public void setfecha(String fecha) {
        this.fecha = new Date(fecha);
    }



    public String getcliente() {
        return cliente;
    }

    public void setcliente(String cliente) {
        this.cliente = cliente;
    }

    public String getdocumento() {
        return documento;
    }

    public void setdocumento(String documento) {
        this.documento = documento;
    }

}
