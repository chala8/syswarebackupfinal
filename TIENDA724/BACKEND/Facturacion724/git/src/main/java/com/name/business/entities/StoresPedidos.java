package com.name.business.entities;

import java.util.Date;

public class StoresPedidos {
    private Long ID_STORE;
    private String DESCRIPTION;

    public StoresPedidos (Long ID_STORE, String DESCRIPTION) {
        this.ID_STORE = ID_STORE;
        this.DESCRIPTION = DESCRIPTION;
    }


    public Long getID_STORE() {
        return ID_STORE;
    }

    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }


}
