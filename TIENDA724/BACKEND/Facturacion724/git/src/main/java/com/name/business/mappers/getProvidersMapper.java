package com.name.business.mappers;

import com.name.business.entities.Bill4Prod;
import com.name.business.entities.getProviders;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class getProvidersMapper implements ResultSetMapper<getProviders> {
    @Override
    public getProviders map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new getProviders(
                resultSet.getLong("ID_THIRD_DESTINITY"),
                resultSet.getString("FULLNAME"),
                resultSet.getLong("ID_STORE_PROVIDER")
        );
    }
}
