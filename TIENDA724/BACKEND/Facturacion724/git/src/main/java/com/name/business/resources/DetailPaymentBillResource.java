package com.name.business.resources;


import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.DetailPaymentBillBusiness;
import com.name.business.entities.*;
import com.name.business.representations.DetailPaymentBillDTO;
import com.name.business.representations.DetailPaymentBillIdDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/payments-details")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DetailPaymentBillResource {
    private DetailPaymentBillBusiness detailPaymentBillBusiness;

    public DetailPaymentBillResource(DetailPaymentBillBusiness detailPaymentBillBusiness) {
        this.detailPaymentBillBusiness = detailPaymentBillBusiness;
    }

    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDetailPaymentBillResource(
            @QueryParam("id_detail_payment_bill") Long id_detail_payment_bill,
                    @QueryParam("id_bill") Long id_bill,
                    @QueryParam("id_way_to_pay") Long id_way_to_pay,
                    @QueryParam("id_payment_method") Long id_payment_method,
                    @QueryParam("payment_value") Double payment_value,
                    @QueryParam("aprobation_code") String aprobation_code,



                    @QueryParam("id_state_det_pay") Long id_state_det_pay,
                    @QueryParam("state_det_pay") Integer state_det_pay,
                    @QueryParam("creation_det_pay") Date creation_det_pay,
                    @QueryParam("update_det_pay") Date update_det_pay){

        Response response;

        Either<IException, List<DetailPaymentBill>> getResponseGeneric = detailPaymentBillBusiness.getDetailPaymentBill(
                new DetailPaymentBill(id_detail_payment_bill,id_bill,id_way_to_pay,id_payment_method,payment_value,aprobation_code,
                        new CommonState(id_state_det_pay, state_det_pay,
                                creation_det_pay, update_det_pay)
                )
        );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/completes")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDetailPaymentBillCompleteResource(
            @QueryParam("id_detail_payment_bill") Long id_detail_payment_bill,
            @QueryParam("payment_value") Double payment_value,
            @QueryParam("id_bill") Long id_bill,
            @QueryParam("aprobation_code") String aprobation_code,

            @QueryParam("id_payment_method") Long id_payment_method,
            @QueryParam("name_payment_method") String name_payment_method,
            @QueryParam("id_state_payment_method") Long id_state_payment_method,
            @QueryParam("state_payment_method") Integer state_payment_method,
            @QueryParam("creation_payment_method") Date creation_payment_method,
            @QueryParam("update_payment_method") Date update_payment_method,


            @QueryParam("id_way_pay") Long id_way_pay,
            @QueryParam("name_way_pay") String name_way_pay,

            @QueryParam("id_state_way_pay") Long id_state_way_pay,
            @QueryParam("state_way_pay") Integer state_way_pay,
            @QueryParam("creation_way_pay") Date creation_way_pay,
            @QueryParam("update_way_pay") Date update_way_pay,



            @QueryParam("id_state_det_pay") Long id_state_det_pay,
            @QueryParam("state_det_pay") Integer state_det_pay,
            @QueryParam("creation_det_pay") Date creation_det_pay,
            @QueryParam("update_det_pay") Date update_det_pay){

        Response response;

        Either<IException, List<DetailPaymentBillComplete>> getResponseGeneric = detailPaymentBillBusiness.getDetailPaymentBillComplete(
                new DetailPaymentBillComplete(id_detail_payment_bill,payment_value,id_bill,aprobation_code,
                        new PaymentMethod(id_payment_method, name_payment_method,
                                new CommonState(id_state_payment_method, state_payment_method,
                                        creation_payment_method, update_payment_method)
                        ),
                        new WayToPay(id_way_pay, name_way_pay,
                                new CommonState(id_state_way_pay, state_way_pay,
                                        creation_way_pay, update_way_pay)
                        ),
                        new CommonState(id_state_det_pay, state_det_pay,
                                creation_det_pay, update_det_pay)
                )
        );

        if (getResponseGeneric.isRight()){
            //System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postDetailPaymentBillResource(@QueryParam("id_bill") Long id_bill, List<DetailPaymentBillDTO> detailPaymentBillDTOList){
        Response response;

        Either<IException, Long> getResponseGeneric = detailPaymentBillBusiness.createDetailPaymentBill(id_bill,detailPaymentBillDTOList);


        if (getResponseGeneric.isRight()){

            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/{id_bill}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateDetailPaymentResource(@PathParam("id_bill") Long id_bill, List<DetailPaymentBillIdDTO> detailBillIdDTOList){
        Response response;
        Either<IException, Long> allViewOffertsEither = detailPaymentBillBusiness.updateDetailPaymentBill(id_bill, detailBillIdDTOList);

        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deleteDetailPaymentResource(@PathParam("id") Long id_detail_payment_bill, @QueryParam("id_bill") Long id_bill){
        Response response;
        Either<IException, Long> allViewOffertsEither = detailPaymentBillBusiness.deleteDetailPaymentBill(id_detail_payment_bill, id_bill);

        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

}
