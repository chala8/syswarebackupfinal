package com.name.business.mappers;

import com.name.business.entities.Bill;
import com.name.business.entities.CommonState;
import com.name.business.entities.comandaDetails;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class comandaDetailsMapper  implements ResultSetMapper<comandaDetails> {

    @Override
    public comandaDetails map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new comandaDetails(
                resultSet.getString("product_store_name"),
                resultSet.getString("cantidad")
        );
    }
}
