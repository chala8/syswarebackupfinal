package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class BillDetailIDDTO {
    private Long id_bill_father;
    private Long id_third_employee;
    private Long id_third;
    private Long id_payment_state;
    private Long id_bill_state;
    private Long id_bill_type;
    private Date purchase_date;
    private Double subtotal;
    private Double tax;
    private Double totalprice;
    private Double discount;
    private CommonStateDTO stateDTO;

    // Attributes optionals
    private List<DetailPaymentBillIdDTO> payments;
    private List<DetailBillIdDTO> details;

    @JsonCreator
    public BillDetailIDDTO(@JsonProperty("id_bill_father") Long id_bill_father, @JsonProperty("id_third_employee") Long id_third_employee,
                           @JsonProperty("id_third") Long id_third, @JsonProperty("id_payment_state") Long id_payment_state,
                           @JsonProperty("id_bill_state") Long id_bill_state, @JsonProperty("id_bill_type") Long id_bill_type,
                           @JsonProperty("purchase_date") Date purchase_date,
                           @JsonProperty("subtotal") Double subtotal, @JsonProperty("tax") Double tax,
                           @JsonProperty("totalprice") Double totalprice,@JsonProperty("discount") Double discount,
                           @JsonProperty("state") CommonStateDTO stateDTO,
                           @JsonProperty("payments") List<DetailPaymentBillIdDTO> payments, @JsonProperty("details") List<DetailBillIdDTO> details) {

        this.id_bill_father = id_bill_father;
        this.id_third_employee = id_third_employee;
        this.id_third = id_third;
        this.id_payment_state = id_payment_state;
        this.id_bill_state = id_bill_state;
        this.id_bill_type = id_bill_type;
        this.purchase_date = purchase_date;
        this.subtotal = subtotal;
        this.tax = tax;
        this.discount=discount;
        this.totalprice = totalprice;
        this.stateDTO = stateDTO;
        this.payments = payments;
        this.details = details;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public List<DetailPaymentBillIdDTO> getPayments() {
        return payments;
    }

    public void setPayments(List<DetailPaymentBillIdDTO> payments) {
        this.payments = payments;
    }

    public List<DetailBillIdDTO> getDetails() {
        return details;
    }

    public void setDetails(List<DetailBillIdDTO> details) {
        this.details = details;
    }

    public Long getId_bill_father() {
        return id_bill_father;
    }

    public void setId_bill_father(Long id_bill_father) {
        this.id_bill_father = id_bill_father;
    }

    public Long getId_third_employee() {
        return id_third_employee;
    }

    public void setId_third_employee(Long id_third_employee) {
        this.id_third_employee = id_third_employee;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_payment_state() {
        return id_payment_state;
    }

    public void setId_payment_state(Long id_payment_state) {
        this.id_payment_state = id_payment_state;
    }

    public Long getId_bill_state() {
        return id_bill_state;
    }

    public void setId_bill_state(Long id_bill_state) {
        this.id_bill_state = id_bill_state;
    }

    public Long getId_bill_type() {
        return id_bill_type;
    }

    public void setId_bill_type(Long id_bill_type) {
        this.id_bill_type = id_bill_type;
    }


    public Date getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(Date purchase_date) {
        this.purchase_date = purchase_date;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(Double totalprice) {
        this.totalprice = totalprice;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
