package com.name.business.entities;

public class Pedido2 {

    private Long ID_BILL;
    private Long ID_STORE;
    private Long ID_STORE_CLIENT;
    private String CLIENTE;
    private String TIENDA;
    private String NUMPEDIDO;
    private String FECHA;
    private String ADDRESS;
    private String PHONE;
    private String MAIL;
    private String LATITUD;
    private String LONGITUD;
    private String NUMDOCUMENTO;
    private String BODY;
    private Long IDTHIRDCLIENTE;
    private Long ID_PAYMENT;
    private String ADDRESSP;
    private String PHONEP;
    private String MAILP;
    private Double LATITUDP;
    private Double LONGITUDP;
    private String TIENDACLIENTE;
    private String DIRTIENDACLIENTE;
    private String CIUDADTIENDACLIENTE;
    private Double TOTALPRICE;
    private Long ID_BILL_STATE;
    private String CITYPROV;


    public Pedido2(Long ID_BILL,
                  Long ID_STORE_CLIENT,
                  Long ID_STORE,
                  String CLIENTE,
                  String TIENDA,
                  String NUMPEDIDO,
                  String FECHA,
                  String ADDRESS,
                  String PHONE,
                  String MAIL,
                  String LATITUD,
                  String LONGITUD,
                  String NUMDOCUMENTO,
                  String BODY,
                  Long IDTHIRDCLIENTE,
                  Long ID_PAYMENT,
                  String ADDRESSP,
                  String PHONEP,
                  String MAILP,
                  Double LATITUDP,
                  Double LONGITUDP,
                  String TIENDACLIENTE,
                  String DIRTIENDACLIENTE,
                  String CIUDADTIENDACLIENTE,
                   Double TOTALPRICE,
                   Long ID_BILL_STATE,
                   String CITYPROV) {
        this.TOTALPRICE = TOTALPRICE;
        this.CITYPROV = CITYPROV;
        this.ID_BILL_STATE = ID_BILL_STATE;
        this.DIRTIENDACLIENTE = DIRTIENDACLIENTE;
        this.TIENDACLIENTE = TIENDACLIENTE;
        this.CIUDADTIENDACLIENTE =CIUDADTIENDACLIENTE;
        this.ADDRESSP = ADDRESSP;
        this.PHONEP = PHONEP;
        this.MAILP = MAILP;
        this.LATITUDP = LATITUDP;
        this.LONGITUDP = LONGITUDP;
        this.ID_PAYMENT = ID_PAYMENT;
        this.IDTHIRDCLIENTE = IDTHIRDCLIENTE;
        this.BODY = BODY;
        this.ID_STORE_CLIENT = ID_STORE_CLIENT;
        this.ID_STORE = ID_STORE;
        this.NUMDOCUMENTO = NUMDOCUMENTO;
        this.ID_BILL = ID_BILL;
        this.CLIENTE = CLIENTE;
        this.TIENDA =  TIENDA;
        this.NUMPEDIDO = NUMPEDIDO;
        this.FECHA = FECHA;
        this.ADDRESS = ADDRESS;
        this.PHONE = PHONE;
        this.MAIL = MAIL;
        this.LATITUD = LATITUD;
        this.LONGITUD = LONGITUD;
    }

    public String getCITYPROV() {
        return CITYPROV;
    }

    public void setCITYPROV(String CITYPROV) {
        this.CITYPROV = CITYPROV;
    }

    public Double getTOTALPRICE() {
        return TOTALPRICE;
    }

    public void setTOTALPRICE(Double TOTALPRICE) {
        this.TOTALPRICE = TOTALPRICE;
    }

    public Long getID_BILL_STATE() {
        return ID_BILL_STATE;
    }

    public void setID_BILL_STATE(Long ID_BILL_STATE) {
        this.ID_BILL_STATE = ID_BILL_STATE;
    }


    public String getTIENDACLIENTE() {
        return TIENDACLIENTE;
    }

    public void setTIENDACLIENTE(String TIENDACLIENTE) {
        this.TIENDACLIENTE = TIENDACLIENTE;
    }

    public String getDIRTIENDACLIENTE() {
        return DIRTIENDACLIENTE;
    }

    public void setDIRTIENDACLIENTE(String DIRTIENDACLIENTE) {
        this.DIRTIENDACLIENTE = DIRTIENDACLIENTE;
    }

    public String getCIUDADTIENDACLIENTE() {
        return CIUDADTIENDACLIENTE;
    }

    public void setCIUDADTIENDACLIENTE(String CIUDADTIENDACLIENTE) {
        this.CIUDADTIENDACLIENTE = CIUDADTIENDACLIENTE;
    }

    public String getADDRESSP() {
        return ADDRESSP;
    }

    public void setADDRESSP(String ADDRESSP) {
        this.ADDRESSP = ADDRESSP;
    }

    public String getPHONEP() {
        return PHONEP;
    }

    public void setPHONEP(String PHONEP) {
        this.PHONEP = PHONEP;
    }

    public String getMAILP() {
        return MAILP;
    }

    public void setMAILP(String MAILP) {
        this.MAILP = MAILP;
    }

    public Double getLATITUDP() {
        return LATITUDP;
    }

    public void setLATITUDP(Double LATITUDP) {
        this.LATITUDP = LATITUDP;
    }

    public Double getLONGITUDP() {
        return LONGITUDP;
    }

    public void setLONGITUDP(Double LONGITUDP) {
        this.LONGITUDP = LONGITUDP;
    }

    public Long getID_PAYMENT() { return ID_PAYMENT; }
    public void setID_PAYMENT(Long ID_PAYMENT) { this.ID_PAYMENT = ID_PAYMENT; }
    //----------------------------------------------------------------------------
    public Long getIDTHIRDCLIENTE() { return IDTHIRDCLIENTE; }
    public void setIDTHIRDCLIENTE(Long IDTHIRDCLIENTE) { this.IDTHIRDCLIENTE = IDTHIRDCLIENTE; }
    //----------------------------------------------------------------------------
    public String getBODY() {
        return BODY;
    }
    public void setBODY(String BODY) {
        this.BODY = BODY;
    }
    //----------------------------------------------------------------------------
    public Long getID_STORE_CLIENT() {
        return ID_STORE_CLIENT;
    }
    public void setID_STORE_CLIENT(Long ID_STORE_CLIENT) {
        this.ID_STORE_CLIENT = ID_STORE_CLIENT;
    }
    //----------------------------------------------------------------------------
    public Long getID_STORE() {
        return ID_STORE;
    }
    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }
    //----------------------------------------------------------------------------
    public String getNUMDOCUMENTO() {
        return NUMDOCUMENTO;
    }
    public void setNUMDOCUMENTO(String NUMDOCUMENTO) {
        this.NUMDOCUMENTO = NUMDOCUMENTO;
    }
    //----------------------------------------------------------------------------
    public String getLONGITUD() {
        return LONGITUD;
    }
    public void setLONGITUD(String LONGITUD) {
        this.LONGITUD = LONGITUD;
    }
    //----------------------------------------------------------------------------
    public String getLATITUD() {
        return LATITUD;
    }
    public void setLATITUD(String LATITUD) {
        this.LATITUD = LATITUD;
    }
    //----------------------------------------------------------------------------
    public String getMAIL() {
        return MAIL;
    }
    public void setMAIL(String MAIL) {
        this.MAIL = MAIL;
    }
    //----------------------------------------------------------------------------
    public String getPHONE() {
        return PHONE;
    }
    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }
    //----------------------------------------------------------------------------
    public String getADDRESS() {
        return ADDRESS;
    }
    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }
    //----------------------------------------------------------------------------
    public Long getID_BILL() {
        return ID_BILL;
    }
    public void setID_BILL(Long ID_BILL) {
        this.ID_BILL = ID_BILL;
    }
    //----------------------------------------------------------------------------
    public String getCLIENTE() {
        return CLIENTE;
    }
    public void setCLIENTE(String CLIENTE) {
        this.CLIENTE = CLIENTE;
    }
    //----------------------------------------------------------------------------
    public String getTIENDA() {
        return TIENDA;
    }
    public void setTIENDA(String TIENDA) {
        this.TIENDA = TIENDA;
    }
    //----------------------------------------------------------------------------
    public String getNUMPEDIDO() {
        return NUMPEDIDO;
    }
    public void setNUMPEDIDO(String NUMPEDIDO) {
        this.NUMPEDIDO = NUMPEDIDO;
    }
    //----------------------------------------------------------------------------
    public String getFECHA() {
        return FECHA;
    }
    public void setFECHA(String FECHA) {
        this.FECHA = FECHA;
    }
    //----------------------------------------------------------------------------
}
