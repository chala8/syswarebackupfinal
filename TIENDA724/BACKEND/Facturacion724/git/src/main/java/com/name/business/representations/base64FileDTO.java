package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class base64FileDTO {
    private String file;
    private String fileName;
    private String format;
    private String filePseudonimn;


    @JsonCreator
    public base64FileDTO(@JsonProperty ("file") String file,
                         @JsonProperty ("fileName")String fileName,
                         @JsonProperty ("format") String format,
                         @JsonProperty ("filePseudonimn") String filePseudonimn
                         ){
        this.file = file;
        this.fileName = fileName;
        this.format = format;
        this.filePseudonimn = filePseudonimn;
    }


    public String getFilePseudonimn() { return filePseudonimn; }

    public void setFilePseudonimn(String filePseudonimn) { this.filePseudonimn = filePseudonimn; }

    public String getformat() {
        return format;
    }

    public void setformat(String format) {
        this.format = format;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }



}

