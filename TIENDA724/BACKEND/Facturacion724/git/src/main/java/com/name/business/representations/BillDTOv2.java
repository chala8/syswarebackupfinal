package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class BillDTOv2 {

    private Long idthirdemployee;
    private Long idthird;
    private Long idbilltype;
    private String notes;
    private Long idthirddestinity;
    private Long idcaja;
    private Long idstore;
    private Long idthirddomiciliario;
    private Long idpaymentmethod;
    private Long idwaytopay;
    private String approvalcode;
    private Long idbankentity;
    private Long idbillstate;
    private String details;
    private Long disccount;
    private String num_documento_factura;
    private Long idthirdvendedor;
    @JsonCreator
    public BillDTOv2(@JsonProperty("idthirdemployee") Long idthirdemployee,
                   @JsonProperty("idthird") Long idthird,
                   @JsonProperty("idbilltype") Long idbilltype,
                   @JsonProperty("notes") String notes,
                   @JsonProperty("idthirddestinity") Long idthirddestinity,
                   @JsonProperty("idcaja") Long idcaja,
                   @JsonProperty("idstore") Long idstore,
                   @JsonProperty("idthirddomiciliario") Long idthirddomiciliario,
                   @JsonProperty("idpaymentmethod") Long idpaymentmethod,
                   @JsonProperty("idwaytopay") Long idwaytopay,
                   @JsonProperty("approvalcode") String approvalcode,
                   @JsonProperty("idbankentity") Long idbankentity,
                   @JsonProperty("idbillstate") Long idbillstate,
                   @JsonProperty("details") String details,
                   @JsonProperty("disccount") Long disccount,
                   @JsonProperty("num_documento_factura") String num_documento_factura,
                   @JsonProperty("idthirdvendedor") Long idthirdvendedor
    ) {
        this.idthirdemployee = idthirdemployee;
        this.idthird = idthird;
        this.idbilltype = idbilltype;
        this.notes = notes;
        this.idthirddestinity = idthirddestinity;
        this.idcaja = idcaja;
        this.idstore = idstore;
        this.idthirddomiciliario = idthirddomiciliario;
        this.idpaymentmethod = idpaymentmethod;
        this.idwaytopay = idwaytopay;
        this.approvalcode = approvalcode;
        this.idbankentity = idbankentity;
        this.idbillstate = idbillstate;
        this.details = details;
        this.disccount = disccount;
        this.num_documento_factura = num_documento_factura;
        this.idthirdvendedor = idthirdvendedor;
    }


    public Long getIdthirdvendedor() {
        return idthirdvendedor;
    }

    public void setIdthirdvendedor(Long idthirdvendedor) {
        this.idthirdvendedor = idthirdvendedor;
    }

    public String getNum_documento_factura() {
        return num_documento_factura;
    }

    public void setNum_documento_factura(String num_documento_factura) {
        this.num_documento_factura = num_documento_factura;
    }

    public Long getdisccount() {
        return disccount;
    }
    public void setdisccount(Long disccount) {
        this.disccount = disccount;
    }

    public Long getidthirdemployee() {
        return idthirdemployee;
    }
    public void setidthirdemployee(Long idthirdemployee) {
        this.idthirdemployee = idthirdemployee;
    }

    public Long getidthird() {
        return idthird;
    }
    public void setidthird(Long idthird) {
        this.idthird = idthird;
    }

    public Long getidbilltype() {
        return idbilltype;
    }
    public void setidbilltype(Long idthird) {
        this.idbilltype = idbilltype;
    }

    public String getnotes() {
        return notes;
    }
    public void setnotes(String notes) {
        this.notes = notes;
    }

    public Long getidthirddestinity() {
        return idthirddestinity;
    }
    public void setidthirddestinity(Long idthirddestinity) { this.idthirddestinity = idthirddestinity; }

    public Long getidcaja() {
        return idcaja;
    }
    public void setidcaja(Long idcaja) {
        this.idcaja = idcaja;
    }

    public Long getidstore() {
        return idstore;
    }
    public void setidstore(Long idstore) {
        this.idstore = idstore;
    }

    public Long getidthirddomiciliario() {
        return idthirddomiciliario;
    }
    public void setidthirddomiciliario(Long idthirddomiciliario) {
        this.idthirddomiciliario = idthirddomiciliario;
    }

    public Long getidpaymentmethod() {
        return idpaymentmethod;
    }
    public void setidpaymentmethod(Long idpaymentmethod) {
        this.idpaymentmethod = idpaymentmethod;
    }

    public Long getidwaytopay() {
        return idwaytopay;
    }
    public void setidwaytopay(Long idwaytopay) {
        this.idwaytopay = idwaytopay;
    }

    public String getapprovalcode() {
        return approvalcode;
    }
    public void setapprovalcode(String approvalcode) {
        this.approvalcode = approvalcode;
    }

    public Long getidbankentity() {
        return idbankentity;
    }
    public void setidbankentity(Long idbankentity) {
        this.idbankentity = idbankentity;
    }

    public Long getidbillstate() {
        return idbillstate;
    }
    public void setidbillstate(Long idbillstate) {
        this.idbillstate = idbillstate;
    }

    public String getDetails() {
        return details;
    }
    public void setDetails(String details) {
        this.details = details;
    }


}
