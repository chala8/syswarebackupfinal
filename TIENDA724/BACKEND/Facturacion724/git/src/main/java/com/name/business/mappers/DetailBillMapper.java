package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.DetailBill;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailBillMapper implements ResultSetMapper<DetailBill> {

    @Override
    public DetailBill map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DetailBill(
                resultSet.getLong("ID_DETAIL_BILL"),
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("ID_PRODUCT_THIRD"),
                resultSet.getInt("QUANTITY"),
                resultSet.getDouble("PRICE"),
                resultSet.getDouble("TAX_PRODUCT"),
                resultSet.getDouble("TAX"),
                new CommonState(
                        resultSet.getLong("ID_CM_DET_BIL"),
                        resultSet.getInt("STATE_DET_BIL"),
                        resultSet.getDate("CREATION_DET_BIL"),
                        resultSet.getDate("UPDATE_DET_BIL")
                )

        );
    }
}
