package com.name.business.mappers;

import com.name.business.entities.Bill4Prod;
import com.name.business.entities.FabByProv;
import com.name.business.entities.getProviders;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FabByProvMapper implements ResultSetMapper<FabByProv> {
    @Override
    public FabByProv map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new FabByProv(
                resultSet.getString("PROVIDER"),
                resultSet.getLong("ID_PROVIDER")
        );
    }
}
