package com.name.business.entities;

import java.util.Date;

public class PedidosManuales {
    private Long ID_TAX;
    private Double PRICE;
    private Long ID_PRODUCT_STORE;
    private String PRODUCT_STORE_NAME;
    private String OWNBARCODE;
    private String PRODUCT_STORE_CODE;
    private Double TAX;
    private Long ID_PROVIDER;
    private String PROVIDER;
    private Long QUANTITY;

    public PedidosManuales(Long ID_TAX,
                                 Double PRICE,
                                 Long ID_PRODUCT_STORE,
                                 String PRODUCT_STORE_NAME,
                                 String OWNBARCODE,
                                 String PRODUCT_STORE_CODE,
                                 Double TAX,
                                 Long ID_PROVIDER,
                                 String PROVIDER,
                                 Long QUANTITY) {
        this.ID_TAX = ID_TAX;
        this.PRICE = PRICE;
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
        this.OWNBARCODE = OWNBARCODE;
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
        this.TAX = TAX;
        this.ID_PROVIDER = ID_PROVIDER;
        this.PROVIDER = PROVIDER;
        this.QUANTITY = QUANTITY;
    }


    public Long getQUANTITY() { return QUANTITY; }

    public void setQUANTITY(Long QUANTITY) { this.QUANTITY = QUANTITY; }



    public Long getID_TAX() {
        return ID_TAX;
    }
    public void setID_TAX(Long ID_TAX) {
        this.ID_TAX = ID_TAX;
    }

    public Double getPRICE() {
        return PRICE;
    }
    public void setPRICE(Double PRICE) { this.PRICE = PRICE; }

    public Long getID_PRODUCT_STORE() {
        return ID_PRODUCT_STORE;
    }
    public void setID_PRODUCT_STORE(Long ID_PRODUCT_STORE) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
    }

    public String getPRODUCT_STORE_NAME() {
        return PRODUCT_STORE_NAME;
    }
    public void setPRODUCT_STORE_NAME(String PRODUCT_STORE_NAME) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
    }

    public String getOWNBARCODE() {
        return OWNBARCODE;
    }
    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }

    public String getPRODUCT_STORE_CODE() {
        return PRODUCT_STORE_CODE;
    }
    public void setPRODUCT_STORE_CODE(String PRODUCT_STORE_CODE) {
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
    }

    public Double getTAX() {
        return TAX;
    }
    public void setTAX(Double TAX) { this.TAX = TAX; }

    public Long getID_PROVIDER() {
        return ID_PROVIDER;
    }
    public void setID_PROVIDER(Long ID_PROVIDER) {
        this.ID_PROVIDER = ID_PROVIDER;
    }

    public String getPROVIDER() {
        return PROVIDER;
    }
    public void setPROVIDER(String PROVIDER) {
        this.PROVIDER = PROVIDER;
    }


}
