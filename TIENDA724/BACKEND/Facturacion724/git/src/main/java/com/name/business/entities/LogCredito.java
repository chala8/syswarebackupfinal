package com.name.business.entities;

public class LogCredito {

    private Long ID_CREDITO_LOG;
    private String TEXTO;
    private String FECHA;


    public LogCredito(Long ID_CREDITO_LOG, String TEXTO, String FECHA) {
        this.ID_CREDITO_LOG = ID_CREDITO_LOG;
        this.TEXTO = TEXTO;
        this.FECHA = FECHA;
    }


    public Long getID_CREDITO_LOG() {
        return ID_CREDITO_LOG;
    }

    public void setID_CREDITO_LOG(Long ID_CREDITO_LOG) {
        this.ID_CREDITO_LOG = ID_CREDITO_LOG;
    }

    public String getTEXTO() {
        return TEXTO;
    }

    public void setTEXTO(String TEXTO) {
        this.TEXTO = TEXTO;
    }

    public String getFECHA() {
        return FECHA;
    }

    public void setFECHA(String FECHA) {
        this.FECHA = FECHA;
    }

}
