package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.ws.rs.QueryParam;
import java.util.Date;
import java.util.List;

public class StateChangeDTO {

    private Long ID_DETALLE_DETAIL_BILL;
    private Long IDESTADODESTINO;
    private String NOTAS;
    private Long IDESTADOORIGEN;
    private Long IDTHIRDUSER;
    private String ACTOR;

    @JsonCreator
    public StateChangeDTO(@JsonProperty("ID_DETALLE_DETAIL_BILL") Long ID_DETALLE_DETAIL_BILL,
                          @JsonProperty("IDESTADODESTINO") Long IDESTADODESTINO,
                          @JsonProperty("NOTAS") String NOTAS,
                          @JsonProperty("IDESTADOORIGEN") Long IDESTADOORIGEN,
                          @JsonProperty("IDTHIRDUSER") Long IDTHIRDUSER,
                          @JsonProperty("ACTOR") String ACTOR) {

        this.ID_DETALLE_DETAIL_BILL = ID_DETALLE_DETAIL_BILL;
        this.IDESTADODESTINO = IDESTADODESTINO;
        this.NOTAS = NOTAS;
        this.IDESTADOORIGEN = IDESTADOORIGEN;
        this.IDTHIRDUSER = IDTHIRDUSER;
        this.ACTOR = ACTOR;

    }


    public Long getID_DETALLE_DETAIL_BILL() {
        return ID_DETALLE_DETAIL_BILL;
    }

    public void setID_DETALLE_DETAIL_BILL(Long ID_DETALLE_DETAIL_BILL) {
        this.ID_DETALLE_DETAIL_BILL = ID_DETALLE_DETAIL_BILL;
    }

    public Long getIDESTADODESTINO() {
        return IDESTADODESTINO;
    }

    public void setIDESTADODESTINO(Long IDESTADODESTINO) {
        this.IDESTADODESTINO = IDESTADODESTINO;
    }

    public String getNOTAS() {
        return NOTAS;
    }

    public void setNOTAS(String NOTAS) {
        this.NOTAS = NOTAS;
    }

    public Long getIDESTADOORIGEN() {
        return IDESTADOORIGEN;
    }

    public void setIDESTADOORIGEN(Long IDESTADOORIGEN) {
        this.IDESTADOORIGEN = IDESTADOORIGEN;
    }

    public Long getIDTHIRDUSER() {
        return IDTHIRDUSER;
    }

    public void setIDTHIRDUSER(Long IDTHIRDUSER) {
        this.IDTHIRDUSER = IDTHIRDUSER;
    }

    public String getACTOR() {
        return ACTOR;
    }

    public void setACTOR(String ACTOR) {
        this.ACTOR = ACTOR;
    }
}
