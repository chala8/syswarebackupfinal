package com.name.business.businesses;

import com.name.business.DAOs.ReorderDAO;
import com.name.business.entities.*;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.billTypeSanitation;
import static com.name.business.sanitations.EntitySanitation.detailBillSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;
import static com.name.business.utils.security.CodeGenerator.generatorConsecutive;

import java.util.Date;
import java.util.List;

public class ReorderBusiness {
    private ReorderDAO reorderDAO;

    public ReorderBusiness(ReorderDAO reorderDAO) {
        this.reorderDAO = reorderDAO;
    }

    public Either<IException, List<reorderProd> > getReorderData( Long id_third,
                                                                         Long id_store,
                                                                         Date date1,
                                                                         Date date2) {
        try {
            List<reorderProd> validator = reorderDAO.getReorderData(id_third,
                    id_store,
                    date1,
                    date2);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<reorderProd> > getReorderData2( Long id_third,
                                                                  Long id_store,
                                                                  Double hours) {
        try {
            List<reorderProd> validator = reorderDAO.getReorderData2(id_third,
                    id_store,hours);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<reorderProd> > getReorderDataManual( Long id_third,
                                                                  Long id_store,
                                                                  Long days) {
        try {
            List<reorderProd> validator = reorderDAO.getReorderDataManual(id_third,
                                                                          id_store,
                                                                          days);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


}
