package com.name.business.entities;

import java.util.Date;

public class LegalData {
    private String RESOLUCION_DIAN;
    private String REGIMEN_TRIBUTARIO;
    private String AUTORETENEDOR;
    private String URL_LOGO;
    private String CITY_NAME;
    private String COUNTRY;
    private String ADDRESS;
    private String PHONE1;
    private String EMAIL;
    private String WEBPAGE;
    private String PREFIX_BILL;
    private String INITIAL_RANGE;
    private String FINAL_RANGE;
    private String START_CONSECUTIVE;
    private String NOTES;


    public LegalData( String RESOLUCION_DIAN,
             String REGIMEN_TRIBUTARIO,
             String AUTORETENEDOR,
             String URL_LOGO,
             String CITY_NAME,
             String COUNTRY,
             String ADDRESS,
             String PHONE1,
             String EMAIL,
             String WEBPAGE,
             String PREFIX_BILL,
             String INITIAL_RANGE,
             String FINAL_RANGE,
             String START_CONSECUTIVE,
             String NOTES) {
        this.RESOLUCION_DIAN = RESOLUCION_DIAN;
        this.REGIMEN_TRIBUTARIO = REGIMEN_TRIBUTARIO;
        this.AUTORETENEDOR = AUTORETENEDOR;
        this.URL_LOGO = URL_LOGO;
        this.CITY_NAME = CITY_NAME;
        this.COUNTRY = COUNTRY;
        this.ADDRESS = ADDRESS;
        this.PHONE1 = PHONE1;
        this.EMAIL = EMAIL;
        this.WEBPAGE = WEBPAGE;
        this.PREFIX_BILL = PREFIX_BILL;
        this.INITIAL_RANGE = INITIAL_RANGE;
        this.FINAL_RANGE = FINAL_RANGE;
        this.START_CONSECUTIVE = START_CONSECUTIVE;
        this.NOTES = NOTES;
    }
    public String getRESOLUCION_DIAN() {
        return RESOLUCION_DIAN;
    }
    public void setRESOLUCION_DIAN(String RESOLUCION_DIAN) {
        this.RESOLUCION_DIAN = RESOLUCION_DIAN;
    }
    public String getREGIMEN_TRIBUTARIO() {
        return REGIMEN_TRIBUTARIO;
    }
    public void setREGIMEN_TRIBUTARIO(String REGIMEN_TRIBUTARIO) {
        this.REGIMEN_TRIBUTARIO = REGIMEN_TRIBUTARIO;
    }
    public String getAUTORETENEDOR() {
        return AUTORETENEDOR;
    }
    public void setAUTORETENEDOR(String AUTORETENEDOR) {
        this.AUTORETENEDOR = AUTORETENEDOR;
    }
    public String getURL_LOGO() {
        return URL_LOGO;
    }
    public void setURL_LOGO(String URL_LOGO) {
        this.URL_LOGO = URL_LOGO;
    }
    public String getCITY_NAME() {
        return CITY_NAME;
    }
    public void setCITY_NAME(String CITY_NAME) {
        this.CITY_NAME = CITY_NAME;
    }
    public String getCOUNTRY() {
        return COUNTRY;
    }
    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }
    public String getADDRESS() {
        return ADDRESS;
    }
    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }
    public String getPHONE1() {
        return PHONE1;
    }
    public void setPHONE1(String PHONE1) {
        this.PHONE1 = PHONE1;
    }
    public String getEMAIL() {
        return EMAIL;
    }
    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }
    public String getWEBPAGE() {
        return WEBPAGE;
    }
    public void setWEBPAGE(String WEBPAGE) {
        this.WEBPAGE = WEBPAGE;
    }
    public String getPREFIX_BILL() {
        return PREFIX_BILL;
    }
    public void setPREFIX_BILL(String PREFIX_BILL) {
        this.PREFIX_BILL = PREFIX_BILL;
    }
    public String getINITIAL_RANGE() {
        return INITIAL_RANGE;
    }
    public void setINITIAL_RANGE(String INITIAL_RANGE) {
        this.INITIAL_RANGE = INITIAL_RANGE;
    }
    public String getFINAL_RANGE() {
        return FINAL_RANGE;
    }
    public void setFINAL_RANGE(String FINAL_RANGE) {
        this.FINAL_RANGE = FINAL_RANGE;
    }
    public String getSTART_CONSECUTIVE() {
        return START_CONSECUTIVE;
    }
    public void setSTART_CONSECUTIVE(String START_CONSECUTIVE) {
        this.START_CONSECUTIVE = START_CONSECUTIVE;
    }
    public String getNOTES() {
        return NOTES;
    }
    public void setNOTES(String NOTES) {
        this.NOTES = NOTES;
    }
}
