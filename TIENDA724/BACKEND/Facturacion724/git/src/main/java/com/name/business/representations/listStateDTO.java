package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class listStateDTO {

    private List<StateChangeDTO> list;

    @JsonCreator
    public listStateDTO(@JsonProperty("list") List<StateChangeDTO> list) {

        this.list = list;

    }

    public List<StateChangeDTO> getList() {
        return list;
    }

    public void setList(List<StateChangeDTO> list) {
        this.list = list;
    }
}
