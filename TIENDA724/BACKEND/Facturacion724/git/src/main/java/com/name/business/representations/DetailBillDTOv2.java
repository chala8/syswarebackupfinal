package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DetailBillDTOv2 {

    private Long cantidad;
    private Double valor;
    private Long idproductstore;
    private Long id_impuesto;


    @JsonCreator
    public DetailBillDTOv2(
                            @JsonProperty("cantidad") Long cantidad,
                            @JsonProperty("valor") Double valor,
                            @JsonProperty("idproductstore") Long idproductstore,
                            @JsonProperty("id_impuesto") Long id_impuesto) {


        this.cantidad = cantidad;
        this.valor = valor;
        this.idproductstore = idproductstore;
        this.id_impuesto = id_impuesto;
    }

    public Long getcantidad() {
        return cantidad;
    }
    public void setcantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public Double getvalor() {
        return valor;
    }
    public void setvalor(Double valor) {
        this.valor = valor;
    }

    public Long getidproductstore() { return idproductstore; }
    public void setidproductstore(Long idproductstore) { this.idproductstore = idproductstore; }

    public Long getid_impuesto() {
        return id_impuesto;
    }
    public void setid_impuesto(Long id_impuesto) {
        this.id_impuesto = id_impuesto;
    }
}
