package com.name.business.mappers;

import com.name.business.entities.RawDetailPaymentBill;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RawDetailPaymentBillMapper implements ResultSetMapper<RawDetailPaymentBill> {
    @Override
    public RawDetailPaymentBill map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new RawDetailPaymentBill(
                resultSet.getLong("ID_DETAIL_PAYMENT_BILL"),
                resultSet.getDouble("PAYMENT_VALUE"),
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("ID_PAYMENT_METHOD"),
                resultSet.getString("APPROBATION_CODE"),
                resultSet.getLong("ID_COMMON_STATE"),
                resultSet.getLong("ID_WAY_TO_PAY"),
                resultSet.getLong("ID_BANK_ENTITY")
        );
    }
}
