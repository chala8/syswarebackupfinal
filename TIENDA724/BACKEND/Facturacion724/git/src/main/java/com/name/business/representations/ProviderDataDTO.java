package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.glassfish.jersey.server.JSONP;

import java.util.Date;
import java.util.List;

public class ProviderDataDTO {
    private String mail;
    private String num_DOCUMENT;
    private String name;
    private String address;
    private String purchase_DATE;
    private String phone;
    private String description;
    private String document_NUMBER;

    @JsonCreator
    public ProviderDataDTO(@JsonProperty("mail") String mail,
            @JsonProperty("num_DOCUMENT") String num_DOCUMENT,
            @JsonProperty("name") String name,
            @JsonProperty("address") String address,
            @JsonProperty("purchase_DATE") String purchase_DATE,
            @JsonProperty("phone") String phone,
            @JsonProperty("description") String description,
            @JsonProperty("document_NUMBER") String document_NUMBER) {
        this.mail = mail;
        this.num_DOCUMENT = num_DOCUMENT;
        this.name = name;
        this.address = address;
        this.purchase_DATE = purchase_DATE;
        this.phone = phone;
        this.description = description;
        this.document_NUMBER = document_NUMBER;
    }

    public String getdocument_NUMBER() {
        return document_NUMBER;
    }

    public void setdocument_NUMBER(String document_NUMBER) {
        this.document_NUMBER = document_NUMBER;
    }


    public String getdescription() {
        return description;
    }

    public void setdescription(String description) {
        this.description = description;
    }


    public String getphone() {
        return phone;
    }

    public void setphone(String phone) {
        this.phone = phone;
    }


    public String getpurchase_DATE() {
        return purchase_DATE;
    }

    public void setpurchase_DATE(String purchase_DATE) {
        this.purchase_DATE = purchase_DATE;
    }


    public String getaddress() {
        return address;
    }

    public void setaddress(String address) {
        this.address = address;
    }


    public String getname() {
        return name;
    }

    public void setname(String name) {
        this.name = name;
    }


    public String getnum_DOCUMENT() {
        return num_DOCUMENT;
    }

    public void setnum_DOCUMENT(String num_DOCUMENT) {
        this.num_DOCUMENT = num_DOCUMENT;
    }


    public String getmail() {
        return mail;
    }

    public void setmail(String mail) {
        this.mail = mail;
    }

}
