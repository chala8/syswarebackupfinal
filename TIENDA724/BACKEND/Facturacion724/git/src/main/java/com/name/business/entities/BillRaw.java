package com.name.business.entities;

import java.util.Date;

public class BillRaw {
    //DEFINO LAS VARIABLES

    private long ID_BILL;
    private long ID_THIRD_EMPLOYEE;
    private long ID_THIRD;
    private long CONSECUTIVE;
    private double TOTALPRICE;
    private String PURCHASE_DATE;
    private long ID_PAYMENT_STATE;
    private long ID_BILL_STATE;
    private double SUBTOTAL;
    private double TAX;
    private long ID_BILL_TYPE;
    private long ID_BILL_FATHER;
    private long ID_COMMON_STATE;
    private double DISCOUNT;
    private long ID_DOCUMENT;
    private long ID_THIRD_DESTINITY;
    private long ID_CAJA;
    private long ID_STORE;
    private String NUM_DOCUMENTO;
    private String NUM_DOCUMENTO_ADICIONAL;
    private long ID_STORE_CLIENT;
    private String NUM_DOCUMENTO_CLIENTE;
    private long ID_THIRD_DOMICILIARIO;
    private String PURCHASE_DATE_DOCUMENT;

    //DEFINO EL CONSTRUCTOR
    public BillRaw(long ID_BILL,
                   long ID_THIRD_EMPLOYEE,
                   long ID_THIRD,
                   long CONSECUTIVE,
                   double TOTALPRICE,
                   String PURCHASE_DATE,
                   long ID_PAYMENT_STATE,
                   long ID_BILL_STATE,
                   double SUBTOTAL,
                   double TAX,
                   long ID_BILL_TYPE,
                   long ID_BILL_FATHER,
                   long ID_COMMON_STATE,
                   double DISCOUNT,
                   long ID_DOCUMENT,
                   long ID_THIRD_DESTINITY,
                   long ID_CAJA,
                   long ID_STORE,
                   String NUM_DOCUMENTO,
                   long ID_STORE_CLIENT,
                   String NUM_DOCUMENTO_CLIENTE,
                   long ID_THIRD_DOMICILIARIO,
                   String PURCHASE_DATE_DOCUMENT,
                   String NUM_DOCUMENTO_ADICIONAL
    ){
        this.ID_BILL = ID_BILL;
        this.ID_THIRD_EMPLOYEE = ID_THIRD_EMPLOYEE;
        this.ID_THIRD = ID_THIRD;
        this.CONSECUTIVE = CONSECUTIVE;
        this.TOTALPRICE = TOTALPRICE;
        this.PURCHASE_DATE = PURCHASE_DATE;
        this.ID_PAYMENT_STATE = ID_PAYMENT_STATE;
        this.ID_BILL_STATE = ID_BILL_STATE;
        this.SUBTOTAL = SUBTOTAL;
        this.TAX = TAX;
        this.ID_BILL_TYPE = ID_BILL_TYPE;
        this.ID_BILL_FATHER = ID_BILL_FATHER;
        this.ID_COMMON_STATE = ID_COMMON_STATE;
        this.DISCOUNT = DISCOUNT;
        this.ID_DOCUMENT = ID_DOCUMENT;
        this.ID_THIRD_DESTINITY = ID_THIRD_DESTINITY;
        this.ID_CAJA = ID_CAJA;
        this.ID_STORE = ID_STORE;
        this.NUM_DOCUMENTO = NUM_DOCUMENTO;
        this.ID_STORE_CLIENT = ID_STORE_CLIENT;
        this.NUM_DOCUMENTO_CLIENTE = NUM_DOCUMENTO_CLIENTE;
        this.ID_THIRD_DOMICILIARIO = ID_THIRD_DOMICILIARIO;
        this.PURCHASE_DATE_DOCUMENT = PURCHASE_DATE_DOCUMENT;
        this.NUM_DOCUMENTO_ADICIONAL = NUM_DOCUMENTO_ADICIONAL;
    }

    //DEFINO LOS GET Y SET
    public String getNUM_DOCUMENTO_ADICIONAL() {
        return NUM_DOCUMENTO_ADICIONAL;
    }

    public void setNUM_DOCUMENTO_ADICIONAL(String NUM_DOCUMENTO_ADICIONAL) { this.NUM_DOCUMENTO_ADICIONAL = NUM_DOCUMENTO_ADICIONAL; }

    public String getPURCHASE_DATE_DOCUMENT() {
        return PURCHASE_DATE_DOCUMENT;
    }

    public void setPURCHASE_DATE_DOCUMENT(String PURCHASE_DATE_DOCUMENT) { this.PURCHASE_DATE_DOCUMENT = PURCHASE_DATE_DOCUMENT; }

    public long getID_BILL() {
        return ID_BILL;
    }

    public void setID_BILL(long ID_BILL) {
        this.ID_BILL = ID_BILL;
    }

    public long getID_THIRD_EMPLOYEE() {
        return ID_THIRD_EMPLOYEE;
    }

    public void setID_THIRD_EMPLOYEE(long ID_THIRD_EMPLOYEE) {
        this.ID_THIRD_EMPLOYEE = ID_THIRD_EMPLOYEE;
    }

    public long getID_THIRD() {
        return ID_THIRD;
    }

    public void setID_THIRD(long ID_THIRD) {
        this.ID_THIRD = ID_THIRD;
    }

    public long getCONSECUTIVE() {
        return CONSECUTIVE;
    }

    public void setCONSECUTIVE(long CONSECUTIVE) {
        this.CONSECUTIVE = CONSECUTIVE;
    }

    public double getTOTALPRICE() {
        return TOTALPRICE;
    }

    public void setTOTALPRICE(double TOTALPRICE) {
        this.TOTALPRICE = TOTALPRICE;
    }

    public String getPURCHASE_DATE() {
        return PURCHASE_DATE;
    }

    public void setPURCHASE_DATE(String PURCHASE_DATE) {
        this.PURCHASE_DATE = PURCHASE_DATE;
    }

    public long getID_PAYMENT_STATE() {
        return ID_PAYMENT_STATE;
    }

    public void setID_PAYMENT_STATE(long ID_PAYMENT_STATE) {
        this.ID_PAYMENT_STATE = ID_PAYMENT_STATE;
    }

    public long getID_BILL_STATE() {
        return ID_BILL_STATE;
    }

    public void setID_BILL_STATE(long ID_BILL_STATE) {
        this.ID_BILL_STATE = ID_BILL_STATE;
    }

    public double getSUBTOTAL() {
        return SUBTOTAL;
    }

    public void setSUBTOTAL(double SUBTOTAL) {
        this.SUBTOTAL = SUBTOTAL;
    }

    public double getTAX() {
        return TAX;
    }

    public void setTAX(double TAX) {
        this.TAX = TAX;
    }

    public long getID_BILL_TYPE() {
        return ID_BILL_TYPE;
    }

    public void setID_BILL_TYPE(long ID_BILL_TYPE) {
        this.ID_BILL_TYPE = ID_BILL_TYPE;
    }

    public long getID_BILL_FATHER() {
        return ID_BILL_FATHER;
    }

    public void setID_BILL_FATHER(long ID_BILL_FATHER) {
        this.ID_BILL_FATHER = ID_BILL_FATHER;
    }

    public long getID_COMMON_STATE() {
        return ID_COMMON_STATE;
    }

    public void setID_COMMON_STATE(long ID_COMMON_STATE) {
        this.ID_COMMON_STATE = ID_COMMON_STATE;
    }

    public double getDISCOUNT() {
        return DISCOUNT;
    }

    public void setDISCOUNT(double DISCOUNT) {
        this.DISCOUNT = DISCOUNT;
    }

    public long getID_DOCUMENT() {
        return ID_DOCUMENT;
    }

    public void setID_DOCUMENT(long ID_DOCUMENT) {
        this.ID_DOCUMENT = ID_DOCUMENT;
    }

    public long getID_THIRD_DESTINITY() {
        return ID_THIRD_DESTINITY;
    }

    public void setID_THIRD_DESTINITY(long ID_THIRD_DESTINITY) {
        this.ID_THIRD_DESTINITY = ID_THIRD_DESTINITY;
    }

    public long getID_CAJA() {
        return ID_CAJA;
    }

    public void setID_CAJA(long ID_CAJA) {
        this.ID_CAJA = ID_CAJA;
    }

    public long getID_STORE() {
        return ID_STORE;
    }

    public void setID_STORE(long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    public String getNUM_DOCUMENTO() {
        return NUM_DOCUMENTO;
    }

    public void setNUM_DOCUMENTO(String NUM_DOCUMENTO) {
        this.NUM_DOCUMENTO = NUM_DOCUMENTO;
    }

    public long getID_STORE_CLIENT() {
        return ID_STORE_CLIENT;
    }

    public void setID_STORE_CLIENT(long ID_STORE_CLIENT) {
        this.ID_STORE_CLIENT = ID_STORE_CLIENT;
    }

    public String getNUM_DOCUMENTO_CLIENTE() {
        return NUM_DOCUMENTO_CLIENTE;
    }

    public void setNUM_DOCUMENTO_CLIENTE(String NUM_DOCUMENTO_CLIENTE) {
        this.NUM_DOCUMENTO_CLIENTE = NUM_DOCUMENTO_CLIENTE;
    }

    public long getID_THIRD_DOMICILIARIO() {
        return ID_THIRD_DOMICILIARIO;
    }

    public void setID_THIRD_DOMICILIARIO(long ID_THIRD_DOMICILIARIO) {
        this.ID_THIRD_DOMICILIARIO = ID_THIRD_DOMICILIARIO;
    }
}
