package com.name.business.entities;

import java.util.Date;

public class ClientePedido {

    private String NUM_DOCUMENT;
    private String PURCHASE_DATE;
    private String DESCRIPTION;
    private String PHONE;
    private String MAIL;
    private String ADDRESS;

    public ClientePedido(String NUM_DOCUMENT,
             String PURCHASE_DATE,
             String DESCRIPTION,
             String PHONE,
             String MAIL,
             String ADDRESS) {
        this.NUM_DOCUMENT = NUM_DOCUMENT;
        this.DESCRIPTION = DESCRIPTION;
        this.PHONE = PHONE;
        this.PURCHASE_DATE = PURCHASE_DATE;
        this.MAIL = MAIL;
        this.ADDRESS = ADDRESS;
    }
    //--------------------------------------------------------------------------------------------------

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }
    //--------------------------------------------------------------------------------------------------

    public String getMAIL() {
        return MAIL;
    }

    public void setMAIL(String MAIL) {
        this.MAIL = MAIL;
    }


    //--------------------------------------------------------------------------------------------------

    public String getPURCHASE_DATE() {
        return PURCHASE_DATE;
    }

    public void setPURCHASE_DATE(String PURCHASE_DATE) {
        this.PURCHASE_DATE = PURCHASE_DATE;
    }

    //--------------------------------------------------------------------------------------------------

    public String getPHONE() {
        return PHONE;
    }

    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    //--------------------------------------------------------------------------------------------------

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    //--------------------------------------------------------------------------------------------------

    public String getNUM_DOCUMENT() {
        return NUM_DOCUMENT;
    }

    public void setNUM_DOCUMENT(String NUM_DOCUMENT) {
        this.NUM_DOCUMENT = NUM_DOCUMENT;
    }

    //--------------------------------------------------------------------------------------------------

}