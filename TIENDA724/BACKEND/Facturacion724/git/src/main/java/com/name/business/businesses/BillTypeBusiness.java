package com.name.business.businesses;

import com.name.business.DAOs.BillTypeDAO;
import com.name.business.entities.*;
import com.name.business.representations.BillStateDTO;
import com.name.business.representations.BillTypeDTO;
import com.name.business.representations.CommonStateDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.billStateSanitation;
import static com.name.business.sanitations.EntitySanitation.billTypeSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

public class BillTypeBusiness {
   private BillTypeDAO billTypeDAO;
   private CommonStateBusiness commonStateBusiness;

    public BillTypeBusiness(BillTypeDAO billTypeDAO, CommonStateBusiness commonStateBusiness) {
        this.billTypeDAO = billTypeDAO;
        this.commonStateBusiness = commonStateBusiness;
    }


    public Either<IException, List<BillType>> getBillType(BillType billType) {
        List<String> msn = new ArrayList<>();
        try {

            //System.out.println("|||||||||||| Starting consults Bill Type ||||||||||||||||| ");
            return Either.right(billTypeDAO.read(billTypeSanitation(billType)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     *
     * @param billTypeDTO
     * @return
     */
    public Either<IException,Long> createBillType(BillTypeDTO billTypeDTO){
        List<String> msn = new ArrayList<>();
        Long id_bill_type = null;
        Long id_common_state = null;

        try {

            //System.out.println("|||||||||||| Starting creation Bill Type  ||||||||||||||||| ");
            if (billTypeDTO!=null){

                if (billTypeDTO.getStateDTO() == null) {
                    billTypeDTO.setStateDTO(new CommonStateDTO(1,null,null));
                }
                Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(billTypeDTO.getStateDTO());
                if (commonThirdEither.isRight()) {
                    id_common_state = commonThirdEither.right().value();
                }

                billTypeDAO.create(id_common_state,formatoStringSql(billTypeDTO.getName()));
                id_bill_type = billTypeDAO.getPkLast();

                return Either.right(id_bill_type);
            }else{
                //msn.add("It does not recognize Bill Type, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * @param id_bill_type
     * @param billTypeDTO
     * @return
     */
    public Either<IException, Long> updateBillType(Long id_bill_type, BillTypeDTO billTypeDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        PaymentState actualRegister=null;

        try {
            //System.out.println("|||||||||||| Starting update Bill Type ||||||||||||||||| ");
            if ((id_bill_type != null && id_bill_type > 0)) {



                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorID(id_bill_type).equals(false)){
                    //msn.add("It ID Bill Type  does not exist register on  Bill Type, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<BillType> read = billTypeDAO.read(
                        new BillType(id_bill_type, null,
                                new CommonState(null,null,null,null))
                );

                if (read.size()<=0){
                    //msn.add("It does not recognize ID Bill Type , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = billTypeDAO.readCommons(formatoLongSql(id_bill_type));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);

                    // Common  State business update
                    commonStateBusiness.updateCommonState(idCommons.getId_common_state(), billTypeDTO.getStateDTO());

                }

                if (billTypeDTO.getName() == null || billTypeDTO.getName().isEmpty()) {
                    billTypeDTO.setName(actualRegister.getName_payment_state());
                } else {
                    billTypeDTO.setName(formatoStringSql(billTypeDTO.getName()));
                }



                // Attribute update
                billTypeDAO.update(id_bill_type,billTypeDTO.getName());

                return Either.right(id_bill_type);

            } else {
                //msn.add("It does not recognize ID Bill Type, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id_bill_type
     * @return
     */
    public Either<IException, Long> deleteBillType(Long id_bill_type) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            //msn.add("OK");

            //System.out.println("|||||||||||| Starting delete Bill  Type ||||||||||||||||| ");

            if (id_bill_type != null && id_bill_type > 0) {

                List<CommonSimple> readCommons = billTypeDAO.readCommons(formatoLongSql(id_bill_type));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());

                return Either.right(id_bill_type);
            } else {
                //msn.add("It does not recognize ID Bill Type, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = billTypeDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }
}
