package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.glassfish.jersey.server.JSONP;

import java.util.Date;
import java.util.List;

public class BillPDTO {
    private Long id_third_employee;
    private Long id_third;
    private Long id_store;
    private Double totalprice;
    private Double subtotal;
    private Double tax;
    private Long id_bill_state;
    private Long id_bill_type;
    private Long id_third_destinity;
    private Long id_store_cliente;
    private String num_documento_cliente;

    @JsonCreator
    public BillPDTO(@JsonProperty("id_third_employee") Long id_third_employee,
                    @JsonProperty("id_third") Long id_third,
                    @JsonProperty("id_store") Long id_store,
                    @JsonProperty("totalprice") Double totalprice,
                    @JsonProperty("subtotal") Double subtotal,
                    @JsonProperty("tax") Double tax,
                    @JsonProperty("id_bill_state") Long id_bill_state,
                    @JsonProperty("id_bill_type") Long id_bill_type,
                    @JsonProperty("id_third_destinity") Long id_third_destinity,
                    @JsonProperty("id_store_cliente") Long id_store_cliente) {

        this.id_third_employee = id_third_employee;
        this.id_third = id_third;
        this.id_store = id_store;
        this.subtotal = subtotal;
        this.tax = tax;
        this.totalprice = totalprice;
        this.id_bill_state = id_bill_state;
        this.id_bill_type = id_bill_type;
        this.id_third_destinity = id_third_destinity;
        this.id_store_cliente = id_store_cliente; }
    public String getnum_documento_cliente() { return num_documento_cliente; }

    public void setnum_documento_cliente(String num_documento_cliente) {
        this.num_documento_cliente = num_documento_cliente;
    }

    public Long getid_third_employee() { return id_third_employee; }

    public void setid_third_employee(Long id_third_employee) {
        this.id_third_employee = id_third_employee;
    }

    public Long getid_third() {
        return id_third;
    }

    public void setid_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getid_store() {
        return id_store;
    }

    public void setid_store(Long id_store) {
        this.id_store = id_store;
    }

    public Long getid_bill_state() {
        return id_bill_state;
    }

    public void setid_bill_state(Long id_bill_state) {
        this.id_bill_state = id_bill_state;
    }

    public Long getid_bill_type() {
        return id_bill_type;
    }

    public void setid_bill_type(Long id_bill_type) {
        this.id_bill_type = id_bill_type;
    }

    public Long getid_third_destinity() {
        return id_third_destinity;
    }

    public void setid_third_destinity(Long id_third_destinity) {
        this.id_third_destinity = id_third_destinity;
    }

    public Long getid_store_cliente() {
        return id_store_cliente;
}

    public void setid_store_cliente(Long id_store_cliente) {
        this.id_store_cliente = id_store_cliente;
    }

    public Double getsubtotal() {
        return subtotal;
    }

    public void setsubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double gettax() {
        return tax;
    }

    public void settax(Double tax) {
        this.tax = tax;
    }

    public Double gettotalprice() {
        return totalprice;
    }

    public void settotalprice(Double totalprice) {
        this.totalprice = totalprice;
    }


}
