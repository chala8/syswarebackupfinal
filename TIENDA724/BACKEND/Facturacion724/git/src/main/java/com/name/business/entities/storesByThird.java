package com.name.business.entities;

import java.util.Date;

public class storesByThird {
    public Long getID_THIRD() {
        return ID_THIRD;
    }

    public void setID_THIRD(Long ID_THIRD) {
        this.ID_THIRD = ID_THIRD;
    }

    public Long getID_STORE() {
        return ID_STORE;
    }

    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getCITY_NAME() {
        return CITY_NAME;
    }

    public void setCITY_NAME(String CITY_NAME) {
        this.CITY_NAME = CITY_NAME;
    }

    public String getLATITUD() {
        return LATITUD;
    }

    public void setLATITUD(String LATITUD) {
        this.LATITUD = LATITUD;
    }

    public String getLONGITUD() {
        return LONGITUD;
    }

    public void setLONGITUD(String LONGITUD) {
        this.LONGITUD = LONGITUD;
    }

    public String getURL_LOGO() {
        return URL_LOGO;
    }

    public void setURL_LOGO(String URL_LOGO) {
        this.URL_LOGO = URL_LOGO;
    }

    public String getPHONE() {
        return PHONE;
    }

    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    public String getMAIL() {
        return MAIL;
    }

    public void setMAIL(String MAIL) {
        this.MAIL = MAIL;
    }

    private Long ID_THIRD;
    private Long ID_STORE;
    private String DESCRIPTION;
    private String ADDRESS;
    private String CITY_NAME;
    private String LATITUD;
    private String LONGITUD;
    private String URL_LOGO;
    private String PHONE;
    private String MAIL;

    public storesByThird (Long ID_THIRD,
             Long ID_STORE,
             String DESCRIPTION,
             String ADDRESS,
             String CITY_NAME,
             String LATITUD,
             String LONGITUD,
             String URL_LOGO,
             String PHONE,
             String MAIL) {
        this.ID_THIRD = ID_THIRD;
        this.ID_STORE = ID_STORE;
        this.DESCRIPTION = DESCRIPTION;
        this.ADDRESS = ADDRESS;
        this.CITY_NAME = CITY_NAME;
        this.LATITUD = LATITUD;
        this.LONGITUD = LONGITUD;
        this.URL_LOGO = URL_LOGO;
        this.PHONE = PHONE;
        this.MAIL = MAIL;
    }

}
