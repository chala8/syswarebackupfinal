package com.name.business.entities;

public class Bills4Credit {

    private Long id_bill;
    private String num_documento;
    private String tienda;

    public Bills4Credit(Long id_bill, String num_documento, String tienda) {
        this.id_bill = id_bill;
        this.num_documento = num_documento;
        this.tienda = tienda;
    }

    public Long getId_bill() {
        return id_bill;
    }

    public void setId_bill(Long id_bill) {
        this.id_bill = id_bill;
    }

    public String getNum_documento() {
        return num_documento;
    }

    public void setNum_documento(String num_documento) {
        this.num_documento = num_documento;
    }

    public String getTienda() {
        return tienda;
    }

    public void setTienda(String tienda) {
        this.tienda = tienda;
    }
}
