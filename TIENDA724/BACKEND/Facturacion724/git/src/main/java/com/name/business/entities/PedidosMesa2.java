package com.name.business.entities;

public class PedidosMesa2 {

    public Long getID_BILL() {
        return ID_BILL;
    }

    public void setID_BILL(Long ID_BILL) {
        this.ID_BILL = ID_BILL;
    }

    public String getPURCHASE_DATE() {
        return PURCHASE_DATE;
    }

    public void setPURCHASE_DATE(String PURCHASE_DATE) {
        this.PURCHASE_DATE = PURCHASE_DATE;
    }

    public Long getID_MESA() {
        return ID_MESA;
    }

    public void setID_MESA(Long ID_MESA) {
        this.ID_MESA = ID_MESA;
    }

    public String getMESA() {
        return MESA;
    }

    public void setMESA(String MESA) {
        this.MESA = MESA;
    }

    public Long getID_THIRD_DOMICILIARIO() {
        return ID_THIRD_DOMICILIARIO;
    }

    public void setID_THIRD_DOMICILIARIO(Long ID_THIRD_DOMICILIARIO) {        this.ID_THIRD_DOMICILIARIO = ID_THIRD_DOMICILIARIO;    }

    public Double getTOTALPRICE() {
        return TOTALPRICE;
    }

    public void setTOTALPRICE(Double TOTALPRICE) {
        this.TOTALPRICE = TOTALPRICE;
    }

    public String getMESERO() {
        return MESERO;
    }

    public void setMESERO(String MESERO) {
        this.MESERO = MESERO;
    }

    public Long getIDAPP() {
        return IDAPP;
    }

    public void setIDAPP(Long IDAPP) {
        this.IDAPP = IDAPP;
    }

    public Long getAMARILLO() {
        return AMARILLO;
    }

    public void setAMARILLO(Long AMARILLO) {
        this.AMARILLO = AMARILLO;
    }

    public Long getNARANJA() {
        return NARANJA;
    }

    public void setNARANJA(Long NARANJA) {
        this.NARANJA = NARANJA;
    }

    public Long getAZUL() {
        return AZUL;
    }

    public void setAZUL(Long AZUL) {
        this.AZUL = AZUL;
    }

    public Long getROJO() {
        return ROJO;
    }

    public void setROJO(Long ROJO) {
        this.ROJO = ROJO;
    }

    public Long getTOTAL_PRODUCTOS() {
        return TOTAL_PRODUCTOS;
    }

    public void setTOTAL_PRODUCTOS(Long TOTAL_PRODUCTOS) {
        this.TOTAL_PRODUCTOS = TOTAL_PRODUCTOS;
    }

    public String getMESA_NUMBER() {
        return MESA_NUMBER;
    }

    public void setMESA_NUMBER(String MESA_NUMBER) {
        this.MESA_NUMBER = MESA_NUMBER;
    }

    public Long getID_STORE() {
        return ID_STORE;
    }

    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    public Long getVIOLETA() {
        return VIOLETA;
    }

    public void setVIOLETA(Long VIOLETA) {
        this.VIOLETA = VIOLETA;
    }

    public String getCLIENTE() {        return CLIENTE;    }

    public void setCLIENTE(String CLIENTE) {        this.CLIENTE = CLIENTE;   }


    public String getESTADO_MESA() {        return ESTADO_MESA;    }


    public void setESTADO_MESA(String ESTADO_MESA) {        this.ESTADO_MESA = ESTADO_MESA;    }

    private Long ID_STORE;
    private Long ID_BILL;
    private String PURCHASE_DATE;
    private Long ID_MESA;
    private String MESA;
    private Long ID_THIRD_DOMICILIARIO;
    private Double TOTALPRICE;
    private String MESERO;
    private Long IDAPP;
    private Long AMARILLO;
    private Long NARANJA;
    private Long AZUL;
    private Long ROJO;
    private Long VIOLETA;
    private Long TOTAL_PRODUCTOS;
    private String MESA_NUMBER;
    private String CLIENTE;

    private String ESTADO_MESA;

    public PedidosMesa2(Long ID_BILL,
                        String PURCHASE_DATE,
                        Long ID_MESA,
                        String MESA,
                        Long ID_THIRD_DOMICILIARIO,
                        Double TOTALPRICE,
                        String MESERO,
                        Long IDAPP,
                        Long AMARILLO,
                        Long NARANJA,
                        Long AZUL,
                        Long ROJO,
                        Long VIOLETA,
                        Long TOTAL_PRODUCTOS,
                        String MESA_NUMBER,
                        Long ID_STORE,
                        String CLIENTE,
                        String ESTADO_MESA
    ) {
        this.ESTADO_MESA = ESTADO_MESA;
        this.CLIENTE = CLIENTE;
        this.VIOLETA = VIOLETA;
        this.ID_STORE = ID_STORE;
        this.MESA_NUMBER = MESA_NUMBER;
        this.ID_BILL = ID_BILL;
        this.PURCHASE_DATE = PURCHASE_DATE;
        this.ID_MESA = ID_MESA;
        this.MESA = MESA;
        this.ID_THIRD_DOMICILIARIO = ID_THIRD_DOMICILIARIO;
        this.TOTALPRICE = TOTALPRICE;
        this.MESERO = MESERO;
        this.IDAPP = IDAPP;
        this.AMARILLO = AMARILLO;
        this.NARANJA = NARANJA;
        this.AZUL = AZUL;
        this.ROJO = ROJO;
        this.TOTAL_PRODUCTOS = TOTAL_PRODUCTOS;
    }


}
