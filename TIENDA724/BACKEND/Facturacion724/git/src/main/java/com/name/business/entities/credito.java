package com.name.business.entities;

import java.util.Date;

public class credito {

    private Long ID_CREDITO;
    private String FECHA_CREACION;
    private Long ID_ESTADO_CREDITO;
    private Long MONTO;
    private String NUM_CUOTAS;
    private String INTERES;
    private String INTERES_MORA;
    private String PERDIODICIDAD_DIAS;
    private String FECHA_APROBACION;
    private String FECHA_DESEMBOLSO;
    private String CUOTA_INICIAL;
    private Long ID_BILL_FACTURA;
    private Long ID_THIRD_CLIENT;
    private Long ID_THIRD_PROVIDER;
    private Long ID_THIRD_CODEUDOR;

    private String cliente;

    private String doccliente;

    private String dircliente;

    private String phonecliente;

    private String ciudadCliente;

    private String codeudor;

    private String doccodeudor;

    private String dircodeudor;

    private String phonecodeudor;

    private String ciudadCodeudor;


    private String estado_credito;



    public credito(Long ID_CREDITO, String FECHA_CREACION, Long ID_ESTADO_CREDITO, Long MONTO, String NUM_CUOTAS, String INTERES, String INTERES_MORA, String PERDIODICIDAD_DIAS, String FECHA_APROBACION, String FECHA_DESEMBOLSO, String CUOTA_INICIAL, Long ID_BILL_FACTURA, Long ID_THIRD_CLIENT, Long ID_THIRD_PROVIDER, Long ID_THIRD_CODEUDOR, String cliente, String doccliente, String dircliente, String phonecliente, String ciudadCliente, String codeudor, String doccodeudor, String dircodeudor, String phonecodeudor, String ciudadCodeudor, String estado_credito) {
        this.ID_CREDITO = ID_CREDITO;
        this.FECHA_CREACION = FECHA_CREACION;
        this.ID_ESTADO_CREDITO = ID_ESTADO_CREDITO;
        this.MONTO = MONTO;
        this.NUM_CUOTAS = NUM_CUOTAS;
        this.INTERES = INTERES;
        this.INTERES_MORA = INTERES_MORA;
        this.PERDIODICIDAD_DIAS = PERDIODICIDAD_DIAS;
        this.FECHA_APROBACION = FECHA_APROBACION;
        this.FECHA_DESEMBOLSO = FECHA_DESEMBOLSO;
        this.CUOTA_INICIAL = CUOTA_INICIAL;
        this.ID_BILL_FACTURA = ID_BILL_FACTURA;
        this.ID_THIRD_CLIENT = ID_THIRD_CLIENT;
        this.ID_THIRD_PROVIDER = ID_THIRD_PROVIDER;
        this.ID_THIRD_CODEUDOR = ID_THIRD_CODEUDOR;
        this.cliente = cliente;
        this.doccliente = doccliente;
        this.dircliente = dircliente;
        this.phonecliente = phonecliente;
        this.ciudadCliente = ciudadCliente;
        this.codeudor = codeudor;
        this.doccodeudor = doccodeudor;
        this.dircodeudor = dircodeudor;
        this.phonecodeudor = phonecodeudor;
        this.ciudadCodeudor = ciudadCodeudor;
        this.estado_credito = estado_credito;
    }


    public String getEstado_credito() { return estado_credito; }


    public void setEstado_credito(String estado_credito) { this.estado_credito = estado_credito; }

    public Long getID_CREDITO() {
        return ID_CREDITO;
    }

    public void setID_CREDITO(Long ID_CREDITO) {
        this.ID_CREDITO = ID_CREDITO;
    }

    public String getFECHA_CREACION() {
        return FECHA_CREACION;
    }

    public void setFECHA_CREACION(String FECHA_CREACION) {
        this.FECHA_CREACION = FECHA_CREACION;
    }

    public Long getID_ESTADO_CREDITO() {
        return ID_ESTADO_CREDITO;
    }

    public void setID_ESTADO_CREDITO(Long ID_ESTADO_CREDITO) {
        this.ID_ESTADO_CREDITO = ID_ESTADO_CREDITO;
    }

    public Long getMONTO() {
        return MONTO;
    }

    public void setMONTO(Long MONTO) {
        this.MONTO = MONTO;
    }

    public String getNUM_CUOTAS() {
        return NUM_CUOTAS;
    }

    public void setNUM_CUOTAS(String NUM_CUOTAS) {
        this.NUM_CUOTAS = NUM_CUOTAS;
    }

    public String getINTERES() {
        return INTERES;
    }

    public void setINTERES(String INTERES) {
        this.INTERES = INTERES;
    }

    public String getINTERES_MORA() {
        return INTERES_MORA;
    }

    public void setINTERES_MORA(String INTERES_MORA) {
        this.INTERES_MORA = INTERES_MORA;
    }

    public String getPERDIODICIDAD_DIAS() {
        return PERDIODICIDAD_DIAS;
    }

    public void setPERDIODICIDAD_DIAS(String PERDIODICIDAD_DIAS) {
        this.PERDIODICIDAD_DIAS = PERDIODICIDAD_DIAS;
    }

    public String getFECHA_APROBACION() {
        return FECHA_APROBACION;
    }

    public void setFECHA_APROBACION(String FECHA_APROBACION) {
        this.FECHA_APROBACION = FECHA_APROBACION;
    }

    public String getFECHA_DESEMBOLSO() {
        return FECHA_DESEMBOLSO;
    }

    public void setFECHA_DESEMBOLSO(String FECHA_DESEMBOLSO) {
        this.FECHA_DESEMBOLSO = FECHA_DESEMBOLSO;
    }

    public String getCUOTA_INICIAL() {
        return CUOTA_INICIAL;
    }

    public void setCUOTA_INICIAL(String CUOTA_INICIAL) {
        this.CUOTA_INICIAL = CUOTA_INICIAL;
    }

    public Long getID_BILL_FACTURA() {
        return ID_BILL_FACTURA;
    }

    public void setID_BILL_FACTURA(Long ID_BILL_FACTURA) {
        this.ID_BILL_FACTURA = ID_BILL_FACTURA;
    }

    public Long getID_THIRD_CLIENT() {
        return ID_THIRD_CLIENT;
    }

    public void setID_THIRD_CLIENT(Long ID_THIRD_CLIENT) {
        this.ID_THIRD_CLIENT = ID_THIRD_CLIENT;
    }

    public Long getID_THIRD_PROVIDER() {
        return ID_THIRD_PROVIDER;
    }

    public void setID_THIRD_PROVIDER(Long ID_THIRD_PROVIDER) {
        this.ID_THIRD_PROVIDER = ID_THIRD_PROVIDER;
    }

    public Long getID_THIRD_CODEUDOR() {
        return ID_THIRD_CODEUDOR;
    }

    public void setID_THIRD_CODEUDOR(Long ID_THIRD_CODEUDOR) {
        this.ID_THIRD_CODEUDOR = ID_THIRD_CODEUDOR;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getDoccliente() {
        return doccliente;
    }

    public void setDoccliente(String doccliente) {
        this.doccliente = doccliente;
    }

    public String getDircliente() {
        return dircliente;
    }

    public void setDircliente(String dircliente) {
        this.dircliente = dircliente;
    }

    public String getPhonecliente() {
        return phonecliente;
    }

    public void setPhonecliente(String phonecliente) {
        this.phonecliente = phonecliente;
    }

    public String getCiudadCliente() {
        return ciudadCliente;
    }

    public void setCiudadCliente(String ciudadCliente) {
        this.ciudadCliente = ciudadCliente;
    }

    public String getCodeudor() {
        return codeudor;
    }

    public void setCodeudor(String codeudor) {
        this.codeudor = codeudor;
    }

    public String getDoccodeudor() {
        return doccodeudor;
    }

    public void setDoccodeudor(String doccodeudor) {
        this.doccodeudor = doccodeudor;
    }

    public String getDircodeudor() {
        return dircodeudor;
    }

    public void setDircodeudor(String dircodeudor) {
        this.dircodeudor = dircodeudor;
    }

    public String getPhonecodeudor() {
        return phonecodeudor;
    }

    public void setPhonecodeudor(String phonecodeudor) {
        this.phonecodeudor = phonecodeudor;
    }

    public String getCiudadCodeudor() {
        return ciudadCodeudor;
    }

    public void setCiudadCodeudor(String ciudadCodeudor) {
        this.ciudadCodeudor = ciudadCodeudor;
    }
}
