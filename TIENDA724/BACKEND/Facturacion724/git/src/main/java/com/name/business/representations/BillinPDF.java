package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class BillinPDF {
    private List<List<String>> detalles;
    private String obs;
    private String razon;
    private String fecha;
    private String nombreEmpresa;
    private String NIT;
    private String id_third;
    private String caso;
    private String tienda;
    private String num_documento;

    @JsonCreator
    public BillinPDF(
            @JsonProperty("detalles") List<List<String>> detalles,
            @JsonProperty("obs") String obs,
            @JsonProperty("razon") String razon,
            @JsonProperty("fecha") String fecha,
            @JsonProperty("nombreEmpresa") String nombreEmpresa,
            @JsonProperty("NIT") String NIT,
            @JsonProperty("id_third") String id_third,
            @JsonProperty("caso") String caso,
            @JsonProperty("tienda") String tienda,
            @JsonProperty("num_documento") String num_documento
    ) {
        this.detalles = detalles;
        this.obs = obs;
        this.razon = razon;
        this.fecha = fecha;
        this.nombreEmpresa = nombreEmpresa;
        this.NIT = NIT;
        this.id_third = id_third;
        this.tienda = tienda;
        this.caso = caso;
        this.num_documento = num_documento;
    }
    ///////////////////
    public List<List<String>> getdetalles() {
        return detalles;
    }
    public void setdetalles(List<List<String>> detalles) { this.detalles = detalles; }
    ///////////////////
    public String gettienda() {
        return tienda;
    }
    public void settienda(String tienda) { this.tienda = tienda; }
    ///////////////////
    public String getnum_documento() {
        return num_documento;
    }
    public void setnum_documento(String num_documento) { this.num_documento = num_documento; }
    ///////////////////
    public String getobs() {
        return obs;
    }
    public void setobs(String obs) { this.obs = obs; }
    ///////////////////
    public String getrazon() {
        return razon;
    }
    public void setrazon(String razon) { this.razon = razon; }
    ///////////////////
    public String getfecha() {
        return fecha;
    }
    public void setfecha(String fecha) { this.fecha = fecha; }
    ///////////////////
    public String getnombreEmpresa() {
        return nombreEmpresa;
    }
    public void setnombreEmpresa(String nombreEmpresa) { this.nombreEmpresa = nombreEmpresa; }
    ///////////////////
    public String getNIT() {
        return NIT;
    }
    public void setNIT(String NIT) { this.NIT = NIT; }
    ///////////////////
    public String getid_third() {
        return id_third;
    }
    public void setid_third(String id_third) { this.id_third = id_third; }
    ///////////////////
    public String getcaso() {
        return caso;
    }
    public void setcaso(String id_third) { this.caso = caso; }
}
