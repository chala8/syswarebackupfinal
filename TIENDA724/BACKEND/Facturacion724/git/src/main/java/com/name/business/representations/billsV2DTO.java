package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class billsV2DTO {

    private List<BillDTOv2> bills;
    private List<Date> dateList;

    @JsonCreator
    public billsV2DTO(@JsonProperty("bills") List<BillDTOv2> bills,
                     @JsonProperty("dates") List<Date> dateList
    ) {
        this.bills = bills;
        this.dateList = dateList;
    }

    public List<BillDTOv2> getbills() {
        return bills;
    }
    public void setbills(List<BillDTOv2> bills) {
        this.bills = bills;
    }

    public List<Date> getdateList() {
        return dateList;
    }
    public void setdateList(List<Date> dateList) {
        this.dateList = dateList;
    }

}
