package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BillStateDTO {
    private String name;
    private CommonStateDTO stateDTO;

    @JsonCreator
    public BillStateDTO(@JsonProperty("name") String name, @JsonProperty("state") CommonStateDTO stateDTO) {
        this.name = name;
        this.stateDTO = stateDTO;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
