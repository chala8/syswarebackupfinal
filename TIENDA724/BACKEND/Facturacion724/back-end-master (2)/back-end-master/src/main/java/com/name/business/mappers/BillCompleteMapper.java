package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillCompleteMapper implements ResultSetMapper<BillComplete> {

    @Override
    public BillComplete map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BillComplete(
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("ID_BILL_FATHER"),
                resultSet.getLong("ID_THIRD_EMPLOYEE"),
                resultSet.getLong("ID_THIRD"),
                resultSet.getString("CONSECUTIVE"),
                resultSet.getDate("PURCHASE_DATE"),
                resultSet.getDouble("SUBTOTAL"),
                resultSet.getDouble("TOTALPRICE"),
                resultSet.getDouble("TAX"),
                resultSet.getDouble("DISCOUNT"),
                new PaymentState(
                        resultSet.getLong("ID_PAYMENT_STATE"),
                        resultSet.getString("NAME_PAY_ST"),
                        new CommonState(
                                resultSet.getLong("ID_CM_PAY_ST"),
                                resultSet.getInt("STATE_CM_PAY_ST"),
                                resultSet.getDate("CREATION_PAY_ST"),
                                resultSet.getDate("UPDATE_PAY_ST")
                        )
                ),
                new BillState(
                    resultSet.getLong("ID_BILL_STATE"),
                    resultSet.getString("NAME_BIL_ST"),
                    new CommonState(
                            resultSet.getLong("ID_CM_BIL_ST"),
                            resultSet.getInt("STATE_CM_BIL_ST"),
                            resultSet.getDate("CREATION_BIL_ST"),
                            resultSet.getDate("UPDATE_BIL_ST")
                    )
                ),
                new BillType(
                        resultSet.getLong("ID_BILL_TYPE"),
                        resultSet.getString("NAME_BIL_TY"),
                        new CommonState(
                                resultSet.getLong("ID_CM_BIL_TY"),
                                resultSet.getInt("STATE_CM_BIL_TY"),
                                resultSet.getDate("CREATION_BIL_TY"),
                                resultSet.getDate("UPDATE_BIL_TY")
                        )
                ),
                new CommonState(
                        resultSet.getLong("ID_CM_BIL"),
                        resultSet.getInt("STATE_BIL"),
                        resultSet.getDate("CREATION_BIL"),
                        resultSet.getDate("UPDATE_BIL")
                )


        );
    }
}
