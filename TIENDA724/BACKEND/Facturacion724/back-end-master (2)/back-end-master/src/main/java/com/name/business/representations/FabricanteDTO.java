package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class FabricanteDTO {
    private String marca;
    private Double costoTotal;
    private Double precioTotal;
    private List<DetallePedidoDTO> detalles;

    @JsonCreator
    public FabricanteDTO(@JsonProperty("marca") String marca,
                         @JsonProperty("costoTotal")  Double costoTotal,
                         @JsonProperty("precioTotal")  Double precioTotal,
                         @JsonProperty("detalles")  List<DetallePedidoDTO> detalles) {

        this.marca = marca;
        this.costoTotal = costoTotal;
        this.precioTotal = precioTotal;
        this.detalles = detalles;
    }

    public String getmarca() {
        return marca;
    }

    public void setmarca(String marca) {
        this.marca = marca;
    }


    public Double getcostoTotal() {
        return costoTotal;
    }

    public void setcostoTotal(Double costoTotal) {
        this.costoTotal = costoTotal;
    }


    public Double getprecioTotal() {
        return precioTotal;
    }

    public void setprecioTotal(Double precioTotal) {
        this.precioTotal = precioTotal;
    }


    public List<DetallePedidoDTO> getdetalles() {
        return detalles;
    }

    public void setdetalles(List<DetallePedidoDTO> detalles) {
        this.detalles = detalles;
    }


}
