package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class DocumentDTO {
    private String title;
    private String body;

    @JsonCreator
    public DocumentDTO(@JsonProperty("title") String title, @JsonProperty("body") String body) {

        this.title = title;
        this.body = body;
    }

    public String gettitle() {
        return title;
    }

    public void settitle(String title) {
        this.title = title;
    }

    public String getbody() {
        return body;
    }

    public void setbody(String body) {
        this.body = body;
    }


}
