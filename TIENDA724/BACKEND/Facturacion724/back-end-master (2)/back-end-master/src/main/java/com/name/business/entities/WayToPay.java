package com.name.business.entities;

import java.util.Date;

public class WayToPay {
    private Long id_way_pay;
    private String name_way_pay;

    private Long id_state_way_pay;
    private Integer state_way_pay;
    private Date creation_way_pay;
    private Date update_way_pay;

    public WayToPay(Long id_way_pay, String name_way_pay, CommonState state) {
        this.id_way_pay = id_way_pay;
        this.name_way_pay = name_way_pay;


        this.id_state_way_pay = state.getId_common_state();
        this.state_way_pay=state.getState();
        this.creation_way_pay = state.getCreation_date();
        this.update_way_pay = state.getUpdate_date();
    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_way_pay(),
                this.getState_way_pay(),
                this.getCreation_way_pay(),
                this.getUpdate_way_pay()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_way_pay =state.getId_common_state();
        this.state_way_pay  =state.getState();
        this.creation_way_pay =state.getCreation_date();
        this.update_way_pay =state.getUpdate_date();
    }

    public Integer getState_way_pay() {
        return state_way_pay;
    }

    public void setState_way_pay(Integer state_way_pay) {
        this.state_way_pay = state_way_pay;
    }

    public Long getId_way_pay() {
        return id_way_pay;
    }

    public void setId_way_pay(Long id_way_pay) {
        this.id_way_pay = id_way_pay;
    }

    public String getName_way_pay() {
        return name_way_pay;
    }

    public void setName_way_pay(String name_way_pay) {
        this.name_way_pay = name_way_pay;
    }

    public Long getId_state_way_pay() {
        return id_state_way_pay;
    }

    public void setId_state_way_pay(Long id_state_way_pay) {
        this.id_state_way_pay = id_state_way_pay;
    }

    public Date getCreation_way_pay() {
        return creation_way_pay;
    }

    public void setCreation_way_pay(Date creation_way_pay) {
        this.creation_way_pay = creation_way_pay;
    }

    public Date getUpdate_way_pay() {
        return update_way_pay;
    }

    public void setUpdate_way_pay(Date update_way_pay) {
        this.update_way_pay = update_way_pay;
    }
}
