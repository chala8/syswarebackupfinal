package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.BillCompleteMapper;
import com.name.business.mappers.BillMapper;
import com.name.business.mappers. TopProdsMapper;
import com.name.business.mappers.BillDetailMapper;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.LegalDataMapper;
import com.name.business.mappers.BillMasterMapper;
import com.name.business.mappers.BillMasterMapper2;
import com.name.business.representations.BillDTO;
import com.name.business.representations.BillDetailIDDTO;
import com.name.business.representations.DocumentDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(BillMapper.class)
public interface BillDAO  {

    @SqlQuery("SELECT ID_BILL FROM BILL WHERE ID_BILL IN (SELECT MAX( ID_BILL ) FROM BILL )\n")
    Long getPkLast();

    @SqlUpdate("INSERT INTO DOCUMENT (ID_DOCUMENT, TITLE, BODY) VALUES (:ID_DOCUMENT,:title,:body)")
    void insertDocument(@Bind("ID_DOCUMENT") Long ID_DOCUMENT, @Bind("title") String title,@Bind("body") String body);

    @SqlUpdate("UPDATE BILL SET ID_DOCUMENT = :ID_DOCUMENT where ID_BILL =:ID_DOCUMENT")
    void updateIdDocument(@Bind("ID_DOCUMENT") Long ID_DOCUMENT);

    @SqlQuery("SELECT COUNT(ID_BILL) FROM BILL WHERE ID_BILL = :id")
    Integer getValidatorID(@Bind("id") Long id);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_BILL  ID, ID_COMMON_STATE FROM BILL WHERE( ID_BILL = :id OR :id IS NULL)\n ")
    List<CommonSimple> readCommons(@Bind("id") Long id);
    
    @SqlQuery("SELECT * FROM V_BILL V_BIL \n" +
            " WHERE (V_BIL.ID_BILL=:bil.id_bill OR :bil.id_bill IS NULL ) AND \n" +
            "      (V_BIL.ID_BILL_FATHER=:bil.id_bill_father OR :bil.id_bill_father IS NULL ) AND\n" +
            "      (V_BIL.ID_THIRD_EMPLOYEE=:bil.id_third_employee OR :bil.id_third_employee IS NULL ) AND\n" +
            "      (V_BIL.CONSECUTIVE=:bil.consecutive OR :bil.consecutive IS NULL ) AND\n" +
            "      (V_BIL.PURCHASE_DATE=:bil.purchase_date OR :bil.purchase_date IS NULL ) AND\n" +
            "      (V_BIL.SUBTOTAL=:bil.subtotal OR :bil.subtotal IS NULL ) AND\n" +
            "      (V_BIL.TOTALPRICE=:bil.totalprice OR :bil.totalprice IS NULL ) AND\n" +
            "      (V_BIL.TAX=:bil.tax OR :bil.tax IS NULL ) AND\n" +
            "      (V_BIL.ID_PAYMENT_STATE=:bil.id_payment_state OR :bil.id_payment_state IS NULL ) AND\n" +
            "      (V_BIL.ID_BILL_STATE=:bil.id_bill_state OR :bil.id_bill_state IS NULL ) AND\n" +
            "      (V_BIL.ID_BILL_TYPE=:bil.id_bill_type OR :bil.id_bill_type IS NULL ) AND\n" +
            "      (V_BIL.ID_CM_BIL=:bil.id_state_bill OR :bil.id_state_bill IS NULL ) AND\n" +
            "      (V_BIL.STATE_BIL=:bil.state_bill OR :bil.state_bill IS NULL ) AND\n" +
            "      (V_BIL.CREATION_BIL=:bil.creation_bill OR :bil.creation_bill IS NULL ) AND\n" +
            "      (V_BIL.UPDATE_BIL=:bil.update_bill OR :bil.update_bill IS NULL ) ")
    List<Bill> read(@BindBean("bil") Bill bill);

    @RegisterMapper(BillCompleteMapper.class)
    @SqlQuery("SELECT * FROM V_BILLCOMPLETE V_BIL \n " +
            "WHERE (V_BIL.ID_BILL =:bil.id_bill OR  :bil.id_bill IS NULL) AND\n" +
            "      (V_BIL.ID_BILL_FATHER =:bil.id_bill_father OR  :bil.id_bill_father IS NULL) AND\n" +
            "      (V_BIL.ID_THIRD_EMPLOYEE =:bil.id_third_employee OR  :bil.id_third_employee IS NULL) AND\n" +
            "      (V_BIL.ID_THIRD =:bil.id_third OR  :bil.id_third IS NULL) AND\n" +
            "      (V_BIL.CONSECUTIVE =:bil.consecutive OR  :bil.consecutive IS NULL) AND\n" +
            "      (V_BIL.PURCHASE_DATE =:bil.purchase_date OR  :bil.purchase_date IS NULL) AND\n" +
            "      (V_BIL.SUBTOTAL =:bil.subtotal OR  :bil.subtotal IS NULL) AND\n" +
            "      (V_BIL.TOTALPRICE =:bil.totalprice OR  :bil.totalprice IS NULL) AND\n" +
            "      (V_BIL.TAX =:bil.tax OR  :bil.tax IS NULL) AND\n" +
            "\n " +
            "      (V_BIL.ID_PAYMENT_STATE =:pay_st.id_payment_state OR  :pay_st.id_payment_state IS NULL) AND\n" +
            "      (V_BIL.NAME_PAY_ST =:pay_st.name_payment_state OR  :pay_st.name_payment_state IS NULL) AND\n" +
            "      (V_BIL.ID_CM_PAY_ST =:pay_st.id_state_payment_state OR  :pay_st.id_state_payment_state IS NULL) AND\n" +
            "      (V_BIL.STATE_CM_PAY_ST =:pay_st.state_payment_state OR  :pay_st.state_payment_state IS NULL) AND\n" +
            "      (V_BIL.CREATION_PAY_ST =:pay_st.creation_payment_state OR  :pay_st.creation_payment_state IS NULL) AND\n" +
            "      (V_BIL.UPDATE_PAY_ST =:pay_st.update_payment_state OR  :pay_st.update_payment_state IS NULL) AND\n" +
            "\n " +
            "      (V_BIL.ID_BILL_STATE =:bil_st.id_bill_state OR  :bil_st.id_bill_state IS NULL) AND\n" +
            "      (V_BIL.NAME_BIL_ST =:bil_st.name_bill_state OR  :bil_st.name_bill_state IS NULL) AND\n" +
            "      (V_BIL.ID_CM_BIL_ST =:bil_st.id_state_bill_state OR  :bil_st.id_state_bill_state IS NULL) AND\n" +
            "      (V_BIL.STATE_CM_BIL_ST =:bil_st.state_bill_state OR  :bil_st.state_bill_state IS NULL) AND\n" +
            "      (V_BIL.CREATION_BIL_ST =:bil_st.creation_bill_state OR  :bil_st.creation_bill_state IS NULL) AND\n" +
            "      (V_BIL.UPDATE_BIL_ST =:bil_st.update_bill_state OR  :bil_st.update_bill_state IS NULL) AND\n" +
            "\n " +
            "      (V_BIL.ID_BILL_TYPE =:bil_ty.id_bill_type OR  :bil_ty.id_bill_type IS NULL) AND\n" +
            "      (V_BIL.NAME_BIL_TY =:bil_ty.name_bill_type OR  :bil_ty.name_bill_type IS NULL) AND\n" +
            "      (V_BIL.ID_CM_BIL_TY =:bil_ty.id_state_bill_type OR  :bil_ty.id_state_bill_type IS NULL) AND\n" +
            "      (V_BIL.STATE_CM_BIL_TY =:bil_ty.state_bill_type OR  :bil_ty.state_bill_type IS NULL) AND\n" +
            "      (V_BIL.CREATION_BIL_TY =:bil_ty.creation_bill_type OR  :bil_ty.creation_bill_type IS NULL) AND\n" +
            "      (V_BIL.UPDATE_BIL_TY =:bil_ty.update_bill_type OR  :bil_ty.update_bill_type IS NULL) AND\n" +

            "      (V_BIL.ID_CM_BIL =:bil.id_state_bill OR  :bil.id_state_bill IS NULL) AND\n" +
            "      (V_BIL.STATE_BIL =:bil.state_bill OR  :bil.state_bill IS NULL) AND\n" +
            "      (V_BIL.CREATION_BIL =:bil.creation_bill OR  :bil.creation_bill IS NULL) AND\n" +
            "      (V_BIL.UPDATE_BIL =:bil.update_bill OR  :bil.update_bill IS NULL) ")
    List<BillComplete> readComplete(@BindBean("bil")BillComplete billComplete, @BindBean("pay_st")PaymentState paymentState,
                                    @BindBean("bil_st")BillState billState, @BindBean("bil_ty") BillType billType);


    @SqlUpdate("INSERT INTO BILL (ID_STORE, ID_COMMON_STATE,ID_THIRD_EMPLOYEE,ID_THIRD,TOTALPRICE,PURCHASE_DATE,ID_PAYMENT_STATE,ID_BILL_STATE,SUBTOTAL,TAX,DISCOUNT,ID_BILL_TYPE,ID_BILL_FATHER, ID_CAJA, ID_THIRD_DESTINITY)\n" +
        "            VALUES (:bil.id_store, :id_common_state,:bil.id_third,:bil.id_third_employee,\n" +
            "        :bil.totalprice,SYSDATE,:bil.id_payment_state,:bil.id_bill_state,:bil.subtotal,:bil.tax,:bil.discount,:bil.id_bill_type,:bil.id_bill_father, :id_caja, :bil.id_third_destiny) \n")
    void create(@Bind("consecutive") String consecutive_final,@Bind("id_common_state")Long id_common_state,@BindBean("bil") BillDTO billDTO, @Bind("id_caja") Long id_caja);



    @SqlUpdate(" UPDATE BILL SET \n" +
            "    ID_THIRD_EMPLOYEE = :bil.id_third_employee, \n" +
            "    ID_THIRD = :bil.id_third, \n" +
            "    TOTALPRICE = :bil.totalprice, \n" +
            "    PURCHASE_DATE = :bil.purchase_date,\n" +
            "    ID_PAYMENT_STATE=:bil.id_payment_state,\n" +
            "    ID_BILL_STATE=:bil.id_bill_state,\n" +
            "    SUBTOTAL=:bil.subtotal,\n" +
            "    TAX=:bil.tax,\n" +
            "    DISCOUNT=:bil.discount,\n" +
            "    ID_BILL_TYPE=:bil.id_bill_type,\n" +
            "    ID_BILL_FATHER=:bil.id_bill_father\n" +
            "    WHERE (ID_BILL =  :id_bill) \n ")
    int update(@Bind("id_bill")Long id_bill, @BindBean("bil") BillDetailIDDTO billDTO);

    @SqlQuery(" select id_third from TERCERO724.third where id_person = :id_person ")
    long getIdThird( @Bind("id_person") long id_person);

    @RegisterMapper(BillMasterMapper.class)
    @SqlQuery("  Select b.NUM_DOCUMENTO,b.purchase_date,l.prefix_bill,b.consecutive, cb.fullname, st.description store_name,ca.caja_number caja,doc.name,cb.document_number, b.subtotal,b.tax,b.totalprice\n" +
            "      from tercero724.document_type doc,tercero724.common_basicinfo cb,tercero724.third t,tercero724.legal_data l,facturacion724.bill b,\n" +
            "        tienda724.caja ca,tienda724.store st\n" +
            "      where doc.id_document_type=cb.id_document_type and cb.id_common_basicinfo=t.id_common_basicinfo and  t.id_legal_data=l.id_legal_data and t.id_third=b.id_third\n" +
            "      and b.id_caja=ca.id_caja and ca.id_store=st.id_store\n" +
            "      and b.id_bill=:id_bill and st.id_store=:id_store  ")
    BillMaster getMaestroFactura(@Bind("id_bill") Long id_bill,
                                 @Bind("id_store") Long id_store);

    @RegisterMapper(BillMasterMapper2.class)
    @SqlQuery("  Select b.purchase_date,l.prefix_bill,b.consecutive, cb.fullname, st.description store_name,doc.name,cb.document_number, b.subtotal,b.tax,b.totalprice\n" +
            "      from tercero724.document_type doc,tercero724.common_basicinfo cb,tercero724.third t,tercero724.legal_data l,facturacion724.bill b,\n" +
            "      tienda724.store st\n" +
            "      where doc.id_document_type=cb.id_document_type and cb.id_common_basicinfo=t.id_common_basicinfo and  t.id_legal_data=l.id_legal_data and t.id_third=b.id_third\n" +
            "      and b.id_bill=:id_bill and st.id_store=:id_store  ")
    BillMaster2 getMaestroFactura2(@Bind("id_bill") Long id_bill,
                                 @Bind("id_store") Long id_store);

    @RegisterMapper(TopProdsMapper.class)
    @SqlQuery("  select ownbarcode,product_store_code ,id_product_third,product_store_name,co.img, count(*) as ventas\n" +
            "from facturacion724.detail_bill db,facturacion724.bill b,tienda724.product_store ps,tienda724.codes co\n" +
            "where db.id_bill=b.id_bill and db.id_product_third=ps.id_product_store and ps.id_code=co.id_code\n" +
            "  and purchase_date between sysdate - 15 and sysdate and ps.id_store in (:idstore) and ownbarcode like '99999%' \n" +
            "group by ownbarcode,product_store_code,id_product_third,product_store_name,co.img\n" +
            "order by 6 desc ")
    List<TopProds> getTopProducts(@Bind("idstore") Long idstore);

    @RegisterMapper(BillDetailMapper.class)
    @SqlQuery("select ps.product_store_name,d.quantity,d.quantity*d.price valor, c.code\n" +
            " from facturacion724.detail_bill d,tienda724.product_store ps, tienda724.codes c\n" +
            " where d.id_product_third=ps.id_product_store\n" +
            "  and id_bill=:id_bill and ps.id_code = c.id_code")
    List<BillDetail> getDetalleFactura(@Bind("id_bill") Long id_bill);


    @SqlQuery("select sum(totalprice)  \n" +
            " from facturacion724.bill \n" +
            " where id_bill_type=1 and purchase_date between (select starting_date from facturacion724.cierre_caja where id_cierre_caja=:id_cierre_caja) and sysdate ")
    Double getSalesCaja(@Bind("id_cierre_caja") Long id_cierre_caja);


    @SqlUpdate("UPDATE FACTURACION724.BILL SET PURCHASE_DATE=:fecha, NUM_DOCUMENTO_CLIENTE=:dato  WHERE ID_BILL=:id_bill ")
    void putCompraBill(@Bind("id_bill")Long id_bill, @Bind("dato") String dato, @Bind("fecha") Date fecha);


    int delete();


    int deletePermanent();


    @RegisterMapper(LegalDataMapper.class)
    @SqlQuery(" select RESOLUCION_DIAN,REGIMEN_TRIBUTARIO,AUTORETENEDOR,URL_LOGO,city_name,co.descripcion country,address,phone1,email,webpage,\n" +
            "    prefix_bill,initial_range,final_range,start_consecutive,notes\n" +
            "    from tercero724.legal_data ld, tercero724.third t,TERCERO724.CITY CI,TERCERO724.COUNTRY CO \n" +
            "    where ld.id_legal_data=t.id_legal_data AND ld.id_city=ci.id_city and ld.id_country=co.id_country\n" +
            "    and id_third=:id_third ")
    List<LegalData> getLegalData(@Bind("id_third") Long id_third);



    @SqlCall(" call facturacion724.actualizar_valores_bill(:idbill) ")
    void procedure2(@Bind("idbill") Long idbill);



}
