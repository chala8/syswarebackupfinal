package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.BillStateBusiness;
import com.name.business.entities.BillData;
import com.name.business.entities.BillState;
import com.name.business.entities.CommonState;
import com.name.business.entities.Refund;
import com.name.business.representations.BillStateDTO;
import com.name.business.representations.PaymentStateDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;
import retrofit2.http.Query;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/billing-state")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BillStateResource {
   private BillStateBusiness billStateBusiness;

    public BillStateResource(BillStateBusiness billStateBusiness) {
        this.billStateBusiness = billStateBusiness;
    }

    @GET
    @Timed
    public Response getBillStateResource(
            @QueryParam("id_bill_state") Long id_bill_state,
            @QueryParam("name_bill_state") String name_bill_state,

            @QueryParam("id_state_bill_state") Long id_state_bill_state,
            @QueryParam("state_bill_state") Integer state_bill_state,
            @QueryParam("creation_bill_state") Date creation_bill_state,
            @QueryParam("update_bill_state") Date update_bill_state){

        Response response;

        Either<IException, List<BillState>> getResponseGeneric = billStateBusiness.getBillState(
                new BillState(id_bill_state, name_bill_state,
                        new CommonState(id_state_bill_state, state_bill_state,
                                creation_bill_state, update_bill_state)
                )
        );

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/pricePerProd")
    @Timed
    public Response getPricePerProd(
            @QueryParam("id_product_third") int id_product_third){

        Response response;

        Either<IException, List<Refund>> getResponseGeneric = billStateBusiness.getPricePerProd(id_product_third);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/billData")
    @Timed
    public Response getBillData(
            @QueryParam("consecutive") String consecutive,
            @QueryParam("id_store") Long id_store){

        Response response;
        System.out.println("--------------------------------");
        System.out.println(consecutive);
        System.out.println("--------------------------------");
        Either<IException, List<BillData>> getResponseGeneric = billStateBusiness.getBillData(consecutive,id_store);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @PUT
    @Path("/documentBody/{id_document}")
    @Timed
    public Response updateBodyBill(@PathParam("id_document") int id_document,@QueryParam("body") String body){

        Response response;
        Either<IException, String> allViewOffertsEither =billStateBusiness.putBodyPerDocument(id_document,body);
        System.out.println(body);
        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @PUT
    @Path("/billState/{id_bill}")
    @Timed
    public Response updateBillState(@PathParam("id_bill") int id_bill,@QueryParam("state") int state){

        Response response;
        Either<IException, String> allViewOffertsEither =billStateBusiness.updateBillState(id_bill,state);

        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateBillStateResource(@PathParam("id") Long id_bill_state, BillStateDTO billStateDTO){
        Response response;
        Either<IException, Long> getBillStateResource = billStateBusiness.updateBillState(id_bill_state, billStateDTO);

        if (getBillStateResource.isRight()){
            System.out.println(getBillStateResource.right().value());
            response=Response.status(Response.Status.OK).entity(getBillStateResource.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getBillStateResource);
        }
        return response;
    }


    @Path("/quantity")
    @PUT
    @Timed
    public Response putInventoryBill(@QueryParam("quantity") Long quantity,
                                     @QueryParam("id_store") Long id_store,
                                     @QueryParam("code") String code,
                                     @QueryParam("id_storage") Long id_storage){
        Response response;
        System.out.println(id_storage);
        Either<IException, Long> id_code = billStateBusiness.getIDCODEBycode(code);
        Either<IException, Long> id_product_store = billStateBusiness.getProductStoreByCodeStore(id_code.right().value(),id_store);
        Either<IException, String> getBillStateResource = null;

        if(id_storage == null){

            Either<IException, Long> id_storage2=billStateBusiness.getStorageByProductStore(id_product_store.right().value());
            Either<IException, Long> quantity_1=billStateBusiness.getQuantityByProductStorage(id_product_store.right().value(),id_storage2.right().value());
            Long quantityFinal = quantity_1.right().value() - quantity;
            getBillStateResource =
                    billStateBusiness.putInventoryBill(quantityFinal,id_product_store.right().value(),
                            id_storage2.right().value());

        }else{
            Either<IException, Long> quantity_1=billStateBusiness.getQuantityByProductStorage(id_product_store.right().value(),id_storage);
            Long quantityFinal = quantity_1.right().value() - quantity;
            getBillStateResource =
                    billStateBusiness.putInventoryBill(quantityFinal,id_product_store.right().value(),
                            id_storage);
        }

        if (getBillStateResource.isRight()){
            System.out.println(getBillStateResource.right().value());
            response=Response.status(Response.Status.OK).entity(getBillStateResource.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getBillStateResource);
        }


        return response;
    }


    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteBillStateResource(@PathParam("id") Long id_bill_state){
        Response response;
        Either<IException, Long> getResponseGeneric = billStateBusiness.deleteBillState(id_bill_state);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }
}
