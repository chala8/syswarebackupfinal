package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.PaymentMethodBusiness;
import com.name.business.entities.CommonState;
import com.name.business.entities.PaymentMethod;
import com.name.business.entities.PaymentState;
import com.name.business.representations.PaymentMethodDTO;
import com.name.business.representations.PaymentStateDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/payments-methods")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PaymentMethodResource {
   private PaymentMethodBusiness  paymentMethodBusiness;

    public PaymentMethodResource(PaymentMethodBusiness paymentMethodBusiness) {
        this.paymentMethodBusiness = paymentMethodBusiness;
    }

    @GET
    @Timed
    public Response getPaymentMethodResource(
            @QueryParam("id_payment_method") Long id_payment_method,
            @QueryParam("name_payment_method") String name_payment_method,

            @QueryParam("id_state_payment_method") Long id_state_payment_method,
            @QueryParam("state_payment_method") Integer state_payment_method,
            @QueryParam("creation_payment_method") Date creation_payment_method,
            @QueryParam("update_payment_method") Date update_payment_method
    ){

        Response response;

        Either<IException, List<PaymentMethod>> getResponseGeneric = paymentMethodBusiness.getPaymentMethod(
                new PaymentMethod(id_payment_method, name_payment_method,
                        new CommonState(id_state_payment_method, state_payment_method,
                                creation_payment_method, update_payment_method)
                )
        );

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @POST
    @Timed
    public Response postPaymentMethodResource(PaymentMethodDTO paymentStateDTO){
        Response response;

        Either<IException, Long> getResponseGeneric = paymentMethodBusiness.creatPaymentMethod(paymentStateDTO);


        if (getResponseGeneric.isRight()){

            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updatePaymentMethodResource(@PathParam("id") Long id_payment_method, PaymentMethodDTO paymentMethodDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = paymentMethodBusiness.updatePaymentMethod(id_payment_method, paymentMethodDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deletePaymentMethodResource(@PathParam("id") Long id_payment_method){
        Response response;
        Either<IException, Long> allViewOffertsEither = paymentMethodBusiness.deletePaymentMethod(id_payment_method);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
