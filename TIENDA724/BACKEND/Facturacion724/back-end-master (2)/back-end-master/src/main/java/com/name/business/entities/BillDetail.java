package com.name.business.entities;

import java.util.Date;

public class BillDetail {

    private String PRODUCT_STORE_NAME;
    private Long QUANTITY;
    private Long VALOR;
    private String CODE;


    public BillDetail(String PRODUCT_STORE_NAME,
                      Long QUANTITY,
                      Long VALOR,
                      String CODE) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
        this.QUANTITY = QUANTITY;
        this.VALOR = VALOR;
        this.CODE = CODE;
    }

    //--------------------------------------------------------------------------------------------------

    public String getPRODUCT_STORE_NAME() {
        return PRODUCT_STORE_NAME;
    }

    public void setPRODUCT_STORE_NAME(String PRODUCT_STORE_NAME) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
    }

    //--------------------------------------------------------------------------------------------------

    public Long getQUANTITY() {
        return QUANTITY;
    }

    public void setQUANTITY(Long QUANTITY) {
        this.QUANTITY = QUANTITY;
    }

    //--------------------------------------------------------------------------------------------------

    public Long getVALOR() {
        return VALOR;
    }

    public void setVALOR(Long VALOR) {
        this.VALOR = VALOR;
    }

    //--------------------------------------------------------------------------------------------------

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    //--------------------------------------------------------------------------------------------------




}
