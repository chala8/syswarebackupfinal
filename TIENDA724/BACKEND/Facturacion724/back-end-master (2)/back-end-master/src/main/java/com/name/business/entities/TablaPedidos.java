package com.name.business.entities;

public class TablaPedidos {

    private Long ID_STORE_CLIENT;
    private Long ID_STORE_PROVIDER;
    private Long ID_THIRD_CLIENT;
    private Long ID_THIRD_EMPLOYEE;
    private Long ID_THIRD_DESTINITY;
    private Long ID_THIRD_EMP_PROVIDER;



    public TablaPedidos(Long ID_STORE_CLIENT,
                        Long ID_STORE_PROVIDER,
                        Long ID_THIRD_CLIENT,
                        Long ID_THIRD_EMPLOYEE,
                        Long ID_THIRD_DESTINITY,
                        Long ID_THIRD_EMP_PROVIDER) {
        this.ID_STORE_CLIENT = ID_STORE_CLIENT;
        this.ID_STORE_PROVIDER = ID_STORE_PROVIDER;
        this.ID_THIRD_CLIENT =  ID_THIRD_CLIENT;
        this.ID_THIRD_EMPLOYEE = ID_THIRD_EMPLOYEE;
        this.ID_THIRD_DESTINITY = ID_THIRD_DESTINITY;
        this.ID_THIRD_EMP_PROVIDER = ID_THIRD_EMP_PROVIDER;
    }
    //----------------------------------------------------------------------------
    public Long getID_STORE_CLIENT() {
        return ID_STORE_CLIENT;
    }
    public void setID_STORE_CLIENT(Long ID_STORE_CLIENT) {
        this.ID_STORE_CLIENT = ID_STORE_CLIENT;
    }
    //----------------------------------------------------------------------------
    public Long getID_STORE_PROVIDER() {
        return ID_STORE_PROVIDER;
    }
    public void setID_STORE_PROVIDER(Long ID_STORE_PROVIDER) {
        this.ID_STORE_PROVIDER = ID_STORE_PROVIDER;
    }
    //----------------------------------------------------------------------------
    public Long getID_THIRD_CLIENT() {
        return ID_THIRD_CLIENT;
    }
    public void setID_THIRD_CLIENT(Long ID_THIRD_CLIENT) {
        this.ID_THIRD_CLIENT = ID_THIRD_CLIENT;
    }
    //----------------------------------------------------------------------------
    public Long getID_THIRD_EMPLOYEE() {
        return ID_THIRD_EMPLOYEE;
    }
    public void setID_THIRD_EMPLOYEE(Long ID_THIRD_EMPLOYEE) {
        this.ID_THIRD_EMPLOYEE = ID_THIRD_EMPLOYEE;
    }
    //----------------------------------------------------------------------------
    public Long getID_THIRD_DESTINITY() {
        return ID_THIRD_DESTINITY;
    }
    public void setID_THIRD_DESTINITY(Long ID_THIRD_DESTINITY) {
        this.ID_THIRD_DESTINITY = ID_THIRD_DESTINITY;
    }
    //----------------------------------------------------------------------------
    public Long getID_THIRD_EMP_PROVIDER() {
        return ID_THIRD_EMP_PROVIDER;
    }
    public void setID_THIRD_EMP_PROVIDER(Long ID_THIRD_EMP_PROVIDER) { this.ID_THIRD_EMP_PROVIDER = ID_THIRD_EMP_PROVIDER; }
    //----------------------------------------------------------------------------
}
