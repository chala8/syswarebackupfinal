package com.name.business.businesses;


import com.name.business.DAOs.DetailPaymentBillDAO;
import com.name.business.entities.*;
import com.name.business.representations.BillDTO;
import com.name.business.representations.DetailBillIdDTO;
import com.name.business.representations.DetailPaymentBillDTO;
import com.name.business.representations.DetailPaymentBillIdDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.detailBillSanitation;
import static com.name.business.sanitations.EntitySanitation.detailPaymentBillCompleteSanitation;
import static com.name.business.sanitations.EntitySanitation.detailPaymentBillSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;
import static com.name.business.utils.security.CodeGenerator.generatorConsecutive;

public class DetailPaymentBillBusiness {
    private DetailPaymentBillDAO detailPaymentBillDAO;
    private CommonStateBusiness commonStateBusiness;
    private PaymentMethodBusiness paymentMethodBusiness;
    private WayToPayBusiness wayToPayBusiness;

    public DetailPaymentBillBusiness(DetailPaymentBillDAO detailPaymentBillDAO, CommonStateBusiness commonStateBusiness, PaymentMethodBusiness paymentMethodBusiness, WayToPayBusiness wayToPayBusiness) {
        this.detailPaymentBillDAO = detailPaymentBillDAO;
        this.commonStateBusiness = commonStateBusiness;
        this.paymentMethodBusiness = paymentMethodBusiness;
        this.wayToPayBusiness = wayToPayBusiness;
    }

    public Either<IException, List<DetailPaymentBill>> getDetailPaymentBill(DetailPaymentBill detailPaymentBill) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Detail Payment ||||||||||||||||| ");
            return Either.right(detailPaymentBillDAO.read(detailPaymentBillSanitation(detailPaymentBill)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<DetailPaymentBillComplete>> getDetailPaymentBillComplete(DetailPaymentBillComplete detailPaymentBillComplete) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Detail Payment ||||||||||||||||| ");
            DetailPaymentBillComplete detailPaymentBillCompleteSanitation = detailPaymentBillCompleteSanitation(detailPaymentBillComplete);
            return Either.right(detailPaymentBillDAO.readComplete(detailPaymentBillCompleteSanitation,detailPaymentBillCompleteSanitation.getPayment_method(),detailPaymentBillCompleteSanitation.getWay_pay()) );
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
    /**
     *
     * @param detailPaymentBillDTOList
     * @return
     */
    public Either<IException,Long> createDetailPaymentBill(Long id_bill, List<DetailPaymentBillDTO> detailPaymentBillDTOList){
        List<String> msn = new ArrayList<>();
        Long id_deatail_payment_bill = null;
        Long id_common_state = null;


        try {
            if (formatoLongSql(id_bill) > 0 && !validatorIDBill(id_bill)) {  /// It can be null, if The Bill does not exist, or It can given the id Biling exist

                msn.add("It ID Bill does not exist register on  Bill, probably it has bad ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if (detailPaymentBillDTOList==null || detailPaymentBillDTOList.size()<1){

                msn.add("It Detail Payment Bill does not exist for register on  Data Base, probably it has bad ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            for (DetailPaymentBillDTO detailPaymentBillDTO:detailPaymentBillDTOList ) {
                System.out.println("|||||||||||| Starting creation Detail Payment Bill   ||||||||||||||||| ");
                if (detailPaymentBillDTO != null) {


                    if (formatoLongSql(detailPaymentBillDTO.getId_payment_method()) > 0 && !paymentMethodBusiness.validatorID(detailPaymentBillDTO.getId_payment_method())) {

                        msn.add("It ID Payment Method does not exist register on  Payment Method, probably it has bad ");
                        return Either.left(new BussinessException(messages_errors(msn)));
                    }

                    if (formatoLongSql(detailPaymentBillDTO.getId_way_to_pay()) > 0 && !wayToPayBusiness.validatorID(detailPaymentBillDTO.getId_way_to_pay())) {

                        msn.add("It ID Way to Pay does not exist register on  Way to Pay, probably it has bad ");
                        return Either.left(new BussinessException(messages_errors(msn)));

                    }


                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(detailPaymentBillDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                    if (detailPaymentBillDTO.getAprobation_code() == null) {
                        detailPaymentBillDTO.setAprobation_code(generatorConsecutive() + "");
                    }


                    detailPaymentBillDAO.create(id_common_state, id_bill, detailPaymentBillDTO);
                    id_deatail_payment_bill = detailPaymentBillDAO.getPkLast();



                } else {
                    msn.add("It does not recognize Product, probably it has bad format");
                }
            }
            if (msn.size()>0){
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            return Either.right(id_bill);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    private Boolean validatorIDBill(Long id_bill) {
        Integer validator = 0;
        if (id_bill==null){
            return false;
        }
        try {
            validator = detailPaymentBillDAO.getValidatorIDBill(id_bill);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }


    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = detailPaymentBillDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }



    /**
     * @param detailPaymentBillIdDTOList
     * @param id_bill
     * @return
     */
    public Either<IException, Long> updateDetailPaymentBill(Long id_bill, List<DetailPaymentBillIdDTO> detailPaymentBillIdDTOList) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        DetailPaymentBill actualRegister=null;
        List<DetailPaymentBill> read= new ArrayList<>();

        try {
            System.out.println("|||||||||||| Starting update Detail Payment Bill ||||||||||||||||| ");
            if ((id_bill != null && id_bill > 0)) {



                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorIDBill(id_bill).equals(false)){
                    msn.add("It ID Bill  does not exist register on  Bill, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                for (DetailPaymentBillIdDTO detailPaymentBillIdDTO:detailPaymentBillIdDTOList){
                    //TODO validate the  id attribute if exist on database a register with this id
                    if (validatorID(detailPaymentBillIdDTO.getId_detail_payment_bill()).equals(false)){
                        msn.add("It ID Detail Bill   does not exist register on  Detail Bill, probably it has bad ");
                        continue;

                    }else {
                        read= detailPaymentBillDAO.read(
                                new DetailPaymentBill(detailPaymentBillIdDTO.getId_detail_payment_bill(), id_bill,null,null,null,null,
                                        new CommonState(null,null,null,null))
                        );



                        if (read.size()<=0){
                            msn.add("It does not recognize ID Detail Payment Bill, probably it has bad format");
                            continue;
                        }else {
                            actualRegister=read.get(0);
                        }


                        List<CommonSimple> readCommons = detailPaymentBillDAO.readCommons(formatoLongSql(detailPaymentBillIdDTO.getId_detail_payment_bill()),null);
                        if (readCommons.size()>0){
                            idCommons = readCommons.get(0);
                            // Common  State business update
                            commonStateBusiness.updateCommonState(idCommons.getId_common_state(), detailPaymentBillIdDTO.getStateDTO());
                        }

                        if (formatoLongSql(detailPaymentBillIdDTO.getId_payment_method()) > 0 && !paymentMethodBusiness.validatorID(detailPaymentBillIdDTO.getId_payment_method())) {

                            msn.add("It ID Payment Method does not exist register on  Payment Method, probably it has bad ");
                            continue;
                        }

                        if (formatoLongSql(detailPaymentBillIdDTO.getId_way_to_pay()) > 0 && !wayToPayBusiness.validatorID(detailPaymentBillIdDTO.getId_way_to_pay())) {

                            msn.add("It ID Way to Pay does not exist register on  Way to Pay, probably it has bad ");
                            continue;

                        }

                        if (detailPaymentBillIdDTO.getId_payment_method() == null || detailPaymentBillIdDTO.getId_payment_method()<=0) {
                            detailPaymentBillIdDTO.setId_payment_method(actualRegister.getId_payment_method());
                        } else {
                            detailPaymentBillIdDTO.setId_payment_method(formatoLongSql(detailPaymentBillIdDTO.getId_payment_method()));
                        }

                        if (detailPaymentBillIdDTO.getId_way_to_pay()== null || detailPaymentBillIdDTO.getId_way_to_pay()<=0) {
                            detailPaymentBillIdDTO.setId_way_to_pay(actualRegister.getId_way_to_pay());
                        } else {
                            detailPaymentBillIdDTO.setId_way_to_pay(formatoLongSql(detailPaymentBillIdDTO.getId_way_to_pay()));
                        }

                        if (detailPaymentBillIdDTO.getAprobation_code() == null) {
                            detailPaymentBillIdDTO.setAprobation_code(actualRegister.getAprobation_code());
                        } else {
                            detailPaymentBillIdDTO.setAprobation_code(formatoStringSql(detailPaymentBillIdDTO.getAprobation_code()));
                        }

                        if (detailPaymentBillIdDTO.getPayment_value() == null) {
                            detailPaymentBillIdDTO.setPayment_value(actualRegister.getPayment_value());
                        } else {
                            detailPaymentBillIdDTO.setPayment_value(formatoDoubleSql(detailPaymentBillIdDTO.getPayment_value()));
                        }

                        // Attribute update
                        detailPaymentBillDAO.update(detailPaymentBillIdDTO.getId_detail_payment_bill(),id_bill,detailPaymentBillIdDTO);
                    }

                }
                return Either.right(id_bill);

            } else {
                msn.add("It does not recognize ID Detail Payment Bill, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * @param id_bill
     * @return
     */
    public Either<IException, Long> deleteDetailPaymentBill(Long id_detail_payment_bill,Long id_bill) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        List<CommonSimple> readCommons= new ArrayList<>();
        try {

            System.out.println("|||||||||||| Starting delete Detail  Payment Bill ||||||||||||||||| ");

            if ((id_detail_payment_bill != null && id_detail_payment_bill > 0) || (id_bill != null && id_bill > 0)) {

                if ((id_detail_payment_bill != null && id_detail_payment_bill > 0) && (id_bill != null && id_bill > 0) ){

                    readCommons = detailPaymentBillDAO.readCommons(formatoLongSql(id_detail_payment_bill),formatoLongSql(id_bill));

                }else  if (id_detail_payment_bill != null && id_detail_payment_bill > 0){
                    readCommons = detailPaymentBillDAO.readCommons(formatoLongSql(id_detail_payment_bill),formatoLongSql(null));
                }else  if (id_bill != null && id_bill > 0){
                    readCommons = detailPaymentBillDAO.readCommons(formatoLongSql(null),formatoLongSql(id_bill));

                }else {
                    msn.add("It does not recognize ID Detail Payment Bill or ID Bill, probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                if (readCommons.size()>0){
                    for (CommonSimple cmSim: readCommons){
                        Either<IException, Long> deleteCommonState = commonStateBusiness.deleteCommonState(cmSim.getId_common_state());
                    }

                }

                return Either.right(id_detail_payment_bill!=null?id_detail_payment_bill:id_bill);


            } else {
                msn.add("It does not recognize ID Detail Payment Bill, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
