package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.reorderProd;
import org.glassfish.jersey.server.JSONP;

import java.util.Date;
import java.util.List;

public class reorderProdDTO {

    private Double PERCENT;
    private String OWNBARCODE;
    private String PRODUCTO;
    private String LINEA;
    private String CATEGORIA;
    private String MARCA;
    private String PRESENTACION;
    private Long COSTO;
    private Long CANTIDAD;

    @JsonCreator
    public reorderProdDTO(
                      @JsonProperty("percent") Double PERCENT,
                      @JsonProperty("ownbarcode") String OWNBARCODE,
                      @JsonProperty("producto") String PRODUCTO,
                      @JsonProperty("linea") String LINEA,
                      @JsonProperty("categoria") String CATEGORIA,
                      @JsonProperty("marca") String MARCA,
                      @JsonProperty("presentacion") String PRESENTACION,
                      @JsonProperty("costo") Long COSTO,
                      @JsonProperty("cantidad") Long CANTIDAD) {
        this.PERCENT = PERCENT;
        this.OWNBARCODE = OWNBARCODE;
        this.PRODUCTO = PRODUCTO;
        this.LINEA = LINEA;
        this.CATEGORIA = CATEGORIA;
        this.MARCA = MARCA;
        this.PRESENTACION = PRESENTACION;
        this.COSTO = COSTO;
        this.CANTIDAD = CANTIDAD;

    }
    public Double getPERCENT() {
        return PERCENT;
    }

    public void setPERCENT(Double PERCENT) {
        this.PERCENT = PERCENT;
    }

    public String getOWNBARCODE() {
        return OWNBARCODE;
    }

    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }

    public String getPRODUCTO() {
        return PRODUCTO;
    }

    public void setPRODUCTO(String PRODUCTO) {
        this.PRODUCTO = PRODUCTO;
    }

    public String getLINEA() {
        return LINEA;
    }

    public void setLINEA(String LINEA) {
        this.LINEA = LINEA;
    }

    public String getCATEGORIA() {
        return CATEGORIA;
    }

    public void setCATEGORIA(String CATEGORIA) {
        this.CATEGORIA = CATEGORIA;
    }

    public String getMARCA() {
        return MARCA;
    }

    public void setMARCA(String MARCA) {
        this.MARCA = MARCA;
    }

    public String getPRESENTACION() {
        return PRESENTACION;
    }

    public void setPRESENTACION(String PRESENTACION) {
        this.PRESENTACION = PRESENTACION;
    }

    public Long getCOSTO() {
        return COSTO;
    }

    public void setCOSTO(Long COSTO) {
        this.COSTO = COSTO;
    }

    public Long getCANTIDAD() {
        return CANTIDAD;
    }

    public void setCANTIDAD(Long CANTIDAD) {
        this.CANTIDAD = CANTIDAD;
    }


}
