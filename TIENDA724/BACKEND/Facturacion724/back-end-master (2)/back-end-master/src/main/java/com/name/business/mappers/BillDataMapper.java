package com.name.business.mappers;

import com.name.business.entities.Bill;
import com.name.business.entities.CommonState;
import com.name.business.entities.BillData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillDataMapper  implements ResultSetMapper<BillData> {

    @Override
    public BillData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BillData(
                resultSet.getLong("ID_BILL_STATE"),
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("ID_DOCUMENT"),
                resultSet.getString("BODY"),
                resultSet.getDate("PURCHASE_DATE"),
                resultSet.getLong("TOTALPRICE")
        );
    }
}
