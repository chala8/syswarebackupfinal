package com.name.business.mappers;


import com.name.business.entities.TablaPedidos;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TablaPedidosMapper implements ResultSetMapper<TablaPedidos> {

    @Override
    public TablaPedidos map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new TablaPedidos(
                resultSet.getLong("ID_STORE_CLIENT"),
                resultSet.getLong("ID_STORE_PROVIDER"),
                resultSet.getLong("ID_THIRD_CLIENT"),
                resultSet.getLong("ID_THIRD_EMPLOYEE"),
                resultSet.getLong("ID_THIRD_DESTINITY"),
                resultSet.getLong("ID_THIRD_EMP_PROVIDER")
        );
    }
}
