package com.name.business.entities;

import java.util.Date;

public class BillState {
    private Long id_bill_state;
    private String name_bill_state;
    private Long id_state_bill_state;
    private Integer state_bill_state;
    private Date creation_bill_state;
    private Date update_bill_state;


    public BillState(Long id_bill_state, String name_bill_state, CommonState state) {
        this.id_bill_state = id_bill_state;
        this.name_bill_state = name_bill_state;
        this.id_state_bill_state = state.getId_common_state();
        this.state_bill_state=state.getState();
        this.creation_bill_state = state.getCreation_date();
        this.update_bill_state = state.getUpdate_date();
    }


    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_bill_state(),
                this.getState_bill_state(),
                this.getCreation_bill_state(),
                this.getUpdate_bill_state()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_bill_state =state.getId_common_state();
        this.state_bill_state  =state.getState();
        this.creation_bill_state =state.getCreation_date();
        this.update_bill_state =state.getUpdate_date();
    }



    public Long getId_bill_state() {
        return id_bill_state;
    }

    public void setId_bill_state(Long id_bill_state) {
        this.id_bill_state = id_bill_state;
    }

    public String getName_bill_state() {
        return name_bill_state;
    }

    public void setName_bill_state(String name_bill_state) {
        this.name_bill_state = name_bill_state;
    }

    public Long getId_state_bill_state() {
        return id_state_bill_state;
    }

    public void setId_state_bill_state(Long id_state_bill_state) {
        this.id_state_bill_state = id_state_bill_state;
    }

    public Integer getState_bill_state() {
        return state_bill_state;
    }

    public void setState_bill_state(Integer state_bill_state) {
        this.state_bill_state = state_bill_state;
    }

    public Date getCreation_bill_state() {
        return creation_bill_state;
    }

    public void setCreation_bill_state(Date creation_bill_state) {
        this.creation_bill_state = creation_bill_state;
    }

    public Date getUpdate_bill_state() {
        return update_bill_state;
    }

    public void setUpdate_bill_state(Date update_bill_state) {
        this.update_bill_state = update_bill_state;
    }

}
