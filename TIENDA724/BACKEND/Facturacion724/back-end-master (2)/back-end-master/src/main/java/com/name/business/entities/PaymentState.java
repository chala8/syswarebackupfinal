package com.name.business.entities;

import java.util.Date;

public class PaymentState {

    private Long id_payment_state;
    private String name_payment_state;
    private Long id_state_payment_state;
    private Integer state_payment_state;
    private Date creation_payment_state;
    private Date update_payment_state;

    public PaymentState(Long id_payment_state, String name_payment_state, CommonState state) {
        this.id_payment_state = id_payment_state;
        this.name_payment_state = name_payment_state;

        this.id_state_payment_state = state.getId_common_state();
        this.state_payment_state=state.getState();
        this.creation_payment_state = state.getCreation_date();
        this.update_payment_state = state.getUpdate_date();
    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_payment_state(),
                this.getState_payment_state(),
                this.getCreation_payment_state(),
                this.getUpdate_payment_state()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_payment_state =state.getId_common_state();
        this.state_payment_state  =state.getState();
        this.creation_payment_state =state.getCreation_date();
        this.update_payment_state =state.getUpdate_date();
    }


    public Integer getState_payment_state() {
        return state_payment_state;
    }

    public void setState_payment_state(Integer state_payment_state) {
        this.state_payment_state = state_payment_state;
    }

    public Long getId_payment_state() {
        return id_payment_state;
    }

    public void setId_payment_state(Long id_payment_state) {
        this.id_payment_state = id_payment_state;
    }

    public String getName_payment_state() {
        return name_payment_state;
    }

    public void setName_payment_state(String name_payment_state) {
        this.name_payment_state = name_payment_state;
    }

    public Long getId_state_payment_state() {
        return id_state_payment_state;
    }

    public void setId_state_payment_state(Long id_state_payment_state) {
        this.id_state_payment_state = id_state_payment_state;
    }

    public Date getCreation_payment_state() {
        return creation_payment_state;
    }

    public void setCreation_payment_state(Date creation_payment_state) {
        this.creation_payment_state = creation_payment_state;
    }

    public Date getUpdate_payment_state() {
        return update_payment_state;
    }

    public void setUpdate_payment_state(Date update_payment_state) {
        this.update_payment_state = update_payment_state;
    }
}
