package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.BillBusiness;
import com.name.business.entities.*;
import com.name.business.representations.BillDTO;
import com.name.business.representations.BillDetailIDDTO;
import com.name.business.representations.BillPdfDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Path("/billing")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BillResource {
    private BillBusiness billBusiness;

    public BillResource(BillBusiness billBusiness) {
        this.billBusiness = billBusiness;
    }


    @GET
    @Timed
    public Response getBillResource(@QueryParam("id_bill") Long id_bill,
                    @QueryParam("id_bill_father") Long id_bill_father,
                    @QueryParam("id_third_employee") Long id_third_employee,
                    @QueryParam("id_third") Long id_third,
                    @QueryParam("id_payment_state") Long id_payment_state,
                    @QueryParam("id_bill_state") Long id_bill_state,
                    @QueryParam("id_bill_type") Long id_bill_type,
                    @QueryParam("consecutive") String consecutive,
                    @QueryParam("purchase_date") Date purchase_date,
                    @QueryParam("subtotal") Double subtotal,
                    @QueryParam("tax") Double tax,
                    @QueryParam("discount") Double discount,
                    @QueryParam("totalprice") Double totalprice,

                    @QueryParam("id_state_bill") Long id_state_bill,
                    @QueryParam("state_bill") Integer state_bill,
                    @QueryParam("creation_bill") Date creation_bill,
                    @QueryParam("update_bill") Date update_bill
    ){

        Response response;

        Either<IException, List<Bill>> getResponseGeneric = billBusiness.getBill(
                new Bill(id_bill,  id_bill_father, id_third_employee, id_third,consecutive,
                        purchase_date,subtotal,totalprice,tax,discount,id_payment_state,id_bill_state,id_bill_type,
                        new CommonState(id_state_bill, state_bill,
                                creation_bill, update_bill)
                )
        );

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




    @GET
    @Path("/details")
    @Timed
    public Response getBillDetailsResource(@QueryParam("id_bill") Long id_bill,
                                            @QueryParam("id_bill_father") Long id_bill_father,
                                            @QueryParam("id_third_employee") Long id_third_employee,
                                            @QueryParam("id_third") Long id_third,
                                            @QueryParam("id_payment_state") Long id_payment_state,
                                            @QueryParam("id_bill_state") Long id_bill_state,
                                            @QueryParam("id_bill_type") Long id_bill_type,
                                            @QueryParam("consecutive") String consecutive,
                                            @QueryParam("purchase_date") Date purchase_date,
                                            @QueryParam("subtotal") Double subtotal,
                                            @QueryParam("tax") Double tax,
                                            @QueryParam("totalprice") Double totalprice,
                                           @QueryParam("discount") Double discount,

                                            @QueryParam("id_state_bill") Long id_state_bill,
                                            @QueryParam("state_bill") Integer state_bill,
                                            @QueryParam("creation_bill") Date creation_bill,
                                            @QueryParam("update_bill") Date update_bill,


                                           @QueryParam("id_detail_bill") Long id_detail_bill,
                                           @QueryParam("id_product_third") Long id_product_third,
                                           @QueryParam("quantity") Integer quantity,
                                           @QueryParam("price") Double price,
                                           @QueryParam("tax_product") Double tax_product,


                                           @QueryParam("id_state_detail_bill") Long id_state_detail_bill,
                                           @QueryParam("state_detail_bill") Integer state_detail_bill,
                                           @QueryParam("creation_detail_bill") Date creation_detail_bill,
                                           @QueryParam("update_detail_bill") Date update_detail_bill,


                                           @QueryParam("id_detail_payment_bill") Long id_detail_payment_bill,
                                           @QueryParam("id_way_to_pay") Long id_way_to_pay,
                                           @QueryParam("id_payment_method") Long id_payment_method,

                                           @QueryParam("payment_value") Double payment_value,
                                           @QueryParam("approbation_code") String approbation_code,



                                           @QueryParam("id_state_detail_pay") Long id_state_det_pay,
                                           @QueryParam("state_detail_pay") Integer state_det_pay,
                                           @QueryParam("creation_detail_pay") Date creation_det_pay,
                                           @QueryParam("update_detail_pay") Date update_det_pay
    ){

        Response response;

        Either<IException, List<HashMap<String,Object>>> getResponseGeneric = billBusiness.getBillDetail(
                new Bill(id_bill,  id_bill_father, id_third_employee, id_third,consecutive,
                        purchase_date,subtotal,totalprice,tax,discount,id_payment_state,id_bill_state,id_bill_type,
                        new CommonState(id_state_bill, state_bill,
                                creation_bill, update_bill)
                ),
                new DetailBill(id_detail_bill,id_bill,id_product_third,quantity,price,tax_product,tax,
                        new CommonState(id_state_detail_bill, state_detail_bill,
                                creation_detail_bill, update_detail_bill)
                ),
                new DetailPaymentBill(id_detail_payment_bill,id_bill,id_way_to_pay,id_payment_method,payment_value,approbation_code,
                        new CommonState(id_state_det_pay, state_det_pay,
                                creation_det_pay, update_det_pay)
                )
        );

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/completes")
    @Timed
    public Response getBillCompleteResource(@QueryParam("id_bill") Long id_bill,
                                            @QueryParam("id_bill_father") Long id_bill_father,
                                            @QueryParam("id_third_employee") Long id_third_employee,
                                            @QueryParam("id_third") Long id_third,
                                            @QueryParam("consecutive") String consecutive,
                                            @QueryParam("purchase_date") Date purchase_date,
                                            @QueryParam("subtotal") Double subtotal,
                                            @QueryParam("tax") Double tax,
                                            @QueryParam("totalprice") Double totalprice,
                                            @QueryParam("discount") Double discount,


                                            @QueryParam("id_payment_state") Long id_payment_state,
                                            @QueryParam("name_payment_state") String name_payment_state,
                                            @QueryParam("id_state_payment_state") Long id_state_payment_state,
                                            @QueryParam("state_payment_state") Integer state_product,
                                            @QueryParam("creation_payment_state") Date creation_payment_state,
                                            @QueryParam("update_payment_state") Date update_payment_state,

                                            @QueryParam("id_bill_state") Long id_bill_state,
                                            @QueryParam("name_bill_state") String name_bill_state,

                                            @QueryParam("id_state_bill_state") Long id_state_bill_state,
                                            @QueryParam("state_bill_state") Integer state_bill_state,
                                            @QueryParam("creation_bill_state") Date creation_bill_state,
                                            @QueryParam("update_bill_state") Date update_bill_state,

                                            @QueryParam("id_bill_type") Long id_bill_type,
                                            @QueryParam("name_bill_type") String name_bill_type,

                                            @QueryParam("id_state_bill_type") Long id_state_bill_type,
                                            @QueryParam("state_bill_type") Integer state_bill_type,
                                            @QueryParam("creation_bill_type") Date creation_bill_type,
                                            @QueryParam("update_bill_type") Date update_bill_type,


                                            @QueryParam("id_state_bill") Long id_state_bill,
                                            @QueryParam("state_bill") Integer state_bill,
                                            @QueryParam("creation_bill") Date creation_bill,
                                            @QueryParam("creation_bill") Date update_bill){

        Response response;

        Either<IException, List<BillComplete>> getResponseGeneric = billBusiness.getBillComplete(
                new BillComplete(
                        id_bill,id_bill_father,id_third_employee,id_third,consecutive,purchase_date,subtotal,totalprice,tax,discount,
                        new PaymentState(id_payment_state, name_payment_state,
                                new CommonState(id_state_payment_state, state_product,
                                        creation_payment_state, update_payment_state)
                        ),
                        new BillState(id_bill_state, name_bill_state,
                                new CommonState(id_state_bill_state, state_bill_state,
                                        creation_bill_state, update_bill_state)
                        ),
                        new BillType(id_bill_type, name_bill_type,
                                new CommonState(id_state_bill_type, state_bill_type,
                                        creation_bill_type, update_bill_type)
                        ),
                        new CommonState(id_state_bill, state_bill,
                                creation_bill, update_bill)
                        )

                );

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/completes/details")
    @Timed
    public Response getBillCompleteDeatilsResource(@QueryParam("id_bill") Long id_bill,
                                            @QueryParam("id_bill_father") Long id_bill_father,
                                            @QueryParam("id_third_employee") Long id_third_employee,
                                            @QueryParam("id_third") Long id_third,
                                            @QueryParam("consecutive") String consecutive,
                                            @QueryParam("purchase_date") Date purchase_date,
                                            @QueryParam("subtotal") Double subtotal,
                                            @QueryParam("tax") Double tax,
                                            @QueryParam("totalprice") Double totalprice,
                                            @QueryParam("discount") Double discount,


                                            @QueryParam("id_payment_state") Long id_payment_state,
                                            @QueryParam("name_payment_state") String name_payment_state,
                                            @QueryParam("id_state_payment_state") Long id_state_payment_state,
                                            @QueryParam("state_payment_state") Integer state_product,
                                            @QueryParam("creation_payment_state") Date creation_payment_state,
                                            @QueryParam("update_payment_state") Date update_payment_state,

                                            @QueryParam("id_bill_state") Long id_bill_state,
                                            @QueryParam("name_bill_state") String name_bill_state,

                                            @QueryParam("id_state_bill_state") Long id_state_bill_state,
                                            @QueryParam("state_bill_state") Integer state_bill_state,
                                            @QueryParam("creation_bill_state") Date creation_bill_state,
                                            @QueryParam("update_bill_state") Date update_bill_state,

                                            @QueryParam("id_bill_type") Long id_bill_type,
                                            @QueryParam("name_bill_type") String name_bill_type,

                                            @QueryParam("id_state_bill_type") Long id_state_bill_type,
                                            @QueryParam("state_bill_type") Integer state_bill_type,
                                            @QueryParam("creation_bill_type") Date creation_bill_type,
                                            @QueryParam("update_bill_type") Date update_bill_type,


                                            @QueryParam("id_state_bill") Long id_state_bill,
                                            @QueryParam("state_bill") Integer state_bill,
                                            @QueryParam("creation_bill") Date creation_bill,
                                            @QueryParam("creation_bill") Date update_bill,





                                           @QueryParam("id_detail_bill") Long id_detail_bill,
                                           @QueryParam("id_product_third") Long id_product_third,
                                           @QueryParam("quantity") Integer quantity,
                                           @QueryParam("price") Double price,
                                           @QueryParam("tax_product") Double tax_product,


                                           @QueryParam("id_state_detail_bill") Long id_state_detail_bill,
                                           @QueryParam("state_detail_bill") Integer state_detail_bill,
                                           @QueryParam("creation_detail_bill") Date creation_detail_bill,
                                           @QueryParam("update_detail_bill") Date update_detail_bill,



                                           @QueryParam("id_detail_payment_bill") Long id_detail_payment_bill,
                                           @QueryParam("payment_value") Double payment_value,
                                           @QueryParam("aprobation_code") String aprobation_code,

                                           @QueryParam("id_payment_method") Long id_payment_method,
                                           @QueryParam("name_payment_method") String name_payment_method,
                                           @QueryParam("id_state_payment_method") Long id_state_payment_method,
                                           @QueryParam("state_payment_method") Integer state_payment_method,
                                           @QueryParam("creation_payment_method") Date creation_payment_method,
                                           @QueryParam("update_payment_method") Date update_payment_method,


                                           @QueryParam("id_way_pay") Long id_way_pay,
                                           @QueryParam("name_way_pay") String name_way_pay,

                                           @QueryParam("id_state_way_pay") Long id_state_way_pay,
                                           @QueryParam("state_way_pay") Integer state_way_pay,
                                           @QueryParam("creation_way_pay") Date creation_way_pay,
                                           @QueryParam("update_way_pay") Date update_way_pay,



                                           @QueryParam("id_state_det_pay") Long id_state_det_pay,
                                           @QueryParam("state_det_pay") Integer state_det_pay,
                                           @QueryParam("creation_det_pay") Date creation_det_pay,
                                           @QueryParam("update_det_pay") Date update_det_pay


                                                   ){

        Response response;

        Either<IException, List<HashMap<String,Object>>> getResponseGeneric = billBusiness.getBillCompleteDetail(
                new BillComplete(
                        id_bill,id_bill_father,id_third_employee,id_third,consecutive,purchase_date,subtotal,totalprice,tax,discount,
                        new PaymentState(id_payment_state, name_payment_state,
                                new CommonState(id_state_payment_state, state_product,
                                        creation_payment_state, update_payment_state)
                        ),
                        new BillState(id_bill_state, name_bill_state,
                                new CommonState(id_state_bill_state, state_bill_state,
                                        creation_bill_state, update_bill_state)
                        ),
                        new BillType(id_bill_type, name_bill_type,
                                new CommonState(id_state_bill_type, state_bill_type,
                                        creation_bill_type, update_bill_type)
                        ),
                        new CommonState(id_state_bill, state_bill,
                                creation_bill, update_bill)
                ),

                new DetailBill(id_detail_bill,id_bill,id_product_third,quantity,price,tax_product,tax,
                        new CommonState(id_state_detail_bill, state_detail_bill,
                                creation_detail_bill, update_detail_bill)
                ),
                new DetailPaymentBillComplete(id_detail_payment_bill,payment_value,id_bill,aprobation_code,
                        new PaymentMethod(id_payment_method, name_payment_method,
                                new CommonState(id_state_payment_method, state_payment_method,
                                        creation_payment_method, update_payment_method)
                        ),
                        new WayToPay(id_way_pay, name_way_pay,
                                new CommonState(id_state_way_pay, state_way_pay,
                                        creation_way_pay, update_way_pay)
                        ),
                        new CommonState(id_state_det_pay, state_det_pay,
                                creation_det_pay, update_det_pay)
                )
        );

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @POST
    @Timed
    public Response postBillResource(BillDTO billDTO, @QueryParam("id_caja") Long id_caja){
        Response response;
        System.out.println("console testing: "+billDTO.getid_third_destiny());
        Either<IException, Long> getResponseGeneric = billBusiness.creatBill(billDTO, id_caja);


        if (getResponseGeneric.isRight()){

            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @POST
    @Path("/procedureup2")
    @Timed
    public Response procedureup2(@QueryParam("idbill") Long idbill){
        Response response;

        response=Response.status(Response.Status.OK).entity(billBusiness.procedure2(idbill)).build();

        return response;

    }




    @Path("/{id}")
    @PUT
    @Timed
    public Response updateBillResource(@PathParam("id") Long id_bill, BillDetailIDDTO billDetailIDDTO){
        Response response;
        Either<IException, Long> getResponseGeneric = billBusiness.updateBill(id_bill, billDetailIDDTO);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteBillResource(@PathParam("id") Long id_bill){
        Response response;
        Either<IException, Long> getResponseGeneric = billBusiness.deleteBill(id_bill);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




    @POST
    @Path("/pdf")
    @Timed
    public Response printPDF(BillPdfDTO billpdfDTO) {
        Response response;
        Either<IException, String> mailEither =Either.right("NOPDF!!!");
        if(billpdfDTO.getpdfSize()==1){
            mailEither = billBusiness.printPDF(billpdfDTO);}
        if(billpdfDTO.getpdfSize()==2){
            mailEither = billBusiness.printPDF2(billpdfDTO);}



        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @POST
    @Path("/pdfs")
    @Timed
    public Response printingPDFs(@QueryParam("id_bill") Long id_bill,
                                 @QueryParam("id_store") Long id_store,
                                 @QueryParam("id_third") Long id_third,
                                 @QueryParam("datos") String datos,
                                 @QueryParam("cajero") String cajero,
                                 @QueryParam("cambio") Long cambio,
                                 @QueryParam("doc_cliente") String doc_cliente,
                                 @QueryParam("nombre_cliente") String nombre_cliente,
                                 @QueryParam("direccion_cliente") String direccion_cliente,
                                 @QueryParam("telefono") String telefono
                                 ) {
        Response response;

        System.out.println("DIRECCION CLIENTE: "+direccion_cliente);
        System.out.println("TELEFONO CLIENTE: "+telefono);
        Either<IException, String> mailEither =Either.right("NOPDF!!!");

        Either<IException, BillMaster> master = billBusiness.getMaestroFactura(id_bill,id_store);
        BillMaster billmaster = master.right().value();

        Either<IException, List<LegalData>> legaldata = billBusiness.getLegalData(id_third);
        List<LegalData> billlegaldata = legaldata.right().value();

        Either<IException, List<List<String>>> details = billBusiness.getDetalleFactura(id_bill);
        List<List<String>> billdetails = details.right().value();


        String autorizacion = "";

        System.out.println(id_store);
        System.out.println(id_store==143);

        if(id_store==141){
            autorizacion = "18763000168739 Autorización: Desde CD1 a CD99999";
        }else{
            if(id_store==142){
                autorizacion = "18763000168739 Autorización: Desde C11 a C199999";
            }else{
                if(id_store==143){
                    autorizacion = "18763000168739 Autorización: Desde VL1 a VL99999";
                }else{
                    if(id_store==221){
                        autorizacion = "Autorización: Desde SJ2 a SJ99999";
                    }else{
                        autorizacion = billlegaldata.get(0).getRESOLUCION_DIAN();
                    }
                }
            }
        }

        BillPdfDTO billpdfdto = new BillPdfDTO(
                billmaster.getFULLNAME(),
                billmaster.getSTORE_NAME(),
                billmaster.getPURCHASE_DATE(),
                billmaster.getCAJA(),
                billmaster.getNUM_DOCUMENTO(),
                billdetails,
                billmaster.getSUBTOTAL(),
                billmaster.getTAX(),
                billmaster.getTOTALPRICE(),
                billmaster.getNAME()+" "+billmaster.getDOCUMENT_NUMBER(),
                datos,
                "Régimen Simplificado",
                cajero,
                cambio,
                billlegaldata.get(0).getCITY_NAME()+", "+billlegaldata.get(0).getADDRESS(),
                billlegaldata.get(0).getPHONE1(),
                doc_cliente,
                nombre_cliente,
                direccion_cliente,
                telefono,
                "",
                new Long(2),
                autorizacion,
                billlegaldata.get(0).getREGIMEN_TRIBUTARIO(),
                billlegaldata.get(0).getPREFIX_BILL(),
                billlegaldata.get(0).getINITIAL_RANGE(),
                billlegaldata.get(0).getFINAL_RANGE()
        );

        System.out.println("CLIENT PHONE: "+billpdfdto.gettelefonoC());
        System.out.println("CLIENT ADDRESS: "+billpdfdto.getdireccionC());

        if(billpdfdto.getpdfSize()==1){
            mailEither = billBusiness.printPDF(billpdfdto);}
        if(billpdfdto.getpdfSize()==2){
            mailEither = billBusiness.printPDF2(billpdfdto);}

        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }


    @POST
    @Path("/pdf2")
    @Timed
    public Response printPDF2(BillPdfDTO billpdfDTO) {
        Response response;
        Either<IException, String> mailEither =Either.right("NOPDF!!!");
        if(billpdfDTO.getpdfSize()==1){
            mailEither = billBusiness.printPDF3(billpdfDTO);}
        if(billpdfDTO.getpdfSize()==2){
            mailEither = billBusiness.printPDF4(billpdfDTO);}

        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }






    @GET
    @Path("/master")
    @Timed
    public Response getMaestroFactura(@QueryParam("id_bill") Long id_bill,
                                    @QueryParam("id_store") Long id_store){

        Response response;

        Either<IException, BillMaster> getResponseGeneric = billBusiness.getMaestroFactura(id_bill,id_store);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/master2")
    @Timed
    public Response getMaestroFactura2(@QueryParam("id_bill") Long id_bill,
                                      @QueryParam("id_store") Long id_store){

        Response response;

        Either<IException, BillMaster2> getResponseGeneric = billBusiness.getMaestroFactura2(id_bill,id_store);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




    @GET
    @Path("/detail")
    @Timed
    public Response getDetalleFactura(@QueryParam("id_bill") Long id_bill){

        Response response;

        Either<IException, List<List<String>>> getResponseGeneric = billBusiness.getDetalleFactura(id_bill);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @GET
    @Path("/venta")
    @Timed
    public Response getSalesCaja(@QueryParam("id_cierre_caja") Long id_cierre_caja){

        Response response;

        Either<IException, Double> getResponseGeneric = billBusiness.getSalesCaja(id_cierre_caja);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @PUT
    @Path("/billCompra")
    @Timed
    public Response putCompraBill(@QueryParam("id_bill") Long id_bill,
                                  @QueryParam("dato") String dato,
                                  @QueryParam("fecha") String fecha){

        Response response;

        Either<IException, String> getResponseGeneric = billBusiness.putCompraBill(id_bill,dato,new Date(fecha));

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @GET
    @Path("/legaldata")
    @Timed
    public Response getLegalData(@QueryParam("id_third") Long id_third){

        Response response;

        Either<IException, List<LegalData>> getResponseGeneric = billBusiness.getLegalData(id_third);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @GET
    @Path("/top20")
    @Timed
    public Response getTopProducts(@QueryParam("idstore") Long idstore){

        Response response;

        Either<IException, List<TopProds>> getResponseGeneric = billBusiness.getTopProducts(idstore);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


}
