package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.BillCompleteMapper;
import com.name.business.mappers.PricePerProdMapper;
import com.name.business.mappers.BillDataMapper;
import com.name.business.representations.BillDTO;
import com.name.business.representations.BillDetailIDDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(BillDataMapper.class)
public interface BillDataDAO {

    @SqlQuery(" SELECT ID_BILL_STATE,ID_BILL,B.ID_DOCUMENT,D.BODY,B.PURCHASE_DATE,B.TOTALPRICE\n" +
            " FROM BILL B, DOCUMENT D\n" +
            " WHERE B.ID_DOCUMENT=D.ID_DOCUMENT\n" +
            " AND consecutive= :consecutive\n"+
            " AND b.ID_STORE = :id_store " +
            " AND b.ID_BILL_TYPE = 1")
    List <BillData> getBillData(@Bind("consecutive") String consecutive,
                                @Bind("id_store") Long id_store);

    int delete();

    int deletePermanent();

}
