package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.DetailBillBusiness;
import com.name.business.entities.CommonState;
import com.name.business.entities.DetailBill;
import com.name.business.representations.DetailBillDTO;
import com.name.business.representations.DetailBillIdDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/billing-details")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DetailBillResource {
  private DetailBillBusiness detailBillBusiness;

    public DetailBillResource(DetailBillBusiness detailBillBusiness) {
        this.detailBillBusiness = detailBillBusiness;
    }

    @GET
    @Timed
    public Response getDetailBillResource(@QueryParam("id_detail_bill") Long id_detail_bill,
                                          @QueryParam("id_bill") Long id_bill,
                                          @QueryParam("id_product_third") Long id_product_third,
                                          @QueryParam("quantity") Integer quantity,
                                          @QueryParam("price") Double price,
                                          @QueryParam("tax_product") Double tax_product,
                                          @QueryParam("tax") Double tax,

                                          @QueryParam("id_state_detail_bill") Long id_state_detail_bill,
                                          @QueryParam("state_detail_bill") Integer state_detail_bill,
                                          @QueryParam("creation_detail_bill") Date creation_detail_bill,
                                          @QueryParam("update_detail_bill") Date update_detail_bill){

        Response response;

        Either<IException, List<DetailBill>> getResponseGeneric = detailBillBusiness.getDetailBill(
                new DetailBill(id_detail_bill,id_bill,id_product_third,quantity,price,tax_product,tax,
                        new CommonState(id_state_detail_bill, state_detail_bill,
                                creation_detail_bill, update_detail_bill)
                )
        );

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @POST
    @Timed
    public Response postDetailBillResource(@QueryParam("id_bill") Long id_bill, List<DetailBillDTO> detailBillDTOList){
        Response response;
        Long burnt_test = new Long(1);

        Either<IException, Long> getResponseGeneric = detailBillBusiness.creatDetailBill(burnt_test,id_bill,detailBillDTOList);


        if (getResponseGeneric.isRight()){

            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/{id_bill}")
    @PUT
    @Timed
    public Response updateDetailBillResource(@PathParam("id_bill") Long id_bill, List<DetailBillIdDTO> detailBillIdDTOList){
        Response response;
        Either<IException, Long> getResponseGeneric = detailBillBusiness.updateDetailBill(id_bill, detailBillIdDTOList);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteDetailBillResource(@PathParam("id") Long id_detail_bill, @QueryParam("id_bill") Long id_bill){
        Response response;
        Either<IException, Long> getResponseGeneric = detailBillBusiness.deleteDetailBill(id_detail_bill,id_bill);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }
}
