package com.name.business.entities;

import java.util.Date;

public class DetailPaymentBill {

    private Long id_detail_payment_bill;
    private Long id_bill; /// It can be null, if The Bill does not exist, or It can given the id Biling exist
    private Long id_way_to_pay;
    private Long id_payment_method;
    private Double payment_value;
    private String aprobation_code;



    private Long id_state_det_pay;
    private Integer state_det_pay;
    private Date creation_det_pay;
    private Date update_det_pay;

    public DetailPaymentBill(Long id_detail_payment_bill, Long id_bill, Long id_way_to_pay, Long id_payment_method, Double payment_value, String aprobation_code, CommonState state) {
        this.id_detail_payment_bill = id_detail_payment_bill;
        this.id_bill = id_bill;
        this.id_way_to_pay = id_way_to_pay;
        this.id_payment_method = id_payment_method;
        this.payment_value = payment_value;
        this.aprobation_code = aprobation_code;

        this.id_state_det_pay = state.getId_common_state();
        this.state_det_pay=state.getState();
        this.creation_det_pay = state.getCreation_date();
        this.update_det_pay = state.getUpdate_date();
    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_det_pay(),
                this.getState_det_pay(),
                this.getCreation_det_pay(),
                this.getUpdate_det_pay()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_det_pay =state.getId_common_state();
        this.state_det_pay  =state.getState();
        this.creation_det_pay =state.getCreation_date();
        this.update_det_pay =state.getUpdate_date();
    }

    public Integer getState_det_pay() {
        return state_det_pay;
    }

    public void setState_det_pay(Integer state_det_pay) {
        this.state_det_pay = state_det_pay;
    }

    public Long getId_detail_payment_bill() {
        return id_detail_payment_bill;
    }

    public void setId_detail_payment_bill(Long id_detail_payment_bill) {
        this.id_detail_payment_bill = id_detail_payment_bill;
    }

    public Long getId_state_det_pay() {
        return id_state_det_pay;
    }

    public void setId_state_det_pay(Long id_state_det_pay) {
        this.id_state_det_pay = id_state_det_pay;
    }

    public Date getCreation_det_pay() {
        return creation_det_pay;
    }

    public void setCreation_det_pay(Date creation_det_pay) {
        this.creation_det_pay = creation_det_pay;
    }

    public Date getUpdate_det_pay() {
        return update_det_pay;
    }

    public void setUpdate_det_pay(Date update_det_pay) {
        this.update_det_pay = update_det_pay;
    }

    public Long getId_bill() {
        return id_bill;
    }

    public void setId_bill(Long id_bill) {
        this.id_bill = id_bill;
    }

    public Long getId_way_to_pay() {
        return id_way_to_pay;
    }

    public void setId_way_to_pay(Long id_way_to_pay) {
        this.id_way_to_pay = id_way_to_pay;
    }

    public Long getId_payment_method() {
        return id_payment_method;
    }

    public void setId_payment_method(Long id_payment_method) {
        this.id_payment_method = id_payment_method;
    }

    public Double getPayment_value() {
        return payment_value;
    }

    public void setPayment_value(Double payment_value) {
        this.payment_value = payment_value;
    }

    public String getAprobation_code() {
        return aprobation_code;
    }

    public void setAprobation_code(String aprobation_code) {
        this.aprobation_code = aprobation_code;
    }


}
