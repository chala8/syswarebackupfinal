package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.DetailPaymentBillCompleteMapper;
import com.name.business.mappers.DetailPaymentBillMapper;
import com.name.business.representations.DetailPaymentBillDTO;
import com.name.business.representations.DetailPaymentBillIdDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(DetailPaymentBillMapper.class)
public interface DetailPaymentBillDAO {

    @SqlQuery("SELECT ID_DETAIL_PAYMENT_BILL FROM DETAIL_PAYMENT_BILL WHERE ID_PAYMENT_METHOD IN (SELECT MAX( ID_DETAIL_PAYMENT_BILL ) FROM DETAIL_PAYMENT_BILL )\n")
    Long getPkLast();

    @SqlQuery("SELECT COUNT(ID_DETAIL_PAYMENT_BILL) FROM DETAIL_PAYMENT_BILL WHERE ID_PAYMENT_METHOD = :id\n")
    Integer getValidatorID(@Bind("id") Long id);


    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery(" SELECT ID_DETAIL_PAYMENT_BILL ID, ID_COMMON_STATE  FROM DETAIL_PAYMENT_BILL WHERE( ID_DETAIL_PAYMENT_BILL = :id OR :id IS NULL) AND ( ID_BILL = :id_bil OR :id_bil IS NULL) \n")
    List<CommonSimple> readCommons(@Bind("id") Long id,@Bind("id_bil") Long id_bil);


    @SqlQuery("SELECT * FROM V_DETAILPAYMENTBILL V_DET_PAY \n " +
            "WHERE (V_DET_PAY.ID_DETAIL_PAYMENT_BILL=:det_pay.id_detail_payment_bill OR :det_pay.id_detail_payment_bill IS NULL ) AND\n" +
            "      (V_DET_PAY.PAYMENT_VALUE=:det_pay.payment_value OR :det_pay.payment_value IS NULL ) AND\n" +
            "      (V_DET_PAY.ID_BILL=:det_pay.id_bill OR :det_pay.id_bill IS NULL ) AND\n" +
            "      (V_DET_PAY.ID_PAYMENT_METHOD=:det_pay.id_payment_method OR :det_pay.id_payment_method IS NULL ) AND\n" +
            "      (V_DET_PAY.APPROBATION_CODE=:det_pay.aprobation_code OR :det_pay.aprobation_code IS NULL ) AND\n" +
            "      (V_DET_PAY.ID_WAY_TO_PAY=:det_pay.id_way_to_pay OR :det_pay.id_way_to_pay IS NULL ) AND\n" +
            "      (V_DET_PAY.ID_CM_DET_PAY_BIL=:det_pay.id_state_det_pay OR :det_pay.id_state_det_pay IS NULL ) AND\n" +
            "      (V_DET_PAY.STATE_DET_PAY_BIL=:det_pay.state_det_pay OR :det_pay.state_det_pay IS NULL ) AND\n" +
            "      (V_DET_PAY.CREATION_DET_PAY_BIL=:det_pay.creation_det_pay OR :det_pay.creation_det_pay IS NULL ) AND\n" +
            "      (V_DET_PAY.UPDATE_DET_PAY_BIL=:det_pay.update_det_pay OR :det_pay.update_det_pay IS NULL )")
    List<DetailPaymentBill> read(@BindBean("det_pay") DetailPaymentBill detailPaymentBill);

    @RegisterMapper(DetailPaymentBillCompleteMapper.class)
    @SqlQuery("SELECT * FROM V_DETAILPAYMETBILLCOMPLETE V_DET_PAY_MET_BIL \n" +
            " WHERE (V_DET_PAY_MET_BIL.ID_DETAIL_PAYMENT_BILL =:det_pay_met_bil.id_detail_payment_bill OR :det_pay_met_bil.id_detail_payment_bill IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.PAYMENT_VALUE =:det_pay_met_bil.payment_value OR :det_pay_met_bil.payment_value IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.ID_BILL =:det_pay_met_bil.id_bill OR :det_pay_met_bil.id_bill IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.APPROBATION_CODE =:det_pay_met_bil.aprobation_code OR :det_pay_met_bil.aprobation_code IS NULL) AND\n" +
            "\n" +
            "       (V_DET_PAY_MET_BIL.ID_PAYMENT_METHOD =:pay_met.id_payment_method OR :pay_met.id_payment_method IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.NAME_PAY_ME LIKE :pay_met.name_payment_method OR :pay_met.name_payment_method IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.ID_CM_PAY_MET =:pay_met.id_state_payment_method OR :pay_met.id_state_payment_method IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.STATE_PAY_ME =:pay_met.state_payment_method OR :pay_met.state_payment_method IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.CREATION_PAY_ME =:pay_met.creation_payment_method OR :pay_met.creation_payment_method IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.UPDATE_PAY_ME =:pay_met.update_payment_method OR :pay_met.update_payment_method IS NULL) AND\n" +
            "\n" +
            "       (V_DET_PAY_MET_BIL.ID_WAY_TO_PAY =:way_pay.id_way_pay OR :way_pay.id_way_pay IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.NAME_WAY_PAY LIKE :way_pay.name_way_pay OR :way_pay.name_way_pay IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.ID_CM_WAY_PAY =:way_pay.id_state_way_pay OR :way_pay.id_state_way_pay IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.STATE_CM_WAY_PAY =:way_pay.state_way_pay OR :way_pay.state_way_pay IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.CREATION_WAY_PAY =:way_pay.creation_way_pay OR :way_pay.creation_way_pay IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.UPDATE_WAY_PAY =:way_pay.update_way_pay OR :way_pay.update_way_pay IS NULL) AND\n" +
            "\n" +
            "       (V_DET_PAY_MET_BIL.ID_CM_DET_PAY_BIL =:det_pay_met_bil.id_state_det_pay OR :det_pay_met_bil.id_state_det_pay IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.STATE_DET_PAY_BIL =:det_pay_met_bil.state_det_pay OR :det_pay_met_bil.state_det_pay IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.CREATION_DET_PAY_BIL =:det_pay_met_bil.creation_det_pay OR :det_pay_met_bil.creation_det_pay IS NULL) AND\n" +
            "       (V_DET_PAY_MET_BIL.UPDATE_DET_PAY_BIL =:det_pay_met_bil.update_det_pay OR :det_pay_met_bil.update_det_pay IS NULL)")
    List<DetailPaymentBillComplete> readComplete(@BindBean("det_pay_met_bil")DetailPaymentBillComplete detailPaymentBillComplete,
                                         @BindBean("pay_met")PaymentMethod paymentMethod, @BindBean("way_pay")WayToPay wayToPay);


    @SqlUpdate("INSERT INTO DETAIL_PAYMENT_BILL ( ID_BILL,ID_COMMON_STATE, ID_PAYMENT_METHOD, ID_WAY_TO_PAY, PAYMENT_VALUE, APPROBATION_CODE)\n " +
            "       VALUES (:id_bill,:id_common_state,:det_pay_bil.id_payment_method,:det_pay_bil.id_way_to_pay,\n " +
            "        :det_pay_bil.payment_value,:det_pay_bil.aprobation_code) ")
    void create(@Bind("id_common_state") Long id_common_state,@Bind("id_bill") Long id_bill,@BindBean("det_pay_bil") DetailPaymentBillDTO detailPaymentBillDTO);


    @SqlUpdate("UPDATE DETAIL_PAYMENT_BILL SET \n " +
            "    APPROBATION_CODE=:det_pay_bil.aprobation_code,\n" +
            "    PAYMENT_VALUE=:det_pay_bil.payment_value,\n " +
            "    ID_PAYMENT_METHOD=:det_pay_bil.id_payment_method,\n " +
            "    ID_WAY_TO_PAY=:det_pay_bil.id_way_to_pay\n " +
            "    WHERE (ID_DETAIL_PAYMENT_BILL =  :id_det_pay_bil OR :id_det_pay_bil IS NULL ) AND\n " +
            "          (ID_BILL =  :id_bill OR :id_bill IS NULL )")
    int update(@Bind("id_det_pay_bil")Long id_detail_payment_bill,@Bind("id_bill") Long id_bill,@BindBean("det_pay_bil")  DetailPaymentBillIdDTO detailPaymentBillIdDTO);


    int delete();


    int deletePermanent();

    @SqlQuery("SELECT COUNT(ID_BILL) FROM BILL WHERE ID_BILL = :id")
    Integer getValidatorIDBill(@Bind("id") Long id_bill);
}
