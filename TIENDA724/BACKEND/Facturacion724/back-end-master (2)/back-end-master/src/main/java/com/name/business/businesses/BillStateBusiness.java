package com.name.business.businesses;

import com.name.business.DAOs.BillDataDAO;
import com.name.business.DAOs.BillStateDAO;
import com.name.business.DAOs.RefundDAO;
import com.name.business.entities.*;
import com.name.business.representations.BillStateDTO;
import com.name.business.representations.CommonStateDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.billStateSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

public class BillStateBusiness {
    private BillStateDAO billStateDAO;
    private RefundDAO refundDAO;
    private CommonStateBusiness commonStateBusiness;
    private BillDataDAO billDataDAO;

    public BillStateBusiness(BillDataDAO billDataDAO,BillStateDAO billStateDAO, CommonStateBusiness commonStateBusiness, RefundDAO refundDAO) {
        this.billStateDAO = billStateDAO;
        this.commonStateBusiness = commonStateBusiness;
        this.refundDAO = refundDAO;
        this.billDataDAO = billDataDAO;
    }

    public Either<IException, List<BillState>> getBillState(BillState billState) {
        List<String> msn = new ArrayList<>();
        try {
            //msn.add("OK");
            System.out.println("|||||||||||| Starting consults Bill State ||||||||||||||||| ");
            return Either.right(billStateDAO.read(billStateSanitation(billState)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, List<Refund>> getPricePerProd(int id_product_third) {
        try {
            System.out.println("|||||||||||| Starting consults Bill State ||||||||||||||||| ");
            return Either.right(refundDAO.getPricePerProd(id_product_third));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, List<BillData>> getBillData(String consecutive, Long id_store) {
        try {
            System.out.println("|||||||||||| Starting consults Bill DATA ||||||||||||||||| ");
            System.out.println("--------------------------------");
            System.out.println(consecutive);
            System.out.println("--------------------------------");
            return Either.right(billDataDAO.getBillData(consecutive,id_store));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }




    public Either<IException, String> putBodyPerDocument(int id_document, String body) {
        try {
            refundDAO.updateDocument(id_document, body);
            return Either.right("OK!");
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, String> updateBillState(int id_bill, int id_bill_state) {
        try {
            refundDAO.updateBill(id_bill, id_bill_state);
            return Either.right("OK!");
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, Long> getProductStoreByCodeStore(Long code, Long id_store) {
        try {
            return Either.right(refundDAO.getProductStoreByCodeStore(code, id_store));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, Long> getStorageByProductStore(Long id_product_store) {
        try {
            return Either.right(refundDAO.getStorageByProductStore(id_product_store));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, Long> getQuantityByProductStorage(Long id_product_store, Long id_storage) {
        try {
            return Either.right(refundDAO.getQuantityByProductStorage(id_product_store,id_storage));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }


    public Either<IException, Long> getIDCODEBycode(String code) {
        try {
            return Either.right(refundDAO.getIDCODEBycode(code));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }

    public Either<IException, String> putInventoryBill(Long quantity, Long id_product_store, Long id_storage) {
        try {
            refundDAO.putInventoryBill(quantity, id_product_store, id_storage);
            return Either.right("OK!");
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage().isEmpty()?INTERNAL_ERROR_MESSAGE:e.getMessage())));
        }
    }



    /**
     *
     * @param billStateDTO
     * @return
     */
    public Either<IException,Long> createBillState(BillStateDTO billStateDTO){
        List<String> msn = new ArrayList<>();
        Long id_payment_state = null;
        Long id_common_state = null;

        try {

            System.out.println("|||||||||||| Starting creation Bill State  ||||||||||||||||| ");
            if (billStateDTO!=null){

                if (billStateDTO.getStateDTO() == null) {
                    billStateDTO.setStateDTO(new CommonStateDTO(1,null,null));
                }
                Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(billStateDTO.getStateDTO());
                if (commonThirdEither.isRight()) {
                    id_common_state = commonThirdEither.right().value();
                }

                billStateDAO.create(id_common_state,formatoStringSql(billStateDTO.getName()));
                id_payment_state = billStateDAO.getPkLast();

                return Either.right(id_payment_state);
            }else{
                msn.add("It does not recognize Bill State, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * @param id_bill_state
     * @param billStateDTO
     * @return
     */
    public Either<IException, Long> updateBillState(Long id_bill_state, BillStateDTO billStateDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        BillState actualRegister=null;

        try {
            System.out.println("|||||||||||| Starting update Bill State ||||||||||||||||| ");
            if ((id_bill_state != null && id_bill_state > 0)) {



                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorID(id_bill_state).equals(false)){
                    msn.add("It ID Bill State  does not exist register on  Bill State, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<BillState> read = billStateDAO.read(
                        new BillState(id_bill_state, null,
                                new CommonState(null,null,null,null))
                );

                if (read.size()<=0){
                    msn.add("It does not recognize ID Bill State , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                actualRegister=read.get(0);

                List<CommonSimple> readCommons = billStateDAO.readCommons(formatoLongSql(id_bill_state));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);

                    // Common  State business update
                    commonStateBusiness.updateCommonState(idCommons.getId_common_state(), billStateDTO.getStateDTO());

                }

                if (billStateDTO.getName() == null || billStateDTO.getName().isEmpty()) {
                    billStateDTO.setName(actualRegister.getName_bill_state());
                } else {
                    billStateDTO.setName(formatoStringSql(billStateDTO.getName()));
                }



                // Attribute update
                billStateDAO.update(id_bill_state,billStateDTO.getName());

                return Either.right(id_bill_state);

            } else {
                msn.add("It does not recognize ID Bill State, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id_bill_state
     * @return
     */
    public Either<IException, Long> deleteBillState(Long id_bill_state) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");

            System.out.println("|||||||||||| Starting delete Bill State ||||||||||||||||| ");

            if (id_bill_state != null && id_bill_state > 0) {

                List<CommonSimple> readCommons = billStateDAO.readCommons(formatoLongSql(id_bill_state));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());

                return Either.right(id_bill_state);
            } else {
                msn.add("It does not recognize ID Bill State, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = billStateDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }
}
