package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.LegalData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LegalDataMapper implements ResultSetMapper<LegalData> {
    @Override
    public LegalData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new LegalData(
                resultSet.getString("RESOLUCION_DIAN"),
                resultSet.getString("REGIMEN_TRIBUTARIO"),
                resultSet.getString("AUTORETENEDOR"),
                resultSet.getString("URL_LOGO"),
                resultSet.getString("CITY_NAME"),
                resultSet.getString("COUNTRY"),
                resultSet.getString("ADDRESS"),
                resultSet.getString("PHONE1"),
                resultSet.getString("EMAIL"),
                resultSet.getString("WEBPAGE"),
                resultSet.getString("PREFIX_BILL"),
                resultSet.getString("INITIAL_RANGE"),
                resultSet.getString("FINAL_RANGE"),
                resultSet.getString("START_CONSECUTIVE"),
                resultSet.getString("NOTES")


        );
    }
}
