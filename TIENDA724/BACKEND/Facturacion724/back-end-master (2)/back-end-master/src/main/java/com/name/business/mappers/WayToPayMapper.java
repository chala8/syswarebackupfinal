package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.WayToPay;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class WayToPayMapper implements ResultSetMapper<WayToPay> {

    @Override
    public WayToPay map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new WayToPay(
                resultSet.getLong("ID_WAY_TO_PAY"),
                resultSet.getString("NAME_WAY_PAY"),
                new CommonState(
                        resultSet.getLong("ID_CM_WAY_PAY"),
                        resultSet.getInt("STATE_CM_WAY_PAY"),
                        resultSet.getDate("CREATION_WAY_PAY"),
                        resultSet.getDate("UPDATE_WAY_PAY")
                )
        );
    }
}
