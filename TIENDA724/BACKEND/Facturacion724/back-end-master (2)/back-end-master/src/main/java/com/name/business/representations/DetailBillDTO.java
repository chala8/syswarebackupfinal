package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DetailBillDTO {

    private Integer quantity;
    private Double price;
    private Double tax;
    private Integer id_product_third;
    private Double tax_product;
    private CommonStateDTO stateDTO;
    private Long id_storage;


    @JsonCreator
    public DetailBillDTO( @JsonProperty("quantity") Integer quantity,
                          @JsonProperty("price") Double price, @JsonProperty("tax") Double tax,
                          @JsonProperty("id_product_third") Integer id_product_third,
                          @JsonProperty("tax_product") Double tax_product, @JsonProperty("state") CommonStateDTO stateDTO,
                          @JsonProperty("id_storage") Long id_storage) {

        this.quantity = quantity;
        this.price = price;
        this.tax = tax;
        this.id_product_third = id_product_third;
        this.tax_product = tax_product;
        this.stateDTO = stateDTO;
        this.id_storage = id_storage;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Integer getId_product_third() {
        return id_product_third;
    }

    public void setId_product_third(Integer id_product_third) {
        this.id_product_third = id_product_third;
    }

    public Double getTax_product() {
        return tax_product;
    }

    public void setTax_product(Double tax_product) {
        this.tax_product = tax_product;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }

    public Long getId_storage() {
        return id_storage;
    }

    public void setId_storage(Long id_storage) {
        this.id_storage = id_storage;
    }
}
