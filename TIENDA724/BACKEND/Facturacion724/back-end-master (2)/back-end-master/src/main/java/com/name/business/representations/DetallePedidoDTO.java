package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DetallePedidoDTO {


    private String FABRICANTE;
    private String MARCA;
    private String LINEA;
    private String CATEGORIA;
    private String PRESENTACION;
    private String PRODUCTO;
    private Long CANTIDAD;
    private Double COSTO;
    private Double COSTOTOTAL;
    private Long ID_PRODUCT_THIRD;


    @JsonCreator
    public DetallePedidoDTO(@JsonProperty("fabricante") String FABRICANTE,
                            @JsonProperty("marca") String MARCA,
                            @JsonProperty("linea") String LINEA,
                            @JsonProperty("categoria") String CATEGORIA,
                            @JsonProperty("presentacion") String PRESENTACION,
                            @JsonProperty("producto") String PRODUCTO,
                            @JsonProperty("cantidad") Long CANTIDAD,
                            @JsonProperty("costo") Double COSTO,
                            @JsonProperty("costototal") Double COSTOTOTAL,
                            @JsonProperty("id_PRODUCT_THIRD") Long ID_PRODUCT_THIRD) {
        this.FABRICANTE = FABRICANTE;
        this.MARCA = MARCA;
        this.LINEA =  LINEA;
        this.CATEGORIA = CATEGORIA;
        this.PRESENTACION = PRESENTACION;
        this.PRODUCTO = PRODUCTO;
        this.CANTIDAD =  CANTIDAD;
        this.COSTO = COSTO;
        this.COSTOTOTAL = COSTOTOTAL;
        this.ID_PRODUCT_THIRD = ID_PRODUCT_THIRD;
    }
    //----------------------------------------------------------------------------
    public Long getID_PRODUCT_THIRD() {
        return ID_PRODUCT_THIRD;
    }
    public void setID_PRODUCT_THIRD(Long ID_PRODUCT_THIRD) {
        this.ID_PRODUCT_THIRD = ID_PRODUCT_THIRD;
    }
    //----------------------------------------------------------------------------
    public String getFABRICANTE() {
        return FABRICANTE;
    }
    public void setFABRICANTE(String FABRICANTE) {
        this.FABRICANTE = FABRICANTE;
    }
    //----------------------------------------------------------------------------
    public String getMARCA() {
        return MARCA;
    }
    public void setMARCA(String MARCA) {
        this.MARCA = MARCA;
    }
    //----------------------------------------------------------------------------
    public String getLINEA() { return LINEA; }
    public void setLINEA(String LINEA) {
        this.LINEA = LINEA;
    }
    //----------------------------------------------------------------------------
    public String getCATEGORIA() {
        return CATEGORIA;
    }
    public void setCATEGORIA(String CATEGORIA) {
        this.CATEGORIA = CATEGORIA;
    }
    //----------------------------------------------------------------------------
    public String getPRESENTACION() {
        return PRESENTACION;
    }
    public void setPRESENTACION(String PRESENTACION) {
        this.PRESENTACION = PRESENTACION;
    }
    //----------------------------------------------------------------------------
    public String getPRODUCTO() {
        return PRODUCTO;
    }
    public void setPRODUCTO(String PRODUCTO) {
        this.PRODUCTO = PRODUCTO;
    }
    //----------------------------------------------------------------------------
    public Long getCANTIDAD() {
        return CANTIDAD;
    }
    public void setCANTIDAD(Long CANTIDAD) {
        this.CANTIDAD = CANTIDAD;
    }
    //----------------------------------------------------------------------------
    public Double getCOSTO() {
        return COSTO;
    }
    public void setCOSTO(Double COSTO) {
        this.COSTO = COSTO;
    }
    //----------------------------------------------------------------------------
    public Double getCOSTOTOTAL() {
        return COSTOTOTAL;
    }
    public void setCOSTOTOTAL(Double COSTOTOTAL) {
        this.COSTOTOTAL = COSTOTOTAL;
    }
}
