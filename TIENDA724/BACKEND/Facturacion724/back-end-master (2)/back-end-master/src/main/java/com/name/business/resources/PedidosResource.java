package com.name.business.resources;

import com.name.business.businesses.BillBusiness;
import com.name.business.entities.BillToUpdate;
import com.name.business.entities.LegalData;
import com.name.business.entities.Pedido;
import com.name.business.entities.DetallePedido;
import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.PedidosBusiness;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/pedidos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PedidosResource {
    private PedidosBusiness pedidosBusiness;
    private BillBusiness billBusiness;

    public PedidosResource(PedidosBusiness pedidosBusiness, BillBusiness billBusiness) {
        this.pedidosBusiness = pedidosBusiness;
        this.billBusiness = billBusiness;
    }



    @GET
    @Path("/ps")
    @Timed
    public Response getOwnBarCode(
            @QueryParam("id_product_store") Long id_product_store){
        Response response;

        Either<IException, String> getProducts = pedidosBusiness.getOwnBarCode(id_product_store);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/own")
    @Timed
    public Response getPsId(
            @QueryParam("code") String code,
            @QueryParam("id_store") Long id_store){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.getPsId(code,id_store);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/tax")
    @Timed
    public Response getTax(
            @QueryParam("idps") Long idps){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.getTax(idps);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/billtoupdate")
    @Timed
    public Response getBillIdByIdStore(
            @QueryParam("idstore") Long idstore,
            @QueryParam("idstoreclient") Long idstoreclient,
            @QueryParam("numpedido") String numpedido){
        Response response;

        Either<IException, BillToUpdate> getProducts = pedidosBusiness.getBillIdByIdStore(idstore,idstoreclient,numpedido);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @PUT
    @Path("/billstate")
    @Timed
    public Response updateBillState(
            @QueryParam("billstate") Long billstate,
            @QueryParam("billid") Long billid){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.updateBillState(billstate,billid);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/master")
    @Timed
    public Response getPedidos(
            @QueryParam("id_store") Long id_store,
            @QueryParam("id_bill_state") Long id_bill_state,
            @QueryParam("id_bill_type") Long id_bill_type){
        Response response;

        Either<IException, List<Pedido>> getProducts = pedidosBusiness.getPedidos(id_store,id_bill_state,id_bill_type);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/price")
    @Timed
    public Response getPrice(
            @QueryParam("idps") Long idps){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.getPrice(idps);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }





    @POST
    @Path("/detalles")
    @Timed
    public Response getDetallesPedidos(PedidoDTO priceListDTO){
        Response response;

        Either<IException, List<DetallePedido>> mailEither = pedidosBusiness.getDetallesPedidos(priceListDTO.getlistaTipos());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



    @POST
    @Path("/detalles2")
    @Timed
    public Response getDetallesPedidos2(PedidoDTO priceListDTO){
        Response response;

        Either<IException, List<DetallePedido>> mailEither = pedidosBusiness.getDetallesPedidos2(priceListDTO.getlistaTipos());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }




    @POST
    @Path("/detailing")
    @Timed
    public Response getPedidoAuts(ReorderDTO reorder){
        Response response;

        Either<IException, String> mailEither = pedidosBusiness.detailintaut(reorder.getreorder(),reorder.getidstore());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



    @POST
    @Path("/create")
    @Timed
    public Response postBill(BillPDTO billPDTO){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.create(billPDTO.getid_third_employee(),
                billPDTO.getid_third(),
                billPDTO.getid_store(),
                billPDTO.gettotalprice(),
                billPDTO.getsubtotal(),
                billPDTO.gettax(),
                billPDTO.getid_bill_state(),
                billPDTO.getid_bill_type(),
                billPDTO.getid_third_destinity(),
                billPDTO.getid_store_cliente());

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/create2")
    @Timed
    public Response postBill2(BillPDTO2 billPDTO){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.create2(billPDTO.getid_third_employee(),
                billPDTO.getid_third(),
                billPDTO.getid_store(),
                billPDTO.gettotalprice(),
                billPDTO.getsubtotal(),
                billPDTO.gettax(),
                billPDTO.getid_bill_state(),
                billPDTO.getid_bill_type(),
                billPDTO.getid_third_destinity(),
                billPDTO.getid_store_cliente(),
                billPDTO.getnum_documento_cliente());

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @POST
    @Path("/detail")
    @Timed
    public Response createDetail(
            @QueryParam("id_bill") Long id_bill,
            @QueryParam("cantidad") Long cantidad,
            @QueryParam("id_ps") Long id_ps,
            @QueryParam("price") Double price,
            @QueryParam("tax") Double tax){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.createDetail(id_bill,cantidad,id_ps,price,tax);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @PUT
    @Path("/quantity")
    @Timed
    public Response putInventoryBill(
            @QueryParam("quantity") Long quantity,
            @QueryParam("id_product_store") Long id_product_store){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.putInventoryBill(quantity,id_product_store);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @PUT
    @Path("/billtotal")
    @Timed
    public Response updateBillTotal(
            @QueryParam("total") Long total,
            @QueryParam("idbill") Long idbill){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.updateBillTotal(total,idbill);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }









    @PUT
    @Path("/billvalues")
    @Timed
    public Response updateBillTotal2(
            @QueryParam("idbill") Long idbill){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.updateBillTotal2(idbill);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }














    @PUT
    @Path("/quantitysum")
    @Timed
    public Response putInventoryBill2(
            @QueryParam("quantity") Long quantity,
            @QueryParam("id_product_store") Long id_product_store){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.putInventoryBill2(quantity,id_product_store);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




    @POST
    @Path("/pdf")
    @Timed
    public Response postPdf(PdfPedidosDTO pedido){
        Response response;

        //Either<IException, Long> getProducts = pedidosBusiness.putInventoryBill2(quantity,id_product_store);



        /*if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;*/
        Either<IException, String> mailEither =Either.right("NOPDF!!!");


        mailEither = pedidosBusiness.printPDF(pedido);
        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }










    @POST
    @Path("/pdf2")
    @Timed
    public Response postPdf2(PdfPedidosDTO pedido){
        Response response;

        //Either<IException, Long> getProducts = pedidosBusiness.putInventoryBill2(quantity,id_product_store);



        /*if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;*/
        Either<IException, String> mailEither =Either.right("NOPDF!!!");


        mailEither = pedidosBusiness.printPDF2(pedido);
        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }




    @POST
    @Path("/pdf3")
    @Timed
    public Response postPdf3(PdfPedidosDTO pedido){
        Response response;

        //Either<IException, Long> getProducts = pedidosBusiness.putInventoryBill2(quantity,id_product_store);



        /*if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;*/
        Either<IException, String> mailEither =Either.right("NOPDF!!!");


        mailEither = pedidosBusiness.printPDF3(pedido);
        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }




    @POST
    @Path("/addNotesBill")
    @Timed
    public Response addNotesBill(@QueryParam("notas") String notas,
                                 @QueryParam("idbc") Long idbc,
                                 @QueryParam("idbp") Long idbp){
        Response response;

        //Either<IException, Long> getProducts = pedidosBusiness.putInventoryBill2(quantity,id_product_store);



        /*if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;*/
        Either<IException, Long> mailEither = pedidosBusiness.addNotesBill(notas,idbc,idbp);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }




    @POST
    @Path("/pickingPdf")
    @Timed
    public Response pickingPdf(PickingConsolidadoPdfDTO pedido){
        Response response;

        response=Response.status(Response.Status.OK).entity("OK!!!").build();

        System.out.println("THIS IS TOTAL COST: "+pedido.getcostoTotal());
        System.out.println("THIS IS TOTAL PRICE: "+pedido.getprecioTotal());

        System.out.println("--LETS CHECK THE LIST OF BRANDS NOW--");

        List<FabricanteDTO> brandList = pedido.getlist();

        for(FabricanteDTO brand: brandList){

            System.out.println("WE ARE ON BRAND: "+brand.getmarca());
            System.out.println("ITS TOTAL COST WAS: "+brand.getcostoTotal());
            System.out.println("ITS TOTAL PRICE WAS: "+brand.getprecioTotal());

            System.out.println("**LETS CHECK THIS BRANDS DETAILS**");

            List<DetallePedidoDTO> details = brand.getdetalles();

            for(DetallePedidoDTO detail: details) {

                System.out.println(detail.getPRODUCTO()+", "+
                                    detail.getCATEGORIA()+", "+
                                    detail.getLINEA()+", "+
                                    detail.getFABRICANTE()+", "+
                                    detail.getMARCA()+", "+
                                    detail.getPRESENTACION()+", "+
                                    detail.getCANTIDAD()+", "+
                                    detail.getCOSTO()+", "+
                                    detail.getCOSTOTOTAL()+", "+
                                    detail.getID_PRODUCT_THIRD());

            }

            System.out.println("**THIS IS THE END OF THE BRAND DETAIL CHECK**");

        }

        System.out.println("--THIS IS THE END OF THE WHOLE THING CHECK--");
        Either<IException, String> mailEither =Either.right("NOPDF!!!");


        mailEither = pedidosBusiness.printPickingPDF(pedido);
        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }


        return response;
    }





    @POST
    @Path("/pickingExcel")
    @Timed
    public Response pickingExcel(PickingConsolidadoPdfDTO pedido){
        Response response;

        response=Response.status(Response.Status.OK).entity("OK!!!").build();

        System.out.println("THIS IS TOTAL COST: "+pedido.getcostoTotal());
        System.out.println("THIS IS TOTAL PRICE: "+pedido.getprecioTotal());

        System.out.println("--LETS CHECK THE LIST OF BRANDS NOW--");

        List<FabricanteDTO> brandList = pedido.getlist();

        for(FabricanteDTO brand: brandList){

            System.out.println("WE ARE ON BRAND: "+brand.getmarca());
            System.out.println("ITS TOTAL COST WAS: "+brand.getcostoTotal());
            System.out.println("ITS TOTAL PRICE WAS: "+brand.getprecioTotal());

            System.out.println("**LETS CHECK THIS BRANDS DETAILS**");

            List<DetallePedidoDTO> details = brand.getdetalles();

            for(DetallePedidoDTO detail: details) {

                System.out.println(detail.getPRODUCTO()+", "+
                        detail.getCATEGORIA()+", "+
                        detail.getLINEA()+", "+
                        detail.getFABRICANTE()+", "+
                        detail.getMARCA()+", "+
                        detail.getPRESENTACION()+", "+
                        detail.getCANTIDAD()+", "+
                        detail.getCOSTO()+", "+
                        detail.getCOSTOTOTAL()+", "+
                        detail.getID_PRODUCT_THIRD());

            }

            System.out.println("**THIS IS THE END OF THE BRAND DETAIL CHECK**");

        }

        System.out.println("--THIS IS THE END OF THE WHOLE THING CHECK--");
        Either<IException, String> mailEither =Either.right("NOPDF!!!");


        mailEither = pedidosBusiness.postExcel(pedido);
        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }


        return response;
    }





    @GET
    @Path("/quantity")
    @Timed
    public Response getQuantity(@QueryParam("id_product_store") Long id_product_store){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.getQuantity(id_product_store);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }










    @POST
    @Path("/procedure2")
    @Timed
    public Response procedure2(
                               @QueryParam("idvehiculo") Long idvehiculo,
                               @QueryParam("idbventa") Long idbventa,
                               @QueryParam("idbcompra") Long idbcompra,
                               @QueryParam("idbremision") Long idbremision,
                               @QueryParam("idstorecliente") Long idstorecliente,
                               @QueryParam("idstoreproveedor") Long idstoreproveedor,
                               @QueryParam("idbpedidoproveedor") Long idbpedidoproveedor){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.procedure2(
                idvehiculo,
                idbventa,
                idbcompra,
                idbremision,
                idstorecliente,
                idstoreproveedor,
                idbpedidoproveedor);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/procedimiento")
    @Timed
    public Response procedimiento(@QueryParam("idbventa") Long idbventa,@QueryParam("idcompra") Long idcompra,@QueryParam("idremision") Long idremision){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.procedimiento(idbventa,idcompra,idremision);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }





}