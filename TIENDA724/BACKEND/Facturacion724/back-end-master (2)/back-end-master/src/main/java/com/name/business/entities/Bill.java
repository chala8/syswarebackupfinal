package com.name.business.entities;

import java.util.Date;

public class Bill {
    private Long id_bill;
    private Long id_bill_father;
    private Long id_third_employee;
    private Long id_third;
    private String consecutive;
    private Date purchase_date;
    private Double subtotal;
    private Double totalprice;
    private Double tax;
    private Double discount;

    private Long id_payment_state;
    private Long id_bill_state;
    private Long id_bill_type;

    private Long id_state_bill;
    private Integer state_bill;
    private Date creation_bill;
    private Date update_bill;




    public Bill(Long id_bill, Long id_bill_father, Long id_third_employee, Long id_third, String consecutive, Date purchase_date, Double subtotal, Double totalprice, Double tax,Double discount, Long id_payment_state, Long id_bill_state, Long id_bill_type, CommonState state) {
        this.id_bill = id_bill;
        this.id_bill_father = id_bill_father;
        this.id_third_employee = id_third_employee;
        this.id_third = id_third;
        this.consecutive = consecutive;
        this.purchase_date = purchase_date;
        this.subtotal = subtotal;
        this.totalprice = totalprice;
        this.tax = tax;
        this.discount=discount;
        this.id_payment_state = id_payment_state;
        this.id_bill_state = id_bill_state;
        this.id_bill_type = id_bill_type;
        this.id_state_bill=state.getId_common_state();
        this.state_bill=state.getState();
        this.creation_bill=state.getCreation_date();
        this.update_bill=state.getUpdate_date();

    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_bill(),
                this.getState_bill(),
                this.getCreation_bill(),
                this.getUpdate_bill()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_bill =state.getId_common_state();
        this.state_bill  =state.getState();
        this.creation_bill =state.getCreation_date();
        this.update_bill =state.getUpdate_date();
    }


    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Long getId_state_bill() {
        return id_state_bill;
    }

    public void setId_state_bill(Long id_state_bill) {
        this.id_state_bill = id_state_bill;
    }

    public Integer getState_bill() {
        return state_bill;
    }

    public void setState_bill(Integer state_bill) {
        this.state_bill = state_bill;
    }

    public Date getCreation_bill() {
        return creation_bill;
    }

    public void setCreation_bill(Date creation_bill) {
        this.creation_bill = creation_bill;
    }

    public Date getUpdate_bill() {
        return update_bill;
    }

    public void setUpdate_bill(Date update_bill) {
        this.update_bill = update_bill;
    }

    public Long getId_bill() {
        return id_bill;
    }

    public void setId_bill(Long id_bill) {
        this.id_bill = id_bill;
    }

    public Long getId_bill_father() {
        return id_bill_father;
    }

    public void setId_bill_father(Long id_bill_father) {
        this.id_bill_father = id_bill_father;
    }

    public Long getId_third_employee() {
        return id_third_employee;
    }

    public void setId_third_employee(Long id_third_employee) {
        this.id_third_employee = id_third_employee;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_payment_state() {
        return id_payment_state;
    }

    public void setId_payment_state(Long id_payment_state) {
        this.id_payment_state = id_payment_state;
    }

    public Long getId_bill_state() {
        return id_bill_state;
    }

    public void setId_bill_state(Long id_bill_state) {
        this.id_bill_state = id_bill_state;
    }

    public Long getId_bill_type() {
        return id_bill_type;
    }

    public void setId_bill_type(Long id_bill_type) {
        this.id_bill_type = id_bill_type;
    }

    public String getConsecutive() {
        return consecutive;
    }

    public void setConsecutive(String consecutive) {
        this.consecutive = consecutive;
    }

    public Date getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(Date purchase_date) {
        this.purchase_date = purchase_date;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(Double totalprice) {
        this.totalprice = totalprice;
    }

}
