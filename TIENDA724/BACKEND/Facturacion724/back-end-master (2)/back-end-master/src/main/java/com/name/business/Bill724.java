package com.name.business;

import com.name.business.DAOs.*;
import com.name.business.businesses.*;
import com.name.business.resources.*;
import io.dropwizard.Application;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.java8.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.commons.dbcp2.BasicDataSource;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.EnumSet;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.name.business.utils.constans.K.URI_BASE;


public class Bill724 extends Application<BillAppConfiguration> {



    public static void main(String[] args) throws Exception  {
        new Bill724().run(args);
    }





    @Override
    public void initialize(Bootstrap<BillAppConfiguration> bootstrap) {

        bootstrap.addBundle(new Java8Bundle());
        bootstrap.addBundle(new MultiPartBundle());


    }
    @Override
    public void run(BillAppConfiguration configuration, Environment environment) throws Exception {
        environment.jersey().setUrlPattern(URI_BASE+"/*");

        // Enable CORS headers
        final FilterRegistration.Dynamic corsFilter =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        corsFilter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        corsFilter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        corsFilter.setInitParameter("allowCredentials", "true");

        // Add URL mapping
        corsFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");




        // Se estable el ambiente de coneccion con JDBI con la base de datos de oracle
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "oracle");


        final CommonStateDAO commonStateDAO = jdbi.onDemand(CommonStateDAO.class);
        final CommonStateBusiness commonStateBusiness = new CommonStateBusiness(commonStateDAO);

        final PedidosDAO pedidosDAO = jdbi.onDemand(PedidosDAO.class);
        final ReorderDAO pedidosDAO2 = jdbi.onDemand(ReorderDAO.class);
        final PedidosBusiness pedidosBusiness = new PedidosBusiness(pedidosDAO,pedidosDAO2);

        //EXECUTE REORDER DAILY
        ZonedDateTime now = ZonedDateTime.now(ZoneId.systemDefault());
        ZonedDateTime nextRun = now.withHour(14).withMinute(35).withSecond(0);
        if(now.compareTo(nextRun) > 0)
            nextRun = nextRun.plusDays(1);

        Duration duration = Duration.between(now, nextRun);
        long initalDelay = duration.getSeconds();

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        Runnable taskWrapper = new Runnable(){
            @Override
            public void run()
            {
                execute(pedidosBusiness);
            }

        };

        scheduler.scheduleAtFixedRate(taskWrapper,
                initalDelay,
                TimeUnit.DAYS.toSeconds(1),
                TimeUnit.SECONDS);

        final BillStateDAO billStateDAO = jdbi.onDemand(BillStateDAO.class);
        final RefundDAO refundDAO = jdbi.onDemand(RefundDAO.class);
        final BillDataDAO billDataDAO = jdbi.onDemand(BillDataDAO.class);
        final BillStateBusiness billStateBusiness= new BillStateBusiness(billDataDAO, billStateDAO, commonStateBusiness, refundDAO);



        final BillTypeDAO billTypeDAO= jdbi.onDemand(BillTypeDAO.class);
        final BillTypeBusiness billTypeBusiness= new BillTypeBusiness(billTypeDAO, commonStateBusiness);

        final DetailBillDAO detailBillDAO= jdbi.onDemand(DetailBillDAO.class);
        final DetailBillBusiness detailBillBusiness= new DetailBillBusiness(detailBillDAO, commonStateBusiness);

        final PaymentMethodDAO paymentMethodDAO= jdbi.onDemand(PaymentMethodDAO.class);
        final PaymentMethodBusiness paymentMethodBusiness= new PaymentMethodBusiness(paymentMethodDAO, commonStateBusiness);

        final ReorderDAO reorderDAO= jdbi.onDemand(ReorderDAO.class);
        final ReorderBusiness reorderBusiness= new ReorderBusiness(reorderDAO);

        final PaymentStateDAO paymentStateDAO= jdbi.onDemand(PaymentStateDAO.class);
        final PaymentStateBusiness paymentStateBusiness = new PaymentStateBusiness(paymentStateDAO, commonStateBusiness);

        final WayToPayDAO wayToPayDAO= jdbi.onDemand(WayToPayDAO.class);
        final WayToPayBusiness wayToPayBusiness= new WayToPayBusiness(wayToPayDAO, commonStateBusiness);

        final DetailPaymentBillDAO detailPaymentBillDAO= jdbi.onDemand(DetailPaymentBillDAO.class);
        final DetailPaymentBillBusiness detailPaymentBillBusiness= new DetailPaymentBillBusiness(detailPaymentBillDAO, commonStateBusiness,paymentMethodBusiness,wayToPayBusiness);

        final BillDAO billDAO= jdbi.onDemand(BillDAO.class);
        final BillBusiness billBusiness= new BillBusiness(billDAO, commonStateBusiness,paymentStateBusiness,
                billStateBusiness,billTypeBusiness,detailBillBusiness,paymentMethodBusiness,detailPaymentBillBusiness);



        BillDatabaseConfiguration routesDatabaseConfiguration = configuration.getDatabaseConfiguration();
        BasicDataSource datasource = createDataSource(routesDatabaseConfiguration);


        environment.jersey().register(new PedidosResource(pedidosBusiness,billBusiness));
        environment.jersey().register(new ReorderResource(reorderBusiness));
        environment.jersey().register(new BillResource(billBusiness));
        environment.jersey().register(new BillStateResource(billStateBusiness));
        environment.jersey().register(new BillTypeResource(billTypeBusiness));
        environment.jersey().register(new DetailBillResource(detailBillBusiness));
        environment.jersey().register(new DetailPaymentBillResource(detailPaymentBillBusiness));
        environment.jersey().register(new PaymentMethodResource(paymentMethodBusiness));
        environment.jersey().register(new PaymentStateResource(paymentStateBusiness) );
        environment.jersey().register(new WayToPayResource(wayToPayBusiness));




        // apply security on http request
        // addAuthorizationFilterAndProvider(environment,tokenBusiness);




        Timer timer = new Timer();

        //timer.scheduleAtFixedRate(fcmThread, 0, 30*1000);   //cada 5 minutos




    }
    /**
     * @Método:  Hace  la configuración para establecer conexión con las bases de datos que se
     * va a utilizar como persistencia del servidor
     * */
    private BasicDataSource createDataSource(BillDatabaseConfiguration billDatabaseConfiguration){

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(billDatabaseConfiguration.getDriverClassName());
        basicDataSource.setUrl(billDatabaseConfiguration.getUrl());
        basicDataSource.setUsername(billDatabaseConfiguration.getUsername());
        basicDataSource.setPassword(billDatabaseConfiguration.getPassword());
        basicDataSource.addConnectionProperty("characterEncoding", "UTF-8");

        basicDataSource.setRemoveAbandonedOnMaintenance(true);
        basicDataSource.setRemoveAbandonedOnBorrow(true);
        basicDataSource.setRemoveAbandonedTimeout(30000);

        return basicDataSource;

    }

    public void execute(PedidosBusiness a){
        System.out.println(a.detailintaut2());
    }




}
