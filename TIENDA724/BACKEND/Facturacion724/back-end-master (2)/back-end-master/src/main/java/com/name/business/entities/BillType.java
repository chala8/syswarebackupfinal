package com.name.business.entities;

import java.util.Date;

public class BillType {
    private Long id_bill_type;
    private String name_bill_type;
    private Long id_state_bill_type;
    private Integer state_bill_type;
    private Date creation_bill_type;
    private Date update_bill_type;

    public BillType(Long id_bill_type, String name_bill_type, CommonState state) {
        this.id_bill_type = id_bill_type;
        this.name_bill_type = name_bill_type;
        this.id_state_bill_type = state.getId_common_state();
        this.state_bill_type=state.getState();
        this.creation_bill_type = state.getCreation_date();
        this.update_bill_type = state.getUpdate_date();
    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_bill_type(),
                this.getState_bill_type(),
                this.getCreation_bill_type(),
                this.getUpdate_bill_type()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_bill_type =state.getId_common_state();
        this.state_bill_type  =state.getState();
        this.creation_bill_type =state.getCreation_date();
        this.update_bill_type =state.getUpdate_date();
    }


    public Integer getState_bill_type() {
        return state_bill_type;
    }

    public void setState_bill_type(Integer state_bill_type) {
        this.state_bill_type = state_bill_type;
    }

    public Long getId_bill_type() {
        return id_bill_type;
    }

    public void setId_bill_type(Long id_bill_type) {
        this.id_bill_type = id_bill_type;
    }

    public String getName_bill_type() {
        return name_bill_type;
    }

    public void setName_bill_type(String name_bill_type) {
        this.name_bill_type = name_bill_type;
    }

    public Long getId_state_bill_type() {
        return id_state_bill_type;
    }

    public void setId_state_bill_type(Long id_state_bill_type) {
        this.id_state_bill_type = id_state_bill_type;
    }

    public Date getCreation_bill_type() {
        return creation_bill_type;
    }

    public void setCreation_bill_type(Date creation_bill_type) {
        this.creation_bill_type = creation_bill_type;
    }

    public Date getUpdate_bill_type() {
        return update_bill_type;
    }

    public void setUpdate_bill_type(Date update_bill_type) {
        this.update_bill_type = update_bill_type;
    }


}
