package com.name.business.entities;

public class Pedido {

    private Long ID_BILL;
    private Long ID_STORE;
    private Long ID_STORE_CLIENT;
    private String CLIENTE;
    private String TIENDA;
    private String NUMPEDIDO;
    private String FECHA;
    private String ADDRESS;
    private String PHONE;
    private String MAIL;
    private String LATITUD;
    private String LONGITUD;
    private String NUMDOCUMENTO;


    public Pedido(Long ID_BILL,
                  Long ID_STORE_CLIENT,
                  Long ID_STORE,
                  String CLIENTE,
                  String TIENDA,
                  String NUMPEDIDO,
                  String FECHA,
                  String ADDRESS,
                  String PHONE,
                  String MAIL,
                  String LATITUD,
                  String LONGITUD,
                  String NUMDOCUMENTO) {
        this.ID_STORE_CLIENT = ID_STORE_CLIENT;
        this.ID_STORE = ID_STORE;
        this.NUMDOCUMENTO = NUMDOCUMENTO;
        this.ID_BILL = ID_BILL;
        this.CLIENTE = CLIENTE;
        this.TIENDA =  TIENDA;
        this.NUMPEDIDO = NUMPEDIDO;
        this.FECHA = FECHA;
        this.ADDRESS = ADDRESS;
        this.PHONE = PHONE;
        this.MAIL = MAIL;
        this.LATITUD = LATITUD;
        this.LONGITUD = LONGITUD;
    }
    //----------------------------------------------------------------------------
    public Long getID_STORE_CLIENT() {
        return ID_STORE_CLIENT;
    }
    public void setID_STORE_CLIENT(Long ID_STORE_CLIENT) {
        this.ID_STORE_CLIENT = ID_STORE_CLIENT;
    }
    //----------------------------------------------------------------------------
    public Long getID_STORE() {
        return ID_STORE;
    }
    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }
    //----------------------------------------------------------------------------
    public String getNUMDOCUMENTO() {
        return NUMDOCUMENTO;
    }
    public void setNUMDOCUMENTO(String NUMDOCUMENTO) {
        this.NUMDOCUMENTO = NUMDOCUMENTO;
    }
    //----------------------------------------------------------------------------
    public String getLONGITUD() {
        return LONGITUD;
    }
    public void setLONGITUD(String LONGITUD) {
        this.LONGITUD = LONGITUD;
    }
    //----------------------------------------------------------------------------
    public String getLATITUD() {
        return LATITUD;
    }
    public void setLATITUD(String LATITUD) {
        this.LATITUD = LATITUD;
    }
    //----------------------------------------------------------------------------
    public String getMAIL() {
        return MAIL;
    }
    public void setMAIL(String MAIL) {
        this.MAIL = MAIL;
    }
    //----------------------------------------------------------------------------
    public String getPHONE() {
        return PHONE;
    }
    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }
    //----------------------------------------------------------------------------
    public String getADDRESS() {
        return ADDRESS;
    }
    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }
    //----------------------------------------------------------------------------
    public Long getID_BILL() {
        return ID_BILL;
    }
    public void setID_BILL(Long ID_BILL) {
        this.ID_BILL = ID_BILL;
    }
    //----------------------------------------------------------------------------
    public String getCLIENTE() {
        return CLIENTE;
    }
    public void setCLIENTE(String CLIENTE) {
        this.CLIENTE = CLIENTE;
    }
    //----------------------------------------------------------------------------
    public String getTIENDA() {
        return TIENDA;
    }
    public void setTIENDA(String TIENDA) {
        this.TIENDA = TIENDA;
    }
    //----------------------------------------------------------------------------
    public String getNUMPEDIDO() {
        return NUMPEDIDO;
    }
    public void setNUMPEDIDO(String NUMPEDIDO) {
        this.NUMPEDIDO = NUMPEDIDO;
    }
    //----------------------------------------------------------------------------
    public String getFECHA() {
        return FECHA;
    }
    public void setFECHA(String FECHA) {
        this.FECHA = FECHA;
    }
    //----------------------------------------------------------------------------
}
