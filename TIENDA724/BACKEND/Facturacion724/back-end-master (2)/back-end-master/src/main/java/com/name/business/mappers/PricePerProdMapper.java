package com.name.business.mappers;

import com.name.business.entities.Bill;
import com.name.business.entities.CommonState;
import com.name.business.entities.Refund;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PricePerProdMapper  implements ResultSetMapper<Refund> {

    @Override
    public Refund map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Refund(
                resultSet.getLong("ID_PRODUCT_THIRD"),
                resultSet.getLong("STANDARD_PRICE"),
                resultSet.getString("DESCRIPTION")
        );
    }
}
