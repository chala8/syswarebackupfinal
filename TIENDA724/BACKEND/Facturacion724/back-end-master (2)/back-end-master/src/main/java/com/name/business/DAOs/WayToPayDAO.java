package com.name.business.DAOs;

import com.name.business.entities.CommonSimple;
import com.name.business.entities.WayToPay;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.WayToPayMapper;
import com.name.business.representations.WayToPayDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(WayToPayMapper.class)
public interface WayToPayDAO  {

    @SqlQuery("SELECT ID_WAY_TO_PAY FROM WAY_TO_PAY WHERE  ID_WAY_TO_PAY IN (SELECT MAX( ID_WAY_TO_PAY) FROM WAY_TO_PAY )\n")
    Long getPkLast();

    @SqlQuery("SELECT COUNT(ID_WAY_TO_PAY) FROM WAY_TO_PAY WHERE  ID_WAY_TO_PAY= :id")
    Integer getValidatorID(@Bind("id") Long id);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_WAY_TO_PAY  ID, ID_COMMON_STATE FROM WAY_TO_PAY WHERE(  ID_WAY_TO_PAY= :id OR :id IS NULL)\n ")
    List<CommonSimple> readCommons(@Bind("id") Long id);

    @SqlQuery("SELECT * FROM V_WAYTOPAY V_WAY_PAY\n" +
            "WHERE (V_WAY_PAY.ID_WAY_TO_PAY=:way_pay.id_way_pay OR :way_pay.id_way_pay IS NULL ) AND\n" +
            "      (V_WAY_PAY.NAME_WAY_PAY LIKE :way_pay.name_way_pay OR :way_pay.name_way_pay IS NULL ) AND\n" +
            "      (V_WAY_PAY.ID_CM_WAY_PAY=:way_pay.id_state_way_pay OR :way_pay.id_state_way_pay IS NULL ) AND\n" +
            "      (V_WAY_PAY.STATE_CM_WAY_PAY =:way_pay.state_way_pay OR :way_pay.state_way_pay IS NULL ) AND\n" +
            "      (V_WAY_PAY.CREATION_WAY_PAY =:way_pay.creation_way_pay OR :way_pay.creation_way_pay IS NULL ) AND\n" +
            "      (V_WAY_PAY.UPDATE_WAY_PAY= :way_pay.update_way_pay OR :way_pay.update_way_pay IS NULL )")
    List<WayToPay> read(@BindBean("way_pay") WayToPay wayToPay);

    @SqlUpdate("INSERT INTO WAY_TO_PAY ( ID_COMMON_STATE,NAME) \n" +
            " VALUES (:id_cm_st,:name_way_pay)\n")
    void create(@Bind("id_cm_st") Long aLong,@Bind("name_way_pay") String wayToPayDTO);

    @SqlUpdate("UPDATE WAY_TO_PAY SET\n" +
            "    NAME = :name\n" +
            "    WHERE (ID_WAY_TO_PAY =  :id_way_pay)")
    int update(@Bind("id_way_pay") Long aLong, @Bind("name") String name);

    @SqlUpdate()
    int delete();

    @SqlUpdate()
    int deletePermanent();

}
