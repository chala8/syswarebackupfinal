package com.name.business.mappers;

import com.name.business.entities.Bill;
import com.name.business.entities.BillToUpdate;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillToUpdateMapper  implements ResultSetMapper<BillToUpdate> {

    @Override
    public BillToUpdate map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BillToUpdate(
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("ID_THIRD"),
                resultSet.getLong("ID_THIRD_EMPLOYEE")
        );
    }
}
