package com.name.business.mappers;

import com.name.business.entities.CommonState;
import com.name.business.entities.PaymentMethod;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PaymentMethodMapper implements ResultSetMapper<PaymentMethod> {
    @Override
    public PaymentMethod map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PaymentMethod(
                resultSet.getLong("ID_PAYMENT_METHOD"),
                resultSet.getString("NAME_PAY_ME"),
                new CommonState(
                        resultSet.getLong("ID_CM_PAY_MET"),
                        resultSet.getInt("STATE_PAY_ME"),
                        resultSet.getDate("CREATION_PAY_ME"),
                        resultSet.getDate("UPDATE_PAY_ME")
                )
        );
    }
}
