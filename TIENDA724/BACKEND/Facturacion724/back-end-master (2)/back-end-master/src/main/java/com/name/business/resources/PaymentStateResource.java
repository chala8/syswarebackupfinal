package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.PaymentStateBusiness;
import com.name.business.entities.CommonState;
import com.name.business.entities.PaymentState;
import com.name.business.representations.PaymentStateDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/payments-state")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PaymentStateResource {
    private PaymentStateBusiness paymentStateBusiness;

    public PaymentStateResource(PaymentStateBusiness paymentStateBusiness) {
        this.paymentStateBusiness = paymentStateBusiness;
    }

    @GET
    @Timed
    public Response getPaymentStateResource(
            @QueryParam("id_payment_state") Long id_payment_state,
            @QueryParam("name_payment_state") String name_payment_state,

            @QueryParam("id_state_payment_state") Long id_state_payment_state,
            @QueryParam("state_payment_state") Integer state_product,
            @QueryParam("creation_payment_state") Date creation_payment_state,
            @QueryParam("update_payment_state") Date update_payment_state
    ){

        Response response;

        Either<IException, List<PaymentState>> getResponseGeneric = paymentStateBusiness.getPaymentState(
                new PaymentState(id_payment_state, name_payment_state,
                        new CommonState(id_state_payment_state, state_product,
                                creation_payment_state, update_payment_state)
                )
        );

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value().size());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @POST
    @Timed
    public Response postPaymentStateResource(PaymentStateDTO paymentStateDTO){
        Response response;

        Either<IException, Long> getResponseGeneric = paymentStateBusiness.createPaymentState(paymentStateDTO);


        if (getResponseGeneric.isRight()){

            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updatePaymentStateResource(@PathParam("id") Long id_payment_state, PaymentStateDTO paymentStateDTO){
        Response response;
        Either<IException, Long> getResponseGeneric = paymentStateBusiness.updatePaymentState(id_payment_state, paymentStateDTO);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    public Response deletePaymentStateResource(@PathParam("id") Long id_payment_state){
        Response response;
        Either<IException, Long> getResponseGeneric = paymentStateBusiness.deletePaymentState(id_payment_state);

        if (getResponseGeneric.isRight()){
            System.out.println(getResponseGeneric.right().value());
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

}
