package com.name.business.mappers;

import com.name.business.entities.BillMaster;
import com.name.business.entities.BillMaster2;
import com.name.business.entities.CommonState;
import com.name.business.entities.BillData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillMasterMapper2 implements ResultSetMapper<BillMaster2> {

    @Override
    public BillMaster2 map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BillMaster2(
                resultSet.getString("PURCHASE_DATE"),
                resultSet.getString("PREFIX_BILL"),
                resultSet.getLong("CONSECUTIVE"),
                resultSet.getString("FULLNAME"),
                resultSet.getString("STORE_NAME"),
                resultSet.getString("NAME"),
                resultSet.getString("DOCUMENT_NUMBER"),
                resultSet.getDouble("SUBTOTAL"),
                resultSet.getDouble("TAX"),
                resultSet.getDouble("TOTALPRICE")
        );
    }
}
