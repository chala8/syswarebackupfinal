package com.name.business.entities;

import java.util.Date;

public class BillMaster {
    private String PURCHASE_DATE;
    private String PREFIX_BILL;
    private Long CONSECUTIVE;
    private String FULLNAME;
    private String CAJA;
    private String STORE_NAME;
    private String NAME;
    private String DOCUMENT_NUMBER;
    private Double SUBTOTAL;
    private Double TAX;
    private Double TOTALPRICE;
    private String NUM_DOCUMENTO;

    public BillMaster(String PURCHASE_DATE,
                      String PREFIX_BILL,
                      Long CONSECUTIVE,
                      String FULLNAME,
                      String CAJA,
                      String STORE_NAME,
                      String NAME,
                      String DOCUMENT_NUMBER,
                      Double SUBTOTAL,
                      Double TAX,
                      Double TOTALPRICE,
                      String NUM_DOCUMENTO) {
        this.PURCHASE_DATE = PURCHASE_DATE;
        this.PREFIX_BILL = PREFIX_BILL;
        this.CONSECUTIVE = CONSECUTIVE;
        this.FULLNAME = FULLNAME;
        this.CAJA = CAJA;
        this.STORE_NAME = STORE_NAME;
        this.NAME = NAME;
        this.DOCUMENT_NUMBER =DOCUMENT_NUMBER;
        this.SUBTOTAL = SUBTOTAL;
        this.TAX = TAX;
        this.TOTALPRICE = TOTALPRICE;
        this.NUM_DOCUMENTO = NUM_DOCUMENTO;
    }
    //--------------------------------------------------------------------------------------------------

    public String getNUM_DOCUMENTO() {
        return NUM_DOCUMENTO;
    }

    public void setNUM_DOCUMENTO(String NUM_DOCUMENTO) {
        this.NUM_DOCUMENTO = NUM_DOCUMENTO;
    }

    //--------------------------------------------------------------------------------------------------

    public String getPURCHASE_DATE() {
        return PURCHASE_DATE;
    }

    public void setPURCHASE_DATE(String PURCHASE_DATE) {
        this.PURCHASE_DATE = PURCHASE_DATE;
    }

    //--------------------------------------------------------------------------------------------------

    public String getPREFIX_BILL() {
        return PREFIX_BILL;
    }

    public void setPREFIX_BILL(String PREFIX_BILL) {
        this.PREFIX_BILL = PREFIX_BILL;
    }

    //--------------------------------------------------------------------------------------------------

    public Long getCONSECUTIVE() {
        return CONSECUTIVE;
    }

    public void setCONSECUTIVE(Long CONSECUTIVE) {
        this.CONSECUTIVE = CONSECUTIVE;
    }

    //--------------------------------------------------------------------------------------------------

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }

    //--------------------------------------------------------------------------------------------------

    public String getCAJA() {
        return CAJA;
    }

    public void setCAJA(String CAJA) {
        this.CAJA = CAJA;
    }

    //--------------------------------------------------------------------------------------------------

    public String getSTORE_NAME() {
        return STORE_NAME;
    }

    public void setSTORE_NAME(String STORE_NAME) {
        this.STORE_NAME = STORE_NAME;
    }

    //--------------------------------------------------------------------------------------------------

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    //--------------------------------------------------------------------------------------------------

    public String getDOCUMENT_NUMBER() {
        return DOCUMENT_NUMBER;
    }

    public void setDOCUMENT_NUMBER(String DOCUMENT_NUMBER) {
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
    }

    //--------------------------------------------------------------------------------------------------

    public Double getSUBTOTAL() {
        return SUBTOTAL;
    }

    public void setSUBTOTAL(Double SUBTOTAL) {
        this.SUBTOTAL = SUBTOTAL;
    }

    //--------------------------------------------------------------------------------------------------

    public Double getTAX() {
        return TAX;
    }

    public void setTAX(Double TAX) {
        this.TAX = TAX;
    }

    //--------------------------------------------------------------------------------------------------

    public Double getTOTALPRICE() {
        return TOTALPRICE;
    }

    public void setTOTALPRICE(Double TOTALPRICE) {
        this.TOTALPRICE = TOTALPRICE;
    }

    //--------------------------------------------------------------------------------------------------






}
