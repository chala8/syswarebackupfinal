package com.name.business.DAOs;
import com.name.business.entities.BillToUpdate;
import com.name.business.entities.Pedido;
import com.name.business.entities.DetallePedido;
import com.name.business.entities.TablaPedidos;
import com.name.business.entities.DetailData;
import com.name.business.mappers.DetailDataMapper;
import com.name.business.mappers.TablaPedidosMapper;
import com.name.business.mappers.BillToUpdateMapper;
import com.name.business.mappers.DetallePedidoMapper;
import com.name.business.mappers.PedidoMapper;
import com.name.business.representations.BillDTO;
import com.name.business.representations.DetailBillDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.unstable.BindIn;

import java.sql.CallableStatement;
import java.util.List;

@RegisterMapper(PedidoMapper.class)
@UseStringTemplate3StatementLocator
public interface PedidosDAO {


    @SqlQuery(" select ownbarcode from tienda724.product_store where id_product_store=:id_product_store ")
    String getOwnBarCode(@Bind("id_product_store") Long id_product_store);


    @SqlQuery(" select id_product_store from tienda724.product_store where ownbarcode=:code and id_store = :id_store ")
    Long getPsId(@Bind("code") String code, @Bind("id_store") Long id_store);


    @RegisterMapper(PedidoMapper.class)
    @SqlQuery(" select b.id_store, b.id_store_client, id_bill,c.fullname cliente,s.description tienda,num_documento,num_documento_cliente numpedido,purchase_date fecha,address,phone,mail,latitud,longitud\n" +
            "from facturacion724.bill b,tienda724.store s,tercero724.third t,tercero724.common_basicinfo c,tercero724.directory d,tercero724.phone p,tercero724.mail e\n" +
            "where b.id_store_client=s.id_store and s.id_directory=d.id_directory and d.id_directory=p.id_directory and d.id_directory=e.id_directory\n" +
            "  and b.id_third_destinity=t.id_third and t.id_common_basicinfo=c.id_common_basicinfo\n" +
            "  and id_bill_type=:id_bill_type and id_bill_state=:id_bill_state and b.id_store=:id_store and p.priority=1 and e.priority=1\n" +
            "order by fecha DESC ")
    List<Pedido> getPedidos(@Bind("id_store") Long id_store,
                            @Bind("id_bill_state") Long id_bill_state,
                            @Bind("id_bill_type") Long id_bill_type);


    @RegisterMapper(DetallePedidoMapper.class)
    @SqlQuery(" select pr.provider fabricante,b.brand marca,com.name linea,co.name categoria,comu.name presentacion,ps.product_store_name producto,\n" +
            "       sum(db.quantity) cantidad,ps.standard_price costo,sum(db.quantity)*ps.standard_price costototal\n" +
            " , db.id_product_third from tienda724.common co,tienda724.category cat,tienda724.common com,tienda724.category linea,tienda724.product p,tienda724.provider pr,\n" +
            "     tienda724.measure_unit mu,tienda724.common comu,tienda724.brand b,tienda724.codes c,tienda724.product_store ps,\n" +
            "     facturacion724.detail_bill db\n" +
            "where co.id_common=cat.id_common and cat.id_category=p.id_category and com.id_common=linea.id_common and linea.id_category=cat.id_category_father\n" +
            "  and p.id_product=c.id_product and c.id_code=ps.id_code and pr.id_provider=b.id_provider\n" +
            "  and c.id_brand=b.id_brand and c.id_measure_unit=mu.id_measure_unit and mu.id_common=comu.id_common\n" +
            "  and ps.id_product_store=db.id_product_third and id_bill in (<id_bill>)\n" +
            "group by db.id_product_third, pr.provider,b.brand,com.name,co.name,comu.name,ps.product_store_name,ps.standard_price\n" +
            "order by 1,2,3,4 ")
    List<DetallePedido> getDetallesPedidos(@BindIn("id_bill") List<Long> id_store);


    @RegisterMapper(DetallePedidoMapper.class)
    @SqlQuery(" select pr.provider fabricante,b.brand marca,com.name linea,co.name categoria,comu.name presentacion,ps.product_store_name producto,\n" +
            "              sum(db.quantity) cantidad,min(pr.price) costo,sum(db.quantity)*min(pr.price) costototal\n" +
            "        ,db.id_product_third \n" +
            "    from tienda724.common co,tienda724.category cat,tienda724.common com,tienda724.category linea,tienda724.product p,tienda724.provider pr,\n" +
            "            tienda724.measure_unit mu,tienda724.common comu,tienda724.brand b,tienda724.codes c,tienda724.product_store ps,\n" +
            "            facturacion724.detail_bill db,tienda724.price pr\n" +
            "       where co.id_common=cat.id_common and cat.id_category=p.id_category and com.id_common=linea.id_common and linea.id_category=cat.id_category_father\n" +
            "         and p.id_product=c.id_product and c.id_code=ps.id_code and pr.id_provider=b.id_provider\n" +
            "         and c.id_brand=b.id_brand and c.id_measure_unit=mu.id_measure_unit and mu.id_common=comu.id_common\n" +
            "         and ps.id_product_store=db.id_product_third and ps.id_product_store=pr.id_product_store\n" +
            "         and id_bill in (<id_bill>)\n" +
            "         group by db.id_product_third, pr.provider,b.brand,com.name,co.name,comu.name,ps.product_store_name,ps.standard_price--,tax\n" +
            "    order by 1,2,3,4 ")
    List<DetallePedido> getDetallesPedidos2(@BindIn("id_bill") List<Long> id_bill);

    @RegisterMapper(TablaPedidosMapper.class)
    @SqlQuery(" select * from tienda724.PARAM_PEDIDOS ")
    List<TablaPedidos> getPedidosT();


    @SqlUpdate(" insert into FACTURACION724.BILL ( ID_COMMON_STATE,ID_THIRD_EMPLOYEE,ID_THIRD,\n" +
            "                                  TOTALPRICE,PURCHASE_DATE,ID_STORE\n" +
            "                                  ,ID_BILL_STATE,SUBTOTAL,TAX,DISCOUNT,ID_BILL_TYPE, ID_THIRD_DESTINITY, ID_STORE_CLIENT)\n" +
            "                                 values ( 540, :id_third_employee,\n" +
            "                                         :id_third,:totalprice,sysdate,:id_store,  \n" +
            "                                         :id_bill_state, :subtotal, :tax, 0,\n" +
            "                                         :id_bill_type,\n" +
            "                                         :id_third_destinity," +
            " :id_store_cliente) ")
    void create(@Bind("id_third_employee") Long id_third_employee,
                @Bind("id_third") Long id_third,
                @Bind("id_store") Long id_store,
                @Bind("totalprice") Double totalprice,
                @Bind("subtotal") Double subtotal,
                @Bind("tax") Double tax,
                @Bind("id_bill_state") Long id_bill_state,
                @Bind("id_bill_type") Long id_bill_type,
                @Bind("id_third_destinity") Long id_third_destinity,
                @Bind("id_store_cliente") Long id_store_cliente
    );

    @SqlUpdate(" insert into FACTURACION724.BILL ( ID_COMMON_STATE,ID_THIRD_EMPLOYEE,ID_THIRD,\n" +
            "                                  TOTALPRICE,PURCHASE_DATE,ID_STORE\n" +
            "                                  ,ID_BILL_STATE,SUBTOTAL,TAX,DISCOUNT,ID_BILL_TYPE, ID_THIRD_DESTINITY, ID_STORE_CLIENT," +
            "                                   NUM_DOCUMENTO_CLIENTE)\n" +
            "                                 values ( 540, :id_third_employee,\n" +
            "                                         :id_third,:totalprice,sysdate,:id_store,  \n" +
            "                                         :id_bill_state, :subtotal, :tax, 0,\n" +
            "                                         :id_bill_type,\n" +
            "                                         :id_third_destinity," +
            "                                         :id_store_cliente," +
            "                                         :num_documento_cliente) ")
    void create2(@Bind("id_third_employee") Long id_third_employee,
                 @Bind("id_third") Long id_third,
                 @Bind("id_store") Long id_store,
                 @Bind("totalprice") Double totalprice,
                 @Bind("subtotal") Double subtotal,
                 @Bind("tax") Double tax,
                 @Bind("id_bill_state") Long id_bill_state,
                 @Bind("id_bill_type") Long id_bill_type,
                 @Bind("id_third_destinity") Long id_third_destinity,
                 @Bind("id_store_cliente") Long id_store_cliente,
                 @Bind("num_documento_cliente") String num_documento_cliente);

    @SqlQuery("SELECT ID_BILL FROM BILL WHERE ID_BILL IN (SELECT MAX( ID_BILL ) FROM BILL )\n")
    Long getPkLast();

    @SqlUpdate(" INSERT INTO DETAIL_BILL ( ID_BILL,ID_COMMON_STATE, QUANTITY, PRICE, TAX, ID_PRODUCT_THIRD,TAX_PRODUCT)\n" +
            "      VALUES (:id_bill,540,:cantidad,:price,\n" +
            "           :tax,:id_ps,1) ")
    void createDetail(@Bind("id_bill") Long id_bill,
                      @Bind("cantidad") Long cantidad,
                      @Bind("id_ps") Long id_ps,
                      @Bind("price") Double price,
                      @Bind("tax") Double tax);

    @SqlQuery("SELECT price FROM tienda724.price WHERE id_product_store = :idps\n")
    List<Long> getPrice(@Bind("idps") Long idps);

    @SqlQuery("SELECT NUM_DOCUMENTO FROM BILL WHERE ID_BILL = :id_bill\n")
    String getNumDoc(@Bind("id_bill") Long id_bill);


    @SqlQuery(" select percent from tienda724.tax_tariff, tienda724.PRODUCT_STORE where id_tax_tariff=id_tax and ID_PRODUCT_STORE = :idps ")
    Long getTax(@Bind("idps") Long idps);

    @RegisterMapper(BillToUpdateMapper.class)
    @SqlQuery("  select id_bill, id_third, id_third_employee from facturacion724.bill where id_store=:idstore and id_store_client=:idstoreclient and num_documento=:numpedido\n ")
    BillToUpdate getBillIdByIdStore(@Bind("idstore") Long idstore,
                                    @Bind("idstoreclient") Long idstoreclient,
                                    @Bind("numpedido") String numpedido);

    @SqlUpdate(" update facturacion724.bill set ID_BILL_STATE=:billstate where id_bill = :billid ")
    void updateBillState(@Bind("billstate") Long billstate,
                      @Bind("billid") Long billid);

    @SqlUpdate(" update tienda724.product_store_inventory " +
            " set quantity = quantity - :quantity " +
            " where id_product_store=:id_product_store and id_storage=:id_storage")
    void putInventoryBill(@Bind("quantity") Long quantity,
                          @Bind("id_product_store") Long id_product_store,
                          @Bind("id_storage") Long id_storage);

    @SqlQuery(" select ID_STORAGE from tienda724.PRODUCT_STORE_INVENTORY where ID_PRODUCT_STORE = :id_product_store")
    Long getStorage(@Bind("id_product_store") Long id_product_store);

    @SqlUpdate(" update tienda724.product_store_inventory " +
            " set quantity = quantity + :quantity " +
            " where id_product_store=:id_product_store and id_storage=:id_storage")
    void putInventoryBill2(@Bind("quantity") Long quantity,
                           @Bind("id_product_store") Long id_product_store,
                           @Bind("id_storage") Long id_storage);

    @SqlQuery("SELECT ID_DETAIL_BILL FROM DETAIL_BILL WHERE ID_DETAIL_BILL IN (SELECT MAX( ID_DETAIL_BILL ) FROM DETAIL_BILL )\n")
    Long getPkLastDetail();

    @RegisterMapper(DetailDataMapper.class)
    @SqlQuery("SELECT QUANTITY, ID_PRODUCT_THIRD from facturacion724.detail_bill where ID_DETAIL_BILL = :iddbill\n")
    DetailData detailData(@Bind("iddbill") Long iddbill);

    @SqlCall(" call tienda724.conversion_pedidos(:idbill,:idps) ")
    void procedure(@Bind("idbill") Long idbill,
                   @Bind("idps") Long idps);

    @SqlCall(" call facturacion724.gestion_pedidos.agregar_notes_pedidos(:nota, :idbc, :idbp) ")
    void addNotesBill(@Bind("nota") String nota,
                      @Bind("idbc") Long idbc,
                      @Bind("idbp") Long idbp);

    @SqlCall(" call facturacion724.gestion_pedidos.filtro_pedidos_cliente(:idbill) ")
    void procedure2(@Bind("idbill") Long idbill);


    @SqlUpdate(" update facturacion724.bill set TOTALPRICE=:total where id_bill = :billid ")
    void updateBillTotal(@Bind("total") Long total,
                         @Bind("billid") Long billid);


    @SqlUpdate(" call facturacion724.actualizar_valores_bill(:billid) ")
    void updateBillTotalandSubtotal(@Bind("billid") Long billid);



    @SqlQuery(" Select sum(quantity) from tienda724.product_store_inventory where id_product_store = :idps ")
    Long getQuantity(@Bind("idps") Long idps);




    @SqlCall(" call facturacion724.gestion_pedidos.completar_pedidos_pendientes(:idbventa, :idbcompra, :idbremision, :idstorecliente, :idstoreproveedor, :idvehiculo, :idbpedidoproveedor) ")
    void procedureVe(
                    @Bind("idvehiculo") Long idvehiculo,
                    @Bind("idbventa") Long idbventa,
                    @Bind("idbcompra") Long idbcompra,
                    @Bind("idbremision") Long idbremision,
                    @Bind("idstorecliente") Long idstorecliente,
                    @Bind("idstoreproveedor") Long idstoreproveedor,
                    @Bind("idbpedidoproveedor") Long idbpedidoproveedor);

    @SqlCall(" call facturacion724.anular_bill_con_detail_cero(:idBill) ")
    void updateBillIf0(@Bind("idBill") Long idBill);

}
