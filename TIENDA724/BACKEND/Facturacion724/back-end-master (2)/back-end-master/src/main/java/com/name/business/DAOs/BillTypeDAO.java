package com.name.business.DAOs;


import com.name.business.entities.BillType;
import com.name.business.entities.CommonSimple;
import com.name.business.mappers.BillTypeMapper;
import com.name.business.mappers.CommonSimpleMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(BillTypeMapper.class)
public interface BillTypeDAO {

    @SqlQuery("SELECT ID_BILL_TYPE FROM BILL_TYPE WHERE ID_BILL_TYPE IN (SELECT MAX( ID_BILL_TYPE ) FROM BILL_TYPE )\n")
    Long getPkLast();

    @SqlQuery("SELECT COUNT(ID_BILL_TYPE) FROM BILL_TYPE WHERE ID_BILL_TYPE = :id\n")
    Integer getValidatorID(@Bind("id") Long id);


    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery(" SELECT ID_BILL_TYPE  ID, ID_COMMON_STATE FROM BILL_TYPE WHERE( ID_BILL_TYPE = :id OR :id IS NULL)\n")
    List<CommonSimple> readCommons(@Bind("id") Long id);

    @SqlQuery("SELECT * FROM V_BILLTYPE V_BIL_TY\n" +
            "WHERE (V_BIL_TY.ID_BILL_TYPE=:bil_ty.id_bill_type OR :bil_ty.id_bill_type IS NULL ) AND\n" +
            "      (V_BIL_TY.NAME_BIL_TY LIKE :bil_ty.name_bill_type OR :bil_ty.name_bill_type IS NULL ) AND\n" +
            "      (V_BIL_TY.ID_CM_BIL_TY=:bil_ty.id_state_bill_type OR :bil_ty.id_state_bill_type IS NULL ) AND\n" +
            "      (V_BIL_TY.STATE_CM_BIL_TY =:bil_ty.state_bill_type OR :bil_ty.state_bill_type IS NULL ) AND\n" +
            "      (V_BIL_TY.CREATION_BIL_TY =:bil_ty.creation_bill_type OR :bil_ty.creation_bill_type IS NULL ) AND\n" +
            "      (V_BIL_TY.UPDATE_BIL_TY= :bil_ty.update_bill_type OR :bil_ty.update_bill_type IS NULL )")
    List<BillType> read(@BindBean("bil_ty") BillType billType);


    @SqlUpdate("INSERT INTO BILL_TYPE ( ID_COMMON_STATE,NAME) \n" +
            "VALUES (:id_cm_st,:name_bil_ty) \n")
    void create(@Bind("id_cm_st") Long id_common_state,@Bind("name_bil_ty") String name_bil_ty);


    @SqlUpdate("UPDATE BILL_TYPE SET \n" +
            "    NAME = :name \n" +
            "    WHERE (ID_BILL_TYPE =  :id_bill_type)")
    int update(@Bind("id_bill_type")Long id_bill_state,@Bind("name") String name);


    int delete();


    int deletePermanent();
}
