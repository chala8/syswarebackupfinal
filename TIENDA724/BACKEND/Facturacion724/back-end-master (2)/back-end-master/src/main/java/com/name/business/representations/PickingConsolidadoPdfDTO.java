package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

public class PickingConsolidadoPdfDTO {
    private String logo;
    private Double costototal;
    private Double preciototal;
    private List<FabricanteDTO> list;

    @JsonCreator
    public PickingConsolidadoPdfDTO(@JsonProperty("logo")  String logo,
                                    @JsonProperty("costototal")  Double costoTotal,
                                    @JsonProperty("preciototal")  Double precioTotal,
                                    @JsonProperty("detalles")  List<FabricanteDTO> list) {
        this.logo = logo;
        this.costototal = costoTotal;
        this.preciototal = precioTotal;
        this.list = list;
    }
    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }


    public Double getcostoTotal() {
        return costototal;
    }

    public void setcostoTotal(Double costototal) {
        this.costototal = costototal;
    }


    public double getprecioTotal() {
        return preciototal;
    }

    public void setprecioTotal(Double preciototal) {
        this.preciototal = preciototal;
    }


    public List<FabricanteDTO> getlist() {
        return list;
    }

    public void setlist(List<FabricanteDTO> list) {
        this.list = list;
    }


}
