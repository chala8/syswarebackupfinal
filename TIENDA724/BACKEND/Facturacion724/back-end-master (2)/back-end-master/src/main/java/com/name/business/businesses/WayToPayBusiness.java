package com.name.business.businesses;

import com.name.business.DAOs.WayToPayDAO;
import com.name.business.entities.*;
import com.name.business.representations.BillTypeDTO;
import com.name.business.representations.CommonStateDTO;
import com.name.business.representations.WayToPayDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.billTypeSanitation;
import static com.name.business.sanitations.EntitySanitation.wayToPaySanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;


public class WayToPayBusiness {
    private WayToPayDAO wayToPayDAO;
    private CommonStateBusiness commonStateBusiness;

    public WayToPayBusiness(WayToPayDAO wayToPayDAO, CommonStateBusiness commonStateBusiness) {
        this.wayToPayDAO = wayToPayDAO;
        this.commonStateBusiness = commonStateBusiness;
    }


    public Either<IException, List<WayToPay>> getWayToPay(WayToPay wayToPay) {
        List<String> msn = new ArrayList<>();
        try {

            System.out.println("|||||||||||| Starting consults Way Pay ||||||||||||||||| ");
            return Either.right(wayToPayDAO.read(wayToPaySanitation(wayToPay)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     *
     * @param wayToPayDTO
     * @return
     */
    public Either<IException,Long> creatWayToPay(WayToPayDTO wayToPayDTO){
        List<String> msn = new ArrayList<>();
        Long id_way_pay = null;
        Long id_common = null;

        try {

            System.out.println("|||||||||||| Starting creation Way Pay ||||||||||||||||| ");
            if (wayToPayDTO!=null){

                if (wayToPayDTO.getStateDTO() == null) {
                    wayToPayDTO.setStateDTO(new CommonStateDTO(1,null,null));
                }
                Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(wayToPayDTO.getStateDTO());
                if (commonThirdEither.isRight()) {
                    id_common = commonThirdEither.right().value();
                }

                wayToPayDAO.create(id_common,formatoStringSql(wayToPayDTO.getName()));
                id_way_pay = wayToPayDAO.getPkLast();

                return Either.right(id_way_pay);
            }else{
                msn.add("It does not recognize Way Pay, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * @param id_way_pay
     * @param wayToPayDTO
     * @return
     */
    public Either<IException, Long> updateWayToPay(Long id_way_pay, WayToPayDTO wayToPayDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        PaymentState actualRegister=null;

        try {
            System.out.println("|||||||||||| Starting update Way Pay ||||||||||||||||| ");
            if ((id_way_pay != null && id_way_pay > 0)) {



                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorID(id_way_pay).equals(false)){
                    msn.add("It ID Way Pay  does not exist register on  Way Pay, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<WayToPay> read = wayToPayDAO.read(
                        new WayToPay(id_way_pay, null,
                                new CommonState(null,null,null,null))
                );

                if (read.size()<=0){
                    msn.add("It does not recognize ID Way Pay , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = wayToPayDAO.readCommons(formatoLongSql(id_way_pay));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);

                    // Common  State business update
                    commonStateBusiness.updateCommonState(idCommons.getId_common_state(), wayToPayDTO.getStateDTO());

                }

                if (wayToPayDTO.getName() == null || wayToPayDTO.getName().isEmpty()) {
                    wayToPayDTO.setName(actualRegister.getName_payment_state());
                } else {
                    wayToPayDTO.setName(formatoStringSql(wayToPayDTO.getName()));
                }



                // Attribute update
                wayToPayDAO.update(id_way_pay,wayToPayDTO.getName());

                return Either.right(id_way_pay);

            } else {
                msn.add("It does not recognize ID Way Pay, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id_way_pay
     * @return
     */
    public Either<IException, Long> deleteWayToPay(Long id_way_pay) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");

            System.out.println("|||||||||||| Starting delete Way Pay ||||||||||||||||| ");

            if (id_way_pay != null && id_way_pay > 0) {

                List<CommonSimple> readCommons = wayToPayDAO.readCommons(formatoLongSql(id_way_pay));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());

                return Either.right(id_way_pay);
            } else {
                msn.add("It does not recognize ID Way Pay, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }




    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = wayToPayDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }
}
