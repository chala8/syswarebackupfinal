package com.name.business.businesses;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.name.business.DAOs.*;
import com.name.business.entities.*;
import com.name.business.representations.BillDTO;
import com.name.business.representations.BillDetailIDDTO;
import com.name.business.representations.BillPdfDTO;
import com.name.business.representations.CommonStateDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.text.NumberFormat;

import static com.name.business.sanitations.EntitySanitation.billCompleteSanitation;
import static com.name.business.sanitations.EntitySanitation.billSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;
import static com.name.business.utils.security.CodeGenerator.generatorConsecutive;
import static java.lang.Math.round;

public class BillBusiness {
    private BillDAO billDAO;
    private CommonStateBusiness commonStateBusiness;
    private PaymentStateBusiness paymentStateBusiness;
    private BillStateBusiness billStateBusiness;
    private BillTypeBusiness billTypeBusiness;
    private DetailBillBusiness detailBillBusiness;
    private PaymentMethodBusiness paymentMethodBusiness;
    private DetailPaymentBillBusiness detailPaymentBillBusiness;

    public BillBusiness(BillDAO billDAO, CommonStateBusiness commonStateBusiness, PaymentStateBusiness paymentStateBusiness,
                        BillStateBusiness billStateBusiness, BillTypeBusiness billTypeBusiness,
                        DetailBillBusiness detailBillBusiness, PaymentMethodBusiness paymentMethodBusiness,
                        DetailPaymentBillBusiness detailPaymentBillBusiness) {
        this.billDAO = billDAO;
        this.commonStateBusiness = commonStateBusiness;
        this.paymentStateBusiness = paymentStateBusiness;
        this.billStateBusiness = billStateBusiness;
        this.billTypeBusiness = billTypeBusiness;
        this.detailBillBusiness = detailBillBusiness;
        this.paymentMethodBusiness = paymentMethodBusiness;
        this.detailPaymentBillBusiness = detailPaymentBillBusiness;
    }






    public Either<IException, Double> getSalesCaja(Long id_cierre_caja) {
        try {
            return Either.right(billDAO.getSalesCaja(id_cierre_caja));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Long procedure2(Long idbill)
    {

        // Check if the word is present in string
        // If found, remove it using removeAll()


        billDAO.procedure2(idbill);


        // Return the resultant string
        return new Long(1);
    }








    /** @TODO for get Bill create json as
     * header{
     *     bill entity
     * }body{
     *
     *     array detail bill entity
     * }
     * for get Bill
     *
     */
    public static String removeWord(String string, String word)
    {

        // Check if the word is present in string
        // If found, remove it using removeAll()
        if (string.contains(word)) {

            // To cover the case
            // if the word is at the
            // beginning of the string
            // or anywhere in the middle
            String tempWord = word + "";
            string = string.replaceAll(tempWord, "");

            // To cover the edge case
            // if the word is at the
            // end of the string
            tempWord = "" + word;
            string = string.replaceAll(tempWord, "");
        }

        // Return the resultant string
        return string;
    }



    public Either<IException, String> printPDF2(BillPdfDTO billpdfDTO){
        // Se crea el documento
        Rectangle envelope = new Rectangle(137, 598);
        Document documento = new Document(envelope, 0f, 0f, 10f, 0f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.


            String empresa = billpdfDTO.getempresa();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 6, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 5, Font.NORMAL);
            Font fontH3 = new Font(HELVETICA, 7, Font.BOLD);
            Font fontH4 = new Font(HELVETICA, 7, Font.NORMAL);
            empresa = empresa.replaceAll(" ","_");
            String nombrePdf = empresa+"_"+billpdfDTO.getconsecutivo().replace(" ","_").replaceAll("_-_","-")+".pdf";
            FileOutputStream ficheroPdf = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/facturas/"+ nombrePdf);
            //FileOutputStream ficheroPdf = new FileOutputStream("./"+ nombrePdf);

            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));






            PdfPTable tablaDatos = new PdfPTable(1);
            PdfPCell cell1 =new PdfPCell(new Phrase(billpdfDTO.getempresa(),fontH3));
            PdfPCell nit =new PdfPCell(new Phrase(billpdfDTO.getnit(),fontH3));
            PdfPCell direccion = new PdfPCell(new Phrase(billpdfDTO.getdireccion(),fontH3));
            PdfPCell telefono = new PdfPCell(new Phrase ("Tel: "+billpdfDTO.gettelefono(),fontH3));
            PdfPCell cell2 =new PdfPCell(new Phrase("Factura # "+billpdfDTO.getconsecutivo().replaceAll(" - ",""),fontH4));
            PdfPCell cella =new PdfPCell(new Phrase("  ",fontH4));
            PdfPCell regimen =new PdfPCell(new Phrase(billpdfDTO.getregimen(),fontH4));
            PdfPCell nombreCajero =new PdfPCell(new Phrase("Cajero: "+removeWord(billpdfDTO.getnombreCajero(),"null"),fontH4));
            PdfPCell cliente =new PdfPCell(new Phrase("Cliente: "+removeWord(billpdfDTO.getcliente(),"null"),fontH4));
            PdfPCell documentoCC = new PdfPCell(new Phrase(billpdfDTO.getcedula(),fontH4));

            documentoCC.setHorizontalAlignment(Element.ALIGN_CENTER);
            documentoCC.setBorder(PdfPCell.NO_BORDER);
            cliente.setHorizontalAlignment(Element.ALIGN_CENTER);
            cliente.setBorder(PdfPCell.NO_BORDER);
            direccion.setBorder(PdfPCell.NO_BORDER);
            telefono.setBorder(PdfPCell.NO_BORDER);
            direccion.setHorizontalAlignment(Element.ALIGN_CENTER);
            telefono.setHorizontalAlignment(Element.ALIGN_CENTER);
            telefono.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBorder(PdfPCell.NO_BORDER);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cella.setBorder(PdfPCell.NO_BORDER);
            nit.setHorizontalAlignment(Element.ALIGN_CENTER);
            nit.setBorder(PdfPCell.NO_BORDER);
            regimen.setHorizontalAlignment(Element.ALIGN_CENTER);
            regimen.setBorder(PdfPCell.NO_BORDER);
            nombreCajero.setHorizontalAlignment(Element.ALIGN_CENTER);
            nombreCajero.setBorder(PdfPCell.NO_BORDER);


            try {
            Image img = Image.getInstance("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/logos/"+billpdfDTO.getnit()+".jpg");
            //Image img = Image.getInstance("./"+billpdfDTO.getnit()+".jpg");
            PdfPCell imagen = new PdfPCell(img, true);
            imagen.setBorder(PdfPCell.NO_BORDER);
            imagen.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDatos.addCell(imagen);
            }catch(Exception e){
                System.out.println(e.toString());
            }


            tablaDatos.addCell(cell1);
            tablaDatos.addCell(nit);
            tablaDatos.addCell(direccion);
            tablaDatos.addCell(telefono);
            tablaDatos.addCell(cella);

            PdfPTable tablaData = new PdfPTable(2);

            //tablaData.addCell(direccion);
            //tablaData.addCell(telefono);
            //tablaData.addCell(cell2);
            //tablaData.addCell(nombreCajero);
            tablaData.addCell(cliente);
            tablaData.addCell(documentoCC);
            PdfPCell direCl =new PdfPCell(new Phrase(billpdfDTO.getdireccionC(),fontH4));
            PdfPCell telCl =new PdfPCell(new Phrase("Tel: "+billpdfDTO.gettelefonoC(),fontH4));
            direCl.setHorizontalAlignment(Element.ALIGN_CENTER);
            direCl.setBorder(PdfPCell.NO_BORDER);
            telCl.setHorizontalAlignment(Element.ALIGN_CENTER);
            telCl.setBorder(PdfPCell.NO_BORDER);
            tablaData.addCell(direCl);
            tablaData.addCell(telCl);
            if(billpdfDTO.getcliente().equals("Cliente Ocasional") ){
                tablaData = new PdfPTable(1);
                cliente.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaData.addCell(cliente);
            }
            PdfPTable tablaFecha = new PdfPTable(1);
            PdfPCell cell3 =new PdfPCell(new Phrase(billpdfDTO.getfecha(),fontH4));
            cell3.setBorder(PdfPCell.NO_BORDER);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaFecha.addCell(cella);
            tablaFecha.addCell(cell3);
            tablaFecha.addCell(cell2);
            tablaFecha.addCell(nombreCajero);
            tablaFecha.addCell(cella);

            PdfPTable tablaDatos2 = new PdfPTable(2);


            PdfPCell cell5 =new PdfPCell(new Phrase("Tienda: "+billpdfDTO.gettienda(),fontH4));



            cell5.setBorder(PdfPCell.NO_BORDER);
            cell5.setHorizontalAlignment(Element.ALIGN_CENTER);


            tablaDatos2.addCell(cell5);

            if(billpdfDTO.getcaja().equals("-1")){
                PdfPCell cell4 =new PdfPCell(new Phrase("PEDIDO",fontH4));
                cell4.setBorder(PdfPCell.NO_BORDER);
                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos2.addCell(cell4);

            }else{
                PdfPCell cell4 =new PdfPCell(new Phrase("Caja: "+billpdfDTO.getcaja(),fontH4));
                cell4.setBorder(PdfPCell.NO_BORDER);
                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos2.addCell(cell4);
            }
            tablaDatos2.addCell(cella);
            tablaDatos2.addCell(cella);


            PdfPTable tablaDetalles = new PdfPTable(3);
            PdfPTable tablaDetalles2 = new PdfPTable(2);

            PdfPCell cell6 =new PdfPCell(new Phrase("Prod",fontH4));
            PdfPCell cell7 =new PdfPCell(new Phrase("Cant",fontH4));
            PdfPCell cell8 =new PdfPCell(new Phrase("Precio",fontH4));

            cell6.setBorder(PdfPCell.NO_BORDER);
            cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell7.setBorder(PdfPCell.NO_BORDER);
            cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell8.setBorder(PdfPCell.NO_BORDER);
            cell8.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDetalles2.addCell(cell7);
            tablaDetalles2.addCell(cell8);
            PdfPCell table = new PdfPCell(tablaDetalles2);
            table.setBorder(PdfPCell.NO_BORDER);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetalles.addCell(cell7);
            tablaDetalles.addCell(cell6);
            tablaDetalles.addCell(cell8);

            PdfPCell saltoLinea = new PdfPCell(new Phrase("    "));

            List<List<String>> theList = billpdfDTO.getdetalles();

            for (int i = 0; i < billpdfDTO.getdetalles().size(); i++) {
                PdfPCell cell9 = new PdfPCell();
                if(theList.get(i).get(1).length()<=23) {
                    cell9 = new PdfPCell(new Phrase(theList.get(i).get(1), fontH2));
                }else{
                    cell9 = new PdfPCell(new Phrase(theList.get(i).get(1).substring(0, 23), fontH2));
                }


                cell9.setBorder(PdfPCell.NO_BORDER);
                cell9.setHorizontalAlignment(Element.ALIGN_LEFT);

                PdfPTable tablaDetalles3 = new PdfPTable(3);
                PdfPCell cell10 = new PdfPCell(new Phrase(theList.get(i).get(0), fontH1));
                int number = new Integer(theList.get(i).get(2));
                PdfPCell cell11 = new PdfPCell(new Phrase(format.format(number), fontH1));
                cell10.setBorder(PdfPCell.NO_BORDER);
                cell10.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell11.setBorder(PdfPCell.NO_BORDER);
                cell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
                saltoLinea.setBorder(PdfPCell.NO_BORDER);
                tablaDetalles.addCell(cell10);
                tablaDetalles.addCell(cell9);
                tablaDetalles.addCell(cell11);
                PdfPCell tablas = new PdfPCell(tablaDetalles3);
                tablas.setBorder(PdfPCell.NO_BORDER);
                tablas.setHorizontalAlignment(Element.ALIGN_CENTER);
                //tablaDetalles.addCell(tablas);
            }

            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            PdfPTable tablaTotales = new PdfPTable(2);
            PdfPCell cell12 = new PdfPCell(new Phrase("Subtotal",fontH4));
            PdfPCell cell13 = new PdfPCell(new Phrase(format.format(billpdfDTO.getsubtotal())+"",fontH4));
            PdfPCell cellPago = new PdfPCell(new Phrase("Cantidad Recibida",fontH4));
            PdfPCell pago = new PdfPCell(new Phrase(format.format((billpdfDTO.getcambio()+round(billpdfDTO.gettotalprice())))+"",fontH4));
            PdfPCell cell14 = new PdfPCell(new Phrase("IVA",fontH4));
            PdfPCell cell15 = new PdfPCell(new Phrase(format.format(billpdfDTO.gettax())+"",fontH4));
            PdfPCell cellcambio = new PdfPCell(new Phrase("Cambio",fontH4));
            PdfPCell celcambio2 = new PdfPCell(new Phrase(format.format(billpdfDTO.getcambio())+"",fontH4));
            PdfPCell cell16 = new PdfPCell(new Phrase("Total",fontH4));
            PdfPCell cell17 = new PdfPCell(new Phrase(format.format(round(billpdfDTO.gettotalprice()))+"",fontH4));
            PdfPCell cell16medio = new PdfPCell(new Phrase("Medio pago",fontH4));
            PdfPCell cell17medio = new PdfPCell(new Phrase(billpdfDTO.getmedioPago()+"",fontH4));
            cell12.setBorder(PdfPCell.NO_BORDER);
            cell13.setBorder(PdfPCell.NO_BORDER);
            cell14.setBorder(PdfPCell.NO_BORDER);
            cell15.setBorder(PdfPCell.NO_BORDER);
            cell16.setBorder(PdfPCell.NO_BORDER);
            cell17.setBorder(PdfPCell.NO_BORDER);
            cellcambio.setBorder(PdfPCell.NO_BORDER);
            celcambio2.setBorder(PdfPCell.NO_BORDER);
            cell16medio.setBorder(PdfPCell.NO_BORDER);
            cell17medio.setBorder(PdfPCell.NO_BORDER);
            cellPago.setBorder(PdfPCell.NO_BORDER);
            pago.setBorder(PdfPCell.NO_BORDER);
            cellPago.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pago.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell13.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell14.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell15.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell16.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell17.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cellcambio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celcambio2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell16medio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell17medio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaTotales.addCell(cell12);
            tablaTotales.addCell(cell13);
            tablaTotales.addCell(cell14);
            tablaTotales.addCell(cell15);
            tablaTotales.addCell(cell16);
            tablaTotales.addCell(cell17);
            tablaTotales.addCell(cellPago);
            tablaTotales.addCell(pago);
            tablaTotales.addCell(cellcambio);
            tablaTotales.addCell(celcambio2);
            tablaTotales.addCell(cell16medio);
            tablaTotales.addCell(cell17medio);



            PdfPTable tablaLegal = new PdfPTable(1);
            tablaLegal.addCell(cella);
            if(billpdfDTO.getregimenT()!= null && !billpdfDTO.getregimenT().isEmpty()){
                String regimenTemp;
                if(billpdfDTO.getregimenT().equals("C")){
                    regimenTemp = "Común";
                }else{
                    regimenTemp = "Simplificado";
                }
                PdfPCell cellRegimen = new PdfPCell(new Phrase("Régimen "+regimenTemp,fontH4));
                cellRegimen.setBorder(PdfPCell.NO_BORDER);
                cellRegimen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellRegimen);
            }

            if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                PdfPCell cellAuth = new PdfPCell(new Phrase("Resolución Dian # "+billpdfDTO.getresolucion_DIAN(),fontH4));
                cellAuth.setBorder(PdfPCell.NO_BORDER);
                cellAuth.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellAuth);
            }

           /* if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                PdfPCell cellDatos = new PdfPCell(new Phrase("Fecha Formalización: 13 de junio de 2019",fontH2));
                cellDatos.setBorder(PdfPCell.NO_BORDER);
                cellDatos.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellDatos);
            }    */

            PdfPCell cellPos = new PdfPCell(new Phrase("Modalidad: POS",fontH4));
            cellPos.setBorder(PdfPCell.NO_BORDER);
            cellPos.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaLegal.addCell(cellPos);

            //if(billpdfDTO.getinitial_RANGE()!= null && !billpdfDTO.getinitial_RANGE().isEmpty() &&
            //        billpdfDTO.getfinal_RANGE()!= null && !billpdfDTO.getfinal_RANGE().isEmpty() &&
            //       billpdfDTO.getprefix()!= null && !billpdfDTO.getprefix().isEmpty()){
            //   PdfPCell cellAutor = new PdfPCell(new Phrase("Autorización: Desde "+billpdfDTO.getprefix()+billpdfDTO.getinitial_RANGE()+" a "+billpdfDTO.getprefix()+billpdfDTO.getfinal_RANGE(),fontH4));
            //   cellAutor.setBorder(PdfPCell.NO_BORDER);
            //   cellAutor.setHorizontalAlignment(Element.ALIGN_CENTER);
            //   tablaLegal.addCell(cellAutor);
            //}



            tablaLegal.addCell(cella);

            PdfPTable tabla = new PdfPTable(1);


            tablaDatos.setKeepTogether(false);
            tablaData.setKeepTogether(false);
            tablaFecha.setKeepTogether(false);
            tablaDatos2.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaTotales.setKeepTogether(false);
            tablaLegal.setKeepTogether(false);

            tablaDatos.setSplitLate(false);
            tablaData.setSplitLate(false);
            tablaFecha.setSplitLate(false);
            tablaDatos2.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaTotales.setSplitLate(false);

            tablaLegal.setSplitLate(false);



            PdfPCell tablaDatosC = new PdfPCell(tablaDatos);
            PdfPCell tablaDataC = new PdfPCell(tablaData);
            PdfPCell tablaFechaC = new PdfPCell(tablaFecha);
            PdfPCell tablaDatos2C = new PdfPCell(tablaDatos2);
            PdfPCell tablaDetallesC = new PdfPCell(tablaDetalles);
            PdfPCell tablaDetalles2C = new PdfPCell(tablaDetalles2);
            PdfPCell tablaTotalesC = new PdfPCell(tablaTotales);
            PdfPCell tablaLegalC = new PdfPCell(tablaLegal);





            tablaDatosC.setBorder(PdfPCell.NO_BORDER);
            tablaDataC.setBorder(PdfPCell.NO_BORDER);
            tablaFechaC.setBorder(PdfPCell.NO_BORDER);
            tablaDatos2C.setBorder(PdfPCell.NO_BORDER);
            tablaDetallesC.setBorder(PdfPCell.NO_BORDER);
            tablaDetalles2C.setBorder(PdfPCell.NO_BORDER);
            tablaTotalesC.setBorder(PdfPCell.NO_BORDER);
            tablaLegalC.setBorder(PdfPCell.NO_BORDER);





            tabla.addCell(tablaDatosC);
            tabla.addCell(tablaDataC);
            tabla.addCell(tablaFechaC);
            tabla.addCell(tablaDatos2C);
            tabla.addCell(tablaDetallesC);
            //tabla.addCell(tablaDetalles2C);
            tabla.addCell(tablaTotalesC);
            tabla.addCell(tablaLegalC);

            tabla.setKeepTogether(false);
            tabla.setSplitLate(false);

            documento.add(tabla);
            response = nombrePdf;

        }catch(Exception e){
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> printPDF4(BillPdfDTO billpdfDTO){
        // Se crea el documento
        Rectangle envelope = new Rectangle(596, 842);
        Document documento = new Document(envelope, 0f, 0f, 10f, 0f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.


            String empresa = billpdfDTO.getempresa();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 12, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 10, Font.NORMAL);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);
            Font fontH4 = new Font(HELVETICA, 14, Font.NORMAL);
            empresa = empresa.replaceAll(" ","_");
            String nombrePdf = empresa+"_"+billpdfDTO.getconsecutivo().replace(" ","_").replaceAll("_-_","-")+".pdf";
            FileOutputStream ficheroPdf = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/facturas/"+ nombrePdf);
            //FileOutputStream ficheroPdf = new FileOutputStream("./"+ nombrePdf);

            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));






            PdfPTable tablaDatos = new PdfPTable(1);
            PdfPCell cell1 =new PdfPCell(new Phrase(billpdfDTO.getempresa(),fontH3));
            PdfPCell nit =new PdfPCell(new Phrase(billpdfDTO.getnit(),fontH3));
            PdfPCell direccion = new PdfPCell(new Phrase(billpdfDTO.getdireccion(),fontH3));
            PdfPCell telefono = new PdfPCell(new Phrase ("Tel: "+billpdfDTO.gettelefono(),fontH3));
            PdfPCell cell2 =new PdfPCell(new Phrase("Factura de Venta # "+billpdfDTO.getconsecutivo().replaceAll(" - ",""),fontH4));
            PdfPCell cella =new PdfPCell(new Phrase("  ",fontH4));
            PdfPCell regimen =new PdfPCell(new Phrase(billpdfDTO.getregimen(),fontH4));
            PdfPCell nombreCajero =new PdfPCell(new Phrase("Cajero: "+removeWord(billpdfDTO.getnombreCajero(),"null"),fontH4));
            PdfPCell cliente =new PdfPCell(new Phrase(billpdfDTO.getcliente().replace("null",""),fontH4));
            PdfPCell documentoCC = new PdfPCell(new Phrase(billpdfDTO.getcedula(),fontH4));
System.out.println(billpdfDTO.getcliente());
            documentoCC.setHorizontalAlignment(Element.ALIGN_CENTER);
            documentoCC.setBorder(PdfPCell.NO_BORDER);
            cliente.setHorizontalAlignment(Element.ALIGN_CENTER);
            cliente.setBorder(PdfPCell.NO_BORDER);
            direccion.setBorder(PdfPCell.NO_BORDER);
            telefono.setBorder(PdfPCell.NO_BORDER);
            direccion.setHorizontalAlignment(Element.ALIGN_CENTER);
            telefono.setHorizontalAlignment(Element.ALIGN_CENTER);
            telefono.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBorder(PdfPCell.NO_BORDER);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cella.setBorder(PdfPCell.NO_BORDER);
            nit.setHorizontalAlignment(Element.ALIGN_CENTER);
            nit.setBorder(PdfPCell.NO_BORDER);
            regimen.setHorizontalAlignment(Element.ALIGN_CENTER);
            regimen.setBorder(PdfPCell.NO_BORDER);
            nombreCajero.setHorizontalAlignment(Element.ALIGN_CENTER);
            nombreCajero.setBorder(PdfPCell.NO_BORDER);


            try {
                Image img = Image.getInstance("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/logos/"+billpdfDTO.getnit()+".jpg");
                //Image img = Image.getInstance("./"+billpdfDTO.getnit()+".jpg");
                PdfPCell imagen = new PdfPCell(img, true);
                imagen.setBorder(PdfPCell.NO_BORDER);
                imagen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos.addCell(imagen);
            }catch(Exception e){
                System.out.println(e.toString());
            }


            tablaDatos.addCell(cell1);
            tablaDatos.addCell(nit);
            tablaDatos.addCell(direccion);
            tablaDatos.addCell(telefono);
            tablaDatos.addCell(cella);

            PdfPTable tablaData = new PdfPTable(1);

            //tablaData.addCell(direccion);
            //tablaData.addCell(telefono);
            //tablaData.addCell(cell2);
            //tablaData.addCell(nombreCajero);
            tablaData.addCell(cliente);
            System.out.println("THIS I NEED" + billpdfDTO.getcliente());
            //tablaData.addCell(documentoCC);
            PdfPCell direCl =new PdfPCell(new Phrase(billpdfDTO.getdireccionC(),fontH4));
            PdfPCell telCl =new PdfPCell(new Phrase("Tel: "+billpdfDTO.gettelefonoC(),fontH4));
            direCl.setHorizontalAlignment(Element.ALIGN_CENTER);
            direCl.setBorder(PdfPCell.NO_BORDER);
            telCl.setHorizontalAlignment(Element.ALIGN_CENTER);
            telCl.setBorder(PdfPCell.NO_BORDER);
            //tablaData.addCell(direCl);
            //tablaData.addCell(telCl);

            PdfPTable tablaFecha = new PdfPTable(1);
            PdfPCell cell3 =new PdfPCell(new Phrase(billpdfDTO.getfecha(),fontH4));
            cell3.setBorder(PdfPCell.NO_BORDER);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaFecha.addCell(cella);
            tablaFecha.addCell(cell3);
            tablaFecha.addCell(cell2);
            tablaFecha.addCell(nombreCajero);
            tablaFecha.addCell(cella);

            PdfPTable tablaDatos2 = new PdfPTable(2);


            PdfPCell cell5 =new PdfPCell(new Phrase("Tienda: "+billpdfDTO.gettienda(),fontH4));



            cell5.setBorder(PdfPCell.NO_BORDER);
            cell5.setHorizontalAlignment(Element.ALIGN_CENTER);


            tablaDatos2.addCell(cell5);

            if(billpdfDTO.getcaja().equals("-1")){
                PdfPCell cell4 =new PdfPCell(new Phrase("PEDIDO",fontH4));
                cell4.setBorder(PdfPCell.NO_BORDER);
                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos2.addCell(cell4);

            }else{
                PdfPCell cell4 =new PdfPCell(new Phrase("Caja: "+billpdfDTO.getcaja(),fontH4));
                cell4.setBorder(PdfPCell.NO_BORDER);
                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos2.addCell(cell4);
            }
            tablaDatos2.addCell(cella);
            tablaDatos2.addCell(cella);


            PdfPTable tablaDetalles = new PdfPTable(3);
            PdfPTable tablaDetalles2 = new PdfPTable(2);

            PdfPCell cell6 =new PdfPCell(new Phrase("Prod",fontH4));
            PdfPCell cell7 =new PdfPCell(new Phrase("Cant",fontH4));
            PdfPCell cell8 =new PdfPCell(new Phrase("Precio",fontH4));

            cell6.setBorder(PdfPCell.NO_BORDER);
            cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell7.setBorder(PdfPCell.NO_BORDER);
            cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell8.setBorder(PdfPCell.NO_BORDER);
            cell8.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDetalles2.addCell(cell7);
            tablaDetalles2.addCell(cell8);
            PdfPCell table = new PdfPCell(tablaDetalles2);
            table.setBorder(PdfPCell.NO_BORDER);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetalles.addCell(cell7);
            tablaDetalles.addCell(cell6);
            tablaDetalles.addCell(cell8);

            PdfPCell saltoLinea = new PdfPCell(new Phrase("    "));

            List<List<String>> theList = billpdfDTO.getdetalles();

            for (int i = 0; i < billpdfDTO.getdetalles().size(); i++) {
                PdfPCell cell9 = new PdfPCell();
                if(theList.get(i).get(1).length()<=23) {
                    cell9 = new PdfPCell(new Phrase(theList.get(i).get(1), fontH2));
                }else{
                    cell9 = new PdfPCell(new Phrase(theList.get(i).get(1).substring(0, 23), fontH2));
                }


                cell9.setBorder(PdfPCell.NO_BORDER);
                cell9.setHorizontalAlignment(Element.ALIGN_LEFT);

                PdfPTable tablaDetalles3 = new PdfPTable(3);
                PdfPCell cell10 = new PdfPCell(new Phrase(theList.get(i).get(0), fontH1));
                int number = new Integer(theList.get(i).get(2));
                PdfPCell cell11 = new PdfPCell(new Phrase(format.format(number), fontH1));
                cell10.setBorder(PdfPCell.NO_BORDER);
                cell10.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell11.setBorder(PdfPCell.NO_BORDER);
                cell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
                saltoLinea.setBorder(PdfPCell.NO_BORDER);
                tablaDetalles.addCell(cell10);
                tablaDetalles.addCell(cell9);
                tablaDetalles.addCell(cell11);
                PdfPCell tablas = new PdfPCell(tablaDetalles3);
                tablas.setBorder(PdfPCell.NO_BORDER);
                tablas.setHorizontalAlignment(Element.ALIGN_CENTER);
                //tablaDetalles.addCell(tablas);
            }


            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            PdfPTable tablaTotales = new PdfPTable(2);
            PdfPCell cell12 = new PdfPCell(new Phrase("Subtotal",fontH4));
            PdfPCell cell13 = new PdfPCell(new Phrase(format.format(billpdfDTO.getsubtotal())+"",fontH4));
            PdfPCell cellPago = new PdfPCell(new Phrase("Cantidad Recibida",fontH4));
            PdfPCell pago = new PdfPCell(new Phrase(format.format((billpdfDTO.getcambio()+round(billpdfDTO.gettotalprice())))+"",fontH4));
            PdfPCell cell14 = new PdfPCell(new Phrase("IVA",fontH4));
            PdfPCell cell15 = new PdfPCell(new Phrase(format.format(billpdfDTO.gettax())+"",fontH4));
            PdfPCell cellcambio = new PdfPCell(new Phrase("Cambio",fontH4));
            PdfPCell celcambio2 = new PdfPCell(new Phrase(format.format(billpdfDTO.getcambio())+"",fontH4));
            PdfPCell cell16 = new PdfPCell(new Phrase("Total",fontH4));
            PdfPCell cell17 = new PdfPCell(new Phrase(format.format(round(billpdfDTO.gettotalprice()))+"",fontH4));
            PdfPCell cell16medio = new PdfPCell(new Phrase("Medio pago",fontH4));
            PdfPCell cell17medio = new PdfPCell(new Phrase(billpdfDTO.getmedioPago()+"",fontH4));
            cell12.setBorder(PdfPCell.NO_BORDER);
            cell13.setBorder(PdfPCell.NO_BORDER);
            cell14.setBorder(PdfPCell.NO_BORDER);
            cell15.setBorder(PdfPCell.NO_BORDER);
            cell16.setBorder(PdfPCell.NO_BORDER);
            cell17.setBorder(PdfPCell.NO_BORDER);
            cellcambio.setBorder(PdfPCell.NO_BORDER);
            celcambio2.setBorder(PdfPCell.NO_BORDER);
            cell16medio.setBorder(PdfPCell.NO_BORDER);
            cell17medio.setBorder(PdfPCell.NO_BORDER);
            cellPago.setBorder(PdfPCell.NO_BORDER);
            pago.setBorder(PdfPCell.NO_BORDER);
            cellPago.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pago.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell13.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell14.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell15.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell16.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell17.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cellcambio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celcambio2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell16medio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell17medio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaTotales.addCell(cell12);
            tablaTotales.addCell(cell13);
            tablaTotales.addCell(cell14);
            tablaTotales.addCell(cell15);
            tablaTotales.addCell(cell16);
            tablaTotales.addCell(cell17);
            tablaTotales.addCell(cellPago);
            tablaTotales.addCell(pago);
            tablaTotales.addCell(cellcambio);
            tablaTotales.addCell(celcambio2);
            tablaTotales.addCell(cell16medio);
            tablaTotales.addCell(cell17medio);



            PdfPTable tablaLegal = new PdfPTable(1);
            tablaLegal.addCell(cella);
            if(billpdfDTO.getregimenT()!= null && !billpdfDTO.getregimenT().isEmpty()){
                String regimenTemp;
                if(billpdfDTO.getregimenT().equals("C")){
                    regimenTemp = "Común";
                }else{
                    regimenTemp = "Simplificado";
                }
                PdfPCell cellRegimen = new PdfPCell(new Phrase("Régimen "+regimenTemp,fontH4));
                cellRegimen.setBorder(PdfPCell.NO_BORDER);
                cellRegimen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellRegimen);
            }

            if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                PdfPCell cellAuth = new PdfPCell(new Phrase("Resolución Dian # "+billpdfDTO.getresolucion_DIAN(),fontH4));
                cellAuth.setBorder(PdfPCell.NO_BORDER);
                cellAuth.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellAuth);
            }

           /* if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                PdfPCell cellDatos = new PdfPCell(new Phrase("Fecha Formalización: 13 de junio de 2019",fontH2));
                cellDatos.setBorder(PdfPCell.NO_BORDER);
                cellDatos.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellDatos);
            }    */

            PdfPCell cellPos = new PdfPCell(new Phrase("Modalidad: POS",fontH4));
            cellPos.setBorder(PdfPCell.NO_BORDER);
            cellPos.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaLegal.addCell(cellPos);

            //if(billpdfDTO.getinitial_RANGE()!= null && !billpdfDTO.getinitial_RANGE().isEmpty() &&
            //        billpdfDTO.getfinal_RANGE()!= null && !billpdfDTO.getfinal_RANGE().isEmpty() &&
            //        billpdfDTO.getprefix()!= null && !billpdfDTO.getprefix().isEmpty()){
            //    PdfPCell cellAutor = new PdfPCell(new Phrase("Autorización: Desde "+billpdfDTO.getprefix()+billpdfDTO.getinitial_RANGE()+" a "+billpdfDTO.getprefix()+billpdfDTO.getfinal_RANGE(),fontH4));
            //    cellAutor.setBorder(PdfPCell.NO_BORDER);
            //    cellAutor.setHorizontalAlignment(Element.ALIGN_CENTER);
            //    tablaLegal.addCell(cellAutor);
            //}



            tablaLegal.addCell(cella);

            PdfPTable tabla = new PdfPTable(1);


            tablaDatos.setKeepTogether(false);
            tablaData.setKeepTogether(false);
            tablaFecha.setKeepTogether(false);
            tablaDatos2.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaTotales.setKeepTogether(false);
            tablaLegal.setKeepTogether(false);

            tablaDatos.setSplitLate(false);
            tablaData.setSplitLate(false);
            tablaFecha.setSplitLate(false);
            tablaDatos2.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaLegal.setSplitLate(false);
            tablaTotales.setSplitLate(false);



            PdfPCell tablaDatosC = new PdfPCell(tablaDatos);
            PdfPCell tablaDataC = new PdfPCell(tablaData);
            PdfPCell tablaFechaC = new PdfPCell(tablaFecha);
            PdfPCell tablaDatos2C = new PdfPCell(tablaDatos2);
            PdfPCell tablaDetallesC = new PdfPCell(tablaDetalles);
            PdfPCell tablaDetalles2C = new PdfPCell(tablaDetalles2);
            PdfPCell tablaLegalC = new PdfPCell(tablaLegal);
            PdfPCell tablaTotalesC = new PdfPCell(tablaTotales);




            tablaTotalesC.setBorder(PdfPCell.NO_BORDER);
            tablaDatosC.setBorder(PdfPCell.NO_BORDER);
            tablaDataC.setBorder(PdfPCell.NO_BORDER);
            tablaFechaC.setBorder(PdfPCell.NO_BORDER);
            tablaDatos2C.setBorder(PdfPCell.NO_BORDER);
            tablaDetallesC.setBorder(PdfPCell.NO_BORDER);
            tablaDetalles2C.setBorder(PdfPCell.NO_BORDER);
            tablaLegalC.setBorder(PdfPCell.NO_BORDER);





            tabla.addCell(tablaDatosC);
            tabla.addCell(tablaDataC);
            tabla.addCell(tablaFechaC);
            tabla.addCell(tablaDatos2C);
            tabla.addCell(tablaDetallesC);
            //tabla.addCell(tablaDetalles2C);
            tabla.addCell(tablaTotalesC);
            tabla.addCell(tablaLegalC);

            tabla.setKeepTogether(false);
            tabla.setSplitLate(false);

            documento.add(tabla);
            response = nombrePdf;

        }catch(Exception e){
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> printPDF3(BillPdfDTO billpdfDTO){
        // Se crea el documento
        Rectangle envelope = new Rectangle(596, 842);
        Document documento = new Document(envelope, 0f, 0f, 10f, 0f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.


            String empresa = billpdfDTO.getempresa();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 12, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 10, Font.NORMAL);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);
            Font fontH4 = new Font(HELVETICA, 14, Font.NORMAL);
            empresa = empresa.replaceAll(" ","_");
            String nombrePdf = empresa+"_"+billpdfDTO.getconsecutivo().replace(" ","_").replaceAll("_-_","-")+"SALIDA.pdf";
            FileOutputStream ficheroPdf = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/facturas/"+ nombrePdf);
            //FileOutputStream ficheroPdf = new FileOutputStream("./"+ nombrePdf);

            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));






            PdfPTable tablaDatos = new PdfPTable(1);
            PdfPCell cell1 =new PdfPCell(new Phrase(billpdfDTO.getempresa(),fontH3));
            PdfPCell nit =new PdfPCell(new Phrase(billpdfDTO.getnit(),fontH3));
            PdfPCell direccion = new PdfPCell(new Phrase(billpdfDTO.getdireccion(),fontH3));
            PdfPCell telefono = new PdfPCell(new Phrase ("Tel: "+billpdfDTO.gettelefono(),fontH3));
            PdfPCell cell2 =new PdfPCell(new Phrase("Salida # "+billpdfDTO.getconsecutivo().replaceAll(" - ",""),fontH4));
            PdfPCell cella =new PdfPCell(new Phrase("  ",fontH4));
            PdfPCell regimen =new PdfPCell(new Phrase(billpdfDTO.getregimen(),fontH4));
            PdfPCell nombreCajero =new PdfPCell(new Phrase("Cajero: "+removeWord(billpdfDTO.getnombreCajero(),"null"),fontH4));
            PdfPCell cliente =new PdfPCell(new Phrase(""+removeWord(billpdfDTO.getcliente(),"null"),fontH4));
            PdfPCell documentoCC = new PdfPCell(new Phrase(billpdfDTO.getcedula(),fontH4));

            documentoCC.setHorizontalAlignment(Element.ALIGN_CENTER);
            documentoCC.setBorder(PdfPCell.NO_BORDER);
            cliente.setHorizontalAlignment(Element.ALIGN_CENTER);
            cliente.setBorder(PdfPCell.NO_BORDER);
            direccion.setBorder(PdfPCell.NO_BORDER);
            telefono.setBorder(PdfPCell.NO_BORDER);
            direccion.setHorizontalAlignment(Element.ALIGN_CENTER);
            telefono.setHorizontalAlignment(Element.ALIGN_CENTER);
            telefono.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBorder(PdfPCell.NO_BORDER);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cella.setBorder(PdfPCell.NO_BORDER);
            nit.setHorizontalAlignment(Element.ALIGN_CENTER);
            nit.setBorder(PdfPCell.NO_BORDER);
            regimen.setHorizontalAlignment(Element.ALIGN_CENTER);
            regimen.setBorder(PdfPCell.NO_BORDER);
            nombreCajero.setHorizontalAlignment(Element.ALIGN_CENTER);
            nombreCajero.setBorder(PdfPCell.NO_BORDER);


            try {
                Image img = Image.getInstance("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/logos/"+billpdfDTO.getnit()+".jpg");
                //Image img = Image.getInstance("./"+billpdfDTO.getnit()+".jpg");
                PdfPCell imagen = new PdfPCell(img, true);
                imagen.setBorder(PdfPCell.NO_BORDER);
                imagen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos.addCell(imagen);
            }catch(Exception e){
                System.out.println(e.toString());
            }


            tablaDatos.addCell(cell1);
            tablaDatos.addCell(nit);
            tablaDatos.addCell(direccion);
            tablaDatos.addCell(telefono);
            tablaDatos.addCell(cella);

            PdfPTable tablaData = new PdfPTable(1);

            //tablaData.addCell(direccion);
            //tablaData.addCell(telefono);
            //tablaData.addCell(cell2);
            //tablaData.addCell(nombreCajero);
            //tablaData.addCell(cliente);
            //tablaData.addCell(documentoCC);
            PdfPCell direCl =new PdfPCell(new Phrase(billpdfDTO.getdireccionC(),fontH4));
            PdfPCell telCl =new PdfPCell(new Phrase("Tel: "+billpdfDTO.gettelefonoC(),fontH4));
            direCl.setHorizontalAlignment(Element.ALIGN_CENTER);
            direCl.setBorder(PdfPCell.NO_BORDER);
            telCl.setHorizontalAlignment(Element.ALIGN_CENTER);
            telCl.setBorder(PdfPCell.NO_BORDER);
            //tablaData.addCell(direCl);
            //tablaData.addCell(telCl);

            PdfPTable tablaFecha = new PdfPTable(1);
            PdfPCell cell3 =new PdfPCell(new Phrase(billpdfDTO.getfecha(),fontH4));
            cell3.setBorder(PdfPCell.NO_BORDER);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaFecha.addCell(cella);
            tablaFecha.addCell(cell3);
            tablaFecha.addCell(cell2);
            tablaFecha.addCell(nombreCajero);
            tablaFecha.addCell(cella);

            PdfPTable tablaDatos2 = new PdfPTable(2);


            PdfPCell cell5 =new PdfPCell(new Phrase("Tienda: "+billpdfDTO.gettienda(),fontH4));



            cell5.setBorder(PdfPCell.NO_BORDER);
            cell5.setHorizontalAlignment(Element.ALIGN_CENTER);


            tablaDatos2.addCell(cell5);

            if(billpdfDTO.getcaja().equals("-1")){
                PdfPCell cell4 =new PdfPCell(new Phrase("REMISIÓN",fontH4));
                cell4.setBorder(PdfPCell.NO_BORDER);
                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos2.addCell(cell4);


            }else{
                PdfPCell cell4 =new PdfPCell(new Phrase("Caja: "+billpdfDTO.getcaja(),fontH4));
                cell4.setBorder(PdfPCell.NO_BORDER);
                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos2.addCell(cell4);
            }
            tablaDatos2.addCell(cella);
            tablaDatos2.addCell(cella);


            PdfPTable tablaDetalles = new PdfPTable(3);
            PdfPTable tablaDetalles2 = new PdfPTable(2);

            PdfPCell cell6 =new PdfPCell(new Phrase("Prod",fontH4));
            PdfPCell cell7 =new PdfPCell(new Phrase("Cant",fontH4));
            PdfPCell cell8 =new PdfPCell(new Phrase("Precio",fontH4));

            cell6.setBorder(PdfPCell.NO_BORDER);
            cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell7.setBorder(PdfPCell.NO_BORDER);
            cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell8.setBorder(PdfPCell.NO_BORDER);
            cell8.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDetalles2.addCell(cell7);
            tablaDetalles2.addCell(cell8);
            PdfPCell table = new PdfPCell(tablaDetalles2);
            table.setBorder(PdfPCell.NO_BORDER);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetalles.addCell(cell7);
            tablaDetalles.addCell(cell6);
            tablaDetalles.addCell(cell8);

            PdfPCell saltoLinea = new PdfPCell(new Phrase("    "));

            List<List<String>> theList = billpdfDTO.getdetalles();

            for (int i = 0; i < billpdfDTO.getdetalles().size(); i++) {
                PdfPCell cell9 = new PdfPCell();
                if(theList.get(i).get(1).length()<=23) {
                    cell9 = new PdfPCell(new Phrase(theList.get(i).get(1), fontH2));
                }else{
                    cell9 = new PdfPCell(new Phrase(theList.get(i).get(1).substring(0, 23), fontH2));
                }


                cell9.setBorder(PdfPCell.NO_BORDER);
                cell9.setHorizontalAlignment(Element.ALIGN_LEFT);

                PdfPTable tablaDetalles3 = new PdfPTable(3);
                PdfPCell cell10 = new PdfPCell(new Phrase(theList.get(i).get(0), fontH1));
                int number = new Integer(theList.get(i).get(2));
                PdfPCell cell11 = new PdfPCell(new Phrase(format.format(number), fontH1));
                cell10.setBorder(PdfPCell.NO_BORDER);
                cell10.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell11.setBorder(PdfPCell.NO_BORDER);
                cell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
                saltoLinea.setBorder(PdfPCell.NO_BORDER);
                tablaDetalles.addCell(cell10);
                tablaDetalles.addCell(cell9);
                tablaDetalles.addCell(cell11);
                PdfPCell tablas = new PdfPCell(tablaDetalles3);
                tablas.setBorder(PdfPCell.NO_BORDER);
                tablas.setHorizontalAlignment(Element.ALIGN_CENTER);
                //tablaDetalles.addCell(tablas);
            }

            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            PdfPTable tablaTotales = new PdfPTable(2);
            PdfPCell cell12 = new PdfPCell(new Phrase("Subtotal",fontH4));
            PdfPCell cell13 = new PdfPCell(new Phrase(format.format(billpdfDTO.getsubtotal())+"",fontH4));
            PdfPCell cellPago = new PdfPCell(new Phrase("Cantidad Recibida",fontH4));
            PdfPCell pago = new PdfPCell(new Phrase(format.format((billpdfDTO.getcambio()+round(billpdfDTO.gettotalprice())))+"",fontH4));
            PdfPCell cell14 = new PdfPCell(new Phrase("IVA",fontH4));
            PdfPCell cell15 = new PdfPCell(new Phrase(format.format(billpdfDTO.gettax())+"",fontH4));
            PdfPCell cellcambio = new PdfPCell(new Phrase("Cambio",fontH4));
            PdfPCell celcambio2 = new PdfPCell(new Phrase(format.format(billpdfDTO.getcambio())+"",fontH4));
            PdfPCell cell16 = new PdfPCell(new Phrase("Total",fontH4));
            PdfPCell cell17 = new PdfPCell(new Phrase(format.format(round(billpdfDTO.gettotalprice()))+"",fontH4));
            PdfPCell cell16medio = new PdfPCell(new Phrase("Medio pago",fontH4));
            PdfPCell cell17medio = new PdfPCell(new Phrase(billpdfDTO.getmedioPago()+"",fontH4));
            cell12.setBorder(PdfPCell.NO_BORDER);
            cell13.setBorder(PdfPCell.NO_BORDER);
            cell14.setBorder(PdfPCell.NO_BORDER);
            cell15.setBorder(PdfPCell.NO_BORDER);
            cell16.setBorder(PdfPCell.NO_BORDER);
            cell17.setBorder(PdfPCell.NO_BORDER);
            cellcambio.setBorder(PdfPCell.NO_BORDER);
            celcambio2.setBorder(PdfPCell.NO_BORDER);
            cell16medio.setBorder(PdfPCell.NO_BORDER);
            cell17medio.setBorder(PdfPCell.NO_BORDER);
            cellPago.setBorder(PdfPCell.NO_BORDER);
            pago.setBorder(PdfPCell.NO_BORDER);
            cellPago.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pago.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell13.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell14.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell15.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell16.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell17.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cellcambio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celcambio2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell16medio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell17medio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaTotales.addCell(cell12);
            tablaTotales.addCell(cell13);
            tablaTotales.addCell(cell14);
            tablaTotales.addCell(cell15);
            tablaTotales.addCell(cell16);
            tablaTotales.addCell(cell17);
            tablaTotales.addCell(cellPago);
            tablaTotales.addCell(pago);
            tablaTotales.addCell(cellcambio);
            tablaTotales.addCell(celcambio2);
            tablaTotales.addCell(cell16medio);
            tablaTotales.addCell(cell17medio);




            PdfPTable tablaLegal = new PdfPTable(1);
            tablaLegal.addCell(cella);
            if(billpdfDTO.getregimenT()!= null && !billpdfDTO.getregimenT().isEmpty()){
                String regimenTemp;
                if(billpdfDTO.getregimenT().equals("C")){
                    regimenTemp = "Común";
                }else{
                    regimenTemp = "Simplificado";
                }
                PdfPCell cellRegimen = new PdfPCell(new Phrase("Régimen "+regimenTemp,fontH4));
                cellRegimen.setBorder(PdfPCell.NO_BORDER);
                cellRegimen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellRegimen);
            }

            if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                PdfPCell cellAuth = new PdfPCell(new Phrase("Resolución Dian # "+billpdfDTO.getresolucion_DIAN(),fontH4));
                cellAuth.setBorder(PdfPCell.NO_BORDER);
                cellAuth.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellAuth);
            }

           /* if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                PdfPCell cellDatos = new PdfPCell(new Phrase("Fecha Formalización: 13 de junio de 2019",fontH2));
                cellDatos.setBorder(PdfPCell.NO_BORDER);
                cellDatos.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellDatos);
            }    */

            PdfPCell cellPos = new PdfPCell(new Phrase("Modalidad: POS",fontH4));
            cellPos.setBorder(PdfPCell.NO_BORDER);
            cellPos.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaLegal.addCell(cellPos);

            //if(billpdfDTO.getinitial_RANGE()!= null && !billpdfDTO.getinitial_RANGE().isEmpty() &&
            //        billpdfDTO.getfinal_RANGE()!= null && !billpdfDTO.getfinal_RANGE().isEmpty() &&
            //        billpdfDTO.getprefix()!= null && !billpdfDTO.getprefix().isEmpty()){
            //    PdfPCell cellAutor = new PdfPCell(new Phrase("Autorización: Desde "+billpdfDTO.getprefix()+billpdfDTO.getinitial_RANGE()+" a "+billpdfDTO.getprefix()+billpdfDTO.getfinal_RANGE(),fontH4));
            //    cellAutor.setBorder(PdfPCell.NO_BORDER);
            //    cellAutor.setHorizontalAlignment(Element.ALIGN_CENTER);
            //    tablaLegal.addCell(cellAutor);
            //}



            tablaLegal.addCell(cella);

            PdfPTable tabla = new PdfPTable(1);


            tablaDatos.setKeepTogether(false);
            tablaData.setKeepTogether(false);
            tablaFecha.setKeepTogether(false);
            tablaDatos2.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaLegal.setKeepTogether(false);
            tablaTotales.setKeepTogether(false);

            tablaDatos.setSplitLate(false);
            tablaData.setSplitLate(false);
            tablaFecha.setSplitLate(false);
            tablaDatos2.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaLegal.setSplitLate(false);
            tablaTotales.setSplitLate(false);



            PdfPCell tablaDatosC = new PdfPCell(tablaDatos);
            PdfPCell tablaDataC = new PdfPCell(tablaData);
            PdfPCell tablaFechaC = new PdfPCell(tablaFecha);
            PdfPCell tablaDatos2C = new PdfPCell(tablaDatos2);
            PdfPCell tablaDetallesC = new PdfPCell(tablaDetalles);
            PdfPCell tablaDetalles2C = new PdfPCell(tablaDetalles2);
            PdfPCell tablaLegalC = new PdfPCell(tablaLegal);
            PdfPCell tablaTotalesC = new PdfPCell(tablaTotales);




            tablaTotalesC.setBorder(PdfPCell.NO_BORDER);
            tablaDatosC.setBorder(PdfPCell.NO_BORDER);
            tablaDataC.setBorder(PdfPCell.NO_BORDER);
            tablaFechaC.setBorder(PdfPCell.NO_BORDER);
            tablaDatos2C.setBorder(PdfPCell.NO_BORDER);
            tablaDetallesC.setBorder(PdfPCell.NO_BORDER);
            tablaDetalles2C.setBorder(PdfPCell.NO_BORDER);
            tablaLegalC.setBorder(PdfPCell.NO_BORDER);





            tabla.addCell(tablaDatosC);
            tabla.addCell(tablaDataC);
            tabla.addCell(tablaFechaC);
            tabla.addCell(tablaDatos2C);
            tabla.addCell(tablaDetallesC);
            //tabla.addCell(tablaDetalles2C);
            tabla.addCell(tablaTotalesC);
            tabla.addCell(tablaLegalC);

            tabla.setKeepTogether(false);
            tabla.setSplitLate(false);

            documento.add(tabla);
            response = nombrePdf;

        }catch(Exception e){
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String> printPDF(BillPdfDTO billpdfDTO){
        // Se crea el documento
        Rectangle envelope = new Rectangle(227, 842);
        Document documento = new Document(envelope, 0f, 0f, 10f, 0f);
        //Document documento = new Document();0

        String response = "Bad!!!";
        try { // Se crea el OutputStream para el fichero donde queremos dejar el pdf.


            String empresa = billpdfDTO.getempresa();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 8, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 8, Font.NORMAL);
            empresa = empresa.replaceAll(" ", "_");
            String nombrePdf = empresa + "_" + billpdfDTO.getconsecutivo().replace(" ", "_").replaceAll("_-_","-") + ".pdf";
            FileOutputStream ficheroPdf = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/facturas/"+ nombrePdf);
            //FileOutputStream ficheroPdf = new FileOutputStream("./" + nombrePdf);

            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento, ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));


            PdfPTable tablaDatos = new PdfPTable(1);
            PdfPCell cell1 = new PdfPCell(new Phrase(billpdfDTO.getempresa(), fontH1));
            PdfPCell nit = new PdfPCell(new Phrase(billpdfDTO.getnit(), fontH1));
            PdfPCell direccion = new PdfPCell(new Phrase(billpdfDTO.getdireccion(), fontH2));
            PdfPCell telefono = new PdfPCell(new Phrase(billpdfDTO.gettelefono(), fontH2));
            PdfPCell cell2 = new PdfPCell(new Phrase("Factura # " + billpdfDTO.getconsecutivo().replaceAll(" - ", ""), fontH2));
            PdfPCell cella = new PdfPCell(new Phrase("  ", fontH2));
            PdfPCell regimen = new PdfPCell(new Phrase(billpdfDTO.getregimen(), fontH2));
            PdfPCell nombreCajero = new PdfPCell(new Phrase("Cajero: " + removeWord(billpdfDTO.getnombreCajero(),"null"), fontH2));
            PdfPCell cliente = new PdfPCell(new Phrase("Cliente: " + removeWord(billpdfDTO.getcliente(),"null"), fontH2));
            PdfPCell documentoCC = new PdfPCell(new Phrase(billpdfDTO.getcedula(), fontH2));

            documentoCC.setHorizontalAlignment(Element.ALIGN_CENTER);
            documentoCC.setBorder(PdfPCell.NO_BORDER);
            cliente.setHorizontalAlignment(Element.ALIGN_CENTER);
            cliente.setBorder(PdfPCell.NO_BORDER);
            direccion.setBorder(PdfPCell.NO_BORDER);
            telefono.setBorder(PdfPCell.NO_BORDER);
            direccion.setHorizontalAlignment(Element.ALIGN_CENTER);
            telefono.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBorder(PdfPCell.NO_BORDER);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cella.setBorder(PdfPCell.NO_BORDER);
            nit.setHorizontalAlignment(Element.ALIGN_CENTER);
            nit.setBorder(PdfPCell.NO_BORDER);
            regimen.setHorizontalAlignment(Element.ALIGN_CENTER);
            regimen.setBorder(PdfPCell.NO_BORDER);
            nombreCajero.setHorizontalAlignment(Element.ALIGN_CENTER);
            nombreCajero.setBorder(PdfPCell.NO_BORDER);


            try {
                Image img = Image.getInstance("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/logos/" + billpdfDTO.getnit() + ".jpg");
                //Image img = Image.getInstance("./"+billpdfDTO.getnit()+".jpg");
                PdfPCell imagen = new PdfPCell(img, true);
                imagen.setBorder(PdfPCell.NO_BORDER);
                imagen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos.addCell(imagen);
            } catch (Exception e) {
                System.out.println(e.toString());
            }
            tablaDatos.addCell(cell1);
            tablaDatos.addCell(nit);
            tablaDatos.addCell(direccion);
            tablaDatos.addCell(telefono);
            tablaDatos.addCell(cella);

            PdfPTable tablaData = new PdfPTable(2);

            //tablaData.addCell(direccion);
            //tablaData.addCell(telefono);
            //tablaData.addCell(cell2);
            //tablaData.addCell(nombreCajero);


            tablaData.addCell(cliente);
            tablaData.addCell(documentoCC);
            PdfPCell direCl = new PdfPCell(new Phrase("Dir: " + billpdfDTO.getdireccionC(), fontH2));
            PdfPCell telCl = new PdfPCell(new Phrase("Tel: " + billpdfDTO.gettelefonoC(), fontH2));
            direCl.setHorizontalAlignment(Element.ALIGN_CENTER);
            direCl.setBorder(PdfPCell.NO_BORDER);
            telCl.setHorizontalAlignment(Element.ALIGN_CENTER);
            telCl.setBorder(PdfPCell.NO_BORDER);
            tablaData.addCell(direCl);
            tablaData.addCell(telCl);
            if (billpdfDTO.getcliente().equals("Cliente Ocasional")) {
                tablaData = new PdfPTable(1);
                cliente.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaData.addCell(cliente);
            }

            PdfPTable tablaFecha = new PdfPTable(1);
            PdfPCell cell3 = new PdfPCell(new Phrase(billpdfDTO.getfecha(), fontH1));
            cell3.setBorder(PdfPCell.NO_BORDER);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaFecha.addCell(cella);
            tablaFecha.addCell(cell3);
            tablaFecha.addCell(cell2);
            tablaFecha.addCell(nombreCajero);
            tablaFecha.addCell(cella);

            PdfPTable tablaDatos2 = new PdfPTable(2);
            PdfPCell cell5 = new PdfPCell(new Phrase("Tienda: " + billpdfDTO.gettienda(), fontH2));
            cell5.setBorder(PdfPCell.NO_BORDER);
            cell5.setHorizontalAlignment(Element.ALIGN_CENTER);


            tablaDatos2.addCell(cell5);
            if (billpdfDTO.getcaja().equals("-1")) {
                PdfPCell cell4 = new PdfPCell(new Phrase("REMISIÓN", fontH2));
                cell4.setBorder(PdfPCell.NO_BORDER);
                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos2.addCell(cell4);

            } else {
                PdfPCell cell4 = new PdfPCell(new Phrase("Caja: " + billpdfDTO.getcaja(), fontH2));
                cell4.setBorder(PdfPCell.NO_BORDER);
                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDatos2.addCell(cell4);
            }
            tablaDatos2.addCell(cella);
            tablaDatos2.addCell(cella);


            PdfPTable tablaDetalles = new PdfPTable(3);
            PdfPTable tablaDetalles2 = new PdfPTable(2);

            PdfPCell cell6 = new PdfPCell(new Phrase("Prod", fontH1));
            PdfPCell cell7 = new PdfPCell(new Phrase("Cant", fontH1));
            PdfPCell cell8 = new PdfPCell(new Phrase("Precio", fontH1));

            cell6.setBorder(PdfPCell.NO_BORDER);
            cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell7.setBorder(PdfPCell.NO_BORDER);
            cell7.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell8.setBorder(PdfPCell.NO_BORDER);
            cell8.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaDetalles2.addCell(cell7);
            tablaDetalles2.addCell(cell8);
            PdfPCell table = new PdfPCell(tablaDetalles2);
            table.setBorder(PdfPCell.NO_BORDER);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetalles.addCell(cell6);
            tablaDetalles.addCell(cell7);
            tablaDetalles.addCell(cell8);

            PdfPCell saltoLinea = new PdfPCell(new Phrase("    "));

            List<List<String>> theList = billpdfDTO.getdetalles();

            for (int i = 0; i < billpdfDTO.getdetalles().size(); i++) {
                PdfPCell cell9 = new PdfPCell();
                if (theList.get(i).get(1).length() <= 20) {
                    cell9 = new PdfPCell(new Phrase(theList.get(i).get(1), fontH2));
                } else {
                    cell9 = new PdfPCell(new Phrase(theList.get(i).get(1).substring(0, 20), fontH2));
                }


                cell9.setBorder(PdfPCell.NO_BORDER);
                cell9.setHorizontalAlignment(Element.ALIGN_CENTER);

                PdfPTable tablaDetalles3 = new PdfPTable(3);
                PdfPCell cell10 = new PdfPCell(new Phrase(theList.get(i).get(0), fontH2));
                PdfPCell cell11 = new PdfPCell(new Phrase("$" + theList.get(i).get(2), fontH2));
                cell10.setBorder(PdfPCell.NO_BORDER);
                cell10.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell11.setBorder(PdfPCell.NO_BORDER);
                cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
                saltoLinea.setBorder(PdfPCell.NO_BORDER);
                tablaDetalles.addCell(cell9);
                tablaDetalles.addCell(cell10);
                tablaDetalles.addCell(cell11);
                PdfPCell tablas = new PdfPCell(tablaDetalles3);
                tablas.setBorder(PdfPCell.NO_BORDER);
                tablas.setHorizontalAlignment(Element.ALIGN_CENTER);
                //tablaDetalles.addCell(tablas);
            }

            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            tablaDetalles.addCell(cella);
            PdfPTable tablaTotales = new PdfPTable(2);
            PdfPCell cell12 = new PdfPCell(new Phrase("Subtotal", fontH2));
            PdfPCell cell13 = new PdfPCell(new Phrase("$" + billpdfDTO.getsubtotal() + "", fontH2));
            PdfPCell cellPago = new PdfPCell(new Phrase("Cantidad Recibida", fontH2));
            PdfPCell pago = new PdfPCell(new Phrase("$" + (billpdfDTO.getcambio() + round(billpdfDTO.gettotalprice())), fontH2));
            PdfPCell cell14 = new PdfPCell(new Phrase("IVA", fontH2));
            PdfPCell cell15 = new PdfPCell(new Phrase("$" + billpdfDTO.gettax(), fontH2));
            PdfPCell cellcambio = new PdfPCell(new Phrase("Cambio", fontH2));
            PdfPCell celcambio2 = new PdfPCell(new Phrase("$" + billpdfDTO.getcambio() + ".0", fontH2));
            PdfPCell cell16 = new PdfPCell(new Phrase("Total", fontH2));
            PdfPCell cell17 = new PdfPCell(new Phrase("$" + round(billpdfDTO.gettotalprice()) + "", fontH2));
            PdfPCell cell16medio = new PdfPCell(new Phrase("Medio pago", fontH2));
            PdfPCell cell17medio = new PdfPCell(new Phrase(billpdfDTO.getmedioPago() + "", fontH2));
            cell12.setBorder(PdfPCell.NO_BORDER);
            cell13.setBorder(PdfPCell.NO_BORDER);
            cell14.setBorder(PdfPCell.NO_BORDER);
            cell15.setBorder(PdfPCell.NO_BORDER);
            cell16.setBorder(PdfPCell.NO_BORDER);
            cell17.setBorder(PdfPCell.NO_BORDER);
            cellcambio.setBorder(PdfPCell.NO_BORDER);
            celcambio2.setBorder(PdfPCell.NO_BORDER);
            cell16medio.setBorder(PdfPCell.NO_BORDER);
            cell17medio.setBorder(PdfPCell.NO_BORDER);
            cellPago.setBorder(PdfPCell.NO_BORDER);
            pago.setBorder(PdfPCell.NO_BORDER);
            cellPago.setHorizontalAlignment(Element.ALIGN_RIGHT);
            pago.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell13.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell14.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell15.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell16.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell17.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cellcambio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celcambio2.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell16medio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell17medio.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tablaTotales.addCell(cell12);
            tablaTotales.addCell(cell13);
            tablaTotales.addCell(cell14);
            tablaTotales.addCell(cell15);
            tablaTotales.addCell(cell16);
            tablaTotales.addCell(cell17);
            tablaTotales.addCell(cellPago);
            tablaTotales.addCell(pago);
            tablaTotales.addCell(cellcambio);
            tablaTotales.addCell(celcambio2);
            tablaTotales.addCell(cell16medio);
            tablaTotales.addCell(cell17medio);


            PdfPTable tablaLegal = new PdfPTable(1);
            tablaLegal.addCell(cella);
            if (billpdfDTO.getregimenT() != null && !billpdfDTO.getregimenT().isEmpty()) {
                String regimenTemp;
                if (billpdfDTO.getregimenT().equals("C")) {
                    regimenTemp = "Común";
                } else {
                    regimenTemp = "Simplificado";
                }
                PdfPCell cellRegimen = new PdfPCell(new Phrase("Régimen " + regimenTemp, fontH2));
                cellRegimen.setBorder(PdfPCell.NO_BORDER);
                cellRegimen.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellRegimen);
            }

            if (billpdfDTO.getresolucion_DIAN() != null && !billpdfDTO.getresolucion_DIAN().isEmpty()) {
                PdfPCell cellAuth = new PdfPCell(new Phrase("Resolución Dian # " + billpdfDTO.getresolucion_DIAN(), fontH2));
                cellAuth.setBorder(PdfPCell.NO_BORDER);
                cellAuth.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellAuth);
            }

           /* if(billpdfDTO.getresolucion_DIAN()!= null && !billpdfDTO.getresolucion_DIAN().isEmpty()){
                PdfPCell cellDatos = new PdfPCell(new Phrase("Fecha Formalización: 13 de junio de 2019",fontH2));
                cellDatos.setBorder(PdfPCell.NO_BORDER);
                cellDatos.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaLegal.addCell(cellDatos);
            }    */

            PdfPCell cellPos = new PdfPCell(new Phrase("Modalidad: POS", fontH2));
            cellPos.setBorder(PdfPCell.NO_BORDER);
            cellPos.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaLegal.addCell(cellPos);

            //if (billpdfDTO.getinitial_RANGE() != null && !billpdfDTO.getinitial_RANGE().isEmpty() &&
            //        billpdfDTO.getfinal_RANGE() != null && !billpdfDTO.getfinal_RANGE().isEmpty() &&
            //        billpdfDTO.getprefix() != null && !billpdfDTO.getprefix().isEmpty()) {
            //    PdfPCell cellAutor = new PdfPCell(new Phrase("Autorización: Desde " + billpdfDTO.getprefix() + billpdfDTO.getinitial_RANGE() + " a " + billpdfDTO.getprefix() + billpdfDTO.getfinal_RANGE(), fontH2));
            //    cellAutor.setBorder(PdfPCell.NO_BORDER);
            //    cellAutor.setHorizontalAlignment(Element.ALIGN_CENTER);
            //    tablaLegal.addCell(cellAutor);
            //}


            tablaLegal.addCell(cella);


            tablaDatos.setKeepTogether(false);
            tablaData.setKeepTogether(false);
            tablaFecha.setKeepTogether(false);
            tablaDatos2.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaDetalles.setKeepTogether(false);
            tablaTotales.setKeepTogether(false);
            tablaLegal.setKeepTogether(false);


            tablaDatos.setSplitLate(false);
            tablaData.setSplitLate(false);
            tablaFecha.setSplitLate(false);
            tablaDatos2.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaDetalles.setSplitLate(false);
            tablaTotales.setSplitLate(false);
            tablaLegal.setSplitLate(false);

            PdfPTable tabla = new PdfPTable(1);
            PdfPCell tablaDatosC = new PdfPCell(tablaDatos);
            PdfPCell tablaDataC = new PdfPCell(tablaData);
            PdfPCell tablaFechaC = new PdfPCell(tablaFecha);
            PdfPCell tablaDatos2C = new PdfPCell(tablaDatos2);
            PdfPCell tablaDetallesC = new PdfPCell(tablaDetalles);
            PdfPCell tablaDetalles2C = new PdfPCell(tablaDetalles2);
            PdfPCell tablaTotalesC = new PdfPCell(tablaTotales);
            PdfPCell tablaLegalC = new PdfPCell(tablaLegal);
            tablaDatosC.setBorder(PdfPCell.NO_BORDER);
            tablaDataC.setBorder(PdfPCell.NO_BORDER);
            tablaFechaC.setBorder(PdfPCell.NO_BORDER);
            tablaDatos2C.setBorder(PdfPCell.NO_BORDER);
            tablaDetallesC.setBorder(PdfPCell.NO_BORDER);
            tablaDetalles2C.setBorder(PdfPCell.NO_BORDER);
            tablaTotalesC.setBorder(PdfPCell.NO_BORDER);
            tablaLegalC.setBorder(PdfPCell.NO_BORDER);

            tabla.setKeepTogether(false);
            tabla.setSplitLate(false);
            tabla.addCell(tablaDatosC);
            tabla.addCell(tablaDataC);
            tabla.addCell(tablaFechaC);
            tabla.addCell(tablaDatos2C);
            tabla.addCell(tablaDetallesC);
            //tabla.addCell(tablaDetalles2C);
            tabla.addCell(tablaTotalesC);
            tabla.addCell(tablaLegalC);
            documento.add(tabla);
            response = nombrePdf;

        }catch(Exception e){
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    /**
     * Get Billing with a filter of bill, so The response will be with  or without this filter.
     * @param bill
     * @return
     */
    public Either<IException, List<Bill>> getBill(Bill bill) {
        List<String> msn = new ArrayList<>();
        try {
            System.out.println("|||||||||||| Starting consults Bill ||||||||||||||||| ");
            return Either.right(billDAO.read(billSanitation(bill)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * Get Billing with details as detail bill and detail payment, using a filter of bill, of detail bill, of detail payment. So The response will be with  or without this filter.
     * @param bill
     * @param detailBill
     * @param detailPaymentBill
     * @return
     */
    public Either<IException, List<HashMap<String,Object>>> getBillDetail(Bill bill,DetailBill detailBill,DetailPaymentBill detailPaymentBill) {
        List<String> msn = new ArrayList<>();

        List<HashMap<String,Object>> response= new ArrayList<>();
        List<Bill> billList= new ArrayList<>();
        List<DetailBill> detailBillList= new ArrayList<>();
        List<DetailPaymentBill> detailPaymentBillList= new ArrayList<>();
        try {
            System.out.println("|||||||||||| Starting consults Bill Detail ||||||||||||||||| ");
            billList=billDAO.read(billSanitation(bill));
            for (Bill billT: billList){
                HashMap<String,Object> response_aux= new HashMap<>();


                Either<IException, List<DetailPaymentBill>> detailPaymentEither = detailPaymentBillBusiness.getDetailPaymentBill(detailPaymentBill);
                if (detailPaymentEither.isRight()){
                    detailPaymentBillList = detailPaymentEither.right().value();
                    if (detailPaymentBillList.size()>0){

                        response_aux.put("detail_payments",detailPaymentBillList);
                    }
                }

                Either<IException, List<DetailBill>> detailBillEither = detailBillBusiness.getDetailBill(detailBill);
                if (detailBillEither.isRight()){
                    detailBillList = detailBillEither.right().value();
                    if (detailBillList.size()>0){

                        response_aux.put("details",detailBillList);
                    }
                }
                response_aux.put("billing",billT);
                response.add(response_aux);
            }

            return Either.right(response);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     *
     * Get Billing with a description more depth using bill complete. So The response will be with  or without this filter.
     * @param billComplete
     * @return
     */
    public Either<IException, List<BillComplete>> getBillComplete(BillComplete billComplete) {
        List<String> msn = new ArrayList<>();
        try {

            System.out.println("|||||||||||| Starting consults Bill ||||||||||||||||| ");

            BillComplete billCompleteSanitation = billCompleteSanitation(billComplete);
            return Either.right(billDAO.readComplete(billCompleteSanitation,billCompleteSanitation.getPayment_state(),billCompleteSanitation.getBill_state(),
                    billCompleteSanitation.getBill_type()));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     *
     * Get Billing with a description more depth. With details as detail bill and detail payment complete, using a filter of bill complete, of detail bill, of detail payment  complete. So The response will be with  or without this filter.
     * @param billComplete
     * @param detailBill
     * @param detailPaymentBillComplete
     * @return
     */
    public Either<IException, List<HashMap<String,Object>>> getBillCompleteDetail(BillComplete billComplete,DetailBill detailBill,DetailPaymentBillComplete detailPaymentBillComplete) {
        List<String> msn = new ArrayList<>();

        List<HashMap<String,Object>> response= new ArrayList<>();
        List<BillComplete> billCompleteList= new ArrayList<>();
        List<DetailBill> detailBillList= new ArrayList<>();
        List<DetailPaymentBillComplete> detailPaymentBillList= new ArrayList<>();
        try {
            System.out.println("|||||||||||| Starting consults Bill Detail ||||||||||||||||| ");
            BillComplete billCompleteSanitation = billCompleteSanitation(billComplete);
            billCompleteList=billDAO.readComplete(billCompleteSanitation,billCompleteSanitation.getPayment_state(),billCompleteSanitation.getBill_state(),
                    billCompleteSanitation.getBill_type());


            for (BillComplete billT: billCompleteList){
                HashMap<String,Object> response_aux= new HashMap<>();


                Either<IException, List<DetailPaymentBillComplete>> detailPaymentEither = detailPaymentBillBusiness.getDetailPaymentBillComplete(detailPaymentBillComplete);
                if (detailPaymentEither.isRight()){
                    detailPaymentBillList = detailPaymentEither.right().value();
                    if (detailPaymentBillList.size()>0){

                        response_aux.put("detail_payments",detailPaymentBillList);
                    }
                }

                Either<IException, List<DetailBill>> detailBillEither = detailBillBusiness.getDetailBill(detailBill);
                if (detailBillEither.isRight()){
                    detailBillList = detailBillEither.right().value();
                    if (detailBillList.size()>0){

                        response_aux.put("details",detailBillList);
                    }
                }

                response_aux.put("billing",billT);
                response.add(response_aux);

            }

            return Either.right(response);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param billDTO
     * @return
     */
    public Either<IException,Long> creatBill(BillDTO billDTO, Long id_caja){
        List<String> msn = new ArrayList<>();
        Long id_bill = null;
        Long id_common_state = null;

        try {
            System.out.println("|||||||||||| Starting creation Bill   ||||||||||||||||| ");
            if (billDTO!=null){

                if (billDTO.getId_bill_father()!=null && formatoLongSql(billDTO.getId_bill_father())>0 && !validatorID(billDTO.getId_bill_father())){

                    msn.add("It ID Bill does not exist register on  Bill, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));

                }


                if (formatoLongSql(billDTO.getId_bill_state())>0 && !billStateBusiness.validatorID(billDTO.getId_bill_state()) ){

                    msn.add("It ID Bill State does not exist register on  Bill State, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                if (formatoLongSql(billDTO.getId_bill_type())>0 && !billTypeBusiness.validatorID(billDTO.getId_bill_type())){

                    msn.add("It ID Bill Type does not exist register on  Bill Type, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));

                }


                if (billDTO.getStateDTO() == null) {
                    billDTO.setStateDTO(new CommonStateDTO(1,null,null));
                }

                Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(billDTO.getStateDTO());
                if (commonThirdEither.isRight()) {
                    id_common_state = commonThirdEither.right().value();
                }
                Integer consecutive = generatorConsecutive();
                String consecutive_final=consecutive+"";
                if (billDTO.getId_third_employee()!=null){
                    consecutive_final=billDTO.getId_third_employee()+":"+consecutive;
                }
                billDAO.create(consecutive_final,id_common_state,billDTO, id_caja);
                id_bill = billDAO.getPkLast();

                billDAO.insertDocument(id_bill,billDTO.getdocumentDTO().gettitle(),billDTO.getdocumentDTO().getbody());

                billDAO.updateIdDocument(id_bill);



                // @TODO begin creation of detail payment bill
                if (billDTO.getPayments()!=null && billDTO.getPayments().size()>0){
                    detailPaymentBillBusiness.createDetailPaymentBill(id_bill,billDTO.getPayments());

                }
                // @TODO begin creation of detail bill

                if (billDTO.getDetails()!=null && billDTO.getDetails().size()>0){
                    detailBillBusiness.creatDetailBill(billDTO.getId_bill_type(),id_bill,billDTO.getDetails());
                }

                return Either.right(id_bill);
            }else{
                msn.add("It does not recognize Bill, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param billDetailIDDTO
     * @param id_bill
     * @return
     */
    public Either<IException, Long> updateBill(Long id_bill, BillDetailIDDTO billDetailIDDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        Bill actualRegister=null;
        List<Bill> read= new ArrayList<>();

        try {
            System.out.println("|||||||||||| Starting update Bill ||||||||||||||||| ");
            if ((id_bill != null && id_bill > 0)) {



                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorID(id_bill).equals(false)){
                    msn.add("It ID Bill  does not exist register on  Bill, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (billDetailIDDTO.getId_bill_father()!=null && formatoLongSql(billDetailIDDTO.getId_bill_father())>0 && !validatorID(billDetailIDDTO.getId_bill_father())){

                    msn.add("It ID Bill does not exist register on  Bill, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));

                }

                if (formatoLongSql(billDetailIDDTO.getId_bill_state())>0 && !billStateBusiness.validatorID(billDetailIDDTO.getId_bill_state()) ){

                    msn.add("It ID Bill State does not exist register on  Bill State, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                if (formatoLongSql(billDetailIDDTO.getId_bill_type())>0 && !billTypeBusiness.validatorID(billDetailIDDTO.getId_bill_type())){

                    msn.add("It ID Bill Type does not exist register on  Bill Type, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));

                }
                if (formatoLongSql(billDetailIDDTO.getId_payment_state())>0 && !paymentStateBusiness.validatorID(billDetailIDDTO.getId_payment_state())){

                    msn.add("It ID Payment State does not exist register on  Payment State, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                read= billDAO.read(
                        new Bill(id_bill,null,null,null,null,null,null,null,null,null,null,null,null,
                                new CommonState(null,null,null,null)));




                if (read.size()<=0){
                    msn.add("It does not recognize ID Bill  , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }else {
                    actualRegister=read.get(0);
                }


                List<CommonSimple> readCommons = billDAO.readCommons(formatoLongSql(id_bill));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                    // Common  State business update
                    commonStateBusiness.updateCommonState(idCommons.getId_common_state(), billDetailIDDTO.getStateDTO());
                }



                if (billDetailIDDTO.getId_third_employee() == null || billDetailIDDTO.getId_third_employee()<0) {
                    billDetailIDDTO.setId_third_employee(actualRegister.getId_third_employee());
                } else {
                    billDetailIDDTO.setId_third_employee(formatoLongSql(billDetailIDDTO.getId_third_employee()));
                }

                if (billDetailIDDTO.getId_third() == null || billDetailIDDTO.getId_third()<0) {
                    billDetailIDDTO.setId_third(actualRegister.getId_third());
                } else {
                    billDetailIDDTO.setId_third_employee(formatoLongSql(billDetailIDDTO.getId_third_employee()));
                }


                if (billDetailIDDTO.getPurchase_date()== null ) {
                    billDetailIDDTO.setPurchase_date(actualRegister.getPurchase_date());
                } else {
                    billDetailIDDTO.setPurchase_date(formatoDateSql(billDetailIDDTO.getPurchase_date()));
                }

                if (billDetailIDDTO.getSubtotal() == null) {
                    billDetailIDDTO.setSubtotal(actualRegister.getSubtotal());
                } else {
                    billDetailIDDTO.setSubtotal(formatoDoubleSql(billDetailIDDTO.getSubtotal()));
                }

                if (billDetailIDDTO.getTax() == null) {
                    billDetailIDDTO.setTax(actualRegister.getTax());
                } else {
                    billDetailIDDTO.setTax(formatoDoubleSql(billDetailIDDTO.getTax()));
                }

                if (billDetailIDDTO.getTotalprice() == null) {
                    billDetailIDDTO.setTotalprice(actualRegister.getTotalprice());
                } else {
                    billDetailIDDTO.setTotalprice(formatoDoubleSql(billDetailIDDTO.getTotalprice()));
                }

                // Attribute update
                billDAO.update(id_bill,billDetailIDDTO);

                if (billDetailIDDTO.getPayments()!=null && billDetailIDDTO.getPayments().size()>0){
                    detailPaymentBillBusiness.updateDetailPaymentBill(id_bill,billDetailIDDTO.getPayments());
                }
                if (billDetailIDDTO.getDetails()!=null && billDetailIDDTO.getDetails().size()>0){
                    detailBillBusiness.updateDetailBill(id_bill,billDetailIDDTO.getDetails());
                }
                return Either.right(id_bill);
            } else {
                msn.add("It does not recognize ID Bill, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Long> deleteBill(Long id_bill) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            if ( (id_bill != null && id_bill > 0)) {

                detailBillBusiness.deleteDetailBill(null,id_bill);
                detailPaymentBillBusiness.deleteDetailPaymentBill(null,id_bill);

                List<CommonSimple> readCommons = billDAO.readCommons(formatoLongSql(id_bill));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());

                return Either.right(id_bill);





            }else{
                msn.add("It does not recognize ID  Bill, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = billDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }

    public Long getIdThird(long id_person) {
        long validator = 0;
        try {
            validator = billDAO.getIdThird(id_person);
            if (validator != 0)
                return validator;
            else
                return new Long(0);
        }catch (Exception e){
            return new Long(0);
        }
    }

    public Either<IException, BillMaster> getMaestroFactura(Long id_bill,Long id_store) {
        try {
            return Either.right(billDAO.getMaestroFactura(id_bill,id_store));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, BillMaster2> getMaestroFactura2(Long id_bill,Long id_store) {
        try {
            return Either.right(billDAO.getMaestroFactura2(id_bill,id_store));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, List<TopProds>> getTopProducts(Long idstore) {
        try {
            return Either.right(billDAO.getTopProducts(idstore));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException,List<List<String>>> getDetalleFactura(Long id_bill) {
        try {
            List<BillDetail> list = billDAO.getDetalleFactura(id_bill);
            List<List<String>> response = new ArrayList<List<String>>();
            for(int i = 0; i < list.size(); i++) {
                List<String> tmp = new ArrayList<String>();
                tmp.add(list.get(i).getQUANTITY()+"");
                tmp.add(list.get(i).getPRODUCT_STORE_NAME());
                tmp.add(list.get(i).getVALOR()+"");
                tmp.add(list.get(i).getCODE()+"");
                response.add(tmp);
            }
            return Either.right(response);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, String> putCompraBill(Long id_bill, String dato, Date fecha) {

        try {
            billDAO.putCompraBill(id_bill,dato,fecha);
            return Either.right("OK!!!");
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    public Either<IException, List<LegalData>> getLegalData(Long id_third) {

        try {
            List<LegalData> a = billDAO.getLegalData(id_third);
            return Either.right(a);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

}
