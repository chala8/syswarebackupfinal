package com.name.business.businesses;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.DAOs.BillDAO;
import com.name.business.DAOs.PedidosDAO;
import com.name.business.DAOs.ReorderDAO;
import com.name.business.entities.*;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.io.FileOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.name.business.utils.constans.K.messages_error;
import static java.lang.Math.round;


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.util.CellRangeAddress;

public class PedidosBusiness {
    private ReorderDAO reorderDAO;
    private PedidosDAO pedidosDAO;

    public PedidosBusiness(PedidosDAO pedidosDAO, ReorderDAO reorderDAO) {
        this.pedidosDAO = pedidosDAO;
        this.reorderDAO = reorderDAO;
    }

    public Either<IException, String> getOwnBarCode(Long id_product_store) {
        try {
            String validator = pedidosDAO.getOwnBarCode(id_product_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> getPsId(String code, Long id_store) {
        try {
            Long validator = pedidosDAO.getPsId(code,id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Pedido>> getPedidos(Long id_store, Long id_bill_state, Long id_bill_type) {
        try {
            List<Pedido> validator = pedidosDAO.getPedidos(id_store,id_bill_state,id_bill_type);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<DetallePedido>> getDetallesPedidos(List<Long> id_store) {
        try {
            List<DetallePedido> validator = pedidosDAO.getDetallesPedidos(id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<DetallePedido>> getDetallesPedidos2(List<Long> id_bill) {
        try {
            List<DetallePedido> validator = pedidosDAO.getDetallesPedidos2(id_bill);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, String> postExcel(PickingConsolidadoPdfDTO pedido){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);


        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            HSSFRichTextString textoT7 = new HSSFRichTextString(" ");
            celdaprodT.setCellValue(textoT7);


            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            HSSFRichTextString textoT4 = new HSSFRichTextString(" ");
            celdalineaT.setCellValue(textoT4);


            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Documento de picking Consolidado");
            celdacateT.setCellValue(textoT5);


            HSSFCell celdamarcaT = filaTitulo.createCell((short)3);
            HSSFRichTextString textoT6 = new HSSFRichTextString(" ");
            celdamarcaT.setCellValue(textoT6);



            HSSFCell celdaCantT = filaTitulo.createCell((short)4);
            HSSFRichTextString textoT = new HSSFRichTextString(" ");
            celdaCantT.setCellValue(textoT);

            HSSFRow filaTitulo2 = hoja.createRow(1);

            HSSFCell celdaprodT2 = filaTitulo2.createCell((short)0);
            HSSFRichTextString textoT72 = new HSSFRichTextString(" ");
            celdaprodT2.setCellValue(textoT72);


            HSSFCell celdalineaT2 = filaTitulo2.createCell((short)1);
            HSSFRichTextString textoT42 = new HSSFRichTextString("Costo Total: "+pedido.getcostoTotal());
            celdalineaT2.setCellValue(textoT42);


            HSSFCell celdacateT2 = filaTitulo2.createCell((short)2);
            HSSFRichTextString textoT52 = new HSSFRichTextString(" ");
            celdacateT2.setCellValue(textoT52);


            HSSFCell celdamarcaT2 = filaTitulo2.createCell((short)3);
            HSSFRichTextString textoT62 = new HSSFRichTextString("Precio Total: "+pedido.getprecioTotal());
            celdamarcaT2.setCellValue(textoT62);



            HSSFCell celdaCantT2 = filaTitulo2.createCell((short)4);
            HSSFRichTextString textoT2 = new HSSFRichTextString(" ");
            celdaCantT2.setCellValue(textoT2);




            HSSFRow filaTitulo3 = hoja.createRow(2);

            HSSFCell celdaprodT23 = filaTitulo3.createCell((short)0);
            HSSFRichTextString textoT723 = new HSSFRichTextString("PRODUCTO");
            celdaprodT23.setCellValue(textoT723);


            HSSFCell celdalineaT23 = filaTitulo3.createCell((short)1);
            HSSFRichTextString textoT423 = new HSSFRichTextString("PRESENTACION");
            celdalineaT23.setCellValue(textoT423);


            HSSFCell celdacateT23 = filaTitulo3.createCell((short)2);
            HSSFRichTextString textoT523 = new HSSFRichTextString("CANTIDAD");
            celdacateT23.setCellValue(textoT523);


            HSSFCell celdamarcaT23 = filaTitulo3.createCell((short)3);
            HSSFRichTextString textoT623 = new HSSFRichTextString("COSTO");
            celdamarcaT23.setCellValue(textoT623);



            HSSFCell celdaCantT23 = filaTitulo3.createCell((short)4);
            HSSFRichTextString textoT23 = new HSSFRichTextString("COSTO TOTAL");
            celdaCantT23.setCellValue(textoT23);


            int cont = 3;

            for(FabricanteDTO element : pedido.getlist()){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(" ");
                celdaprod.setCellValue(texto7);


                HSSFCell celdalinea = fila.createCell((short)1);
                HSSFRichTextString texto4 = new HSSFRichTextString(" ");
                celdalinea.setCellValue(texto4);


                HSSFCell celdacate = fila.createCell((short)2);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getmarca().toUpperCase());
                celdacate.setCellValue(texto5);


                HSSFCell celdamarca = fila.createCell((short)3);
                HSSFRichTextString texto6 = new HSSFRichTextString(" ");
                celdamarca.setCellValue(texto6);



                HSSFCell celdaCant = fila.createCell((short)4);
                HSSFRichTextString texto = new HSSFRichTextString(" ");
                celdaCant.setCellValue(texto);

                cont++;


                for(DetallePedidoDTO item : element.getdetalles()){
                    HSSFRow filaTmp = hoja.createRow(cont);

                    HSSFCell celdaprodTmp = filaTmp.createCell((short)0);
                    HSSFRichTextString texto7Tmp = new HSSFRichTextString(item.getPRODUCTO());
                    celdaprodTmp.setCellValue(texto7Tmp);


                    HSSFCell celdalineaTmp = filaTmp.createCell((short)1);
                    HSSFRichTextString texto4Tmp = new HSSFRichTextString(item.getPRESENTACION());
                    celdalineaTmp.setCellValue(texto4Tmp);


                    HSSFCell celdacateTmp = filaTmp.createCell((short)2);
                    HSSFRichTextString texto5Tmp = new HSSFRichTextString(item.getCANTIDAD()+"");
                    celdacateTmp.setCellValue(texto5Tmp);


                    HSSFCell celdamarcaTmp = filaTmp.createCell((short)3);
                    HSSFRichTextString texto6Tmp = new HSSFRichTextString(item.getCOSTO()+"");
                    celdamarcaTmp.setCellValue(texto6Tmp);


                    HSSFCell celdaCantTmp = filaTmp.createCell((short)4);
                    HSSFRichTextString textoTmp = new HSSFRichTextString(item.getCOSTOTOTAL()+"");
                    celdaCantTmp.setCellValue(textoTmp);

                    cont++;

                }

                HSSFRow filaTmp = hoja.createRow(cont);

                HSSFCell celdaprodTmp = filaTmp.createCell((short)0);
                HSSFRichTextString texto7Tmp = new HSSFRichTextString(" ");
                celdaprodTmp.setCellValue(texto7Tmp);


                HSSFCell celdalineaTmp = filaTmp.createCell((short)1);
                HSSFRichTextString texto4Tmp = new HSSFRichTextString("Precio Marca: "+element.getprecioTotal());
                celdalineaTmp.setCellValue(texto4Tmp);


                HSSFCell celdacateTmp = filaTmp.createCell((short)2);
                HSSFRichTextString texto5Tmp = new HSSFRichTextString(" ");
                celdacateTmp.setCellValue(texto5Tmp);


                HSSFCell celdamarcaTmp = filaTmp.createCell((short)3);
                HSSFRichTextString texto6Tmp = new HSSFRichTextString("Costo Marca: "+element.getcostoTotal());
                celdamarcaTmp.setCellValue(texto6Tmp);


                HSSFCell celdaCantTmp = filaTmp.createCell((short)4);
                HSSFRichTextString textoTmp = new HSSFRichTextString(" ");
                celdaCantTmp.setCellValue(textoTmp);

                cont++;


                HSSFRow filaTmp2 = hoja.createRow(cont);

                HSSFCell celdaprodTmp2 = filaTmp2.createCell((short)0);
                HSSFRichTextString texto7Tmp2 = new HSSFRichTextString(" ");
                celdaprodTmp2.setCellValue(texto7Tmp2);


                HSSFCell celdalineaTmp2 = filaTmp2.createCell((short)1);
                HSSFRichTextString texto4Tmp2 = new HSSFRichTextString(" ");
                celdalineaTmp2.setCellValue(texto4Tmp2);


                HSSFCell celdacateTmp2 = filaTmp2.createCell((short)2);
                HSSFRichTextString texto5Tmp2 = new HSSFRichTextString(" ");
                celdacateTmp2.setCellValue(texto5Tmp2);


                HSSFCell celdamarcaTmp2 = filaTmp2.createCell((short)3);
                HSSFRichTextString texto6Tmp2 = new HSSFRichTextString(" ");
                celdamarcaTmp2.setCellValue(texto6Tmp2);


                HSSFCell celdaCantTmp2 = filaTmp2.createCell((short)4);
                HSSFRichTextString textoTmp2 = new HSSFRichTextString(" ");
                celdaCantTmp2.setCellValue(textoTmp2);

                cont++;
            }

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/reportes/"+Name+"pickingConsolidado.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+"pickingConsolidado.xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"pickingConsolidado.xls";
        }catch (Exception e){
            System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String> detailintaut(List<reorderProd> reorder,Long idstore) {

        String Response = "";
        try {
            List<TablaPedidos> validator = pedidosDAO.getPedidosT();
            TablaPedidos tp = validator.get(0);
            for (TablaPedidos ele:validator) {
               if(ele.getID_STORE_CLIENT() == idstore){
                   tp = ele;
               }
            }

            List<reorderProd> list = reorder;
                if(!list.isEmpty()){
                    pedidosDAO.create(tp.getID_THIRD_EMPLOYEE(),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),new Double(0),new Double(0),new Double(0),new Long(61),new Long(87),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER());
                    Long billClientId = pedidosDAO.getPkLast();
                    //String numdoc = pedidosDAO.getNumDoc(billClientId);
                    //pedidosDAO.create2(tp.getID_THIRD_EMP_PROVIDER(),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER(),new Double(0),new Double(0),new Double(0),new Long(62),new Long(86),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),numdoc);
                    //Long billProviderID = pedidosDAO.getPkLast();
                    System.out.println("this is id: "+billClientId);
                    for(reorderProd rp: list){
                        try{

                            Long idpsC = pedidosDAO.getPsId(rp.getOWNBARCODE(),tp.getID_STORE_CLIENT());
                            pedidosDAO.createDetail(billClientId,rp.getCANTIDAD(),idpsC,new Double(rp.getCOSTO()/rp.getCANTIDAD()),rp.getPERCENT());
                            Long idDetail = pedidosDAO.getPkLastDetail();
                            //pedidosDAO.procedure(billClientId,idpsC);
                            //Long quantity = pedidosDAO.detailData(idDetail).getQUANTITY();
                            //Long idpsP = pedidosDAO.getPsId(pedidosDAO.getOwnBarCode(pedidosDAO.detailData(idDetail).getID_PRODUCT_STORE()),tp.getID_STORE_PROVIDER());
                            //pedidosDAO.createDetail(billProviderID,quantity,idpsP,new Double(0),new Double(0));

                        }catch(Exception e2){
                            Response+=rp.getOWNBARCODE()+",";
                        }
                    }
                    pedidosDAO.procedure2(billClientId);

                }


            return Either.right("ok!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public String detailintaut2() {

        String Response = "";
        try {
            List<TablaPedidos> validator = pedidosDAO.getPedidosT();

            for (TablaPedidos tp:validator) {

                System.out.println("--------------------------------------");
                System.out.println(tp.getID_STORE_CLIENT());
                System.out.println(tp.getID_THIRD_CLIENT());
                System.out.println(tp.getID_STORE_PROVIDER());
                System.out.println(tp.getID_THIRD_DESTINITY());
                System.out.println(tp.getID_THIRD_EMPLOYEE());
                System.out.println(tp.getID_THIRD_EMP_PROVIDER());
                System.out.println("--------------------------------------");

                List<reorderProd> list = reorderDAO.getReorderData(tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(), new Date(), new Date());

                System.out.println("THE LIST IS EMPTY?"+list.isEmpty());
                /*if(!list.isEmpty()){
                    pedidosDAO.create(tp.getID_THIRD_EMPLOYEE(),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),new Double(0),new Double(0),new Double(0),new Long(61),new Long(87),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER());
                    Long billClientId = pedidosDAO.getPkLast();
                    String numdoc = pedidosDAO.getNumDoc(billClientId);
                    pedidosDAO.create2(tp.getID_THIRD_EMP_PROVIDER(),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER(),new Double(0),new Double(0),new Double(0),new Long(62),new Long(86),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),numdoc);
                    Long billProviderID = pedidosDAO.getPkLast();
                    for(reorderProd rp: list){
                        try{
                            Long idpsC = pedidosDAO.getPsId(rp.getOWNBARCODE(),tp.getID_STORE_CLIENT());
                            pedidosDAO.createDetail(billClientId,rp.getCANTIDAD(),idpsC,new Double(0),new Double(0));

                            Long idDetail = pedidosDAO.getPkLastDetail();
                            System.out.println("------------------------------------");
                            System.out.println(pedidosDAO.detailData(idDetail).getID_PRODUCT_STORE());
                            pedidosDAO.procedure(billClientId,idpsC);
                            Long quantity = pedidosDAO.detailData(idDetail).getQUANTITY();
                            Long idpsP = pedidosDAO.getPsId(pedidosDAO.getOwnBarCode(pedidosDAO.detailData(idDetail).getID_PRODUCT_STORE()),tp.getID_STORE_PROVIDER());
                            pedidosDAO.createDetail(billProviderID,quantity,idpsP,new Double(0),new Double(0));
                        }catch(Exception e2){
                               Response+=rp.getOWNBARCODE()+",";
                        }
                    }
                }
*/
                if(!list.isEmpty()){
                    pedidosDAO.create(tp.getID_THIRD_EMPLOYEE(),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),new Double(0),new Double(0),new Double(0),new Long(61),new Long(87),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER());
                    Long billClientId = pedidosDAO.getPkLast();
                    //String numdoc = pedidosDAO.getNumDoc(billClientId);
                    //pedidosDAO.create2(tp.getID_THIRD_EMP_PROVIDER(),tp.getID_THIRD_DESTINITY(),tp.getID_STORE_PROVIDER(),new Double(0),new Double(0),new Double(0),new Long(62),new Long(86),tp.getID_THIRD_CLIENT(),tp.getID_STORE_CLIENT(),numdoc);
                    //Long billProviderID = pedidosDAO.getPkLast();
                    System.out.println("this is id: "+billClientId);
                    for(reorderProd rp: list){
                        try{

                            Long idpsC = pedidosDAO.getPsId(rp.getOWNBARCODE(),tp.getID_STORE_CLIENT());
                            pedidosDAO.createDetail(billClientId,rp.getCANTIDAD(),idpsC,new Double(rp.getCOSTO()/rp.getCANTIDAD()),rp.getPERCENT());
                            Long idDetail = pedidosDAO.getPkLastDetail();
                            //pedidosDAO.procedure(billClientId,idpsC);
                            //Long quantity = pedidosDAO.detailData(idDetail).getQUANTITY();
                            //Long idpsP = pedidosDAO.getPsId(pedidosDAO.getOwnBarCode(pedidosDAO.detailData(idDetail).getID_PRODUCT_STORE()),tp.getID_STORE_PROVIDER());
                            //pedidosDAO.createDetail(billProviderID,quantity,idpsP,new Double(0),new Double(0));

                        }catch(Exception e2){
                            Response+=rp.getOWNBARCODE()+",";
                        }
                    }
                    pedidosDAO.procedure2(billClientId);

                }

            }
            System.out.println("ENVIO DE PEDIDOS TERMINADO");
            return Response;

        }catch (Exception e){
            System.out.println(new TechnicalException(messages_error(e.toString())));
            Response = e.toString();
            return Response;
        }
    }


    public Either<IException, Long> create(Long id_third_employee,
                                          Long id_third,
                                          Long id_store,
                                          Double totalprice,
                                          Double subtotal,
                                          Double tax,
                                          Long id_bill_state,
                                          Long id_bill_type,
                                          Long id_third_destinity,
                                          Long id_store_cliente) {
        try {
            pedidosDAO.create( id_third_employee,
                               id_third,
                               id_store,
                               totalprice,
                               subtotal,
                               tax,
                               id_bill_state,
                               id_bill_type,
                               id_third_destinity,
                               id_store_cliente);
            Long validator = pedidosDAO.getPkLast();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> create2(Long id_third_employee,
                                           Long id_third,
                                           Long id_store,
                                           Double totalprice,
                                           Double subtotal,
                                           Double tax,
                                           Long id_bill_state,
                                           Long id_bill_type,
                                           Long id_third_destinity,
                                           Long id_store_cliente,
                                            String num_doc) {
        try {
            pedidosDAO.create2( id_third_employee,
                    id_third,
                    id_store,
                    totalprice,
                    subtotal,
                    tax,
                    id_bill_state,
                    id_bill_type,
                    id_third_destinity,
                    id_store_cliente,
                    num_doc);
            Long validator = pedidosDAO.getPkLast();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long> createDetail(Long id_bill,
                                                 Long cantidad,
                                                 Long id_ps,
                                                 Double price,
                                                 Double tax) {
        try {
            pedidosDAO.createDetail( id_bill,
                    cantidad,
                    id_ps,
                    price,
                    tax);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> getPrice(Long id_ps) {
        try {
            List<Long> listPrices = pedidosDAO.getPrice(id_ps);
            Long smallest = listPrices.get(0);
            System.out.println(smallest);
            for(Long elem: listPrices){
                if(elem<=smallest){
                    smallest = elem;
                }
            }
            Long validator = smallest;
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }





    public Either<IException, Long>  addNotesBill(String notas,Long idbc,Long idbp) {
        try {
            pedidosDAO.addNotesBill( notas, idbc, idbp);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long> putInventoryBill(Long quantity,
                                                     Long id_product_store) {
        try {
            pedidosDAO.putInventoryBill( quantity,
                                         id_product_store,
                                         pedidosDAO.getStorage(id_product_store));
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> getpsid(String code, Long id_store) {
        try {
            return Either.right(pedidosDAO.getPsId(code, id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> putInventoryBill2(Long quantity,
                                                     Long id_product_store) {
        try {
            pedidosDAO.putInventoryBill2( quantity,
                    id_product_store,
                    pedidosDAO.getStorage(id_product_store));
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> getTax(Long id_product_store) {
        try {
            Long validator = pedidosDAO.getTax(id_product_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, BillToUpdate> getBillIdByIdStore(Long idstore,
                                                       Long idstoreclient,
                                                       String numpedido) {
        try {
            BillToUpdate validator = pedidosDAO.getBillIdByIdStore(idstore,idstoreclient,numpedido);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> updateBillState(Long billstate,
                                                    Long billid) {
        try {
            pedidosDAO.updateBillState(billstate,billid);
            return Either.right(new Long(1));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, Long> updateBillTotal(Long total,
                                                    Long idbil) {
        try {
            pedidosDAO.updateBillTotal(total, idbil);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }






    public Either<IException, Long> updateBillTotal2(Long idbil) {
        try {

            List<Long> billList = new ArrayList<>();
            billList.add(idbil);
            Double total = new Double(0);
            Double subtotal = new Double(0);
            int tax = 0;

            List<DetallePedido> details = pedidosDAO.getDetallesPedidos2(billList);

            for(DetallePedido detail: details){
                total += detail.getCOSTO()*detail.getCANTIDAD();
                subtotal+= detail.getCOSTO()*detail.getCANTIDAD();
            }

            pedidosDAO.updateBillTotalandSubtotal(idbil);
            Long validator = new Long (idbil);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }




    public Either<IException, String> printPickingPDF(PickingConsolidadoPdfDTO pedido) {





        Document documento = new Document(PageSize.A4, 0f, 0f, 35f, 0f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        DecimalFormat df = new DecimalFormat("###.##");
        try{
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);

            Font fontH1 = new Font(HELVETICA, 6, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 4, Font.NORMAL);
            Font fontH3 = new Font(HELVETICA, 6, Font.BOLD);
            Font fontH4 = new Font(HELVETICA, 8, Font.NORMAL);
            Font fontH5 = new Font(HELVETICA,  10,Font.BOLD);
            Font fontH6 = new Font(HELVETICA, 8, Font.BOLD);

            String nombrePdf = "picking.pdf";

            FileOutputStream ficheroPdf = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/remisiones/"+ nombrePdf);
            //FileOutputStream ficheroPdf = new FileOutputStream("./"+ nombrePdf);

            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));



            //DEFINO UNA CELDA VACIA PARA USO DE SALTO DE LINEA
            PdfPCell cellblanks =new PdfPCell(new Phrase(" "));
            cellblanks.setBorder(PdfPCell.NO_BORDER);
            cellblanks.setHorizontalAlignment(Element.ALIGN_CENTER);



            //DEFINO LA TABLA PRINCIPAL
            PdfPTable table = new PdfPTable(1);


            //INGRESO EL ICONO
            try {
                //Image img = Image.getInstance("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/logos/" + pedido.getLogo() + ".jpg");
                Image img = Image.getInstance("./"+pedido.getLogo() +".jpg");
                img.scaleAbsolute(200f, 200f);
                PdfPCell imagen = new PdfPCell(img, true);
                imagen.setBorder(PdfPCell.NO_BORDER);
                imagen.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(imagen);
            } catch (Exception e) {
                System.out.println(e.toString());
            }



            //DEFINO LA TABLA QUE CONTIENE EL TITULO DEL DOCUMENTO
            PdfPTable tablaTitulo = new PdfPTable(1);
            PdfPCell cellTitulo =new PdfPCell(new Phrase("Documento de picking Consolidado",fontH3));
            cellTitulo.setBorder(PdfPCell.NO_BORDER);
            cellTitulo.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaTitulo.addCell(cellTitulo);
            tablaTitulo.addCell(cellblanks);
            tablaTitulo.setKeepTogether(false);
            tablaTitulo.setSplitLate(false);
            PdfPCell titleCell = new PdfPCell(tablaTitulo);
            titleCell.setBorder(PdfPCell.NO_BORDER);
            titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(titleCell);


            //DEFINO LA TABLA QUE CONTIENE LOS COSTOS Y PRECIOS TOTALES DEL DOCUMENTO
            PdfPTable tablaTotales = new PdfPTable(2);
            PdfPCell cellTotalesCostosTitulo =new PdfPCell(new Phrase("Costo Total",fontH5));
            cellTotalesCostosTitulo.setBorder(PdfPCell.NO_BORDER);
            cellTotalesCostosTitulo.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaTotales.addCell(cellTotalesCostosTitulo);
            PdfPCell cellTotalesPreciosTitulo =new PdfPCell(new Phrase("Precio Total",fontH5));
            cellTotalesPreciosTitulo.setBorder(PdfPCell.NO_BORDER);
            cellTotalesPreciosTitulo.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaTotales.addCell(cellTotalesPreciosTitulo);
            PdfPCell cellTotalesCostos =new PdfPCell(new Phrase(format.format(roundAvoid(pedido.getcostoTotal(),2))+"",fontH4));
            cellTotalesCostos.setBorder(PdfPCell.NO_BORDER);
            cellTotalesCostos.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaTotales.addCell(cellTotalesCostos);
            PdfPCell cellTotalesPrecios =new PdfPCell(new Phrase(format.format(roundAvoid(pedido.getprecioTotal(),2))+"",fontH4));
            cellTotalesPrecios.setBorder(PdfPCell.NO_BORDER);
            cellTotalesPrecios.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaTotales.addCell(cellTotalesPrecios);
            tablaTotales.addCell(cellblanks);
            tablaTotales.addCell(cellblanks);
            tablaTotales.setKeepTogether(false);
            tablaTotales.setSplitLate(false);
            PdfPCell titleTotals = new PdfPCell(tablaTotales);
            titleTotals.setBorder(PdfPCell.NO_BORDER);
            titleTotals.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(titleTotals);


            PdfPTable tablaDetalles2 = new PdfPTable(5);
            tablaDetalles2.setWidths(new int[]{3,1,1,1,1});

            PdfPCell titleDetalle1 =new PdfPCell(new Phrase("PRODUCTO",fontH6));
            titleDetalle1.setBorder(PdfPCell.BOTTOM);
            titleDetalle1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetalles2.addCell(titleDetalle1);

            PdfPCell titleDetalle3 =new PdfPCell(new Phrase("PRESENTACION",fontH6));
            titleDetalle3.setBorder(PdfPCell.BOTTOM);
            titleDetalle3.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetalles2.addCell(titleDetalle3);

            PdfPCell titleDetalle4 =new PdfPCell(new Phrase("CANTIDAD",fontH6));
            titleDetalle4.setBorder(PdfPCell.BOTTOM);
            titleDetalle4.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetalles2.addCell(titleDetalle4);

            PdfPCell titleDetalle5 =new PdfPCell(new Phrase("COSTO",fontH6));
            titleDetalle5.setBorder(PdfPCell.BOTTOM);
            titleDetalle5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetalles2.addCell(titleDetalle5);

            PdfPCell titleDetalle6 =new PdfPCell(new Phrase("COSTO TOTAL",fontH6));
            titleDetalle6.setBorder(PdfPCell.BOTTOM);
            titleDetalle6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetalles2.addCell(titleDetalle6);

            tablaDetalles2.setKeepTogether(false);
            tablaDetalles2.setSplitLate(false);
            PdfPCell cellDetailTable2 = new PdfPCell(tablaDetalles2);
            cellDetailTable2.setBorder(PdfPCell.NO_BORDER);
            cellDetailTable2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cellDetailTable2);


            //DEFINO EL CICLO PARA RECORRER LOS FABRICANTES
            for(FabricanteDTO temporal : pedido.getlist()){
                PdfPTable tablaFabTemp = new PdfPTable(1);


                //DEFINO LA CELDA QUE INCLUIRA EL TITULO DE LA MARCA
                PdfPCell titleMarca =new PdfPCell(new Phrase("FABRICANTE: "+temporal.getmarca().toUpperCase(),fontH3));
                titleMarca.setBorder(PdfPCell.TOP);
                titleMarca.setBorder(PdfPCell.BOTTOM);
                titleMarca.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaFabTemp.addCell(titleMarca);



                PdfPTable tablaDetalles = new PdfPTable(5);
                tablaDetalles.setWidths(new int[]{3,1,1,1,1});


                //DEFINO EL CICLO PARA LOS DETALLES DE LA MARCA
                for(DetallePedidoDTO detail: temporal.getdetalles()){

                    PdfPCell Detalle1 =new PdfPCell(new Phrase(detail.getPRODUCTO(),fontH2));
                    Detalle1.setBorder(PdfPCell.NO_BORDER);
                    Detalle1.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tablaDetalles.addCell(Detalle1);

                    PdfPCell Detalle3 =new PdfPCell(new Phrase(detail.getPRESENTACION(),fontH1));
                    Detalle3.setBorder(PdfPCell.NO_BORDER);
                    Detalle3.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tablaDetalles.addCell(Detalle3);

                    PdfPCell Detalle4 =new PdfPCell(new Phrase(detail.getCANTIDAD()+"",fontH1));
                    Detalle4.setBorder(PdfPCell.NO_BORDER);
                    Detalle4.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tablaDetalles.addCell(Detalle4);

                    PdfPCell Detalle5 =new PdfPCell(new Phrase(format.format(roundAvoid(detail.getCOSTO(),2))+"",fontH1));
                    Detalle5.setBorder(PdfPCell.NO_BORDER);
                    Detalle5.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tablaDetalles.addCell(Detalle5);

                    PdfPCell Detalle6 =new PdfPCell(new Phrase(format.format(roundAvoid(detail.getCOSTOTOTAL(),2))+"",fontH1));
                    Detalle6.setBorder(PdfPCell.NO_BORDER);
                    Detalle6.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tablaDetalles.addCell(Detalle6);

                }


                tablaDetalles.setKeepTogether(false);
                tablaDetalles.setSplitLate(false);
                PdfPCell cellDetailTable = new PdfPCell(tablaDetalles);
                cellDetailTable.setBorder(PdfPCell.NO_BORDER);
                cellDetailTable.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaFabTemp.addCell(cellDetailTable);



                //DEFINO LA TABLA QUE CONTIENE LOS VALORES DE COSTO Y PRECIO DE LA MARCA
                PdfPTable tablaCostoPrecioTemp = new PdfPTable(2);
                PdfPCell titleCostoMarca =new PdfPCell(new Phrase("COSTO DE "+temporal.getmarca().toUpperCase()+":",fontH3));
                titleCostoMarca.setBorder(PdfPCell.BOTTOM);
                titleCostoMarca.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaCostoPrecioTemp.addCell(titleCostoMarca);
                PdfPCell titlePrecioMarca =new PdfPCell(new Phrase("PRECIO DE "+temporal.getmarca().toUpperCase()+":",fontH3));
                titlePrecioMarca.setBorder(PdfPCell.BOTTOM);
                titlePrecioMarca.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaCostoPrecioTemp.addCell(titlePrecioMarca);
                PdfPCell CostoMarca =new PdfPCell(new Phrase(format.format(roundAvoid(temporal.getcostoTotal(),2))+"",fontH1));
                CostoMarca.setBorder(PdfPCell.BOTTOM);
                CostoMarca.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaCostoPrecioTemp.addCell(CostoMarca);
                PdfPCell PrecioMarca =new PdfPCell(new Phrase(format.format(roundAvoid(temporal.getprecioTotal(),2))+"",fontH1));
                PrecioMarca.setBorder(PdfPCell.BOTTOM);
                PrecioMarca.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaCostoPrecioTemp.addCell(PrecioMarca);
                tablaCostoPrecioTemp.setKeepTogether(false);
                tablaCostoPrecioTemp.setSplitLate(false);
                PdfPCell cellCosto = new PdfPCell(tablaCostoPrecioTemp);
                cellCosto.setBorder(PdfPCell.NO_BORDER);
                cellCosto.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaFabTemp.addCell(cellCosto);



                tablaFabTemp.setKeepTogether(false);
                tablaFabTemp.setSplitLate(false);
                PdfPCell tempCell = new PdfPCell(tablaFabTemp);
                tempCell.setBorder(PdfPCell.NO_BORDER);
                tempCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(tempCell);

            }


            table.setKeepTogether(false);
            table.setSplitLate(false);
            documento.add(table);
            response = nombrePdf;

        }catch(Exception e){
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }

    }
    public Either<IException, String> printPDF3(PdfPedidosDTO pedidosDTO){
        Document documento = new Document(PageSize.A4, 0f, 0f, 35f, 0f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.


            String docNum = pedidosDTO.getdocumento();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 12, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 12, Font.BOLD);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);
            String cliente = pedidosDTO.getcliente().replaceAll(" ","_");
            String nombrePdf = "pickingg.pdf";
            FileOutputStream ficheroPdf = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/remisiones/"+ nombrePdf);
            //FileOutputStream ficheroPdf = new FileOutputStream("./"+ nombrePdf);

            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));



            PdfPCell cellblanks =new PdfPCell(new Phrase(" "));
            cellblanks.setBorder(PdfPCell.NO_BORDER);
            cellblanks.setHorizontalAlignment(Element.ALIGN_CENTER);


            PdfPTable tablaDatos = new PdfPTable(1);
            PdfPCell cell1 =new PdfPCell(new Phrase("Documento de picking Consolidado",fontH3));
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDatos.addCell(cell1);
            tablaDatos.addCell(cellblanks);




            PdfPTable tablaDetails = new PdfPTable(5);

            PdfPCell cellprod = new PdfPCell(new Phrase("Producto",fontH2));
            cellprod.setBorder(PdfPCell.BOTTOM);
            cellprod.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(cellprod);

            PdfPCell celcat = new PdfPCell(new Phrase("Categoria",fontH2));
            celcat.setBorder(PdfPCell.BOTTOM);
            celcat.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celcat);

            PdfPCell celcant = new PdfPCell(new Phrase("Cantidad",fontH2));
            celcant.setBorder(PdfPCell.BOTTOM);
            celcant.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celcant);

            PdfPCell cellcost = new PdfPCell(new Phrase("Costo",fontH2));
            cellcost.setBorder(PdfPCell.BOTTOM);
            cellcost.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(cellcost);

            PdfPCell celltotcost = new PdfPCell(new Phrase("Costo Total",fontH2));
            celltotcost.setBorder(PdfPCell.BOTTOM);
            celltotcost.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celltotcost);

            List<PdfPedidoDetailDTO> detail_list = pedidosDTO.getdetail_list();

            for(int i=0; i<detail_list.size(); i++){

                PdfPedidoDetailDTO element = detail_list.get(i);


                PdfPCell cellprod2 = new PdfPCell(new Phrase(element.getproducto(),fontH1));
                cellprod2.setBorder(PdfPCell.NO_BORDER);
                cellprod2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(cellprod2);

                PdfPCell celcat2 = new PdfPCell(new Phrase(element.getcategoria(),fontH1));
                celcat2.setBorder(PdfPCell.NO_BORDER);
                celcat2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celcat2);

                PdfPCell celcant2 = new PdfPCell(new Phrase(element.getcantidad()+"",fontH1));
                celcant2.setBorder(PdfPCell.NO_BORDER);
                celcant2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celcant2);

                PdfPCell cellcost2 = new PdfPCell(new Phrase(element.getcosto()+"",fontH1));
                cellcost2.setBorder(PdfPCell.NO_BORDER);
                cellcost2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(cellcost2);

                PdfPCell celltotcost2 = new PdfPCell(new Phrase(element.getcostototal()+"",fontH1));
                celltotcost2.setBorder(PdfPCell.NO_BORDER);
                celltotcost2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celltotcost2);

            }


            PdfPTable table = new PdfPTable(1);
            tablaDatos.setKeepTogether(false);
            tablaDatos.setSplitLate(false);
            PdfPCell hed = new PdfPCell(tablaDatos);
            hed.setBorder(PdfPCell.NO_BORDER);
            hed.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed);

            tablaDetails.setSplitLate(false);
            tablaDetails.setKeepTogether(false);
            PdfPCell hed3 = new PdfPCell(tablaDetails);
            hed3.setBorder(PdfPCell.NO_BORDER);
            hed3.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed3);

            table.setKeepTogether(false);
            table.setSplitLate(false);

            documento.add(table);





            response = nombrePdf;

        }catch(Exception e){
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }












    public Either<IException, String> printPDF2(PdfPedidosDTO pedidosDTO){
        Document documento = new Document(PageSize.A4, 0f, 0f, 35f, 0f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.


            String docNum = pedidosDTO.getdocumento();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 12, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 12, Font.BOLD);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);
            String cliente = pedidosDTO.getcliente().replaceAll(" ","_");
            String nombrePdf = docNum+pedidosDTO.getdocumento_cliente()+cliente+"pickingi.pdf";
            FileOutputStream ficheroPdf = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/remisiones/"+ nombrePdf);
            //FileOutputStream ficheroPdf = new FileOutputStream("./"+ nombrePdf);

            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));



            PdfPCell cellblanks =new PdfPCell(new Phrase(" "));
            cellblanks.setBorder(PdfPCell.NO_BORDER);
            cellblanks.setHorizontalAlignment(Element.ALIGN_CENTER);


            PdfPTable tablaDatos = new PdfPTable(1);
            PdfPCell cell1 =new PdfPCell(new Phrase("Documento de picking individual: "+pedidosDTO.getdocumento(),fontH3));
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDatos.addCell(cell1);
            tablaDatos.addCell(cellblanks);


            PdfPTable tablaHeader1 = new PdfPTable(2);
            PdfPCell cellTel = new PdfPCell(new Phrase(pedidosDTO.getcorreo(),fontH2));
            cellTel.setBorder(PdfPCell.NO_BORDER);
            cellTel.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader1.addCell(cellTel);
            PdfPCell celldir = new PdfPCell(new Phrase(pedidosDTO.getdireccion(),fontH2));
            celldir.setBorder(PdfPCell.NO_BORDER);
            celldir.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader1.addCell(celldir);
            tablaHeader1.addCell(cellblanks);
            tablaHeader1.addCell(cellblanks);


            PdfPTable tablaHeader2 = new PdfPTable(3);
            PdfPCell cellcliente = new PdfPCell(new Phrase(pedidosDTO.getcliente(),fontH1));
            cellcliente.setBorder(PdfPCell.NO_BORDER);
            cellcliente.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(cellcliente);
            PdfPCell cellFecha = new PdfPCell(new Phrase(""+pedidosDTO.getfecha(),fontH1));
            cellFecha.setBorder(PdfPCell.NO_BORDER);
            cellFecha.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(cellFecha);
            PdfPCell cellDocCliente = new PdfPCell(new Phrase(pedidosDTO.getdocumento_cliente(),fontH1));
            cellDocCliente.setBorder(PdfPCell.NO_BORDER);
            cellDocCliente.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(cellDocCliente);



            tablaHeader2.addCell(cellblanks);
            tablaHeader2.addCell(cellblanks);
            tablaHeader2.addCell(cellblanks);



            PdfPTable tablaDetails = new PdfPTable(5);

            PdfPCell cellprod = new PdfPCell(new Phrase("Producto",fontH2));
            cellprod.setBorder(PdfPCell.BOTTOM);
            cellprod.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(cellprod);

            PdfPCell celcat = new PdfPCell(new Phrase("Categoria",fontH2));
            celcat.setBorder(PdfPCell.BOTTOM);
            celcat.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celcat);

            PdfPCell celcant = new PdfPCell(new Phrase("Cantidad",fontH2));
            celcant.setBorder(PdfPCell.BOTTOM);
            celcant.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celcant);

            PdfPCell cellcost = new PdfPCell(new Phrase("Costo",fontH2));
            cellcost.setBorder(PdfPCell.BOTTOM);
            cellcost.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(cellcost);

            PdfPCell celltotcost = new PdfPCell(new Phrase("Costo Total",fontH2));
            celltotcost.setBorder(PdfPCell.BOTTOM);
            celltotcost.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celltotcost);

            List<PdfPedidoDetailDTO> detail_list = pedidosDTO.getdetail_list();

            for(int i=0; i<detail_list.size(); i++){

                PdfPedidoDetailDTO element = detail_list.get(i);


                PdfPCell cellprod2 = new PdfPCell(new Phrase(element.getproducto(),fontH1));
                cellprod2.setBorder(PdfPCell.NO_BORDER);
                cellprod2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(cellprod2);

                PdfPCell celcat2 = new PdfPCell(new Phrase(element.getcategoria(),fontH1));
                celcat2.setBorder(PdfPCell.NO_BORDER);
                celcat2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celcat2);

                PdfPCell celcant2 = new PdfPCell(new Phrase(element.getcantidad()+"",fontH1));
                celcant2.setBorder(PdfPCell.NO_BORDER);
                celcant2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celcant2);

                PdfPCell cellcost2 = new PdfPCell(new Phrase(element.getcosto()+"",fontH1));
                cellcost2.setBorder(PdfPCell.NO_BORDER);
                cellcost2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(cellcost2);

                PdfPCell celltotcost2 = new PdfPCell(new Phrase(element.getcostototal()+"",fontH1));
                celltotcost2.setBorder(PdfPCell.NO_BORDER);
                celltotcost2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celltotcost2);

            }


            PdfPTable table = new PdfPTable(1);
            tablaDatos.setKeepTogether(false);
            tablaDatos.setSplitLate(false);
            PdfPCell hed = new PdfPCell(tablaDatos);
            hed.setBorder(PdfPCell.NO_BORDER);
            hed.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed);

            tablaHeader1.setKeepTogether(false);
            tablaHeader1.setSplitLate(false);
            PdfPCell hed1 = new PdfPCell(tablaHeader1);
            hed1.setBorder(PdfPCell.NO_BORDER);
            hed1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed1);

            tablaHeader2.setSplitLate(false);
            tablaHeader2.setKeepTogether(false);
            PdfPCell hed2 = new PdfPCell(tablaHeader2);
            hed2.setBorder(PdfPCell.NO_BORDER);
            hed2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed2);

            tablaDetails.setSplitLate(false);
            tablaDetails.setKeepTogether(false);
            PdfPCell hed3 = new PdfPCell(tablaDetails);
            hed3.setBorder(PdfPCell.NO_BORDER);
            hed3.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed3);

            table.setKeepTogether(false);
            table.setSplitLate(false);

            documento.add(table);





            response = nombrePdf;

        }catch(Exception e){
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }









    public Either<IException, String> printPDF(PdfPedidosDTO pedidosDTO){
        Document documento = new Document(PageSize.A4, 0f, 0f, 35f, 0f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        try{ // Se crea el OutputStream para el fichero donde queremos dejar el pdf.


            String docNum = pedidosDTO.getdocumento();
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);
            Font fontH1 = new Font(HELVETICA, 12, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 12, Font.BOLD);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);
            String cliente = pedidosDTO.getcliente().replaceAll(" ","_");
            String nombrePdf = docNum+pedidosDTO.getdocumento_cliente()+cliente+".pdf";
            FileOutputStream ficheroPdf = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/remisiones/"+ nombrePdf);
            //FileOutputStream ficheroPdf = new FileOutputStream("./"+ nombrePdf);

            // Se asocia el documento al OutputStream y se indica que el espaciado entre
            // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));



            PdfPCell cellblanks =new PdfPCell(new Phrase(" "));
            cellblanks.setBorder(PdfPCell.NO_BORDER);
            cellblanks.setHorizontalAlignment(Element.ALIGN_CENTER);


            PdfPTable tablaDatos = new PdfPTable(1);
            PdfPCell cell1 =new PdfPCell(new Phrase("Documento de remision: "+pedidosDTO.getdocumento(),fontH3));
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDatos.addCell(cell1);
            tablaDatos.addCell(cellblanks);


            PdfPTable tablaHeader1 = new PdfPTable(2);
            PdfPCell cellTel = new PdfPCell(new Phrase(pedidosDTO.getcorreo(),fontH2));
            cellTel.setBorder(PdfPCell.NO_BORDER);
            cellTel.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader1.addCell(cellTel);
            PdfPCell celldir = new PdfPCell(new Phrase(pedidosDTO.getdireccion(),fontH2));
            celldir.setBorder(PdfPCell.NO_BORDER);
            celldir.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader1.addCell(celldir);
            tablaHeader1.addCell(cellblanks);
            tablaHeader1.addCell(cellblanks);


            PdfPTable tablaHeader2 = new PdfPTable(3);
            PdfPCell cellcliente = new PdfPCell(new Phrase(pedidosDTO.getcliente(),fontH1));
            cellcliente.setBorder(PdfPCell.NO_BORDER);
            cellcliente.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(cellcliente);
            PdfPCell cellFecha = new PdfPCell(new Phrase(""+pedidosDTO.getfecha(),fontH1));
            cellFecha.setBorder(PdfPCell.NO_BORDER);
            cellFecha.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(cellFecha);
            PdfPCell cellDocCliente = new PdfPCell(new Phrase(pedidosDTO.getdocumento_cliente(),fontH1));
            cellDocCliente.setBorder(PdfPCell.NO_BORDER);
            cellDocCliente.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(cellDocCliente);
            PdfPCell celltotal = new PdfPCell(new Phrase("Total: "+pedidosDTO.gettotal(),fontH1));
            celltotal.setBorder(PdfPCell.NO_BORDER);
            celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(celltotal);
            PdfPCell cellsubtotal = new PdfPCell(new Phrase("Subtotal: "+pedidosDTO.getsubtotal(),fontH1));
            cellsubtotal.setBorder(PdfPCell.NO_BORDER);
            cellsubtotal.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(cellsubtotal);
            PdfPCell celltax = new PdfPCell(new Phrase("Tax: "+pedidosDTO.gettax(),fontH1));
            celltax.setBorder(PdfPCell.NO_BORDER);
            celltax.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaHeader2.addCell(celltax);
            tablaHeader2.addCell(cellblanks);
            tablaHeader2.addCell(cellblanks);
            tablaHeader2.addCell(cellblanks);


            PdfPTable tablaDetails = new PdfPTable(5);

            PdfPCell cellprod = new PdfPCell(new Phrase("Producto",fontH2));
            cellprod.setBorder(PdfPCell.BOTTOM);
            cellprod.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(cellprod);

            PdfPCell celcat = new PdfPCell(new Phrase("Categoria",fontH2));
            celcat.setBorder(PdfPCell.BOTTOM);
            celcat.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celcat);

            PdfPCell celcant = new PdfPCell(new Phrase("Cantidad",fontH2));
            celcant.setBorder(PdfPCell.BOTTOM);
            celcant.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celcant);

            PdfPCell cellcost = new PdfPCell(new Phrase("Costo",fontH2));
            cellcost.setBorder(PdfPCell.BOTTOM);
            cellcost.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(cellcost);

            PdfPCell celltotcost = new PdfPCell(new Phrase("Costo Total",fontH2));
            celltotcost.setBorder(PdfPCell.BOTTOM);
            celltotcost.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celltotcost);

            List<PdfPedidoDetailDTO> detail_list = pedidosDTO.getdetail_list();

            for(int i=0; i<detail_list.size(); i++){

                PdfPedidoDetailDTO element = detail_list.get(i);

                PdfPCell cellprod2 = new PdfPCell(new Phrase(element.getproducto(),fontH1));
                cellprod2.setBorder(PdfPCell.NO_BORDER);
                cellprod2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(cellprod2);

                PdfPCell celcat2 = new PdfPCell(new Phrase(element.getcategoria(),fontH1));
                celcat2.setBorder(PdfPCell.NO_BORDER);
                celcat2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celcat2);

                PdfPCell celcant2 = new PdfPCell(new Phrase(element.getcantidad()+"",fontH1));
                celcant2.setBorder(PdfPCell.NO_BORDER);
                celcant2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celcant2);

                PdfPCell cellcost2 = new PdfPCell(new Phrase(element.getcosto()+"",fontH1));
                cellcost2.setBorder(PdfPCell.NO_BORDER);
                cellcost2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(cellcost2);

                PdfPCell celltotcost2 = new PdfPCell(new Phrase(element.getcostototal()+"",fontH1));
                celltotcost2.setBorder(PdfPCell.NO_BORDER);
                celltotcost2.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celltotcost2);

            }


            PdfPTable table = new PdfPTable(1);
            tablaDatos.setKeepTogether(false);
            tablaDatos.setSplitLate(false);
            PdfPCell hed = new PdfPCell(tablaDatos);
            hed.setBorder(PdfPCell.NO_BORDER);
            hed.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed);

            tablaHeader1.setKeepTogether(false);
            tablaHeader1.setSplitLate(false);
            PdfPCell hed1 = new PdfPCell(tablaHeader1);
            hed1.setBorder(PdfPCell.NO_BORDER);
            hed1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed1);

            tablaHeader2.setSplitLate(false);
            tablaHeader2.setKeepTogether(false);
            PdfPCell hed2 = new PdfPCell(tablaHeader2);
            hed2.setBorder(PdfPCell.NO_BORDER);
            hed2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed2);

            tablaDetails.setSplitLate(false);
            tablaDetails.setKeepTogether(false);
            PdfPCell hed3 = new PdfPCell(tablaDetails);
            hed3.setBorder(PdfPCell.NO_BORDER);
            hed3.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed3);

            table.setKeepTogether(false);
            table.setSplitLate(false);

            documento.add(table);





            response = nombrePdf;

        }catch(Exception e){
            response = e.toString();
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> getQuantity(Long idps) {
        try {
            return Either.right(pedidosDAO.getQuantity(idps));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> procedure2(Long idvehiculo,
                                               Long idbventa,
                                               Long idbcompra,
                                               Long idbremision,
                                               Long idstorecliente,
                                               Long idstoreproveedor,
                                               Long idbpedidoproveedor) {
        try {
            pedidosDAO.procedureVe(
                    idvehiculo,
                    idbventa,
                    idbcompra,
                    idbremision,
                    idstorecliente,
                    idstoreproveedor,
                    idbpedidoproveedor);
            Long validator = new Long (1);
            System.out.println("IDVENTA: "+idbventa);
            System.out.println("IDCOMPRA: "+idbcompra);
            System.out.println("IDREMISION: "+idbremision);
            System.out.println("IDPEDIDO: "+idbpedidoproveedor);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }







    public Either<IException, Long> procedimiento(Long idbventa,Long idcompra,Long idremision) {
        try {
            CallableStatement stmt;
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@sysware.cw2rwsqphepo.us-east-1.rds.amazonaws.com:1521:ORCL","facturacion724","tienda724");
            stmt = con.prepareCall("select facturacion724.GET_VALIDAR_DETAIL_BILL_ZERO(?) from dual");
            stmt.setLong(1,idbventa );
            //stmt.setString(2, bb);
            ResultSet rs = stmt.executeQuery();
            Long validator = new Long(0);
            while (rs.next()) {
                validator = rs.getLong(1);
            }

            rs.close();
            stmt.close();
            con.close();
           if(validator==0){
            pedidosDAO.updateBillIf0(idbventa);
            pedidosDAO.updateBillIf0(idcompra);
            pedidosDAO.updateBillIf0(idremision);
           }
            return Either.right(validator);

        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }







}
