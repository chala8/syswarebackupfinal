package com.name.business.entities;

import java.util.List;

/**
 * Created by trossky on 14/07/17.
 */
public class ThirdLocationComplete {

    private  Long id_third_location;
    private  Long id_third;
    private  Long id_location;
    private  CommonThird state;
    private List<Directory> directory;

    public ThirdLocationComplete(Long id_third_location, Long id_third, Long id_location, CommonThird state, List<Directory> directory) {
        this.id_third_location = id_third_location;
        this.id_third = id_third;
        this.id_location = id_location;
        this.state = state;
        this.directory = directory;
    }

    public Long getId_third_location() {
        return id_third_location;
    }

    public void setId_third_location(Long id_third_location) {
        this.id_third_location = id_third_location;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_location() {
        return id_location;
    }

    public void setId_location(Long id_location) {
        this.id_location = id_location;
    }

    public CommonThird getState() {
        return state;
    }

    public void setState(CommonThird state) {
        this.state = state;
    }

    public List<Directory> getDirectory() {
        return directory;
    }

    public void setDirectory(List<Directory> directory) {
        this.directory = directory;
    }
}
