package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.City;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CityMapper implements ResultSetMapper<City>  {

    @Override
    public City map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new City(
                resultSet.getString("ID_CITY"),
                resultSet.getString("CITY_NAME")
        );
    }
}
