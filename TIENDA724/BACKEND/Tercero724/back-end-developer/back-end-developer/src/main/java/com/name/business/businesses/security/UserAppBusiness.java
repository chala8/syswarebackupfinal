package com.name.business.businesses.security;

import com.name.business.DAOs.security.UserAppDAO;

public class UserAppBusiness {
    private UserAppDAO userAppDAO;

    public UserAppBusiness(UserAppDAO userAppDAO) {
        this.userAppDAO = userAppDAO;
    }


    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = userAppDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

}
