package com.name.business.representations;

import java.util.List;

/**
 * Created by luis on 24/07/17.
 */
public class ContactTranslatorDTO {
    private List<String> contact;
    private List<Integer> priority;

    public ContactTranslatorDTO(List<String> contact, List<Integer> priority) {
        this.contact = contact;
        this.priority = priority;
    }

    public List<String> getContact() {
        return contact;
    }

    public void setContact(List<String> contact) {
        this.contact = contact;
    }

    public List<Integer> getPriority() {
        return priority;
    }

    public void setPriority(List<Integer> priority) {
        this.priority = priority;
    }
}
