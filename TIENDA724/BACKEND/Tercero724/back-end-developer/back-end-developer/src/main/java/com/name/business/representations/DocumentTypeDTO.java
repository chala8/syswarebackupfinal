package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.CommonThird;
import org.w3c.dom.DocumentType;

/**
 * Created by Niki on 17/07/2017.
 */
public class DocumentTypeDTO {

    private String name;
    private Common_ThirdDTO common_ThirdDTO;

    @JsonCreator
    public DocumentTypeDTO(@JsonProperty("name") String name,
                           @JsonProperty("state") Common_ThirdDTO common_thirdDTO) {
        this.common_ThirdDTO = common_thirdDTO;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Common_ThirdDTO getCommon_ThirdDTO() {
        return common_ThirdDTO;
    }

    public void setCommon_ThirdDTO(Common_ThirdDTO common_ThirdDTO) {
        this.common_ThirdDTO = common_ThirdDTO;
    }
}
