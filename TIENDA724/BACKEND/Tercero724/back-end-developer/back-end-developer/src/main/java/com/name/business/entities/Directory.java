package com.name.business.entities;

import java.util.List;

/**
 * Created by trossky on 14/07/17.
 */
public class Directory {

    private Long id_directory;
    private String address;
    private String country;
    private String city;
    private String webpage;
    private CommonThird state;
    private List<Mail> mails;
    private List<Phone> phones;

    public Directory(Long id_directory, String address, String country, String city, String webpage, CommonThird state, List<Mail> mails, List<Phone> phones) {
        this.id_directory = id_directory;
        this.address = address;
        this.country = country;
        this.city = city;
        this.webpage = webpage;
        this.state = state;
        this.mails = mails;
        this.phones = phones;
    }

    public List<Mail> getMails() {
        return mails;
    }

    public void setMails(List<Mail> mails) {
        this.mails = mails;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }



    public CommonThird getState() {
        return state;
    }

    public void setState(CommonThird state) {
        this.state = state;
    }

    public Long getId_directory() {
        return id_directory;
    }

    public void setId_directory(Long id_directory) {
        this.id_directory = id_directory;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getWebpage() {
        return webpage;
    }

    public void setWebpage(String webpage) {
        this.webpage = webpage;
    }
}
