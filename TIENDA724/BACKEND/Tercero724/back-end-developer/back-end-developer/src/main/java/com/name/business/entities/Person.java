package com.name.business.entities;

import java.util.Date;

/**
 * Created by trossky on 14/07/17.
 */
public class Person {
    private Long id_person;

    private String first_name;
    private String second_name;
    private String first_lastname;
    private String second_lastname;
    private Date birthday;

    private CommonBasicInfo info;
    private CommonThird state;
    private Object directory;
    private Object employee;
    private Object UUID;

    public Person(Long id_person, String first_name, String second_name, String first_lastname, String second_lastname, Date birthday, CommonBasicInfo info, CommonThird state, Object directory, Object employee, Object UUID) {
        this.id_person = id_person;
        this.first_name = first_name;
        this.second_name = second_name;
        this.first_lastname = first_lastname;
        this.second_lastname = second_lastname;
        this.birthday = birthday;
        this.info = info;
        this.state = state;
        this.directory = directory;
        this.employee = employee;
        this.UUID = UUID;
    }

    public Long getId_person() {
        return id_person;
    }

    public void setId_person(Long id_person) {
        this.id_person = id_person;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public String getFirst_lastname() {
        return first_lastname;
    }

    public void setFirst_lastname(String first_lastname) {
        this.first_lastname = first_lastname;
    }

    public String getSecond_lastname() {
        return second_lastname;
    }

    public void setSecond_lastname(String second_lastname) {
        this.second_lastname = second_lastname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public CommonBasicInfo getInfo() {
        return info;
    }

    public void setInfo(CommonBasicInfo info) {
        this.info = info;
    }

    public CommonThird getState() {
        return state;
    }

    public void setState(CommonThird state) {
        this.state = state;
    }

    public Object getDirectory() {
        return directory;
    }

    public void setDirectory(Object directory) {
        this.directory = directory;
    }

    public Object getEmployee() {
        return employee;
    }

    public void setEmployee(Object employee) {
        this.employee = employee;
    }

    public Object getUUID() {
        return UUID;
    }

    public void setUUID(Object UUID) {
        this.UUID = UUID;
    }
}
