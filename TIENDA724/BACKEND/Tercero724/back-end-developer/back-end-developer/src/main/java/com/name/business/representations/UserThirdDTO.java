package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by trossky on 14/07/17.
 */
public class UserThirdDTO {


    private String UUID;
    private Common_ThirdDTO  state;

    @JsonCreator
    public UserThirdDTO(@JsonProperty("UUID") String UUID,
                        @JsonProperty("state") Common_ThirdDTO state) {

        this.UUID = UUID;
        this.state = state;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public Common_ThirdDTO getState() {
        return state;
    }

    public void setState(Common_ThirdDTO state) {
        this.state = state;
    }
}
