package com.name.business.mappers;


import com.name.business.entities.DocumentPerson;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DocumentPersonMapper implements ResultSetMapper<DocumentPerson>{

    @Override
    public DocumentPerson map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DocumentPerson(
                resultSet.getString("FULLNAME"),
                resultSet.getString("DOCUMENT_TYPE"),
                resultSet.getString("DOCUMENT_NUMBER"),
                resultSet.getString("CITY"),
                resultSet.getString("ADDRESS"),
                resultSet.getString("PHONE"),
                resultSet.getLong("ID_PERSON")
        );
    }
}