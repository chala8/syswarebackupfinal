package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.Mail;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MailMapper implements ResultSetMapper<Mail> {

    @Override
    public Mail map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Mail(
           resultSet.getLong("ID_MAIL"),
           resultSet.getLong("ID_DIRECTORY"),
           resultSet.getString("MAIL"),
           resultSet.getInt("PRIORITY"),
                new CommonThird(
                        resultSet.getLong("ID_COMMON_THIRD"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_MAIL"),
                        resultSet.getDate("MODIFY_MAIL")
                )
        );
    }
}
