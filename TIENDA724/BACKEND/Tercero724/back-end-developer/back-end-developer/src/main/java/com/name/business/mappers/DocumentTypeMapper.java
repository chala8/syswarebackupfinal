package com.name.business.mappers;

import com.name.business.entities.CommonBasicInfo;
import com.name.business.entities.CommonThird;
import com.name.business.entities.DocumentType;
import com.name.business.representations.Common_ThirdDTO;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Niki on 17/07/2017.
 */
public class DocumentTypeMapper implements ResultSetMapper<DocumentType> {

    @Override
    public DocumentType map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {

        return new DocumentType(
                resultSet.getLong("ID_DOCUMENT_TYPE"),
                resultSet.getString("NAME"),
                new CommonThird(
                        resultSet.getLong("ID_COMMON_THIRD"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_DATE"),
                        resultSet.getDate("MODIFY_DATE")
                )
        );
    }
}