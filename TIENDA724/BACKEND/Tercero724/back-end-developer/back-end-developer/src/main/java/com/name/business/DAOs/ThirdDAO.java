package com.name.business.DAOs;

import com.name.business.entities.Third;
import com.name.business.entities.City;
import com.name.business.entities.BillThird;
import com.name.business.entities.DocumentPerson;
import com.name.business.mappers.ThirdMapper;
import com.name.business.mappers.CityMapper;
import com.name.business.mappers.DocumentPersonMapper;
import com.name.business.mappers.BillThirdMapper;
import com.name.business.representations.ThirdDTO;
import com.name.business.representations.complex.ThirdCompleteDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(ThirdMapper.class)
public interface ThirdDAO {

    /**
     *
     * @param thirdDTO
     * @param id_application
     * @return
     */
    @SqlUpdate("INSERT INTO THIRD " +
            " ( ID_THIRD_TYPE, ID_COMMON_BASICINFO, " +
            " ID_THIRD_FATHER, ID_PERSON, ID_COMMON_THIRD,ID_APPLICATION ) VALUES " +
            " (:th.id_third_type, :th.id_common_basicinfo, " +
            " :th.id_third_father, :th.id_person, :th.id_common_third, :id_application)")
    int create(@BindBean("th") ThirdDTO thirdDTO,@Bind("id_application") Long id_application);

    /**
     *
     * @param id_third
     * @param id_third_father
     * @param id_person
     * @param id_third_type
     * @param th_type_name
     * @param id_c_th_type
     * @param state_c_th_type
     * @param creation_c_th_type
     * @param modify_c_th_type
     * @param id_cb_third
     * @param id_typedoc_third
     * @param doc_third
     * @param typedoc_third
     * @param name_third
     * @param logo
     * @param id_common_third
     * @param state_third
     * @param creation_third
     * @param modify_third
     * @return
     */
    @SqlQuery("SELECT ID_THIRD, ID_THIRD_FATHER,ID_PERSON, ID_THIRD_TYPE,TH_TYPE_NAME,ID_C_TH_TYPE,STATE_C_TH_TYPE,CREATION_C_TH_TYPE,MODIFY_C_TH_TYPE, " +
            "  ID_CB_THIRD,ID_TYPEDOC_THIRD,DOC_THIRD,TYPEDOC_THIRD,NAME_THIRD,LOGO,ID_CTH_DOCT,STATE_DOCT,CREATION_DOCT,MODIFY_DOCT,ID_COMMON_THIRD,STATE_THIRD,CREATION_THIRD,MODIFY_THIRD " +
            " FROM V_THIRD V_T " +
            "WHERE (V_T.ID_THIRD=:ID_THIRD OR :ID_THIRD  IS NULL ) AND" +
            "      (V_T.ID_THIRD_FATHER=:ID_THIRD_FATHER OR :ID_THIRD_FATHER IS NULL ) AND" +
            "      (V_T.ID_PERSON=:ID_PERSON OR :ID_PERSON IS NULL ) AND" +
            "      (V_T.ID_THIRD_TYPE=:ID_THIRD_TYPE OR :ID_THIRD_TYPE IS NULL ) AND" +
            "      (V_T.TH_TYPE_NAME LIKE :TH_TYPE_NAME OR :TH_TYPE_NAME IS NULL ) AND" +
            "      (V_T.ID_C_TH_TYPE =:ID_C_TH_TYPE OR :ID_C_TH_TYPE IS NULL ) AND" +
            "      (V_T.STATE_C_TH_TYPE =:STATE_C_TH_TYPE OR :STATE_C_TH_TYPE IS NULL ) AND" +
            "      (V_T.CREATION_C_TH_TYPE=:CREATION_C_TH_TYPE OR :CREATION_C_TH_TYPE  IS NULL ) AND" +
            "      (V_T.MODIFY_C_TH_TYPE =:MODIFY_C_TH_TYPE  OR :MODIFY_C_TH_TYPE IS NULL ) AND" +
            "      (V_T.ID_CB_THIRD = :ID_CB_THIRD OR :ID_CB_THIRD IS NULL ) AND" +
            "      (V_T.ID_TYPEDOC_THIRD=:ID_TYPEDOC_THIRD  OR :ID_TYPEDOC_THIRD IS NULL ) AND" +
            "      (V_T.DOC_THIRD=:DOC_THIRD OR :DOC_THIRD IS NULL ) AND" +
            "      (V_T.TYPEDOC_THIRD LIKE :TYPEDOC_THIRD OR :TYPEDOC_THIRD IS NULL ) AND" +
            "      (V_T.NAME_THIRD LIKE :NAME_THIRD OR :NAME_THIRD IS NULL ) AND" +
            "      (V_T.LOGO =:LOGO OR :LOGO IS NULL ) AND" +
            "      (V_T.ID_COMMON_THIRD =:ID_COMMON_THIRD OR :ID_COMMON_THIRD  IS NULL ) AND" +
            "      (V_T.STATE_THIRD =:STATE_THIRD OR :STATE_THIRD  IS NULL ) AND " +
            "      (V_T.CREATION_THIRD =:CREATION_THIRD OR :CREATION_THIRD IS NULL ) AND" +
            "      (V_T.MODIFY_THIRD=:MODIFY_THIRD OR :MODIFY_THIRD IS NULL )")
    List<Third> read(@Bind("ID_THIRD") Long id_third, @Bind("ID_THIRD_FATHER") Long id_third_father,@Bind("ID_PERSON") Long id_person, @Bind("ID_THIRD_TYPE") Long id_third_type, @Bind("TH_TYPE_NAME") String th_type_name, @Bind("ID_C_TH_TYPE")Long id_c_th_type,
                     @Bind("STATE_C_TH_TYPE") Integer state_c_th_type, @Bind("CREATION_C_TH_TYPE") Date creation_c_th_type , @Bind("MODIFY_C_TH_TYPE") Date modify_c_th_type,
                     @Bind("ID_CB_THIRD") Long id_cb_third, @Bind("ID_TYPEDOC_THIRD") Long id_typedoc_third, @Bind("DOC_THIRD") String doc_third, @Bind("TYPEDOC_THIRD") String typedoc_third, @Bind("NAME_THIRD") String name_third,
                     @Bind("LOGO")String logo, @Bind("ID_COMMON_THIRD") Long id_common_third, @Bind("STATE_THIRD") Integer state_third, @Bind("CREATION_THIRD")Date creation_third , @Bind("MODIFY_THIRD")Date modify_third);

    /**
     *
     * @param idThird
     * @param third
     * @return
     */
    @SqlUpdate("UPDATE THIRD SET " +
            "ID_THIRD_TYPE = :thirdDTO.id_third_type, " +
            "ID_THIRD_FATHER = :thirdDTO.id_third_father, " +
            "WHERE " +
            "ID_THIRD = :idThird")
    int update(@Bind("idThird") Long idThird, @BindBean("thirdDTO") ThirdCompleteDTO third);

    /**
     *
     * @param idThird
     * @return
     */
    @SqlQuery("SELECT ID_COMMON_THIRD FROM THIRD WHERE ID_THIRD = :idThird")
    List<Long> delete(@Bind("idThird") Long idThird);

    /***
     *
     * @param id_third
     * @param third
     * @return
     */
    @SqlUpdate("DELETE FROM THIRD " +
            "WHERE " +
            "  ( ID_THIRD = :id_third OR :id_third IS NULL ) AND " +
            "  ( ID_THIRD_TYPE = :th.third_type OR :th.third_type IS NULL ) AND  " +
            "  ( ID_COMMON_BASICINFO = :th.id_common_basicinfo OR :th.id_common_basicinfo IS NULL ) AND " +
            "  ( ID_THIRD_FATHER = :th.id_third_father OR th.id_third_father IS NULL ) AND " +
            "  ( ID_PERSON = :th.id_person OR :th.id_person IS NULL ) AND " +
            "  ( ID_COMMON_THIRD =  :th.id_common_third OR :th.id_common_third IS NULL ) ")
    int delete(@Bind("id_third") Long id_third,@BindBean("th") ThirdDTO third);

    /**
     *
     * @return
     */
    @SqlQuery("SELECT ID_THIRD " +
            "FROM THIRD " +
            "WHERE ID_THIRD IN (SELECT MAX(ID_THIRD) FROM THIRD)")
    Long getPkLast();

    @SqlQuery("SELECT count(ID_THIRD) FROM THIRD WHERE ID_THIRD=:id")
    Integer getValidatorID(@Bind("id") Long id_user_third);




    @RegisterMapper(DocumentPersonMapper.class)
    @SqlQuery(" select p.id_person, fullname, name document_type,document_number,city,address,ph.phone\n" +
            "   from tercero724.person p, tercero724.common_basicinfo cbi, tercero724.document_type d, tercero724.directory dir, tercero724.phone ph\n" +
            "   where p.id_common_basicinfo=cbi.id_common_basicinfo and cbi.id_document_type=d.id_document_type and p.id_directory=dir.id_directory\n" +
            "   and dir.id_directory=ph.id_directory and priority=1\n" +
            "   and document_number=:document_number ")
    List<DocumentPerson> getPerson(@Bind("document_number") String document_number);


    @SqlQuery("SELECT ID_DIRECTORY FROM DIRECTORY WHERE ID_DIRECTORY IN (SELECT MAX(ID_DIRECTORY) FROM DIRECTORY)")
    Long getLastDirectory();

    @SqlUpdate(" UPDATE THIRD SET ID_DIRECTORY = :id_directory WHERE ID_THIRD = :id_third ")
    void updateDir(@Bind("id_third") Long id_third,@Bind("id_directory") Long id_directory);


    @RegisterMapper(BillThirdMapper.class)
    @SqlQuery(" select dt.name document_type, document_number,fullname,count(*) NumVentas,sum(totalprice) TotalVentas ,address, phone, mail\n" +
            "               from facturacion724.bill b,tercero724.third t,tercero724.common_basicinfo cb,tercero724.document_type dt\n" +
            "               ,tercero724.directory dir,tercero724.phone ph,tercero724.mail ma\n" +
            "               where b.id_third_destinity=t.id_third and t.id_common_basicinfo=cb.id_common_basicinfo and cb.id_document_type=dt.id_document_type\n" +
            "                 and t.id_directory=dir.id_directory(+) and dir.id_directory=ph.id_directory(+) and dir.id_directory=ma.id_directory(+)\n" +
            "                 and id_bill_state=1 and id_third_destinity is not null\n" +
            "                 and id_bill_type=:bill_type and purchase_date between :date1 and (:date2+0.99) and b.id_third=:id_third\n" +
            "               group by dt.name,document_number,fullname,address, phone, mail\n" +
            "               UNION\n" +
            "               select 'N/A' document_type, 'N/A' document_number,'Ocasional' fullname,count(*) NumVentas,sum(totalprice) TotalVentas , null address, null phone, null mail\n" +
            "               from facturacion724.bill b\n" +
            "               where \n" +
            "                 id_bill_state=1\n" +
            "                 and id_bill_type=:bill_type and purchase_date between :date1 and (:date2+0.99)\n" +
            "                 and b.id_third=:id_third and id_third_destinity is null order by 4 desc")
    List<BillThird> getThirdsForBill(@Bind("id_third") Long id_third,
                                     @Bind("date1") Date date1,
                                     @Bind("date2") Date date2,
                                     @Bind("bill_type") Long bill_type);




    @RegisterMapper(CityMapper.class)
    @SqlQuery(" select ID_CITY, CITY_NAME from TERCERO724.CITY order by 1")
    List<City> getCities();

    @SqlUpdate(" UPDATE DIRECTORY SET ID_CITY = :id_city \n" +
               " WHERE ID_DIRECTORY = :id_directory")
    Integer updateDirectoryCity(@Bind("id_directory") String id_directory,
                                   @Bind("id_city") String id_city);


    @SqlQuery(" SELECT ID_DIRECTORY FROM THIRD WHERE THIRD.ID_THIRD = :id_third ")
    String getDirID(@Bind("id_third") Long id_third);

}
