package com.name.business.sanitizers;

import com.name.business.entities.CommonThird;

import java.util.Date;

import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class CommonThirdSanitize {

    public static CommonThird toCommonThird( Long id_common_third, Integer state_third, Date creation_third, Date modify_third){

            CommonThird  commonThird= new CommonThird(formatoLongSql(id_common_third) , formatoIntegerSql(state_third), formatoDateSql(creation_third), formatoDateSql(modify_third));
            return commonThird;

    }
}
