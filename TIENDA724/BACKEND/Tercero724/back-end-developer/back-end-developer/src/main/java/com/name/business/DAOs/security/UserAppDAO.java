package com.name.business.DAOs.security;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;

public interface UserAppDAO {

    @SqlQuery("SELECT count(ID_USER_APP) FROM USER_APP WHERE ID_USER_APP=:id")
    Integer getValidatorID(@Bind("id") Long id_user_third);

}
