package com.name.business.entities;

public class DirectoryData {


    private String ADDRESS;
    private String CITY;


    public DirectoryData(
                           String CITY,
                           String ADDRESS) {
        this.CITY = CITY;
        this.ADDRESS = ADDRESS;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }


}
