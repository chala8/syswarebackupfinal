package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.DirectoryData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DirectoryDataMapper implements ResultSetMapper<DirectoryData>  {

    @Override
    public DirectoryData map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new DirectoryData(
                resultSet.getString("CITY"),
                resultSet.getString("ADDRESS")
        );
    }
}
