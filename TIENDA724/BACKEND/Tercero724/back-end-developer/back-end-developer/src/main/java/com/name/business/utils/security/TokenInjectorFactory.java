package com.name.business.utils.security;



import com.name.business.entities.Aplicacion;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.name.business.entities.Token;
import org.glassfish.hk2.api.Factory;


import static com.name.business.utils.constans.K.TOKEN_ATTRIBUTE;


public class TokenInjectorFactory implements Factory<Token> {

    private HttpServletRequest httpRequest;

    @Inject
    public TokenInjectorFactory(HttpServletRequest httpRequest) {



        this.httpRequest = httpRequest;
    }

    @Override
    public Token provide() {
        Token aplicacion = (Token) httpRequest.getAttribute(AuthorizationUri.TOKEN_ATTRIBUTE);

        return aplicacion;
    }

    @Override
    public void dispose(Token aplicacion) {

    }
}
