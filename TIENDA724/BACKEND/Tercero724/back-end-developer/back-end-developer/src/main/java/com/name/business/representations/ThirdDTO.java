package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by trossky on 14/07/17.
 */
public class ThirdDTO {

    private Long id_third_type;
    private Long id_common_third;
    private Long id_common_basicinfo;
    private Long id_person;

    private Long id_third_father;

    public ThirdDTO(@JsonProperty("id_third_type") Long id_third_type,
                    @JsonProperty("id_common_third") Long id_common_third,
                    @JsonProperty("id_common_basicinfo") Long id_common_basicinfo,
                    @JsonProperty("id_person") Long id_person,
                    @JsonProperty("id_third_father") Long id_third_father) {
        this.id_third_type = id_third_type;
        this.id_common_third = id_common_third;
        this.id_common_basicinfo = id_common_basicinfo;
        this.id_person = id_person;
        this.id_third_father = id_third_father;
    }

    public Long getId_third_type() {
        return id_third_type;
    }

    public void setId_third_type(Long id_third_type) {
        this.id_third_type = id_third_type;
    }

    public Long getId_common_third() {
        return id_common_third;
    }

    public void setId_common_third(Long id_common_third) {
        this.id_common_third = id_common_third;
    }

    public Long getId_common_basicinfo() {
        return id_common_basicinfo;
    }

    public void setId_common_basicinfo(Long id_common_basicinfo) {
        this.id_common_basicinfo = id_common_basicinfo;
    }

    public Long getId_person() {
        return id_person;
    }

    public void setId_person(Long id_person) {
        this.id_person = id_person;
    }

    public Long getId_third_father() {
        return id_third_father;
    }

    public void setId_third_father(Long id_third_father) {
        this.id_third_father = id_third_father;
    }
}
