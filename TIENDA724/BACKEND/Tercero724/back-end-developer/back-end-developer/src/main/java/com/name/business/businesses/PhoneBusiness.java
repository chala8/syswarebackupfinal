package com.name.business.businesses;

import com.name.business.DAOs.PhoneDAO;
import com.name.business.entities.Phone;
import com.name.business.representations.ContactTranslatorDTO;
import com.name.business.representations.MailDTO;
import com.name.business.representations.PhoneDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.translators.StringTranslator.contactPhoneTranslator;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

/**
 * Created by trossky on 14/07/17.
 */
public class PhoneBusiness {

    private PhoneDAO phoneDAO;
    private CommonThirdBusiness commonThirdBusiness;

    public PhoneBusiness(PhoneDAO phoneDAO, CommonThirdBusiness commonThirdBusiness) {
        this.phoneDAO = phoneDAO;
        this.commonThirdBusiness = commonThirdBusiness;
    }

    public Either<IException, String> createPhone(Long id_directory, List<PhoneDTO> phoneDTOList, Long id_aplicacion) {
        List<String> msn = new ArrayList<>();
        List<Long> id_common_third_list=new ArrayList<>();
        try {
            if (validatorDirectoryID(id_directory).equals(false)){
                msn.add("It ID Directory does not exist register on  Directory, probably it has bad ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            System.out.println("|||||||||||| Starting creation Phone  ||||||||||||||||| ");
            if (!phoneDTOList.equals(null) || phoneDTOList.size()>0 ) {

                ContactTranslatorDTO contactTranslatorDTO = contactPhoneTranslator(phoneDTOList);
                for (PhoneDTO phoneDTO: phoneDTOList){
                    Long value= new Long(0);
                    Either<IException, Long> commonThirdEither = commonThirdBusiness.createCommonThird(phoneDTO.getState());
                    if (commonThirdEither.isRight()){
                        value = commonThirdEither.right().value();

                    }
                    if (value<=0){
                        id_common_third_list.add(null);
                    } else {
                        id_common_third_list.add(value);
                    }

                }

                phoneDAO.bulkPhone(id_directory, contactTranslatorDTO.getContact(), contactTranslatorDTO.getPriority(),id_common_third_list, id_aplicacion);
                msn.add("Register "+id_common_third_list.size()+" phones");
                return Either.right(msn.get(0));
            } else {
                msn.add("It does not recognize Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException,List<Phone>> getPhones(Long id_phone, String phone, Integer priority, Long id_directory, Long id_common_third, Integer state, Date creation_phone, Date modify_phone) {
        try{

            List<Phone> read = phoneDAO.read(formatoLongSql(id_phone),formatoStringSql(phone),formatoIntegerSql(priority),formatoLongSql(id_directory),
                    formatoLongSql(id_common_third),formatoIntegerSql(state),formatoDateSql(creation_phone),formatoDateSql(modify_phone));

            return Either.right(read);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param idPhone
     * @param PhoneDTO
     * @return
     */
    public Either<IException, Long> updatePhone(Long idPhone, PhoneDTO PhoneDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting UPDATE DocumentType ||||||||||||||||| ");
            if (idPhone != null && idPhone > 0) {

                if (validatorID(idPhone).equals(false)){
                    msn.add("It ID Phone does not exist register on  Phone, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<Phone> phones = phoneDAO.read(idPhone, null, null, null,null,null,null,null);


                if(phones.size() >0){
                    Phone actualPhone = phones.get(0);
                // Validate Phone inserted, if null, set the name to the actual in the database, if not it is formatted to sql
                if (PhoneDTO.getPhone() == null || PhoneDTO.getPhone().length() < 7) {
                    PhoneDTO.setPhone(actualPhone.getPhone());
                } else {
                    PhoneDTO.setPhone(formatoStringSql(PhoneDTO.getPhone()));
                }

                // Validate priority inserted, if null, set the name to the actual in the database, if not it is formatted to sql
                if (PhoneDTO.getPriority() == null || PhoneDTO.getPriority() < 0) {
                    PhoneDTO.setPriority(actualPhone.getPriority());
                } else {
                    PhoneDTO.setPriority(formatoIntegerSql(PhoneDTO.getPriority()));
                }

                phoneDAO.update(formatoLongSql(idPhone), PhoneDTO);

                //TO DO when Phone has common third variable
                Long idCommon = phoneDAO.delete(idPhone).get(0);
                commonThirdBusiness.updateCommonThird(idCommon, PhoneDTO.getState());

                return Either.right(idPhone);}
                else{
                    msn.add("It does not exist ID phone, probably it is a wrong id.");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
            } else {
                msn.add("It does not recognize ID phone, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param idPhone
     * @return
     */
    public Either<IException, Long> deletePhone(Long idPhone) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting DELETE (Change State) DocumentType ||||||||||||||||| ");
            if (idPhone != null && idPhone > 0) {

                if (validatorID(idPhone).equals(false)){
                    msn.add("It ID Phone does not exist register on  Phone, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                Long idCommon = phoneDAO.delete(formatoLongSql(idPhone)).get(0);

                //TO DO when Phone has common Third implemented
                commonThirdBusiness.deleteCommonThird(idCommon);
                return Either.right(idPhone);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = phoneDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    private Boolean validatorDirectoryID(Long id_directory) {
        Integer validator = 0;
        try {
            validator = phoneDAO.validatorDirectoryID(id_directory);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }
}
