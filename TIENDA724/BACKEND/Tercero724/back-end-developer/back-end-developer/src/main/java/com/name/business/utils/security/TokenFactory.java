package com.name.business.utils.security;

import com.name.business.entities.UserThird;
import com.name.business.utils.constans.K;

import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import org.joda.time.DateTime;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Random;

import static com.name.business.utils.exeptions.ManagementException.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.exeptions.ManagementException.INVALID_USER;


public class TokenFactory {
    private static final int TOKEN_CHARACTER_LIMIT=20;
    private static final String SECRET_KEY_FACTORY_ALGORITHM="PBKDF2WithHmacSHA1";


    /**
     * Calculates and returns the hash of  a given string using PBXDF2WithHmacSHA1
     * */
    private static final Either<IException,String> getHashFromString(String string){
        Either<IException,String> hashFromStringEither=null;
        byte[] security= new byte[16];
        Random random= new Random();
        random.nextBytes(security);
        KeySpec keySpec=new PBEKeySpec("password".toCharArray(),security,65536,128);
        try {
            SecretKeyFactory secretKeyFactory= SecretKeyFactory.getInstance(SECRET_KEY_FACTORY_ALGORITHM);
            byte[] hash=secretKeyFactory.generateSecret(keySpec).getEncoded();
            String stringHash=new BigInteger(1,hash).toString(16);

            return Either.right(stringHash);

        }catch (NoSuchAlgorithmException | InvalidKeySpecException e){
            return Either.left(new TechnicalException(K.INTERNAL_ERROR_MESSAGE));
        }
    }
    /**
     * Generates the authentication token.
     *
     * */
    public static final Either<IException,String> getKey_Token(UserThird user, String password){
        Either<IException,String> KEY_TOKEN_Either=null;
        if ((user!=null) && !user.getUUID().isEmpty()&& !password.isEmpty() ){

            long currentMillis= DateTime.now().getMillis();
            String BASE_FOR_KEY_TOKEN=currentMillis+user.getUUID()+password;

            Either<IException,String> hashFromStrigEither=getHashFromString(BASE_FOR_KEY_TOKEN);

            if (hashFromStrigEither.isRight()){

                String hashFromStrig=hashFromStrigEither.right().value();
                String GENERATED_KEY_TOKEN=hashFromStrig.substring(0,TOKEN_CHARACTER_LIMIT);

                /**             RETORNA EL TOKEN GENERADO          */
                return Either.right(GENERATED_KEY_TOKEN);
            }else{
                return Either.left(INTERNAL_ERROR_MESSAGE());
            }

        }else{
            return Either.left(INVALID_USER());
        }

    }



}
