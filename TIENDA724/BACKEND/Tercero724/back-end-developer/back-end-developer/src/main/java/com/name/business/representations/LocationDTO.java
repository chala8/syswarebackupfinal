package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.CommonThird;

public class LocationDTO {

    private Long id_third;
    private Long id_directory;
    private Common_ThirdDTO common_thirdDTO;

    @JsonCreator
    public LocationDTO(@JsonProperty("id_third") Long id_third,@JsonProperty("id_directory") Long id_directory,@JsonProperty("state") Common_ThirdDTO common_thirdDTO) {
        this.id_third = id_third;
        this.id_directory = id_directory;
        this.common_thirdDTO = common_thirdDTO;
    }

    public long getId_third() {
        return id_third;
    }

    public void setId_third(long id_third) {
        this.id_third = id_third;
    }

    public long getId_directory() {
        return id_directory;
    }

    public void setId_directory(long id_directory) {
        this.id_directory = id_directory;
    }

    public Common_ThirdDTO getCommon_thirdDTO() {
        return common_thirdDTO;
    }

    public void setCommon_thirdDTO(Common_ThirdDTO common_thirdDTO) {
        this.common_thirdDTO = common_thirdDTO;
    }
}
