package com.name.business.utils.security;


/**
 * Created by luis on 9/06/17.
 */
public class TokenGenerator {


    public static  String uuid()
    {
        return java.util.UUID.randomUUID().toString().replaceAll("-","");
    }

    //generamos un codigo de licencia a los usuarios para la app.
    public static String tokenGenerate( boolean isDached) {
        String codeTrial = uuid();
        String TOKEN="";
        int codeLength=codeTrial.length();
        for (int i = 1; i < codeLength; i++) {
            TOKEN=TOKEN+codeTrial.charAt(i);

            if ( i==6 && i<codeLength && isDached){
                TOKEN+=":";
            }else{
                if ( i> 8 && i%9==0 && i<codeLength && isDached){
                    TOKEN+="-";
                }
            }

        }
        return TOKEN.toUpperCase();
    }
}
