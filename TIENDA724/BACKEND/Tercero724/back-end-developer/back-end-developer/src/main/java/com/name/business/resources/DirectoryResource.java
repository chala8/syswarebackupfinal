package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.DirectoryBusiness;
import com.name.business.entities.Aplicacion;
import com.name.business.entities.Directory;
import com.name.business.entities.DirectoryData;
import com.name.business.entities.Token;
import com.name.business.representations.DirectoryDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/directories")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DirectoryResource {

    private DirectoryBusiness directoryBusiness;

    /**
     *
     * @param directoryBusiness
     */
    public DirectoryResource(DirectoryBusiness directoryBusiness) {
        this.directoryBusiness = directoryBusiness;
    }

    /**
     *
     * @param directoryDTO
     * @return
     */
    @POST
    @Timed
    public Response postDirectory(@Context Token token, DirectoryDTO directoryDTO,@QueryParam("id_app") Long id_app){
        Response response;

        if (id_app==null){
            id_app=token.getAplicacion().getId_aplicacion();
        }
        Either<IException, Long> mailEither = directoryBusiness.createDirectory(directoryDTO, id_app);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    /**
     *
     * @param id_directory
     * @param address
     * @param country
     * @param city
     * @param webpage
     * @param id_ct_directory
     * @param state_dir
     * @param creation_dir
     * @param modify
     * @return
     */
    @GET
    @Timed
    public Response getPersonResource(@Context Token token,@QueryParam("id_directory") Long id_directory, @QueryParam("address") String address,
                                      @QueryParam("country") String country,@QueryParam("city") String city,
                                      @QueryParam("webpage") String webpage,@QueryParam("id_ct_directory")  Long id_ct_directory,
                                      @QueryParam("state_dir") Integer state_dir, @QueryParam("creation_dir")Date creation_dir,
                                      @QueryParam("modify_directory") Date modify
            ){
        Response response;

        Either<IException, List<Directory>> allViewOffertsEither =directoryBusiness.getDirectory(id_directory,address,
                country,city, webpage,id_ct_directory,
                state_dir, creation_dir, modify);


        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param idDirectory
     * @param directoryDTO
     * @return
     */
    @Path("/{idDirectory}")
    @PUT
    @Timed
    public Response updateDirectoryResource(@Context Token token,@PathParam("idDirectory") Long idDirectory, DirectoryDTO directoryDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = directoryBusiness.updateDirectory(idDirectory, directoryDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param idDirectory
     * @return
     */
    @Path("/{idDirectory}")
    @DELETE
    @Timed
    public Response deleteDirectoryResource(@Context Token token,@PathParam("idDirectory") Long idDirectory){
        Response response;
        Either<IException, Long> allViewOffertsEither = directoryBusiness.deleteDirectory(idDirectory);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }



    @GET
    @Path("/generalDir")
    @Timed
    public Response getGeneralDirectoryData(@QueryParam("id_directory") Long id) {

        Response response;

        Either<IException, DirectoryData> getProductsEither = directoryBusiness.getGeneralDirectoryData(id);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/phone")
    @Timed
    public Response getPhone(@QueryParam("id_directory") Long id) {

        Response response;

        Either<IException, List<String>> getProductsEither = directoryBusiness.getPhone(id);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @GET
    @Path("/mail")
    @Timed
    public Response getMail(@QueryParam("id_directory") Long id) {

        Response response;

        Either<IException, List<String>> getProductsEither = directoryBusiness.getMail(id);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @PUT
    @Path("/phoneAndMail")
    @Timed
    public Response putphoneandmail(@QueryParam("id_directory") Long id,
                                    @QueryParam("phone") String phone,
                                    @QueryParam("mail") String mail,
                                    @QueryParam("address") String address) {

        Response response;

        Either<IException, Long> getProductsEither = directoryBusiness.putphoneandmail(id,phone,mail,address);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

}
