package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.Location;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LocationMapper  implements ResultSetMapper<Location> {
    @Override
    public Location map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Location(
        resultSet.getLong("ID_LOCATION"),
        resultSet.getLong("ID_THIRD"),
        resultSet.getLong("ID_DIRECTORY"),
                new CommonThird(
                        resultSet.getLong("ID_COMMON_THIRD"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_LOCATION"),
                        resultSet.getDate("MODIFY__LOCATION")
                )
        );
    }
}
