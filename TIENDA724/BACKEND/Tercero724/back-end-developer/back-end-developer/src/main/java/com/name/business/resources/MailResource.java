package com.name.business.resources;


import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.MailBusiness;
import com.name.business.entities.Aplicacion;
import com.name.business.entities.Mail;
import com.name.business.entities.Token;
import com.name.business.representations.MailDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Path("/mails")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MailResource {
    private MailBusiness mailBusiness;

    /**
     *
     * @param mailBusiness
     */
    public MailResource(MailBusiness mailBusiness) {
        this.mailBusiness = mailBusiness;
    }

    /**
     *
     * @param id_directory
     * @param mailDTO
     * @return
     */
    @POST
    @Timed
    public Response postMail(@Context Token token, @QueryParam("id_directory")Long id_directory, List<MailDTO> mailDTO,@QueryParam("id_app") Long id_app){
        Response response;

        if (id_app==null){
            id_app=token.getAplicacion().getId_aplicacion();
        }


        Either<IException, String> mailEither = mailBusiness.createMail(id_directory, mailDTO, id_app);

        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    /**
     *
     * @param id_mail
     * @param mail
     * @param priority
     * @param id_directory
     * @return
     */
    @GET
    @Timed
    public Response getPersonResource(@Context Token aplicacion,@QueryParam("id_mail") Long id_mail, @QueryParam("mail")String mail,
                                      @QueryParam("priority") Integer priority,
                                      @QueryParam("id_diretory") Long id_directory, @QueryParam("id_common_third") Long id_common_third,
                                      @QueryParam("state") Integer state, @QueryParam("creation_mail") Date creation_mail, @QueryParam("modify_mail")Date modify_mail){
        Response response;

        Either<IException, List<Mail>> allViewOffertsEither =mailBusiness.getMails(id_mail,mail,  priority, id_directory,id_common_third,state,creation_mail,modify_mail);


        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id_Mail}")
    @PUT
    @Timed
    public Response updateMailResource(@Context Token token,@PathParam("id_Mail") Long id_Mail, MailDTO mailDTO,@QueryParam("id_app") Long id_app){
        Response response;

        if (id_app==null){
            id_app=token.getAplicacion().getId_aplicacion();
        }
        Either<IException, Long> allViewOffertsEither = mailBusiness.updateMail(id_Mail, mailDTO,id_app);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id_Mail}")
    @DELETE
    @Timed
    public Response deleteMailResource(@Context Token token,@PathParam("id_Mail") Long id_Mail){
        Response response;
        Either<IException, Long> allViewOffertsEither = mailBusiness.deleteMailType(id_Mail);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param mail
     * @return
     */
    @Path("/validators/mails")
    @GET
    @Timed
    public Response getValidatorMAILByAPP(@Context Token token,@QueryParam("mail") String mail,
                                          @QueryParam("id_user_third") Long id_user_third,@QueryParam("id_app") Long id_app){
        Response response;

        if (id_app==null){
            id_app=token.getAplicacion().getId_aplicacion();
        }
        Either<IException, Boolean> allViewOffertsEither = mailBusiness.getValidatorMAILByAPP(mail,
                id_app,id_user_third);
        HashMap<String,Object> responseMap= new HashMap<>();


        if (allViewOffertsEither.isRight()){
            responseMap.put("response",allViewOffertsEither.right().value());
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(responseMap).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
