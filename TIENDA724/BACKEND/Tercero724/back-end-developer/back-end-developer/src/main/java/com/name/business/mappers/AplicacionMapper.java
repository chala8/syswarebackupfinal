package com.name.business.mappers;


import com.name.business.entities.Aplicacion;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class AplicacionMapper implements ResultSetMapper<Aplicacion> {
    @Override
    public Aplicacion map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Aplicacion(
                resultSet.getLong("ID_APPLICATION"),
                resultSet.getString("NAME"),
                resultSet.getString("DESCRIPTION"),
                resultSet.getString("VERSION"),
                resultSet.getString("CREATION_DATE"),
                resultSet.getString("UPDATE_DATE"),
                resultSet.getString("KEY_APPLICATION"),
                resultSet.getString("KEY_SERVER")
        );
    }
}
