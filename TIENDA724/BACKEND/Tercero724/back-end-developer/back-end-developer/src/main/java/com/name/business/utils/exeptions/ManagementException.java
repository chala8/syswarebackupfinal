package com.name.business.utils.exeptions;

import java.util.HashMap;

import static com.name.business.utils.constans.K.*;

/**
 * Se Adminsitran todas las Exepciones, a nivel de Cliente. Formularios o Formatos incoherentes.
 * Los errores que podria responder el API al Usuario
 */
public class ManagementException {




    //Una Excepcion Genral de un Try Catch
    public static IException catchException(Exception e) {

        return new TechnicalException(e.getMessage());

    }

    public static IException noFoundException(String object) {
        return new BussinessException(object+" not found");
    }

    public static HashMap<String, String> okException(String KEY, String VALUE) {
        return new HashMap<String,String>(){{put(KEY,VALUE);}};
    }

    public static HashMap<String, Long> okException(String KEY, long VALUE) {
        return new HashMap<String,Long>(){{put(KEY,new Long(VALUE));}};
    }



    public static IException acctionFailedException(String auth) {
        return new BussinessException(auth+" failed please try again");
    }
    public static  IException INTERNAL_ERROR_MESSAGE() {
        return new TechnicalException(INTERNAL_ERROR_MESSAGE);
    }
    public static  IException INVALID_USER() {
        return new BussinessException(INVALID_USER);
    }
    public static  IException INVALID_USER_PASS() {
        return new BussinessException(INVALID_USER_PASS);
    }
}
