package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Mail;
import com.name.business.entities.Phone;

import java.util.List;

public class DirectoryDTO {

    private String address;
    private String country;
    private String city;
    private String webpage;
    private Common_ThirdDTO common_thirdDTO;
    private List<PhoneDTO> phoneDTOList;
    private List<MailDTO> mailDTOList;

    @JsonCreator
    public DirectoryDTO(@JsonProperty("address") String address,
                        @JsonProperty("country") String country,
                        @JsonProperty("city") String city,
                        @JsonProperty("webpage") String webpage,
                        @JsonProperty("state") Common_ThirdDTO common_thirdDTO,
                        @JsonProperty("phones") List<PhoneDTO> phoneDTOList,
                        @JsonProperty("mails") List<MailDTO> mailDTOList) {
        this.address = address;
        this.country = country;
        this.city = city;
        this.webpage = webpage;
        this.common_thirdDTO = common_thirdDTO;
        this.phoneDTOList = phoneDTOList;
        this.mailDTOList = mailDTOList;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getWebpage() {
        return webpage;
    }

    public void setWebpage(String webpage) {
        this.webpage = webpage;
    }

    public Common_ThirdDTO getCommon_thirdDTO() {
        return common_thirdDTO;
    }

    public void setCommon_thirdDTO(Common_ThirdDTO common_thirdDTO) {
        this.common_thirdDTO = common_thirdDTO;
    }

    public List<PhoneDTO> getPhoneDTOList() {
        return phoneDTOList;
    }

    public void setPhoneDTOList(List<PhoneDTO> phoneDTOList) {
        this.phoneDTOList = phoneDTOList;
    }

    public List<MailDTO> getMailDTOList() {
        return mailDTOList;
    }

    public void setMailDTOList(List<MailDTO> mailDTOList) {
        this.mailDTOList = mailDTOList;
    }
}
