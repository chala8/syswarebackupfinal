package com.name.business.entities;

/**
 * Created by trossky on 14/07/17.
 */
public class Third {

    private Long id_third;
    private Long id_third_father;
    private Object profile;
    private ThirdType type;
    private CommonThird state;
    private CommonBasicInfo info;
    private Object directory;

    public Third(Long id_third, Long id_third_father, Object profile, ThirdType type, CommonThird state, CommonBasicInfo info, Object directory) {
        this.id_third = id_third;
        this.id_third_father = id_third_father;
        this.profile = profile;
        this.type = type;
        this.state = state;
        this.info = info;
        this.directory = directory;
    }

    public Object getDirectory() {
        return directory;
    }

    public void setDirectory(Object directory) {
        this.directory = directory;
    }

    public Object getProfile() {
        return profile;
    }

    public void setProfile(Object profile) {
        this.profile = profile;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_third_father() {
        return id_third_father;
    }

    public void setId_third_father(Long id_third_father) {
        this.id_third_father = id_third_father;
    }

    public ThirdType getType() {
        return type;
    }

    public void setType(ThirdType type) {
        this.type = type;
    }

    public CommonThird getState() {
        return state;
    }

    public void setState(CommonThird state) {
        this.state = state;
    }

    public CommonBasicInfo getInfo() {
        return info;
    }

    public void setInfo(CommonBasicInfo info) {
        this.info = info;
    }
}
