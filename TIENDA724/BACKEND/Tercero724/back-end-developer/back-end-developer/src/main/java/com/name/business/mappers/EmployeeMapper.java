package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.Employee;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeMapper implements ResultSetMapper<Employee> {
    @Override
    public Employee map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Employee(
         resultSet.getLong ("ID_EMPLOYEE"),
         resultSet.getDouble ("SALARY"),
                new CommonThird(
                        resultSet.getLong("ID_COMMON_THIRD"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_EMPLOYEE"),
                        resultSet.getDate("MODIFY_EMPLOYEE")
                )

        );
    }
}
