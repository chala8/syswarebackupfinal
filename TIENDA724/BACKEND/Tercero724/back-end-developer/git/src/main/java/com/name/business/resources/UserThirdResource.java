package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.UserThirdBusiness;
import com.name.business.entities.Aplicacion;
import com.name.business.entities.Token;
import com.name.business.entities.UserThird;
import com.name.business.representations.UserThirdDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserThirdResource {
    private UserThirdBusiness userThirdBusiness;

    /**
     *
     * @param userThirdBusiness
     */
    public UserThirdResource(UserThirdBusiness userThirdBusiness) {
        this.userThirdBusiness = userThirdBusiness;

    }

    /**
     *
     * @param id_person
     * @param userThirdDTO
     * @return
     */
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postUserThird(@Context Token token,@QueryParam("id_app")Long id_app,
                                  @QueryParam("id_person")Long id_person, UserThirdDTO userThirdDTO){
        Response response;
        if (id_app==null){
            if (token.getAplicacion()!=null){
                id_app=token.getAplicacion().getId_aplicacion();
            }
        }

        Either<IException, Long> allViewOffertsEither =userThirdBusiness.createUserThird(id_person, userThirdDTO,id_app);

        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_user_third
     * @param UUID
     * @param id_person
     * @param id_common_third
     * @param state
     * @param creation_user
     * @param modify_user
     * @return
     */
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getUserThirdResource(@Context Token token,@QueryParam("id_user_third")Long id_user_third, @QueryParam("uuid")String UUID,
                                         @QueryParam("id_person") Long id_person,@QueryParam("id_common_third")Long id_common_third,
                                         @QueryParam("state") Integer state, @QueryParam("creation_user")Date creation_user,
                                         @QueryParam("modify_user") Date modify_user){
        Response response;

        Either<IException, List<UserThird>> allViewOffertsEither =userThirdBusiness.getUserThird(id_user_third,UUID, id_person,
                                                                                    id_common_third,state, creation_user, modify_user);

        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_UserThird
     * @param userThirdDTO
     * @return
     *
     */
    @Path("/{id_UserThird}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateUserThirdResource(@Context Token token,@PathParam("id_UserThird") Long id_UserThird, UserThirdDTO userThirdDTO,@QueryParam("id_app") Long id_app){
        Response response;

        Either<IException, Long> allViewOffertsEither = userThirdBusiness.updateUserThird(id_UserThird,id_app, userThirdDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_UserThird
     * @return
     */
    @Path("/{id_UserThird}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deleteUserThirdResource(@Context Token token,@PathParam("id_UserThird") Long id_UserThird){
        Response response;
        Either<IException, Long> allViewOffertsEither = userThirdBusiness.deleteUserThird(id_UserThird);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param uuid
     * @return
     */
    @Path("/validators/uuid")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getValidatorUUIDByAPP(@Context Token token,@QueryParam("uuid") String uuid,
                                          @QueryParam("id_user_third") Long id_user_third,@QueryParam("id_app") Long id_app){
        Response response;

        if (id_app==null){
            id_app=token.getAplicacion().getId_aplicacion();
        }
        Either<IException, Boolean> allViewOffertsEither = userThirdBusiness.getValidatorUUIDByAPP(uuid,id_app,id_user_third);
        HashMap<String,Object> responseMap= new HashMap<>();


        if (allViewOffertsEither.isRight()){
            responseMap.put("response",allViewOffertsEither.right().value());
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(responseMap).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }


    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response actualizar_Thirdfather(@QueryParam("user")String user){
        Response response;

        Boolean allViewOffertsEither =userThirdBusiness.actualizar_Thirdfather(user);

        if (allViewOffertsEither){

            response=Response.status(Response.Status.OK).entity("1").build();
        }else {
            response= Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("0").build();
        }
        return response;
    }
}
