package com.name.business.entities;

/**
 * Created by trossky on 14/07/17.
 */
public class CommonBasicInfo {

    private Long id_common_basicinfo;
    private Long id_document_type;
    private String fullname;
    private String img;
    private String document_number;
    private String type_document;

    public CommonBasicInfo(Long id_common_basicinfo, String fullname, String img, String document_number, DocumentType documentType) {
        this.id_common_basicinfo = id_common_basicinfo;
        this.id_document_type = documentType.getId_document_type();
        this.fullname = fullname;
        this.img = img;
        this.document_number = document_number;
        this.type_document = documentType.getName();
    }

    public String getType_document() {
        return type_document;
    }

    public void setType_document(String type_document) {
        this.type_document = type_document;
    }

    public Long getId_common_basicinfo() {
        return id_common_basicinfo;
    }

    public void setId_common_basicinfo(Long id_common_basicinfo) {
        this.id_common_basicinfo = id_common_basicinfo;
    }

    public Long getId_document_type() {
        return id_document_type;
    }

    public void setId_document_type(Long id_document_type) {
        this.id_document_type = id_document_type;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDocument_number() {
        return document_number;
    }

    public void setDocument_number(String document_number) {
        this.document_number = document_number;
    }
}
