package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ThirdMapper implements ResultSetMapper<Third> {
    @Override
    public Third map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Third(
        resultSet.getLong("ID_THIRD"),
        resultSet.getLong("ID_THIRD_FATHER"),
        resultSet.getLong("ID_PERSON"),
                new ThirdType(
                        resultSet.getLong("ID_THIRD_TYPE"),
                        resultSet.getLong("ID_C_TH_TYPE"),
                        resultSet.getString("TH_TYPE_NAME"),
                        new CommonThird(
                                resultSet.getLong("ID_C_TH_TYPE"),
                                resultSet.getInt("STATE_C_TH_TYPE"),
                                resultSet.getDate("CREATION_C_TH_TYPE"),
                                resultSet.getDate("MODIFY_C_TH_TYPE")
                        )
                ),
                new CommonThird(
                        resultSet.getLong("ID_COMMON_THIRD"),
                        resultSet.getInt("STATE_THIRD"),
                        resultSet.getDate("CREATION_THIRD"),
                        resultSet.getDate("MODIFY_THIRD")
                ),
                new CommonBasicInfo(
                        resultSet.getLong("ID_CB_THIRD"),
                        resultSet.getString("NAME_THIRD"),
                        resultSet.getString("LOGO"),
                        resultSet.getString("DOC_THIRD"),
                        new DocumentType(
                                resultSet.getLong("ID_TYPEDOC_THIRD"),
                                resultSet.getString("TYPEDOC_THIRD"),
                                new CommonThird(
                                        resultSet.getLong("ID_CTH_DOCT"),
                                        resultSet.getInt("STATE_DOCT"),
                                        resultSet.getDate("CREATION_DOCT"),
                                        resultSet.getDate("MODIFY_DOCT")
                                )
                        )
                ),
                null
        );
    }
}
