package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Directory;
import com.name.business.entities.Employee;
import com.name.business.entities.UserThird;

import java.util.Date;

/**
 * Created by trossky on 14/07/17.
 */
public class PersonDTO {


    private String first_name;;

    private String second_name;

    private String first_lastname;

    private String second_lastname;
    private Date birthday;

    private CommonBasicInfoDTO commonBasicInfoDTO;
    private Common_ThirdDTO common_thirdDTO;
    private DirectoryDTO directoryDTO;
    private EmployeeDTO employee;
    private UserThirdDTO uuid;

    @JsonCreator
    public PersonDTO(@JsonProperty("first_name") String first_name,@JsonProperty("second_name") String second_name,
                     @JsonProperty("first_lastname") String first_lastname, @JsonProperty("second_lastname") String second_lastname,
                     @JsonProperty("birthday") Date birthday,@JsonProperty("info") CommonBasicInfoDTO commonBasicInfoDTO,
                     @JsonProperty("state") Common_ThirdDTO common_thirdDTO,@JsonProperty("directory") DirectoryDTO directoryDTO,
                     @JsonProperty("employee") EmployeeDTO employee,@JsonProperty("UUID") UserThirdDTO uuid) {
        this.first_name = first_name;
        this.second_name = second_name;
        this.first_lastname = first_lastname;
        this.second_lastname = second_lastname;
        this.birthday = birthday;
        this.commonBasicInfoDTO = commonBasicInfoDTO;
        this.common_thirdDTO = common_thirdDTO;
        this.directoryDTO = directoryDTO;
        this.employee=employee;
        this.uuid=uuid;
    }

    public CommonBasicInfoDTO getCommonBasicInfoDTO() {
        return commonBasicInfoDTO;
    }

    public EmployeeDTO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDTO employee) {
        this.employee = employee;
    }

    public UserThirdDTO getUuid() {
        return uuid;
    }

    public void setUuid(UserThirdDTO uuid) {
        this.uuid = uuid;
    }

    public void setCommonBasicInfoDTO(CommonBasicInfoDTO commonBasicInfoDTO) {
        this.commonBasicInfoDTO = commonBasicInfoDTO;
    }

    public Common_ThirdDTO getCommon_thirdDTO() {
        return common_thirdDTO;
    }

    public void setCommon_thirdDTO(Common_ThirdDTO common_thirdDTO) {
        this.common_thirdDTO = common_thirdDTO;
    }

    public DirectoryDTO getDirectoryDTO() {
        return directoryDTO;
    }

    public void setDirectoryDTO(DirectoryDTO directoryDTO) {
        this.directoryDTO = directoryDTO;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public String getFirst_lastname() {
        return first_lastname;
    }

    public void setFirst_lastname(String first_lastname) {
        this.first_lastname = first_lastname;
    }

    public String getSecond_lastname() {
        return second_lastname;
    }

    public void setSecond_lastname(String second_lastname) {
        this.second_lastname = second_lastname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
