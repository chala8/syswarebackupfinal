package com.name.business.representations;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AplicacionDTO {

    private String nombre;
    private String descripcion;
    private String version;
    private String KEY_APPLICATION;


    @JsonCreator
    public AplicacionDTO(@JsonProperty("name") String nombre,
                         @JsonProperty("description") String descripcion,
                         @JsonProperty("version") String version,
                         @JsonProperty("KEY_APPLICATION") String KEY_APPLICATION) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.version = version;
        this.KEY_APPLICATION=KEY_APPLICATION;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getKEY_APPLICATION() {
        return KEY_APPLICATION;
    }

    public void setKEY_APPLICATION(String KEY_APPLICATION) {
        this.KEY_APPLICATION = KEY_APPLICATION;
    }
}
