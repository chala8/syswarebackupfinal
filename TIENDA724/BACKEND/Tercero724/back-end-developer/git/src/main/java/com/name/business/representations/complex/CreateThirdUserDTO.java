package com.name.business.representations.complex;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class CreateThirdUserDTO {

    private String firstname;
    private String secondname;
    private String firstlastname;
    private String secondlastname;
    private Long iddocumenttype;
    private String docnumber;
    private String direccion;
    private String idcity;
    private String telefono;
    private String email;
    private Date birthDate;
    private Long gender;
    private double lat;
    private double lng;

    @JsonCreator
    public CreateThirdUserDTO(
            @JsonProperty("firstname") String firstname,
            @JsonProperty("secondname") String secondname,
            @JsonProperty("firstlastname") String firstlastname,
            @JsonProperty("secondlastname") String secondlastname,
            @JsonProperty("iddocumenttype") Long iddocumenttype,
            @JsonProperty("docnumber") String docnumber,
            @JsonProperty("direccion") String direccion,
            @JsonProperty("idcity") String idcity,
            @JsonProperty("telefono") String telefono,
            @JsonProperty("email") String email,
            @JsonProperty("birthDate") Date birthDate,
            @JsonProperty("gender") Long gender,
            @JsonProperty("lat") double lat,
            @JsonProperty("lng") double lng
            ) {
        this.firstname = firstname;
        this.secondname = secondname;
        this.firstlastname = firstlastname;
        this.secondlastname = secondlastname;
        this.iddocumenttype = iddocumenttype;
        this.docnumber = docnumber;
        this.direccion = direccion;
        this.idcity = idcity;
        this.telefono = telefono;
        this.email = email;
        this.birthDate = birthDate;
        this.gender = gender;
        this.lat = lat;
        this.lng = lng;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Long getGender() {
        return gender;
    }

    public void setGender(Long gender) {
        this.gender = gender;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public String getFirstlastname() {
        return firstlastname;
    }

    public void setFirstlastname(String firstlastname) {
        this.firstlastname = firstlastname;
    }

    public String getSecondlastname() {
        return secondlastname;
    }

    public void setSecondlastname(String secondlastname) {
        this.secondlastname = secondlastname;
    }

    public Long getIddocumenttype() {
        return iddocumenttype;
    }

    public void setIddocumenttype(Long iddocumenttype) {
        this.iddocumenttype = iddocumenttype;
    }

    public String getDocnumber() {
        return docnumber;
    }

    public void setDocnumber(String docnumber) {
        this.docnumber = docnumber;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getIdcity() {
        return idcity;
    }

    public void setIdcity(String idcity) {
        this.idcity = idcity;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
