package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.Gender;
import com.name.business.entities.Location;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GenderMapper  implements ResultSetMapper<Gender> {
    @Override
    public Gender map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Gender(
                resultSet.getLong("ID_GENERO"),
                resultSet.getString("GENERO")
        );
    }
}
