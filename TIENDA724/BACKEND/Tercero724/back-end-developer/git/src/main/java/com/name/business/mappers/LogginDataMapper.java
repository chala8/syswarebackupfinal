package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.LogginData;
import com.name.business.entities.Mail;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LogginDataMapper implements ResultSetMapper<LogginData> {

    @Override
    public LogginData map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new LogginData(
                resultSet.getLong("ID_THIRD_FATHER"),
                resultSet.getString("FULLNAMEFATHER"),
                resultSet.getString("DOCUTYPEFATHER"),
                resultSet.getString("DOCNUMBERFATHER"),
                resultSet.getString("FIRST_NAME"),
                resultSet.getString("SECOND_NAME"),
                resultSet.getString("FIRST_LASTNAME"),
                resultSet.getString("SECOND_LASTNAME"),
                resultSet.getString("FULLNAME"),
                resultSet.getLong("ID_PERSON"),
                resultSet.getString("DOCUMENT_NUMBER"),
                resultSet.getString("DOCUMENT_TYPE"),
                resultSet.getString("IMG")
        );
    }
}
