package com.name.business.mappers;

import com.name.business.entities.CommonBasicInfo;
import com.name.business.entities.CommonThird;
import com.name.business.entities.DocumentType;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Niki on 17/07/2017.
 */
public class Common_BasicInfoMapper implements ResultSetMapper<CommonBasicInfo> {

    @Override
    public CommonBasicInfo map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {

        return new CommonBasicInfo(
        resultSet.getLong("ID_COMMON_BASICINFO"),
        resultSet.getString("FULLNAME"),
        resultSet.getString("IMG"),
        resultSet.getString("DOCUMENT_NUMBER"),
                new DocumentType(
                        resultSet.getLong("ID_DOCUMENT_TYPE"),
                        resultSet.getString("TYPE_DOCUMENT"),
                        new CommonThird(
                                resultSet.getLong("ID_COMMON_THIRD"),
                                resultSet.getInt("STATE"),
                                resultSet.getDate("CREATION_DATE"),
                                resultSet.getDate("MODIFY_DATE")
                        )
                )
        );
    }
}