package com.name.business.entities;

import java.util.Date;


public class CommonThird {

    private Long id_common_third;
    private Integer state;
    private Date create_date;
    private Date modify_date;

    public CommonThird(Long id_common_third, Integer state, Date create_date, Date modify_date) {
        this.id_common_third = id_common_third;
        this.state = state;
        this.create_date = create_date;
        this.modify_date = modify_date;
    }

    public Long getId_common_third() {
        return id_common_third;
    }

    public void setId_common_third(Long id_common_third) {
        this.id_common_third = id_common_third;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getModify_date() {
        return modify_date;
    }

    public void setModify_date(Date modify_date) {
        this.modify_date = modify_date;
    }
}
