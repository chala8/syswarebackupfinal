package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.*;
import com.name.business.representations.ThirdDTO;
import com.name.business.representations.complex.ThirdCompleteDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(ThirdMapper.class)
public interface ThirdDAO {

    /**
     *
     * @param thirdDTO
     * @param id_application
     * @return
     */
    @SqlUpdate("INSERT INTO THIRD " +
            " ( ID_THIRD_TYPE, ID_COMMON_BASICINFO, " +
            " ID_THIRD_FATHER, ID_PERSON, ID_COMMON_THIRD,ID_APPLICATION ) VALUES " +
            " (:th.id_third_type, :th.id_common_basicinfo, " +
            " :th.id_third_father, :th.id_person, :th.id_common_third, :id_application)")
    int create(@BindBean("th") ThirdDTO thirdDTO,@Bind("id_application") Long id_application);

    /**
     *
     * @param id_third
     * @param id_third_father
     * @param id_person
     * @param id_third_type
     * @param th_type_name
     * @param id_c_th_type
     * @param state_c_th_type
     * @param creation_c_th_type
     * @param modify_c_th_type
     * @param id_cb_third
     * @param id_typedoc_third
     * @param doc_third
     * @param typedoc_third
     * @param name_third
     * @param logo
     * @param id_common_third
     * @param state_third
     * @param creation_third
     * @param modify_third
     * @return
     */
    @SqlQuery("SELECT ID_THIRD, ID_THIRD_FATHER,ID_PERSON, ID_THIRD_TYPE,TH_TYPE_NAME,ID_C_TH_TYPE,STATE_C_TH_TYPE,CREATION_C_TH_TYPE,MODIFY_C_TH_TYPE, " +
            "  ID_CB_THIRD,ID_TYPEDOC_THIRD,DOC_THIRD,TYPEDOC_THIRD,NAME_THIRD,LOGO,ID_CTH_DOCT,STATE_DOCT,CREATION_DOCT,MODIFY_DOCT,ID_COMMON_THIRD,STATE_THIRD,CREATION_THIRD,MODIFY_THIRD " +
            " FROM V_THIRD V_T " +
            "WHERE (V_T.ID_THIRD=:ID_THIRD OR :ID_THIRD  IS NULL ) AND" +
            "      (V_T.ID_THIRD_FATHER=:ID_THIRD_FATHER OR :ID_THIRD_FATHER IS NULL ) AND" +
            "      (V_T.ID_PERSON=:ID_PERSON OR :ID_PERSON IS NULL ) AND" +
            "      (V_T.ID_THIRD_TYPE=:ID_THIRD_TYPE OR :ID_THIRD_TYPE IS NULL ) AND" +
            "      (V_T.TH_TYPE_NAME LIKE :TH_TYPE_NAME OR :TH_TYPE_NAME IS NULL ) AND" +
            "      (V_T.ID_C_TH_TYPE =:ID_C_TH_TYPE OR :ID_C_TH_TYPE IS NULL ) AND" +
            "      (V_T.STATE_C_TH_TYPE =:STATE_C_TH_TYPE OR :STATE_C_TH_TYPE IS NULL ) AND" +
            "      (V_T.CREATION_C_TH_TYPE=:CREATION_C_TH_TYPE OR :CREATION_C_TH_TYPE  IS NULL ) AND" +
            "      (V_T.MODIFY_C_TH_TYPE =:MODIFY_C_TH_TYPE  OR :MODIFY_C_TH_TYPE IS NULL ) AND" +
            "      (V_T.ID_CB_THIRD = :ID_CB_THIRD OR :ID_CB_THIRD IS NULL ) AND" +
            "      (V_T.ID_TYPEDOC_THIRD=:ID_TYPEDOC_THIRD  OR :ID_TYPEDOC_THIRD IS NULL ) AND" +
            "      (V_T.DOC_THIRD=:DOC_THIRD OR :DOC_THIRD IS NULL ) AND" +
            "      (V_T.TYPEDOC_THIRD LIKE :TYPEDOC_THIRD OR :TYPEDOC_THIRD IS NULL ) AND" +
            "      (V_T.NAME_THIRD LIKE :NAME_THIRD OR :NAME_THIRD IS NULL ) AND" +
            "      (V_T.LOGO =:LOGO OR :LOGO IS NULL ) AND" +
            "      (V_T.ID_COMMON_THIRD =:ID_COMMON_THIRD OR :ID_COMMON_THIRD  IS NULL ) AND" +
            "      (V_T.STATE_THIRD =:STATE_THIRD OR :STATE_THIRD  IS NULL ) AND " +
            "      (V_T.CREATION_THIRD =:CREATION_THIRD OR :CREATION_THIRD IS NULL ) AND" +
            "      (V_T.MODIFY_THIRD=:MODIFY_THIRD OR :MODIFY_THIRD IS NULL )")
    List<Third> read(@Bind("ID_THIRD") Long id_third, @Bind("ID_THIRD_FATHER") Long id_third_father,@Bind("ID_PERSON") Long id_person, @Bind("ID_THIRD_TYPE") Long id_third_type, @Bind("TH_TYPE_NAME") String th_type_name, @Bind("ID_C_TH_TYPE")Long id_c_th_type,
                     @Bind("STATE_C_TH_TYPE") Integer state_c_th_type, @Bind("CREATION_C_TH_TYPE") Date creation_c_th_type , @Bind("MODIFY_C_TH_TYPE") Date modify_c_th_type,
                     @Bind("ID_CB_THIRD") Long id_cb_third, @Bind("ID_TYPEDOC_THIRD") Long id_typedoc_third, @Bind("DOC_THIRD") String doc_third, @Bind("TYPEDOC_THIRD") String typedoc_third, @Bind("NAME_THIRD") String name_third,
                     @Bind("LOGO")String logo, @Bind("ID_COMMON_THIRD") Long id_common_third, @Bind("STATE_THIRD") Integer state_third, @Bind("CREATION_THIRD")Date creation_third , @Bind("MODIFY_THIRD")Date modify_third);

    /**
     *
     * @param idThird
     * @param third
     * @return
     */
    @SqlUpdate("UPDATE THIRD SET " +
            "ID_THIRD_TYPE = :thirdDTO.id_third_type, " +
            "ID_THIRD_FATHER = :thirdDTO.id_third_father, " +
            "WHERE " +
            "ID_THIRD = :idThird")
    int update(@Bind("idThird") Long idThird, @BindBean("thirdDTO") ThirdCompleteDTO third);

    /**
     *
     * @param idThird
     * @return
     */
    @SqlQuery("SELECT ID_COMMON_THIRD FROM THIRD WHERE ID_THIRD = :idThird")
    List<Long> delete(@Bind("idThird") Long idThird);

    /***
     *
     * @param id_third
     * @param third
     * @return
     */
    @SqlUpdate("DELETE FROM THIRD " +
            "WHERE " +
            "  ( ID_THIRD = :id_third OR :id_third IS NULL ) AND " +
            "  ( ID_THIRD_TYPE = :th.third_type OR :th.third_type IS NULL ) AND  " +
            "  ( ID_COMMON_BASICINFO = :th.id_common_basicinfo OR :th.id_common_basicinfo IS NULL ) AND " +
            "  ( ID_THIRD_FATHER = :th.id_third_father OR th.id_third_father IS NULL ) AND " +
            "  ( ID_PERSON = :th.id_person OR :th.id_person IS NULL ) AND " +
            "  ( ID_COMMON_THIRD =  :th.id_common_third OR :th.id_common_third IS NULL ) ")
    int delete(@Bind("id_third") Long id_third,@BindBean("th") ThirdDTO third);

    /**
     *
     * @return
     */
    @SqlQuery("SELECT ID_THIRD " +
            "FROM THIRD " +
            "WHERE ID_THIRD IN (SELECT MAX(ID_THIRD) FROM THIRD)")
    Long getPkLast();

    @SqlQuery("SELECT count(ID_THIRD) FROM THIRD WHERE ID_THIRD=:id")
    Integer getValidatorID(@Bind("id") Long id_user_third);




    @RegisterMapper(DocumentPersonMapper.class)
    @SqlQuery(" select p.id_person, fullname, name document_type,document_number,city,address,ph.phone\n" +
            "   from tercero724.person p, tercero724.common_basicinfo cbi, tercero724.document_type d, tercero724.directory dir, tercero724.phone ph\n" +
            "   where p.id_common_basicinfo=cbi.id_common_basicinfo and cbi.id_document_type=d.id_document_type and p.id_directory=dir.id_directory\n" +
            "   and dir.id_directory=ph.id_directory and priority=1\n" +
            "   and document_number=:document_number ")
    List<DocumentPerson> getPerson(@Bind("document_number") String document_number);


    @SqlQuery("SELECT ID_DIRECTORY FROM DIRECTORY WHERE ID_DIRECTORY IN (SELECT MAX(ID_DIRECTORY) FROM DIRECTORY)")
    Long getLastDirectory();

    @SqlUpdate(" UPDATE THIRD SET ID_DIRECTORY = :id_directory WHERE ID_THIRD = :id_third ")
    void updateDir(@Bind("id_third") Long id_third,@Bind("id_directory") Long id_directory);


    @RegisterMapper(BillThirdMapper.class)
    @SqlQuery(" select dt.name document_type, document_number,fullname,count(*) NumVentas,sum(totalprice) TotalVentas ,address, phone, mail\n" +
            "               from facturacion724.bill b,tercero724.third t,tercero724.common_basicinfo cb,tercero724.document_type dt\n" +
            "               ,tercero724.directory dir,tercero724.phone ph,tercero724.mail ma\n" +
            "               where b.id_third_destinity=t.id_third and t.id_common_basicinfo=cb.id_common_basicinfo and cb.id_document_type=dt.id_document_type\n" +
            "                 and t.id_directory=dir.id_directory(+) and dir.id_directory=ph.id_directory(+) and dir.id_directory=ma.id_directory(+)\n" +
            "                 and id_bill_state=1 and id_third_destinity is not null\n" +
            "                 and id_bill_type=:bill_type and purchase_date between :date1 and (:date2+0.99) and b.id_third=:id_third\n" +
            "               group by dt.name,document_number,fullname,address, phone, mail\n" +
            "               UNION\n" +
            "               select 'N/A' document_type, 'N/A' document_number,'Ocasional' fullname,count(*) NumVentas,sum(totalprice) TotalVentas , null address, null phone, null mail\n" +
            "               from facturacion724.bill b\n" +
            "               where \n" +
            "                 id_bill_state=1\n" +
            "                 and id_bill_type=:bill_type and purchase_date between :date1 and (:date2+0.99)\n" +
            "                 and b.id_third=:id_third and id_third_destinity is null order by 4 desc")
    List<BillThird> getThirdsForBill(@Bind("id_third") Long id_third,
                                     @Bind("date1") Date date1,
                                     @Bind("date2") Date date2,
                                     @Bind("bill_type") Long bill_type);

    @RegisterMapper(BillThirdMapper.class)
    @SqlQuery(" select dt.name document_type, document_number,fullname,'-1' as NUMVENTAS,'-1' as TOTALVENTAS,address, phone, mail\n" +
            "from tercero724.third third,tercero724.common_basicinfo common_basicinfo,tercero724.document_type dt\n" +
            "   ,tercero724.directory dir,tercero724.phone ph,tercero724.mail ma\n" +
            "where third.id_common_basicinfo=common_basicinfo.id_common_basicinfo and\n" +
            "      common_basicinfo.id_document_type=dt.id_document_type and\n" +
            "      third.id_directory=dir.id_directory(+) and\n" +
            "      dir.id_directory=ph.id_directory(+) and\n" +
            "      dir.id_directory=ma.id_directory(+) and\n" +
            "        third.id_third=:id_third")
    BillThird getThirdBasicData(@Bind("id_third") Long id_third);


    @SqlQuery(" select id_third from tercero724.common_basicinfo cbi, tercero724.third th\n" +
            "where cbi.id_common_basicinfo=th.id_common_basicinfo and id_document_type=:id_document_type and document_number=:document_number ")
    Long getIdThird(@Bind("id_document_type") Long id_document_type,@Bind("document_number") String document_number);


    @SqlQuery("select id_third_employee id_third from tienda724.store_empleado where id_store=:id_store union select t.id_third from tienda724.caja_person cp" +
            ",tienda724.caja c,tercero724.third t where cp.id_caja=c.id_caja and cp.id_person=t.id_person and c.id_store=:id_store")
    List<Long> IDThirdsFromIDStore(@Bind("id_store") Long id_store);




    @RegisterMapper(CityMapper.class)
    @SqlQuery(" select ID_CITY, CITY_NAME from TERCERO724.CITY order by 1")
    List<City> getCities();



    @RegisterMapper(ThirdDataMapper.class)
    @SqlQuery(" SELECT T.ID_THIRD,CBI.FULLNAME,DT.NAME,CBI.DOCUMENT_NUMBER,MAIL, DIR.ADDRESS, P.PHONE\n" +
            "FROM TIENDA724.PARAM_PEDIDOS PP, TIENDA724.STORE ST,TERCERO724.THIRD T,TERCERO724.COMMON_BASICINFO CBI,TERCERO724.DOCUMENT_TYPE DT,\n" +
            "     TERCERO724.DIRECTORY DIR,TERCERO724.MAIL M, TERCERO724.PHONE P\n" +
            "WHERE PP.ID_STORE_PROVIDER=ST.ID_STORE AND ST.ID_THIRD=T.ID_THIRD AND T.ID_COMMON_BASICINFO=CBI.ID_COMMON_BASICINFO\n" +
            "  AND CBI.ID_DOCUMENT_TYPE=DT.ID_DOCUMENT_TYPE AND T.ID_DIRECTORY=DIR.ID_DIRECTORY AND DIR.ID_DIRECTORY=M.ID_DIRECTORY\n" +
            "  AND PP.ID_STORE_CLIENT=:idstore AND P.ID_DIRECTORY = DIR.ID_DIRECTORY ")
    List<ThirdData> getThirdData(@Bind("idstore") Long idstore);



    @SqlUpdate(" UPDATE DIRECTORY SET ID_CITY = :id_city \n" +
               " WHERE ID_DIRECTORY = :id_directory")
    Integer updateDirectoryCity(@Bind("id_directory") String id_directory,
                                   @Bind("id_city") String id_city);


    @SqlQuery(" SELECT ID_DIRECTORY FROM THIRD WHERE THIRD.ID_THIRD = :id_third ")
    String getDirID(@Bind("id_third") Long id_third);

    @SqlCall(" call TERCERO724.CREAR_TERCERO(:typesa,:nombre1,:nombre2,:nombre3,:nombre4,:doctype,:docnumber,:addres,:id_city,:telefono,:correo,:idgenero,:fechanacimiento) ")
    void newThird(@Bind("typesa") String typesa,
                  @Bind("nombre1") String nombre1,
                  @Bind("nombre2") String nombre2,
                  @Bind("nombre3") String nombre3,
                  @Bind("nombre4") String nombre4,
                  @Bind("doctype") Long doctype,
                  @Bind("docnumber") String docnumber,
                  @Bind("addres") String addres,
                  @Bind("id_city") String id_city,
                  @Bind("telefono") String telefono,
                  @Bind("correo") String correo,
                  @Bind("idgenero") Long idgenero,
                  @Bind("fechanacimiento") Date fechanacimiento);

    @SqlCall(" call TERCERO724.CREAR_USUARIO_TERCERO" +
            "(:typesa,:nombre1,:nombre2,:nombre3,:nombre4,:doctype,:docnumber,:addres,:id_city,:telefono,:correo,26,:gender,:birthDate,:lat,:lng) ")
    void newUserThird(@Bind("typesa") String typesa,
                  @Bind("nombre1") String nombre1,
                  @Bind("nombre2") String nombre2,
                  @Bind("nombre3") String nombre3,
                  @Bind("nombre4") String nombre4,
                  @Bind("doctype") Long doctype,
                  @Bind("docnumber") String docnumber,
                  @Bind("addres") String addres,
                  @Bind("id_city") String id_city,
                  @Bind("telefono") String telefono,
                  @Bind("correo") String correo,
                  @Bind("birthDate") Date birthDate,
                  @Bind("gender") Long gender,
                  @Bind("lat") double lat,
                  @Bind("lng") double lng);

    @RegisterMapper(GenderMapper.class)
    @SqlQuery(" SELECT ID_GENERO,GENERO FROM  TERCERO724.GENERO ORDER BY 2 ")
    List<Gender> getGender();



    @SqlCall(" call TERCERO724.ACTUALIZAR_TERCERO(:idthird ,:addressc ,:phonec ,:mailc ) ")
    void updateThird(@Bind("idthird") Long idthird,
                  @Bind("addressc") String addressc,
                  @Bind("phonec") String phonec,
                  @Bind("mailc") String mailc);


    @RegisterMapper(LogginDataMapper.class)
    @SqlQuery(" SELECT T.ID_THIRD_FATHER,CBIF.FULLNAME FULLNAMEFATHER,DTF.NAME DOCUTYPEFATHER,CBIF.DOCUMENT_NUMBER DOCNUMBERFATHER\n" +
            "     ,FIRST_NAME,SECOND_NAME,FIRST_LASTNAME,SECOND_LASTNAME,CBI.FULLNAME,T.ID_PERSON,CBI.DOCUMENT_NUMBER,DT.NAME DOCUMENT_TYPE,CBI.IMG\n" +
            "FROM TERCERO724.THIRD T,TERCERO724.THIRD FATHER,TERCERO724.PERSON P,TERCERO724.COMMON_BASICINFO CBI,TERCERO724.DOCUMENT_TYPE DT\n" +
            "   ,TERCERO724.COMMON_BASICINFO CBIF,TERCERO724.DOCUMENT_TYPE DTF\n" +
            "WHERE T.ID_THIRD_FATHER=FATHER.ID_THIRD AND T.ID_PERSON=P.ID_PERSON AND T.ID_COMMON_BASICINFO=CBI.ID_COMMON_BASICINFO\n" +
            "  AND CBI.ID_DOCUMENT_TYPE=DT.ID_DOCUMENT_TYPE\n" +
            "  AND FATHER.ID_COMMON_BASICINFO=CBIF.ID_COMMON_BASICINFO AND CBIF.ID_DOCUMENT_TYPE=DTF.ID_DOCUMENT_TYPE\n" +
            "  AND T.ID_THIRD= :idthird")
    List<LogginData> getLogginData(
            @Bind("idthird") Long idthird
    );

    @SqlQuery(" select id_city from TERCERO724.directory where ID_DIRECTORY = (select id_directory from TERCERO724.THIRD where ID_THIRD = :id_third) ")
    Long getCity(@Bind("id_third") Long id_third);
}
