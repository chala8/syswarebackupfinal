package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class EmployeeDTO {
    private Double salary;
    private Common_ThirdDTO state;

    @JsonCreator
    public EmployeeDTO(@JsonProperty("salary") Double salary,
                       @JsonProperty("state") Common_ThirdDTO state) {
        this.salary = salary;
        this.state = state;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Common_ThirdDTO getState() {
        return state;
    }

    public void setState(Common_ThirdDTO state) {
        this.state = state;
    }
}

