package com.name.business.entities;

import java.util.Date;

public class DocumentPerson2 {

    private String FULLNAME;
    private String DOCUMENT_TYPE;
    private String DOCUMENT_NUMBER;
    private String CITY;
    private String ADDRESS;
    private String PHONE;
    private Long ID_PERSON;
    private Long ID_DIRECTORY;
    private String LATITUD;
    private String LONGITUD;
    private String GENERO;
    private Long ID_PERSON_REAL;
    private Date BIRTHDAY;
    private Long CITY_ID;
    private String CITY_NAME;


    public DocumentPerson2(String FULLNAME,
                          String DOCUMENT_TYPE,
                          String DOCUMENT_NUMBER,
                          String CITY,
                          String ADDRESS,
                          String PHONE,
                          Long ID_PERSON,
                          Long ID_DIRECTORY,
                          String LATITUD,
                          String LONGITUD,
                          String GENERO,
                           Long ID_PERSON_REAL,
                           Date BIRTHDAY,
                           Long CITY_ID,
                           String CITY_NAME) {
        this.LATITUD = LATITUD;
        this.BIRTHDAY = BIRTHDAY;
        this.LONGITUD = LONGITUD;
        this.GENERO = GENERO;
        this.FULLNAME = FULLNAME;
        this.DOCUMENT_TYPE = DOCUMENT_TYPE;
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
        this.CITY = CITY;
        this.ADDRESS = ADDRESS;
        this.PHONE = PHONE;
        this.ID_PERSON = ID_PERSON;
        this.ID_DIRECTORY = ID_DIRECTORY;
        this.ID_PERSON_REAL = ID_PERSON_REAL;
        this.CITY_ID = CITY_ID;
        this.CITY_NAME = CITY_NAME;
    }

    public Long getCITY_ID() {
        return CITY_ID;
    }

    public void setCITY_ID(Long CITY_ID) {
        this.CITY_ID = CITY_ID;
    }

    public String getCITY_NAME() {
        return CITY_NAME;
    }

    public void setCITY_NAME(String CITY_NAME) {
        this.CITY_NAME = CITY_NAME;
    }

    public Date getBIRTHDAY() {
        return BIRTHDAY;
    }

    public void setBIRTHDAY(Date BIRTHDAY) {
        this.BIRTHDAY = BIRTHDAY;
    }

    public Long getID_PERSON_REAL() {
        return ID_PERSON_REAL;
    }

    public void setID_PERSON_REAL(Long ID_PERSON_REAL) {
        this.ID_PERSON_REAL = ID_PERSON_REAL;
    }

    public String getLATITUD() {
        return LATITUD;
    }

    public void setLATITUD(String LATITUD) {
        this.LATITUD = LATITUD;
    }

    public String getLONGITUD() {
        return LONGITUD;
    }

    public void setLONGITUD(String LONGITUD) {
        this.LONGITUD = LONGITUD;
    }

    public String getGENERO() {
        return GENERO;
    }

    public void setGENERO(String GENERO) {
        this.GENERO = GENERO;
    }

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }

    public String getDOCUMENT_TYPE() {
        return DOCUMENT_TYPE;
    }

    public void setDOCUMENT_TYPE(String DOCUMENT_TYPE) {
        this.DOCUMENT_TYPE = DOCUMENT_TYPE;
    }

    public String getDOCUMENT_NUMBER() {
        return DOCUMENT_NUMBER;
    }

    public void setDOCUMENT_NUMBER(String DOCUMENT_NUMBER) {
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getPHONE() {
        return PHONE;
    }

    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    public Long getID_PERSON() {
        return ID_PERSON;
    }

    public void setID_PERSON(Long ID_PERSON) {
        this.ID_PERSON = ID_PERSON;
    }

    public Long getID_DIRECTORY() {
        return ID_DIRECTORY;
    }

    public void setID_DIRECTORY(Long ID_DIRECTORY) {
        this.ID_DIRECTORY = ID_DIRECTORY;
    }




}
