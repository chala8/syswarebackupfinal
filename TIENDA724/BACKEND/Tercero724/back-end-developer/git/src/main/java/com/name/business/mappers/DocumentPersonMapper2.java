package com.name.business.mappers;


import com.name.business.entities.DocumentPerson2;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DocumentPersonMapper2 implements ResultSetMapper<DocumentPerson2>{

    @Override
    public DocumentPerson2 map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DocumentPerson2(
                resultSet.getString("FULLNAME"),
                resultSet.getString("DOCUMENT_TYPE"),
                resultSet.getString("DOCUMENT_NUMBER"),
                resultSet.getString("MAIL"),
                resultSet.getString("ADDRESS"),
                resultSet.getString("PHONE"),
                resultSet.getLong("ID_THIRD"),
                resultSet.getLong("ID_DIRECTORY"),
                resultSet.getString("LATITUD"),
                resultSet.getString("LONGITUD"),
                resultSet.getString("GENERO"),
                resultSet.getLong("ID_PERSON"),
                resultSet.getDate("BIRTHDAY"),
                resultSet.getLong("ID_CITY"),
                resultSet.getString("CITY_NAME")
        );
    }
}