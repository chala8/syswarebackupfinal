package com.name.business.entities;

/**
 * Created by trossky on 14/07/17.
 */
public class UserThird {

    private Long id_user_third;
    private Long id_person;
    private String UUID;
    private CommonThird state;


    public UserThird(Long id_user_third, Long id_person, String UUID, CommonThird state) {
        this.id_user_third = id_user_third;
        this.id_person = id_person;
        this.UUID = UUID;
        this.state = state;
    }

    public Long getId_person() {
        return id_person;
    }

    public void setId_person(Long id_person) {
        this.id_person = id_person;
    }

    public Long getId_user_third() {
        return id_user_third;
    }

    public void setId_user_third(Long id_user_third) {
        this.id_user_third = id_user_third;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public CommonThird getState() {
        return state;
    }

    public void setState(CommonThird state) {
        this.state = state;
    }
}
