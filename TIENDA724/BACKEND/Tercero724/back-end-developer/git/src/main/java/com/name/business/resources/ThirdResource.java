package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.ThirdBusiness;
import com.name.business.entities.*;
import com.name.business.entities.DocumentPerson;
import com.name.business.entities.Third;
import com.name.business.entities.Token;
import com.name.business.representations.complex.CreateThirdUserDTO;
import com.name.business.representations.complex.ThirdCompleteDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;
import retrofit2.http.Query;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

/**
 * Created by luis on 24/07/17.
 */
@Path("/thirds")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ThirdResource {

    private final ThirdBusiness  thirdBusiness;

    public ThirdResource(ThirdBusiness thirdBusiness) {
        this.thirdBusiness = thirdBusiness;
    }

    /**
     *
     * @param id_third
     * @param id_third_father
     * @param id_person
     * @param id_third_type
     * @param th_type_name
     * @param id_c_th_type
     * @param state_c_th_type
     * @param creation_c_th_type
     * @param modify_c_th_type
     * @param id_cb_third
     * @param id_typedoc_third
     * @param doc_third
     * @param typedoc_third
     * @param name_third
     * @param logo
     * @param id_common_third
     * @param state_third
     * @param creation_third
     * @param modify_third
     * @param first_name
     * @param second_name
     * @param first_lastname
     * @param second_lastname
     * @param birthday
     * @param id_dir_person
     * @param id_cbi_person
     * @param id_doctype_person
     * @param doc_person
     * @param typedoc_person
     * @param fullname_person
     * @param img_person
     * @param id_c_th_person
     * @param state_person
     * @param creation_person
     * @param modify_person
     * @return
     */
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getThirdResourceList(@QueryParam("id_third") Long id_third,@QueryParam("id_third_father") Long id_third_father,
                                         @QueryParam("id_person") Long id_person,@QueryParam("id_third_type") Long id_third_type,
                                         @QueryParam("th_type_name")String th_type_name,@QueryParam("id_c_th_type") Long id_c_th_type,
                                         @QueryParam("state_c_th_type") Integer state_c_th_type, @QueryParam("creation_c_th_type") Date creation_c_th_type,
                                         @QueryParam("modify_c_th_type") Date modify_c_th_type,@QueryParam("id_cb_third") Long id_cb_third,
                                         @QueryParam("id_typedoc_third") Long id_typedoc_third,@QueryParam("doc_third")  String doc_third,
                                         @QueryParam("typedoc_third")String typedoc_third,  @QueryParam("name_third") String name_third,
                                         @QueryParam("logo") String logo, @QueryParam("id_common_third")Long id_common_third,
                                         @QueryParam("state_third")Integer state_third,@QueryParam("creation_third") Date creation_third,
                                         @QueryParam("modify_third") Date modify_third,
                                         @QueryParam("first_name") String first_name,
                                         @QueryParam("second_name") String second_name,@QueryParam("first_lastname")  String first_lastname,
                                         @QueryParam("second_lastname") String second_lastname,@QueryParam("birthday")  Date birthday,
                                         @QueryParam("id_dir_person") Long id_dir_person,@QueryParam("id_cbi_person") Long id_cbi_person,
                                         @QueryParam("id_doctype_person") Long id_doctype_person,@QueryParam("doc_person")  String doc_person,
                                         @QueryParam("typedoc_person") Long typedoc_person,@QueryParam("fullname_person")  String fullname_person,
                                         @QueryParam("img_person") String img_person, @QueryParam("UUID") String UUID,
                                         @QueryParam("id_c_th_person") Long id_c_th_person,@QueryParam("state_person")  Integer state_person,
                                         @QueryParam("creation_person") Date creation_person,@QueryParam("modify_person")  Date modify_person){
        Response response;

        Either<IException, List<Third>> allViewOffertsEither = thirdBusiness.getThird(id_third, id_third_father,id_person, id_third_type,  th_type_name,  id_c_th_type,
                state_c_th_type,  creation_c_th_type, modify_c_th_type,
                id_cb_third, id_typedoc_third,  doc_third, typedoc_third,  name_third,
                logo, id_common_third,  state_third,  creation_third,  modify_third,first_name,
                second_name,first_lastname,
                second_lastname, birthday,
                id_dir_person, id_cbi_person,
                id_doctype_person,doc_person,
                typedoc_person, fullname_person,
                img_person,UUID,
                id_c_th_person, state_person,
                creation_person,modify_person);


        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param thirdCompleteDTO
     * @return
     */
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postThirdResource(@Context Token token, ThirdCompleteDTO thirdCompleteDTO,@QueryParam("id_app") Long id_app){
        Response response;

        if (id_app==null){
            id_app=token.getAplicacion().getId_aplicacion();
        }
        Either<IException, Long> allViewOffertsEither =thirdBusiness.createThird(thirdCompleteDTO,id_app);

        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }

        return response;
    }

    /**
     *
     * @param createThirdUserDTO
     * @return
     */
    @Path("/CreateThirdUser")
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response CreateThirdUserResource(CreateThirdUserDTO createThirdUserDTO){
        Response response;

        Either<IException, Long> allViewOffertsEither =thirdBusiness.CreateThirdUser(createThirdUserDTO);

        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }

        return response;
    }

    /**
     *
     * @param id_third
     * @return
     */
    @Path("/getBasicThirdInfo")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getThirdBasicData(@Context Token token, @QueryParam("id_third") Long id_third){
        Response response;

        Either<IException, BillThird> third =thirdBusiness.getThirdBasicData(id_third);

        if (third.isRight()){
            response=Response.status(Response.Status.OK).entity(third.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(third);
        }

        return response;
    }

    @Path("/IDThirdsFromIDStore")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response IDThirdsFromIDStore(@Context Token token, @QueryParam("id_store") Long id_store){
        Response response;

        Either<IException, List<Long>> third =thirdBusiness.IDThirdsFromIDStore(id_store);

        if (third.isRight()){
            response=Response.status(Response.Status.OK).entity(third.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(third);
        }

        return response;
    }




    @Path("/idThird")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getIdThird(@QueryParam("id_document_type") Long id_document_type,@QueryParam("document_number") String document_number){
        Response response;

        Either<IException, Long> third =thirdBusiness.getIdThird(id_document_type,document_number);

        if (third.isRight()){
            response=Response.status(Response.Status.OK).entity(third.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(third);
        }

        return response;
    }





    @Path("/getthirddata")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getThirdData(@Context Token token, @QueryParam("idstore") Long idstore){
        Response response;

        Either<IException, List<ThirdData>> third =thirdBusiness.getThirdData(idstore);

        if (third.isRight()){
            response=Response.status(Response.Status.OK).entity(third.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(third);
        }

        return response;
    }




    /**
     *
     * @param id_third
     * @param thirdDTO
     * @return
     */
    @Path("/{id_third}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateThirdResource(@PathParam("id_third") Long id_third, ThirdCompleteDTO thirdDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = thirdBusiness.updateThird(id_third, thirdDTO);

        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_third
     * @return
     */
    @Path("/{id_third}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deleteThirdResource(@PathParam("id_third") Long id_third){
        Response response;
        Either<IException, Long> allViewOffertsEither = thirdBusiness.deleteThird(id_third);
        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @GET
    @Path("/person")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPerson(@QueryParam("document_number") String document_number) {
        Response response;

        Either<IException, List<DocumentPerson>> getCategoriesEither = thirdBusiness.getPerson(document_number);
        if (getCategoriesEither.isRight()) {
            System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }

    @GET
    @Path("/dir")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getLastDirectory() {
        Response response;

        Either<IException, Long> getCategoriesEither = thirdBusiness.getLastDirectory();
        if (getCategoriesEither.isRight()) {
            System.out.println(getCategoriesEither.right().value());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }


    @PUT
    @Path("/dir")
    @Timed
    @RolesAllowed({"Auth"})
    public Response putDirectory(@QueryParam("id_third") Long id_third,@QueryParam("id_directory") Long id_directory ){
        Response response;
        Either<IException, String> allViewOffertsEither = thirdBusiness.putLastDirectory(id_third, id_directory);

        if (allViewOffertsEither.isRight()){
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }


    @GET
    @Path("/listThird")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getThirdsForBill(@QueryParam("id_third") Long id_third,
                                     @QueryParam("date1") String date1,
                                     @QueryParam("date2") String date2,
                                     @QueryParam("bill_type") Long bill_type) {
        Response response;

        Either<IException, List<BillThird>> getCategoriesEither = thirdBusiness.getThirdsForBill(id_third,new Date (date1),new Date(date2),bill_type);
        if (getCategoriesEither.isRight()) {
            System.out.println(getCategoriesEither.right().value());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }


    @GET
    @Path("/cities")
    @Timed
    @RolesAllowed({"Auth","PublicAPI"})
    public Response getCities() {
        Response response;

        Either<IException, List<City>> getCategoriesEither = thirdBusiness.getCities();
        if (getCategoriesEither.isRight()) {
            System.out.println(getCategoriesEither.right().value());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }



    @GET
    @Path("/genders")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getGender() {
        Response response;

        Either<IException, List<Gender>> getCategoriesEither = thirdBusiness.getGender();
        if (getCategoriesEither.isRight()) {
            System.out.println(getCategoriesEither.right().value());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }



    @GET
    @Path("/getCity")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCity(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, Long> getCategoriesEither = thirdBusiness.getCity(id_third);
        if (getCategoriesEither.isRight()) {
            System.out.println(getCategoriesEither.right().value());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }



    @GET
    @Path("/logginData")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getLogginData(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<LogginData>> getCategoriesEither = thirdBusiness.getLogginData(id_third);
        if (getCategoriesEither.isRight()) {
            System.out.println(getCategoriesEither.right().value());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }



    @POST
    @Path("/updateThird")
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateThird(@QueryParam("idthird") Long idthird,
                                @QueryParam("addressc") String addressc,
                                @QueryParam("phonec") String phonec,
                                @QueryParam("mailc") String mailc) {
        Response response;

        Either<IException, Long> getCategoriesEither = thirdBusiness.updateThird(idthird,
                addressc,
                phonec,
                mailc);
        if (getCategoriesEither.isRight()) {
            System.out.println(getCategoriesEither.right().value());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }



    @PUT
    @Path("/city")
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateDirectoryCity(@QueryParam("id_third") Long id_third,
                                        @QueryParam("id_city") String id_city) {
        Response response;

        Either<IException, String> getCategoriesEither = thirdBusiness.updateDirectoryCity(id_third,id_city);
        if (getCategoriesEither.isRight()) {
            System.out.println(getCategoriesEither.right().value());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }


    @POST
    @Path("/newThird")
    @Timed
    @RolesAllowed({"Auth"})
    public Response newThird(@QueryParam("typesa") String typesa,
    @QueryParam("nombre1") String nombre1,
    @QueryParam("nombre2") String nombre2,
    @QueryParam("nombre3") String nombre3,
    @QueryParam("nombre4") String nombre4,
    @QueryParam("doctype") Long doctype,
    @QueryParam("docnumber") String docnumber,
    @QueryParam("addres") String addres,
    @QueryParam("id_city") String id_city,
    @QueryParam("telefono") String telefono,
    @QueryParam("correo") String correo,
    @QueryParam("idgenero") Long idgenero,
    @QueryParam("fechanacimiento") String fechanacimiento) {
        Response response;
        System.out.println(id_city);
        Long getCategoriesEither = thirdBusiness.newThird(typesa,
                nombre1,
                nombre2,
                nombre3,
                nombre4,
                doctype,
                docnumber,
                addres,
                id_city,
                telefono,
                correo,
                idgenero,
                fechanacimiento);

            response = Response.status(Response.Status.OK).entity(getCategoriesEither).build();

        return response;
    }

}
