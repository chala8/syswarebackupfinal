package com.name.business;



import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class Third724AppConfiguration extends Configuration {


    @Valid
    @NotNull
    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();

    @Valid
    @NotNull
    private Third724DatabaseConfiguration databaseConfiguration;


    public DataSourceFactory getDataSourceFactory(){
        return database;
    }
    public Third724DatabaseConfiguration getDatabaseConfiguration(){
        return databaseConfiguration;
    }

}
