package com.name.business.representations;

/**
 * Created by luis_ on 28/01/2017.
 */
public class TiketDTO {

    private final String token_value;
    private final String http_verb;
    private final String requested_uri;


    public TiketDTO(String token_value, String http_verb, String requested_uri) {
        this.token_value = token_value;
        this.http_verb = http_verb;
        this.requested_uri = requested_uri;
    }

    public String getToken_value() {
        return token_value;
    }

    public String getHttp_verb() {
        return http_verb;
    }

    public String getRequested_uri() {
        return requested_uri;
    }
}
