package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.EmployeeBusiness;
import com.name.business.entities.Employee;
import com.name.business.representations.EmployeeDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/employees")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EmployeeResource {

    private final EmployeeBusiness employeeBusiness;

    /**
     *
     * @param employeeBusiness
     */
    public EmployeeResource(EmployeeBusiness employeeBusiness) {
        this.employeeBusiness = employeeBusiness;
    }

    /**
     *
     * @param id_person
     * @param employeeDTO
     * @return
     */
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postPerson(@QueryParam("id_person")Long id_person, EmployeeDTO employeeDTO){
        Response response;

        Either<IException, Long> allViewOffertsEither =employeeBusiness.createEmployee(id_person, employeeDTO);


        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }


    @Path("/userThird")
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response insertUserThird(@QueryParam("id_person")Long id_person){
        Response response;

        Either<IException, Long> allViewOffertsEither =employeeBusiness.insertUserThird(id_person);


        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_employee
     * @param id_person
     * @param salary
     * @param id_common_third
     * @param state
     * @param creation_employee
     * @param modify_employee
     * @return
     */
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPersonResource(@QueryParam("id_employee") Long id_employee,@QueryParam("id_person")Long id_person,
                                      @QueryParam("salary") Double salary,
                                      @QueryParam("id_common_third") Long id_common_third, @QueryParam("state")Integer state,
                                      @QueryParam("creation_employee") Date creation_employee,@QueryParam("modify_employee") Date modify_employee){
        Response response;

        Either<IException, List<Employee>> allViewOffertsEither =employeeBusiness.getEmployee(id_employee,id_person,  salary,
                id_common_third,state,
                creation_employee, modify_employee);


        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_employee
     * @param empDTO
     * @return
     */
    @Path("/{id_employee}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateEmployeeResource(@PathParam("id_employee") Long id_employee, EmployeeDTO empDTO ){
        Response response;
        Either<IException, Long> allViewOffertsEither = employeeBusiness.updateEmployee(id_employee, empDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_employee
     * @return
     */
    @Path("/{id_employee}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deleteEmployeeResource(@PathParam("id_employee") Long id_employee){
        Response response;
        Either<IException, Long> allViewOffertsEither = employeeBusiness.deleteEmployee(id_employee);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
