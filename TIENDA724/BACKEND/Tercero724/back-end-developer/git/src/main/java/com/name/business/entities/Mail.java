package com.name.business.entities;

/**
 * Created by trossky on 14/07/17.
 */
public class Mail {
    private Long id_mail;
    private Long id_directory;
    private String mail;
    private Integer priority;
    private CommonThird state;

    public Mail(Long id_mail, Long id_directory, String mail, Integer priority, CommonThird state) {
        this.id_mail = id_mail;
        this.id_directory = id_directory;
        this.mail = mail;
        this.priority = priority;
        this.state = state;
    }

    public CommonThird getState() {
        return state;
    }

    public void setState(CommonThird state) {
        this.state = state;
    }

    public Long getId_mail() {
        return id_mail;
    }

    public void setId_mail(Long id_mail) {
        this.id_mail = id_mail;
    }

    public Long getId_directory() {
        return id_directory;
    }

    public void setId_directory(Long id_directory) {
        this.id_directory = id_directory;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
