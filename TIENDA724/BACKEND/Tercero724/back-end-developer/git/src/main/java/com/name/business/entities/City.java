package com.name.business.entities;

public class City {


    private String ID_CITY;
    private String CITY_NAME;


    public City(
            String ID_CITY,
            String CITY_NAME) {
        this.ID_CITY = ID_CITY;
        this.CITY_NAME = CITY_NAME;
    }

    public String getID_CITY() {
        return ID_CITY;
    }

    public void setID_CITY(String ID_CITY) {
        this.ID_CITY = ID_CITY;
    }

    public String getCITY_NAME() {
        return CITY_NAME;
    }

    public void setCITY_NAME(String CITY_NAME) {
        this.CITY_NAME = CITY_NAME;
    }


}
