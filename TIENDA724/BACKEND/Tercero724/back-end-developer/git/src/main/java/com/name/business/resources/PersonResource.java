package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.PersonBusiness;
import com.name.business.entities.Aplicacion;
import com.name.business.entities.DocumentPerson2;
import com.name.business.entities.Person;
import com.name.business.entities.Token;
import com.name.business.representations.PersonDTO;
import com.name.business.representations.UpdatePersonDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/persons")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PersonResource {
    private PersonBusiness personBusiness;

    /**
     *
     * @param personBusiness
     */
    public PersonResource(PersonBusiness personBusiness) {
        this.personBusiness = personBusiness;
    }

    /**
     *
     * @param personDTO
     * @return
     */
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postPerson(@Context Token token, PersonDTO personDTO,@QueryParam("id_app") Long id_app){
        Response response;

        if (id_app==null){
            id_app=token.getAplicacion().getId_aplicacion();
        }

        Either<IException, Long> allViewOffertsEither =personBusiness.createPerson(personDTO,id_app);


        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param id_person
     * @param first_name
     * @param second_name
     * @param first_lastname
     * @param second_lastname
     * @param birthday
     * @param id_dir_person
     * @param id_cbi_person
     * @param id_doctype_person
     * @param doc_person
     * @param typedoc_person
     * @param fullname_person
     * @param img_person
     * @param id_c_th_person
     * @param state_person
     * @param creation_person
     * @param modify_person
     * @return
     */
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPersonResource(@Context Aplicacion aplicacion,@QueryParam("id_person") Long id_person,@QueryParam("first_name") String first_name,
                                      @QueryParam("second_name") String second_name,@QueryParam("first_lastname") String first_lastname,
                                      @QueryParam("second_lastname") String second_lastname,@QueryParam("birthday") Date birthday,
                                      @QueryParam("id_dir_person") Long id_dir_person,@QueryParam("id_cbi_person")Long id_cbi_person,
                                      @QueryParam("id_doctype_person") Long id_doctype_person, @QueryParam("doc_person") String doc_person,
                                      @QueryParam("typedoc_person") Long typedoc_person,@QueryParam("fullname_person") String fullname_person,
                                      @QueryParam("img_person") String img_person,@QueryParam("UUID") String UUID,
                                      @QueryParam("id_c_th_person") Long id_c_th_person,@QueryParam("state_person") Integer state_person,
                                      @QueryParam("creation_person") Date creation_person,@QueryParam("modify_person") Date modify_person){
        Response response;

        Either<IException, List<Person>> allViewOffertsEither =personBusiness.getPerson( id_person, first_name,
                second_name,first_lastname,
                second_lastname, birthday,
                id_dir_person, id_cbi_person,
                id_doctype_person, doc_person,
                typedoc_person, fullname_person,
                img_person, UUID,
                id_c_th_person, state_person,
                creation_person,  modify_person);


        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/IdThirdFromUUID")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response IdThirdFromUUID(@Context Aplicacion aplicacion,@QueryParam("uuid") Long uuid){
        Response response;
        Either<IException, Long> IdThird = personBusiness.IdThirdFromUUID(uuid);

        if (IdThird.isRight()){
            System.out.println(IdThird.right().value());
            response=Response.status(Response.Status.OK).entity(IdThird.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(IdThird);
        }
        return response;
    }

    @Path("/{idPerson}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updatePersonResource(@Context Aplicacion aplicacion,@PathParam("idPerson") Long idPerson, PersonDTO personDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = personBusiness.updatePerson(idPerson, personDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{idPerson}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deletePersonResource(@Context Aplicacion aplicacion,@PathParam("idPerson") Long idPerson){
        Response response;
        Either<IException, Long> allViewOffertsEither = personBusiness.deletePerson(idPerson);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @GET
    @Path("/search")
    @Timed
    @RolesAllowed({"Auth"})
    public Response searchPerson(@QueryParam("doc_person") String document_number) {
        Response response;

        Either<IException, List<DocumentPerson2>> getCategoriesEither = personBusiness.searchPerson(document_number);
        if (getCategoriesEither.isRight()) {
            System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(getCategoriesEither.left().value()).build();
        }
        return response;
    }

    @GET
    @Path("/searchPersonFromId_Third")
    @Timed
    @RolesAllowed({"Auth"})

    public Response searchPersonFromId_Third(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<DocumentPerson2>> getCategoriesEither = personBusiness.searchPersonFromId_Third(id_third);
        if (getCategoriesEither.isRight()) {
            System.out.println("getCategoriesEither.right().value().size() es ");
            System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(getCategoriesEither.left().value()).build();
        }
        return response;
    }


    @GET
    @Path("/idperson")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getIdPerson(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, Long> getCategoriesEither = personBusiness.getIdPerson(id_third);
        if (getCategoriesEither.isRight()) {
            System.out.println(getCategoriesEither.right().value());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(getCategoriesEither.left().value()).build();
        }
        return response;
    }


    @Path("/actualizar_person")
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response actualizar_person(@Context Aplicacion aplicacion, UpdatePersonDTO updatePersonDTO){
        Response response;
        Either<IException, Long> retorno = personBusiness.actualizar_person(updatePersonDTO);

        if (retorno.isRight()){
            System.out.println(retorno.right().value());
            response=Response.status(Response.Status.OK).entity(retorno.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(retorno);
        }
        return response;
    }
}
