package com.name.business.translators;

import com.name.business.representations.ContactTranslatorDTO;
import com.name.business.representations.MailDTO;
import com.name.business.representations.PhoneDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luis on 12/04/17.
 */
public class StringTranslator {

    public static ContactTranslatorDTO contactPhoneTranslator(List<PhoneDTO> phoneDTOList) {
        List<String> stringList= new ArrayList<>();
        List<Integer> integerList= new ArrayList<>();
        for (PhoneDTO phoneDTO : phoneDTOList){
            stringList.add(phoneDTO.getPhone());
            integerList.add(phoneDTO.getPriority());

        }
        return new ContactTranslatorDTO(stringList,integerList);
    }

    public static ContactTranslatorDTO contactMailTranslator(List<MailDTO> phoneDTOList) {
        List<String> stringList= new ArrayList<>();
        List<Integer> integerList= new ArrayList<>();
        for (MailDTO phoneDTO : phoneDTOList){
            stringList.add(phoneDTO.getMail());
            integerList.add(phoneDTO.getPriority());

        }
        return new ContactTranslatorDTO(stringList,integerList);
    }


}
