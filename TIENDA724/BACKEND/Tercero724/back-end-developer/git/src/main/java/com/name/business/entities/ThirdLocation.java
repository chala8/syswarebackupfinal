package com.name.business.entities;

/**
 * Created by trossky on 14/07/17.
 */
public class ThirdLocation {

    private  Long id_third_location;
    private  Long id_common_third;
    private  Long id_third;
    private  Long id_location;
    private CommonThird state;

    public ThirdLocation(Long id_third_location, Long id_common_third, Long id_third, Long id_location, CommonThird state) {
        this.id_third_location = id_third_location;
        this.id_common_third = id_common_third;
        this.id_third = id_third;
        this.id_location = id_location;
        this.state = state;
    }

    public CommonThird getState() {
        return state;
    }

    public void setState(CommonThird state) {
        this.state = state;
    }

    public Long getId_third_location() {
        return id_third_location;
    }

    public void setId_third_location(Long id_third_location) {
        this.id_third_location = id_third_location;
    }

    public Long getId_common_third() {
        return id_common_third;
    }

    public void setId_common_third(Long id_common_third) {
        this.id_common_third = id_common_third;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public Long getId_location() {
        return id_location;
    }

    public void setId_location(Long id_location) {
        this.id_location = id_location;
    }
}
