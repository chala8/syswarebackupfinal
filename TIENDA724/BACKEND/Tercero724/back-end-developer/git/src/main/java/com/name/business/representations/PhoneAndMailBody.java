package com.name.business.representations;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.ws.rs.QueryParam;

public class PhoneAndMailBody {

    private Long id_directory;
    private String phone;
    private String mail;
    private String address;
    private Double lat;
    private Double lng;


    @JsonCreator
    public PhoneAndMailBody(@JsonProperty("id_directory") Long id_directory,
                            @JsonProperty("phone") String phone,
                            @JsonProperty("mail") String mail,
                            @JsonProperty("address") String address,
                            @JsonProperty("lat") Double lat,
                            @JsonProperty("lng") Double lng) {
        this.id_directory = id_directory;
        this.phone = phone;
        this.mail = mail;
        this.address=address;
        this.lat=lat;
        this.lng=lng;
    }

    public Long getId_directory() {
        return id_directory;
    }

    public void setId_directory(Long id_directory) {
        this.id_directory = id_directory;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
