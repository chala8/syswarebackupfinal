package com.name.business.businesses;

import com.name.business.DAOs.ThirdDAO;
import com.name.business.entities.*;
import com.name.business.representations.*;
import com.name.business.representations.complex.CreateThirdUserDTO;
import com.name.business.representations.complex.ThirdCompleteDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;


public class ThirdBusiness {

    private ThirdDAO thirdDAO;
    private PersonBusiness personBusiness;
    private CommonBasicInfoBusiness commonBasicInfoBusiness;
    private CommonThirdBusiness commonThirdBusiness;
    private DirectoryBusiness directoryBusiness;
    private LocationBusiness locationBusiness;
    private ThirdLocationBusiness thirdLocationBusiness;

    /**
     *
     * @param thirdDAO
     * @param personBusiness
     * @param commonBasicInfoBusiness
     * @param commonThirdBusiness
     * @param directoryBusiness
     * @param locationBusiness
     * @param thirdLocationBusiness
     */
    public ThirdBusiness(ThirdDAO thirdDAO, PersonBusiness personBusiness, CommonBasicInfoBusiness commonBasicInfoBusiness, CommonThirdBusiness commonThirdBusiness, DirectoryBusiness directoryBusiness, LocationBusiness locationBusiness, ThirdLocationBusiness thirdLocationBusiness) {
        this.thirdDAO = thirdDAO;
        this.personBusiness = personBusiness;
        this.commonBasicInfoBusiness = commonBasicInfoBusiness;
        this.commonThirdBusiness = commonThirdBusiness;
        this.directoryBusiness = directoryBusiness;
        this.locationBusiness = locationBusiness;
        this.thirdLocationBusiness = thirdLocationBusiness;
    }

    /**
     *
     * @param id_third
     * @param id_third_father
     * @param id_person
     * @param id_third_type
     * @param th_type_name
     * @param id_c_th_type
     * @param state_c_th_type
     * @param creation_c_th_type
     * @param modify_c_th_type
     * @param id_cb_third
     * @param id_typedoc_third
     * @param doc_third
     * @param typedoc_third
     * @param name_third
     * @param logo
     * @param id_common_third
     * @param state_third
     * @param creation_third
     * @param modify_third
     * @param first_name
     * @param second_name
     * @param first_lastname
     * @param second_lastname
     * @param birthday
     * @param id_dir_person
     * @param id_cbi_person
     * @param id_doctype_person
     * @param doc_person
     * @param typedoc_person
     * @param fullname_person
     * @param img_person
     * @param id_c_th_person
     * @param state_person
     * @param creation_person
     * @param modify_person
     * @return
     */
    public Either<IException, List<Third>> getThird(Long id_third, Long id_third_father,Long id_person, Long id_third_type, String th_type_name, Long id_c_th_type,
                                                   Integer state_c_th_type, Date creation_c_th_type, Date modify_c_th_type,
                                                    Long id_cb_third, Long id_typedoc_third,  String doc_third,String typedoc_third,  String name_third,
                                                    String logo, Long id_common_third, Integer state_third, Date creation_third, Date modify_third,
                                                    String first_name,
                                                    String second_name, String first_lastname,
                                                    String second_lastname, Date birthday,
                                                    Long id_dir_person,  Long id_cbi_person,
                                                    Long id_doctype_person, String doc_person,
                                                    Long typedoc_person, String fullname_person,
                                                    String img_person, String UUID,
                                                    Long id_c_th_person, Integer state_person,
                                                    Date creation_person, Date modify_person) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Third ||||||||||||||||| ");
            if (null == null) {
                List<Third> read = thirdDAO.read(formatoLongSql(id_third),formatoLongSql(id_third_father),formatoLongSql(id_person),formatoLongSql(id_third_type), formatoLIKESql(th_type_name),formatoLongSql(id_c_th_type), formatoIntegerSql(state_c_th_type),
                        formatoDateSql(creation_c_th_type),formatoDateSql(modify_c_th_type),formatoLongSql(id_cb_third),formatoLongSql(id_typedoc_third),formatoStringSql(doc_third),formatoLIKESql(typedoc_third),formatoLIKESql(name_third),
                        formatoStringSql(logo),formatoLongSql(id_common_third),formatoIntegerSql(state_third),formatoDateSql(creation_third),formatoDateSql(modify_third));

                /*Either<IException, List<Third>> loadThirdPerson=loadThirdPerson(read,first_name,second_name,first_lastname,second_lastname,birthday,id_dir_person,id_cbi_person,id_doctype_person,doc_person,typedoc_person,
                        fullname_person,img_person,UUID,
                        id_c_th_person,state_person,creation_person,modify_person);


                return Either.right(read);*/

                return loadThirdPerson(read,first_name,second_name,first_lastname,second_lastname,birthday,id_dir_person,id_cbi_person,id_doctype_person,doc_person,typedoc_person,
                        fullname_person,img_person,UUID,
                        id_c_th_person,state_person,creation_person,modify_person);

            } else {
                msn.add("It does not recognize Third , probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param read
     * @param first_name
     * @param second_name
     * @param first_lastname
     * @param second_lastname
     * @param birthday
     * @param id_dir_person
     * @param id_cbi_person
     * @param id_doctype_person
     * @param doc_person
     * @param typedoc_person
     * @param fullname_person
     * @param img_person
     * @param id_c_th_person
     * @param state_person
     * @param creation_person
     * @param modify_person
     * @return
     */
    private Either<IException, List<Third>> loadThirdPerson(List<Third> read,String first_name,
                                                            String second_name, String first_lastname,
                                                            String second_lastname, Date birthday,
                                                            Long id_dir_person,  Long id_cbi_person,
                                                            Long id_doctype_person, String doc_person,
                                                            Long typedoc_person, String fullname_person,
                                                            String img_person, String UUID,
                                                            Long id_c_th_person, Integer state_person,
                                                            Date creation_person, Date modify_person) {
        List<Directory> directoryList= new ArrayList<>();
        for (int i = 0; i < read.size(); i++) {
            Long id_person = (Long) read.get(i).getProfile();
            Long id_third_father = (Long) read.get(i).getId_third_father();
            if (id_person>0){
                System.out.println("Load data person");
                Either<IException, List<Person>> personEither = personBusiness.getPerson(formatoLongSql(id_person),
                        first_name,second_name,first_lastname,second_lastname,birthday,id_dir_person,id_cbi_person,id_doctype_person,
                        doc_person,typedoc_person,fullname_person,img_person,UUID,
                        id_c_th_person,state_person,creation_person,modify_person);
                if (personEither.isRight()){

                    List<Person> personList = personEither.right().value();
                    if (personList.size()>0){
                        read.get(i).setProfile(personList.get(0));
                    }

                }
            }

            if (id_third_father<=0  || id_person<=0){
                Either<IException, List<Directory>> locationEither = locationBusiness.getLocation(null,
                        read.get(i).getId_third(), null, null, null, null, null);
                if (locationEither.isRight()){
                    directoryList = locationEither.right().value();
                    read.get(i).setDirectory(directoryList);
                }

            }else{
                //TODO third location
                Either<IException, List<Directory>> thirdLocationEither = thirdLocationBusiness.getThirdLocation(null,
                        read.get(i).getId_third(), null, null, null, null, null);
                if (thirdLocationEither.isRight()){

                    directoryList = thirdLocationEither.right().value();
                    read.get(i).setDirectory(directoryList);

                }
            }
        }
        return Either.right(read);
    }
    public Either<IException, Long>  CreateThirdUser(CreateThirdUserDTO createThirdUserDTO){
        List<String> msn = new ArrayList<>();
        try {
            if(createThirdUserDTO.getFirstname() == null || createThirdUserDTO.getFirstname().equals("")){
                msn.add("Missing FName");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if(createThirdUserDTO.getSecondname() == null || createThirdUserDTO.getSecondname().equals("")){
                createThirdUserDTO.setSecondname("");
            }
            if(createThirdUserDTO.getFirstlastname() == null || createThirdUserDTO.getFirstlastname().equals("")){
                createThirdUserDTO.setFirstlastname("");
            }
            if(createThirdUserDTO.getSecondlastname() == null || createThirdUserDTO.getSecondlastname().equals("")){
                createThirdUserDTO.setSecondlastname("");
            }
            if(createThirdUserDTO.getIddocumenttype() <= 0){
                msn.add("Missing DocType");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if(createThirdUserDTO.getDocnumber() == null || createThirdUserDTO.getDocnumber().equals("")){
                msn.add("Missing Docnumber");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if(createThirdUserDTO.getDireccion() == null || createThirdUserDTO.getDireccion().equals("")){
                msn.add("Missing Address");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if(createThirdUserDTO.getIdcity() == null || createThirdUserDTO.getIdcity().equals("")){
                msn.add("Missing Idcity");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if(createThirdUserDTO.getTelefono() == null || createThirdUserDTO.getTelefono().equals("")){
                msn.add("Missing Phone");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if(createThirdUserDTO.getEmail() == null || createThirdUserDTO.getEmail().equals("")){
                msn.add("Missing Email");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if(createThirdUserDTO.getBirthDate() == null){
                msn.add("Missing BirthDate");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if(createThirdUserDTO.getGender() <= 0){
                msn.add("Missing Gender");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if(createThirdUserDTO.getLat() == 0){
                msn.add("Missing Lat");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if(createThirdUserDTO.getLng() == 0){
                msn.add("Missing Lng");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            //Si llega a este pounto es que tiene todos los datos
            thirdDAO.newUserThird(
                    "P",
                    createThirdUserDTO.getFirstname(),
                    createThirdUserDTO.getSecondname(),
                    createThirdUserDTO.getFirstlastname(),
                    createThirdUserDTO.getSecondlastname(),
                    createThirdUserDTO.getIddocumenttype(),
                    createThirdUserDTO.getDocnumber(),
                    createThirdUserDTO.getDireccion(),
                    createThirdUserDTO.getIdcity(),
                    createThirdUserDTO.getTelefono(),
                    createThirdUserDTO.getEmail(),
                    createThirdUserDTO.getBirthDate(),
                    createThirdUserDTO.getGender(),
                    createThirdUserDTO.getLat(),
                    createThirdUserDTO.getLng()
            );
            Long id_third=thirdDAO.getPkLast();
            return Either.right(id_third);
        }catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     *
     * @param thirdCompleteDTO
     * @param id_aplicacion
     * @return
     */
    public Either<IException, Long> createThird(ThirdCompleteDTO thirdCompleteDTO, Long id_aplicacion) {
        List<String> msn = new ArrayList<>();
        Long id_common_basicinfo=null;
        Long id_common_third=null;
        Long id_person=null;
        Long id_directory=null;
        Long id_third=null;
        System.out.println(thirdDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Third  ||||||||||||||||| ");
            if (thirdCompleteDTO != null) {



                if (thirdCompleteDTO.getId_third_father()!=null && thirdCompleteDTO.getId_third_father()>0 && !validatorID(thirdCompleteDTO.getId_third_father())){
                    msn.add("It ID Third Father  does not exist register on  Third  , probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }




                // TODO regiter basic info data
                if (thirdCompleteDTO.getBasicInfoDTO() != null) {

                    System.out.println("NULOOOOOOOOOOOOOOOOOOOOO");

                    Either<IException, Long> commonBasicInfoEither = commonBasicInfoBusiness.createCommonBasicInfo(thirdCompleteDTO.getBasicInfoDTO(),id_aplicacion);

                    if (commonBasicInfoEither.isRight()){
                        id_common_basicinfo=commonBasicInfoEither.right().value();
                    }
                }



                // TODO regiter common third
                if (thirdCompleteDTO.getCommon_thirdDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonThirdBusiness.createCommonThird(thirdCompleteDTO.getCommon_thirdDTO());
                    if (commonThirdEither.isRight()){
                        id_common_third=commonThirdEither.right().value();
                    }
                }

                // TODO regiter person
                if (thirdCompleteDTO.getPersonDTO() != null) {
                    Either<IException, Long> commonThirdEither = personBusiness.createPerson(thirdCompleteDTO.getPersonDTO(),id_aplicacion);
                    if (commonThirdEither.isRight()){
                        id_person=commonThirdEither.right().value();
                    }
                }
                    // TODO regiter third

                ThirdDTO thirdDTO= new ThirdDTO(thirdCompleteDTO.getId_third_type(),id_common_third,id_common_basicinfo,id_person,thirdCompleteDTO.getId_third_father());
                thirdDAO.create(thirdDTO,id_aplicacion);
                id_third=thirdDAO.getPkLast();

                    // TODO after regiter directory
                if (thirdCompleteDTO.getDirectoryDTO()!=null){
                    Either<IException, Long> directoryEither = directoryBusiness.createDirectory(thirdCompleteDTO.getDirectoryDTO(),id_aplicacion);
                    if (directoryEither.isRight()){
                        id_directory=directoryEither.right().value();

                        if (id_directory>0){

                            if(thirdCompleteDTO.getId_third_father()!=null && thirdCompleteDTO.getId_third_father()>0 && thirdCompleteDTO.getPersonDTO()!=null  ){
                                // Si es tercero hijo, se debe cargar la ubicacion de su padre.
                                Either<IException, List<LocationComplete>> locationEither = locationBusiness.getLocationComplete(null, thirdCompleteDTO.getId_third_father(), null, null, null, null, null);
                                if (locationEither.isRight() && locationEither.right().value().size()>0){
                                    Long id_location = locationEither.right().value().get(0).getId_location();

                                    Either<IException, Long> thirdLocationEither = thirdLocationBusiness.createThirdLocation(new ThirdLocationDTO(id_third, id_location, new Common_ThirdDTO(new Long(1), new Date(), new Date())));

                                    if (thirdLocationEither.isRight()){

                                    }


                                }else{
                                    Either<IException, Long> createlocationEither = locationBusiness.createLocation(new LocationDTO(id_third, id_directory, new Common_ThirdDTO(new Long(1), new Date(), new Date())));

                                    if (createlocationEither.isRight()){

                                    }
                                }


                            }else{
                                // Si es un padre, se crea la ubicacion geografica

                                Either<IException, Long> locationEither = locationBusiness.createLocation(new LocationDTO(id_third, id_directory, new Common_ThirdDTO(new Long(1), new Date(), new Date())));

                                if (locationEither.isRight()){

                                }
                            }


                        }
                    }
                }



                return Either.right(Long.valueOf(thirdDAO.getPkLast()));
            } else {
                msn.add("It does not recognize Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param id_third
     * @param thirdDTO
     * @return
     */
    public Either<IException, Long> updateThird(Long id_third ,ThirdCompleteDTO thirdDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update  Third ||||||||||||||||| ");
            if (thirdDTO != null) {

                if ( !validatorID(id_third)){
                    msn.add("It ID Third   does not exist register on  Third  , probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (thirdDTO.getId_third_father()!=null && thirdDTO.getId_third_father()>0 && !validatorID(thirdDTO.getId_third_father())){
                    msn.add("It ID Third Father  does not exist register on  Third  , probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }



                Third actualThird = thirdDAO.read(id_third, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null).get(0);

                // Validate Id_third_father inserted, if null, set the Id_third_father to the actual in the database, if not it is formatted to sql
                if (thirdDTO.getId_third_father() == null || thirdDTO.getId_third_father().equals("")) {
                    thirdDTO.setId_third_father(actualThird.getId_third_father());
                } else {
                    thirdDTO.setId_third_father(formatoLongSql(thirdDTO.getId_third_father()));
                }

                // Validate Id_third_type inserted, if null, set the Id_third_type to the actual in the database, if not it is formatted to sql
                if (thirdDTO.getId_third_type() == null || thirdDTO.getId_third_type().equals("")) {
                    thirdDTO.setId_third_type(actualThird.getType().getId_third_type());
                } else {
                    thirdDTO.setId_third_type(formatoLongSql(thirdDTO.getId_third_type()));
                }

                // Update common third
                commonThirdBusiness.updateCommonThird(actualThird.getState().getId_common_third(), thirdDTO.getCommon_thirdDTO());

                // Update common basic info
                commonBasicInfoBusiness.updateCommonBasicInfo(actualThird.getInfo().getId_common_basicinfo(), thirdDTO.getBasicInfoDTO());

                // Update directory
                directoryBusiness.updateDirectory(((Directory) actualThird.getDirectory()).getId_directory(), thirdDTO.getDirectoryDTO());

                thirdDAO.update(id_third, thirdDTO);

                return Either.right(null);

            } else {
                msn.add("It does not recognize  Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param idThird
     * @return
     */
    public Either<IException, Long> deleteThird(Long idThird) {
        List<String> msn = new ArrayList<>();
        try {
            if ( !validatorID(idThird)){
                msn.add("It ID Third   does not exist register on  Third  , probably it has bad ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            System.out.println("|||||||||||| Starting DELETE (Change State) DocumentType ||||||||||||||||| ");
            if (idThird != null && idThird > 0) {

                Long idCommon = thirdDAO.delete(formatoLongSql(idThird)).get(0);
                commonThirdBusiness.deleteCommonThird(idCommon);
                return Either.right(idThird);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = thirdDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    /**
     * @param id_third
     * */
    public Either<IException, BillThird> getThirdBasicData(Long id_third) {
        try {
            BillThird answer = thirdDAO.getThirdBasicData(id_third);
            return Either.right(answer);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Long>> IDThirdsFromIDStore(Long id_store) {
        try {
            List<Long> answer = thirdDAO.IDThirdsFromIDStore(id_store);
            return Either.right(answer);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long> getIdThird(Long id_document_type,String document_number) {
        try {
            Long answer = thirdDAO.getIdThird(id_document_type,document_number);
            return Either.right(answer);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<DocumentPerson> > getPerson(String document_number) {
        try {
            List<DocumentPerson> validator = thirdDAO.getPerson(document_number);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> getLastDirectory() {
        try {
            Long id = thirdDAO.getLastDirectory();
            return Either.right(id);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> putLastDirectory(Long id_third, Long id_directory) {
        try {
            thirdDAO.updateDir(id_third, id_directory);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<BillThird>> getThirdsForBill(Long id_third,
                                                                Date date1,
                                                                Date date2,
                                                                Long bill_type) {
        try {
            System.out.println(date1);
            System.out.println(date2);
             List<BillThird> answer = thirdDAO.getThirdsForBill(id_third,date1,date2,bill_type);
            return Either.right(answer);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<City>> getCities() {
        try {
            List<City> answer = thirdDAO.getCities();
            return Either.right(answer);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> updateDirectoryCity(Long id_third, String id_city) {
        try {
            String idDir =thirdDAO.getDirID(id_third);
            thirdDAO.updateDirectoryCity(idDir, id_city);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<ThirdData>> getThirdData(Long idstore) {
        try {
            List<ThirdData> idDir =thirdDAO.getThirdData(idstore);

            return Either.right(idDir);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Long newThird(String typesa,
                         String nombre1,
                         String nombre2,
                         String nombre3,
                         String nombre4,
                         Long doctype,
                         String docnumber,
                         String addres,
                         String id_city,
                         String telefono,
                         String correo,
                         Long idgenero,
                         String fechanacimiento)
    {
        try{

            System.out.println(id_city);
                thirdDAO.newThird(typesa,
                        nombre1,
                        nombre2,
                        nombre3,
                        nombre4,
                        doctype,
                        docnumber,
                        addres.replaceAll("_","#"),
                        id_city,
                        telefono,
                        correo,
                        idgenero,
                        new Date(fechanacimiento));

                Long responseId = thirdDAO.getPkLast();


                // Return the resultant string
                return new Long(responseId);
        }catch (Exception e){
            e.printStackTrace();
            return new Long(0);
        }

    }

    private Date increaseDate (Date a, long id_period) {
        Calendar c = Calendar.getInstance();
        c.setTime(a);
        if(id_period == -1)
        {c.add(Calendar.DAY_OF_MONTH,-1);}
        if(id_period == 1)
        {c.add(Calendar.DAY_OF_MONTH,1);}
        if(id_period == 2)
        {c.add(Calendar.DAY_OF_MONTH,7);}
        if(id_period == 3)
        {c.add(Calendar.MONTH, 1);}
        Date response = c.getTime();
        return response;
    }



    public Either<IException, List<Gender>> getGender() {
        try {
            List<Gender> idDir =thirdDAO.getGender();

            return Either.right(idDir);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, Long> getCity(Long id_city) {
        try {
            Long idDir =thirdDAO.getCity(id_city);

            return Either.right(idDir);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }








    public Either<IException, List<LogginData>> getLogginData( Long idthird) {
        try {
            List<LogginData> idDir =thirdDAO.getLogginData(idthird);

            return Either.right(idDir);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, Long> updateThird(Long idthird,
                                                String addressc,
                                                String phonec,
                                                String mailc) {
        try {
            thirdDAO.updateThird(idthird,
                     addressc,
                     phonec,
                     mailc);

            return Either.right(new Long(1));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

}
