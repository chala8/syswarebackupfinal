package com.name.business.representations;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class UpdatePersonDTO {

    private Long id_person;
    private String phone;
    private String mail;
    private String address;
    private String id_city;
    private Double lat;
    private Double lng;
    private Long genero;//el ID
    private Date birthDate;


    @JsonCreator
    public UpdatePersonDTO(@JsonProperty("id_person") Long id_person,
                           @JsonProperty("phone") String phone,
                           @JsonProperty("mail") String mail,
                           @JsonProperty("address") String address,
                           @JsonProperty("id_city") String id_city,
                           @JsonProperty("lat") Double lat,
                           @JsonProperty("lng") Double lng,
                           @JsonProperty("genero") Long genero,
                           @JsonProperty("birthDate") Date birthDate) {
        this.genero = genero;
        this.birthDate = birthDate;
        this.id_person = id_person;
        this.phone = phone;
        this.mail = mail;
        this.address=address;
        this.id_city=id_city;
        this.lat=lat;
        this.lng=lng;
    }

    public String getId_city() {
        return id_city;
    }

    public void setId_city(String id_city) {
        this.id_city = id_city;
    }

    public Long getGenero() {
        return genero;
    }

    public void setGenero(Long genero) {
        this.genero = genero;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Long getId_person() {
        return id_person;
    }

    public void setId_person(Long id_person) {
        this.id_person = id_person;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
