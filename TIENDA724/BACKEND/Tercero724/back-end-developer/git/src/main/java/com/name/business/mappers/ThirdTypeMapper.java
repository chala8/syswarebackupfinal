package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.ThirdType;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ThirdTypeMapper implements ResultSetMapper<ThirdType> {

    @Override
    public ThirdType map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        return new ThirdType(
        r.getLong("ID_THIRD_TYPE"),
        r.getLong("ID_COMMON_THIRD"),
        r.getString("NAME"),
                new CommonThird(
                        r.getLong("ID_COMMON_THIRD"),
                        r.getInt("STATE"),
                        r.getDate("CREATION_DATE"),
                        r.getDate("MODIFY_DATE")
                )
        );
    }
}
