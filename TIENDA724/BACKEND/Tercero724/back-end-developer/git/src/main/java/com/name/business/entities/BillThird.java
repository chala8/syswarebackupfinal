package com.name.business.entities;

public class BillThird {

    private String DOCUMENT_TYPE;
    private String DOCUMENT_NUMBER;
    private String FULLNAME;
    private Long NUMVENTAS;
    private Long TOTALVENTAS;
    private String ADDRESS;
    private String PHONE;
    private String MAIL;


    public BillThird(String DOCUMENT_TYPE,
                     String DOCUMENT_NUMBER,
                     String FULLNAME,
                     Long NUMVENTAS,
                     Long TOTALVENTAS,
                     String ADDRESS,
                     String PHONE,
                     String MAIL
                     ) {
        this.FULLNAME = FULLNAME;
        this.DOCUMENT_TYPE = DOCUMENT_TYPE;
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
        this.NUMVENTAS = NUMVENTAS;
        this.TOTALVENTAS = TOTALVENTAS;
        this.ADDRESS = ADDRESS;
        this.PHONE = PHONE;
        this.MAIL = MAIL;
    }

    public String getMAIL() {
        return MAIL;
    }

    public void setMAIL(String MAIL) {
        this.MAIL = MAIL;
    }

    public String getPHONE() {
        return PHONE;
    }

    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }

    public String getDOCUMENT_TYPE() {
        return DOCUMENT_TYPE;
    }

    public void setDOCUMENT_TYPE(String DOCUMENT_TYPE) {
        this.DOCUMENT_TYPE = DOCUMENT_TYPE;
    }

    public String getDOCUMENT_NUMBER() {
        return DOCUMENT_NUMBER;
    }

    public void setDOCUMENT_NUMBER(String DOCUMENT_NUMBER) {
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
    }

    public Long getNUMVENTAS() {
        return NUMVENTAS;
    }

    public void setNUMVENTAS(Long PHONE) {
        this.NUMVENTAS = NUMVENTAS;
    }

    public Long getTOTALVENTAS() {
        return TOTALVENTAS;
    }

    public void setTOTALVENTAS(Long TOTALVENTAS) {
        this.TOTALVENTAS = TOTALVENTAS;
    }




}
