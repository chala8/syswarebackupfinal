package com.name.business.mappers;

import com.name.business.entities.CommonBasicInfo;
import com.name.business.entities.CommonThird;
import com.name.business.entities.DocumentType;
import com.name.business.entities.Person;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class PersonMapper implements ResultSetMapper<Person>{
    @Override
    public Person map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Person(
        resultSet.getLong("ID_PERSON"),
        resultSet.getString("FIRST_NAME"),
        resultSet.getString("SECOND_NAME"),
        resultSet.getString("FIRST_LASTNAME"),
        resultSet.getString("SECOND_LASTNAME"),
        resultSet.getDate("BIRTHDAY"),
                new CommonBasicInfo(
                        resultSet.getLong("ID_CBI_PERSON"),
                        resultSet.getString("FULLNAME_PERSON"),
                        resultSet.getString("IMG_PERSON"),
                        resultSet.getString("DOC_PERSON"),
                        new DocumentType(
                                resultSet.getLong("ID_DOCTYPE"),
                                resultSet.getString("TYPEDOC_PPERSON"),
                                new CommonThird(
                                        resultSet.getLong("ID_CTH_DOCT"),
                                        resultSet.getInt("STATE_DOCT"),
                                        resultSet.getDate("CREATION_DOCT"),
                                        resultSet.getDate("MODIFY_DOCT")
                                )
                        )
                ),
                new CommonThird(
                        resultSet.getLong("ID_C_TH_PERSON"),
                        resultSet.getInt("STATE_PERSON"),
                        resultSet.getDate("CREATION_PERSON"),
                        resultSet.getDate("MODIFY_PERSON")
                ),
                resultSet.getLong("ID_DIR_PERSON"),
                null,
                null
        );
    }
}
