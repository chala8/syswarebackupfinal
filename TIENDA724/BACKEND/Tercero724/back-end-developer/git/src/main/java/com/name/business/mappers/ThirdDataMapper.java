package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.Mail;
import com.name.business.entities.ThirdData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ThirdDataMapper implements ResultSetMapper<ThirdData> {

    @Override
    public ThirdData map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new ThirdData(
                resultSet.getLong("ID_THIRD"),
                resultSet.getString("FULLNAME"),
                resultSet.getString("NAME"),
                resultSet.getString("DOCUMENT_NUMBER"),
                resultSet.getString("MAIL"),
                resultSet.getString("ADDRESS"),
                resultSet.getString("PHONE")
        );
    }
}
