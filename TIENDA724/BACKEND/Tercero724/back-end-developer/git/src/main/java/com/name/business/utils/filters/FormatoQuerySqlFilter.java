package com.name.business.utils.filters;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by luis on 12/04/17.
 */
public  class FormatoQuerySqlFilter {

    public static String formatoLIKESql(String input){
        if (input!=null)
            return input.isEmpty()?null:"%"+input+"%";
        return null;
    }
    public static String formatoStringSql(String input){
        if (input!=null)
            return input.isEmpty()?null:""+input+"";
        return null;
    }
    public static String formatoDouebleLIKESql(Double input){
        if (input!=null)
            return input<0?null:"%"+input+"%";
        return null;
    }



    public static Long formatoLongSql(Long input){
        if (input!=null)
            return input<1?null:+input;
        return null;
    }

    public static Date formatoDateSql(Date input){
        if (input!=null)
            return input;
        return null;
    }
    public static Integer formatoIntegerSql(Integer input){
        if (input!=null)
            return input<1?null:+input;
        return null;
    }

    public static Double formatoDoubleSql(Double input){
        if (input!=null)
            return input<0?null:+input;
        return null;
    }
    public static Timestamp formatoFechaFinSql(Long input){
        if(input!=null){

            Timestamp timestamp = new Timestamp(input);
            return new Timestamp(timestamp.getYear(),timestamp.getMonth(),timestamp.getDate()+1,0,0,0,0);
        }else{
            return  null;
        }
    }

    public static Timestamp formatoFechaInicioSql(Long input){
        return  (input==null)? Timestamp.valueOf(LocalDateTime.now()):new Timestamp(input);
    }
}
