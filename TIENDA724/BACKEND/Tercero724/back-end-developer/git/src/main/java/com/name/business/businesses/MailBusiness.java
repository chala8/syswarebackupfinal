package com.name.business.businesses;

import com.name.business.DAOs.MailDAO;
import com.name.business.entities.Mail;
import com.name.business.representations.ContactTranslatorDTO;
import com.name.business.representations.MailDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import org.glassfish.jersey.internal.util.collection.KeyComparator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.translators.StringTranslator.contactMailTranslator;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

/**
 * Created by trossky on 14/07/17.
 */
public class MailBusiness {
    private MailDAO mailDAO;
    private CommonThirdBusiness commonThirdBusiness;

    /**
     * 
     * @param mailDAO
     */
    public MailBusiness(MailDAO mailDAO, CommonThirdBusiness commonThirdBusiness) {
        this.mailDAO = mailDAO;
        this.commonThirdBusiness = commonThirdBusiness;
    }

    /**
     * 
     * @param id_directory
     * @param mailDTOList
     * @return
     */
    public  Either<IException, String> createMail(Long id_directory, List<MailDTO> mailDTOList, Long id_app) {
        List<String> msn = new ArrayList<>();
        System.out.println(mailDAO.getPkLast());
        List<Long> id_common_third_list=new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Mail  ||||||||||||||||| ");
            if (mailDTOList!=null || mailDTOList.size()>0 ) {

                if (validatorDirectoryID(id_directory).equals(false)){
                    msn.add("It ID Directory does not exist register on  Directory, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                ContactTranslatorDTO contacttranslatorDTO = contactMailTranslator(mailDTOList);
                Long value;
                for (MailDTO mailDTO: mailDTOList){
                    value=new Long(0);
                    Either<IException, Long> commonThirdEither = commonThirdBusiness.createCommonThird(mailDTO.getState());
                    if (commonThirdEither.isRight()){
                        value=commonThirdEither.right().value();

                    }

                    if (value<=0){
                        id_common_third_list.add(null);
                    }else{
                        id_common_third_list.add(value);
                    }

                }
                for (String s:contacttranslatorDTO.getContact()){
                    Either<IException, Boolean> validatorUUIDByAPPEither = getValidatorMAILByAPP(s, id_app, null);
                    if (validatorUUIDByAPPEither.isRight()){
                        if (validatorUUIDByAPPEither.right().value()){
                            msn.add("This MAIL already exist");
                            return Either.left(new BussinessException(messages_errors(msn)));
                        }
                    }

                }
                mailDAO.bulkMail(id_directory,contacttranslatorDTO.getContact(),contacttranslatorDTO.getPriority(),id_common_third_list,id_app);
                return Either.right(msn.get(0));
            } else {
                msn.add("It does not recognize Mail, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * 
     * @param id_mail
     * @param mail
     * @param priority
     * @param id_directory
     * @return
     */
    public Either<IException, List<Mail>> getMails(Long id_mail, String mail, Integer priority, Long id_directory,
                                                   Long id_common_third, Integer state, Date creation_mail, Date modify_mail) {
        try{
            List<Mail> read = mailDAO.read(formatoLongSql(id_mail),formatoStringSql(mail),formatoIntegerSql(priority),formatoLongSql(id_directory),
                    formatoLongSql(id_common_third),formatoIntegerSql(state),formatoDateSql(creation_mail),formatoDateSql(modify_mail));
            return Either.right(read);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * 
     * @param idMail
     * @param mailDTO
     * @param id_app
     * @return
     */
    public Either<IException, Long> updateMail(Long idMail, MailDTO mailDTO, Long id_app) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting UPDATE mail ||||||||||||||||| ");
            if (idMail != null && idMail > 0) {

                if (validatorID(idMail).equals(false)){
                    msn.add("It ID MAIL does not exist register on  MAIL, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<Mail> mails = mailDAO.read(idMail, null, null, null,null,null,null,null);
                if(mails.size() > 0){
                Mail actualMail = mails.get(0);
                // Validate mail inserted, if null, set the name to the actual in the database, if not it is formatted to sql
                if (mailDTO.getMail() == null || mailDTO.getMail().equals("")) {
                    mailDTO.setMail(actualMail.getMail());
                } else {
                    mailDTO.setMail(formatoStringSql(mailDTO.getMail()));
                }

                // Validate priority inserted, if null, set the name to the actual in the database, if not it is formatted to sql
                if (mailDTO.getPriority() == null || mailDTO.getPriority() < 0) {
                    mailDTO.setPriority(actualMail.getPriority());
                } else {
                    mailDTO.setPriority(formatoIntegerSql(mailDTO.getPriority()));
                }

                    Either<IException, Boolean> validatorUUIDByAPPEither = getValidatorMAILByAPP(mailDTO.getMail(), id_app, idMail);
                    if (validatorUUIDByAPPEither.isRight()){
                        if (validatorUUIDByAPPEither.right().value()){
                            msn.add("This MAIL already exist");
                            return Either.left(new BussinessException(messages_errors(msn)));
                        }
                    }

                
                mailDAO.update(formatoLongSql(idMail), mailDTO);

                //TO DO when mail has common third variable
                Long idCommon = mailDAO.delete(formatoLongSql(idMail)).get(0);
                commonThirdBusiness.updateCommonThird(idCommon, mailDTO.getState());

                return Either.right(idMail);}
                else{
                    msn.add("It does not recognize ID Mail, probably it does not exist");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param idMail
     * @return
     */
    public Either<IException, Long> deleteMailType(Long idMail) {
        List<String> msn = new ArrayList<>();
        try {
            if (validatorID(idMail).equals(false)){
                msn.add("It ID MAIL does not exist register on  MAIL, probably it has bad ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            System.out.println("|||||||||||| Starting DELETE (Change State) DocumentType ||||||||||||||||| ");
            if (idMail != null && idMail > 0) {

                Long idCommon = mailDAO.delete(formatoLongSql(idMail)).get(0);

                //TO DO when Mail has common Third implemented
                //commonThirdBusiness.deleteCommonThird(idCommon);
                return Either.right(idMail);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = mailDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    private Boolean validatorDirectoryID(Long id_directory) {
        Integer validator = 0;
        try {
            validator = mailDAO.validatorDirectoryID(id_directory);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    public Either<IException, Boolean> getValidatorMAILByAPP(String mail,Long id_app, Long idUserThird) {
        List<String> msn = new ArrayList<>();
        Integer validator = 0;

        try {

            validator = mailDAO.getValidatorMAILByAPP(formatoStringSql(mail),formatoLongSql(id_app),formatoLongSql(idUserThird));
            if (validator>0){

                return Either.right(true);
            }
            return Either.right(false);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
