package com.name.business.businesses;

import com.name.business.DAOs.UserThirdDAO;
import com.name.business.entities.UserThird;
import com.name.business.representations.UserThirdDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoDateSql;

/**
 * Created by trossky on 14/07/17.
 */
public class UserThirdBusiness {

   private UserThirdDAO userThirdDAO;
   private CommonThirdBusiness commonThirdBusiness;

    /**
     *
     * @param userThirdDAO
     * @param commonThirdBusiness
     */
    public UserThirdBusiness(UserThirdDAO userThirdDAO, CommonThirdBusiness commonThirdBusiness) {
        this.userThirdDAO = userThirdDAO;
        this.commonThirdBusiness = commonThirdBusiness;
    }

    /**
     *
     * @param id_user_third
     * @param UUID
     * @param id_person
     * @param id_common_third
     * @param state
     * @param creation_user
     * @param modify_user
     * @return
     */
    public Either<IException, List<UserThird>> getUserThird(Long id_user_third, String UUID, Long id_person,
                                                            Long id_common_third, Integer state,
                                                            Date creation_user, Date modify_user) {
        List<String> msn = new ArrayList<>();

        try{
            System.out.println("|||||||||||| Starting consults data User ||||||||||||||||| ");

            List<UserThird> read = userThirdDAO.read(formatoLongSql(id_user_third),formatoStringSql(UUID),
                                                    formatoLongSql(id_person),formatoLongSql(id_common_third),
                    formatoIntegerSql(state),formatoDateSql(creation_user),formatoDateSql(modify_user));

            return Either.right(read);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    /**
     *
     * @param id_person
     * @param userThirdDTO
     * @param id_aplicacion
     * @return
     */
    public Either<IException, Long> createUserThird(Long id_person, UserThirdDTO userThirdDTO, Long id_aplicacion) {
        List<String> msn = new ArrayList<>();

        Long id_common_third=null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation User Third ||||||||||||||||| ");
            if (id_person!=null && userThirdDTO != null) {

                if (validatorIDPerson(id_person).equals(false)){
                    msn.add("It ID Person does not exist register on  Person, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (userThirdDTO.getState()!=null){
                    Either<IException, Long> commonThirdEither = commonThirdBusiness.createCommonThird(userThirdDTO.getState());

                    if (commonThirdEither.isRight()){
                        id_common_third=commonThirdEither.right().value();
                    }
                }

                Either<IException, Boolean> validatorUUIDByAPPEither = getValidatorUUIDByAPP(userThirdDTO.getUUID(), id_aplicacion, null);
                if (validatorUUIDByAPPEither.isRight()){
                    if (validatorUUIDByAPPEither.right().value()){
                        msn.add("This UUID already exist");
                        return Either.left(new BussinessException(messages_errors(msn)));
                    }
                }

                userThirdDAO.create(userThirdDTO.getUUID(),formatoLongSql(id_person),id_common_third,formatoLongSql(id_aplicacion));

                return Either.right(userThirdDAO.getPkLast());

            } else {
                msn.add("It does not recognize User Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> updateUserThird(Long idUser, Long id_app, UserThirdDTO userThirdDTO) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting UPDATE DocumentType ||||||||||||||||| ");
            if (idUser != null && idUser > 0) {
                if (validatorID(idUser).equals(false)){
                    msn.add("It ID User Third does not exist register on  User Third, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                UserThird actualUserThird = userThirdDAO.read(formatoLongSql(idUser), null, null, null, null, null, null).get(0);

                // Validate UUID inserted, if null, set the UUID to the actual in the database, if not it is formatted to sql
                if (userThirdDTO.getUUID() == null || userThirdDTO.getUUID().equals("")) {
                    userThirdDTO.setUUID(formatoStringSql(actualUserThird.getUUID()));
                } else {
                    userThirdDTO.setUUID(formatoStringSql(userThirdDTO.getUUID()));
                }
                Either<IException, Boolean> validatorUUIDByAPPEither = getValidatorUUIDByAPP(userThirdDTO.getUUID(), id_app, idUser);
                if (validatorUUIDByAPPEither.isRight()){
                    if (validatorUUIDByAPPEither.right().value()){
                        msn.add("This UUID already exist");
                        return Either.left(new BussinessException(messages_errors(msn)));
                    }
                }

                userThirdDAO.update(formatoLongSql(idUser), userThirdDTO);
                Long idCommon = userThirdDAO.delete(formatoLongSql(idUser)).get(0);
                commonThirdBusiness.updateCommonThird(idCommon, userThirdDTO.getState());
                return Either.right(idUser);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
    
    /**
     *
     * @param idUser
     * @return
     */
    public Either<IException, Long> deleteUserThird(Long idUser) {
        List<String> msn = new ArrayList<>();
        try {
            if (validatorID(idUser).equals(false)){
                msn.add("It ID User Third does not exist register on  User Third, probably it has bad ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

            System.out.println("|||||||||||| Starting DELETE (Change State) DocumentType ||||||||||||||||| ");
            if (idUser != null && idUser > 0) {

                Long idCommon = userThirdDAO.delete(formatoLongSql(idUser)).get(0);

                // TO DO when thidLocation has common third
                commonThirdBusiness.deleteCommonThird(idCommon);
                return Either.right(idUser);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = userThirdDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    /**
     * @param id_person
     * */
    public Boolean validatorIDPerson(Long id_person) {
        Integer validator = 0;
        try {
            validator = userThirdDAO.getValidatorIDPerson(id_person);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    public Either<IException, Boolean> getValidatorUUIDByAPP(String uuid,Long id_app, Long idUserThird) {
        List<String> msn = new ArrayList<>();
        Integer validator = 0;

        try {

            validator = userThirdDAO.getValidatorUUIDByAPP(formatoStringSql(uuid),formatoLongSql(id_app),formatoLongSql(idUserThird));
            if (validator>0){

                return Either.right(true);
            }
            return Either.right(false);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Boolean actualizar_Thirdfather(String user) {
        try {
            userThirdDAO.actualizar_Thirdfather(user);
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
