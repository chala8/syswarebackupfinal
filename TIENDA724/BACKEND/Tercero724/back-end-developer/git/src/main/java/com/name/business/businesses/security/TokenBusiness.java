package com.name.business.businesses.security;


import com.name.business.DAOs.AplicacionDAO;
import com.name.business.DAOs.TokenDAO;
import com.name.business.entities.Aplicacion;
import com.name.business.representations.TiketDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.utils.exeptions.ManagementException.INTERNAL_ERROR_MESSAGE;


/**
 * Created by luis_ on 28/01/2017.
 */
public class TokenBusiness {

    private TokenDAO tokenDAO;
    private AplicacionDAO aplicacionDAO;

    public TokenBusiness(TokenDAO tokenDAO, AplicacionDAO aplicacionDAO) {
        this.tokenDAO = tokenDAO;
        this.aplicacionDAO = aplicacionDAO;
    }

    public Either<IException, Aplicacion> tokenIsAuthorized(TiketDTO tiketDTO) {

        Either<IException,Aplicacion> responseEither=null;
        try {
            //Token consultToken=tokenDAO.FIND_TOKEN(tiketDTO.getToken_value());
            List<Aplicacion> aplicacionList= new ArrayList<>();
             aplicacionList = aplicacionDAO.OBTENER_APLICACIONES(null, null, null, null, null,
                    null, null, tiketDTO.getToken_value(), null);
            if(aplicacionList!=null && aplicacionList.size()>0 && aplicacionList.size()==1 ){

                return Either.right(aplicacionList.get(0));
                /*
                Either<IException,List<String>> URIs_forRoleUsingHttpVerbEither =
                        getURIs_forRoleUsingHttpVerb(consultToken.getId_role(),tiketDTO.getHttp_verb());

                if (URIs_forRoleUsingHttpVerbEither.isRight()){
                    List<String>getURIList=URIs_forRoleUsingHttpVerbEither.right().value();

                    if (!getURIList.isEmpty()){
                        boolean isURIPermitted=false;
                        for (int i = 0; i < getURIList.size() && !isURIPermitted; i++) {
                            String URI= API_VERSSION +getURIList.get(i);
                            isURIPermitted= urlsMatch(tiketDTO.getRequested_uri(),URI);

                        }
                        if (isURIPermitted){
                            TokenDTO tokenDTO= new TokenDTO(consultToken.getKEY_TOKEN(),consultToken.getId_role(),
                                    consultToken.getId_person(),consultToken.getId_user(),1,consultToken.getUsername(),consultToken.getFullName(),
                                    consultToken.getLastName());
                            return Either.right(tokenDTO);
                        }else{
                            return Either.right(null);
                        }
                    }else{
                        return Either.right(null);
                    }
                }else{
                    return Either.left(URIs_forRoleUsingHttpVerbEither.left().value());
                }
*/
            }else{
                /**
                 * TODO si el token es falso, no esta autorizado, hacer un insert en la base de datos,
                 * que registre el intento de acceder a una url con una session especifica.
                 * */
                return Either.left(new BussinessException("The KEY_TOKEN isn't valid"));

            }
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(INTERNAL_ERROR_MESSAGE());
        }

    }

    private Either<IException, List<String>> getURIs_forRoleUsingHttpVerb(long id_role, String http_verb) {
        try {
            List<String> getURIList=tokenDAO.FIND_URIS_BY_ROLE(id_role,http_verb);
            return Either.right(getURIList);
        }catch (Exception e){
            return Either.left(INTERNAL_ERROR_MESSAGE());
        }
    }

    public Either<IException, Boolean> deleteToken(long id_user, String KEY_TOKEN) {
        try {
            int affect_rows=tokenDAO.DELETE_TOKEN(KEY_TOKEN);
            if (affect_rows>0){
                return Either.right(true);
            }else{
                return Either.left(INTERNAL_ERROR_MESSAGE());
            }
        }catch (Exception e){
            return Either.left(INTERNAL_ERROR_MESSAGE());
        }


    }


    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = tokenDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }



}
