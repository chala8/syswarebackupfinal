package com.name.business.entities;

/**
 * Created by trossky on 14/07/17.
 */
public class ThirdData {

    private Long ID_THIRD;
    private String FULLNAME;
    private String NAME;
    private String DOCUMENT_NUMBER;
    private String MAIL;
    private String ADDRESS;
    private String PHONE;

    public ThirdData(Long ID_THIRD,
             String FULLNAME,
             String NAME,
             String DOCUMENT_NUMBER,
             String MAIL,
             String ADDRESS,
             String PHONE) {
        this.ID_THIRD = ID_THIRD;
        this.FULLNAME = FULLNAME;
        this.NAME = NAME;
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
        this.MAIL = MAIL;
        this.ADDRESS = ADDRESS;
        this.PHONE = PHONE;
    }

    public Long getID_THIRD() { return ID_THIRD; }
    public void setID_THIRD(Long ID_THIRD) {
        this.ID_THIRD = ID_THIRD;
    }
    public String getFULLNAME(){ return FULLNAME; }
    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }
    public String getNAME(){ return NAME; }
    public void setNAME(String NAME) {
        this.NAME = NAME;
    }
    public String getDOCUMENT_NUMBER(){ return DOCUMENT_NUMBER; }
    public void setDOCUMENT_NUMBER(String DOCUMENT_NUMBER) {
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
    }
    public String getMAIL(){ return MAIL; }
    public void setMAIL(String MAIL) {
        this.MAIL = MAIL;
    }
    public String getADDRESS(){ return ADDRESS; }
    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }
    public String getPHONE(){ return PHONE; }
    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

}
