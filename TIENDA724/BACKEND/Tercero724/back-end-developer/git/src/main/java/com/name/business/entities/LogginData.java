package com.name.business.entities;

/**
 * Created by trossky on 14/07/17.
 */
public class LogginData {
    public Long getID_THIRD_FATHER() {
        return ID_THIRD_FATHER;
    }

    public void setID_THIRD_FATHER(Long ID_THIRD_FATHER) {
        this.ID_THIRD_FATHER = ID_THIRD_FATHER;
    }

    public String getFULLNAMEFATHER() {
        return FULLNAMEFATHER;
    }

    public void setFULLNAMEFATHER(String FULLNAMEFATHER) {
        this.FULLNAMEFATHER = FULLNAMEFATHER;
    }

    public String getDOCUTYPEFATHER() {
        return DOCUTYPEFATHER;
    }

    public void setDOCUTYPEFATHER(String DOCUTYPEFATHER) {
        this.DOCUTYPEFATHER = DOCUTYPEFATHER;
    }

    public String getDOCNUMBERFATHER() {
        return DOCNUMBERFATHER;
    }

    public void setDOCNUMBERFATHER(String DOCNUMBERFATHER) {
        this.DOCNUMBERFATHER = DOCNUMBERFATHER;
    }

    public String getFIRST_NAME() {
        return FIRST_NAME;
    }

    public void setFIRST_NAME(String FIRST_NAME) {
        this.FIRST_NAME = FIRST_NAME;
    }

    public String getSECOND_NAME() {
        return SECOND_NAME;
    }

    public void setSECOND_NAME(String SECOND_NAME) {
        this.SECOND_NAME = SECOND_NAME;
    }

    public String getFIRST_LASTNAME() {
        return FIRST_LASTNAME;
    }

    public void setFIRST_LASTNAME(String FIRST_LASTNAME) {
        this.FIRST_LASTNAME = FIRST_LASTNAME;
    }

    public String getSECOND_LASTNAME() {
        return SECOND_LASTNAME;
    }

    public void setSECOND_LASTNAME(String SECOND_LASTNAME) {
        this.SECOND_LASTNAME = SECOND_LASTNAME;
    }

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }

    public Long getID_PERSON() {
        return ID_PERSON;
    }

    public void setID_PERSON(Long ID_PERSON) {
        this.ID_PERSON = ID_PERSON;
    }

    public String getDOCUMENT_NUMBER() {
        return DOCUMENT_NUMBER;
    }

    public void setDOCUMENT_NUMBER(String DOCUMENT_NUMBER) {
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
    }

    public String getDOCUMENT_TYPE() {
        return DOCUMENT_TYPE;
    }

    public void setDOCUMENT_TYPE(String DOCUMENT_TYPE) {
        this.DOCUMENT_TYPE = DOCUMENT_TYPE;
    }

    public String getIMG() {
        return IMG;
    }

    public void setIMG(String IMG) {
        this.IMG = IMG;
    }

    private Long ID_THIRD_FATHER;
    private String FULLNAMEFATHER;
    private String DOCUTYPEFATHER;
    private String DOCNUMBERFATHER;
    private String FIRST_NAME;
    private String SECOND_NAME;
    private String FIRST_LASTNAME;
    private String SECOND_LASTNAME;
    private String FULLNAME;
    private Long ID_PERSON;
    private String DOCUMENT_NUMBER;
    private String DOCUMENT_TYPE;
    private String IMG;

    public LogginData(Long ID_THIRD_FATHER,
             String FULLNAMEFATHER,
             String DOCUTYPEFATHER,
             String DOCNUMBERFATHER,
             String FIRST_NAME,
             String SECOND_NAME,
             String FIRST_LASTNAME,
             String SECOND_LASTNAME,
             String FULLNAME,
             Long ID_PERSON,
             String DOCUMENT_NUMBER,
             String DOCUMENT_TYPE,
             String IMG) {
        this.ID_THIRD_FATHER = ID_THIRD_FATHER;
        this.FULLNAMEFATHER = FULLNAMEFATHER;
        this.DOCUTYPEFATHER = DOCUTYPEFATHER;
        this.DOCNUMBERFATHER = DOCNUMBERFATHER;
        this.FIRST_NAME = FIRST_NAME;
        this.SECOND_NAME = SECOND_NAME;
        this.FIRST_LASTNAME = FIRST_LASTNAME;
        this.SECOND_LASTNAME = SECOND_LASTNAME;
        this.FULLNAME = FULLNAME;
        this.ID_PERSON = ID_PERSON;
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
        this.DOCUMENT_TYPE = DOCUMENT_TYPE;
        this.IMG = IMG;
    }

}
