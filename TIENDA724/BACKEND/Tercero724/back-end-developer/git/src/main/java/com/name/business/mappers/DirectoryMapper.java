package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.Directory;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DirectoryMapper implements ResultSetMapper<Directory>  {

    @Override
    public Directory map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Directory(
        resultSet.getLong("ID_DIRECTORY"),
        resultSet.getString("ADDRESS"),
        resultSet.getString("COUNTRY"),
        resultSet.getString("CITY"),
        resultSet.getString("WEBPAGE"),
                new CommonThird(
                        resultSet.getLong("ID_CT_DIRECTORY"),
                        resultSet.getInt("STATE_DIR"),
                        resultSet.getDate("CREATION_DIR"),
                        resultSet.getDate("MODIFY_DIR")
                ),
                null,
                null,
                0,
                0
        );
    }
}
