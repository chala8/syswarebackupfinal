package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.ThirdTypeBusiness;
import com.name.business.entities.ThirdType;
import com.name.business.entities.Token;
import com.name.business.representations.ThirdTypeDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

/**
 * Created by luis on 21/07/17.
 */
@Path("/thirds-type")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ThirdTypeResource {

    private final ThirdTypeBusiness thirdTypeBusiness;

    /**
     *
     * @param thirdTypeBusiness
     */
    public ThirdTypeResource(ThirdTypeBusiness thirdTypeBusiness) {
        this.thirdTypeBusiness = thirdTypeBusiness;
    }

    /**
     *
     * @param id_third_type
     * @param name
     * @param id_common_third
     * @param state
     * @return
     */
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getThirdTypeResourceList(@QueryParam("id_third_type") Long id_third_type, @QueryParam("name")String name, @QueryParam("id_common_third") Long id_common_third, @QueryParam("state") Integer state){
        Response response;

        Either<IException, List<ThirdType>> allViewOffertsEither = thirdTypeBusiness.getThirdType(id_third_type, name,id_common_third,state);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param thirdTypeDTO
     * @return
     */
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postThirdTypeResource(@Context Token token, @QueryParam("id_app") Long id_app, ThirdTypeDTO thirdTypeDTO){
        Response response;

        if (id_app==null){
            if (token.getAplicacion()!=null){
                id_app=token.getAplicacion().getId_aplicacion();
            }
        }

        Either<IException, Long> allViewOffertsEither =thirdTypeBusiness.createThirdType(thirdTypeDTO,formatoLongSql(id_app));


        if (allViewOffertsEither.isRight()){

            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param idThirdType
     * @param thirdTypeDTO
     * @return
     */
    @Path("/{idThirdType}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateThirdTypeResource(@PathParam("idThirdType") Long idThirdType, ThirdTypeDTO thirdTypeDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = thirdTypeBusiness.updateThirdType(idThirdType, thirdTypeDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param idThirdType
     * @return
     */
    @Path("/{idThirdType}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deleteThirdTypeResource(@PathParam("idThirdType") Long idThirdType){
        Response response;
        Either<IException, Long> allViewOffertsEither = thirdTypeBusiness.deleteThirdType(idThirdType);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
