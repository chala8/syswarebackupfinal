package com.name.business.entities;

public class DocumentPerson {

    private String FULLNAME;
    private String DOCUMENT_TYPE;
    private String DOCUMENT_NUMBER;
    private String CITY;
    private String ADDRESS;
    private String PHONE;
    private Long ID_PERSON;


    public DocumentPerson(String FULLNAME,
                          String DOCUMENT_TYPE,
                          String DOCUMENT_NUMBER,
                          String CITY,
                          String ADDRESS,
                          String PHONE,
                          Long ID_PERSON) {
        this.FULLNAME = FULLNAME;
        this.DOCUMENT_TYPE = DOCUMENT_TYPE;
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
        this.CITY = CITY;
        this.ADDRESS = ADDRESS;
        this.PHONE = PHONE;
        this.ID_PERSON = ID_PERSON;
    }

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }

    public String getDOCUMENT_TYPE() {
        return DOCUMENT_TYPE;
    }

    public void setDOCUMENT_TYPE(String DOCUMENT_TYPE) {
        this.DOCUMENT_TYPE = DOCUMENT_TYPE;
    }

    public String getDOCUMENT_NUMBER() {
        return DOCUMENT_NUMBER;
    }

    public void setDOCUMENT_NUMBER(String DOCUMENT_NUMBER) {
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
    }

    public String getCITY() {
        return CITY;
    }

    public void setCITY(String CITY) {
        this.CITY = CITY;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getPHONE() {
        return PHONE;
    }

    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    public Long getID_PERSON() {
        return ID_PERSON;
    }

    public void setID_PERSON(Long ID_PERSON) {
        this.ID_PERSON = ID_PERSON;
    }




}
