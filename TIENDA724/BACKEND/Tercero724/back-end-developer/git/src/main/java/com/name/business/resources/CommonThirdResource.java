package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.CommonThirdBusiness;
import com.name.business.entities.CommonThird;
import com.name.business.representations.Common_ThirdDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/commons-third")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CommonThirdResource {

    private final CommonThirdBusiness commonThirdBusiness;

    /**
     *
     * @param commonThirdBusiness
     */
    public CommonThirdResource(CommonThirdBusiness commonThirdBusiness) {
        this.commonThirdBusiness = commonThirdBusiness;
    }

    /**
     *
     * @param id_common_third
     * @param STATE
     * @param creation_date
     * @param modify_date
     * @return
     */
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCommonThirdResourceList(@QueryParam("id_common_third") Long id_common_third, @QueryParam("state")Integer STATE, @QueryParam("creation_date") Date creation_date, @QueryParam("modify_date") Date modify_date){
        Response response;
        Either<IException, List<CommonThird>> allViewOffertsEither = commonThirdBusiness.getCommonThird(id_common_third,  STATE, creation_date, modify_date);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param idCommonThird
     * @param ctDTO
     * @return
     */
    @Path("/{idCommonThird}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateCommonThirdResource(@PathParam("idCommonThird") Long idCommonThird, Common_ThirdDTO ctDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = commonThirdBusiness.updateCommonThird(idCommonThird, ctDTO);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param idCommonThird
     * @return
     */
    @Path("/{idCommonThird}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deleteCommonThirdResource(@PathParam("idCommonThird") Long idCommonThird){
        Response response;
        Either<IException, Long> allViewOffertsEither = commonThirdBusiness.deleteCommonThird(idCommonThird);

        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }
}
