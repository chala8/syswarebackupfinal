package com.name.business.sanitizers;

import com.name.business.entities.ThirdType;

import java.util.Date;

import static com.name.business.sanitizers.CommonThirdSanitize.toCommonThird;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class ThirdTypeSanitize {

    public static ThirdType toThirdType(Long id_third_type,Long id_common_third, String name, Integer state_third, Date creation_third, Date modify_third){
        ThirdType  thirdType= new ThirdType(formatoLongSql(id_third_type),formatoLongSql(id_common_third),formatoStringSql(name),
                toCommonThird(id_common_third,state_third,creation_third,modify_third));
        return thirdType;

    }
}
