package com.name.business.mappers;

import com.name.business.entities.CommonThird;
import com.name.business.entities.Directory;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DirectoryMapperv2 implements ResultSetMapper<Directory>  {

    @Override
    public Directory map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Directory(
            resultSet.getLong("ID_DIRECTORY"),
            resultSet.getString("ADDRESS"),
            resultSet.getString("COUNTRY"),
            resultSet.getString("CITY"),
            resultSet.getString("WEBPAGE"),
            null,
            null,
            null,
            resultSet.getDouble("LATITUD"),
            resultSet.getDouble("LONGITUD")
        );
    }
}
