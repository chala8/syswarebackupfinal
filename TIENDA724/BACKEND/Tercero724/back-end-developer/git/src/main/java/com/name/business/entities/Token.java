package com.name.business.entities;

/**
 * Created by luis_ on 29/01/2017.
 */
public class Token {
    private Aplicacion aplicacion;
    private UserThird userThird;
    private Long idUserApp;

    public Token(Aplicacion aplicacion, Long idUserApp) {
        this.aplicacion = aplicacion;
        this.idUserApp = idUserApp;
    }

    public Aplicacion getAplicacion() {
        return aplicacion;
    }

    public void setAplicacion(Aplicacion aplicacion) {
        this.aplicacion = aplicacion;
    }

    public UserThird getUserThird() {
        return userThird;
    }

    public void setUserThird(UserThird userThird) {
        this.userThird = userThird;
    }

    public Long getIdUserApp() {
        return idUserApp;
    }

    public void setIdUserApp(Long idUserApp) {
        this.idUserApp = idUserApp;
    }
}
