package com.name.business.representations.complex;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Directory;
import com.name.business.representations.CommonBasicInfoDTO;
import com.name.business.representations.Common_ThirdDTO;
import com.name.business.representations.DirectoryDTO;
import com.name.business.representations.PersonDTO;

/**
 * Created by trossky on 14/07/17.
 */
public class ThirdCompleteDTO {

    private Long id_third_type;
    private Long id_third_father;
    private CommonBasicInfoDTO basicInfoDTO;
    private Common_ThirdDTO common_thirdDTO;
    private PersonDTO personDTO;
    private DirectoryDTO directoryDTO;

    @JsonCreator
    public ThirdCompleteDTO( @JsonProperty("id_third_type") Long id_third_type,
                             @JsonProperty("id_third_father") Long id_third_father,
                             @JsonProperty("info") CommonBasicInfoDTO basicInfoDTO,
                             @JsonProperty("state") Common_ThirdDTO common_thirdDTO,
                             @JsonProperty("profile") PersonDTO personDTO,
                             @JsonProperty("directory") DirectoryDTO directoryDTO) {
        this.id_third_type = id_third_type;
        this.id_third_father = id_third_father;
        this.basicInfoDTO = basicInfoDTO;
        this.common_thirdDTO = common_thirdDTO;
        this.personDTO = personDTO;
        this.directoryDTO= directoryDTO;
    }

    public Long getId_third_father() {
        return id_third_father;
    }

    public void setId_third_father(Long id_third_father) {
        this.id_third_father = id_third_father;
    }

    public PersonDTO getPersonDTO() {
        return personDTO;
    }

    public void setPersonDTO(PersonDTO personDTO) {
        this.personDTO = personDTO;
    }

    public Long getId_third_type() {
        return id_third_type;
    }

    public void setId_third_type(Long id_third_type) {
        this.id_third_type = id_third_type;
    }

    public CommonBasicInfoDTO getBasicInfoDTO() {
        return basicInfoDTO;
    }

    public void setBasicInfoDTO(CommonBasicInfoDTO basicInfoDTO) {
        this.basicInfoDTO = basicInfoDTO;
    }

    public Common_ThirdDTO getCommon_thirdDTO() {
        return common_thirdDTO;
    }

    public void setCommon_thirdDTO(Common_ThirdDTO common_thirdDTO) {
        this.common_thirdDTO = common_thirdDTO;
    }

    public DirectoryDTO getDirectoryDTO() {
        return directoryDTO;
    }

    public void setDirectoryDTO(DirectoryDTO directoryDTO) {
        this.directoryDTO = directoryDTO;
    }
}
