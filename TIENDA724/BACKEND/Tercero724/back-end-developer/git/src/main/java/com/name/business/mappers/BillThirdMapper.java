package com.name.business.mappers;


import com.name.business.entities.BillThird;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillThirdMapper implements ResultSetMapper<BillThird>{

    @Override
    public BillThird map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BillThird(
                resultSet.getString("DOCUMENT_TYPE"),
                resultSet.getString("DOCUMENT_NUMBER"),
                resultSet.getString("FULLNAME"),
                resultSet.getLong("NUMVENTAS"),
                resultSet.getLong("TOTALVENTAS"),
                resultSet.getString("ADDRESS"),
                resultSet.getString("PHONE"),
                resultSet.getString("MAIL")
        );
    }
}