package com.name.business.resources;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.codahale.metrics.annotation.Timed;

import com.name.business.businesses.LogInBusiness;
import com.name.business.entities.LogInResponse;
import com.name.business.entities.RolData;
import com.name.business.representations.LogInDTO;
import com.name.business.representations.VerifyDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import at.favre.lib.*;

import java.util.*;
import java.util.stream.Collectors;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;

@Path("")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LogInResource {
    private LogInBusiness logInBusiness;

    public LogInResource(LogInBusiness logInBusiness) {
        this.logInBusiness = logInBusiness;
    }

    @Path("/login")
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response Login(@QueryParam("id_aplicacion") Long id_aplicacion, @QueryParam("dispositivo") String dispositivo, LogInDTO dto){

        Response response;


        String userEncripted = BCrypt.withDefaults().hashToString(12, dto.getUsername().toCharArray());
        String passwordEncripted = BCrypt.withDefaults().hashToString(12, dto.getPassword().toCharArray());;

        Either<IException, LogInResponse> getResponseGeneric = logInBusiness.login(dto.getUsername(), dto.getPassword(), id_aplicacion);

        if (getResponseGeneric.isRight()){
            //create SESSION
            LogInResponse l = getResponseGeneric.right().value();
            String sessionID = "";
            String UUID = l.getId_usuario();
            String subject = "";
            long ttlMillis = new Long(999999999);

            List<RolData> roles = l.getRoles();
            String rolesListString = "";

            for (RolData p:roles) {
                rolesListString=rolesListString+p.getId_rol()+",";
            }


            logInBusiness.crear_session(UUID,rolesListString.substring(0,rolesListString.length()-1),"Pendiente",id_aplicacion);

            //get SESSIONID
            sessionID = logInBusiness.getSessionId(UUID,"Pendiente",id_aplicacion);


            System.out.println(sessionID+","+UUID+","+subject+","+ttlMillis);
            //create TOKEN
            String token = logInBusiness.createJWT(sessionID,UUID,subject,ttlMillis);

            //update TOKEN on SESSION
            logInBusiness.update_token(sessionID,token);


            Cookie cookiev1 = new Cookie("SESSIONID", token);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, 1);
            NewCookie cookie = new NewCookie(cookiev1,"",99999999,calendar.getTime(),true,true);
            response=Response.status(Response.Status.OK).cookie(cookie).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }

        return response;
    }



    @Path("/verify")
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response Verify( VerifyDTO dto){

        Response response;

        String jwt = dto.getJwt();
        Either<IException, Boolean> getResponseGeneric;
        getResponseGeneric = logInBusiness.verifyWeb(jwt);
        if(getResponseGeneric.isRight()){
            response = Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else{
            getResponseGeneric = Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }

        return response;
    }



    @Path("/logout")
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response logout(VerifyDTO dto,@CookieParam("SESSIONID") String jwt){

        Response response;

        Either<IException, Long> getResponseGeneric;
        getResponseGeneric = logInBusiness.logout(jwt);
        if(getResponseGeneric.isRight()){
            response = Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else{
            getResponseGeneric = Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }

        return response;

    }



    @Path("/loginClient")
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response LoginClient(@QueryParam("id_aplicacion") Long id_aplicacion, @QueryParam("dispositivo") String dispositivo, LogInDTO dto){

        Response response;


        String userEncripted = BCrypt.withDefaults().hashToString(12, dto.getUsername().toCharArray());
        String passwordEncripted = BCrypt.withDefaults().hashToString(12, dto.getPassword().toCharArray());;

        Either<IException, LogInResponse> getResponseGeneric = logInBusiness.login(dto.getUsername(), dto.getPassword(), id_aplicacion);

        if (getResponseGeneric.isRight()){
            //create SESSION
            LogInResponse l = getResponseGeneric.right().value();
            String sessionID = "";
            String UUID = l.getId_usuario();
            String subject = "";
            long ttlMillis = new Long(999999999);

            List<RolData> roles = l.getRoles();
            String rolesListString = "";

            for (RolData p:roles) {
                rolesListString=rolesListString+p.getId_rol()+",";
            }


            logInBusiness.crear_session(UUID,rolesListString.substring(0,rolesListString.length()-1),"Pendiente",id_aplicacion);

            //get SESSIONID
            sessionID = logInBusiness.getSessionId(UUID,"Pendiente",id_aplicacion);


            System.out.println(sessionID+","+UUID+","+subject+","+ttlMillis);
            //create TOKEN
            String token = logInBusiness.createJWT(sessionID,UUID,subject,ttlMillis);

            //update TOKEN on SESSION
            logInBusiness.update_token(sessionID,token);


            Cookie cookiev1 = new Cookie("SESSIONID", token);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, 1);
            NewCookie cookie = new NewCookie(cookiev1,"",99999999,calendar.getTime(),true,true);
            response=Response.status(Response.Status.OK).cookie(cookie).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }

        return response;
    }


    @Path("/CreateClient")
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response CreateClient(@QueryParam("id_aplicacion") Long id_aplicacion, @QueryParam("dispositivo") String dispositivo, LogInDTO dto){

        Response response;


        String userEncripted = BCrypt.withDefaults().hashToString(12, dto.getUsername().toCharArray());
        String passwordEncripted = BCrypt.withDefaults().hashToString(12, dto.getPassword().toCharArray());;

        Either<IException, LogInResponse> getResponseGeneric = logInBusiness.login(dto.getUsername(), dto.getPassword(), id_aplicacion);

        if (getResponseGeneric.isRight()){
            //create SESSION
            LogInResponse l = getResponseGeneric.right().value();
            String sessionID = "";
            String UUID = l.getId_usuario();
            String subject = "";
            long ttlMillis = new Long(999999999);

            List<RolData> roles = l.getRoles();
            String rolesListString = "";

            for (RolData p:roles) {
                rolesListString=rolesListString+p.getId_rol()+",";
            }


            logInBusiness.crear_session(UUID,rolesListString.substring(0,rolesListString.length()-1),"Pendiente",id_aplicacion);

            //get SESSIONID
            sessionID = logInBusiness.getSessionId(UUID,"Pendiente",id_aplicacion);


            System.out.println(sessionID+","+UUID+","+subject+","+ttlMillis);
            //create TOKEN
            String token = logInBusiness.createJWT(sessionID,UUID,subject,ttlMillis);

            //update TOKEN on SESSION
            logInBusiness.update_token(sessionID,token);


            Cookie cookiev1 = new Cookie("SESSIONID", token);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, 1);
            NewCookie cookie = new NewCookie(cookiev1,"",99999999,calendar.getTime(),true,true);
            response=Response.status(Response.Status.OK).cookie(cookie).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }

        return response;
    }



}
