package com.name.business.mappers;


import com.name.business.entities.ThirdData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ThirdDataMapper implements ResultSetMapper<ThirdData> {

    @Override
    public ThirdData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ThirdData(
                resultSet.getLong("id_third"),
                resultSet.getString("fist_name"),
                resultSet.getString("second_name"),
                resultSet.getString("last_name"),
                resultSet.getString("second_last_name"),
                resultSet.getString("fullname"),
                resultSet.getString("document_type"),
                resultSet.getString("document"),
                resultSet.getLong("id_third_father"),
                resultSet.getLong("id_person"),
                resultSet.getString("img"),
                resultSet.getString("document_father"),
                resultSet.getString("document_type_father"),
                resultSet.getString("fullname_father")
        );
    }
}
