package com.name.business.entities;

public class ThirdData {
    private Long id_third;
    private String fist_name;
    private String second_name;
    private String last_name;
    private String second_last_name;
    private String fullname;
    private String document_type;
    private String document;
    private Long id_third_father;
    private Long id_person;
    private String img;
    private String document_father;
    private String document_type_father;
    private String fullname_father;

    public ThirdData(Long id_third, String fist_name, String second_name, String last_name, String second_last_name, String fullname, String document_type, String document, Long id_third_father, Long id_person, String img, String document_father, String document_type_father, String fullname_father) {
        this.id_third = id_third;
        this.fist_name = fist_name;
        this.second_name = second_name;
        this.last_name = last_name;
        this.second_last_name = second_last_name;
        this.fullname = fullname;
        this.document_type = document_type;
        this.document = document;
        this.id_third_father = id_third_father;
        this.id_person = id_person;
        this.img = img;
        this.document_father = document_father;
        this.document_type_father = document_type_father;
        this.fullname_father = fullname_father;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public String getFist_name() {
        return fist_name;
    }

    public void setFist_name(String fist_name) {
        this.fist_name = fist_name;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getSecond_last_name() {
        return second_last_name;
    }

    public void setSecond_last_name(String second_last_name) {
        this.second_last_name = second_last_name;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getDocument_type() {
        return document_type;
    }

    public void setDocument_type(String document_type) {
        this.document_type = document_type;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Long getId_third_father() {
        return id_third_father;
    }

    public void setId_third_father(Long id_third_father) {
        this.id_third_father = id_third_father;
    }

    public Long getId_person() {
        return id_person;
    }

    public void setId_person(Long id_person) {
        this.id_person = id_person;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDocument_father() {
        return document_father;
    }

    public void setDocument_father(String document_father) {
        this.document_father = document_father;
    }

    public String getDocument_type_father() {
        return document_type_father;
    }

    public void setDocument_type_father(String document_type_father) {
        this.document_type_father = document_type_father;
    }

    public String getFullname_father() {
        return fullname_father;
    }

    public void setFullname_father(String fullname_father) {
        this.fullname_father = fullname_father;
    }
}
