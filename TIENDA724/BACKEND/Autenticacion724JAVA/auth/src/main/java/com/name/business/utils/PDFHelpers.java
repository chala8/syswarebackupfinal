package com.name.business.utils;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PDFHelpers {
    //Valores por defecto
    private static float PaddingDefault = 2;

    //metodo para sin fondo y con padding
    public static void CellMaker(String Texto, Font font, int border, int horitontal, int vertical, PdfPTable tabla,float padding){
        CellMaker(Texto, font,border,horitontal,vertical,tabla, null,padding);
    }
    //metodo para sin fondo y sin padding
    public static void CellMaker(String Texto, Font font, int border, int horitontal, int vertical, PdfPTable tabla){
        CellMaker(Texto, font,border,horitontal,vertical,tabla, null,PaddingDefault);
    }
    //metodo para generar varias celdas vacias
    public static void EmptyCellMaker(int Cantidad, int border, int horitontal, int vertical, PdfPTable tabla, float padding) throws IOException, DocumentException {
        BaseFont HELVETICA = BaseFont.createFont(
                BaseFont.HELVETICA_BOLD,
                BaseFont.WINANSI, false);
        Font font = new Font(HELVETICA, 6, Font.NORMAL);
        for(int n = 0;n<Cantidad;n++){
            CellMaker(" ", font,border,horitontal,vertical,tabla, null,padding != -1?padding:PaddingDefault);
        }
    }
    public static void EmptyCellMaker(int Cantidad, int border, PdfPTable tabla) throws IOException, DocumentException {
        EmptyCellMaker(Cantidad,border,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tabla,PaddingDefault);
    }
    //metodo para con fondo y sin padding
    public static void CellMaker(String Texto, Font font, int border, int horitontal, int vertical, PdfPTable tabla,BaseColor fondo){
        CellMaker(Texto, font,border,horitontal,vertical,tabla, fondo,PaddingDefault);
    }
    //metodo para AGREGAR TABLAS
    public static void CellMaker(PdfPTable tablaAAgregar, int border, int horitontal, int vertical, PdfPTable tabla){
        PdfPCell celda = new PdfPCell(tablaAAgregar);
        celda.setUseAscender(true);
        celda.setBorder(border);
        celda.setHorizontalAlignment(horitontal);
        celda.setVerticalAlignment(vertical);
        celda.setBackgroundColor(null);
        tabla.addCell(celda);
    }
    //metodo completo
    public static void CellMaker(String Texto, Font font,int border,int horitontal,int vertical,PdfPTable tabla,BaseColor fondo,float padding){
        PdfPCell celda = new PdfPCell(new Phrase(Texto,font));
        celda.setUseAscender(true);
        celda.setPaddingTop(padding);
        celda.setPaddingBottom(padding);
        celda.setBorder(border);
        celda.setHorizontalAlignment(horitontal);
        celda.setVerticalAlignment(vertical);
        celda.setBackgroundColor(fondo);
        tabla.addCell(celda);
    }

    public static void PaginarPDF(String ruta) throws Exception {
        PaginarPDF(ruta,1);
    }

    public static void PaginarPDF(String ruta,int Tipo) throws Exception {
        Document documento = new Document(PageSize.A4, 0f, 0f, 0f, 0f);
        if(Tipo == 1){//Normal
            documento = new Document(PageSize.A4, 0f, 0f, 0f, 0f);
        }else if(Tipo == 2){//Cinta
            documento = new Document(new Rectangle(137, 598), 0f, 0f, 0f, 0f);
        }
        String RutaTemp = ruta.substring(0,ruta.length()-8);
        FileOutputStream FileOutputStream1 = new FileOutputStream(RutaTemp);
        PdfWriter writer = PdfWriter.getInstance(documento, FileOutputStream1);
        documento.open();
        PdfReader reader = new PdfReader(ruta);
        int n = reader.getNumberOfPages();
        PdfImportedPage page;
        // Go through all pages
        for (int i = 1; i <= n; i++) {
            page = writer.getImportedPage(reader, i);
            Image instance = Image.getInstance(page);
            documento.add(instance);
            Rectangle pageSize = documento.getPageSize();
            if(Tipo == 1){//Normal
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase(String.format("%s/%s", String.valueOf(writer.getCurrentPageNumber()), n)),
                        pageSize.getRight(20), pageSize.getBottom(30), 0);
            }else if(Tipo == 2){//Cinta
                BaseFont HELVETICA = BaseFont.createFont(
                        BaseFont.HELVETICA_BOLD,
                        BaseFont.WINANSI, false);
                Font font = new Font(HELVETICA, 6, Font.NORMAL);
                ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, new Phrase(String.format("%s/%s", String.valueOf(writer.getCurrentPageNumber()), n),font),
                        pageSize.getRight(20), pageSize.getBottom(20), 0);
            }
            documento.newPage();
        }
        documento.close();
        reader.close();
        File file = new File(ruta);
        if(file.delete()) { //System.out.println("File deleted successfully");
             }
        else { //System.out.println("Failed to delete the file");
             }
    }
}
