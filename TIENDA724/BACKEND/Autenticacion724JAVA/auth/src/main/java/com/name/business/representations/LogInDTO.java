package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LogInDTO {
    private String username;
    private String password;
    //private CommonStateDTO stateDTO;

    @JsonCreator
    public LogInDTO(@JsonProperty("usuario") String username,@JsonProperty("clave") String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
