package com.name.business.mappers;

import com.name.business.entities.MenuData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MenuDataMapper implements ResultSetMapper<MenuData> {
    @Override
    public MenuData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new MenuData(
                resultSet.getLong("id_menu"),
                resultSet.getLong("id_aplicacion"),
                resultSet.getString("nombre"),
                resultSet.getLong("tipo"),
                resultSet.getString("ruta"),
                resultSet.getString("icono"),
                resultSet.getString("avatar"),
                resultSet.getString("background"),
                resultSet.getString("estado"),
                resultSet.getString("dispositivo"),
                resultSet.getLong("id_menu_padre")
        );
    }
}
