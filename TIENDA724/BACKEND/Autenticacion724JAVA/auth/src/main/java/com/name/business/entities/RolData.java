package com.name.business.entities;

public class RolData {

    private Long id_rol;
    private Long id_aplicacion;
    private String rol;
    private String descripcion;

    public RolData(Long id_rol, Long id_aplicacion, String rol, String descripcion) {
        this.id_rol = id_rol;
        this.id_aplicacion = id_aplicacion;
        this.rol = rol;
        this.descripcion = descripcion;
    }

    public Long getId_rol() {
        return id_rol;
    }

    public void setId_rol(Long id_rol) {
        this.id_rol = id_rol;
    }

    public Long getId_aplicacion() {
        return id_aplicacion;
    }

    public void setId_aplicacion(Long id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
