package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.*;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlCall;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

public interface LogInDAO {

    @RegisterMapper(UserDataMapper.class)
    @SqlQuery("select \"clave\" " +
            "from auth2.\"usuario\" " +
            "where \"idApplicacionId\"=:appid and \"usuario\"=:user ")
    String getEncryptedPassword(@Bind("appid") Long appid,
                                @Bind("user") String user);

    @RegisterMapper(UserDataMapper.class)
    @SqlQuery("select \"id_usuario\",\"id_third\" idthird,\"idApplicacionId\" \"id_aplicacion\",\"uuid\" " +
            "from auth2.\"usuario\" " +
            "where \"idApplicacionId\"=:appid and \"usuario\"=:user ")
    UserData getUserData(@Bind("appid") Long appid,
                         @Bind("user") String user,
                         @Bind("password") String password);

    @RegisterMapper(MenuDataMapper.class)
    @SqlQuery("select m.\"id\" \"id_menu\" ,m.\"idApplicacionId\" \"id_aplicacion\",m.\"nombre\" \"nombre\",m.\"tipo\" \"tipo\",m.\"ruta\" \"ruta\", " +
            " m.\"icono\" \"icono\",m.\"avatar\" \"avatar\",m.\"background\" \"background\",m.\"estado\" \"estado\", " +
            " m.\"dispositivo\" \"dispositivo\",m.\"idPadreId\" \"id_menu_padre\" " +
            " from AUTH2.\"usuario_roles_rol\" rr, AUTH2.\"rol\" r,AUTH2.\"rol_menus_menu\" rm,AUTH2.\"menu\" m " +
            " where rr.\"rolId\"=r.\"id\" AND rr.\"rolId\"=rm.\"rolId\" AND rm.\"menuId\"=m.\"id\" " +
            " AND \"usuarioUuid\"=:uuid")
    List<MenuData> getMenuData(@Bind("uuid") String uuid);

    @RegisterMapper(RolDataMapper.class)
    @SqlQuery("select r.\"id\" \"id_rol\", r.\"idApplicacionId\" \"id_aplicacion\",r.\"Rol\" \"rol\",r.\"Descripcion\" \"descripcion\" " +
            "from AUTH2.\"usuario_roles_rol\" rr, AUTH2.\"rol\" r where rr.\"rolId\"=r.\"id\" " +
            "AND \"usuarioUuid\"=:uuid")
    List<RolData> getRolData(@Bind("uuid") String uuid);

    @RegisterMapper(ThirdDataMapper.class)
    @SqlQuery("SELECT t.id_third \"id_third\",p.FIRST_NAME \"fist_name\",p.SECOND_NAME \"second_name\",p.FIRST_LASTNAME \"last_name\"" +
            " ,p.SECOND_LASTNAME \"second_last_name\",cbi.FULLNAME \"fullname\",dt.NAME \"document_type\"" +
            " ,cbi.DOCUMENT_NUMBER \"document\",t.id_third_father \"id_third_father\",p.id_person \"id_person\",cbi.img \"img\"" +
            " ,cbif.DOCUMENT_NUMBER \"document_father\",dtf.NAME \"document_type_father\",cbif.fullname \"fullname_father\"" +
            " FROM tercero724.person p,tercero724.third t,tercero724.common_basicinfo cbi,tercero724.third tf" +
            " ,tercero724.document_type dt,TERCERO724.COMMON_BASICINFO cbif,TERCERO724.DOCUMENT_TYPE dtf" +
            " WHERE p.ID_PERSON=t.ID_PERSON and t.ID_COMMON_BASICINFO=cbi.ID_COMMON_BASICINFO" +
            " and t.id_third_father=tf.ID_THIRD and dt.id_document_type=cbi.id_document_type" +
            " and tf.ID_COMMON_BASICINFO=cbif.ID_COMMON_BASICINFO and cbif.ID_DOCUMENT_TYPE=dtf.ID_DOCUMENT_TYPE" +
            " and t.ID_THIRD=:id_third")
    ThirdData getThirdData(@Bind("id_third") Long id_third);

    @SqlCall("begin auth2.crear_session(:userID, :roles, :token, :idApplicacionId); end; ")
    void crear_session(@Bind("userID") String userID,
                       @Bind("roles") String roles,
                       @Bind("token") String token,
                       @Bind("idApplicacionId") Long idApplicacionId);


    @SqlQuery("select \"id\" from auth2.\"session\" where \"userID\"=:userID and \"idApplicacionId\"=:idApplicacionId and \"token\"=:token")
    String getSessionId(@Bind("userID") String userID,
                        @Bind("token") String token,
                        @Bind("idApplicacionId") Long idApplicacionId);

    @SqlCall("begin auth2.update_token(:idsession, :newtoken); end; ")
    void update_token(@Bind("idsession") String idsession,
                      @Bind("newtoken") String newtoken);


    @SqlQuery("select \"id\" from auth2.\"session\" where \"id\"=:idsession")
    String getSessionId(@Bind("idsession") String idsession);

    @SqlUpdate("update AUTH2.\"session\" set \"LogOut_Date\"=systimestamp where \"id\"=:sessionid")
    void logOutDate(@Bind("sessionid") String sessionid);



}
