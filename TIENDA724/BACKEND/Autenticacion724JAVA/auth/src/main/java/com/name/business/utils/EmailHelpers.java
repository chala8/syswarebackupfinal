package com.name.business.utils;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

public class EmailHelpers {
    //VARIABLES PARA CONEXIÓN CORREO
    static final String FROM = "soporte@tienda724.com";
    static final String FROMNAME = "Soporte Tienda 724";
    static final String SMTP_USERNAME = "soporte@tienda724.com";
    static final String SMTP_PASSWORD = "Lx3BPR5j8pdt2xmm";
    static final String HOST = "smtp.mail.us-east-1.awsapps.com";
    static final int PORT = 465;
    ///////////////////////////////////

    //METODOS CORREO
    public static Boolean SendEmail(String Destinatario, String Asunto, String CuerpoCorreo, java.util.List<String> ListadoArchivos) throws UnsupportedEncodingException, MessagingException {
        String[] CorreoEnFormatoLista = {Destinatario};
        return SendEmail(CorreoEnFormatoLista,Asunto,CuerpoCorreo,ListadoArchivos);
    }

    public static Boolean SendEmail(String[] Destinatario, String Asunto, String CuerpoCorreo, List<String> ListadoArchivos) throws MessagingException, UnsupportedEncodingException {
        Boolean Resultado = true;//De ser falso significaria que ocurrió algun error
        // Create a Properties object to contain connection configuration information.
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtps");
        props.put("mail.smtp.port", PORT);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");

        // Create a Session object to represent a mail session with the specified properties.
        Session session = Session.getDefaultInstance(props);

        // Create a message with the specified information.
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(FROM,FROMNAME));
        //Agrego los receptores del correo
        //System.out.println("Destinatarios:");
        for(String cc:Destinatario){
            //System.out.println(cc);
            msg.addRecipients(Message.RecipientType.CC,InternetAddress.parse(cc));
        }
        ////////////////
        msg.setSubject(Asunto);
        //
        //msg.setContent(CuerpoCorreo,"text/html");
        //
        //Cuerpo
        MimeBodyPart p1 = new MimeBodyPart();
        p1.setText(CuerpoCorreo,null,"html");

        Multipart mp = new MimeMultipart();
        mp.addBodyPart(p1);

        //file(s)
        for(int j = 0;j<ListadoArchivos.size();j++){
            //System.out.println("ListadoArchivos.get(j) es "+ListadoArchivos.get(j));
            MimeBodyPart p2 = new MimeBodyPart();
            FileDataSource fds = new FileDataSource(ListadoArchivos.get(j));
            p2.setDataHandler(new DataHandler(fds));
            p2.setFileName(fds.getName());
            //
            mp.addBodyPart(p2);
        }

        msg.setContent(mp);
        //Metodo que envia el correo, SINCRONICO
        Transport transport = session.getTransport();
        try
        {
            //System.out.println("Sending...");
            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            //System.out.println("Email sent!");
            Resultado = true;
        }
        catch (Exception ex) {
            //System.out.println("The email was not sent.");
            //System.out.println("Error message: " + ex.getMessage());
            //System.out.println("Error message toString: " + ex.toString());
            Resultado = false;
        }
        finally
        {
            // Close and terminate the connection.
            transport.close();
        }
        return Resultado;
    }
}
