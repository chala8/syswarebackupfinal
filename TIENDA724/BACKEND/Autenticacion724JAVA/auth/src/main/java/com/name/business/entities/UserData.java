package com.name.business.entities;

import java.util.Date;

public class UserData {


    private Long id_usuario;
    private Long idthird;
    private Long id_aplicacion;
    private String uuid;

    public UserData(Long id_usuario, Long idthird, Long id_aplicacion, String uuid) {
        this.id_usuario = id_usuario;
        this.idthird = idthird;
        this.id_aplicacion = id_aplicacion;
        this.uuid = uuid;
    }

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Long getIdthird() {
        return idthird;
    }

    public void setIdthird(Long idthird) {
        this.idthird = idthird;
    }

    public Long getId_aplicacion() {
        return id_aplicacion;
    }

    public void setId_aplicacion(Long id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
