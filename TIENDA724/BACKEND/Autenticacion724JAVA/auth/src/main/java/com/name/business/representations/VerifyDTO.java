package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class VerifyDTO {
    private String jwt;
    //private CommonStateDTO stateDTO;

    @JsonCreator
    public VerifyDTO(@JsonProperty("token") String jwt) {
        this.jwt = jwt;
        //this.stateDTO = stateDTO;en
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
