package com.name.business.businesses;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.name.business.DAOs.LogInDAO;
import com.name.business.entities.*;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;

public class LogInBusiness {

    private static final String SECRET_KEY = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA3Njd0pYnpLnZjmTB77xD" +
                                             "Q3wy/gaD0RmrDYKlGCysWl6dGeYUVnOwWzoIJGGzbekDRsShogvLlGtfSXAcMAf3" +
                                             "viquvpkHJcZPLiDLhrVVcQTouiEXoWu3xCMQRddnwRar9axmbhVEdyk6OKjP0Cjx" +
                                             "T5QYd6N0k+v1mUJ5iOBa/HFwkWPZqdYR/+BIKh/N7px1do1sMacBbGKLdC7uKrjj" +
                                             "yyh9dYPdfO0ltVR06aZlI57UgBayh1PXGwQzw+3+i4qdTWHi6Xa9VepHqk3FDgks" +
                                             "LmcOAD7YE4krZVYmEU2fVoBxkceVf4jcYzI3bpCAZb+z8hSEqvNMXxN5otgABXoi" +
                                             "VGkVRx8M3KpQBWS57f9rWSAPamTCcXzC6M7av2gJziawJoA52BFBjB86WMYMWAGB" +
                                             "KKcHZ+EnYYYODm4Oj1s7g1nEXBJa/A+G7wRsjUrzvtqUpMCK3aJuLh2bZjMWoWOd" +
                                             "dfV8UC4xqE+AzAGfE8xc71w5mxzcsNJzUBEfjfG4kxzMwBgz3we/fdKfFHT/C3Ht" +
                                             "2X4x8THrmnN/v3HaG52vA5+NYQyGW7iCZWKdESJUttsQDFWhzGAq2T6hjMZG3FPO" +
                                             "pjve6yK4PMj0Rqxa0pWn/qkxOmVIU1X1PxwL7EHfBfx6v01nmHfd5EATTfInNco2" +
                                             "s4Zfc1RNNOzkUTvNSglp0nECAwEAAQ==";
    private boolean PruebasLocales = false;
    private LogInDAO logInDAO;
    @SuppressWarnings("FieldCanBeLocal")
    private String URI_Terceros = System.getenv("SYSWARE_SERVER").equals("prod") ? "https://tienda724.com:8446/v1/":"https://pruebas.tienda724.com:8446/v1/";
    //private String URI_Terceros = "http://localhost:8446/v1/";

    public LogInBusiness(LogInDAO logInDAO) {
        this.logInDAO = logInDAO;
        try{
            if(System.getenv("SYSWARE_DEVPC").equals("yes")){this.PruebasLocales = true;}
        }catch (Exception e){
            this.PruebasLocales = false;
            ////System.out.println("Can't Get SYSWARE_DEVPC envr var");
        }
    }

    public Either<IException, LogInResponse> login(String username, String password, Long id_aplication) {
        try {

            //OBTENGO DATOS DE LAS CONSULTAS
            String passwordBD = logInDAO.getEncryptedPassword(id_aplication,username);
            if(!BCrypt.verifyer().verify(password.toCharArray(), passwordBD.toCharArray()).verified){return Either.left(new TechnicalException(messages_error("FALLO DE AUTENTICACION")));}


            UserData userData = logInDAO.getUserData(id_aplication,username,password);
            List<MenuData> menuList = logInDAO.getMenuData(userData.getUuid());
            List<RolData> rolList = logInDAO.getRolData(userData.getUuid());
            ThirdData thirdData = logInDAO.getThirdData(userData.getIdthird());

            UserThirdData userThirdData = getUserThirdData(thirdData);


            //LLENO DATOS PARA ESTRUCTURAS DE TERCEROS PADRES
            FatherThirdData fatherThirdData = getFatherThirdData(thirdData);

            //LLENO DATOS PARA EL LOG IN RESPONSE
            LogInResponse response = getLogInResponse(username, userData, menuList, rolList, userThirdData, fatherThirdData);

            return Either.right(response);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param thirdData
     * @return
     */
    private FatherThirdData getFatherThirdData(ThirdData thirdData) {
        FatherThirdData fatherThirdData = new FatherThirdData();

        fatherThirdData.setDocument(thirdData.getDocument_father());
        fatherThirdData.setDocument_type(thirdData.getDocument_type_father());
        fatherThirdData.setFullname(thirdData.getFullname_father());
        return fatherThirdData;
    }

    private LogInResponse getLogInResponse(String username, UserData userData, List<MenuData> menuList, List<RolData> rolList, UserThirdData userThirdData, FatherThirdData fatherThirdData) {
        LogInResponse response = new LogInResponse();

        response.setId_usuario(userData.getUuid());
        response.setId_aplicacion(userData.getId_aplicacion());
        response.setUsuario(username);
        response.setMenus(menuList);
        response.setRoles(rolList);
        response.setId_third(userData.getIdthird());
        response.setThird(userThirdData);
        response.setThird_father(fatherThirdData);
        return response;
    }

    private UserThirdData getUserThirdData(ThirdData thirdData) {
        UserThirdData userThirdData = new UserThirdData();

        userThirdData.setFist_name(thirdData.getFist_name());
        userThirdData.setSecond_name(thirdData.getSecond_name());
        userThirdData.setLast_name(thirdData.getLast_name());
        userThirdData.setSecond_last_name(thirdData.getSecond_last_name());
        userThirdData.setFullname(thirdData.getFullname());
        userThirdData.setDocument_type(thirdData.getDocument_type());
        userThirdData.setDocument(thirdData.getDocument());
        userThirdData.setId_third_father(thirdData.getId_third_father());
        userThirdData.setId_person(thirdData.getId_person());
        userThirdData.setImg(thirdData.getImg());
        return userThirdData;
    }

    public void crear_session(String userID,
                              String roles,
                              String token,
                              Long idApplicacionId){
        logInDAO.crear_session(userID,roles,token,idApplicacionId);
    }


    public static String createJWT(String id, String issuer, String subject, long ttlMillis) {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId(id)
                .setIssuedAt(now)
                .setSubject(subject)
                .setIssuer(issuer)
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
        if (ttlMillis > 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }


    public String getSessionId(String userID,
                              String token,
                              Long idApplicacionId){
        return logInDAO.getSessionId(userID,token,idApplicacionId);
    }

    public void update_token(String idsession,String newtoken){
        logInDAO.update_token(idsession,newtoken);
    }

    public Either<IException, Boolean> verifyWeb(String jwt){

        try{
            Claims decodedToken = decodeJWT(jwt);
            String id = logInDAO.getSessionId(decodedToken.getId());
            if(!(id!=null && id!="")){
                return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
            }
            return Either.right(new Boolean(true));
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    public static Claims decodeJWT(String jwt) {
        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(SECRET_KEY))
                .parseClaimsJws(jwt).getBody();
        return claims;
    }



    public Either<IException, Long> logout(String jwt){

        try{
            Claims decodedToken = decodeJWT(jwt);
            String id = logInDAO.getSessionId(decodedToken.getId());
            if(!(id!=null && id!="")){
                return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
            }
            logInDAO.logOutDate(id);
            return Either.right(new Long(1));
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }


}
