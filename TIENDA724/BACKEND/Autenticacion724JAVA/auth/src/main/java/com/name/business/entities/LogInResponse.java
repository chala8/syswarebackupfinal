package com.name.business.entities;

import java.util.List;

public class LogInResponse {

    private String id_usuario;
    private Long id_aplicacion;
    private String usuario;
    private List<MenuData> menus;
    private List<RolData> roles;
    private Long id_third;
    private UserThirdData third;
    private FatherThirdData third_father;

    public LogInResponse() {

    }

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Long getId_aplicacion() {
        return id_aplicacion;
    }

    public void setId_aplicacion(Long id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public List<MenuData> getMenus() {
        return menus;
    }

    public void setMenus(List<MenuData> menus) {
        this.menus = menus;
    }

    public List<RolData> getRoles() {
        return roles;
    }

    public void setRoles(List<RolData> roles) {
        this.roles = roles;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public UserThirdData getThird() {
        return third;
    }

    public void setThird(UserThirdData third) {
        this.third = third;
    }

    public FatherThirdData getThird_father() {
        return third_father;
    }

    public void setThird_father(FatherThirdData third_father) {
        this.third_father = third_father;
    }
}
