package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RolDataMapper  implements ResultSetMapper<RolData> {

    @Override
    public RolData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new RolData(
                resultSet.getLong("id_rol"),
                resultSet.getLong("id_aplicacion"),
                resultSet.getString("rol"),
                resultSet.getString("descripcion")
        );
    }
}
