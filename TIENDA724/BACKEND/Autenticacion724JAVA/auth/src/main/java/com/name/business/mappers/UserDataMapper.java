package com.name.business.mappers;


import com.name.business.entities.UserData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDataMapper implements ResultSetMapper<UserData> {

    @Override
    public UserData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new UserData(
                resultSet.getLong("id_usuario"),
                resultSet.getLong("idthird"),
                resultSet.getLong("id_aplicacion"),
                resultSet.getString("uuid")
        );
    }
}
