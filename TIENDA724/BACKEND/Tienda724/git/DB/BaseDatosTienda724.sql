create sequence ATTRIBUTE_DETAIL_LIST_SEQ
/

create sequence ATTRIBUTE_SEQ
/

create sequence ATTRIBUTE_LIST_SEQ
/

create sequence ATTRIBUTE_VALUE_SEQ
/

create sequence CATEGORY_SEQ
/

create sequence COMMON_SEQ
/

create sequence COMMON_STATE_SEQ
/

create sequence INVENTORY_SEQ
/

create sequence INVENTORY_DETAIL_SEQ
/

create sequence MEASURE_UNIT_SEQ
/

create sequence PRICE_LIST_SEQ
/

create sequence PRODUCT_SEQ
/

create sequence PRODUCT_THIRD_SEQ
/

create sequence TAX_TARIFF_SEQ
/

create sequence CODES_SEQ
/

create table COMMON
(
  ID_COMMON NUMBER not null
    primary key,
  NAME VARCHAR2(50),
  DESCRIPTION VARCHAR2(100)
)
/

create trigger BICOMMON
before insert
  on COMMON
for each row
  begin
    if :NEW.ID_COMMON is null then
      select "COMMON_SEQ".nextval into :NEW.ID_COMMON from dual;
    end if;
  end;
/

create table PRODUCT
(
  ID_PRODUCT NUMBER not null
    constraint PRODUCT_PK
    primary key,
  STOCK NUMBER default NULL,
  STOCK_MIN NUMBER default NULL,
  IMG_URL VARCHAR2(100) not null,
  ID_COMMON NUMBER
    constraint PRODUCT_COMMON_ID_COMMON_FK
    references COMMON,
  ID_CATEGORY NUMBER,
  CODE VARCHAR2(200),
  ID_TAX NUMBER,
  ID_COMMON_STATE NUMBER
)
/

create trigger BIPRODUCT
before insert
  on PRODUCT
for each row
  begin
    if :NEW.ID_PRODUCT is null then
      select "PRODUCT_SEQ".nextval into :NEW.ID_PRODUCT from dual;
    end if;
  end;
/

create table CATEGORY
(
  ID_CATEGORY NUMBER not null
    constraint ID_CATEGORY_PK
    primary key,
  ID_COMMON NUMBER
    constraint CATEGORY_COMMON_ID_COMMON_FK
    references COMMON,
  IMG_URL VARCHAR2(250),
  ID_CATEGORY_FATHER NUMBER
    constraint CATEGGORY_FK
    references CATEGORY,
  ID_COMMON_STATE NUMBER,
  ID_THIRD NUMBER
)
/

create trigger BICATEGORY
before insert
  on CATEGORY
for each row
  begin
    if :NEW.ID_CATEGORY is null then
      select "CATEGORY_SEQ".nextval into :NEW.ID_CATEGORY from dual;
    end if;
  end;
/

alter table PRODUCT
  add constraint PRODUCY_FK
foreign key (ID_CATEGORY) references CATEGORY
/

create table PRODUCT_THIRD
(
  ID_PRODUCT_THIRD NUMBER not null
    constraint ID_PRODUCT_PRICE_PK
    primary key,
  ID_THIRD NUMBER,
  STANDARD_PRICE FLOAT,
  MIN_PRICE FLOAT default NULL,
  ID_PRODUCT NUMBER,
  ID_MEASURE_UNIT NUMBER,
  ID_COMMON_STATE NUMBER,
  ID_ATTRIBUTE_LIST NUMBER,
  CODE VARCHAR2(500),
  ID_CATEGORY NUMBER,
  LOCATION VARCHAR2(50),
  ID_CODE NUMBER,
  TEST NUMBER
)
/

create trigger BIPRODUCT_THIRD
before insert
  on PRODUCT_THIRD
for each row
  begin
    if :NEW.ID_PRODUCT_THIRD is null then
      select "PRODUCT_THIRD_SEQ".nextval into :NEW.ID_PRODUCT_THIRD from dual;
    end if;
  end;
/

create table MEASURE_UNIT
(
  ID_MEASURE_UNIT NUMBER not null
    constraint ID_MEASURE_PK
    primary key,
  ID_COMMON NUMBER
    constraint MEASURE_COMMON_FK
    references COMMON,
  ID_COMMON_STATE NUMBER,
  ID_MEASURE_UNIT_FATHER NUMBER
    constraint MESURE_UNIT_FK
    references MEASURE_UNIT
)
/

create trigger BIMEASURE_UNIT
before insert
  on MEASURE_UNIT
for each row
  begin
    if :NEW.ID_MEASURE_UNIT is null then
      select "MEASURE_UNIT_SEQ".nextval into :NEW.ID_MEASURE_UNIT from dual;
    end if;
  end;
/

create table PRICE_LIST
(
  ID_PRICE_LIST NUMBER not null
    constraint ID_PRICE_PK
    primary key,
  ID_PRODUCT_THIRD NUMBER
    constraint PRIE_FK
    references PRODUCT_THIRD,
  ID_COMMON NUMBER
    constraint PRICE_LIST_COMMON_ID_COMMON_FK
    references COMMON,
  STANDARD_PRICE FLOAT default NULL,
  MIN_PRICE FLOAT,
  ID_COMMON_STATE NUMBER
)
/

create trigger BIPRICE_LIST
before insert
  on PRICE_LIST
for each row
  begin
    if :NEW.ID_PRICE_LIST is null then
      select "PRICE_LIST_SEQ".nextval into :NEW.ID_PRICE_LIST from dual;
    end if;
  end;
/

create table ATTRIBUTE
(
  ID_ATTRIBUTE NUMBER not null
    constraint ID_ATTRIBUTE_TYPE_PK
    primary key,
  ID_COMMON NUMBER
    constraint ATTRIBUTE_TYPE_COMMTRIBUTE_FK
    references COMMON,
  ID_COMMON_STATE NUMBER
)
/

create trigger BIATTRIBUTE
before insert
  on ATTRIBUTE
for each row
  begin
    if :NEW.ID_ATTRIBUTE is null then
      select "ATTRIBUTE_SEQ".nextval into :NEW.ID_ATTRIBUTE from dual;
    end if;
  end;
/

create table ATTRIBUTE_VALUE
(
  ID_ATTRIBUTE_VALUE NUMBER not null
    constraint ID_ATTRIBUTE_VALUE_PK
    primary key,
  ID_ATTRIBUTE NUMBER
    constraint ATTRIBUTEE_FK
    references ATTRIBUTE,
  ID_COMMON NUMBER
    constraint ATTVAL_ATTCOMM_FK
    references COMMON,
  ID_COMMON_STATE NUMBER
)
/

create trigger BIATTRIBUTE_VALUE
before insert
  on ATTRIBUTE_VALUE
for each row
  begin
    if :NEW.ID_ATTRIBUTE_VALUE is null then
      select "ATTRIBUTE_VALUE_SEQ".nextval into :NEW.ID_ATTRIBUTE_VALUE from dual;
    end if;
  end;
/

create table INVENTORY
(
  ID_INVENTORY NUMBER not null
    primary key,
  ID_COMMON NUMBER
    constraint INVENTORY_COMMON_ID_COMMON_FK
    references COMMON,
  ID_COMMON_STATE NUMBER,
  ID_THIRD NUMBER
)
/

create trigger BIINVENTORY
before insert
  on INVENTORY
for each row
  begin
    if :NEW.ID_INVENTORY is null then
      select "INVENTORY_SEQ".nextval into :NEW.ID_INVENTORY from dual;
    end if;
  end;
/

create table INVENTORY_DETAIL
(
  ID_INVENTORY_DETAIL NUMBER not null
    primary key,
  ID_INVENTORY NUMBER
    constraint INVETORY_FK
    references INVENTORY,
  ID_PRODUCT_THIRD NUMBER
    constraint INVENTUCT_PRIICE_FK
    references PRODUCT_THIRD,
  QUANTITY NUMBER,
  ID_COMMON_STATE NUMBER,
  CODE VARCHAR2(500),
  ID_CODE NUMBER,
  STOCK_MIN NUMBER
)
/

create trigger BIINVENTORY_DETAIL
before insert
  on INVENTORY_DETAIL
for each row
  begin
    if :NEW.ID_INVENTORY_DETAIL is null then
      select "INVENTORY_DETAIL_SEQ".nextval into :NEW.ID_INVENTORY_DETAIL from dual;
    end if;
  end;
/

create table COMMON_STATE
(
  ID_COMMON_STATE NUMBER not null
    primary key,
  STATE NUMBER,
  CREATION_DATE DATE,
  MODIFY_DATE DATE
)
/

create trigger BICOMMON_STATE
before insert
  on COMMON_STATE
for each row
  begin
    if :NEW.ID_COMMON_STATE is null then
      select "COMMON_STATE_SEQ".nextval into :NEW.ID_COMMON_STATE from dual;
    end if;
  end;
/

alter table PRODUCT
  add constraint PRODUCT_COMMONATE_FK
foreign key (ID_COMMON_STATE) references COMMON_STATE
/

alter table CATEGORY
  add constraint CATEGORY_COMMONK
foreign key (ID_COMMON_STATE) references COMMON_STATE
/

alter table PRODUCT_THIRD
  add constraint PRODUCT_PRICE_N_STATE_FK
foreign key (ID_COMMON_STATE) references COMMON_STATE
/

alter table MEASURE_UNIT
  add constraint MEASUREAS_FK
foreign key (ID_COMMON_STATE) references COMMON_STATE
/

alter table PRICE_LIST
  add constraint PRICE_LIS_STATE_FK
foreign key (ID_COMMON_STATE) references COMMON_STATE
/

alter table ATTRIBUTE
  add constraint ATTRIBUTE_TYPE_MON_STATE_FK
foreign key (ID_COMMON_STATE) references COMMON_STATE
/

alter table ATTRIBUTE_VALUE
  add constraint ATTRIBUTE_VALUE_COMMONTE_FK
foreign key (ID_COMMON_STATE) references COMMON_STATE
/

alter table INVENTORY
  add constraint INVENTORY_CSTATE_FK
foreign key (ID_COMMON_STATE) references COMMON_STATE
/

alter table INVENTORY_DETAIL
  add constraint INVENT_ID_COMMON_STATE_FK
foreign key (ID_COMMON_STATE) references COMMON_STATE
/

create table ATTRIBUTE_LIST
(
  ID_ATTRIBUTE_LIST NUMBER not null
    primary key,
  ID_COMMON_STATE NUMBER
    constraint ATTRIBUT_STATE_FK
    references COMMON_STATE
)
/

create trigger BIATTRIBUTE_LIST
before insert
  on ATTRIBUTE_LIST
for each row
  begin
    if :NEW.ID_ATTRIBUTE_LIST is null then
      select "ATTRIBUTE_LIST_SEQ".nextval into :NEW.ID_ATTRIBUTE_LIST from dual;
    end if;
  end;
/

create table ATTRIBUTE_DETAIL_LIST
(
  ID_ATTRIBUTE_DETAIL_LIST NUMBER not null
    primary key,
  ID_ATTRIBUTE_LIST NUMBER
    constraint ATTRIBUTE_DETARIBUTE_LIST_FK
    references ATTRIBUTE_LIST,
  ID_ATTRIBUTE_VALUE NUMBER
    constraint ATTRIBUTTE_VALUE_FK
    references ATTRIBUTE_VALUE,
  ID_COMMON_STATE NUMBER
    constraint ATTRIBE_FK
    references COMMON_STATE
)
/

create trigger BIATTRIBUTE_DETAIL_LIST
before insert
  on ATTRIBUTE_DETAIL_LIST
for each row
  begin
    if :NEW.ID_ATTRIBUTE_DETAIL_LIST is null then
      select "ATTRIBUTE_DETAIL_LIST_SEQ".nextval into :NEW.ID_ATTRIBUTE_DETAIL_LIST from dual;
    end if;
  end;
/

create table TAX_TARIFF
(
  ID_TAX_TARIFF NUMBER not null
    constraint ID_TAX_TARIFF_PK
    primary key,
  ID_COMMON NUMBER default NULL
    constraint TAX_TARIFF_COMMON_ID_COMMON_FK
    references COMMON,
  ID_COMMON_STATE NUMBER default NULL
    constraint TAX_TARIFF_COMMON_ID_ON_FK
    references COMMON_STATE,
  PERCENT FLOAT
)
/

create trigger BITAX_TARIFF
before insert
  on TAX_TARIFF
for each row
  begin
    if :NEW.ID_TAX_TARIFF is null then
      select "TAX_TARIFF_SEQ".nextval into :NEW.ID_TAX_TARIFF from dual;
    end if;
  end;
/

create table CODES
(
  ID_CODE NUMBER not null
    primary key,
  CODE VARCHAR2(600),
  IMG VARCHAR2(600),
  ID_PRODUCT NUMBER,
  ID_MEASURE_UNIT NUMBER,
  ID_ATTRIBUTE_LIST NUMBER,
  ID_COMMON_STATE NUMBER,
  ID_THIRD NUMBER,
  SUGGESTED_PRICE NUMBER
)
/

create unique index CODES_CODE_UINDEX
  on CODES (CODE)
/

create trigger BICODES
before insert
  on CODES
for each row
  begin
    if :NEW.ID_CODE is null then
      select "CODES_SEQ".nextval into :NEW.ID_CODE from dual;
    end if;
  end;
/

alter table PRODUCT_THIRD
  add constraint PRODUCT_THIDE_FK
foreign key (ID_CODE) references CODES
/

alter table INVENTORY_DETAIL
  add constraint INVENTDE_FK
foreign key (ID_CODE) references CODES
/

CREATE VIEW V_ATTRIBUTE AS SELECT ATT.ID_ATTRIBUTE, CO.ID_COMMON, CO.NAME,CO.DESCRIPTION,
                             CO_ST.ID_COMMON_STATE ID_STATE, CO_ST.STATE,CO_ST.CREATION_DATE CREATION_ATTRIBUTE,CO_ST.MODIFY_DATE MODIFY_ATTRIBUTE
                           FROM ATTRIBUTE ATT
                             INNER JOIN COMMON CO ON ATT.ID_COMMON = CO.ID_COMMON
                             INNER JOIN COMMON_STATE CO_ST ON ATT.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
/

CREATE VIEW V_ATTRIBUTE_VALUE AS SELECT ATT_VAL.ID_ATTRIBUTE_VALUE,ATT_VAL.ID_ATTRIBUTE,
                                   CO.ID_COMMON, CO.NAME,CO.DESCRIPTION,
                                   CO_ST.ID_COMMON_STATE ID_STATE, CO_ST.STATE,CO_ST.CREATION_DATE CREATION_ATTRIBUTE_VALUE,CO_ST.MODIFY_DATE MODIFY_ATTRIBUTE_VALUE
                                 FROM ATTRIBUTE_VALUE ATT_VAL
                                   INNER JOIN COMMON CO ON ATT_VAL.ID_COMMON = CO.ID_COMMON
                                   INNER JOIN COMMON_STATE CO_ST ON ATT_VAL.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
/

CREATE VIEW V_TAX_TARIFF AS SELECT  TAX.ID_TAX_TARIFF,TAX.PERCENT,
                              CO.ID_COMMON,CO.NAME,CO.DESCRIPTION,
                              CO_ST.ID_COMMON_STATE ID_STATE, CO_ST.STATE,CO_ST.CREATION_DATE CREATION_TAX,CO_ST.MODIFY_DATE MODIFY_TAX
                            FROM TAX_TARIFF TAX
                              INNER JOIN COMMON CO ON TAX.ID_COMMON = CO.ID_COMMON
                              INNER JOIN COMMON_STATE CO_ST ON TAX.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
/

CREATE VIEW V_INVENTORY AS SELECT INV.ID_INVENTORY, INV.ID_THIRD,
                             CO.ID_COMMON,CO.NAME,CO.DESCRIPTION,
                             CO_ST.ID_COMMON_STATE ID_STATE, CO_ST.STATE,CO_ST.CREATION_DATE CREATION_INVENTORY,CO_ST.MODIFY_DATE MODIFY_INVENTORY

                           FROM INVENTORY INV
                             INNER JOIN COMMON CO ON INV.ID_COMMON = CO.ID_COMMON
                             INNER JOIN  COMMON_STATE CO_ST ON INV.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
/

CREATE VIEW V_PRODUCT AS SELECT PRO.ID_PRODUCT,PRO.ID_CATEGORY, PRO.STOCK,PRO.STOCK_MIN,PRO.IMG_URL, PRO.CODE,PRO.ID_TAX,
                           CO.ID_COMMON,CO.NAME,CO.DESCRIPTION,
                           CO_ST.ID_COMMON_STATE ID_STATE, CO_ST.STATE,CO_ST.CREATION_DATE CREATION_PRODUCT,CO_ST.MODIFY_DATE MODIFY_PRODUCT
                         FROM PRODUCT PRO
                           INNER JOIN  COMMON CO ON PRO.ID_COMMON = CO.ID_COMMON
                           INNER JOIN COMMON_STATE CO_ST ON PRO.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
/

CREATE VIEW V_MEASURE_UNIT AS SELECT ME_UNIT.ID_MEASURE_UNIT,ME_UNIT.ID_MEASURE_UNIT_FATHER,
                                CO.ID_COMMON,CO.NAME,CO.DESCRIPTION,
                                CO_ST.ID_COMMON_STATE ID_STATE, CO_ST.STATE,CO_ST.CREATION_DATE CREATION_MEASURE_UNIT,CO_ST.MODIFY_DATE MODIFY_MEASURE_UNIT

                              FROM MEASURE_UNIT ME_UNIT
                                INNER JOIN COMMON CO ON ME_UNIT.ID_COMMON = CO.ID_COMMON
                                INNER JOIN COMMON_STATE CO_ST ON ME_UNIT.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
/

CREATE VIEW V_ATTRIBUTE_LIST AS SELECT ATT_L.ID_ATTRIBUTE_LIST,
                                  CO_ST.ID_COMMON_STATE ID_STATE, CO_ST.STATE,CO_ST.CREATION_DATE CREATION_ATTRIBUTE_LIST,CO_ST.MODIFY_DATE MODIFY_ATTRIBUTE_LIST

                                FROM ATTRIBUTE_LIST ATT_L
                                  INNER JOIN COMMON_STATE CO_ST ON ATT_L.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
/

CREATE VIEW V_ATTRIBUTE_DETAIL_LIST AS SELECT ATT_L_DET.ID_ATTRIBUTE_DETAIL_LIST, ATT_L_DET.ID_ATTRIBUTE_LIST,
                                         CO_ST.ID_COMMON_STATE ID_STATE_ATT_L_DET, CO_ST.STATE STATE_ATT_L_DET,CO_ST.CREATION_DATE CREATION_ATTRIBUTE_DETAIL_LIST,CO_ST.MODIFY_DATE MODIFY_ATTRIBUTE_DETAIL_LIST,
                                         V_ATT_VAL."ID_ATTRIBUTE_VALUE",V_ATT_VAL."ID_ATTRIBUTE",V_ATT_VAL."ID_COMMON",V_ATT_VAL."NAME",V_ATT_VAL."DESCRIPTION",V_ATT_VAL."ID_STATE",V_ATT_VAL."STATE",V_ATT_VAL."CREATION_ATTRIBUTE_VALUE",V_ATT_VAL."MODIFY_ATTRIBUTE_VALUE"
                                       FROM ATTRIBUTE_DETAIL_LIST ATT_L_DET
                                         INNER JOIN V_ATTRIBUTE_VALUE V_ATT_VAL ON V_ATT_VAL.ID_ATTRIBUTE_VALUE=ATT_L_DET.ID_ATTRIBUTE_VALUE
                                         INNER JOIN COMMON_STATE CO_ST ON ATT_L_DET.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
/

CREATE VIEW V_PRICE_LIST AS SELECT  PRO_L.ID_PRICE_LIST,PRO_L.ID_PRODUCT_THIRD,PRO_L.STANDARD_PRICE,PRO_L.MIN_PRICE,
                              CO.ID_COMMON,CO.NAME,CO.DESCRIPTION,
                              CO_ST.ID_COMMON_STATE ID_STATE, CO_ST.STATE,CO_ST.CREATION_DATE CREATION_PRICE_LIST,CO_ST.MODIFY_DATE MODIFY_PRICE_LIST
                            FROM PRICE_LIST PRO_L
                              INNER JOIN  COMMON CO ON PRO_L.ID_COMMON = CO.ID_COMMON
                              INNER JOIN  COMMON_STATE CO_ST ON PRO_L.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
/

CREATE VIEW V_CATEGORY AS SELECT  CAT.ID_CATEGORY,CAT.ID_CATEGORY_FATHER, CAT.IMG_URL,CAT.ID_THIRD ID_THIRD_CATEGORY,
                            CO.ID_COMMON,CO.NAME,CO.DESCRIPTION,
                                                                                      CO_ST.ID_COMMON_STATE ID_STATE, CO_ST.STATE,CO_ST.CREATION_DATE CREATION_CATEGORY,CO_ST.MODIFY_DATE MODIFY_CATEGORY
                          FROM CATEGORY CAT
                            INNER JOIN COMMON CO ON CAT.ID_COMMON = CO.ID_COMMON
                            INNER JOIN COMMON_STATE CO_ST ON CAT.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
/

CREATE VIEW V_CODE AS SELECT  COD.ID_CODE, COD.CODE, COD.IMG IMG_COD,
                        COD.SUGGESTED_PRICE,
                                                     COD.ID_THIRD ID_THIRD_COD,
                                                     COD.ID_PRODUCT ID_PRODUCT_COD, COD.ID_MEASURE_UNIT ID_MEASURE_UNIT_COD, COD.ID_ATTRIBUTE_LIST ID_ATTRIBUTE_LIST_COD,
                                                     CO_ST.ID_COMMON_STATE ID_STATE_CODE, CO_ST.STATE STATE_CODE, CO_ST.CREATION_DATE CREATION_CODE, CO_ST.MODIFY_DATE MODIFY_CODE
                      FROM CODES COD
                        INNER JOIN COMMON_STATE CO_ST ON COD.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
/

CREATE VIEW V_INVENTORY_DETAIL AS SELECT INV_DET.ID_INVENTORY_DETAIL, INV_DET.ID_INVENTORY,
                                    INV_DET.ID_PRODUCT_THIRD, INV_DET.QUANTITY,INV_DET.STOCK_MIN STOCK_MIN_INV,
                                    COD.CODE,
                                                                               CO_ST.ID_COMMON_STATE ID_STATE,
                                    CO_ST.STATE,CO_ST.CREATION_DATE CREATION_INVENTORY_DETAIL,
                                                                               CO_ST.MODIFY_DATE MODIFY_INVENTORY_DETAIL
                                  FROM INVENTORY_DETAIL INV_DET
                                    INNER JOIN COMMON_STATE CO_ST ON INV_DET.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
                                    LEFT JOIN PRODUCT_THIRD PR_TH ON INV_DET.ID_PRODUCT_THIRD = PR_TH.ID_PRODUCT_THIRD
                                    LEFT JOIN CODES COD ON PR_TH.ID_CODE = COD.ID_CODE
/

CREATE VIEW V_PRODUCT_THIRD AS SELECT  PRO_TH.ID_PRODUCT_THIRD, PRO_TH.ID_THIRD,COD.ID_ATTRIBUTE_LIST_COD ID_ATTRIBUTE_LIST,PRO_TH.STANDARD_PRICE,
                                 PRO_TH.MIN_PRICE,PRO_TH.ID_CATEGORY ID_CATEGORY_PR_TH,PRO_TH.LOCATION LOCATION_PR_TH,PRO_TH.ID_CODE,

                                                                                CO_ST.ID_COMMON_STATE ID_STATE, CO_ST.STATE,CO_ST.CREATION_DATE CREATION_PRODUCT_THIRD,CO_ST.MODIFY_DATE MODIFY_PRODUCT_THIRD,

                                 V_ME_UNT.ID_MEASURE_UNIT,V_ME_UNT.ID_MEASURE_UNIT_FATHER, V_ME_UNT.ID_COMMON ID_COMMON_UNT, V_ME_UNT.NAME NAME_UNT,
                                 V_ME_UNT.DESCRIPTION DESCRIPTION_UNT,
                                 V_ME_UNT.ID_STATE ID_STATE_UNT, V_ME_UNT.STATE STATE_UNT, V_ME_UNT.CREATION_MEASURE_UNIT,V_ME_UNT.MODIFY_MEASURE_UNIT,

                                 COD.CODE,COD.SUGGESTED_PRICE,COD.ID_THIRD_COD,COD.ID_PRODUCT_COD ID_PRODUCT

                               FROM PRODUCT_THIRD PRO_TH
                                 LEFT JOIN   V_CODE  COD ON  PRO_TH.ID_CODE = COD.ID_CODE
                                 LEFT JOIN V_MEASURE_UNIT V_ME_UNT ON V_ME_UNT.ID_MEASURE_UNIT = COD.ID_MEASURE_UNIT_COD
                                 INNER JOIN COMMON_STATE CO_ST ON  PRO_TH.ID_COMMON_STATE = CO_ST.ID_COMMON_STATE
/

CREATE VIEW V_PRODUCT_THIRD_COMPLETE AS SELECT
V_PRO_TH."ID_PRODUCT_THIRD",V_PRO_TH."ID_THIRD",V_PRO_TH.ID_PRODUCT,
V_PRO_TH."ID_ATTRIBUTE_LIST",V_PRO_TH."STANDARD_PRICE",V_PRO_TH."MIN_PRICE",V_
PRO_TH."ID_STATE",V_PRO_TH."STATE",                                           
V_PRO_TH."CREATION_PRODUCT_THIRD",V_PRO_TH."MODIFY_PRODUCT_THIRD",V_PRO_TH."ID
_MEASURE_UNIT",                                           V_PRO_TH."ID_MEASURE
_UNIT_FATHER",V_PRO_TH."ID_COMMON_UNT",V_PRO_TH."NAME_UNT",V_PRO_TH."DESCRIPTI
ON_UNT",V_PRO_TH."ID_STATE_UNT",V_PRO_TH."STATE_UNT",V_PRO_TH."CREATION_MEASUR
E_UNIT",V_PRO_TH."MODIFY_MEASURE_UNIT",V_PRO.ID_CATEGORY,V_PRO.STOCK,V_PRO.STO
CK_MIN,V_PRO.IMG_URL,V_PRO.CODE CODE_PRO,
V_PRO_TH.ID_CATEGORY_PR_TH,V_PRO_TH.LOCATION_PR_TH,
V_PRO.ID_TAX,V_PRO.ID_COMMON
ID_COMMON_PRO,V_PRO.NAME,V_PRO.DESCRIPTION,V_PRO.ID_STATE ID_STATE_PRO,
V_PRO.STATE STATE_PRO,V_PRO.CREATION_PRODUCT,V_PRO.MODIFY_PRODUCT,

                                          V_COD."ID_CODE",V_COD."CODE",V_COD."IMG_COD",V_COD.SUGGESTED_PRICE,V_COD.ID_THIRD_COD,V_COD."ID_PRODUCT_COD",V_COD."ID_MEASURE_UNIT_COD",V_COD."ID_ATTRIBUTE_LIST_COD",V_COD."ID_STATE_CODE",V_COD."STATE_CODE",V_COD."CREATION_CODE",V_COD."MODIFY_CODE"  FROM V_PRODUCT_THIRD V_PRO_TH
  LEFT JOIN V_CODE V_COD ON V_PRO_TH.ID_CODE=V_COD.ID_CODE
  LEFT JOIN V_PRODUCT V_PRO ON V_COD.ID_PRODUCT_COD=V_PRO.ID_PRODUCT
/

CREATE VIEW V_INVENTORY_DET_COMPLETE AS SELECT
                                          V_INV_DET.ID_INVENTORY_DETAIL,V_INV_DET.ID_INVENTORY,
                                          V_INV_DET.QUANTITY,V_INV_DET.STOCK_MIN_INV,V_INV_DET.CODE CODE_INV,
                                                                                     V_INV_DET.ID_STATE ID_STATE_INV, V_INV_DET.STATE STATE_INV,
                                          V_INV_DET.CREATION_INVENTORY_DETAIL,V_INV_DET.MODIFY_INVENTORY_DETAIL,

                                          V_PRO_TH_COMP.ID_PRODUCT_THIRD,V_PRO_TH_COMP.ID_THIRD,
                                          V_PRO_TH_COMP.STANDARD_PRICE,V_PRO_TH_COMP.MIN_PRICE,
                                          V_PRO_TH_COMP.ID_CODE,
                                          V_PRO_TH_COMP.ID_CATEGORY_PR_TH,
                                          V_PRO_TH_COMP.LOCATION_PR_TH,
                                          V_PRO_TH_COMP.ID_STATE,V_PRO_TH_COMP.STATE,
                                          V_PRO_TH_COMP.CREATION_PRODUCT_THIRD,V_PRO_TH_COMP.MODIFY_PRODUCT_THIRD,

                                          V_PRO_TH_COMP.ID_MEASURE_UNIT_COD,V_PRO_TH_COMP.ID_MEASURE_UNIT_FATHER,
                                          V_PRO_TH_COMP.ID_COMMON_UNT,V_PRO_TH_COMP.NAME_UNT,
                                          V_PRO_TH_COMP.DESCRIPTION_UNT,V_PRO_TH_COMP.ID_STATE_UNT,
                                          V_PRO_TH_COMP.STATE_UNT,V_PRO_TH_COMP.CREATION_MEASURE_UNIT,
                                          V_PRO_TH_COMP.MODIFY_MEASURE_UNIT,
                                          V_PRO_TH_COMP.ID_ATTRIBUTE_LIST_COD,
                                          V_PRO_TH_COMP.ID_PRODUCT,

                                          V_PRO_TH_COMP.ID_CATEGORY,
                                          V_PRO_TH_COMP.STOCK,V_PRO_TH_COMP.STOCK_MIN,
                                          V_PRO_TH_COMP.IMG_URL,
                                          V_PRO_TH_COMP.CODE,
                                          V_PRO_TH_COMP.ID_TAX,
                                          V_PRO_TH_COMP.ID_COMMON_PRO,
                                          V_PRO_TH_COMP.NAME,V_PRO_TH_COMP.DESCRIPTION,
                                          V_PRO_TH_COMP.ID_STATE_PRO,
                                          V_PRO_TH_COMP.STATE_PRO,
                                          V_PRO_TH_COMP.CREATION_PRODUCT,
                                          V_PRO_TH_COMP.MODIFY_PRODUCT,
                                          V_PRO_TH_COMP.IMG_COD,
                                          V_PRO_TH_COMP.SUGGESTED_PRICE,
                                          V_PRO_TH_COMP.ID_THIRD_COD,
                                          V_PRO_TH_COMP.ID_STATE_CODE,
                                          V_PRO_TH_COMP.STATE_CODE,V_PRO_TH_COMP.CREATION_CODE,V_PRO_TH_COMP.MODIFY_CODE
                                        FROM V_INVENTORY_DETAIL V_INV_DET
                                          INNER JOIN V_PRODUCT_THIRD_COMPLETE V_PRO_TH_COMP ON V_INV_DET.ID_PRODUCT_THIRD=V_PRO_TH_COMP.ID_PRODUCT_THIRD
/

CREATE VIEW V_CODE_COMPLTE AS SELECT  V_COD.ID_CODE, V_COD.CODE CODE_FINAL, V_COD.IMG_COD,
                                V_COD.SUGGESTED_PRICE,
                                V_COD.ID_THIRD_COD,


                                V_COD.ID_ATTRIBUTE_LIST_COD,
                                V_COD.ID_STATE_CODE,
                                V_COD.STATE_CODE,
                                V_COD.CREATION_CODE,
                                V_COD.MODIFY_CODE,
                                V_MEAS_UNT.ID_MEASURE_UNIT,
                                V_MEAS_UNT.ID_MEASURE_UNIT_FATHER,
                                                     V_MEAS_UNT.ID_COMMON ID_COMMON_MEAS_UNT, V_MEAS_UNT.NAME NAME_MEAS_UNT, V_MEAS_UNT.DESCRIPTION DESCRIPTION_MEAS_UNT,
                                                     V_MEAS_UNT.ID_STATE ID_STATE_MEAS_UNT, V_MEAS_UNT.STATE STATE_MEAS_UNT, V_MEAS_UNT.CREATION_MEASURE_UNIT,V_MEAS_UNT.MODIFY_MEASURE_UNIT,
                                V_PROD."ID_PRODUCT",V_PROD."ID_CATEGORY",V_PROD."STOCK",V_PROD."STOCK_MIN",V_PROD."IMG_URL",V_PROD."CODE",V_PROD."ID_TAX",V_PROD."ID_COMMON",V_PROD."NAME",V_PROD."DESCRIPTION",V_PROD."ID_STATE",V_PROD."STATE",V_PROD."CREATION_PRODUCT",V_PROD."MODIFY_PRODUCT"
                              FROM V_CODE V_COD
                                INNER JOIN V_MEASURE_UNIT V_MEAS_UNT ON V_COD.ID_MEASURE_UNIT_COD = V_MEAS_UNT.ID_MEASURE_UNIT
                                INNER JOIN V_PRODUCT V_PROD ON V_COD.ID_PRODUCT_COD=V_PROD.ID_PRODUCT
/

