package com.name.business.entities;

public class Store {

    private Long ID_STORE;
    private String STORE_NAME;
    private Long ID_DIRECTORY;
    private Long STORE_NUMBER;
    private Long ENTRADA_CONSECUTIVE_INITIAL;
    private Long SALIDA_CONSECUTIVE_INITIAL;



    public Store(Long ID_STORE, String STORE_NAME,Long ID_DIRECTORY,
            Long STORE_NUMBER, Long ENTRADA_CONSECUTIVE_INITIAL, Long SALIDA_CONSECUTIVE_INITIAL) {
        this.ID_STORE = ID_STORE;
        this.STORE_NAME = STORE_NAME;
        this.ID_DIRECTORY = ID_DIRECTORY;
        this.STORE_NUMBER = STORE_NUMBER;
        this.ENTRADA_CONSECUTIVE_INITIAL = ENTRADA_CONSECUTIVE_INITIAL;
        this.SALIDA_CONSECUTIVE_INITIAL = SALIDA_CONSECUTIVE_INITIAL;
    }

    //-------------------------------------------------------------------------------------------

    public Long getID_STORE() {
        return ID_STORE;
    }

    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    //-------------------------------------------------------------------------------------------

    public String getSTORE_NAME() {
        return STORE_NAME;
    }

    public void setSTORE_NAME(String STORE_NAME) {
        this.STORE_NAME = STORE_NAME;
    }

    //-------------------------------------------------------------------------------------------

    public Long getID_DIRECTORY() {
        return ID_DIRECTORY;
    }

    public void setID_DIRECTORY(Long ID_DIRECTORY) {
        this.ID_DIRECTORY = ID_DIRECTORY;
    }

    //-------------------------------------------------------------------------------------------

    public Long getSTORE_NUMBER() {
        return STORE_NUMBER;
    }

    public void setSTORE_NUMBER(Long STORE_NUMBER) {
        this.STORE_NUMBER = STORE_NUMBER;
    }

    //-------------------------------------------------------------------------------------------

    public Long getENTRADA_CONSECUTIVE_INITIAL() {
        return ENTRADA_CONSECUTIVE_INITIAL;
    }

    public void setENTRADA_CONSECUTIVE_INITIAL(Long ENTRADA_CONSECUTIVE_INITIAL) {
        this.ENTRADA_CONSECUTIVE_INITIAL = ENTRADA_CONSECUTIVE_INITIAL;
    }

    //-------------------------------------------------------------------------------------------

    public Long getSALIDA_CONSECUTIVE_INITIAL() {
        return SALIDA_CONSECUTIVE_INITIAL;
    }

    public void setSALIDA_CONSECUTIVE_INITIAL(Long SALIDA_CONSECUTIVE_INITIAL) {
        this.SALIDA_CONSECUTIVE_INITIAL = SALIDA_CONSECUTIVE_INITIAL;
    }

    //-------------------------------------------------------------------------------------------





}
