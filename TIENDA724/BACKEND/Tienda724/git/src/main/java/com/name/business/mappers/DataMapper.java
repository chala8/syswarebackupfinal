package com.name.business.mappers;


import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DataMapper implements ResultSetMapper<Data> {

    @Override
    public Data map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Data(
                resultSet.getString("nombretienda"),
                resultSet.getString("nombrepropietario"),
                resultSet.getString("url_logo"),
                resultSet.getString("ciudad"),
                resultSet.getString("direccion"),
                resultSet.getString("telefono"),
                resultSet.getString("email"),
                resultSet.getString("latitud"),
                resultSet.getString("longitud"),
                resultSet.getString("id_country")
        );
    }
}
