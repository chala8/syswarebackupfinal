package com.name.business.entities;

public class MUName {

    private Long ID_MEASURE_UNIT;
    private String MEASURE_UNIT;
    private String DESCRIPTION;



    public MUName(Long ID_MEASURE_UNIT, String MEASURE_UNIT, String DESCRIPTION) {
        this.ID_MEASURE_UNIT = ID_MEASURE_UNIT;
        this.MEASURE_UNIT = MEASURE_UNIT;
        this.DESCRIPTION =  DESCRIPTION;
    }
    //--------------------------------------------------------------------------

    public Long getID_MEASURE_UNIT() { return ID_MEASURE_UNIT; }

    public void setID_MEASURE_UNIT(Long ID_MEASURE_UNIT) { this.ID_MEASURE_UNIT = ID_MEASURE_UNIT; }

    //--------------------------------------------------------------------------

    public String getMEASURE_UNIT() { return MEASURE_UNIT; }

    public void setMEASURE_UNIT(String MEASURE_UNIT) { this.MEASURE_UNIT = MEASURE_UNIT; }

    //--------------------------------------------------------------------------

    public String getDESCRIPTION() { return DESCRIPTION; }

    public void setDESCRIPTION(String DESCRIPTION) { this.DESCRIPTION = DESCRIPTION; }

    //--------------------------------------------------------------------------



}
