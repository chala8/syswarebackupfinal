package com.name.business.entities;

public class Pedido {

    private Long ID_BILL;
    private String CLIENTE;
    private String TIENDA;
    private String NUMPEDIDO;
    private String FECHA;



    public Pedido(Long ID_BILL,
                  String CLIENTE,
                  String TIENDA,
                  String NUMPEDIDO,
                  String FECHA) {
        this.ID_BILL = ID_BILL;
        this.CLIENTE = CLIENTE;
        this.TIENDA =  TIENDA;
        this.NUMPEDIDO = NUMPEDIDO;
        this.FECHA = FECHA;
    }
    //----------------------------------------------------------------------------
    public Long getID_BILL() {
        return ID_BILL;
    }
    public void setID_BILL(Long ID_BILL) {
        this.ID_BILL = ID_BILL;
    }
    //----------------------------------------------------------------------------
    public String getCLIENTE() {
        return CLIENTE;
    }
    public void setCLIENTE(String CLIENTE) {
        this.CLIENTE = CLIENTE;
    }
    //----------------------------------------------------------------------------
    public String getTIENDA() {
        return TIENDA;
    }
    public void setTIENDA(String TIENDA) {
        this.TIENDA = TIENDA;
    }
    //----------------------------------------------------------------------------
    public String getNUMPEDIDO() {
        return NUMPEDIDO;
    }
    public void setNUMPEDIDO(String NUMPEDIDO) {
        this.NUMPEDIDO = NUMPEDIDO;
    }
    //----------------------------------------------------------------------------
    public String getFECHA() {
        return FECHA;
    }
    public void setFECHA(String FECHA) {
        this.FECHA = FECHA;
    }
    //----------------------------------------------------------------------------
}
