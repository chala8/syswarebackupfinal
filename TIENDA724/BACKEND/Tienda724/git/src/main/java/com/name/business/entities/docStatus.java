package com.name.business.entities;
import org.skife.jdbi.cglib.core.Local;

import java.time.LocalDate;

import java.util.Date;

public class docStatus {
    private String document_status;
    private Long id_document_status;



    public docStatus(String document_status,Long id_document_status) {
        this.document_status = document_status;
        this.id_document_status = id_document_status;
    }

    public String getdocument_status() {
        return document_status;
    }

    public void setdocument_status(String document_status) {
        this.document_status = document_status;
    }

    //------------------------------------------------------------------------------

    public Long getid_document_status() {
        return id_document_status;
    }

    public void setid_document_status(Long id_document_status) {
        this.id_document_status = id_document_status;
    }

    //------------------------------------------------------------------------------


}


