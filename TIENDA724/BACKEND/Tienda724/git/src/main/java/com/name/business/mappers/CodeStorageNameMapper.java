package com.name.business.mappers;

import com.name.business.entities.Code2Name;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CodeStorageNameMapper implements ResultSetMapper<Code2Name>{

    @Override
    public Code2Name map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Code2Name(
                resultSet.getLong("ID_PRODUCT_STORE"),
                resultSet.getString("PRODUCT_STORE_NAME"),
                resultSet.getString("PRODUCT_STORE_CODE"),
                resultSet.getLong("STANDARD_PRICE")
        );
    }
}