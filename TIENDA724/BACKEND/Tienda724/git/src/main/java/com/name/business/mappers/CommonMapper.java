package com.name.business.mappers;

import com.name.business.entities.Common;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CommonMapper implements ResultSetMapper<Common> {

    @Override
    public Common map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Common(
                resultSet.getLong("ID_COMMON"),
                resultSet.getString("NAME"),
                resultSet.getString("DESCRIPTION")
        );
    }
}
