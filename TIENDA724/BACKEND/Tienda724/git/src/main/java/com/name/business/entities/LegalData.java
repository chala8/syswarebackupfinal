package com.name.business.entities;

public class LegalData {

    private String RESOLUCION_DIAN;
    private String REGIMEN_TRIBUTARIO;
    private String AUTORETENEDOR;
    private String URL_LOGO;
    private String CITY_NAME;
    private String COUNTRY_NAME;
    private String ADDRESS;
    private Long PHONE1;
    private String EMAIL;
    private String WEBPAGE;
    private String PREFIX_BILL;
    private String NOTES;

    public LegalData(String RESOLUCION_DIAN,
            String REGIMEN_TRIBUTARIO,
            String AUTORETENEDOR,
            String URL_LOGO,
            String CITY_NAME,
            String COUNTRY_NAME,
            String ADDRESS,
            Long PHONE1,
            String EMAIL,
            String WEBPAGE,
            String PREFIX_BILL,
            String NOTES) {
        this.RESOLUCION_DIAN = RESOLUCION_DIAN;
        this.REGIMEN_TRIBUTARIO = REGIMEN_TRIBUTARIO;
        this.AUTORETENEDOR =  AUTORETENEDOR;
        this.URL_LOGO = URL_LOGO;
        this.CITY_NAME = CITY_NAME;
        this.COUNTRY_NAME =  COUNTRY_NAME;
        this.ADDRESS = ADDRESS;
        this.PHONE1 = PHONE1;
        this.EMAIL =  EMAIL;
        this.WEBPAGE = WEBPAGE;
        this.PREFIX_BILL = PREFIX_BILL;
        this.NOTES =  NOTES;
    }
    //--------------------------------------------------------------------------

    public String getRESOLUCION_DIAN() { return RESOLUCION_DIAN; }

    public void setRESOLUCION_DIAN(String RESOLUCION_DIAN) { this.RESOLUCION_DIAN = RESOLUCION_DIAN; }

    //--------------------------------------------------------------------------

    public String getREGIMEN_TRIBUTARIO() { return REGIMEN_TRIBUTARIO; }

    public void setREGIMEN_TRIBUTARIO(String REGIMEN_TRIBUTARIO) { this.REGIMEN_TRIBUTARIO = REGIMEN_TRIBUTARIO; }

    //--------------------------------------------------------------------------

    public String getAUTORETENEDOR() { return AUTORETENEDOR; }

    public void setAUTORETENEDOR(String AUTORETENEDOR) { this.AUTORETENEDOR = AUTORETENEDOR; }

    //--------------------------------------------------------------------------

    public String getURL_LOGO() { return URL_LOGO; }

    public void setURL_LOGO(String URL_LOGO) { this.URL_LOGO = URL_LOGO; }

    //--------------------------------------------------------------------------

    public String getCITY_NAME() { return CITY_NAME; }

    public void setCITY_NAME(String CITY_NAME) { this.CITY_NAME = CITY_NAME; }

    //--------------------------------------------------------------------------

    public String getCOUNTRY_NAME() { return COUNTRY_NAME; }

    public void setCOUNTRY_NAME(String COUNTRY_NAME) { this.COUNTRY_NAME = COUNTRY_NAME; }

    //--------------------------------------------------------------------------

    public String getADDRESS() { return ADDRESS; }

    public void setADDRESS(String ADDRESS) { this.ADDRESS = ADDRESS; }

    //--------------------------------------------------------------------------

    public Long getPHONE1() { return PHONE1; }

    public void setPHONE1(Long PHONE1) { this.PHONE1 = PHONE1; }

    //--------------------------------------------------------------------------

    public String getEMAIL() { return EMAIL; }

    public void setEMAIL(String EMAIL) { this.EMAIL = EMAIL; }

    //--------------------------------------------------------------------------

    public String getWEBPAGE() { return WEBPAGE; }

    public void setWEBPAGE(String WEBPAGE) { this.WEBPAGE = WEBPAGE; }

    //--------------------------------------------------------------------------

    public String getPREFIX_BILL() { return PREFIX_BILL; }

    public void setPREFIX_BILL(String PREFIX_BILL) { this.PREFIX_BILL = PREFIX_BILL; }

    //--------------------------------------------------------------------------

    public String getNOTES() { return NOTES; }

    public void setNOTES(String NOTES) { this.NOTES = NOTES; }

    //--------------------------------------------------------------------------



}
