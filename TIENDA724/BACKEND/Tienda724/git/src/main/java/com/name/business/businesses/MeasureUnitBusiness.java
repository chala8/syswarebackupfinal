package com.name.business.businesses;

import com.name.business.DAOs.MeasureUnitDAO;
import com.name.business.entities.*;
import com.name.business.entities.MeasureUnit;
import com.name.business.representations.MeasureUnitDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.measureUnitSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class MeasureUnitBusiness {
    private MeasureUnitDAO measureUnitDAO;
    private CommonBusiness commonBusiness;
    private CommonStateBusiness commonStateBusiness;

    public MeasureUnitBusiness(MeasureUnitDAO measureUnitDAO, CommonBusiness commonBusiness, CommonStateBusiness commonStateBusiness) {
        this.measureUnitDAO = measureUnitDAO;
        this.commonBusiness = commonBusiness;
        this.commonStateBusiness = commonStateBusiness;
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = measureUnitDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    public Either<IException, Long> createMeasureUnit(MeasureUnitDTO measureUnitDTO) {

        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_product = null;
        Long id_common_state = null;
        //System.out.println(measureUnitDAO.getPkLast());
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting creation Product  ||||||||||||||||| ");
            if (measureUnitDTO!=null){

                //TODO validate the  id_measure_unit if exist on database a register with this id
                if (measureUnitDTO.getId_measure_unit_father()!=null && measureUnitDTO.getId_measure_unit_father()>0 && validatorID(measureUnitDTO.getId_measure_unit_father()).equals(false)){
                    //msn.add("It ID Measure does not exist register on  Measure, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (measureUnitDTO.getCommonDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonBusiness.createCommon(measureUnitDTO.getCommonDTO());
                    if (commonThirdEither.isRight()) {
                        id_common = commonThirdEither.right().value();
                    }
                }

                if (measureUnitDTO.getStateDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(measureUnitDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }

                measureUnitDAO.create(formatoLongSql(measureUnitDTO.getId_measure_unit_father()),formatoLongSql(id_common),formatoLongSql(id_common_state));
                id_product = measureUnitDAO.getPkLast();

                return Either.right(id_product);
            }else{
                //msn.add("It does not recognize Product, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * @param id_measure_unit
     * @return
     */
    public Either<IException, Long> deleteMeasureUnit(Long id_measure_unit) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting delete Measure  Unit ||||||||||||||||| ");
            if (id_measure_unit != null && id_measure_unit > 0) {
                List<CommonSimple> readCommons = measureUnitDAO.readCommons(formatoLongSql(id_measure_unit));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common
                return Either.right(id_measure_unit);
            } else {
                //msn.add("It does not recognize ID Measure Unit List, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_measure_unit
     * @param measureUnitDTO
     * @return
     */
    public Either<IException, Long> updateMeasureUnit(Long id_measure_unit, MeasureUnitDTO measureUnitDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        MeasureUnit actualMeasureUnit=null;

        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting update Inventory ||||||||||||||||| ");
            if ((id_measure_unit != null && id_measure_unit > 0)) {

                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorID(id_measure_unit).equals(false)){
                    //msn.add("It ID Attribute does not exist register on  Attribute Value, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<MeasureUnit> read = measureUnitDAO.read(new MeasureUnit(id_measure_unit,null,
                        new Common(null,null,null),
                        new CommonState(null,null,null,null)));

                if (read.size()<=0){
                    //msn.add("It does not recognize ID Inventory, probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = measureUnitDAO.readCommons(formatoLongSql(id_measure_unit));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }


                // Common basic info update
                commonBusiness.updateCommon(idCommons.getId_common(), measureUnitDTO.getCommonDTO());

                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), measureUnitDTO.getStateDTO());

                actualMeasureUnit = read.get(0);
                // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                if (measureUnitDTO.getId_measure_unit_father() == null || measureUnitDTO.getId_measure_unit_father()<1) {
                    measureUnitDTO.setId_measure_unit_father(formatoLongSql(actualMeasureUnit.getId_measure_unit_father()));
                } else {
                    measureUnitDTO.setId_measure_unit_father(formatoLongSql(measureUnitDTO.getId_measure_unit_father()));
                }
                // Inventory update
                measureUnitDAO.update(id_measure_unit,measureUnitDTO.getId_measure_unit_father());

                return Either.right(id_measure_unit);

            } else {
                //msn.add("It does not recognize ID Inventory, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param measureUnit
     *
     * @return
     */
    public Either<IException, List<MeasureUnit>> getMeasureUnit(MeasureUnit measureUnit) {
        List<String> msn = new ArrayList<>();
        try {

            //System.out.println("|||||||||||| Starting consults MeasureUnit ||||||||||||||||| ");
            MeasureUnit measureUnitSanitation = measureUnitSanitation(measureUnit);
            return Either.right(measureUnitDAO.read(measureUnitSanitation));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

}
