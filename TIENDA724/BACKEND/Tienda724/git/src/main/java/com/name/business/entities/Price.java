package com.name.business.entities;

public class Price {

    private String PRICE_DESCRIPTION;
    private double PRICE;
    private double pct_descuento;


    public Price(String PRICE_DESCRIPTION, double PRICE, double pct_descuento) {
        this.PRICE_DESCRIPTION = PRICE_DESCRIPTION;
        this.PRICE = PRICE;
        this.pct_descuento = pct_descuento;
    }

    public double getPRICE() {
        return PRICE;
    }

    public void setPRICE(double PRICE) {
        this.PRICE = PRICE;
    }

    public String getPRICE_DESCRIPTION() {
        return PRICE_DESCRIPTION;
    }

    public void setPRICE_DESCRIPTION(String PRICE_DESCRIPTION) {
        this.PRICE_DESCRIPTION = PRICE_DESCRIPTION;
    }

    public double getPct_descuento() {
        return pct_descuento;
    }

    public void setPct_descuento(double pct_descuento) {
        this.pct_descuento = pct_descuento;
    }
}
