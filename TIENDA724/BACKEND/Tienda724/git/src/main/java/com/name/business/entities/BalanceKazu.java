package com.name.business.entities;

public class BalanceKazu {

    private Long CODIGO_CUENTA;
    private String DESCRIPCION;
    private Double VALOR;
    private Long ID_STORE;
    private Double DEBITO;
    private Double CREDITO;
    private Double SALDO;
    private Double SALDO_INICIAL;
    private Double SALDO_TOTAL;


    public BalanceKazu(Long CODIGO_CUENTA,
                       String DESCRIPCION,
                       Double VALOR,
                       Long ID_STORE,
                       Double DEBITO,
                       Double CREDITO,
                       Double SALDO,
                       Double SALDO_INICIAL,
                       Double SALDO_TOTAL) {
        this.CODIGO_CUENTA = CODIGO_CUENTA;
        this.DESCRIPCION = DESCRIPCION;
        this.SALDO = SALDO;
        this.VALOR = VALOR;
        this.ID_STORE = ID_STORE;
        this.DEBITO = DEBITO;
        this.CREDITO = CREDITO;
        this.SALDO_TOTAL = SALDO_TOTAL;
        this.SALDO_INICIAL = SALDO_INICIAL;
    }
    public void setSALDO_INICIAL(Double SALDO_INICIAL) {
        this.SALDO_INICIAL = SALDO_INICIAL;
    }

    public Double getSALDO_INICIAL() {
        return SALDO_INICIAL;
    }

    public void setSALDO_TOTAL(Double SALDO_TOTAL) {
        this.SALDO_TOTAL = SALDO_TOTAL;
    }

    public Double getSALDO_TOTAL() {
        return SALDO_TOTAL;
    }

    public void setCREDITO(Double CREDITO) {
        this.CREDITO = CREDITO;
    }

    public Double getCREDITO() {
        return CREDITO;
    }

    public void setDEBITO(Double DEBITO) {
        this.DEBITO = DEBITO;
    }

    public Double getDEBITO() {
        return DEBITO;
    }

    public void setVALOR(Double VALOR) {
        this.VALOR = VALOR;
    }

    public Double getVALOR() {
        return VALOR;
    }

    public Long getID_STORE() {
        return ID_STORE;
    }

    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    public Long getCODIGO_CUENTA() {
        return CODIGO_CUENTA;
    }

    public void setCODIGO_CUENTA(Long CODIGO_CUENTA) {
        this.CODIGO_CUENTA = CODIGO_CUENTA;
    }

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }

    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }

    public Double getSALDO() {
        return SALDO;
    }

    public void setSALDO(Double SALDO) {
        this.SALDO = SALDO;
    }
}
