package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class AttributeDTO {
    private CommonDTO commonDTO;
    private CommonStateDTO stateDTO;
    private  List<AttributeValueDTO> attributeValueDTOS;


    @JsonCreator
    public AttributeDTO(@JsonProperty("values")  List<AttributeValueDTO> attributeValueDTOS,
                        @JsonProperty("common") CommonDTO commonDTO,
                        @JsonProperty("state") CommonStateDTO stateDTO) {
        this.attributeValueDTOS=attributeValueDTOS;
        this.commonDTO = commonDTO;
        this.stateDTO = stateDTO;
    }

    public List<AttributeValueDTO> getAttributeValueDTOS() {
        return attributeValueDTOS;
    }

    public void setAttributeValueDTOS(List<AttributeValueDTO> attributeValueDTOS) {
        this.attributeValueDTOS = attributeValueDTOS;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
