package com.name.business.DAOs;

import com.name.business.entities.CommonSimple;
import com.name.business.entities.MeasureUnit;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.MeasureUnitMapper;
import com.name.business.representations.InventoryDetailDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

import java.util.List;

@RegisterMapper(MeasureUnitMapper.class)
public interface MeasureUnitDAO {

    @SqlQuery("SELECT ID_MEASURE_UNIT FROM MEASURE_UNIT WHERE ID_MEASURE_UNIT IN (SELECT MAX(ID_MEASURE_UNIT) FROM MEASURE_UNIT)")
    Long getPkLast();

    @SqlQuery("SELECT COUNT(ID_MEASURE_UNIT) FROM MEASURE_UNIT WHERE ID_MEASURE_UNIT = :id_measure_unit")
    Integer getValidatorID(@Bind("id_measure_unit") Long id_measure_unit);


    @SqlUpdate("INSERT INTO MEASURE_UNIT ( ID_MEASURE_UNIT_FATHER,ID_COMMON,ID_COMMON_STATE) VALUES " +
            "                        (:id_measure_unit_father,:id_common,:id_common_state)")
    void create(@Bind("id_measure_unit_father") Long id_measure_unit_father,@Bind("id_common") Long id_common,
                @Bind("id_common_state") Long id_common_state);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_MEASURE_UNIT ID,ID_COMMON ID_COMMON, ID_COMMON_STATE FROM MEASURE_UNIT " +
            "  WHERE (ID_MEASURE_UNIT = :id_measure_unit OR :id_measure_unit IS NULL)")
    List<CommonSimple> readCommons(@Bind("id_measure_unit") Long id_measure_unit);

    @SqlQuery("SELECT * FROM V_MEASURE_UNIT V_MEASURE_UNIT WHERE \n" +
            "  ( V_MEASURE_UNIT.ID_MEASURE_UNIT = :measure_unit.id_measure_unit or :measure_unit.id_measure_unit is NULL ) AND\n" +
            "  ( V_MEASURE_UNIT.ID_MEASURE_UNIT_FATHER= :measure_unit.id_measure_unit_father or :measure_unit.id_measure_unit_father is NULL ) AND\n" +
            "  ( V_MEASURE_UNIT.ID_COMMON= :measure_unit.id_common_measure_unit or :measure_unit.id_common_measure_unit is NULL ) AND\n" +
            "  ( V_MEASURE_UNIT.NAME LIKE :measure_unit.name_measure_unit or :measure_unit.name_measure_unit is NULL ) AND\n" +
            "  ( V_MEASURE_UNIT.DESCRIPTION LIKE :measure_unit.description_measure_unit or :measure_unit.description_measure_unit is NULL ) AND\n" +
            "  ( V_MEASURE_UNIT.ID_STATE= :measure_unit.id_state_measure_unit or :measure_unit.id_state_measure_unit is NULL ) AND\n" +
            "  ( V_MEASURE_UNIT.STATE= :measure_unit.state_measure_unit or :measure_unit.state_measure_unit is NULL ) AND\n" +
            "  ( V_MEASURE_UNIT.CREATION_MEASURE_UNIT= :measure_unit.creation_measure_unit or :measure_unit.creation_measure_unit is NULL ) AND\n" +
            "  ( V_MEASURE_UNIT.MODIFY_MEASURE_UNIT= :measure_unit.modify_measure_unit or :measure_unit.modify_measure_unit is NULL )\n")
    List<MeasureUnit> read(@BindBean("measure_unit") MeasureUnit measureUnit);


    @SqlUpdate("UPDATE MEASURE_UNIT SET\n" +
            "    ID_MEASURE_UNIT_FATHER = :id_measure_unit_father\n" +
            "    WHERE\n" +
            "            (ID_MEASURE_UNIT =  :id_measure_unit)\n")
    void update( @Bind("id_measure_unit") Long id_inventory_detail, @Bind("id_measure_unit_father") Long id_inventory);




}
