package com.name.business.DAOs;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

/**
 * Created by luis on 19/04/17.
 */
public interface DatoDAO {

    @SqlUpdate("INSERT INTO MAIL ( MAIL,PRIORITY) VALUES (:pa,:p)")
    void CREAR_DATO(@Bind("pa") String a, @Bind("p") int i);
}
