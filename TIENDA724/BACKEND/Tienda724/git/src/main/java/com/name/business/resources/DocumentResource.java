package com.name.business.resources;


import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.DocumentBusiness;
import com.name.business.entities.Document;
import com.name.business.representations.DocumentDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/documents")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DocumentResource {

    private DocumentBusiness documentBusiness;

    public DocumentResource(DocumentBusiness documentBusiness) {
        this.documentBusiness = documentBusiness;
    }

    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCategories(@QueryParam("id_document") Long id_document, @QueryParam("id_common") Long id_common,
                                  @QueryParam("id_document_father") Long id_document_father, @QueryParam("img_url") String img_url,
                                  @QueryParam("name") String name, @QueryParam("description") String description,
                                  @QueryParam("id_third_document") Long id_third_document,
                                  @QueryParam("id_state") Long id_state, @QueryParam("state") Integer state,
                                  @QueryParam("creation_attribute") Date creation_attribute,
                                  @QueryParam("modify_attribute") Date modify_attribute) {
        Response response;

        Either<IException, List<Document>> getCategoriesEither = documentBusiness.getDocuments(

                new Document(id_document, name, description)

                );
        if (getCategoriesEither.isRight()) {
            //System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }


    /**
     *
     * @param documentDTO
     * @return status
     */
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postDocumentResource(DocumentDTO documentDTO){
        Response response;

        Either<IException, Long> mailEither = documentBusiness.createDocument(documentDTO);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateDocumentResource(@PathParam("id") Long id_document, DocumentDTO documentDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = documentBusiness.updateDocument(id_document, documentDTO);

        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deletePersonResource(@PathParam("id") Long id_document){
        Response response;
        Either<IException, Long> allViewOffertsEither = documentBusiness.deleteDocument(id_document);

        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

}


