package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;

public class StorageDTO {


    private Long id_store;
    private String description;
    private Long storage_number;

    @JsonCreator
    public StorageDTO(@JsonProperty("id_store") Long id_store,
                       @JsonProperty("description") String description,
                           @JsonProperty("storage_number") Long storage_number) {
        this.id_store = id_store;
        this.description = description;
        this.storage_number = storage_number;
    }

    //------------------------------------------------------------------------------------------------

    public Long getid_store() {
        return id_store;
    }

    public void setid_store(Long id_store) {
        this.id_store = id_store;
    }

    //------------------------------------------------------------------------------------------------

    public String getdescription() {
        return description;
    }

    public void setdescription(String description) {
        this.description = description;
    }

    //------------------------------------------------------------------------------------------------

    public Long getstorage_number() { return storage_number; }

    public void setstorage_number(Long storage_number) { this.storage_number = storage_number; }

    //------------------------------------------------------------------------------------------------


}
