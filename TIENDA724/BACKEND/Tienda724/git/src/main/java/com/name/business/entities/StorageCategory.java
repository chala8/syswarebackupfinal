package com.name.business.entities;

public class StorageCategory {

    private Long ID_CATEGORY;
    private Long ID_CATEGORY_FATHER;
    private Long ID_THIRD;
    private String NAME;
    private String DESCRIPTION;
    private String IMG_URL;


    public StorageCategory(Long ID_CATEGORY,
                           Long ID_CATEGORY_FATHER,
                           Long ID_THIRD,
                           String NAME,
                           String DESCRIPTION,
                           String IMG_URL) {
        this.ID_CATEGORY = ID_CATEGORY;
        this.ID_CATEGORY_FATHER = ID_CATEGORY_FATHER;
        this.ID_THIRD = ID_THIRD;
        this.NAME = NAME;
        this.DESCRIPTION = DESCRIPTION;
        this.IMG_URL = IMG_URL;

    }

    public Long getID_CATEGORY() {
        return ID_CATEGORY;
    }

    public void setID_CATEGORY(Long ID_CATEGORY) {
        this.ID_CATEGORY = ID_CATEGORY;
    }

    //----------------------------------------------------------------------------------------------

    public Long getID_CATEGORY_FATHER() {
        return ID_CATEGORY_FATHER;
    }

    public void setID_CATEGORY_FATHER(Long ID_CATEGORY_FATHER) {
        this.ID_CATEGORY_FATHER = ID_CATEGORY_FATHER;
    }

    //----------------------------------------------------------------------------------------------

    public Long getID_THIRD() {
        return ID_THIRD;
    }

    public void setID_THIRD(Long ID_THIRD) {
        this.ID_THIRD = ID_THIRD;
    }

    //----------------------------------------------------------------------------------------------

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    //----------------------------------------------------------------------------------------------

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    //----------------------------------------------------------------------------------------------

    public String getIMG_URL() {
        return IMG_URL;
    }

    public void setIMG_URL(String IMG_URL) {
        this.IMG_URL = IMG_URL;
    }

    //----------------------------------------------------------------------------------------------



}
