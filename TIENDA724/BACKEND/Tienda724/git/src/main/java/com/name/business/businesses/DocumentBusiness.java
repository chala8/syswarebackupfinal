package com.name.business.businesses;

import com.name.business.DAOs.DocumentDAO;
import com.name.business.entities.Document;
import com.name.business.representations.DocumentDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.documentSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

public class DocumentBusiness {

    private DocumentDAO documentDAO;

    public DocumentBusiness(DocumentDAO documentDAO) {
        this.documentDAO = documentDAO;
    }

    /**
     *
     * @param document
     *
     * @return
     */
    public Either<IException, List<Document>> getDocuments(Document document) {
        List<String> msn = new ArrayList<>();
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting consults Document ||||||||||||||||| ");
            return Either.right(documentDAO.read(documentSanitation(document)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException,Long> createDocument(DocumentDTO documentDTO){
        List<String> msn = new ArrayList<>();
        Long id_document = null;
        //System.out.println(documentDAO.getPkLast());
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting creation Attibute  ||||||||||||||||| ");
            if (documentDTO!=null){

                documentDAO.create(documentDTO);

                id_document = documentDAO.getPkLast();
                return Either.right(id_document);
            }else{
                //msn.add("It does not recognize Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     *
     * @param id_document
     * @param documentDTO
     * @return
     */
    public Either<IException, Long> updateDocument(Long id_document, DocumentDTO documentDTO) {
        List<String> msn = new ArrayList<>();
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting update Document  ||||||||||||||||| ");
            if (documentDTO != null && (id_document!=null || id_document>0)) {

                // document update
                documentDAO.update(formatoLongSql(id_document),validatorDocumentDTO(id_document,documentDTO));

                return Either.right(id_document);

            } else {
                //msn.add("It does not recognize Document , probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    private DocumentDTO validatorDocumentDTO(Long id_document, DocumentDTO documentDTO) {
        try {
            Document currentDocument=null;
            List<Document> documentList = documentDAO.read(new Document(id_document, null, null));
            if (documentList.size()>0){
                currentDocument = documentList.get(0);
            }

            // Validate Name inserted, if null, set the Name to the actual in the database, if not it is formatted to sql
            if (documentDTO.getName() == null || documentDTO.getName().equals("")) {
                documentDTO.setName(currentDocument.getName());
            } else {
                documentDTO.setName(formatoStringSql(documentDTO.getName()));
            }

            // Validate Name inserted, if null, set the Name to the actual in the database, if not it is formatted to sql
            if (documentDTO.getDescription() == null || documentDTO.getDescription().equals("")) {
                documentDTO.setDescription(currentDocument.getDescription());
            } else {
                documentDTO.setDescription(formatoStringSql(documentDTO.getDescription()));
            }
            return documentDTO;

        }catch (Exception e){
            return  null;
        }

    }


    /**
     * @param idDocument
     * @return
     */
    public Either<IException, Long> deleteDocument(Long idDocument) {
        List<String> msn = new ArrayList<>();
        try {
            if (idDocument != 0) {
                //msn.add("OK");
                //System.out.println("|||||||||||| Starting CHANGE STATE DELETE Document ||||||||||||||||| ");
                documentDAO.delete(formatoLongSql(idDocument), new Date());

                return Either.right(idDocument);
            } else {
                //msn.add("It does not recognize ID Document, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
