package com.name.business.entities;

public class ProductName {

    private Long ID_PRODUCT;
    private String PRODUCT;
    private String DESCRIPTION;
    private Long ID_THIRD;



    public ProductName(Long ID_PRODUCT, String PRODUCT, String DESCRIPTION, Long ID_THIRD) {
        this.ID_PRODUCT = ID_PRODUCT;
        this.PRODUCT = PRODUCT;
        this.DESCRIPTION =  DESCRIPTION;
        this.ID_THIRD = ID_THIRD;
    }

    public Long getID_PRODUCT() {
        return ID_PRODUCT;
    }

    public void setID_PRODUCT(Long ID_PRODUCT) {
        this.ID_PRODUCT = ID_PRODUCT;
    }

    public String getPRODUCT() {
        return PRODUCT;
    }

    public void setPRODUCT(String PRODUCT) {
        this.PRODUCT = PRODUCT;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public Long getID_THIRD() {
        return ID_THIRD;
    }

    public void setID_THIRD(Long ID_THIRD) {
        this.ID_THIRD = ID_THIRD;
    }


}
