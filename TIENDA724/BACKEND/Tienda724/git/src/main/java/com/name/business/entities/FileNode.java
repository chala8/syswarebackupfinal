package com.name.business.entities;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by luis on 8/04/17.
 */
public class FileNode {

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<FileNode> getChildren() {
        return children;
    }

    public void setChildren(List<FileNode> children) {
        this.children = children;
    }

    public String getCodigoCuenta() {
        return codigoCuenta;
    }

    public void setCodigoCuenta(String codigoCuenta) {
        this.codigoCuenta = codigoCuenta;
    }


    private String filename;
    private String codigoCuenta;
    private String type;
    private List<FileNode> children;

    public FileNode(String filename,
                    String codigoCuenta,
                    String type,
                    List<FileNode> children) {
        this.filename = filename;
        this.type = type;
        this.codigoCuenta = codigoCuenta;
        this.children = children;
    }

}
