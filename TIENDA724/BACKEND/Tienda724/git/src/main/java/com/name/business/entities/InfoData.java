package com.name.business.entities;

import java.util.Date;

public class InfoData {
    private Long idstoreclient;
    private Long idthirdfather;
    //
    private String Logo_Cliente;
    private String Nombretienda_Cliente;
    private String Nombreciudad_Cliente;
    private String Direccion_Cliente;
    private String Telefono_Cliente;
    private String Email_Cliente;
    private String Lat_Cliente;
    private String Lng_Cliente;
    //
    private String Logo_Prov;
    private String Nombretienda_Prov;
    private String Nombreciudad_Prov;
    private String Direccion_Prov;
    private String Telefono_Prov;
    private String Email_Prov;


    public InfoData(Long idstoreclient, Long idthirdfather, String logo_Cliente, String nombretienda_Cliente, String nombreciudad_Cliente, String direccion_Cliente,
                    String telefono_Cliente, String email_Cliente, String logo_Prov, String nombretienda_Prov, String nombreciudad_Prov, String direccion_Prov,
                    String telefono_Prov, String email_Prov, String Lat_Cliente, String Lng_Cliente) {
        this.Lat_Cliente = Lat_Cliente;
        this.Lng_Cliente = Lng_Cliente;
        this.idstoreclient = idstoreclient;
        this.idthirdfather = idthirdfather;
        Logo_Cliente = logo_Cliente;
        Nombretienda_Cliente = nombretienda_Cliente;
        Nombreciudad_Cliente = nombreciudad_Cliente;
        Direccion_Cliente = direccion_Cliente;
        Telefono_Cliente = telefono_Cliente;
        Email_Cliente = email_Cliente;
        Logo_Prov = logo_Prov;
        Nombretienda_Prov = nombretienda_Prov;
        Nombreciudad_Prov = nombreciudad_Prov;
        Direccion_Prov = direccion_Prov;
        Telefono_Prov = telefono_Prov;
        Email_Prov = email_Prov;
    }

    public String getLat_Cliente() {
        return Lat_Cliente;
    }

    public void setLat_Cliente(String lat_Cliente) {
        Lat_Cliente = lat_Cliente;
    }

    public String getLng_Cliente() {
        return Lng_Cliente;
    }

    public void setLng_Cliente(String lng_Cliente) {
        Lng_Cliente = lng_Cliente;
    }

    public Long getIdstoreclient() {
        return idstoreclient;
    }

    public void setIdstoreclient(Long idstoreclient) {
        this.idstoreclient = idstoreclient;
    }

    public Long getIdthirdfather() {
        return idthirdfather;
    }

    public void setIdthirdfather(Long idthirdfather) {
        this.idthirdfather = idthirdfather;
    }

    public String getLogo_Cliente() {
        return Logo_Cliente;
    }

    public void setLogo_Cliente(String logo_Cliente) {
        Logo_Cliente = logo_Cliente;
    }

    public String getNombretienda_Cliente() {
        return Nombretienda_Cliente;
    }

    public void setNombretienda_Cliente(String nombretienda_Cliente) {
        Nombretienda_Cliente = nombretienda_Cliente;
    }

    public String getNombreciudad_Cliente() {
        return Nombreciudad_Cliente;
    }

    public void setNombreciudad_Cliente(String nombreciudad_Cliente) {
        Nombreciudad_Cliente = nombreciudad_Cliente;
    }

    public String getDireccion_Cliente() {
        return Direccion_Cliente;
    }

    public void setDireccion_Cliente(String direccion_Cliente) {
        Direccion_Cliente = direccion_Cliente;
    }

    public String getTelefono_Cliente() {
        return Telefono_Cliente;
    }

    public void setTelefono_Cliente(String telefono_Cliente) {
        Telefono_Cliente = telefono_Cliente;
    }

    public String getEmail_Cliente() {
        return Email_Cliente;
    }

    public void setEmail_Cliente(String email_Cliente) {
        Email_Cliente = email_Cliente;
    }

    public String getLogo_Prov() {
        return Logo_Prov;
    }

    public void setLogo_Prov(String logo_Prov) {
        Logo_Prov = logo_Prov;
    }

    public String getNombretienda_Prov() {
        return Nombretienda_Prov;
    }

    public void setNombretienda_Prov(String nombretienda_Prov) {
        Nombretienda_Prov = nombretienda_Prov;
    }

    public String getNombreciudad_Prov() {
        return Nombreciudad_Prov;
    }

    public void setNombreciudad_Prov(String nombreciudad_Prov) {
        Nombreciudad_Prov = nombreciudad_Prov;
    }

    public String getDireccion_Prov() {
        return Direccion_Prov;
    }

    public void setDireccion_Prov(String direccion_Prov) {
        Direccion_Prov = direccion_Prov;
    }

    public String getTelefono_Prov() {
        return Telefono_Prov;
    }

    public void setTelefono_Prov(String telefono_Prov) {
        Telefono_Prov = telefono_Prov;
    }

    public String getEmail_Prov() {
        return Email_Prov;
    }

    public void setEmail_Prov(String email_Prov) {
        Email_Prov = email_Prov;
    }
}
