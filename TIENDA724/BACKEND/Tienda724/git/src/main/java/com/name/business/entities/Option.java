package com.name.business.entities;

public class Option {
    private String OPCION;
    private String DESCRIPCION;
    private Boolean CHECK;

    public Option(String OPCION, String DESCRIPCION) {
        this.OPCION = OPCION;
        this.DESCRIPCION = DESCRIPCION;
        this.CHECK = false;
    }

    public Boolean getCHECK() {
        return CHECK;
    }

    public void setCHECK(Boolean CHECK) {
        this.CHECK = CHECK;
    }

    public String getOPCION() {
        return OPCION;
    }

    public void setOPCION(String OPCION) {
        this.OPCION = OPCION;
    }

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }

    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }
}
