package com.name.business.entities;

import java.util.Date;

public class Code2Name {
    private Long ID_CODE;
    private String OWNBARCODE;
    private String PRODUCT_STORE_NAME;
    private String PRODUCT_STORE_CODE;
    private Long STANDARD_PRICE;
    private Long QUANTITY;
    private Long ID_PRODUCT_STORE;
    private String BRAND;
    private String MUN;



    public Code2Name(Long ID_CODE,
                     String OWNBARCODE,
                     String PRODUCT_STORE_NAME,
                     String PRODUCT_STORE_CODE,
                     Long STANDARD_PRICE,
                     Long QUANTITY,
                     String BRAND,
                     String MUN) {
        this.ID_CODE = ID_CODE;
        this.OWNBARCODE = OWNBARCODE;
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
        this.STANDARD_PRICE = STANDARD_PRICE;
        this.QUANTITY = QUANTITY;
        this.BRAND = BRAND;
        this.MUN = MUN;
    }

    public Code2Name(Long ID_PRODUCT_STORE,
                     String PRODUCT_STORE_NAME,
                     String PRODUCT_STORE_CODE,
                     Long STANDARD_PRICE) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
        this.STANDARD_PRICE = STANDARD_PRICE;
    }

    public Long getID_PRODUCT_STORE() {
        return ID_PRODUCT_STORE;
    }

    public void setID_PRODUCT_STORE(Long ID_PRODUCT_STORE) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
    }

    //------------------------------------------------------------------------------

    public Long getID_CODE() {
        return ID_CODE;
    }

    public void setID_CODE(Long ID_CODE) {
        this.ID_CODE = ID_CODE;
    }

    //------------------------------------------------------------------------------

    public String getOWNBARCODE() {
        return OWNBARCODE;
    }

    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }

    //------------------------------------------------------------------------------

    public String getPRODUCT_STORE_NAME() {
        return PRODUCT_STORE_NAME;
    }

    public void setPRODUCT_STORE_NAME(String PRODUCT_STORE_NAME) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
    }

    //------------------------------------------------------------------------------

    public String getPRODUCT_STORE_CODE() {
        return PRODUCT_STORE_CODE;
    }

    public void setPRODUCT_STORE_CODE(String PRODUCT_STORE_CODE) {
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
    }

    //------------------------------------------------------------------------------

    public Long getSTANDARD_PRICE() {
        return STANDARD_PRICE;
    }

    public void setSTANDARD_PRICE(Long STANDARD_PRICE) {
        this.STANDARD_PRICE = STANDARD_PRICE;
    }

    //------------------------------------------------------------------------------
    public Long getQUANTITY() {
        return QUANTITY;
    }

    public void setQUANTITY(Long QUANTITY) {
        this.QUANTITY = QUANTITY;
    }

    //------------------------------------------------------------------------------

    public String getBRAND() {
        return BRAND;
    }

    public void setBRAND(String BRAND) {
        this.BRAND = BRAND;
    }

    //------------------------------------------------------------------------------

    public String getMUN() {
        return MUN;
    }

    public void setMUN(String MUN) {
        this.MUN = MUN;
    }

    //------------------------------------------------------------------------------
}


