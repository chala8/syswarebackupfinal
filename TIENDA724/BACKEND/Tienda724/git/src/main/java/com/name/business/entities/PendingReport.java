package com.name.business.entities;

public class PendingReport {

    private String OWNBARCODE;
    private String PRODUCT_STORE_NAME;
    private Long QUANTITY;



    public PendingReport(String OWNBARCODE, String PRODUCT_STORE_NAME, Long QUANTITY) {
        this.OWNBARCODE = OWNBARCODE;
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
        this.QUANTITY =  QUANTITY;
    }

    public String getOWNBARCODE() {
        return OWNBARCODE;
    }

    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }

    public String getPRODUCT_STORE_NAME() {
        return PRODUCT_STORE_NAME;
    }

    public void setPRODUCT_STORE_NAME(String PRODUCT_STORE_NAME) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
    }

    public Long getQUANTITY() {
        return QUANTITY;
    }

    public void setQUANTITY(Long QUANTITY) {
        this.QUANTITY = QUANTITY;
    }


}
