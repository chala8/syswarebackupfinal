package com.name.business.entities;

import java.util.Date;

public class InventoryName {

    private Long ID_TAX;
    private Double STANDARD_PRICE;
    private Long ID_PRODUCT_STORE;
    private String PRODUCT_STORE_NAME;
    private String CODE;
    private Long ID_INVENTORY_DETAIL;
    private String OWNBARCODE;
    private String PRODUCT_STORE_CODE;



    public InventoryName(Long ID_TAX,
                         Double STANDARD_PRICE,
                         Long ID_PRODUCT_STORE,
                         String PRODUCT_STORE_NAME,
                         String CODE,
                         Long ID_INVENTORY_DETAIL,
                         String OWNBARCODE,
                         String PRODUCT_STORE_CODE) {
        this.ID_TAX = ID_TAX;
        this.STANDARD_PRICE = STANDARD_PRICE;
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
        this.CODE = CODE;
        this.ID_INVENTORY_DETAIL = ID_INVENTORY_DETAIL;
        this.OWNBARCODE = OWNBARCODE;
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
    }

    //------------------------------------------------------------------------------------------------

    public Long getID_TAX() {
        return ID_TAX;
    }

    public void setID_TAX(Long ID_TAX) {
        this.ID_TAX = ID_TAX;
    }

    //------------------------------------------------------------------------------------------------

    public Double getSTANDARD_PRICE() {
        return STANDARD_PRICE;
    }

    public void setSTANDARD_PRICE(Double STANDARD_PRICE) {
        this.STANDARD_PRICE = STANDARD_PRICE;
    }

    //------------------------------------------------------------------------------------------------

    public Long getID_PRODUCT_STORE() {
        return ID_PRODUCT_STORE;
    }

    public void setID_PRODUCT_STORE(Long ID_PRODUCT_STORE) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
    }

    //------------------------------------------------------------------------------------------------

    public String getPRODUCT_STORE_NAME() {
        return PRODUCT_STORE_NAME;
    }

    public void setPRODUCT_STORE_NAME(String PRODUCT_STORE_NAME) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
    }

    //------------------------------------------------------------------------------------------------

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    //------------------------------------------------------------------------------------------------

    public Long getID_INVENTORY_DETAIL() {
        return ID_INVENTORY_DETAIL;
    }

    public void setID_INVENTORY_DETAIL(Long ID_INVENTORY_DETAIL) {
        this.ID_INVENTORY_DETAIL = ID_INVENTORY_DETAIL;
    }

    //-----------------------------------------------------------------------------------------------

    public String getOWNBARCODE() {
        return OWNBARCODE;
    }

    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }

    //------------------------------------------------------------------------------------------------

    public String getPRODUCT_STORE_CODE() {
        return PRODUCT_STORE_CODE;
    }

    public void setPRODUCT_STORE_CODE(String PRODUCT_STORE_CODE) {
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
    }

    //------------------------------------------------------------------------------------------------
}