package com.name.business.mappers;


import com.name.business.entities.ItemCaja;
import com.name.business.entities.kazu724Document;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class kazu724DocumentMapper implements ResultSetMapper<kazu724Document> {

    @Override
    public kazu724Document map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new kazu724Document(
                resultSet.getLong("ID_DOCUMENT"),
                resultSet.getString("FECHA"),
                resultSet.getLong("ID_DOCUMENT_TYPE"),
                resultSet.getString("DOCUMENT_TYPE"),
                resultSet.getString("DOCUMENT_STATUS"),
                resultSet.getLong("CONSECUTIVO"),
                resultSet.getLong("ID_DOCUMENT_STATUS"),
                resultSet.getString("NOTES"),
                resultSet.getLong("ID_STORE"),
                resultSet.getString("STORE"),
                resultSet.getLong("ID_THIRD"),
                resultSet.getString("FULLNAME")
                );
    }
}
