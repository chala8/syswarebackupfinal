package com.name.business.entities;

public class DetallePlanilla {


    private String PURCHASE_DATE;
    private String NUM_DOCUMENTO;
    private Long ID_BILL_FACTURA;
    private Double TOTALPRICE;



    public DetallePlanilla(
            String PURCHASE_DATE,
            String NUM_DOCUMENTO,
            Long ID_BILL_FACTURA,
            Double TOTALPRICE
    ) {
        this.PURCHASE_DATE = PURCHASE_DATE;
        this.NUM_DOCUMENTO = NUM_DOCUMENTO;
        this.ID_BILL_FACTURA =  ID_BILL_FACTURA;
        this.TOTALPRICE = TOTALPRICE;

    }
    //----------------------------------------------------------------------------
    public String getPURCHASE_DATE() {
        return PURCHASE_DATE;
    }
    public void setPURCHASE_DATE(String PURCHASE_DATE) {
        this.PURCHASE_DATE = PURCHASE_DATE;
    }
    //----------------------------------------------------------------------------
    public String getNUM_DOCUMENTO() {
        return NUM_DOCUMENTO;
    }
    public void setNUM_DOCUMENTO(String NUM_DOCUMENTO) {
        this.NUM_DOCUMENTO = NUM_DOCUMENTO;
    }
    //----------------------------------------------------------------------------
    public Long getID_BILL_FACTURA() {
        return ID_BILL_FACTURA;
    }
    public void setID_BILL_FACTURA(Long ID_BILL_FACTURA) {
        this.ID_BILL_FACTURA = ID_BILL_FACTURA;
    }
    //----------------------------------------------------------------------------
    public Double getTOTALPRICE() {
        return TOTALPRICE;
    }
    public void setTOTALPRICE(Double TOTALPRICE) {
        this.TOTALPRICE = TOTALPRICE;
    }
    //----------------------------------------------------------------------------
}
