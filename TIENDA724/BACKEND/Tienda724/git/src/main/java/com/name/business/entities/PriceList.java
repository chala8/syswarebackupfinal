package com.name.business.entities;

import java.util.Date;

public class PriceList {
    private Long id_price_list;
    private Long id_product_third_pr_list;
    private Double standard_price;
    private Double min_price;

    private Long id_common_pr_list;
    private String name_pr_list;
    private String description_pr_list;
    private Long id_state_pr_list;
    private Integer state_pr_list;
    private Date creation_pr_list;
    private Date modify_pr_list;



    public PriceList(Long id_price_list, Long id_product_third_pr_list, Double standard_price, Double min_price,
                     Common common, CommonState state) {
        this.id_price_list = id_price_list;
        this.id_product_third_pr_list = id_product_third_pr_list;
        this.standard_price = standard_price;
        this.min_price = min_price;
        this.id_price_list = id_price_list;
        this.id_product_third_pr_list = id_product_third_pr_list;
        this.standard_price = standard_price;
        this.min_price = min_price;
        this.id_common_pr_list = common.getId_common();
        this.name_pr_list = common.getName();
        this.description_pr_list = common.getDescription();
        this.id_state_pr_list = state.getId_common_state();
        this.state_pr_list = state.getState();
        this.creation_pr_list = state.getCreation_date();
        this.modify_pr_list = state.getModify_date();
    }


    public Common loadingCommon() {
        return new Common(this.getId_common_pr_list(),
                this.getName_pr_list(),
                this.getDescription_pr_list());
    }

    public void setCommon(Common common) {
        this.id_common_pr_list = common.getId_common();
        this.name_pr_list = common.getName();
        this.description_pr_list = common.getDescription();

    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_pr_list(),
                this.getState_pr_list(),
                this.getCreation_pr_list(),
                this.getModify_pr_list()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_pr_list=state.getId_common_state();
        this.state_pr_list=state.getState();
        this.creation_pr_list=state.getCreation_date();
        this.modify_pr_list=state.getModify_date();
    }

    public Long getId_price_list() {
        return id_price_list;
    }

    public void setId_price_list(Long id_price_list) {
        this.id_price_list = id_price_list;
    }

    public Long getId_product_third_pr_list() {
        return id_product_third_pr_list;
    }

    public void setId_product_third_pr_list(Long id_product_third_pr_list) {
        this.id_product_third_pr_list = id_product_third_pr_list;
    }

    public Double getStandard_price() {
        return standard_price;
    }

    public void setStandard_price(Double standard_price) {
        this.standard_price = standard_price;
    }

    public Double getMin_price() {
        return min_price;
    }

    public void setMin_price(Double min_price) {
        this.min_price = min_price;
    }

    public Long getId_common_pr_list() {
        return id_common_pr_list;
    }

    public void setId_common_pr_list(Long id_common_pr_list) {
        this.id_common_pr_list = id_common_pr_list;
    }

    public String getName_pr_list() {
        return name_pr_list;
    }

    public void setName_pr_list(String name_pr_list) {
        this.name_pr_list = name_pr_list;
    }

    public String getDescription_pr_list() {
        return description_pr_list;
    }

    public void setDescription_pr_list(String description_pr_list) {
        this.description_pr_list = description_pr_list;
    }

    public Long getId_state_pr_list() {
        return id_state_pr_list;
    }

    public void setId_state_pr_list(Long id_state_pr_list) {
        this.id_state_pr_list = id_state_pr_list;
    }

    public Integer getState_pr_list() {
        return state_pr_list;
    }

    public void setState_pr_list(Integer state_pr_list) {
        this.state_pr_list = state_pr_list;
    }

    public Date getCreation_pr_list() {
        return creation_pr_list;
    }

    public void setCreation_pr_list(Date creation_pr_list) {
        this.creation_pr_list = creation_pr_list;
    }

    public Date getModify_pr_list() {
        return modify_pr_list;
    }

    public void setModify_pr_list(Date modify_pr_list) {
        this.modify_pr_list = modify_pr_list;
    }
}
