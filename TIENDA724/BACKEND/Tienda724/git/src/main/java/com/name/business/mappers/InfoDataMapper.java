package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InfoDataMapper implements ResultSetMapper<InfoData> {
    @Override
    public InfoData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new InfoData(
                resultSet.getLong("idstoreclient"),
                resultSet.getLong("idthirdfather"),
                resultSet.getString("LOGOCLIENT"),
                resultSet.getString("storenameclient"),
                resultSet.getString("cityclient"),
                resultSet.getString("addressclient"),
                resultSet.getString("phoneclient"),
                resultSet.getString("mailclient"),
                resultSet.getString("LOGOPROV"),
                resultSet.getString("storenameprov"),
                resultSet.getString("cityprov"),
                resultSet.getString("addressprov"),
                resultSet.getString("phoneprov"),
                resultSet.getString("mailprov"),
                resultSet.getString("latitudstorec"),
                resultSet.getString("longitudstorec")
        );
    }
}
