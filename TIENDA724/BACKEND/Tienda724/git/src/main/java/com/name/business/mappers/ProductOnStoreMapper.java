package com.name.business.mappers;


import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductOnStoreMapper implements ResultSetMapper<ProductOnStore> {
    @Override
    public ProductOnStore map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ProductOnStore(
                resultSet.getString("DESCRIPTION"),
                resultSet.getLong("QUANTITY"),
                resultSet.getString("PRODUCT_STORE_NAME"),
                resultSet.getString("PRODUCT_STORE_CODE"),
                resultSet.getString("CODE"),
                resultSet.getString("MUN"),
                resultSet.getString("BRAND")
        );
    }
}
