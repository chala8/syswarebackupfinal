package com.name.business.entities;

import java.util.Date;

public class TaxTariff
{
    private Long id_tax_tariff;
    private Double percent;
    private Long id_common_tax;
    private String name_tax;
    private String description_tax;
    private Long id_state_tax;
    private Integer state_tax;
    private Date creation_tax;
    private Date modify_tax;

    public TaxTariff(Long id_tax_tariff, Double percent, Common common, CommonState commonState) {
        this.id_tax_tariff = id_tax_tariff;
        this.percent = percent;
        this.id_common_tax = common.getId_common();
        this.name_tax = common.getName();
        this.description_tax = common.getDescription();
        this.id_state_tax = commonState.getId_common_state();
        this.state_tax = commonState.getState();
        this.creation_tax = commonState.getCreation_date();
        this.modify_tax = commonState.getModify_date();

    }

    public Common loadingCommon() {
        return new Common(this.getId_common_tax(),
                this.getName_tax(),
                this.getDescription_tax());
    }

    public void setCommon(Common common) {
        this.id_common_tax = common.getId_common();
        this.name_tax = common.getName();
        this.description_tax = common.getDescription();

    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_tax(),
                this.getState_tax(),
                this.getCreation_tax(),
                this.getModify_tax()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_tax=state.getId_common_state();
        this.state_tax=state.getState();
        this.creation_tax=state.getCreation_date();
        this.modify_tax=state.getModify_date();
    }

    public Long getId_tax_tariff() {
        return id_tax_tariff;
    }

    public void setId_tax_tariff(Long id_tax_tariff) {
        this.id_tax_tariff = id_tax_tariff;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public Long getId_common_tax() {
        return id_common_tax;
    }

    public void setId_common_tax(Long id_common_tax) {
        this.id_common_tax = id_common_tax;
    }

    public String getName_tax() {
        return name_tax;
    }

    public void setName_tax(String name_tax) {
        this.name_tax = name_tax;
    }

    public String getDescription_tax() {
        return description_tax;
    }

    public void setDescription_tax(String description_tax) {
        this.description_tax = description_tax;
    }

    public Long getId_state_tax() {
        return id_state_tax;
    }

    public void setId_state_tax(Long id_state_tax) {
        this.id_state_tax = id_state_tax;
    }

    public Integer getState_tax() {
        return state_tax;
    }

    public void setState_tax(Integer state_tax) {
        this.state_tax = state_tax;
    }

    public Date getCreation_tax() {
        return creation_tax;
    }

    public void setCreation_tax(Date creation_tax) {
        this.creation_tax = creation_tax;
    }

    public Date getModify_tax() {
        return modify_tax;
    }

    public void setModify_tax(Date modify_tax) {
        this.modify_tax = modify_tax;
    }
}
