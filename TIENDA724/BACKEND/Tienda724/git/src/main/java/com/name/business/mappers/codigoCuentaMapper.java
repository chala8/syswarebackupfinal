package com.name.business.mappers;


import com.name.business.entities.codigoCuenta;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class codigoCuentaMapper implements ResultSetMapper<codigoCuenta> {

    @Override
    public codigoCuenta map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new codigoCuenta(
                resultSet.getString("CODIGO_CUENTA"),
                resultSet.getString("DESCRIPCION")
        );
    }
}
