package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VendedorDistribuidorMapper implements ResultSetMapper<VendedorDistribuidor>{

    @Override
    public VendedorDistribuidor map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new VendedorDistribuidor(
                resultSet.getLong("ID_THIRD_VENDEDOR"),
                resultSet.getString("VENDEDOR"),
                resultSet.getLong("ID_THIRD_PROVEEDOR"),
                resultSet.getString("PROVEEDOR"),
                resultSet.getLong("ID_STORE_PROVEEDOR")
        );
    }
}