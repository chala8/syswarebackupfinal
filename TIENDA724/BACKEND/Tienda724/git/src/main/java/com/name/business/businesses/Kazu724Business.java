package com.name.business.businesses;


import com.name.business.DAOs.Kazu724DAO;
import com.name.business.entities.*;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.utils.constans.K.*;


public class Kazu724Business {

    private Kazu724DAO kazu724DAO;

    public Kazu724Business(Kazu724DAO kazu724DAO) {
        this.kazu724DAO = kazu724DAO;
    }



    public Either<IException, List<docTypeKazu> > getDocType( ) {
        try {
            List<docTypeKazu> validator = kazu724DAO.getDocType();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<kazu724Document> > getDocumentHeader( Long id_store,
                                                                         String date1,
                                                                         String date2,
                                                                         List<String> statusList,
                                                                         List<String> typeList) {
        try {
            List<kazu724Document> validator = kazu724DAO.getDocuments(id_store, new Date(date1), new Date(date2),statusList, typeList);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<kazu724Detail> > getDetailsDocument( Long id_document) {
        try {
            List<kazu724Detail> validator = kazu724DAO.getDetailsDocument(id_document);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List<FileNode> > getTreeAccount() {
        try {
            List<FileNode> response = new ArrayList<>();
            List<codigoCuenta> validator = kazu724DAO.gecCodCuentaGen();

            for (codigoCuenta p:validator) {
                FileNode tmp = new FileNode(p.getDESCRIPCION(),p.getCODIGO_CUENTA(),"exe",getSons(new Long(p.getCODIGO_CUENTA())));
                response.add(tmp);
            }


            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public List<FileNode> getSons(Long codCuenta){
        List<codigoCuenta> sons = kazu724DAO.gecCodCuenta(codCuenta);
        List<FileNode> processedSons = new ArrayList<>();

        if(sons.size()>0){
            for (codigoCuenta p:sons) {
                FileNode tmp = new FileNode(p.getDESCRIPCION(),p.getCODIGO_CUENTA(),"exe",getSons(new Long(p.getCODIGO_CUENTA())));
                processedSons.add(tmp);
            }
        }

        return processedSons;
    }


    public  Long  updateDocument( Long id_document, Long id_status, String notes) {
        try {
            kazu724DAO.updateDocument(id_status,notes,id_document);
            return new Long(1);
        }catch (Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }


    public  Long  deleteDocumentDetail( Long id_detail) {
        try {
            kazu724DAO.deleteDocumentDetail(id_detail);
            return new Long(1);
        }catch (Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }

    public  Long  insertDocumentDetail(Long cod_cuenta,
                                       double valor,
                                       String naturaleza,
                                       String notas,
                                       Long id_document) {
        try {
            kazu724DAO.insertDocumentDetail(cod_cuenta, valor, naturaleza, notas, id_document);
            return new Long(1);
        }catch (Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }



    public  Long  insertPuc(Long cod_cuenta,
                                       String descripcion,
                                       Long codigo_padre,
                                       Long id_country) {
        try {
            kazu724DAO.insertPuc(cod_cuenta, descripcion, codigo_padre, id_country);
            return new Long(1);
        }catch (Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }


    public Either<IException, List<BalanceKazu> >   getBalance(Long id_store) {
        try {
            List<BalanceKazu> validator = kazu724DAO.getBalance(id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<docStatus> > getDocStatus( ) {
        try {
            List<docStatus> validator = kazu724DAO.getDocStatus();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<codigoCuenta> > gecCodCuentaGen( ) {
        try {
            List<codigoCuenta> validator = kazu724DAO.gecCodCuentaGen();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<codigoCuenta> > gecCodCuenta( Long cp) {
        try {
            List<codigoCuenta> validator = kazu724DAO.gecCodCuenta(cp);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


//-----------------------------------------------------------------------------------------------------------------------


    public Either<IException, Integer > postPuc( Long cc,
                                                  String description,
                                                  Long ccp,
                                                  Long id_country,
                                                  Long id_store) {
        try {
            kazu724DAO.postPuc(cc,
                        description,
                        ccp,
                        id_country,
                        id_store);
            return Either.right(1);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long > postStatus(  Long id_document_status,
                                                     Long id_document_type,
                                                     String notes,
                                                     Long id_store,
                                                     Long id_third_user) {
        try {
            kazu724DAO.postStatus(id_document_status,
                    id_document_type,
                    notes,
                    id_store,
                    id_third_user);
            return Either.right(kazu724DAO.getPkLast());
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Integer > postDocDetail(   Long cc,
                                                         double valor,
                                                         String naturaleza,
                                                         String notes,
                                                         Long id_document,
                                                         Long id_country,
                                                         Long id_third) {
        try {
            kazu724DAO.postDocDetail(cc,valor,
                    naturaleza,
                    notes,
                    id_document,
                    id_country,
                    id_third);
            return Either.right(1);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Integer generarBalance(Long id_store,
                                  Long profundidad,
                                  Date fecha_inicial,
                                  Date fecha_final) {
        try {
            kazu724DAO.generarBalance(id_store,
                     profundidad,
                     fecha_inicial,
                     fecha_final);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }


    }




    public Long postDocument(Long iddocumenttype,
                                  Long iddocumentstatus,
                                  String notes,
                                  Long idstore,
                                  Long idthirduser,
                                  Long idthird,
                                  String docdetails) {
        try {
            kazu724DAO.postDocument(

                    iddocumenttype,
                    iddocumentstatus,
                    notes,
                    idstore,
                    idthirduser,
                    idthird,
                    docdetails

            );
            return new Long(1);
        }catch (Exception e){
            e.printStackTrace();

            return new Long(0);}
    }
}
