package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;

public class ProductStoreInventoryNameDTO {


    private Long id_product_store;
    private Long id_storage;
    private Long quantity;

    @JsonCreator
    public ProductStoreInventoryNameDTO(@JsonProperty("id_product_store") Long id_product_store,
                               @JsonProperty("id_storage") Long id_storage,
                               @JsonProperty("quantity") Long quantity) {
        this.id_product_store = id_product_store;
        this.id_storage = id_storage;
        this.quantity = quantity;
    }

    //------------------------------------------------------------------------------------------------

    public Long getid_product_store() {
        return id_product_store;
    }

    public void setid_product_store(Long id_product_store) {
        this.id_product_store = id_product_store;
    }

    //------------------------------------------------------------------------------------------------

    public Long getid_storage() { return id_storage; }

    public void setid_storage(Long id_storage) { this.id_storage = id_storage; }

    //------------------------------------------------------------------------------------------------

    public Long getquantity() {
        return quantity;
    }

    public void setquantity(Long quantity) {
        this.quantity = quantity;
    }

    //------------------------------------------------------------------------------------------------


}
