package com.name.business.mappers;

import com.name.business.entities.Store;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.Vehiculo;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VehiculoMapper implements ResultSetMapper<Vehiculo>{

    @Override
    public Vehiculo map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Vehiculo(
                resultSet.getLong("ID_VEHICULO"),
                resultSet.getString("PLACA"),
                resultSet.getString("CONDUCTOR")
        );
    }
}