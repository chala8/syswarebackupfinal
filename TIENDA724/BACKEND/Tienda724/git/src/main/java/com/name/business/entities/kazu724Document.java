package com.name.business.entities;

public class kazu724Document {

    private Long ID_DOCUMENT;
    private String FECHA;
    private Long ID_DOCUMENT_TYPE;
    private String DOCUMENT_TYPE;
    private String DOCUMENT_STATUS;
    private Long CONSECUTIVO;
    private Long ID_DOCUMENT_STATUS;
    private String NOTES;
    private Long ID_STORE;
    private String STORE;
    private Long ID_THIRD;
    private String FULLNAME;



    public kazu724Document(Long ID_DOCUMENT,
             String FECHA,
             Long ID_DOCUMENT_TYPE,
             String DOCUMENT_TYPE,
             String DOCUMENT_STATUS,
             Long CONSECUTIVO,
             Long ID_DOCUMENT_STATUS,
             String NOTES,
             Long ID_STORE,
             String STORE,
             Long ID_THIRD,
             String FULLNAME) {
        this.FULLNAME = FULLNAME;
        this.ID_THIRD = ID_THIRD;
        this.DOCUMENT_STATUS = DOCUMENT_STATUS;
        this.DOCUMENT_TYPE = DOCUMENT_TYPE;
        this.ID_DOCUMENT = ID_DOCUMENT;
        this.FECHA = FECHA;
        this.ID_DOCUMENT_TYPE =  ID_DOCUMENT_TYPE;
        this.CONSECUTIVO = CONSECUTIVO;
        this.ID_DOCUMENT_STATUS = ID_DOCUMENT_STATUS;
        this.NOTES =  NOTES;
        this.ID_STORE = ID_STORE;
        this.STORE =  STORE;
    }


    public Long getID_THIRD() {
        return ID_THIRD;
    }

    public void setID_THIRD(Long ID_THIRD) {
        this.ID_THIRD = ID_THIRD;
    }

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }


    public Long getID_DOCUMENT() {
        return ID_DOCUMENT;
    }
    public void setID_DOCUMENT(Long ID_DOCUMENT) {
        this.ID_DOCUMENT = ID_DOCUMENT;
    }

    public String getFECHA() {
        return FECHA;
    }
    public void setFECHA(String FECHA) {
        this.FECHA = FECHA;
    }

    public Long getID_DOCUMENT_TYPE() {
        return ID_DOCUMENT_TYPE;
    }
    public void setID_DOCUMENT_TYPE(Long ID_DOCUMENT_TYPE) {
        this.ID_DOCUMENT_TYPE = ID_DOCUMENT_TYPE;
    }

    public Long getCONSECUTIVO() {
        return CONSECUTIVO;
    }
    public void setCONSECUTIVO(Long CONSECUTIVO) { this.CONSECUTIVO = CONSECUTIVO;}

    public Long getID_DOCUMENT_STATUS() {
        return ID_DOCUMENT_STATUS;
    }
    public void setID_DOCUMENT_STATUS(Long ID_DOCUMENT_STATUS) {
        this.ID_DOCUMENT_STATUS = ID_DOCUMENT_STATUS;
    }

    public String getNOTES() {
        return NOTES;
    }
    public void setNOTES(String NOTES) {
        this.NOTES = NOTES;
    }

    public Long getID_STORE() {
        return ID_STORE;
    }
    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    public String getSTORE() {
        return STORE;
    }
    public void setSTORE(String STORE) {
        this.STORE = STORE;
    }

    public String getDOCUMENT_TYPE() {
        return DOCUMENT_TYPE;
    }
    public void setDOCUMENT_TYPE(String DOCUMENT_TYPE) {
        this.DOCUMENT_TYPE = DOCUMENT_TYPE;
    }

    public String getDOCUMENT_STATUS() {
        return DOCUMENT_STATUS;
    }
    public void setDOCUMENT_STATUS(String DOCUMENT_STATUS) {
        this.DOCUMENT_STATUS = DOCUMENT_STATUS;
    }


}
