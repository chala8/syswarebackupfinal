package com.name.business.utils.auth;
import io.dropwizard.auth.AuthenticationException;
import com.google.common.base.Optional;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("Guava")
public class CustomAuthenticator implements Authenticator<CustomCredentials, CustomAuthUser> {

    @Override
    public Optional<CustomAuthUser> authenticate(CustomCredentials credentials) throws AuthenticationException {
        CustomAuthUser authenticatedUser = null;
        String token = credentials.getToken();
        //
        if (token != null) {
            if(token.equals("3020D4:1FG-2U113E822-A1TE04529-68CF")){
                List<String> roles = new ArrayList<>();
                roles.add("Auth");//NO BORRAR, este rol representa un usuario autenticado
                authenticatedUser = new CustomAuthUser("Servidor", roles);
            }else if(token.equals("FE651467B48D552C2EFBC8B13EBA9")){
                List<String> roles = new ArrayList<>();
                roles.add("PublicAPI");//NO BORRAR, este rol representa un usuario no necesariamente autenticado para utilizar api publica
                authenticatedUser = new CustomAuthUser("PublicAPI", roles);
            }else{
                //realizo la petición para validar token
                String response = "";
                try {
                    //System.out.println("variable SYSWARE_SERVER es ");
                    //System.out.println(System.getenv("SYSWARE_SERVER"));
                    SSLContextBuilder SSLBuilder = SSLContexts.custom();
                    if(System.getenv("SYSWARE_SERVER").equals("prod")){
                        File file = new File(new File(".").getCanonicalPath()+"/ssl.jks");
                        SSLBuilder = SSLBuilder.loadTrustMaterial(file,"Jkbotero2020$$".toCharArray());
                    }else{
                        File file = new File(new File(".").getCanonicalPath()+"/localhost.jks");
                        try{if(System.getenv("SYSWARE_DEVPC").equals("yes")){SSLBuilder = SSLBuilder.loadTrustMaterial(file,"lel123".toCharArray());}}
                        catch (Exception e){SSLBuilder = SSLBuilder.loadTrustMaterial(file,"Jkbotero2020$$".toCharArray());}
                    }
                    //SSL
                    //Building the SSLContext usiong the build() method
                    SSLContext sslcontext = SSLBuilder.build();
                    //Creating SSLConnectionSocketFactory object
                    SSLConnectionSocketFactory sslConSocFactory = new SSLConnectionSocketFactory(sslcontext, new NoopHostnameVerifier());
                    /////////
                    HttpPost httpPost = new HttpPost(System.getenv("SYSWARE_SERVER").equals("prod") ? "https://tienda724.com:8449/verify":"https://pruebas.tienda724.com:8449/verify");
                    httpPost.setHeader("Content-Type", "application/json");
                    StringEntity params =new StringEntity("{\"token\":\""+token+"\"}");
                    httpPost.setEntity(params);
                    HttpClient httpClient = HttpClientBuilder.create().setSSLSocketFactory(sslConSocFactory).build();
                    BasicResponseHandler responseHandler = new BasicResponseHandler();
                    response = httpClient.execute(httpPost, responseHandler);
                } catch (CertificateException | NoSuchAlgorithmException | KeyStoreException | KeyManagementException | IOException e) {
                    e.printStackTrace();
                    response = "";
                }
                ////System.out.println("response es '"+response+"'");
                if(response != null){
                    if(!response.equals("")){
                        //Agrego los roles al listado
                        List<String> roles = new ArrayList<>();
                        roles.add("Auth");//NO BORRAR, este rol representa un usuario autenticado
                        //Agregar el resto de los roles, futuro
                        authenticatedUser = new CustomAuthUser("Usuario", roles);
                    }
                }
            }
        }

        return Optional.fromNullable(authenticatedUser);
    }
}
