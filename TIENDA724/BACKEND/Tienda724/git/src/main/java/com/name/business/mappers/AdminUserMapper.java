package com.name.business.mappers;

import com.name.business.entities.AdminUser;
import com.name.business.entities.Asociacion;
import com.name.business.entities.AttributeList;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AdminUserMapper implements ResultSetMapper<AdminUser>{

    @Override
    public AdminUser map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new AdminUser(
                resultSet.getString("USUARIO"),
                resultSet.getString("CLAVE")

        );
    }
}
