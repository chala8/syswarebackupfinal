package com.name.business.entities;

import java.util.Date;

public class CodeName {
    private Long ID_CODE;
    private String CODE;
    private String PRODUCT_NAME;
    private String UNIDAD;
    private Long SUGGESTED_PRICE;
    private String BRAND;



    public CodeName(Long ID_CODE, String CODE, String PRODUCT_NAME, String UNIDAD, Long SUGGESTED_PRICE, String BRAND) {
        this.ID_CODE = ID_CODE;
        this.CODE = CODE;
        this.PRODUCT_NAME = PRODUCT_NAME;
        this.UNIDAD = UNIDAD;
        this.SUGGESTED_PRICE = SUGGESTED_PRICE;
        this.BRAND = BRAND;
    }

    public Long getID_CODE() {
        return ID_CODE;
    }

    public void setID_CODE(Long ID_CODE) {
        this.ID_CODE = ID_CODE;
    }

    //------------------------------------------------------------------------------

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    //------------------------------------------------------------------------------

    public String getPRODUCT_NAME() {
        return PRODUCT_NAME;
    }

    public void setPRODUCT_NAME(String PRODUCT_NAME) { this.PRODUCT_NAME = PRODUCT_NAME; }

    //------------------------------------------------------------------------------

    public String getUNIDAD() {
        return UNIDAD;
    }

    public void setUNIDAD(String UNIDAD) { this.UNIDAD = UNIDAD; }

    //------------------------------------------------------------------------------

    public Long getSUGGESTED_PRICEE() {
        return SUGGESTED_PRICE;
    }

    public void setSUGGESTED_PRICE(Long SUGGESTED_PRICE) {
        this.SUGGESTED_PRICE = SUGGESTED_PRICE;
    }

    //------------------------------------------------------------------------------

    public String getBRAND() {
        return BRAND;
    }

    public void setBRAND(String BRAND) {
        this.BRAND = BRAND;
    }

    //------------------------------------------------------------------------------


}


