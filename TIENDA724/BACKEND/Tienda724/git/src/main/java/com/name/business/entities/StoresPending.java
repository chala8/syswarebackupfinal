package com.name.business.entities;

public class StoresPending {

    private String DESCRIPTION;
    private Long ID_STORE_CLIENT;



    public StoresPending(String DESCRIPTION, Long ID_STORE_CLIENT) {
        this.DESCRIPTION = DESCRIPTION;
        this.ID_STORE_CLIENT = ID_STORE_CLIENT;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public Long getID_STORE_CLIENT() {
        return ID_STORE_CLIENT;
    }

    public void setID_STORE_CLIENT(Long ID_STORE_CLIENT) {
        this.ID_STORE_CLIENT = ID_STORE_CLIENT;
    }


}
