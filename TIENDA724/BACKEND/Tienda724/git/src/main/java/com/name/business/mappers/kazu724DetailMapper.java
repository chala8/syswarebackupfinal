package com.name.business.mappers;


import com.name.business.entities.ItemCaja;
import com.name.business.entities.kazu724Detail;
import com.name.business.entities.kazu724Document;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class kazu724DetailMapper implements ResultSetMapper<kazu724Detail> {

    @Override
    public kazu724Detail map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new kazu724Detail(
                resultSet.getLong("ID_DOCUMENT_DETAIL"),
                resultSet.getLong("CODIGO_CUENTA"),
                resultSet.getString("DESCRIPCION"),
                resultSet.getLong("VALOR"),
                resultSet.getString("NATURALEZA"),
                resultSet.getString("NOTAS"),
                resultSet.getLong("ID_THIRD"),
                resultSet.getString("FULLNAME")
        );
    }
}
