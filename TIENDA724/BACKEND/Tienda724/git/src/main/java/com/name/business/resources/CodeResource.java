package com.name.business.resources;


import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.CategoryBusiness;
import com.name.business.businesses.CodeBusiness;
import com.name.business.entities.Category;
import com.name.business.entities.Code;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.completes.CodeComplete;
import com.name.business.representations.CategoryDTO;
import com.name.business.representations.CodeDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/codes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CodeResource {

    private CodeBusiness codeBusiness;

    public CodeResource(CodeBusiness codeBusiness) {
        this.codeBusiness = codeBusiness;
    }

    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCategories(@QueryParam("id_code") Long id_code,
                                  @QueryParam("code") String code,@QueryParam("img") String img,
                                  @QueryParam("suggested_price") Double suggested_price,@QueryParam("id_third_cod") Long id_third_cod,
                                  @QueryParam("id_product") Long id_product,
                                  @QueryParam("id_measure_unit") Long id_measure_unit,
                                  @QueryParam("id_attribute_list") Long id_attribute_list,
                                  @QueryParam("id_state") Long id_state, @QueryParam("state") Integer state,
                                  @QueryParam("creation_code") Date creation_code,
                                  @QueryParam("modify_code") Date modify_code) {
        Response response;

        Either<IException, List<Code>> getCategoriesEither = codeBusiness.
                getCodes(

                        new Code(id_code,code,img,suggested_price,id_third_cod,id_product,id_measure_unit,id_attribute_list,
                                new CommonState(id_state, state,
                                        creation_code, modify_code)
                        )
                );
        if (getCategoriesEither.isRight()) {
            //System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }
    @GET
    @Path("/complete")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCodesComplete(@QueryParam("id_code") Long id_code,
                                  @QueryParam("code") String code,@QueryParam("img") String img,
                                  @QueryParam("suggested_price") Double suggested_price,@QueryParam("id_third_cod") Long id_third_cod,
                                  @QueryParam("id_product") Long id_product,
                                  @QueryParam("id_measure_unit") Long id_measure_unit,
                                  @QueryParam("id_attribute_list") Long id_attribute_list,
                                  @QueryParam("id_state") Long id_state, @QueryParam("state") Integer state,
                                  @QueryParam("creation_code") Date creation_code,
                                  @QueryParam("modify_code") Date modify_code,
                                  @QueryParam("is_tree_category") Boolean isTreeCategory,
                                     @QueryParam("is_attribs") Boolean    is_attribs) {
        Response response;

        Either<IException, List<CodeComplete>> getCodesCompleteEither = codeBusiness.
                getCodesComplete(

                        new Code(id_code,code,img,suggested_price,id_third_cod,id_product,id_measure_unit,id_attribute_list,
                                new CommonState(id_state, state,
                                        creation_code, modify_code)
                        ),
                        isTreeCategory,is_attribs
                );
        if (getCodesCompleteEither.isRight()) {
            //System.out.println(getCodesCompleteEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCodesCompleteEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCodesCompleteEither);
        }
        return response;
    }



    /**
     *
     * @param categoryDTO
     * @return status
     *
     */
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postCategoryResource(CodeDTO categoryDTO){
        Response response;

        Either<IException, Long> mailEither = codeBusiness.createCode(categoryDTO);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/{id}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateCategoryResource(@PathParam("id") Long id_category,@QueryParam("code") String code, CodeDTO categoryDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = codeBusiness.updateCode(id_category,code, categoryDTO);

        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deletePersonResource(@PathParam("id") Long id_category, @QueryParam("code") String code){
        Response response;
        Either<IException, Long> allViewOffertsEither = codeBusiness.deleteCode(id_category,code);

        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

}


