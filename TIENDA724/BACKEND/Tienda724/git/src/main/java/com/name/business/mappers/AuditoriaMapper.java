package com.name.business.mappers;

import com.name.business.entities.Auditoria;
import com.name.business.entities.Brand;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AuditoriaMapper implements ResultSetMapper<Auditoria>{

    @Override
    public Auditoria map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Auditoria(
                resultSet.getString("FECHA"),
                resultSet.getString("EMPLEADO"),
                resultSet.getString("MESSAGE"),
                resultSet.getString("VALOR ANTERIOR"),
                resultSet.getString("VALOR CAMBIO")
        );
    }
}