package com.name.business.entities;

import java.util.Date;

public class Code {
    private Long id_code;
    private String code;
    private String img;
    private Double suggested_price;
    private Long id_third_cod;
    private Long id_product;
    private Long id_measure_unit;
    private Long id_attribute_list;


    private Long id_state_cod;
    private Integer state_cod;
    private Date creation_cod;
    private Date modify_cod;



    public Code(Long id_code, String code, String img, Double suggested_price, Long id_third_cod, Long id_product, Long id_measure_unit, Long id_attribute_list,
                CommonState state) {
        this.id_code = id_code;
        this.code = code;
        this.img = img;
        this.suggested_price = suggested_price;
        this.id_third_cod = id_third_cod;
        this.id_product = id_product;
        this.id_measure_unit = id_measure_unit;
        this.id_attribute_list = id_attribute_list;

        this.id_state_cod =state!=null?state.getId_common_state():null;
        this.state_cod= state!=null?state.getState():null;
        this.creation_cod = state!=null?state.getCreation_date():null;
        this.modify_cod = state!=null?state.getModify_date():null;
    }


    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_cod(),
                this.getState_cod(),
                this.getCreation_cod(),
                this.getModify_cod()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_cod=state.getId_common_state();
        this.state_cod=state.getState();
        this.modify_cod=state.getCreation_date();
        this.modify_cod=state.getModify_date();
    }

    public Double getSuggested_price() {
        return suggested_price;
    }

    public void setSuggested_price(Double suggested_price) {
        this.suggested_price = suggested_price;
    }

    public Long getId_third_cod() {
        return id_third_cod;
    }

    public void setId_third_cod(Long id_third_cod) {
        this.id_third_cod = id_third_cod;
    }

    public Long getId_state_cod() {
        return id_state_cod;
    }

    public void setId_state_cod(Long id_state_cod) {
        this.id_state_cod = id_state_cod;
    }

    public Integer getState_cod() {
        return state_cod;
    }

    public void setState_cod(Integer state_cod) {
        this.state_cod = state_cod;
    }

    public Date getCreation_cod() {
        return creation_cod;
    }

    public void setCreation_cod(Date creation_cod) {
        this.creation_cod = creation_cod;
    }

    public Date getModify_cod() {
        return modify_cod;
    }

    public void setModify_cod(Date modify_cod) {
        this.modify_cod = modify_cod;
    }



    public Long getId_code() {
        return id_code;
    }

    public void setId_code(Long id_code) {
        this.id_code = id_code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Long getId_product() {
        return id_product;
    }

    public void setId_product(Long id_product) {
        this.id_product = id_product;
    }

    public Long getId_measure_unit() {
        return id_measure_unit;
    }

    public void setId_measure_unit(Long id_measure_unit) {
        this.id_measure_unit = id_measure_unit;
    }

    public Long getId_attribute_list() {
        return id_attribute_list;
    }

    public void setId_attribute_list(Long id_attribute_list) {
        this.id_attribute_list = id_attribute_list;
    }
}
