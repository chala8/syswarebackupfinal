package com.name.business.entities;
import org.skife.jdbi.cglib.core.Local;

import java.time.LocalDate;

import java.util.Date;

public class DateReport {
    private Long subtotal;
    private Long tax;
    private Long total;
    private String labelDate;
    int counter;



    public DateReport(Long subtotal, Long tax, Long total) {
        this.subtotal = subtotal;
        this.tax = tax;
        this.total = total;
    }

    public Long getsubtotal() {
        return subtotal;
    }

    public void setsubtotal(Long subtotal) {
        this.subtotal = subtotal;
    }

    //------------------------------------------------------------------------------

    public Long gettax() {
        return tax;
    }

    public void settax(Long tax) {
        this.tax = tax;
    }

    //------------------------------------------------------------------------------

    public Long gettotal() {
        return total;
    }

    public void settotal(Long total) {
        this.total = total;
    }

    //------------------------------------------------------------------------------

    public String getlabelDate() {
        return labelDate;
    }

    public void setlabelDate(String labelDate) {
        this.labelDate = labelDate;
    }

    //------------------------------------------------------------------------------

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    //------------------------------------------------------------------------------



}


