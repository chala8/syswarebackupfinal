package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;
import java.util.List;

public class LogDetailDTO {


    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getFecha_EVENTO() {
        return fecha_EVENTO;
    }

    public void setFecha_EVENTO(String fecha_EVENTO) {
        this.fecha_EVENTO = fecha_EVENTO;
    }

    public Long getId_THIRD_USER() {
        return id_THIRD_USER;
    }

    public void setId_THIRD_USER(Long id_THIRD_USER) {
        this.id_THIRD_USER = id_THIRD_USER;
    }

    public Long getIdestadodestino() {
        return idestadodestino;
    }

    public void setIdestadodestino(Long idestadodestino) {
        this.idestadodestino = idestadodestino;
    }

    public Long getIdestadoorigen() {
        return idestadoorigen;
    }

    public void setIdestadoorigen(Long idestadoorigen) {
        this.idestadoorigen = idestadoorigen;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    private String actor;
    private String fecha_EVENTO;
    private Long id_THIRD_USER;
    private Long idestadodestino;
    private Long idestadoorigen;
    private String notas;


    @JsonCreator
    public LogDetailDTO(
                        @JsonProperty("actor") String actor,
                        @JsonProperty("fecha_EVENTO") String fecha_EVENTO,
                        @JsonProperty("id_THIRD_USER") Long id_THIRD_USER,
                        @JsonProperty("idestadodestino") Long idestadodestino,
                        @JsonProperty("idestadoorigen") Long idestadoorigen,
                        @JsonProperty("notas") String notas) {

        this.actor = actor;
        this.fecha_EVENTO = fecha_EVENTO;
        this.id_THIRD_USER = id_THIRD_USER;
        this.idestadodestino = idestadodestino;
        this.idestadoorigen = idestadoorigen;
        this.notas = notas;
    }



}
