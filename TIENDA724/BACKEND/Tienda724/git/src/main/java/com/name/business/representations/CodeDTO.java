package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CodeDTO {

    private String code;
    private String img;
    private Double suggested_price;
    private Long id_third_cod;
    private Long id_product;
    private Long id_measure_unit;
    private Long id_attribute_list;
    private CommonStateDTO stateDTO;


    @JsonCreator
    public CodeDTO(@JsonProperty("code") String code,@JsonProperty("img") String img,
                   @JsonProperty("suggested_price") Double suggested_price,
                   @JsonProperty("id_third_cod") Long id_third_cod,
                   @JsonProperty("id_product") Long id_product,
                   @JsonProperty("id_measure_unit") Long id_measure_unit,@JsonProperty("id_attribute_list") Long id_attribute_list,
                   @JsonProperty("state") CommonStateDTO stateDTO) {
        this.code = code;
        this.img = img;
        this.suggested_price=suggested_price;
        this.id_third_cod=id_third_cod;
        this.id_product = id_product;
        this.id_measure_unit = id_measure_unit;
        this.id_attribute_list = id_attribute_list;
        this.stateDTO = stateDTO;
    }

    public Double getSuggested_price() {
        return suggested_price;
    }

    public void setSuggested_price(Double suggested_price) {
        this.suggested_price = suggested_price;
    }

    public Long getId_third_cod() {
        return id_third_cod;
    }

    public void setId_third_cod(Long id_third_cod) {
        this.id_third_cod = id_third_cod;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Long getId_product() {
        return id_product;
    }

    public void setId_product(Long id_product) {
        this.id_product = id_product;
    }

    public Long getId_measure_unit() {
        return id_measure_unit;
    }

    public void setId_measure_unit(Long id_measure_unit) {
        this.id_measure_unit = id_measure_unit;
    }

    public Long getId_attribute_list() {
        return id_attribute_list;
    }

    public void setId_attribute_list(Long id_attribute_list) {
        this.id_attribute_list = id_attribute_list;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
