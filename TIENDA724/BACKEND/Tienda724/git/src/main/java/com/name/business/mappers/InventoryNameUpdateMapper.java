package com.name.business.mappers;


import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.InventoryName;
import com.name.business.entities.InventoryNameUpdate;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InventoryNameUpdateMapper implements ResultSetMapper<InventoryNameUpdate> {
    @Override
    public InventoryNameUpdate map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new InventoryNameUpdate(
                resultSet.getLong("ID_TAX"),
                resultSet.getDouble("STANDARD_PRICE"),
                resultSet.getLong("ID_PRODUCT_STORE"),
                resultSet.getString("PRODUCT_STORE_NAME"),
                resultSet.getString("CODE"),
                resultSet.getLong("ID_INVENTORY_DETAIL"),
                resultSet.getString("OWNBARCODE"),
                resultSet.getString("PRODUCT_STORE_CODE"),
                resultSet.getString("STATUS")
        );
    }
}
