package com.name.business.mappers;

import com.name.business.entities.Brand;
import com.name.business.entities.CajeroInfo;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CajeroInfoMapper implements ResultSetMapper<CajeroInfo>{

    @Override
    public CajeroInfo map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CajeroInfo(
                resultSet.getLong("CAJA_NUMBER"),
                resultSet.getString("DESCRIPTION")
        );
    }
}