package com.name.business.entities;

import java.util.List;

public class CategoriaNodo {

    private String LEVEL;
    private String category;
    private Long id_category;
    private Long id_category_father;
    private String category_father;
    private String CATEGORY_URL;
    private String CATEGORY_FATHER_URL;
    private List<CategoriaNodo> hijos;


    public CategoriaNodo(String LEVEL, String category, Long id_category, Long id_category_father, String category_father, String CATEGORY_URL, String CATEGORY_FATHER_URL) {
        this.LEVEL = LEVEL;
        this.category = category;
        this.id_category = id_category;
        this.id_category_father = id_category_father;
        this.category_father = category_father;
        this.CATEGORY_URL = CATEGORY_URL;
        this.CATEGORY_FATHER_URL = CATEGORY_FATHER_URL;
    }

    public List<CategoriaNodo> getHijos() {
        return hijos;
    }

    public void setHijos(List<CategoriaNodo> hijos) {
        this.hijos = hijos;
    }

    public String getLEVEL() {
        return LEVEL;
    }

    public void setLEVEL(String LEVEL) {
        this.LEVEL = LEVEL;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getId_category() {
        return id_category;
    }

    public void setId_category(Long id_category) {
        this.id_category = id_category;
    }

    public Long getId_category_father() {
        return id_category_father;
    }

    public void setId_category_father(Long id_category_father) {
        this.id_category_father = id_category_father;
    }

    public String getCategory_father() {
        return category_father;
    }

    public void setCategory_father(String category_father) {
        this.category_father = category_father;
    }

    public String getCATEGORY_URL() {
        return CATEGORY_URL;
    }

    public void setCATEGORY_URL(String CATEGORY_URL) {
        this.CATEGORY_URL = CATEGORY_URL;
    }

    public String getCATEGORY_FATHER_URL() {
        return CATEGORY_FATHER_URL;
    }

    public void setCATEGORY_FATHER_URL(String CATEGORY_FATHER_URL) {
        this.CATEGORY_FATHER_URL = CATEGORY_FATHER_URL;
    }
}
