package com.name.business.entities;

public class Phone {

    private Long ID_PHONE;
    private Long PHONE;
    private Long PRIORITY;



    public Phone(Long ID_PHONE, Long PHONE, Long PRIORITY) {
        this.ID_PHONE = ID_PHONE;
        this.PHONE = PHONE;
        this.PRIORITY =  PRIORITY;
    }

    public Long getID_PHONE() {
        return ID_PHONE;
    }

    public void setID_PHONE(Long ID_PHONE) {
        this.ID_PHONE = ID_PHONE;
    }

    public Long getPHONE() {
        return PHONE;
    }

    public void setPHONE(Long PHONE) {
        this.PHONE = PHONE;
    }

    public Long getPRIORITY() {
        return PRIORITY;
    }

    public void setPRIORITY(Long PRIORITY) {
        this.PRIORITY = PRIORITY;
    }


}
