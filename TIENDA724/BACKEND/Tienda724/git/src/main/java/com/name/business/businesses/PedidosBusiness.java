package com.name.business.businesses;

import com.itextpdf.text.*;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.*;
import com.name.business.DAOs.PedidosDAO;
import com.name.business.entities.*;
import com.name.business.representations.MeasureUnitDTO;
import com.name.business.representations.PlanillaDTO;
import com.name.business.representations.PlanillaDetailDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.name.business.sanitations.EntitySanitation.measureUnitSanitation;
import static com.name.business.utils.PDFHelpers.CellMaker;
import static com.name.business.utils.PDFHelpers.PaginarPDF;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static java.lang.Math.round;

public class PedidosBusiness {
    private PedidosDAO pedidosDAO;
    private boolean PruebasLocales = false;

    public PedidosBusiness(PedidosDAO pedidosDAO) {
        this.pedidosDAO = pedidosDAO;
    }

    public Either<IException, String > getOwnBarCode(Long id_product_store) {
        try {
            String validator = pedidosDAO.getOwnBarCode(id_product_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Vehiculo> > getVehiculos() {
        try {
            List<Vehiculo> validator = pedidosDAO.getVehiculos();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Long> > getVehiculosids() {
        try {
            List<Long> validator = pedidosDAO.getVehiculosIds();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long > getPsId(String code, Long id_store) {
        try {
            Long validator = pedidosDAO.getPsId(code,id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Integer> updatePlanilla(String observaciones, Long idplanilla) {
        try {
            //System.out.println("----a-----");
            pedidosDAO.updatePlanilla(observaciones, idplanilla);
            //System.out.println("----b-----");
            return Either.right(1);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Integer> updateUmbral(Long pctumbral,
                                                   String barcodehijo,
                                                   Long idstorehijo) {
        try {
            //System.out.println("----a-----");
            pedidosDAO.updateUmbral(pctumbral, barcodehijo, idstorehijo);
            //System.out.println("----b-----");
            return Either.right(1);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

/*
    public Either<IException, List<Planilla> > getPlanillas(Long idvehiculo,
                                                            Long idstore) {
        try {
            List<Planilla> validator = pedidosDAO.getPlanillas(idvehiculo,idstore);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }
*/

    public Either<IException, List<Planilla> > getPlanillasV2(List<String> idvehiculo,
                                                              List<String> status,
                                                              Long idstore,
                                                              String Date1,
                                                              String Date2) {
        try {
            List<Planilla> validator = pedidosDAO.getPlanillasV2(idvehiculo,status,idstore,new Date(Date1), new Date(Date2));
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Asociacion> > getAsociaciones(Long idstorep, Long idstoreh) {
        try {
            List<Asociacion> validator = pedidosDAO.getAsociaciones(idstorep, idstoreh);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<VendedorDistribuidor> > getVendedoresDistribuidor(Long id_store_cliente,
                                                                                     Long id_app) {
        try {
            List<VendedorDistribuidor> validator = pedidosDAO.getVendedoresDistribuidor(id_store_cliente,
                    id_app);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Long> > getIdStores(Long id_third) {
        try {
            List<Long> validator = pedidosDAO.getIdStores(id_third);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<DetallePlanilla> > getDetallesPlanillas(Long idplanilla) {
        try {
            List<DetallePlanilla> validator = pedidosDAO.getDetallesPlanillas(idplanilla);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<PlanillaDetail> > getPlanillaDetail(Long idplanilla) {
        try {
            List<PlanillaDetail> validator = pedidosDAO.getPlanillaDetail(idplanilla);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postProcedure(Long idbill,
                                                     Long idbillstate,
                                                     Long code,
                                                     Long idstore,
                                                     Long cantidad,
                                                     String tipo) {
        try {
            pedidosDAO.procedure(idbill,
                     idbillstate,
                     code,
                     idstore,
                     cantidad,
                     tipo);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }






    public Either<IException, Long > actualizar_inventario_a_cero(
                                                     Long idstore) {
        try {
            pedidosDAO.actualizar_inventario_a_cero(
                    idstore);
            return Either.right(new Long (1));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }





    public Either<IException, Long > agregar_conversion(String barcodehijo,
                                                                  String barcodepadre,
                                                                  Long factor,
                                                                  Long prioridad) {
        try {
            pedidosDAO.agregar_conversion(barcodehijo,
                     barcodepadre,
                     factor,
                     prioridad);
            return Either.right(new Long (1));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, Long > eliminar_asociacion(String barcodehijo,
                                                        String barcodepadre,
                                                        Long idstorecliente,
                                                        Long idstoreprov) {
        try {
            pedidosDAO.eliminar_asociacion(barcodehijo,
                    barcodepadre,
                    idstorecliente,
                    idstoreprov);
            return Either.right(new Long (1));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }






    public Either<IException, Long > insertNewPlanilla(Long idvehiculo,
                                                     Long idstore) {
        try {
            pedidosDAO.insertNewPlanilla(idvehiculo,
                    idstore);
            return Either.right(new Long(1));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String> postpdf(PlanillaDTO master, List<PlanillaDetailDTO> detalles, String logo){
        Document documento = new Document(PageSize.A4, 0f, 0f, 30f, 30f);
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";
        String ruta = "";
        FileOutputStream ficheroPdf = null;
        try{
            //DEFINO LAS FUENTES
            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);

            Font fontH1 = new Font(HELVETICA, 12, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 12, Font.BOLD);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);

            //DEFINO RUTAS
            String nombrePdf = master.getnum_PLANILLA()+"_planilla.pdf_tmp.pdf";
            if(PruebasLocales){
                ruta = new File(".").getCanonicalPath()+"/"+ nombrePdf;
            }else{
                ruta = "/usr/share/nginx/html/planillas/"+ nombrePdf;
            }
            ficheroPdf = new FileOutputStream(ruta);

            //INICIALIZO EL DOCUMENTO
            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));

            //INGRESO EL LOGO
            try {
                Image img;
                if(PruebasLocales){
                    img = Image.getInstance(new File(".").getCanonicalPath()+"/"+logo +".jpg");
                }else{
                    img = Image.getInstance("/usr/share/nginx/html/logos/" + logo + ".jpg");
                }
                img.scalePercent(50);
                Rectangle pageSize = documento.getPageSize();
                img.setAbsolutePosition(pageSize.getRight(0) - (img.getWidth()/2), pageSize.getTop(0) - (img.getHeight()/2));
                //img.setAbsolutePosition(500f, 500f);
                documento.add(img);
            } catch (Exception e) {
                //System.out.println(e.toString());
            }

            //CELDAS BLANCAS
            PdfPCell cellblanks =new PdfPCell(new Phrase(" "));
            cellblanks.setBorder(PdfPCell.NO_BORDER);
            cellblanks.setHorizontalAlignment(Element.ALIGN_CENTER);

            //TABLA CON ENCABEZADO DEL DOCUMENTO
            float AnchoTablas = 90;
            PdfPTable tablaDatos = new PdfPTable(1);
            tablaDatos.setWidthPercentage(AnchoTablas);


            //Linea
            CellMaker("Documento de Planilla para: "+master.getnum_PLANILLA(),fontH3,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos);
            tablaDatos.addCell(cellblanks);
            tablaDatos.addCell(cellblanks);


            //DEFINO LA TABLA ENCIMA DE DETALLES
            float[] AnchosTablaSuperior = new float[]{1.1f,1.1f,2,0.8f};
            PdfPTable tablaMaster = new PdfPTable(4);
            tablaMaster.setWidthPercentage(AnchoTablas);
            tablaMaster.setWidths(AnchosTablaSuperior);

            //TITULOS COLUMAS
            //PLACA
            CellMaker("Placa",fontH2,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaMaster);

            //NUM PLANILLA
            CellMaker("N° Planilla",fontH2,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaMaster);

            //FECHA
            CellMaker("Fecha de Apertura de Planilla",fontH2,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaMaster);
            tablaMaster.addCell(cellblanks);

            //DATO PLACA
            CellMaker(master.getplaca(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaMaster);

            //DATO NUM PLANILLA
            CellMaker(master.getnum_PLANILLA(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaMaster);

            //DATO FECHA
            CellMaker(master.getfecha_INICIO(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaMaster);
            tablaMaster.addCell(cellblanks);

            //TABLA QUE CONTIENE TITULO DETALLES
            PdfPTable tablaDatos2= new PdfPTable(1);
            tablaDatos2.setWidthPercentage(AnchoTablas);

            tablaDatos2.addCell(cellblanks);

            //TITULO DETALLES
            CellMaker("DETALLES",fontH2,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDatos2);

            //DEFINO LA TABLA PRINCIPAL
            float[] AnchosTabla = new float[]{1.4f,2f,2f,1f};
            PdfPTable tablaDetails = new PdfPTable(4);
            tablaDetails.setWidthPercentage(AnchoTablas);
            tablaDetails.setWidths(AnchosTabla);

            //TITULOS COLUMNAS
            float Padding = 5f;
            //NUM DOCUMENTO
            CellMaker("N° Documento",fontH2,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,Padding);

            //TIENDA
            CellMaker("Tienda",fontH2,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,Padding);

            //DIRECCION
            CellMaker("Direccion",fontH2,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,Padding);

            //VALOR
            CellMaker("Valor",fontH2,PdfPCell.BOTTOM,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,Padding);

            boolean LeTocaGris = false;
            int R = 180;
            int G = 180;
            int B = 180;
            double Total = 0;
            //CICLO PARA LLENAR TODOS LOS DATOS
            for( PlanillaDetailDTO element : detalles ){

                Font fontH1TMP = new Font(fontH1.getBaseFont(), fontH1.getSize(), fontH1.getStyle());

                if(LeTocaGris){ fontH1TMP.setColor(BaseColor.WHITE); }
                else{ fontH1TMP.setColor(BaseColor.BLACK); }

                //NUMERO DOCUMENTO
                CellMaker(element.getnum_DOCUMENTO(),fontH1TMP,PdfPCell.RIGHT,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,LeTocaGris ? new BaseColor(R,G,B):BaseColor.WHITE,Padding);

                //TIENDA
                CellMaker(element.getstore(),fontH1TMP,PdfPCell.RIGHT,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,LeTocaGris ? new BaseColor(R,G,B):BaseColor.WHITE,Padding);

                //DIRECCIÓN
                CellMaker(element.getaddress()+"",fontH1TMP,PdfPCell.RIGHT,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,LeTocaGris ? new BaseColor(R,G,B):BaseColor.WHITE,Padding);

                //VALOR
                CellMaker(format.format(roundAvoid(Double.parseDouble(element.getvalor()),2))+"",fontH1TMP,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_MIDDLE,tablaDetails,LeTocaGris ? new BaseColor(R,G,B):BaseColor.WHITE,Padding);

                Total += roundAvoid(Double.parseDouble(element.getvalor()),2);

                LeTocaGris = !LeTocaGris;
            }

            //TOTAL
            PdfPTable tablaTotales = new PdfPTable(4);
            tablaTotales.setWidthPercentage(AnchoTablas);
            tablaTotales.setWidths(new float[]{0.5f,3.1f,1,1});

            CellMaker("",fontH2,PdfPCell.TOP,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales); CellMaker("",fontH2,PdfPCell.TOP,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales); CellMaker("",fontH2,PdfPCell.TOP,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales); CellMaker("",fontH2,PdfPCell.TOP,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks);
            tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks);
            tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks);

            //VALOR TOTAL
            CellMaker("",fontH2,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker("",fontH2,PdfPCell.BOTTOM,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker("Total",fontH2,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            CellMaker(format.format(Total)+"",fontH2,PdfPCell.NO_BORDER,Element.ALIGN_RIGHT,Element.ALIGN_MIDDLE,tablaTotales);
            //
            tablaTotales.addCell(cellblanks);
            CellMaker(master.getconductor(),fontH1,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablaTotales);
            tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks);
            //
            tablaTotales.addCell(cellblanks);
            CellMaker("Conductor",fontH2,PdfPCell.NO_BORDER,Element.ALIGN_CENTER,Element.ALIGN_TOP,tablaTotales);
            tablaTotales.addCell(cellblanks); tablaTotales.addCell(cellblanks);


            tablaTotales.setKeepTogether(false);
            tablaTotales.setSplitLate(false);

            PdfPCell tablaTotalesC = new PdfPCell(tablaTotales);
            tablaTotalesC.setBorder(PdfPCell.NO_BORDER);

            //PARAMETROS PARA INSERTAR EN EL DOCUMENTO LO GENERADO PREVIAMENTE
            PdfPTable table = new PdfPTable(1);
            table.setWidthPercentage(AnchoTablas);
            tablaDatos.setKeepTogether(false);
            tablaDatos.setSplitLate(false);
            PdfPCell hed = new PdfPCell(tablaDatos);
            hed.setBorder(PdfPCell.NO_BORDER);
            hed.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed);

            tablaMaster.setSplitLate(false);
            tablaMaster.setKeepTogether(false);
            PdfPCell hed3 = new PdfPCell(tablaMaster);
            hed3.setBorder(PdfPCell.NO_BORDER);
            hed3.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed3);

            tablaDatos2.setSplitLate(false);
            tablaDatos2.setKeepTogether(false);
            PdfPCell hed32 = new PdfPCell(tablaDatos2);
            hed32.setBorder(PdfPCell.NO_BORDER);
            hed32.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed32);

            tablaDetails.setSplitLate(false);
            tablaDetails.setKeepTogether(false);
            PdfPCell hed322 = new PdfPCell(tablaDetails);
            hed322.setBorder(PdfPCell.NO_BORDER);
            hed322.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed322);

            table.setKeepTogether(false);
            table.setSplitLate(false);

            table.addCell(tablaTotalesC);

            documento.add(table);

            response = nombrePdf.substring(0,nombrePdf.length()-8);
        }catch(Exception e){
            response = e.toString();
        }
        try {
            documento.close();
            ficheroPdf.close();
            PaginarPDF(ruta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }

    }

    public Either<IException, List<Pedido>> getPedidos(Long id_store, Long id_bill_state, Long id_bill_type) {
        try {
            List<Pedido> validator = pedidosDAO.getPedidos(id_store,id_bill_state,id_bill_type);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<DetallePedido>> getDetallesPedidos(List<Long> id_store) {
        try {
            List<DetallePedido> validator = pedidosDAO.getDetallesPedidos(id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }
}
