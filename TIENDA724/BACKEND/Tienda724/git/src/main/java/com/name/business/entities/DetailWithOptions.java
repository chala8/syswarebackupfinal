package com.name.business.entities;

public class DetailWithOptions {

    private Long id_product_store;
    private String producto;
    private String notas;
    private Long id_bill_state;
    private String estado;

    public DetailWithOptions(Long id_product_store, String producto, String notas, Long id_bill_state, String estado) {
        this.id_product_store = id_product_store;
        this.producto = producto;
        this.notas = notas;
        this.id_bill_state = id_bill_state;
        this.estado = estado;
    }

    public Long getId_product_store() {
        return id_product_store;
    }

    public void setId_product_store(Long id_product_store) {
        this.id_product_store = id_product_store;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public Long getId_bill_state() {
        return id_bill_state;
    }

    public void setId_bill_state(Long id_bill_state) {
        this.id_bill_state = id_bill_state;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
