package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;
import java.util.List;

public class KardexLogDTO {


    public Long getCantidad_MOVIMIENTO() {
        return cantidad_MOVIMIENTO;
    }

    public void setCantidad_MOVIMIENTO(Long cantidad_MOVIMIENTO) {
        this.cantidad_MOVIMIENTO = cantidad_MOVIMIENTO;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getFecha_MOVIMIENTO() {
        return fecha_MOVIMIENTO;
    }

    public void setFecha_MOVIMIENTO(String fecha_MOVIMIENTO) {
        this.fecha_MOVIMIENTO = fecha_MOVIMIENTO;
    }

    public Long getId_BILL() {
        return id_BILL;
    }

    public void setId_BILL(Long id_BILL) {
        this.id_BILL = id_BILL;
    }

    public Long getId_PRODUCT_STORE() {
        return id_PRODUCT_STORE;
    }

    public void setId_PRODUCT_STORE(Long id_PRODUCT_STORE) {
        this.id_PRODUCT_STORE = id_PRODUCT_STORE;
    }

    public Long getInventario_FINAL() {
        return inventario_FINAL;
    }

    public void setInventario_FINAL(Long inventario_FINAL) {
        this.inventario_FINAL = inventario_FINAL;
    }

    public Long getInventario_INICIAL() {
        return inventario_INICIAL;
    }

    public void setInventario_INICIAL(Long inventario_INICIAL) {
        this.inventario_INICIAL = inventario_INICIAL;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getTipo_MOVIMIENTO() {
        return tipo_MOVIMIENTO;
    }

    public void setTipo_MOVIMIENTO(String tipo_MOVIMIENTO) {
        this.tipo_MOVIMIENTO = tipo_MOVIMIENTO;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    private Long cantidad_MOVIMIENTO;
    private String documento;
    private String fecha_MOVIMIENTO;
    private Long id_BILL;
    private Long id_PRODUCT_STORE;
    private Long inventario_FINAL;
    private Long inventario_INICIAL;
    private String producto;
    private String tipo_MOVIMIENTO;
    private String usuario;


    @JsonCreator
    public KardexLogDTO(@JsonProperty("cantidad_MOVIMIENTO") Long cantidad_MOVIMIENTO,
                     @JsonProperty("documento") String documento,
                     @JsonProperty("fecha_MOVIMIENTO") String fecha_MOVIMIENTO,
                     @JsonProperty("id_BILL") Long id_BILL,
                     @JsonProperty("id_PRODUCT_STORE") Long id_PRODUCT_STORE,
                     @JsonProperty("inventario_FINAL") Long inventario_FINAL,
                     @JsonProperty("inventario_INICIAL") Long inventario_INICIAL,
                     @JsonProperty("producto") String producto,
                     @JsonProperty("tipo_MOVIMIENTO") String tipo_MOVIMIENTO,
                     @JsonProperty("usuario") String usuario) {

        this.cantidad_MOVIMIENTO = cantidad_MOVIMIENTO;
        this.documento = documento;
        this.fecha_MOVIMIENTO = fecha_MOVIMIENTO;
        this.id_BILL = id_BILL;
        this.id_PRODUCT_STORE = id_PRODUCT_STORE;
        this.inventario_FINAL = inventario_FINAL;
        this.inventario_INICIAL = inventario_INICIAL;
        this.producto = producto;
        this.tipo_MOVIMIENTO = tipo_MOVIMIENTO;
        this.usuario = usuario;
    }



}
