package com.name.business.mappers;


import com.name.business.entities.Pedido;
import com.name.business.entities.DetallePedido;
import com.name.business.entities.PendingReport;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PendingReportMapper implements ResultSetMapper<PendingReport> {

    @Override
    public PendingReport map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PendingReport(

                resultSet.getString("OWNBARCODE"),
                resultSet.getString("PRODUCT_STORE_NAME"),
                resultSet.getLong("QUANTITY")
        );
    }

}
