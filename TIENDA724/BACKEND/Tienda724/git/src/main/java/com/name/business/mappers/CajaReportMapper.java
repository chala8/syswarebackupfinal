package com.name.business.mappers;

import com.name.business.entities.Brand;
import com.name.business.entities.CajaReport;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CajaReportMapper implements ResultSetMapper<CajaReport>{

    @Override
    public CajaReport map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CajaReport(
                resultSet.getLong("ID_CAJA"),
                resultSet.getString("DESCRIPTION")
        );
    }
}