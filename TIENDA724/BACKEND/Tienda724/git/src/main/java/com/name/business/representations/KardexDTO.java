package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;
import java.util.List;

public class KardexDTO {

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }

    public List<KardexLogDTO> getLog() {
        return log;
    }

    public void setLog(List<KardexLogDTO> log) {
        this.log = log;
    }

    private String categoria;
    private String linea;
    private String producto;
    private String date1;
    private String date2;
    private List<KardexLogDTO> log;


    @JsonCreator
    public KardexDTO(@JsonProperty("categoria") String categoria,
                      @JsonProperty("linea") String linea,
                      @JsonProperty("producto") String producto,
                      @JsonProperty("date1") String date1,
                      @JsonProperty("date2") String date2,
                      @JsonProperty("log") List<KardexLogDTO> log) {

        this.categoria = categoria;
        this.linea = linea;
        this.producto = producto;
        this.date1 = date1;
        this.date2 = date2;
        this.log = log;
    }



}
