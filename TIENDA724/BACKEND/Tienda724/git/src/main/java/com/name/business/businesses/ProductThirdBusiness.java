package com.name.business.businesses;

import com.name.business.DAOs.ProductThirdDAO;
import com.name.business.entities.*;
import com.name.business.entities.completes.CodeComplete;
import com.name.business.representations.ProductThirdDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import org.json.JSONObject;
import org.skife.jdbi.v2.sqlobject.Transaction;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.productThirdSanitation;
import static com.name.business.sanitations.EntitySanitation.productThirdSimpleSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class ProductThirdBusiness {

    private ProductThirdDAO productThirdDAO;
    private ProductBusiness productBusiness;
    private CommonBusiness commonBusiness;
    private CommonStateBusiness commonStateBusiness;
    private MeasureUnitBusiness measureUnitBusiness;
    private AttributeListBusiness attributeListBusiness;
    private CategoryBusiness categoryBusiness;
    private CodeBusiness codeBusiness;


    public ProductThirdBusiness(ProductThirdDAO productThirdDAO, ProductBusiness productBusiness, CommonBusiness commonBusiness,
                                CommonStateBusiness commonStateBusiness, MeasureUnitBusiness measureUnitBusiness, AttributeListBusiness attributeListBusiness, CategoryBusiness categoryBusiness, CodeBusiness codeBusiness) {
        this.productThirdDAO = productThirdDAO;
        this.productBusiness = productBusiness;
        this.commonBusiness = commonBusiness;
        this.commonStateBusiness = commonStateBusiness;
        this.measureUnitBusiness = measureUnitBusiness;
        this.attributeListBusiness = attributeListBusiness;
        this.categoryBusiness = categoryBusiness;
        this.codeBusiness = codeBusiness;
    }

    @Transaction
    public Either<IException, Long> createProductThird(ProductThirdDTO productThirdDTO,Long id_code,String code) {
        List<String> msn = new ArrayList<>();
        Long id_product = null;
        Long id_common_state = null;
        Long id_code_new=null;
        Either<IException, List<Code>> codes=null;
        Code currentCode=null;

        try {

            //System.out.println("|||||||||||| Starting creation Product Third  ||||||||||||||||| ");
            if (productThirdDTO!=null){



                if ((id_code!=null &&  id_code>0) || (code!=null &&  !code.isEmpty())){

                    if ((code!=null &&  !code.isEmpty())){
                    codes=codeBusiness.getCodes(new Code(id_code,code,null,null,null,null,null,null,
                            new CommonState(null,null,null,null)));


                        if (codes.isRight()){
                            if(codes.right().value().size()>0){
                                currentCode=codes.right().value().get(0);
                                id_code_new=currentCode.getId_code();
                            }else{
                                //msn.add("You have problems with the barcode. Please verify it ");
                                return Either.left(new BussinessException(messages_errors(msn)));
                            }
                        }else{
                            return Either.left(codes.left().value());
                        }
                    }else{
                        id_code_new=id_code;
                    }



                }else if(productThirdDTO.getCodeDTO()!=null){
                    Either<IException, Long> codeBusinessCode = codeBusiness.createCode(productThirdDTO.getCodeDTO());
                    if (codeBusinessCode.isRight()){
                        if(codeBusinessCode.right().value()>0){
                            id_code_new=codeBusinessCode.right().value();
                        }else{
                            //msn.add("You have problems with the barcode. Please verify it ");
                            return Either.left(new BussinessException(messages_errors(msn)));
                        }

                    }else{
                        return Either.left(codeBusinessCode.left().value());
                    }
                }else{
                    //msn.add("You have problems with the barcode. Please verify it ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }



                if (productThirdDTO.getId_category_third()!=null && categoryBusiness.validatorID(productThirdDTO.getId_category_third()).equals(false)){
                    //msn.add("It ID Category does not exist register on  Mea, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                if (productThirdDTO.getStateDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(productThirdDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }

                productThirdDAO.create(productThirdDTO,formatoLongSql(id_common_state),formatoLongSql(id_code_new));
                id_product = productThirdDAO.getPkLast();

                return Either.right(id_product);
            }else{
                //msn.add("It does not recognize Product, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = productThirdDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    public Either<IException, List<ProductThirdSimple>> getProductThirdSimple(ProductThirdSimple productThirdSimple) {
        List<String> msn = new ArrayList<>();
        try {

            Either<IException, List<Code>> codes = codeBusiness.getCodes(new Code(null, null, null,null,null, null, null, null,
                    new CommonState(
                            null, null, null, null
                    )));

            List<Code> codeList = codes.right().value();
            //System.out.println(codeList.size());

            for (Code code: codeList) {
                //System.out.println((JSONObject.wrap(code)));
            }

            //System.out.println("|||||||||||| Starting consults Product Third Simple ||||||||||||||||| ");
            ProductThirdSimple productThirdSimpleSanitation = productThirdSimpleSanitation(productThirdSimple);
            return Either.right(productThirdDAO.readSimple(productThirdSimpleSanitation, productThirdSimpleSanitation.getMeasure_unit()));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<ProductThird>> getProductThird(ProductThird productThird, Boolean is_code_complete) {
        List<String> msn = new ArrayList<>();
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting consults Product Third ||||||||||||||||| ");
            ProductThird productThirdSanitation = productThirdSanitation(productThird);
            List<ProductThird> productThirdList = productThirdDAO.read(productThirdSanitation.getDescription(), productThirdSanitation.getDescription().getMeasure_unit(),
                    productThirdSanitation.getProduct());

            if(formatoBooleanQP(is_code_complete)){
                for (int i = 0; i < productThirdList.size(); i++) {
                    Code code = (Code) productThirdList.get(i).getCode();
                    Either<IException, List<CodeComplete>> codesCompleteEither = codeBusiness.getCodesComplete(
                            new Code(code.getId_code(),null,null,null,null,
                                    null,null,null,
                                    new CommonState(null,null,null,null)),
                            false, true);
                    if (codesCompleteEither.isRight()){
                        productThirdList.get(i).setCode(codesCompleteEither.right().value());
                    }

                }
            }
            return Either.right(productThirdList);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * @param id_product_third
     * @return
     */
    public Either<IException, Long> deleteProductThird(Long id_product_third) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting delete Attribute  List ||||||||||||||||| ");
            if (id_product_third != null && id_product_third > 0) {
                List<CommonSimple> readCommons = productThirdDAO.readCommons(formatoLongSql(id_product_third));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common
                return Either.right(id_product_third);
            } else {
                //msn.add("It does not recognize ID Attribute List, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_product_third
     * @param productThirdDTO
     * @return
     */
    public Either<IException, Long> updateProductThird(Long id_product_third,String code_param, ProductThirdDTO productThirdDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        ProductThirdSimple actualProductThirdSimple =null;

        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting update Product ||||||||||||||||| ");
            if ((id_product_third != null && id_product_third > 0)) {

                //TODO validate the  id attribute if exist on database a register with this id


                if (productThirdDTO.getId_category_third()!=null && categoryBusiness.validatorID(productThirdDTO.getId_category_third()).equals(false)){
                    //msn.add("It ID Category does not exist register on  Category, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                ProductThirdSimple productThirdSimple = new ProductThirdSimple(id_product_third, null, null, null, null,null,null,null,
                        new CommonState(null, null, null, null),
                        new MeasureUnit(null, null,
                                new Common(null, null, null),
                                new CommonState(null, null, null, null)
                        ), null, null
                );
                List<ProductThirdSimple> read = productThirdDAO.readSimple(productThirdSimple, productThirdSimple.getMeasure_unit());

                if (read.size()<=0){
                    //msn.add("It does not recognize ID Product third, probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = productThirdDAO.readCommons(formatoLongSql(id_product_third));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }

                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), productThirdDTO.getStateDTO());

                actualProductThirdSimple = read.get(0);


               if(productThirdDTO.getCodeDTO()!=null){
                   codeBusiness.updateCode(actualProductThirdSimple.getId_code(),formatoStringSql(code_param),productThirdDTO.getCodeDTO());
               }

                // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                if (productThirdDTO.getId_third() == null || productThirdDTO.getId_third()<1) {
                    productThirdDTO.setId_third(actualProductThirdSimple.getId_third());
                } else {
                    productThirdDTO.setId_third(formatoLongSql(productThirdDTO.getId_third()));
                }

                if (productThirdDTO.getStandard_price() == null) {
                    productThirdDTO.setStandard_price(actualProductThirdSimple.getStandard_price());
                } else {
                    productThirdDTO.setStandard_price(formatoDoubleSql(productThirdDTO.getStandard_price()));
                }
                if (productThirdDTO.getMin_price() == null) {
                    productThirdDTO.setMin_price(actualProductThirdSimple.getMin_price());
                } else {
                    productThirdDTO.setMin_price(formatoDoubleSql(productThirdDTO.getMin_price()));
                }

                // Attribute update
                productThirdDAO.update(id_product_third,actualProductThirdSimple.getId_code(),productThirdDTO);

                return Either.right(id_product_third);

            } else {
                //msn.add("It does not recognize ID Code, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

}
