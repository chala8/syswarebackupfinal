package com.name.business.mappers;

import com.name.business.entities.Store;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.StoresPending;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StoresPendingMapper implements ResultSetMapper<StoresPending>{

    @Override
    public StoresPending map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new StoresPending(
                resultSet.getString("DESCRIPTION"),
                resultSet.getLong("ID_STORE_CLIENT")

        );
    }
}