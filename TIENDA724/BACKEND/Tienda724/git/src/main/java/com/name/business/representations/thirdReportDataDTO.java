package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;

public class thirdReportDataDTO {


    private String document_NUMBER;
    private String fullname;
    private String mail;
    private Long numventas;
    private Long totalventas;
    private String phone;
    private String document_TYPE;
    private String address;

    @JsonCreator
    public thirdReportDataDTO(@JsonProperty("document_NUMBER") String document_NUMBER,
                              @JsonProperty("fullname") String fullname,
                              @JsonProperty("mail") String mail,
                              @JsonProperty("numventas") Long numventas,
                              @JsonProperty("totalventas") Long totalventas,
                              @JsonProperty("phone") String phone,
                              @JsonProperty("document_TYPE") String document_TYPE,
                              @JsonProperty("address") String address) {
        this.document_NUMBER = document_NUMBER;
        this.fullname = fullname;
        this.mail = mail;
        this.numventas = numventas;
        this.totalventas = totalventas;
        this.phone = phone;
        this.document_TYPE = document_TYPE;
        this.address = address;
    }

    //------------------------------------------------------------------------------------------------

    public Long getnumventas() {
        return numventas;
    }

    public void setnumventas(Long numventas) {
        this.numventas = numventas;
    }

    //------------------------------------------------------------------------------------------------

    public Long gettotalventas() {
        return totalventas;
    }

    public void settotalventas(Long totalventas) {
        this.totalventas = totalventas;
    }

    //------------------------------------------------------------------------------------------------

    public String getdocument_NUMBER() {
        return document_NUMBER;
    }

    public void setdocument_NUMBER(String document_NUMBER) { this.document_NUMBER = document_NUMBER; }

    //------------------------------------------------------------------------------------------------

    public String getfullname() {
        return fullname;
    }

    public void setfullname(String fullname) {
        this.fullname = fullname;
    }

    //------------------------------------------------------------------------------------------------

    public String getmail() {
        return mail;
    }

    public void setmail(String mail) {
        this.mail = mail;
    }

    //------------------------------------------------------------------------------------------------

    public String getphone() {
        return phone;
    }

    public void setphone(String phone) {
        this.phone = phone;
    }

    //------------------------------------------------------------------------------------------------

    public String getdocument_TYPE() {
        return document_TYPE;
    }

    public void setdocument_TYPE(String document_TYPE) {
        this.document_TYPE = document_TYPE;
    }

    //------------------------------------------------------------------------------------------------

    public String getaddress() {
        return address;
    }

    public void setaddress(String address) {
        this.address = address;
    }

    //------------------------------------------------------------------------------------------------

}
