package com.name.business.DAOs;

import com.name.business.entities.CommonSimple;
import com.name.business.entities.TaxTariff;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.TaxTariffMapper;
import com.name.business.representations.TaxTariffDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;


@RegisterMapper(TaxTariffMapper.class)
public interface TaxTariffDAO {

    @SqlQuery("SELECT ID_TAX_TARIFF FROM TAX_TARIFF WHERE ID_TAX_TARIFF IN (SELECT MAX(ID_TAX_TARIFF) FROM TAX_TARIFF)")
    Long getPkLast();

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_TAX_TARIFF ID,ID_COMMON, ID_COMMON_STATE FROM TAX_TARIFF " +
            "WHERE (ID_TAX_TARIFF= :id_tax_tariff OR :id_tax_tariff IS NULL)")
    List<CommonSimple> readCommons(@Bind("id_tax_tariff") Long id_tax_tariff);

    @SqlQuery("SELECT * FROM V_TAX_TARIFF V_TAX\n" +
            "WHERE\n" +
            "  ( V_TAX.ID_TAX_TARIFF =:tax.id_tax_tariff OR :tax.id_tax_tariff IS NULL) AND\n" +
            "  ( V_TAX.PERCENT =:tax.percent OR :tax.percent IS NULL) AND\n" +
            "  ( V_TAX.ID_COMMON =:tax.id_common_tax OR :tax.id_common_tax IS NULL) AND\n" +
            "  ( V_TAX.NAME LIKE :tax.name_tax OR :tax.name_tax IS NULL) AND\n" +
            "  ( V_TAX.DESCRIPTION LIKE :tax.description_tax OR :tax.description_tax IS NULL) AND\n" +
            "  ( V_TAX.ID_STATE =:tax.id_state_tax OR :tax.id_state_tax IS NULL) AND\n" +
            "  ( V_TAX.STATE =:tax.state_tax OR :tax.state_tax IS NULL) AND\n" +
            "  ( V_TAX.CREATION_TAX =:tax.creation_tax OR :tax.creation_tax IS NULL) AND\n" +
            "  ( V_TAX.MODIFY_TAX =:tax.modify_tax OR :tax.modify_tax IS NULL)")
    List<TaxTariff> read(@BindBean("tax")TaxTariff taxTariff );

    @SqlQuery("SELECT COUNT(ID_TAX_TARIFF) FROM TAX_TARIFF WHERE ID_TAX_TARIFF = :id_taxt_tariff")
    Integer getValidatorID(@Bind("id_taxt_tariff") Long id);


    @SqlUpdate("INSERT INTO TAX_TARIFF ( ID_COMMON,ID_COMMON_STATE,PERCENT) VALUES " +
            "(:id_common,:id_common_state,:tax.percent)")
    void create(@BindBean("tax") TaxTariffDTO taxTariffDTO, @Bind("id_common") Long id_common, @Bind("id_common_state") Long id_common_state);

    @SqlUpdate("UPDATE TAX_TARIFF SET PERCENT =:tax.percent\n"+
            "WHERE (ID_TAX_TARIFF =  :id_tax_tariff)")
        void update(@Bind("id_tax_tariff") Long id_tax_tariff, @BindBean("tax") TaxTariffDTO taxTariffDTO);

}
