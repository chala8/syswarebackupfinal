package com.name.business.mappers;

import com.name.business.entities.CodeName;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.ProductStoreName;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StoreXCodeMapper implements ResultSetMapper<ProductStoreName>{

    @Override
    public ProductStoreName map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ProductStoreName(

                resultSet.getLong("ID_STORE"),
                resultSet.getString("DESCRIPTION"),
                resultSet.getLong("STORE_NUMBER"),
                resultSet.getLong("ID_PRODUCT_STORE"),
                resultSet.getString("PRODUCT_STORE_NAME"),
                resultSet.getLong("PRODUCT_STORE_CODE"),
                resultSet.getString("OWNBARCODE"),
                resultSet.getLong("STANDARD_PRICE")

        );
    }
}