package com.name.business.mappers;

import com.name.business.entities.BalanceKazu;
import com.name.business.entities.Brand;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BalanceKazuMapper implements ResultSetMapper<BalanceKazu>{

    @Override
    public BalanceKazu map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BalanceKazu(
                resultSet.getLong("CODIGO_CUENTA"),
                resultSet.getString("DESCRIPCION"),
                resultSet.getDouble("VALOR"),
                resultSet.getLong("ID_STORE"),
                resultSet.getDouble("DEBITO"),
                resultSet.getDouble("CREDITO"),
                resultSet.getDouble("SALDO"),
                resultSet.getDouble("SALDO_INICIAL"),
                resultSet.getDouble("SALDO_TOTAL")
        );
    }
}