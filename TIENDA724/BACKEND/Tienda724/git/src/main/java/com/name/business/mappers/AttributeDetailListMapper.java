package com.name.business.mappers;

import com.name.business.entities.AttributeDetailList;
import com.name.business.entities.AttributeValue;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AttributeDetailListMapper implements ResultSetMapper<AttributeDetailList> {
    @Override
    public AttributeDetailList map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new AttributeDetailList(
                resultSet.getLong("ID_ATTRIBUTE_DETAIL_LIST"),
                resultSet.getLong("ID_ATTRIBUTE_LIST"),
                new CommonState(
                        resultSet.getLong("ID_STATE_ATT_L_DET"),
                        resultSet.getInt("STATE_ATT_L_DET"),
                        resultSet.getDate("CREATION_ATTRIBUTE_DETAIL_LIST"),
                        resultSet.getDate("MODIFY_ATTRIBUTE_DETAIL_LIST")
                ),
                new AttributeValue(
                        resultSet.getLong("ID_ATTRIBUTE_VALUE"),
                        resultSet.getLong("ID_ATTRIBUTE"),
                        new Common(
                                resultSet.getLong("ID_COMMON"),
                                resultSet.getString("NAME"),
                                resultSet.getString("DESCRIPTION")
                        ),
                        new CommonState(
                                resultSet.getLong("ID_STATE"),
                                resultSet.getInt("STATE"),
                                resultSet.getDate("CREATION_ATTRIBUTE_VALUE"),
                                resultSet.getDate("MODIFY_ATTRIBUTE_VALUE")
                        )
                )
        );
    }
}
