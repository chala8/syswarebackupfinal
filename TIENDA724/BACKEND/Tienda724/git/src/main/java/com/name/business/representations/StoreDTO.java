package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;

public class StoreDTO {


    private Long id_third;
    private String description;
    private Long id_directory;
    private Long store_number;
    private Long entrada_consecutive_initial;
    private Long salida_consecutive_initial;

    @JsonCreator
    public StoreDTO(@JsonProperty("id_third") Long id_third,
                    @JsonProperty("description") String description,
                    @JsonProperty("id_directory") Long id_directory,
                    @JsonProperty("store_number") Long store_number,
                    @JsonProperty("entrada_consecutive_initial") Long entrada_consecutive_initial,
                    @JsonProperty("salida_consecutive_initial") Long salida_consecutive_initial) {
        this.id_third = id_third;
        this.description = description;
        this.id_directory = id_directory;
        this.store_number = store_number;
        this.entrada_consecutive_initial = entrada_consecutive_initial;
        this.salida_consecutive_initial = salida_consecutive_initial;
    }

    //------------------------------------------------------------------------------------------------

    public Long getid_third() {
        return id_third;
    }

    public void setid_third(Long id_third) {
        this.id_third = id_third;
    }

    //------------------------------------------------------------------------------------------------

    public String getdescription() {
        return description;
    }

    public void setdescription(String description) { this.description = description; }

    //------------------------------------------------------------------------------------------------

    public Long getid_directory() {
        return id_directory;
    }

    public void setid_directory(Long id_directory) {
        this.id_directory = id_directory;
    }

    //------------------------------------------------------------------------------------------------

    public Long getstore_number() {
        return store_number;
    }

    public void setstore_number(Long store_number) {
        this.store_number = store_number;
    }

    //------------------------------------------------------------------------------------------------

    public Long getentrada_consecutive_initial() {
        return entrada_consecutive_initial;
    }

    public void setentrada_consecutive_initial(Long entrada_consecutive_initial) {
        this.entrada_consecutive_initial = entrada_consecutive_initial;
    }

    //------------------------------------------------------------------------------------------------

    public Long getsalida_consecutive_initial() {
        return salida_consecutive_initial;
    }

    public void setsalida_consecutive_initial(Long salida_consecutive_initial) {
        this.salida_consecutive_initial = salida_consecutive_initial;
    }


}
