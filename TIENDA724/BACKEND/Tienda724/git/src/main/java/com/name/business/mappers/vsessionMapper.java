package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class vsessionMapper implements ResultSetMapper<vsession>{

    @Override
    public vsession map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new vsession(
                resultSet.getString("username"),
                resultSet.getLong("count")
        );
    }
}