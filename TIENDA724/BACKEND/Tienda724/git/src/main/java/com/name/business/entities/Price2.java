package com.name.business.entities;

public class Price2 {
    private Long ID_PRODUCT_STORE;
    private String PRICE_DESCRIPTION;
    private double PRICE;
    private double pct_descuento;



    public Price2(Long ID_PRODUCT_STORE, String PRICE_DESCRIPTION, double PRICE, double pct_descuento) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
        this.PRICE_DESCRIPTION = PRICE_DESCRIPTION;
        this.PRICE = PRICE;
        this.pct_descuento = pct_descuento;
    }

    public Long getID_PRODUCT_STORE() {
        return ID_PRODUCT_STORE;
    }

    public void setID_PRODUCT_STORE(Long ID_PRODUCT_STORE) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
    }

    public double getPRICE() {
        return PRICE;
    }

    public void setPRICE(double PRICE) {
        this.PRICE = PRICE;
    }

    public String getPRICE_DESCRIPTION() {
        return PRICE_DESCRIPTION;
    }

    public void setPRICE_DESCRIPTION(String PRICE_DESCRIPTION) {
        this.PRICE_DESCRIPTION = PRICE_DESCRIPTION;
    }

    public double getPct_descuento() {        return pct_descuento;    }

    public void setPct_descuento(double pct_descuento) {        this.pct_descuento = pct_descuento;    }
}
