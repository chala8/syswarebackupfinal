package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VendorMapper implements ResultSetMapper<Vendor>{

    @Override
    public Vendor map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Vendor(
                resultSet.getLong("IDTHIRDVENDEDOR"),
                resultSet.getString("VENDEDOR")
        );
    }
}