package com.name.business.mappers;

import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.TaxTariff;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TaxTariffMapper implements ResultSetMapper<TaxTariff> {

    @Override
    public TaxTariff map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new TaxTariff(
                resultSet.getLong("ID_TAX_TARIFF"),
                resultSet.getDouble("PERCENT"),
                new Common(
                        resultSet.getLong("ID_COMMON"),
                        resultSet.getString("NAME"),
                        resultSet.getString("DESCRIPTION")
                ),
                new CommonState(
                        resultSet.getLong("ID_STATE"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_TAX"),
                        resultSet.getDate("MODIFY_TAX")
                )
        );
    }
}
