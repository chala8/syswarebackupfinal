package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;
import java.util.List;

public class InventoryReportDTO {

    private Long days;
    private List<Long> listStore;
    private List<Long> listLine;
    private List<Long> listCategory;
    private List<Long> listBrand;
    @JsonCreator
    public InventoryReportDTO(@JsonProperty("days") Long days,
                              @JsonProperty("listStore") List<Long> listStore,
                              @JsonProperty("listLine") List<Long> listLine,
                              @JsonProperty("listCategory") List<Long> listCategory,
                              @JsonProperty("listBrand") List<Long> listBrand) {

        this.days = days;
        this.listStore = listStore;
        this.listLine = listLine;
        this.listCategory = listCategory;
        this.listBrand = listBrand;
    }

    //------------------------------------------------------------------------------------------------

    public Long  getdays() {
        return days;
    }

    public void setdays(Long days) {
        this.days = days;
    }

    //------------------------------------------------------------------------------------------------

    public List<Long>  getlistStore() {
        return listStore;
    }

    public void setlistStore(List<Long> listStore) {
        this.listStore = listStore;
    }

    //------------------------------------------------------------------------------------------------

    public List<Long>  getlistLine() {
        return listLine;
    }

    public void setlistLine(List<Long> listLine) {
        this.listLine = listLine;
    }

    //------------------------------------------------------------------------------------------------

    public List<Long>  getlistCategory() {
        return listCategory;
    }

    public void setlistCategory(List<Long> listCategory) {
        this.listCategory = listCategory;
    }

    //------------------------------------------------------------------------------------------------

    public List<Long>  getlistBrand() {
        return listBrand;
    }

    public void setlistBrand(List<Long> listBrand) {
        this.listBrand = listBrand;
    }

    //------------------------------------------------------------------------------------------------




}
