package com.name.business.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class excelRowProd {


    private String categoria;
    private Long costo;
    private Long numventas;
    private Double pct_margen;
    private Long utilidad;
    private Long venta;
    private String ownbarcode;
    private String product;
    private String marca;
    private String linea;

    @JsonCreator
    public excelRowProd(
            @JsonProperty("categoria") String categoria,
            @JsonProperty("costo") Long costo,
            @JsonProperty("numventas") Long numventas,
            @JsonProperty("ownbarcode") String ownbarcode,
            @JsonProperty("pct_MARGEN") Double pct_margen,
            @JsonProperty("utilidad") Long utilidad,
            @JsonProperty("marca") String marca,
            @JsonProperty("linea") String linea,
            @JsonProperty("product") String product,
            @JsonProperty("venta") Long venta) {
        this.ownbarcode = ownbarcode;
        this.categoria = categoria;
        this.costo = costo;
        this.numventas = numventas;
        this.pct_margen = pct_margen;
        this.utilidad = utilidad;
        this.marca = marca;
        this.linea = linea;
        this.product = product;
        this.venta = venta;
    }


    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }


    public String getownbarcode() {
        return ownbarcode;
    }

    public void setownbarcode(String ownbarcode) {
        this.ownbarcode = ownbarcode;
    }


    public Double getpct_margen() {
        return pct_margen;
    }

    public void setpct_margen(Double pct_margen) {
        this.pct_margen = pct_margen;
    }


    public Long getcosto() {
        return costo;
    }

    public void setcosto(Long costo) {
        this.costo = costo;
    }


    public Long getnumventas() {
        return numventas;
    }

    public void setnumventas(Long numventas) {
        this.numventas = numventas;
    }


    public Long getutilidad() {
        return utilidad;
    }

    public void setutilidad(Long utilidad) {
        this.utilidad = utilidad;
    }


    public Long getventa() {
        return venta;
    }

    public void setventa(Long venta) {
        this.venta = venta;
    }


    public String getcategoria() {
        return categoria;
    }

    public void setcategoria(String categoria) {
        this.categoria = categoria;
    }

}
