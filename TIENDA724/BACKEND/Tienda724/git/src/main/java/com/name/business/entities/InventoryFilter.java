
package com.name.business.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.representations.AttributeValueFilter;

import java.util.List;

public class InventoryFilter {
    @Deprecated
    String name_th;

    Long id_third;
    @Deprecated
    String number_document_type_th;
    // card 2
    @Deprecated
    String name_th_venue;
    @Deprecated
    Long id_document_type_th_venue;
    @Deprecated
    String number_document_type_th_venue;

    Long id_venue;
    // card 3
    Long id_productor;
    Long id_cat_productor;

    // card 4
    Long id_category_th;
    // card 5
    Long id_measure_unit;
    // card 6
    List<AttributeValueFilter> attributeValues;

    @JsonCreator
    public InventoryFilter(@JsonProperty("name_th") String name_th,
                           @JsonProperty("id_document_type_th") Long id_third,
                           @JsonProperty("number_document_type_th") String number_document_type_th,
                           @JsonProperty("name_th_venue") String name_th_venue,
                           @JsonProperty("id_document_type_th_venue") Long id_document_type_th_venue,
                           @JsonProperty("number_document_type_th_venue") String number_document_type_th_venue,
                           @JsonProperty("id_venue") Long id_venue,
                           @JsonProperty("id_productor") Long id_productor,
                           @JsonProperty("id_cat_productor") Long id_cat_productor,
                           @JsonProperty("id_category_th") Long id_category_th,
                           @JsonProperty("id_measure_unit") Long id_measure_unit,
                           @JsonProperty("attributeValues") List<AttributeValueFilter> attributeValues) {
        this.name_th = name_th;
        this.id_third = id_third;
        this.number_document_type_th = number_document_type_th;
        this.name_th_venue = name_th_venue;
        this.id_document_type_th_venue = id_document_type_th_venue;
        this.number_document_type_th_venue = number_document_type_th_venue;
        this.id_venue = id_venue;
        this.id_productor = id_productor;
        this.id_cat_productor = id_cat_productor;
        this.id_category_th = id_category_th;
        this.id_measure_unit = id_measure_unit;
        this.attributeValues=attributeValues;

    }


    @Deprecated
    public String getName_th() {
        return name_th;
    }
    @Deprecated
    public void setName_th(String name_th) {
        this.name_th = name_th;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    @Deprecated
    public String getNumber_document_type_th() {
        return number_document_type_th;
    }

    @Deprecated
    public void setNumber_document_type_th(String number_document_type_th) {
        this.number_document_type_th = number_document_type_th;
    }

    public String getName_th_venue() {
        return name_th_venue;
    }

    @Deprecated
    public void setName_th_venue(String name_th_venue) {
        this.name_th_venue = name_th_venue;
    }

    @Deprecated
    public Long getId_document_type_th_venue() {
        return id_document_type_th_venue;
    }

    @Deprecated
    public void setId_document_type_th_venue(Long id_document_type_th_venue) {
        this.id_document_type_th_venue = id_document_type_th_venue;
    }

    @Deprecated
    public String getNumber_document_type_th_venue() {
        return number_document_type_th_venue;
    }

    @Deprecated
    public void setNumber_document_type_th_venue(String number_document_type_th_venue) {
        this.number_document_type_th_venue = number_document_type_th_venue;
    }

    public Long getId_venue() {
        return id_venue;
    }

    public void setId_venue(Long id_venue) {
        this.id_venue = id_venue;
    }

    public Long getId_productor() {
        return id_productor;
    }

    public void setId_productor(Long id_productor) {
        this.id_productor = id_productor;
    }

    public Long getId_cat_productor() {
        return id_cat_productor;
    }

    public void setId_cat_productor(Long id_cat_productor) {
        this.id_cat_productor = id_cat_productor;
    }

    public Long getId_category_th() {
        return id_category_th;
    }

    public void setId_category_th(Long id_category_th) {
        this.id_category_th = id_category_th;
    }

    public Long getId_measure_unit() {
        return id_measure_unit;
    }

    public void setId_measure_unit(Long id_measure_unit) {
        this.id_measure_unit = id_measure_unit;
    }

    public List<AttributeValueFilter> getAttributeValues() {
        return attributeValues;
    }

    public void setAttributeValues(List<AttributeValueFilter> attributeValues) {
        this.attributeValues = attributeValues;
    }
}