package com.name.business.mappers;


import com.name.business.entities.Dato;
import com.name.business.entities.DetailWithOptions;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetailWithOptionsMapper implements ResultSetMapper<DetailWithOptions> {

    @Override
    public DetailWithOptions map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DetailWithOptions(
                resultSet.getLong("id_product_third"),
                resultSet.getString("producto"),
                resultSet.getString("notas"),
                resultSet.getLong("id_bill_state"),
                resultSet.getString("estado")
        );
    }
}
