package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class KazuDetailDTO {

    private Long cc;
    private double valor;
    private String naturaleza;
    private String notes;
    private Long id_document;
    private Long id_country;
    private Long id_third;

    @JsonCreator
    public KazuDetailDTO(
            @JsonProperty("cc") Long cc,
            @JsonProperty("valor") double valor,
            @JsonProperty("naturaleza") String naturaleza,
            @JsonProperty("notes") String notes,
            @JsonProperty("id_document") Long id_document,
            @JsonProperty("id_country") Long id_country,
            @JsonProperty("id_third") Long id_third) {
        this.cc = cc;
        this.valor = valor;
        this.naturaleza = naturaleza;
        this.notes = notes;
        this.id_document = id_document;
        this.id_country = id_country;
        this.id_third = id_third;

    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    //--------------------------------------------------------------------------
    public Long getcc() { return cc; }
    public void setcc(Long cc) { this.cc = cc; }

    //--------------------------------------------------------------------------
    public double getvalor() { return valor; }
    public void setvalor(double valor) { this.valor = valor; }

    //--------------------------------------------------------------------------
    public String getnaturaleza() { return naturaleza; }
    public void setnaturaleza(String naturaleza) { this.naturaleza = naturaleza; }

    //--------------------------------------------------------------------------
    public String getnotes() { return notes; }
    public void setnotes(String notes) { this.notes = notes; }

    //--------------------------------------------------------------------------
    public Long getid_document() { return id_document; }
    public void setid_document(Long id_document) { this.id_document = id_document; }

    //--------------------------------------------------------------------------
    public Long getid_country() { return id_country; }
    public void setid_country(Long id_country) { this.id_country = id_country; }

    //--------------------------------------------------------------------------

}
