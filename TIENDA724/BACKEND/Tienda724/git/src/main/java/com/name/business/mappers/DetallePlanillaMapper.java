package com.name.business.mappers;


import com.name.business.entities.Dato;
import com.name.business.entities.DetallePedido;
import com.name.business.entities.DetallePlanilla;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DetallePlanillaMapper implements ResultSetMapper<DetallePlanilla> {

    @Override
    public DetallePlanilla map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DetallePlanilla(
                resultSet.getString("PURCHASE_DATE"),
                resultSet.getString("NUM_DOCUMENTO"),
                resultSet.getLong("ID_BILL_FACTURA"),
                resultSet.getDouble("TOTALPRICE")
        );
    }
}
