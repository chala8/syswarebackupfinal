package com.name.business.entities;

public class Base64File {

    private String file;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private String fileName;

    public Base64File(String file,
                      String fileName
    ) {
        this.file = file;
        this.fileName = fileName;
    }

}
