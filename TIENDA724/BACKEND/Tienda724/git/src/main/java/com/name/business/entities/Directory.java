package com.name.business.entities;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Directory {

    private  String dir_name;
    private  String address;
    private  String webpage;
    private Long id_city;
    private double latitud;
    private double longitud;

    @JsonCreator
    public Directory(@JsonProperty("dir_name") String dir_name,
                     @JsonProperty("address")  String address,
                     @JsonProperty("webpage")  String webpage,
                     @JsonProperty("id_city") Long id_city,
                     @JsonProperty("latitud")  double latitud,
                     @JsonProperty("longitud")  double longitud) {
        this.dir_name = dir_name;
        this.address = address;
        this.webpage = webpage;
        this.id_city = id_city;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getdir_name() {
        return dir_name;
    }

    public void setdir_name(String dir_name) {
        this.dir_name = dir_name;
    }

    //------------------------------------------------------------------------------------

    public String getaddress() {
        return address;
    }

    public void setaddress(String address) {
        this.address = address;
    }

    //------------------------------------------------------------------------------------

    public String getwebpage() {
        return webpage;
    }

    public void setwebpage(String webpage) {
        this.webpage = webpage;
    }

    //------------------------------------------------------------------------------------

    public Long getid_city() {
        return id_city;
    }

    public void setid_city(Long id_city) {
        this.id_city = id_city;
    }

    //------------------------------------------------------------------------------------


    public double getlatitud() {
        return latitud;
    }

    public void setlatitud(double latitud) {
        this.latitud = latitud;
    }

    //------------------------------------------------------------------------------------


    public double getlongitud() {
        return longitud;
    }

    public void setlongitud(double longitud) {
        this.longitud = longitud;
    }

    //------------------------------------------------------------------------------------
}