package com.name.business.entities;

public class Category {

    private Long id_category;
    private Common common;
    private String img_url;
    private Long id_category_father;
    private Long id_third_category;
    private CommonState state;


    public Category(Long id_category, Common common, String img_url, Long id_category_father,Long id_third_category, CommonState state) {
        this.id_category = id_category;
        this.common = common;
        this.img_url = img_url;
        this.id_category_father = id_category_father;
        this.id_third_category=id_third_category;
        this.state = state;
    }

    public Long getId_third_category() {
        return id_third_category;
    }

    public void setId_third_category(Long id_third_category) {
        this.id_third_category = id_third_category;
    }

    public Long getId_category() {
        return id_category;
    }

    public void setId_category(Long id_category) {
        this.id_category = id_category;
    }

    public Common getCommon() {
        return common;
    }

    public void setCommon(Common common) {
        this.common = common;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public Long getId_category_father() {
        return id_category_father;
    }

    public void setId_category_father(Long id_category_father) {
        this.id_category_father = id_category_father;
    }

    public CommonState getState() {
        return state;
    }

    public void setState(CommonState state) {
        this.state = state;
    }
}
