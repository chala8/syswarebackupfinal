package com.name.business.entities;

public class ProductStoreName {

    private Long ID_STORE;
    private String DESCRIPTION;
    private Long STORE_NUMBER;
    private Long ID_PRODUCT_STORE;
    private String PRODUCT_STORE_NAME;
    private Long PRODUCT_STORE_CODE;
    private String OWNBARCODE;
    private Long STANDARD_PRICE;


    public ProductStoreName(Long ID_STORE,
                           String DESCRIPTION,
                           Long STORE_NUMBER,
                           Long ID_PRODUCT_STORE,
                           String PRODUCT_STORE_NAME,
                           Long PRODUCT_STORE_CODE,
                           String OWNBARCODE,
                           Long STANDARD_PRICE) {
        this.ID_STORE = ID_STORE;
        this.DESCRIPTION = DESCRIPTION;
        this.STORE_NUMBER =  STORE_NUMBER;
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
        this.OWNBARCODE =  OWNBARCODE;
        this.STANDARD_PRICE = STANDARD_PRICE;
    }

    //---------------------------------------------------------------------------

    public Long getID_STORE() {
        return ID_STORE;
    }

    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    //---------------------------------------------------------------------------

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    //---------------------------------------------------------------------------

    public Long getSTORE_NUMBER() {
        return STORE_NUMBER;
    }

    public void setSTORE_NUMBER(Long STORE_NUMBER) {
        this.STORE_NUMBER = STORE_NUMBER;
    }

    //---------------------------------------------------------------------------

    public Long getID_PRODUCT_STORE() {
        return ID_PRODUCT_STORE;
    }

    public void setID_PRODUCT_STORE(Long ID_PRODUCT_STORE) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
    }

    //---------------------------------------------------------------------------

    public String getPRODUCT_STORE_NAME() {
        return PRODUCT_STORE_NAME;
    }

    public void setPRODUCT_STORE_NAME(String PRODUCT_STORE_NAME) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
    }

    //---------------------------------------------------------------------------

    public String getOWNBARCODE() {
        return OWNBARCODE;
    }

    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }

    //---------------------------------------------------------------------------

    public Long getPRODUCT_STORE_CODE() {
        return PRODUCT_STORE_CODE;
    }

    public void setPRODUCT_STORE_CODE(Long PRODUCT_STORE_CODE) {
        this.PRODUCT_STORE_CODE = PRODUCT_STORE_CODE;
    }

    //---------------------------------------------------------------------------

    public Long getSTANDARD_PRICE() {
        return STANDARD_PRICE;
    }

    public void setSTANDARD_PRICE(Long STANDARD_PRICE) {
        this.STANDARD_PRICE = STANDARD_PRICE;
    }
}
