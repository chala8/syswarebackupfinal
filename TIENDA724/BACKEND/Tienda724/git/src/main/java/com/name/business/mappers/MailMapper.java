package com.name.business.mappers;

import com.name.business.entities.Mail;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MailMapper implements ResultSetMapper<Mail>{

    @Override
    public Mail map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Mail(
                resultSet.getLong("ID_MAIL"),
                resultSet.getString("MAIL"),
                resultSet.getLong("PRIORITY")

        );
    }
}