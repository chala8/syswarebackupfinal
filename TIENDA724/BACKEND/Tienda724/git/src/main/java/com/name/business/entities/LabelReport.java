package com.name.business.entities;
import org.skife.jdbi.cglib.core.Local;

import java.time.LocalDate;

import java.util.Date;

public class LabelReport {
    private String label;
    private Long total;



    public LabelReport(Long total) {
        this.total = total;
    }

    public String getlabel() {
        return label;
    }

    public void setlabel(String label) {
        this.label = label;
    }

    //------------------------------------------------------------------------------

    public Long gettotal() {
        return total;
    }

    public void settotal(Long total) {
        this.total = total;
    }

    //------------------------------------------------------------------------------


}


