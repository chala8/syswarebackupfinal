package com.name.business.entities;

public class CajeroInfo {

    private Long CAJA_NUMBER;
    private String DESCRIPTION;


    public CajeroInfo(Long CAJA_NUMBER, String DESCRIPTION) {
        this.CAJA_NUMBER = CAJA_NUMBER;
        this.DESCRIPTION = DESCRIPTION;
    }

    public Long getCAJA_NUMBER() {
        return CAJA_NUMBER;
    }

    public void setCAJA_NUMBER(Long CAJA_NUMBER) {
        this.CAJA_NUMBER = CAJA_NUMBER;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

}
