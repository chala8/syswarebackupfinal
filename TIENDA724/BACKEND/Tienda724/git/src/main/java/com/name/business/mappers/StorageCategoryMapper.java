package com.name.business.mappers;

import com.name.business.entities.StorageCategory;
import com.name.business.entities.StorageName;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StorageCategoryMapper implements ResultSetMapper<StorageCategory>{

    @Override
    public StorageCategory map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new StorageCategory(
                resultSet.getLong("ID_CATEGORY"),
                resultSet.getLong("ID_CATEGORY_FATHER"),
                resultSet.getLong("ID_THIRD"),
                resultSet.getString("NAME"),
                resultSet.getString("DESCRIPTION"),
                resultSet.getString("IMG_URL")
        );
    }
}