package com.name.business.businesses;


import com.name.business.DAOs.StoreDAO;
import com.name.business.entities.*;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONObject;
import org.skife.jdbi.v2.sqlobject.Bind;

import java.util.List;

import static com.name.business.utils.constans.K.*;


public class StoreBusiness {

    private StoreDAO storeDAO;

    public StoreBusiness(StoreDAO storeDAO) {
        this.storeDAO = storeDAO;
    }


    public Either<IException, List<Store> > getStoreByThird2(Long id_third) {
        try {
            return Either.right(storeDAO.getStoreByThird2(id_third));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List<Store> > getStoreByThird(Long id_third) {
        try {
            return Either.right(storeDAO.getStoreByThird(id_third));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List<Store> > getStoresClients(Long idstore) {
        try {
            return Either.right(storeDAO.getStoresClients(idstore));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Long> > getIdDirectoryByIdStore(Long id_store) {
        try {
            return Either.right(storeDAO.getIdDirectoryByIdStore(id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, List<Vendor> > getVendors(Long id_third) {
        try {
            return Either.right(storeDAO.getVendors(id_third));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postStore(Long id_third,
                                                String description,
                                                Long id_directory,
                                                Long store_number,
                                                Long entrada_consecutive_initial,
                                                Long salida_consecutive_initial) {
        try {
            storeDAO.postStore(id_third,description,id_directory,store_number,entrada_consecutive_initial,salida_consecutive_initial);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putStore(Long id_third,
                                                String description,
                                                Long store_number,
                                                Long entrada_consecutive_initial,
                                                Long salida_consecutive_initial,
                                                Long id_store) {
        try {
            storeDAO.putStore(id_third,description,store_number,entrada_consecutive_initial,salida_consecutive_initial,id_store);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Register> > getCajaByStore(Long id_store) {
        try {
            return Either.right(storeDAO.getCajaByStore(id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postCaja(Long id_store,
                                                String description,
                                                Long caja_number) {
        try {
            storeDAO.postCaja(id_store,description,caja_number);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putCaja(String description,
                                               Long caja_number,
                                               Long id_caja) {
        try {
            storeDAO.putCaja(description,caja_number,id_caja);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Storage> > getStorageByStore(Long id_store) {
        try {
            return Either.right(storeDAO.getStorageByStore(id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postStorage(Long id_store,
                                                   String description,
                                                   Long storage_number) {
        try {
            storeDAO.postStorage(id_store,description,storage_number);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putStorage(String description,
                                                  Long storage_number,
                                                  Long id_storage) {
        try {
            storeDAO.putStorage(description,storage_number,id_storage);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Directory> > getDirectorybyID(Long id_directory) {
        try {
            return Either.right(storeDAO.getDirectorybyID(id_directory));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<String> > getCityByID(Long id_city) {
        try {
            return Either.right(storeDAO.getCityByID(id_city));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<String> > getCitiesByState(Long id_state) {
        try {
            return Either.right(storeDAO.getCitiesByState(id_state));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<State> > getStates() {
        try {
            return Either.right(storeDAO.getStates());
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Phone> > getPhonesByDirectory(Long id_directory) {
        try {
            return Either.right(storeDAO.getPhonesByDirectory(id_directory));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Mail> > getMailsByDirectory(Long id_directory) {
        try {
            return Either.right(storeDAO.getMailsByDirectory(id_directory));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<StorageCategory> > getCategoryByStore(Long ID_THIRD) {
        try {
            return Either.right(storeDAO.getCategoryByStore(ID_THIRD));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<StoreName> > getStoreByCategory(Long id_category) {
        try {
            return Either.right(storeDAO.getStoreByCategory(id_category));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Price> > getPriceList(Long id_product_store) {
        try {
            return Either.right(storeDAO.getPriceList(id_product_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Price> > getPriceListView(Long id_product_store,Long idbp) {
        try {
            return Either.right(storeDAO.getPriceListView(id_product_store,idbp));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String > postPrice(Long ID_PRODUCT_STORE,
                                                   String PRICE_DESCRIPTION,
                                                   Double PRICE) {
        try {
            storeDAO.postPrice(ID_PRODUCT_STORE,PRICE_DESCRIPTION,PRICE);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long > postProductStore(Long id_store,
                                                        Long id_code,
                                                        String product_store_name,
                                                        Long standard_price,
                                                        String product_store_code,
                                                        String ownbarcode) {
        try {
            storeDAO.postProductStore(id_store,id_code,product_store_name,standard_price,product_store_code,ownbarcode);
            return Either.right(storeDAO.getPkLast());
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long > putThird(Long id_third) {
        try {
            storeDAO.putThird(id_third);
            return Either.right(storeDAO.getPkLast());
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long > getThird(Long id_person) {
        try {
            Long response = storeDAO.getThird(id_person);
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long > getPsId(String code, Long id_store) {
        try {
            return Either.right(storeDAO.getPsId(code,id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Long> > getTipoStore(Long id_store) {
        try {
            return Either.right(storeDAO.getTipoStore(id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<DatosTienda> > getDatosTienda(Long id_store_proveedor,
                                                              Long id_app) {
        try {
            return Either.right(storeDAO.getDatosTienda(id_store_proveedor,id_app));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<ItemCaja> > getCajaItems(Long id_person) {
        try {
            return Either.right(storeDAO.getCajaItems(id_person));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Integer > putStandardPrice(Double standard_price,
                                                        Long id_ps) {
        try {
            storeDAO.putStandardPrice(standard_price,id_ps);
            return Either.right(1);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Double > getStandardPriceByPs(Long id_ps) {
        try {
            return Either.right(storeDAO.getStandardPriceByPs(id_ps));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> confirmar_pedido_cliente(
                                                             Long idbillcliente,
                                                             String notes) {
        try {
            storeDAO.confirmar_pedido_cliente( idbillcliente,notes)
                                            ;
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, Long> devolucionVenta(Long id_bill,
                                                             Long idcaja) {
        try {
            storeDAO.devolucionVenta( id_bill,
                    idcaja)
            ;
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, Long> createParamPedidos(Long id_store_provider,
                                                       Long id_store_client,
                                                       Long id_third_cliente,
                                                       Long id_third_employee_cliente,
                                                       Long id_third_proveedor) {
        try {
            storeDAO.createParamPedidos(id_store_client, id_store_provider,id_third_cliente,id_third_employee_cliente,id_third_proveedor, null, null, null, "MANUAL", "S", "N");
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long> copyNew() {
        try {
            storeDAO.copyNew();
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, Long> crearTiendaCliente(Long idthirdprop,
                                                       Long idthirdempresa,
                                                       String storename,
                                                       String estienda,
                                                       String username,
                                                       String copiaproductos,
                                                       String esproveedor,
                                                       Long idstorecliente,
                                                       Long idtempcliente) {
        try {
            storeDAO.crearTiendaCliente(idthirdprop,
                    idthirdempresa,
                    storename,
                    estienda,
                    username,
                    copiaproductos,
                    esproveedor,
                    idstorecliente,
                    idtempcliente);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, List<InfoData> > getInfobyIdthird(Long id_store_provider,Long id_third) {
        try {
            return Either.right(storeDAO.getInfobyIdthird(id_store_provider,id_third));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long > getIfIsOnParam(Long id_store_provider,Long id_third) {
        try {
            return Either.right(storeDAO.getIfIsOnParam(id_store_provider,id_third));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long > updateProvCode( Long ID_STORE_PROVEEDOR,
                                                     String codProv,
                                                     String ownbarcode) {
        try {
            storeDAO.updateProvCode(  ID_STORE_PROVEEDOR,
                    codProv,
                    ownbarcode);
            return Either.right(new Long(1));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Float > GET_VALOR_DOMICILIO(String IDSTORE,
                                                          String DISTANCIA_KM) {
        try {
            Float response = storeDAO.GET_VALOR_DOMICILIO(IDSTORE,DISTANCIA_KM);
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long > getProductStore(String ownbarcode,String id_store) {
        try {
            Long response = storeDAO.getProductStore(ownbarcode, id_store);
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List<Option> > getOptions(String idps) {
        try {

            List<Option> listaOpciones = storeDAO.getOptions(idps);

            return Either.right(listaOpciones);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<DetailWithOptions> > getNotesForDetailsWithOptions(String idbill) {
        try {

            List<DetailWithOptions> listaOpciones = storeDAO.getNotesForDetailsWithOptions(idbill);

            return Either.right(listaOpciones);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, String> getStoreNameById(String idStore) {
        try {

            String listaOpciones = storeDAO.getStoreNameById(idStore);

            return Either.right(listaOpciones);
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

}
