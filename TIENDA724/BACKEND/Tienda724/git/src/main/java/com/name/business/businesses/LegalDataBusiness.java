package com.name.business.businesses;

import com.name.business.DAOs.LegalDataDAO;
import com.name.business.entities.*;
import com.name.business.representations.MeasureUnitDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.measureUnitSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class LegalDataBusiness {
    private LegalDataDAO legalDataDAO;

    public LegalDataBusiness(LegalDataDAO legalDataDAO) {
        this.legalDataDAO = legalDataDAO;
    }

    public Either<IException, LegalData > getLegalDataByID(Long id_legal_data) {
        try {
            LegalData validator = legalDataDAO.getLegalDataByID(id_legal_data);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long > getLegalDataByThird(Long id_third) {
        try {
            Long validator = legalDataDAO.getLegalDataByThird(id_third);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postLegalData(String resolucion_dian,
                                                     String regimen_tributario,
                                                     String autoretenedor,
                                                     String url_logo,
                                                     Long id_city,
                                                     Long id_country,
                                                     String address,
                                                     Long phone1,
                                                     String email,
                                                     String prefix_bill,
                                                     String notes) {
        try {
            legalDataDAO.postLegalData(resolucion_dian,
                                        regimen_tributario,
                                        autoretenedor,
                                        url_logo,
                                        id_city,
                                        id_country,
                                        address,
                                        phone1,
                                        email,
                                        prefix_bill,
                                        notes);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putLegalDatabyID(String resolucion_dian,
                                                        String address,
                                                        Long phone1,
                                                        String email,
                                                        String notes,
                                                        Long id_legal_data) {
        try {
            legalDataDAO.putLegalDatabyID(resolucion_dian,
                                            address,
                                            phone1,
                                            email,
                                            notes,
                                            id_legal_data);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String > putThirdLegalData(Long id_third,
                                                         Long id_legal_data) {
        try {
            legalDataDAO.putThirdLegalData(id_third,
                    id_legal_data);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }
}