package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CategoryNameDTO {
    private String img_url;
    private Long id_category_father;
    private Long id_third_category;
    private String categoryDescription;
    private String categoryName;


    @JsonCreator
    public CategoryNameDTO(@JsonProperty ("img_url") String img_url,
                       @JsonProperty ("id_category_father") Long id_category_father,
                       @JsonProperty ("id_third_category") Long id_third_category,
                       @JsonProperty ("categoryDescription")String categoryDescription,
                       @JsonProperty ("categoryName") String categoryName) {

        this.img_url = img_url;
        this.id_category_father = id_category_father;
        this.id_third_category = id_third_category;
        this.categoryDescription = categoryDescription;
        this.categoryName = categoryName;
    }

    public Long getId_third_category() {
        return id_third_category;
    }

    public void setId_third_category(Long id_third_category) {
        this.id_third_category = id_third_category;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public Long getId_category_father() {
        return id_category_father;
    }

    public void setId_category_father(Long id_category_father) {
        this.id_category_father = id_category_father;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}

