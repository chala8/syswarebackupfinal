package com.name.business.representations;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TaxTariffDTO {

    private Double percent;
    private CommonDTO commonDTO;
    private CommonStateDTO stateDTO;

    @JsonCreator
    public TaxTariffDTO(@JsonProperty("percent") Double percent, @JsonProperty("common") CommonDTO commonDTO,
                        @JsonProperty("state") CommonStateDTO stateDTO) {

        this.percent=percent;
        this.commonDTO = commonDTO;
        this.stateDTO = stateDTO;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
