package com.name.business.businesses;

import java.io.File;
import java.util.*;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import com.name.business.DAOs.ReportsDAO;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import com.name.business.entities.*;
import fj.data.Either;

import java.text.SimpleDateFormat;
import java.text.DateFormat;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.pdf.PdfPTable;
import java.io.FileOutputStream;
import java.util.stream.Collectors;


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;


import static com.name.business.utils.constans.K.*;

/**
 * Created by luis on 19/04/17.
 */
public class ReportsBusiness {

    private ReportsDAO reportsDAO;
    private Boolean pruebasLocales = false;

    public ReportsBusiness(ReportsDAO reportsDAO) {
        this.reportsDAO = reportsDAO;
    }


    public Either<IException, String> printPDF(){
        // Se crea el documento
        Document documento = new Document();
        String response = "Bad!!!";
         try{
        // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
                FileOutputStream ficheroPdf = new FileOutputStream("fichero.pdf");

        // Se asocia el documento al OutputStream y se indica que el espaciado entre
        // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
                PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);

        // Se abre el documento.
                documento.open();
                documento.add(new Paragraph("Esto es el primer párrafo, normalito"));

                documento.add(new Paragraph("Este es el segundo y tiene una fuente rara",
                        FontFactory.getFont("arial",   // fuente
                                22,                            // tamaño
                                Font.ITALIC,                   // estilo
                                BaseColor.CYAN)));             // color
                PdfPTable tabla = new PdfPTable(3);
                for (int i = 0; i < 15; i++)
                {
                    tabla.addCell("celda " + i);
                }
                response = "Good!!!";
        documento.add(tabla);}catch(Exception e){
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <CategoryReport>> getTotalesFacturacionPorPeriodoCategorias(Long id_third,
                                                                             Long id_store,
                                                                             Date date1,
                                                                             Date date2) {
        List <CategoryReport> dataResponse = reportsDAO.getTotalesFacturacionPorPeriodoCategorias(id_third,id_store,date1,date2);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Long> asociarCajaStore(Long idperson,
                                                                              Long idstore,
                                                                              Long idcaja) {
        try {
            reportsDAO.asociarCajaStore(idperson,idstore,idcaja);
            return Either.right(new Long(1));
        }catch (Exception e){
            return Either.right(new Long(0));
        }
    }




    public Either<IException, List <CajaReport>> getAllboxes(Long id_store) {
        List <CajaReport> dataResponse = reportsDAO.getAllboxes(id_store);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, List <StoresPending>> getPendingstores(Long id_store) {
        List <StoresPending> dataResponse = reportsDAO.getPendingstores(id_store);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List <PendingReport>> getPendingReport(Long id_store, String id_storec) {
        List<String> list_id_storec = new ArrayList<String>(Arrays.asList(id_storec.split(",")));
        List <PendingReport> dataResponse = reportsDAO.getPendingReport(id_store,list_id_storec);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, List <CategoryReport>> getTotalesFacturacionPorPeriodoProductos(Long id_third,
                                                                                               Long id_store,
                                                                                               Date date1,
                                                                                              Date date2) {
        List <CategoryReport> dataResponse = reportsDAO.getTotalesFacturacionPorPeriodoProductos(id_third,id_store,date1,date2);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List <CategoryReport>> getTotalesFacturacionPorPeriodoProductosLinea(Long id_cat,
                                                                                              Long id_store,
                                                                                                   Date date1,
                                                                                                   Date date2) {
        List <CategoryReport> dataResponse = reportsDAO.getTotalesFacturacionPorPeriodoProductosLinea(id_cat,id_store,date1,date2);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List <CategoryReport>> getTotalesFacturacionPorPeriodoProductosCate(Long id_cat,
                                                                                                   Long id_store,
                                                                                                  String date1,
                                                                                                  String date2) {
        List <CategoryReport> dataResponse = reportsDAO.getTotalesFacturacionPorPeriodoProductosCate(id_cat,id_store,date1,date2);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, List <CategoryReport>> getTotalesFacturacionPorPeriodoProductosBrand(Long id_brand,
                                                                                                  Long id_store,
                                                                                                   String date1,
                                                                                                   String date2) {
        List <CategoryReport> dataResponse = reportsDAO.getTotalesFacturacionPorPeriodoProductosBrand(id_brand,id_store,date1,date2);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List <BillReport>> getTotalesFacturacionPorPeriodo(Long id_third,
                                                                                 String id_store,
                                                                                 String date1,
                                                                                 String date2,
                                                                                 Long typemove) {
        List<String> storeList = new ArrayList<String>(Arrays.asList(id_store.split(",")));
        List <BillReport> dataResponse = reportsDAO.getTotalesFacturacionPorPeriodo(storeList,new Date(date1),new Date(date2),typemove);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List <BillReport>> getTotalesFacturacionPorPeriodo2(Long id_third,
                                                                                 Long id_store,
                                                                                 String date1,
                                                                                 String date2,
                                                                                 Long typemove) {
        List <BillReport> dataResponse = reportsDAO.getTotalesFacturacionPorPeriodo2(id_store,new Date(date1),new Date(date2),typemove);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List <DateReport>> getBillTotal(Long id_bill_type,
                                                              Long id_period,
                                                              Date date1,
                                                              Date date2,
                                                              Long id_third,
                                                              List<Long> listStore) {
        List <DateReport> dataResponse = new ArrayList<DateReport>();
        int cont = 1;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy kk:mm");
        Date tmp = increaseDate(date2,1);
        if(id_period == 4){
            tmp = date1;
            for(int i =0;i<23;i++){
                tmp = increaseDate(tmp,4);
            }

            //System.out.println("this is the end date:"+tmp.toString());
            Date date3 = date1;
            for(int i=0;i<7;i++){
                date3 = increaseDate(date3,4);
            }
            //System.out.println("this is the start date:"+date3.toString());
            Date date4 = increaseDate(date3,id_period);
            for (Date tmp2 = date3, tmp3=date4 ; tmp2.before(tmp) ; tmp2 = increaseDate(tmp2,id_period), tmp3 = increaseDate(tmp3,id_period)) {
                DateReport temp = reportsDAO.getBillTotal(id_bill_type,tmp2,tmp3, id_third, listStore);
                temp.setlabelDate(format.format(tmp3));
                dataResponse.add(temp);
            }
        }else{
        for (Date date3 = date1, date4 = increaseDate(date1,id_period) ; date3.before(tmp) ; date3 = increaseDate(date3,id_period), date4 = increaseDate(date4,id_period)) {
            DateReport temp = reportsDAO.getBillTotal(id_bill_type,date3,date4, id_third, listStore);
            temp.setCounter(cont);
            temp.setlabelDate(format.format(increaseDate(date4,-1)));
            dataResponse.add(temp);
            cont++;
        }
        }
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <LabelReport>> getTotalByBillTypeProduct(Long id_bill_type,
                                                                             Long id_product_store,
                                                                             Long id_period,
                                                                             Date date1,
                                                                             Date date2,
                                                                            List<Long> id_store) {
        List <LabelReport> dataResponse = new ArrayList<LabelReport>();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy kk:mm");
        Date tmp = increaseDate(date2,1);
        if(id_period == 4){
            tmp = date1;
            for(int i =0;i<23;i++){
                tmp = increaseDate(tmp,4);
            }

            //System.out.println("this is the end date:"+tmp.toString());
            Date date3 = date1;
            for(int i=0;i<7;i++){
                date3 = increaseDate(date3,4);
            }
            //System.out.println("this is the start date:"+date3.toString());
            Date date4 = increaseDate(date3,id_period);
            for (Date tmp2 = date3, tmp3=date4 ; tmp2.before(tmp) ; tmp2 = increaseDate(tmp2,id_period), tmp3 = increaseDate(tmp3,id_period)) {
                LabelReport temp = reportsDAO.getTotalByBillTypeProduct(id_bill_type,id_product_store,tmp2,tmp3,id_store);
                temp.setlabel(format.format(tmp3));

                //System.out.println(tmp3.toString());
                dataResponse.add(temp);
            }
        }else{
        for (Date date3 = date1, date4 = increaseDate(date1,id_period) ; date3.before(tmp) ; date3 = increaseDate(date3,id_period), date4 = increaseDate(date4,id_period)) {
            LabelReport temp = reportsDAO.getTotalByBillTypeProduct(id_bill_type,id_product_store,date3,date4,id_store);
            temp.setlabel(format.format(increaseDate(date4,-1)));
            dataResponse.add(temp);
        }
        }

        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Long auditoria(String nombre,
                          Long id_third_employee,
                          Long id_store,
                          String valor_anterior,
                          String valor_nuevo)
    {
        Long response = new Long(0);
        // Check if the word is present in string
        // If found, remove it using removeAll()
        try{
            reportsDAO.auditoria( nombre,
                     id_third_employee,
                     id_store,
                     valor_anterior,
                     valor_nuevo);


            // Return the resultant string

            response = new Long(1);

        }catch(Exception e){
            e.printStackTrace();
            response = new Long(0);
        }

        return response;

    }

    public Either<IException, List <LabelReport>> getTotalByBillTypeLine(Long id_bill_type,
                                                                             Long id_category,
                                                                             Long id_period,
                                                                             Date date1,
                                                                             Date date2,
                                                                             Long id_third,
                                                                             List<Long> id_store
    ) {
        List <LabelReport> dataResponse = new ArrayList<LabelReport>();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy kk:mm" );
        Date tmp = increaseDate(date2,1);
        if(id_period == 4){
            tmp = date1;
            for(int i =0;i<23;i++){
                tmp = increaseDate(tmp,4);
            }

            //System.out.println("this is the end date:"+tmp.toString());
            Date date3 = date1;
            for(int i=0;i<7;i++){
                date3 = increaseDate(date3,4);
            }
            //System.out.println("this is the start date:"+date3.toString());
            Date date4 = increaseDate(date3,id_period);
            for (Date tmp2 = date3, tmp3=date4 ; tmp2.before(tmp) ; tmp2 = increaseDate(tmp2,id_period), tmp3 = increaseDate(tmp3,id_period)) {
                LabelReport temp = reportsDAO.getTotalByBillTypeLine(id_bill_type,id_category,tmp2,tmp3,id_third, id_store);
                temp.setlabel(format.format(tmp3));
                dataResponse.add(temp);
            }
        }else{
            for (Date date3 = date1, date4 = increaseDate(date1,id_period) ; date3.before(tmp) ; date3 = increaseDate(date3,id_period), date4 = increaseDate(date4,id_period)) {
                LabelReport temp = reportsDAO.getTotalByBillTypeLine(id_bill_type,id_category,date3,date4, id_third, id_store);
                temp.setlabel(format.format(increaseDate(date4,-1)));
                dataResponse.add(temp);
            }
        }
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <LabelReport>> getTotalByBillTypeCategory(Long id_bill_type,
                                                                             Long id_category,
                                                                             Long id_period,
                                                                             Date date1,
                                                                             Date date2,
                                                                             Long id_third,
                                                                             List<Long> id_store
                                                                             ) {
        List <LabelReport> dataResponse = new ArrayList<LabelReport>();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy kk:mm" );
        Date tmp = increaseDate(date2,1);
        if(id_period == 4){
            tmp = date1;
            for(int i =0;i<23;i++){
                tmp = increaseDate(tmp,4);
            }

            //System.out.println("this is the end date:"+tmp.toString());
            Date date3 = date1;
            for(int i=0;i<7;i++){
                date3 = increaseDate(date3,4);
            }
            //System.out.println("this is the start date:"+date3.toString());
            Date date4 = increaseDate(date3,id_period);
            for (Date tmp2 = date3, tmp3=date4 ; tmp2.before(tmp) ; tmp2 = increaseDate(tmp2,id_period), tmp3 = increaseDate(tmp3,id_period)) {
                LabelReport temp = reportsDAO.getTotalByBillTypeCategory(id_bill_type,id_category,tmp2,tmp3,id_third, id_store);
                temp.setlabel(format.format(tmp3));
                dataResponse.add(temp);
            }
        }else{
        for (Date date3 = date1, date4 = increaseDate(date1,id_period) ; date3.before(tmp) ; date3 = increaseDate(date3,id_period), date4 = increaseDate(date4,id_period)) {
            LabelReport temp = reportsDAO.getTotalByBillTypeCategory(id_bill_type,id_category,date3,date4, id_third, id_store);
            temp.setlabel(format.format(increaseDate(date4,-1)));
            dataResponse.add(temp);
        }
        }
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    private Date increaseDate (Date a, long id_period) {
        Calendar c = Calendar.getInstance();
        c.setTime(a);
        if(id_period == -1)
        {c.add(Calendar.DAY_OF_MONTH,-1);}
        if(id_period == 1)
        {c.add(Calendar.DAY_OF_MONTH,1);}
        if(id_period == 2)
        {c.add(Calendar.DAY_OF_MONTH,7);}
        if(id_period == 3)
        {c.add(Calendar.MONTH, 1);}
        if(id_period == 4)
        {c.add(Calendar.HOUR_OF_DAY, 1);}
        Date response = c.getTime();
        return response;
    }

    public Either<IException, List<NewProductReport>> getNewProducts(List<String> listStore) {

        try {
            return Either.right(reportsDAO.getNewProducts(listStore));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, DateReport2> getTotalByCategory(Long id_bill_type,
                                                              Date date1,
                                                              Date date2) {

        try {
            return Either.right(reportsDAO.getTotalByCategory(id_bill_type,date1,date2));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, DateReport2> getTotalByBrand(Long id_bill_type,
                                                       Date date1,
                                                       Date date2) {

        try {
            return Either.right(reportsDAO.getTotalByBrand(id_bill_type,date1,date2));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Double> getMargen(Long id_ps) {

        try {
            return Either.right(reportsDAO.getMargen(id_ps));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> putNombre(Long id_ps, String nombre) {

        try {
            reportsDAO.putNombre(id_ps, nombre);
            return Either.right(new Long(1));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long> putQuickCode(Long id_ps, String code, Long id_store) {

        try {
            List<Long> list = reportsDAO.getListCodes(id_store,code);
            //System.out.println("THIS IS DE SIXE OF LIST: "+list.size());
            if(list.size()>0){
                return Either.right(new Long(0));

            }else{
                reportsDAO.putQuickCode(id_ps, code);
                return Either.right(new Long(1));

            }
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Double> putMargen(Long id_ps, Double margen) {

        try {
            reportsDAO.putMargen(id_ps, margen);
            return Either.right(new Double(1));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List <Auditoria>> getAuditoria(Long id_store,
                                                           String date1,
                                                           String date2) {

        try {
            return Either.right(reportsDAO.getAuditoria(id_store,new Date(date1),new Date(date2)));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <DateReport2>> getTotalByProduct(Long id_bill_type,
                                                       Date date1,
                                                       Date date2, Long id_store) {

        try {
            return Either.right(reportsDAO.getTotalByProduct(id_bill_type,date1,date2,id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <Price>> getPriceByPS(Long id_ps) {

        try {
            return Either.right(reportsDAO.getPriceByPS(id_ps));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <InvetoryReport>> getInventarioCons(List<Long> listStore,
                                                                       List<Long> listLine,
                                                                       List<Long> listCategory,
                                                                       List<Long> listBrand){

        try {
            //Long response = reportsDAO.get5Mil(listStore);
            //System.out.println(response);
            List<Long> emptyList = new ArrayList<>();
            if(listBrand.size()>1000){
                List<Long> first = new ArrayList<>(listBrand.subList(0, 998));
                List<Long> second = new ArrayList<>(listBrand.subList(998, listBrand.size()));
                System.out.println("-----------------------STORE----------------------");
                System.out.println(Arrays.toString(listStore.toArray()));
                System.out.println("-----------------------LINE----------------------");
                System.out.println(Arrays.toString(listLine.toArray()));
                System.out.println("-----------------------CATEGORY----------------------");
                System.out.println(Arrays.toString(listCategory.toArray()));
                System.out.println("-----------------------BRANDS----------------------");
                System.out.println("SIZE: "+listBrand.size());
                System.out.println("----------------------------------------------------------");
                System.out.println("FIRST: ");
                System.out.println(Arrays.toString(first.toArray()));
                System.out.println("SIZE: "+first.size());
                System.out.println("----------------------------------------------------------");
                System.out.println("SECOND: ");
                System.out.println(Arrays.toString(second.toArray()));
                System.out.println("SIZE: "+second.size());
                System.out.println("----------------------------------------------------------");

                return Either.right(reportsDAO.getInventarioCons(reportsDAO.get5Mil(listStore),listStore,listLine,listCategory,first,second));

            }


            return Either.right(reportsDAO.getInventarioCons(reportsDAO.get5Mil(listStore),listStore,listLine,listCategory,listBrand,emptyList));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <DiasRotacion>> getDiasRotacion(    Long days,
                                                                       List<Long> listStore,
                                                                       List<Long> listLine,
                                                                       List<Long> listCategory,
                                                                       List<Long> listBrand){

        try {
            //System.out.println(listStore);
            //System.out.println(listLine);
            //System.out.println(listCategory);
            //System.out.println(listBrand);
            return Either.right(reportsDAO.getDiasRotacion(days,listStore,listLine,listCategory,listBrand));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <Brand>> getBrands(){

        try {
            return Either.right(reportsDAO.getBrands());
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <Long>> getSonCat(List<Long> listStore){

        try {
            return Either.right(reportsDAO.getSonCat(listStore));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }


    public Either<IException, String> postExcelPedidos(ArrayList<excelRowPedidos> list, Long daysAsk, Long daysBetween){
        //System.out.println("ESTOS SON LOS DIAS: "+daysBetween);

        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);


        DataFormat format2 = libro.createDataFormat();
        HSSFRow filaTitulo = hoja.createRow(0);

        Double subTotal = 0.0;
        Double iva = 0.0;

        HSSFCellStyle my_style = libro.createCellStyle();
        /* Create HSSFFont object from the workbook */
        HSSFFont my_font=libro.createFont();
        /* set the weight of the font */
        my_font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        /* attach the font to the style created earlier */
        my_style.setFont(my_font);
        /* At this stage, we have a bold style created which we can attach to a cell */

        HSSFCellStyle my_style2 = libro.createCellStyle();
        my_style2.setDataFormat(format2.getFormat("#"));


        for(excelRowPedidos element: list){
            Double prom = (Math.round((double)element.getcantidad_VENDIDA()/(double)daysBetween*10.0)/10.0);
            if(Math.round((((double)daysAsk*(double)element.getcantidad_VENDIDA()/(double)daysBetween)-element.getcantidad_EN_INVENTARIO()))>0){

                subTotal+=element.getcosto()*Math.round((((double)daysAsk*(double)element.getcantidad_VENDIDA()/(double)daysBetween)-element.getcantidad_EN_INVENTARIO()));
                iva+=element.getcosto()*Math.round((((double)daysAsk*(double)element.getcantidad_VENDIDA()/(double)daysBetween)-element.getcantidad_EN_INVENTARIO()))*element.getiva()/100;

            }
        }

        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);

        //MERGEO ZONA 1
        hoja.addMergedRegion(new CellRangeAddress(0,0,0,13));
        HSSFCell celdaprodT01 = filaTitulo.createCell((short)0);
        HSSFRichTextString textoT701 = new HSSFRichTextString("Cantidad Productos: "+list.size());
        celdaprodT01.setCellValue(textoT701);
        celdaprodT01.setCellStyle(my_style);
        //MERGEO ZONA 2
        filaTitulo = hoja.createRow(1);
        hoja.addMergedRegion(new CellRangeAddress(1,1,0,13));
        HSSFCell celdaprodT012 = filaTitulo.createCell((short)0);
        HSSFRichTextString textoT7012 = new HSSFRichTextString("Subtotal: "+format.format(roundAvoid(subTotal,0)));
        celdaprodT012.setCellValue(textoT7012);
        celdaprodT012.setCellStyle(my_style);
        //MERGEO ZONA 3
        filaTitulo = hoja.createRow(2);
        hoja.addMergedRegion(new CellRangeAddress(2,2,0,13));
        HSSFCell celdaprodT013 = filaTitulo.createCell((short)0);
        HSSFRichTextString textoT7013 = new HSSFRichTextString("IVA: "+format.format(roundAvoid(iva,0)));
        celdaprodT013.setCellValue(textoT7013);
        celdaprodT013.setCellStyle(my_style);
        //MERGEO ZONA 4
        filaTitulo = hoja.createRow(3);
        hoja.addMergedRegion(new CellRangeAddress(3,3,0,13));
        HSSFCell celdaprodT014 = filaTitulo.createCell((short)0);
        HSSFRichTextString textoT7014 = new HSSFRichTextString("Total: "+format.format(roundAvoid(subTotal+iva,0)));
        celdaprodT014.setCellValue(textoT7014);
        celdaprodT014.setCellStyle(my_style);





        try {

            HSSFRow filaTitulo2 = hoja.createRow(4);

            HSSFCell celdaprodT2 = filaTitulo2.createCell((short)0);
            HSSFRichTextString textoT72 = new HSSFRichTextString("Proveedor");
            celdaprodT2.setCellValue(textoT72);
            celdaprodT2.setCellStyle(my_style);


            HSSFCell celdaprodT = filaTitulo2.createCell((short)1);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Codigo de Barras");
            celdaprodT.setCellValue(textoT7);
            celdaprodT.setCellStyle(my_style);


            HSSFCell celdalineaT = filaTitulo2.createCell((short)2);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Producto");
            celdalineaT.setCellValue(textoT4);
            celdalineaT.setCellStyle(my_style);


            HSSFCell celdacateT = filaTitulo2.createCell((short)3);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Presentacion");
            celdacateT.setCellValue(textoT5);
            celdacateT.setCellStyle(my_style);


            HSSFCell celdaFab = filaTitulo2.createCell((short)4);
            HSSFRichTextString textoT6f = new HSSFRichTextString("Fabricante");
            celdaFab.setCellValue(textoT6f);
            celdaFab.setCellStyle(my_style);


            HSSFCell celdamarcaT = filaTitulo2.createCell((short)5);
            HSSFRichTextString textoT6 = new HSSFRichTextString("Marca");
            celdamarcaT.setCellValue(textoT6);
            celdamarcaT.setCellStyle(my_style);


            HSSFCell celdaCantT = filaTitulo2.createCell((short)6);
            HSSFRichTextString textoT = new HSSFRichTextString("Linea");
            celdaCantT.setCellValue(textoT);
            celdaCantT.setCellStyle(my_style);


            HSSFCell celdacostT = filaTitulo2.createCell((short)7);
            HSSFRichTextString textoT1 = new HSSFRichTextString("Categoria");
            celdacostT.setCellValue(textoT1);
            celdacostT.setCellStyle(my_style);


            HSSFCell celdapctT3c = filaTitulo2.createCell((short)8);
            HSSFRichTextString textoT33c = new HSSFRichTextString("Costo");
            celdapctT3c.setCellValue(textoT33c);
            celdapctT3c.setCellStyle(my_style);


            HSSFCell celdapctT4i = filaTitulo2.createCell((short)9);
            HSSFRichTextString textoT34i = new HSSFRichTextString("% IVA");
            celdapctT4i.setCellValue(textoT34i);
            celdapctT4i.setCellStyle(my_style);


            HSSFCell celdacosttotT = filaTitulo2.createCell((short)10);
            HSSFRichTextString textoT2 = new HSSFRichTextString("Cantidad Vendida");
            celdacosttotT.setCellValue(textoT2);
            celdacosttotT.setCellStyle(my_style);


            HSSFCell celdapctT = filaTitulo2.createCell((short)11);
            HSSFRichTextString textoT3 = new HSSFRichTextString("Cantidad en Inventario");
            celdapctT.setCellValue(textoT3);
            celdapctT.setCellStyle(my_style);


            HSSFCell celdapctT2 = filaTitulo2.createCell((short)12);
            HSSFRichTextString textoT32 = new HSSFRichTextString("Promedio Diario");
            celdapctT2.setCellValue(textoT32);
            celdapctT2.setCellStyle(my_style);


            HSSFCell celdapctT3 = filaTitulo2.createCell((short)13);
            HSSFRichTextString textoT33 = new HSSFRichTextString("Cantidad a pedir");
            celdapctT3.setCellValue(textoT33);
            celdapctT3.setCellStyle(my_style);


            HSSFCell celdapctT4 = filaTitulo2.createCell((short)14);
            HSSFRichTextString textoT34 = new HSSFRichTextString("Cantidad Contra Inventario");
            celdapctT4.setCellValue(textoT34);
            celdapctT4.setCellStyle(my_style);


            int cont = 5;
            for(excelRowPedidos element: list){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod22 = fila.createCell((short)0);
                HSSFRichTextString texto722 = new HSSFRichTextString(element.getProveedor()+"");
                celdaprod22.setCellValue(texto722);


                HSSFCell celdaprod = fila.createCell((short)1);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getbarcode()+"");
                celdaprod.setCellValue(texto7);


                HSSFCell celdalinea = fila.createCell((short)2);
                HSSFRichTextString texto4 = new HSSFRichTextString(element.getproducto()+"");
                celdalinea.setCellValue(texto4);


                HSSFCell celdacate = fila.createCell((short)3);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getpresentacion()+"");
                celdacate.setCellValue(texto5);


                HSSFCell celdamarcaf = fila.createCell((short)4);
                HSSFRichTextString texto6f = new HSSFRichTextString(element.getfabricante()+"");
                celdamarcaf.setCellValue(texto6f);


                HSSFCell celdamarca = fila.createCell((short)5);
                HSSFRichTextString texto6 = new HSSFRichTextString(element.getmarca()+"");
                celdamarca.setCellValue(texto6);


                HSSFCell celdaCant = fila.createCell((short)6);
                HSSFRichTextString texto = new HSSFRichTextString(element.getlinea()+"");
                celdaCant.setCellValue(texto);


                HSSFCell celdacost = fila.createCell((short)7);
                HSSFRichTextString texto1 = new HSSFRichTextString(element.getcategoria()+"");
                celdacost.setCellValue(texto1);


                HSSFCell celdapct4c = fila.createCell((short)8);
                celdapct4c.setCellValue(Double.parseDouble(element.getcosto()+""));


                HSSFCell celdapct4i = fila.createCell((short)9);
                celdapct4i.setCellValue(Double.parseDouble(element.getiva()+""));


                HSSFCell celdacosttot = fila.createCell((short)10);
                celdacosttot.setCellValue(Double.parseDouble(element.getcantidad_VENDIDA()+""));


                HSSFCell celdapct = fila.createCell((short)11);
                celdapct.setCellValue(Double.parseDouble(element.getcantidad_EN_INVENTARIO()+""));

                Double  promedio = (double)element.getcantidad_VENDIDA()/(double)daysBetween;
                HSSFCell celdapct2 = fila.createCell((short)12);
                celdapct2.setCellValue(Double.parseDouble((Math.round(promedio*10.0)/10.0)+""));

                HSSFCell celdapct3 = fila.createCell((short)13);
                celdapct3.setCellValue(Double.parseDouble(Math.round(((double)daysAsk*(double)element.getcantidad_VENDIDA()/(double)daysBetween))+""));


                HSSFCell celdapct4 = fila.createCell((short)14);
                celdapct4.setCellValue(Double.parseDouble(Math.round((((double)daysAsk*(double)element.getcantidad_VENDIDA()/(double)daysBetween)-element.getcantidad_EN_INVENTARIO()))+""));




                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"pedidos.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+".xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"pedidos.xls";
        }catch (Exception e){
            //System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> postExcelLog(LogDTO log){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);

        CreationHelper creationHelper = libro.getCreationHelper();
        CellStyle EstiloDinero = libro.createCellStyle();
        EstiloDinero.setDataFormat(creationHelper.createDataFormat().getFormat(
                "\"$\"#,##0.00_);\\(\"- $\"#,##0.00\\)"));
        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short) 0);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Documento ");
            celdaprodT.setCellValue(textoT7);

            HSSFCell celdapctT2 = filaTitulo.createCell((short) 1);
            HSSFRichTextString textoT32 = new HSSFRichTextString("Pedido Cliente");
            celdapctT2.setCellValue(textoT32);

            HSSFCell celdapctT3 = filaTitulo.createCell((short) 2);
            HSSFRichTextString textoT33 = new HSSFRichTextString("Cliente ");
            celdapctT3.setCellValue(textoT33);

            HSSFCell celdalineaT = filaTitulo.createCell((short) 3);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Direccion ");
            celdalineaT.setCellValue(textoT4);

            HSSFCell celdacateT = filaTitulo.createCell((short) 4);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Telefono ");
            celdacateT.setCellValue(textoT5);



            HSSFRow filaTitulo2 = hoja.createRow(1);

            HSSFCell celdaprodT2 = filaTitulo2.createCell((short) 0);
            HSSFRichTextString textoT72 = new HSSFRichTextString(log.getDocumento());
            celdaprodT2.setCellValue(textoT72);

            HSSFCell celdapctT22 = filaTitulo2.createCell((short) 1);
            HSSFRichTextString textoT322 = new HSSFRichTextString(log.getPedidoCliente());
            celdapctT22.setCellValue(textoT322);

            HSSFCell celdapctT32 = filaTitulo2.createCell((short) 2);
            HSSFRichTextString textoT332 = new HSSFRichTextString(log.getCliente());
            celdapctT32.setCellValue(textoT332);

            HSSFCell celdalineaT2 = filaTitulo2.createCell((short) 3);
            HSSFRichTextString textoT42 = new HSSFRichTextString(log.getDireccion());
            celdalineaT2.setCellValue(textoT42);

            HSSFCell celdacateT2 = filaTitulo2.createCell((short) 4);
            HSSFRichTextString textoT52 = new HSSFRichTextString(log.getTelefono());
            celdacateT.setCellValue(textoT52);



            HSSFRow filaTitulo22 = hoja.createRow(2);

            HSSFCell celdaprodT22 = filaTitulo22.createCell((short) 0);
            HSSFRichTextString textoT722 = new HSSFRichTextString(" ");
            celdaprodT22.setCellValue(textoT722);

            HSSFCell celdapctT222 = filaTitulo22.createCell((short) 1);
            HSSFRichTextString textoT3222 = new HSSFRichTextString("Tiempo Total");
            celdapctT222.setCellValue(textoT3222);

            HSSFCell celdapctT322 = filaTitulo22.createCell((short) 2);
            HSSFRichTextString textoT3322 = new HSSFRichTextString(" ");
            celdapctT322.setCellValue(textoT3322);

            HSSFCell celdalineaT22 = filaTitulo22.createCell((short) 3);
            HSSFRichTextString textoT422 = new HSSFRichTextString("Tiempo Entrega");
            celdalineaT22.setCellValue(textoT422);

            HSSFCell celdacateT22 = filaTitulo22.createCell((short) 4);
            HSSFRichTextString textoT522 = new HSSFRichTextString(" ");
            celdacateT22.setCellValue(textoT522);



            HSSFRow filaTitulo223 = hoja.createRow(3);

            HSSFCell celdaprodT223 = filaTitulo223.createCell((short) 0);
            HSSFRichTextString textoT7223 = new HSSFRichTextString(" ");
            celdaprodT223.setCellValue(textoT7223);

            HSSFCell celdapctT2223 = filaTitulo223.createCell((short) 1);
            HSSFRichTextString textoT32223 = new HSSFRichTextString(log.getTiempoTotal()+" Segundos.");
            celdapctT2223.setCellValue(textoT32223);

            HSSFCell celdapctT3223 = filaTitulo223.createCell((short) 2);
            HSSFRichTextString textoT33223 = new HSSFRichTextString(" ");
            celdapctT3223.setCellValue(textoT33223);

            HSSFCell celdalineaT223 = filaTitulo223.createCell((short) 3);
            HSSFRichTextString textoT4223 = new HSSFRichTextString(log.getTiempoEntrega()+" Segundos.");
            celdalineaT223.setCellValue(textoT4223);

            HSSFCell celdacateT223 = filaTitulo223.createCell((short) 4);
            HSSFRichTextString textoT5223 = new HSSFRichTextString(" ");
            celdacateT223.setCellValue(textoT5223);



            HSSFRow filaTitulo2234 = hoja.createRow(4);

            HSSFCell celdaprodT2234 = filaTitulo2234.createCell((short) 0);
            HSSFRichTextString textoT72234 = new HSSFRichTextString("Fecha Evento");
            celdaprodT2234.setCellValue(textoT72234);

            HSSFCell celdapctT22234 = filaTitulo2234.createCell((short) 1);
            HSSFRichTextString textoT322234 = new HSSFRichTextString("Notas");
            celdapctT22234.setCellValue(textoT322234);

            HSSFCell celdapctT32234 = filaTitulo2234.createCell((short) 2);
            HSSFRichTextString textoT332234 = new HSSFRichTextString("Actor");
            celdapctT32234.setCellValue(textoT332234);

            HSSFCell celdalineaT2234 = filaTitulo2234.createCell((short) 3);
            HSSFRichTextString textoT42234 = new HSSFRichTextString("Tiempo (Segundos)");
            celdalineaT2234.setCellValue(textoT42234);



            int cont = 5;
            int cont2 =0;
            for(LogDetailDTO element: log.getLog()){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getFecha_EVENTO()+"");
                celdaprod.setCellValue(texto7);

                HSSFCell celdapct2 = fila.createCell((short)1);
                HSSFRichTextString texto32 = new HSSFRichTextString(element.getNotas()+"");
                celdapct2.setCellValue(texto32);

                if(element.getActor().equals("P")){
                    HSSFCell celdapct3 = fila.createCell((short)2);
                    HSSFRichTextString texto33 = new HSSFRichTextString("Proveedor");
                    celdapct3.setCellValue(texto33);
                }else{
                    HSSFCell celdapct3 = fila.createCell((short)2);
                    HSSFRichTextString texto33 = new HSSFRichTextString("Cliente");
                    celdapct3.setCellValue(texto33);
                }

                try{

                Date a = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(log.getLog().get(cont2).getFecha_EVENTO());
                Date b = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(log.getLog().get(cont2+1).getFecha_EVENTO());

                Long diff =b.getTime() - a.getTime();

                Long segs = diff/1000;

                segs = Math.abs(segs);

                    HSSFCell celdalinea = fila.createCell((short)3);
                    celdalinea.setCellValue(segs);

                }catch(Exception e){
                    HSSFCell celdalinea = fila.createCell((short)3);
                    HSSFRichTextString texto4 = new HSSFRichTextString("ESTADO INICIAL");
                    celdalinea.setCellValue(texto4);
                }


                cont2++;
                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"log.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+"log.xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"log.xls";
        }catch (Exception e){
            //System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> postExcelKardex(KardexDTO kardex){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);

        CreationHelper creationHelper = libro.getCreationHelper();
        CellStyle EstiloDinero = libro.createCellStyle();
        EstiloDinero.setDataFormat(creationHelper.createDataFormat().getFormat(
                "\"$\"#,##0.00_);\\(\"- $\"#,##0.00\\)"));
        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short) 0);
            HSSFRichTextString textoT7 = new HSSFRichTextString(" ");
            celdaprodT.setCellValue(textoT7);

            HSSFCell celdapctT2 = filaTitulo.createCell((short) 1);
            HSSFRichTextString textoT32 = new HSSFRichTextString("Producto");
            celdapctT2.setCellValue(textoT32);

            HSSFCell celdapctT3 = filaTitulo.createCell((short) 2);
            HSSFRichTextString textoT33 = new HSSFRichTextString(" ");
            celdapctT3.setCellValue(textoT33);

            HSSFCell celdalineaT = filaTitulo.createCell((short) 3);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Linea");
            celdalineaT.setCellValue(textoT4);

            HSSFCell celdacateT = filaTitulo.createCell((short) 4);
            HSSFRichTextString textoT5 = new HSSFRichTextString(" ");
            celdacateT.setCellValue(textoT5);

            HSSFCell celdamarcaT42 = filaTitulo.createCell((short) 5);
            HSSFRichTextString textoT642 = new HSSFRichTextString("Categoria");
            celdamarcaT42.setCellValue(textoT642);

            HSSFCell celdamarcaT = filaTitulo.createCell((short) 6);
            HSSFRichTextString textoT6 = new HSSFRichTextString(" ");
            celdamarcaT.setCellValue(textoT6);



            HSSFRow filaTitulo2 = hoja.createRow(1);

            HSSFCell celdaprodT2 = filaTitulo2.createCell((short) 0);
            HSSFRichTextString textoT72 = new HSSFRichTextString(" ");
            celdaprodT2.setCellValue(textoT72);

            HSSFCell celdapctT22 = filaTitulo2.createCell((short) 1);
            HSSFRichTextString textoT322 = new HSSFRichTextString(kardex.getProducto());
            celdapctT22.setCellValue(textoT322);

            HSSFCell celdapctT32 = filaTitulo2.createCell((short) 2);
            HSSFRichTextString textoT332 = new HSSFRichTextString(" ");
            celdapctT32.setCellValue(textoT332);

            HSSFCell celdalineaT2 = filaTitulo2.createCell((short) 3);
            HSSFRichTextString textoT42 = new HSSFRichTextString(kardex.getLinea());
            celdalineaT2.setCellValue(textoT42);

            HSSFCell celdacateT2 = filaTitulo2.createCell((short) 4);
            HSSFRichTextString textoT52 = new HSSFRichTextString(" ");
            celdacateT.setCellValue(textoT52);

            HSSFCell celdamarcaT422 = filaTitulo2.createCell((short) 5);
            HSSFRichTextString textoT6422 = new HSSFRichTextString(kardex.getCategoria());
            celdamarcaT422.setCellValue(textoT6422);

            HSSFCell celdamarcaT2 = filaTitulo2.createCell((short) 6);
            HSSFRichTextString textoT62 = new HSSFRichTextString(" ");
            celdamarcaT2.setCellValue(textoT62);



            HSSFRow filaTitulo22 = hoja.createRow(2);

            HSSFCell celdaprodT22 = filaTitulo22.createCell((short) 0);
            HSSFRichTextString textoT722 = new HSSFRichTextString(" ");
            celdaprodT22.setCellValue(textoT722);

            HSSFCell celdapctT222 = filaTitulo22.createCell((short) 1);
            HSSFRichTextString textoT3222 = new HSSFRichTextString(" ");
            celdapctT222.setCellValue(textoT3222);

            HSSFCell celdapctT322 = filaTitulo22.createCell((short) 2);
            HSSFRichTextString textoT3322 = new HSSFRichTextString("Fecha inicial");
            celdapctT322.setCellValue(textoT3322);

            HSSFCell celdalineaT22 = filaTitulo22.createCell((short) 3);
            HSSFRichTextString textoT422 = new HSSFRichTextString(" ");
            celdalineaT22.setCellValue(textoT422);

            HSSFCell celdacateT22 = filaTitulo22.createCell((short) 4);
            HSSFRichTextString textoT522 = new HSSFRichTextString("Fecha Final");
            celdacateT22.setCellValue(textoT522);

            HSSFCell celdamarcaT4222 = filaTitulo22.createCell((short) 5);
            HSSFRichTextString textoT64222 = new HSSFRichTextString(" ");
            celdamarcaT4222.setCellValue(textoT64222);

            HSSFCell celdamarcaT22 = filaTitulo22.createCell((short) 6);
            HSSFRichTextString textoT622 = new HSSFRichTextString(" ");
            celdamarcaT22.setCellValue(textoT622);



            HSSFRow filaTitulo223 = hoja.createRow(3);

            HSSFCell celdaprodT223 = filaTitulo223.createCell((short) 0);
            HSSFRichTextString textoT7223 = new HSSFRichTextString(" ");
            celdaprodT223.setCellValue(textoT7223);

            HSSFCell celdapctT2223 = filaTitulo223.createCell((short) 1);
            HSSFRichTextString textoT32223 = new HSSFRichTextString(" ");
            celdapctT2223.setCellValue(textoT32223);

            HSSFCell celdapctT3223 = filaTitulo223.createCell((short) 2);
            HSSFRichTextString textoT33223 = new HSSFRichTextString(kardex.getDate1());
            celdapctT3223.setCellValue(textoT33223);

            HSSFCell celdalineaT223 = filaTitulo223.createCell((short) 3);
            HSSFRichTextString textoT4223 = new HSSFRichTextString(" ");
            celdalineaT223.setCellValue(textoT4223);

            HSSFCell celdacateT223 = filaTitulo223.createCell((short) 4);
            HSSFRichTextString textoT5223 = new HSSFRichTextString(kardex.getDate2());
            celdacateT223.setCellValue(textoT5223);

            HSSFCell celdamarcaT42223 = filaTitulo223.createCell((short) 5);
            HSSFRichTextString textoT642223 = new HSSFRichTextString(" ");
            celdamarcaT42223.setCellValue(textoT642223);

            HSSFCell celdamarcaT223 = filaTitulo223.createCell((short) 6);
            HSSFRichTextString textoT6223 = new HSSFRichTextString(" ");
            celdamarcaT223.setCellValue(textoT6223);



            HSSFRow filaTitulo2234 = hoja.createRow(4);

            HSSFCell celdaprodT2234 = filaTitulo2234.createCell((short) 0);
            HSSFRichTextString textoT72234 = new HSSFRichTextString("Usuario");
            celdaprodT2234.setCellValue(textoT72234);

            HSSFCell celdapctT22234 = filaTitulo2234.createCell((short) 1);
            HSSFRichTextString textoT322234 = new HSSFRichTextString("Fecha Movimiento");
            celdapctT22234.setCellValue(textoT322234);

            HSSFCell celdapctT32234 = filaTitulo2234.createCell((short) 2);
            HSSFRichTextString textoT332234 = new HSSFRichTextString("Tipo Movimiento");
            celdapctT32234.setCellValue(textoT332234);

            HSSFCell celdalineaT2234 = filaTitulo2234.createCell((short) 3);
            HSSFRichTextString textoT42234 = new HSSFRichTextString("Documento");
            celdalineaT2234.setCellValue(textoT42234);

            HSSFCell celdacateT2234 = filaTitulo2234.createCell((short) 4);
            HSSFRichTextString textoT52234 = new HSSFRichTextString("Cantidad");
            celdacateT2234.setCellValue(textoT52234);

            HSSFCell celdamarcaT422234 = filaTitulo2234.createCell((short) 5);
            HSSFRichTextString textoT6422234 = new HSSFRichTextString("Inventario Inicial");
            celdamarcaT422234.setCellValue(textoT6422234);

            HSSFCell celdamarcaT2234 = filaTitulo2234.createCell((short) 6);
            HSSFRichTextString textoT62234 = new HSSFRichTextString("Inventario Final");
            celdamarcaT2234.setCellValue(textoT62234);




            int cont = 5;
            for(KardexLogDTO element: kardex.getLog()){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getUsuario()+"");
                celdaprod.setCellValue(texto7);

                HSSFCell celdapct2 = fila.createCell((short)1);
                HSSFRichTextString texto32 = new HSSFRichTextString(element.getFecha_MOVIMIENTO()+"");
                celdapct2.setCellValue(texto32);

                HSSFCell celdapct3 = fila.createCell((short)2);
                HSSFRichTextString texto33 = new HSSFRichTextString(element.getTipo_MOVIMIENTO()+"");
                celdapct3.setCellValue(texto33);

                if(element.getDocumento()==null){
                    HSSFCell celdalinea = fila.createCell((short)3);
                    HSSFRichTextString texto4 = new HSSFRichTextString(" ");
                    celdalinea.setCellValue(texto4);
                }else{

                    HSSFCell celdalinea = fila.createCell((short)3);
                    HSSFRichTextString texto4 = new HSSFRichTextString(element.getDocumento()+"");
                    celdalinea.setCellValue(texto4);
                }

                HSSFCell celdacate = fila.createCell((short)4);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getCantidad_MOVIMIENTO()+"");
                celdacate.setCellValue(texto5);

                HSSFCell celdamarca32 = fila.createCell((short)5);
                HSSFRichTextString texto632= new HSSFRichTextString(element.getInventario_INICIAL()+"");
                celdamarca32.setCellValue(texto632);

                HSSFCell celdamarca = fila.createCell((short)6);
                HSSFRichTextString texto6 = new HSSFRichTextString(element.getInventario_FINAL()+"");
                celdamarca.setCellValue(texto6);

                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"kardex.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+".xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"kardex.xls";
        }catch (Exception e){
            //System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, String> postExcelPend(ArrayList<ExcelRowPendDTO> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);

        CreationHelper creationHelper = libro.getCreationHelper();
        CellStyle EstiloDinero = libro.createCellStyle();
        EstiloDinero.setDataFormat(creationHelper.createDataFormat().getFormat(
                "\"$\"#,##0.00_);\\(\"- $\"#,##0.00\\)"));
        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short) 0);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Codigo de Barras");
            celdaprodT.setCellValue(textoT7);

            HSSFCell celdapctT2 = filaTitulo.createCell((short) 1);
            HSSFRichTextString textoT32 = new HSSFRichTextString("Nombre Producto");
            celdapctT2.setCellValue(textoT32);

            HSSFCell celdapctT3 = filaTitulo.createCell((short) 2);
            HSSFRichTextString textoT33 = new HSSFRichTextString("Cantidad");
            celdapctT3.setCellValue(textoT33);

            int cont = 1;

            for(ExcelRowPendDTO element: list){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getOwnbarcode()+"");
                celdaprod.setCellValue(texto7);

                HSSFCell celdapct2 = fila.createCell((short)1);
                HSSFRichTextString texto32 = new HSSFRichTextString(element.getProduct_STORE_NAME()+"");
                celdapct2.setCellValue(texto32);

                HSSFCell celdapct3 = fila.createCell((short)2);
                HSSFRichTextString texto33 = new HSSFRichTextString(element.getQuantity()+"");
                celdapct3.setCellValue(texto33);

                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"reportePendientes.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+".xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"reportePendientes.xls";
        }catch (Exception e){
            //System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, String> postExcel(ArrayList<excelRow> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);

        CreationHelper creationHelper = libro.getCreationHelper();
        CellStyle EstiloDinero = libro.createCellStyle();
        EstiloDinero.setDataFormat(creationHelper.createDataFormat().getFormat(
                "\"$\"#,##0.00_);\\(\"- $\"#,##0.00\\)"));
        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short) 0);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Producto");
            celdaprodT.setCellValue(textoT7);

            HSSFCell celdapctT2 = filaTitulo.createCell((short) 1);
            HSSFRichTextString textoT32 = new HSSFRichTextString("Codigo de Barras");
            celdapctT2.setCellValue(textoT32);

            HSSFCell celdapctT3 = filaTitulo.createCell((short) 2);
            HSSFRichTextString textoT33 = new HSSFRichTextString("Codigo tienda");
            celdapctT3.setCellValue(textoT33);

            HSSFCell celdalineaT = filaTitulo.createCell((short) 3);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Linea");
            celdalineaT.setCellValue(textoT4);

            HSSFCell celdacateT = filaTitulo.createCell((short) 4);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Categoria");
            celdacateT.setCellValue(textoT5);

            HSSFCell celdamarcaT42 = filaTitulo.createCell((short) 5);
            HSSFRichTextString textoT642 = new HSSFRichTextString("Fabricante");
            celdamarcaT42.setCellValue(textoT642);

            HSSFCell celdamarcaT = filaTitulo.createCell((short) 6);
            HSSFRichTextString textoT6 = new HSSFRichTextString("Marca");
            celdamarcaT.setCellValue(textoT6);

            HSSFCell celdaCantT = filaTitulo.createCell((short) 7);
            HSSFRichTextString textoT = new HSSFRichTextString("Cantidad");
            celdaCantT.setCellValue(textoT);

            HSSFCell celdacostT54 = filaTitulo.createCell((short) 8);
            HSSFRichTextString textoT154 = new HSSFRichTextString("IVA%");
            celdacostT54.setCellValue(textoT154);

            HSSFCell celdapctT4 = filaTitulo.createCell((short)9);
            HSSFRichTextString textoT34 = new HSSFRichTextString("Precio");
            celdapctT4.setCellValue(textoT34);

            HSSFCell celdacostT = filaTitulo.createCell((short) 10);
            HSSFRichTextString textoT1 = new HSSFRichTextString("Costo");
            celdacostT.setCellValue(textoT1);

            HSSFCell celdacosttotT = filaTitulo.createCell((short) 11);
            HSSFRichTextString textoT2 = new HSSFRichTextString("Costo Total");
            celdacosttotT.setCellValue(textoT2);

            HSSFCell celdapctT = filaTitulo.createCell((short) 12);
            HSSFRichTextString textoT3 = new HSSFRichTextString("PCT");
            celdapctT.setCellValue(textoT3);

            HSSFCell celdapctT232 = filaTitulo.createCell((short) 13);
            HSSFRichTextString textoT3232 = new HSSFRichTextString("No Disponibles");
            celdapctT232.setCellValue(textoT3232);


            int cont = 1;
            for(excelRow element: list){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getproducto()+"");
                celdaprod.setCellValue(texto7);

                HSSFCell celdapct2 = fila.createCell((short)1);
                HSSFRichTextString texto32 = new HSSFRichTextString(element.getbarcode()+"");
                celdapct2.setCellValue(texto32);

                HSSFCell celdapct3 = fila.createCell((short)2);
                HSSFRichTextString texto33 = new HSSFRichTextString(element.getcodigotienda()+"");
                celdapct3.setCellValue(texto33);

                HSSFCell celdalinea = fila.createCell((short)3);
                HSSFRichTextString texto4 = new HSSFRichTextString(element.getlinea()+"");
                celdalinea.setCellValue(texto4);

                HSSFCell celdacate = fila.createCell((short)4);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getcategoria()+"");
                celdacate.setCellValue(texto5);

                HSSFCell celdamarca32 = fila.createCell((short)5);
                HSSFRichTextString texto632= new HSSFRichTextString(element.getFabricante()+"");
                celdamarca32.setCellValue(texto632);

                HSSFCell celdamarca = fila.createCell((short)6);
                HSSFRichTextString texto6 = new HSSFRichTextString(element.getmarca()+"");
                celdamarca.setCellValue(texto6);

                HSSFCell celdaCant = fila.createCell((short)7);
                celdaCant.setCellValue(element.getcantidad());

                HSSFCell celdacost22 = fila.createCell((short)8);
                celdacost22.setCellValue(element.getIva());

                HSSFCell celdapct4 = fila.createCell((short)9);
                celdapct4.setCellValue(element.getprecio());
                celdapct4.setCellStyle(EstiloDinero);

                HSSFCell celdacost = fila.createCell((short)10);
                celdacost.setCellValue(element.getcosto());
                celdacost.setCellStyle(EstiloDinero);

                HSSFCell celdacosttot = fila.createCell((short)11);
                celdacosttot.setCellValue(element.getcostototal());
                celdacosttot.setCellStyle(EstiloDinero);

                HSSFCell celdapct = fila.createCell((short)12);
                HSSFRichTextString texto3 = new HSSFRichTextString(element.getpct_inventario()+"");
                celdapct.setCellValue(texto3);

                HSSFCell celdacosttotfdfd = fila.createCell((short)13);
                celdacosttotfdfd.setCellValue(element.getNodisp());

                //System.out.println(
                        //element.getcantidad()+","+
                        //element.getcosto()+","+
                        //element.getcostototal()+","+
                        //element.getpct_inventario()+","+
                //element.getlinea()+","+
                //      element.getcategoria()+","+
                //      element.getmarca()+","+
                //      element.getproducto()+".");

                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"inventario.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+".xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"inventario.xls";
        }catch (Exception e){
            //System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> postExcelThirds(ArrayList<thirdReportDataDTO> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        try {
            HSSFRow filaTitulo = hoja.createRow(0);

            CellStyle fuenteBold = libro.createCellStyle();//Create style
            HSSFFont font = libro.createFont();//Create font
            font.setBold(true);//Make font bold
            fuenteBold.setFont(font);//set it to bold

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            celdaprodT.setCellValue("Nombre");
            celdaprodT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            celdalineaT.setCellValue("Documento");
            celdalineaT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            celdacateT.setCellValue("# Transacciones");
            celdacateT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdamarcaT = filaTitulo.createCell((short)3);
            celdamarcaT.setCellValue("Total Transacciones");
            celdamarcaT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdaCantT = filaTitulo.createCell((short)4);
            celdaCantT.setCellValue("Direccion");
            celdaCantT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacostT = filaTitulo.createCell((short)5);
            celdacostT.setCellValue("Telefono");
            celdacostT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacosttotT = filaTitulo.createCell((short)6);
            celdacosttotT.setCellValue("Correo");
            celdacosttotT.setCellStyle(fuenteBold);//Set the style

            int cont = 1;
            CreationHelper creationHelper = libro.getCreationHelper();
            CellStyle EstiloDinero = libro.createCellStyle();
            EstiloDinero.setDataFormat(creationHelper.createDataFormat().getFormat(
                    "\"$\"#,##0.00_);\\(\"- $\"#,##0.00\\)"));
            for(thirdReportDataDTO element: list){
                HSSFRow fila = hoja.createRow(cont);

                //NOMBRE
                HSSFCell celdaprod = fila.createCell((short)0);
                celdaprod.setCellValue(element.getfullname()+"");

                //DOCUMENTO
                HSSFCell celdalinea = fila.createCell((short)1);
                celdalinea.setCellValue(element.getdocument_TYPE()+"-"+element.getdocument_NUMBER());

                //NUMERO TRANSACCIONES
                HSSFCell celdacate = fila.createCell((short)2);
                celdacate.setCellValue(Double.parseDouble(element.getnumventas()+""));

                //TOTAL TRANSACCIONES
                HSSFCell celdamarca = fila.createCell((short)3);
                celdamarca.setCellValue(Double.parseDouble(element.gettotalventas()+""));
                celdamarca.setCellStyle(EstiloDinero);

                //DIRECCIÓN
                HSSFCell celdaCant = fila.createCell((short)4);
                celdaCant.setCellValue(element.getaddress()+"");

                //TELEFONO
                HSSFCell celdacost = fila.createCell((short)5);
                celdacost.setCellValue(element.getphone());

                //CORREO
                HSSFCell celdacosttot = fila.createCell((short)6);
                celdacosttot.setCellValue(element.getmail());

                cont++;
            }
            //AJUSTO LOS ANCHOS DE LAS COLUMNAS
            hoja.setColumnWidth(0,(int)(2048*1.5f));//NOMBRE
            hoja.setColumnWidth(1,(int)(2048*2.5f));//DOCUMENTO
            hoja.setColumnWidth(2,(int)(2048*2.3f));//# TRANSACCIONES
            hoja.setColumnWidth(3,(int)(2048*3));//TOTAL TRANSACCIONES
            hoja.setColumnWidth(4,(int)(2048*3));//DIRECCIÓN
            hoja.setColumnWidth(5,(int)(2048*3));//TELEFONO
            hoja.setColumnWidth(6,(int)(2048*3));//CORREO
            //////////////////////
            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero;
            if(pruebasLocales){
                elFichero = new FileOutputStream("./"+Name+"terceros.xls");
            }else{
                elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"terceros.xls");
            }
            libro.write(elFichero);
            elFichero.close();
            response = Name+"terceros.xls";
        }catch (Exception e){
            //System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> postExcelRotacion(ArrayList<excelRowRot> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);


        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Producto");
            celdaprodT.setCellValue(textoT7);


            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Linea");
            celdalineaT.setCellValue(textoT4);


            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Categoria");
            celdacateT.setCellValue(textoT5);


            HSSFCell celdamarcaT = filaTitulo.createCell((short)3);
            HSSFRichTextString textoT6 = new HSSFRichTextString("Marca");
            celdamarcaT.setCellValue(textoT6);



            HSSFCell celdaCantT = filaTitulo.createCell((short)4);
            HSSFRichTextString textoT = new HSSFRichTextString("Cantidad Actual");
            celdaCantT.setCellValue(textoT);


            HSSFCell celdacostT = filaTitulo.createCell((short)5);
            HSSFRichTextString textoT1 = new HSSFRichTextString("Cantidad Vendida");
            celdacostT.setCellValue(textoT1);


            HSSFCell celdacosttotT = filaTitulo.createCell((short)6);
            HSSFRichTextString textoT2 = new HSSFRichTextString("Dias Rotacion");
            celdacosttotT.setCellValue(textoT2);



            int cont = 1;
            for(excelRowRot element: list){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getproducto()+"");
                celdaprod.setCellValue(texto7);


                HSSFCell celdalinea = fila.createCell((short)1);
                HSSFRichTextString texto4 = new HSSFRichTextString(element.getlinea()+"");
                celdalinea.setCellValue(texto4);


                HSSFCell celdacate = fila.createCell((short)2);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getcategoria()+"");
                celdacate.setCellValue(texto5);


                HSSFCell celdamarca = fila.createCell((short)3);
                HSSFRichTextString texto6 = new HSSFRichTextString(element.getmarca()+"");
                celdamarca.setCellValue(texto6);



                HSSFCell celdaCant = fila.createCell((short)4);
                HSSFRichTextString texto = new HSSFRichTextString(element.getcantidadactual()+"");
                celdaCant.setCellValue(texto);


                HSSFCell celdacost = fila.createCell((short)5);
                HSSFRichTextString texto1 = new HSSFRichTextString(element.getcantidadvendida()+"");
                celdacost.setCellValue(texto1);


                HSSFCell celdacosttot = fila.createCell((short)6);
                HSSFRichTextString texto2 = new HSSFRichTextString(element.getdias_rotacion()+"");
                celdacosttot.setCellValue(texto2);



                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"rotacion.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+"rotacion.xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"rotacion.xls";
        }catch (Exception e){
            //System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> postExcelProductos(ArrayList<excelRowProd> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
       try {
           /*Workbook workbook = WorkbookFactory.create(new File("C:/0TIENDA724/tienda724-backend-tienda/14122019145013productos.xls"));
           Sheet sheet = workbook.getSheetAt(0);

           // Create a DataFormatter to format and get each cell's value as String
           DataFormatter dataFormatter = new DataFormatter();

           Iterator<Sheet> sheetIterator = workbook.sheetIterator();
           //System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
           Iterator<Row> rowIterator = sheet.rowIterator();
           while (rowIterator.hasNext()) {
               Row row = rowIterator.next();

               // Now let's iterate over the columns of the current row
               Iterator<Cell> cellIterator = row.cellIterator();

               while (cellIterator.hasNext()) {
                   Cell cell = cellIterator.next();
                   String cellValue = dataFormatter.formatCellValue(cell);
                   System.out.print(cellValue +" f("+cell.getCellStyle().getDataFormatString() +")" + "\t");
               }
               //System.out.println();
           }*/
            HSSFRow filaTitulo = hoja.createRow(0);

            CellStyle fuenteBold = libro.createCellStyle();//Create style
            HSSFFont font = libro.createFont();//Create font
            font.setBold(true);//Make font bold
            fuenteBold.setFont(font);//set it to bold

            HSSFCell celdaOwn = filaTitulo.createCell((short)0);
            HSSFRichTextString textoT24 = new HSSFRichTextString("Codigo De Barras");
            celdaOwn.setCellValue(textoT24);
           celdaOwn.setCellStyle(fuenteBold);//Set the style


            HSSFCell celdaprodT = filaTitulo.createCell((short)1);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Producto");
            celdaprodT.setCellValue(textoT7);
            celdaprodT.setCellStyle(fuenteBold);//Set the style


            HSSFCell celdalineaT = filaTitulo.createCell((short)2);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Ventas");
            celdalineaT.setCellValue(textoT4);
            celdalineaT.setCellStyle(fuenteBold);//Set the style


            HSSFCell celdacateT = filaTitulo.createCell((short)3);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Costo");
            celdacateT.setCellValue(textoT5);
            celdacateT.setCellStyle(fuenteBold);//Set the style


            HSSFCell celdamarcaT = filaTitulo.createCell((short)4);
            HSSFRichTextString textoT6 = new HSSFRichTextString("Utilidades");
            celdamarcaT.setCellValue(textoT6);
            celdamarcaT.setCellStyle(fuenteBold);//Set the style



            HSSFCell celdaCantT = filaTitulo.createCell((short)5);
            HSSFRichTextString textoT = new HSSFRichTextString("Margen");
            celdaCantT.setCellValue(textoT);
            celdaCantT.setCellStyle(fuenteBold);//Set the style


            HSSFCell celdacostT = filaTitulo.createCell((short)6);
            HSSFRichTextString textoT1 = new HSSFRichTextString("# Ventas");
            celdacostT.setCellValue(textoT1);
            celdacostT.setCellStyle(fuenteBold);//Set the style



           HSSFCell celdamarcaT1 = filaTitulo.createCell((short)7);
           HSSFRichTextString textoT61 = new HSSFRichTextString("Linea");
           celdamarcaT1.setCellValue(textoT61);
           celdamarcaT1.setCellStyle(fuenteBold);//Set the style


           HSSFCell celdaCantT2 = filaTitulo.createCell((short)8);
           HSSFRichTextString textoT2 = new HSSFRichTextString("Categoria");
           celdaCantT2.setCellValue(textoT2);
           celdaCantT2.setCellStyle(fuenteBold);//Set the style


           HSSFCell celdacostT3 = filaTitulo.createCell((short)9);
           HSSFRichTextString textoT13 = new HSSFRichTextString("Marca");
           celdacostT3.setCellValue(textoT13);
           celdacostT3.setCellStyle(fuenteBold);//Set the style

            CreationHelper creationHelper = libro.getCreationHelper();
            CellStyle EstiloDinero = libro.createCellStyle();
            EstiloDinero.setDataFormat(creationHelper.createDataFormat().getFormat(
                    "\"$\"#,##0.00_);\\(\"- $\"#,##0.00\\)"));
            int cont = 1;
            for(excelRowProd element: list) {

                HSSFRow fila = hoja.createRow(cont);

                //CODIGO DEL PRODUCTO
                HSSFCell celdaown= fila.createCell((short)0);
                celdaown.setCellValue(element.getownbarcode()+"");
                celdaown.setCellType(CellType.STRING);

                //CATEGORIA/NOMBRE PRODUCTO
                HSSFCell celdaprod = fila.createCell((short)1);
                celdaprod.setCellValue(element.getProduct());

                //PRECIO DE VENTA
                HSSFCell celdalinea = fila.createCell((short)2);
                celdalinea.setCellValue(Double.parseDouble(element.getventa()+""));
                celdalinea.setCellStyle(EstiloDinero);

                //COSTO DEL PRODUCTO
                HSSFCell celdacate = fila.createCell((short)3);
                celdacate.setCellValue(Double.parseDouble(element.getcosto()+""));
                celdacate.setCellStyle(EstiloDinero);

                //UTILIDAD PRODUCTO
                HSSFCell celdamarca = fila.createCell((short)4);
                celdamarca.setCellValue(Double.parseDouble(element.getutilidad()+""));
                celdamarca.setCellStyle(EstiloDinero);

                //MARGEN
                HSSFCell celdaCant = fila.createCell((short)5);
                celdaCant.setCellValue(Double.parseDouble(element.getpct_margen()+""));
                celdaCant.setCellStyle(EstiloDinero);

                //NUMERO DE VENTAS
                HSSFCell celdacost = fila.createCell((short)6);
                celdacost.setCellValue(Double.parseDouble(element.getnumventas()+""));


                //LINEA
                HSSFCell celdamarca1 = fila.createCell((short)7);
                celdamarca1.setCellValue(element.getLinea()+"");

                //CATEGORIA
                HSSFCell celdaCant1 = fila.createCell((short)8);
                celdaCant1.setCellValue(element.getcategoria()+"");

                //MARCA
                HSSFCell celdacost1 = fila.createCell((short)9);
                celdacost1.setCellValue(element.getMarca()+"");

                cont++;
            }

            //AJUSTO LOS ANCHOS DE LAS COLUMNAS
            hoja.setColumnWidth(0,(int)(2048*2));
            hoja.setColumnWidth(1,2048*7);
            hoja.setColumnWidth(2,(int)(2048*1.7f));
            hoja.setColumnWidth(3,(int)(2048*1.7f));
            hoja.setColumnWidth(4,(int)(2048*1.7f));
            hoja.setColumnWidth(5,(int)(2048*1.7f));
            hoja.setColumnWidth(6,(int)(2048*1.4f));
            hoja.setColumnWidth(7,(int)(2048*1.4f));
            hoja.setColumnWidth(8,(int)(2048*1.4f));
            hoja.setColumnWidth(9,(int)(2048*1.4f));
            /////////////
            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero;
            if(pruebasLocales){
                elFichero = new FileOutputStream("./"+Name+"productos.xls");
            }else{
                elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"productos.xls");
            }
            libro.write(elFichero);
            elFichero.close();
            response = Name+"productos.xls";
        }catch (Exception e){
            e.printStackTrace();
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> postExcelCategorias(ArrayList<excelRowProd> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        try {
            HSSFRow filaTitulo = hoja.createRow(0);

            CellStyle fuenteBold = libro.createCellStyle();//Create style
            HSSFFont font = libro.createFont();//Create font
            font.setBold(true);//Make font bold
            fuenteBold.setFont(font);//set it to bold

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            celdaprodT.setCellValue("Linea");
            celdaprodT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdaprodT2 = filaTitulo.createCell((short)1);
            celdaprodT2.setCellValue("Categoria");
            celdaprodT2.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdalineaT = filaTitulo.createCell((short)2);
            celdalineaT.setCellValue("Ventas");
            celdalineaT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacateT = filaTitulo.createCell((short)3);
            celdacateT.setCellValue("Costo");
            celdacateT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdamarcaT = filaTitulo.createCell((short)4);
            celdamarcaT.setCellValue("Utilidades");
            celdamarcaT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdaCantT = filaTitulo.createCell((short)5);
            celdaCantT.setCellValue("Margen");
            celdaCantT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacostT = filaTitulo.createCell((short)6);
            celdacostT.setCellValue("# Ventas");
            celdacostT.setCellStyle(fuenteBold);//Set the style

            int cont = 1;
            DataFormat format = libro.createDataFormat();
            CreationHelper creationHelper = libro.getCreationHelper();
            CellStyle EstiloDinero = libro.createCellStyle();
            EstiloDinero.setDataFormat(creationHelper.createDataFormat().getFormat(
                    "\"$\"#,##0.00_);\\(\"- $\"#,##0.00\\)"));
            for(excelRowProd element: list){
                HSSFRow fila = hoja.createRow(cont);

                //NOMBRE/LINEA Categoria
                HSSFCell celdaprod = fila.createCell((short)0);
                celdaprod.setCellValue(element.getLinea());
                celdaprod.setCellStyle(EstiloDinero);

                //NOMBRE/CATEGORIA PRODUCTO
                HSSFCell celdaprod2 = fila.createCell((short)1);
                celdaprod2.setCellValue(element.getcategoria());
                celdaprod2.setCellStyle(EstiloDinero);

                //PRECIO VENTAS
                HSSFCell celdalinea = fila.createCell((short)2);
                celdalinea.setCellValue(element.getventa());
                celdalinea.setCellStyle(EstiloDinero);

                //COSTOS PRODUCTOS
                HSSFCell celdacate = fila.createCell((short)3);
                celdacate.setCellValue(element.getcosto());
                celdacate.setCellStyle(EstiloDinero);

                //UTILIDADES PRODUCTOS
                HSSFCell celdamarca = fila.createCell((short)4);
                celdamarca.setCellValue(element.getutilidad());
                celdamarca.setCellStyle(EstiloDinero);

                //MARGEN PRODUCTOS
                HSSFCell celdaCant = fila.createCell((short)5);
                celdaCant.setCellValue(element.getpct_margen());
                celdaCant.setCellStyle(EstiloDinero);

                //NUMERO DE VENTAS
                HSSFCell celdacost = fila.createCell((short)6);
                celdacost.setCellValue(element.getnumventas());

                cont++;
            }

            //AJUSTO LOS ANCHOS DE LAS COLUMNAS
            hoja.setColumnWidth(0,(int)(2048*3));
            hoja.setColumnWidth(1,2048*2);
            hoja.setColumnWidth(2,(int)(2048*2));
            hoja.setColumnWidth(3,(int)(2048*2));
            hoja.setColumnWidth(4,(int)(2048*2));
            hoja.setColumnWidth(5,(int)(2048*1.5f));
            //////////////////////
            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero;
            if(pruebasLocales){
                elFichero = new FileOutputStream("./"+Name+"categorias.xls");
            }else{
                elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"categorias.xls");
            }
            libro.write(elFichero);
            elFichero.close();
            response = Name+"categorias.xls";
        }catch (Exception e){
            //System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> postExcelFacturas(ArrayList<excelRowFact> list,Long tipofactura){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            CellStyle fuenteBold = libro.createCellStyle();//Create style
            HSSFFont font = libro.createFont();//Create font
            font.setBold(true);//Make font bold
            fuenteBold.setFont(font);//set it to bold

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            celdaprodT.setCellValue("Consecutivo");
            celdaprodT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            celdalineaT.setCellValue("Fecha Factura");
            celdalineaT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            celdacateT.setCellValue("Ventas");
            celdacateT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdamarcaT = filaTitulo.createCell((short)3);
            celdamarcaT.setCellValue("Costo");
            celdamarcaT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdamarcaTax = filaTitulo.createCell((short)4);
            celdamarcaTax.setCellValue("Impuesto");
            celdamarcaTax.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdaCantT = filaTitulo.createCell((short)5);
            celdaCantT.setCellValue("Utilidades");
            celdaCantT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacostT = filaTitulo.createCell((short)6);
            celdacostT.setCellValue("Margen Venta");
            celdacostT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacostT2 = filaTitulo.createCell((short)7);
            celdacostT2.setCellValue("Margen Costo");
            celdacostT2.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacostT3 = filaTitulo.createCell((short)8);
            celdacostT3.setCellValue("Caja");
            celdacostT3.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacostT4 = filaTitulo.createCell((short)9);
            celdacostT4.setCellValue("Cajero");
            celdacostT4.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacostf = filaTitulo.createCell((short)10);
            celdacostf.setCellValue("Tipo Factura");
            celdacostf.setCellStyle(fuenteBold);//Set the style

            HSSFCell domi = filaTitulo.createCell((short)11);
            domi.setCellValue("Domicilario");
            domi.setCellStyle(fuenteBold);//Set the style

            int cont = 1;
            CreationHelper creationHelper = libro.getCreationHelper();
            CellStyle EstiloDinero = libro.createCellStyle();
            EstiloDinero.setDataFormat(creationHelper.createDataFormat().getFormat(
                    "\"$\"#,##0.00_);\\(\"- $\"#,##0.00\\)"));
            for(excelRowFact element: list) {
                HSSFRow fila = hoja.createRow(cont);

                //EL CONSECUTIVO
                HSSFCell celdaprod = fila.createCell((short) 0);
                celdaprod.setCellValue(element.getfullname() + "-" + element.getprefix_BILL() + "-" + element.getconsecutive());

                //FECHA FACTURA
                HSSFCell celdalinea = fila.createCell((short) 1);
                celdalinea.setCellValue(element.getpurchase_DATE());

                //VENTAS
                HSSFCell celdacate = fila.createCell((short) 2);
                celdacate.setCellValue(Double.parseDouble(element.getventa()+""));
                celdacate.setCellStyle(EstiloDinero);

                //COSTOS
                HSSFCell celdamarca = fila.createCell((short) 3);
                celdamarca.setCellValue(Double.parseDouble(element.getcosto()+""));
                celdamarca.setCellStyle(EstiloDinero);

                //IMPUESTOS
                HSSFCell celdamarcaIm = fila.createCell((short) 4);
                celdamarcaIm.setCellValue(Double.parseDouble(element.gettax()+""));
                celdamarcaIm.setCellStyle(EstiloDinero);

                //UTILIDADES
                HSSFCell celdaCant = fila.createCell((short) 5);
                if(tipofactura==1){
                    celdaCant.setCellValue(Double.parseDouble(element.getutilidad()+""));
                    celdaCant.setCellStyle(EstiloDinero);
                }else{
                    celdaCant.setCellValue(" ");
                }
                //MARGEN VENTA
                HSSFCell celdacost = fila.createCell((short) 6);
                if(tipofactura==1){
                    celdacost.setCellValue(element.getpct_margen_venta() + " %");
                }else{
                    celdacost.setCellValue(" ");
                }

                //MARGEN COSTO
                HSSFCell celdacost2 = fila.createCell((short) 7);

                if(tipofactura==1){
                    celdacost2.setCellValue(element.getpct_margen_costo() + " %");
                }else{
                    celdacost2.setCellValue(" ");
                }

                //CAJA
                HSSFCell celdacost3 = fila.createCell((short) 8);

                if(tipofactura==1){
                    celdacost3.setCellValue(element.getcaja() + "");
                }else{
                    celdacost3.setCellValue(" ");
                }

                //CAJERO
                HSSFCell celdacost4 = fila.createCell((short)9);
                celdacost4.setCellValue(element.getcajero()+"");

                //TIPO FACTURA
                HSSFCell celdacostff = fila.createCell((short)10);
                String a = element.getTipofactura();
                if (Integer.parseInt(a)==0){
                    celdacostff.setCellValue("Anulada por devolucion");
                }else{
                    if (Integer.parseInt(a)==1){
                        celdacostff.setCellValue("Efectivo");
                    }else{
                        if (Integer.parseInt(a)==2){
                            celdacostff.setCellValue("T. Debito");
                        }else{
                            if(Integer.parseInt(a)==3){
                                celdacostff.setCellValue("T. Credito");
                            }
                        }
                    }
                }
                //DOMICILIARIO
                HSSFCell celdadomi = fila.createCell((short)11);

                if(tipofactura==1){
                    celdadomi.setCellValue(element.getdomiciliario());
                }else{
                    celdadomi.setCellValue(" ");
                }
                cont++;
            }
            //AJUSTO LOS ANCHOS DE LAS COLUMNAS
            hoja.setColumnWidth(0,(int)(2048*5f));//consecutivo
            hoja.setColumnWidth(1,(int)(2048*3f));//fecha
            hoja.setColumnWidth(2,(int)(2048*2f));//ventas
            hoja.setColumnWidth(3,(int)(2048*2f));//costos
            hoja.setColumnWidth(4,(int)(2048*2f));//utilidades
            hoja.setColumnWidth(5,(int)(2048*1.5f));//margen venta
            hoja.setColumnWidth(6,(int)(2048*1.5f));//margen costo
            hoja.setColumnWidth(7,(int)(2048*1f));//caja
            hoja.setColumnWidth(8,(int)(2048*3f));//cajero
            hoja.setColumnWidth(9,(int)(2048*1.5f));//tipo factura
            hoja.setColumnWidth(10,(int)(2048*5f));//domicilario
            /////////////
            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero;
            if(pruebasLocales){
                elFichero = new FileOutputStream("./"+Name+"facturas.xls");
            }else{
                elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"facturas.xls");
            }
            libro.write(elFichero);
            elFichero.close();
            response = Name+"facturas.xls";
        }catch (Exception e){
            //System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            //System.out.println(e.toString());
            //System.out.println(messages_error(e.toString()));
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> postExcelCaja(ArrayList<excelRowCaja> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        try {
            HSSFRow filaTitulo = hoja.createRow(0);

            CellStyle fuenteBold = libro.createCellStyle();//Create style
            HSSFFont font = libro.createFont();//Create font
            font.setBold(true);//Make font bold
            fuenteBold.setFont(font);//set it to bold

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            celdaprodT.setCellValue("Consecutivo");
            celdaprodT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            celdalineaT.setCellValue("Fecha Apertura");
            celdalineaT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            celdacateT.setCellValue("Fecha Cierre");
            celdacateT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacateT2 = filaTitulo.createCell((short)3);
            celdacateT2.setCellValue("Balance");
            celdacateT2.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdamarcaT = filaTitulo.createCell((short)4);
            celdamarcaT.setCellValue("Cajero");
            celdamarcaT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdaCantT = filaTitulo.createCell((short)5);
            celdaCantT.setCellValue("Caja");
            celdaCantT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacostT = filaTitulo.createCell((short)6);
            celdacostT.setCellValue("Notas Cierre");
            celdacostT.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacostT2 = filaTitulo.createCell((short)7);
            celdacostT2.setCellValue("Fecha Movimiento");
            celdacostT2.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacostT3 = filaTitulo.createCell((short)8);
            celdacostT3.setCellValue("Valor");
            celdacostT3.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacostT4 = filaTitulo.createCell((short)9);
            celdacostT4.setCellValue("Naturaleza");
            celdacostT4.setCellStyle(fuenteBold);//Set the style

            HSSFCell celdacostT5 = filaTitulo.createCell((short)10);
            celdacostT5.setCellValue("Notas Movimiento");
            celdacostT5.setCellStyle(fuenteBold);//Set the style

            int cont = 1;
            CreationHelper creationHelper = libro.getCreationHelper();
            CellStyle EstiloDinero = libro.createCellStyle();
            EstiloDinero.setDataFormat(creationHelper.createDataFormat().getFormat(
                    "\"$\"#,##0.00_);\\(\"- $\"#,##0.00\\)"));
            for(excelRowCaja element: list){
                HSSFRow fila = hoja.createRow(cont);

                //CONSECUTIVO
                HSSFCell celdaprod = fila.createCell((short)0);
                celdaprod.setCellValue(element.getconsecutive()+"");

                //FECHA APERTURA
                HSSFCell celdalinea = fila.createCell((short)1);
                celdalinea.setCellValue(element.getstarting_DATE()+"");

                //FECHA DE CIERRE
                HSSFCell celdacate = fila.createCell((short)2);
                celdacate.setCellValue(element.getclosing_DATE()+"");

                //BALANCE
                HSSFCell celdacate2 = fila.createCell((short)3);
                celdacate2.setCellValue(Double.parseDouble(element.getBALANCE()+""));
                celdacate2.setCellStyle(EstiloDinero);

                //NOMBRE COMPLETO
                HSSFCell celdamarca = fila.createCell((short)4);
                celdamarca.setCellValue(element.getfullname()+"");

                //CAJA
                HSSFCell celdaCant = fila.createCell((short)5);
                celdaCant.setCellValue(Double.parseDouble(element.getcaja_NUMBER()+""));

                //NOTAS
                HSSFCell celdacost = fila.createCell((short)6);
                celdacost.setCellValue(element.getnotes()+"");

                //FECHA DE MOVIMIENTO
                HSSFCell celdacost2 = fila.createCell((short)7);
                celdacost2.setCellValue(element.getMOVEMENT_DATE()+"");

                //VALOR
                HSSFCell celdacost3 = fila.createCell((short)8);
                celdacost3.setCellValue(Double.parseDouble(element.getVALOR()+""));
                celdacost3.setCellStyle(EstiloDinero);

                //NATURALEZA
                if(element.getNATURALEZA().equals("C")){
                    HSSFCell celdacost4 = fila.createCell((short)9);
                    celdacost4.setCellValue("Egreso a la Caja");
                }
                if(element.getNATURALEZA().equals("D")){
                    HSSFCell celdacost4 = fila.createCell((short)9);
                    celdacost4.setCellValue("Ingreso a la Caja");
                }
                //NOTAS MOVIMIENTO
                HSSFCell celdacost5 = fila.createCell((short)10);
                celdacost5.setCellValue(element.getNOTAS()+"");

                cont++;
            }
            //AJUSTO LOS ANCHOS DE LAS COLUMNAS
            hoja.setColumnWidth(0,(int)(2048*1.8f));//CONSEVUTIVO
            hoja.setColumnWidth(1,(int)(2048*2.5f));//FECHA APERTURA
            hoja.setColumnWidth(2,(int)(2048*2.5f));//FECHA CIERRE
            hoja.setColumnWidth(3,(int)(2048*2.5f));//BALANCE
            hoja.setColumnWidth(4,(int)(2048*4));//CAJERO
            hoja.setColumnWidth(5,(int)(2048*1));//CAJA
            hoja.setColumnWidth(6,(int)(2048*2));//NOTAS CIERRE
            hoja.setColumnWidth(7,(int)(2048*2.5f));//FECHA MOVIMIENTO
            hoja.setColumnWidth(8,(int)(2048*2.5f));//VALOR
            hoja.setColumnWidth(9,(int)(2048*2.4f));//NATURALEZA MOVIMIENTO
            hoja.setColumnWidth(10,(int)(2048*4));//NOTAS MOVIMIENTO
            ////////////
            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero;
            if(pruebasLocales){
                elFichero = new FileOutputStream("./"+Name+"cajas.xls");
            }else{
                elFichero = new FileOutputStream("/usr/share/nginx/html/reportes/"+Name+"cajas.xls");
            }
            libro.write(elFichero);
            elFichero.close();
            response = Name+"cajas.xls";
        }catch (Exception e){
            //System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> getNoDisponibles(Long idbill,
                                                     List<Long> listStore,
                                                     String code) {
        Long dataResponse = reportsDAO.getNoDisponibles(new Long(idbill),
                listStore,
                code);
        if(dataResponse == null){
            dataResponse = new Long(0);
        }
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String> getIvaStore(Long idstore) {
        String dataResponse = reportsDAO.getIvaStore(idstore);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException,List <Data>> getData(
            Long id_store) {
        List <Data> dataResponse = reportsDAO.getData(
                id_store);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List <NoDisp>> getSumNoDisponibles(
                                                     List<Long> listStore) {
        List <NoDisp> dataResponse = reportsDAO.getSumNoDisponibles(
                listStore);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public  Long putNewProduct(Long idps,
                                                  Long idproduct,
                                                  Long idcode,
                                                  Long idms,
                                                  Long idb,
                                                  Long idcat,
                                                  String psname) {
        try {
            reportsDAO.putNewProduct(idps,
                    idproduct,
                    idcode,
                    idms,
                    idb,
                    idcat,
                    psname);
            return new Long (1);
        }catch (Exception e){
            e.printStackTrace();
            return new Long (0);
        }
    }

    public  Long GENERAR_PEDIDOS_STOCK_MINIMO(Long idstorep,
                               Long weekday) {
        try {
            reportsDAO.GENERAR_PEDIDOS_STOCK_MINIMO(idstorep,
                    weekday);
            return new Long (1);
        }catch (Exception e){
            e.printStackTrace();
            return new Long (0);
        }
    }


    public Either<IException,List<AdminUser>> getAdminUserList(Long idrol,Long id_store) {
        List<AdminUser> dataResponse = reportsDAO.getAdminUserList(idrol,id_store);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }
}
