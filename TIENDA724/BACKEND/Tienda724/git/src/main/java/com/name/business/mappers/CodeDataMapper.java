package com.name.business.mappers;

import com.name.business.entities.codeData;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CodeDataMapper implements ResultSetMapper<codeData>{

    @Override
    public codeData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new codeData(
                resultSet.getString("CODE"),
                resultSet.getString("PRODUCT_NAME"),
                resultSet.getString("MUN_NAME"),
                resultSet.getString("BRAND_NAME")

        );
    }
}