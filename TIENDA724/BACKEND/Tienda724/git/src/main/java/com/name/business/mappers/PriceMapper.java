package com.name.business.mappers;


import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.MUName;
import com.name.business.entities.Price;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PriceMapper implements ResultSetMapper<Price> {
    @Override
    public Price map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Price(
                resultSet.getString("PRICE_DESCRIPTION"),
                resultSet.getDouble("PRICE"),
                resultSet.getDouble("pct_descuento")
        );
    }
}
