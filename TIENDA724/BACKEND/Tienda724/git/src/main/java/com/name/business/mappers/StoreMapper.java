package com.name.business.mappers;

import com.name.business.entities.Store;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StoreMapper implements ResultSetMapper<Store>{

    @Override
    public Store map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Store(
                resultSet.getLong("ID_STORE"),
                resultSet.getString("STORE_NAME"),
                resultSet.getLong("ID_DIRECTORY"),
                resultSet.getLong("STORE_NUMBER"),
                resultSet.getLong("ENTRADA_CONSECUTIVE_INITIAL"),
                resultSet.getLong("SALIDA_CONSECUTIVE_INITIAL")

        );
    }
}