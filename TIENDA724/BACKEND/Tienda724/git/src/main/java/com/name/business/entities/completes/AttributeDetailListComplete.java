package com.name.business.entities.completes;

import com.name.business.entities.AttributeList;
import com.name.business.entities.CommonState;

import java.util.List;

public class AttributeDetailListComplete {
    private Long id_attribute_list;
    private CommonState state;
    private List<AttributeComplete> attributes;

    public AttributeDetailListComplete(Long id_attribute_list, CommonState state, List<AttributeComplete> attributes) {
        this.id_attribute_list = id_attribute_list;
        this.state = state;
        this.attributes = attributes;
    }

    public List<AttributeComplete> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<AttributeComplete> attributes) {
        this.attributes = attributes;
    }

    public Long getId_attribute_list() {
        return id_attribute_list;
    }

    public void setId_attribute_list(Long id_attribute_list) {
        this.id_attribute_list = id_attribute_list;
    }

    public CommonState getState() {
        return state;
    }

    public void setState(CommonState state) {
        this.state = state;
    }
}
