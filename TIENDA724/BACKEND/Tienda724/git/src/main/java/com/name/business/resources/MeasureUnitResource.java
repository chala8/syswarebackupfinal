package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.MeasureUnitBusiness;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.MeasureUnit;
import com.name.business.representations.MeasureUnitDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.util.Date;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("measure-units")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class MeasureUnitResource {
    private MeasureUnitBusiness measureUnitBusiness;

    public MeasureUnitResource(MeasureUnitBusiness measureUnitBusiness) {
        this.measureUnitBusiness=measureUnitBusiness;
    }

    /**
     *
     * @param measureUnitDTO
     * @return status
     */
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postProductResource(MeasureUnitDTO measureUnitDTO){
        Response response;
        Either<IException, Long> mailEither = measureUnitBusiness.createMeasureUnit(measureUnitDTO);

        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deletePersonThirdResource(@PathParam("id") Long id_measure_unit){
        Response response;
        Either<IException, Long> allViewOffertsEither = measureUnitBusiness.deleteMeasureUnit(id_measure_unit);

        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getMeasureUnit(
            @QueryParam("id_measure_unit") Long id_measure_unit,
            @QueryParam("id_measure_unit_father") Long id_measure_unit_father,
            @QueryParam("id_common_measure_unit") Long id_common_measure_unit,
            @QueryParam("name_measure_unit") String name_measure_unit,
            @QueryParam("description_measure_unit") String description_measure_unit,

            @QueryParam("id_state_measure_unit") Long id_state_measure_unit,
            @QueryParam("state_measure_unit") Integer state_measure_unit,
            @QueryParam("creation_measure_unit") Date creation_measure_unit,
            @QueryParam("modify_measure_unit") Date modify_measure_unit
    )
    {
        Response response;

        Either<IException, List<MeasureUnit>> getMeasureUnit = measureUnitBusiness.getMeasureUnit(
                new MeasureUnit(id_measure_unit, id_measure_unit_father,
                        new Common(
                                id_common_measure_unit,
                                name_measure_unit,
                                description_measure_unit
                        ),

                        new CommonState(
                                id_state_measure_unit,
                                state_measure_unit,
                                creation_measure_unit,
                                modify_measure_unit
                        )

                )
        );

        if (getMeasureUnit.isRight()){
            //System.out.println(getMeasureUnit.right().value().size());
            response=Response.status(Response.Status.OK).entity(getMeasureUnit.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getMeasureUnit);
        }
        return response;
    }



    @Path("/{id}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateMeasureUnitResource(@PathParam("id") Long id_measure_unit, MeasureUnitDTO measureUnitDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = measureUnitBusiness.updateMeasureUnit(id_measure_unit, measureUnitDTO);

        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

}
