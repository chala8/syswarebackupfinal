package com.name.business.mappers;


import com.name.business.entities.Directory;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DirectoryMapper implements ResultSetMapper<Directory> {

    @Override
    public Directory map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Directory(
                resultSet.getString("DIR_NAME"),
                resultSet.getString("ADDRESS"),
                resultSet.getString("WEBPAGE"),
                resultSet.getLong("ID_CITY"),
                resultSet.getDouble("LATITUD"),
                resultSet.getDouble("LONGITUD")
        );
    }
}
