package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;

public class CodeNameDTO {

    private String code;
    private Long id_product;
    private Long id_measure_unit;
    private Long suggested_price;
    private Long id_third;
    private Long id_brand;
    @JsonCreator
    public CodeNameDTO(@JsonProperty("code") String code,
                      @JsonProperty("id_product") Long id_product,
                      @JsonProperty("id_measure_unit") Long id_measure_unit,
                      @JsonProperty("suggested_price") Long suggested_price,
                       @JsonProperty("id_third") Long id_third,
                       @JsonProperty("id_brand") Long id_brand) {
        this.code = code;
        this.id_product = id_product;
        this.id_measure_unit = id_measure_unit;
        this.suggested_price = suggested_price;
        this.id_third = id_third;
        this.id_brand = id_brand;

    }

    //------------------------------------------------------------------------------------------

    public String getcode() {
        return code;
    }

    public void setcode(String code) {
        this.code = code;
    }

    //------------------------------------------------------------------------------------------

    public Long getid_product() {
        return id_product;
    }

    public void setid_product(Long id_product) {
        this.id_product = id_product;
    }

    //------------------------------------------------------------------------------------------

    public Long getid_measure_unit() {
        return id_measure_unit;
    }

    public void setid_measure_unit(Long id_measure_unit) {
        this.id_measure_unit = id_measure_unit;
    }

    //------------------------------------------------------------------------------------------

    public Long getsuggested_price() {
        return suggested_price;
    }

    public void setsuggested_price(Long suggested_price) {
        this.suggested_price = suggested_price;
    }

    //------------------------------------------------------------------------------------------

    public Long getid_third() {
        return id_third;
    }

    public void setid_third(Long id_third) {
        this.id_third = id_third;
    }

    //------------------------------------------------------------------------------------------

    public Long getid_brand() {
        return id_brand;
    }

    public void setid_brand(Long id_brand) {
        this.id_brand = id_brand;
    }


}
