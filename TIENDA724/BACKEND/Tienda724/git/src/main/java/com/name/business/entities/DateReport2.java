package com.name.business.entities;
import org.skife.jdbi.cglib.core.Local;

import java.time.LocalDate;

import java.util.Date;

public class DateReport2 {
    private String field;
    private Long subtotal;
    private Long tax;
    private Long total;



    public DateReport2(String field,Long subtotal, Long tax, Long total) {
        this.field = field;
        this.subtotal = subtotal;
        this.tax = tax;
        this.total = total;
    }

    public String getfield() {
        return field;
    }

    public void setfield(String field) {
        this.field = field;
    }

    //------------------------------------------------------------------------------

    public Long getsubtotal() {
        return subtotal;
    }

    public void setsubtotal(Long subtotal) {
        this.subtotal = subtotal;
    }

    //------------------------------------------------------------------------------

    public Long gettax() {
        return tax;
    }

    public void settax(Long tax) {
        this.tax = tax;
    }

    //------------------------------------------------------------------------------

    public Long gettotal() {
        return total;
    }

    public void settotal(Long total) {
        this.total = total;
    }

    //------------------------------------------------------------------------------


}


