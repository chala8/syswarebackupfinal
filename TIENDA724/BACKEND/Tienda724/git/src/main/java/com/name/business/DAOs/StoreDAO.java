package com.name.business.DAOs;

import com.name.business.entities.Storage;
import com.name.business.entities.Store;
import com.name.business.entities.Register;
import com.name.business.entities.Directory;
import com.name.business.entities.State;
import com.name.business.entities.Phone;
import com.name.business.entities.*;
import com.name.business.mappers.*;

import com.name.business.representations.InventoryDetailDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;



public interface StoreDAO {

    @RegisterMapper(StoreMapper.class)
    @SqlQuery("select s.id_store,description store_name,\n" +
            "       id_directory,store_number,entrada_consecutive_initial,\n" +
            "       salida_consecutive_initial\n" +
            " from tienda724.store s\n" +
            " where s.id_third=:id_third")
    List<Store> getStoreByThird2(@Bind("id_third") Long id_third);

    @RegisterMapper(StoreMapper.class)
    @SqlQuery("select s.id_store,description store_name,\n" +
            "       id_directory,store_number,entrada_consecutive_initial,\n" +
            "       salida_consecutive_initial\n" +
            " from tienda724.store s, tienda724.store_empleado se\n" +
            " where se.id_third_employee=:id_third and se.ID_STORE=s.ID_STORE ORDER BY STORE_NUMBER")
    List<Store> getStoreByThird(@Bind("id_third") Long id_third);


    @RegisterMapper(StoreMapper.class)
    @SqlQuery("select id_store,description store_name,id_directory,store_number,s.ENTRADA_CONSECUTIVE_INITIAL, s.salida_consecutive_initial from tienda724.param_pedidos pp,tienda724.store s\n" +
            "where pp.id_store_client=s.id_store and id_store_provider=:idstore")
    List<Store> getStoresClients(@Bind("idstore") Long idstore);



    @RegisterMapper(StoreMapper.class)
    @SqlQuery(" select id_directory from tienda724.store where id_store=:id_store ")
    List<Long> getIdDirectoryByIdStore (@Bind("id_store") Long id_store);

    @RegisterMapper(VendorMapper.class)
    @SqlQuery(" select hev.ID_THIRD idthirdvendedor,v.NOMBRE||' '||v.apellido vendedor\n" +
            "from otros.ruteros r,OTROS.VENDEDORES v,OTROS.HOMOLOGACION_ECOM_VENDEDOR hev,OTROS.HOMOLOGACION_ECOM_TERCEROS HET\n" +
            "where r.CODIGO_VENDEDOR_ECOM=v.codigo and r.CODIGO_VENDEDOR_ECOM=hev.CODIGO_VENDEDOR_ECOM\n" +
            "  AND HET.CODIGOCLIENTEECOM=r.CODIGOCLIENTEECOM\n" +
            "  AND HET.ID_THIRD=:id_third ")
    List<Vendor> getVendors (@Bind("id_third") Long id_third);

    //-------------------------------------------------------------------------------------------

    @SqlUpdate(" INSERT INTO tienda724.store(id_store,id_third,description,id_common_state, " +
               " id_directory,store_number, " +
               " entrada_consecutive_initial,salida_consecutive_initial) " +
               " VALUES(tienda724.store_seq.nextval,:id_third,:description,null,:id_directory," +
               " :store_number,:entrada_consecutive_initial,:salida_consecutive_initial) ")
    void postStore(@Bind("id_third") Long id_third,
                        @Bind("description") String description,
                        @Bind("id_directory") Long id_directory,
                        @Bind("store_number") Long store_number,
                        @Bind("entrada_consecutive_initial") Long entrada_consecutive_initial,
                        @Bind("salida_consecutive_initial") Long salida_consecutive_initial);

    @SqlUpdate(" UPDATE tienda724.store " +
               " SET description=:description,store_number=:store_number," +
               " entrada_consecutive_initial=:entrada_consecutive_initial," +
               " salida_consecutive_initial=:salida_consecutive_initial" +
               " WHERE id_store=:id_store")
    void putStore(@Bind("id_third") Long id_third,
                        @Bind("description") String description,
                        @Bind("store_number") Long store_number,
                        @Bind("entrada_consecutive_initial") Long entrada_consecutive_initial,
                        @Bind("salida_consecutive_initial") Long salida_consecutive_initial,
                        @Bind("id_store") Long id_store);


    //-----------------------------------------------------------------------------------------

    @RegisterMapper(RegisterBoxMapper.class)
    @SqlQuery(" select id_caja,description caja_name,caja_number " +
            " from tienda724.caja " +
            " where id_store=:id_store ")
    List<Register> getCajaByStore (@Bind("id_store") Long id_store);

    @SqlUpdate(" insert into tienda724.caja(id_caja,id_store,description,caja_number) " +
            " values(tienda724.caja_seq.nextval,:id_store,:description,:caja_number) ")
    void postCaja(@Bind("id_store") Long id_store,
                  @Bind("description") String description,
                  @Bind("caja_number") Long caja_number);

    @SqlUpdate(" update tienda724.caja " +
            " set description=:description,caja_number=:caja_number " +
            " where id_caja=:id_caja ")
    void putCaja(@Bind("description") String description,
                 @Bind("caja_number") Long caja_number,
                 @Bind("id_caja") Long id_caja);

    //------------------------------------------------------------------------------------------------

    @RegisterMapper(StorageMapper.class)
    @SqlQuery(" select id_storage,description storage_name,storage_number " +
              " from tienda724.storage " +
              " where id_store=:id_store ")
    List<Storage> getStorageByStore (@Bind("id_store") Long id_store);


    @SqlUpdate(" insert into tienda724.storage(id_storage,id_store,description,storage_number) " +
               " values(tienda724.storage_seq.nextval,:id_store,:description,:storage_number) ")
    void postStorage(@Bind("id_store") Long id_store,
                     @Bind("description") String description,
                     @Bind("storage_number") Long storage_number);


    @SqlUpdate(" update tienda724.storage " +
            " set description=:description,storage_number=:storage_number " +
            " where id_storage=:id_storage ")
    void putStorage(@Bind("description") String description,
                 @Bind("storage_number") Long storage_number,
                 @Bind("id_storage") Long id_storage);


    //------------------------------------------------------------------------------------------------

    @RegisterMapper(DirectoryMapper.class)
    @SqlQuery(" select dir_name,address,webpage,id_city,latitud,longitud " +
              " from tercero724.directory " +
              " where id_directory=:id_directory ")
    List<Directory> getDirectorybyID (@Bind("id_directory") Long id_directory);


    @SqlQuery(" select city_name||' - '||state_name AS city_name " +
              " from tercero724.city c,tercero724.state s " +
              " where c.id_state=s.id_state and id_city=:id_city ")
    List<String> getCityByID (@Bind("id_city") Long id_city);


    @SqlQuery(" select city_name from tercero724.city where id_state=:id_state ")
    List<String> getCitiesByState (@Bind("id_state") Long id_state);

    //---------------------------------------------------------------------------------

    @RegisterMapper(StateMapper.class)
    @SqlQuery(" select id_state,state_name from tercero724.state ")
    List<State> getStates ();

    @RegisterMapper(PhoneMapper.class)
    @SqlQuery("select id_phone,phone,priority from tercero724.phone where id_directory=:id_directory")
    List<Phone> getPhonesByDirectory (@Bind("id_directory") Long id_directory);

    @RegisterMapper(MailMapper.class)
    @SqlQuery("select id_mail,mail,priority from tercero724.mail where id_directory=:id_directory")
    List<Mail> getMailsByDirectory (@Bind("id_directory") Long id_directory);

    //----------------------------------------------------------------------------------

    @RegisterMapper(StorageCategoryMapper.class)
    @SqlQuery(" SELECT DISTINCT d.ID_CATEGORY, d.ID_CATEGORY_FATHER, b.ID_THIRD, e.NAME, e.DESCRIPTION, d.IMG_URL\n" +
            " FROM TIENDA724.PRODUCT_STORE a, TIENDA724.CODES b, TIENDA724.PRODUCT c, TIENDA724.CATEGORY d, TIENDA724.COMMON e, TIENDA724.STORE f\n" +
            " WHERE a.ID_STORE = f.ID_STORE AND a.ID_CODE = b.ID_CODE AND b.ID_PRODUCT = c.ID_PRODUCT AND c.ID_CATEGORY = d.ID_CATEGORY\n" +
            "  AND e.ID_COMMON = d.ID_COMMON AND f.ID_THIRD = :ID_THIRD ")
    List<StorageCategory> getCategoryByStore(@Bind("ID_THIRD") Long ID_THIRD);


    @RegisterMapper(StoreNameMapper.class)
    @SqlQuery(" SELECT a.ID_STORE, a.DESCRIPTION " +
            " FROM TIENDA724.STORE a, TIENDA724.PRODUCT_STORE b, TIENDA724.CODES c, TIENDA724.PRODUCT d " +
            " WHERE a.ID_STORE = b.ID_STORE AND b.ID_CODE = c.ID_CODE AND c.ID_PRODUCT = d.ID_PRODUCT AND d.ID_CATEGORY = :id_category ")
    List<StoreName> getStoreByCategory(@Bind("id_category") Long id_category);

    //-----------------------------------------------------------------------------------

    @RegisterMapper(PriceMapper.class)
    @SqlQuery(" select price_description,price,pct_descuento from " +
            " tienda724.price where id_product_store=:id_product_store order by price desc ")
    List<Price> getPriceList(@Bind("id_product_store") Long id_product_store);


    @RegisterMapper(PriceMapper.class)
    @SqlQuery(" select 'General' price_description,price,0 pct_descuento \n" +
            "from facturacion724.detail_bill db \n" +
            "where id_product_third=:id_product_store and id_bill=:idbillprov order by price desc ")
    List<Price> getPriceListView(@Bind("id_product_store") Long id_product_store,@Bind("idbillprov") Long idbillprov);


    @SqlUpdate(" insert into tienda724.PRICE(ID_PRODUCT_STORE, PRICE_DESCRIPTION, PRICE) " +
            " values(:ID_PRODUCT_STORE,:PRICE_DESCRIPTION,:PRICE) ")
    void postPrice(@Bind("ID_PRODUCT_STORE") Long ID_PRODUCT_STORE,
                   @Bind("PRICE_DESCRIPTION") String PRICE_DESCRIPTION,
                   @Bind("PRICE") Double PRICE);


    @SqlUpdate(" insert into tienda724.PRODUCT_STORE(id_store,id_code,product_store_name,standard_price,product_store_code,ownbarcode) " +
               " values(:id_store,:id_code,:product_store_name,:standard_price,:product_store_code,:ownbarcode) ")
    void postProductStore (@Bind("id_store") Long id_store,
                           @Bind("id_code") Long id_code,
                           @Bind("product_store_name") String product_store_name,
                           @Bind("standard_price") Long standard_price,
                           @Bind("product_store_code") String product_store_code,
                           @Bind("ownbarcode") String ownbarcode);

    @SqlUpdate(" update TERCERO724.third set ID_COMMON_BASICINFO =\n" +
            " (select p.ID_COMMON_BASICINFO from TERCERO724.PERSON p, tercero724.THIRD t where t.ID_PERSON = p.ID_PERSON and t.ID_THIRD = :id_third)\n" +
            " where id_third = :id_third ")
    void putThird(@Bind("id_third") Long id_third);

    @SqlQuery(" select t.ID_THIRD from TERCERO724.PERSON p, tercero724.THIRD t where t.ID_PERSON = p.ID_PERSON and p.ID_PERSON = :id_person")
    Long getThird(@Bind("id_person") Long id_person);

    @SqlQuery("SELECT ID_PRODUCT_STORE FROM TIENDA724.PRODUCT_STORE WHERE ID_PRODUCT_STORE IN (SELECT MAX(ID_PRODUCT_STORE) FROM TIENDA724.PRODUCT_STORE)")
    Long getPkLast();


    @SqlQuery(" select ID_PRODUCT_STORE from tienda724.PRODUCT_STORE\n" +
            " where PRODUCT_STORE_CODE = :code\n" +
            "  and ID_STORE = :id_store ")
    Long getPsId(@Bind("code") String code,
                 @Bind("id_store") Long id_store);

    @RegisterMapper(ItemCajaMapper.class)
    @SqlQuery(" select c.id_caja,c.id_store\n" +
            " from tienda724.caja_person cp,tienda724.caja c\n" +
            " where cp.id_caja=c.id_caja\n" +
            " and cp.id_person=:id_person")
    List<ItemCaja> getCajaItems(@Bind("id_person") Long id_person);

    @RegisterMapper(DatosTiendaMapper.class)
    @SqlQuery(" select s.id_third,id_store_cliente,description tienda,cbi.fullname cliente \n" +
            "from otros.ruteros r,tienda724.store s,tercero724.third tp,tercero724.third t,\n" +
            "tercero724.common_basicinfo cbi\n" +
            "where r.id_store_cliente=s.id_store and s.id_third=tp.id_third and tp.id_third=t.id_third_father\n" +
            "and t.id_common_basicinfo=cbi.id_common_basicinfo\n" +
            "and id_store_proveedor = :id_store_proveedor and id_app = :id_app order by cliente ")
    List<DatosTienda> getDatosTienda(@Bind("id_store_proveedor") Long id_store_proveedor,
                                     @Bind("id_app") Long id_app);

    @SqlQuery(" select ID_TIPO_STORE from tienda724.STORE_TIPOSTORE where ID_STORE = :id_store ")
    List<Long> getTipoStore (@Bind("id_store") Long id_store);


    @SqlUpdate(" UPDATE tienda724.PRODUCT_STORE\n" +
            " SET STANDARD_PRICE = :standard_price\n" +
            " WHERE ID_PRODUCT_STORE = :id_ps")
    void putStandardPrice(@Bind("standard_price") Double standard_price,
                          @Bind("id_ps") Long id_ps);


    @SqlQuery(" SELECT STANDARD_PRICE from TIENDA724.PRODUCT_STORE WHERE ID_PRODUCT_STORE = :id_ps ")
    Double getStandardPriceByPs (@Bind("id_ps") Long id_ps);


    @SqlCall(" call facturacion724.gestion_pedidos.confirmar_pedido_cliente(:idbillcliente, :notes) ")
    void confirmar_pedido_cliente(@Bind("idbillcliente") Long idbillcliente,
                                  @Bind("notes") String notes);

    @SqlCall(" call FACTURACION724.DEVOLUCION_VENTA(:id_bill,:idcaja) ")
    void devolucionVenta(@Bind("id_bill") Long id_bill,
                                  @Bind("idcaja") Long idcaja);

    @SqlCall(" call TIENDA724.CREAR_TIENDA_CLIENTE(:idthirdprop,:idthirdempresa,:storename,:estienda,:username,:copiaproductos,:esproveedor,:idstorecliente,:idtempcliente) ")
    void crearTiendaCliente(@Bind("idthirdprop") Long idthirdprop,
                            @Bind("idthirdempresa") Long idthirdempresa,
                            @Bind("storename") String storename,
                            @Bind("estienda") String estienda,
                            @Bind("username") String username,
                            @Bind("copiaproductos") String copiaproductos,
                            @Bind("esproveedor") String esproveedor,
                            @Bind("idstorecliente") Long idstorecliente,
                            @Bind("idtempcliente") Long idtempcliente);


    @SqlCall(" call TIENDA724.CREAR_PARAM_PEDIDOS(:ID_STORE_CLIENTE, :ID_STORE_PROVEEDOR, :ID_THIRD_CLIENTE, :ID_THIRD_EMPLOYEE_CLIENTE," +
            "        :ID_THIRD_PROVEEDOR, :ID_THIRD_EMPLOYEE_PROVEEDOR, :HORA, :DIAS_PERIODICIDAD,\n" +
            "        :STATUS, :DISPONIBLE, :FALTANTES) ")
    void createParamPedidos(@Bind("ID_STORE_CLIENTE") Long ID_STORE_CLIENTE,
                            @Bind("ID_STORE_PROVEEDOR") Long ID_STORE_PROVEEDOR,
                            @Bind("ID_THIRD_CLIENTE") Long ID_THIRD_CLIENTE,
                            @Bind("ID_THIRD_EMPLOYEE_CLIENTE") Long ID_THIRD_EMPLOYEE_CLIENTE,
                            @Bind("ID_THIRD_PROVEEDOR") Long ID_THIRD_PROVEEDOR,
                            @Bind("ID_THIRD_EMPLOYEE_PROVEEDOR") Long ID_THIRD_EMPLOYEE_PROVEEDOR,
                            @Bind("HORA") String HORA,
                            @Bind("DIAS_PERIODICIDAD") Long DIAS_PERIODICIDAD,
                            @Bind("STATUS") String STATUS,
                            @Bind("DISPONIBLE") String DISPONIBLE,
                            @Bind("FALTANTES") String FALTANTES);


    @RegisterMapper(InfoDataMapper.class)
    @SqlQuery("select id_store_client idstoreclient,tclient.id_third_father idthirdfather,id_store_provider idsotreprovider\n" +
            "                  ,sc.DESCRIPTION storenameclient,ldclient.URL_LOGO logoclient,dirclient.address addressclient,cityclient.city_name cityclient,mailclient.mail mailclient,phoneclient.PHONE phoneclient\n" +
            "                  ,sp.DESCRIPTION storenameprov,ldprov.URL_LOGO logoprov\n" +
            "                  ,dirprov.address addressprov,cityprov.city_name cityprov,mailprov.mail mailprov,phoneprov.PHONE phoneprov\n" +
            "                  ,dirclient.ID_CITY,dirclient.latitud latitudstorec,dirclient.longitud longitudstorec\n" +
            "                      from tienda724.store sc,tienda724.param_pedidos pp,tercero724.third tclient,tercero724.third tclientfather\n" +
            "                        ,tercero724.directory dirclient,tercero724.city cityclient,tercero724.mail mailclient,tercero724.phone phoneclient\n" +
            "                        ,tercero724.legal_data ldclient\n" +
            "                        ,tienda724.store sp,tercero724.third tprov,tercero724.third tprovfather\n" +
            "                        ,tercero724.directory dirprov,tercero724.city cityprov,tercero724.mail mailprov,tercero724.phone phoneprov\n" +
            "                        ,tercero724.legal_data ldprov\n" +
            "                        where sc.id_third=tclient.id_third_father and sc.id_store=pp.id_store_client\n" +
            "                        and tclientfather.id_third=tclient.id_third_father\n" +
            "                        and sc.id_directory=dirclient.id_directory and dirclient.id_city=cityclient.id_city\n" +
            "                        and sc.id_directory=mailclient.id_directory and sc.id_directory=phoneclient.id_directory\n" +
            "                        and mailclient.PRIORITY=1 and phoneclient.PRIORITY=1\n" +
            "                        and tclientfather.ID_LEGAL_DATA=ldclient.id_legal_data\n" +
            "                        and sp.id_store=pp.id_store_provider and sp.id_third=tprov.id_third_father\n" +
            "                        and tprovfather.id_third=tprov.id_third_father\n" +
            "                        and sp.id_directory=dirprov.id_directory and dirprov.id_city=cityprov.id_city\n" +
            "                        and sp.id_directory=mailprov.id_directory and sp.id_directory=phoneprov.id_directory\n" +
            "                        and mailprov.PRIORITY=1 and phoneprov.PRIORITY=1\n" +
            "                        and tprovfather.ID_LEGAL_DATA=ldprov.id_legal_data\n" +
            "                        and pp.id_store_provider=:id_store_provider\n" +
            "                        and tclient.id_third=:id_third")
    List<InfoData> getInfobyIdthird(@Bind("id_store_provider") Long id_store_provider,
                                    @Bind("id_third") Long id_third);


    @SqlQuery(" SELECT COUNT(*) FROM TIENDA724.PARAM_PEDIDOS WHERE ID_STORE_CLIENT=:ID_STORE_CLIENTE AND ID_STORE_PROVIDER=:ID_STORE_PROVEEDOR ")
    Long getIfIsOnParam(@Bind("ID_STORE_PROVEEDOR") Long ID_STORE_PROVEEDOR,
                        @Bind("ID_STORE_CLIENTE") Long ID_STORE_CLIENTE);


    @SqlUpdate(" UPDATE TIENDA724.PRODUCT_STORE SET CODIGO_PROVEEDOR=:codProv WHERE ID_STORE=:ID_STORE_PROVEEDOR AND OWNBARCODE=:ownbarcode ")
    void updateProvCode(@Bind("ID_STORE_PROVEEDOR") Long ID_STORE_PROVEEDOR,
                        @Bind("codProv") String codProv,
                        @Bind("ownbarcode") String ownbarcode);


    @SqlCall(" call tienda724.copia_productos(49007,49047,'N') ")
    void copyNew();


    @SqlQuery(" select TIENDA724.GET_VALOR_DOMICILIO(:IDSTORE, :DISTANCIA_KM) FROM dual ")
    Float GET_VALOR_DOMICILIO(@Bind("IDSTORE") String IDSTORE,
                              @Bind("DISTANCIA_KM") String DISTANCIA_KM);


    @SqlQuery(" Select id_product_store from tienda724.product_store where ownbarcode=:ownbarcode and id_store=:id_store ")
    Long getProductStore(@Bind("ownbarcode") String ownbarcode,
                         @Bind("id_store") String id_store);

    @RegisterMapper(OptionMapper.class)
    @SqlQuery(" select p.OPCION,p.DESCRIPCION\n" +
            "from tienda724.product_store ps, tienda724.opciones p,\n" +
            "tienda724.opciones_ps ops \n" +
            "where ps.ID_PRODUCT_STORE=ops.ID_PRODUCT_STORE and p.ID_OPCION=ops.ID_OPCION\n" +
            "and ops.id_product_store=:idps and ops.status='ACTIVO' ")
    List<Option> getOptions (@Bind("idps") String idps);


    @RegisterMapper(DetailWithOptionsMapper.class)
    @SqlQuery(" select db.id_product_third,ps.product_store_name producto,ddb.notas,ddb.id_bill_state,bs.NAME estado \n" +
            "from facturacion724.detalle_detail_bill ddb,facturacion724.detail_bill db,\n" +
            "tienda724.product_store ps,facturacion724.bill_state bs\n" +
            "where ddb.id_bill=db.id_bill and ddb.id_product_store=db.id_product_third\n" +
            "and db.id_product_third=ps.id_product_store and bs.ID_BILL_STATE=ddb.ID_BILL_STATE\n" +
            "and ddb.id_bill = :idbill")
    List<DetailWithOptions> getNotesForDetailsWithOptions (@Bind("idbill") String idbill);



    @SqlQuery("select description from tienda724.store where id_store=:idStore")
    String getStoreNameById (@Bind("idStore") String idStore);

}