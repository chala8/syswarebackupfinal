package com.name.business.sanitations;

import com.name.business.entities.*;

import com.name.business.entities.Attribute;
import com.name.business.entities.Category;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;


import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class EntitySanitation {

    public static Common commonSanitation(Common common){
        Common commonSanitation=null;
        if (common!=null){
            commonSanitation= new Common(
                    formatoLongSql(common.getId_common()),
                    formatoLIKESql(common.getName()),
                    formatoLIKESql(common.getDescription())
            );
        }else{
            commonSanitation= new Common(
                    formatoLongSql(null),
                    formatoLIKESql(null),
                    formatoLIKESql(null)
            );
        }

        return commonSanitation;
    }

    public static CommonState commonStateSanitation(CommonState commonState){
        CommonState commonStateSanitation=null;
        if(commonState!=null){
            commonStateSanitation= new CommonState(

                    formatoLongSql(commonState.getId_common_state()),
                    formatoIntegerSql(commonState.getState()),
                    formatoDateSql(commonState.getCreation_date()),
                    formatoDateSql(commonState.getModify_date())
            );


        }else {
            commonStateSanitation= new CommonState(

                    formatoLongSql(null),
                    formatoIntegerSql(null),
                    formatoDateSql(null),
                    formatoDateSql(null)
            );


        }
                return commonStateSanitation;
    }

    public static Attribute attributeSanitation(Attribute attribute){
        Attribute attributeSanitation=null;
        if(attribute!=null){
            attributeSanitation= new Attribute(
                    formatoLongSql(attribute.getId_attribute()),
                    commonSanitation(attribute.getCommon()),
                    commonStateSanitation(attribute.getState())

            );
        }else{
            attributeSanitation= new Attribute(
                    formatoLongSql(null),
                    commonSanitation(new Common(null,null,null)),
                    commonStateSanitation(new CommonState(null,1,null,null))

            );
        }


        return attributeSanitation;
    }

    public static AttributeDetailList attributeDeatilListSanitation(AttributeDetailList attributeDetailList){
        AttributeDetailList attributeDeatilListSanitation=null;
        attributeDeatilListSanitation= new AttributeDetailList(
                formatoLongSql(attributeDetailList.getId_attribute_detail_list()),
                formatoLongSql(attributeDetailList.getId_attribute_list()),
                commonStateSanitation(attributeDetailList.loadingCommonState()),
                attributeValueSanitation(attributeDetailList.getValue())
        );
        return attributeDeatilListSanitation;
    }
    public static AttributeList attributeListSanitation(AttributeList attributeList){

        AttributeList attributeListSanitation=null;
        attributeListSanitation= new AttributeList(
                formatoLongSql(attributeList.getId_attribute_list()),
                commonStateSanitation(attributeList.getState()));

        return attributeListSanitation;
    }

    public static AttributeValue attributeValueSanitation(AttributeValue attributeValue){

        AttributeValue attributeValueSanitation=null;
        if(attributeValue!=null){
            attributeValueSanitation= new AttributeValue(
                    formatoLongSql(attributeValue.getId_attribute_value()),
                    formatoLongSql(attributeValue.getId_attribute()),
                    commonSanitation(new Common(
                            attributeValue.getId_common_attrib_value(),
                            attributeValue.getName_attrib_value(),
                            attributeValue.getDescription_attrib_value()
                    )),
                    commonStateSanitation(
                            new CommonState(
                                    attributeValue.getId_state_attrib_value(),
                                    attributeValue.getState_attrib_value(),
                                    attributeValue.getCreation_attrib_value(),
                                    attributeValue.getModify_attrib_value()
                            )
                    ));
        }else{
            attributeValueSanitation= new AttributeValue(
                    formatoLongSql(null),
                    formatoLongSql(null),
                    commonSanitation(null),
                    commonStateSanitation(
                            null
                    ));
        }


        return attributeValueSanitation;
    }

    public static Category categorySanitation(Category category){
        Category categorySanitation = null;
        categorySanitation= new Category(
                formatoLongSql(category.getId_category()),
                commonSanitation(category.getCommon()),
                formatoStringSql(category.getImg_url()),
                formatoLongSql(category.getId_category_father()),
                formatoLongSql(category.getId_third_category()),
                commonStateSanitation(category.getState())
        );
        return categorySanitation;
    }



    public static Product productSanitation(Product product){
        Product productSanitation = null;
        if(product!=null){
            productSanitation= new Product(
                    formatoLongSql(product.getId_product()),
                    formatoLongSql(product.getId_category()),
                    formatoIntegerSql(product.getStock()),
                    formatoIntegerSql(product.getStock_min()),
                    formatoStringSql(product.getImg_url()),
                    formatoStringSql(product.getCode()),
                    formatoLongSql(product.getId_tax()),

                    commonSanitation(product.loadingCommon()),
                    commonStateSanitation(product.loadingCommonState())
            );
        }else{
            productSanitation= new Product(
                    formatoLongSql(null),
                    formatoLongSql(null),
                    formatoIntegerSql(null),
                    formatoIntegerSql(null),
                    formatoStringSql(null),
                    formatoStringSql(null),
                    formatoLongSql(null),

                    commonSanitation(new Common(null,null,null)),
                    commonStateSanitation(new CommonState(null,1,null,null))
            );
        }

        return productSanitation;
    }

    public static ProductThirdSimple productThirdSimpleSanitation(ProductThirdSimple productThirdSimple){
        ProductThirdSimple productThirdSimpleSanitation = null;

        if (productThirdSimple!=null){
            productThirdSimpleSanitation = new ProductThirdSimple(
                    formatoLongSql(productThirdSimple.getId_product_third()),
                    formatoLongSql(productThirdSimple.getId_third()),
                    formatoDoubleSql(productThirdSimple.getStandard_price()),
                    formatoDoubleSql(productThirdSimple.getMin_price()),
                    formatoLongSql(productThirdSimple.getId_code()),
                    formatoStringSql(productThirdSimple.getCode()),
                    formatoLongSql(productThirdSimple.getId_category_third()),
                    formatoStringSql(productThirdSimple.getLocation()),
                    commonStateSanitation(productThirdSimple.loadingCommonState()),
                    measureUnitSanitation(productThirdSimple.getMeasure_unit()),
                    formatoLongSql(productThirdSimple.getId_product()),
                    formatoLongSql(productThirdSimple.getId_attribute_list()));
        }else{
            productThirdSimpleSanitation = new ProductThirdSimple(
                    formatoLongSql(null),
                    formatoLongSql(null),
                    formatoDoubleSql(null),
                    formatoDoubleSql(null),
                    formatoLongSql(null),
                    formatoStringSql(null),
                    formatoLongSql(null),
                    formatoStringSql(null),
                    commonStateSanitation(null),
                    measureUnitSanitation(null),
                    formatoLongSql(null),
                    formatoLongSql(null));
        }

        return productThirdSimpleSanitation;
    }

    public static ProductThird productThirdSanitation(ProductThird productThird){
        ProductThird productThirdSanitation = null;
        productThirdSanitation = new ProductThird(
                productSanitation(productThird.getProduct()),
                productThirdSimpleSanitation(productThird.getDescription()),
                codeSanitation((Code) productThird.getCode())
                );
        return productThirdSanitation;
    }

    public static MeasureUnit measureUnitSanitation(MeasureUnit measure_unit) {
        MeasureUnit measureUnitSanitation = null;
        if (measure_unit!=null){
            measureUnitSanitation= new MeasureUnit(
                    formatoLongSql(measure_unit.getId_measure_unit()),
                    formatoLongSql(measure_unit.getId_measure_unit_father()),
                    commonSanitation(measure_unit.loadingCommon()),
                    commonStateSanitation(measure_unit.loadingCommonState())
            );
        }else {
            measureUnitSanitation= new MeasureUnit(
                    formatoLongSql(null),
                    formatoLongSql(null),
                    commonSanitation(null),
                    commonStateSanitation(null)
            );
        }
        return measureUnitSanitation;
    }

    public static Inventory inventorySanitation(Inventory inventory) {
        Inventory inventorySanitation = null;
        inventorySanitation= new Inventory(
                formatoLongSql(inventory.getId_inventory()),
                formatoLongSql(inventory.getId_third()),
                commonSanitation(inventory.loadingCommon()),
                commonStateSanitation(inventory.loadingCommonState())
        );
        return inventorySanitation;
    }
    public static InventoryDetailSimple inventoryDetailSanitationSimple(InventoryDetailSimple inventoryDetailSimple) {

        InventoryDetailSimple inventoryDetailSimpleSanitation = null;
        if(inventoryDetailSimple!=null){
            inventoryDetailSimpleSanitation = new InventoryDetailSimple(
                    formatoLongSql(inventoryDetailSimple.getId_inventory_detail()),
                    formatoLongSql(inventoryDetailSimple.getId_inventory()),
                    formatoLongSql(inventoryDetailSimple.getId_product_third()),
                    formatoIntegerSql(inventoryDetailSimple.getQuantity()),
                    formatoIntegerSql(inventoryDetailSimple.getStock_min()),
                    formatoStringSql(inventoryDetailSimple.getCode()),
                    commonStateSanitation(inventoryDetailSimple.loadingCommonState())
            );
        }else {
            inventoryDetailSimpleSanitation = new InventoryDetailSimple(
                    formatoLongSql(null),
                    formatoLongSql(null),
                    formatoLongSql(null),
                    formatoIntegerSql(null),
                    formatoIntegerSql(null),
                    formatoStringSql(null),
                    commonStateSanitation(new CommonState(null,1,null,null))
            );
        }

        return inventoryDetailSimpleSanitation;
    }



    public static InventoryDetail inventoryDetailSanitation(InventoryDetail inventoryDetail) {
        InventoryDetail inventoryDetailSanitation = null;
        if (inventoryDetail!=null){
            inventoryDetailSanitation = new InventoryDetail(
                    inventoryDetailSanitationSimple(inventoryDetail.getDetail()),
                    productSanitation(inventoryDetail.getProduct()),
                    productThirdSimpleSanitation(inventoryDetail.getDescription())
            );
        }else{
            inventoryDetailSanitation = new InventoryDetail(
                    inventoryDetailSanitationSimple(null),
                    productSanitation(null),
                    productThirdSimpleSanitation(null)
            );

        }

        return inventoryDetailSanitation;
    }

    public static TaxTariff taxTariffSanitation(TaxTariff taxTariff) {
        TaxTariff taxTariffSanitation = null;
        taxTariffSanitation = new TaxTariff(

        formatoLongSql(taxTariff.getId_tax_tariff()),
        formatoDoubleSql(taxTariff.getPercent()),
        commonSanitation(taxTariff.loadingCommon()),
        commonStateSanitation(taxTariff.loadingCommonState())
        );
        return taxTariffSanitation;
    }

    public static PriceList priceListSanitation(PriceList priceList) {
        PriceList priceListSanitation = null;
        priceListSanitation = new PriceList(
                formatoLongSql(priceList.getId_price_list()),
                formatoLongSql(priceList.getId_product_third_pr_list()),
                formatoDoubleSql(priceList.getStandard_price()),
                formatoDoubleSql(priceList.getMin_price()),
                commonSanitation(priceList.loadingCommon()),
                commonStateSanitation(priceList.loadingCommonState())
        );
        return priceListSanitation;
    }

    public static Code codeSanitation(Code code) {
        Code codeSanitation = null;
        if(code!=null){
            codeSanitation = new Code(
                    formatoLongSql(code.getId_code()),
                    formatoStringSql(code.getCode()),
                    formatoStringSql(code.getImg()),
                    formatoDoubleSql(code.getSuggested_price()),
                    formatoLongSql(code.getId_third_cod()),
                    formatoLongSql(code.getId_product()),
                    formatoLongSql(code.getId_measure_unit()),
                    formatoLongSql(code.getId_attribute_list()),
                    commonStateSanitation(code.loadingCommonState())
            );

        }else {

            codeSanitation = new Code(
                    formatoLongSql(null),
                    formatoStringSql(null),
                    formatoStringSql(null),
                    formatoDoubleSql(null),
                    formatoLongSql(null),
                    formatoLongSql(null),
                    formatoLongSql(null),
                    formatoLongSql(null),
                    commonStateSanitation(null)
            );

        }

        return codeSanitation;
    }

    public static Document documentSanitation(Document document){
        Document documentSanitation=null;
        if (document!=null){
            documentSanitation= new Document(
                    formatoLongSql(document.getId_common()),
                    formatoLIKESql(document.getName()),
                    formatoLIKESql(document.getDescription())
            );
        }else{
            documentSanitation= new Document(
                    formatoLongSql(null),
                    formatoLIKESql(null),
                    formatoLIKESql(null)
            );
        }

        return documentSanitation;
    }









}

