package com.name.business.DAOs;


import com.name.business.entities.Code;
import com.name.business.entities.CommonSimple;
import com.name.business.entities.completes.CodeComplete;
import com.name.business.mappers.CodeMapper;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.mappers.completes.CodeCompleteMapper;
import com.name.business.representations.CategoryDTO;
import com.name.business.representations.CodeDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(CodeMapper.class)
public interface CodeDAO {

    @SqlQuery("SELECT * FROM V_CODE V_COD\n " +
            "WHERE (V_COD.ID_CODE = :COD.id_code OR :COD.id_code IS  NULL) AND\n" +
            "      (V_COD.CODE = :COD.code OR :COD.code IS  NULL) AND \n" +
            "      (V_COD.IMG_COD = :COD.img OR :COD.img IS  NULL) AND\n" +
            "      (V_COD.SUGGESTED_PRICE=:COD.suggested_price OR :COD.suggested_price IS NULL )AND\n" +
            "      (V_COD.ID_THIRD_COD=:COD.id_third_cod OR :COD.id_third_cod IS NULL )AND\n" +
            "      (V_COD.ID_PRODUCT_COD = :COD.id_product OR :COD.id_product IS  NULL) AND \n" +
            "      (V_COD.ID_MEASURE_UNIT_COD = :COD.id_measure_unit OR :COD.id_measure_unit IS  NULL) AND \n" +
            "      (V_COD.ID_ATTRIBUTE_LIST_COD = :COD.id_attribute_list OR :COD.id_attribute_list IS  NULL) AND \n" +
            "      (V_COD.ID_STATE_CODE = :COD.id_state_cod OR :COD.id_state_cod IS  NULL) AND \n" +
            "      (V_COD.STATE_CODE = :COD.state_cod OR :COD.state_cod IS  NULL) AND \n" +
            "      (V_COD.CREATION_CODE = :COD.creation_cod OR :COD.creation_cod IS  NULL) AND \n" +
            "      (V_COD.MODIFY_CODE = :COD.modify_cod OR :COD.modify_cod IS  NULL) ")
    List<Code>  read(@BindBean("COD")Code code);

    @RegisterMapper(CodeCompleteMapper.class)
    @SqlQuery("SELECT * FROM V_CODE_COMPLTE V_COD \n " +
            "WHERE (V_COD.ID_CODE = :COD.id_code OR :COD.id_code IS  NULL) AND\n" +
            "      (V_COD.CODE_FINAL = :COD.code OR :COD.code IS  NULL) AND \n" +
            "      (V_COD.IMG_COD = :COD.img OR :COD.img IS  NULL) AND\n" +
            "      (V_COD.SUGGESTED_PRICE=:COD.suggested_price OR :COD.suggested_price IS NULL )AND\n" +
            "      (V_COD.ID_THIRD_COD=:COD.id_third_cod OR :COD.id_third_cod IS NULL )AND\n" +
            "      (V_COD.ID_PRODUCT = :COD.id_product OR :COD.id_product IS  NULL) AND \n" +
            "      (V_COD.ID_MEASURE_UNIT = :COD.id_measure_unit OR :COD.id_measure_unit IS  NULL) AND \n" +
            "      (V_COD.ID_ATTRIBUTE_LIST_COD = :COD.id_attribute_list OR :COD.id_attribute_list IS  NULL) AND \n" +
            "      (V_COD.ID_STATE_CODE = :COD.id_state_cod OR :COD.id_state_cod IS  NULL) AND \n" +
            "      (V_COD.STATE_CODE = :COD.state_cod OR :COD.state_cod IS  NULL) AND \n" +
            "      (V_COD.CREATION_CODE = :COD.creation_cod OR :COD.creation_cod IS  NULL) AND \n" +
            "      (V_COD.MODIFY_CODE = :COD.modify_cod OR :COD.modify_cod IS  NULL) ")
    List<CodeComplete>  readComplete(@BindBean("COD")Code code);




    @SqlUpdate("INSERT INTO CODES " +
            "            ( CODE,IMG,SUGGESTED_PRICE,ID_THIRD,ID_PRODUCT,ID_MEASURE_UNIT,ID_ATTRIBUTE_LIST,ID_COMMON_STATE) " +
            "    VALUES (:cat.code,:cat.img,:cat.suggested_price,:cat.id_third_cod,:cat.id_product,:cat.id_measure_unit,:cat.id_attribute_list,:id_state) ")
    void create(@BindBean("cat") CodeDTO codeDTO, @Bind("id_state") Long id_common_state);

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_CODE ID,ID_PRODUCT ID_COMMON, ID_COMMON_STATE FROM CODES  " +
            "  WHERE (ID_CODE = :id_code OR :id_code IS NULL) AND (CODE = :code OR :code IS NULL)")
    List<CommonSimple> readCommons(@Bind("id_code") Long id_product_third, @Bind("code") String code);

    @SqlQuery("SELECT COUNT(ID_CODE) FROM CODES WHERE ID_CODE = :id_code")
    Integer getValidatorID(@Bind("id_code") Long id_product_third);

    @SqlQuery("SELECT ID_CODE FROM CODES WHERE ID_CODE IN (SELECT MAX(ID_CODE) FROM CODES)")
    Long getPkLast();

    @SqlQuery("SELECT COUNT(ID_PRODUCT ) FROM PRODUCT WHERE ID_PRODUCT = :id_product")
    Integer validatorIDProduct(@Bind("id_product") Long id_product);

    @SqlQuery("SELECT COUNT(ID_ATTRIBUTE_LIST) FROM ATTRIBUTE_LIST WHERE ID_ATTRIBUTE_LIST = :id_attribute_list")
    Integer validatorIDAttributeList(@Bind("id_attribute_list") Long id_attribute_list);

    @SqlQuery("SELECT COUNT(ID_MEASURE_UNIT) FROM  MEASURE_UNIT WHERE  ID_MEASURE_UNIT= :id_measure_unit")
    Integer validatorIDMeasureUnit(@Bind("id_measure_unit") Long id_measure_unit);


    @SqlUpdate("UPDATE CODES SET " +
            "    CODE = :pro_th.code, " +
            "    IMG = :pro_th.img, " +
            "    SUGGESTED_PRICE=:pro_th.suggested_price, " +
            "    ID_THIRD_COD=:pro_th.id_third_cod," +

            "    ID_PRODUCT = :pro_th.id_product, " +
            "    ID_MEASURE_UNIT = :pro_th.id_measure_unit, " +
            "    ID_ATTRIBUTE_LIST = :pro_th.id_attribute_list " +
            "    WHERE  (ID_CODE=  :id_code OR :id_code IS NULL ) AND  (CODE=  :code_param OR :code_param IS NULL) ")
    void update( @Bind("id_code") Long id_code,@Bind("code_param") String code_param, @BindBean("pro_th") CodeDTO codeDTO);

    @SqlQuery("SELECT COUNT(ID_CODE) FROM CODES WHERE CODE = :code")
    Integer getValidatorCode(@Bind("code") String code);
}
