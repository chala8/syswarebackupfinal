package com.name.business.mappers;

import com.name.business.entities.PlanillaDetail;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PlanillaDetailMapper implements ResultSetMapper<PlanillaDetail>{

    @Override
    public PlanillaDetail map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new PlanillaDetail(
                resultSet.getString("NUM_DOCUMENTO"),
                resultSet.getString("VALOR"),
                resultSet.getString("STORE"),
                resultSet.getString("ADDRESS")
        );
    }
}