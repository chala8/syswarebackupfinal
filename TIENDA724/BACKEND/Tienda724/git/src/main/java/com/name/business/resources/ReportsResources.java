package com.name.business.resources;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.businesses.MeasureUnitNameBusiness;
import com.name.business.businesses.ReportsBusiness;
import com.name.business.entities.*;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import java.util.*;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;


@Path("/resource")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ReportsResources {
    private ReportsBusiness reportsBusiness;

    public ReportsResources(ReportsBusiness reportsBusiness) {
        this.reportsBusiness = reportsBusiness;
    }

    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getBillTotal(@QueryParam("id_bill_type") Long id_bill_type,
                                 @QueryParam("id_period") Long id_period,
                                 @QueryParam("date1") String date1,
                                 @QueryParam("date2") String date2,
                                 @QueryParam("id_third") Long id_third,
                                 @QueryParam("id_store")  List<Long> listStore) {

        Response response;

        Either<IException, List <DateReport>> getProductsEither = reportsBusiness.getBillTotal(
                id_bill_type, id_period, new Date(date1), new Date(date2), id_third,listStore
        );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/bycategory")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalByBillTypeCategory(@QueryParam("id_bill_type") Long id_bill_type,
                                               @QueryParam("id_category") Long id_category,
                                               @QueryParam("id_period") Long id_period,
                                               @QueryParam("date1") String date1,
                                               @QueryParam("date2") String date2,
                                               @QueryParam("id_third") Long id_third,
                                               @QueryParam("id_store") List<Long> id_store) {

        Response response;

        Either<IException, List <LabelReport>> getProductsEither = reportsBusiness.getTotalByBillTypeCategory(
                id_bill_type, id_category, id_period, new Date(date1), new Date(date2), id_third, id_store
        );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/auditoria")
    @Timed
    @RolesAllowed({"Auth"})
    public Response auditoria(@QueryParam("nombre") String nombre,
                              @QueryParam("id_third_employee") Long id_third_employee,
                              @QueryParam("id_store") Long id_store,
                              @QueryParam("valor_anterior") String valor_anterior,
                              @QueryParam("valor_nuevo") String valor_nuevo){
        Response response;

        response=Response.status(Response.Status.OK).entity(reportsBusiness.auditoria(nombre,
                id_third_employee,
                id_store,
                valor_anterior,
                valor_nuevo)).build();

        return response;

    }


    @POST
    @Path("/byline")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalByBillTypeLine(@QueryParam("id_bill_type") Long id_bill_type,
                                               @QueryParam("id_category") Long id_category,
                                               @QueryParam("id_period") Long id_period,
                                               @QueryParam("date1") String date1,
                                               @QueryParam("date2") String date2,
                                               @QueryParam("id_third") Long id_third,
                                               @QueryParam("id_store") List<Long> id_store) {

        Response response;

        Either<IException, List <LabelReport>> getProductsEither = reportsBusiness.getTotalByBillTypeLine(
                id_bill_type, id_category, id_period, new Date(date1), new Date(date2), id_third, id_store
        );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @GET
    @Path("/reportcategory")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalByBillTypeCategory(@QueryParam("id_third") Long id_third,
                                               @QueryParam("id_store") Long id_store,
                                               @QueryParam("date1") String date1,
                                               @QueryParam("date2") String date2) {

        Response response;

        Either<IException, List <CategoryReport>> getProductsEither = reportsBusiness.getTotalesFacturacionPorPeriodoCategorias(
                id_third, id_store, new Date(date1), new Date(date2) );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }




    @GET
    @Path("/boxes")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalByBillTypeCategory(@QueryParam("id_store") Long id_store) {

        Response response;

        Either<IException, List <CajaReport>> getProductsEither = reportsBusiness.getAllboxes(id_store);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }





    @GET
    @Path("/pendingStores")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPendingstores(@QueryParam("id_store") Long id_store) {

        Response response;

        Either<IException, List <StoresPending>> getProductsEither = reportsBusiness.getPendingstores(id_store);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }





    @GET
    @Path("/pendingReport")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPendingReport(@QueryParam("id_store") Long id_store,@QueryParam("id_storec") String id_storec) {

        Response response;

        Either<IException, List <PendingReport>> getProductsEither = reportsBusiness.getPendingReport(id_store, id_storec);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }





    @GET
    @Path("/minmargen")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getMargen(
            @QueryParam("id_ps") Long id_ps) {

        Response response;

        Either<IException, Double> getProductsEither = reportsBusiness.getMargen(id_ps);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @PUT
    @Path("/minmargen")
    @Timed
    @RolesAllowed({"Auth"})
    public Response putMargen(
            @QueryParam("id_ps") Long id_ps,
            @QueryParam("margen") Double margen) {

        Response response;

        Either<IException, Double> getProductsEither = reportsBusiness.putMargen(id_ps,margen);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/nombre")
    @Timed
    @RolesAllowed({"Auth"})
    public Response putNombre(
            @QueryParam("id_ps") Long id_ps,
            @QueryParam("nombre") String nombre) {

        Response response;

        Either<IException, Long> getProductsEither = reportsBusiness.putNombre(id_ps,nombre);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/quickCode")
    @Timed
    @RolesAllowed({"Auth"})
    public Response putNombre(
            @QueryParam("id_ps") Long id_ps,
            @QueryParam("code") String code,
            @QueryParam("id_store") Long id_store) {

        Response response;

        Either<IException, Long> getProductsEither = reportsBusiness.putQuickCode(id_ps,code,id_store);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/getAuditory")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getAuditoria(
                                               @QueryParam("id_store") Long id_store,
                                               @QueryParam("date1") String date1,
                                               @QueryParam("date2") String date2) {

        Response response;

        Either<IException, List <Auditoria>> getProductsEither = reportsBusiness.getAuditoria(
                 id_store, date1, date2 );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }





    @GET
    @Path("/reportproduct")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalesFacturacionPorPeriodoProductos(@QueryParam("id_third") Long id_third,
                                               @QueryParam("id_store") Long id_store,
                                               @QueryParam("date1") String date1,
                                               @QueryParam("date2") String date2) {

        Response response;

        Either<IException, List <CategoryReport>> getProductsEither = reportsBusiness.getTotalesFacturacionPorPeriodoProductos(
                id_third, id_store, new Date(date1), new Date(date2) );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @GET
    @Path("/reportproduct/line")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalesFacturacionPorPeriodoProductosLinea(@QueryParam("id_cat") Long id_cat,
                                                             @QueryParam("id_store") Long id_store,
                                                             @QueryParam("date1") String date1,
                                                             @QueryParam("date2") String date2) {

        Response response;

        Either<IException, List <CategoryReport>> getProductsEither = reportsBusiness.getTotalesFacturacionPorPeriodoProductosLinea(
                id_cat, id_store,new Date(date1), new Date(date2) );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @GET
    @Path("/reportproduct/cat")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalesFacturacionPorPeriodoProductosCate(@QueryParam("id_cat") Long id_cat,
                                                                  @QueryParam("id_store") Long id_store,
                                                                  @QueryParam("date1") String date1,
                                                                  @QueryParam("date2") String date2) {

        Response response;

        Either<IException, List <CategoryReport>> getProductsEither = reportsBusiness.getTotalesFacturacionPorPeriodoProductosCate(
                id_cat, id_store, date1, date2 );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @GET
    @Path("/reportproduct/brand")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalesFacturacionPorPeriodoProductosBrand(@QueryParam("id_brand") Long id_brand,
                                                                 @QueryParam("id_store") Long id_store,
                                                                 @QueryParam("date1") String date1,
                                                                 @QueryParam("date2") String date2) {

        Response response;

        Either<IException, List <CategoryReport>> getProductsEither = reportsBusiness.getTotalesFacturacionPorPeriodoProductosBrand(
                id_brand, id_store, date1, date2 );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }




    @GET
    @Path("/reportbill")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalesFacturacionPorPeriodo(@QueryParam("id_third") Long id_third,
                                                    @QueryParam("id_store") String id_store,
                                                    @QueryParam("date1") String date1,
                                                    @QueryParam("date2") String date2,
                                                    @QueryParam("typemove") Long typemove) {

        Response response;

        Either<IException,List <BillReport>> getProductsEither = reportsBusiness.getTotalesFacturacionPorPeriodo(
                id_third, id_store, date1, date2,typemove );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/reportbill2")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalesFacturacionPorPeriodo2(@QueryParam("id_third") Long id_third,
                                                    @QueryParam("id_store") Long id_store,
                                                    @QueryParam("date1") String date1,
                                                    @QueryParam("date2") String date2,
                                                    @QueryParam("typemove") Long typemove) {

        Response response;

        Either<IException,List <BillReport>> getProductsEither = reportsBusiness.getTotalesFacturacionPorPeriodo2(
                id_third, id_store, date1, date2,typemove );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/byproduct")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalByBillTypeProduct(@QueryParam("id_bill_type") Long id_bill_type,
                                               @QueryParam("id_product_store") Long id_product_store,
                                               @QueryParam("id_period") Long id_period,
                                               @QueryParam("date1") String date1,
                                               @QueryParam("date2") String date2,
                                              @QueryParam("id_store") List<Long> id_store) {

        Response response;

        Either<IException, List <LabelReport>> getProductsEither = reportsBusiness.getTotalByBillTypeProduct(
                id_bill_type, id_product_store, id_period, new Date(date1), new Date(date2),id_store
        );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/category")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalByCategory(@QueryParam("id_bill_type") Long id_bill_type,
                                 @QueryParam("date1") String date1,
                                 @QueryParam("date2") String date2) {
        Response response;

        Either<IException, DateReport2> getProductsEither = reportsBusiness.getTotalByCategory(
                id_bill_type, new Date(date1), new Date(date2)
        );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }
    @GET
    @Path("/brand")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalByBrand(@QueryParam("id_bill_type") Long id_bill_type,
                                 @QueryParam("date1") String date1,
                                 @QueryParam("date2") String date2) {
        Response response;

        Either<IException, DateReport2> getProductsEither = reportsBusiness.getTotalByBrand(
                id_bill_type, new Date(date1), new Date(date2)
        );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }
    @GET
    @Path("/product")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTotalByProduct(@QueryParam("id_bill_type") Long id_bill_type,
                                 @QueryParam("date1") String date1,
                                 @QueryParam("date2") String date2,
                                      @QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List <DateReport2>> getProductsEither = reportsBusiness.getTotalByProduct(
                id_bill_type, new Date(date1), new Date(date2), id_store
        );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/pdf")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postCategoryResource(){
        Response response;

        Either<IException, String> mailEither = reportsBusiness.printPDF();


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }


    @POST
    @Path("/getnewproducts")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getNewProducts(@QueryParam("id_store") String id_store){

        Response response;

        List<String> myList = new ArrayList<String>(Arrays.asList(id_store.split(",")));

        Either<IException, List<NewProductReport>> mailEither = reportsBusiness.getNewProducts(myList);

        if (mailEither.isRight()){
            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }


    @POST
    @Path("/updatenewproduct")
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateNewProduct(@QueryParam("idps") Long idps,
                                   @QueryParam("idproduct") Long idproduct,
                                   @QueryParam("idcode") Long idcode,
                                   @QueryParam("idms") Long idms,
                                   @QueryParam("idb") Long idb,
                                   @QueryParam("idcat") Long idcat,
                                   @QueryParam("psname") String psname){

        Response response;

        Long mailEither = reportsBusiness.putNewProduct(idps,
                idproduct,
                idcode,
                idms,
                idb,
                idcat,
                psname);


            response=Response.status(Response.Status.OK).entity(mailEither).build();

        return response;
    }



    @POST
    @Path("/perfecto")
    @Timed
    @RolesAllowed({"Auth"})
    public Response GENERAR_PEDIDOS_STOCK_MINIMO(@QueryParam("idstorep") Long idstorep,
                                     @QueryParam("weekday") Long weekday){

        Response response;

        Long mailEither = reportsBusiness.GENERAR_PEDIDOS_STOCK_MINIMO(idstorep,
                weekday);


        response=Response.status(Response.Status.OK).entity(mailEither).build();

        return response;
    }


    @POST
    @Path("/asociarAPerson")
    @Timed
    @RolesAllowed({"Auth"})
    public Response asociarCajaStore(@QueryParam("idperson") Long idperson,
                                     @QueryParam("idstore") Long idstore,
                                     @QueryParam("idcaja") Long idcaja){
        Response response;

        Either<IException, Long> mailEither = reportsBusiness.asociarCajaStore(idperson,idstore,idcaja);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }




    @GET
    @Path("/pricebyps")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPriceByPS(@QueryParam("id_ps") Long id_ps) {

        Response response;

        Either<IException, List <Price>> getProductsEither = reportsBusiness.getPriceByPS(id_ps
        );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/inventory")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getInventarioCons(InventoryReportDTO inventorydata) {

        Response response;

        Either<IException, List <InvetoryReport>> getProductsEither = reportsBusiness.getInventarioCons(
                inventorydata.getlistStore(),inventorydata.getlistLine(),inventorydata.getlistCategory(),inventorydata.getlistBrand());
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/rotacion")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDiasRotacion(InventoryReportDTO inventorydata) {

        Response response;

        Either<IException, List <DiasRotacion>> getProductsEither = reportsBusiness.getDiasRotacion(inventorydata.getdays(),
                inventorydata.getlistStore(),inventorydata.getlistLine(),inventorydata.getlistCategory(),inventorydata.getlistBrand());
        if (getProductsEither.isRight()) {
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/brands")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getBrands() {

        Response response;

        Either<IException, List <Brand>> getProductsEither = reportsBusiness.getBrands();
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/getsons")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getSonCat(InventoryReportDTO inventorydata) {

        Response response;

        Either<IException, List <Long>> getProductsEither = reportsBusiness.getSonCat(
                inventorydata.getlistStore());
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/inventory/Excel")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postExcel(ArrayList<excelRow> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcel(excelRows);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @POST
    @Path("/pendientes/excel")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postPendExcel(ArrayList<ExcelRowPendDTO> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelPend(excelRows);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @POST
    @Path("/kardex/Excel")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postExcelKardex(KardexDTO kardex) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelKardex(kardex);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @POST
    @Path("/logs/Excel")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postExcelLogs(LogDTO log) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelLog(log);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @POST
    @Path("/pedidos/Excel")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postExcelPedidos(@QueryParam("days_ask") Long days1, @QueryParam("days_between") Long days2, ArrayList<excelRowPedidos> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelPedidos(excelRows, days1, days2);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @POST
    @Path("/personas/Excel")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postExcelThirds(ArrayList<thirdReportDataDTO> excelRows) {

        Response response = Response.status(Response.Status.OK).entity(1).build();


        Either<IException, String> getProductsEither = reportsBusiness.postExcelThirds(excelRows);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/rotacion/Excel")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postExcelRotacion(ArrayList<excelRowRot> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelRotacion(excelRows);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/productos/Excel")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postExcelProductos(ArrayList<excelRowProd> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelProductos(excelRows);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/categorias/Excel")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postExcelCategorias(ArrayList<excelRowProd> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelCategorias(excelRows);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/facturas/Excel")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postExcelFacturas(ArrayList<excelRowFact> excelRows,@QueryParam("tipofactura") Long tipofactura) {
        Response response;

        Either<IException, String> getProductsEither = reportsBusiness.postExcelFacturas(excelRows, tipofactura);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/cajas/Excel")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postExcelCaja(ArrayList<excelRowCaja> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelCaja(excelRows);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/nodisponibles")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getNoDisponibles(@QueryParam("idbill") Long idbill,
                                     @QueryParam("listStore") List<Long> listStore,
                                     @QueryParam("code") String code) {

        Response response;

        Either<IException, Long> getProductsEither = reportsBusiness.getNoDisponibles( idbill,
                listStore,
                 code );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/ivatienda")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getIvaStore(@QueryParam("idstore") Long idstore) {

        Response response;

        Either<IException, String> getProductsEither = reportsBusiness.getIvaStore( idstore );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/sumnodisponibles")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getSumNoDisponibles(StorelistDTO dto) {

        Response response;

        Either<IException, List <NoDisp>> getProductsEither = reportsBusiness.getSumNoDisponibles(
                dto.getlistStore());
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/getData")
    @Timed
    @RolesAllowed({"Auth","PublicAPI"})
    public Response getData(@QueryParam("id_store") Long id_store) {

        Response response;

        Either<IException, List <Data>> getProductsEither = reportsBusiness.getData(id_store);

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }




    @GET
    @Path("/adminusers")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getAdminUserList(@QueryParam("idrol") Long idrol,
                                     @QueryParam("id_store") Long id_store) {

        Response response;

        Either<IException, List<AdminUser>> getProductsEither = reportsBusiness.getAdminUserList( idrol, id_store );
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


}

