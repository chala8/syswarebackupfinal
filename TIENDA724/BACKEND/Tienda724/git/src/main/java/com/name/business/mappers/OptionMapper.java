package com.name.business.mappers;


import com.name.business.entities.Option;
import com.name.business.entities.Pedido;
import com.name.business.entities.DetallePedido;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OptionMapper implements ResultSetMapper<Option> {

    @Override
    public Option map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Option(
                resultSet.getString("OPCION"),
                resultSet.getString("DESCRIPCION")
        );
    }
}
