package com.name.business.entities;

public class PlanillaDetail {

    private String NUM_DOCUMENTO;
    private String VALOR;
    private String STORE;
    private String ADDRESS;



    public PlanillaDetail(String NUM_DOCUMENTO,
             String VALOR,
             String STORE,
             String ADDRESS) {
        this.NUM_DOCUMENTO = NUM_DOCUMENTO;
        this.VALOR = VALOR;
        this.STORE =  STORE;
        this.ADDRESS =  ADDRESS;
    }

    public String getNUM_DOCUMENTO() {
        return NUM_DOCUMENTO;
    }

    public void setNUM_DOCUMENTO(String NUM_DOCUMENTO) {
        this.NUM_DOCUMENTO = NUM_DOCUMENTO;
    }


    public String getVALOR() {
        return VALOR;
    }

    public void setVALOR(String VALOR) {
        this.VALOR = VALOR;
    }


    public String getSTORE() {
        return STORE;
    }

    public void setSTORE(String STORE) {
        this.STORE = STORE;
    }


    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }
}
