package com.name.business.resources;


import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.Kazu724Business;
import com.name.business.businesses.CodeBusiness;
import com.name.business.entities.*;
import com.name.business.entities.completes.CodeComplete;
import com.name.business.representations.KazuPucDTO;
import com.name.business.representations.KazuStatusDTO;
import com.name.business.representations.KazuDetailDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;
import retrofit2.http.Query;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Path("/kazu724")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class kazu724Resource {

    private Kazu724Business kazu724Business;

    public kazu724Resource(Kazu724Business kazu724Business) {
        this.kazu724Business = kazu724Business;
    }


    @GET
    @Path("/getdoctype")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDocType() {

        Response response;

        Either<IException, List<docTypeKazu>> getProductsEither = kazu724Business.getDocType();
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }




    @POST
    @Path("/postDocument")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postDocument(@QueryParam("iddocumenttype") Long iddocumenttype,
                                 @QueryParam("iddocumentstatus") Long iddocumentstatus,
                                 @QueryParam("notes") String notes,
                                 @QueryParam("idstore") Long idstore,
                                 @QueryParam("idthirduser") Long idthirduser,
                                 @QueryParam("idthird") Long idthird,
                                 @QueryParam("docdetails") String docdetails) {

        Long getProductsEither = kazu724Business.postDocument(

                iddocumenttype,
                iddocumentstatus,
                notes,
                idstore,
                idthirduser,
                idthird,
                docdetails

        );

        return Response.status(Response.Status.OK).entity(getProductsEither).build();

    }




    @GET
    @Path("/getBalance")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getBalance(@QueryParam("id_store") Long id_store) {

        Response response;

        Either<IException, List<BalanceKazu> > getProductsEither = kazu724Business.getBalance(id_store);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @GET
    @Path("/getDocumentHeader")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDocumentHeader(@QueryParam("id_store") Long id_store,
                                      @QueryParam("date1") String date1,
                                      @QueryParam("date2") String date2,
                                      @QueryParam("statusList") String statusList,
                                      @QueryParam("typeList") String typeList) {

        Response response;

        Either<IException, List<kazu724Document>> getProductsEither = kazu724Business.getDocumentHeader(id_store,
                                                                                                        date1,
                                                                                                        date2,
                Arrays.asList(statusList.split(",")),
                Arrays.asList(typeList.split(",")));
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/getDetailsDocument")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDetailsDocument(@QueryParam("id_document") Long id_document) {

        Response response;

        Either<IException, List<kazu724Detail>> getProductsEither = kazu724Business.getDetailsDocument(id_document);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }




    @GET
    @Path("/getAccountTree")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getAccountTree() {

        Response response;

        Either<IException, List<FileNode>> getProductsEither = kazu724Business.getTreeAccount();
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }




    @GET
    @Path("/getdocstatus")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDocStatus() {

        Response response;

        Either<IException, List<docStatus>> getProductsEither = kazu724Business.getDocStatus();
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/getcodcuentagen")
    @Timed
    @RolesAllowed({"Auth"})
    public Response gecCodCuentaGen() {

        Response response;

        Either<IException, List<codigoCuenta>> getProductsEither = kazu724Business.gecCodCuentaGen();
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/getcodcuenta")
    @Timed
    @RolesAllowed({"Auth"})
    public Response gecCodCuenta(@QueryParam("cp") Long cp) {

        Response response;

        Either<IException, List<codigoCuenta>> getProductsEither = kazu724Business.gecCodCuenta(cp);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    //-----------------------------------------------------------------------------------------

    @POST
    @Path("/puc")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postPuc(KazuPucDTO kazuPucDTO){
        Response response;

         Long mailEither = kazu724Business.insertPuc(kazuPucDTO.getcc(),kazuPucDTO.getdescription(),kazuPucDTO.getccp(),kazuPucDTO.getid_country());


            response=Response.status(Response.Status.OK).entity(mailEither).build();

        return response;
    }


    @POST
    @Path("/doc")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postStatus(KazuStatusDTO kazuStatusDTO){
        Response response;

        Either<IException, Long> mailEither = kazu724Business.postStatus(kazuStatusDTO.getid_document_status(),kazuStatusDTO.getid_document_type(),kazuStatusDTO.getnotes(),kazuStatusDTO.getid_store(),kazuStatusDTO.getid_third_user());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }


    @PUT
    @Path("/updateDocument")
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateDocument(@QueryParam("id_status") Long id_status,
                               @QueryParam("notes") String notes,
                               @QueryParam("id_doc") Long id_doc){
        Response response;

        Long mailEither = kazu724Business.updateDocument(id_doc,id_status,notes);

        response=Response.status(Response.Status.OK).entity(mailEither).build();

        return response;
    }



    @DELETE
    @Path("/deleteDocumentDetail")
    @Timed
    @RolesAllowed({"Auth"})
    public Response deleteDocumentDetail(@QueryParam("id_detail") Long id_detail){
        Response response;

        Long mailEither = kazu724Business.deleteDocumentDetail(id_detail);

        response=Response.status(Response.Status.OK).entity(mailEither).build();

        return response;
    }


    @POST
    @Path("/detail")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postDocDetail(KazuDetailDTO kazuDetailDTO){
        Response response;

        Either<IException, Integer> mailEither = kazu724Business.postDocDetail(kazuDetailDTO.getcc(),kazuDetailDTO.getvalor(),kazuDetailDTO.getnaturaleza(),kazuDetailDTO.getnotes(),kazuDetailDTO.getid_document(),kazuDetailDTO.getid_country(),kazuDetailDTO.getId_third());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @POST
    @Path("/generateBalance")
    @Timed
    @RolesAllowed({"Auth"})
    public Response generarBalance(@QueryParam("id_store") Long id_store,
                                   @QueryParam("profundidad") Long profundidad,
                                   @QueryParam("fecha_inicial") String fecha_inicial,
                                   @QueryParam("fecha_final") String fecha_final){
        Response response;

        Integer mailEither= kazu724Business.generarBalance(id_store,
                profundidad,
                new Date(fecha_inicial),
                new Date(fecha_final));

            response=Response.status(Response.Status.OK).entity(mailEither).build();

        return response;
    }



}


