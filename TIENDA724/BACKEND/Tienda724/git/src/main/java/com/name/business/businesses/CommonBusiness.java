package com.name.business.businesses;

import com.name.business.DAOs.CommonDAO;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.representations.CommonDTO;
import com.name.business.representations.CommonStateDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.attributeSanitation;
import static com.name.business.sanitations.EntitySanitation.commonSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

public class CommonBusiness {

    private CommonDAO commonDAO;

    public CommonBusiness(CommonDAO commonDAO) {
        this.commonDAO = commonDAO;
    }

    /**
     *
     * @param common
     *
     * @return
     */
    public Either<IException, List<Common>> getCommons(Common common) {
        List<String> msn = new ArrayList<>();
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting consults Common ||||||||||||||||| ");
            return Either.right(commonDAO.read(commonSanitation(common)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException,Long> createCommon(CommonDTO commonDTO){
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        //System.out.println(commonDAO.getPkLast());
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting creation Attibute  ||||||||||||||||| ");
            if (commonDTO!=null){

                commonDAO.create(commonDTO);

                id_common = commonDAO.getPkLast();
                return Either.right(id_common);
            }else{
                //msn.add("It does not recognize Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     *
     * @param id_common
     * @param commonDTO
     * @return
     */
    public Either<IException, Long> updateCommon(Long id_common, CommonDTO commonDTO) {
        List<String> msn = new ArrayList<>();
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting update Common  ||||||||||||||||| ");
            if (commonDTO != null && (id_common!=null || id_common>0)) {

                // Common update
                commonDAO.update(formatoLongSql(id_common),validatorCommonDTO(id_common,commonDTO));

                return Either.right(id_common);

            } else {
                //msn.add("It does not recognize Common , probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    private CommonDTO validatorCommonDTO(Long id_common, CommonDTO commonDTO) {
        try {
            Common currentCommon=null;
            List<Common> commonList = commonDAO.read(new Common(id_common, null, null));
            if (commonList.size()>0){
                currentCommon = commonList.get(0);
            }

            // Validate Name inserted, if null, set the Name to the actual in the database, if not it is formatted to sql
            if (commonDTO.getName() == null || commonDTO.getName().equals("")) {
                commonDTO.setName(currentCommon.getName());
            } else {
                commonDTO.setName(formatoStringSql(commonDTO.getName()));
            }

            // Validate Name inserted, if null, set the Name to the actual in the database, if not it is formatted to sql
            if (commonDTO.getDescription() == null || commonDTO.getDescription().equals("")) {
                commonDTO.setDescription(currentCommon.getDescription());
            } else {
                commonDTO.setDescription(formatoStringSql(commonDTO.getDescription()));
            }
            return commonDTO;

        }catch (Exception e){
            return  null;
        }

    }


    /**
     * @param idCommon
     * @return
     */
    public Either<IException, Long> deleteCommon(Long idCommon) {
        List<String> msn = new ArrayList<>();
        try {
            if (idCommon != 0) {
                //msn.add("OK");
                //System.out.println("|||||||||||| Starting CHANGE STATE DELETE Common ||||||||||||||||| ");
                commonDAO.delete(formatoLongSql(idCommon), new Date());

                return Either.right(idCommon);
            } else {
                //msn.add("It does not recognize ID Common, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
