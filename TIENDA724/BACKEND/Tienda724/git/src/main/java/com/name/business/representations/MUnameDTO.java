package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;

public class MUnameDTO {


    private String MUName;
    private String MUDescription;
    private Long id_measure_unit_father;
    private Long id_third;

    @JsonCreator
    public MUnameDTO(@JsonProperty("MUName") String MUName,
                     @JsonProperty("MUDescription") String MUDescription,
                     @JsonProperty("id_measure_unit_father") Long id_measure_unit_father,
                     @JsonProperty("id_third") Long id_third) {
        this.MUName = MUName;
        this.MUDescription = MUDescription;
        this.id_measure_unit_father = id_measure_unit_father;
        this.id_third = id_third;
    }

    //-------------------------------------------------------------------------------------

    public String getMUName() {
        return MUName;
    }

    public void setMUName(String MUName) {
        this.MUName = MUName;
    }

    //-------------------------------------------------------------------------------------

    public String getMUDescription() {
        return MUDescription;
    }

    public void setMUDescription(String MUDescription) {
        this.MUDescription = MUDescription;
    }

    //-------------------------------------------------------------------------------------

    public Long getid_measure_unit_father() {
        return id_measure_unit_father;
    }

    public void setid_measure_unit_father(Long id_measure_unit_father) {
        this.id_measure_unit_father = id_measure_unit_father;
    }

    //-------------------------------------------------------------------------------------

    public Long getid_third() {
        return id_third;
    }

    public void setid_third(Long id_third) {
        this.id_third = id_third;
    }

    //-------------------------------------------------------------------------------------


}
