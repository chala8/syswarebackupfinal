package com.name.business.entities;


public class ProductThird {

    private Product product;
    private ProductThirdSimple  description;
    private Object code;


    public ProductThird(Product product, ProductThirdSimple description, Code code) {
        this.product = product;
        this.description = description;
        this.code = code;
    }

    public Object getCode() {
        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ProductThirdSimple getDescription() {
        return description;
    }

    public void setDescription(ProductThirdSimple description) {
        this.description = description;
    }
}