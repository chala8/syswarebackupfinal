package com.name.business.entities;

public class NewProductReport {

    private Long ID_PRODUCT_STORE;
    private Long ID_STORE;
    private Long ID_CODE;
    private String CODE;
    private String PRODUCT_STORE_NAME;
    private Long ID_PRODUCT;




    public NewProductReport(Long ID_PRODUCT_STORE,
                             Long ID_STORE,
                             Long ID_CODE,
                             String CODE,
                             String PRODUCT_STORE_NAME,
                             Long ID_PRODUCT) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
        this.ID_STORE = ID_STORE;
        this.ID_CODE =  ID_CODE;
        this.CODE = CODE;
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
        this.ID_PRODUCT =  ID_PRODUCT;
    }
    public Long getID_PRODUCT_STORE() {
        return ID_PRODUCT_STORE;
    }
    public void setID_PRODUCT_STORE(Long ID_PRODUCT_STORE) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
    }

    public Long getID_STORE() {
        return ID_STORE;
    }
    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    public Long getID_CODE() {
        return ID_CODE;
    }
    public void setID_CODE(Long ID_CODE) {
        this.ID_CODE = ID_CODE;
    }

    public String getCODE() {
        return CODE;
    }
    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getPRODUCT_STORE_NAME() {
        return PRODUCT_STORE_NAME;
    }
    public void setPRODUCT_STORE_NAME(String PRODUCT_STORE_NAME) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
    }

    public Long getID_PRODUCT() {
        return ID_PRODUCT;
    }
    public void setID_PRODUCT(Long ID_PRODUCT) {
        this.ID_PRODUCT = ID_PRODUCT;
    }


}
