package com.name.business.mappers;

import com.name.business.entities.StorageName;
import com.name.business.entities.Register;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RegisterBoxMapper implements ResultSetMapper<Register>{

    @Override
    public Register map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Register(
                resultSet.getLong("ID_CAJA"),
                resultSet.getString("CAJA_NAME"),
                resultSet.getLong("CAJA_NUMBER")
        );
    }
}