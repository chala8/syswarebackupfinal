package com.name.business.entities;

public class CajaReport {

    private Long ID_CAJA;
    private String DESCRIPTION;


    public CajaReport(Long ID_CAJA, String DESCRIPTION) {
        this.ID_CAJA = ID_CAJA;
        this.DESCRIPTION = DESCRIPTION;
    }


    public Long getID_CAJA() {
        return ID_CAJA;
    }

    public void setID_CAJA(Long ID_CAJA) {
        this.ID_CAJA = ID_CAJA;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }


}
