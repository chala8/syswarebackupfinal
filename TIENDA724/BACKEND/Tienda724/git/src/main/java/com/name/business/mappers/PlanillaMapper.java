package com.name.business.mappers;

import com.name.business.entities.Planilla;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PlanillaMapper implements ResultSetMapper<Planilla>{

    @Override
    public Planilla map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Planilla(
                resultSet.getLong("ID_PLANILLA"),
                resultSet.getString("FECHA_INICIO"),
                resultSet.getString("NUM_PLANILLA"),
                resultSet.getString("PLACA"),
                resultSet.getString("CONDUCTOR"),
                resultSet.getString("STATUS")
        );
    }
}