package com.name.business.mappers;

import com.name.business.entities.Asociacion;
import com.name.business.entities.AttributeList;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AsociacionMapper implements ResultSetMapper<Asociacion>{

    @Override
    public Asociacion map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Asociacion(
                        resultSet.getString("PRODUCTO_HIJO"),
                        resultSet.getString("CODIGO_HIJO"),
                        resultSet.getString("PRODUCTO_PADRE"),
                        resultSet.getString("CODIGO_PADRE"),
                        resultSet.getLong("factor"),
                        resultSet.getLong("pct_umbral")

        );
    }
}
