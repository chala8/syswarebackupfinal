package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class InventoryDTO {
    private Long id_third;
    private CommonDTO commonDTO;
    private CommonStateDTO stateDTO;
    private List<InventoryDetailDTO> detailDTOS;

    @JsonCreator
    public InventoryDTO(@JsonProperty("id_third") Long id_third, @JsonProperty("common") CommonDTO commonDTO,
                        @JsonProperty("state") CommonStateDTO stateDTO,
                        @JsonProperty("details") List<InventoryDetailDTO> detailDTOS) {
        this.id_third = id_third;
        this.commonDTO = commonDTO;
        this.stateDTO = stateDTO;
        this.detailDTOS = detailDTOS;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }

    public List<InventoryDetailDTO> getDetailDTOS() {
        return detailDTOS;
    }

    public void setDetailDTOS(List<InventoryDetailDTO> detailDTOS) {
        this.detailDTOS = detailDTOS;
    }


}
