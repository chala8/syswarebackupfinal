package com.name.business.mappers;


import com.name.business.entities.Dato;
import com.name.business.entities.DatosTienda;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DatosTiendaMapper implements ResultSetMapper<DatosTienda> {

    @Override
    public DatosTienda map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DatosTienda(
                resultSet.getLong("ID_THIRD"),
                resultSet.getLong("ID_STORE_CLIENTE"),
                resultSet.getString("TIENDA"),
                resultSet.getString("CLIENTE")
        );
    }
}
