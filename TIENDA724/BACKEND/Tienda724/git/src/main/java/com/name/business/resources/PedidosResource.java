package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.PedidosBusiness;
import com.name.business.businesses.PriceListBusiness;
import com.name.business.entities.*;
import com.name.business.representations.PedidoDTO;
import com.name.business.representations.PlanillaDTO;
import com.name.business.representations.PlanillaDetailDTO;
import com.name.business.representations.PlanillaPdfDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.unstable.BindIn;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Path("/pedidos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PedidosResource {
    private PedidosBusiness pedidosBusiness;


    public PedidosResource(PedidosBusiness pedidosBusiness) {
        this.pedidosBusiness = pedidosBusiness;
    }



    @GET
    @Path("/ps")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getOwnBarCode(
            @QueryParam("id_product_store") Long id_product_store){
        Response response;

        Either<IException, String> getProducts = pedidosBusiness.getOwnBarCode(id_product_store);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/vehiculos")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getVehiculos(){
        Response response;

        Either<IException, List<Vehiculo>> getProducts = pedidosBusiness.getVehiculos();

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/vehiculos/ids")
    @Timed
    public Response getVehiculosids(){
        Response response;

        Either<IException, List<Long>> getProducts = pedidosBusiness.getVehiculosids();

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @PUT
    @Path("/closeplanilla")
    @Timed
    @RolesAllowed({"Auth"})
    public Response updatePlanilla(@QueryParam("observaciones") String observaciones,
                                   @QueryParam("idplanilla") Long idplanilla){
        Response response;
        Either<IException, Integer> allViewOffertsEither = pedidosBusiness.updatePlanilla(observaciones, idplanilla);
        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }




    @PUT
    @Path("/updateUmbral")
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateUmbral(@QueryParam("pctumbral") Long pctumbral,
                                 @QueryParam("barcodehijo") String barcodehijo,
                                 @QueryParam("idstorehijo") Long idstorehijo){
        Response response;
        Either<IException, Integer> allViewOffertsEither = pedidosBusiness.updateUmbral(pctumbral, barcodehijo, idstorehijo);
        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

/*
    @GET
    @Path("/planillas")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPlanillas(@QueryParam("idvehiculo") Long idvehiculo,
                                 @QueryParam("idstore") Long idstore){
        Response response;

        Either<IException, List<Planilla>> getProducts = pedidosBusiness.getPlanillas(idvehiculo,idstore);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }
*/


    @GET
    @Path("/planillas")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPlanillasV2(@QueryParam("idvehiculo") String idvehiculo,
                                   @QueryParam("status") String status,
                                   @QueryParam("idstore") Long idstore,
                                   @QueryParam("date1") String Date1,
                                   @QueryParam("date2") String Date2){
        Response response;
        List<String> items2 = Arrays.asList(idvehiculo.split("\\s*,\\s*"));
        List<String> items = Arrays.asList(status.split("\\s*,\\s*"));
        Either<IException, List<Planilla>> getProducts = pedidosBusiness.getPlanillasV2(items2,items,idstore,Date1,Date2);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @GET
    @Path("/asociaciones")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPlanillasV2(@QueryParam("idstorep") Long idstorep, @QueryParam("idstoreh") Long idstoreh){
        Response response;
        Either<IException, List<Asociacion>> getProducts = pedidosBusiness.getAsociaciones(idstorep, idstoreh);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/getVendedoresDistribuidor")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getVendedoresDistribuidor(@QueryParam("id_store_cliente") Long id_store_cliente,
                                              @QueryParam("id_app") Long id_app){
        Response response;
        Either<IException, List<VendedorDistribuidor>> getProducts = pedidosBusiness.getVendedoresDistribuidor(id_store_cliente, id_app);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/getIdStores")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getIdStores(@QueryParam("id_third") Long id_third){
        Response response;
        Either<IException, List<Long>> getProducts = pedidosBusiness.getIdStores(id_third);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/planillas/detalles")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDetallesPlanillas(@QueryParam("idplanilla") Long idplanilla){
        Response response;

        Either<IException, List<DetallePlanilla>> getProducts = pedidosBusiness.getDetallesPlanillas(idplanilla);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @GET
    @Path("/planillaDetail")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPlanillaDetail(@QueryParam("idplanilla") Long idplanilla){
        Response response;

        Either<IException, List<PlanillaDetail>> getProducts = pedidosBusiness.getPlanillaDetail(idplanilla);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @POST
    @Path("/procedure")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postProcedure(    @QueryParam("idbill") Long idbill,
                                      @QueryParam("idbillstate") Long idbillstate,
                                      @QueryParam("code") Long code,
                                      @QueryParam("idstore") Long idstore,
                                      @QueryParam("cantidad") Long cantidad,
                                      @QueryParam("tipo") String tipo){
        Response response;

        Either<IException, String > getProducts =  pedidosBusiness.postProcedure(idbill,
                idbillstate,
                code,
                idstore,
                cantidad,
                tipo);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @POST
    @Path("/actualizarInventarioACero")
    @Timed
    @RolesAllowed({"Auth"})
    public Response actualizar_inventario_a_cero(
                                      @QueryParam("idstore") Long idstore){
        Response response;

        Either<IException, Long > getProducts =  pedidosBusiness.actualizar_inventario_a_cero(
                idstore);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/agregarConversion")
    @Timed
    @RolesAllowed({"Auth"})
    public Response agregar_conversion(
            @QueryParam("barcodehijo") String barcodehijo,
            @QueryParam("barcodepadre") String barcodepadre,
            @QueryParam("factor") Long factor,
            @QueryParam("prioridad") Long prioridad){
        Response response;

        Either<IException, Long > getProducts =  pedidosBusiness.agregar_conversion(barcodehijo,
                barcodepadre,
                factor,
                prioridad);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/eliminarAsociacion")
    @Timed
    @RolesAllowed({"Auth"})
    public Response eliminar_asociacion(
            @QueryParam("barcodehijo") String barcodehijo,
            @QueryParam("barcodepadre") String barcodepadre,
            @QueryParam("idstorecliente") Long idstorecliente,
            @QueryParam("idstoreprov") Long idstoreprov){
        Response response;

        Either<IException, Long > getProducts =  pedidosBusiness.eliminar_asociacion(barcodehijo,
                barcodepadre,
                idstorecliente,
                idstoreprov);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




    @POST
    @Path("/newPlanilla")
    @Timed
    @RolesAllowed({"Auth"})
    public Response insertNewPlanilla(@QueryParam("idvehiculo") Long idvehiculo,
                                      @QueryParam("idstore") Long idstore){
        Response response;

        Either<IException, Long > getProducts =  pedidosBusiness.insertNewPlanilla(idvehiculo,
                idstore);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @POST
    @Path("/pdf")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postpdf(PlanillaPdfDTO planilla){
        Response response;
        PlanillaDTO master = planilla.getmaster();
        List<PlanillaDetailDTO> detalles = planilla.getdetalles();
        //System.out.println(planilla.getlogo());
        Either<IException, String> getProducts = pedidosBusiness.postpdf(
                master,
                detalles,
                planilla.getlogo()
        );

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
/*

        //System.out.println("---------------------MASTER--------------------");
        //System.out.println(master.getconductor());
        //System.out.println(master.getfecha_INICIO());
        //System.out.println(master.getid_PLANILLA());
        //System.out.println(master.getnum_PLANILLA());
        //System.out.println(master.getplaca());
        //System.out.println("---------------------DETALLES--------------------");
        for( PlanillaDetailDTO element : detalles ){
            //System.out.println(element.getaddress());
            //System.out.println(element.getnum_DOCUMENTO());
            //System.out.println(element.getstore());
            //System.out.println(element.getvalor());
        }
  */      return response;
    }

    @GET
    @Path("/own")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPsId(
            @QueryParam("code") String code,
            @QueryParam("id_store") Long id_store){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.getPsId(code,id_store);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @GET
    @Path("/master")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPedidos(
            @QueryParam("id_store") Long id_store,
            @QueryParam("id_bill_state") Long id_bill_state,
            @QueryParam("id_bill_type") Long id_bill_type){
        Response response;

        Either<IException, List<Pedido>> getProducts = pedidosBusiness.getPedidos(id_store,id_bill_state,id_bill_type);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @POST
    @Path("/detalles")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDetallesPedidos(PedidoDTO priceListDTO){
        Response response;

        Either<IException, List<DetallePedido>> mailEither = pedidosBusiness.getDetallesPedidos(priceListDTO.getlistaTipos());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }
}
