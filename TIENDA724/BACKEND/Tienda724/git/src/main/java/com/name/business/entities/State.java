package com.name.business.entities;

public class State {

    private Long id_state;
    private String state_name;


    public State( Long id_state,
                    String state_name) {
        this.id_state = id_state;
        this.state_name = state_name;
    }

    //---------------------------------------------------------------------------

    public Long getid_state() {
        return id_state;
    }

    public void setid_state(Long id_state) {
        this.id_state = id_state;
    }

    //---------------------------------------------------------------------------

    public String getstate_name() {
        return state_name;
    }

    public void setstate_name(String state_name) {
        this.state_name = state_name;
    }

    //---------------------------------------------------------------------------

}
