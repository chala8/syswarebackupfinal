package com.name.business;

import ch.qos.logback.core.net.server.Client;
import com.name.business.DAOs.*;
import com.name.business.Threads.FCMThread;
import com.name.business.businesses.*;
import com.name.business.resources.*;
import com.name.business.utils.auth.AuthenticateFilter;
import com.name.business.utils.auth.CustomAuthenticator;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.java8.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.commons.dbcp2.BasicDataSource;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;
import java.util.Timer;
import java.util.Calendar;

import static com.name.business.utils.constans.K.URI_BASE;


public class Store724 extends Application<Store724AppConfiguration> {



    public static void main(String[] args) throws Exception  {
        new Store724().run(args);

    }





    @Override
    public void initialize(Bootstrap<Store724AppConfiguration> bootstrap) {

        bootstrap.addBundle(new Java8Bundle());
        bootstrap.addBundle(new MultiPartBundle());


    }
    @Override
    public void run(Store724AppConfiguration configuration, Environment environment) throws Exception {

        environment.jersey().setUrlPattern(URI_BASE+"/*");

        // Enable CORS headers
        final FilterRegistration.Dynamic corsFilter =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        corsFilter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        corsFilter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,Key_Server");
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        corsFilter.setInitParameter("allowCredentials", "true");

        // Add URL mapping
        corsFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");



        // Se estable el ambiente de coneccion con JDBI con la base de datos de oracle

        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "oracle");

        final PedidosDAO pedidosDAO = jdbi.onDemand(PedidosDAO.class);
        final PedidosBusiness pedidosBusiness = new PedidosBusiness(pedidosDAO);

        final Kazu724DAO kazu724DAO = jdbi.onDemand(Kazu724DAO.class);
        final Kazu724Business kazu724Business = new Kazu724Business(kazu724DAO);

        final MeasureUnitNameDAO measureUnitNameDAO= jdbi.onDemand(MeasureUnitNameDAO.class);
        final MeasureUnitNameBusiness measureUnitNameBusiness= new MeasureUnitNameBusiness(measureUnitNameDAO);

        final LegalDataDAO legalDataDao= jdbi.onDemand(LegalDataDAO.class);
        final LegalDataBusiness legalDataBusiness= new LegalDataBusiness(legalDataDao);

        final CommonStateDAO commonStateDAO= jdbi.onDemand(CommonStateDAO.class);
        final CommonStateBusiness commonStateBusiness= new CommonStateBusiness(commonStateDAO);

        final CommonDAO commonDAO= jdbi.onDemand(CommonDAO.class);
        final CommonBusiness  commonBusiness= new CommonBusiness(commonDAO);

        final AttributeValueDAO attributeValueDAO = jdbi.onDemand(AttributeValueDAO.class);
        final AttributeValueBusiness attributeValueBusiness = new AttributeValueBusiness(attributeValueDAO,
                commonBusiness,commonStateBusiness);

        final AttributeDAO attributeDAO= jdbi.onDemand(AttributeDAO.class);
        final AttributeBusiness attributeBusiness= new AttributeBusiness(attributeDAO,commonBusiness,commonStateBusiness,attributeValueBusiness);


        final AttributeDetailListDAO attributeDetailListDAO= jdbi.onDemand(AttributeDetailListDAO.class);
        final AttributeDetailListBusiness attributeDetailListBusiness= new AttributeDetailListBusiness(
                attributeDetailListDAO,attributeValueBusiness,commonStateBusiness);

        final AttributeListDAO  attributeListDAO= jdbi.onDemand(AttributeListDAO.class);
        final AttributeListBusiness attributeListBusiness= new AttributeListBusiness(attributeListDAO,commonStateBusiness,attributeDetailListBusiness);



        final CategoryDAO categoryDAO= jdbi.onDemand(CategoryDAO.class);
        final CategoryBusiness categoryBusiness= new CategoryBusiness(categoryDAO, commonBusiness, commonStateBusiness);


        final CategoryNameDAO categoryNameDAO = jdbi.onDemand(CategoryNameDAO.class);
        final CategoryNameBusiness categoryNameBusiness= new CategoryNameBusiness(categoryNameDAO);


        final ProductNameDAO productNameDAO = jdbi.onDemand(ProductNameDAO.class);
        final ProductNameBusiness productNameBusiness= new ProductNameBusiness(productNameDAO);


        final CodeDAO codeDAO= jdbi.onDemand(CodeDAO.class);
        final CodeBusiness codeBusiness= new CodeBusiness(codeDAO,commonStateBusiness,attributeDetailListBusiness,attributeBusiness);

        final ProductDAO productDAO= jdbi.onDemand(ProductDAO.class);
        final ProductBusiness productBusiness= new ProductBusiness(productDAO,commonBusiness,commonStateBusiness,categoryBusiness);

        final MeasureUnitDAO measureUnitDAO= jdbi.onDemand(MeasureUnitDAO.class);
        final MeasureUnitBusiness measureUnitBusiness = new MeasureUnitBusiness(measureUnitDAO,commonBusiness,commonStateBusiness);

        final ProductThirdDAO productThirdDAO= jdbi.onDemand(ProductThirdDAO.class);
        final ProductThirdBusiness productThirdBusiness = new ProductThirdBusiness(productThirdDAO,productBusiness,commonBusiness,
                commonStateBusiness,measureUnitBusiness,attributeListBusiness,categoryBusiness,codeBusiness);

        final InventoryDetailDAO inventoryDetailDAO = jdbi.onDemand(InventoryDetailDAO.class);
        final InventoryDetailBusiness inventoryDetailBusiness= new InventoryDetailBusiness(inventoryDetailDAO,commonStateBusiness,productThirdBusiness);

        final InventoryDAO inventoryDAO= jdbi.onDemand(InventoryDAO.class);
        final InventoryBusiness inventoryBusiness= new InventoryBusiness(inventoryDAO,commonBusiness,commonStateBusiness,inventoryDetailBusiness,attributeBusiness,attributeDetailListBusiness,categoryBusiness,codeBusiness);

        final PriceListDAO priceListDAO = jdbi.onDemand(PriceListDAO.class);
        final PriceListBusiness priceListBusiness = new PriceListBusiness(priceListDAO,commonBusiness,commonStateBusiness);

        final TaxTariffDAO taxTariffDAO=jdbi.onDemand(TaxTariffDAO.class);
        final TaxTariffBusiness taxTariffBusiness= new TaxTariffBusiness(taxTariffDAO,commonBusiness,commonStateBusiness);

        final DocumentDAO documentDAO= jdbi.onDemand(DocumentDAO.class);
        final DocumentBusiness documentBusiness= new DocumentBusiness(documentDAO);

        final StoreDAO storeDAO= jdbi.onDemand(StoreDAO.class);
        final StoreBusiness storeBusiness= new StoreBusiness(storeDAO);

        final ReportsDAO reportsDao= jdbi.onDemand(ReportsDAO.class);
        final ReportsBusiness reportsBusiness= new ReportsBusiness(reportsDao);


        Store724DatabaseConfiguration routesDatabaseConfiguration = configuration.getDatabaseConfiguration();
        BasicDataSource datasource = createDataSource(routesDatabaseConfiguration);

        //AUTH
        AuthenticateFilter filter = new AuthenticateFilter(new CustomAuthenticator());
        environment.jersey().register(new AuthDynamicFeature(filter));
        environment.jersey().register(RolesAllowedDynamicFeature.class);
        ////////

        environment.jersey().register( new PedidosResource(pedidosBusiness));
        environment.jersey().register( new kazu724Resource(kazu724Business));
        environment.jersey().register( new ReportsResources(reportsBusiness));
        environment.jersey().register( new ProductNameResource(productNameBusiness));
        environment.jersey().register( new LegalDataResource(legalDataBusiness));
        environment.jersey().register( new StoreResource(storeBusiness));
        environment.jersey().register( new MeasureUnitNameResource(measureUnitNameBusiness));
        environment.jersey().register( new CategoryResource(categoryBusiness));
        environment.jersey().register( new CategoryNameResource(categoryNameBusiness));
        environment.jersey().register( new AttributeResource(attributeBusiness));
        environment.jersey().register( new AttributeValueResource(attributeValueBusiness));
        environment.jersey().register( new AttributeListResurce(attributeListBusiness));
        environment.jersey().register( new AttributeDetailListResource( attributeDetailListBusiness));
        environment.jersey().register( new ProductResource( productBusiness));
        environment.jersey().register( new ProductThirdResource(productThirdBusiness));
        environment.jersey().register( new MeasureUnitResource(measureUnitBusiness));
        environment.jersey().register( new InventoryDetailResource(inventoryDetailBusiness));
        environment.jersey().register( new InventoryResource(inventoryBusiness));
        environment.jersey().register( new TaxTariffResource(taxTariffBusiness));
        environment.jersey().register( new PriceListResource(priceListBusiness));
        environment.jersey().register( new CodeResource(codeBusiness));
        environment.jersey().register( new DocumentResource(documentBusiness));



        // apply security on http request
        // addAuthorizationFilterAndProvider(environment,tokenBusiness);


        final FCM_DAO fcmDao =jdbi.onDemand(FCM_DAO.class);
        final FCMBusiness fcmBusiness= new FCMBusiness(fcmDao);



        final FCMThread fcmThread= new FCMThread(fcmBusiness);









        Timer timer = new Timer();

        //timer.scheduleAtFixedRate(fcmThread, 0, 30*1000);   //cada 5 minutos




    }
    /**
     * @Método:  Hace  la configuración para establecer conexión con las bases de datos que se
     * va a utilizar como persistencia del servidor
     * */
    private BasicDataSource createDataSource(Store724DatabaseConfiguration store724DatabaseConfiguration){

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(store724DatabaseConfiguration.getDriverClassName());
        basicDataSource.setUrl(store724DatabaseConfiguration.getUrl());
        basicDataSource.setUsername(store724DatabaseConfiguration.getUsername());
        basicDataSource.setPassword(store724DatabaseConfiguration.getPassword());
        basicDataSource.addConnectionProperty("characterEncoding", "UTF-8");

        basicDataSource.setRemoveAbandonedOnMaintenance(true);
        basicDataSource.setRemoveAbandonedOnBorrow(true);
        basicDataSource.setRemoveAbandonedTimeout(30000);

        return basicDataSource;

    }




}
