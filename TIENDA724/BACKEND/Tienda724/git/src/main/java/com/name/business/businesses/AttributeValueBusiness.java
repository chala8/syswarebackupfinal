package com.name.business.businesses;

import com.name.business.DAOs.AttributeValueDAO;
import com.name.business.entities.AttributeValue;
import com.name.business.entities.Common;
import com.name.business.entities.CommonSimple;
import com.name.business.entities.CommonState;
import com.name.business.representations.AttributeValueDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.attributeValueSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class AttributeValueBusiness {

    private AttributeValueDAO attributeValueDAO;

    private CommonBusiness commonBusiness;
    private CommonStateBusiness commonStateBusiness;


    public AttributeValueBusiness(AttributeValueDAO attributeValueDAO, CommonBusiness commonBusiness, CommonStateBusiness commonStateBusiness) {
        this.attributeValueDAO = attributeValueDAO;
        this.commonBusiness = commonBusiness;
        this.commonStateBusiness = commonStateBusiness;
    }


    public Either<IException,Long> createAttributeValue( Long id_attribute,List<AttributeValueDTO> attributeValueDTOList){
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_attribute_value = null;
        Long id_common_state = null;

        //System.out.println(attributeValueDAO.getPkLast());
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting creation Attribute Value ||||||||||||||||| ");
            if (attributeValueDTOList!=null && attributeValueDTOList.size()>0){

                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorAttributeID(id_attribute).equals(false)){
                    //msn.add("It ID Attribute does not exist register on  Attribute, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                for( AttributeValueDTO attributeValueDTO:attributeValueDTOList){
                    if (attributeValueDTO.getCommonDTO() != null) {
                        Either<IException, Long> commonThirdEither = commonBusiness.createCommon(attributeValueDTO.getCommonDTO());
                        if (commonThirdEither.isRight()) {
                            id_common = commonThirdEither.right().value();
                        }
                    }

                    if (attributeValueDTO.getStateDTO() != null) {
                        Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(attributeValueDTO.getStateDTO());
                        if (commonThirdEither.isRight()) {
                            id_common_state = commonThirdEither.right().value();
                        }
                    }


                }


                attributeValueDAO.create(formatoLongSql(id_attribute),formatoLongSql(id_common),formatoLongSql(id_common_state));
                id_attribute_value = attributeValueDAO.getPkLast();

                return Either.right(id_attribute_value);
            }else{
                //msn.add("It does not recognize Attibute Value, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_attribute_value
     * @param attributeValueDTO
     * @return
     */
    public Either<IException, Long> updateAttributeValue(Long id_attribute_value, Long id_attribute,AttributeValueDTO attributeValueDTO) {
        List<String> msn = new ArrayList<>();

        AttributeValue actualAttributeValue=null;

        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting update Attribute Value||||||||||||||||| ");


                //TODO validate the  id attribute if exist on database a register with this id
                if (id_attribute_value!=null && getValidatorID(id_attribute_value).equals(false)){
                    //msn.add("It ID Attribute Value does not exist register on  Attribute Value, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                //TODO validate the  id attribute if exist on database a register with this id
                if (id_attribute!=null && validatorAttributeID(id_attribute).equals(false)){
                    //msn.add("It ID Attribute does not exist register on  Attribute, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<AttributeValue> read = attributeValueDAO.read(new AttributeValue(id_attribute_value, null, new Common(null,null,null), new CommonState(null,null,null,null)));

                if (read.size()<=0){
                    //msn.add("It does not recognize ID Attribute Value, probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = attributeValueDAO.readCommons(formatoLongSql(id_attribute_value), formatoLongSql(id_attribute));
                if (readCommons.size()>0){
                    for (CommonSimple idCommons:readCommons){
                        // Common basic info update
                        commonBusiness.updateCommon(idCommons.getId_common(), attributeValueDTO.getCommonDTO());

                        // Common  State business update
                        commonStateBusiness.updateCommonState(idCommons.getId_common_state(), attributeValueDTO.getStateDTO());
                    }

                }




                actualAttributeValue = read.get(0);



                // Attribute update
                //attributeValueDAO.update(id_attribute_value,getId_attribute());

                return Either.right(id_attribute_value);

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *
     * @param attributeValue
     *
     * @return
     */
    public Either<IException, List<AttributeValue>> getAttributeValues(AttributeValue attributeValue) {
        List<String> msn = new ArrayList<>();
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting consults Attribute value ||||||||||||||||| ");
            AttributeValue attributeDetailListSanitation = attributeValueSanitation(attributeValue);
            return Either.right(attributeValueDAO.read(attributeDetailListSanitation));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id_attribute_value
     * @return
     */
    public Either<IException, Long> deleteAttributeValue(Long id_attribute_value,Long id_attribute) {
        List<String> msn = new ArrayList<>();

        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting delete Attribute  List ||||||||||||||||| ");
            if (id_attribute_value != null && id_attribute_value > 0) {
                List<CommonSimple> readCommons = attributeValueDAO.readCommons(formatoLongSql(id_attribute_value),formatoLongSql(id_attribute));
                if (readCommons.size()>0){
                    for (CommonSimple idCommons:readCommons){
                        commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                    }

                }

                //TODO when it would be determine how to delete common
                return Either.right(id_attribute_value);
            } else {
                //msn.add("It does not recognize ID Attribute List, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorAttributeID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = attributeValueDAO.getValidatorAttributeID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }
    /**
     * @param id
     * */
    public Boolean getValidatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = attributeValueDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }


}
