package com.name.business.entities;

public class CategoryReport {

    private String PRODUCT;
    private Long VENTA;
    private Long COSTO;
    private Long UTILIDAD;
    private Double PCT_MARGEN;
    private Long NUMVENTAS;
    private String OWNBARCODE;
    private String MARCA;
    private String LINEA;
    private String CATEGORIA;


    public CategoryReport(String CATEGORIA,
            Long VENTA,
            Long COSTO,
            Long UTILIDAD,
            Double PCT_MARGEN,
            Long NUMVENTAS,
            String LINEA) {
        this.LINEA = LINEA;
        this.CATEGORIA = CATEGORIA;
        this.VENTA = VENTA;
        this.COSTO = COSTO;
        this.UTILIDAD = UTILIDAD;
        this.PCT_MARGEN = PCT_MARGEN;
        this.NUMVENTAS = NUMVENTAS;
    }

    public CategoryReport(
            String OWNBARCODE,
            String PRODUCT,
            Long VENTA,
            Long COSTO,
            Long UTILIDAD,
            Double PCT_MARGEN,
            Long NUMVENTAS,
            String MARCA,
            String LINEA,
            String CATEGORIA) {
        this.OWNBARCODE = OWNBARCODE;
        this.PRODUCT = PRODUCT;
        this.VENTA = VENTA;
        this.COSTO = COSTO;
        this.UTILIDAD = UTILIDAD;
        this.PCT_MARGEN = PCT_MARGEN;
        this.NUMVENTAS = NUMVENTAS;
        this.MARCA = MARCA;
        this.LINEA = LINEA;
        this.CATEGORIA = CATEGORIA;
    }
    public String getOWNBARCODE() {
        return OWNBARCODE;
    }

    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }

    //-------------------------------------------------------------------------

    public Long getNUMVENTAS() {
        return NUMVENTAS;
    }

    public void setNUMVENTAS(Long NUMVENTAS) {
        this.NUMVENTAS = NUMVENTAS;
    }

    //-------------------------------------------------------------------------

    public Long getVENTA() {
        return VENTA;
    }

    public void setVENTA(Long VENTA) {
        this.VENTA = VENTA;
    }

    //-------------------------------------------------------------------------

    public String getCATEGORIA() {
        return CATEGORIA;
    }

    public void setCATEGORIA(String CATEGORIA) {
        this.CATEGORIA = CATEGORIA;
    }

    //---------------------------------------------------------------------------

    public Long getCOSTO() {
        return COSTO;
    }

    public void setCOSTO(Long COSTO) {
        this.COSTO = COSTO;
    }

    //-------------------------------------------------------------------------

    public Long getUTILIDAD() {
        return UTILIDAD;
    }

    public void setUTILIDAD(Long UTILIDAD) {
        this.UTILIDAD = UTILIDAD;
    }

    //-------------------------------------------------------------------------


    public Double getPCT_MARGEN() {
        return PCT_MARGEN;
    }

    public void setPCT_MARGEN(Double PCT_MARGEN) {
        this.PCT_MARGEN = PCT_MARGEN;
    }

    public String getMARCA() {
        return MARCA;
    }

    public void setMARCA(String MARCA) {
        this.MARCA = MARCA;
    }

    public String getLINEA() {
        return LINEA;
    }

    public void setLINEA(String LINEA) {
        this.LINEA = LINEA;
    }

    public String getPRODUCT() {
        return PRODUCT;
    }

    public void setPRODUCT(String PRODUCT) {
        this.PRODUCT = PRODUCT;
    }

    //-------------------------------------------------------------------------


}
