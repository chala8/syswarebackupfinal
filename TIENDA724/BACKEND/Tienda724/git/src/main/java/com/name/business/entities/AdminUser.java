package com.name.business.entities;

public class AdminUser {

    private String USUARIO;
    private String CLAVE;


    public AdminUser(String USUARIO, String CLAVE) {
        this.USUARIO = USUARIO;
        this.CLAVE = CLAVE;
    }

    public String getUSUARIO() {
        return USUARIO;
    }

    public void setUSUARIO(String USUARIO) {
        this.USUARIO = USUARIO;
    }

    public String getCLAVE() {
        return CLAVE;
    }

    public void setCLAVE(String CLAVE) {
        this.CLAVE = CLAVE;
    }
}
