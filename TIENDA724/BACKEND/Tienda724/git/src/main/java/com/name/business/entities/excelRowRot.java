package com.name.business.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class excelRowRot {

    private Long cantidadactual;
    private String categoria;
    private Long cantidadvendida;
    private String linea;
    private String marca;
    private Double dias_rotacion;
    private String producto;

    @JsonCreator
    public excelRowRot(
            @JsonProperty("cantidadactual") Long cantidadactual,
            @JsonProperty("categoria") String categoria,
            @JsonProperty("cantidadvendida") Long cantidadvendida,
            @JsonProperty("linea") String linea,
            @JsonProperty("marca") String marca,
            @JsonProperty("dias_ROTACION") Double dias_rotacion,
            @JsonProperty("producto") String producto) {
        this.cantidadactual = cantidadactual;
        this.categoria = categoria;
        this.cantidadvendida = cantidadvendida;
        this.linea = linea;
        this.marca = marca;
        this.dias_rotacion = dias_rotacion;
        this.producto = producto;
    }

    public Double getdias_rotacion() {
        return dias_rotacion;
    }

    public void setdias_rotacion(Double dias_rotacion) {
        this.dias_rotacion = dias_rotacion;
    }


    public String getproducto() {
        return producto;
    }

    public void setproducto(String producto) {
        this.producto = producto;
    }


    public String getmarca() {
        return marca;
    }

    public void setmarca(String marca) {
        this.marca = marca;
    }


    public String getlinea() {
        return linea;
    }

    public void setlinea(String linea) {
        this.linea = linea;
    }


    public Long getcantidadvendida() {
        return cantidadvendida;
    }

    public void setcantidadvendida(Long cantidadvendida) {
        this.cantidadvendida = cantidadvendida;
    }


    public Long getcantidadactual() {
        return cantidadactual;
    }

    public void setcantidadactual(Long cantidadactual) {
        this.cantidadactual = cantidadactual;
    }


    public String getcategoria() {
        return categoria;
    }

    public void setcategoria(String categoria) {
        this.categoria = categoria;
    }

}
