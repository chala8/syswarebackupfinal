package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;
import java.util.List;

public class PedidoDTO {


    private List<Long> listaTipos;

    @JsonCreator
    public PedidoDTO(@JsonProperty("listaTipos") List<Long>  listaTipos) {
        this.listaTipos = listaTipos;
    }

    //------------------------------------------------------------------------------------------------

    public List<Long> getlistaTipos() {
        return listaTipos;
    }

    public void getlistaTipos(List<Long> listaTipos) {
        this.listaTipos = listaTipos;
    }

    //------------------------------------------------------------------------------------------------



}
