package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;
import java.util.List;

public class LogDTO {

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getPedidoCliente() {
        return pedidoCliente;
    }

    public void setPedidoCliente(String pedidoCliente) {
        this.pedidoCliente = pedidoCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Long getTiempoTotal() {
        return tiempoTotal;
    }

    public void setTiempoTotal(Long tiempoTotal) {
        this.tiempoTotal = tiempoTotal;
    }

    public String getTiempoEntrega() {
        return tiempoEntrega;
    }

    public void setTiempoEntrega(String tiempoEntrega) {
        this.tiempoEntrega = tiempoEntrega;
    }

    public List<LogDetailDTO> getLog() {
        return log;
    }

    public void setLog(List<LogDetailDTO> log) {
        this.log = log;
    }

    private String documento;
    private String pedidoCliente;
    private String cliente;
    private String direccion;
    private String telefono;
    private Long tiempoTotal;
    private String tiempoEntrega;
    private List<LogDetailDTO> log;


    @JsonCreator
    public LogDTO(@JsonProperty("documento") String documento,
                  @JsonProperty("pedidoCliente") String pedidoCliente,
                  @JsonProperty("cliente") String cliente,
                  @JsonProperty("direccion") String direccion,
                  @JsonProperty("telefono") String telefono,
                  @JsonProperty("tiempoTotal") Long tiempoTotal,
                  @JsonProperty("tiempoEntrega") String tiempoEntrega,
                  @JsonProperty("log") List<LogDetailDTO> log) {

        this.documento = documento;
        this.pedidoCliente = pedidoCliente;
        this.cliente = cliente;
        this.direccion = direccion;
        this.telefono = telefono;
        this.tiempoTotal = tiempoTotal;
        this.tiempoEntrega = tiempoEntrega;
        this.log = log;
    }



}
