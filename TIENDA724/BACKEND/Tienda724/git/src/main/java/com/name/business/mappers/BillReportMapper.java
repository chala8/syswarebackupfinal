package com.name.business.mappers;

import com.name.business.entities.CategoryName;
import com.name.business.entities.BillReport;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillReportMapper implements ResultSetMapper<BillReport>{

    @Override
    public BillReport map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BillReport(
                resultSet.getDouble("TAX"),
                resultSet.getString("FULLNAME"),
                resultSet.getString("PREFIX_BILL"),
                resultSet.getString("FECHA"),
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("CONSECUTIVE"),
                resultSet.getDouble("VENTA"),
                resultSet.getDouble("COSTO"),
                resultSet.getDouble("UTILIDAD"),
                resultSet.getDouble("PCT_MARGEN_VENTA"),
                resultSet.getString("CAJA"),
                resultSet.getString("NUM_DOCUMENTO"),
                resultSet.getDouble("PCT_MARGEN_COSTO"),
                resultSet.getString("TIENDA"),
                resultSet.getString("CAJERO"),
                resultSet.getLong("ID_BILL_STATE"),
                resultSet.getString("DOMICILIARIO"),
                resultSet.getLong("TIPOFACTURA"),
                resultSet.getLong("ID_MESA")

        );
    }
}