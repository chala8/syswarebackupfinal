package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoriaNodoMapper implements ResultSetMapper<CategoriaNodo>{

    @Override
    public CategoriaNodo map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CategoriaNodo(
                resultSet.getString("LEVEL"),
                resultSet.getString("category"),
                resultSet.getLong("id_category"),
                resultSet.getLong("id_category_father"),
                resultSet.getString("category_father"),
                resultSet.getString("CATEGORY_URL"),
                resultSet.getString("CATEGORY_FATHER_URL")
        );
    }
}