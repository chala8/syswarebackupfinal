package com.name.business.businesses;

import com.name.business.DAOs.InventoryDAO;
import com.name.business.entities.*;
import com.name.business.entities.completes.AttributeComplete;
import com.name.business.entities.completes.CodeComplete;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import org.json.JSONObject;
import org.skife.jdbi.v2.sqlobject.Transaction;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;

import static com.name.business.sanitations.EntitySanitation.inventorySanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

public class InventoryBusiness {
    private InventoryDAO inventoryDAO;
    private CommonBusiness commonBusiness;
    private CommonStateBusiness commonStateBusiness;
    private InventoryDetailBusiness inventoryDetailBusiness;
    private AttributeBusiness attributeBusiness;
    private AttributeDetailListBusiness attributeDetailListBusiness;
    private CategoryBusiness categoryBusiness;


    private CodeBusiness codeBusiness;

    public InventoryBusiness(InventoryDAO inventoryDAO, CommonBusiness commonBusiness, CommonStateBusiness commonStateBusiness, InventoryDetailBusiness inventoryDetailBusiness, AttributeBusiness attributeBusiness, AttributeDetailListBusiness attributeDetailListBusiness, CategoryBusiness categoryBusiness, CodeBusiness codeBusiness) {
        this.inventoryDAO = inventoryDAO;
        this.commonBusiness = commonBusiness;
        this.commonStateBusiness = commonStateBusiness;
        this.inventoryDetailBusiness = inventoryDetailBusiness;
        this.attributeBusiness = attributeBusiness;
        this.attributeDetailListBusiness = attributeDetailListBusiness;
        this.categoryBusiness = categoryBusiness;
        this.codeBusiness = codeBusiness;
    }


    public  Long ponerNegativosEnCero(Long idstore) {
        try {
            inventoryDAO.ponerNegativosEnCero(idstore);
            return new Long (1);
        }catch (Exception e){
            e.printStackTrace();
            return new Long (0);
        }
    }



        public Either<IException,Long> postImage(Base64FileDTO dto) {
            try {

                String Base64File = dto.getFile();
                String extension = dto.getformat();
                String fileName = dto.getFileName();

                // create a buffered image
                BufferedImage image = null;
                byte[] imageByte;

                BASE64Decoder decoder = new BASE64Decoder();
                imageByte = decoder.decodeBuffer(Base64File.split(",")[1]);
                ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
                image = ImageIO.read(bis);
                bis.close();

                // write the image to a file
                File outputfile = new File("/usr/share/nginx/html/imagenes/productos/"+fileName+"."+extension);
                ImageIO.write(image, extension, outputfile);


                //System.out.println("/imagenes/productos/"+fileName+"."+extension+" , "+fileName);
                inventoryDAO.updateImageUrl("https://tienda724.com/imagenes/productos/"+fileName+"."+extension,fileName);


                return Either.right(new Long(1));
            }catch (Exception e){
                e.printStackTrace();
                return Either.left(new TechnicalException(messages_error(e.getMessage())));
            }
        }


        public Either<IException,Long> createInventory(InventoryDTO inventoryDTO){
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_common_state = null;
        Long id_inventory = null;
        //System.out.println(inventoryDAO.getPkLast());
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting creation Inventory  ||||||||||||||||| ");
            if (inventoryDTO!=null){

                if (inventoryDTO.getCommonDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonBusiness.createCommon(inventoryDTO.getCommonDTO());
                    if (commonThirdEither.isRight()) {
                        id_common = commonThirdEither.right().value();
                    }
                }

                if (inventoryDTO.getStateDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(inventoryDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }
                inventoryDAO.create(formatoLongSql(inventoryDTO.getId_third()),formatoLongSql(id_common),formatoLongSql(id_common_state));
                id_inventory = inventoryDAO.getPkLast();

                if (inventoryDTO.getDetailDTOS() != null && inventoryDTO.getDetailDTOS().size()>0 && id_inventory>0) {
                    Either<IException, Long> commonThirdEither = inventoryDetailBusiness.createInventoryDetail(id_inventory,inventoryDTO.getDetailDTOS());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }

                return Either.right(id_inventory);
            }else{
                //msn.add("It does not recognize Inventory, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_inventory
     * @param inventoryDTO
     * @return
     */
    public Either<IException, Long> updateInventary(Long id_inventory, InventoryDTO inventoryDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        Inventory actualInventary=null;

        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting update Inventory ||||||||||||||||| ");
            if ((id_inventory != null && id_inventory > 0)) {

                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorID(id_inventory).equals(false)){
                    //msn.add("It ID Inventory does not exist register on  Inventory Value, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                List<Inventory> read = inventoryDAO.read(new Inventory(id_inventory,null,
                        new Common(null,null,null),
                        new CommonState(null,null,null,null)));

                if (read.size()<=0){
                    //msn.add("It does not recognize ID Inventory, probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = inventoryDAO.readCommons(formatoLongSql(id_inventory));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }


                // Common basic info update
                commonBusiness.updateCommon(idCommons.getId_common(), inventoryDTO.getCommonDTO());

                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), inventoryDTO.getStateDTO());

                actualInventary = read.get(0);
                // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                if (inventoryDTO.getId_third() == null || inventoryDTO.getId_third()<1) {
                    inventoryDTO.setId_third(actualInventary.getId_third());
                } else {
                    inventoryDTO.setId_third(formatoLongSql(inventoryDTO.getId_third()));
                }
                // Inventory update
                inventoryDAO.update(id_inventory,inventoryDTO.getId_third());

                return Either.right(id_inventory);

            } else {
                //msn.add("It does not recognize ID Inventory, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     *
     * @param inventory
     *
     * @return
     */
    public Either<IException, List<Inventory>> getInventories(Inventory inventory) {
        List<String> msn = new ArrayList<>();
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting consults Inventories  ||||||||||||||||| ");
            Inventory inventorySanitation = inventorySanitation(inventory);
            return Either.right(inventoryDAO.read(inventorySanitation));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_inventory
     * @return
     */
    public Either<IException, Long> deleteInventory(Long id_inventory) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            //msn.add("OK");
            //System.out.println("|||||||||||| Starting delete Inventory ||||||||||||||||| ");
            if (id_inventory != null && id_inventory > 0) {
                List<CommonSimple> readCommons = inventoryDAO.readCommons(formatoLongSql(id_inventory));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                Either<IException, Long> deleteCommonStateEither = commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common

                // TODO Now delete all your inventory detail
                Either<IException, Long> deleteInventoryDetail=inventoryDetailBusiness.deleteInventoryDetail(null,id_inventory);

                return Either.right(id_inventory);
            } else {
                //msn.add("It does not recognize ID Inventory, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = inventoryDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }

    public Either<IException, String> getImgUrl(
                                                     String barcode) {
        String dataResponse = inventoryDAO.getImgUrl(barcode);

        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    @Transaction
    public Either<IException, String> discountInventary(Long id_inventory, List<InventoryQuantityDTO> inventoryQuantityDTOList) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        List<InventoryDetailDTO> inventoryDetailDTOList= new ArrayList<>();
        try {
            if (!validatorID(id_inventory)){
                return Either.left(new BussinessException("It ID Inventory does not exist register on Inventory, probably it has bad"));
            }
            if (id_inventory!=null && id_inventory>0){
                if (inventoryQuantityDTOList!=null && inventoryQuantityDTOList.size()>0){
                    Either<IException, List<InventoryDetailSimple>> loadingDeatilInventoryEither = loadingDetailInventory(id_inventory, inventoryQuantityDTOList,IS_DISCOUNT);
                    if (loadingDeatilInventoryEither.isRight()){
                        List<InventoryDetailSimple> inventoryDetailSimples = loadingDeatilInventoryEither.right().value();
                        for (InventoryDetailSimple  detailSimple:inventoryDetailSimples){

                            inventoryDetailDTOList.add(new InventoryDetailDTO(
                                    detailSimple.getId_inventory_detail(),detailSimple.getId_product_third(),
                                    detailSimple.getQuantity(),detailSimple.getStock_min(),detailSimple.getCode(),
                                    new CommonStateDTO(detailSimple.getState_inv_detail(),detailSimple.getCreation_inv_detail(),detailSimple.getModify_inv_detail())
                                    ));
                        }

                        if (inventoryDetailDTOList.size()>0){
                            //System.out.println("------------ INICIA PROCESO DE DESCARGA DEL INVENARIO -------------");
                            Either<IException, Long> updateInventaryDetailEither = inventoryDetailBusiness.updateInventaryDetail(id_inventory, inventoryDetailDTOList);
                            if (updateInventaryDetailEither.isRight()){

                                return Either.right("Inventory download was successful");

                            }else{
                                return Either.left(updateInventaryDetailEither.left().value());
                            }

                        }else{

                            //msn.add("Inventory download was interrupted");
                            return Either.left(new BussinessException(messages_errors(msn)));
                        }

                    }else{
                        return Either.left(loadingDeatilInventoryEither.left().value());
                    }

                }else{
                    //msn.add("The Product Third is empty");
                    return Either.left(new BussinessException(messages_errors(msn)));

                }
            }else{
                //msn.add("The ID Inventory  is necessary ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<InventoryDetailSimple>> loadingDetailInventory(Long id_inventory, List<InventoryQuantityDTO> inventoryQuantityDTOList, Boolean is_discount) {
        List<String> msn = new ArrayList<>();
        List<InventoryDetailSimple> inventoryDetailList= new ArrayList<>();
        try {
            if (!validatorID(id_inventory)){
                return Either.left(new BussinessException("It ID Inventory does not exist register on Inventory, probably it has bad"));
            }
            if (inventoryQuantityDTOList!=null && inventoryQuantityDTOList.size()>0){
                for (InventoryQuantityDTO quantityDTO:inventoryQuantityDTOList){
                    Either<IException, List<InventoryDetailSimple>> inventoryDetailSimpleEither = inventoryDetailBusiness.getInventoryDetailSimple(
                            new InventoryDetailSimple(
                                    quantityDTO.getId_inventory_detail(),id_inventory,quantityDTO.getId_product_third(),null,null,null,
                                    new CommonState(null,null,null,null)
                                    )
                    );

                    if (inventoryDetailSimpleEither.isRight() && inventoryDetailSimpleEither.right().value().size()>0){
                        List<InventoryDetailSimple> detailSimples = inventoryDetailSimpleEither.right().value();
                        if (detailSimples.size()==1){

                            InventoryDetailSimple inventoryDetailSimple = detailSimples.get(0);

                            if (is_discount){ // It if is for do a discount

                                if ( inventoryDetailSimple.getQuantity()!=null && inventoryDetailSimple.getQuantity()>0 && (inventoryDetailSimple.getQuantity()-quantityDTO.getQuantity())>=0){
                                    inventoryDetailSimple.setQuantity(inventoryDetailSimple.getQuantity()-quantityDTO.getQuantity());
                                    inventoryDetailList.add(inventoryDetailSimple);

                                }else{
                                    //msn.add("The Product Third "+quantityDTO.getId_product_third()+" with detail inventory "+
                                           // inventoryDetailSimple.getId_inventory_detail()+" does not have enough units");
                                    return Either.left(new BussinessException(messages_errors(msn)));
                                }
                            }else{
                                if ( inventoryDetailSimple.getQuantity()!=null && inventoryDetailSimple.getQuantity()>0){
                                    inventoryDetailSimple.setQuantity(inventoryDetailSimple.getQuantity()+quantityDTO.getQuantity());
                                    inventoryDetailList.add(inventoryDetailSimple);

                                }else{
                                    inventoryDetailSimple.setQuantity(quantityDTO.getQuantity());
                                    inventoryDetailList.add(inventoryDetailSimple);
                                }

                            }



                        }else {
                            //msn.add("The Product Third "+quantityDTO.getId_product_third()+" has more than one match");
                            return Either.left(new BussinessException(messages_errors(msn)));
                        }

                        inventoryDetailList.addAll(inventoryDetailSimpleEither.right().value());

                    }else{
                        //msn.add("The Product Third "+quantityDTO.getId_product_third()+" doesn't exist on inventory");
                        return Either.left(new BussinessException(messages_errors(msn)));
                    }
                }
                return Either.right(inventoryDetailList);

            }else {
                //msn.add("The Product Third is empty");
                return Either.left(new BussinessException(messages_errors(msn)));

            }

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, String> plusInventory(Long id_inventory, List<InventoryQuantityDTO> inventoryQuantityDTOList) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        List<InventoryDetailDTO> inventoryDetailDTOList= new ArrayList<>();
        try {
            if (id_inventory!=null && id_inventory>0){
                if (inventoryQuantityDTOList!=null && inventoryQuantityDTOList.size()>0){
                    Either<IException, List<InventoryDetailSimple>> loadingDeatilInventoryEither = loadingDetailInventory(id_inventory, inventoryQuantityDTOList,!IS_DISCOUNT);
                    if (loadingDeatilInventoryEither.isRight()){
                        List<InventoryDetailSimple> inventoryDetailSimples = loadingDeatilInventoryEither.right().value();
                        for (InventoryDetailSimple  detailSimple:inventoryDetailSimples){

                            inventoryDetailDTOList.add(new InventoryDetailDTO(
                                    detailSimple.getId_inventory_detail(),detailSimple.getId_product_third(),
                                    detailSimple.getQuantity(),detailSimple.getStock_min(),detailSimple.getCode(),
                                    new CommonStateDTO(detailSimple.getState_inv_detail(),detailSimple.getCreation_inv_detail(),detailSimple.getModify_inv_detail())
                            ));
                        }

                        if (inventoryDetailDTOList.size()>0){
                            //System.out.println("------------ INICIA PROCESO DE CARGA DEL INVENARIO -------------");
                            Either<IException, Long> updateInventaryDetailEither = inventoryDetailBusiness.updateInventaryDetail(id_inventory, inventoryDetailDTOList);
                            if (updateInventaryDetailEither.isRight()){

                                return Either.right("Inventory load was successful");

                            }else{
                                return Either.left(updateInventaryDetailEither.left().value());
                            }

                        }else{

                            //msn.add("Inventory load was interrupted");
                            return Either.left(new BussinessException(messages_errors(msn)));
                        }

                    }else{
                        return Either.left(loadingDeatilInventoryEither.left().value());
                    }

                }else{
                    //msn.add("The Product Third is empty");
                    return Either.left(new BussinessException(messages_errors(msn)));

                }
            }else{
                //msn.add("The ID Inventory  is necessary ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }

        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, HashMap<String,Object>> getInventoryParameters(Long id_inventory, Long id_third) {
        HashMap<String,Object> response= new HashMap<>();
        List<AttributeComplete> attributeCompleteListResponse= new ArrayList<>();
        List<MeasureUnit> measureUnitsResponse= new ArrayList<>();
        List<Category> productorsResponse= new ArrayList<>();
        List<TreeNode> categoriesResponse= new ArrayList<>();
        List<TreeNode> myCategoriesResponse= new ArrayList<>();
        // Auxiliaries
        HashMap<Long,AttributeValue> attributeValueHashMap= new HashMap<>();
        HashMap<Long,List<AttributeValue>> attributComleteMap= new HashMap<>();
        HashMap<Long,Category> productorsHashMap= new HashMap<>();

        List<Attribute> attributeList= new ArrayList<>();
        List<InventoryDetail> getDetails= new ArrayList<>();
        List<TreeNode> treeNodeList= new ArrayList<>();

        // measure units

        HashMap<Long,MeasureUnit> measureUnitHashMap= new HashMap<>();
        try {
            //
            if (formatoLongSql(id_inventory)==null || formatoLongSql(id_inventory)<0) {
                List<Inventory> inventoryList = inventoryDAO.read(new Inventory(formatoLongSql(id_inventory), formatoLongSql(id_third), null, null));

                for (Inventory inventory: inventoryList){
                    List<InventoryDetail> details = getDetails(inventory.getId_inventory());

                    if(details!=null&&details.size()>0)
                        getDetails.addAll(details);

                }
            }else{
                getDetails=getDetails(id_inventory);
            }
            if (getDetails.size()>0){
                attributeValueHashMap =builtAttributeValue(getDetails);
                measureUnitsResponse=builtMeasureUnit(getDetails);
                treeNodeList = builtProductors(getDetails);

                myCategoriesResponse=builtMyCategories(getDetails);

            }
            Set<Map.Entry<Long,AttributeValue>> set= attributeValueHashMap.entrySet();
            for (Map.Entry<Long,AttributeValue> entry: set){
                List<AttributeValue> attributeValueListofMap = attributComleteMap.get(entry.getValue().getId_attribute());
                if(attributeValueListofMap!=null){
                    attributeValueListofMap.add(entry.getValue());
                }else{
                    attributeValueListofMap=new ArrayList<>();
                    attributeValueListofMap.add(entry.getValue());
                }
                attributComleteMap.put(entry.getValue().getId_attribute(),attributeValueListofMap);

            }

            Set<Map.Entry<Long,List<AttributeValue>>> setattributComleteMap= attributComleteMap.entrySet();
            for (Map.Entry<Long,List<AttributeValue>> entry: setattributComleteMap){
                entry.getKey();
                Either<IException, Object> attributesEither = attributeBusiness.getAttributes(
                        new Attribute(entry.getKey(), null, null), false
                );
                if (attributesEither.isRight()){

                    attributeList= (List<Attribute>) attributesEither.right().value();
                    if (attributeList.size()>0){
                        attributeList.get(0);
                        attributeCompleteListResponse.add(
                                new AttributeComplete(
                                        attributeList.get(0),entry.getValue()
                                )
                        );


                    }
                }
            }


            for (TreeNode treeNode: treeNodeList){
                Category category=((Category)treeNode.getData());
                productorsHashMap.put(category.getId_category(),category);
                categoriesResponse.add(treeNode.getParent());

            }



            Set<Map.Entry<Long,Category>> entrySet= productorsHashMap.entrySet();
            for (Map.Entry<Long,Category> unitEntry:entrySet){
                productorsResponse.add(unitEntry.getValue());

            }




            if (measureUnitsResponse!=null && measureUnitsResponse.size()>0){
                response.put("units",measureUnitsResponse);
            }
            if (attributeCompleteListResponse!=null && attributeCompleteListResponse.size()>0){
                response.put("attributes",attributeCompleteListResponse);
            }
            if (productorsResponse!=null && productorsResponse.size()>0){
                response.put("productors",productorsResponse);
            }
            if (categoriesResponse!=null && categoriesResponse.size()>0){
                response.put("categories",categoriesResponse);
            }
            if (myCategoriesResponse!=null && myCategoriesResponse.size()>0){
                response.put("my_categories",myCategoriesResponse);
            }

            return Either.right(response);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    private List<TreeNode> builtProductors(List<InventoryDetail> getDetails) {
        List<TreeNode> treeNodeList= new ArrayList<>();
        Either<IException, List<TreeNode>> categoriesTreeEither=null;

        try {
            HashMap<Long,Long> id_categoriesHashMap= new HashMap<>();
            for (InventoryDetail detail: getDetails){
                Long id_category = detail.getProduct().getId_category();
                id_categoriesHashMap.put(id_category,id_category);
            }
            Set<Map.Entry<Long,Long>> entries= id_categoriesHashMap.entrySet();
            for (Map.Entry<Long,Long> entry:entries){
                categoriesTreeEither = categoryBusiness.getCategoriesTree(
                        new Category(
                                entry.getKey(), null, null, null, null,
                                null
                        ), true, true, true, true
                );

                if(categoriesTreeEither.isRight()&& categoriesTreeEither.right().value().size()>0){
                    treeNodeList.addAll(categoriesTreeEither.right().value());
                }

            }
            return treeNodeList;

        }catch (Exception e){
            return null;
        }

    }

    private List<TreeNode> builtMyCategories(List<InventoryDetail> getDetails) {

        List<TreeNode> treeNodeList= new ArrayList<>();
        Either<IException, List<TreeNode>> categoriesTreeEither=null;
        try {
            HashMap<Long,Long> id_categoriesHashMap= new HashMap<>();
            for (InventoryDetail detail: getDetails){
                Long id_category = detail.getDescription().getId_category_third();;
                id_categoriesHashMap.put(id_category,id_category);
            }
            Set<Map.Entry<Long,Long>> entries= id_categoriesHashMap.entrySet();
            for (Map.Entry<Long,Long> entry:entries){
                categoriesTreeEither = categoryBusiness.getCategoriesTree(
                        new Category(
                                null, null, null, null, entry.getKey(),
                                null
                        ), true, true, true, true
                );

                if(categoriesTreeEither.isRight()&& categoriesTreeEither.right().value().size()>0){
                    treeNodeList.addAll(categoriesTreeEither.right().value());
                }
            }

            return treeNodeList;

        }catch (Exception e){
            return null;
        }

    }

    private List<MeasureUnit>  builtMeasureUnit(List<InventoryDetail> getDetails) {
        HashMap<Long,MeasureUnit> measureUnitHashMap= new HashMap<>();
        List<MeasureUnit> measureUnits= new ArrayList<>();

        try {
            for (InventoryDetail detail: getDetails){
                measureUnitHashMap.put(detail.getDescription().getMeasure_unit().getId_measure_unit(),detail.getDescription().getMeasure_unit());
            }
            Set<Map.Entry<Long,MeasureUnit>> entrySet= measureUnitHashMap.entrySet();
            for (Map.Entry<Long,MeasureUnit> unitEntry:entrySet){
                measureUnits.add(unitEntry.getValue());

            }
            return measureUnits;
        }catch (Exception e){
            return null;
        }
    }

    private HashMap<Long,AttributeValue> builtAttributeValue(List<InventoryDetail> getDetails) {
        List<AttributeValue> attributeValueList= new ArrayList<>();
        List<AttributeDetailList> attributeDetailLists= new ArrayList<>();
        HashMap<Long,AttributeValue> attributeValueHashMap= new HashMap<>();
        try {
            for (InventoryDetail detail: getDetails){

                Either<IException, List<AttributeDetailList>> attributeDetailListsEither = attributeDetailListBusiness.getAttributeDetailLists(
                        new AttributeDetailList(null, detail.getDescription().getId_attribute_list(), null, null)
                );
                if (attributeDetailListsEither.isRight()&& attributeDetailListsEither.right().value().size()>0){
                    attributeDetailLists.addAll(attributeDetailListsEither.right().value());



                }
            }

            for (AttributeDetailList detailList: attributeDetailLists){
                attributeValueList.add(detailList.getValue());
            }

            // begin Sanitization of Value's List
            for (AttributeValue value:attributeValueList){
                attributeValueHashMap.put(value.getId_attribute_value(),value);
            }
            // clear list
            attributeValueList.clear();
            Set<Map.Entry<Long,AttributeValue>> set= attributeValueHashMap.entrySet();
            for (Map.Entry<Long,AttributeValue> entry: set){
                attributeValueList.add(entry.getValue());
            }


            return attributeValueHashMap;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }


    }

    private List<InventoryDetail> getDetails(Long id_inventory) {

        if (formatoLongSql(id_inventory)!=null || formatoLongSql(id_inventory)>0) {
            Either<IException, List<InventoryDetail>> inventoryDetailsEither = inventoryDetailBusiness.getInventoryDetails(
                    new InventoryDetail(
                            new InventoryDetailSimple(null, formatoLongSql(id_inventory), null, null, null, null,
                                    new CommonState(null, 1, null, null)
                            ),
                            null, null
                    ), null
            );
            if (inventoryDetailsEither.isRight()){
                return inventoryDetailsEither.right().value();
            }else{
                return null;
            }

        }
        return null;
    }

    public void print(Integer i,String name,String description) {
        //System.out.println("PORCESS DATA CASE  # "+i+"\n"+
                //"NAME: -> \n\t"+name+"-  \n"+
                //"DESCRIPTION -> \n\t "+description+"\n");
    }



    /***
     * Proccess Data
     *  {
     *        String name_th;
              Long id_document_type_th;
              String number_document_type_th;
             // card 2
             String name_th_venue;
             Long id_document_type_th_venue;

             String number_document_type_th_venue;
             Long id_venue;

             // card 3
             Long id_productor;
             Long id_cat_productor;

             // card 4
             Long id_category_th;
             // card 5
             Long id_measure_unit;
             // card 6
             List<AttributeValueFilter> attributeValues;
     *  }
     * @param id_inventory
     * @param id_third
     * @param inventoryFilter
     * @return
     */

    public Either<IException,List<InventoryDetail>> getAttributesFilters(Long id_inventory, Long id_third, InventoryFilter inventoryFilter) {
        // RESPONSE
         List<InventoryDetail> inventoryDetailListResponse= new ArrayList<>();
         Either<IException, List<InventoryDetail>> getInventoryDetailsEitherResponse= null;

         // Temp
        List<InventoryDetail> inventoryDetailList= new ArrayList<>();
        HashMap<Long,List<InventoryDetail> > longinventoryDetailListHashMap=new HashMap<>();
        // Map unique detail inventory
        HashMap<Long,InventoryDetail> longinventoryDetailHashMap=new HashMap<>();
        //By Function
        HashMap<Long,InventoryDetail> longinventoryDetailHashMapTemp=new HashMap<>();
        int i=0;
        try {

            longinventoryDetailHashMapTemp=beginDetailInv(formatoLongSql(id_inventory),formatoLongSql(id_third),longinventoryDetailHashMapTemp);


            if ((formatoLongSql(inventoryFilter.getId_third())!=null && formatoLongSql(inventoryFilter.getId_third()) >0) ||
                    (formatoStringSql(inventoryFilter.getNumber_document_type_th())!=null && !formatoStringSql(inventoryFilter.getNumber_document_type_th()).isEmpty())){// case 1

                if((formatoLongSql(inventoryFilter.getId_third())!=null && formatoLongSql(inventoryFilter.getId_third()) >0)){
                    print(1,"SEARCH THIRD","Buscar primero el tercero, para obtener el ID\n Y despues el inventario");
                    longinventoryDetailHashMapTemp=filterTHIRD(inventoryFilter.getId_third(),longinventoryDetailHashMapTemp);


                }else{
                    print(1,"SEARCH THIRD BY DOC","Buscar primero el tercero, para obtener el ID\n Y despues el inventario");

                    longinventoryDetailHashMapTemp=filterID_DOC_THIRD(inventoryFilter.getNumber_document_type_th(),longinventoryDetailHashMapTemp);

                }


            }
            if ((formatoLongSql(inventoryFilter.getId_venue())!=null && formatoLongSql(inventoryFilter.getId_venue()) >0) ||
                    (formatoStringSql(inventoryFilter.getNumber_document_type_th_venue())!=null && !formatoStringSql(inventoryFilter.getNumber_document_type_th_venue()).isEmpty())){// case 2

                if((formatoLongSql(inventoryFilter.getId_venue())!=null && formatoLongSql(inventoryFilter.getId_venue()) >0)){
                    print(2,"SEARCH THIRD VENUE","Buscar  por Sedes de un TERCERO, para obtneer el ID,\n y Luego sus inventarios");
                    longinventoryDetailHashMapTemp=filterVENUE(inventoryFilter.getId_venue(),longinventoryDetailHashMapTemp);
                }else{
                    print(2,"SEARCH THIRD VENUE BY DOC","Buscar  por Sedes de un TERCERO, para obtener el ID,\n y Luego sus inventarios");
                    longinventoryDetailHashMapTemp=filterID_DOC_VENUE(inventoryFilter.getNumber_document_type_th_venue(),longinventoryDetailHashMapTemp);
                }

            }
            if ((formatoLongSql(inventoryFilter.getId_productor())!=null && formatoLongSql(inventoryFilter.getId_productor()) >0) ||
                    (formatoLongSql(inventoryFilter.getId_cat_productor())!=null && formatoLongSql(inventoryFilter.getId_cat_productor()) >0)){// case 3 and 4

                if(( !(formatoLongSql(inventoryFilter.getId_cat_productor())!=null && formatoLongSql(inventoryFilter.getId_cat_productor()) >0) &&
                        formatoLongSql(inventoryFilter.getId_productor())!=null && formatoLongSql(inventoryFilter.getId_productor()) >0)){
                    print(3,"SEARCH BY PRODUCTOR","Buscar  invenarios que tengan relacionados productos del productor, \n No puede buscar por categorias de tercero.");
                    longinventoryDetailHashMapTemp=filterPRODUCTOR(inventoryFilter.getId_productor(),longinventoryDetailHashMapTemp);
                }else{
                    print(4,"SEARCH BY CATEORY","Buscar  invenarios que tengan relacionados a una categoria de productor");
                    longinventoryDetailHashMapTemp=filterCategoryPRODUCTOR(inventoryFilter.getId_cat_productor(),longinventoryDetailHashMapTemp);


                }


            }
            if (formatoLongSql(inventoryFilter.getId_category_th())!=null && formatoLongSql(inventoryFilter.getId_category_th()) >0){// case 5
                print(5,"BY MY CATEGORY"," NO DEBE BUCAR POR PRODUCTOR, NI SUS CATEGORIAS, ES MUTUAMENTE EXCLUYENTE ESTE FILTRO\n" +
                        "Buscar  invenarios que tengan relacionados a una categoria de TERCERO");
                longinventoryDetailHashMapTemp=filterCategoryThird(inventoryFilter.getId_category_th(),longinventoryDetailHashMapTemp);
            }

            if (formatoLongSql(inventoryFilter.getId_measure_unit())!=null && formatoLongSql(inventoryFilter.getId_measure_unit()) >0 ){// case 6
                print(6,"BY  id_measure_unit "," Busca los registros que tengan esta unidad de medida");
                longinventoryDetailHashMapTemp=filterMeasureUnit(id_inventory,inventoryFilter.getId_measure_unit(),longinventoryDetailHashMapTemp);
            }

            if (inventoryFilter.getAttributeValues()!=null&&inventoryFilter.getAttributeValues().size()>0 ){// case 5
                print(7,"BY  Valores "," Busca los registros que tengan N valores de Atributo");
                longinventoryDetailHashMapTemp=filterValueList(id_inventory,inventoryFilter.getAttributeValues(),longinventoryDetailHashMapTemp);
            }

            if (!longinventoryDetailHashMapTemp.isEmpty()){
                print(8,"FIN 1"," FIN 1");
                Set<Map.Entry<Long,InventoryDetail>> entrySet= longinventoryDetailHashMapTemp.entrySet();

                for (Map.Entry<Long,InventoryDetail> entry:entrySet){
                    inventoryDetailList.add(entry.getValue());
                }
                return Either.right(inventoryDetailList);
            }
            print(8,"FIN "," FIN");

            return Either.right(inventoryDetailList);


            // Response temp
        /*    return
                    inventoryDetailBusiness.getInventoryDetails(
                            new InventoryDetail(
                                    new InventoryDetailSimple(null, formatoLongSql(id_inventory), null, null, null, null,
                                            new CommonState(null, 1, null, null)
                                    ),
                                    null, null
                            ), null
                    );*/

        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public  List<InventoryDetail> inventoryDetailsEither(Long id_inventory){
        List<InventoryDetail> detailList= new ArrayList<>();
        try {

            Either<IException, List<InventoryDetail>> inventoryDetailsEither = inventoryDetailBusiness.getInventoryDetails(
                    new InventoryDetail(
                            new InventoryDetailSimple(null, formatoLongSql(id_inventory), null, null, null, null,
                                    new CommonState(null, 1, null, null)
                            ),
                            null, new ProductThirdSimple(null,null, null, null, null, null, null,
                            null, new CommonState(null, 1, null, null) ,
                            new MeasureUnit(null,null,
                                    new Common(null,null,null),
                                    new CommonState(null, 1, null, null) ),null,null)
                    ), new Code(null, null, null, null, null, null, null, null, null)
            );


            if (inventoryDetailsEither.isRight()&&inventoryDetailsEither.right().value().size()>0){
                return  inventoryDetailsEither.right().value();
            }
            return detailList;

        }catch (Exception e){
            return detailList;
        }
    }

    private HashMap<Long, InventoryDetail> beginDetailInv(Long id_inventory, Long id_third, HashMap<Long, InventoryDetail> longinventoryDetailHashMapTemp) {
        List<InventoryDetail> detailList= new ArrayList<>();
        try{


                    Either<IException, List<Inventory>> inventoriesEither = getInventories(
                            new Inventory(formatoLongSql(id_inventory), formatoLongSql(id_third), null, null)
                    );

                    if (inventoriesEither.isRight()&&inventoriesEither.right().value().size()>0){
                        List<Inventory> inventories = inventoriesEither.right().value();
                        if (inventories.size()>0);
                        for (Inventory inventory:inventories){

                            List<InventoryDetail> inventoryDetailList = inventoryDetailsEither(inventory.getId_inventory());
                            if (inventoryDetailList!=null&& inventoryDetailList.size()>0){
                                detailList.addAll(inventoryDetailList);
                            }
                        }
                    }




            if (detailList.size()>0){
                for (InventoryDetail detail: detailList){

                    longinventoryDetailHashMapTemp.put(detail.getDetail().getId_inventory_detail(),detail);
                }
            }




            return longinventoryDetailHashMapTemp;
        }catch (Exception e){
            e.printStackTrace();
            return longinventoryDetailHashMapTemp;
        }



    };

    private HashMap<Long, InventoryDetail> filterTHIRD(Long id__third, HashMap<Long, InventoryDetail> longinventoryDetailHashMapTemp) {
        return beginDetailInv(null,id__third,new HashMap<>());

    }

    /**
     * @apiNote
     * Este metodo no soportado por esta API
     * @param id__third
     * @param longinventoryDetailHashMapTemp
     * @return
     */
    @Deprecated
    private HashMap<Long, InventoryDetail> filterID_DOC_THIRD(String id__third, HashMap<Long, InventoryDetail> longinventoryDetailHashMapTemp) {
       return longinventoryDetailHashMapTemp;

    }

    private HashMap<Long, InventoryDetail> filterVENUE(Long id__third_venue, HashMap<Long, InventoryDetail> longinventoryDetailHashMapTemp) {
        return beginDetailInv(null,id__third_venue,new HashMap<>());
    }


    /**
     * @apiNote
     * Este metodo no soportado por esta API
     * @param id_measure_unit
     * @param longinventoryDetailHashMapTemp
     * @return
     */
    @Deprecated
    private HashMap<Long, InventoryDetail> filterID_DOC_VENUE(String id_measure_unit, HashMap<Long, InventoryDetail> longinventoryDetailHashMapTemp) {
        return longinventoryDetailHashMapTemp;
    }

    private HashMap<Long, InventoryDetail> filterValueList(Long id_inventory,List<AttributeValueFilter> attributeValueFilters, HashMap<Long, InventoryDetail> longinventoryDetailHashMapTemp) {
        HashMap<Long, InventoryDetail> longinventoryAux= new HashMap<>();
        try {
            HashMap<Long, Long> id_codesMap= new HashMap<>();
            HashMap<Long, Object> values= new HashMap<>();


            Either<IException, List<CodeComplete>> codesCompleteEither = codeBusiness.getCodesComplete(
                   null,
                    false, true);

            if (codesCompleteEither.isRight()&&codesCompleteEither.right().value().size()>0){
                List<CodeComplete> valueCodeCompleteList = codesCompleteEither.right().value();

                for (CodeComplete codeComplete:valueCodeCompleteList){
                    ////System.out.println("\n\n\t CODE ->"+JSONObject.wrap(codeComplete));
                    if (codeComplete.getAttribute_list() instanceof java.util.List ){

                        List<AttributeDetailList> detailListListGeneral=(List<AttributeDetailList>)codeComplete.getAttribute_list();

                        if (detailListListGeneral!=null && detailListListGeneral.size()>0){



                            for (AttributeDetailList detailList:detailListListGeneral){

                                Long id_attribute_value = detailList.getValue().getId_attribute_value();



                                for (AttributeValueFilter valueFilter: attributeValueFilters){


                                    if (id_attribute_value==valueFilter.getId_attribute_value()){

                                        id_codesMap.put(codeComplete.getId_code(),codeComplete.getId_code());
                                    }
                                }
                            }
                        }
                    }
                }
            }


            Set<Map.Entry<Long,Long>> entrySet= id_codesMap.entrySet();
            for (Map.Entry<Long,Long> entry:entrySet){

                Either<IException, List<InventoryDetail>> inventoryDetailsEither = inventoryDetailBusiness.getInventoryDetails(
                        new InventoryDetail(
                                new InventoryDetailSimple(null, formatoLongSql(id_inventory), null, null, null, null,
                                        new CommonState(null, 1, null, null)
                                ),
                                null, new ProductThirdSimple(null,null, null, null, null, null, null,
                                null, new CommonState(null, 1, null, null) ,
                                new MeasureUnit(null,null,
                                        new Common(null,null,null),
                                        new CommonState(null, 1, null, null) ),null,null)
                        ), new Code(entry.getValue(), null, null, null, null, null, null, null, null)
                );
                if (inventoryDetailsEither.isRight() && inventoryDetailsEither.right().value().size()>0){
                    List<InventoryDetail> details = inventoryDetailsEither.right().value();

                    for (InventoryDetail detail: details){
                        if (longinventoryDetailHashMapTemp.containsKey(detail.getDetail().getId_inventory_detail())){
                            longinventoryAux.put(detail.getDetail().getId_inventory_detail(),detail);
                        }


                    }
                }


            }



            return longinventoryAux;

        }catch (Exception e){
            e.printStackTrace();
            return longinventoryAux;
        }

    }

    private HashMap<Long, InventoryDetail> filterCategoryThird(Long id_category_th, HashMap<Long, InventoryDetail> longinventoryDetailHashMapTemp) {
        HashMap<Long, InventoryDetail> longinventoryAux= new HashMap<>();
        try {
            if (!longinventoryDetailHashMapTemp.isEmpty()) {
                Set<Map.Entry<Long, InventoryDetail>> entrySet = longinventoryDetailHashMapTemp.entrySet();
                for (Map.Entry<Long, InventoryDetail> entry : entrySet) {
                    if (entry.getValue().getDescription().getId_category_third() == formatoLongSql(id_category_th)) {
                        longinventoryAux.put(entry.getKey(), entry.getValue());
                    }

                }
            }
            return longinventoryAux;
        }catch (Exception e){
            return longinventoryDetailHashMapTemp;
        }

    }

    private HashMap<Long, InventoryDetail> filterMeasureUnit(Long id_inventory, Long id_measure_unit, HashMap<Long, InventoryDetail> longinventoryDetailHashMapTemp) {
        HashMap<Long, InventoryDetail> longinventoryAux= new HashMap<>();

        try {


            Either<IException, List<InventoryDetail>> inventoryDetailsEither = inventoryDetailBusiness.getInventoryDetails(
                    new InventoryDetail(
                            new InventoryDetailSimple(null, formatoLongSql(id_inventory), null, null, null, null,
                                    new CommonState(null, 1, null, null)
                            ),
                            null, new ProductThirdSimple(null,null, null, null, null, null, null,
                            null, new CommonState(null, 1, null, null) ,
                            new MeasureUnit(id_measure_unit,null,
                                    new Common(null,null,null),
                                    new CommonState(null, 1, null, null) ),null,null)
                    ), new Code(null, null, null, null, null, null, null, null, null)
            );

            if (inventoryDetailsEither.isRight()&&inventoryDetailsEither.right().value().size()>0){
                List<InventoryDetail> detailList = inventoryDetailsEither.right().value();

                for (InventoryDetail detail: detailList){

                    //System.out.println(JSONObject.wrap(detailList));
                    if (longinventoryDetailHashMapTemp.containsKey(detail.getDetail().getId_inventory_detail())){

                    }

                    longinventoryAux.put(detail.getDetail().getId_inventory_detail(),detail);
                }

            }

            return longinventoryAux;

        }catch (Exception e){
            e.printStackTrace();
            return longinventoryAux;

        }
    }

    private HashMap<Long, InventoryDetail> filterCategoryPRODUCTOR(Long id_category_pro, HashMap<Long, InventoryDetail> longinventoryDetailHashMapTemp) {
        HashMap<Long, InventoryDetail> longinventoryAux= new HashMap<>();
        try {
            if (!longinventoryDetailHashMapTemp.isEmpty()) {
                Set<Map.Entry<Long, InventoryDetail>> entrySet = longinventoryDetailHashMapTemp.entrySet();
                for (Map.Entry<Long, InventoryDetail> entry : entrySet) {
                    if (entry.getValue().getProduct().getId_category() == formatoLongSql(id_category_pro)) {
                        longinventoryAux.put(entry.getKey(), entry.getValue());
                    }

                }
            }
            return longinventoryAux;
          }catch (Exception e){
            return longinventoryDetailHashMapTemp;
        }
    }

    private HashMap<Long, InventoryDetail> filterPRODUCTOR(Long id_productor, HashMap<Long, InventoryDetail> longinventoryDetailHashMapTemp) {
        //Either<IException, List<TreeNode>> categoriesTree = categoryBusiness.getCategoriesTree(new Category(id_productor, null, null, null, null, null), false, false, false, false);
        return longinventoryDetailHashMapTemp;
    }
}
