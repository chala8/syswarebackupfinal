package com.name.business.mappers;


import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Price2Mapper implements ResultSetMapper<Price2> {
    @Override
    public Price2 map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Price2(
                resultSet.getLong("id_product_store"),
                resultSet.getString("price_description"),
                resultSet.getDouble("price"),
                resultSet.getDouble("pct_descuento")
        );
    }
}
