package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.*;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlCall;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;



public interface ProductNameDAO {


    @RegisterMapper(CodeNameMapper.class)
    @SqlQuery("select p.id_third,p.id_product,c.name PRODUCT ,c.description\n" +
            " from tienda724.product p,tienda724.common c\n" +
            " where p.id_common=c.id_common and p.id_category=:id_category and p.id_third is null")
    List<ProductName> getProductsByCategory(@Bind("id_category") Long id_category);


    @RegisterMapper(CodeNameMapper.class)
    @SqlQuery("select p.id_third, p.id_product,c.name PRODUCT,c.description\n" +
            " from tienda724.product p,tienda724.common c\n" +
            " where p.id_common=c.id_common and p.id_category=:id_category and p.id_third=:id_third order by 2")
    List<ProductName> getProductsByCategoryAndThird(@Bind("id_category") Long id_category,@Bind("id_third") Long id_third);


    //--------------------------------------------------------------------------------

    @SqlUpdate("insert into tienda724.common(name,description)\n" +
            " values(:productName, :productDescription)")
    void insertCommon(@Bind("productName") String productName,
                      @Bind("productDescription") String productDescription);

    @SqlQuery("select max(id_common) from tienda724.common where name=:productName and description=:productDescription")
    Long getLastCommonId(@Bind("productName") String productName,
                         @Bind("productDescription") String productDescription);

    @SqlUpdate("insert  into tienda724.product(stock,stock_min,img_url,id_common,id_category,code,id_tax,id_common_state,new_column,id_third)\n" +
            " values(null,null,null,:id_common,:id_category,null,:id_tax,null,null,:id_third)")
    void insertProduct(@Bind("id_common") Long id_common,
                       @Bind("id_category") Long id_category,
                       @Bind("id_tax") Long id_tax,
                       @Bind("id_third") Long id_third);

    //--------------------------------------------------------------------------------

    @SqlQuery("select id_common from tienda724.product where id_product=:id_product")
    Long getCommon(@Bind("id_product") Long id_product);

    @SqlUpdate("update tienda724.common set name=:productName, description=:productDescription\n" +
            " where id_common=:id_common")
    void updateCommon(@Bind("productName") String productName,
                      @Bind("productDescription") String productDescription,
                      @Bind("id_common") Long id_common);

    @SqlUpdate("update tienda724.product set id_category=:id_category,id_tax=:id_tax\n" +
            " where id_product=:id_product")
    void updateProduct(@Bind("id_category") Long id_category,
                        @Bind("id_tax") Long id_tax,
                        @Bind("id_product") Long id_product);

    //--------------------------------------------------------------------------------

    @SqlUpdate("insert into tienda724.codes(code,img,id_product,id_measure_unit," +
            " id_attribute_list,id_common_state,id_third,suggested_price, id_brand)" +
            " values(:code,null,:id_product,:id_measure_unit,null,null,:id_third,:suggested_price, :id_brand)")
    void postCode(@Bind("code") String code,
                  @Bind("id_product") Long id_product,
                  @Bind("id_measure_unit") Long id_measure_unit,
                  @Bind("id_third") Long id_third,
                  @Bind("suggested_price") Long suggested_price,
                  @Bind("id_brand") Long id_brand);

    //--------------------------------------------------------------------------------

    @SqlUpdate("update tienda724.codes set id_third=null \n" +
            " where id_code=:id_code")
    void putCodeByThird(@Bind("id_code") Long id_code);

    //--------------------------------------------------------------------------------

    @RegisterMapper(CodeName2Mapper.class)
    @SqlQuery("Select id_code,z.code,cp.name Product_name, c.name Unidad,suggested_price, br.brand" +
            " From Tienda724.codes z, tienda724.product p, Tienda724.measure_unit m, Tienda724.common  c, " +
            " Tienda724.common cp, Tienda724.brand br" +
            " Where z.id_product=p.id_product and \n" +
            " p.id_common=cp.id_common and \n" +
            " z.id_measure_unit=m.id_measure_unit and\n" +
            " m.id_common=c.id_common and\n" +
            " z.id_product=:id_product and z.code is not null and z.id_third is null and br.ID_BRAND = z.ID_BRAND")
    List<CodeName> getCodesGeneralByProduct(@Bind("id_product") Long id_product);


    @RegisterMapper(CodeName2Mapper.class)
    @SqlQuery(" Select id_code,z.code,cp.name Product_name, c.name Unidad,suggested_price, br.brand\n" +
            " From Tienda724.codes z, tienda724.product p, Tienda724.measure_unit m, Tienda724.common  c,\n" +
            "     Tienda724.common cp, Tienda724.brand br Where z.id_product=p.id_product and p.id_common=cp.id_common and\n" +
            "                                                   z.id_measure_unit=m.id_measure_unit and\n" +
            "                                                   m.id_common=c.id_common and z.id_product=:id_product and\n" +
            "                                                   z.code is not null and z.id_third=:id_third and br.ID_BRAND = z.ID_BRAND")
    List<CodeName> getOwnCodesByProduct(@Bind("id_product") Long id_product, @Bind("id_third") Long id_third);



    @SqlUpdate("Update Tienda724.codes " +
            " Set code=:code, id_measure_unit=:id_measure_unit, id_third=null " +
            "  id_code=:id_code and id_third=:id_third ")
    void putCodesToGeneral(@Bind("id_code") Long id_code,
                           @Bind("id_third") Long id_third,
                           @Bind("code") String code,
                           @Bind("id_measure_unit") Long id_measure_unit);

    @SqlUpdate("Update Tienda724.codes " +
            " Set id_measure_unit=:id_measure_unit " +
            " where id_code=:id_code and id_third=:id_third ")
    void putCodesByThird(@Bind("id_code") Long id_code,
                         @Bind("id_third") Long id_third,
                         @Bind("id_measure_unit") Long id_measure_unit);

    @SqlUpdate("Update Tienda724.codes " +
            " Set code=:code, id_measure_unit=:id_measure_unit, suggested_price=:suggested_price " +
            " where id_code=:id_code ")
    void putGenericCodes(@Bind("id_code") Long id_code,
                         @Bind("id_measure_unit") Long id_measure_unit,
                         @Bind("code") String code,
                         @Bind("suggested_price") Long suggested_price);

    @RegisterMapper(StoreXCodeMapper.class)
    @SqlQuery("select s.id_store,description,store_number,id_product_store, " +
            " product_store_name, product_store_code,ownbarcode, standard_price " +
            " from tienda724.product_store ps, Tienda724.store s " +
            " where ps.id_store=s.id_store and id_code=:id_code")
    List<ProductStoreName> getStoreByCode (@Bind("id_code") Long id_code);



    @RegisterMapper(StorageNameMapper.class)
    @SqlQuery("Select s.id_storage,description,storage_number,Quantity " +
            " From Tienda724.product_store_inventory psi,Tienda724.storage s " +
            " Where psi.id_storage=s.id_storage and id_product_store=:id_product_store")
    List<StorageName> getStorageByProductStore (@Bind("id_product_store") Long id_product_store);


    @RegisterMapper(StoreNameMapper.class)
    @SqlQuery(" Select id_store,description " +
            " From Tienda724.store " +
            " Where id_third=:id_third")
    List<StoreName> getStoreByThird (@Bind("id_third") Long id_third);


    @RegisterMapper(StorageName2Mapper.class)
    @SqlQuery(" Select id_storage, storage_number, description" +
            " From Tienda724.storage" +
            " Where id_store=:id_store")
    List<Storage2Name> getStorageByStore (@Bind("id_store") Long id_store);


    @RegisterMapper(CodeName3Mapper.class)
    @SqlQuery(" Select ps.id_code,ownbarcode,ps.product_store_name,product_store_code,standard_price,quantity,\n" +
            "       br.BRAND, cm.NAME as MUN\n" +
            " From Tienda724.product_store ps,Tienda724.product_store_inventory psi,\n" +
            "     TIENDA724.COMMON cm, tienda724.BRAND br, TIENDA724.MEASURE_UNIT mun,\n" +
            "     TIENDA724.CODES cd\n" +
            "       Where ps.id_product_store=psi.id_product_store\n" +
            "         and Id_storage=:Id_storage and cd.ID_CODE = ps.ID_CODE\n" +
            "         and cd.ID_BRAND = br.ID_BRAND and mun.ID_MEASURE_UNIT = cd.ID_MEASURE_UNIT\n" +
            "         and mun.ID_COMMON = cm.ID_COMMON\n" +
            " ORDER BY PRODUCT_STORE_NAME")
    List<Code2Name> getProductStoreByStorage (@Bind("Id_storage") Long Id_storage);


    @RegisterMapper(CodeStorageNameMapper.class)
    @SqlQuery(" Select id_product_store, product_store_name,product_store_code,standard_price " +
            " from tienda724.product_store " +
            " where id_code=:id_code and id_store=:id_store")
    List<Code2Name> getProductStoreByCodeStore  (@Bind("id_code") Long id_code, @Bind("id_store") Long id_store);


    @SqlUpdate(" Insert into Tienda724.product_store(id_code,id_store," +
            " product_store_name,product_store_code,standard_price,ownbarcode) " +
            " Values(:id_code,:id_store,:product_store_name,:product_store_code,:standard_price,:ownbarcode) ")
    void postProductStore(@Bind("id_code") Long id_code,
                          @Bind("id_store") Long id_store,
                          @Bind("product_store_name") String product_store_name,
                          @Bind("product_store_code") Long product_store_code,
                          @Bind("standard_price") Long standard_price,
                          @Bind("ownbarcode") String ownbarcode);

    @SqlUpdate("insert into tienda724.product_store_inventory" +
            " (id_product_store,id_storage,quantity)" +
            " values(:id_product_store,:id_storage,:quantity) ")
    void postProductStoreInventory(@Bind("id_product_store") Long id_product_store,
                          @Bind("id_storage") Long id_storage,
                          @Bind("quantity") Long quantity);


    @SqlUpdate("Update Tienda724.product_store " +
            " Set ownbarcode=:ownbarcode, " +
            " Product_store_name=:product_store_name, " +
            " Product_store_code=:product_store_code, " +
            " Standard_price=:standard_price " +
            " Where id_code=:id_code and id_store=:id_store")
    void putProductStore(@Bind("id_code") Long id_code,
                          @Bind("id_store") Long id_store,
                          @Bind("product_store_name") String product_store_name,
                          @Bind("product_store_code") Long product_store_code,
                          @Bind("standard_price") Long standard_price,
                          @Bind("ownbarcode") String ownbarcode);

    @SqlUpdate("Update Tienda724.product_store_inventory " +
            " Set quantity=:quantity " +
            " Where id_product_store=:id_product_store and id_storage=:id_storage ")
    void putProductStoreInventory (@Bind("id_product_store") Long id_product_store,
                                   @Bind("id_storage") Long id_storage,
                                   @Bind("quantity") Long quantity);




    //----------------------------------------------------------------------------------------------------------





    @SqlCall(" call ACTUALIZAR_DATOS_INVENTARIO(:idps," +
            " :costo, :precioanterior, :precionuevo, :cantidad, :tax, :itdhirduser, :status) ")
    void procedure2(@Bind("idps") Long idps,
                    @Bind("costo") Double costo,
                    @Bind("precioanterior") Double precioanterior,
                    @Bind("precionuevo") Double precionuevo,
                    @Bind("cantidad") Long cantidad,
                    @Bind("tax") Double tax,
                    @Bind("itdhirduser") Long itdhirduser,
                    @Bind("status") String status);



















    @RegisterMapper(InventoryNameActivosMapper.class)
    @SqlQuery("select b.id_tax,b.standard_price, b.id_product_store, b.product_store_name,\n" +
            "                               b.OWNBARCODE code, b.OWNBARCODE , b.PRODUCT_STORE_CODE, b.id_product_store as id_inventory_detail\n" +
            "                                ,b.STATUS,PSI.QUANTITY,s.INVENTARIO_DISPONIBLE\n" +
            "                                ,p.ID_CATEGORY,c.IMG\n" +
            "                        from tienda724.PRODUCT_STORE b,TIENDA724.PRODUCT_STORE_INVENTORY PSI,tienda724.store s,\n" +
            "                        tienda724.codes c,tienda724.product p\n" +
            "                        where B.ID_PRODUCT_STORE=PSI.ID_PRODUCT_STORE AND b.id_store=s.id_store\n" +
            "                          AND C.ID_CODE=B.ID_CODE AND C.ID_PRODUCT=P.ID_PRODUCT\n" +
            "                          AND b.ID_STORE = :ID_STORE\n" +
            "                          and b.DISPONIBLE_VENTA='S' and b.status='ACTIVO'\n" +
            "                       ORDER BY B.PRODUCT_STORE_NAME")
    List<InventoryNameActivos> getInventoryListActivos (@Bind("ID_STORE") Long ID_STORE);






    @RegisterMapper(InventoryNameMapper.class)
    @SqlQuery("select b.id_tax,b.standard_price, b.id_product_store, b.product_store_name,\n" +
            "              b.OWNBARCODE code, b.OWNBARCODE , b.PRODUCT_STORE_CODE, b.id_product_store as id_inventory_detail\n" +
            "         from tienda724.PRODUCT_STORE b\n" +
            "         where b.ID_STORE = :ID_STORE")
    List<InventoryName> getInventoryList (@Bind("ID_STORE") Long ID_STORE);



    @RegisterMapper(InventoryNameUpdateMapper.class)
    @SqlQuery("select b.id_tax,b.standard_price, b.id_product_store, b.product_store_name,\n" +
            "              b.OWNBARCODE code, b.OWNBARCODE , b.PRODUCT_STORE_CODE, b.id_product_store as id_inventory_detail, status\n" +
            "         from tienda724.PRODUCT_STORE b\n" +
            "         where b.ID_STORE = :ID_STORE")
    List<InventoryNameUpdate> getInventoryListUpdate(@Bind("ID_STORE") Long ID_STORE);





    @RegisterMapper(InventoryNameCompraMapper.class)
    @SqlQuery("select id_tax,standard_price, id_product_store, product_store_name,OWNBARCODE code, OWNBARCODE,PRODUCT_STORE_CODE,\n" +
            "                          id_product_store as id_inventory_detail,psprov.CODIGO_PROVEEDOR\n" +
            "                    from tienda724.product_store psprov\n" +
            "                     where psprov.id_store = :ID_STORE_PROV\n" +
            "                     and psprov.status='ACTIVO'\n" +
            "                    union\n" +
            "                    select\n" +
            "                    b.id_tax,b.standard_price, b.id_product_store, b.product_store_name,b.OWNBARCODE code, b.OWNBARCODE,b.PRODUCT_STORE_CODE,\n" +
            "                         b.id_product_store as id_inventory_detail,'' CODIGO_PROVEEDOR\n" +
            "                   from tienda724.PRODUCT_STORE b\n" +
            "                     where  b.ID_STORE = :ID_STORE_CLIENT \n" +
            "                     and b.status='ACTIVO'\n" +
            "                     order by CODIGO_PROVEEDOR nulls last")
    List<InventoryNameCompra> getInventoryListCompra(@Bind("ID_STORE_CLIENT") Long ID_STORE_CLIENT,
                                               @Bind("ID_STORE_PROV") Long ID_STORE_PROV);







    @SqlQuery("select sum(quantity) from tienda724.product_store_inventory where id_product_store=:id_product_store")
    Long getQuantity (@Bind("id_product_store") Long id_product_store);





    @RegisterMapper(vsessionMapper.class)
    @SqlQuery("select username,count(*) count from v$session group by username")
    List<vsession> vsession ();

    //----------------------------------------------------------------------------------------------------------

    @SqlQuery(" select ID_CODE from codes where code =:code and ID_THIRD is null ")
    List<Long> getCodesGeneralByCode(@Bind("code") String code);

    @RegisterMapper(CodeDataMapper.class)
    @SqlQuery("  select a.CODE, c.NAME as PRODUCT_NAME, e.NAME as MUN_NAME, f.BRAND as BRAND_NAME\n" +
            " from TIENDA724.CODES a, TIENDA724.PRODUCT b, TIENDA724.COMMON c, TIENDA724.MEASURE_UNIT d,\n" +
            "      TIENDA724.COMMON e, TIENDA724.BRAND f, TIENDA724.COMMON g\n" +
            " where\n" +
            " a.ID_CODE = :id_code and\n" +
            " a.ID_PRODUCT = b.ID_PRODUCT\n" +
            " and b.ID_COMMON = c.ID_COMMON and a.ID_MEASURE_UNIT = d.ID_MEASURE_UNIT and\n" +
            " e.ID_COMMON = d.ID_COMMON and a.ID_BRAND = f.ID_BRAND and ROWNUM < 2 ")
    List<codeData> getCodeByIdCode(@Bind("id_code") Long id_code);



    @RegisterMapper(CategoriaNodoMapper.class)
    @SqlQuery("  SELECT LEVEL, a.name category,a.id_category,a.id_category_father,colinea.name category_father\n" +
            "            ,A.IMG_URL CATEGORY_URL,LINEA.IMG_URL CATEGORY_FATHER_URL\n" +
            "            FROM   \n" +
            "            (select com.name,cat.ID_CATEGORY,cat.ID_CATEGORY_FATHER,cat.IMG_URL \n" +
            "            from tienda724.common com,tienda724.category cat,tienda724.product p, tienda724.codes c,\n" +
            "            TIENDA724.PRODUCT_STORE ps\n" +
            "            where com.id_common(+)=cat.id_common and cat.id_category(+)=p.ID_CATEGORY and p.ID_PRODUCT(+)=c.ID_PRODUCT\n" +
            "            and c.id_code(+)=ps.id_code and ps.id_store=:idstore and ps.DISPONIBLE_VENTA='S'\n" +
            "            union\n" +
            "            select com.name,cat.ID_CATEGORY,cat.ID_CATEGORY_FATHER,cat.IMG_URL \n" +
            "            from tienda724.common com,tienda724.category cat, TIENDA724.CATEGORY_TIPOSTORE ct\n" +
            "            ,tienda724.tipo_store ts,TIENDA724.STORE_TIPOSTORE sts\n" +
            "            where com.id_common=cat.id_common and cat.ID_CATEGORY=ct.ID_CATEGORY \n" +
            "            and ct.ID_TIPO_STORE=ts.ID_TIPO_STORE and sts.ID_TIPO_STORE=ts.ID_TIPO_STORE \n" +
            "            and sts.id_store=:idstore) A,tienda724.category linea,tienda724.common colinea \n" +
            "            where a.id_category_father=linea.ID_CATEGORY(+) and linea.id_common=colinea.ID_COMMON(+)\n" +
            "            START WITH a.id_category_father is null\n" +
            "            CONNECT BY a.id_category_father = PRIOR a.ID_CATEGORY ")
    List<CategoriaNodo> getCategoriasParaArbol(@Bind("idstore") Long idstore);
}
