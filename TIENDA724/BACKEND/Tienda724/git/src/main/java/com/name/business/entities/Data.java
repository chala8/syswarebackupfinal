package com.name.business.entities;

public class Data {

    public String getNombretienda() {
        return nombretienda;
    }

    public void setNombretienda(String nombretienda) {
        this.nombretienda = nombretienda;
    }

    public String getNombrepropietario() {
        return nombrepropietario;
    }

    public void setNombrepropietario(String nombrepropietario) {
        this.nombrepropietario = nombrepropietario;
    }

    public String getUrl_logo() {
        return url_logo;
    }

    public void setUrl_logo(String url_logo) {
        this.url_logo = url_logo;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getId_country() { return id_country; }

    public void setId_country(String id_country) { this.id_country = id_country; }

    String nombretienda;
    String nombrepropietario;
    String url_logo;
    String ciudad;
    String direccion;
    String telefono;
    String email;
    String latitud;
    String longitud;


    String id_country;



    public Data( String nombretienda,
            String nombrepropietario,
            String url_logo,
            String ciudad,
            String direccion,
            String telefono,
            String email,
            String latitud,
            String longitud,
            String id_country) {
        this.id_country = id_country;
        this.nombretienda = nombretienda;
        this.nombrepropietario = nombrepropietario;
        this.url_logo = url_logo;
        this.ciudad = ciudad;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.latitud = latitud;
        this.longitud = longitud;
    }



}
