package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;

public class RegisterDTO {


    private Long id_store;
    private String description;
    private Long caja_number;

    @JsonCreator
    public RegisterDTO(@JsonProperty("id_store") Long id_store,
                       @JsonProperty("description") String description,
                       @JsonProperty("caja_number") Long caja_number) {
        this.id_store = id_store;
        this.description = description;
        this.caja_number = caja_number;
    }

    //------------------------------------------------------------------------------------------------

    public Long getid_store() {
        return id_store;
    }

    public void setid_store(Long id_store) {
        this.id_store = id_store;
    }

    //------------------------------------------------------------------------------------------------

    public String getdescription() {
        return description;
    }

    public void setdescription(String description) {
        this.description = description;
    }

    //------------------------------------------------------------------------------------------------

    public Long getcaja_number() { return caja_number; }

    public void setcaja_number(Long caja_number) { this.caja_number = caja_number; }

    //------------------------------------------------------------------------------------------------


}
