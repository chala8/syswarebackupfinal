package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class ExcelRowPendDTO {


    public String getOwnbarcode() {
        return ownbarcode;
    }

    public void setOwnbarcode(String ownbarcode) {
        this.ownbarcode = ownbarcode;
    }

    public String getProduct_STORE_NAME() {
        return product_STORE_NAME;
    }

    public void setProduct_STORE_NAME(String product_STORE_NAME) {
        this.product_STORE_NAME = product_STORE_NAME;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    private String ownbarcode;
    private String product_STORE_NAME;
    private Long quantity;


    @JsonCreator
    public ExcelRowPendDTO(
            @JsonProperty("ownbarcode") String ownbarcode,
            @JsonProperty("product_STORE_NAME") String product_STORE_NAME,
            @JsonProperty("quantity") Long quantity
    ) {
        this.ownbarcode = ownbarcode;
        this.product_STORE_NAME = product_STORE_NAME;
        this.quantity = quantity;
    }

}
