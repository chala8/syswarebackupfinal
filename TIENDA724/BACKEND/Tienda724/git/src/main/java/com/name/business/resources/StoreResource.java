package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.StoreBusiness;
import com.name.business.entities.*;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;
import org.json.JSONObject;
import org.skife.jdbi.v2.sqlobject.Bind;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Path("/store")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StoreResource {
    private StoreBusiness storeBusiness;

    public StoreResource(StoreBusiness storeBusiness) {
        this.storeBusiness = storeBusiness;
    }


    @GET
    @Path("/vendors")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getVendors(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<Vendor>> getProductsEither = storeBusiness.getVendors(id_third);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Timed
    @Path("/get/2")
    @RolesAllowed({"Auth"})
    public Response getStoreByThird2(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<Store>> getProductsEither = storeBusiness.getStoreByThird2(id_third);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getStoreByThird(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<Store>> getProductsEither = storeBusiness.getStoreByThird(id_third);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Timed
    @Path("/store2")
    @RolesAllowed({"Auth"})
    public Response getStoresClients(@QueryParam("idstore") Long idstore) {
        Response response;

        Either<IException, List<Store>> getProductsEither = storeBusiness.getStoresClients(idstore);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/id")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getIdDirectoryByIdStore(@QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List<Long>> getProductsEither = storeBusiness.getIdDirectoryByIdStore(id_store);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postStore(StoreDTO storeDTO) {
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.postStore(
                storeDTO.getid_third(),
                storeDTO.getdescription(),
                storeDTO.getid_directory(),
                storeDTO.getstore_number(),
                storeDTO.getentrada_consecutive_initial(),
                storeDTO.getsalida_consecutive_initial()
        );

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response putStore(@QueryParam("id_store") Long id_store,StoreDTO storeDTO) {
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.putStore(
                storeDTO.getid_third(),
                storeDTO.getdescription(),
                storeDTO.getstore_number(),
                storeDTO.getentrada_consecutive_initial(),
                storeDTO.getsalida_consecutive_initial(),
                id_store);

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @PUT
    @Timed
    @Path("/updateCodProv")
    @RolesAllowed({"Auth"})
    public Response updateProvCode(@QueryParam("id_store")  Long ID_STORE_PROVEEDOR,
                                   @QueryParam("codProv") String codProv,
                                   @QueryParam("ownbarcode") String ownbarcode) {
        Response response;

        Either<IException, Long> getProductsEither = storeBusiness.updateProvCode(ID_STORE_PROVEEDOR,
                codProv,
                ownbarcode);

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/b")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCajaByStore(@QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List<Register>> getProductsEither = storeBusiness.getCajaByStore(id_store);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/b")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postCaja(RegisterDTO registerDTO) {
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.postCaja(
                registerDTO.getid_store(),
                registerDTO.getdescription(),
                registerDTO.getcaja_number());

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/b")
    @Timed
    @RolesAllowed({"Auth"})
    public Response putCaja(@QueryParam("id_caja") Long id_caja,RegisterDTO registerDTO) {
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.putCaja(
                registerDTO.getdescription(),
                registerDTO.getcaja_number(),
                id_caja);

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/s")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getStorageByStore(@QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List<Storage>> getProductsEither = storeBusiness.getStorageByStore(id_store);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/s/categories")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCategoryByStore(@QueryParam("id_third") Long ID_THIRD) {
        Response response;

        Either<IException, List<StorageCategory>> getProductsEither = storeBusiness.getCategoryByStore(ID_THIRD);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/s/by")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getStoreByCategory(@QueryParam("id_category") Long id_category) {
        Response response;

        Either<IException, List<StoreName>> getProductsEither = storeBusiness.getStoreByCategory(id_category);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/s")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postStorage(StorageDTO storageDTO) {
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.postStorage(
                storageDTO.getid_store(),
                storageDTO.getdescription(),
                storageDTO.getstorage_number());

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/s")
    @Timed
    @RolesAllowed({"Auth"})
    public Response putStorage(@QueryParam("id_store") Long id_store,StorageDTO storageDTO) {
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.putStorage(
                storageDTO.getdescription(),
                storageDTO.getstorage_number(),
                id_store);

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    //------------------------------------------------------------------------------------------------------------------


    @GET
    @Path("/d")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDirectorybyID(@QueryParam("id_directory") Long id_directory) {
        Response response;

        Either<IException, List<Directory>> getProductsEither = storeBusiness.getDirectorybyID(id_directory);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/d/city")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCityByID(@QueryParam("id_city") Long id_city) {
        Response response;

        Either<IException, List<String>> getProductsEither = storeBusiness.getCityByID(id_city);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/d/city/bystate")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCitiesByState(@QueryParam("id_state") Long id_state) {
        Response response;

        Either<IException, List<String>> getProductsEither = storeBusiness.getCitiesByState(id_state);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/states")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getStates() {
        Response response;

        Either<IException, List<State>> getProductsEither = storeBusiness.getStates();
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/phones")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPhonesByDirectory(@QueryParam("id_directory") Long id_directory) {
        Response response;

        Either<IException, List<Phone>> getProductsEither = storeBusiness.getPhonesByDirectory(id_directory);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/mails")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getMailsByDirectory(@QueryParam("id_directory") Long id_directory) {
        Response response;

        Either<IException, List<Mail>> getProductsEither = storeBusiness.getMailsByDirectory(id_directory);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    //------------------------------------------------------------------------------------------------------------------


    @GET
    @Path("/pricelist")
    @Timed
    @RolesAllowed({"Auth","PublicAPI"})
    public Response getPriceList(@QueryParam("id_product_store") Long id_product_store) {
        Response response;

        Either<IException, List<Price>> getProductsEither = storeBusiness.getPriceList(id_product_store);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/pricelist/vista")
    @Timed
    @RolesAllowed({"Auth","PublicAPI"})
    public Response getPriceListView(@QueryParam("id_product_store") Long id_product_store,@QueryParam("idbp") Long idbp) {
        Response response;

        Either<IException, List<Price>> getProductsEither = storeBusiness.getPriceListView(id_product_store,idbp);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/pricelist")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postPrice(@QueryParam("id_product_store") Long id_product_store,
                              @QueryParam("price_description") String price_description,
                              @QueryParam("price") Double price
                              ) {
        //System.out.println(id_product_store);
        //System.out.println(price_description);
        //System.out.println(price);
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.postPrice(id_product_store,
                price_description,
                price);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/third")
    @Timed
    @RolesAllowed({"Auth"})
    public Response putThird(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, Long> getProductsEither = storeBusiness.putThird(id_third);

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/third")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getThird(@QueryParam("id_person") Long id_person) {
        Response response;

        Either<IException, Long> getProductsEither = storeBusiness.getThird(id_person);

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/psid")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPsId(
                            @QueryParam("code") String code,
                            @QueryParam("id_store") Long id_store) {
        Response response;
        Either<IException, Long> getProductsEither = storeBusiness.getPsId(code,id_store);

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/ps")
    @Timed
    @RolesAllowed({"Auth"})
    public Response postProductStore(ProductStoreDTO productStoreDTO) {
        Response response;

        Either<IException, Long> getProductsEither = storeBusiness.postProductStore(
                productStoreDTO.getid_store(),
                productStoreDTO.getid_code(),
                productStoreDTO.getproduct_store_name(),
                productStoreDTO.getstandard_price(),
                productStoreDTO.getproduct_store_code(),
                productStoreDTO.getownbarcode()
        );

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/tipoStore")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTipoStore(@QueryParam("id_store") Long id_store) {

        Response response;

        Either<IException, List<Long>> getProductsEither = storeBusiness.getTipoStore(id_store);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/cajaitems")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCajaItems(@QueryParam("id_person") Long id_person) {
        Response response;

        Either<IException, List<ItemCaja>> getProductsEither = storeBusiness.getCajaItems(id_person);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/getDatosTienda")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getDatosTienda(@QueryParam("id_store_proveedor") Long id_store_proveedor,
                                 @QueryParam("id_app") Long id_app) {
        Response response;

        Either<IException, List<DatosTienda>> getProductsEither = storeBusiness.getDatosTienda(id_store_proveedor, id_app);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @PUT
    @Path("/standardprice")
    @Timed
    @RolesAllowed({"Auth"})
    public Response putStandardPrice(@QueryParam("standard_price") Double standard_price,@QueryParam("id_ps") Long id_ps) {
        Response response;

        Either<IException, Integer> getProductsEither = storeBusiness.putStandardPrice(
                standard_price,
                id_ps);

        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/stPriceByPs")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getStandardPriceByPs(@QueryParam("id_ps") Long id_ps) {
        Response response;

        Either<IException, Double> getProductsEither = storeBusiness.getStandardPriceByPs(id_ps);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @POST
    @Path("/confirmarPC")
    @Timed
    @RolesAllowed({"Auth"})
    public Response confirmar_pedido_cliente(@QueryParam("idbillcliente") Long idbillcliente,
                                             @QueryParam("notes") String notese){
        Response response;

        Either<IException, Long> getProducts = storeBusiness.confirmar_pedido_cliente(
                idbillcliente,
                notese);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




    @POST
    @Path("/devolucionVenta")
    @Timed
    @RolesAllowed({"Auth"})
    public Response devolucionVenta(@QueryParam("id_bill") Long id_bill,
                                    @QueryParam("idcaja") Long idcaja){
        Response response;

        Either<IException, Long> getProducts = storeBusiness.devolucionVenta(id_bill,
                idcaja);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/crearTiendaCliente")
    @Timed
    @RolesAllowed({"Auth"})
    public Response crearTiendaCliente(@QueryParam("idthirdprop") Long idthirdprop,
                                    @QueryParam("idthirdempresa") Long idthirdempresa,
                                    @QueryParam("storename") String storename,
                                    @QueryParam("estienda") String estienda,
                                    @QueryParam("username") String username,
                                    @QueryParam("copiaproductos") String copiaproductos,
                                    @QueryParam("esproveedor") String esproveedor,
                                    @QueryParam("idstorecliente") Long idstorecliente,
                                    @QueryParam("idtempcliente") Long idtempcliente){
        Response response;

        Either<IException, Long> getProducts = storeBusiness.crearTiendaCliente(idthirdprop,
                idthirdempresa,
                storename,
                estienda,
                username,
                copiaproductos,
                esproveedor,
                idstorecliente,
                idtempcliente);

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/getIdThirdFatherByProvider")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getIdThirdFatherByProvider(@QueryParam("id_store_provider") Long id_store_provider,
                                         @QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<InfoData>> getProductsEither = storeBusiness.getInfobyIdthird(id_store_provider,id_third);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/getIfIsOnParam")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getIfIsOnParam(@QueryParam("id_store_provider") Long id_store_provider,
                                   @QueryParam("id_store_client") Long id_store_client) {
        Response response;

        Either<IException, Long> getProductsEither = storeBusiness.getIfIsOnParam(id_store_provider,id_store_client);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/createParamPedidos")
    @Timed
    @RolesAllowed({"Auth"})
    public Response createParamPedidos(@QueryParam("id_store_provider") Long id_store_provider,
                                       @QueryParam("id_store_client") Long id_store_client,
                                       @QueryParam("id_third_cliente") Long id_third_cliente,
                                       @QueryParam("id_third_employee_cliente") Long id_third_employee_cliente,
                                       @QueryParam("id_third_proveedor") Long id_third_proveedor) {
        Response response;

        Either<IException, Long> getProductsEither = storeBusiness.createParamPedidos(id_store_provider,id_store_client,id_third_cliente,id_third_employee_cliente,id_third_proveedor);
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/copyNew")
    @Timed
    @RolesAllowed({"Auth"})
    public Response copyNew() {
        Response response;

        Either<IException, Long> getProductsEither = storeBusiness.copyNew();
        if (getProductsEither.isRight()) {
            //System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/GET_VALOR_DOMICILIO")
    @Timed
    public Response GET_VALOR_DOMICILIO(
            @QueryParam("IDSTORE") String IDSTORE,
            @QueryParam("DISTANCIA_KM") String DISTANCIA_KM){
        Response response;

        Either<IException, Float> getProducts = storeBusiness.GET_VALOR_DOMICILIO(IDSTORE,DISTANCIA_KM);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @GET
    @Path("/getProductStore")
    @Timed
    public Response getProductStore(
            @QueryParam("ownbarcode") String ownbarcode,
            @QueryParam("id_store") String id_store){
        Response response;

        Either<IException, Long> getProducts = storeBusiness.getProductStore(ownbarcode, id_store);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/getOptions")
    @Timed
    public Response getOptions(
            @QueryParam("idps") String idps){
        Response response;

        Either<IException, List<Option>> getProducts = storeBusiness.getOptions(idps);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/getNotesForDetailsWithOptions")
    @Timed
    public Response getNotesForDetailsWithOptions(
            @QueryParam("idbill") String idbill){
        Response response;

        Either<IException, List<DetailWithOptions>> getProducts = storeBusiness.getNotesForDetailsWithOptions(idbill);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/getStoreNameById")
    @Timed
    public Response getStoreNameById(
            @QueryParam("idStore") String idStore){
        Response response;

        Either<IException, String> getProducts = storeBusiness.getStoreNameById(idStore);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

}




