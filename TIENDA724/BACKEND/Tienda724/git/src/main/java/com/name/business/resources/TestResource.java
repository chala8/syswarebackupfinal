package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.DatoBusiness;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by luis on 1/10/16.
 */
@Path("/tests")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TestResource {

    private DatoBusiness datoBusiness;

    public TestResource(DatoBusiness datoBusiness) {
        this.datoBusiness = datoBusiness;
    }

    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTestList(){

        datoBusiness.crearDato(null);
        return Response.status(Response.Status.OK).build();
    }
}
