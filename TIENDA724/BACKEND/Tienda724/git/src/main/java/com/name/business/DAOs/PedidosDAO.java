package com.name.business.DAOs;

import com.name.business.entities.*;

import com.name.business.mappers.*;
import com.name.business.representations.PriceListDTO;

import java.util.Date;
import java.util.List;

import org.skife.jdbi.v2.sqlobject.*;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.unstable.BindIn;

@RegisterMapper(PriceListMapper.class)
@UseStringTemplate3StatementLocator
public interface PedidosDAO {


    @SqlQuery(" select ownbarcode from tienda724.product_store where id_product_store=:id_product_store ")
    String getOwnBarCode(@Bind("id_product_store") Long id_product_store);


    @RegisterMapper(VehiculoMapper.class)
    @SqlQuery(" SELECT ID_VEHICULO,PLACA,NVL(FIRST_NAME,'')||' '||NVL(SECOND_NAME,'')||' '||NVL(FIRST_LASTNAME,'') AS CONDUCTOR\n" +
            " FROM FACTURACION724.VEHICULO V,TERCERO724.PERSON P\n" +
            " WHERE V.ID_PERSON=P.ID_PERSON\n" +
            " ORDER BY ID_VEHICULO ")
    List<Vehiculo> getVehiculos();



    @SqlQuery(" SELECT ID_VEHICULO\n" +
            " FROM FACTURACION724.VEHICULO V,TERCERO724.PERSON P\n" +
            " WHERE V.ID_PERSON=P.ID_PERSON\n" +
            " ORDER BY ID_VEHICULO ")
    List<Long> getVehiculosIds();


    /*
    @RegisterMapper(PlanillaMapper.class)
    @SqlQuery(" SELECT ID_PLANILLA,FECHA_INICIO,NUM_PLANILLA,PLACA,NVL(FIRST_NAME,'')||' '||NVL(SECOND_NAME,'')||' '||NVL(FIRST_LASTNAME,'') AS CONDUCTOR\n" +
            "    FROM FACTURACION724.PLANILLA P,FACTURACION724.VEHICULO V,TERCERO724.PERSON PE\n" +
            "    WHERE P.ID_VEHICULO=V.ID_VEHICULO AND V.ID_PERSON=PE.ID_PERSON\n" +
            "    AND P.ID_VEHICULO=:idvehiculo AND P.STATUS='O' AND P.ID_STORE=:idstore")
    List<Planilla> getPlanillas(@Bind("idvehiculo") Long idvehiculo,
                                @Bind("idstore") Long idstore);
     */


    @RegisterMapper(PlanillaMapper.class)
    @SqlQuery(" SELECT ID_PLANILLA,FECHA_INICIO,NUM_PLANILLA,PLACA,NVL(FIRST_NAME,'')||' '||NVL(SECOND_NAME,'')||' '||NVL(FIRST_LASTNAME,'') AS CONDUCTOR, P.STATUS\n" +
            "           FROM FACTURACION724.PLANILLA P,FACTURACION724.VEHICULO V,TERCERO724.PERSON PE\n" +
            "           WHERE P.ID_VEHICULO=V.ID_VEHICULO AND V.ID_PERSON=PE.ID_PERSON\n" +
            "        AND P.ID_VEHICULO in (<idvehiculo>) AND P.STATUS in (<status>) AND P.ID_STORE=:idstore and p.fecha_inicio between trunc(:date1) and trunc(:date2) + 1 ")
    List<Planilla> getPlanillasV2(@BindIn("idvehiculo") List<String> idvehiculo,
                                  @BindIn("status") List<String> status,
                                  @Bind("idstore") Long idstore,
                                  @Bind("date1") Date Date1,
                                  @Bind("date2") Date Date2);


    @RegisterMapper(DetallePlanillaMapper.class)
    @SqlQuery(" SELECT dp.ID_BILL_FACTURA, b.TOTALPRICE, b.PURCHASE_DATE, b.NUM_DOCUMENTO\n" +
            "     FROM facturacion724.DETALLE_PLANILLA dp, facturacion724.BILL b\n" +
            "     where dp.ID_BILL_FACTURA = b.ID_BILL and dp.ID_PLANILLA=:idplanilla ")
    List<DetallePlanilla> getDetallesPlanillas(@Bind("idplanilla") Long idplanilla);


    @RegisterMapper(PlanillaDetailMapper.class)
    @SqlQuery(" SELECT b.num_documento,b.totalprice valor,st.description store,d.address \n" +
              " FROM facturacion724.planilla pl,facturacion724.detalle_planilla dp,facturacion724.bill b, tienda724.store st, tercero724.directory d\n" +
              " WHERE  pl.id_planilla=dp.id_planilla and dp.id_bill_factura=b.id_bill \n" +
              " and b.id_store_client=st.id_store and st.id_directory=d.id_directory\n" +
              " AND pl.ID_PLANILLA=:idplanilla")
    List<PlanillaDetail> getPlanillaDetail(@Bind("idplanilla") Long idplanilla);



    @SqlQuery(" select id_product_store from tienda724.product_store where ownbarcode=:code and id_store = :id_store ")
    Long getPsId(@Bind("code") String code, @Bind("id_store") Long id_store);



    @SqlUpdate(" UPDATE FACTURACiON724.PLANILLA SET NOTES=NOTES||' - '||to_char(sysdate,'dd-mm-yyyy hh24:mi')||' - '||:observaciones ,status='C', FECHA_CIERRE=sysdate where id_planilla=:idplanilla ")
    void updatePlanilla(@Bind("observaciones") String observaciones,
                        @Bind("idplanilla") Long idplanilla);


    @SqlUpdate(" update tienda724.product_store set pct_umbral=:pctumbral where ownbarcode=:barcodehijo and id_store=:idstorehijo ")
    void updateUmbral(@Bind("pctumbral") Long pctumbral,
                      @Bind("barcodehijo") String barcodehijo,
                      @Bind("idstorehijo") Long idstorehijo);


    @SqlUpdate(" INSERT INTO FACTURACION724.PLANILLA (FECHA_INICIO, ID_VEHICULO, STATUS, NOTES, ID_STORE)\n" +
            " VALUES (SYSDATE, :idvehiculo, 'O','--Inicio planilla  ',:idstore) ")
    void insertNewPlanilla(@Bind("idvehiculo") Long idvehiculo,
                        @Bind("idstore") Long idstore);


    @RegisterMapper(PedidoMapper.class)
    @SqlQuery(" select id_bill,c.fullname cliente,s.description tienda,num_documento_cliente numpedido,purchase_date fecha\n" +
            "from facturacion724.bill b,tienda724.store s,tercero724.third t,tercero724.common_basicinfo c\n" +
            "where b.id_store_client=s.id_store and id_bill_type=:id_bill_type and id_bill_state=:id_bill_state and b.id_store=:id_store\n" +
            "and b.id_third_destinity=t.id_third and t.id_common_basicinfo=c.id_common_basicinfo ")
    List<Pedido>  getPedidos(@Bind("id_store") Long id_store,
                             @Bind("id_bill_state") Long id_bill_state,
                             @Bind("id_bill_type") Long id_bill_type);


    @RegisterMapper(DetallePedidoMapper.class)
    @SqlQuery(" select pr.provider fabricante,b.brand marca,com.name linea,co.name categoria,comu.name presentacion,ps.product_store_name producto,\n" +
            "       sum(db.quantity) cantidad,ps.standard_price costo,sum(db.quantity)*ps.standard_price costototal\n" +
            "from tienda724.common co,tienda724.category cat,tienda724.common com,tienda724.category linea,tienda724.product p,tienda724.provider pr,\n" +
            "     tienda724.measure_unit mu,tienda724.common comu,tienda724.brand b,tienda724.codes c,tienda724.product_store ps,\n" +
            "     facturacion724.detail_bill db\n" +
            "where co.id_common=cat.id_common and cat.id_category=p.id_category and com.id_common=linea.id_common and linea.id_category=cat.id_category_father\n" +
            "  and p.id_product=c.id_product and c.id_code=ps.id_code and pr.id_provider=b.id_provider\n" +
            "  and c.id_brand=b.id_brand and c.id_measure_unit=mu.id_measure_unit and mu.id_common=comu.id_common\n" +
            "  and ps.id_product_store=db.id_product_third and id_bill in (<id_bill>)\n" +
            "group by pr.provider,b.brand,com.name,co.name,comu.name,ps.product_store_name,ps.standard_price\n" +
            "order by 1,2,3,4 ")
    List<DetallePedido>  getDetallesPedidos(@BindIn("id_bill") List<Long> id_store);




    @SqlCall(" call tienda724.actualizar_casos_especiales(:idbill,:idbillstate,:code,:idstore,:cantidad,:tipo) ")
    void procedure(@Bind("idbill") Long idbill,
                   @Bind("idbillstate") Long idbillstate,
                   @Bind("code") Long code,
                   @Bind("idstore") Long idstore,
                   @Bind("cantidad") Long cantidad,
                   @Bind("tipo") String tipo);




    @SqlCall(" call tienda724.actualizar_inventario_a_cero(:idstore) ")
    void actualizar_inventario_a_cero(@Bind("idstore") Long idstore);



    @SqlCall(" call tienda724.agregar_conversion(:barcodehijo, :barcodepadre, :factor, :prioridad) ")
    void agregar_conversion(@Bind("barcodehijo") String barcodehijo,
                            @Bind("barcodepadre") String barcodepadre,
                            @Bind("factor") Long factor,
                            @Bind("prioridad") Long prioridad);


    @SqlCall(" call tienda724.eliminar_asociacion(:barcodehijo,:barcodepadre,:idstorecliente,:idstoreprov) ")
    void eliminar_asociacion(@Bind("barcodehijo") String barcodehijo,
                            @Bind("barcodepadre") String barcodepadre,
                            @Bind("idstorecliente") Long idstorecliente,
                            @Bind("idstoreprov") Long idstoreprov);


    @RegisterMapper(AsociacionMapper.class)
    @SqlQuery(" SELECT PSH.PRODUCT_STORE_NAME PRODUCTO_HIJO,PSH.OWNBARCODE CODIGO_HIJO,PSP.PRODUCT_STORE_NAME PRODUCTO_PADRE,PSP.OWNBARCODE CODIGO_PADRE,PC.VALUE factor,psh.pct_umbral\n" +
            "FROM TIENDA724.PRODUCT_CONVERSION PC, TIENDA724.PRODUCT_STORE PSP, TIENDA724.PRODUCT_STORE PSH\n" +
            "WHERE PC.ID_CODE=PSH.ID_CODE AND PC.ID_CODE_ORIGIN=PSP.ID_CODE AND PSP.ID_STORE=:idstorep AND PSH.ID_STORE=:idstoreh ")
    List<Asociacion> getAsociaciones(@Bind("idstorep") Long idstorep,
                                     @Bind("idstoreh") Long idstoreh);



    @RegisterMapper(VendedorDistribuidorMapper.class)
    @SqlQuery(" select tp.id_third id_third_proveedor, cbip.fullname Proveedor,r.ID_STORE_PROVEEDOR,r.ID_THIRD_VENDEDOR,cbi.FULLNAME vendedor\n" +
            "from otros.ruteros r,tercero724.third t,tercero724.common_basicinfo cbi\n" +
            ",tienda724.store sp,tercero724.third tp,tercero724.common_basicinfo cbip\n" +
            "where r.ID_THIRD_VENDEDOR=t.ID_THIRD and t.ID_COMMON_BASICINFO=cbi.ID_COMMON_BASICINFO\n" +
            "and r.ID_STORE_PROVEEDOR=sp.id_store and sp.ID_THIRD=tp.ID_THIRD and tp.ID_COMMON_BASICINFO=cbip.ID_COMMON_BASICINFO\n" +
            "and id_store_cliente=:id_store_cliente and id_app=:id_app ")
    List<VendedorDistribuidor> getVendedoresDistribuidor(@Bind("id_store_cliente") Long id_store_cliente,
                                                         @Bind("id_app") Long id_app);


    @SqlQuery(" select s.id_store idstore from tercero724.third t,tienda724.store s,tercero724.third tf\n" +
            "where t.id_third_father=tf.id_third and s.ID_THIRD=tf.id_third\n" +
            "and t.id_third=:id_third\n ")
    List<Long> getIdStores(@Bind("id_third") Long id_third);

}
