package com.name.business.entities;

import java.util.Date;

public class codeData {
    private String code;
    private String product_name;
    private String munName;
    private String brandName;


    public codeData(String code,
                     String product_name,
                     String munName,
                     String brandName) {
        this.code = code;
        this.product_name = product_name;
        this.munName = munName;
        this.brandName = brandName;
    }

    public String getcode() {
        return code;
    }

    public void setcode(String code) {
        this.code = code;
    }

    //------------------------------------------------------------------------------

    public String getproduct_name() {
        return product_name;
    }

    public void setproduct_name(String product_name) {
        this.product_name = product_name;
    }

    //------------------------------------------------------------------------------

    public String getmunName() {
        return munName;
    }

    public void setmunName(String munName) {
        this.munName = munName;
    }

    //------------------------------------------------------------------------------

    public String getbrandName() {
        return brandName;
    }

    public void setbrandName(String brandName) {
        this.brandName = brandName;
    }

    //------------------------------------------------------------------------------

}


