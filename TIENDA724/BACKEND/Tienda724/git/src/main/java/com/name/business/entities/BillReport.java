package com.name.business.entities;

public class BillReport {

    private String FULLNAME;
    private String PREFIX_BILL;
    private String PURCHASE_DATE;
    private Long ID_BILL;
    private Long CONSECUTIVE;
    private Double VENTA;
    private Double COSTO;
    private Double UTILIDAD;
    private Double PCT_MARGEN_VENTA;
    private Double PCT_MARGEN_COSTO;
    private String CAJA;
    private String NUM_DOCUMENTO;
    private String TIENDA;
    private String CAJERO;
    private Long ID_BILL_STATE;
    private String DOMICILIARIO;
    private Double TAX;
    private Long TIPOFACTURA;
    private Long ID_MESA;

    public BillReport(Double TAX,
                        String FULLNAME,
                        String PREFIX_BILL,
                        String PURCHASE_DATE,
                        Long ID_BILL,
                        Long CONSECUTIVE,
                        Double VENTA,
                        Double COSTO,
                        Double UTILIDAD,
                        Double PCT_MARGEN_VENTA,
                        String CAJA,
                        String NUM_DOCUMENTO,
                        Double PCT_MARGEN_COSTO,
                        String TIENDA,
                        String CAJERO,
                        Long ID_BILL_STATE,
                        String DOMICILIARIO,
                        Long TIPOFACTURA,
                        Long ID_MESA
                      ) {
        this.ID_MESA = ID_MESA;
        this.TAX = TAX;
        this.FULLNAME = FULLNAME;
        this.PREFIX_BILL = PREFIX_BILL;
        this.PURCHASE_DATE = PURCHASE_DATE;
        this.ID_BILL = ID_BILL;
        this.CONSECUTIVE = CONSECUTIVE;
        this.VENTA = VENTA;
        this.COSTO = COSTO;
        this.UTILIDAD = UTILIDAD;
        this.PCT_MARGEN_VENTA = PCT_MARGEN_VENTA;
        this.CAJA = CAJA;
        this.NUM_DOCUMENTO = NUM_DOCUMENTO;
        this.PCT_MARGEN_COSTO = PCT_MARGEN_COSTO;
        this.TIENDA = TIENDA;
        this.CAJERO = CAJERO;
        this.ID_BILL_STATE = ID_BILL_STATE;
        this.DOMICILIARIO = DOMICILIARIO;
        this.TIPOFACTURA = TIPOFACTURA;
    }

    public Long getID_MESA() {
        return ID_MESA;
    }

    public void setID_MESA(Long ID_MESA) {
        this.ID_MESA = ID_MESA;
    }

    //---------------------------------------------------------------------------

    public Long getTIPOFACTURA() {
        return TIPOFACTURA;
    }

    public void setTIPOFACTURA(Long TIPOFACTURA) {
        this.TIPOFACTURA = TIPOFACTURA;
    }

    //---------------------------------------------------------------------------

    public Double getTAX() {
        return TAX;
    }

    public void setTAX(Double TAX) {
        this.TAX = TAX;
    }

    //---------------------------------------------------------------------------

    public String getDOMICILIARIO() {
        return DOMICILIARIO;
    }

    public void setDOMICILIARIO(String DOMICILIARIO) {
        this.DOMICILIARIO = DOMICILIARIO;
    }

    //---------------------------------------------------------------------------

    public String getCAJERO() {
        return CAJERO;
    }

    public void setCAJERO(String CAJERO) {
        this.CAJERO = CAJERO;
    }

    //---------------------------------------------------------------------------

    public String getTIENDA() {
        return TIENDA;
    }

    public void setTIENDA(String TIENDA) {
        this.TIENDA = TIENDA;
    }

    //---------------------------------------------------------------------------

    public String getNUM_DOCUMENTO() {
        return NUM_DOCUMENTO;
    }

    public void setNUM_DOCUMENTO(String NUM_DOCUMENTO) {
        this.NUM_DOCUMENTO = NUM_DOCUMENTO;
    }

    //---------------------------------------------------------------------------

    public String getCAJA() {
        return CAJA;
    }

    public void setCAJA(String CAJA) {
        this.CAJA = CAJA;
    }

    //---------------------------------------------------------------------------

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }

    //---------------------------------------------------------------------------

    public String getPREFIX_BILL() {
        return PREFIX_BILL;
    }

    public void setPREFIX_BILL(String PREFIX_BILL) {
        this.PREFIX_BILL = PREFIX_BILL;
    }

    //---------------------------------------------------------------------------
    public String getPURCHASE_DATE() {
        return PURCHASE_DATE;
    }

    public void setPURCHASE_DATE(String PURCHASE_DATE) {
        this.PURCHASE_DATE = PURCHASE_DATE;
    }

    //---------------------------------------------------------------------------
    public Long getID_BILL() {
        return ID_BILL;
    }

    public void setID_BILL(Long ID_BILL) {
        this.ID_BILL = ID_BILL;
    }

    //---------------------------------------------------------------------------
    public Long getCONSECUTIVE() {
        return CONSECUTIVE;
    }

    public void setCONSECUTIVE(Long CONSECUTIVE) {
        this.CONSECUTIVE = CONSECUTIVE;
    }

    //---------------------------------------------------------------------------
    public Double getVENTA() {
        return VENTA;
    }

    public void setVENTA(Double VENTA) {
        this.VENTA = VENTA;
    }

    //-------------------------------------------------------------------------

    public Double getCOSTO() {
        return COSTO;
    }

    public void setCOSTO(Double COSTO) {
        this.COSTO = COSTO;
    }

    //-------------------------------------------------------------------------

    public Double getUTILIDAD() {
        return UTILIDAD;
    }

    public void setUTILIDAD(Double UTILIDAD) {
        this.UTILIDAD = UTILIDAD;
    }

    //-------------------------------------------------------------------------


    public Double getPCT_MARGEN_VENTA() {
        return PCT_MARGEN_VENTA;
    }

    public void setPCT_MARGEN_VENTA(Double PCT_MARGEN_VENTA) {
        this.PCT_MARGEN_VENTA = PCT_MARGEN_VENTA;
    }

    //-------------------------------------------------------------------------

    public Double getPCT_MARGEN_COSTO() {
        return PCT_MARGEN_COSTO;
    }

    public void setPCT_MARGEN_COSTO(Double PCT_MARGEN_COSTO) {
        this.PCT_MARGEN_COSTO = PCT_MARGEN_COSTO;
    }

    //-------------------------------------------------------------------------
    public Long getID_BILL_STATE() {
        return ID_BILL_STATE;
    }

    public void setID_BILL_STATE(Long ID_BILL_STATE) {
        this.ID_BILL_STATE = ID_BILL_STATE;
    }

}
