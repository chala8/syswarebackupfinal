package com.name.business.DAOs;

import com.name.business.entities.Storage;
import com.name.business.entities.Store;
import com.name.business.entities.Register;
import com.name.business.entities.Directory;
import com.name.business.entities.State;
import com.name.business.entities.Phone;
import com.name.business.entities.*;
import com.name.business.mappers.*;

import com.name.business.representations.InventoryDetailDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.unstable.BindIn;

import java.util.Date;
import java.util.List;


@UseStringTemplate3StatementLocator
public interface Kazu724DAO {

    @RegisterMapper(BalanceKazuMapper.class)
    @SqlQuery(" select b.codigo_cuenta, p.descripcion, b.valor, b.ID_STORE, b.DEBITO, b.CREDITO,  b.saldo, b.SALDO_INICIAL, b.SALDO_TOTAL  from kazu724.balance_temp2 b, kazu724.puc p where b.codigo_cuenta = p.codigo_cuenta and p.id_country=169 and b.id_store=:id_store order by codigo_cuenta")
    List<BalanceKazu> getBalance(@Bind("id_store") Long id_store);

    @RegisterMapper(docTypeKazuMapper.class)
    @SqlQuery(" select id_document_type,document_type from kazu724.document_type order by 2 ")
    List<docTypeKazu> getDocType();

    @RegisterMapper(docStatusMapper.class)
    @SqlQuery(" select id_document_status,document_status from kazu724.document_status order by 2 ")
    List<docStatus> getDocStatus();

    @RegisterMapper(codigoCuentaMapper.class)
    @SqlQuery(" select codigo_cuenta,descripcion from kazu724.puc where codigo_cuenta_padre is null order by 1 ")
    List<codigoCuenta> gecCodCuentaGen();

    @RegisterMapper(codigoCuentaMapper.class)
    @SqlQuery(" select codigo_cuenta,descripcion from kazu724.puc where codigo_cuenta_padre=:cp order by 1")
    List<codigoCuenta> gecCodCuenta(@Bind("cp") Long cp);
    //---------------------------------------------------------------------------------------------------------


    @SqlUpdate(" insert into kazu724.puc values(:cc,:description,:ccp,:id_country,:id_store) ")
    void postPuc(@Bind("cc") Long cc,
                 @Bind("description") String description,
                 @Bind("ccp") Long ccp,
                 @Bind("id_country") Long id_country,
                 @Bind("id_store") Long id_store);

    @SqlUpdate(" insert into kazu724.document(id_document,fecha,id_document_type,id_document_status,notes,id_store,id_third_user) \n" +
            "    values(kazu724.document_seq.nextval,sysdate,:id_document_type,:id_document_status,:notes,:id_store,:id_third_user) ")
    void postStatus(@Bind("id_document_status") Long id_document_status,
                    @Bind("id_document_type") Long id_document_type,
                    @Bind("notes") String notes,
                    @Bind("id_store") Long id_store,
                    @Bind("id_third_user") Long id_third_user);

    @SqlUpdate(" insert into kazu724.document_detail(CODIGO_CUENTA,VALOR,NATURALEZA,NOTAS,ID_DOCUMENT,ID_COUNTRY,ID_THIRD) \n" +
            "     values(:cc,:valor,:naturaleza,:notes,:id_document,:id_country,:id_third) ")
    void postDocDetail(@Bind("cc") Long cc,
                       @Bind("valor") double valor,
                       @Bind("naturaleza") String naturaleza,
                       @Bind("notes") String notes,
                       @Bind("id_document") Long id_document,
                       @Bind("id_country") Long id_country,
                       @Bind("id_third") Long id_third);

    @RegisterMapper(kazu724DocumentMapper.class)
    @SqlQuery(" select id_document,fecha,d.id_document_status,document_status,d.id_document_type,document_type,consecutivo,notes,d.id_store,s.description store, d.id_third, cb.fullname\n" +
            "from kazu724.document d,kazu724.document_status ds,kazu724.document_type dt,tienda724.store s, tercero724.third t, tercero724.common_basicinfo cb\n" +
            "where d.id_document_status=ds.id_document_status and d.id_document_type=dt.id_document_type and d.id_store=s.id_store\n" +
            "and d.id_document_status in (<statusList>) and d.id_document_type in (<typeList>) and d.id_store=:id_store\n" +
            "and d.id_third = t.id_third and t.id_common_basicinfo = cb.id_common_basicinfo\n" +
            "and fecha between trunc(:date1) and trunc(:date2)+1\n" +
            "order by fecha ")
    List<kazu724Document> getDocuments(@Bind("id_store") Long id_store,
                                       @Bind("date1") Date date1,
                                       @Bind("date2") Date date2,
                                       @BindIn("statusList") List<String> statusList,
                                       @BindIn("typeList") List<String> typeList);




    @RegisterMapper(kazu724DetailMapper.class)
    @SqlQuery(" select dd.id_third, p.codigo_cuenta,p.descripcion,valor,naturaleza,notas,dd.id_document_detail, cb.fullname from kazu724.document_detail dd, tercero724.third t, tercero724.common_basicinfo cb, kazu724.puc p " +
            "where dd.codigo_cuenta=p.codigo_cuenta and dd.id_country=p.id_country and dd.id_document=:id_document and dd.id_third = t.id_third and t.id_common_basicinfo = cb.id_common_basicinfo " +
            "order by p.codigo_cuenta ")
    List<kazu724Detail> getDetailsDocument(@Bind("id_document") Long id_document);


    @SqlUpdate(" UPDATE KAZU724.DOCUMENT SET NOTES= :notes, ID_DOCUMENT_STATUS=:id_status where ID_DOCUMENT = :id_doc ")
    void updateDocument(@Bind("id_status") Long id_status,
                        @Bind("notes") String notes,
                        @Bind("id_doc") Long id_doc);



    @SqlCall(" call kazu724.generar_balance(:id_store, :profundidad, :fecha_inicial, :fecha_final,'',169) ")
    void generarBalance(@Bind("id_store") Long id_store,
                   @Bind("profundidad") Long profundidad,
                   @Bind("fecha_inicial") Date fecha_inicial,
                   @Bind("fecha_final") Date fecha_final);

    @SqlCall(" call KAZU724.CREATE_DOCUMENT_KAZU(:iddocumenttype, :iddocumentstatus, :notes, :idstore, :idthirduser, :idthird, :docdetails) ")
    void postDocument(@Bind("iddocumenttype") Long iddocumenttype,
                      @Bind("iddocumentstatus") Long iddocumentstatus,
                      @Bind("notes") String notes,
                      @Bind("idstore") Long idstore,
                      @Bind("idthirduser") Long idthirduser,
                      @Bind("idthird") Long idthird,
                      @Bind("docdetails") String docdetails);


    @SqlUpdate(" DELETE FROM kazu724.document_detail WHERE ID_DOCUMENT_DETAIL= :id_detail ")
    void deleteDocumentDetail(@Bind("id_detail") Long id_detail);

    @SqlUpdate(" INSERT INTO KAZU724.document_detail (CODIGO_CUENTA,VALOR,NATURALEZA,NOTAS,ID_DOCUMENT,ID_COUNTRY) VALUES (:cod_cuenta, :valor, :naturaleza, :notas, :id_document, 169) ")
    void insertDocumentDetail(@Bind("cod_cuenta") Long cod_cuenta,
                            @Bind("valor") double valor,
                            @Bind("naturaleza") String naturaleza,
                            @Bind("notas") String notas,
                            @Bind("id_document") Long id_document);


    @SqlUpdate(" INSERT INTO KAZU724.PUC (CODIGO_CUENTA, DESCRIPCION, CODIGO_CUENTA_PADRE, ID_COUNTRY) VALUES (:cc, :desc, :ccp, :id_country) ")
    void insertPuc (@Bind("cc") Long cc,
                    @Bind("desc") String desc,
                    @Bind("ccp") Long ccp,
                    @Bind("id_country") Long id_country);


    @SqlQuery("SELECT ID_DOCUMENT FROM kazu724.DOCUMENT WHERE ID_DOCUMENT IN (SELECT MAX( ID_DOCUMENT ) FROM kazu724.DOCUMENT )\n")
    Long getPkLast();

}