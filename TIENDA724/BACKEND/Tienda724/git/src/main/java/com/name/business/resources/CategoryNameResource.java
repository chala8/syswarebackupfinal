package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.CategoryNameBusiness;
import com.name.business.entities.*;
import com.name.business.representations.CategoryDTO;
import com.name.business.representations.ListaTipoDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import com.name.business.representations.CategoryNameDTO;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Path("/categories2")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategoryNameResource {
    private CategoryNameBusiness categoryNameBusiness;

    public CategoryNameResource(CategoryNameBusiness categoryNameBusiness) {
        this.categoryNameBusiness = categoryNameBusiness;
    }

    @POST
    @Path("/get")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCategories(ListaTipoDTO listaTipoDTO) {
        Response response;
        Either<IException, List<CategoryName>> getCategoriesEither = categoryNameBusiness.readCategoryName(listaTipoDTO);
        if (getCategoriesEither.isRight()) {
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }

    @GET
    @Path("/children")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCategoriesChildren(@QueryParam("id_category_father") Long id_category_father) {
        Response response;

        Either<IException, List<CategoryName>> getCategoriesEither = categoryNameBusiness.readCategoryChildren(id_category_father);
        if (getCategoriesEither.isRight()) {
            //System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }


    // FALLA--------

    @GET
    @Path("/byThird")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCategoriesByThird(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<CategoryName>> getCategoriesEither = categoryNameBusiness.readCategoryByThird(id_third);
        if (getCategoriesEither.isRight()) {
            //System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }

    //-----------------


    @GET
    @Path("/children/byThird")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getCategoriesChildrenByThird(@QueryParam("id_category_father") Long id_category_father,@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<CategoryName>> getCategoriesEither = categoryNameBusiness.getCategoriesChildrenByThird(id_category_father,id_third);
        if (getCategoriesEither.isRight()) {
            //System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }


    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postCategory(CategoryNameDTO categoryNameDTO) {
        Response response;

        Either<IException, String> getCategoriesEither = categoryNameBusiness.postCategory(
                categoryNameDTO.getCategoryName(),
                categoryNameDTO.getCategoryDescription(),
                categoryNameDTO.getImg_url(),
                categoryNameDTO.getId_category_father(),
                categoryNameDTO.getId_third_category());
        if (getCategoriesEither.isRight()) {
            //System.out.println(getCategoriesEither.right().value());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }


    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response putCategory(@QueryParam("id_category") Long id_category,CategoryNameDTO categoryNameDTO) {
        Response response;

        Either<IException, String> getCategoriesEither = categoryNameBusiness.putCategory(
                id_category,
                categoryNameDTO.getCategoryName(),
                categoryNameDTO.getCategoryDescription(),
                categoryNameDTO.getImg_url());
        if (getCategoriesEither.isRight()) {
            //System.out.println(getCategoriesEither.right().value());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }



    @GET
    @Path("/productsOnStore")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getProductsOnStoreByCategory(@QueryParam("id_third") Long id_third, @QueryParam("id_category") Long id_category, @QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List<ProductOnStore>> getCategoriesEither = categoryNameBusiness.getProductsOnStoreByCategory(id_third, id_category, id_store);
        if (getCategoriesEither.isRight()) {
            //System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }


    @GET
    @Path("/productStore")
    @Timed
    @RolesAllowed({"Auth"})
    public Response getProductStoreByCategory(@QueryParam("id_ps") Long id_ps, @QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List<ProductStore>> getCategoriesEither = categoryNameBusiness.getProductStoreByCategory(id_ps,id_store);
        if (getCategoriesEither.isRight()) {
            //System.out.println(getCategoriesEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getCategoriesEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getCategoriesEither);
        }
        return response;
    }
}
