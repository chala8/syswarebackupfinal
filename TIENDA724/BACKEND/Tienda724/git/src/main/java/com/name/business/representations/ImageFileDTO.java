package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

public class ImageFileDTO {

    public FormDataMultiPart getFile() {
        return file;
    }

    public void setFile(FormDataMultiPart file) {
        this.file = file;
    }

    private FormDataMultiPart file;

    @JsonCreator
    public ImageFileDTO(@JsonProperty("file") FormDataMultiPart file) {
        this.file=file;
    }


}
