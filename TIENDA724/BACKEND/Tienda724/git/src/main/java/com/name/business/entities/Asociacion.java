package com.name.business.entities;

public class Asociacion {

    private String PRODUCTO_HIJO;
    private String CODIGO_HIJO;
    private String PRODUCTO_PADRE;
    private String CODIGO_PADRE;
    private Long factor;
    private Long pct_umbral;

    public Asociacion(String PRODUCTO_HIJO,
             String CODIGO_HIJO,
             String PRODUCTO_PADRE,
             String CODIGO_PADRE,
             Long factor,
             Long pct_umbral) {
                this.PRODUCTO_HIJO = PRODUCTO_HIJO;
                this.CODIGO_HIJO = CODIGO_HIJO;
                this.PRODUCTO_PADRE = PRODUCTO_PADRE;
                this.CODIGO_PADRE = CODIGO_PADRE;
                this.factor = factor;
                this.pct_umbral = pct_umbral;
    }



    public String getPRODUCTO_HIJO() {
        return PRODUCTO_HIJO;
    }

    public void setPRODUCTO_HIJO(String PRODUCTO_HIJO) {
        this.PRODUCTO_HIJO = PRODUCTO_HIJO;
    }

    public String getCODIGO_HIJO() {
        return CODIGO_HIJO;
    }

    public void setCODIGO_HIJO(String CODIGO_HIJO) {
        this.CODIGO_HIJO = CODIGO_HIJO;
    }

    public String getPRODUCTO_PADRE() {
        return PRODUCTO_PADRE;
    }

    public void setPRODUCTO_PADRE(String PRODUCTO_PADRE) {
        this.PRODUCTO_PADRE = PRODUCTO_PADRE;
    }

    public String getCODIGO_PADRE() {
        return CODIGO_PADRE;
    }

    public void setCODIGO_PADRE(String CODIGO_PADRE) {
        this.CODIGO_PADRE = CODIGO_PADRE;
    }

    public Long getFactor() {
        return factor;
    }

    public void setFactor(Long factor) {
        this.factor = factor;
    }

    public Long getPct_umbral() {
        return pct_umbral;
    }

    public void setPct_umbral(Long pct_umbral) {
        this.pct_umbral = pct_umbral;
    }



}
