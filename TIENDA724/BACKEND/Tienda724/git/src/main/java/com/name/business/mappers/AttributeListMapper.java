package com.name.business.mappers;

import com.name.business.entities.AttributeList;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AttributeListMapper implements ResultSetMapper<AttributeList>{

    @Override
    public AttributeList map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new AttributeList(
                resultSet.getLong("ID_ATTRIBUTE_LIST"),
                new CommonState(
                        resultSet.getLong("ID_STATE"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_ATTRIBUTE_LIST"),
                        resultSet.getDate("MODIFY_ATTRIBUTE_LIST")
                )
        );
    }
}
