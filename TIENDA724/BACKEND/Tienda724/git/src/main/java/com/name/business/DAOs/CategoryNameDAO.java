package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.*;
import com.name.business.representations.CategoryDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.unstable.BindIn;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import javax.ws.rs.BeanParam;
import java.util.List;


@UseStringTemplate3StatementLocator
public interface CategoryNameDAO {
    @RegisterMapper(CategoryNameMapper.class)
    @SqlQuery(" select a.id_category ID_CATEGORY,c.name CATEGORY, a.IMG_URL\n" +
              "        from tienda724.category a,tienda724.common c, tienda724.category_tipostore ct\n" +
              "        where a.ID_COMMON=c.ID_COMMON\n" +
              "     AND a.id_category_father IS NULL and id_third is null and ct.ID_TIPO_STORE IN (<lista>) and ct.id_category = a.id_category order by 2 ")
    List<CategoryName> readCategoryName(@BindIn("lista") List<Long> lista);

    @RegisterMapper(CategoryNameMapper.class)
    @SqlQuery(" select a.id_category ID_CATEGORY,c.name CATEGORY, a.IMG_URL\n" +
            " from tienda724.category a,tienda724.common c\n" +
            " where a.ID_COMMON=c.ID_COMMON\n" +
            " AND a.id_category_father=:id_category_father")
    List<CategoryName> readCategoryChildren(@Bind("id_category_father") Long id_category_father);

    @RegisterMapper(CategoryNameMapper.class)
    @SqlQuery("select a.id_category ID_CATEGORY,c.name CATEGORY, a.IMG_URL\n" +
            " from tienda724.category a,tienda724.common c\n" +
            " where a.ID_COMMON=c.ID_COMMON\n" +
            "  AND a.id_category_father IS NULL and id_third is null\n" +
            " UNION\n" +
            " select a.id_category ID_CATEGORY,c.name CATEGORY, a.IMG_URL\n" +
            " from tienda724.category a,tienda724.common c\n" +
            " where a.ID_COMMON=c.ID_COMMON\n" +
            "  AND a.id_category_father IS NULL and id_third=:id_third order by 2")
    List<CategoryName> readCategoryNameByThird(@Bind("id_third") Long id_third);

    @RegisterMapper(CategoryNameMapper.class)
    @SqlQuery(" select a.id_category ID_CATEGORY,c.name CATEGORY, a.IMG_URL\n" +
            " from tienda724.category a,tienda724.common c\n" +
            " where a.ID_COMMON=c.ID_COMMON\n" +
            "  AND a.id_category_father=:id_category_father and id_third is null\n" +
            " UNION\n" +
            " select a.id_category ID_CATEGORY,c.name CATEGORY, a.IMG_URL\n" +
            " from tienda724.category a,tienda724.common c\n" +
            " where a.ID_COMMON=c.ID_COMMON\n" +
            "  AND a.id_category_father=:id_category_father and id_third=:id_third")
    List<CategoryName> getCategoriesChildrenByThird(@Bind("id_category_father") Long id_category_father, @Bind("id_third") Long id_third);




    //--------------------------------------------------------------------------------

    @SqlUpdate("insert into tienda724.common(name,description)\n" +
            " values(:categoryName, :categoryDescription)")
    void insertCommon(@Bind("categoryName") String categoryName,
                      @Bind("categoryDescription") String categoryDescription);

    @SqlQuery("select max(id_common) from tienda724.common where name=:categoryName and description=:categoryDescription")
    Long getLastCommonId(@Bind("categoryName") String categoryName,
                               @Bind("categoryDescription") String categoryDescription);

    @SqlUpdate("insert into tienda724.category(id_common,img_url,id_category_father,id_common_state,id_third,id_tipo_store) \n" +
            " values(:id_common,:img_url,:id_category_father,null,:id_third,0)")
    void insertCategory(@Bind("id_common") Long id_common,
                        @Bind("img_url") String img_url,
                        @Bind("id_category_father") Long id_category_father,
                        @Bind("id_third") Long id_third);

    //--------------------------------------------------------------------------------

    @SqlQuery("select id_common from tienda724.category where id_category=:id_category")
    Long getCommon(@Bind("id_category") Long id_category);

    @SqlUpdate("update tienda724.common set name=:categoryName, description=:categoryDescription\n" +
            " where id_common=:id_common")
    void updateCommon(@Bind("categoryName") String categoryName,
                      @Bind("categoryDescription") String categoryDescription,
                      @Bind("id_common") Long id_common);

    @SqlUpdate("update tienda724.category set img_url=:img_url\n" +
            " where id_category=:id_category")
    void updateCategory(@Bind("id_category") Long id_category,
                        @Bind("img_url") String img_url);

    //--------------------------------------------------------------------------------


    @RegisterMapper(ProductOnStoreMapper.class)
    @SqlQuery(" select f.DESCRIPTION, a.quantity, b.PRODUCT_STORE_NAME, b.PRODUCT_STORE_CODE, c.CODE, cm.NAME as MUN, br.BRAND\n" +
            " from PRODUCT_STORE_INVENTORY a, PRODUCT_STORE b, CODES c,\n" +
            "     PRODUCT d, STORE e, tienda724.STORAGE f, tienda724.MEASURE_UNIT m, COMMON cm, BRAND br\n" +
            " where a.ID_PRODUCT_STORE = b.ID_PRODUCT_STORE\n " +
            "  and b.ID_CODE = c.ID_CODE\n" +
            "  and c.ID_PRODUCT = d.ID_PRODUCT\n" +
            "  and d.ID_CATEGORY = :id_category\n" +
            "  and b.ID_STORE = e.ID_STORE\n" +
            "  and e.ID_THIRD = :id_third\n" +
            "  and a.ID_STORAGE = f.ID_STORAGE\n" +
            "  and c.ID_MEASURE_UNIT = m.ID_MEASURE_UNIT\n" +
            "  and cm.ID_COMMON = m.ID_COMMON\n" +
            "  and br.ID_BRAND = c.ID_BRAND" +
            "  and b.ID_STORE = :id_store " +
            "  ORDER BY b.PRODUCT_STORE_NAME ")
    List<ProductOnStore> getProductsOnStoreByCategory(@Bind("id_third") Long id_third,
                                                      @Bind("id_category") Long id_category,
                                                      @Bind("id_store") Long id_store);

    @RegisterMapper(ProductStoreMapper.class)
    @SqlQuery("Select id_product_store,product_store_name\n" +
            " from tienda724.product_store ps,tienda724.codes c,tienda724.product p\n" +
            " where ps.id_code=c.id_code and c.id_product=p.id_product and\n" +
            "    p.id_category=:id_category and ps.id_store = :id_store")
    List<ProductStore> getProductStoreByCategory (@Bind("id_category") Long id_category,
                                                  @Bind("id_store") Long id_store);




}