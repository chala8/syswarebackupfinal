package com.name.business.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class excelRowPedidos {

    private String presentacion;
    private String marca;
    private String categoria;
    private String linea;
    private String producto;
    private String barcode;
    private Long cantidad_VENDIDA;
    private Long cantidad_EN_INVENTARIO;
    private String fabricante;
    private Double costo;
    private Double iva;
    private String proveedor;

    @JsonCreator
    public excelRowPedidos(
            @JsonProperty("presentacion") String presentacion,
            @JsonProperty("marca") String marca,
            @JsonProperty("categoria") String categoria,
            @JsonProperty("linea") String linea,
            @JsonProperty("producto") String producto,
            @JsonProperty("barcode") String barcode,
            @JsonProperty("cantidad_VENDIDA") Long cantidad_VENDIDA,
            @JsonProperty("cantidad_EN_INVENTARIO") Long cantidad_EN_INVENTARIO,
            @JsonProperty("fabricante") String fabricante,
            @JsonProperty("costo") Double costo,
            @JsonProperty("iva") Double iva,
            @JsonProperty("idt") Long idt,
            @JsonProperty("idps") Double idps,
            @JsonProperty("id_PROVIDER") Long id_PROVIDER,
            @JsonProperty("id_STORE") Long id_STORE,
            @JsonProperty("id_THIRD") Long id_THIRD,
            @JsonProperty("proveedor") String proveedor
    ) {
        this.proveedor = proveedor;
        this.presentacion = presentacion;
        this.categoria = categoria;
        this.cantidad_VENDIDA = cantidad_VENDIDA;
        this.cantidad_EN_INVENTARIO = cantidad_EN_INVENTARIO;
        this.linea = linea;
        this.marca = marca;
        this.producto = producto;
        this.barcode = barcode;
        this.costo = costo;
        this.iva = iva;
        this.fabricante = fabricante;

    }


    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;  }


    public Double getcosto() {
        return costo;
    }
    public void setcosto(Double costo) {
        this.costo = costo;
    }


    public Double getiva() {
        return iva;
    }
    public void setiva(Double iva) {
        this.iva = iva;
    }


    public String getfabricante() {
        return fabricante;
    }
    public void setfabricante(String fabricante) {
        this.fabricante = fabricante;
    }


    public String getbarcode() {
        return barcode;
    }
    public void setbarcode(String barcode) {
        this.barcode = barcode;
    }


    public String getproducto() {
        return producto;
    }
    public void setproducto(String producto) {
        this.producto = producto;
    }


    public String getpresentacion() {
        return presentacion;
    }
    public void setpresentacion(String presentacion) {
        this.presentacion = presentacion;
    }


    public Long getcantidad_VENDIDA() {
        return cantidad_VENDIDA;
    }
    public void setcantidad_VENDIDA(Long cantidad_VENDIDA) {
        this.cantidad_VENDIDA = cantidad_VENDIDA;
    }


    public String getmarca() {
        return marca;
    }
    public void setmarca(String marca) {
        this.marca = marca;
    }


    public String getlinea() {
        return linea;
    }
    public void setlinea(String linea) {
        this.linea = linea;
    }


    public Long getcantidad_EN_INVENTARIO() {
        return cantidad_EN_INVENTARIO;
    }
    public void setcantidad_EN_INVENTARIO(Long cantidad_EN_INVENTARIO) { this.cantidad_EN_INVENTARIO = cantidad_EN_INVENTARIO; }


    public String getcategoria() {
        return categoria;
    }
    public void setcategoria(String categoria) {
        this.categoria = categoria;
    }

}
