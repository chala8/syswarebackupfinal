package com.name.business.DAOs;
import java.time.LocalDate;
import com.name.business.entities.*;
import com.name.business.mappers.*;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.unstable.BindIn;

import java.util.Date;
import java.util.List;


@UseStringTemplate3StatementLocator
public interface ReportsDAO {

    @RegisterMapper(DateReportMapper.class)
    @SqlQuery("select sum(subtotal) subtotal,sum(tax) tax,sum(venta) Total from (\n" +
            "                 select b.id_bill_state, b.num_documento, co.fullname,l.prefix_bill,b.purchase_date fecha,b.id_bill,b.consecutive,\n" +
            "                        round(sum(quantity*price*(1+d.tax/100)) ,2) venta,\n" +
            "                        round(sum(quantity*standard_price*(1+d.tax/100)) ,2) costo,\n" +
            "                        round(sum(quantity*price*(1+d.tax/100)) ,2)-round(sum(quantity*standard_price*(1+d.tax/100)) ,2) utilidad,\n" +
            "                        round(( round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100)))  )*100/round(sum(quantity*decode(price,0,1)*(1+d.tax/100))) ,1) pct_margen_venta,\n" +
            "                        round(( round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100)))  )*100/round(sum(quantity*decode(standard_price,0,1)*(1+d.tax/100))),1) pct_margen_costo,\n" +
            "                       st.description tienda,ca.caja_number caja,cbi.fullname cajero\n" +
            "                         ,round(sum(quantity*price*(d.tax/100)) ,2) tax,round(sum(quantity*price) ,2) subtotal\n" +
            "                 from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps,tercero724.third t,tercero724.legal_data l,\n" +
            "                      tercero724.common_basicinfo co,tienda724.caja ca,tienda724.store st,tercero724.third cajero,tercero724.common_basicinfo cbi\n" +
            "                         ,facturacion724.detail_payment_bill dpb\n" +
            "                 where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store and b.id_third=t.id_third\n" +
            "                   and t.id_common_basicinfo=co.id_common_basicinfo and t.id_legal_data=l.id_legal_data and b.id_caja=ca.id_caja(+)\n" +
            "                   and b.id_store=st.id_store and b.id_third_employee=cajero.id_third and cajero.id_common_basicinfo=cbi.id_common_basicinfo\n" +
            "                   and b.id_bill=dpb.id_bill\n" +
            "                   and b.id_bill_type=:id_bill_type and b.id_bill_state in (1) and b.id_store  in (<listStore>)\n" +
            "                   and b.purchase_date between :date1 and :date2\n" +
            "                   and dpb.id_payment_method in (1,2,3)\n" +
            "                 group by b.id_bill_state, b.num_documento, co.fullname,l.prefix_bill,b.purchase_date,b.id_bill,b.consecutive,st.description,ca.caja_number,cbi.fullname\n" +
            "                 order by id_bill_state,fecha desc\n" +
            "             ) ")
    DateReport getBillTotal(@Bind("id_bill_type") Long id_bill_type,
                            @Bind("date1") Date date1,
                            @Bind("date2") Date date2,
                            @Bind("id_third") Long id_third,
                            @BindIn ("listStore") List<Long> listStore);

    @RegisterMapper(DateReportCatMapper.class)
    @SqlQuery(" select co.name categoria,sum(b.subtotal) subtotal,sum(b.tax) tax,sum(b.totalprice) Total\n" +
            " from facturacion724.bill b,facturacion724.detail_bill d,tienda724.product_store ps, \n" +
            "     tienda724.codes c, TIENDA724.PRODUCT p, tienda724.category cat, tienda724.common co\n" +
            " where b.id_bill=d.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code \n" +
            "      and c.id_product=p.id_product and p.id_category=cat.id_category and cat.id_common=co.id_common \n" +
            "      and b.id_bill_type=:id_bill_type and b.purchase_date between :date1 and :date2\n" +
            " group by co.name\n" +
            " order by 2 desc ")
    DateReport2 getTotalByCategory(@Bind("id_bill_type") Long id_bill_type,
                                   @Bind("date1") Date date1,
                                   @Bind("date2") Date date2);

    @RegisterMapper(DateReportBrandMapper.class)
    @SqlQuery(" select br.brand Marca,sum(b.subtotal) subtotal,sum(b.tax) tax,sum(b.totalprice) Total\n" +
            " from facturacion724.bill b,facturacion724.detail_bill d,tienda724.product_store ps, \n" +
            "     tienda724.codes c, tienda724.brand br\n" +
            " where b.id_bill=d.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code and c.id_brand=br.id_brand  \n" +
            "      and b.id_bill_type=:id_bill_type and b.purchase_date between :date1 and :date2\n" +
            " group by br.brand \n" +
            " order by 2 desc ")
    DateReport2 getTotalByBrand(@Bind("id_bill_type") Long id_bill_type,
                                @Bind("date1") Date date1,
                                @Bind("date2") Date date2);


    @RegisterMapper(DateReportProdMapper.class)
    @SqlQuery(" select ps.product_store_name,sum(b.subtotal) subtotal,sum(b.tax) tax,sum(b.totalprice) Total\n" +
            " from facturacion724.bill b,facturacion724.detail_bill d,tienda724.product_store ps\n" +
            " where b.id_bill=d.id_bill and d.id_product_third=ps.id_product_store\n" +
            "  and b.id_bill_type=:id_bill_type and b.purchase_date between :date1 and (:date2+0.99) and ps.id_store=:id_store\n" +
            " group by ps.product_store_name\n" +
            " order by 2 desc ")
    List <DateReport2> getTotalByProduct(@Bind("id_bill_type") Long id_bill_type,
                                         @Bind("date1") Date date1,
                                         @Bind("date2") Date date2,
                                         @Bind("id_store") Long id_store);


    @RegisterMapper(LabelReportMapper.class)
    @SqlQuery("  select sum(totalprice) as TOTAL\n" +
            "             from facturacion724.bill b,facturacion724.detail_bill d,tienda724.product_store ps,tienda724.codes c,tienda724.product p\n" +
            "             where b.id_bill=d.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code and c.id_product=p.id_product and\n" +
            "             id_bill_state=1 and id_bill_type=:id_bill_type and p.id_category=:id_category \n" +
            "             and b.purchase_date between :date1 and :date2 \n" +
            "             and ps.ID_STORE in (<listStore>)")
    LabelReport getTotalByBillTypeCategory(@Bind("id_bill_type") Long id_bill_type,
                                           @Bind("id_category") Long id_category,
                                           @Bind("date1") Date date1,
                                           @Bind("date2") Date date2,
                                           @Bind("id_third") Long id_third,
                                           @BindIn ("listStore") List<Long> listStore);



    @RegisterMapper(LabelReportMapper.class)
    @SqlQuery(" select round(sum(d.quantity*d.price*(1+d.tax/100)) ,2) TOTAL\n" +
            "                from facturacion724.bill b,facturacion724.detail_bill d,tienda724.product_store ps,tienda724.codes c,tienda724.product p,\n" +
            "                tienda724.category cat\n" +
            "                where b.id_bill=d.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code and c.id_product=p.id_product and\n" +
            "                id_bill_state=1 and id_bill_type=:id_bill_type\n" +
            "                and p.id_category=cat.id_category and cat.id_category_father=:id_category\n" +
            "                and b.purchase_date between :date1 and :date2 \n" +
            "                and b.ID_STORE in (<listStore>) ")
    LabelReport getTotalByBillTypeLine(@Bind("id_bill_type") Long id_bill_type,
                                           @Bind("id_category") Long id_category,
                                           @Bind("date1") Date date1,
                                           @Bind("date2") Date date2,
                                           @Bind("id_third") Long id_third,
                                           @BindIn ("listStore") List<Long> listStore);



    @RegisterMapper(AuditoriaMapper.class)
    @SqlQuery(" select event_date fecha,cbi.fullname Empleado,MESSAGE,OLD_VALUE \"Valor Anterior\",NEW_VALUE \"Valor Cambio\"\n" +
            " from facturacion724.auditoria a,tercero724.third t,tercero724.common_basicinfo cbi\n" +
            " where a.id_third_employee=t.id_third and t.id_common_basicinfo=cbi.id_common_basicinfo\n" +
            "  and idstore=:id_store\n" +
            "  and event_date between trunc(:date1) and trunc(:date2)+1" +
            " order by 1 desc ")
    List <Auditoria> getAuditoria(@Bind("id_store") Long id_store,
                           @Bind("date1") Date date1,
                           @Bind("date2") Date date2);



    @SqlQuery("SELECT PCT_MARGEN_MINIMO_VENTA " +
            " FROM TIENDA724.PRODUCT_STORE WHERE ID_PRODUCT_STORE=:id_ps")
    Double getMargen(@Bind("id_ps") Long id_ps);

    @SqlCall(" call tienda724.update_product(:idps, :idproduct, :idcode, :idms, :idb, :idcat, :psname) ")
    void putNewProduct(@Bind("idps") Long idps,
                       @Bind("idproduct") Long idproduct,
                       @Bind("idcode") Long idcode,
                       @Bind("idms") Long idms,
                       @Bind("idb") Long idb,
                       @Bind("idcat") Long idcat,
                       @Bind("psname") String psname);


    @SqlCall(" call facturacion724.GENERAR_PEDIDOS_STOCK_MINIMO(:idstorep,:weekday) ")
    void GENERAR_PEDIDOS_STOCK_MINIMO(@Bind("idstorep") Long idstorep,
                       @Bind("weekday") Long weekday);


    @SqlUpdate("UPDATE tienda724.PRODUCT_STORE " +
            " SET PCT_MARGEN_MINIMO_VENTA = :margen " +
            " WHERE ID_PRODUCT_STORE = :id_ps ")
    void putMargen(@Bind("id_ps") Long id_ps,
                   @Bind("margen") Double margen);

    @SqlUpdate("UPDATE tienda724.PRODUCT_STORE " +
            " SET PRODUCT_STORE_NAME = :nombre " +
            " WHERE ID_PRODUCT_STORE = :id_ps ")
    void putNombre(@Bind("id_ps") Long id_ps,
                   @Bind("nombre") String nombre);


    @SqlUpdate("UPDATE tienda724.PRODUCT_STORE " +
            " SET PRODUCT_STORE_CODE = :code " +
            " WHERE ID_PRODUCT_STORE = :id_ps ")
    void putQuickCode(@Bind("id_ps") Long id_ps,
                      @Bind("code") String code);


    @SqlQuery(" select ID_PRODUCT_STORE from tienda724.PRODUCT_STORE where ID_STORE=:id_store and PRODUCT_STORE_CODE=:code ")
    List<Long> getListCodes(@Bind("id_store") Long id_store,
                     @Bind("code") String code);


    @SqlCall(" call facturacion724.registrar_auditoria_pos(1, " +
            " :nombre, :id_third_employee, :id_store, " +
            " :valor_anterior, :valor_nuevo) ")
    void auditoria(@Bind("nombre") String nombre,
                   @Bind("id_third_employee") Long id_third_employee,
                   @Bind("id_store") Long id_store,
                   @Bind("valor_anterior") String valor_anterior,
                   @Bind("valor_nuevo") String valor_nuevo);


    @SqlCall(" call tienda724.asociar_person(:idperson,:idstore,:idcaja) ")
    void asociarCajaStore(@Bind("idperson") Long idperson,
                          @Bind("idstore") Long idstore,
                          @Bind("idcaja") Long idcaja);


    @RegisterMapper(LabelReportMapper.class)
    @SqlQuery(" select sum(totalprice) as TOTAL " +
            " from facturacion724.bill b,facturacion724.detail_bill d " +
            " where b.id_bill=d.id_bill " +
            " and b.id_bill_state=1 and id_bill_type=:id_bill_type and d.id_product_third=:id_product_store and b.id_store in (<id_store>) and purchase_date between trunc(:date1) and trunc(:date2)+1 ")
    LabelReport getTotalByBillTypeProduct(@Bind("id_bill_type") Long id_bill_type,
                                          @Bind("id_product_store") Long id_product_store,
                                          @Bind("date1") Date date1,
                                          @Bind("date2") Date date2,
                                          @BindIn("id_store") List<Long> id_store);


    @RegisterMapper(CategoryReportMapper.class)
    @SqlQuery(" select clinea.name,t.name categoria,\n" +
            " round(sum(quantity*price*(1+d.tax/100))) venta,\n" +
            " round(sum(quantity*standard_price*(1+d.tax/100))) costo,\n" +
            " round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100))) utilidad,\n" +
            " round(( round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100)))  )*100/round(sum(quantity*price*(1+d.tax/100))) ,1) pct_margen,\n" +
            " sum(quantity) numventas\n" +
            " from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps,tienda724.codes co,tienda724.product pr,\n" +
            " tienda724.category cat,tienda724.common t,tienda724.category linea,tienda724.common clinea\n" +
            " where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=co.id_code and co.id_product=pr.id_product\n" +
            " and pr.id_category=cat.id_category and cat.id_common=t.id_common\n" +
            " and cat.id_category_father=linea.id_category and linea.id_common=clinea.id_common\n" +
            " and id_bill_type=1 and id_bill_state=1\n" +
            " and ps.id_store=:id_store\n" +
            " and b.purchase_date between TRUNC(:date1) and TRUNC(:date2)+1\n" +
            " group by clinea.name,t.name\n" +
            " order by 3 desc")
    List <CategoryReport> getTotalesFacturacionPorPeriodoCategorias(@Bind("id_third") Long id_third,
                                                     @Bind("id_store") Long id_store,
                                                     @Bind("date1") Date date1,
                                                     @Bind("date2") Date date2);
////////////////////////
    @RegisterMapper(ProductReportMapper.class)
    @SqlQuery("select ps.ownbarcode, ps.PRODUCT_STORE_NAME PRODUCT\n" +
            ",br.brand marca,cl.name linea,cc.name Categoria\n" +
            "                                     ,round(sum(quantity*price*(1+d.tax/100))) venta,\n" +
            "                                      round(sum(quantity*standard_price*(1+d.tax/100))) costo,\n" +
            "                                      round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100))) utilidad,\n" +
            "                                      ROUND(   (round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100))))*100/DECODE(    round(sum(  quantity*price*(1+d.tax/100) ) ,1)  ,0,1,\n" +
            "                                       round(  sum(quantity*price*(1+d.tax/100)) ,1)   )  ,1) pct_margen,\n" +
            "                                      sum(quantity) numventas\n" +
            "                                      from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps\n" +
            "                                      ,tienda724.codes c,tienda724.brand br,tienda724.product p\n" +
            "                                      ,tienda724.category linea,tienda724.category cat,tienda724.common cl,tienda724.common cc\n" +
            "                                      where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store\n" +
            "                                      and ps.id_code=c.id_code and c.id_brand=br.id_brand and c.id_product=p.id_product\n" +
            "                                      and p.id_category=cat.id_category and cat.id_category_father=linea.id_category and linea.id_category_father is null\n" +
            "                                      and linea.id_common=cl.id_common and cat.id_common=cc.id_common\n" +
            "                                      and id_bill_type=1 and id_bill_state=1 and ps.id_store=:id_store\n" +
            "                                      and b.purchase_date between TRUNC(:date1) and TRUNC(:date2)+1\n" +
            "                                     group by ps. ownbarcode, PRODUCT_STORE_NAME,br.brand,cl.name,cc.name\n" +
            "                                 order by 2 desc")
    List <CategoryReport> getTotalesFacturacionPorPeriodoProductos(@Bind("id_third") Long id_third,
                                                                    @Bind("id_store") Long id_store,
                                                                   @Bind("date1") Date date1,
                                                                   @Bind("date2") Date date2);


    @RegisterMapper(ProductReportMapper.class)
    @SqlQuery("select ps.ownbarcode, ps.PRODUCT_STORE_NAME product\n" +
            ",br.brand marca,cl.name linea,cc.name Categoria\n" +
            "                                     ,round(sum(quantity*price*(1+d.tax/100))) venta,\n" +
            "                                      round(sum(quantity*standard_price*(1+d.tax/100))) costo,\n" +
            "                                      round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100))) utilidad,\n" +
            "                                      ROUND(   (round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100))))*100/DECODE(    round(sum(  quantity*price*(1+d.tax/100) ) ,1)  ,0,1,\n" +
            "                                       round(  sum(quantity*price*(1+d.tax/100)) ,1)   )  ,1) pct_margen,\n" +
            "                                      sum(quantity) numventas\n" +
            "                                      from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps\n" +
            "                                      ,tienda724.codes c,tienda724.brand br,tienda724.product p\n" +
            "                                      ,tienda724.category linea,tienda724.category cat,tienda724.common cl,tienda724.common cc\n" +
            "                                      where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store\n" +
            "                                      and ps.id_code=c.id_code and c.id_brand=br.id_brand and c.id_product=p.id_product\n" +
            "                                      and p.id_category=cat.id_category and cat.id_category_father=linea.id_category and linea.id_category_father is null\n" +
            "                                      and linea.id_common=cl.id_common and cat.id_common=cc.id_common\n" +
            "                                      AND LINEA.ID_CATEGORY=:id_cat \n" +
            "                                      and id_bill_type=1 and id_bill_state=1 and ps.id_store=:id_store\n" +
            "                                      and b.purchase_date between TRUNC(:date1) and TRUNC(:date2)+1\n" +
            "                                     group by ps.ownbarcode, PRODUCT_STORE_NAME,br.brand,cl.name,cc.name\n" +
            "                                 order by 5 desc")
    List <CategoryReport> getTotalesFacturacionPorPeriodoProductosLinea(@Bind("id_cat") Long id_cat,
                                                                   @Bind("id_store") Long id_store,
                                                                        @Bind("date1") Date date1,
                                                                        @Bind("date2") Date date2);


    @RegisterMapper(ProductReportMapper.class)
    @SqlQuery("select\n" +
            "                ps.ownbarcode, ps.PRODUCT_STORE_NAME PRODUCT\n" +
            "                ,cl.name linea,cc.name Categoria,br.brand marca\n" +
            "                ,round(sum(quantity*price*(1+d.tax/100))) venta,\n" +
            "                round(sum(quantity*standard_price*(1+d.tax/100))) costo,\n" +
            "                round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100))) utilidad,\n" +
            "                ROUND(   (round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100))))*100/DECODE(    round(sum(  quantity*price*(1+d.tax/100) ) ,1)  ,0\n" +
            "                ,1,round(  sum(quantity*price*(1+d.tax/100)) ,1)   )  ,1) pct_margen,\n" +
            "                sum(quantity) numventas\n" +
            "               from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps\n" +
            "               ,tienda724.codes c,tienda724.brand br,tienda724.product p\n" +
            "               ,tienda724.category linea,tienda724.category cat,tienda724.common cl,tienda724.common cc\n" +
            "               where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store\n" +
            "              and ps.id_code=c.id_code and c.id_brand=br.id_brand and c.id_product=p.id_product\n" +
            "              and p.id_category=cat.id_category and cat.id_category_father=linea.id_category and linea.id_category_father is null\n" +
            "              and linea.id_common=cl.id_common and cat.id_common=cc.id_common and p.id_category=:id_cat\n" +
            "              and id_bill_type=1 and id_bill_state=1 and ps.id_store=:id_store\n" +
            "              and b.purchase_date between TRUNC(TO_DATE(:date1, 'YYYY/MM/DD')) and TRUNC(TO_DATE(:date2, 'YYYY/MM/DD'))+1\n" +
            "               group by ps. ownbarcode, PRODUCT_STORE_NAME,cl.name,cc.name,br.brand\n" +
            "               order by 2")
    List <CategoryReport> getTotalesFacturacionPorPeriodoProductosCate(@Bind("id_cat") Long id_third,
                                                                        @Bind("id_store") Long id_store,
                                                                        @Bind("date1") String date1,
                                                                        @Bind("date2") String date2);


    @RegisterMapper(ProductReportMapper.class)
    @SqlQuery("select\n" +
            "                ps.ownbarcode, ps.PRODUCT_STORE_NAME PRODUCT\n" +
            "                ,cl.name linea,cc.name Categoria,br.brand marca\n" +
            "            ,round(sum(quantity*price*(1+d.tax/100))) venta,\n" +
            "                round(sum(quantity*standard_price*(1+d.tax/100))) costo,\n" +
            "            round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100))) utilidad,\n" +
            "                ROUND(   (round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100))))*100/DECODE(    round(sum(  quantity*price*(1+d.tax/100) ) ,1)  ,0\n" +
            "                ,1,round(  sum(quantity*price*(1+d.tax/100)) ,1)   )  ,1) pct_margen,\n" +
            "                sum(quantity) numventas\n" +
            "            from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps\n" +
            "               ,tienda724.codes c,tienda724.brand br,tienda724.product p\n" +
            "               ,tienda724.category linea,tienda724.category cat,tienda724.common cl,tienda724.common cc\n" +
            "            where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store\n" +
            "              and ps.id_code=c.id_code and c.id_brand=br.id_brand and c.id_product=p.id_product\n" +
            "              and p.id_category=cat.id_category and cat.id_category_father=linea.id_category and linea.id_category_father is null\n" +
            "              and linea.id_common=cl.id_common and cat.id_common=cc.id_common \n" +
            "              AND BR.ID_BRAND=:id_brand and ps.id_store=:id_store\n" +
            "              and b.purchase_date between TRUNC(TO_DATE(:date1, 'YYYY/MM/DD')) and TRUNC(TO_DATE(:date2, 'YYYY/MM/DD'))+1\n" +
            "            group by ps.ownbarcode,PRODUCT_STORE_NAME,cl.name,cc.name,br.brand\n" +
            "            order by 2")
    List <CategoryReport> getTotalesFacturacionPorPeriodoProductosBrand(@Bind("id_brand") Long id_third,
                                                                       @Bind("id_store") Long id_store,
                                                                       @Bind("date1") String date1,
                                                                       @Bind("date2") String date2);
////////////////////////////

    @RegisterMapper(NewProductReportMapper.class)
    @SqlQuery(" select id_product_store,id_store,ps.id_code,c.code,product_store_name,p.id_product \n" +
            "from tienda724.product_store ps,tienda724.codes c,tienda724.product p\n" +
            "where ps.id_code=c.id_code and c.id_product=p.id_product \n" +
            "and c.id_measure_unit=223 and c.id_brand=4368 and p.id_category=1109 and id_store in (<listStore>)\n" +
            "order by id_store,product_store_name ")
    List <NewProductReport> getNewProducts(@BindIn("listStore") List<String> listStore);


    @RegisterMapper(CajaReportMapper.class)
    @SqlQuery(" select * from caja where ID_STORE=:id_store ")
    List <CajaReport> getAllboxes(@Bind("id_store") Long id_store);


    @RegisterMapper(PendingReportMapper.class)
    @SqlQuery(" select ps.OWNBARCODE, ps.PRODUCT_STORE_NAME, sum(db.QUANTITY) QUANTITY\n" +
            "             from facturacion724.BILL b,facturacion724.DETAIL_BILL db, TIENDA724.product_store ps\n" +
            "             where b.id_bill=db.id_bill and db.ID_PRODUCT_THIRD=ps.ID_PRODUCT_STORE  \n" +
            "             and ID_BILL_type=89 and id_bill_state=65 and b.id_store=:idstore and id_store_client in (<idstorec>)\n" +
            "             group by ps.OWNBARCODE, ps.PRODUCT_STORE_NAME\n" +
            "             order by 3 desc ")
    List <PendingReport> getPendingReport(@Bind("idstore") Long idstore,
                                          @BindIn("idstorec") List<String> idstorec);



    @RegisterMapper(StoresPendingMapper.class)
    @SqlQuery(" select pp.ID_STORE_CLIENT, s.DESCRIPTION from TIENDA724.PARAM_PEDIDOS pp, TIENDA724.STORE s\n" +
            "    where pp.ID_STORE_PROVIDER=:idstore\n" +
            "    and s.ID_STORE=pp.ID_STORE_CLIENT ")
    List <StoresPending> getPendingstores(@Bind("idstore") Long idstore);



    @RegisterMapper(BillReportMapper.class)
    @SqlQuery("SELECT round(sum(quantity*price*d.tax/100) ,2) tax, b.id_bill_state, b.num_documento, co.fullname,l.prefix_bill,b.purchase_date fecha,b.id_bill,b.consecutive,\n" +
            "       round(sum(quantity*price*(1+d.tax/100)) ,2) venta,\n" +
            "       round(sum(quantity*standard_price*(1+d.tax/100)) ,2) costo,\n" +
            "       round(sum(quantity*price*(1+d.tax/100)) ,2)-round(sum(quantity*standard_price*(1+d.tax/100)) ,2) utilidad,\n" +
            "       round(   ( round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100)))  )*100/(sum(quantity*decode(price,0,1,price)*(1+d.tax/100))) ,1) pct_margen_venta,\n" +
            "       round(( round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100)))  )*100/(sum(quantity*decode(standard_price,0,1,standard_price)*(1+d.tax/100))),1) pct_margen_costo,\n" +
            "       st.description tienda,ca.caja_number caja,cbi.fullname cajero\n" +
            "        ,CBIDOM.FULLNAME DOMICILIARIO\n" +
            "        ,dpb.id_payment_method TIPOFACTURA,id_mesa\n" +
            "from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps,tercero724.third t,tercero724.legal_data l,\n" +
            "    tercero724.common_basicinfo co,tienda724.caja ca,tienda724.store st,tercero724.third cajero,tercero724.common_basicinfo cbi\n" +
            "   ,facturacion724.detail_payment_bill dpb\n" +
            "   ,TERCERO724.THIRD THDOM,TERCERO724.COMMON_BASICINFO CBIDOM\n" +
            "where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store and b.id_third=t.id_third\n" +
            "  and t.id_common_basicinfo=co.id_common_basicinfo and t.id_legal_data=l.id_legal_data and b.id_caja=ca.id_caja(+)\n" +
            "  and b.id_store=st.id_store and b.id_third_employee=cajero.id_third and cajero.id_common_basicinfo=cbi.id_common_basicinfo\n" +
            "  AND b.id_third_domiciliario=THDOM.id_third and THDOM.ID_COMMON_BASICINFO=CBIDOM.ID_COMMON_BASICINFO\n" +
            "  and b.id_bill=dpb.id_bill\n" +
            "  and b.id_bill_type=:typemove and b.id_bill_state in (1) and b.id_store in (<id_store>)\n" +
            "  and b.purchase_date between TRUNC(:date1) and TRUNC(:date2)+1\n" +
            "  and dpb.id_payment_method in (1,2,3,4)\n" +
            "group by b.id_bill_state, b.num_documento, co.fullname,l.prefix_bill,b.purchase_date,b.id_bill,b.consecutive\n" +
            "       ,st.description,ca.caja_number,cbi.fullname\n" +
            "       ,CBIDOM.FULLNAME,dpb.id_payment_method\n" +
            "       ,id_mesa\n" +
            "UNION\n" +
            "select round(sum(quantity*price*d.tax/100) ,2) tax, b.id_bill_state, b.num_documento, co.fullname,l.prefix_bill,b.purchase_date fecha,b.id_bill,b.consecutive,\n" +
            "       round(sum(quantity*price*(1+d.tax/100)) ,2) venta,\n" +
            "       round(sum(quantity*standard_price*(1+d.tax/100)) ,2) costo,\n" +
            "       round(sum(quantity*price*(1+d.tax/100)) ,2)-round(sum(quantity*standard_price*(1+d.tax/100)) ,2) utilidad,\n" +
            "       round(( round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100)))  )*100/(sum(quantity*decode(price,0,1,price)*(1+d.tax/100))) ,1) pct_margen_venta,\n" +
            "       round(( round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100)))  )*100/(sum(quantity*decode(standard_price,0,1,standard_price)*(1+d.tax/100))),1) pct_margen_costo,\n" +
            "       st.description tienda,ca.caja_number caja,cbi.fullname cajero\n" +
            "        ,CBIDOM.FULLNAME DOMICILIARIO\n" +
            "        ,0 TIPOFACTURA ,id_mesa\n" +
            "from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps,tercero724.third t,tercero724.legal_data l,\n" +
            "     tercero724.common_basicinfo co,tienda724.caja ca,tienda724.store st,tercero724.third cajero,tercero724.common_basicinfo cbi\n" +
            "        ,TERCERO724.THIRD THDOM,TERCERO724.COMMON_BASICINFO CBIDOM\n" +
            "where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store and b.id_third=t.id_third\n" +
            "  and t.id_common_basicinfo=co.id_common_basicinfo and t.id_legal_data=l.id_legal_data and b.id_caja=ca.id_caja(+)\n" +
            "  and b.id_store=st.id_store and b.id_third_employee=cajero.id_third and cajero.id_common_basicinfo=cbi.id_common_basicinfo\n" +
            "  AND b.id_third_domiciliario=THDOM.id_third and THDOM.ID_COMMON_BASICINFO=CBIDOM.ID_COMMON_BASICINFO\n" +
            "  and b.id_bill_type=:typemove and b.id_bill_state IN (41) and b.id_store in (<id_store>)\n" +
            "  and b.purchase_date between TRUNC(:date1) and TRUNC(:date2)+1\n" +
            "group by b.id_bill_state, b.num_documento, co.fullname,l.prefix_bill,b.purchase_date,b.id_bill,b.consecutive,\n" +
            "         st.description,ca.caja_number,cbi.fullname\n" +
            "        ,CBIDOM.FULLNAME,id_mesa\n" +
            "order by id_bill_state,fecha desc\n")
    List <BillReport> getTotalesFacturacionPorPeriodo(@BindIn("id_store") List<String> id_store,
                                                      @Bind("date1") Date date1,
                                                      @Bind("date2") Date date2,
                                                      @Bind("typemove") Long typemove);


    @RegisterMapper(BillReportMapper.class)
    @SqlQuery(" SELECT round(sum(quantity*price*d.tax/100) ,2) tax, b.id_bill_state, b.num_documento, co.fullname,l.prefix_bill,b.purchase_date fecha,b.id_bill,b.consecutive,\n" +
            "           round(sum(quantity*price*(1+d.tax/100)) ,2) venta,\n" +
            "           round(sum(quantity*standard_price*(1+d.tax/100)) ,2) costo,\n" +
            "           round(sum(quantity*price*(1+d.tax/100)) ,2)-round(sum(quantity*standard_price*(1+d.tax/100)) ,2) utilidad,\n" +
            "           round(   ( round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100)))  )*100/(sum(quantity*decode(price,0,1,price)*(1+d.tax/100))) ,1) pct_margen_venta,\n" +
            "           round(( round(sum(quantity*price*(1+d.tax/100))) - round(sum(quantity*standard_price*(1+d.tax/100)))  )*100/(sum(quantity*decode(standard_price,0,1,standard_price)*(1+d.tax/100))),1) pct_margen_costo,\n" +
            "           st.description tienda,ca.caja_number caja,cbi.fullname cajero\n" +
            "         ,CBIDOM.FULLNAME DOMICILIARIO\n" +
            "         ,dpb.id_payment_method TIPOFACTURA,b.id_mesa\n" +
            "         from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps,tercero724.third t,tercero724.legal_data l,\n" +
            "         tercero724.common_basicinfo co,tienda724.caja ca,tienda724.store st,tercero724.third cajero,tercero724.common_basicinfo cbi\n" +
            "            ,facturacion724.detail_payment_bill dpb\n" +
            "            ,TERCERO724.THIRD THDOM,TERCERO724.COMMON_BASICINFO CBIDOM\n" +
            "         where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store and b.id_third=t.id_third\n" +
            "           and t.id_common_basicinfo=co.id_common_basicinfo and t.id_legal_data=l.id_legal_data and b.id_caja=ca.id_caja(+)\n" +
            "           and b.id_store=st.id_store and b.id_third_employee=cajero.id_third and cajero.id_common_basicinfo=cbi.id_common_basicinfo\n" +
            "           AND b.id_third_domiciliario=THDOM.id_third and THDOM.ID_COMMON_BASICINFO=CBIDOM.ID_COMMON_BASICINFO\n" +
            "           and b.id_bill=dpb.id_bill\n" +
            "           and b.id_bill_type=:typemove and b.id_bill_state in (1) and b.id_store=:id_store\n" +
            "           and b.purchase_date between TRUNC(:date1) and TRUNC(:date2)+1\n" +
            "           and dpb.id_payment_method in (1,2,3,4)\n" +
            "         group by b.id_bill_state, b.num_documento, co.fullname,l.prefix_bill,b.purchase_date,b.id_bill,b.consecutive\n" +
            "           ,st.description,ca.caja_number,cbi.fullname\n" +
            "           ,CBIDOM.FULLNAME,dpb.id_payment_method,b.id_mesa\n" +
            "         order by id_bill_state,fecha desc")
    List <BillReport> getTotalesFacturacionPorPeriodo2(@Bind("id_store") Long id_store,
                                                      @Bind("date1") Date date1,
                                                      @Bind("date2") Date date2,
                                                      @Bind("typemove") Long typemove);


    @RegisterMapper(PriceMapper.class)
    @SqlQuery(" select price_description, price from price where ID_PRODUCT_STORE= :id_ps ")
    List <Price> getPriceByPS(@Bind("id_ps") Long id_ps);

    @RegisterMapper(InvetoryReportMapper.class)
    @SqlQuery("select distinct ownbarcode barcode,product_store_code CodigoTienda,com.name linea,co.name categoria,pr.provider fabricante,b.brand marca,ps.product_store_name producto,psi.quantity cantidad,tt.percent iva,ps.standard_price costo,price precio,\n" +
            "               quantity*standard_price costototal, round(quantity*standard_price*100/decode(:response,0,1,:response),2) pct_inventario\n" +
            "         from tienda724.common co,tienda724.category cat,tienda724.common com,tienda724.category linea,tienda724.product p,tienda724.codes c,\n" +
            "             tienda724.product_store ps,tienda724.product_store_inventory psi,tienda724.brand b,tienda724.measure_unit mu,tienda724.common comu,tienda724.price p,\n" +
            "             tienda724.provider pr,tienda724.tax_tariff tt\n" +
            "         where co.id_common=cat.id_common and cat.id_category=p.id_category and com.id_common=linea.id_common and linea.id_category=cat.id_category_father\n" +
            "          and p.id_product=c.id_product and c.id_code=ps.id_code and ps.id_product_store=psi.id_product_store and c.id_brand=b.id_brand\n" +
            "          and c.id_measure_unit=mu.id_measure_unit and mu.id_common=comu.id_common and ps.id_product_store=p.id_product_store\n" +
            "          and b.id_provider=pr.id_provider and ps.id_tax=tt.id_tax_tariff\n" +
            "          and cat.id_category_father is not null\n" +
            "          and ps.id_store in (<listStore>)\n" +
            "          and linea.id_category in(<listLine>) and cat.id_category in(<listCategory>)\n" +
            "          and (   b.id_brand in(<listBrand>) or  b.id_brand in(<listBrand2>))\n" +
            "           and ps.STATUS='ACTIVO'\n" +
            "         order by producto " )
    List <InvetoryReport> getInventarioCons(@Bind("response") Long response,
                                            @BindIn("listStore") List<Long> listStore,
                                            @BindIn("listLine") List<Long> listLine,
                                            @BindIn("listCategory") List<Long> listCategory,
                                            @BindIn("listBrand") List<Long> listBrand1,
                                            @BindIn("listBrand2") List<Long> listBrand2);

    @SqlQuery(" select sum(quantity*standard_price) costototal from tienda724.product_store ps,tienda724.product_store_inventory psi\n" +
            " where ps.id_product_store=psi.id_product_store and id_store in (<listStore>) ")
    Long get5Mil(@BindIn("listStore") List<Long> listStore);


    @RegisterMapper(DiasRotacionMapper.class)
    @SqlQuery(" select com.name linea,co.name categoria,b.brand marca,ps.product_store_name producto\n" +
              "     ,psi.quantity cantidadactual,sum(db.quantity) cantidadvendida,ROUND(psi.quantity/(sum(db.quantity)/:days),1) DIAS_ROTACION\n" +
              " from facturacion724.bill b,facturacion724.detail_bill db,\n" +
              "     tienda724.common co,tienda724.category cat,tienda724.common com,tienda724.category linea,tienda724.product p,tienda724.codes c,\n" +
              "     tienda724.product_store ps,tienda724.product_store_inventory psi,tienda724.brand b,tienda724.measure_unit mu,tienda724.common comu\n" +
              " where b.id_bill(+)=db.id_bill and db.id_product_third(+)=ps.id_product_store and  co.id_common=cat.id_common and cat.id_category=p.id_category and com.id_common=linea.id_common and linea.id_category=cat.id_category_father\n" +
              "  and p.id_product=c.id_product and c.id_code=ps.id_code and ps.id_product_store=psi.id_product_store and c.id_brand=b.id_brand\n" +
              "  and c.id_measure_unit=mu.id_measure_unit and mu.id_common=comu.id_common and cat.id_category_father is not null\n" +
              "  and ps.id_store in (<listStore>) and linea.id_category in(<listLine>) and cat.id_category in(<listCategory>)\n" +
              "  and b.id_brand in(<listBrand>)\n" +
              "  and b.purchase_date between sysdate-:days and sysdate\n" +
              "  and b.id_bill_type=1 and b.id_bill_state=1\n" +
              " group by com.name,co.name,b.brand,ps.product_store_name, psi.quantity \n" +
              " order by DIAS_ROTACION desc ")
    List <DiasRotacion> getDiasRotacion(@Bind("days") Long days,
                                        @BindIn("listStore") List<Long> listStore,
                                        @BindIn("listLine") List<Long> listLine,
                                        @BindIn("listCategory") List<Long> listCategory,
                                        @BindIn("listBrand") List<Long> listBrand);

    @RegisterMapper(Brand2Mapper.class)
    @SqlQuery(" select ID_BRAND,BRAND from tienda724.BRAND ")
    List <Brand> getBrands();


    @SqlQuery(" select a.id_category from tienda724.category a where a.id_category_father in (select b.id_category from tienda724.category_tipostore b where b.ID_TIPO_STORE in (<listStore>)) order by 1 ")
    List <Long> getSonCat(@BindIn("listStore") List<Long> listStore);


    @SqlQuery(" select quantity from facturacion724.detail_bill where id_bill=:idbill and \n" +
            " id_product_third in (select id_product_store from tienda724.product_store where ownbarcode=:code and id_store in (<listStore>)) ")
    Long getNoDisponibles(@Bind("idbill") Long idbill,
                          @BindIn("listStore") List<Long> listStore,
                          @Bind("code") String code);



    @SqlQuery(" select iva from tienda724.store where id_store=:idstore  ")
    String getIvaStore(@Bind("idstore") Long idstore);

    @RegisterMapper(NoDispMapper.class)
    @SqlQuery(" select ownbarcode,nvl(sum(db.quantity),0) no_disponible\n" +
            "from facturacion724.detail_bill db, tienda724.product_store ps\n" +
            "where db.id_product_third=ps.id_product_store and ps.id_store in (<listStore>)\n" +
            "  and id_bill in (-4001,-4002,-4003,-4004,-4005)\n" +
            "group by ownbarcode ")
    List <NoDisp> getSumNoDisponibles(@BindIn("listStore") List<Long> listStore);


    @RegisterMapper(DataMapper.class)
    @SqlQuery(" select s.description nombretienda,cbi.fullname nombrepropietario,ld.url_logo,city.city_name ciudad,d.address direccion,p.phone telefono\n" +
            "         ,m.mail email,d.latitud,d.longitud,re.id_country\n" +
            "        from tienda724.store s,tercero724.third t,tercero724.legal_data ld,tercero724.common_basicinfo cbi,\n" +
            "              tercero724.directory d,tercero724.mail m,tercero724.phone p,tercero724.city city,tercero724.state st,TERCERO724.REGION re\n" +
            "        where s.id_third=t.id_third and t.id_legal_data=ld.id_legal_data and t.id_common_basicinfo=cbi.id_common_basicinfo\n" +
            "          and s.id_directory=d.id_directory and d.id_directory=m.ID_DIRECTORY and d.id_directory=p.ID_DIRECTORY\n" +
            "          and d.id_city=city.id_city\n" +
            "          and city.ID_STATE=st.ID_STATE and st.ID_REGION=re.ID_REGION\n" +
            "          and s.id_store=:id_store ")
    List<Data> getData(@Bind("id_store") Long id_store);

    @RegisterMapper(AdminUserMapper.class)
    @SqlQuery(" select u.\"usuario\",u.\"clave\" " +
            "from tienda724.store s,tercero724.third t,tercero724.third th,auth2.\"usuario\" u, " +
            "auth2.\"usuario_roles_rol\" r " +
            "where s.id_third=t.id_third and t.id_third=th.id_third_father " +
            "and th.id_third=u.\"id_third\" and u.\"uuid\"=r.\"usuarioUuid\" " +
            "and id_store=:id_store and r.\"rolId\"= :rol ")
    List<AdminUser> getAdminUserList(@Bind("rol") Long rol,
                          @Bind("id_store") Long id_store);

}


 