package com.name.business.entities;

public class Vendor {

    public Long getIDTHIRDVENDEDOR() {
        return IDTHIRDVENDEDOR;
    }

    public void setIDTHIRDVENDEDOR(Long IDTHIRDVENDEDOR) {
        this.IDTHIRDVENDEDOR = IDTHIRDVENDEDOR;
    }

    public String getVENDEDOR() {
        return VENDEDOR;
    }

    public void setVENDEDOR(String VENDEDOR) {
        this.VENDEDOR = VENDEDOR;
    }

    private Long IDTHIRDVENDEDOR;
    private String VENDEDOR;

    public Vendor(Long IDTHIRDVENDEDOR, String VENDEDOR) {
        this.IDTHIRDVENDEDOR = IDTHIRDVENDEDOR;
        this.VENDEDOR = VENDEDOR;
    }




}
