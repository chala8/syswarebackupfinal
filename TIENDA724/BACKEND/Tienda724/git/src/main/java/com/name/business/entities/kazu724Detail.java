package com.name.business.entities;

public class kazu724Detail {

    private Long ID_DOCUMENT_DETAIL;
    private Long CODIGO_CUENTA;
    private String DESCRIPCION;
    private Long VALOR;
    private String NATURALEZA;
    private String NOTAS;
    private Long ID_THIRD;
    private String FULLNAME;

    public kazu724Detail(Long ID_DOCUMENT_DETAIL,
                         Long CODIGO_CUENTA,
                         String DESCRIPCION,
                         Long VALOR,
                         String NATURALEZA,
                         String NOTAS,
                         Long ID_THIRD,
                         String FULLNAME) {
        this.FULLNAME = FULLNAME;
        this.ID_THIRD = ID_THIRD;
        this.ID_DOCUMENT_DETAIL = ID_DOCUMENT_DETAIL;
        this.CODIGO_CUENTA = CODIGO_CUENTA;
        this.DESCRIPCION = DESCRIPCION;
        this.VALOR = VALOR;
        this.NATURALEZA = NATURALEZA;
        this.NOTAS =  NOTAS;
    }

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }

    public Long getID_THIRD() {
        return ID_THIRD;
    }

    public void setID_THIRD(Long ID_THIRD) {
        this.ID_THIRD = ID_THIRD;
    }

    public Long getID_DOCUMENT_DETAIL() {
        return ID_DOCUMENT_DETAIL;
    }
    public void setID_DOCUMENT_DETAIL(Long ID_DOCUMENT_DETAIL) {
        this.ID_DOCUMENT_DETAIL = ID_DOCUMENT_DETAIL;
    }

    public Long getCODIGO_CUENTA() {
        return CODIGO_CUENTA;
    }
    public void setCODIGO_CUENTA(Long CODIGO_CUENTA) {
        this.CODIGO_CUENTA = CODIGO_CUENTA;
    }

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }
    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }

    public Long getVALOR() {
        return VALOR;
    }
    public void setVALOR(Long VALOR) {
        this.VALOR = VALOR;
    }

    public String getNATURALEZA() {
        return NATURALEZA;
    }
    public void setNATURALEZA(String NATURALEZA) {
        this.NATURALEZA = NATURALEZA;
    }

    public String getNOTAS() {
        return NOTAS;
    }
    public void setNOTAS(String NOTAS) {
        this.NOTAS = NOTAS;
    }


}
