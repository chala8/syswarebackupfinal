package com.name.business.entities;

public class Storage2Name {

    private Long ID_STORAGE;
    private String DESCRIPTION;
    private Long STORAGE_NUMBER;



    public Storage2Name(Long ID_STORAGE, String DESCRIPTION, Long STORAGE_NUMBER) {
        this.ID_STORAGE = ID_STORAGE;
        this.DESCRIPTION = DESCRIPTION;
        this.STORAGE_NUMBER =  STORAGE_NUMBER;
    }

    //-------------------------------------------------------------------------------------------

    public Long getID_STORAGE() {
        return ID_STORAGE;
    }

    public void setID_STORAGE(Long ID_STORAGE) {
        this.ID_STORAGE = ID_STORAGE;
    }

    //-------------------------------------------------------------------------------------------

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    //-------------------------------------------------------------------------------------------

    public Long getSTORAGE_NUMBER() {
        return STORAGE_NUMBER;
    }

    public void setSTORAGE_NUMBER(Long STORAGE_NUMBER) {
        this.STORAGE_NUMBER = STORAGE_NUMBER;
    }

    //-------------------------------------------------------------------------------------------





}
