package com.name.business.mappers;

import com.name.business.entities.CategoryName;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryNameMapper implements ResultSetMapper<CategoryName>{

    @Override
    public CategoryName map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CategoryName(
                resultSet.getLong("ID_CATEGORY"),
                resultSet.getString("CATEGORY"),
                resultSet.getString("IMG_URL")

        );
    }
}