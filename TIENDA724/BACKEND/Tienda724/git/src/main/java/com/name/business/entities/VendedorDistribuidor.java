package com.name.business.entities;

public class VendedorDistribuidor {

    public Long getID_THIRD_VENDEDOR() {
        return ID_THIRD_VENDEDOR;
    }

    public void setID_THIRD_VENDEDOR(Long ID_THIRD_VENDEDOR) {
        this.ID_THIRD_VENDEDOR = ID_THIRD_VENDEDOR;
    }

    public String getVENDEDOR() {
        return VENDEDOR;
    }

    public void setVENDEDOR(String VENDEDOR) {
        this.VENDEDOR = VENDEDOR;
    }


    public Long getID_THIRD_PROVEEDOR() {
        return ID_THIRD_PROVEEDOR;
    }

    public void setID_THIRD_PROVEEDOR(Long ID_THIRD_PROVEEDOR) {
        this.ID_THIRD_PROVEEDOR = ID_THIRD_PROVEEDOR;
    }

    public String getPROVEEDOR() {
        return PROVEEDOR;
    }

    public void setPROVEEDOR(String PROVEEDOR) {
        this.PROVEEDOR = PROVEEDOR;
    }

    public Long getID_STORE_PROVEEDOR() {
        return ID_STORE_PROVEEDOR;
    }

    public void setID_STORE_PROVEEDOR(Long ID_STORE_PROVEEDOR) {
        this.ID_STORE_PROVEEDOR = ID_STORE_PROVEEDOR;
    }

    private Long ID_THIRD_VENDEDOR;
    private String VENDEDOR;
    private Long ID_THIRD_PROVEEDOR;
    private String PROVEEDOR;
    private Long ID_STORE_PROVEEDOR;

    public VendedorDistribuidor(Long ID_THIRD_VENDEDOR,
                                String VENDEDOR,
                                Long ID_THIRD_PROVEEDOR,
                                String PROVEEDOR,
                                Long ID_STORE_PROVEEDOR) {
        this.ID_THIRD_VENDEDOR = ID_THIRD_VENDEDOR;
        this.VENDEDOR = VENDEDOR;
        this.ID_THIRD_PROVEEDOR = ID_THIRD_PROVEEDOR;
        this.PROVEEDOR = PROVEEDOR;
        this.ID_STORE_PROVEEDOR = ID_STORE_PROVEEDOR;

    }




}
