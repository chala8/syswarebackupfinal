package com.name.business.mappers;

import com.name.business.entities.Phone;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PhoneMapper implements ResultSetMapper<Phone>{

    @Override
    public Phone map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Phone(
                resultSet.getLong("ID_PHONE"),
                resultSet.getLong("PHONE"),
                resultSet.getLong("PRIORITY")

        );
    }
}