package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.representations.PlanillaDTO;
import com.name.business.representations.PlanillaDetailDTO;
import java.util.List;

public class PlanillaPdfDTO {

    private String logo;
    private PlanillaDTO  master;
    private List<PlanillaDetailDTO>  detalles;

    @JsonCreator
    public PlanillaPdfDTO(@JsonProperty("logo")String logo,
                          @JsonProperty("master") PlanillaDTO  master,
                          @JsonProperty("detalles") List<PlanillaDetailDTO>  detalles) {
        this.logo = logo;
        this.master = master;
        this.detalles = detalles;
    }

    //------------------------------------------------------------------------------------------------

    public String  getlogo() {
        return logo;
    }

    public void setlogo(String  logo) {
        this.logo = logo;
    }

    //------------------------------------------------------------------------------------------------

    //------------------------------------------------------------------------------------------------

    public List<PlanillaDetailDTO>  getdetalles() {
        return detalles;
    }

    public void setdetalles(List<PlanillaDetailDTO>  detalles) {
        this.detalles = detalles;
    }

    //------------------------------------------------------------------------------------------------

    //------------------------------------------------------------------------------------------------

    public PlanillaDTO  getmaster() {
        return master;
    }

    public void setmaster(PlanillaDTO master) {
        this.master = master;
    }

    //------------------------------------------------------------------------------------------------



}
