package com.name.business.mappers;


import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.InventoryName;
import com.name.business.entities.InventoryNameActivos;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InventoryNameActivosMapper implements ResultSetMapper<InventoryNameActivos> {
    @Override
    public InventoryNameActivos map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new InventoryNameActivos(
                resultSet.getLong("ID_TAX"),
                resultSet.getDouble("STANDARD_PRICE"),
                resultSet.getLong("ID_PRODUCT_STORE"),
                resultSet.getString("PRODUCT_STORE_NAME"),
                resultSet.getString("CODE"),
                resultSet.getLong("ID_INVENTORY_DETAIL"),
                resultSet.getString("OWNBARCODE"),
                resultSet.getString("PRODUCT_STORE_CODE"),
                resultSet.getString("STATUS"),
                resultSet.getLong("QUANTITY"),
                resultSet.getString("INVENTARIO_DISPONIBLE"),
                resultSet.getLong("ID_CATEGORY"),
                resultSet.getString("IMG")
        );
    }
}
