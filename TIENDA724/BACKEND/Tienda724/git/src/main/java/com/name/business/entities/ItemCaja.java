package com.name.business.entities;

import java.util.Date;

public class ItemCaja {

    private Long ID_CAJA;
    private Long ID_STORE;



    public ItemCaja(Long ID_CAJA,
                    Long ID_STORE) {
        this.ID_CAJA = ID_CAJA;
        this.ID_STORE = ID_STORE;
    }

    //------------------------------------------------------------------------------------------------

    public Long getID_CAJA() {
        return ID_CAJA;
    }

    public void setID_CAJA(Long ID_CAJA) {
        this.ID_CAJA = ID_CAJA;
    }

    //------------------------------------------------------------------------------------------------

    public Long getID_STORE() {
        return ID_STORE;
    }

    public void setID_STORE(Long ID_STORE) {
        this.ID_STORE = ID_STORE;
    }

    //------------------------------------------------------------------------------------------------


}