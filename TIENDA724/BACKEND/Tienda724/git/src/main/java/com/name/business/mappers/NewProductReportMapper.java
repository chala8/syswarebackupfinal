package com.name.business.mappers;


import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.NewProductReport;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NewProductReportMapper implements ResultSetMapper<NewProductReport> {
    @Override
    public NewProductReport map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new NewProductReport(
                resultSet.getLong("ID_PRODUCT_STORE"),
                resultSet.getLong("ID_STORE"),
                resultSet.getLong("ID_CODE"),
                resultSet.getString("CODE"),
                resultSet.getString("PRODUCT_STORE_NAME"),
                resultSet.getLong("ID_PRODUCT")
        );
    }
}
