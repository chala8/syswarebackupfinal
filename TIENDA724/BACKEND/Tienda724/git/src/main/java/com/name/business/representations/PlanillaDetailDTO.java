package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;
import java.util.List;

public class PlanillaDetailDTO {


    private String store;
    private String num_DOCUMENTO;
    private String valor;
    private String address;

    @JsonCreator
    public PlanillaDetailDTO(@JsonProperty("store") String store,
                       @JsonProperty("num_DOCUMENTO") String num_DOCUMENTO,
                       @JsonProperty("valor") String valor,
                       @JsonProperty("address") String address) {
        this.store = store;
        this.num_DOCUMENTO = num_DOCUMENTO;
        this.valor = valor;
        this.address = address;
    }
    //------------------------------------------------------------------------------------------------
    public String getstore() {
        return store;
    }
    public void setstore(String store) {
        this.store = store;
    }
    //------------------------------------------------------------------------------------------------
    public String getnum_DOCUMENTO() {
        return num_DOCUMENTO;
    }
    public void setnum_DOCUMENTO(String num_DOCUMENTO) {
        this.num_DOCUMENTO = num_DOCUMENTO;
    }
    //------------------------------------------------------------------------------------------------
    public String getvalor() {
        return valor;
    }
    public void setvalor(String valor) {
        this.valor = valor;
    }
    //------------------------------------------------------------------------------------------------
    public String getaddress() {
        return address;
    }
    public void setaddress(String address) {
        this.address = address;
    }
    //------------------------------------------------------------------------------------------------



}
