package com.name.business.entities;

public class Auditoria {

    private String FECHA;
    private String EMPLEADO;
    private String MESSAGE;
    private String VALOR_ANTERIOR;
    private String VALOR_CAMBIO;


    public Auditoria(String FECHA,
             String EMPLEADO,
             String MESSAGE,
                     String VALOR_ANTERIOR,
                     String VALOR_CAMBIO) {
        this.FECHA = FECHA;
        this.EMPLEADO = EMPLEADO;
        this.MESSAGE = MESSAGE;
        this.VALOR_ANTERIOR = VALOR_ANTERIOR;
        this.VALOR_CAMBIO = VALOR_CAMBIO;
    }

    public String getFECHA() {
        return FECHA;
    }

    public void setFECHA(String FECHA) {
        this.FECHA = FECHA;
    }

    public String getEMPLEADO() {
        return EMPLEADO;
    }

    public void setEMPLEADO(String EMPLEADO) {
        this.EMPLEADO = EMPLEADO;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public String getVALOR_ANTERIOR() {
        return VALOR_ANTERIOR;
    }

    public void setVALOR_ANTERIOR(String VALOR_ANTERIOR) {
        this.VALOR_ANTERIOR = VALOR_ANTERIOR;
    }

    public String getVALOR_CAMBIO() {
        return VALOR_CAMBIO;
    }

    public void setVALOR_CAMBIO(String VALOR_CAMBIO) {
        this.VALOR_CAMBIO = VALOR_CAMBIO;
    }

}
