package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.PriceListBusiness;
import com.name.business.entities.*;
import com.name.business.representations.PriceListDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/price-list")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PriceListResource {
    private PriceListBusiness priceListBusiness;

    public PriceListResource(PriceListBusiness priceListBusiness) {
        this.priceListBusiness = priceListBusiness;
    }



    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getTaxTariff(
                    @QueryParam("id_price_list") Long id_price_list,
                    @QueryParam("id_product_third_pr_list") Long id_product_third_pr_list,
                    @QueryParam("standard_price") Double standard_price,
                    @QueryParam("min_price") Double min_price,

                    @QueryParam("id_common_pr_list") Long id_common_pr_list,
                    @QueryParam("name_pr_list") String name_pr_list,
                    @QueryParam("description_pr_list") String description_pr_list,
                    @QueryParam("id_state_pr_list") Long id_state_pr_list,
                    @QueryParam("state_pr_list") Integer state_pr_list,
                    @QueryParam("creation_pr_list") Date creation_pr_list,
                    @QueryParam("modify_pr_list") Date modify_pr_list)

    {
        Response response;

        Either<IException, List<PriceList>> getProducts = priceListBusiness.getPriceList(
                new PriceList( id_price_list,  id_product_third_pr_list,  standard_price, min_price,
                        new Common(
                                id_common_pr_list,
                                name_pr_list,
                                description_pr_list
                        ),
                        new CommonState(id_state_pr_list, state_pr_list,
                                creation_pr_list, modify_pr_list)
                )
        );

        if (getProducts.isRight()){
            //System.out.println(getProducts.right().value().size());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }

    @Path("/{id}")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deletePersonResource(@PathParam("id") Long id_product){
        Response response;
        Either<IException, Long> allViewOffertsEither = priceListBusiness.deleteProduct(id_product);

        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }


    @Path("/priceList")
    @GET
    @Timed
    @RolesAllowed({"Auth"})
    public Response getPriceList(@QueryParam("idstore") Long idstore){
        Response response;
        Either<IException, List<Price2>> allViewOffertsEither = priceListBusiness.getPriceList2(idstore);

        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }



    @Path("/{id}")
    @PUT
    @Timed
    @RolesAllowed({"Auth"})
    public Response updateProductResource(@PathParam("id") Long id_product, PriceListDTO priceListDTO){
        Response response;
        Either<IException, Long> allViewOffertsEither = priceListBusiness.updateProduct(id_product, priceListDTO);

        if (allViewOffertsEither.isRight()){
            //System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    /**
     *
     * @param priceListDTO
     * @return status
     */
    @POST
    @Timed
    @RolesAllowed({"Auth"})
    public Response postProductResource(PriceListDTO priceListDTO){
        Response response;

        Either<IException, Long> mailEither = priceListBusiness.createProduct(priceListDTO);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }

    @Path("/delete")
    @DELETE
    @Timed
    @RolesAllowed({"Auth"})
    public Response deletePrice(@QueryParam("idps") Long idps, @QueryParam("price") String price){
        Response response;

        Either<IException, Long> mailEither = priceListBusiness.deletePrice(idps,price);


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



}
