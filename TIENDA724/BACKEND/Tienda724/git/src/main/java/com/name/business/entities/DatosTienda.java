package com.name.business.entities;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DatosTienda {

    public Long getId_store_cliente() {
        return id_store_cliente;
    }

    public void setId_store_cliente(Long id_store_cliente) {
        this.id_store_cliente = id_store_cliente;
    }

    public String getTienda() {
        return tienda;
    }

    public void setTienda(String tienda) {
        this.tienda = tienda;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Long getId_third() {
        return id_third;
    }

    public void setId_third(Long id_third) {
        this.id_third = id_third;
    }

    private Long id_third;
    private  Long id_store_cliente;
    private  String tienda;
    private  String cliente;

    public DatosTienda(Long id_third, Long id_store_cliente,String tienda,String cliente) {
        this.id_third = id_third;
        this.id_store_cliente = id_store_cliente;
        this.tienda = tienda;
        this.cliente = cliente;
    }


}
