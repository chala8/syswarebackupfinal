package com.name.business.resources;
import java.time.LocalDate;
import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.businesses.MeasureUnitNameBusiness;
import com.name.business.businesses.ReportsBusiness;
import com.name.business.entities.*;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import java.util.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;


@Path("/resource")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ReportsResources {
    private ReportsBusiness reportsBusiness;

    public ReportsResources(ReportsBusiness reportsBusiness) {
        this.reportsBusiness = reportsBusiness;
    }

    @GET
    @Timed
    public Response getBillTotal(@QueryParam("id_bill_type") Long id_bill_type,
                                 @QueryParam("id_period") Long id_period,
                                 @QueryParam("date1") String date1,
                                 @QueryParam("date2") String date2,
                                 @QueryParam("id_third") Long id_third,
                                 @QueryParam("id_store")  List<Long> listStore) {

        Response response;

        Either<IException, List <DateReport>> getProductsEither = reportsBusiness.getBillTotal(
                id_bill_type, id_period, new Date(date1), new Date(date2), id_third,listStore
        );
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/bycategory")
    @Timed
    public Response getTotalByBillTypeCategory(@QueryParam("id_bill_type") Long id_bill_type,
                                               @QueryParam("id_category") Long id_category,
                                               @QueryParam("id_period") Long id_period,
                                               @QueryParam("date1") String date1,
                                               @QueryParam("date2") String date2,
                                               @QueryParam("id_third") Long id_third,
                                               @QueryParam("id_store") List<Long> id_store) {

        Response response;

        Either<IException, List <LabelReport>> getProductsEither = reportsBusiness.getTotalByBillTypeCategory(
                id_bill_type, id_category, id_period, new Date(date1), new Date(date2), id_third, id_store
        );
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/reportcategory")
    @Timed
    public Response getTotalByBillTypeCategory(@QueryParam("id_third") Long id_third,
                                               @QueryParam("id_store") Long id_store,
                                               @QueryParam("date1") String date1,
                                               @QueryParam("date2") String date2) {

        Response response;

        Either<IException, List <CategoryReport>> getProductsEither = reportsBusiness.getTotalesFacturacionPorPeriodoCategorias(
                id_third, id_store, new Date(date1), new Date(date2) );
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/reportproduct")
    @Timed
    public Response getTotalesFacturacionPorPeriodoProductos(@QueryParam("id_third") Long id_third,
                                               @QueryParam("id_store") Long id_store,
                                               @QueryParam("date1") String date1,
                                               @QueryParam("date2") String date2) {

        Response response;

        Either<IException, List <CategoryReport>> getProductsEither = reportsBusiness.getTotalesFacturacionPorPeriodoProductos(
                id_third, id_store, new Date(date1), new Date(date2) );
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/reportbill")
    @Timed
    public Response getTotalesFacturacionPorPeriodo(@QueryParam("id_third") Long id_third,
                                                    @QueryParam("id_store") Long id_store,
                                                    @QueryParam("date1") String date1,
                                                    @QueryParam("date2") String date2,
                                                    @QueryParam("typemove") Long typemove) {

        Response response;

        Either<IException,List <BillReport>> getProductsEither = reportsBusiness.getTotalesFacturacionPorPeriodo(
                id_third, id_store, date1, date2,typemove );
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @GET
    @Path("/byproduct")
    @Timed
    public Response getTotalByBillTypeProduct(@QueryParam("id_bill_type") Long id_bill_type,
                                               @QueryParam("id_product_store") Long id_product_store,
                                               @QueryParam("id_period") Long id_period,
                                               @QueryParam("date1") String date1,
                                               @QueryParam("date2") String date2,
                                              @QueryParam("id_store") List<Long> id_store) {

        Response response;

        Either<IException, List <LabelReport>> getProductsEither = reportsBusiness.getTotalByBillTypeProduct(
                id_bill_type, id_product_store, id_period, new Date(date1), new Date(date2),id_store
        );
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/category")
    @Timed
    public Response getTotalByCategory(@QueryParam("id_bill_type") Long id_bill_type,
                                 @QueryParam("date1") String date1,
                                 @QueryParam("date2") String date2) {
        Response response;

        Either<IException, DateReport2> getProductsEither = reportsBusiness.getTotalByCategory(
                id_bill_type, new Date(date1), new Date(date2)
        );
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }
    @GET
    @Path("/brand")
    @Timed
    public Response getTotalByBrand(@QueryParam("id_bill_type") Long id_bill_type,
                                 @QueryParam("date1") String date1,
                                 @QueryParam("date2") String date2) {
        Response response;

        Either<IException, DateReport2> getProductsEither = reportsBusiness.getTotalByBrand(
                id_bill_type, new Date(date1), new Date(date2)
        );
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }
    @GET
    @Path("/product")
    @Timed
    public Response getTotalByProduct(@QueryParam("id_bill_type") Long id_bill_type,
                                 @QueryParam("date1") String date1,
                                 @QueryParam("date2") String date2,
                                      @QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List <DateReport2>> getProductsEither = reportsBusiness.getTotalByProduct(
                id_bill_type, new Date(date1), new Date(date2), id_store
        );
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/pdf")
    @Timed
    public Response postCategoryResource(){
        Response response;

        Either<IException, String> mailEither = reportsBusiness.printPDF();


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }


    @GET
    @Path("/pricebyps")
    @Timed
    public Response getPriceByPS(@QueryParam("id_ps") Long id_ps) {

        Response response;

        Either<IException, List <Price>> getProductsEither = reportsBusiness.getPriceByPS(id_ps
        );
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/inventory")
    @Timed
    public Response getInventarioCons(InventoryReportDTO inventorydata) {

        Response response;

        Either<IException, List <InvetoryReport>> getProductsEither = reportsBusiness.getInventarioCons(
                inventorydata.getlistStore(),inventorydata.getlistLine(),inventorydata.getlistCategory(),inventorydata.getlistBrand());
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/rotacion")
    @Timed
    public Response getDiasRotacion(InventoryReportDTO inventorydata) {

        Response response;

        Either<IException, List <DiasRotacion>> getProductsEither = reportsBusiness.getDiasRotacion(inventorydata.getdays(),
                inventorydata.getlistStore(),inventorydata.getlistLine(),inventorydata.getlistCategory(),inventorydata.getlistBrand());
        if (getProductsEither.isRight()) {
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/brands")
    @Timed
    public Response getBrands() {

        Response response;

        Either<IException, List <Brand>> getProductsEither = reportsBusiness.getBrands();
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/getsons")
    @Timed
    public Response getSonCat(InventoryReportDTO inventorydata) {

        Response response;

        Either<IException, List <Long>> getProductsEither = reportsBusiness.getSonCat(
                inventorydata.getlistStore());
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/inventory/Excel")
    @Timed
    public Response postExcel(ArrayList<excelRow> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcel(excelRows);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/personas/Excel")
    @Timed
    public Response postExcelThirds(ArrayList<thirdReportDataDTO> excelRows) {

        Response response = Response.status(Response.Status.OK).entity(1).build();


        Either<IException, String> getProductsEither = reportsBusiness.postExcelThirds(excelRows);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/rotacion/Excel")
    @Timed
    public Response postExcelRotacion(ArrayList<excelRowRot> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelRotacion(excelRows);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/productos/Excel")
    @Timed
    public Response postExcelProductos(ArrayList<excelRowProd> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelProductos(excelRows);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/categorias/Excel")
    @Timed
    public Response postExcelCategorias(ArrayList<excelRowProd> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelCategorias(excelRows);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/facturas/Excel")
    @Timed
    public Response postExcelFacturas(ArrayList<excelRowFact> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelFacturas(excelRows);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/cajas/Excel")
    @Timed
    public Response postExcelCaja(ArrayList<excelRowCaja> excelRows) {

        Response response;


        Either<IException, String> getProductsEither = reportsBusiness.postExcelCaja(excelRows);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/nodisponibles")
    @Timed
    public Response getNoDisponibles(@QueryParam("idbill") Long idbill,
                                     @QueryParam("listStore") List<Long> listStore,
                                     @QueryParam("code") String code) {

        Response response;

        Either<IException, Long> getProductsEither = reportsBusiness.getNoDisponibles( idbill,
                listStore,
                 code );
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/sumnodisponibles")
    @Timed
    public Response getSumNoDisponibles(StorelistDTO dto) {

        Response response;

        Either<IException, List <NoDisp>> getProductsEither = reportsBusiness.getSumNoDisponibles(
                dto.getlistStore());
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


}

