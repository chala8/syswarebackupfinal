package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BrandDTO {
    private String brand;
    private Long id_brand_father;
    private Long id_third;
    private String url_img;


    @JsonCreator
    public BrandDTO(@JsonProperty ("brand") String brand,
                    @JsonProperty ("id_brand_father") Long id_brand_father,
                    @JsonProperty ("id_third") Long id_third,
                    @JsonProperty ("url_img")String url_img){

        this.brand = brand;
        this.id_brand_father = id_brand_father;
        this.id_third = id_third;
        this.url_img = url_img;
    }

    //-------------------------------------------------------------------------------------------------

    public String getbrand() {
        return brand;
    }

    public void setbrand(String brand) {
        this.brand = brand;
    }

    //-------------------------------------------------------------------------------------------------

    public Long getid_brand_father() {
        return id_brand_father;
    }

    public void setid_brand_father(Long id_brand_father) {
        this.id_brand_father = id_brand_father;
    }

    //-------------------------------------------------------------------------------------------------

    public Long getid_third() {
        return id_third;
    }

    public void setid_third(Long id_third) {
        this.id_third = id_third;
    }

    //-------------------------------------------------------------------------------------------------

    public String geturl_img() {
        return url_img;
    }

    public void seturl_img(String url_img) {
        this.url_img = url_img;
    }



}

