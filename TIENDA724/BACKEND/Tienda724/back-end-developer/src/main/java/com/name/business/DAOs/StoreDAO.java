package com.name.business.DAOs;

import com.name.business.entities.Storage;
import com.name.business.entities.Store;
import com.name.business.entities.Register;
import com.name.business.entities.Directory;
import com.name.business.entities.State;
import com.name.business.entities.Phone;
import com.name.business.entities.*;
import com.name.business.mappers.DirectoryMapper;
import com.name.business.mappers.RegisterBoxMapper;
import com.name.business.mappers.StateMapper;
import com.name.business.mappers.StoreMapper;
import com.name.business.mappers.StorageMapper;
import com.name.business.mappers.PhoneMapper;
import com.name.business.mappers.MailMapper;
import com.name.business.mappers.StorageCategoryMapper;
import com.name.business.mappers.StoreNameMapper;
import com.name.business.mappers.PriceMapper;
import com.name.business.mappers.ItemCajaMapper;

import com.name.business.representations.InventoryDetailDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;



public interface StoreDAO {

    @RegisterMapper(StoreMapper.class)
    @SqlQuery("select id_store,description store_name, " +
            " id_directory,store_number,entrada_consecutive_initial, " +
            " salida_consecutive_initial " +
            " from tienda724.store " +
            " where id_third=:id_third ORDER BY STORE_NUMBER")
    List<Store> getStoreByThird(@Bind("id_third") Long id_third);

    @RegisterMapper(StoreMapper.class)
    @SqlQuery(" select id_directory from tienda724.store where id_store=:id_store ")
    List<Long> getIdDirectoryByIdStore (@Bind("id_store") Long id_store);

    //-------------------------------------------------------------------------------------------

    @SqlUpdate(" INSERT INTO tienda724.store(id_store,id_third,description,id_common_state, " +
               " id_directory,store_number, " +
               " entrada_consecutive_initial,salida_consecutive_initial) " +
               " VALUES(tienda724.store_seq.nextval,:id_third,:description,null,:id_directory," +
               " :store_number,:entrada_consecutive_initial,:salida_consecutive_initial) ")
    void postStore(@Bind("id_third") Long id_third,
                        @Bind("description") String description,
                        @Bind("id_directory") Long id_directory,
                        @Bind("store_number") Long store_number,
                        @Bind("entrada_consecutive_initial") Long entrada_consecutive_initial,
                        @Bind("salida_consecutive_initial") Long salida_consecutive_initial);

    @SqlUpdate(" UPDATE tienda724.store " +
               " SET description=:description,store_number=:store_number," +
               " entrada_consecutive_initial=:entrada_consecutive_initial," +
               " salida_consecutive_initial=:salida_consecutive_initial" +
               " WHERE id_store=:id_store")
    void putStore(@Bind("id_third") Long id_third,
                        @Bind("description") String description,
                        @Bind("store_number") Long store_number,
                        @Bind("entrada_consecutive_initial") Long entrada_consecutive_initial,
                        @Bind("salida_consecutive_initial") Long salida_consecutive_initial,
                        @Bind("id_store") Long id_store);


    //-----------------------------------------------------------------------------------------

    @RegisterMapper(RegisterBoxMapper.class)
    @SqlQuery(" select id_caja,description caja_name,caja_number " +
            " from tienda724.caja " +
            " where id_store=:id_store ")
    List<Register> getCajaByStore (@Bind("id_store") Long id_store);

    @SqlUpdate(" insert into tienda724.caja(id_caja,id_store,description,caja_number) " +
            " values(tienda724.caja_seq.nextval,:id_store,:description,:caja_number) ")
    void postCaja(@Bind("id_store") Long id_store,
                  @Bind("description") String description,
                  @Bind("caja_number") Long caja_number);

    @SqlUpdate(" update tienda724.caja " +
            " set description=:description,caja_number=:caja_number " +
            " where id_caja=:id_caja ")
    void putCaja(@Bind("description") String description,
                 @Bind("caja_number") Long caja_number,
                 @Bind("id_caja") Long id_caja);

    //------------------------------------------------------------------------------------------------

    @RegisterMapper(StorageMapper.class)
    @SqlQuery(" select id_storage,description storage_name,storage_number " +
              " from tienda724.storage " +
              " where id_store=:id_store ")
    List<Storage> getStorageByStore (@Bind("id_store") Long id_store);


    @SqlUpdate(" insert into tienda724.storage(id_storage,id_store,description,storage_number) " +
               " values(tienda724.storage_seq.nextval,:id_store,:description,:storage_number) ")
    void postStorage(@Bind("id_store") Long id_store,
                     @Bind("description") String description,
                     @Bind("storage_number") Long storage_number);


    @SqlUpdate(" update tienda724.storage " +
            " set description=:description,storage_number=:storage_number " +
            " where id_storage=:id_storage ")
    void putStorage(@Bind("description") String description,
                 @Bind("storage_number") Long storage_number,
                 @Bind("id_storage") Long id_storage);


    //------------------------------------------------------------------------------------------------

    @RegisterMapper(DirectoryMapper.class)
    @SqlQuery(" select dir_name,address,webpage,id_city,latitud,longitud " +
              " from tercero724.directory " +
              " where id_directory=:id_directory ")
    List<Directory> getDirectorybyID (@Bind("id_directory") Long id_directory);


    @SqlQuery(" select city_name||' - '||state_name AS city_name " +
              " from tercero724.city c,tercero724.state s " +
              " where c.id_state=s.id_state and id_city=:id_city ")
    List<String> getCityByID (@Bind("id_city") Long id_city);


    @SqlQuery(" select city_name from tercero724.city where id_state=:id_state ")
    List<String> getCitiesByState (@Bind("id_state") Long id_state);

    //---------------------------------------------------------------------------------

    @RegisterMapper(StateMapper.class)
    @SqlQuery(" select id_state,state_name from tercero724.state ")
    List<State> getStates ();

    @RegisterMapper(PhoneMapper.class)
    @SqlQuery("select id_phone,phone,priority from tercero724.phone where id_directory=:id_directory")
    List<Phone> getPhonesByDirectory (@Bind("id_directory") Long id_directory);

    @RegisterMapper(MailMapper.class)
    @SqlQuery("select id_mail,mail,priority from tercero724.mail where id_directory=:id_directory")
    List<Mail> getMailsByDirectory (@Bind("id_directory") Long id_directory);

    //----------------------------------------------------------------------------------

    @RegisterMapper(StorageCategoryMapper.class)
    @SqlQuery(" SELECT DISTINCT d.ID_CATEGORY, d.ID_CATEGORY_FATHER, b.ID_THIRD, e.NAME, e.DESCRIPTION, d.IMG_URL\n" +
            " FROM TIENDA724.PRODUCT_STORE a, TIENDA724.CODES b, TIENDA724.PRODUCT c, TIENDA724.CATEGORY d, TIENDA724.COMMON e, TIENDA724.STORE f\n" +
            " WHERE a.ID_STORE = f.ID_STORE AND a.ID_CODE = b.ID_CODE AND b.ID_PRODUCT = c.ID_PRODUCT AND c.ID_CATEGORY = d.ID_CATEGORY\n" +
            "  AND e.ID_COMMON = d.ID_COMMON AND f.ID_THIRD = :ID_THIRD ")
    List<StorageCategory> getCategoryByStore(@Bind("ID_THIRD") Long ID_THIRD);


    @RegisterMapper(StoreNameMapper.class)
    @SqlQuery(" SELECT a.ID_STORE, a.DESCRIPTION " +
            " FROM TIENDA724.STORE a, TIENDA724.PRODUCT_STORE b, TIENDA724.CODES c, TIENDA724.PRODUCT d " +
            " WHERE a.ID_STORE = b.ID_STORE AND b.ID_CODE = c.ID_CODE AND c.ID_PRODUCT = d.ID_PRODUCT AND d.ID_CATEGORY = :id_category ")
    List<StoreName> getStoreByCategory(@Bind("id_category") Long id_category);

    //-----------------------------------------------------------------------------------

    @RegisterMapper(PriceMapper.class)
    @SqlQuery(" select price_description,price from " +
            " tienda724.price where id_product_store=:id_product_store order by price desc ")
    List<Price> getPriceList(@Bind("id_product_store") Long id_product_store);


    @SqlUpdate(" insert into tienda724.PRICE(ID_PRODUCT_STORE, PRICE_DESCRIPTION, PRICE) " +
            " values(:ID_PRODUCT_STORE,:PRICE_DESCRIPTION,:PRICE) ")
    void postPrice(@Bind("ID_PRODUCT_STORE") Long ID_PRODUCT_STORE,
                   @Bind("PRICE_DESCRIPTION") String PRICE_DESCRIPTION,
                   @Bind("PRICE") Double PRICE);


    @SqlUpdate(" insert into tienda724.PRODUCT_STORE(id_store,id_code,product_store_name,standard_price,product_store_code,ownbarcode) " +
               " values(:id_store,:id_code,:product_store_name,:standard_price,:product_store_code,:ownbarcode) ")
    void postProductStore (@Bind("id_store") Long id_store,
                           @Bind("id_code") Long id_code,
                           @Bind("product_store_name") String product_store_name,
                           @Bind("standard_price") Long standard_price,
                           @Bind("product_store_code") String product_store_code,
                           @Bind("ownbarcode") String ownbarcode);

    @SqlUpdate(" update TERCERO724.third set ID_COMMON_BASICINFO =\n" +
            " (select p.ID_COMMON_BASICINFO from TERCERO724.PERSON p, tercero724.THIRD t where t.ID_PERSON = p.ID_PERSON and t.ID_THIRD = :id_third)\n" +
            " where id_third = :id_third ")
    void putThird(@Bind("id_third") Long id_third);

    @SqlQuery(" select t.ID_THIRD from TERCERO724.PERSON p, tercero724.THIRD t where t.ID_PERSON = p.ID_PERSON and p.ID_PERSON = :id_person")
    Long getThird(@Bind("id_person") Long id_person);

    @SqlQuery("SELECT ID_PRODUCT_STORE FROM TIENDA724.PRODUCT_STORE WHERE ID_PRODUCT_STORE IN (SELECT MAX(ID_PRODUCT_STORE) FROM TIENDA724.PRODUCT_STORE)")
    Long getPkLast();


    @SqlQuery(" select ID_PRODUCT_STORE from tienda724.PRODUCT_STORE\n" +
            " where PRODUCT_STORE_CODE = :code\n" +
            "  and ID_STORE = :id_store ")
    Long getPsId(@Bind("code") String code,
                 @Bind("id_store") Long id_store);

    @RegisterMapper(ItemCajaMapper.class)
    @SqlQuery(" select c.id_caja,c.id_store\n" +
            " from tienda724.caja_person cp,tienda724.caja c\n" +
            " where cp.id_caja=c.id_caja\n" +
            " and cp.id_person=:id_person")
    List<ItemCaja> getCajaItems(@Bind("id_person") Long id_person);

    @SqlQuery(" select ID_TIPO_STORE from tienda724.STORE_TIPOSTORE where ID_STORE = :id_store ")
    List<Long> getTipoStore (@Bind("id_store") Long id_store);


    @SqlUpdate(" UPDATE tienda724.PRODUCT_STORE\n" +
            " SET STANDARD_PRICE = :standard_price\n" +
            " WHERE ID_PRODUCT_STORE = :id_ps")
    void putStandardPrice(@Bind("standard_price") Double standard_price,
                          @Bind("id_ps") Long id_ps);


    @SqlQuery(" SELECT STANDARD_PRICE from TIENDA724.PRODUCT_STORE WHERE ID_PRODUCT_STORE = :id_ps ")
    Double getStandardPriceByPs (@Bind("id_ps") Long id_ps);


    @SqlCall(" call facturacion724.gestion_pedidos.confirmar_pedido_cliente(:numpedido, :idbilltype, :idstore) ")
    void confirmar_pedido_cliente(@Bind("numpedido") String numpedido,
                                  @Bind("idbilltype") Long idbilltype,
                                  @Bind("idstore") Long idstore);

    @SqlCall(" call FACTURACION724.DEVOLUCION_VENTA(:consecutivo,:idstore,:idcaja) ")
    void devolucionVenta(@Bind("consecutivo") Long consecutivo,
                                  @Bind("idcaja") Long idcaja,
                                  @Bind("idstore") Long idstore);
}