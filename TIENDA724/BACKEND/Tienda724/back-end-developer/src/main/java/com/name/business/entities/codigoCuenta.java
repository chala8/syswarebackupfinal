package com.name.business.entities;
import org.skife.jdbi.cglib.core.Local;

import java.time.LocalDate;

import java.util.Date;

public class codigoCuenta {
    private String CODIGO_CUENTA;
    private String DESCRIPCION;



    public codigoCuenta(String CODIGO_CUENTA,String DESCRIPCION) {
        this.CODIGO_CUENTA = CODIGO_CUENTA;
        this.DESCRIPCION = DESCRIPCION;
    }

    public String getCODIGO_CUENTA() {
        return CODIGO_CUENTA;
    }

    public void setCODIGO_CUENTA(String CODIGO_CUENTA) {
        this.CODIGO_CUENTA = CODIGO_CUENTA;
    }

    //------------------------------------------------------------------------------

    public String getDESCRIPCION() {
        return DESCRIPCION;
    }

    public void setDESCRIPCION(String DESCRIPCION) {
        this.DESCRIPCION = DESCRIPCION;
    }

    //------------------------------------------------------------------------------


}


