package com.name.business.entities;

public class ProductStore {

    private Long ID_PRODUCT_STORE;
    private String PRODUCT_STORE_NAME;


    public ProductStore(Long ID_PRODUCT_STORE,
                        String PRODUCT_STORE_NAME) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;}

    //---------------------------------------------------------------------------

    public Long getID_PRODUCT_STORE() {
        return ID_PRODUCT_STORE;
    }

    public void setID_PRODUCT_STORE(Long ID_PRODUCT_STORE) {
        this.ID_PRODUCT_STORE = ID_PRODUCT_STORE;
    }

    //---------------------------------------------------------------------------

    public String getPRODUCT_STORE_NAME() {
        return PRODUCT_STORE_NAME;
    }

    public void setPRODUCT_STORE_NAME(String PRODUCT_STORE_NAME) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
    }

    //---------------------------------------------------------------------------


}
