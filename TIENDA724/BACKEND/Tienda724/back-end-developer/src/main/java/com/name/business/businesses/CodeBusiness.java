package com.name.business.businesses;

import com.name.business.DAOs.CodeDAO;
import com.name.business.entities.*;
import com.name.business.entities.completes.AttributeComplete;
import com.name.business.entities.completes.CodeComplete;
import com.name.business.representations.CodeDTO;
import com.name.business.representations.CommonStateDTO;
import com.name.business.representations.ProductThirdDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.codeSanitation;
import static com.name.business.sanitations.EntitySanitation.priceListSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class CodeBusiness {


    private CodeDAO codeDAO;
    private CommonStateBusiness commonStateBusiness;
    private AttributeDetailListBusiness attributeDetailListBusiness;
    private AttributeBusiness attributeBusiness;

    public CodeBusiness(CodeDAO codeDAO, CommonStateBusiness commonStateBusiness, AttributeDetailListBusiness attributeDetailListBusiness, AttributeBusiness attributeBusiness) {
        this.codeDAO = codeDAO;
        this.commonStateBusiness = commonStateBusiness;
        this.attributeDetailListBusiness = attributeDetailListBusiness;
        this.attributeBusiness = attributeBusiness;
    }

    /**
     *
     * @param code
     *
     * @return
     */
    public Either<IException, List<Code>> getCodes(Code code) {
        List<String> msn = new ArrayList<>();
        try {
            System.out.println("|||||||||||| Starting consults Code ||||||||||||||||| ");
            Code codeSanitation = codeSanitation(code);
            List<Code> read = codeDAO.read(code);
            return Either.right(read);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     *@TODO You should create a logic, so You built the attribute list with your values
     * @param code
     *
     * @return
     */
    public Either<IException, List<CodeComplete>> getCodesComplete(Code code, Boolean isTreeCategory,Boolean isAttribs) {
        List<String> msn = new ArrayList<>();
        List<CodeComplete> read= new ArrayList<>();
        try {
            System.out.println("|||||||||||| Starting consults Code Complete ||||||||||||||||| ");
            Code codeSanitation = codeSanitation(code);
            read = codeDAO.readComplete(codeSanitation);

            if (formatoBooleanQP(isAttribs)){

                for (int i = 0; i < read.size(); i++) {
                    Long id= (Long) read.get(i).getAttribute_list();
                    Either<IException, List<AttributeDetailList>> attributeDetailListsEither = attributeDetailListBusiness.getAttributeDetailLists(

                            new AttributeDetailList(null, formatoLongSql(id), new CommonState(null, 1, null, null),
                                    new AttributeValue(null, null, new Common(null, null, null),
                                            new CommonState(null, null, null, null)))
                    );
                    if(attributeDetailListsEither.isRight() && attributeDetailListsEither.right().value().size()>0){

                        List<AttributeDetailList> detailListList = attributeDetailListsEither.right().value();
                        read.get(i).setAttribute_list(detailListList);

                    }
                }
            }
            return Either.right(read);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> createCode(CodeDTO codeDTO) {
        List<String> msn = new ArrayList<>();
        Long id_product = null;
        Long id_common_state = null;

        try {

            System.out.println("|||||||||||| Starting creation Code  ||||||||||||||||| ");
            if (codeDTO!=null){

                if ( validatorCODE(formatoStringSql(codeDTO.getCode()))){
                    msn.add("It CODE does exist register on  Code, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                //TODO validate the  id_product if exist on database a register with this id
                if (validatorIDProduct(codeDTO.getId_product()).equals(false)){
                    msn.add("It ID Product does not exist register on  Product, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                //TODO validate the  id_measure_unit  if exist on database a register with this id
                if (validatorIDMeasureUnit(codeDTO.getId_measure_unit()).equals(false)){
                    msn.add("It ID Measure Unit does not exist register on  Measure Unit, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (codeDTO.getId_attribute_list() !=null && validatorIDAttributeList(codeDTO.getId_attribute_list()).equals(false)){
                    msn.add("It ID Attribute List does not exist register on  AttributeList, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }


                if (codeDTO.getStateDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(codeDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }
                codeDTO.setId_third_cod(formatoLongSql(codeDTO.getId_third_cod()));
                codeDAO.create(codeDTO,formatoLongSql(id_common_state));
                id_product = codeDAO.getPkLast();

                return Either.right(id_product);
            }else{
                msn.add("It does not recognize Product, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    private Boolean validatorIDAttributeList(Long id_attribute_list) {
        Integer validator = 0;
        try {
            validator = codeDAO.validatorIDAttributeList(id_attribute_list);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    private Boolean validatorIDMeasureUnit(Long id_measure_unit) {
        Integer validator = 0;
        try {
            validator = codeDAO.validatorIDMeasureUnit(id_measure_unit);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }

    }

    private Boolean validatorIDProduct(Long id_product) {
        Integer validator = 0;
        try {
            validator = codeDAO.validatorIDProduct(id_product);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }


    /**
     * @param id_code
     * @return
     */
    public Either<IException, Long> deleteCode(Long id_code,String code) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete Attribute  List ||||||||||||||||| ");
            if ((id_code != null && id_code > 0) || (code != null && !code.isEmpty()) ) {
                List<CommonSimple> readCommons = codeDAO.readCommons(formatoLongSql(id_code), formatoStringSql(code));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common
                return Either.right(id_code);
            } else {
                msn.add("It does not recognize ID Attribute List, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_code
     * @param codeDTO
     * @return
     */
    public Either<IException, Long> updateCode(Long id_code,String code_param, CodeDTO codeDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        Code currentCode =null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update Product ||||||||||||||||| ");
            if ((id_code != null && id_code > 0)) {


                if (validatorID(id_code).equals(false)){
                    msn.add("It ID Code does not exist register on  Codes, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                //TODO validate the  id attribute if exist on database a register with this id
                if (validatorIDProduct(codeDTO.getId_product()).equals(false)){
                    msn.add("It ID Product Third does not exist register on  Product third , probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                if (validatorIDMeasureUnit(codeDTO.getId_measure_unit()).equals(false)){
                    msn.add("It ID Measure Unit does not exist register on  Measure Unit , probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                if (validatorIDAttributeList(codeDTO.getId_attribute_list()).equals(false)){
                    msn.add("It ID Attribute List does not exist register on  Attribute List , probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }




                Code code = new Code(id_code, null, null, null, null,null,null,null,
                        new CommonState(null, null, null, null));


                List<Code> read = codeDAO.read(code);

                if (read.size()<=0){
                    msn.add("It does not recognize ID Product third, probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = codeDAO.readCommons(formatoLongSql(id_code),formatoStringSql(code.getCode()));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }

                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), codeDTO.getStateDTO());

                currentCode = read.get(0);

                // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                if (codeDTO.getCode() == null || codeDTO.getCode().isEmpty()) {
                    codeDTO.setCode(currentCode.getCode());
                } else {
                    codeDTO.setCode(formatoStringSql(codeDTO.getCode()));
                }

                if (codeDTO.getImg() == null || codeDTO.getImg().isEmpty()) {
                    codeDTO.setImg(currentCode.getImg());
                } else {
                    codeDTO.setImg(formatoStringSql(codeDTO.getImg()));
                }

                if (codeDTO.getId_product() == null) {
                    codeDTO.setId_product(currentCode.getId_product());
                } else {
                    codeDTO.setId_product(formatoLongSql(codeDTO.getId_product()));
                }

                if (codeDTO.getId_measure_unit() == null || codeDTO.getId_measure_unit()<1) {
                    codeDTO.setId_measure_unit(currentCode.getId_measure_unit());
                } else {
                    codeDTO.setId_measure_unit(formatoLongSql(codeDTO.getId_measure_unit()));
                }

                if (codeDTO.getId_attribute_list() == null || codeDTO.getId_attribute_list()<1) {
                    codeDTO.setId_attribute_list(currentCode.getId_attribute_list());
                } else {
                    codeDTO.setId_attribute_list(formatoLongSql(codeDTO.getId_attribute_list()));
                }


                // Attribute update
                codeDAO.update(id_code,formatoStringSql(code_param), codeDTO);

                return Either.right(id_code);

            } else {
                msn.add("It does not recognize ID Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
            validator = codeDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }

    /**
     * @param code
     * */
    public Boolean validatorCODE(String code) {
        Integer validator = 0;
        try {
            validator = codeDAO.getValidatorCode(code);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }
}
