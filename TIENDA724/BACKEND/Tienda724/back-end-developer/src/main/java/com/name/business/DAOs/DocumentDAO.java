package com.name.business.DAOs;

import com.name.business.entities.Document;
import com.name.business.mappers.DocumentMapper;
import com.name.business.representations.DocumentDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(DocumentMapper.class)
public interface DocumentDAO {

    @SqlQuery("SELECT ID_DOCUMENT FROM DOCUMENT WHERE ID_DOCUMENT IN (SELECT MAX(DOCUMENT.ID_DOCUMENT) FROM DOCUMENT)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO DOCUMENT ( TITLE,BODY) VALUES (:co.name,:co.description)")
    void create(@BindBean("co") DocumentDTO documentDTO);


    /**
     *
     * @param id_DOCUMENT
     * @param documentDTO
     * @return
     */
    @SqlUpdate("UPDATE DOCUMENT SET " +
            "TITLE = :DOCUMENT.name, " +
            "BODY = :DOCUMENT.description " +
            "WHERE (ID_DOCUMENT=:id_DOCUMENT) ")
    int update(@Bind("id_DOCUMENT") Long id_DOCUMENT, @BindBean("DOCUMENT") DocumentDTO documentDTO);

    @SqlQuery("SELECT * FROM DOCUMENT CO " +
            "WHERE (CO.ID_DOCUMENT=:co.id_common OR :co.id_common IS NULL )AND  " +
            "      (CO.TITLE LIKE  :co.name OR :co.name IS NULL )AND " +
            "      (co.BODY LIKE :co.description OR :co.description IS NULL )")
    List<Document> read(@BindBean("co") Document document);


    void delete(Long aLong, Date date);
}
