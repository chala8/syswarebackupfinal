package com.name.business.entities;

public class Attribute {

    private Long id_attribute;
    private Common common;
    private CommonState state;

    public Attribute(Long id_attribute, Common common,
                     CommonState state) {
        this.id_attribute = id_attribute;
        this.common = common;
        this.state = state;
    }

    public Long getId_attribute() {
        return id_attribute;
    }

    public void setId_attribute(Long id_attribute) {
        this.id_attribute = id_attribute;
    }

    public Common getCommon() {
        return common;
    }

    public void setCommon(Common common) {
        this.common = common;
    }

    public CommonState getState() {
        return state;
    }

    public void setState(CommonState state) {
        this.state = state;
    }
}
