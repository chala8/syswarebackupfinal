package com.name.business.entities;

public class CategoryReport {

    private String CATEGORIA;
    private Long VENTA;
    private Long COSTO;
    private Long UTILIDAD;
    private Double PCT_MARGEN;
    private Long NUMVENTAS;
    private String OWNBARCODE;


    public CategoryReport(String CATEGORIA,
            Long VENTA,
            Long COSTO,
            Long UTILIDAD,
            Double PCT_MARGEN,
            Long NUMVENTAS) {
        this.CATEGORIA = CATEGORIA;
        this.VENTA = VENTA;
        this.COSTO = COSTO;
        this.UTILIDAD = UTILIDAD;
        this.PCT_MARGEN = PCT_MARGEN;
        this.NUMVENTAS = NUMVENTAS;
    }

    public CategoryReport(
            String OWNBARCODE,
            String CATEGORIA,
          Long VENTA,
          Long COSTO,
          Long UTILIDAD,
          Double PCT_MARGEN,
          Long NUMVENTAS) {
        this.OWNBARCODE = OWNBARCODE;
        this.CATEGORIA = CATEGORIA;
        this.VENTA = VENTA;
        this.COSTO = COSTO;
        this.UTILIDAD = UTILIDAD;
        this.PCT_MARGEN = PCT_MARGEN;
        this.NUMVENTAS = NUMVENTAS;
    }
    public String getOWNBARCODE() {
        return OWNBARCODE;
    }

    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }

    //-------------------------------------------------------------------------

    public Long getNUMVENTAS() {
        return NUMVENTAS;
    }

    public void setNUMVENTAS(Long NUMVENTAS) {
        this.NUMVENTAS = NUMVENTAS;
    }

    //-------------------------------------------------------------------------

    public Long getVENTA() {
        return VENTA;
    }

    public void setVENTA(Long VENTA) {
        this.VENTA = VENTA;
    }

    //-------------------------------------------------------------------------

    public String getCATEGORIA() {
        return CATEGORIA;
    }

    public void setCATEGORIA(String CATEGORIA) {
        this.CATEGORIA = CATEGORIA;
    }

    //---------------------------------------------------------------------------

    public Long getCOSTO() {
        return COSTO;
    }

    public void setCOSTO(Long COSTO) {
        this.COSTO = COSTO;
    }

    //-------------------------------------------------------------------------

    public Long getUTILIDAD() {
        return UTILIDAD;
    }

    public void setUTILIDAD(Long UTILIDAD) {
        this.UTILIDAD = UTILIDAD;
    }

    //-------------------------------------------------------------------------


    public Double getPCT_MARGEN() {
        return PCT_MARGEN;
    }

    public void setPCT_MARGEN(Double PCT_MARGEN) {
        this.PCT_MARGEN = PCT_MARGEN;
    }

    //-------------------------------------------------------------------------


}
