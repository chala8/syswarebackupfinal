package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;

public class ProductNameDTO {


    private Long id_category;
    private Long id_tax;
    private Long id_third;
    private String productName;
    private String productDescription;

    @JsonCreator
    public ProductNameDTO(@JsonProperty("id_category") Long id_category,
                          @JsonProperty("id_tax") Long id_tax,
                          @JsonProperty("id_third") Long id_third,
                          @JsonProperty("productName") String productName,
                          @JsonProperty("productDescription") String productDescription) {
        this.id_category = id_category;
        this.id_tax = id_tax;
        this.id_third = id_third;
        this.productName = productName;
        this.productDescription = productDescription;
    }

    public Long getid_category() {
        return id_category;
    }

    public void setid_category(Long id_category) {
        this.id_category = id_category;
    }

    public Long getid_tax() {
        return id_tax;
    }

    public void setid_tax(Long id_tax) {
        this.id_tax = id_tax;
    }

    public Long getid_third() {
        return id_third;
    }

    public void setid_third(Long id_third) {
        this.id_third = id_third;
    }

    public String getproductName() {
        return productName;
    }

    public void setproductName(String productName) {
        this.productName = productName;
    }

    public String getproductDescription() {
        return productDescription;
    }

    public void setproductDescription(String categoryDescription) {
        this.productDescription = productDescription;
    }
}
