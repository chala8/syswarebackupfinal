package com.name.business.entities;

public class Register {

    private Long ID_CAJA;
    private String CAJA_NAME;
    private Long CAJA_NUMBER;


    public Register( Long ID_CAJA,
                     String CAJA_NAME,
                     Long CAJA_NUMBER) {
        this.ID_CAJA = ID_CAJA;
        this.CAJA_NAME = CAJA_NAME;
        this.CAJA_NUMBER =  CAJA_NUMBER;
    }

    //---------------------------------------------------------------------------

    public Long getID_CAJA() {
        return ID_CAJA;
    }

    public void setID_CAJA(Long ID_CAJA) {
        this.ID_CAJA = ID_CAJA;
    }

    //---------------------------------------------------------------------------

    public String getCAJA_NAME() {
        return CAJA_NAME;
    }

    public void setCAJA_NAME(String CAJA_NAME) {
        this.CAJA_NAME = CAJA_NAME;
    }

    //---------------------------------------------------------------------------

    public Long getCAJA_NUMBER() {
        return CAJA_NUMBER;
    }

    public void setCAJA_NUMBER(Long CAJA_NUMBER) {
        this.CAJA_NUMBER = CAJA_NUMBER;
    }

    //---------------------------------------------------------------------------

}
