package com.name.business.mappers;

import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductMapper implements ResultSetMapper<Product> {

    @Override
    public Product map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Product(
                resultSet.getLong("ID_PRODUCT"),
                resultSet.getLong("ID_CATEGORY"),
                resultSet.getInt("STOCK"),
                resultSet.getInt("STOCK_MIN"),
                resultSet.getString("IMG_URL"),
                resultSet.getString("CODE"),
                resultSet.getLong("ID_TAX"),
                new Common(
                        resultSet.getLong("ID_COMMON"),
                        resultSet.getString("NAME"),
                        resultSet.getString("DESCRIPTION")
                ),
                new CommonState(
                        resultSet.getLong("ID_STATE"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_PRODUCT"),
                        resultSet.getDate("MODIFY_PRODUCT")
                )
        );
    }


}
