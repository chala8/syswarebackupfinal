package com.name.business.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class excelRowFact {
    private String prefix_BILL;
    private Long costo;
    private Long id_BILL;
    private Long id_BILL_STATE;
    private String tienda;
    private Long venta;
    private Long utilidad;
    private String cajero;
    private String num_documento;
    private String caja;
    private String fullname;
    private String purchase_DATE;
    private Long consecutive;
    private Double pct_margen_venta;
    private Double pct_margen_costo;


    @JsonCreator
    public excelRowFact(
            @JsonProperty("fullname") String fullname,
            @JsonProperty("purchase_DATE") String purchase_DATE,
            @JsonProperty("prefix_BILL") String prefix_BILL,
            @JsonProperty("pct_MARGEN_COSTO") Double pct_margen_costo,
            @JsonProperty("pct_MARGEN_VENTA") Double pct_margen_venta,
            @JsonProperty("consecutive") Long consecutive,
            @JsonProperty("costo") Long costo,
            @JsonProperty("utilidad") Long utilidad,
            @JsonProperty("venta") Long venta,
            @JsonProperty("id_BILL") Long id_BILL,
            @JsonProperty("id_BILL_STATE") Long id_BILL_STATE,
            @JsonProperty("tienda") String tienda,
            @JsonProperty("cajero") String cajero,
            @JsonProperty("num_DOCUMENTO") String num_documento,
            @JsonProperty("caja") String caja) {
        this.caja = caja;
        this.num_documento = num_documento;
        this.pct_margen_venta = pct_margen_venta;
        this.cajero = cajero;
        this.tienda = tienda;
        this.fullname = fullname;
        this.purchase_DATE = purchase_DATE;
        this.prefix_BILL = prefix_BILL;
        this.pct_margen_costo = pct_margen_costo;
        this.consecutive = consecutive;
        this.costo = costo;
        this.utilidad = utilidad;
        this.venta = venta;
        this.id_BILL = id_BILL;
        this.id_BILL_STATE = id_BILL_STATE;
    }

    public String getfullname() {
        return fullname;
    }
    public void setfullname(String fullname) {
        this.fullname = fullname;
    }

    public String getpurchase_DATE() {
        return purchase_DATE;
    }
    public void setpurchase_DATE(String purchase_DATE) {
        this.purchase_DATE = purchase_DATE;
    }

    public String getprefix_BILL() {
        return prefix_BILL;
    }
    public void setprefix_BILL(String prefix_BILL) {
        this.prefix_BILL = prefix_BILL;
    }

    public Double getpct_margen_costo() {
        return pct_margen_venta;
    }
    public void setpct_margen_costo(Double pct_margen_costo) {
        this.pct_margen_costo = pct_margen_costo;
    }

    public Long getconsecutive() {
        return consecutive;
    }
    public void setconsecutive(Long consecutive) {
        this.consecutive = consecutive;
    }

    public Long getcosto() {
        return costo;
    }
    public void setcosto(Long costo) {
        this.costo = costo;
    }

    public Long getutilidad() {
        return utilidad;
    }
    public void setutilidad(Long utilidad) {
        this.utilidad = utilidad;
    }

    public Long getventa() {
        return venta;
    }
    public void setventa(Long venta) {
        this.venta = venta;
    }

    public Long getid_BILL() {
        return id_BILL;
    }
    public void setid_BILL(Long id_BILL) {
        this.id_BILL = id_BILL;
    }

    public Long getid_BILL_STATE() {
        return id_BILL_STATE;
    }
    public void setid_BILL_STATE(Long id_BILL_STATE) {
        this.id_BILL_STATE = id_BILL_STATE;
    }

}
