package com.name.business.mappers;


import com.name.business.entities.Code;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CodeMapper  implements ResultSetMapper<Code> {

    @Override
    public Code map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Code(
                resultSet.getLong("ID_CODE"),
                resultSet.getString("CODE"),
                resultSet.getString("IMG_COD"),
                resultSet.getDouble("SUGGESTED_PRICE"),
                resultSet.getLong("ID_THIRD_COD"),
                resultSet.getLong("ID_PRODUCT_COD"),
                resultSet.getLong("ID_MEASURE_UNIT_COD"),
                resultSet.getLong("ID_ATTRIBUTE_LIST_COD"),
                new CommonState(
                        resultSet.getLong("ID_STATE_CODE"),
                        resultSet.getInt("STATE_CODE"),
                        resultSet.getDate("CREATION_CODE"),
                        resultSet.getDate("MODIFY_CODE")
                )
        );
    }
}
