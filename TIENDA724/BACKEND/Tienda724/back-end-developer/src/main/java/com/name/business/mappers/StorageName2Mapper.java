package com.name.business.mappers;

import com.name.business.entities.Storage2Name;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StorageName2Mapper implements ResultSetMapper<Storage2Name>{

    @Override
    public Storage2Name map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Storage2Name(
                resultSet.getLong("ID_STORAGE"),
                resultSet.getString("DESCRIPTION"),
                resultSet.getLong("STORAGE_NUMBER")
        );
    }
}