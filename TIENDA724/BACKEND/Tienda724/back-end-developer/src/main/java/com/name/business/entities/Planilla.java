package com.name.business.entities;

import java.util.Date;

public class Planilla {

    private Long ID_PLANILLA;
    private String FECHA_INICIO;
    private String NUM_PLANILLA;
    private String PLACA;
    private String CONDUCTOR;



    public Planilla(Long ID_PLANILLA,
             String FECHA_INICIO,
             String NUM_PLANILLA,
             String PLACA,
             String CONDUCTOR) {
        this.ID_PLANILLA = ID_PLANILLA;
        this.FECHA_INICIO = FECHA_INICIO;
        this.NUM_PLANILLA =  NUM_PLANILLA;
        this.PLACA =  PLACA;
        this.CONDUCTOR =  CONDUCTOR;
    }

    public Long getID_PLANILLA() {
        return ID_PLANILLA;
    }

    public void setID_PLANILLA(Long ID_PLANILLA) {
        this.ID_PLANILLA = ID_PLANILLA;
    }

    public String getFECHA_INICIO() {
        return FECHA_INICIO;
    }

    public void setFECHA_INICIO(String FECHA_INICIO) {
        this.FECHA_INICIO = FECHA_INICIO;
    }

    public String getNUM_PLANILLA() {
        return NUM_PLANILLA;
    }

    public void setNUM_PLANILLA(String NUM_PLANILLA) {
        this.NUM_PLANILLA = NUM_PLANILLA;
    }

    public String getPLACA() {
        return PLACA;
    }

    public void setPLACA(String PLACA) {
        this.PLACA = PLACA;
    }

    public String getCONDUCTOR() {
        return CONDUCTOR;
    }

    public void setCONDUCTOR(String CONDUCTOR) {
        this.CONDUCTOR = CONDUCTOR;
    }


}
