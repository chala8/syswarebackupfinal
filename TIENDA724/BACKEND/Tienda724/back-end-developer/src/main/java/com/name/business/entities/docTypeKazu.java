package com.name.business.entities;
import org.skife.jdbi.cglib.core.Local;

import java.time.LocalDate;

import java.util.Date;

public class docTypeKazu {
    private String document_type;
    private Long id_document_type;



    public docTypeKazu(String document_type,Long id_document_type) {
        this.document_type = document_type;
        this.id_document_type = id_document_type;
    }

    public String getdocument_type() {
        return document_type;
    }

    public void setdocument_type(String document_type) {
        this.document_type = document_type;
    }

    //------------------------------------------------------------------------------

    public Long getid_document_type() {
        return id_document_type;
    }

    public void setid_document_type(Long id_document_type) {
        this.id_document_type = id_document_type;
    }

    //------------------------------------------------------------------------------


}


