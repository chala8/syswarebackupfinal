package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;
import java.util.List;

public class PlanillaDTO {


    private String fecha_INICIO;
    private String conductor;
    private String placa;
    private String num_PLANILLA;
    private Long id_PLANILLA;

    @JsonCreator
    public PlanillaDTO(@JsonProperty("fecha_INICIO") String fecha_INICIO,
            @JsonProperty("conductor") String conductor,
            @JsonProperty("placa") String placa,
            @JsonProperty("num_PLANILLA") String num_PLANILLA,
            @JsonProperty("id_PLANILLA") Long id_PLANILLA) {
        this.fecha_INICIO = fecha_INICIO;
        this.conductor = conductor;
        this.placa = placa;
        this.num_PLANILLA = num_PLANILLA;
        this.id_PLANILLA = id_PLANILLA;
    }
    //------------------------------------------------------------------------------------------------
    public String getfecha_INICIO() {
        return fecha_INICIO;
    }
    public void setfecha_INICIO(String fecha_INICIO) {
        this.fecha_INICIO = fecha_INICIO;
    }
    //------------------------------------------------------------------------------------------------
    public String getconductor() {
        return conductor;
    }
    public void setconductor(String conductor) {
        this.conductor = conductor;
    }
    //------------------------------------------------------------------------------------------------
    public String getplaca() {
        return placa;
    }
    public void setplaca(String placa) {
        this.placa = placa;
    }
    //------------------------------------------------------------------------------------------------
    public String getnum_PLANILLA() {
        return num_PLANILLA;
    }
    public void setnum_PLANILLA(String num_PLANILLA) {
        this.num_PLANILLA = num_PLANILLA;
    }
    //------------------------------------------------------------------------------------------------
    public Long getid_PLANILLA() {
        return id_PLANILLA;
    }
    public void setid_PLANILLA(Long id_PLANILLA) {
        this.id_PLANILLA = id_PLANILLA;
    }
    //------------------------------------------------------------------------------------------------



}
