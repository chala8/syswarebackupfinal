package com.name.business.mappers;


import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductStoreMapper implements ResultSetMapper<ProductStore> {
    @Override
    public ProductStore map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ProductStore(
                resultSet.getLong("ID_PRODUCT_STORE"),
                resultSet.getString("PRODUCT_STORE_NAME")
        );
    }
}
