package com.name.business.entities;

public class StorageName {

    private Long ID_STORAGE;
    private String DESCRIPTION;
    private Long STORAGE_NUMBER;
    private Long QUANTITY;



    public StorageName(Long ID_STORAGE, String DESCRIPTION, Long STORAGE_NUMBER, Long QUANTITY) {
        this.ID_STORAGE = ID_STORAGE;
        this.DESCRIPTION = DESCRIPTION;
        this.STORAGE_NUMBER =  STORAGE_NUMBER;
        this.QUANTITY = QUANTITY;
    }

    //-------------------------------------------------------------------------------------------

    public Long getID_STORAGE() {
        return ID_STORAGE;
    }

    public void setID_STORAGE(Long ID_STORAGE) {
        this.ID_STORAGE = ID_STORAGE;
    }

    //-------------------------------------------------------------------------------------------

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    //-------------------------------------------------------------------------------------------

    public Long getSTORAGE_NUMBER() {
        return STORAGE_NUMBER;
    }

    public void setSTORAGE_NUMBER(Long STORAGE_NUMBER) {
        this.STORAGE_NUMBER = STORAGE_NUMBER;
    }

    //-------------------------------------------------------------------------------------------

    public Long getQUANTITY() {
        return QUANTITY;
    }

    public void setQUANTITY(Long QUANTITY) {
        this.QUANTITY = QUANTITY;
    }



}
