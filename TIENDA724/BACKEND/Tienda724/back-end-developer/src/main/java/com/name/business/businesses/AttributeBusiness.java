package com.name.business.businesses;

import com.name.business.DAOs.AttributeDAO;
import com.name.business.entities.*;
import com.name.business.entities.completes.AttributeComplete;
import com.name.business.representations.AttributeDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.attributeSanitation;
import static com.name.business.utils.constans.K.*;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoBooleanSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoStringSql;


public class AttributeBusiness {

    private AttributeDAO attributeDAO;
    private CommonBusiness  commonBusiness;
    private CommonStateBusiness commonStateBusiness;
    private AttributeValueBusiness attributeValueBusiness;

    public AttributeBusiness(AttributeDAO attributeDAO, CommonBusiness commonBusiness, CommonStateBusiness commonStateBusiness, AttributeValueBusiness attributeValueBusiness) {
        this.attributeDAO = attributeDAO;
        this.commonBusiness = commonBusiness;
        this.commonStateBusiness = commonStateBusiness;
        this.attributeValueBusiness=attributeValueBusiness;
    }
    /**
     *
     * @param attribute
     *
     * @return
     */
    public Either<IException,Object> getAttributes(Attribute attribute,Boolean is_values) {
        List<String> msn = new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting consults Attribute ||||||||||||||||| ");
            Attribute attributeSanitation = attributeSanitation(attribute);
            List<Attribute> attributeList= new ArrayList<>();
             attributeList = attributeDAO.read(attributeSanitation, attributeSanitation.getCommon(), attributeSanitation.getState());
            List<AttributeComplete> attributeComplteList = new ArrayList<>();
            List<AttributeValue> attributeValueListAux=new ArrayList<>();
            if(attributeList.size()>0 && formatoBooleanSql(is_values)!=null && formatoBooleanSql(is_values)){
                Either<IException, List<AttributeValue>> attributeValueIExceptionListEither = attributeValueBusiness.getAttributeValues(new AttributeValue(null, null,
                        new Common(null, null, null), new CommonState(null, 1, null, null)));

                if(attributeValueIExceptionListEither.isRight() && attributeValueIExceptionListEither.right().value().size()>0){
                    List<AttributeValue> attributeValueList = attributeValueIExceptionListEither.right().value();

                    for (int i = 0; i < attributeList.size(); i++) {
                        attributeValueListAux=new ArrayList<>();
                        for (AttributeValue value:attributeValueList){

                            if(attributeList.get(i).getId_attribute()==value.getId_attribute()){
                                attributeValueListAux.add(value);

                            }

                        }
                        if( attributeValueListAux.size()>0){
                            attributeComplteList.add(
                                    new AttributeComplete( attributeList.get(i),attributeValueListAux)
                            );
                        }

                    }
                    if( attributeComplteList.size()>0){
                        return Either.right(attributeComplteList);

                    }


                }

            }
            return Either.right(attributeList);
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException,Long> createAttribute(AttributeDTO attributeDTO){
        List<String> msn = new ArrayList<>();
        Long id_common = null;
        Long id_attribute = null;
        Long id_common_state = null;
        System.out.println(attributeDAO.getPkLast());
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Attibute  ||||||||||||||||| ");
            if (attributeDTO!=null){

                if (attributeDTO.getCommonDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonBusiness.createCommon(attributeDTO.getCommonDTO());
                    if (commonThirdEither.isRight()) {
                        id_common = commonThirdEither.right().value();
                    }
                }

                if (attributeDTO.getStateDTO() != null) {
                    Either<IException, Long> commonThirdEither = commonStateBusiness.createCommonState(attributeDTO.getStateDTO());
                    if (commonThirdEither.isRight()) {
                        id_common_state = commonThirdEither.right().value();
                    }
                }
                attributeDAO.create(formatoLongSql(id_common),formatoLongSql(id_common_state));
                id_attribute = attributeDAO.getPkLast();

                if(attributeDTO.getAttributeValueDTOS()!=null &&attributeDTO.getAttributeValueDTOS().size()>0){
                    attributeValueBusiness.createAttributeValue(id_attribute,attributeDTO.getAttributeValueDTOS());
                }

                return Either.right(id_attribute);
            }else{
                msn.add("It does not recognize Third, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    /**
     * @param id_attribute
     * @param attributeDTO
     * @return
     */
    public Either<IException, Long> updateAttribute(Long id_attribute, AttributeDTO attributeDTO) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        Attribute currentAttribute= null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting creation Attribute ||||||||||||||||| ");
            if ((id_attribute != null && id_attribute > 0) && attributeDTO!=null) {

                List<CommonSimple> readCommons = attributeDAO.readCommons(formatoLongSql(id_attribute));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }


                // Common basic info update
                commonBusiness.updateCommon(idCommons.getId_common(), attributeDTO.getCommonDTO());

                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), attributeDTO.getStateDTO());


                // Attribute update
                //attributeDAO.update(id_attribute,attributeDTO);
                if(attributeDTO.getAttributeValueDTOS()!=null &&attributeDTO.getAttributeValueDTOS().size()>0){
                    attributeValueBusiness.createAttributeValue(id_attribute,attributeDTO.getAttributeValueDTOS());
                }

                return Either.right(id_attribute);

            } else {
                msn.add("It does not recognize ID Person, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_attribute
     * @return
     */
    public Either<IException, Long> deleteAttribute(Long id_attribute) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete person ||||||||||||||||| ");
            if (id_attribute != null && id_attribute > 0) {
                List<CommonSimple> readCommons = attributeDAO.readCommons(formatoLongSql(id_attribute));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common
                attributeValueBusiness.deleteAttributeValue(null,id_attribute);

                return Either.right(id_attribute);
            } else {
                msn.add("It does not recognize ID documentType, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        if (id==null){
            return false;
        }
        try {
            validator = attributeDAO.getValidatorID(id);
            if (validator != 0)
                return true;
            else
                return false;
        }catch (Exception e){
            return false;
        }
    }


}
