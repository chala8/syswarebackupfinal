package com.name.business.mappers;


import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.InventoryName;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InventoryNameMapper implements ResultSetMapper<InventoryName> {
    @Override
    public InventoryName map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new InventoryName(
                resultSet.getLong("ID_TAX"),
                resultSet.getDouble("STANDARD_PRICE"),
                resultSet.getLong("ID_PRODUCT_STORE"),
                resultSet.getString("PRODUCT_STORE_NAME"),
                resultSet.getString("CODE"),
                resultSet.getLong("ID_INVENTORY_DETAIL"),
                resultSet.getString("OWNBARCODE"),
                resultSet.getString("PRODUCT_STORE_CODE")
        );
    }
}
