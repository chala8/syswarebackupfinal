package com.name.business.DAOs;

import com.name.business.entities.Common;
import com.name.business.mappers.CommonMapper;
import com.name.business.mappers.CommonSimpleMapper;
import com.name.business.representations.CommonDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.Date;
import java.util.List;

@RegisterMapper(CommonMapper.class)
public interface CommonDAO {

    @SqlQuery("SELECT ID_COMMON FROM COMMON WHERE ID_COMMON IN (SELECT MAX(COMMON.ID_COMMON) FROM COMMON)")
    Long getPkLast();

    @SqlUpdate("INSERT INTO COMMON ( NAME,DESCRIPTION) VALUES (:co.name,:co.description)")
    void create(@BindBean("co")CommonDTO commonDTO);


    /**
     *
     * @param id_common
     * @param commonDTO
     * @return
     */
    @SqlUpdate("UPDATE COMMON SET " +
            "NAME = :common.name, " +
            "DESCRIPTION = :common.description " +
            "WHERE (ID_COMMON=:id_common) ")
    int update(@Bind("id_common") Long id_common, @BindBean("common") CommonDTO commonDTO);

    @SqlQuery("SELECT * FROM COMMON CO " +
            "WHERE (CO.ID_COMMON=:co.id_common OR :co.id_common IS NULL )AND  " +
            "      (CO.NAME LIKE  :co.name OR :co.name IS NULL )AND " +
            "      (co.DESCRIPTION LIKE :co.description OR :co.description IS NULL )")
    List<Common> read(@BindBean("co") Common common);


    void delete(Long aLong, Date date);
}
