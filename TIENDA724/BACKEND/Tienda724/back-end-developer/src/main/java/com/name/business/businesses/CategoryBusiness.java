package com.name.business.businesses;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.name.business.DAOs.CategoryDAO;
import com.name.business.entities.*;
import com.name.business.representations.CategoryDTO;
import com.name.business.representations.CommonStateDTO;
import com.name.business.representations.ProductDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;
import org.apache.commons.configuration2.tree.NodeTreeWalker;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.categorySanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.*;

public class CategoryBusiness {

    private CategoryDAO categoryDAO;
    private CommonBusiness commonBusiness;
    private CommonStateBusiness commonStateBusiness;

    public CategoryBusiness(CategoryDAO categoryDAO, CommonBusiness commonBusiness, CommonStateBusiness commonStateBusiness) {
        this.categoryDAO = categoryDAO;
        this.commonBusiness = commonBusiness;
        this.commonStateBusiness = commonStateBusiness;
    }
    @Deprecated
    public Either<IException, List<Category>> getCategories1(Category category) {
        {
            List<String> msn = new ArrayList<>();
            try {

                System.out.println("|||||||||||| Starting consults Category ||||||||||||||||| ");
                Category categorySanitation = categorySanitation(category);
                return Either.right(categoryDAO.read(categorySanitation, categorySanitation.getCommon(), categorySanitation.getState()));

            } catch (Exception e) {

                e.printStackTrace();

                return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
            }
        }
    }

    public Either<IException, List<TreeNode>> getCategoriesTree(Category category, Boolean child,Boolean childs,Boolean father,Boolean fathers) {
        {
            List<String> msn = new ArrayList<>();
            List<TreeNode> treeNodeList= new ArrayList<>();
            TreeNode treeNode=null;
            try {
                System.out.println("|||||||||||| Starting consults Category ||||||||||||||||| ");
                Category categorySanitation = categorySanitation(category);
                List<Category> read = categoryDAO.read(categorySanitation, categorySanitation.getCommon(), categorySanitation.getState());
                int i=0;
                for (Category cat: read){
                    if(categorySanitation.getId_category_father()==null &&categorySanitation.getId_category()==null
                            &&categorySanitation.getId_third_category()==null){
                        if ( cat.getId_category_father()==null || cat.getId_category_father()<=0){
                            treeNode=new TreeNode(cat);
                            treeNode=builtCategoryTree(treeNode,true,false,false,false);
                            treeNodeList.add(treeNode);
                        }
                    }else{
                        System.out.println("---->, "+JSONObject.wrap(cat)+"\n\t");
                        treeNode=new TreeNode(cat);
                        System.out.println("---->, "+JSONObject.wrap(treeNode.getData())+"\n\t");
                        treeNode=builtCategoryTreeFather(treeNode,true);
                        treeNodeList.add(treeNode);
                    }
                }
                return Either.right(treeNodeList);
            } catch (Exception e) {
                e.printStackTrace();
                return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
            }
        }
    }

    private TreeNode builtCategoryTreeFather(TreeNode treeNode, Boolean child) {
        if(!child || treeNode.getData()==null){
            return treeNode;
        }else{


            Long id_category_father = ((Category) treeNode.getData()).getId_category_father();
            System.out.println("TREE NODE --> \n "+((Category) treeNode.getData()).getId_category());
            List<Category> categoryListFatherList = categoryDAO.read_fast(id_category_father,null , null, null);

            for (int i=0; i< categoryListFatherList.size(); i++){

                System.out.println("HERE FATHER--> \n "+JSONObject.wrap(categoryListFatherList.get(i)));

                TreeNode aux=new TreeNode(categoryListFatherList.get(i));
                aux.setParent(treeNode);

                if(categoryListFatherList.get(i).getId_category_father()==null || categoryListFatherList.get(i).getId_category_father()<=0){
                    child=false;
                }
                return builtCategoryTreeFather(aux,child);
            }

        }
        return treeNode;
    }



    private TreeNode builtCategoryTree(TreeNode treeNode, Boolean child, Boolean childs, Boolean father, Boolean fathers) {
            if(!child){
                return treeNode;
            }else{
                Long id_category = ((Category) treeNode.getData()).getId_category();
                List<Category> categoryList = categoryDAO.read_fast(null,id_category , null, null);
                if(categoryList.size()<=0){
                    return treeNode;
                }else{
                    List<Object> children = treeNode.getChildren();
                    for (int j=0; j< categoryList.size(); j++){
                        if (children!=null){
                            children.add(categoryList.get(j));
                        }else{
                            children= new ArrayList<>();
                            children.add(categoryList.get(j));
                        }
                    }
                    treeNode.setChildren(children);
                }

                for (int i=0; i< categoryList.size(); i++){
                    System.out.println(JSONObject.wrap(categoryList.get(i))+"\n\t");

                    TreeNode aux=new TreeNode(categoryList.get(i));

                    aux.setParent(treeNode);

                    if(categoryDAO.read_fast(null,categoryList.get(i).getId_category() , null, null).size()>0){
                        return builtCategoryTree(aux,(i+1<categoryList.size()),false,false,false);
                    }
                }
            }
            return treeNode;
        }




    public Either<IException,Long> createCategory(CategoryDTO categoryDTO) {
        Long id_common = null;
        Long id_common_state = null;
        Long id_category = null;
        List<String> msn = new ArrayList<>();

        try {
            System.out.println("|||||||||||| Starting creation Category  ||||||||||||||||| ");

            if (categoryDTO != null){

                if (categoryDTO.getId_category_father() != null) {
                    if (validatorID(categoryDTO.getId_category_father()).equals(false)) {
                        msn.add("It ID Category father does not exist register on  Category, probably it has bad ");
                        return Either.left(new BussinessException(messages_errors(msn)));
                    }
                }

                //Verificar si existe un common
                    if (categoryDTO.getCommonDTO() != null) {
                        Either<IException, Long> commonEither = commonBusiness.createCommon(categoryDTO.getCommonDTO());
                        if (commonEither.isRight()) {
                            id_common = commonEither.right().value();
                        }
                    }

                    //Verificar si viene un commonState

                    if (categoryDTO.getCommonStateDTO() != null) {
                        Either<IException, Long> commonStateEither = commonStateBusiness.createCommonState(categoryDTO.getCommonStateDTO());
                        if (commonStateEither.isRight()) {
                            id_common_state = commonStateEither.right().value();
                        }
                    }else{
                        Either<IException, Long> commonStateEither = commonStateBusiness.createCommonState(new CommonStateDTO(null,null,null));
                        if (commonStateEither.isRight()) {
                            id_common_state = commonStateEither.right().value();
                        }
                    }

                    //Crear el category
                    categoryDAO.create(categoryDTO, formatoLongSql(id_common), formatoLongSql(id_common_state));
                    id_category = categoryDAO.getPkLast();
                    return Either.right(id_category);

                } else {
                    msn.add("It does not recognize the Category, probably it has bad format");
                }
                return Either.left(new BussinessException(messages_errors(msn)));

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id
     * */
    public Boolean validatorID(Long id) {
        Integer validator = 0;
        try {
             validator = categoryDAO.getValidatorID(id);
            if (validator>0){
                return true;
            }
            return false;
        }catch (Exception e){
            return false;
        }
    }


    /**
     * @param id_category
     * @param categoryDTO
     * @return
     */
    public Either<IException,Long> updateCategory(Long id_category, CategoryDTO categoryDTO) {

        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        Category actualCategory=null;

        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting update Category ||||||||||||||||| ");
            if ((id_category != null && id_category > 0)) {


                List<Category> read = categoryDAO.read(
                        new Category(id_category, null, null, null, null,null),
                        new Common(null,null,null),
                        new CommonState(null,null,null,null)
                );

                if (read.size()<=0){
                    msn.add("It does not recognize ID Category , probably it has bad format");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                //TODO validate the  id attribute if exist on database a register with this id
                if (categoryDTO.getId_category_father() != null  && validatorID(categoryDTO.getId_category_father()).equals(false)) {
                    msn.add("It ID Category father does not exist register on  Category, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

                List<CommonSimple> readCommons = categoryDAO.readCommons(formatoLongSql(id_category));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }


                // Common basic info update
                commonBusiness.updateCommon(idCommons.getId_common(), categoryDTO.getCommonDTO());

                // Common  State business update
                commonStateBusiness.updateCommonState(idCommons.getId_common_state(), categoryDTO.getCommonStateDTO());

                actualCategory = read.get(0);
                // Validate First_lastname inserted, if null, set the First_lastname to the actual in the database, if not it is formatted to sql
                if (categoryDTO.getImg_url() == null || categoryDTO.getImg_url().isEmpty()) {
                    categoryDTO.setImg_url(actualCategory.getImg_url());
                } else {
                    categoryDTO.setImg_url(formatoStringSql(categoryDTO.getImg_url()));
                }

                if (categoryDTO.getId_category_father() != null && categoryDTO.getId_category_father()<1) {
                    categoryDTO.setId_category_father(actualCategory.getId_category_father());
                } else {
                    categoryDTO.setId_category_father(formatoLongSql(categoryDTO.getId_category_father()));
                }

                // Attribute update
                categoryDAO.update(id_category,categoryDTO);

                return Either.right(id_category);

            } else {
                msn.add("It does not recognize ID Category, probably it has bad format");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    /**
     * @param id_category
     * @return
     */
    public Either<IException, Long> deleteCategory(Long id_category) {
        List<String> msn = new ArrayList<>();
        CommonSimple idCommons = null;
        try {
            msn.add("OK");
            System.out.println("|||||||||||| Starting delete category ||||||||||||||||| ");
            if (id_category != null && id_category > 0) {
                if(validatorID(id_category) == false){
                    msn.add("It ID Category does not exist register on  Category, probably it has bad ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
                List<CommonSimple> readCommons = categoryDAO.readCommons(formatoLongSql(id_category));
                if (readCommons.size()>0){
                    idCommons = readCommons.get(0);
                }
                commonStateBusiness.deleteCommonState(idCommons.getId_common_state());
                //TODO when it would be determine how to delete common
                return Either.right(id_category);
            } else {
                msn.add("It does not recognize id_category, probably it has bad format");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


}
