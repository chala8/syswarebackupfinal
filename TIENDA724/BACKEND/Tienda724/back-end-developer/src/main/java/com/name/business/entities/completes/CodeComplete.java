package com.name.business.entities.completes;

import com.name.business.entities.CommonState;

import java.util.Date;

public class CodeComplete {
    private Long id_code;
    private String code;
    private String img;
    private Double suggested_price;
    private Long id_third_cod;

    // you can sent only product o product complete o product with category tree
    private Object product;
    private Object measure_unit;
    // you can sent attribute list simple or complete
    private Object attribute_list;


    private Long id_state_cod;
    private Integer state_cod;
    private Date creation_cod;
    private Date modify_cod;

    public CodeComplete(Long id_code, String code, String img, Double suggested_price, Long id_third_cod, Object product,
                        Object measure_unit, Object attribute_list, CommonState state) {
        this.id_code = id_code;
        this.code = code;
        this.img = img;
        this.suggested_price = suggested_price;
        this.id_third_cod = id_third_cod;
        this.product = product;
        this.measure_unit = measure_unit;
        this.attribute_list = attribute_list;
        this.id_state_cod = state.getId_common_state();
        this.state_cod= state.getState();
        this.creation_cod = state.getCreation_date();
        this.modify_cod = state.getModify_date();
    }

    public CommonState loadingCommonState() {
        return new CommonState(
                this.getId_state_cod(),
                this.getState_cod(),
                this.getCreation_cod(),
                this.getModify_cod()
        );
    }

    public void setCommonState(CommonState state) {
        this.id_state_cod=state.getId_common_state();
        this.state_cod=state.getState();
        this.modify_cod=state.getCreation_date();
        this.modify_cod=state.getModify_date();
    }

    public Long getId_code() {
        return id_code;
    }

    public void setId_code(Long id_code) {
        this.id_code = id_code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Double getSuggested_price() {
        return suggested_price;
    }

    public void setSuggested_price(Double suggested_price) {
        this.suggested_price = suggested_price;
    }

    public Long getId_third_cod() {
        return id_third_cod;
    }

    public void setId_third_cod(Long id_third_cod) {
        this.id_third_cod = id_third_cod;
    }

    public Object getProduct() {
        return product;
    }

    public void setProduct(Object product) {
        this.product = product;
    }

    public Object getMeasure_unit() {
        return measure_unit;
    }

    public void setMeasure_unit(Object measure_unit) {
        this.measure_unit = measure_unit;
    }

    public Object getAttribute_list() {
        return attribute_list;
    }

    public void setAttribute_list(Object attribute_list) {
        this.attribute_list = attribute_list;
    }

    public Long getId_state_cod() {
        return id_state_cod;
    }

    public void setId_state_cod(Long id_state_cod) {
        this.id_state_cod = id_state_cod;
    }

    public Integer getState_cod() {
        return state_cod;
    }

    public void setState_cod(Integer state_cod) {
        this.state_cod = state_cod;
    }

    public Date getCreation_cod() {
        return creation_cod;
    }

    public void setCreation_cod(Date creation_cod) {
        this.creation_cod = creation_cod;
    }

    public Date getModify_cod() {
        return modify_cod;
    }

    public void setModify_cod(Date modify_cod) {
        this.modify_cod = modify_cod;
    }
}
