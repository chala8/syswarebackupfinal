package com.name.business.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class excelRowCaja {


    private String caja_NUMBER;
    private String closing_DATE;
    private String fullname;
    private String notes;
    private String starting_DATE;
    private Long consecutive;
    private String MOVEMENT_DATE;
    private Long VALOR;
    private String NATURALEZA;
    private String NOTAS;
    private Long BALANCE;


    @JsonCreator
    public excelRowCaja(
            @JsonProperty("idcaja") Long codigo,
            @JsonProperty("caja_NUMBER") String caja_NUMBER,
            @JsonProperty("closing_DATE") String closing_DATE,
            @JsonProperty("fullname") String fullname,
            @JsonProperty("notes") String notes,
            @JsonProperty("starting_DATE") String starting_DATE,
            @JsonProperty("consecutive") Long consecutive,
            @JsonProperty("movement_DATE") String MOVEMENT_DATE,
            @JsonProperty("valor") Long VALOR,
            @JsonProperty("naturaleza") String NATURALEZA,
            @JsonProperty("balance") Long BALANCE,
            @JsonProperty("notas") String NOTAS) {
        this.BALANCE = BALANCE;
        this.caja_NUMBER = caja_NUMBER;
        this.closing_DATE = closing_DATE;
        this.fullname = fullname;
        this.notes = notes;
        this.starting_DATE = starting_DATE;
        this.consecutive = consecutive;
        this.MOVEMENT_DATE = MOVEMENT_DATE;
        this.VALOR = VALOR;
        this.NATURALEZA = NATURALEZA;
        this.NOTAS = NOTAS;
    }
    public Long getBALANCE() {
        return BALANCE;
    }

    public void setBALANCE(Long BALANCE) {
        this.BALANCE = BALANCE;
    }

    public String getMOVEMENT_DATE() {
        return MOVEMENT_DATE;
    }

    public void setMOVEMENT_DATE(String MOVEMENT_DATE) {
        this.MOVEMENT_DATE = MOVEMENT_DATE;
    }

    public Long getVALOR() {
        return VALOR;
    }

    public void setVALOR(Long VALOR) {
        this.VALOR = VALOR;
    }

    public String getNATURALEZA() {
        return NATURALEZA;
    }

    public void setNATURALEZA(String NATURALEZA) {
        this.NATURALEZA = NATURALEZA;
    }

    public String getNOTAS() {
        return NOTAS;
    }

    public void setNOTAS(String NOTAS) {
        this.NOTAS = NOTAS;
    }

    public String getcaja_NUMBER() {
        return caja_NUMBER;
    }

    public void setcaja_NUMBER(String caja_NUMBER) {
        this.caja_NUMBER = caja_NUMBER;
    }

    public String getclosing_DATE() {
        return closing_DATE;
    }

    public void setclosing_DATE(String closing_DATE) {
        this.closing_DATE = closing_DATE;
    }

    public String getfullname() {
        return fullname;
    }

    public void setfullname(String fullname) {
        this.fullname = fullname;
    }

    public String getnotes() {
        return notes;
    }

    public void setnotes(String notes) {
        this.notes = notes;
    }

    public String getstarting_DATE() {
        return starting_DATE;
    }

    public void setstarting_DATE(String starting_DATE) {
        this.starting_DATE = starting_DATE;
    }

    public Long getconsecutive() {
        return consecutive;
    }

    public void setconsecutive(Long consecutive) {
        this.consecutive = consecutive;
    }
}
