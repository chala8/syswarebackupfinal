package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class LegalDataDTO {

    private String RESOLUCION_DIAN;
    private String REGIMEN_TRIBUTARIO;
    private String AUTORETENEDOR;
    private String URL_LOGO;
    private Long CITY_NAME;
    private Long COUNTRY_NAME;
    private String ADDRESS;
    private Long PHONE1;
    private String EMAIL;
    private String PREFIX_BILL;
    private String NOTES;

    @JsonCreator
    public LegalDataDTO(@JsonProperty("resolucion_dian")String RESOLUCION_DIAN,
                        @JsonProperty("regimen_tributario")String REGIMEN_TRIBUTARIO,
                        @JsonProperty("autoretenedor")String AUTORETENEDOR,
                        @JsonProperty("url_logo")String URL_LOGO,
                        @JsonProperty("id_city")Long CITY_NAME,
                        @JsonProperty("id_country")Long COUNTRY_NAME,
                        @JsonProperty("address")String ADDRESS,
                        @JsonProperty("phone1")Long PHONE1,
                        @JsonProperty("email")String EMAIL,
                        @JsonProperty("prefix_bill")String PREFIX_BILL,
                        @JsonProperty("notes")String NOTES) {
        this.RESOLUCION_DIAN = RESOLUCION_DIAN;
        this.REGIMEN_TRIBUTARIO = REGIMEN_TRIBUTARIO;
        this.AUTORETENEDOR = AUTORETENEDOR;
        this.URL_LOGO = URL_LOGO;
        this.CITY_NAME = CITY_NAME;
        this.COUNTRY_NAME = COUNTRY_NAME;
        this.ADDRESS = ADDRESS;
        this.PHONE1 = PHONE1;
        this.EMAIL = EMAIL;
        this.PREFIX_BILL = PREFIX_BILL;
        this.NOTES = NOTES;


    }

    //--------------------------------------------------------------------------

    public String getRESOLUCION_DIAN() { return RESOLUCION_DIAN; }

    public void setRESOLUCION_DIAN(String RESOLUCION_DIAN) { this.RESOLUCION_DIAN = RESOLUCION_DIAN; }

    //--------------------------------------------------------------------------

    public String getREGIMEN_TRIBUTARIO() { return REGIMEN_TRIBUTARIO; }

    public void setREGIMEN_TRIBUTARIO(String REGIMEN_TRIBUTARIO) { this.REGIMEN_TRIBUTARIO = REGIMEN_TRIBUTARIO; }

    //--------------------------------------------------------------------------

    public String getAUTORETENEDOR() { return AUTORETENEDOR; }

    public void setAUTORETENEDOR(String AUTORETENEDOR) { this.AUTORETENEDOR = AUTORETENEDOR; }

    //--------------------------------------------------------------------------

    public String getURL_LOGO() { return URL_LOGO; }

    public void setURL_LOGO(String URL_LOGO) { this.URL_LOGO = URL_LOGO; }

    //--------------------------------------------------------------------------

    public Long getCITY_NAME() { return CITY_NAME; }

    public void setCITY_NAME(Long CITY_NAME) { this.CITY_NAME = CITY_NAME; }

    //--------------------------------------------------------------------------

    public Long getCOUNTRY_NAME() { return COUNTRY_NAME; }

    public void setCOUNTRY_NAME(Long COUNTRY_NAME) { this.COUNTRY_NAME = COUNTRY_NAME; }

    //--------------------------------------------------------------------------

    public String getADDRESS() { return ADDRESS; }

    public void setADDRESS(String ADDRESS) { this.ADDRESS = ADDRESS; }

    //--------------------------------------------------------------------------

    public Long getPHONE1() { return PHONE1; }

    public void setPHONE1(Long PHONE1) { this.PHONE1 = PHONE1; }

    //--------------------------------------------------------------------------

    public String getEMAIL() { return EMAIL; }

    public void setEMAIL(String EMAIL) { this.EMAIL = EMAIL; }

    //--------------------------------------------------------------------------

    public String getPREFIX_BILL() { return PREFIX_BILL; }

    public void setPREFIX_BILL(String PREFIX_BILL) { this.PREFIX_BILL = PREFIX_BILL; }

    //--------------------------------------------------------------------------

    public String getNOTES() { return NOTES; }

    public void setNOTES(String NOTES) { this.NOTES = NOTES; }

    //--------------------------------------------------------------------------



}
