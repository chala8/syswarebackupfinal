package com.name.business.businesses;

import java.util.Locale;
import java.text.NumberFormat;

import com.name.business.DAOs.ReportsDAO;
import com.name.business.representations.DatoDTO;
import com.name.business.representations.thirdReportDataDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import com.name.business.entities.*;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.pdf.PdfPTable;
import java.io.FileOutputStream;


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.util.CellRangeAddress;




import static com.name.business.utils.constans.K.*;

/**
 * Created by luis on 19/04/17.
 */
public class ReportsBusiness {

    private ReportsDAO reportsDAO;

    public ReportsBusiness(ReportsDAO reportsDAO) {
        this.reportsDAO = reportsDAO;
    }


    public Either<IException, String> printPDF(){
        // Se crea el documento
        Document documento = new Document();
        String response = "Bad!!!";
         try{
        // Se crea el OutputStream para el fichero donde queremos dejar el pdf.
                FileOutputStream ficheroPdf = new FileOutputStream("fichero.pdf");

        // Se asocia el documento al OutputStream y se indica que el espaciado entre
        // lineas sera de 20. Esta llamada debe hacerse antes de abrir el documento
                PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);

        // Se abre el documento.
                documento.open();
                documento.add(new Paragraph("Esto es el primer párrafo, normalito"));

                documento.add(new Paragraph("Este es el segundo y tiene una fuente rara",
                        FontFactory.getFont("arial",   // fuente
                                22,                            // tamaño
                                Font.ITALIC,                   // estilo
                                BaseColor.CYAN)));             // color
                PdfPTable tabla = new PdfPTable(3);
                for (int i = 0; i < 15; i++)
                {
                    tabla.addCell("celda " + i);
                }
                response = "Good!!!";
        documento.add(tabla);}catch(Exception e){
        }
        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <CategoryReport>> getTotalesFacturacionPorPeriodoCategorias(Long id_third,
                                                                             Long id_store,
                                                                             Date date1,
                                                                             Date date2) {
        List <CategoryReport> dataResponse = reportsDAO.getTotalesFacturacionPorPeriodoCategorias(id_third,id_store,date1,increaseDate(date2,1));
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List <CategoryReport>> getTotalesFacturacionPorPeriodoProductos(Long id_third,
                                                                                               Long id_store,
                                                                                               Date date1,
                                                                                               Date date2) {
        List <CategoryReport> dataResponse = reportsDAO.getTotalesFacturacionPorPeriodoProductos(id_third,id_store,date1,increaseDate(date2,1));
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List <BillReport>> getTotalesFacturacionPorPeriodo(Long id_third,
                                                                                 Long id_store,
                                                                                 String date1,
                                                                                 String date2,
                                                                                 Long typemove) {
        List <BillReport> dataResponse = reportsDAO.getTotalesFacturacionPorPeriodo(id_third,id_store,date1,date2,typemove);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List <DateReport>> getBillTotal(Long id_bill_type,
                                                              Long id_period,
                                                              Date date1,
                                                              Date date2,
                                                              Long id_third,
                                                              List<Long> listStore) {
        List <DateReport> dataResponse = new ArrayList<DateReport>();
        int cont = 1;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy kk:mm");
        Date tmp = increaseDate(date2,1);
        if(id_period == 4){
            tmp = date1;
            for(int i =0;i<23;i++){
                tmp = increaseDate(tmp,4);
            }

            System.out.println("this is the end date:"+tmp.toString());
            Date date3 = date1;
            for(int i=0;i<7;i++){
                date3 = increaseDate(date3,4);
            }
            System.out.println("this is the start date:"+date3.toString());
            Date date4 = increaseDate(date3,id_period);
            for (Date tmp2 = date3, tmp3=date4 ; tmp2.before(tmp) ; tmp2 = increaseDate(tmp2,id_period), tmp3 = increaseDate(tmp3,id_period)) {
                DateReport temp = reportsDAO.getBillTotal(id_bill_type,tmp2,tmp3, id_third, listStore);
                temp.setlabelDate(format.format(tmp3));
                dataResponse.add(temp);
            }
        }else{
        for (Date date3 = date1, date4 = increaseDate(date1,id_period) ; date3.before(tmp) ; date3 = increaseDate(date3,id_period), date4 = increaseDate(date4,id_period)) {
            DateReport temp = reportsDAO.getBillTotal(id_bill_type,date3,date4, id_third, listStore);
            temp.setCounter(cont);
            temp.setlabelDate(format.format(increaseDate(date4,-1)));
            dataResponse.add(temp);
            cont++;
        }
        }
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List <LabelReport>> getTotalByBillTypeProduct(Long id_bill_type,
                                                                             Long id_product_store,
                                                                             Long id_period,
                                                                             Date date1,
                                                                             Date date2,
                                                                            List<Long> id_store) {
        List <LabelReport> dataResponse = new ArrayList<LabelReport>();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy kk:mm");
        Date tmp = increaseDate(date2,1);
        if(id_period == 4){
            tmp = date1;
            for(int i =0;i<23;i++){
                tmp = increaseDate(tmp,4);
            }

            System.out.println("this is the end date:"+tmp.toString());
            Date date3 = date1;
            for(int i=0;i<7;i++){
                date3 = increaseDate(date3,4);
            }
            System.out.println("this is the start date:"+date3.toString());
            Date date4 = increaseDate(date3,id_period);
            for (Date tmp2 = date3, tmp3=date4 ; tmp2.before(tmp) ; tmp2 = increaseDate(tmp2,id_period), tmp3 = increaseDate(tmp3,id_period)) {
                LabelReport temp = reportsDAO.getTotalByBillTypeProduct(id_bill_type,id_product_store,tmp2,tmp3,id_store);
                temp.setlabel(format.format(tmp3));

                System.out.println(tmp3.toString());
                dataResponse.add(temp);
            }
        }else{
        for (Date date3 = date1, date4 = increaseDate(date1,id_period) ; date3.before(tmp) ; date3 = increaseDate(date3,id_period), date4 = increaseDate(date4,id_period)) {
            LabelReport temp = reportsDAO.getTotalByBillTypeProduct(id_bill_type,id_product_store,date3,date4,id_store);
            temp.setlabel(format.format(increaseDate(date4,-1)));
            dataResponse.add(temp);
        }
        }

        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List <LabelReport>> getTotalByBillTypeCategory(Long id_bill_type,
                                                                             Long id_category,
                                                                             Long id_period,
                                                                             Date date1,
                                                                             Date date2,
                                                                             Long id_third,
                                                                             List<Long> id_store
                                                                             ) {
        List <LabelReport> dataResponse = new ArrayList<LabelReport>();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy kk:mm" );
        Date tmp = increaseDate(date2,1);
        if(id_period == 4){
            tmp = date1;
            for(int i =0;i<23;i++){
                tmp = increaseDate(tmp,4);
            }

            System.out.println("this is the end date:"+tmp.toString());
            Date date3 = date1;
            for(int i=0;i<7;i++){
                date3 = increaseDate(date3,4);
            }
            System.out.println("this is the start date:"+date3.toString());
            Date date4 = increaseDate(date3,id_period);
            for (Date tmp2 = date3, tmp3=date4 ; tmp2.before(tmp) ; tmp2 = increaseDate(tmp2,id_period), tmp3 = increaseDate(tmp3,id_period)) {
                LabelReport temp = reportsDAO.getTotalByBillTypeCategory(id_bill_type,id_category,tmp2,tmp3,id_third, id_store);
                temp.setlabel(format.format(tmp3));
                dataResponse.add(temp);
            }
        }else{
        for (Date date3 = date1, date4 = increaseDate(date1,id_period) ; date3.before(tmp) ; date3 = increaseDate(date3,id_period), date4 = increaseDate(date4,id_period)) {
            LabelReport temp = reportsDAO.getTotalByBillTypeCategory(id_bill_type,id_category,date3,date4, id_third, id_store);
            temp.setlabel(format.format(increaseDate(date4,-1)));
            dataResponse.add(temp);
        }
        }
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    private Date increaseDate (Date a, long id_period) {
        Calendar c = Calendar.getInstance();
        c.setTime(a);
        if(id_period == -1)
        {c.add(Calendar.DAY_OF_MONTH,-1);}
        if(id_period == 1)
        {c.add(Calendar.DAY_OF_MONTH,1);}
        if(id_period == 2)
        {c.add(Calendar.DAY_OF_MONTH,7);}
        if(id_period == 3)
        {c.add(Calendar.MONTH, 1);}
        if(id_period == 4)
        {c.add(Calendar.HOUR_OF_DAY, 1);}
        Date response = c.getTime();
        return response;
    }

    public Either<IException, DateReport2> getTotalByCategory(Long id_bill_type,
                                                       Date date1,
                                                       Date date2) {

        try {
            return Either.right(reportsDAO.getTotalByCategory(id_bill_type,date1,date2));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, DateReport2> getTotalByBrand(Long id_bill_type,
                                                       Date date1,
                                                       Date date2) {

        try {
            return Either.right(reportsDAO.getTotalByBrand(id_bill_type,date1,date2));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <DateReport2>> getTotalByProduct(Long id_bill_type,
                                                       Date date1,
                                                       Date date2, Long id_store) {

        try {
            return Either.right(reportsDAO.getTotalByProduct(id_bill_type,date1,date2,id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <Price>> getPriceByPS(Long id_ps) {

        try {
            return Either.right(reportsDAO.getPriceByPS(id_ps));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <InvetoryReport>> getInventarioCons(List<Long> listStore,
                                                                       List<Long> listLine,
                                                                       List<Long> listCategory,
                                                                       List<Long> listBrand){

        try {
            Long response = reportsDAO.get5Mil(listStore);
            System.out.println(response);
            return Either.right(reportsDAO.getInventarioCons(reportsDAO.get5Mil(listStore),listStore,listLine,listCategory,listBrand));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <DiasRotacion>> getDiasRotacion(    Long days,
                                                                       List<Long> listStore,
                                                                       List<Long> listLine,
                                                                       List<Long> listCategory,
                                                                       List<Long> listBrand){

        try {
            System.out.println(listStore);
            System.out.println(listLine);
            System.out.println(listCategory);
            System.out.println(listBrand);
            return Either.right(reportsDAO.getDiasRotacion(days,listStore,listLine,listCategory,listBrand));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <Brand>> getBrands(){

        try {
            return Either.right(reportsDAO.getBrands());
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List <Long>> getSonCat(List<Long> listStore){

        try {
            Long response = reportsDAO.get5Mil(listStore);
            System.out.println(response);
            return Either.right(reportsDAO.getSonCat(listStore));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, String> postExcel(ArrayList<excelRow> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);


        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Producto");
            celdaprodT.setCellValue(textoT7);


            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Linea");
            celdalineaT.setCellValue(textoT4);


            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Categoria");
            celdacateT.setCellValue(textoT5);


            HSSFCell celdamarcaT = filaTitulo.createCell((short)3);
            HSSFRichTextString textoT6 = new HSSFRichTextString("Marca");
            celdamarcaT.setCellValue(textoT6);



            HSSFCell celdaCantT = filaTitulo.createCell((short)4);
            HSSFRichTextString textoT = new HSSFRichTextString("Cantidad");
            celdaCantT.setCellValue(textoT);


            HSSFCell celdacostT = filaTitulo.createCell((short)5);
            HSSFRichTextString textoT1 = new HSSFRichTextString("Costo");
            celdacostT.setCellValue(textoT1);


            HSSFCell celdacosttotT = filaTitulo.createCell((short)6);
            HSSFRichTextString textoT2 = new HSSFRichTextString("Costo Total");
            celdacosttotT.setCellValue(textoT2);


            HSSFCell celdapctT = filaTitulo.createCell((short)7);
            HSSFRichTextString textoT3 = new HSSFRichTextString("PCT");
            celdapctT.setCellValue(textoT3);


            HSSFCell celdapctT2 = filaTitulo.createCell((short)8);
            HSSFRichTextString textoT32 = new HSSFRichTextString("Codigo de Barras");
            celdapctT2.setCellValue(textoT32);


            HSSFCell celdapctT3 = filaTitulo.createCell((short)9);
            HSSFRichTextString textoT33 = new HSSFRichTextString("Codigo tienda");
            celdapctT3.setCellValue(textoT33);


            HSSFCell celdapctT4 = filaTitulo.createCell((short)10);
            HSSFRichTextString textoT34 = new HSSFRichTextString("Precio");
            celdapctT4.setCellValue(textoT34);


            int cont = 1;
            for(excelRow element: list){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getproducto()+"");
                celdaprod.setCellValue(texto7);


                HSSFCell celdalinea = fila.createCell((short)1);
                HSSFRichTextString texto4 = new HSSFRichTextString(element.getlinea()+"");
                celdalinea.setCellValue(texto4);


                HSSFCell celdacate = fila.createCell((short)2);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getcategoria()+"");
                celdacate.setCellValue(texto5);


                HSSFCell celdamarca = fila.createCell((short)3);
                HSSFRichTextString texto6 = new HSSFRichTextString(element.getmarca()+"");
                celdamarca.setCellValue(texto6);



                HSSFCell celdaCant = fila.createCell((short)4);
                HSSFRichTextString texto = new HSSFRichTextString(element.getcantidad()+"");
                celdaCant.setCellValue(texto);


                HSSFCell celdacost = fila.createCell((short)5);
                HSSFRichTextString texto1 = new HSSFRichTextString(fmt.format(element.getcosto()).substring(1,fmt.format(element.getcosto()).length()));
                celdacost.setCellValue(texto1);


                HSSFCell celdacosttot = fila.createCell((short)6);
                HSSFRichTextString texto2 = new HSSFRichTextString(fmt.format(element.getcostototal()).substring(1,fmt.format(element.getcostototal()).length()));
                celdacosttot.setCellValue(texto2);


                HSSFCell celdapct = fila.createCell((short)7);
                HSSFRichTextString texto3 = new HSSFRichTextString(element.getpct_inventario()+"");
                celdapct.setCellValue(texto3);


                HSSFCell celdapct2 = fila.createCell((short)8);
                HSSFRichTextString texto32 = new HSSFRichTextString(element.getbarcode()+"");
                celdapct2.setCellValue(texto32);


                HSSFCell celdapct3 = fila.createCell((short)9);
                HSSFRichTextString texto33 = new HSSFRichTextString(element.getcodigotienda()+"");
                celdapct3.setCellValue(texto33);


                HSSFCell celdapct4 = fila.createCell((short)10);
                HSSFRichTextString texto34 = new HSSFRichTextString(element.getprecio()+"");
                celdapct4.setCellValue(texto34);



                System.out.println(
                        element.getcantidad()+","+
                        element.getcosto()+","+
                        element.getcostototal()+","+
                        element.getpct_inventario()+","+
                        element.getlinea()+","+
                        element.getcategoria()+","+
                        element.getmarca()+","+
                        element.getproducto()+".");

                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/reportes/"+Name+"inventario.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+".xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"inventario.xls";
        }catch (Exception e){
            System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, String> postExcelThirds(ArrayList<thirdReportDataDTO> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);


        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Nombre");
            celdaprodT.setCellValue(textoT7);


            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Documento");
            celdalineaT.setCellValue(textoT4);


            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            HSSFRichTextString textoT5 = new HSSFRichTextString("# Transacciones");
            celdacateT.setCellValue(textoT5);


            HSSFCell celdamarcaT = filaTitulo.createCell((short)3);
            HSSFRichTextString textoT6 = new HSSFRichTextString("Total Transacciones");
            celdamarcaT.setCellValue(textoT6);



            HSSFCell celdaCantT = filaTitulo.createCell((short)4);
            HSSFRichTextString textoT = new HSSFRichTextString("Direccion");
            celdaCantT.setCellValue(textoT);


            HSSFCell celdacostT = filaTitulo.createCell((short)5);
            HSSFRichTextString textoT1 = new HSSFRichTextString("Telefono");
            celdacostT.setCellValue(textoT1);


            HSSFCell celdacosttotT = filaTitulo.createCell((short)6);
            HSSFRichTextString textoT2 = new HSSFRichTextString("Correo");
            celdacosttotT.setCellValue(textoT2);



            int cont = 1;
            for(thirdReportDataDTO element: list){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getfullname()+"");
                celdaprod.setCellValue(texto7);


                HSSFCell celdalinea = fila.createCell((short)1);
                HSSFRichTextString texto4 = new HSSFRichTextString(element.getdocument_TYPE()+"-"+element.getdocument_NUMBER());
                celdalinea.setCellValue(texto4);


                HSSFCell celdacate = fila.createCell((short)2);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getnumventas()+"");
                celdacate.setCellValue(texto5);


                HSSFCell celdamarca = fila.createCell((short)3);
                HSSFRichTextString texto6 = new HSSFRichTextString(fmt.format(element.gettotalventas())+"");
                celdamarca.setCellValue(texto6);



                HSSFCell celdaCant = fila.createCell((short)4);
                HSSFRichTextString texto = new HSSFRichTextString(element.getaddress()+"");
                celdaCant.setCellValue(texto);


                HSSFCell celdacost = fila.createCell((short)5);
                HSSFRichTextString texto1 = new HSSFRichTextString(element.getphone());
                celdacost.setCellValue(texto1);


                HSSFCell celdacosttot = fila.createCell((short)6);
                HSSFRichTextString texto2 = new HSSFRichTextString(element.getmail());
                celdacosttot.setCellValue(texto2);



                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/reportes/"+Name+"terceros.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+"terceros.xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"terceros.xls";
        }catch (Exception e){
            System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, String> postExcelRotacion(ArrayList<excelRowRot> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);


        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Producto");
            celdaprodT.setCellValue(textoT7);


            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Linea");
            celdalineaT.setCellValue(textoT4);


            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Categoria");
            celdacateT.setCellValue(textoT5);


            HSSFCell celdamarcaT = filaTitulo.createCell((short)3);
            HSSFRichTextString textoT6 = new HSSFRichTextString("Marca");
            celdamarcaT.setCellValue(textoT6);



            HSSFCell celdaCantT = filaTitulo.createCell((short)4);
            HSSFRichTextString textoT = new HSSFRichTextString("Cantidad Actual");
            celdaCantT.setCellValue(textoT);


            HSSFCell celdacostT = filaTitulo.createCell((short)5);
            HSSFRichTextString textoT1 = new HSSFRichTextString("Cantidad Vendida");
            celdacostT.setCellValue(textoT1);


            HSSFCell celdacosttotT = filaTitulo.createCell((short)6);
            HSSFRichTextString textoT2 = new HSSFRichTextString("Dias Rotacion");
            celdacosttotT.setCellValue(textoT2);



            int cont = 1;
            for(excelRowRot element: list){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getproducto()+"");
                celdaprod.setCellValue(texto7);


                HSSFCell celdalinea = fila.createCell((short)1);
                HSSFRichTextString texto4 = new HSSFRichTextString(element.getlinea()+"");
                celdalinea.setCellValue(texto4);


                HSSFCell celdacate = fila.createCell((short)2);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getcategoria()+"");
                celdacate.setCellValue(texto5);


                HSSFCell celdamarca = fila.createCell((short)3);
                HSSFRichTextString texto6 = new HSSFRichTextString(element.getmarca()+"");
                celdamarca.setCellValue(texto6);



                HSSFCell celdaCant = fila.createCell((short)4);
                HSSFRichTextString texto = new HSSFRichTextString(element.getcantidadactual()+"");
                celdaCant.setCellValue(texto);


                HSSFCell celdacost = fila.createCell((short)5);
                HSSFRichTextString texto1 = new HSSFRichTextString(element.getcantidadvendida()+"");
                celdacost.setCellValue(texto1);


                HSSFCell celdacosttot = fila.createCell((short)6);
                HSSFRichTextString texto2 = new HSSFRichTextString(element.getdias_rotacion()+"");
                celdacosttot.setCellValue(texto2);



                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/reportes/"+Name+"rotacion.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+"rotacion.xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"rotacion.xls";
        }catch (Exception e){
            System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String> postExcelProductos(ArrayList<excelRowProd> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);


        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Producto");
            celdaprodT.setCellValue(textoT7);


            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Ventas");
            celdalineaT.setCellValue(textoT4);


            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Costo");
            celdacateT.setCellValue(textoT5);


            HSSFCell celdamarcaT = filaTitulo.createCell((short)3);
            HSSFRichTextString textoT6 = new HSSFRichTextString("Utilidades");
            celdamarcaT.setCellValue(textoT6);



            HSSFCell celdaCantT = filaTitulo.createCell((short)4);
            HSSFRichTextString textoT = new HSSFRichTextString("Margen");
            celdaCantT.setCellValue(textoT);


            HSSFCell celdacostT = filaTitulo.createCell((short)5);
            HSSFRichTextString textoT1 = new HSSFRichTextString("# Ventas");
            celdacostT.setCellValue(textoT1);


            int cont = 1;
            for(excelRowProd element: list){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getcategoria()+"");
                celdaprod.setCellValue(texto7);


                HSSFCell celdalinea = fila.createCell((short)1);
                HSSFRichTextString texto4 = new HSSFRichTextString(element.getventa()+"");
                celdalinea.setCellValue(texto4);


                HSSFCell celdacate = fila.createCell((short)2);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getcosto()+"");
                celdacate.setCellValue(texto5);


                HSSFCell celdamarca = fila.createCell((short)3);
                HSSFRichTextString texto6 = new HSSFRichTextString(element.getutilidad()+"");
                celdamarca.setCellValue(texto6);



                HSSFCell celdaCant = fila.createCell((short)4);
                HSSFRichTextString texto = new HSSFRichTextString(element.getpct_margen()+"");
                celdaCant.setCellValue(texto);


                HSSFCell celdacost = fila.createCell((short)5);
                HSSFRichTextString texto1 = new HSSFRichTextString(element.getnumventas()+"");
                celdacost.setCellValue(texto1);


                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/reportes/"+Name+"productos.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+"productos.xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"productos.xls";
        }catch (Exception e){
            System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String> postExcelCategorias(ArrayList<excelRowProd> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);


        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Categoria");
            celdaprodT.setCellValue(textoT7);


            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Ventas");
            celdalineaT.setCellValue(textoT4);


            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Costo");
            celdacateT.setCellValue(textoT5);


            HSSFCell celdamarcaT = filaTitulo.createCell((short)3);
            HSSFRichTextString textoT6 = new HSSFRichTextString("Utilidades");
            celdamarcaT.setCellValue(textoT6);



            HSSFCell celdaCantT = filaTitulo.createCell((short)4);
            HSSFRichTextString textoT = new HSSFRichTextString("Margen");
            celdaCantT.setCellValue(textoT);


            HSSFCell celdacostT = filaTitulo.createCell((short)5);
            HSSFRichTextString textoT1 = new HSSFRichTextString("# Ventas");
            celdacostT.setCellValue(textoT1);


            int cont = 1;
            for(excelRowProd element: list){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getcategoria()+"");
                celdaprod.setCellValue(texto7);


                HSSFCell celdalinea = fila.createCell((short)1);
                HSSFRichTextString texto4 = new HSSFRichTextString(element.getventa()+"");
                celdalinea.setCellValue(texto4);


                HSSFCell celdacate = fila.createCell((short)2);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getcosto()+"");
                celdacate.setCellValue(texto5);


                HSSFCell celdamarca = fila.createCell((short)3);
                HSSFRichTextString texto6 = new HSSFRichTextString(element.getutilidad()+"");
                celdamarca.setCellValue(texto6);



                HSSFCell celdaCant = fila.createCell((short)4);
                HSSFRichTextString texto = new HSSFRichTextString(element.getpct_margen()+"");
                celdaCant.setCellValue(texto);


                HSSFCell celdacost = fila.createCell((short)5);
                HSSFRichTextString texto1 = new HSSFRichTextString(element.getnumventas()+"");
                celdacost.setCellValue(texto1);


                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/reportes/"+Name+"categorias.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+"categorias.xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"categorias.xls";
        }catch (Exception e){
            System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String> postExcelFacturas(ArrayList<excelRowFact> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);


        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Consecutivo");
            celdaprodT.setCellValue(textoT7);


            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Fecha Factura");
            celdalineaT.setCellValue(textoT4);


            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Ventas");
            celdacateT.setCellValue(textoT5);


            HSSFCell celdamarcaT = filaTitulo.createCell((short)3);
            HSSFRichTextString textoT6 = new HSSFRichTextString("Costo");
            celdamarcaT.setCellValue(textoT6);


            HSSFCell celdaCantT = filaTitulo.createCell((short)4);
            HSSFRichTextString textoT = new HSSFRichTextString("Utilidades");
            celdaCantT.setCellValue(textoT);


            HSSFCell celdacostT = filaTitulo.createCell((short)5);
            HSSFRichTextString textoT1 = new HSSFRichTextString("Margen");
            celdacostT.setCellValue(textoT1);

            HSSFCell celdacostf = filaTitulo.createCell((short)5);
            HSSFRichTextString textoTf = new HSSFRichTextString("Estado Factura");
            celdacostf.setCellValue(textoTf);

            int cont = 1;
            for(excelRowFact element: list){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getfullname()+"-"+element.getprefix_BILL()+"-"+element.getconsecutive());
                celdaprod.setCellValue(texto7);


                HSSFCell celdalinea = fila.createCell((short)1);
                HSSFRichTextString texto4 = new HSSFRichTextString(element.getpurchase_DATE()+"");
                celdalinea.setCellValue(texto4);


                HSSFCell celdacate = fila.createCell((short)2);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getventa()+"");
                celdacate.setCellValue(texto5);


                HSSFCell celdamarca = fila.createCell((short)3);
                HSSFRichTextString texto6 = new HSSFRichTextString(element.getcosto()+"");
                celdamarca.setCellValue(texto6);


                HSSFCell celdaCant = fila.createCell((short)4);
                HSSFRichTextString texto = new HSSFRichTextString(element.getutilidad()+"");
                celdaCant.setCellValue(texto);


                HSSFCell celdacost = fila.createCell((short)5);
                HSSFRichTextString texto1 = new HSSFRichTextString(element.getpct_margen_costo()+"");
                celdacost.setCellValue(texto1);

                HSSFCell celdacostff = fila.createCell((short)5);
                Long a = element.getid_BILL_STATE();
                if (a==1){
                    HSSFRichTextString texto1ff = new HSSFRichTextString("F");
                    celdacostff.setCellValue(texto1ff);
                }else{
                    if (a==41){
                        HSSFRichTextString texto1ff = new HSSFRichTextString("AD");
                        celdacostff.setCellValue(texto1ff);
                    }
                }


                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/reportes/"+Name+"facturas.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+"facturas.xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"facturas.xls";
        }catch (Exception e){
            System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String> postExcelCaja(ArrayList<excelRowCaja> list){
        // Se crea el documento
        String response = "0";
        HSSFWorkbook libro = new HSSFWorkbook();
        HSSFSheet hoja = libro.createSheet();
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);


        try {

            HSSFRow filaTitulo = hoja.createRow(0);

            HSSFCell celdaprodT = filaTitulo.createCell((short)0);
            HSSFRichTextString textoT7 = new HSSFRichTextString("Consecutivo");
            celdaprodT.setCellValue(textoT7);


            HSSFCell celdalineaT = filaTitulo.createCell((short)1);
            HSSFRichTextString textoT4 = new HSSFRichTextString("Fecha Apertura");
            celdalineaT.setCellValue(textoT4);


            HSSFCell celdacateT = filaTitulo.createCell((short)2);
            HSSFRichTextString textoT5 = new HSSFRichTextString("Fecha Cierre");
            celdacateT.setCellValue(textoT5);

            HSSFCell celdacateT2 = filaTitulo.createCell((short)3);
            HSSFRichTextString textoT52 = new HSSFRichTextString("Balance");
            celdacateT2.setCellValue(textoT52);


            HSSFCell celdamarcaT = filaTitulo.createCell((short)4);
            HSSFRichTextString textoT6 = new HSSFRichTextString("Cajero");
            celdamarcaT.setCellValue(textoT6);


            HSSFCell celdaCantT = filaTitulo.createCell((short)5);
            HSSFRichTextString textoT = new HSSFRichTextString("Caja");
            celdaCantT.setCellValue(textoT);


            HSSFCell celdacostT = filaTitulo.createCell((short)6);
            HSSFRichTextString textoT1 = new HSSFRichTextString("Notas Cierre");
            celdacostT.setCellValue(textoT1);


            HSSFCell celdacostT2 = filaTitulo.createCell((short)7);
            HSSFRichTextString textoT12 = new HSSFRichTextString("Fecha Movimiento");
            celdacostT2.setCellValue(textoT12);


            HSSFCell celdacostT3 = filaTitulo.createCell((short)8);
            HSSFRichTextString textoT13 = new HSSFRichTextString("Valor");
            celdacostT3.setCellValue(textoT13);


            HSSFCell celdacostT4 = filaTitulo.createCell((short)9);
            HSSFRichTextString textoT14 = new HSSFRichTextString("Naturaleza");
            celdacostT4.setCellValue(textoT14);


            HSSFCell celdacostT5 = filaTitulo.createCell((short)10);
            HSSFRichTextString textoT15 = new HSSFRichTextString("Notas Movimiento");
            celdacostT5.setCellValue(textoT15);


            int cont = 1;
            for(excelRowCaja element: list){

                HSSFRow fila = hoja.createRow(cont);

                HSSFCell celdaprod = fila.createCell((short)0);
                HSSFRichTextString texto7 = new HSSFRichTextString(element.getconsecutive()+"");
                celdaprod.setCellValue(texto7);


                HSSFCell celdalinea = fila.createCell((short)1);
                HSSFRichTextString texto4 = new HSSFRichTextString(element.getstarting_DATE()+"");
                celdalinea.setCellValue(texto4);


                HSSFCell celdacate = fila.createCell((short)2);
                HSSFRichTextString texto5 = new HSSFRichTextString(element.getclosing_DATE()+"");
                celdacate.setCellValue(texto5);


                HSSFCell celdacate2 = fila.createCell((short)3);
                HSSFRichTextString texto52 = new HSSFRichTextString(element.getBALANCE()+"");
                celdacate2.setCellValue(texto52);

                HSSFCell celdamarca = fila.createCell((short)4);
                HSSFRichTextString texto6 = new HSSFRichTextString(element.getfullname()+"");
                celdamarca.setCellValue(texto6);


                HSSFCell celdaCant = fila.createCell((short)5);
                HSSFRichTextString texto = new HSSFRichTextString(element.getcaja_NUMBER()+"");
                celdaCant.setCellValue(texto);


                HSSFCell celdacost = fila.createCell((short)6);
                HSSFRichTextString texto1 = new HSSFRichTextString(element.getnotes()+"");
                celdacost.setCellValue(texto1);

                HSSFCell celdacost2 = fila.createCell((short)7);
                HSSFRichTextString texto12 = new HSSFRichTextString(element.getMOVEMENT_DATE()+"");
                celdacost2.setCellValue(texto12);

                HSSFCell celdacost3 = fila.createCell((short)8);
                HSSFRichTextString texto13 = new HSSFRichTextString(element.getVALOR()+"");
                celdacost3.setCellValue(texto13);

                if(element.getNATURALEZA().equals("C")){
                    HSSFCell celdacost4 = fila.createCell((short)9);
                    HSSFRichTextString texto14 = new HSSFRichTextString("Egreso a la Caja");
                    celdacost4.setCellValue(texto14);
                }
                if(element.getNATURALEZA().equals("D")){
                    HSSFCell celdacost4 = fila.createCell((short)9);
                    HSSFRichTextString texto14 = new HSSFRichTextString("Ingreso a la Caja");
                    celdacost4.setCellValue(texto14);
                }
                HSSFCell celdacost5 = fila.createCell((short)10);
                HSSFRichTextString texto15 = new HSSFRichTextString(element.getNOTAS()+"");
                celdacost5.setCellValue(texto15);


                cont++;}

            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            FileOutputStream elFichero = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/reportes/"+Name+"cajas.xls");
            //FileOutputStream elFichero = new FileOutputStream("./"+Name+"cajas.xls");
            libro.write(elFichero);
            elFichero.close();
            response = Name+"cajas.xls";
        }catch (Exception e){
            System.out.println(e.toString());
            response = "0";
        }
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long> getNoDisponibles(Long idbill,
                                                     List<Long> listStore,
                                                     String code) {
        Long dataResponse = reportsDAO.getNoDisponibles(new Long(idbill),
                listStore,
                code);
        if(dataResponse == null){
            dataResponse = new Long(0);
        }
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List <NoDisp>> getSumNoDisponibles(
                                                     List<Long> listStore) {
        List <NoDisp> dataResponse = reportsDAO.getSumNoDisponibles(
                listStore);
        try {
            return Either.right(dataResponse);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }
}
