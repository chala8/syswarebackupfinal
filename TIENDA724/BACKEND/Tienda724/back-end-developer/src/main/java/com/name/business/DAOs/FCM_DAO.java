package com.name.business.DAOs;

import com.name.business.entities.FCM;
import com.name.business.mappers.FCM_Mapper;
import com.name.business.representations.FCM_DTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.sql.Timestamp;
import java.util.List;

@RegisterMapper(FCM_Mapper.class)
public interface FCM_DAO {

    @SqlQuery("SELECT * FROM FCM  WHERE  (status=:status OR :status IS NULL ) AND  (type=:type OR :type IS NULL ) AND (  (:start IS NULL AND  :end IS NULL ) OR ( (date_sent>=:start OR :start IS NULL )  AND (date_sent<=:end OR :end IS NULL)  )); ")
    List<FCM> FCMS_LIST(@Bind("status") Integer status, @Bind("type") String type, @Bind("start") Timestamp start, @Bind("end") Timestamp end);



    @SqlUpdate("INSERT INTO FCM ( title, description,data_info,status, date_sent,type) VALUES "+
            "(:fcm.title, :fcm.description,:fcm.data_info,:fcm.status, :fcm.date_sent,:fcm.type)")
    @GetGeneratedKeys
    long CREATE_FCM(@BindBean("fcm") FCM_DTO fcm_dto);




    @SqlUpdate("UPDATE  FCM SET title=:fcm.title, description=:fcm.description, data_info=:fcm.data_info, " +
            "status=:fcm.status, date_sent=:fcm.date_sent,type=:fcm.type WHERE id=:fcm.id ")
    int EDIT_FCM(@BindBean("fcm") FCM data);


    @SqlUpdate("DELETE_PRODUCTS_OFFER FROM FCM where id =:id_fcm")
    int DELETE(@Bind("id_fcm") Long id_fcm);

}
