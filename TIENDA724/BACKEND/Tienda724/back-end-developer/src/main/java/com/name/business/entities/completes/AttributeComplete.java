package com.name.business.entities.completes;

import com.name.business.entities.Attribute;
import com.name.business.entities.AttributeValue;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;

import java.util.List;

public class AttributeComplete {

    private Attribute attribute;
    private List<AttributeValue> values;

    public AttributeComplete(Attribute attribute, List<AttributeValue> values) {
        this.attribute = attribute;
        this.values = values;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public List<AttributeValue> getValues() {
        return values;
    }

    public void setValues(List<AttributeValue> values) {
        this.values = values;
    }
}
