package com.name.business.entities;

public class Price {

    private String PRICE_DESCRIPTION;
    private double PRICE;



    public Price(String PRICE_DESCRIPTION, double PRICE) {
        this.PRICE_DESCRIPTION = PRICE_DESCRIPTION;
        this.PRICE = PRICE;
    }

    public double getPRICE() {
        return PRICE;
    }

    public void setPRICE(double PRICE) {
        this.PRICE = PRICE;
    }

    public String getPRICE_DESCRIPTION() {
        return PRICE_DESCRIPTION;
    }

    public void setPRICE_DESCRIPTION(String PRICE_DESCRIPTION) {
        this.PRICE_DESCRIPTION = PRICE_DESCRIPTION;
    }

}
