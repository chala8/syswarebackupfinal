package com.name.business;

/**
 *  La clase Store724DatabaseConfiguration: Tiene los respectivos parámetros para poder establecer una comunicación con
 *  la base de datos y lograr una conexión satisfactoria.
 *  Como son las credenciales y la base de datos que en este caso es MySql
 * */
public class Store724DatabaseConfiguration {

    private String driverClassName;
    private String url;
    private String username;
    private String password;

    public String getDriverClassName() {
        return driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
