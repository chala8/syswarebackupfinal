package com.name.business.mappers;


import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.MUName;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MUNameMapper implements ResultSetMapper<MUName> {
    @Override
    public MUName map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new MUName(
                resultSet.getLong("ID_MEASURE_UNIT"),
                resultSet.getString("MEASURE_UNIT"),
                resultSet.getString("DESCRIPTION")
        );
    }
}
