package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.representations.PlanillaDTO;
import com.name.business.representations.PlanillaDetailDTO;
import java.util.List;

public class PlanillaPdfDTO {


    private PlanillaDTO  master;
    private List<PlanillaDetailDTO>  detalles;

    @JsonCreator
    public PlanillaPdfDTO(@JsonProperty("master") PlanillaDTO  master,
                          @JsonProperty("detalles") List<PlanillaDetailDTO>  detalles) {
        this.master = master;
        this.detalles = detalles;
    }

    //------------------------------------------------------------------------------------------------

    public List<PlanillaDetailDTO>  getdetalles() {
        return detalles;
    }

    public void setdetalles(List<PlanillaDetailDTO>  listaTipos) {
        this.detalles = detalles;
    }

    //------------------------------------------------------------------------------------------------

    //------------------------------------------------------------------------------------------------

    public PlanillaDTO  getmaster() {
        return master;
    }

    public void setmaster(PlanillaDTO master) {
        this.master = master;
    }

    //------------------------------------------------------------------------------------------------



}
