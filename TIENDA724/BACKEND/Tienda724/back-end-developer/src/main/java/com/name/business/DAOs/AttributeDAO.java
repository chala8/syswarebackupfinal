package com.name.business.DAOs;

import com.name.business.entities.*;
import com.name.business.mappers.AttributeMapper;
import com.name.business.mappers.CommonSimpleMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(AttributeMapper.class)
public interface AttributeDAO {

    @SqlQuery("SELECT * FROM  V_ATTRIBUTE V_ATT " +
            "  WHERE " +
            "    (V_ATT.ID_ATTRIBUTE=:att.id_attribute OR :att.id_attribute IS NULL ) AND" +
            "    (V_ATT.ID_COMMON=:co.id_common OR :co.id_common IS NULL ) AND " +
            "    (V_ATT.NAME LIKE :co.name OR :co.name IS NULL ) AND " +
            "    (V_ATT.DESCRIPTION LIKE  :co.description OR :co.description IS NULL ) AND " +
            "    (V_ATT.ID_STATE = :co_st.id_common_state OR :co_st.id_common_state IS NULL ) AND " +
            "    (V_ATT.STATE=:co_st.state OR :co_st.state IS NULL ) AND " +
            "    (V_ATT.CREATION_ATTRIBUTE=:co_st.creation_date OR :co_st.creation_date IS NULL ) AND " +
            "    (V_ATT.MODIFY_ATTRIBUTE=:co_st.creation_date OR :co_st.modify_date IS NULL )")
    List<Attribute> read(@BindBean("att") Attribute attribute,@BindBean("co") Common common,@BindBean("co_st") CommonState commonState);

    @SqlUpdate("INSERT INTO ATTRIBUTE ( ID_COMMON,ID_COMMON_STATE) VALUES (:id_common,:id_common_state)")
    void create(@Bind("id_common") Long id_common,@Bind("id_common_state") Long id_common_state);


    @SqlQuery("SELECT ID_ATTRIBUTE FROM ATTRIBUTE WHERE ID_ATTRIBUTE IN (SELECT MAX(ID_ATTRIBUTE) FROM ATTRIBUTE)")
    Long getPkLast();

    @SqlQuery("SELECT COUNT(ID_ATTRIBUTE) FROM ATTRIBUTE WHERE ID_ATTRIBUTE = :id_atribute")
    Integer getValidatorID(@Bind("id_atribute") Long id_atribute);

    /**
     *
     * @param id_attribute
     * @return
     */

    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_ATTRIBUTE ID,ID_COMMON, ID_COMMON_STATE FROM ATTRIBUTE " +
            "  WHERE (ID_ATTRIBUTE = :id_attribute OR :id_attribute IS NULL)")
    List<CommonSimple> readCommons(@Bind("id_attribute") Long id_attribute);

}
