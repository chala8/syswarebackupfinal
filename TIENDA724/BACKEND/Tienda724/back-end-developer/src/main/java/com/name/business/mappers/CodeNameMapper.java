package com.name.business.mappers;

import com.name.business.entities.ProductName;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CodeNameMapper implements ResultSetMapper<ProductName>{

    @Override
    public ProductName map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ProductName(
                resultSet.getLong("ID_PRODUCT"),
                resultSet.getString("PRODUCT"),
                resultSet.getString("DESCRIPTION"),
                resultSet.getLong("ID_THIRD")

        );
    }
}