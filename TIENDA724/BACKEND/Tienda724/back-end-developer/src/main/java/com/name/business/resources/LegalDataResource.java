package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.LegalDataBusiness;
import com.name.business.businesses.MeasureUnitNameBusiness;
import com.name.business.entities.*;
import com.name.business.representations.LegalDataDTO;
import com.name.business.representations.BrandDTO;
import com.name.business.representations.ProductStoreNameDTO;
import com.name.business.representations.ProductStoreInventoryNameDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Path("/legaldata")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LegalDataResource {
    private LegalDataBusiness LegalDataBusiness;

    public LegalDataResource(LegalDataBusiness LegalDataBusiness) {
        this.LegalDataBusiness = LegalDataBusiness;
    }

    @GET
    @Timed
    public Response getLegalDataByID( @QueryParam("id_legal_data") Long id_legal_data) {
        Response response;

        Either<IException, LegalData> getProductsEither = LegalDataBusiness.getLegalDataByID(id_legal_data);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Timed
    public Response postLegalData(LegalDataDTO legalDataDTO) {
        Response response;

        Either<IException, String> getProductsEither = LegalDataBusiness.postLegalData(
                legalDataDTO.getRESOLUCION_DIAN(),
                legalDataDTO.getREGIMEN_TRIBUTARIO(),
                legalDataDTO.getAUTORETENEDOR(),
                legalDataDTO.getURL_LOGO(),
                legalDataDTO.getCITY_NAME(),
                legalDataDTO.getCOUNTRY_NAME(),
                legalDataDTO.getADDRESS(),
                legalDataDTO.getPHONE1(),
                legalDataDTO.getEMAIL(),
                legalDataDTO.getPREFIX_BILL(),
                legalDataDTO.getNOTES());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @PUT
    @Path("/third")
    @Timed
    public Response putThirdLegalData (@QueryParam("id_third") Long id_third, @QueryParam("id_legal_data") Long id_legal_data) {
        Response response;

        Either<IException, String> getProductsEither = LegalDataBusiness.putThirdLegalData  (
                id_third,
                id_legal_data);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @PUT
    @Timed
    public Response putLegalDatabyID (@QueryParam("id_legal_data") Long id_legal_data, LegalDataDTO legalDataDTO) {
        Response response;

        Either<IException, String> getProductsEither = LegalDataBusiness.putLegalDatabyID(
                legalDataDTO.getRESOLUCION_DIAN(),
                legalDataDTO.getADDRESS(),
                legalDataDTO.getPHONE1(),
                legalDataDTO.getEMAIL(),
                legalDataDTO.getNOTES(),
                id_legal_data);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/third")
    @Timed
    public Response getLegalDataByThird( @QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, Long> getProductsEither = LegalDataBusiness.getLegalDataByThird(id_third);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }
}
