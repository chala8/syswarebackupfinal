package com.name.business.businesses;


import com.name.business.DAOs.StoreDAO;
import com.name.business.entities.*;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.List;

import static com.name.business.utils.constans.K.*;


public class StoreBusiness {

    private StoreDAO storeDAO;

    public StoreBusiness(StoreDAO storeDAO) {
        this.storeDAO = storeDAO;
    }



    public Either<IException, List<Store> > getStoreByThird(Long id_third) {
        try {
            return Either.right(storeDAO.getStoreByThird(id_third));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Long> > getIdDirectoryByIdStore(Long id_store) {
        try {
            return Either.right(storeDAO.getIdDirectoryByIdStore(id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postStore(Long id_third,
                                                String description,
                                                Long id_directory,
                                                Long store_number,
                                                Long entrada_consecutive_initial,
                                                Long salida_consecutive_initial) {
        try {
            storeDAO.postStore(id_third,description,id_directory,store_number,entrada_consecutive_initial,salida_consecutive_initial);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putStore(Long id_third,
                                                String description,
                                                Long store_number,
                                                Long entrada_consecutive_initial,
                                                Long salida_consecutive_initial,
                                                Long id_store) {
        try {
            storeDAO.putStore(id_third,description,store_number,entrada_consecutive_initial,salida_consecutive_initial,id_store);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Register> > getCajaByStore(Long id_store) {
        try {
            return Either.right(storeDAO.getCajaByStore(id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postCaja(Long id_store,
                                                String description,
                                                Long caja_number) {
        try {
            storeDAO.postCaja(id_store,description,caja_number);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putCaja(String description,
                                               Long caja_number,
                                               Long id_caja) {
        try {
            storeDAO.putCaja(description,caja_number,id_caja);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Storage> > getStorageByStore(Long id_store) {
        try {
            return Either.right(storeDAO.getStorageByStore(id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postStorage(Long id_store,
                                                   String description,
                                                   Long storage_number) {
        try {
            storeDAO.postStorage(id_store,description,storage_number);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putStorage(String description,
                                                  Long storage_number,
                                                  Long id_storage) {
        try {
            storeDAO.putStorage(description,storage_number,id_storage);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Directory> > getDirectorybyID(Long id_directory) {
        try {
            return Either.right(storeDAO.getDirectorybyID(id_directory));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<String> > getCityByID(Long id_city) {
        try {
            return Either.right(storeDAO.getCityByID(id_city));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<String> > getCitiesByState(Long id_state) {
        try {
            return Either.right(storeDAO.getCitiesByState(id_state));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<State> > getStates() {
        try {
            return Either.right(storeDAO.getStates());
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Phone> > getPhonesByDirectory(Long id_directory) {
        try {
            return Either.right(storeDAO.getPhonesByDirectory(id_directory));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Mail> > getMailsByDirectory(Long id_directory) {
        try {
            return Either.right(storeDAO.getMailsByDirectory(id_directory));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<StorageCategory> > getCategoryByStore(Long ID_THIRD) {
        try {
            return Either.right(storeDAO.getCategoryByStore(ID_THIRD));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<StoreName> > getStoreByCategory(Long id_category) {
        try {
            return Either.right(storeDAO.getStoreByCategory(id_category));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Price> > getPriceList(Long id_product_store) {
        try {
            return Either.right(storeDAO.getPriceList(id_product_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String > postPrice(Long ID_PRODUCT_STORE,
                                                   String PRICE_DESCRIPTION,
                                                   Double PRICE) {
        try {
            storeDAO.postPrice(ID_PRODUCT_STORE,PRICE_DESCRIPTION,PRICE);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long > postProductStore(Long id_store,
                                                        Long id_code,
                                                        String product_store_name,
                                                        Long standard_price,
                                                        String product_store_code,
                                                        String ownbarcode) {
        try {
            storeDAO.postProductStore(id_store,id_code,product_store_name,standard_price,product_store_code,ownbarcode);
            return Either.right(storeDAO.getPkLast());
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long > putThird(Long id_third) {
        try {
            storeDAO.putThird(id_third);
            return Either.right(storeDAO.getPkLast());
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long > getThird(Long id_person) {
        try {
            Long response = storeDAO.getThird(id_person);
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long > getPsId(String code, Long id_store) {
        try {
            return Either.right(storeDAO.getPsId(code,id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Long> > getTipoStore(Long id_store) {
        try {
            return Either.right(storeDAO.getTipoStore(id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<ItemCaja> > getCajaItems(Long id_person) {
        try {
            return Either.right(storeDAO.getCajaItems(id_person));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Integer > putStandardPrice(Double standard_price,
                                                        Long id_ps) {
        try {
            storeDAO.putStandardPrice(standard_price,id_ps);
            return Either.right(1);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Double > getStandardPriceByPs(Long id_ps) {
        try {
            return Either.right(storeDAO.getStandardPriceByPs(id_ps));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Long> confirmar_pedido_cliente(String numpedido,
                                                             Long idbilltype,
                                                             Long idstore) {
        try {
            storeDAO.confirmar_pedido_cliente( numpedido,
                    idbilltype,
                    idstore)
                                            ;
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




    public Either<IException, Long> devolucionVenta(Long consecutivo,
                                                             Long idcaja,
                                                             Long idstore) {
        try {
            storeDAO.devolucionVenta( consecutivo,
                    idcaja,
                    idstore)
            ;
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }




}
