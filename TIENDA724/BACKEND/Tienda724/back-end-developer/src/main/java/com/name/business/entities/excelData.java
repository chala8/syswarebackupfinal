package com.name.business.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class excelData {

    private ArrayList<excelRow> excelRows;

    @JsonCreator
    public excelData(@JsonProperty("excelrows")ArrayList<excelRow> excelRows) {
        this.excelRows = excelRows;
    }

    public ArrayList<excelRow> getexcelRows() {
        return excelRows;
    }

    public void setexcelRows(ArrayList<excelRow> excelRows) {
        this.excelRows = excelRows;
    }

}
