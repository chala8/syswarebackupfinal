package com.name.business.mappers;


import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LabelReportMapper implements ResultSetMapper<LabelReport> {
    @Override
    public LabelReport map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new LabelReport(
                resultSet.getLong("TOTAL")
        );
    }
}
