package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;

public class ProductDTO {

    private Long id_category;
    private Long id_tax;
    private String img_url;
    private String code;
    private CommonDTO  commonDTO;
    private CommonStateDTO stateDTO;

    @JsonCreator
    public ProductDTO(@JsonProperty("id_category") Long id_category,
                      @JsonProperty("id_tax") Long id_tax,
                      @JsonProperty("img_url") String img_url,
                      @JsonProperty("code") String code,
                      @JsonProperty("common") CommonDTO commonDTO,
                      @JsonProperty("state") CommonStateDTO stateDTO) {
        this.id_category = id_category;
        this.id_tax = id_tax;
        this.img_url = img_url;
        this.code = code;
        this.commonDTO = commonDTO;
        this.stateDTO = stateDTO;
    }

    public Long getId_category() {
        return id_category;
    }

    public void setId_category(Long id_category) {
        this.id_category = id_category;
    }

    public Long getId_tax() {
        return id_tax;
    }

    public void setId_tax(Long id_tax) {
        this.id_tax = id_tax;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CommonDTO getCommonDTO() {
        return commonDTO;
    }

    public void setCommonDTO(CommonDTO commonDTO) {
        this.commonDTO = commonDTO;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
