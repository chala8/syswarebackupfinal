package com.name.business.mappers;


import com.name.business.entities.docStatus;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class docStatusMapper implements ResultSetMapper<docStatus> {

    @Override
    public docStatus map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new docStatus(
                resultSet.getString("DOCUMENT_STATUS"),
                resultSet.getLong("ID_DOCUMENT_STATUS")
        );
    }
}
