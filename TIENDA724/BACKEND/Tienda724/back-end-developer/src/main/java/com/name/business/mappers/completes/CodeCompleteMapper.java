package com.name.business.mappers.completes;

import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.MeasureUnit;
import com.name.business.entities.Product;
import com.name.business.entities.completes.CodeComplete;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class CodeCompleteMapper implements ResultSetMapper<CodeComplete> {
    @Override
    public CodeComplete map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CodeComplete(
                resultSet.getLong("ID_CODE"),
                resultSet.getString("CODE_FINAL"),
                resultSet.getString("IMG_COD"),
                resultSet.getDouble("SUGGESTED_PRICE"),
                resultSet.getLong("ID_THIRD_COD"),

                new Product(
                        resultSet.getLong("ID_PRODUCT"),
                        resultSet.getLong("ID_CATEGORY"),
                        resultSet.getInt("STOCK"),
                        resultSet.getInt("STOCK_MIN"),
                        resultSet.getString("IMG_URL"),
                        resultSet.getString("CODE"),
                        resultSet.getLong("ID_TAX"),
                        new Common(
                                resultSet.getLong("ID_COMMON"),
                                resultSet.getString("NAME"),
                                resultSet.getString("DESCRIPTION")
                        ),
                        new CommonState(
                                resultSet.getLong("ID_STATE"),
                                resultSet.getInt("STATE"),
                                resultSet.getDate("CREATION_PRODUCT"),
                                resultSet.getDate("MODIFY_PRODUCT")
                        )
                ),
                new MeasureUnit(

                                resultSet.getLong("ID_MEASURE_UNIT"),
                                resultSet.getLong("ID_MEASURE_UNIT_FATHER"),

                                new Common(
                                        resultSet.getLong("ID_COMMON_MEAS_UNT"),
                                        resultSet.getString("NAME_MEAS_UNT"),
                                        resultSet.getString("DESCRIPTION_MEAS_UNT")
                                ),

                                new CommonState(
                                        resultSet.getLong("ID_STATE_MEAS_UNT"),
                                        resultSet.getInt("STATE_MEAS_UNT"),
                                        resultSet.getDate("CREATION_MEASURE_UNIT"),
                                        resultSet.getDate("MODIFY_MEASURE_UNIT")
                                )



                ),
                resultSet.getLong("ID_ATTRIBUTE_LIST_COD"),
                new CommonState(
                        resultSet.getLong("ID_STATE_CODE"),
                        resultSet.getInt("STATE_CODE"),
                        resultSet.getDate("CREATION_CODE"),
                        resultSet.getDate("MODIFY_CODE")
                )
        );
    }
}
