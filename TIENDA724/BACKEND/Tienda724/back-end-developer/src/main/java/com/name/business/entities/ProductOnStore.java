package com.name.business.entities;

public class ProductOnStore {

    private Long QUANTITY;
    private String PRODUCT_STORE_NAME;
    private String OWNBARCODE;
    private String DESCRIPTION;
    private String CODE;
    private String MUN;
    private String BRAND;



    public ProductOnStore(String DESCRIPTION,
                          Long QUANTITY,
                          String PRODUCT_STORE_NAME,
                          String OWNBARCODE,
                          String CODE,
                          String MUN,
                          String BRAND) {
        this.DESCRIPTION = DESCRIPTION;
        this.QUANTITY = QUANTITY;
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
        this.OWNBARCODE =  OWNBARCODE;
        this.CODE = CODE;
        this.MUN = MUN;
        this.BRAND = BRAND;
    }

    public Long getQUANTITY() {
        return QUANTITY;
    }

    public void setQUANTITY(Long QUANTITY) {
        this.QUANTITY = QUANTITY;
    }

    public String getPRODUCT_STORE_NAME() {
        return PRODUCT_STORE_NAME;
    }

    public void setPRODUCT_STORE_NAME(String PRODUCT_STORE_NAME) {
        this.PRODUCT_STORE_NAME = PRODUCT_STORE_NAME;
    }

    public String getOWNBARCODE() {
        return OWNBARCODE;
    }

    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getMUN() {
        return MUN;
    }

    public void setMUN(String MUN) {
        this.MUN = MUN;
    }

    public String getBRAND() {
        return BRAND;
    }

    public void setBRAND(String BRAND) {
        this.BRAND = BRAND;
    }



}
