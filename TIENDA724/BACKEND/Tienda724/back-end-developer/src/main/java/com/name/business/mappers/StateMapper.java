package com.name.business.mappers;

import com.name.business.entities.State;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StateMapper implements ResultSetMapper<State>{

    @Override
    public State map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new State(
                resultSet.getLong("ID_STATE"),
                resultSet.getString("STATE_NAME")
        );
    }
}