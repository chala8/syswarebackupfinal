package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.MeasureUnitNameBusiness;
import com.name.business.entities.*;
import com.name.business.representations.MUnameDTO;
import com.name.business.representations.BrandDTO;
import com.name.business.representations.ProductStoreNameDTO;
import com.name.business.representations.ProductStoreInventoryNameDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Path("/mun")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MeasureUnitNameResource {
    private MeasureUnitNameBusiness measureUnitNameBusiness;

    public MeasureUnitNameResource(MeasureUnitNameBusiness measureUnitNameBusiness) {
        this.measureUnitNameBusiness = measureUnitNameBusiness;
    }

    @GET
    @Timed
    public Response getFirstLevelGenericMU() {
        Response response;

        Either<IException, List<MUName>> getProductsEither = measureUnitNameBusiness.getFirstLevelGenericMU();
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/bymu")
    @Timed
    public Response getGenericMUByMU(@QueryParam("id_measure_unit_father") Long id_measure_unit_father) {
        Response response;

        Either<IException, List<MUName>> getProductsEither = measureUnitNameBusiness.getGenericMUByMU(id_measure_unit_father);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/bythird")
    @Timed
    public Response getFirstLevelMUByThird(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<MUName>> getProductsEither = measureUnitNameBusiness.getFirstLevelMUByThird(id_third);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/bythirdAndmu")
    @Timed
    public Response getMUByMUByThird(@QueryParam("id_measure_unit_father") Long id_measure_unit_father,@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<MUName>> getProductsEither = measureUnitNameBusiness.getMUByMUByThird(id_measure_unit_father,id_third);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Timed
    public Response postMeasureUnit(MUnameDTO munameDTO) {
        Response response;

        Either<IException, String> getProductsEither = measureUnitNameBusiness.postMeasureUnit(
                munameDTO.getMUName(),
                munameDTO.getMUDescription(),
                munameDTO.getid_measure_unit_father(),
                munameDTO.getid_third());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @PUT
    @Timed
    public Response putMeasureUnit (@QueryParam("id_measure_unit") Long id_measure_unit, MUnameDTO munameDTO) {
        Response response;

        Either<IException, String> getProductsEither = measureUnitNameBusiness.putMeasureUnit  (
                id_measure_unit,
                munameDTO.getMUName(),
                munameDTO.getMUDescription(),
                munameDTO.getid_measure_unit_father());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/brand")
    @Timed
    public Response getFirstLevelGenericBrands() {
        Response response;

        Either<IException, List<Brand>> getProductsEither = measureUnitNameBusiness.getFirstLevelGenericBrands();
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/brand/bybrand")
    @Timed
    public Response getGenericBrandsByBrand(@QueryParam("id_brand_father") Long id_brand_father) {
        Response response;

        Either<IException, List<Brand>> getProductsEither = measureUnitNameBusiness.getGenericBrandsByBrand(id_brand_father);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/brand/bythird")
    @Timed
    public Response getFirstLevelBrandsByThird(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<Brand>> getProductsEither = measureUnitNameBusiness.getFirstLevelBrandsByThird(id_third);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/brand/bybrandbythird")
    @Timed
    public Response getBrandsByBrandByThird(@QueryParam("id_brand_father") Long id_brand_father,@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<Brand>> getProductsEither = measureUnitNameBusiness.getBrandsByBrandByThird(id_brand_father,id_third);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/brand")
    @Timed
    public Response postBrand (BrandDTO brandDTO) {
        Response response;

        Either<IException, String> getProductsEither = measureUnitNameBusiness.postBrand(
                brandDTO.getbrand(),
                brandDTO.getid_brand_father(),
                brandDTO.getid_third(),
                brandDTO.geturl_img());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/brand")
    @Timed
    public Response putBrand (@QueryParam("id_brand") Long id_brand, BrandDTO brandDTO) {
        Response response;

        Either<IException, String> getProductsEither = measureUnitNameBusiness.putBrand(
                brandDTO.getbrand(),
                brandDTO.getid_brand_father(),
                brandDTO.geturl_img(),
                id_brand);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


}
