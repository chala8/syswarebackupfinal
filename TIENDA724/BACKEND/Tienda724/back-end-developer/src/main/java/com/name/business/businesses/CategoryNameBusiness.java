package com.name.business.businesses;


import com.name.business.DAOs.CategoryNameDAO;
import com.name.business.entities.*;
import com.name.business.representations.ListaTipoDTO;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.List;

import static com.name.business.utils.constans.K.*;


public class CategoryNameBusiness {

    private CategoryNameDAO categoryNameDAO;

    public CategoryNameBusiness(CategoryNameDAO categoryNameDAO) {
        this.categoryNameDAO = categoryNameDAO;
    }


    public Either<IException, List<CategoryName> > readCategoryName(ListaTipoDTO listaTipoDTO) {
        try {
            String lista = ""+listaTipoDTO.getlistaTipos().get(0);
            for(int i = 1; i<listaTipoDTO.getlistaTipos().size();i++){
                lista+=","+listaTipoDTO.getlistaTipos().get(i);
            }
            List<CategoryName> validator = categoryNameDAO.readCategoryName(listaTipoDTO.getlistaTipos());
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<CategoryName> > readCategoryChildren(Long id_category_father) {
        try {
            List<CategoryName> validator = categoryNameDAO.readCategoryChildren(id_category_father);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<CategoryName> > readCategoryByThird(Long id_third) {
        try {
            List<CategoryName> validator = categoryNameDAO.readCategoryNameByThird(id_third);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<CategoryName> > getCategoriesChildrenByThird(Long id_category_father ,Long id_third) {
        try {
            List<CategoryName> validator = categoryNameDAO.getCategoriesChildrenByThird(id_category_father,id_third);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String > postCategory(String categoryName, String categoryDescription, String img_url, Long id_category_father, Long id_third) {
        try {
            categoryNameDAO.insertCommon(categoryName, categoryDescription);
            Long id_common = categoryNameDAO.getLastCommonId(categoryName, categoryDescription);
            categoryNameDAO.insertCategory(id_common,img_url,id_category_father,id_third);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putCategory(Long id_category, String categoryName, String categoryDescription, String img_url) {
        try {
            Long id_common = categoryNameDAO.getCommon(id_category);
            categoryNameDAO.updateCommon(categoryName, categoryDescription,id_common);
            categoryNameDAO.updateCategory(id_category,img_url);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<ProductOnStore> > getProductsOnStoreByCategory(Long id_third ,Long id_category,Long id_store) {
        try {
            List<ProductOnStore> validator = categoryNameDAO.getProductsOnStoreByCategory(id_third,id_category,id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<ProductStore> > getProductStoreByCategory(Long id_category, Long id_store) {
        try {
            List<ProductStore> validator = categoryNameDAO.getProductStoreByCategory(id_category, id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



}
