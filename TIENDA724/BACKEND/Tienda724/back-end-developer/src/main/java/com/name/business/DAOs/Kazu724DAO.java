package com.name.business.DAOs;

import com.name.business.entities.Storage;
import com.name.business.entities.Store;
import com.name.business.entities.Register;
import com.name.business.entities.Directory;
import com.name.business.entities.State;
import com.name.business.entities.Phone;
import com.name.business.entities.*;
import com.name.business.mappers.docTypeKazuMapper;
import com.name.business.mappers.codigoCuentaMapper;
import com.name.business.mappers.StateMapper;
import com.name.business.mappers.StoreMapper;
import com.name.business.mappers.StorageMapper;
import com.name.business.mappers.PhoneMapper;
import com.name.business.mappers.MailMapper;
import com.name.business.mappers.docStatusMapper;
import com.name.business.mappers.StoreNameMapper;
import com.name.business.mappers.PriceMapper;
import com.name.business.mappers.ItemCajaMapper;

import com.name.business.representations.InventoryDetailDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;



public interface Kazu724DAO {

    @RegisterMapper(docTypeKazuMapper.class)
    @SqlQuery(" select id_document_type,document_type from kazu724.document_type order by 2 ")
    List<docTypeKazu> getDocType();

    @RegisterMapper(docStatusMapper.class)
    @SqlQuery(" select id_document_status,document_status from kazu724.document_status order by 2 ")
    List<docStatus> getDocStatus();

    @RegisterMapper(codigoCuentaMapper.class)
    @SqlQuery(" select codigo_cuenta,descripcion from kazu724.puc where codigo_cuenta_padre is null order by 1 ")
    List<codigoCuenta> gecCodCuentaGen();

    @RegisterMapper(codigoCuentaMapper.class)
    @SqlQuery(" select codigo_cuenta,descripcion from kazu724.puc where codigo_cuenta_padre=:cp order by 1")
    List<codigoCuenta> gecCodCuenta(@Bind("cp") Long cp);
    //---------------------------------------------------------------------------------------------------------


    @SqlUpdate(" insert into kazu724.puc values(:cc,:description,:ccp,:id_country,:id_store) ")
    void postPuc(@Bind("cc") Long cc,
                 @Bind("description") String description,
                 @Bind("ccp") Long ccp,
                 @Bind("id_country") Long id_country,
                 @Bind("id_store") Long id_store);

    @SqlUpdate(" insert into kazu724.document(id_document,fecha,id_document_type,id_document_status,notes,id_store,id_third_user) \n" +
            "    values(kazu724.document_seq.nextval,sysdate,:id_document_type,:id_document_status,:notes,:id_store,:id_third_user) ")
    void postStatus(@Bind("id_document_status") Long id_document_status,
                    @Bind("id_document_type") Long id_document_type,
                    @Bind("notes") String notes,
                    @Bind("id_store") Long id_store,
                    @Bind("id_third_user") Long id_third_user);

    @SqlUpdate(" insert into kazu724.document_detail values(:cc,:valor,:naturaleza,:notes,:id_document,:id_country, 61) ")
    void postDocDetail(@Bind("cc") Long cc,
                       @Bind("valor") double valor,
                       @Bind("naturaleza") String naturaleza,
                       @Bind("notes") String notes,
                       @Bind("id_document") Long id_document,
                       @Bind("id_country") Long id_country);

    @SqlQuery("SELECT ID_DOCUMENT FROM kazu724.DOCUMENT WHERE ID_DOCUMENT IN (SELECT MAX( ID_DOCUMENT ) FROM kazu724.DOCUMENT )\n")
    Long getPkLast();

}