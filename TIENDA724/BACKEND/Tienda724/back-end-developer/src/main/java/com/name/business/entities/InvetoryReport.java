package com.name.business.entities;

import java.util.Date;

public class InvetoryReport {
    private String BARCODE;
    private String CODIGOTIENDA;
    private Long PRECIO;
    private String LINEA;
    private String CATEGORIA;
    private String MARCA;
    private String PRODUCTO;
    private Long CANTIDAD;
    private Long COSTO;
    private Long COSTOTOTAL;
    private Double PCT_INVENTARIO;


    public InvetoryReport(String BARCODE,
                          String CODIGOTIENDA,
                          Long PRECIO,
                          String LINEA,
                          String CATEGORIA,
                          String MARCA,
                          String PRODUCTO,
                          Long CANTIDAD,
                          Long COSTO,
                          Long COSTOTOTAL,
                          Double PCT_INVENTARIO) {
        this.BARCODE = BARCODE;
        this.CODIGOTIENDA = CODIGOTIENDA;
        this.PRECIO = PRECIO;
        this.LINEA = LINEA;
        this.CATEGORIA = CATEGORIA;
        this.MARCA = MARCA;
        this.PRODUCTO = PRODUCTO;
        this.CANTIDAD = CANTIDAD;
        this.COSTO = COSTO;
        this.COSTOTOTAL = COSTOTOTAL;
        this.PCT_INVENTARIO = PCT_INVENTARIO;

    }
    public String getBARCODE() {
        return BARCODE;
    }

    public void setBARCODE(String BARCODE) {
        this.BARCODE = BARCODE;
    }

    public String getCODIGOTIENDA() {
        return CODIGOTIENDA;
    }

    public void setCODIGOTIENDA(String CODIGOTIENDA) {
        this.CODIGOTIENDA = CODIGOTIENDA;
    }

    public Long getPRECIO() {
        return PRECIO;
    }

    public void getPRECIO(Long PRECIO) {
        this.PRECIO = PRECIO;
    }


    public String getLINEA() {
        return LINEA;
    }

    public void setLINEA(String LINEA) {
        this.LINEA = LINEA;
    }

    public String getCATEGORIA() {
        return CATEGORIA;
    }

    public void setCATEGORIA(String CATEGORIA) {
        this.CATEGORIA = CATEGORIA;
    }

    public String getMARCA() {
        return MARCA;
    }

    public void setMARCA(String MARCA) {
        this.MARCA = MARCA;
    }

    public String getPRODUCTO() {
        return PRODUCTO;
    }

    public void setPRODUCTO(String PRODUCTO) {
        this.PRODUCTO = PRODUCTO;
    }

    public Long getCANTIDAD() {
        return CANTIDAD;
    }

    public void setCANTIDAD(Long CANTIDAD) {
        this.CANTIDAD = CANTIDAD;
    }

    public Long getCOSTO() {
        return COSTO;
    }

    public void setCOSTO(Long COSTO) {
        this.COSTO = COSTO;
    }


    public Long getCOSTOTOTAL() {
        return COSTOTOTAL;
    }

    public void setCOSTOTOTAL(Long COSTOTOTAL) {
        this.COSTOTOTAL = COSTOTOTAL;
    }

    public Double getPCT_INVENTARIO() {
        return PCT_INVENTARIO;
    }

    public void setPCT_INVENTARIO(Double PCT_INVENTARIO) {
        this.PCT_INVENTARIO = PCT_INVENTARIO;
    }

}

