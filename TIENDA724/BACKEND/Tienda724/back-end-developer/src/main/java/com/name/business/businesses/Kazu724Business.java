package com.name.business.businesses;


import com.name.business.DAOs.Kazu724DAO;
import com.name.business.entities.*;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.List;

import static com.name.business.utils.constans.K.*;


public class Kazu724Business {

    private Kazu724DAO kazu724DAO;

    public Kazu724Business(Kazu724DAO kazu724DAO) {
        this.kazu724DAO = kazu724DAO;
    }



    public Either<IException, List<docTypeKazu> > getDocType( ) {
        try {
            List<docTypeKazu> validator = kazu724DAO.getDocType();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<docStatus> > getDocStatus( ) {
        try {
            List<docStatus> validator = kazu724DAO.getDocStatus();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<codigoCuenta> > gecCodCuentaGen( ) {
        try {
            List<codigoCuenta> validator = kazu724DAO.gecCodCuentaGen();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<codigoCuenta> > gecCodCuenta( Long cp) {
        try {
            List<codigoCuenta> validator = kazu724DAO.gecCodCuenta(cp);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


//-----------------------------------------------------------------------------------------------------------------------


    public Either<IException, Integer > postPuc( Long cc,
                                                  String description,
                                                  Long ccp,
                                                  Long id_country,
                                                  Long id_store) {
        try {
            kazu724DAO.postPuc(cc,
                        description,
                        ccp,
                        id_country,
                        id_store);
            return Either.right(1);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long > postStatus(  Long id_document_status,
                                                     Long id_document_type,
                                                     String notes,
                                                     Long id_store,
                                                     Long id_third_user) {
        try {
            kazu724DAO.postStatus(id_document_status,
                    id_document_type,
                    notes,
                    id_store,
                    id_third_user);
            return Either.right(kazu724DAO.getPkLast());
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Integer > postDocDetail(   Long cc,
                                                         double valor,
                                                         String naturaleza,
                                                         String notes,
                                                         Long id_document,
                                                         Long id_country) {
        try {
            kazu724DAO.postDocDetail(cc,valor,
                    naturaleza,
                    notes,
                    id_document,
                    id_country);
            return Either.right(1);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }
}
