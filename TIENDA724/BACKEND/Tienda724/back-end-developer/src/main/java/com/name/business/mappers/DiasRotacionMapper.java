package com.name.business.mappers;

import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.DiasRotacion;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DiasRotacionMapper implements ResultSetMapper<DiasRotacion> {

    @Override
    public DiasRotacion map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DiasRotacion(
                resultSet.getString("LINEA"),
                resultSet.getString("CATEGORIA"),
                resultSet.getString("MARCA"),
                resultSet.getString("PRODUCTO"),
                resultSet.getLong("CANTIDADACTUAL"),
                resultSet.getLong("CANTIDADVENDIDA"),
                resultSet.getDouble("DIAS_ROTACION")

        );
    }
}
