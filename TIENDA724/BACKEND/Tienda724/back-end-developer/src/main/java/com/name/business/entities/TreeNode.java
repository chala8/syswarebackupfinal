package com.name.business.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Trossky Developer
 * @version v1.0
 * @apiNote This class is a adaption from Primeng's TreeNode.If You use this implementation,
 * then You must do reference to author.
 *
 * */
public class TreeNode {


    private Object data;

    private List<Object> children;

    private TreeNode parent;

    public TreeNode(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public List<Object> getChildren() {
        return children;
    }

    public void setChildren(List<Object> children) {
        this.children = children;
    }

    public TreeNode getParent() {
        return parent;
    }

    public void setParent(TreeNode parent) {
        this.parent = parent;
    }
}
