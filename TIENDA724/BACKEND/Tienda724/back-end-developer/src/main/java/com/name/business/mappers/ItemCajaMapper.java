package com.name.business.mappers;


import com.name.business.entities.ItemCaja;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ItemCajaMapper implements ResultSetMapper<ItemCaja> {

    @Override
    public ItemCaja map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ItemCaja(
                resultSet.getLong("ID_CAJA"),
                resultSet.getLong("ID_STORE")
        );
    }
}
