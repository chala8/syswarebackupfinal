package com.name.business.mappers;


import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.NoDisp;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NoDispMapper implements ResultSetMapper<NoDisp> {
    @Override
    public NoDisp map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new NoDisp(
                resultSet.getString("OWNBARCODE"),
                resultSet.getLong("NO_DISPONIBLE")
        );
    }
}
