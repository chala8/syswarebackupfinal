package com.name.business.DAOs;

import com.name.business.entities.CommonSimple;
import com.name.business.entities.LegalData;
import com.name.business.mappers.LegalDataMapper;
import com.name.business.mappers.InventoryMapper;
import com.name.business.representations.InventoryDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;
@RegisterMapper(LegalDataMapper.class)
public interface LegalDataDAO {

    @SqlUpdate(" insert into tercero724.legal_data(resolucion_dian,regimen_tributario,autoretenedor,url_logo,id_city,id_country,\n" +
            "             address,phone1,email,webpage,prefix_bill,initial_range,final_range,start_consecutive,notes)\n" +
            " values(:resolucion_dian,:regimen_tributario,:autoretenedor,:url_logo,:id_city,:id_country," +
            " :address,:phone1, " +
            " :email,null,:prefix_bill,1,10000,1,:notes) ")
    void postLegalData(@Bind("resolucion_dian")String resolucion_dian,
                       @Bind("regimen_tributario")String regimen_tributario,
                       @Bind("autoretenedor")String autoretenedor,
                       @Bind("url_logo")String url_logo,
                       @Bind("id_city")Long id_city,
                       @Bind("id_country")Long id_country,
                       @Bind("address")String address,
                       @Bind("phone1")Long phone1,
                       @Bind("email")String email,
                       @Bind("prefix_bill")String prefix_bill,
                       @Bind("notes")String notes);

    @SqlQuery(" select resolucion_dian,regimen_tributario,autoretenedor,url_logo,city_name,descripcion country_name,address, " +
            "       phone1,email,webpage,prefix_bill,initial_range,final_range,start_consecutive,notes " +
            " from   tercero724.legal_data l, tercero724.city c, tercero724.country p " +
            " where l.id_city=c.id_city and l.id_country=p.id_country and id_legal_data=:id_legal_data")
    LegalData getLegalDataByID( @Bind("id_legal_data")Long id_legal_data);

    @SqlUpdate(" update tercero724.legal_data " +
            "    set resolucion_dian=:resolucion_dian, " +
            "    address=:address, " +
            "    phone1=:phone1, " +
            "    email=:email, " +
            "    notes=:notes " +
            "    where id_legal_data=:id_legal_data ")
    void putLegalDatabyID(@Bind("resolucion_dian")String resolucion_dian,
                          @Bind("address")String address,
                          @Bind("phone1")Long phone1,
                          @Bind("email")String email,
                          @Bind("notes")String notes,
                          @Bind("id_legal_data")Long id_legal_data);

    @SqlUpdate(" update tercero724.third\n" +
            " set id_legal_data=:id_legal_data \n" +
            " where id_third=:id_third ")
    void putThirdLegalData(@Bind("id_third")Long id_third,
                          @Bind("id_legal_data")Long id_legal_data);


    @SqlQuery(" select ID_LEGAL_DATA from tercero724.THIRD where ID_THIRD = :id_third ")
    Long getLegalDataByThird( @Bind("id_third")Long id_third);


}
