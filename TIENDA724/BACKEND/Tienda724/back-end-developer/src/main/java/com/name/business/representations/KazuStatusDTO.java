package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class KazuStatusDTO {

    private Long id_document_status;
    private Long id_document_type;
    private String notes;
    private Long id_store;
    private Long id_third_user;

    @JsonCreator
    public KazuStatusDTO(
                         @JsonProperty("id_document_status") Long id_document_status,
                         @JsonProperty("id_document_type") Long id_document_type,
                         @JsonProperty("notes") String notes,
                         @JsonProperty("id_store") Long id_store,
                         @JsonProperty("id_third_user") Long id_third_user) {
        this.id_document_status = id_document_status;
        this.id_document_type = id_document_type;
        this.notes = notes;
        this.id_third_user = id_third_user;
        this.id_store = id_store;


    }

    //--------------------------------------------------------------------------
    public Long getid_third_user() { return id_third_user; }
    public void setid_third_user(Long id_third_user) { this.id_third_user = id_third_user; }

    //--------------------------------------------------------------------------
    public String getnotes() { return notes; }
    public void setnotes(String notes) { this.notes = notes; }

    //--------------------------------------------------------------------------
    public Long getid_store() { return id_store; }
    public void setid_store(Long ccp) { this.id_store = id_store; }

    //--------------------------------------------------------------------------
    public Long getid_document_type() { return id_document_type; }
    public void setid_document_type(Long id_document_type) { this.id_document_type = id_document_type; }

    //--------------------------------------------------------------------------
    public Long getid_document_status() { return id_document_status; }
    public void setid_document_status(Long id_document_status) { this.id_document_status = id_document_status; }

    //--------------------------------------------------------------------------

}
