package com.name.business.entities;

public class Master {

    private String PURCHASE_DATE;
    private String PREFIX_BILL;
    private Long CONSECUTIVE;
    private String FULLNAME;
    private String STORE_NAME;
    private String CAJA;
    private String NAME;
    private String DOCUMENT_NUMBER;
    private Long SUBTOTAL;
    private Long TAX;
    private Long TOTALPRICE;



    public Master(String PURCHASE_DATE,
                  String PREFIX_BILL,
                  Long CONSECUTIVE,
                  String FULLNAME,
                  String STORE_NAME,
                  String CAJA,
                  String NAME,
                  String DOCUMENT_NUMBER,
                  Long SUBTOTAL,
                  Long TAX,
                  Long TOTALPRICE) {
        this.PURCHASE_DATE = PURCHASE_DATE;
        this.PREFIX_BILL = PREFIX_BILL;
        this.CONSECUTIVE =  CONSECUTIVE;
        this.FULLNAME = FULLNAME;
        this.STORE_NAME = STORE_NAME;
        this.CAJA =  CAJA;
        this.NAME = NAME;
        this.DOCUMENT_NUMBER =  DOCUMENT_NUMBER;
        this.SUBTOTAL = SUBTOTAL;
        this.TAX = TAX;
        this.TOTALPRICE =  TOTALPRICE;
    }

    public String getPURCHASE_DATE() {
        return PURCHASE_DATE;
    }

    public void setPURCHASE_DATE(String PURCHASE_DATE) {
        this.PURCHASE_DATE = PURCHASE_DATE;
    }

    //-----------------------------------------------------------------------------

    public String getPREFIX_BILL() {
        return PREFIX_BILL;
    }

    public void setPREFIX_BILL(String PREFIX_BILL) {
        this.PREFIX_BILL = PREFIX_BILL;
    }

    //----------------------------------------------------------------------------

    public Long getCONSECUTIVE() {
        return CONSECUTIVE;
    }

    public void setCONSECUTIVE(Long CONSECUTIVE) {
        this.CONSECUTIVE = CONSECUTIVE;
    }

    //----------------------------------------------------------------------------

    public String getFULLNAME() {
        return FULLNAME;
    }

    public void setFULLNAME(String FULLNAME) {
        this.FULLNAME = FULLNAME;
    }

    //----------------------------------------------------------------------------

    public String getSTORE_NAME() {
        return STORE_NAME;
    }

    public void setSTORE_NAME(String STORE_NAME) {
        this.STORE_NAME = STORE_NAME;
    }

    //----------------------------------------------------------------------------

    public String getCAJA() {
        return CAJA;
    }

    public void setCAJA(String CAJA) {
        this.CAJA = CAJA;
    }

    //----------------------------------------------------------------------------

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    //----------------------------------------------------------------------------

    public String getDOCUMENT_NUMBER() {
        return DOCUMENT_NUMBER;
    }

    public void setDOCUMENT_NUMBER(String DOCUMENT_NUMBER) {
        this.DOCUMENT_NUMBER = DOCUMENT_NUMBER;
    }

    //----------------------------------------------------------------------------

    public Long getSUBTOTAL() {
        return SUBTOTAL;
    }

    public void setSUBTOTAL(Long SUBTOTAL) {
        this.SUBTOTAL = SUBTOTAL;
    }

    //----------------------------------------------------------------------------

    public Long getTAX() {
        return TAX;
    }

    public void setTAX(Long TAX) {
        this.TAX = TAX;
    }

    //----------------------------------------------------------------------------

    public Long getTOTALPRICE() {
        return TOTALPRICE;
    }

    public void setTOTALPRICE(Long TOTALPRICE) {
        this.TOTALPRICE = TOTALPRICE;
    }

    //----------------------------------------------------------------------------
}
