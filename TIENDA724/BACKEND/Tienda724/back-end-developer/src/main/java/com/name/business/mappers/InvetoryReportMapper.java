package com.name.business.mappers;

import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.InvetoryReport;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class InvetoryReportMapper implements ResultSetMapper<InvetoryReport> {

    @Override
    public InvetoryReport map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new InvetoryReport(
                resultSet.getString("BARCODE"),
                resultSet.getString("CODIGOTIENDA"),
                resultSet.getLong("PRECIO"),
                resultSet.getString("LINEA"),
                resultSet.getString("CATEGORIA"),
                resultSet.getString("MARCA"),
                resultSet.getString("PRODUCTO"),
                resultSet.getLong("CANTIDAD"),
                resultSet.getLong("COSTO"),
                resultSet.getLong("COSTOTOTAL"),
                resultSet.getDouble("PCT_INVENTARIO")

        );
    }
}
