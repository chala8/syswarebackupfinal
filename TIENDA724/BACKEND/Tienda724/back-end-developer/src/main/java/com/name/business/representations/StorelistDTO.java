package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;
import java.util.List;

public class StorelistDTO {


    List<Long> listStore;

    @JsonCreator
    public StorelistDTO(@JsonProperty("listStore") List<Long> listStore) {
        this.listStore = listStore;
    }
    //------------------------------------------------------------------------------------------------
    public List<Long>  getlistStore() {
        return listStore;
    }
    public void setlistStore(List<Long>  listStore) {
        this.listStore = listStore;
    }
    //------------------------------------------------------------------------------------------------



}
