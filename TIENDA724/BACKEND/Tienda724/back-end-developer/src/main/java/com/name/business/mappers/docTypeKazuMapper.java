package com.name.business.mappers;


import com.name.business.entities.docTypeKazu;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class docTypeKazuMapper implements ResultSetMapper<docTypeKazu> {

    @Override
    public docTypeKazu map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new docTypeKazu(
                resultSet.getString("DOCUMENT_TYPE"),
                resultSet.getLong("ID_DOCUMENT_TYPE")
        );
    }
}
