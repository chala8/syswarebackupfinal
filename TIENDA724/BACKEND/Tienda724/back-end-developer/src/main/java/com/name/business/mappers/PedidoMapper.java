package com.name.business.mappers;


import com.name.business.entities.Pedido;
import com.name.business.entities.DetallePedido;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PedidoMapper implements ResultSetMapper<Pedido> {

    @Override
    public Pedido map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Pedido(
                resultSet.getLong("ID_BILL"),
                resultSet.getString("CLIENTE"),
                resultSet.getString("TIENDA"),
                resultSet.getString("NUMPEDIDO"),
                resultSet.getString("FECHA")
        );
    }
}
