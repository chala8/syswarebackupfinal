package com.name.business.mappers;

import com.name.business.entities.Storage;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StorageMapper implements ResultSetMapper<Storage>{

    @Override
    public Storage map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Storage(
                resultSet.getLong("ID_STORAGE"),
                resultSet.getString("STORAGE_NAME"),
                resultSet.getLong("STORAGE_NUMBER")
        );
    }
}