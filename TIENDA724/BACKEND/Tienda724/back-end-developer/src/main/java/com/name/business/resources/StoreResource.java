package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.StoreBusiness;
import com.name.business.entities.*;
import com.name.business.representations.*;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Path("/store")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StoreResource {
    private StoreBusiness storeBusiness;

    public StoreResource(StoreBusiness storeBusiness) {
        this.storeBusiness = storeBusiness;
    }

    @GET
    @Timed
    public Response getStoreByThird(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<Store>> getProductsEither = storeBusiness.getStoreByThird(id_third);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/id")
    @Timed
    public Response getIdDirectoryByIdStore(@QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List<Long>> getProductsEither = storeBusiness.getIdDirectoryByIdStore(id_store);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Timed
    public Response postStore(StoreDTO storeDTO) {
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.postStore(
                storeDTO.getid_third(),
                storeDTO.getdescription(),
                storeDTO.getid_directory(),
                storeDTO.getstore_number(),
                storeDTO.getentrada_consecutive_initial(),
                storeDTO.getsalida_consecutive_initial()
        );

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Timed
    public Response putStore(@QueryParam("id_store") Long id_store,StoreDTO storeDTO) {
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.putStore(
                storeDTO.getid_third(),
                storeDTO.getdescription(),
                storeDTO.getstore_number(),
                storeDTO.getentrada_consecutive_initial(),
                storeDTO.getsalida_consecutive_initial(),
                id_store);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/b")
    @Timed
    public Response getCajaByStore(@QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List<Register>> getProductsEither = storeBusiness.getCajaByStore(id_store);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/b")
    @Timed
    public Response postCaja(RegisterDTO registerDTO) {
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.postCaja(
                registerDTO.getid_store(),
                registerDTO.getdescription(),
                registerDTO.getcaja_number());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/b")
    @Timed
    public Response putCaja(@QueryParam("id_caja") Long id_caja,RegisterDTO registerDTO) {
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.putCaja(
                registerDTO.getdescription(),
                registerDTO.getcaja_number(),
                id_caja);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/s")
    @Timed
    public Response getStorageByStore(@QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List<Storage>> getProductsEither = storeBusiness.getStorageByStore(id_store);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/s/categories")
    @Timed
    public Response getCategoryByStore(@QueryParam("id_third") Long ID_THIRD) {
        Response response;

        Either<IException, List<StorageCategory>> getProductsEither = storeBusiness.getCategoryByStore(ID_THIRD);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/s/by")
    @Timed
    public Response getStoreByCategory(@QueryParam("id_category") Long id_category) {
        Response response;

        Either<IException, List<StoreName>> getProductsEither = storeBusiness.getStoreByCategory(id_category);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/s")
    @Timed
    public Response postStorage(StorageDTO storageDTO) {
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.postStorage(
                storageDTO.getid_store(),
                storageDTO.getdescription(),
                storageDTO.getstorage_number());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/s")
    @Timed
    public Response putStorage(@QueryParam("id_store") Long id_store,StorageDTO storageDTO) {
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.putStorage(
                storageDTO.getdescription(),
                storageDTO.getstorage_number(),
                id_store);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    //------------------------------------------------------------------------------------------------------------------


    @GET
    @Path("/d")
    @Timed
    public Response getDirectorybyID(@QueryParam("id_directory") Long id_directory) {
        Response response;

        Either<IException, List<Directory>> getProductsEither = storeBusiness.getDirectorybyID(id_directory);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/d/city")
    @Timed
    public Response getCityByID(@QueryParam("id_city") Long id_city) {
        Response response;

        Either<IException, List<String>> getProductsEither = storeBusiness.getCityByID(id_city);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/d/city/bystate")
    @Timed
    public Response getCitiesByState(@QueryParam("id_state") Long id_state) {
        Response response;

        Either<IException, List<String>> getProductsEither = storeBusiness.getCitiesByState(id_state);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/states")
    @Timed
    public Response getStates() {
        Response response;

        Either<IException, List<State>> getProductsEither = storeBusiness.getStates();
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/phones")
    @Timed
    public Response getPhonesByDirectory(@QueryParam("id_directory") Long id_directory) {
        Response response;

        Either<IException, List<Phone>> getProductsEither = storeBusiness.getPhonesByDirectory(id_directory);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/mails")
    @Timed
    public Response getMailsByDirectory(@QueryParam("id_directory") Long id_directory) {
        Response response;

        Either<IException, List<Mail>> getProductsEither = storeBusiness.getMailsByDirectory(id_directory);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    //------------------------------------------------------------------------------------------------------------------


    @GET
    @Path("/pricelist")
    @Timed
    public Response getPriceList(@QueryParam("id_product_store") Long id_product_store) {
        Response response;

        Either<IException, List<Price>> getProductsEither = storeBusiness.getPriceList(id_product_store);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/pricelist")
    @Timed
    public Response postPrice(@QueryParam("id_product_store") Long id_product_store,
                              @QueryParam("price_description") String price_description,
                              @QueryParam("price") Double price
                              ) {
        System.out.println(id_product_store);
        System.out.println(price_description);
        System.out.println(price);
        Response response;

        Either<IException, String> getProductsEither = storeBusiness.postPrice(id_product_store,
                price_description,
                price);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/third")
    @Timed
    public Response putThird(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, Long> getProductsEither = storeBusiness.putThird(id_third);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/third")
    @Timed
    public Response getThird(@QueryParam("id_person") Long id_person) {
        Response response;

        Either<IException, Long> getProductsEither = storeBusiness.getThird(id_person);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/psid")
    @Timed
    public Response getPsId(
                            @QueryParam("code") String code,
                            @QueryParam("id_store") Long id_store) {
        Response response;
        Either<IException, Long> getProductsEither = storeBusiness.getPsId(code,id_store);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/ps")
    @Timed
    public Response postProductStore(ProductStoreDTO productStoreDTO) {
        Response response;

        Either<IException, Long> getProductsEither = storeBusiness.postProductStore(
                productStoreDTO.getid_store(),
                productStoreDTO.getid_code(),
                productStoreDTO.getproduct_store_name(),
                productStoreDTO.getstandard_price(),
                productStoreDTO.getproduct_store_code(),
                productStoreDTO.getownbarcode()
        );

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/tipoStore")
    @Timed
    public Response getTipoStore(@QueryParam("id_store") Long id_store) {

        Response response;

        Either<IException, List<Long>> getProductsEither = storeBusiness.getTipoStore(id_store);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/cajaitems")
    @Timed
    public Response getCajaItems(@QueryParam("id_person") Long id_person) {
        Response response;

        Either<IException, List<ItemCaja>> getProductsEither = storeBusiness.getCajaItems(id_person);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/standardprice")
    @Timed
    public Response putStandardPrice(@QueryParam("standard_price") Double standard_price,@QueryParam("id_ps") Long id_ps) {
        Response response;

        Either<IException, Integer> getProductsEither = storeBusiness.putStandardPrice(
                standard_price,
                id_ps);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/stPriceByPs")
    @Timed
    public Response getStandardPriceByPs(@QueryParam("id_ps") Long id_ps) {
        Response response;

        Either<IException, Double> getProductsEither = storeBusiness.getStandardPriceByPs(id_ps);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @POST
    @Path("/confirmarPC")
    @Timed
    public Response confirmar_pedido_cliente(@QueryParam("numpedido") String numpedido,
                                             @QueryParam("idbilltype") Long idbilltype,
                                             @QueryParam("idstore") Long idstore){
        Response response;

        Either<IException, Long> getProducts = storeBusiness.confirmar_pedido_cliente(numpedido,
                idbilltype,
                idstore);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




    @POST
    @Path("/devolucionVenta")
    @Timed
    public Response devolucionVenta(@QueryParam("consecutivo") Long consecutivo,
                                    @QueryParam("idcaja") Long idcaja,
                                    @QueryParam("idstore") Long idstore){
        Response response;

        Either<IException, Long> getProducts = storeBusiness.devolucionVenta(consecutivo,
                idcaja,
                idstore);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }




}




