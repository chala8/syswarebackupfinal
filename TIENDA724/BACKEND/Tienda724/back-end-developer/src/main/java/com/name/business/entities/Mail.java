package com.name.business.entities;

public class Mail {

    private Long ID_MAIL;
    private String MAIL;
    private Long PRIORITY;



    public Mail(Long ID_MAIL, String MAIL, Long PRIORITY) {
        this.ID_MAIL = ID_MAIL;
        this.MAIL = MAIL;
        this.PRIORITY =  PRIORITY;
    }

    public Long getID_MAIL() {
        return ID_MAIL;
    }

    public void setID_MAIL(Long ID_MAIL) {
        this.ID_MAIL = ID_MAIL;
    }

    public String getMAIL() {
        return MAIL;
    }

    public void setMAIL(String MAIL) {
        this.MAIL = MAIL;
    }

    public Long getPRIORITY() {
        return PRIORITY;
    }

    public void setPRIORITY(Long PRIORITY) {
        this.PRIORITY = PRIORITY;
    }


}
