package com.name.business.entities;

public class CategoryName {

    private Long ID_CATEGORY;
    private String CATEGORY;
    private String IMG_URL;


    public CategoryName(Long id_category, String category, String IMG_URL) {
        this.ID_CATEGORY = id_category;
        this.CATEGORY = category;
        this.IMG_URL = IMG_URL;
    }

    public Long getID_CATEGORY() {
        return ID_CATEGORY;
    }

    public void setID_CATEGORY(Long id_category) {
        this.ID_CATEGORY = id_category;
    }

    public String getCATEGORY() {
        return CATEGORY;
    }

    public void setCATEGORY(String category) {
        this.CATEGORY = category;
    }

    public String getIMG_URL() {
        return IMG_URL;
    }

    public void setIMG_URL(String IMG_URL) {
        this.IMG_URL = IMG_URL;
    }


}
