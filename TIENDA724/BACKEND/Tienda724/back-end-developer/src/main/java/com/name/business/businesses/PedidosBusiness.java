package com.name.business.businesses;

import com.itextpdf.text.*;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.name.business.DAOs.PedidosDAO;
import com.name.business.entities.*;
import com.name.business.representations.MeasureUnitDTO;
import com.name.business.representations.PlanillaDTO;
import com.name.business.representations.PlanillaDetailDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.name.business.sanitations.EntitySanitation.measureUnitSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class PedidosBusiness {
    private PedidosDAO pedidosDAO;

    public PedidosBusiness(PedidosDAO pedidosDAO) {
        this.pedidosDAO = pedidosDAO;
    }

    public Either<IException, String > getOwnBarCode(Long id_product_store) {
        try {
            String validator = pedidosDAO.getOwnBarCode(id_product_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Vehiculo> > getVehiculos() {
        try {
            List<Vehiculo> validator = pedidosDAO.getVehiculos();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, Long > getPsId(String code, Long id_store) {
        try {
            Long validator = pedidosDAO.getPsId(code,id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, Integer> updatePlanilla(String observaciones, Long idplanilla) {
        try {
            System.out.println("----a-----");
            pedidosDAO.updatePlanilla(observaciones, idplanilla);
            System.out.println("----b-----");
            return Either.right(1);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Planilla> > getPlanillas(Long idvehiculo,
                                                            Long idstore) {
        try {
            List<Planilla> validator = pedidosDAO.getPlanillas(idvehiculo,idstore);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List<PlanillaDetail> > getPlanillaDetail(Long idplanilla) {
        try {
            List<PlanillaDetail> validator = pedidosDAO.getPlanillaDetail(idplanilla);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postProcedure(Long idbill,
                                                     Long idbillstate,
                                                     Long code,
                                                     Long idstore,
                                                     Long cantidad,
                                                     String tipo) {
        try {
            pedidosDAO.procedure(idbill,
                     idbillstate,
                     code,
                     idstore,
                     cantidad,
                     tipo);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String> postpdf(PlanillaDTO master, List<PlanillaDetailDTO> detalles){


        Document documento = new Document(PageSize.A4, 0f, 0f, 35f, 0f);
        //Document documento = new Document();
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.US);
        String response = "Bad!!!";

        try{


            BaseFont HELVETICA = BaseFont.createFont(
                    BaseFont.HELVETICA_BOLD,
                    BaseFont.WINANSI, false);


            Font fontH1 = new Font(HELVETICA, 12, Font.NORMAL);
            Font fontH2 = new Font(HELVETICA, 12, Font.BOLD);
            Font fontH3 = new Font(HELVETICA, 14, Font.BOLD);


            String nombrePdf = master.getnum_PLANILLA()+"_planilla.pdf";
            FileOutputStream ficheroPdf = new FileOutputStream("/u01/tomcat/apache-tomcat-9.0.0.M21/webapps/planillas/"+ nombrePdf);
            //FileOutputStream ficheroPdf = new FileOutputStream("./"+ nombrePdf);


            PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(20);
            documento.open();
            documento.newPage();
            documento.add(new Chunk(""));


            PdfPCell cellblanks =new PdfPCell(new Phrase(" "));
            cellblanks.setBorder(PdfPCell.NO_BORDER);
            cellblanks.setHorizontalAlignment(Element.ALIGN_CENTER);


            PdfPTable tablaDatos = new PdfPTable(1);
            PdfPCell cell1 =new PdfPCell(new Phrase("Documento de Planilla para: "+master.getnum_PLANILLA(),fontH3));
            cell1.setBorder(PdfPCell.NO_BORDER);
            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDatos.addCell(cell1);
            tablaDatos.addCell(cellblanks);


            PdfPTable tablaMaster = new PdfPTable(4);


            PdfPCell cellprod = new PdfPCell(new Phrase("Conductor",fontH2));
            cellprod.setBorder(PdfPCell.NO_BORDER);
            cellprod.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaMaster.addCell(cellprod);

            PdfPCell celcat = new PdfPCell(new Phrase("Placa",fontH2));
            celcat.setBorder(PdfPCell.NO_BORDER);
            celcat.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaMaster.addCell(celcat);

            PdfPCell celcant = new PdfPCell(new Phrase("Numero de Planilla",fontH2));
            celcant.setBorder(PdfPCell.NO_BORDER);
            celcant.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaMaster.addCell(celcant);

            PdfPCell cellcost = new PdfPCell(new Phrase("Fecha de Apertura de Planilla",fontH2));
            cellcost.setBorder(PdfPCell.NO_BORDER);
            cellcost.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaMaster.addCell(cellcost);

            PdfPCell cellprod2= new PdfPCell(new Phrase(master.getconductor(),fontH1));
            cellprod2.setBorder(PdfPCell.NO_BORDER);
            cellprod2.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaMaster.addCell(cellprod2);

            PdfPCell celcat2 = new PdfPCell(new Phrase(master.getplaca(),fontH1));
            celcat2.setBorder(PdfPCell.NO_BORDER);
            celcat2.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaMaster.addCell(celcat2);

            PdfPCell celcant2 = new PdfPCell(new Phrase(master.getnum_PLANILLA(),fontH1));
            celcant2.setBorder(PdfPCell.NO_BORDER);
            celcant2.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaMaster.addCell(celcant2);

            PdfPCell cellcost2 = new PdfPCell(new Phrase(master.getfecha_INICIO(),fontH1));
            cellcost2.setBorder(PdfPCell.NO_BORDER);
            cellcost2.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaMaster.addCell(cellcost2);



            PdfPTable tablaDatos2= new PdfPTable(1);
            tablaDatos2.addCell(cellblanks);
            tablaDatos2.addCell(cellblanks);
            tablaDatos2.addCell(cellblanks);
            PdfPCell cell1d =new PdfPCell(new Phrase("DETALLES",fontH2));
            cell1d.setBorder(PdfPCell.NO_BORDER);
            cell1d.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDatos2.addCell(cell1d);
            tablaDatos2.addCell(cellblanks);



            PdfPTable tablaDetails = new PdfPTable(4);


            PdfPCell cellprodd = new PdfPCell(new Phrase("Numero Documento",fontH2));
            cellprodd.setBorder(PdfPCell.BOTTOM);
            cellprodd.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(cellprodd);

            PdfPCell celcatd = new PdfPCell(new Phrase("Tienda",fontH2));
            celcatd.setBorder(PdfPCell.BOTTOM);
            celcatd.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celcatd);

            PdfPCell celcantd = new PdfPCell(new Phrase("Direccion",fontH2));
            celcantd.setBorder(PdfPCell.BOTTOM);
            celcantd.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(celcantd);

            PdfPCell cellcostd = new PdfPCell(new Phrase("Valor",fontH2));
            cellcostd.setBorder(PdfPCell.BOTTOM);
            cellcostd.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDetails.addCell(cellcostd);

            for( PlanillaDetailDTO element : detalles ){



                PdfPCell cellprod2b = new PdfPCell(new Phrase(element.getnum_DOCUMENTO(),fontH1));
                cellprod2b.setBorder(PdfPCell.NO_BORDER);
                cellprod2b.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(cellprod2b);

                PdfPCell celcat2b = new PdfPCell(new Phrase(element.getstore(),fontH1));
                celcat2b.setBorder(PdfPCell.NO_BORDER);
                celcat2b.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celcat2b);

                PdfPCell celcant2b = new PdfPCell(new Phrase(element.getaddress()+"",fontH1));
                celcant2b.setBorder(PdfPCell.NO_BORDER);
                celcant2b.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(celcant2b);

                PdfPCell cellcost2b = new PdfPCell(new Phrase(element.getvalor()+"",fontH1));
                cellcost2b.setBorder(PdfPCell.NO_BORDER);
                cellcost2b.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaDetails.addCell(cellcost2b);

            }

            PdfPTable table = new PdfPTable(1);
            tablaDatos.setKeepTogether(false);
            tablaDatos.setSplitLate(false);
            PdfPCell hed = new PdfPCell(tablaDatos);
            hed.setBorder(PdfPCell.NO_BORDER);
            hed.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed);

            tablaMaster.setSplitLate(false);
            tablaMaster.setKeepTogether(false);
            PdfPCell hed3 = new PdfPCell(tablaMaster);
            hed3.setBorder(PdfPCell.NO_BORDER);
            hed3.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed3);

            tablaDatos2.setSplitLate(false);
            tablaDatos2.setKeepTogether(false);
            PdfPCell hed32 = new PdfPCell(tablaDatos2);
            hed32.setBorder(PdfPCell.NO_BORDER);
            hed32.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed32);

            tablaDetails.setSplitLate(false);
            tablaDetails.setKeepTogether(false);
            PdfPCell hed322 = new PdfPCell(tablaDetails);
            hed322.setBorder(PdfPCell.NO_BORDER);
            hed322.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hed322);

            table.setKeepTogether(false);
            table.setSplitLate(false);

            documento.add(table);

            response = nombrePdf;




        }catch(Exception e){
            response = e.toString();
        }

        documento.close();
        try {
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }

    }


    public Either<IException, List<Pedido>> getPedidos(Long id_store, Long id_bill_state, Long id_bill_type) {
        try {
            List<Pedido> validator = pedidosDAO.getPedidos(id_store,id_bill_state,id_bill_type);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }
    public Either<IException, List<DetallePedido>> getDetallesPedidos(List<Long> id_store) {
        try {
            List<DetallePedido> validator = pedidosDAO.getDetallesPedidos(id_store);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, Float > GET_VALOR_DOMICILIO(String IDSTORE,
                                                          String DISTANCIA_KM) {
        try {
            Float response = pedidosDAO.GET_VALOR_DOMICILIO(IDSTORE,DISTANCIA_KM);
            return Either.right(response);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


}
