package com.name.business.mappers;

import com.name.business.entities.Document;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DocumentMapper implements ResultSetMapper<Document> {

    @Override
    public Document map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Document(
                resultSet.getLong("ID_DOCUMENT"),
                resultSet.getString("TITLE"),
                resultSet.getString("BODY")
        );
    }
}
