package com.name.business.entities;

public class Vehiculo {

    private Long ID_VEHICULO;
    private String PLACA;
    private String CONDUCTOR;

    public Vehiculo(Long ID_VEHICULO, String PLACA,String CONDUCTOR) {
        this.ID_VEHICULO = ID_VEHICULO;
        this.PLACA = PLACA;
        this.CONDUCTOR = CONDUCTOR;
    }

    //-------------------------------------------------------------------------------------------

    public Long getID_VEHICULO() {
        return ID_VEHICULO;
    }

    public void setID_VEHICULO(Long ID_VEHICULO) {
        this.ID_VEHICULO = ID_VEHICULO;
    }

    //-------------------------------------------------------------------------------------------

    public String getPLACA() {
        return PLACA;
    }

    public void setPLACA(String PLACA) {
        this.PLACA = PLACA;
    }

    //-------------------------------------------------------------------------------------------

    public String getCONDUCTOR() {
        return CONDUCTOR;
    }

    public void setCONDUCTOR(String CONDUCTOR) {
        this.CONDUCTOR = CONDUCTOR;
    }

    //-------------------------------------------------------------------------------------------



}
