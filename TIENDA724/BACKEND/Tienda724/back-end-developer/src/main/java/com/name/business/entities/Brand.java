package com.name.business.entities;

public class Brand {

    private Long ID_BRAND;
    private String BRAND;
    private String URL_IMG;


    public Brand(Long ID_BRAND, String BRAND, String URL_IMG) {
        this.ID_BRAND = ID_BRAND;
        this.BRAND = BRAND;
        this.URL_IMG = URL_IMG;
    }

    public Brand(Long ID_BRAND, String BRAND) {
        this.ID_BRAND = ID_BRAND;
        this.BRAND = BRAND;
    }

    public Long getID_BRAND() {
        return ID_BRAND;
    }

    public void setID_BRAND(Long ID_BRAND) {
        this.ID_BRAND = ID_BRAND;
    }

    public String getBRAND() {
        return BRAND;
    }

    public void setBRAND(String BRAND) {
        this.BRAND = BRAND;
    }

    public String getURL_IMG() {
        return URL_IMG;
    }

    public void setURL_IMG(String URL_IMG) {
        this.URL_IMG = URL_IMG;
    }


}
