package com.name.business.businesses;

import com.name.business.DAOs.MeasureUnitNameDAO;
import com.name.business.entities.*;
import com.name.business.representations.MeasureUnitDTO;
import com.name.business.utils.exeptions.BussinessException;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.name.business.sanitations.EntitySanitation.measureUnitSanitation;
import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;
import static com.name.business.utils.constans.K.messages_errors;
import static com.name.business.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

public class MeasureUnitNameBusiness {
    private MeasureUnitNameDAO measureUnitNameDAO;

    public MeasureUnitNameBusiness(MeasureUnitNameDAO measureUnitNameDAO) {
        this.measureUnitNameDAO = measureUnitNameDAO;
    }

    public Either<IException, List<MUName> > getFirstLevelGenericMU() {
        try {
            List<MUName> validator = measureUnitNameDAO.getFirstLevelGenericMU();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<MUName> > getGenericMUByMU(Long id_measure_unit_father) {
        try {
            List<MUName> validator = measureUnitNameDAO.getGenericMUByMU(id_measure_unit_father);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<MUName> > getFirstLevelMUByThird(Long id_third) {
        try {
            List<MUName> validator = measureUnitNameDAO.getFirstLevelMUByThird(id_third);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<MUName> > getMUByMUByThird(Long id_measure_unit_father, Long id_third) {
        try {
            List<MUName> validator = measureUnitNameDAO.getMUByMUByThird(id_measure_unit_father,id_third);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String > postMeasureUnit(String MUName, String MUDescription, Long id_measure_unit_father, Long id_third) {
        try {
            measureUnitNameDAO.insertCommon(MUName, MUDescription);
            Long id_common = measureUnitNameDAO.getLastCommonId(MUName, MUDescription);
            measureUnitNameDAO.postMeasureUnit(id_common,id_measure_unit_father,id_third);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putMeasureUnit(Long id_measure_unit, String MUName, String MUDescription, Long id_measure_unit_father ) {
        try {
            Long id_common = measureUnitNameDAO.getIdCommonByMU(id_measure_unit);
            measureUnitNameDAO.putCommon(id_common, MUName, MUDescription);
            measureUnitNameDAO.putMeasureUnit(id_measure_unit,id_measure_unit_father);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Brand> > getFirstLevelGenericBrands() {
        try {
            List<Brand> validator = measureUnitNameDAO.getFirstLevelGenericBrands();
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Brand> > getGenericBrandsByBrand(Long id_brand_father) {
        try {
            List<Brand> validator = measureUnitNameDAO.getGenericBrandsByBrand(id_brand_father);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Brand> > getFirstLevelBrandsByThird(Long id_third) {
        try {
            List<Brand> validator = measureUnitNameDAO.getFirstLevelBrandsByThird(id_third);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Brand> > getBrandsByBrandByThird(Long id_brand_father,Long id_third) {
        try {
            List<Brand> validator = measureUnitNameDAO.getBrandsByBrandByThird(id_brand_father, id_third);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postBrand(String brand, Long id_brand_father, Long id_third, String url_img) {
        try {
            measureUnitNameDAO.postBrand(brand,id_brand_father,id_third,url_img);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putBrand(String brand, Long id_brand_father, String url_img, Long id_brand ) {
        try {
            measureUnitNameDAO.putBrand(brand,id_brand_father,url_img,id_brand);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

}
