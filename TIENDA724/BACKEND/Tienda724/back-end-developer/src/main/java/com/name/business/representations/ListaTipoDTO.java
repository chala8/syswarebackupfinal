package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;
import java.util.List;

public class ListaTipoDTO {


    private List<Long> listaTipos;

    @JsonCreator
    public ListaTipoDTO(@JsonProperty("listaTipos") List<Long>  listaTipos) {
        this.listaTipos = listaTipos;
    }

    //------------------------------------------------------------------------------------------------

    public List<Long> getlistaTipos() {
        return listaTipos;
    }

    public void getlistaTipos(List<Long> listaTipos) {
        this.listaTipos = listaTipos;
    }

    //------------------------------------------------------------------------------------------------



}
