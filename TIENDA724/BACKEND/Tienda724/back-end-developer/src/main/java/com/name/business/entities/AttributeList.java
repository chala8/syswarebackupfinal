package com.name.business.entities;

public class AttributeList {
    private Long id_attribute_list;
    private CommonState state;

    public AttributeList(Long id_attribute_list, CommonState state) {
        this.id_attribute_list = id_attribute_list;
        this.state = state;
    }

    public Long getId_attribute_list() {
        return id_attribute_list;
    }

    public void setId_attribute_list(Long id_attribute_list) {
        this.id_attribute_list = id_attribute_list;
    }

    public CommonState getState() {
        return state;
    }

    public void setState(CommonState state) {
        this.state = state;
    }
}
