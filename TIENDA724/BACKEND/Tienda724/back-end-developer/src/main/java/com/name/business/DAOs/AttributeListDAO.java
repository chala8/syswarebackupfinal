package com.name.business.DAOs;

import com.name.business.entities.AttributeList;
import com.name.business.entities.CommonSimple;
import com.name.business.entities.CommonState;
import com.name.business.mappers.AttributeListMapper;
import com.name.business.mappers.CommonSimpleMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(AttributeListMapper.class)
public interface AttributeListDAO {

    @SqlUpdate("INSERT INTO ATTRIBUTE_LIST ( ID_COMMON_STATE) VALUES (:id_common_state)")
    void create( @Bind("id_common_state") Long id_common_state);


    @SqlQuery(" SELECT ID_ATTRIBUTE_LIST FROM ATTRIBUTE_LIST WHERE ID_ATTRIBUTE_LIST IN (SELECT MAX(ID_ATTRIBUTE_LIST) FROM ATTRIBUTE_LIST) ")
    Long getPkLast();

    @SqlQuery("SELECT COUNT(ID_ATTRIBUTE_LIST) FROM ATTRIBUTE_LIST WHERE ID_ATTRIBUTE_LIST = :id_atribute_list ")
    Integer getValidatorID( @Bind ("id_atribute_list") Long id_atribute_list );

    @SqlQuery("SELECT * FROM V_ATTRIBUTE_LIST V_ATT_L " +
            "WHERE " +
            "  (V_ATT_L.ID_ATTRIBUTE_LIST =:att_list.id_attribute_list OR :att_list.id_attribute_list IS NULL ) AND " +
            "  (V_ATT_L.ID_STATE =:co_st.id_common_state OR :co_st.id_common_state IS NULL ) AND " +
            "  (V_ATT_L.STATE =:co_st.state OR :co_st.state  IS NULL ) AND " +
            "  (V_ATT_L.CREATION_ATTRIBUTE_LIST =:co_st.creation_date OR :co_st.creation_date  IS NULL ) AND " +
            "  (V_ATT_L.MODIFY_ATTRIBUTE_LIST =:co_st.modify_date OR :co_st.modify_date  IS NULL )")
    List<AttributeList> read(@BindBean("att_list") AttributeList attributeListSanitation,@BindBean("co_st") CommonState state);


    @RegisterMapper(CommonSimpleMapper.class)
    @SqlQuery("SELECT ID_ATTRIBUTE_LIST ID,ID_ATTRIBUTE_LIST ID_COMMON, ID_COMMON_STATE FROM ATTRIBUTE_LIST " +
            "  WHERE (ID_ATTRIBUTE_LIST = :id_attribute_list OR :id_attribute_list IS NULL)")
    List<CommonSimple> readCommons(@Bind("id_attribute_list") Long id_attribute_list);
}
