package com.name.business.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class excelRow {

    private Long cantidad;
    private String categoria;
    private Long costo;
    private Long costototal;
    private String linea;
    private String marca;
    private Double pct_inventario;
    private String producto;
    private String barcode;
    private String codigotienda;
    private Long precio;
    private Long nodisp;


    @JsonCreator
    public excelRow(
                    @JsonProperty("cantidad") Long cantidad,
                    @JsonProperty("categoria") String categoria,
                    @JsonProperty("costo") Long costo,
                    @JsonProperty("costototal") Long costototal,
                    @JsonProperty("linea") String linea,
                    @JsonProperty("marca") String marca,
                    @JsonProperty("pct_INVENTARIO") Double pct_inventario,
                    @JsonProperty("producto") String producto,
                    @JsonProperty("barcode") String barcode,
                    @JsonProperty("codigotienda") String codigotienda,
                    @JsonProperty("precio") Long precio,
                    @JsonProperty("nodisp") Long nodisp
                    ) {
        this.cantidad = cantidad;
        this.categoria = categoria;
        this.costo = costo;
        this.costototal = costototal;
        this.linea = linea;
        this.marca = marca;
        this.pct_inventario = pct_inventario;
        this.producto = producto;
        this.barcode = barcode;
        this.codigotienda = codigotienda;
        this.precio = precio;
        this.nodisp = nodisp;
    }

    public String getbarcode() {
        return barcode;
    }

    public void setbarcode(String barcode) {
        this.barcode = barcode;
    }


    public String getcodigotienda() {
        return codigotienda;
    }

    public void setcodigotienda(String codigotienda) {
        this.codigotienda = codigotienda;
    }


    public String getproducto() {
        return producto;
    }

    public void setproducto(String producto) {
        this.producto = producto;
    }


    public Double getpct_inventario() {
        return pct_inventario;
    }

    public void setpct_inventario(Double pct_inventario) {
        this.pct_inventario = pct_inventario;
    }


    public Long getprecio() {
        return precio;
    }

    public void setprecio(Long precio) {
        this.precio = precio;
    }


    public String getmarca() {
        return marca;
    }

    public void setmarca(String marca) {
        this.marca = marca;
    }


    public String getlinea() {
        return linea;
    }

    public void setlinea(String linea) {
        this.linea = linea;
    }


    public Long getcostototal() {
        return costototal;
    }

    public void setcostototal(Long costototal) {
        this.costototal = costototal;
    }


    public Long getcosto() {
        return costo;
    }

    public void setcosto(Long costo) {
        this.costo = costo;
    }


    public Long getcantidad() {
        return cantidad;
    }

    public void setcantidad(Long cantidad) {
        this.cantidad = cantidad;
    }


    public String getcategoria() {
        return categoria;
    }

    public void setcategoria(String categoria) {
        this.categoria = categoria;
    }

}
