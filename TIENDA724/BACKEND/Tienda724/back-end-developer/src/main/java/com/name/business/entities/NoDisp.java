package com.name.business.entities;

public class NoDisp {

    private String OWNBARCODE;
    private Long NO_DISPONIBLE;



    public NoDisp( String OWNBARCODE, Long NO_DISPONIBLE) {
        this.OWNBARCODE = OWNBARCODE;
        this.NO_DISPONIBLE =  NO_DISPONIBLE;
    }


    public String getOWNBARCODE() {
        return OWNBARCODE;
    }

    public void setOWNBARCODE(String OWNBARCODE) {
        this.OWNBARCODE = OWNBARCODE;
    }

    public Long getNO_DISPONIBLE() {
        return NO_DISPONIBLE;
    }

    public void setNO_DISPONIBLE(Long NO_DISPONIBLE) {
        this.NO_DISPONIBLE = NO_DISPONIBLE;
    }


}
