package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class KazuPucDTO {

    private Long cc;
    private String description;
    private Long ccp;
    private Long id_country;
    private Long id_store;

    @JsonCreator
    public KazuPucDTO(@JsonProperty("cc") Long cc,
                      @JsonProperty("description") String description,
                      @JsonProperty("ccp") Long ccp,
                      @JsonProperty("id_country") Long id_country,
                      @JsonProperty("id_store") Long id_store) {
        this.cc = cc;
        this.description = description;
        this.ccp = ccp;
        this.id_country = id_country;
        this.id_store = id_store;


    }

    //--------------------------------------------------------------------------
    public Long getcc() { return cc; }
    public void setcc(Long cc) { this.cc = cc; }

    //--------------------------------------------------------------------------
    public String getdescription() { return description; }
    public void setdescription(String description) { this.description = description; }

    //--------------------------------------------------------------------------
    public Long getccp() { return ccp; }
    public void setccp(Long ccp) { this.ccp = ccp; }

    //--------------------------------------------------------------------------
    public Long getid_country() { return id_country; }
    public void setid_country(Long id_country) { this.id_country = id_country; }

    //--------------------------------------------------------------------------
    public Long getid_store() { return id_store; }
    public void setid_store(Long id_store) { this.id_store = id_store; }

    //--------------------------------------------------------------------------

}
