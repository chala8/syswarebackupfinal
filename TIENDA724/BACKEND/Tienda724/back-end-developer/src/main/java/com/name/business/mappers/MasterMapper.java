package com.name.business.mappers;

import com.name.business.entities.Mail;
import com.name.business.entities.Master;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MasterMapper implements ResultSetMapper<Master>{

    @Override
    public Master map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Master(
                resultSet.getString("PURCHASE_DATE"),
                resultSet.getString("PREFIX_BILL"),
                resultSet.getLong("CONSECUTIVE"),
                resultSet.getString("FULLNAME"),
                resultSet.getString("STORE_NAME"),
                resultSet.getString("CAJA"),
                resultSet.getString("NAME"),
                resultSet.getString("DOCUMENT_NUMBER"),
                resultSet.getLong("SUBTOTAL"),
                resultSet.getLong("TAX"),
                resultSet.getLong("TOTALPRICE")

        );
    }
}