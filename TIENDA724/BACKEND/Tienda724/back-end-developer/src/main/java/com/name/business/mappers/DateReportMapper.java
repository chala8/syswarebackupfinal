package com.name.business.mappers;


import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DateReportMapper implements ResultSetMapper<DateReport> {

    @Override
    public DateReport map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DateReport(
                resultSet.getLong("SUBTOTAL"),
                resultSet.getLong("TAX"),
                resultSet.getLong("TOTAL")
        );
    }
}
