package com.name.business.entities;

public class Storage {

    private Long id_storage;
    private String storage_name;
    private Long storage_number;


    public Storage( Long id_storage,
                     String storage_name,
                     Long storage_number) {
        this.id_storage = id_storage;
        this.storage_name = storage_name;
        this.storage_number =  storage_number;
    }

    //---------------------------------------------------------------------------

    public Long getid_storage() {
        return id_storage;
    }

    public void setid_storage(Long id_storage) {
        this.id_storage = id_storage;
    }

    //---------------------------------------------------------------------------

    public String getstorage_name() {
        return storage_name;
    }

    public void setstorage_name(String storage_name) {
        this.storage_name = storage_name;
    }

    //---------------------------------------------------------------------------

    public Long getstorage_number() {
        return storage_number;
    }

    public void setstorage_number(Long storage_number) {
        this.storage_number = storage_number;
    }

    //---------------------------------------------------------------------------

}
