package com.name.business.mappers;


import com.name.business.entities.*;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DateReportCatMapper implements ResultSetMapper<DateReport2> {

    @Override
    public DateReport2 map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DateReport2(
                resultSet.getString("CATEGORIA"),
                resultSet.getLong("SUBTOTAL"),
                resultSet.getLong("TAX"),
                resultSet.getLong("TOTAL")
        );
    }
}
