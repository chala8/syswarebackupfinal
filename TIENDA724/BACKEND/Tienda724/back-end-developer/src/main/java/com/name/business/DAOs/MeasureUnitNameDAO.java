package com.name.business.DAOs;

import com.name.business.entities.CommonSimple;
import com.name.business.entities.MUName;
import com.name.business.entities.Brand;
import com.name.business.mappers.BrandMapper;
import com.name.business.mappers.MUNameMapper;
import com.name.business.representations.InventoryDetailDTO;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;



public interface MeasureUnitNameDAO {

    @SqlQuery("SELECT ID_MEASURE_UNIT FROM MEASURE_UNIT WHERE ID_MEASURE_UNIT IN (SELECT MAX(ID_MEASURE_UNIT) FROM MEASURE_UNIT)")
    Long getPkLast();

    @RegisterMapper(MUNameMapper.class)
    @SqlQuery(" select id_measure_unit,c.name measure_unit,c.description " +
            " from tienda724.measure_unit b,tienda724.common c " +
            " where b.id_common=c.id_common and id_measure_unit_father is null " +
            "and id_third is null order by 2")
    List<MUName> getFirstLevelGenericMU();


    @RegisterMapper(MUNameMapper.class)
    @SqlQuery("select id_measure_unit,c.name measure_unit,c.description " +
            " from tienda724.measure_unit b,tienda724.common c " +
            " where b.id_common=c.id_common and id_measure_unit_father=:id_measure_unit_father and id_third is null")
    List<MUName> getGenericMUByMU(@Bind("id_measure_unit_father") Long id_measure_unit_father);


    @RegisterMapper(MUNameMapper.class)
    @SqlQuery(" select id_measure_unit,c.name measure_unit,c.description\n" +
            " from tienda724.measure_unit b,tienda724.common c\n" +
            " where b.id_common=c.id_common and id_measure_unit_father is null and id_third is null\n" +
            " UNION\n" +
            " select id_measure_unit,c.name measure_unit,c.description\n" +
            " from tienda724.measure_unit b,tienda724.common c\n" +
            " where b.id_common=c.id_common and id_measure_unit_father is null and id_third =:id_third")
    List<MUName> getFirstLevelMUByThird(@Bind("id_third") Long id_third);


    @RegisterMapper(MUNameMapper.class)
    @SqlQuery(" select id_measure_unit,c.name measure_unit,c.description\n" +
            " from tienda724.measure_unit b,tienda724.common c \n" +
            " where b.id_common=c.id_common and id_measure_unit_father=:id_measure_unit_father and id_third is null\n" +
            " UNION\n" +
            " select id_measure_unit,c.name measure_unit,c.description\n" +
            " from tienda724.measure_unit b,tienda724.common c \n" +
            " where b.id_common=c.id_common and id_measure_unit_father=:id_measure_unit_father and id_third =:id_third")
    List<MUName> getMUByMUByThird(@Bind("id_measure_unit_father") Long id_measure_unit_father, @Bind("id_third") Long id_third);



    //--------------------------------------------------------------------------------



    @SqlUpdate("insert into tienda724.common(name,description)\n" +
            " values(:MUName, :MUDescription)")
    void insertCommon(@Bind("MUName") String MUName,
                      @Bind("MUDescription") String MUDescription);

    @SqlQuery("select max(id_common) from tienda724.common where name=:MUName and description=:MUDescription")
    Long getLastCommonId(@Bind("MUName") String MUName,
                         @Bind("MUDescription") String MUDescription);

    @SqlUpdate("INSERT INTO TIENDA724.MEASURE_UNIT " +
            " (id_common,id_common_state,id_measure_unit_father,id_third) " +
            " VALUES(:id_common,null,:id_measure_unit_father,:id_third) ")
    void postMeasureUnit(@Bind("id_common") Long id_common,
                         @Bind("id_measure_unit_father") Long id_measure_unit_father,
                         @Bind("id_third") Long id_third);


    //--------------------------------------------------------------------------------


    @SqlQuery(" select id_common from tienda724.measure_unit where id_measure_unit=:id_measure_unit ")
    Long getIdCommonByMU(@Bind("id_measure_unit") Long id_measure_unit);


    @SqlUpdate(" update tienda724.common set name=:name ,description=:description where id_common=:id_common ")
    void putCommon (@Bind("id_common") Long id_common,
                    @Bind("name") String name,
                    @Bind("description") String description);

    @SqlUpdate("UPDATE TIENDA724.MEASURE_UNIT " +
            " SET id_measure_unit_father=:id_measure_unit_father " +
            " WHERE id_measure_unit=:id_measure_unit")
    void putMeasureUnit (@Bind("id_measure_unit") Long id_measure_unit,
                         @Bind("id_measure_unit_father") Long id_measure_unit_father);


    //--------------------------------------------------------------------------------


    @RegisterMapper(BrandMapper.class)
    @SqlQuery(" select id_brand,brand,url_img " +
            " from tienda724.brand " +
            " where id_brand_father is null and id_third is null order by 2 ")
    List<Brand> getFirstLevelGenericBrands();

    @RegisterMapper(BrandMapper.class)
    @SqlQuery(" select id_brand,brand,url_img " +
            " from tienda724.brand " +
            " where id_brand_father=:id_brand_father and id_third is null ")
    List<Brand> getGenericBrandsByBrand(@Bind("id_brand_father") Long id_brand_father);

    @RegisterMapper(BrandMapper.class)
    @SqlQuery(" select id_brand,brand,url_img " +
            " from tienda724.brand " +
            " where id_brand_father is null and id_third is null " +
            " UNION " +
            " select id_brand,brand,url_img " +
            " from tienda724.brand " +
            " where id_brand_father is null and id_third =:id_third ")
    List<Brand> getFirstLevelBrandsByThird(@Bind("id_third") Long id_third);

    @RegisterMapper(BrandMapper.class)
    @SqlQuery(" select id_brand,brand,url_img " +
            " from tienda724.brand " +
            " where id_brand_father=2 and id_third is null " +
            " UNION " +
            " select id_brand,brand,url_img " +
            " from tienda724.brand " +
            " where id_brand_father=:id_brand_father and id_third=:id_third ")
    List<Brand> getBrandsByBrandByThird(@Bind("id_brand_father") Long id_brand_father,
                                        @Bind("id_third") Long id_third);


    //--------------------------------------------------------------------------------

    @SqlUpdate(" INSERT INTO TIENDA724.BRAND(id_brand,brand,id_brand_father,id_third,url_img) " +
            " VALUES(tienda724.brand_seq.nextval,:brand,:id_brand_father,:id_third,:url_img) ")
    void postBrand (@Bind("brand") String brand,
                    @Bind("id_brand_father") Long id_brand_father,
                    @Bind("id_third") Long id_third,
                    @Bind("url_img") String url_img);


    @SqlUpdate(" UPDATE TIENDA724.BRAND " +
            " SET brand=:brand,id_brand_father=:id_brand_father,url_img=:url_img" +
            " WHERE id_brand=:id_brand ")
    void putBrand  (@Bind("brand") String brand,
                    @Bind("id_brand_father") Long id_brand_father,
                    @Bind("url_img") String url_img,
                    @Bind("id_brand") Long id_brand);


}
