package com.name.business.resources;


import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.Kazu724Business;
import com.name.business.businesses.CodeBusiness;
import com.name.business.entities.*;
import com.name.business.entities.completes.CodeComplete;
import com.name.business.representations.KazuPucDTO;
import com.name.business.representations.KazuStatusDTO;
import com.name.business.representations.KazuDetailDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/kazu724")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class kazu724Resource {

    private Kazu724Business kazu724Business;

    public kazu724Resource(Kazu724Business kazu724Business) {
        this.kazu724Business = kazu724Business;
    }


    @GET
    @Path("/getdoctype")
    @Timed
    public Response getDocType() {

        Response response;

        Either<IException, List<docTypeKazu>> getProductsEither = kazu724Business.getDocType();
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/getdocstatus")
    @Timed
    public Response getDocStatus() {

        Response response;

        Either<IException, List<docStatus>> getProductsEither = kazu724Business.getDocStatus();
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/getcodcuentagen")
    @Timed
    public Response gecCodCuentaGen() {

        Response response;

        Either<IException, List<codigoCuenta>> getProductsEither = kazu724Business.gecCodCuentaGen();
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/getcodcuenta")
    @Timed
    public Response gecCodCuenta(@QueryParam("cp") Long cp) {

        Response response;

        Either<IException, List<codigoCuenta>> getProductsEither = kazu724Business.gecCodCuenta(cp);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    //-----------------------------------------------------------------------------------------

    @POST
    @Path("/puc")
    @Timed
    public Response postPuc(KazuPucDTO kazuPucDTO){
        Response response;

        Either<IException, Integer> mailEither = kazu724Business.postPuc(kazuPucDTO.getcc(),kazuPucDTO.getdescription(),kazuPucDTO.getccp(),kazuPucDTO.getid_country(),kazuPucDTO.getid_store());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }


    @POST
    @Path("/doc")
    @Timed
    public Response postStatus(KazuStatusDTO kazuStatusDTO){
        Response response;

        Either<IException, Long> mailEither = kazu724Business.postStatus(kazuStatusDTO.getid_document_status(),kazuStatusDTO.getid_document_type(),kazuStatusDTO.getnotes(),kazuStatusDTO.getid_store(),kazuStatusDTO.getid_third_user());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }


    @POST
    @Path("/detail")
    @Timed
    public Response postDocDetail(KazuDetailDTO kazuDetailDTO){
        Response response;

        Either<IException, Integer> mailEither = kazu724Business.postDocDetail(kazuDetailDTO.getcc(),kazuDetailDTO.getvalor(),kazuDetailDTO.getnaturaleza(),kazuDetailDTO.getnotes(),kazuDetailDTO.getid_document(),kazuDetailDTO.getid_country());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }
}


