package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.ProductNameBusiness;
import com.name.business.entities.*;
import com.name.business.representations.CodeNameDTO;
import com.name.business.representations.ProductNameDTO;
import com.name.business.representations.ProductStoreNameDTO;
import com.name.business.representations.ProductStoreInventoryNameDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;
import retrofit2.http.Query;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Path("/products2")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProductNameResource {
    private ProductNameBusiness productNameBusiness;

    public ProductNameResource(ProductNameBusiness productNameBusiness) {
        this.productNameBusiness = productNameBusiness;
    }

    @GET
    @Timed
    public Response getProductsByCategory(@QueryParam("id_category") Long id_category) {
        Response response;

        Either<IException, List<ProductName>> getProductsEither = productNameBusiness.getProductsByCategory(id_category);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/byThird")
    @Timed
    public Response getProductsByCategoryAndThird(@QueryParam("id_category") Long id_category, @QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<ProductName>> getProductsEither = productNameBusiness.getProductsByCategoryAndThird(id_category,id_third);
        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Timed
    public Response postProduct(ProductNameDTO productNameDTO) {
        Response response;

        Either<IException, String> getProductsEither = productNameBusiness.postProduct(
                productNameDTO.getproductName(),
                productNameDTO.getproductDescription(),
                productNameDTO.getid_category(),
                productNameDTO.getid_tax(),
                productNameDTO.getid_third());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Timed
    public Response putProduct(@QueryParam("id_product") Long id_product,ProductNameDTO productNameDTO) {
        Response response;

        Either<IException, String> getProductsEither = productNameBusiness.putProduct(
                id_product,
                productNameDTO.getid_category(),
                productNameDTO.getid_tax(),
                productNameDTO.getproductName(),
                productNameDTO.getproductDescription());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/code")
    @Timed
    public Response postCode(CodeNameDTO codeNameDTO) {
        Response response;

        Either<IException, String> getProductsEither = productNameBusiness.postCode(
                codeNameDTO.getcode(),
                codeNameDTO.getid_product(),
                codeNameDTO.getid_measure_unit(),
                codeNameDTO.getid_third(),
                codeNameDTO.getsuggested_price(),
                codeNameDTO.getid_brand()
        );

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/code/togeneric")
    @Timed
    public Response putCodeByThird(@QueryParam("id_code") Long id_code) {
        Response response;

        Either<IException, String> getProductsEither = productNameBusiness.putCodeByThird(id_code);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/code/general")
    @Timed
    public Response getCodesGeneralByCode(@QueryParam("code") String code) {
        Response response;

        Either<IException, List<Long>> getProductsEither = productNameBusiness.getCodesGeneralByCode(code);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/code")
    @Timed
    public Response getCodesGeneralByProduct(@QueryParam("id_product") Long id_product) {
        Response response;

        Either<IException, List<CodeName>> getProductsEither = productNameBusiness.getCodesGeneralByProduct(id_product);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/code/own")
    @Timed
    public Response getOwnCodesByProduct(@QueryParam("id_product") Long id_product,@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<CodeName>> getProductsEither = productNameBusiness.getOwnCodesByProduct(id_product,id_third);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/code/togeneric/bythird")
    @Timed
    public Response putCodesToGeneral(@QueryParam("id_code") Long id_code, CodeNameDTO codeNameDTO) {
        Response response;

        Either<IException, String> getProductsEither = productNameBusiness.putCodesToGeneral(
                id_code,
                codeNameDTO.getid_third(),
                codeNameDTO.getcode(),
                codeNameDTO.getid_measure_unit());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @PUT
    @Path("/code/update")
    @Timed
    public Response putGenericCodes(@QueryParam("id_code") Long id_code, CodeNameDTO codeNameDTO) {
        Response response;

        Either<IException, String> getProductsEither = productNameBusiness.putGenericCodes(
                id_code,
                codeNameDTO.getsuggested_price(),
                codeNameDTO.getcode(),
                codeNameDTO.getid_measure_unit());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @PUT
    @Path("/code/update/bythird")
    @Timed
    public Response putCodesByThird(@QueryParam("id_code") Long id_code, CodeNameDTO codeNameDTO) {
        Response response;

        Either<IException, String> getProductsEither = productNameBusiness.putCodesByThird(
                id_code,
                codeNameDTO.getid_third(),
                codeNameDTO.getid_measure_unit());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/storexcode")
    @Timed
    public Response getOwnCodesByProduct(@QueryParam("id_code") Long id_code) {
        Response response;

        Either<IException, List<ProductStoreName>> getProductsEither = productNameBusiness.getStoreByCode(id_code);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/storexproduct")
    @Timed
    public Response getStorageByProductStore(@QueryParam("id_product_store") Long id_product_store) {
        Response response;

        Either<IException, List<StorageName>> getProductsEither = productNameBusiness.getStorageByProductStore(id_product_store);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/storexthird")
    @Timed
    public Response getStoreByThird(@QueryParam("id_third") Long id_third) {
        Response response;

        Either<IException, List<StoreName>> getProductsEither = productNameBusiness.getStoreByThird(id_third);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/storagexstore")
    @Timed
    public Response getStorageByStore(@QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List<Storage2Name>> getProductsEither = productNameBusiness.getStorageByStore(id_store);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @GET
    @Path("/productxstorage")
    @Timed
    public Response getProductStoreByStorage(@QueryParam("id_storage") Long id_storage) {
        Response response;

        Either<IException, List<Code2Name>> getProductsEither = productNameBusiness.getProductStoreByStorage(id_storage);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @GET
    @Path("/productxstorebycode")
    @Timed
    public Response getProductStoreByCodeStore(@QueryParam("id_code") Long id_code, @QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List<Code2Name>> getProductsEither = productNameBusiness.getProductStoreByCodeStore(id_code, id_store);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @POST
    @Path("/productstore")
    @Timed
    public Response postProductStore(ProductStoreNameDTO productStoreNameDTO) {
        Response response;

        Either<IException, String> getProductsEither = productNameBusiness.postProductStore(
                productStoreNameDTO.getid_code(),
                productStoreNameDTO.getid_store(),
                productStoreNameDTO.getproduct_store_name(),
                productStoreNameDTO.getproduct_store_code(),
                productStoreNameDTO.getstandard_price(),
                productStoreNameDTO.getownbarcode()
                );

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }


    @POST
    @Path("/productstoreinventory")
    @Timed
    public Response postProductStoreInventory(ProductStoreInventoryNameDTO productStoreInventoryNameDTO) {
        Response response;

        Either<IException, String> getProductsEither = productNameBusiness.postProductStoreInventory(
                productStoreInventoryNameDTO.getid_product_store(),
                productStoreInventoryNameDTO.getid_storage(),
                productStoreInventoryNameDTO.getquantity());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @PUT
    @Path("/updateproductstore")
    @Timed
    public Response putProductStore(ProductStoreNameDTO productStoreNameDTO) {
        Response response;

        Either<IException, String> getProductsEither = productNameBusiness.putProductStore(
                productStoreNameDTO.getid_code(),
                productStoreNameDTO.getid_store(),
                productStoreNameDTO.getproduct_store_name(),
                productStoreNameDTO.getproduct_store_code(),
                productStoreNameDTO.getstandard_price(),
                productStoreNameDTO.getownbarcode()
        );

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }

    @PUT
    @Path("/updateproductstoreinventory")
    @Timed
    public Response putProductStoreInventory(ProductStoreInventoryNameDTO productStoreInventoryNameDTO) {
        Response response;

        Either<IException, String> getProductsEither = productNameBusiness.putProductStoreInventory(
                productStoreInventoryNameDTO.getid_product_store(),
                productStoreInventoryNameDTO.getid_storage(),
                productStoreInventoryNameDTO.getquantity());

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }




    //-------------------------------------------------------------





    @GET
    @Path("/inventoryList")
    @Timed
    public Response getInventoryList(@QueryParam("id_store") Long id_store) {
        Response response;

        Either<IException, List<InventoryName>> getProductsEither = productNameBusiness.getInventoryList(id_store);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }






    @GET
    @Path("/quantity")
    @Timed
    public Response getQuantity(@QueryParam("id_product_store") Long id_product_store) {
        Response response;

        Either<IException, Long> getProductsEither = productNameBusiness.getQuantity(id_product_store);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }



    @POST
    @Path("/procedure2")
    public Response procedure2(@QueryParam("idps") Long idps,
                               @QueryParam("costo") Double costo,
                               @QueryParam("precioanterior") Double precioanterior,
                               @QueryParam("precionuevo") Double precionuevo,
                               @QueryParam("cantidad") Long cantidad) {
        Response response;

        Either<IException, Long> getProducts = productNameBusiness.procedure2(idps,costo,precioanterior,precionuevo,cantidad);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }









    //-----------------------------------------------------




    @GET
    @Path("/code/byid")
    @Timed
    public Response getCodeByIdCode(@QueryParam("id_code") Long id_code) {
        Response response;

        Either<IException, List<codeData>> getProductsEither = productNameBusiness.getCodeByIdCode(id_code);

        if (getProductsEither.isRight()) {
            System.out.println(getProductsEither.right().value());
            response = Response.status(Response.Status.OK).entity(getProductsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(getProductsEither);
        }
        return response;
    }




}
