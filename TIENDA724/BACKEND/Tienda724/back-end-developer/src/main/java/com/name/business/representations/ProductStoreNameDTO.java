package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.name.business.entities.Common;

public class ProductStoreNameDTO {


    private Long id_code;
    private Long id_store;
    private String product_store_name;
    private Long product_store_code;
    private Long standard_price;
    private String ownbarcode;

    @JsonCreator
    public ProductStoreNameDTO(@JsonProperty("id_code") Long id_code,
                                @JsonProperty("id_store") Long id_store,
                                @JsonProperty("product_store_name") String product_store_name,
                                @JsonProperty("product_store_code") Long product_store_code,
                                @JsonProperty("standard_price") Long standard_price,
                                @JsonProperty("ownbarcode") String ownbarcode) {
        this.id_code = id_code;
        this.id_store = id_store;
        this.product_store_name = product_store_name;
        this.product_store_code = product_store_code;
        this.standard_price = standard_price;
        this.ownbarcode = ownbarcode;
    }

    //------------------------------------------------------------------------------------------------

    public Long getid_code() {
        return id_code;
    }

    public void setid_code(Long id_code) {
        this.id_code = id_code;
    }

    //------------------------------------------------------------------------------------------------

    public Long getid_store() { return id_store; }

    public void setid_store(Long id_store) { this.id_store = id_store; }

    //------------------------------------------------------------------------------------------------

    public String getproduct_store_name() {
        return product_store_name;
    }

    public void setproduct_store_name(String product_store_name) {
        this.product_store_name = product_store_name;
    }

    //------------------------------------------------------------------------------------------------

    public Long getstandard_price() { return standard_price; }

    public void setstandard_price(Long standard_price) { this.standard_price = standard_price; }

    //------------------------------------------------------------------------------------------------

    public Long getproduct_store_code() { return product_store_code; }

    public void setproduct_store_code(Long product_store_code) { this.product_store_code = product_store_code; }

    //------------------------------------------------------------------------------------------------

    public String getownbarcode() {
        return ownbarcode;
    }

    public void setownbarcode(String ownbarcode) {
        this.ownbarcode = ownbarcode;
    }

    //------------------------------------------------------------------------------------------------

    }
