package com.name.business.businesses;


import com.name.business.DAOs.ProductNameDAO;
import com.name.business.entities.*;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.List;

import static com.name.business.utils.constans.K.*;


public class ProductNameBusiness {

    private ProductNameDAO productNameDAO;

    public ProductNameBusiness(ProductNameDAO productNameDAO) {
        this.productNameDAO = productNameDAO;
    }


    public Either<IException, List<ProductName> > getProductsByCategory(Long id_category) {
        try {
            List<ProductName> validator = productNameDAO.getProductsByCategory(id_category);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<ProductName> > getProductsByCategoryAndThird(Long id_category, Long id_third) {
        try {
            List<ProductName> validator = productNameDAO.getProductsByCategoryAndThird(id_category, id_third);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postProduct(String productName, String productDescription,
                                                   Long id_category, Long id_tax,
                                                   Long id_third) {
        try {
            productNameDAO.insertCommon(productName, productDescription);
            Long id_common = productNameDAO.getLastCommonId(productName, productDescription);
            productNameDAO.insertProduct(id_common,id_category,id_tax,id_third);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putProduct(Long id_product, Long id_category, Long id_tax, String productName, String productDescription) {
        try {
            Long id_common = productNameDAO.getCommon(id_product);
            productNameDAO.updateCommon(productName, productDescription,id_common);
            productNameDAO.updateProduct(id_category,id_tax,id_product);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postCode(String code, Long id_product, Long id_measure_unit, Long id_third, Long suggested_price, Long id_brand) {
        try {
            productNameDAO.postCode(code,id_product,id_measure_unit,id_third,suggested_price, id_brand);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putCodeByThird(Long id_code) {
        try {
            productNameDAO.putCodeByThird(id_code);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<CodeName> > getCodesGeneralByProduct(Long id_product) {
        try {
            return Either.right(productNameDAO.getCodesGeneralByProduct(id_product));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<CodeName> > getOwnCodesByProduct(Long id_product, Long id_third) {
        try {
            return Either.right(productNameDAO.getOwnCodesByProduct(id_product,id_third));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Long> > getCodesGeneralByCode(String code) {
        try {
            return Either.right(productNameDAO.getCodesGeneralByCode(code));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putGenericCodes(Long id_code, Long SuggestedPrice, String code, Long id_measure_unit) {
        try {
            productNameDAO.putGenericCodes(id_code, id_measure_unit, code, SuggestedPrice);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putCodesToGeneral(Long id_code, Long id_third, String code, Long id_measure_unit) {
        try {
            productNameDAO.putCodesToGeneral(id_code, id_third, code, id_measure_unit);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putCodesByThird(Long id_code, Long id_third,  Long id_measure_unit) {
        try {
            productNameDAO.putCodesByThird(id_code, id_third, id_measure_unit);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<ProductStoreName> > getStoreByCode(Long id_code) {
        try {
            return Either.right(productNameDAO.getStoreByCode(id_code));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<StorageName> > getStorageByProductStore(Long id_product_store) {
        try {
            return Either.right(productNameDAO.getStorageByProductStore(id_product_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    public Either<IException, List<StoreName> > getStoreByThird(Long id_third) {
        try {
            return Either.right(productNameDAO.getStoreByThird(id_third));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, List<Storage2Name> > getStorageByStore(Long id_store) {
        try {
            return Either.right(productNameDAO.getStorageByStore(id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Code2Name> > getProductStoreByStorage(Long Id_storage) {
        try {
            System.out.println(productNameDAO.getProductStoreByStorage(Id_storage));
            return Either.right(productNameDAO.getProductStoreByStorage(Id_storage));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, List<Code2Name> > getProductStoreByCodeStore(Long id_code, Long id_store) {
        try {
            return Either.right(productNameDAO.getProductStoreByCodeStore(id_code, id_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    public Either<IException, String > postProductStore(Long id_code,
                                                        Long id_store,
                                                        String product_store_name,
                                                        Long product_store_code,
                                                        Long standard_price,
                                                        String ownbarcode) {
        try {
            productNameDAO.postProductStore(id_code,id_store,product_store_name,product_store_code,standard_price,ownbarcode);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > postProductStoreInventory(Long id_product_store,
                                                                 Long id_storage,
                                                                 Long quantity) {
        try {
            productNameDAO.postProductStoreInventory(id_product_store,id_storage,quantity);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putProductStore(Long id_code,
                                                        Long id_store,
                                                        String product_store_name,
                                                        Long product_store_code,
                                                        Long standard_price,
                                                        String ownbarcode) {
        try {
            productNameDAO.putProductStore(id_code,id_store,product_store_name,product_store_code,standard_price,ownbarcode);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }

    public Either<IException, String > putProductStoreInventory(Long id_product_store,
                                                                 Long id_storage,
                                                                 Long quantity) {
        try {
            productNameDAO.putProductStoreInventory(id_product_store,id_storage,quantity);
            return Either.right("OK!!!");
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }


    //---------------------------------------------------------





    public Either<IException, List<InventoryName> > getInventoryList(Long ID_STORE) {
        try {
            return Either.right(productNameDAO.getInventoryList(ID_STORE));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }





    public Either<IException, Long> procedure2(Long idps,
                                               Double costo,
                                               Double precioanterior,
                                               Double precionuevo,
                                                               Long cantidad) {
        try {
            productNameDAO.procedure2( idps,
                     costo,
                     precioanterior,
                     precionuevo,
                     cantidad);
            Long validator = new Long (1);
            return Either.right(validator);
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }







    public Either<IException, Long > getQuantity(Long id_product_store) {
        try {
            return Either.right(productNameDAO.getQuantity(id_product_store));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }



    //---------------------------------------------------------



    public Either<IException, List<codeData> > getCodeByIdCode(Long id_code) {
        try {
            return Either.right(productNameDAO.getCodeByIdCode(id_code));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(e.toString())));
        }
    }
}
