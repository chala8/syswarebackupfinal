package com.name.business.mappers;

import com.name.business.entities.Category;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CategoryMapper implements ResultSetMapper<Category>{

    @Override
    public Category map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Category(
                resultSet.getLong("ID_CATEGORY"),
                new Common(resultSet.getLong("ID_COMMON"),
                        resultSet.getString("NAME"),
                        resultSet.getString("DESCRIPTION")),
                resultSet.getString("IMG_URL"),
                resultSet.getLong("ID_CATEGORY_FATHER"),
                resultSet.getLong("ID_THIRD_CATEGORY"),
                new CommonState(resultSet.getLong("ID_STATE"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_CATEGORY"),
                        resultSet.getDate("MODIFY_CATEGORY")
                )
        );
    }
}
