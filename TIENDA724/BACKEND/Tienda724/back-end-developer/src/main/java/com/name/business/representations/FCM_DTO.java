package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;

/**
 * Created by luis on 8/04/17.
 */
public class FCM_DTO {


    private String title;
    private String description;
    private String data_info;
    private Integer status;
    private Timestamp date_sent;
    private String type;

    @JsonCreator
    public FCM_DTO(@JsonProperty("title") String title,@JsonProperty("description") String description,
                   @JsonProperty("data_info") String data_info,@JsonProperty("status") Integer status,
                   @JsonProperty("date_sent") Timestamp date_sent,@JsonProperty("type") String type) {

        this.title = title;
        this.description = description;
        this.data_info = data_info;
        this.status = status;
        this.date_sent = date_sent;
        this.type=type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getData_info() {
        return data_info;
    }

    public void setData_info(String data_info) {
        this.data_info = data_info;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Timestamp getDate_sent() {
        return date_sent;
    }

    public void setDate_sent(Timestamp date_sent) {
        this.date_sent = date_sent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
