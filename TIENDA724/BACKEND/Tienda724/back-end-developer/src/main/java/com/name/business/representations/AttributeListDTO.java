package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class AttributeListDTO {
    private List<AttributeDetailListDTO> attributeDetailListDTOS;
    private CommonStateDTO stateDTO;


    @JsonCreator
    public AttributeListDTO(@JsonProperty("details") List<AttributeDetailListDTO> attributeDetailListDTOS,
            @JsonProperty("state") CommonStateDTO stateDTO) {

        this.attributeDetailListDTOS=attributeDetailListDTOS;
        this.stateDTO = stateDTO;

    }

    public List<AttributeDetailListDTO> getAttributeDetailListDTOS() {
        return attributeDetailListDTOS;
    }

    public void setAttributeDetailListDTOS(List<AttributeDetailListDTO> attributeDetailListDTOS) {
        this.attributeDetailListDTOS = attributeDetailListDTOS;
    }

    public CommonStateDTO getStateDTO() {
        return stateDTO;
    }

    public void setStateDTO(CommonStateDTO stateDTO) {
        this.stateDTO = stateDTO;
    }
}
