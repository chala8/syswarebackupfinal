package com.name.business.mappers;

import com.name.business.entities.StorageName;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StorageNameMapper implements ResultSetMapper<StorageName>{

    @Override
    public StorageName map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new StorageName(
                resultSet.getLong("ID_STORAGE"),
                resultSet.getString("DESCRIPTION"),
                resultSet.getLong("STORAGE_NUMBER"),
                resultSet.getLong("QUANTITY")

        );
    }
}