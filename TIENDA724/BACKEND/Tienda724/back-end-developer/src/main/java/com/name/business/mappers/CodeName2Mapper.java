package com.name.business.mappers;

import com.name.business.entities.CodeName;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CodeName2Mapper implements ResultSetMapper<CodeName>{

    @Override
    public CodeName map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CodeName(
                resultSet.getLong("ID_CODE"),
                resultSet.getString("CODE"),
                resultSet.getString("PRODUCT_NAME"),
                resultSet.getString("UNIDAD"),
                resultSet.getLong("SUGGESTED_PRICE"),
                resultSet.getString("BRAND")

        );
    }
}