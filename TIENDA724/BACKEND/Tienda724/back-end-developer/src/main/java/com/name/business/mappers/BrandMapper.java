package com.name.business.mappers;

import com.name.business.entities.Brand;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BrandMapper implements ResultSetMapper<Brand>{

    @Override
    public Brand map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Brand(
                resultSet.getLong("ID_BRAND"),
                resultSet.getString("BRAND"),
                resultSet.getString("URL_IMG")
        );
    }
}