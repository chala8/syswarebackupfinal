package com.name.business.entities;

import java.util.Date;

public class DiasRotacion {

    private String LINEA;
    private String CATEGORIA;
    private String MARCA;
    private String PRODUCTO;
    private Long CANTIDADACTUAL;
    private Long CANTIDADVENDIDA;
    private Double DIAS_ROTACION;


    public DiasRotacion(String LINEA,
                          String CATEGORIA,
                          String MARCA,
                          String PRODUCTO,
                          Long CANTIDADACTUAL,
                          Long CANTIDADVENDIDA,
                          Double DIAS_ROTACION) {
        this.LINEA = LINEA;
        this.CATEGORIA = CATEGORIA;
        this.MARCA = MARCA;
        this.PRODUCTO = PRODUCTO;
        this.CANTIDADACTUAL = CANTIDADACTUAL;
        this.CANTIDADVENDIDA = CANTIDADVENDIDA;
        this.DIAS_ROTACION = DIAS_ROTACION;

    }
    public String getLINEA() {
        return LINEA;
    }

    public void setLINEA(String LINEA) {
        this.LINEA = LINEA;
    }

    public String getCATEGORIA() {
        return CATEGORIA;
    }

    public void setCATEGORIA(String CATEGORIA) {
        this.CATEGORIA = CATEGORIA;
    }

    public String getMARCA() { return MARCA; }

    public void setMARCA(String MARCA) {
        this.MARCA = MARCA;
    }

    public String getPRODUCTO() {
        return PRODUCTO;
    }

    public void setPRODUCTO(String PRODUCTO) {
        this.PRODUCTO = PRODUCTO;
    }

    public Long getCANTIDADACTUAL() {
        return CANTIDADACTUAL;
    }

    public void setCANTIDADACTUAL(Long CANTIDADACTUAL) {
        this.CANTIDADACTUAL = CANTIDADACTUAL;
    }

    public Long getCANTIDADVENDIDA() {
        return CANTIDADVENDIDA;
    }

    public void setCANTIDADVENDIDA(Long COSTO) {
        this.CANTIDADVENDIDA = CANTIDADVENDIDA;
    }

    public Double getDIAS_ROTACION() {
        return DIAS_ROTACION;
    }

    public void setDIAS_ROTACION(Double DIAS_ROTACION) {
        this.DIAS_ROTACION = DIAS_ROTACION;
    }

}

