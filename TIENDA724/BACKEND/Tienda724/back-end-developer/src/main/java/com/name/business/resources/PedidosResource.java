package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.PedidosBusiness;
import com.name.business.businesses.PriceListBusiness;
import com.name.business.entities.*;
import com.name.business.representations.PedidoDTO;
import com.name.business.representations.PlanillaDTO;
import com.name.business.representations.PlanillaDetailDTO;
import com.name.business.representations.PlanillaPdfDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;

@Path("/pedidos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PedidosResource {
    private PedidosBusiness pedidosBusiness;


    public PedidosResource(PedidosBusiness pedidosBusiness) {
        this.pedidosBusiness = pedidosBusiness;
    }



    @GET
    @Path("/ps")
    @Timed
    public Response getOwnBarCode(
            @QueryParam("id_product_store") Long id_product_store){
        Response response;

        Either<IException, String> getProducts = pedidosBusiness.getOwnBarCode(id_product_store);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/vehiculos")
    @Timed
    public Response getVehiculos(){
        Response response;

        Either<IException, List<Vehiculo>> getProducts = pedidosBusiness.getVehiculos();

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @PUT
    @Path("/closeplanilla")
    @Timed
    public Response updatePlanilla(@QueryParam("observaciones") String observaciones,
                                   @QueryParam("idplanilla") Long idplanilla){
        Response response;
        Either<IException, Integer> allViewOffertsEither = pedidosBusiness.updatePlanilla(observaciones, idplanilla);
        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }


    @GET
    @Path("/planillas")
    @Timed
    public Response getPlanillas(@QueryParam("idvehiculo") Long idvehiculo,
                                 @QueryParam("idstore") Long idstore){
        Response response;

        Either<IException, List<Planilla>> getProducts = pedidosBusiness.getPlanillas(idvehiculo,idstore);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/planillaDetail")
    @Timed
    public Response getPlanillaDetail(@QueryParam("idplanilla") Long idplanilla){
        Response response;

        Either<IException, List<PlanillaDetail>> getProducts = pedidosBusiness.getPlanillaDetail(idplanilla);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }





    @POST
    @Path("/procedure")
    @Timed
    public Response postProcedure(    @QueryParam("idbill") Long idbill,
                                      @QueryParam("idbillstate") Long idbillstate,
                                      @QueryParam("code") Long code,
                                      @QueryParam("idstore") Long idstore,
                                      @QueryParam("cantidad") Long cantidad,
                                      @QueryParam("tipo") String tipo){
        Response response;

        Either<IException, String > getProducts =  pedidosBusiness.postProcedure(idbill,
                idbillstate,
                code,
                idstore,
                cantidad,
                tipo);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



    @POST
    @Path("/pdf")
    @Timed
    public Response postpdf(PlanillaPdfDTO planilla){
        Response response;
        PlanillaDTO master = planilla.getmaster();
        List<PlanillaDetailDTO> detalles = planilla.getdetalles();

        Either<IException, String> getProducts = pedidosBusiness.postpdf(
                master,
                detalles
        );

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
/*

        System.out.println("---------------------MASTER--------------------");
        System.out.println(master.getconductor());
        System.out.println(master.getfecha_INICIO());
        System.out.println(master.getid_PLANILLA());
        System.out.println(master.getnum_PLANILLA());
        System.out.println(master.getplaca());
        System.out.println("---------------------DETALLES--------------------");
        for( PlanillaDetailDTO element : detalles ){
            System.out.println(element.getaddress());
            System.out.println(element.getnum_DOCUMENTO());
            System.out.println(element.getstore());
            System.out.println(element.getvalor());
        }
  */      return response;
    }




    @GET
    @Path("/own")
    @Timed
    public Response getPsId(
            @QueryParam("code") String code,
            @QueryParam("id_store") Long id_store){
        Response response;

        Either<IException, Long> getProducts = pedidosBusiness.getPsId(code,id_store);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @GET
    @Path("/master")
    @Timed
    public Response getPedidos(
            @QueryParam("id_store") Long id_store,
            @QueryParam("id_bill_state") Long id_bill_state,
            @QueryParam("id_bill_type") Long id_bill_type){
        Response response;

        Either<IException, List<Pedido>> getProducts = pedidosBusiness.getPedidos(id_store,id_bill_state,id_bill_type);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }


    @POST
    @Path("/detalles")
    @Timed
    public Response getDetallesPedidos(PedidoDTO priceListDTO){
        Response response;

        Either<IException, List<DetallePedido>> mailEither = pedidosBusiness.getDetallesPedidos(priceListDTO.getlistaTipos());


        if (mailEither.isRight()){

            response=Response.status(Response.Status.OK).entity(mailEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(mailEither);
        }
        return response;
    }



    @GET
    @Path("/GET_VALOR_DOMICILIO")
    @Timed
    public Response GET_VALOR_DOMICILIO(
            @QueryParam("IDSTORE") String IDSTORE,
            @QueryParam("DISTANCIA_KM") String DISTANCIA_KM){
        Response response;

        Either<IException, Float> getProducts = pedidosBusiness.GET_VALOR_DOMICILIO(IDSTORE,DISTANCIA_KM);

        if (getProducts.isRight()){
            System.out.println(getProducts.right().value());
            response=Response.status(Response.Status.OK).entity(getProducts.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getProducts);
        }
        return response;
    }



}
