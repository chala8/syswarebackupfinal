package com.name.business.mappers;

import com.name.business.entities.CategoryName;
import com.name.business.entities.CategoryReport;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductReportMapper implements ResultSetMapper<CategoryReport>{

    @Override
    public CategoryReport map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new CategoryReport(
                resultSet.getString("OWNBARCODE"),
                resultSet.getString("PRODUCTO"),
                resultSet.getLong("VENTA"),
                resultSet.getLong("COSTO"),
                resultSet.getLong("UTILIDAD"),
                resultSet.getDouble("PCT_MARGEN"),
                resultSet.getLong("NUMVENTAS")
        );
    }
}