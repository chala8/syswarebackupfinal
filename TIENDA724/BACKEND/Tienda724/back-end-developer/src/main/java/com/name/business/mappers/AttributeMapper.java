package com.name.business.mappers;

import com.name.business.entities.Attribute;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AttributeMapper implements ResultSetMapper<Attribute> {
    @Override
    public Attribute map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Attribute(
                resultSet.getLong("ID_ATTRIBUTE"),
                new Common(
                        resultSet.getLong("ID_COMMON"),
                        resultSet.getString("NAME"),
                        resultSet.getString("DESCRIPTION")
                ),
                new CommonState(
                        resultSet.getLong("ID_STATE"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_ATTRIBUTE"),
                        resultSet.getDate("MODIFY_ATTRIBUTE")
                )
        );
    }
}
