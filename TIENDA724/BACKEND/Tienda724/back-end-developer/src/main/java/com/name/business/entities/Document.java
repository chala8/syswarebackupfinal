package com.name.business.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Document {

    private Long id_common;
    private String name;
    private String description;

    @JsonCreator
    public Document(@JsonProperty("id_common")Long id_common, @JsonProperty("title") String name,
                    @JsonProperty("body") String description) {
        this.id_common = id_common;
        this.name = name;
        this.description = description;
    }

    public Long getId_common() {
        return id_common;
    }

    public void setId_common(Long id_common) {
        this.id_common = id_common;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
