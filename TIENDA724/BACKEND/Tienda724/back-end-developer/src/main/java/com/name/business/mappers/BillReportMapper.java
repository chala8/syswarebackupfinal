package com.name.business.mappers;

import com.name.business.entities.CategoryName;
import com.name.business.entities.BillReport;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BillReportMapper implements ResultSetMapper<BillReport>{

    @Override
    public BillReport map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new BillReport(
                resultSet.getString("FULLNAME"),
                resultSet.getString("PREFIX_BILL"),
                resultSet.getString("PURCHASE_DATE"),
                resultSet.getLong("ID_BILL"),
                resultSet.getLong("CONSECUTIVE"),
                resultSet.getLong("VENTA"),
                resultSet.getLong("COSTO"),
                resultSet.getLong("UTILIDAD"),
                resultSet.getDouble("PCT_MARGEN_VENTA"),
                resultSet.getString("CAJA"),
                resultSet.getString("NUM_DOCUMENTO"),
                resultSet.getDouble("PCT_MARGEN_COSTO"),
                resultSet.getString("TIENDA"),
                resultSet.getString("CAJERO"),
                resultSet.getLong("ID_BILL_STATE")

        );
    }
}