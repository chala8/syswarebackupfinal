package com.name.business.mappers;

import com.name.business.entities.StoreName;
import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StoreNameMapper implements ResultSetMapper<StoreName>{

    @Override
    public StoreName map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new StoreName(
                resultSet.getLong("ID_STORE"),
                resultSet.getString("DESCRIPTION")

        );
    }
}