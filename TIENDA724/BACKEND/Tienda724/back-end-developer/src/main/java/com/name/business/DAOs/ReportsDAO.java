package com.name.business.DAOs;
import java.time.LocalDate;
import com.name.business.entities.*;
import com.name.business.mappers.*;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.unstable.BindIn;

import java.util.Date;
import java.util.List;


@UseStringTemplate3StatementLocator
public interface ReportsDAO {

    @RegisterMapper(DateReportMapper.class)
    @SqlQuery(" select sum(b.subtotal) subtotal,sum(b.tax) tax,sum(b.totalprice) Total " +
            " from facturacion724.bill b " +
            " where b.id_bill_type=:id_bill_type and b.id_bill_state=1 and b.purchase_date between :date1 and :date2" +
            " and b.id_third=:id_third and b.id_store in (<listStore>) ")
    DateReport getBillTotal(@Bind("id_bill_type") Long id_bill_type,
                            @Bind("date1") Date date1,
                            @Bind("date2") Date date2,
                            @Bind("id_third") Long id_third,
                            @BindIn ("listStore") List<Long> listStore);

    @RegisterMapper(DateReportCatMapper.class)
    @SqlQuery(" select co.name categoria,sum(b.subtotal) subtotal,sum(b.tax) tax,sum(b.totalprice) Total\n" +
            " from facturacion724.bill b,facturacion724.detail_bill d,tienda724.product_store ps, \n" +
            "     tienda724.codes c, TIENDA724.PRODUCT p, tienda724.category cat, tienda724.common co\n" +
            " where b.id_bill=d.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code \n" +
            "      and c.id_product=p.id_product and p.id_category=cat.id_category and cat.id_common=co.id_common \n" +
            "      and b.id_bill_type=:id_bill_type and b.purchase_date between :date1 and :date2\n" +
            " group by co.name\n" +
            " order by 2 desc ")
    DateReport2 getTotalByCategory(@Bind("id_bill_type") Long id_bill_type,
                                   @Bind("date1") Date date1,
                                   @Bind("date2") Date date2);

    @RegisterMapper(DateReportBrandMapper.class)
    @SqlQuery(" select br.brand Marca,sum(b.subtotal) subtotal,sum(b.tax) tax,sum(b.totalprice) Total\n" +
            " from facturacion724.bill b,facturacion724.detail_bill d,tienda724.product_store ps, \n" +
            "     tienda724.codes c, tienda724.brand br\n" +
            " where b.id_bill=d.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code and c.id_brand=br.id_brand  \n" +
            "      and b.id_bill_type=:id_bill_type and b.purchase_date between :date1 and :date2\n" +
            " group by br.brand \n" +
            " order by 2 desc ")
    DateReport2 getTotalByBrand(@Bind("id_bill_type") Long id_bill_type,
                                @Bind("date1") Date date1,
                                @Bind("date2") Date date2);


    @RegisterMapper(DateReportProdMapper.class)
    @SqlQuery(" select ps.product_store_name,sum(b.subtotal) subtotal,sum(b.tax) tax,sum(b.totalprice) Total\n" +
            " from facturacion724.bill b,facturacion724.detail_bill d,tienda724.product_store ps\n" +
            " where b.id_bill=d.id_bill and d.id_product_third=ps.id_product_store\n" +
            "  and b.id_bill_type=:id_bill_type and b.purchase_date between :date1 and (:date2+0.99) and ps.id_store=:id_store\n" +
            " group by ps.product_store_name\n" +
            " order by 2 desc ")
    List <DateReport2> getTotalByProduct(@Bind("id_bill_type") Long id_bill_type,
                                         @Bind("date1") Date date1,
                                         @Bind("date2") Date date2,
                                         @Bind("id_store") Long id_store);


    @RegisterMapper(LabelReportMapper.class)
    @SqlQuery(" select sum(totalprice) as TOTAL\n" +
            " from facturacion724.bill b,facturacion724.detail_bill d,tienda724.product_store ps,tienda724.codes c,tienda724.product p\n" +
            " where b.id_bill=d.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=c.id_code and c.id_product=p.id_product and\n" +
            " id_bill_state=1 and id_bill_type=:id_bill_type and p.id_category=:id_category and purchase_date between :date1 and :date2 " +
            " and b.id_third = :id_third and ps.ID_STORE in (<listStore>)")
    LabelReport getTotalByBillTypeCategory(@Bind("id_bill_type") Long id_bill_type,
                                           @Bind("id_category") Long id_category,
                                           @Bind("date1") Date date1,
                                           @Bind("date2") Date date2,
                                           @Bind("id_third") Long id_third,
                                           @BindIn ("listStore") List<Long> listStore);

    @RegisterMapper(LabelReportMapper.class)
    @SqlQuery(" select sum(totalprice) as TOTAL " +
            " from facturacion724.bill b,facturacion724.detail_bill d " +
            " where b.id_bill=d.id_bill " +
            " and b.id_bill_state=1 and id_bill_type=:id_bill_type and d.id_product_third=:id_product_store and b.id_store in (<id_store>) and purchase_date between :date1 and :date2 ")
    LabelReport getTotalByBillTypeProduct(@Bind("id_bill_type") Long id_bill_type,
                                          @Bind("id_product_store") Long id_product_store,
                                          @Bind("date1") Date date1,
                                          @Bind("date2") Date date2,
                                          @BindIn("id_store") List<Long> id_store);


    @RegisterMapper(CategoryReportMapper.class)
    @SqlQuery(" select t.name categoria,sum(quantity*price) venta,sum(quantity*standard_price) costo,\n" +
            "       sum(quantity*price)-sum(quantity*standard_price) utilidad,\n" +
            "       round((sum(quantity*price)-sum(quantity*standard_price))*100/sum(quantity*standard_price),2) pct_margen,\n" +
            "       sum(quantity) numventas\n" +
            " from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps,tienda724.codes co,tienda724.product pr,\n" +
            "      tienda724.category cat,tienda724.common t\n" +
            " where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store and ps.id_code=co.id_code and co.id_product=pr.id_product\n" +
            "   and pr.id_category=cat.id_category and cat.id_common=t.id_common\n" +
            "   and id_bill_type=1 and b.id_third=:id_third and ps.id_store=:id_store and purchase_date between :date1 and :date2+1\n" +
            " group by t.name\n" +
            " order by 4 desc")
    List <CategoryReport> getTotalesFacturacionPorPeriodoCategorias(@Bind("id_third") Long id_third,
                                                     @Bind("id_store") Long id_store, @Bind("date1") Date date1,
                                                     @Bind("date2") Date date2);

    @RegisterMapper(ProductReportMapper.class)
    @SqlQuery(" select ps.ownbarcode, ps.product_store_name producto,sum(quantity*price) venta,sum(quantity*standard_price) costo,\n" +
            "       sum(quantity*price)-sum(quantity*standard_price) utilidad,\n" +
            "       round((sum(quantity*price)-sum(quantity*standard_price))*100/sum(quantity*standard_price),2) pct_margen,\n" +
            "       sum(quantity) numventas\n" +
            " from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps\n" +
            " where d.id_bill=b.id_bill and d.id_product_third=ps.id_product_store\n" +
            "   and id_bill_type=1 and b.id_third=:id_third and ps.id_store=:id_store and purchase_date between :date1 and :date2+1\n" +
            " group by ps. ownbarcode, ps.product_store_name\n" +
            " order by 4 desc ")
    List <CategoryReport> getTotalesFacturacionPorPeriodoProductos(@Bind("id_third") Long id_third,
                                                                    @Bind("id_store") Long id_store, @Bind("date1") Date date1,
                                                                    @Bind("date2") Date date2);

    @RegisterMapper(BillReportMapper.class)
    @SqlQuery(" select b.id_bill_state, b.num_documento, co.fullname,l.prefix_bill,b.purchase_date,b.id_bill,b.consecutive,sum(quantity*price) venta,sum(quantity*standard_price) costo,\n" +
            "       sum(quantity*price)-sum(quantity*standard_price) utilidad,round((sum(quantity*price)-sum(quantity*standard_price))*100/sum(quantity*price),2) pct_margen_venta,\n" +
            "       round((sum(quantity*price)-sum(quantity*standard_price))*100/sum(quantity*standard_price),2) pct_margen_costo,\n" +
            "       st.description tienda,ca.caja_number caja,cbi.fullname cajero\n" +
            " from facturacion724.detail_bill d,facturacion724.bill b,tienda724.product_store ps,tercero724.third t,tercero724.legal_data l,\n" +
            "     tercero724.common_basicinfo co,tienda724.caja ca,tienda724.store st,tercero724.third cajero,tercero724.common_basicinfo cbi\n" +
            " where d.id_bill=b.id_bill and b.id_bill_state in (1,41) and d.id_product_third=ps.id_product_store and b.id_third=t.id_third\n" +
            "  and t.id_common_basicinfo=co.id_common_basicinfo and t.id_legal_data=l.id_legal_data and b.id_caja=ca.id_caja(+)\n" +
            "  and b.id_store=st.id_store and b.id_third_employee=cajero.id_third and cajero.id_common_basicinfo=cbi.id_common_basicinfo\n" +
            "  and b.id_bill_type=:typemove and b.id_third=:id_third and b.id_store=:id_store and b.purchase_date between to_date(:date1,'yyyy/mm/dd') and to_date(:date2,'yyyy/mm/dd')+1\n" +
            " group by b.id_bill_state, b.num_documento, co.fullname,l.prefix_bill,b.purchase_date,b.id_bill,b.consecutive,st.description,ca.caja_number,cbi.fullname\n" +
            " order by b.purchase_date desc ")
    List <BillReport> getTotalesFacturacionPorPeriodo(@Bind("id_third") Long id_third,
                                                      @Bind("id_store") Long id_store,
                                                      @Bind("date1") String date1,
                                                      @Bind("date2") String date2,
                                                      @Bind("typemove") Long typemove);

    @RegisterMapper(PriceMapper.class)
    @SqlQuery(" select price_description, price from price where ID_PRODUCT_STORE= :id_ps ")
    List <Price> getPriceByPS(@Bind("id_ps") Long id_ps);

    @RegisterMapper(InvetoryReportMapper.class)
    @SqlQuery("select ownbarcode barcode,product_store_code CodigoTienda,com.name linea,co.name categoria,b.brand marca,ps.product_store_name producto,psi.quantity cantidad,ps.standard_price costo,price precio,\n" +
            "       quantity*standard_price costototal, round(quantity*standard_price*100/:response,2) pct_inventario\n" +
            " from tienda724.common co,tienda724.category cat,tienda724.common com,tienda724.category linea,tienda724.product p,tienda724.codes c,\n" +
            "     tienda724.product_store ps,tienda724.product_store_inventory psi,tienda724.brand b,tienda724.measure_unit mu,tienda724.common comu,tienda724.price p\n" +
            " where co.id_common=cat.id_common and cat.id_category=p.id_category and com.id_common=linea.id_common and linea.id_category=cat.id_category_father\n" +
            "  and p.id_product=c.id_product and c.id_code=ps.id_code and ps.id_product_store=psi.id_product_store and c.id_brand=b.id_brand\n" +
            "  and c.id_measure_unit=mu.id_measure_unit and mu.id_common=comu.id_common and ps.id_product_store=p.id_product_store\n" +
            "  and cat.id_category_father is not null\n" +
            "  and ps.id_store in (<listStore>) and linea.id_category in(<listLine>) and cat.id_category in(<listCategory>) and b.id_brand in(<listBrand>)\n" +
            " order by 1,2,4")
    List <InvetoryReport> getInventarioCons(@Bind("response") Long response,
                                            @BindIn("listStore") List<Long> listStore,
                                            @BindIn("listLine") List<Long> listLine,
                                            @BindIn("listCategory") List<Long> listCategory,
                                            @BindIn("listBrand") List<Long> listBrand);

    @SqlQuery(" select sum(quantity*standard_price) costototal from tienda724.product_store ps,tienda724.product_store_inventory psi\n" +
            " where ps.id_product_store=psi.id_product_store and id_store in (<listStore>) ")
    Long get5Mil(@BindIn("listStore") List<Long> listStore);


    @RegisterMapper(DiasRotacionMapper.class)
    @SqlQuery(" select com.name linea,co.name categoria,b.brand marca,ps.product_store_name producto\n" +
              "     ,psi.quantity cantidadactual,sum(db.quantity) cantidadvendida,ROUND(psi.quantity/(sum(db.quantity)/:days),1) DIAS_ROTACION\n" +
              " from facturacion724.bill b,facturacion724.detail_bill db,\n" +
              "     tienda724.common co,tienda724.category cat,tienda724.common com,tienda724.category linea,tienda724.product p,tienda724.codes c,\n" +
              "     tienda724.product_store ps,tienda724.product_store_inventory psi,tienda724.brand b,tienda724.measure_unit mu,tienda724.common comu\n" +
              " where b.id_bill(+)=db.id_bill and db.id_product_third(+)=ps.id_product_store and  co.id_common=cat.id_common and cat.id_category=p.id_category and com.id_common=linea.id_common and linea.id_category=cat.id_category_father\n" +
              "  and p.id_product=c.id_product and c.id_code=ps.id_code and ps.id_product_store=psi.id_product_store and c.id_brand=b.id_brand\n" +
              "  and c.id_measure_unit=mu.id_measure_unit and mu.id_common=comu.id_common and cat.id_category_father is not null\n" +
              "  and ps.id_store in (<listStore>) and linea.id_category in(<listLine>) and cat.id_category in(<listCategory>)\n" +
              "  and b.id_brand in(<listBrand>)\n" +
              "  and b.purchase_date between sysdate-:days and sysdate\n" +
              "  and b.id_bill_type=1 and b.id_bill_state=1\n" +
              " group by com.name,co.name,b.brand,ps.product_store_name, psi.quantity \n" +
              " order by DIAS_ROTACION desc ")
    List <DiasRotacion> getDiasRotacion(@Bind("days") Long days,
                                        @BindIn("listStore") List<Long> listStore,
                                        @BindIn("listLine") List<Long> listLine,
                                        @BindIn("listCategory") List<Long> listCategory,
                                        @BindIn("listBrand") List<Long> listBrand);

    @RegisterMapper(Brand2Mapper.class)
    @SqlQuery(" select ID_BRAND,BRAND from tienda724.BRAND ")
    List <Brand> getBrands();


    @SqlQuery(" select a.ID_CATEGORY from category a" +
            " where a.ID_CATEGORY_FATHER is not null and (select b.ID_TIPO_STORE from category b where a.ID_CATEGORY_FATHER = b.ID_CATEGORY) in (<listStore>) order by 1 ")
    List <Long> getSonCat(@BindIn("listStore") List<Long> listStore);


    @SqlQuery(" select quantity from facturacion724.detail_bill where id_bill=:idbill and \n" +
            " id_product_third in (select id_product_store from tienda724.product_store where ownbarcode=:code and id_store in (<listStore>)) ")
    Long getNoDisponibles(@Bind("idbill") Long idbill,
                          @BindIn("listStore") List<Long> listStore,
                          @Bind("code") String code);

    @RegisterMapper(NoDispMapper.class)
    @SqlQuery(" select ownbarcode,nvl(sum(db.quantity),0) no_disponible\n" +
            "from facturacion724.detail_bill db, tienda724.product_store ps\n" +
            "where db.id_product_third=ps.id_product_store and ps.id_store in (<listStore>)\n" +
            "  and id_bill in (-4001,-4002,-4003,-4004,-4005)\n" +
            "group by ownbarcode ")
    List <NoDisp> getSumNoDisponibles(@BindIn("listStore") List<Long> listStore);

}


 