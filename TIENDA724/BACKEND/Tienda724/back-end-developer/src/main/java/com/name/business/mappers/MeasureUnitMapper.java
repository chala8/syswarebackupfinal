package com.name.business.mappers;


import com.name.business.entities.Common;
import com.name.business.entities.CommonState;
import com.name.business.entities.MeasureUnit;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MeasureUnitMapper implements ResultSetMapper<MeasureUnit> {
    @Override
    public MeasureUnit map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new MeasureUnit(
                resultSet.getLong("ID_MEASURE_UNIT"),
                resultSet.getLong("ID_MEASURE_UNIT_FATHER"),
                new Common(
                        resultSet.getLong("ID_COMMON"),
                        resultSet.getString("NAME"),
                        resultSet.getString("DESCRIPTION")
                ),
                new CommonState(
                        resultSet.getLong("ID_STATE"),
                        resultSet.getInt("STATE"),
                        resultSet.getDate("CREATION_MEASURE_UNIT"),
                        resultSet.getDate("MODIFY_MEASURE_UNIT")
                )
        );
    }
}
