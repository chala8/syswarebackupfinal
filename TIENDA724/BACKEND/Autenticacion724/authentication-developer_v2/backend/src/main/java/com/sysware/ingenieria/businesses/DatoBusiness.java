package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.DatoDAO;
import com.sysware.ingenieria.representations.DatoDTO;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.sysware.ingenieria.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.sysware.ingenieria.utils.constans.K.messages_error;
import static com.sysware.ingenieria.utils.constans.K.messages_errors;

/**
 * Created by luis on 19/04/17.
 */
public class DatoBusiness {

    private DatoDAO datoDAO;

    public DatoBusiness(DatoDAO datoDAO) {
        this.datoDAO = datoDAO;
    }

    public Either<IException, Long> crearDato(DatoDTO datoDTO) {
        List<String> msn=new ArrayList<>();
        try {
            msn.add("OK");
            System.out.println("            Inicia a crear Dato    ***********************************");
            if (datoDTO!=null){
                long id_dato = datoDAO.CREAR_DATO(datoDTO);
                if (id_dato>0){
                    return Either.right(id_dato);
                }else{
                    msn.add("No se puedo registrar el dato");

                }
            }else{
                msn.add("No se reconoce el Dato, esta mal formado");

            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
