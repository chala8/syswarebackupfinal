package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Menus;
import com.sysware.ingenieria.mappers.MenuMapper;
import com.sysware.ingenieria.representations.MenuDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(MenuMapper.class)
public interface MenuDAO {

    @SqlQuery("SELECT ID_MENU FROM MENUS WHERE ID_MENU IN (SELECT MAX( ID_MENU ) FROM MENUS )\n ")
    Long getPkLast();

    @SqlQuery("SELECT  * FROM menus  menu  WHERE"+
            " (menu.id_menu=:id_menu OR :id_menu IS NULL) AND"+
            " (menu.id_aplicacion=:id_aplicacion OR :id_aplicacion IS NULL) AND"+
            " (menu.nombre=:nombre OR :nombre IS NULL) AND"+
            " (menu.tipo=:tipo OR :tipo IS NULL) AND"+
            " (menu.ruta=:ruta OR :ruta IS NULL) AND"+
            " (menu.icono=:icono OR :icono IS NULL) AND"+
            " (menu.avatar=:avatar OR :avatar IS NULL) AND"+
            " (menu.background=:background OR :background IS NULL) AND"+
            " (menu.dispositivo=:dispositivo OR :dispositivo IS NULL) AND"+
            " (menu.estado=:estado OR :estado IS NULL)")
    List<Menus> OBTENER_MENUS(@Bind("id_menu")Long id_menu, @Bind("id_aplicacion") Long id_aplicacion,@Bind("nombre") String nombre,@Bind("tipo") Integer tipo,@Bind("ruta") String ruta,@Bind("icono") String icono,@Bind("avatar") String avatar,@Bind("background") String background,@Bind("estado") Integer estado,@Bind("dispositivo") String dispositivo);

    @SqlUpdate("INSERT INTO menus ( id_aplicacion, nombre, tipo, ruta, icono, avatar, background,estado) VALUES (:menu.id_aplicacion, :menu.nombre, :menu.tipo, :menu.ruta, :menu.icono, :menu.avatar, :menu.background,:menu.estado)")
    long CREAR_MENU(@Bind("id_usuario") Long id_usuario_app, @BindBean("menu")MenuDTO menuDTO);

    @SqlQuery("SELECT count(id_aplicacion) FROM menus  menu   WHERE " +
              " (menu.id_aplicacion=:id_aplicacion AND  menu.nombre=:nombre) ")
    Integer VERIFICAR_MENU(@Bind("id_aplicacion") Long id_usuario_app,@Bind("nombre") String nombre);






}
