package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.UsuarioAppDAO;
import com.sysware.ingenieria.entities.UsuarioApp;
import com.sysware.ingenieria.representations.AuthDTO;
import com.sysware.ingenieria.representations.UsuarioAppDTO;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.ManagementException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.sysware.ingenieria.utils.constans.K.*;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoLIKESql;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoLongSql;



public class UsuarioAppBusiness {

    private UsuarioAppDAO usuarioAppDAO;

    public UsuarioAppBusiness(UsuarioAppDAO usuarioAppDAO) {
        this.usuarioAppDAO = usuarioAppDAO;
    }

    public Either<IException,List<UsuarioApp>> obtenerUsuarioApp(Long id_usuario_app, String usuario, String clave){
        try {
            List<UsuarioApp> aplicacionList = usuarioAppDAO.OBTENER_USUARIO(formatoLongSql(id_usuario_app),formatoLIKESql(usuario),formatoLIKESql(clave));
            return Either.right(aplicacionList);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }


    public Either<IException,List<UsuarioApp>> verificarUsuario(String nombre,String clave){
        try {
            if ((formatoLIKESql(nombre)!=null) &&(formatoLIKESql(clave)!=null)){
                return Either.right(usuarioAppDAO.VEFICAR_CREDENCIALES(formatoLIKESql(nombre),(formatoLIKESql(clave))));

            }else if((formatoLIKESql(nombre)!=null)){
                return Either.right(usuarioAppDAO.OBTENER_USUARIO_POR_NOMBRE((nombre.toLowerCase())));

            }else if((formatoLIKESql(clave)!=null)){
                return Either.right(usuarioAppDAO.OBTENER_USUARIO_POR_NOMBRE((clave.toLowerCase())));
            }else{
                return Either.left(new BussinessException("No se puede identificar el usuario con los parametros."));
            }


        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }



    public Either<IException, Long> crearUsuarioApp(UsuarioAppDTO usuarioAppDTO) {
        List<String> msn=new ArrayList<>();

        try {
            if (usuarioAppDTO.getUsuario()==null || usuarioAppDTO.getUsuario().isEmpty()||usuarioAppDTO.getClave()==null || usuarioAppDTO.getClave().isEmpty()){
                msn.add("No se reconoce formato del usuario , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            Either<IException, List<UsuarioApp>> usuarioApp = verificarUsuario( usuarioAppDTO.getUsuario(), null);

            System.out.println("            Inicia a crear Oferta    ***********************************");
            if (usuarioApp.isRight() && usuarioApp.right().value().isEmpty()){


            if (usuarioAppDTO!=null){

                long id_oferta = usuarioAppDAO.CREAR_USUARIOAPP(usuarioAppDTO.getUsuario().toLowerCase(),usuarioAppDTO.getClave().toLowerCase());

                if (id_oferta>0){

                    return Either.right(usuarioAppDAO.getPkLast());


                }else{
                    msn.add("No se reconoce formato del usuario , esta mal formado");

                }
            }
            }else{
                msn.add("El Usuario ["+usuarioAppDTO.getUsuario()+"] ya existe!");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException,Integer> validarUsuario(String nombre){
        try {
           if((formatoLIKESql(nombre)!=null)) {
               Either<IException, List<UsuarioApp>> verificarUsuario = verificarUsuario(nombre, null);

               if (verificarUsuario.isRight() && verificarUsuario.right().value().isEmpty()){
                   return Either.right(NO_EXISTE_COINCIDENCIA);
               }else{
                   return Either.right(EXISTE_COINCIDENCIA);
               }
           }else{
                return Either.left(new BussinessException("No se puede identificar el usuario con los parametros."));
            }


        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }

    public Either<IException,UsuarioApp> iniarSesion(AuthDTO authDTO){
        try {
            String usuario=authDTO.getUsuario();
            String clave=authDTO.getClave();
            if ((formatoLIKESql(usuario)!=null) &&(formatoLIKESql(clave)!=null)){
                List<UsuarioApp> usuarioApps = usuarioAppDAO.VEFICAR_CREDENCIALES(usuario, clave);
                if (!usuarioApps.isEmpty()&& usuarioApps.size()==1){
                    return Either.right(usuarioApps.get(0));
                }else{
                    return Either.left(new BussinessException(INVALID_USER_PASS));
                }
            }else {
                return Either.left(new BussinessException("No se puede identificar el usuario con los parametros."));
            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }


}
