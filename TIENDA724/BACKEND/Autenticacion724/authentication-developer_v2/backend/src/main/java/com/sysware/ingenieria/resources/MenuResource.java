package com.sysware.ingenieria.resources;

import com.sysware.ingenieria.businesses.MenuBusiness;
import com.sysware.ingenieria.entities.Menus;
import com.sysware.ingenieria.entities.Rol;
import com.sysware.ingenieria.representations.MenuDTO;
import com.sysware.ingenieria.representations.RolDTO;
import com.sysware.ingenieria.utils.exeptions.ExceptionResponse;
import com.sysware.ingenieria.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/menus")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MenuResource {
    private MenuBusiness menuBusiness;

    public MenuResource(MenuBusiness menuBusiness) {
        this.menuBusiness = menuBusiness;
    }

    @GET
    public Response obtenerListaMenus(@QueryParam("id_menu")Long id_menu,@QueryParam("id_aplicacion") Long id_aplicacion,
                                             @QueryParam("nombre") String nombre,@QueryParam("tipo") Integer tipo,
                                             @QueryParam("ruta") String ruta,@QueryParam("icono") String icono,
                                             @QueryParam("avatar") String avatar,@QueryParam("background") String background,
                                             @QueryParam("estado") Integer estado,
                                             @QueryParam("dispositivo") String dispositivo){
        Response response;

        Either<IException, List<Menus>> allViewOffertsEither = menuBusiness.obtenerMenus(id_menu,  id_aplicacion,  nombre,  tipo,  ruta,  icono,  avatar,  background, estado,dispositivo);


        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @POST
    public Response crearMenu(@QueryParam("id_usuario_app")Long id_usuario, MenuDTO menuDTO) {
        Either<IException, Long> offerEither = menuBusiness.crearMenu(id_usuario,menuDTO);
        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @GET
    @Path("/rol")
    public Response obtenerListaMenusPorRol(@QueryParam("id_rol")Long id_rol,@QueryParam("id_menu")Long id_menu){
        Response response;

        Either<IException, List<Menus>> allViewOffertsEither = menuBusiness.obtenerListaMenusPorRol(id_rol,  id_menu);


        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }


}
