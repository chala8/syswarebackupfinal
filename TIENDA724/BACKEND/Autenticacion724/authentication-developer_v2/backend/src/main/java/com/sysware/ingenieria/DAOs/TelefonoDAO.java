package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Telefono;
import com.sysware.ingenieria.mappers.TelefonoMapper;
import com.sysware.ingenieria.representations.TelefonoDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * Created by luis on 30/06/17.
 */
@RegisterMapper(TelefonoMapper.class)
public interface TelefonoDAO {

    @SqlQuery("SELECT ID_TELEFONO FROM TELEFONOS WHERE ID_TELEFONO IN (SELECT MAX( ID_TELEFONO ) FROM TELEFONOS)\n ")
    Long getPkLast();


    @SqlBatch("INSERT INTO telefonos  ( id_directorio, numero, tipo, preferencia ) " +
            "        VALUES (:id_directorio, :numero, :tipo, :preferencia ) ")
    void CREAR_MAILS(@Bind("id_directorio") Long id_directorio,
                     @Bind("numero") List<String> mailTelList,
                     @Bind("tipo") List<String> tipoList,
                     @Bind("preferencia") List<Integer> preferenciaList);

    @SqlUpdate("UPDATE telefonos SET numero=:tel.numero, tipo=:tel.tipo, " +
            " preferencia=:tel.preferencia  WHERE  (id_directorio=:id_directorio AND id_telefono=:tel.id_telefono) ")
    Long EDITAR_TELEFONO(@Bind("id_directorio") Long id_directorio,@BindBean("tel") Telefono telefono);

    @SqlUpdate("DELETE  FROM  telefonos WHERE (id_directorio=:id_directorio or :id_directorio IS NULL ) AND (id_telefono=:id_telefono or :id_telefono IS NULL )")
    Integer ELIMINAR_TELEFONO(@Bind("id_directorio") Long id_directorio, @Bind("id_telefono")Long id_telefono);


    @SqlQuery(" SELECT  * FROM telefonos  tel " +
            "    WHERE (tel.id_telefono=:id_telefono OR :id_telefono IS NULL ) AND " +
            "    (tel.id_directorio=:id_directorio OR :id_directorio IS NULL ) AND " +
            "    (tel.numero LIKE :numero OR :numero IS NULL ) AND " +
            "    (tel.tipo LIKE :tipo OR :tipo IS NULL ) AND " +
            "    (tel.preferencia = :preferencia OR :preferencia IS NULL )")
    List<Telefono> OBTENER_TELEFONOS(@Bind("id_telefono") Long id_telefono,@Bind("id_directorio") Long id_directorio,
                                     @Bind("numero") String numero,@Bind("tipo") String tipo,
                                     @Bind("preferencia") Integer preferencia);

}
