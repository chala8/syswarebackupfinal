package com.sysware.ingenieria.threads;

import com.google.gson.JsonObject;
import com.sysware.ingenieria.businesses.FCMBusiness;
import com.sysware.ingenieria.entities.FCM;
import com.sysware.ingenieria.services.handle.FCMHelper;
import com.sysware.ingenieria.utils.exeptions.IException;
import fj.data.Either;

import java.util.List;
import java.util.TimerTask;

/**
 * Created by luis on 8/04/17.
 */
public class FCMThread extends TimerTask {
    private FCMBusiness fcmBusiness;

    public FCMThread(FCMBusiness fcmBusiness) {
        super();
        this.fcmBusiness = fcmBusiness;
    }



    @Override
    public boolean cancel() {
        return super.cancel();
    }

    @Override
    public long scheduledExecutionTime() {
        return super.scheduledExecutionTime();
    }

    @Override
    public void run() {
        Either<IException, List<FCM>> fcm = fcmBusiness.getFCM(null, null, null, null);
        if (fcm.isRight()){
            beginFCM(fcm.right().value());


        }


    }

    private void beginFCM(List<FCM> value) {
        for (FCM fcm: value ) {
            System.out.println("******************************+++++++++++++");
            System.out.println("Promociones "+fcm.getTitle());
            System.out.println("Estado "+fcm.getStatus());
            sentFCMDevise(fcm);
            fcm.setStatus(0);
            fcmBusiness.updateFCM(fcm);
        }
    }

    public static void sentFCMDevise(FCM fcmModel) {
        FCMHelper fcm = FCMHelper.getInstance();
        JsonObject dataObject = new JsonObject();
        dataObject.addProperty("titulo", "Este es el titular"); // See GSON-Reference
        dataObject.addProperty("descripcion", "Aquí estará todo el contenido de la noticia");// See GSON-Reference


        JsonObject notificationObject = new JsonObject();
        notificationObject.addProperty("title", "Notificacion "); // See GSON-Reference
        notificationObject.addProperty("body", "Enviado desde el Servidor Dropwizard!"); // See GSON-Reference


            try {
                    fcm.sendData(FCMHelper.TYPE_TO,"e3nTRKp9AV4:APA91bF10ajMqNDI7zVLkRcT4TMbKVA2geiQ0kLyIticI49vpHErawqnHXLWZsu5Fv3UhFH9T9qQxqrRjcukp8rXlKmJ-FqYqa0wxvLCjgMAZJXw9rY4FC1ES06LBw6fNVEwk6xfOubF",dataObject);


                  // fcm.sendTopicNotificationAndData("",notificationObject, dataObject);






                //fcm.sendData(FCMHelper.TYPE_TO, DEVICE_TEST, dataObject); // RECIPIENT is the token of the device, or device group, or a topic.
            } catch (Exception e) {
                e.printStackTrace();

            }


    }

    public static void main(String[] args) {
        sentFCMDevise(null);
    }
}
