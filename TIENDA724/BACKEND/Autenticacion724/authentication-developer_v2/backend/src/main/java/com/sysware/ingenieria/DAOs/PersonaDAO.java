package com.sysware.ingenieria.DAOs;


import com.sysware.ingenieria.entities.PersonaUsuario;
import com.sysware.ingenieria.mappers.PersonaUsuarioMapper;
import com.sysware.ingenieria.representations.PersonaDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.ExternalizedSqlViaStringTemplate3;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.unstable.BindIn;

import java.util.List;

/**
 * Created by luis on 30/06/17.
 */
@UseStringTemplate3StatementLocator

public interface PersonaDAO {

    @SqlQuery("SELECT ID_PERSONA FROM PERSONAS WHERE ID_PERSONA IN (SELECT MAX( ID_PERSONA ) FROM PERSONAS) ")
    Long getPkLast();

    @SqlUpdate("INSERT INTO personas  ( uid_usuario,documento, " +
            "                        tipo_documento,primer_nombre, " +
            "                        segundo_nombre,primer_apellido, " +
            "                        segundo_apellido) " +
            "        VALUES ( :pe.uid_usuario,:pe.documento, " +
            "                 :pe.tipo_documento,:pe.primer_nombre, " +
            "                 :pe.segundo_nombre,:pe.primer_apellido, " +
            "                 :pe.segundo_apellido)  ")
    Long CREAR_USUARIO(@BindBean("pe") PersonaDTO personaDTO);

    @SqlUpdate("UPDATE personas SET documento=:ap.documento, tipo_documento=:ap.tipo_documento, " +
            " primer_nombre=:ap.primer_nombre, segundo_nombre=:ap.segundo_nombre," +
            " primer_apellido=:ap.primer_apellido, segundo_apellido=:ap.segundo_apellido  WHERE  (uid_usuario=:uuid_usuario) ")
    int EDITAR_PERSONA(@Bind("uuid_usuario")Integer uuid_usuario, @BindBean("ap") PersonaDTO personaDTO);


    @SqlUpdate("DELETE  FROM personas WHERE uid_usuario=:uid_usuario")
    int ELIMINAR_PERSONA(@Bind("id_persona") Long id_persona,@Bind("uid_usuario") Long uid_usuario);

    @SqlQuery("SELECT id_persona FROM personas  pe  WHERE " +
            " (pe.uid_usuario=:uid_usuario )")
    long OBTENER_IDPERSONA(@Bind("uid_usuario")Long uid_usuario);


    @RegisterMapper(PersonaUsuarioMapper.class)
    @SqlQuery("SELECT * FROM v_persona_usuario   pe_us " +
            "  WHERE "+
            "        (pe_us.id_aplicacion=:id_aplicacion OR :id_aplicacion IS NULL ) AND " +
            "        (pe_us.usuario=:usuario OR :usuario IS NULL ) AND " +
            "        (pe_us.clave=:clave OR :clave IS NULL ) AND " +
            "        (pe_us.id_persona=:id_persona OR :id_persona IS NULL ) AND " +
            "        (pe_us.documento=:documento OR :documento IS NULL ) AND " +
            "        (pe_us.tipo_documento=:tipo_documento OR :tipo_documento IS NULL ) AND " +
            "        (pe_us.primer_nombre=:primer_nombre OR :primer_nombre IS NULL ) AND " +
            "        (pe_us.segundo_nombre=:segundo_nombre OR :segundo_nombre IS NULL ) AND " +
            "        (pe_us.primer_apellido=:primer_apellido OR :primer_apellido IS NULL ) AND " +
            "        (pe_us.segundo_apellido=:segundo_apellido OR :segundo_apellido IS NULL ) ")
    List<PersonaUsuario> LISTA_PERSONA( @Bind("id_aplicacion")Long id_aplicacion,
                                       @Bind("usuario") String usuario, @Bind("clave")String clave,
                                       @Bind("id_persona") Long id_persona,
                                       @Bind("documento") String documento, @Bind("tipo_documento") String tipo_documento,
                                       @Bind("primer_nombre") String primer_nombre, @Bind("segundo_nombre") String segundo_nombre,
                                       @Bind("primer_apellido") String primer_apellido, @Bind("segundo_apellido") String segundo_apellido);

    @RegisterMapper(PersonaUsuarioMapper.class)
    @SqlQuery("SELECT * FROM v_persona_usuario   pe_us " +
            "  WHERE (pe_us.uuid in (<uuid>) ) AND " +
            "        (pe_us.id_aplicacion=:id_aplicacion OR :id_aplicacion IS NULL ) AND " +
            "        (pe_us.usuario=:usuario OR :usuario IS NULL ) AND " +
            "        (pe_us.clave=:clave OR :clave IS NULL ) AND " +
            "        (pe_us.id_persona=:id_persona OR :id_persona IS NULL ) AND " +
            "        (pe_us.documento=:documento OR :documento IS NULL ) AND " +
            "        (pe_us.tipo_documento=:tipo_documento OR :tipo_documento IS NULL ) AND " +
            "        (pe_us.primer_nombre=:primer_nombre OR :primer_nombre IS NULL ) AND " +
            "        (pe_us.segundo_nombre=:segundo_nombre OR :segundo_nombre IS NULL ) AND " +
            "        (pe_us.primer_apellido=:primer_apellido OR :primer_apellido IS NULL ) AND " +
            "        (pe_us.segundo_apellido=:segundo_apellido OR :segundo_apellido IS NULL ) ")
    List<PersonaUsuario> LISTA_PERSONA_UUID(@BindIn("uuid") List<Long> uuid, @Bind("id_aplicacion")Long id_aplicacion,
                                       @Bind("usuario") String usuario, @Bind("clave")String clave,
                                       @Bind("id_persona") Long id_persona,
                                       @Bind("documento") String documento, @Bind("tipo_documento") String tipo_documento,
                                       @Bind("primer_nombre") String primer_nombre, @Bind("segundo_nombre") String segundo_nombre,
                                       @Bind("primer_apellido") String primer_apellido, @Bind("segundo_apellido") String segundo_apellido);


    /*Long CREAR_USUARIO(@Bind("uid_usuario") Integer uid_usuario,@Bind("documento") String documento,
                       @Bind("tipo_documento") String tipo_documento,@Bind("primer_nombre") String primer_nombre,
                       @Bind("segundo_nombre") String segundo_nombre,@Bind("primer_apellido") String primer_apellido,
                       @Bind("segundo_apellido") String segundo_apellido);*/
}
