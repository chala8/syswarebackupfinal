package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by luis on 30/06/17.
 */
public class TelefonoDTO {
    private String numero;
    private String tipo;
    private Integer preferencia;

    @JsonCreator
    public TelefonoDTO(@JsonProperty("numero") String numero,@JsonProperty("tipo") String tipo,
                       @JsonProperty("preferencia") Integer preferencia) {
        this.numero = numero;
        this.tipo = tipo;
        this.preferencia = preferencia;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getPreferencia() {
        return preferencia;
    }

    public void setPreferencia(Integer preferencia) {
        this.preferencia = preferencia;
    }
}
