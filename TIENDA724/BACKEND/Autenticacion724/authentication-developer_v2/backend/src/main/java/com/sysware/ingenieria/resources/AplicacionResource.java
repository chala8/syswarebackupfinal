package com.sysware.ingenieria.resources;


import com.sysware.ingenieria.businesses.AplicacionBusiness;
import com.sysware.ingenieria.entities.Aplicacion;
import com.sysware.ingenieria.representations.AplicacionDTO;
import com.sysware.ingenieria.utils.exeptions.ExceptionResponse;
import com.sysware.ingenieria.utils.exeptions.IException;
import fj.data.Either;



import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Path("/aplicaciones")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AplicacionResource {
    AplicacionBusiness aplicacionBusiness;

    public AplicacionResource(AplicacionBusiness aplicacionBusiness) {
        this.aplicacionBusiness = aplicacionBusiness;
    }

    @GET
    public Response obtenerListaAplicaciones(@QueryParam("id_aplicacion")Long id_applicacion,
                                             @QueryParam("id_usuario_app")Long id_usuario_app,
                                             @QueryParam("nombre") String nombre,
                                             @QueryParam("descripcion") String descripcion, @QueryParam("version") String version,
                                             @QueryParam("fecha_creacion") String fecha_creacion, @QueryParam("fecha_actualizacion") String fecha_actualizacion,
                                             @QueryParam("key_aplicacion") String key_aplicacion, @QueryParam("key_servidor") String key_servidor){
        Response response;

        Either<IException, List<Aplicacion>> allViewOffertsEither = aplicacionBusiness.obtenerAplicaciones(id_applicacion,id_usuario_app, nombre, descripcion, version, fecha_creacion, fecha_actualizacion, key_aplicacion, key_servidor);


        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @POST
    public Response crearOferta(@QueryParam("id_usuario")Long id_ususario, AplicacionDTO aplicacionDTO) {
        Either<IException, Long> offerEither = aplicacionBusiness.crearAplicacion(id_ususario, aplicacionDTO);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @PUT
    public Response modificarAplicacion(@QueryParam("id_aplicacion")Long id_aplicacion ,@QueryParam("id_usuario_app")Long id_usuario, AplicacionDTO aplicacionDTO) {
        Either<IException, Long> offerEither = aplicacionBusiness.modificarAplicacion(id_aplicacion,id_usuario, aplicacionDTO);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }
}
