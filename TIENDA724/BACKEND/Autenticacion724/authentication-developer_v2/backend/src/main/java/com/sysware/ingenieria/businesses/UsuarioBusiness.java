package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.UsuarioDAO;
import com.sysware.ingenieria.entities.*;
import com.sysware.ingenieria.representations.*;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.ManagementException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.sysware.ingenieria.utils.constans.K.*;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.*;
import static com.sysware.ingenieria.utils.security.CodeGenerator._uuid;

/**
 * Created by luis on 14/05/17.
 */
public class UsuarioBusiness {
    private UsuarioDAO usuarioDAO;
    private RolBusiness rolBusiness;
    private MenuBusiness menuBusiness;
    private PersonaBusiness personaBusiness;


    public UsuarioBusiness(UsuarioDAO usuarioDAO, RolBusiness rolBusiness, MenuBusiness menuBusiness, PersonaBusiness personaBusiness) {
        this.usuarioDAO = usuarioDAO;
        this.rolBusiness = rolBusiness;
        this.menuBusiness = menuBusiness;
        this.personaBusiness = personaBusiness;
    }

    public Either<IException,List<Usuario>> obtenerUsuarioApp(Long id_aplicacion, Long id_usuario_app, String usuario, String clave){
        try {
            List<Usuario> aplicacionList = usuarioDAO.OBTENER_USUARIO(formatoLongSql(id_aplicacion),formatoLongSql(id_usuario_app),formatoLIKESql(usuario),formatoLIKESql(clave));
            return Either.right(aplicacionList);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }

    public Either<IException, Long> crearUsuarioFULL(PersonaFullDatosDTO  personaFullDatosDTO) {
        List<String> msn=new ArrayList<>();
        try {
            if ( personaFullDatosDTO==null || personaFullDatosDTO.getUsuarioDTO()==null){
                msn.add("No se reconoce formato del Usuario , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                UsuarioDTO usuarioAppDTO=personaFullDatosDTO.getUsuarioDTO();
                Either<IException, Long> usuarioAppEither = crearUsuarioApp(usuarioAppDTO);
                if (usuarioAppEither.isRight() && personaFullDatosDTO.getPersonaDTO()!=null && personaFullDatosDTO.getDirectorioDTO()!=null ){

                    PersonaDTO personaDTO = personaFullDatosDTO.getPersonaDTO();
                    personaDTO.setUid_usuario(Math.toIntExact(usuarioAppEither.right().value()));
                    PersonaDirectorioDTO personaDirectorioDTO= new PersonaDirectorioDTO(personaDTO,personaFullDatosDTO.getDirectorioDTO());
                    Either<IException, Long> crearPersonaEither = personaBusiness.crearPersona(personaFullDatosDTO);
                    return Either.right(usuarioAppEither.right().value());

                }else{
                    msn.add("No se reconoce formato del Usuario , esta mal formado");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }

            }





        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }

    }

    public Either<IException, Long> crearUsuarioApp( UsuarioDTO usuarioAppDTO) {
        List<String> msn=new ArrayList<>();
        List<Long> id_rolesParaCrear=new ArrayList<>();

        try {
            if (usuarioAppDTO.getId_aplicacion()==null|| usuarioAppDTO.getUsuario()==null || usuarioAppDTO.getUsuario().isEmpty()||usuarioAppDTO.getClave()==null || usuarioAppDTO.getClave().isEmpty()){
                msn.add("No se reconoce formato del usuario , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            Either<IException, List<Usuario>> usuarioApp = verificarUsuario( formatoLongSql(usuarioAppDTO.getId_aplicacion()),null,usuarioAppDTO.getUsuario().toLowerCase(), null);

            System.out.println("            Inicia a crear Usuario    ***********************************");
            if (usuarioApp.isRight() && usuarioApp.right().value().isEmpty()){


                if (usuarioAppDTO!=null){
                    Integer _uuid = _uuid();

                    long id_usuario = usuarioDAO.CREAR_USUARIOAPP(formatoLongSql(usuarioAppDTO.getId_aplicacion()),_uuid,usuarioAppDTO.getUsuario().toLowerCase(),usuarioAppDTO.getClave());

                    if (id_usuario>0){ // Si se crea ok usuario,

                        if (usuarioAppDTO.getId_roles()!=null || usuarioAppDTO.getId_roles().size()>0){
                            id_rolesParaCrear=usuarioAppDTO.getId_roles();

                            //Llamar business para crear los usuarios roles
                            if (usuarioAppDTO.getId_roles().size()>0){
                                Either<IException, Long> crearUsuarioRolesEither = crearUsuarioRoles(id_usuario,_uuid, usuarioAppDTO.getId_roles());


                            }

                        }

                        return Either.right(Long.valueOf(_uuid));


                    }else{
                        msn.add("No se reconoce formato del usuario , esta mal formado");

                    }
                }
            }else{
                msn.add("El Usuario ["+usuarioAppDTO.getUsuario()+"] ya existe!");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException,List<Usuario>> verificarUsuario(Long id_aplicacion, Integer _uuid, String nombre, String clave){
        try {
            if ((formatoLongSql(id_aplicacion)!=null) && (formatoStringSql(nombre)!=null) &&(formatoStringSql(clave)!=null)){
                return Either.right(usuarioDAO.VEFICAR_CREDENCIALES(formatoLongSql(id_aplicacion),formatoStringSql(nombre),(formatoStringSql(clave))));

            }else if((formatoLongSql(id_aplicacion)!=null) &&(formatoStringSql(nombre)!=null)){

                if ((formatoIntegerSql(_uuid)!=null)){
                    return Either.right(usuarioDAO.OBTENER_USUARIO_POR_NOMBRE(formatoLongSql(id_aplicacion),formatoIntegerSql(_uuid),(nombre.toLowerCase())));
                }else{
                    return Either.right(usuarioDAO.OBTENER_USUARIO_POR_NOMBRE(formatoLongSql(id_aplicacion),formatoIntegerSql(_uuid),(nombre.toLowerCase())));
                }


            }else if((formatoLongSql(id_aplicacion)!=null) &&(formatoStringSql(clave)!=null)){
                return Either.right(usuarioDAO.OBTENER_USUARIO_POR_NOMBRE(formatoLongSql(id_aplicacion),formatoIntegerSql(_uuid),(clave.toLowerCase())));
            }else{
                return Either.left(new BussinessException("No se puede identificar el usuario con los parametros."));
            }


        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }

    public Either<IException, Integer> editarUsuario(Integer _uuid, UsuarioDTO usuarioAppDTO, boolean actualizarRol) {
        List<String> msn=new ArrayList<>();

        try {
            if (_uuid==null||usuarioAppDTO.getId_aplicacion()==null|| usuarioAppDTO.getUsuario()==null || usuarioAppDTO.getUsuario().isEmpty()||usuarioAppDTO.getClave()==null || usuarioAppDTO.getClave().isEmpty()){
                msn.add("No se reconoce formato del usuario , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            Either<IException, List<Usuario>> usuarioApp = verificarUsuario( usuarioAppDTO.getId_aplicacion(),_uuid,usuarioAppDTO.getUsuario(), null);

            System.out.println("            Inicia a modificar roles    ***********************************");
            if (usuarioApp.isRight() && usuarioApp.right().value().isEmpty()){


                if (usuarioAppDTO!=null){

                    int id_oferta = usuarioDAO.EDITAR_USUARIO(formatoLongSql(usuarioAppDTO.getId_aplicacion()),formatoIntegerSql(_uuid),usuarioAppDTO.getUsuario().toLowerCase(),usuarioAppDTO.getClave().toLowerCase());

                    if (id_oferta>0){

                        if (usuarioAppDTO.getId_roles()!=null||usuarioAppDTO.getId_roles().size()>0){

                            if (actualizarRol){
                                Either<IException, Long> crearUsuarioRolesEither = editarUsuarioRoles(null, _uuid, usuarioAppDTO.getId_roles());
                            }else{
                                Either<IException, Long> crearUsuarioRolesEither = crearUsuarioRoles(null, _uuid, usuarioAppDTO.getId_roles());
                            }



                        }

                        return Either.right(_uuid);


                    }else{
                        msn.add("No se reconoce formato del usuario , esta mal formado");

                    }
                }
            }else{
                msn.add("El Usuario ["+usuarioAppDTO.getUsuario()+"] ya existe!");
            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> editarUsuarioRoles(Long id_usuario, Integer _uuid, List<Long> id_rolLista) {
        List<String> msn=new ArrayList<>();
        List<Long> id_rolListaFinal=new ArrayList<>();
        try {

            if (formatoIntegerSql(_uuid)==null|| id_rolLista.size()<=0){
                msn.add("Fatan datos  id rol y/o id rol");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            for (Long id_rol:id_rolLista ){
                if (verificarUsuarioRoles(id_usuario,_uuid,id_rol)){
                    msn.add("El rol "+id_rol+" ya existe");

                }else{
                    id_rolListaFinal.add(id_rol);
                }
            }
            if (id_rolListaFinal.size()==0){
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear Rol    ***********************************");

            long id_rol_menu=0;
            for (Long id_rol:id_rolListaFinal ){
                id_rol_menu = usuarioDAO.EDITAR_USUARIO_ROL(formatoLongSql(id_rol),null,formatoIntegerSql(_uuid));
            }


            if (id_rol_menu>0){

                return Either.right(id_rol_menu);


            }else{
                msn.add("No se reconoce formato del rol , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));

            }


        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, Long> crearUsuarioRoles(Long id_usuario, Integer _uuid, List<Long> id_rolLista) {
        List<String> msn=new ArrayList<>();
        List<Long> id_rolListaFinal=new ArrayList<>();
        try {

            if (formatoIntegerSql(_uuid)==null|| id_rolLista.size()<=0){
                msn.add("Fatan datos  id rol y/o id menu");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            for (Long id_rol:id_rolLista ){
                if (verificarUsuarioRoles(id_usuario,_uuid,id_rol)){
                    msn.add("El menu "+id_rol+" ya existe");

                }else{
                    id_rolListaFinal.add(id_rol);
                }
            }
            if (id_rolListaFinal.size()==0){
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear Rol    ***********************************");

            long id_rol_menu=0;
            for (Long id_rol:id_rolListaFinal ){
                id_rol_menu = usuarioDAO.CREAR_USUARIO_ROL(formatoLongSql(id_usuario),formatoIntegerSql(_uuid),formatoLongSql(id_rol) );
            }


            if (id_rol_menu>0){

                return Either.right(usuarioDAO.getPkLastUSUARIOS_ROLES());


            }else{
                msn.add("No se reconoce formato del rol , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));

            }


        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    private boolean verificarUsuarioRoles(Long id_usuario, Integer _uuid, Long id_rol) {
        return (usuarioDAO.VERIFICAR_USUARIO_ROL(formatoLongSql(id_usuario),_uuid,id_rol)==0)?false:true;
    }

    public Either<IException, List<Long>> obtenerIdRolesPorUsuario(Long id_usuario_rol,Long id_rol, Long _uuid) {

        List<Long> idMenus_Lista= new ArrayList<>();
        List<String> msn=new ArrayList<>();

        try {
            if (formatoLongSql(id_rol)==null && formatoLongSql(_uuid)==null){
                msn.add("Faltan datos  id rol y/o id usuario ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            idMenus_Lista=usuarioDAO.OBTENER_ID_ROLES_POR_USUARIO(formatoLongSql(id_usuario_rol),formatoLongSql(id_rol),formatoLongSql(_uuid));
            return Either.right(idMenus_Lista);

        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    public Either<IException, Integer> borrarRolesPorUsuario(Long _uuid, List<Long> id_menuRolLista) {
        List<Integer> idMenus_ListaFinal= new ArrayList<>();
        List<String> msn=new ArrayList<>();

        try {
            if (formatoLongSql(_uuid)==null){
                msn.add("Faltan datos  id rol  ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if (id_menuRolLista!=null&&id_menuRolLista.size()>0){
                for (Long id_rol:id_menuRolLista)
                    idMenus_ListaFinal.add(usuarioDAO.ELIMINAR_ROL_USUARIO(null,formatoLongSql(id_rol),formatoLongSql(_uuid)));

                return Either.right(id_menuRolLista.size());
            }else{
                Integer filas_afectadas = usuarioDAO.ELIMINAR_ROL_USUARIO(null, formatoLongSql(null), formatoLongSql(_uuid));
                return Either.right(filas_afectadas);
            }


        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, TokenDTO> iniarSesion(Long id_aplicacion, String dispositivo, AuthDTO authDTO) {
        try {
            TokenDTO tokenDTO= new TokenDTO(null,null,null,null,null);
            String usuario=authDTO.getUsuario();
            String clave=authDTO.getClave();
            if ((formatoLIKESql(usuario)!=null) &&(formatoLIKESql(clave)!=null)){
                List<Usuario> usuarioApps = usuarioDAO.VEFICAR_CREDENCIALES(id_aplicacion,usuario, clave);
                if (!usuarioApps.isEmpty()&& usuarioApps.size()==1){



                    Usuario usuarioModel = usuarioApps.get(0);


                    // Crea Parte del token
                    tokenDTO.setId_aplicacion(usuarioModel.getId_aplicacion());
                    Long toke= new Long(Long.valueOf(usuarioModel.get_uuid()));
                    tokenDTO.setId_usuario(toke);
                    tokenDTO.setUsuario(usuarioModel.getUsuario());

                    List<Rol> rolList=new ArrayList<>();
                    List<Long> id_roles=new ArrayList<>();
                    Either<IException, List<Long>> idRolesPorUsuarioEither = obtenerIdRolesPorUsuario(null, null, usuarioModel.get_uuid());
                    if(idRolesPorUsuarioEither.isRight() &&idRolesPorUsuarioEither.right().value().size()>0){
                         id_roles = idRolesPorUsuarioEither.right().value();
                        for (Long id_rol:id_roles) {
                            Either<IException, List<Rol>> obtenerRolesEither = rolBusiness.obtenerRoles(usuarioModel.getId_aplicacion(), id_rol, null, null);
                            if (obtenerRolesEither.isRight()&& obtenerRolesEither.right().value().size()>0){
                                rolList.addAll(obtenerRolesEither.right().value());
                            }
                        }
                    }
                    List<Menus> menusList=new ArrayList<>();
                    if (rolList.size()>0){
                        tokenDTO.setRols(rolList);
                        for (Long id_rol:id_roles){
                            Either<IException, List<Long>> menusPorRolEither = rolBusiness.obtenerIdMenusPorRol(id_rol, null);

                            if (menusPorRolEither.isRight() && menusPorRolEither.right().value().size()>0){
                                List<Long> id_menus = menusPorRolEither.right().value();
                                for(Long id_menu:id_menus){
                                    Either<IException, List<Menus>> obtenerMenusEither = menuBusiness.obtenerMenus(id_menu, null, null, null, null, null, null, null, null,formatoStringSqlMAYUS(dispositivo));

                                    if (obtenerMenusEither.isRight()&& obtenerMenusEither.right().value().size()>0){
                                        List<Menus> menus = obtenerMenusEither.right().value();
                                        for (int i=0; i<menus.size();i++){
                                            for (int j=0; j<menus.size();j++){
                                                if(menus.get(i).getId_menu() !=menus.get(j).getId_menu() && i!=j){
                                                    menusList.add(menus.get(i));
                                                }
                                            }
                                        }
                                        // menusList.addAll(obtenerMenusEither.right().value());

                                    }
                                }

                            }
                        }
                    }

                    if (menusList.size()>0){
                        tokenDTO.setMenus(menusList);
                    }





                    return Either.right(tokenDTO);
                }else{
                    return Either.left(new BussinessException(INVALID_USER_PASS));
                }
            }else {
                return Either.left(new BussinessException("No se puede identificar el usuario con los parametros."));
            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }

    public Either<IException, Long> editarDirectorio(Long id_directorio, Directorio directorio) {
        return personaBusiness.editarDirectorio(id_directorio,directorio);
    }

    public Either<IException, Long> editarPersona(Integer uuid_usuario, PersonaDTO personaDTO) {
        return personaBusiness.editarPersona(uuid_usuario,personaDTO);
    }

    public Either<IException, Long> eliminarDirectorio(Long id_directorio, Long id_mail, Long id_telefono) {
        return personaBusiness.eliminarDirectorio( id_directorio, id_mail,id_telefono);
    }



    public Either<IException, Long> eliminarPersona(Long id_persona, Long uuid_usuario) {
        return personaBusiness.eliminarPersona(id_persona,uuid_usuario);
    }

    public Either<IException, List<PersonaUsuario>> ListaPersona(List<Long> uuid, Long id_aplicacion, String usuario, String clave, Long id_persona,
                                                                 String documento, String tipo_documento,
                                                                 String primer_nombre, String segundo_nombre,
                                                                 String primer_apellido, String segundo_apellido) {
        Either<IException, List<PersonaUsuario>> ListaPersona=personaBusiness.ListaPersona(uuid,id_aplicacion,usuario,clave,id_persona,
                documento,tipo_documento,primer_nombre,segundo_nombre,primer_apellido,segundo_apellido);

        if (ListaPersona.isRight()){
            List<PersonaUsuario> usuarioList= ListaPersona.right().value();
            for (int i=0; i<usuarioList.size();i++){
                usuarioList.get(i).getUsuario().get_uuid();
                Either<IException, List<Rol>> iExceptionListEither = rolBusiness.obtenerRolesByUUID(null, null, null, null, null, null,
                        Long.valueOf(usuarioList.get(i).getUsuario().get_uuid()));
                if(iExceptionListEither.isRight()){
                    List<Rol> rols=iExceptionListEither.right().value();
                    usuarioList.get(i).getUsuario().setRoles(rols);
                }

            }
            return Either.right(usuarioList);
        }else{
            return Either.left(ListaPersona.left().value());
        }
    }

    public Either<IException, Long> eliminarUsuario( Long uuid_usuario) {
        List<Integer> idMenus_ListaFinal= new ArrayList<>();
        List<String> msn=new ArrayList<>();

        try {
            if (formatoLongSql(uuid_usuario)==null){
                msn.add("Faltan datos   ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{

                Either<IException, Long> eliminarPersonaEither = personaBusiness.eliminarPersona(null, uuid_usuario);

                if (eliminarPersonaEither.isRight()){
                    // todo Eliminar los roles del usuario antes de ser eliminando
                    Either<IException, Integer> borrarRolesPorUsuarioEither = borrarRolesPorUsuario(uuid_usuario, null);
                    if (borrarRolesPorUsuarioEither.isRight()){
                        Long filas=new Long(0);
                        int fila = usuarioDAO.ELIMINAR_USUARIO(null, uuid_usuario);
                        filas= Long.valueOf(fila);
                        if (filas>0){
                            return Either.right(filas);
                        }else{
                            msn.add("No se puede elimanar el usuario");
                            return Either.left(new BussinessException(messages_errors(msn)));
                        }

                    }else{
                        msn.add("No se puede elimanar el usuario");
                        return Either.left(new BussinessException(messages_errors(msn)));
                    }

                }else{
                    msn.add("No se puede eliminar el usuario  ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
            }
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
