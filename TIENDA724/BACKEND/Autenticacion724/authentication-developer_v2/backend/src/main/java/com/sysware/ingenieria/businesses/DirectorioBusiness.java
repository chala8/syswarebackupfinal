package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.DirectorioDAO;
import com.sysware.ingenieria.entities.Directorio;
import com.sysware.ingenieria.entities.Mail;
import com.sysware.ingenieria.entities.Telefono;
import com.sysware.ingenieria.representations.DirectorioDTO;
import com.sysware.ingenieria.representations.MailDTO;
import com.sysware.ingenieria.representations.TelefonoDTO;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.sysware.ingenieria.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.sysware.ingenieria.utils.constans.K.messages_error;
import static com.sysware.ingenieria.utils.constans.K.messages_errors;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

/**
 * Created by luis on 30/06/17.
 */
public class DirectorioBusiness {

    private DirectorioDAO directorioDAO;
    private TelefonoBusiness telefonoBusiness;
    private MailBusiness mailBusiness;

    public DirectorioBusiness(DirectorioDAO directorioDAO, TelefonoBusiness telefonoBusiness, MailBusiness mailBusiness) {
        this.directorioDAO = directorioDAO;
        this.telefonoBusiness = telefonoBusiness;
        this.mailBusiness = mailBusiness;
    }

    public Either<IException, Long> crearDirectorio(Long id_persona, DirectorioDTO directorioDTO) {
        List<TelefonoDTO> telefonoDTOList=directorioDTO.getTelefonoDTOS();
        List<MailDTO> mailDTOList=directorioDTO.getMailDTOList();
        List<String> msn=new ArrayList<>();
        List<Long> id_rolesParaCrear=new ArrayList<>();

        try {
            if (id_persona==null||id_persona<=0){
                msn.add("No se reconoce formato del directorio , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                Long id_directorio = directorioDAO.CREAR_DIRECTORIO(id_persona);

                if (id_directorio>0){

                    // Iniciar proceso de creacion de directorio
                    // Crear mails
                    Either<IException, Long> crearTelefonoEither = telefonoBusiness.crearTelefono(id_directorio, telefonoDTOList);
                    // Crear Telefonos
                    Either<IException, Long> crearMailEither = mailBusiness.crearMail(id_directorio, mailDTOList);

                    return Either.right(id_directorio);
                }else {
                    msn.add("Fallo el proceso de creacion de directorio");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }



            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> editarDirectorio(Long id_directorio, Directorio directorioDTO) {

        List<Telefono> telefonoList=directorioDTO.getTelefonos();
        List<Mail> mailList=directorioDTO.getMails();
        List<String> msn=new ArrayList<>();
        Long filas_afectadas=null;

        try {
            if (id_directorio==null||id_directorio<=0){
                msn.add("No se reconoce formato del directorio , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                // Iniciar proceso de creacion de directorio
                // Crear mails
                Either<IException, Long> crearTelefonoEither = telefonoBusiness.editarTelefono(id_directorio, telefonoList);
                // Crear Telefonos
                Either<IException, Long> crearMailEither = mailBusiness.editarMail(id_directorio, mailList);

                return Either.right(id_directorio);




            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> eliminarDirectorio(Long id_directorio,Long id_mail,Long id_telefono) {
        List<String> msn=new ArrayList<>();
        Long filas_afectadas=null;
        try {
            if (formatoLongSql(id_directorio)==null &&formatoLongSql(id_telefono)==null && formatoLongSql(id_mail)==null){
                msn.add("No se reconoce formato del directorio , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                telefonoBusiness.borrarTelefonoo(formatoLongSql(id_directorio),formatoLongSql(id_telefono));
                mailBusiness.borrarMail(formatoLongSql(id_directorio), formatoLongSql(id_mail));
                filas_afectadas =Long.valueOf(  directorioDAO.ELIMINAR_DIRECTORIO(formatoLongSql(id_directorio), null));
                if (filas_afectadas>0){
                    return Either.right(filas_afectadas);
                }else{
                    msn.add("No se reconoce formato del directorio , esta mal formado");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> obtenerDirectorio(Long id_directorio, Long id_persona) {
        List<String> msn=new ArrayList<>();
        Long id_direct=null;
        try {
            if (formatoLongSql(id_directorio)==null &&formatoLongSql(id_persona)==null ){
                msn.add("No se reconoce formato del directorio , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{

                id_direct = directorioDAO.OBTENER_DIRECTORIO(formatoLongSql(id_directorio), formatoLongSql(id_persona));
                if (id_direct>0){
                    return Either.right(id_direct);
                }else{
                    msn.add("No se reconoce formato del directorio , esta mal formado");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Directorio> obtenerDirectorioCompleto(Long id_directorio, Long id_persona) {

        List<String> msn=new ArrayList<>();
        Long id_direct=null;
        try {
            if (formatoLongSql(id_directorio)==null &&formatoLongSql(id_persona)==null ){
                msn.add("No se reconoce formato del directorio , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                id_direct = directorioDAO.OBTENER_DIRECTORIO(formatoLongSql(id_directorio), formatoLongSql(id_persona));
                if (id_direct!=null &&id_direct>0){
                    List<Telefono> telefonoList= new ArrayList<>();
                    List<Mail> mailList= new ArrayList<>();
                    Either<IException, List<Telefono>> obtenerTelefonosEither = telefonoBusiness.obtenerTelefonos(formatoLongSql(id_direct),
                                                                                null, null, null, null);
                    Either<IException, List<Mail>> obtenerMailsEither = mailBusiness.obtenerMails(formatoLongSql(id_direct),
                            null, null, null, null);

                    if (obtenerTelefonosEither.isRight()){
                        telefonoList = obtenerTelefonosEither.right().value();

                    }
                    if (obtenerMailsEither.isRight()){
                         mailList = obtenerMailsEither.right().value();

                    }

                    return Either.right(new Directorio(id_direct,mailList,telefonoList));
                }else{
                    msn.add("No se reconoce formato del directorio , esta mal formado");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }
}
