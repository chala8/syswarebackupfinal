package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by luis on 4/05/17.
 */
public class MenuDTO {
    private Long id_aplicacion;
    private String nombre;
    private Integer tipo;
    private String ruta;
    private String icono;
    private String avatar;
    private String background;
    private Integer estado;
    private String dispositivo;
    private Long id_menu_padre;

    @JsonCreator
    public MenuDTO(@JsonProperty("id_aplicacion") Long id_aplicacion,@JsonProperty("nombre") String nombre,
                   @JsonProperty("tipo") Integer tipo,@JsonProperty("ruta") String ruta,
                   @JsonProperty("icono") String icono,@JsonProperty("avatar") String avatar,
                   @JsonProperty("background") String background,@JsonProperty("estado") Integer estado,
                   @JsonProperty("id_menu_padre") Long id_menu_padre) {
        this.id_aplicacion = id_aplicacion;
        this.nombre = nombre;
        this.tipo = tipo;
        this.ruta = ruta;
        this.icono = icono;
        this.avatar = avatar;
        this.background = background;
        this.estado = estado;
        this.id_menu_padre=id_menu_padre;
    }

    public Long getId_menu_padre() {
        return id_menu_padre;
    }

    public void setId_menu_padre(Long id_menu_padre) {
        this.id_menu_padre = id_menu_padre;
    }

    public String getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(String dispositivo) {
        this.dispositivo = dispositivo;
    }

    public Long getId_aplicacion() {
        return id_aplicacion;
    }

    public void setId_aplicacion(Long id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }
}
