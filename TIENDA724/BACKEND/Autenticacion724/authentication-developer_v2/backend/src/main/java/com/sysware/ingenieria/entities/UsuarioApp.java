package com.sysware.ingenieria.entities;

/*
 ************************************
 *                                  *
 * Created by luis on 3/05/17.      *
 *                                  *
 ************************************
*/

public class UsuarioApp {
    private Long id_usuario_app;
    private String usuario;
    private String clave;

    public UsuarioApp(Long id_usuario_app, String usuario, String clave) {
        this.id_usuario_app = id_usuario_app;
        this.usuario = usuario;
        this.clave = clave;
    }

    public Long getId_usuario_app() {
        return id_usuario_app;
    }

    public void setId_usuario_app(Long id_usuario_app) {
        this.id_usuario_app = id_usuario_app;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}
