package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.TelefonoDAO;
import com.sysware.ingenieria.entities.Telefono;
import com.sysware.ingenieria.representations.TelefonoDTO;
import com.sysware.ingenieria.translators.TelefonoMailBulk;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.sysware.ingenieria.translators.TelefonoMailBulk.traductorTelefono;
import static com.sysware.ingenieria.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.sysware.ingenieria.utils.constans.K.messages_error;
import static com.sysware.ingenieria.utils.constans.K.messages_errors;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.*;

/**
 * Created by luis on 30/06/17.
 */
public class TelefonoBusiness {
    private TelefonoDAO telefonoDAO;

    public TelefonoBusiness(TelefonoDAO telefonoDAO) {
        this.telefonoDAO = telefonoDAO;
    }

    public Either<IException, Long> crearTelefono(Long id_directorio, List<TelefonoDTO> telefonoDTOList) {
        List<String> msn=new ArrayList<>();
        List<Long> id_rolesParaCrear=new ArrayList<>();

        try {
            if (id_directorio==null||id_directorio<=0 || telefonoDTOList==null || telefonoDTOList.isEmpty()||telefonoDTOList==null || telefonoDTOList.size()<=0 ){
                msn.add("No se reconoce formato del Telefono , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                TelefonoMailBulk telefonoMailBulk = traductorTelefono(telefonoDTOList);
                telefonoDAO.CREAR_MAILS(id_directorio,telefonoMailBulk.getMailTelList(),telefonoMailBulk.getTipoList(),telefonoMailBulk.getPreferenciaList());
                return Either.right(id_directorio);
            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> editarTelefono(Long id_directorio, List<Telefono> telefonoList) {
        List<String> msn=new ArrayList<>();

        try {
            if (id_directorio==null||id_directorio<=0 || telefonoList==null || telefonoList.isEmpty()|| telefonoList.size()<=0 ){
                msn.add("No se reconoce formato del Telefono , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                Long filas_afectadas=new Long(0);
                for (Telefono telefono: telefonoList) {
                    filas_afectadas = telefonoDAO.EDITAR_TELEFONO(id_directorio, telefono);

                }
                return Either.right(id_directorio);
            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Integer> borrarTelefonoo( Long id_directorio,Long id_telefono) {
        List<String> msn=new ArrayList<>();

        try {
            if (formatoLongSql(id_directorio)==null&&formatoLongSql(id_telefono)==null){
                msn.add("Faltan datos  para eliminar el telefono  ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                Integer filas = telefonoDAO.ELIMINAR_TELEFONO(formatoLongSql(id_directorio), formatoLongSql(id_telefono));
                if (filas>0){
                    return Either.right(filas);
                }else {
                    msn.add("No se puede eliminar el Telefono ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<Telefono>> obtenerTelefonos( Long id_directorio,Long id_telefono,String numero,
                                                               String tipo, Integer preferencia) {
        List<Telefono> telefonoList= new ArrayList<>();
        List<String> msn=new ArrayList<>();

        try {
            if (formatoLongSql(id_directorio)==null&&formatoLongSql(id_telefono)==null){
                msn.add("Faltan datos  para eliminar el telefono  ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                telefonoList = telefonoDAO.OBTENER_TELEFONOS(formatoLongSql(id_telefono), formatoLongSql(id_directorio),
                                                            formatoLIKESql(numero),formatoLIKESql(tipo), formatoIntegerSql(preferencia));
                return Either.right(telefonoList);

            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

}
