package com.sysware.ingenieria;

import com.sysware.ingenieria.DAOs.*;
import com.sysware.ingenieria.entities.*;
import com.sysware.ingenieria.threads.FCMThread;
import com.sysware.ingenieria.businesses.*;
import com.sysware.ingenieria.resources.*;
import io.dropwizard.Application;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.java8.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.apache.commons.dbcp2.BasicDataSource;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;
import java.util.Timer;

import static com.sysware.ingenieria.utils.constans.K.URI_BASE;


public class AuthApp extends Application<AuthAppConfiguration> {



    public static void main(String[] args) throws Exception  {
        new AuthApp().run(args);

    }





    @Override
    public void initialize(Bootstrap<AuthAppConfiguration> bootstrap) {

        bootstrap.addBundle(new Java8Bundle());
        bootstrap.addBundle(new MultiPartBundle());


    }
    @Override
    public void run(AuthAppConfiguration configuration, Environment environment) throws Exception {
        environment.jersey().setUrlPattern(URI_BASE+"/*");

        // Enable CORS headers
        final FilterRegistration.Dynamic corsFilter =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        corsFilter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        corsFilter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
        corsFilter.setInitParameter("allowCredentials", "true");

        // Add URL mapping
        corsFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");




        // Se estable el ambiente de coneccion con JDBI con la base de datos de MYSQL
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mysql");

        AuthAppDatabaseConfiguration routesDatabaseConfiguration = configuration.getDatabaseConfiguration();
        BasicDataSource datasource = createDataSource(routesDatabaseConfiguration);

        environment.jersey().register(new TestResource());



        // apply security on http request
        // addAuthorizationFilterAndProvider(environment,tokenBusiness);


        final  DatoDAO datoDAO= jdbi.onDemand(DatoDAO.class);
        final DatoBusiness datoBusiness= new DatoBusiness(datoDAO);



        final FCM_DAO fcmDao =jdbi.onDemand(FCM_DAO.class);
        final FCMBusiness fcmBusiness= new FCMBusiness(fcmDao);



        final FCMThread fcmThread= new FCMThread(fcmBusiness);

        final UsuarioAppDAO usuarioAppDAO= jdbi.onDemand(UsuarioAppDAO.class);
        final UsuarioAppBusiness usuarioAppBusiness= new UsuarioAppBusiness(usuarioAppDAO);

        final AplicacionDAO aplicacionDAO =jdbi.onDemand(AplicacionDAO.class);
        final AplicacionBusiness aplicacionBusiness= new AplicacionBusiness(aplicacionDAO);

        final RolDAO rolDAO =jdbi.onDemand(RolDAO.class);
        final RolBusiness rolBusiness= new RolBusiness(rolDAO);

        final MenuDAO menuDAO= jdbi.onDemand(MenuDAO.class);
        final MenuBusiness menuBusiness= new MenuBusiness(menuDAO);

        final PermisoAPIDAO permisoAPIDAO= jdbi.onDemand(PermisoAPIDAO.class);
        final PermisoAPIBusiness permisoAPIBusiness= new PermisoAPIBusiness(permisoAPIDAO);

        final TelefonoDAO telefonoDAO= jdbi.onDemand(TelefonoDAO.class);
        final TelefonoBusiness telefonoBusiness = new TelefonoBusiness(telefonoDAO);

        final MailDAO mailDAO =jdbi.onDemand(MailDAO.class);
        final MailBusiness mailBusiness= new MailBusiness(mailDAO);
        final DirectorioDAO directorioDAO = jdbi.onDemand(DirectorioDAO.class);
        final DirectorioBusiness directorioBusiness= new DirectorioBusiness(directorioDAO,telefonoBusiness,mailBusiness);

        final PersonaDAO personaDAO = jdbi.onDemand(PersonaDAO.class);
        final PersonaBusiness personaBusiness= new PersonaBusiness(personaDAO,directorioBusiness);


        final UsuarioDAO usuarioDAO= jdbi.onDemand(UsuarioDAO.class);
        final UsuarioBusiness usuarioBusiness= new UsuarioBusiness(usuarioDAO,rolBusiness,menuBusiness,personaBusiness);




        environment.jersey().register(new TestResource());           //servicio para test
        environment.jersey().register(new AuthResource(usuarioAppBusiness));           //servicio para test
        environment.jersey().register(new AplicacionResource(aplicacionBusiness));
        environment.jersey().register(new RolResource(rolBusiness));
        environment.jersey().register(new MenuResource(menuBusiness));
        environment.jersey().register(new PermisoAPIResource(permisoAPIBusiness));
        environment.jersey().register(new UsuarioResource(usuarioBusiness));




        Timer timer = new Timer();

        //timer.scheduleAtFixedRate(fcmThread, 0, 30*1000);   //cada 5 minutos




    }
    /**
     * @Método:  Hace  la configuración para establecer conexión con las bases de datos que se
     * va a utilizar como persistencia del servidor
     * */
    private BasicDataSource createDataSource(AuthAppDatabaseConfiguration authAppDatabaseConfiguration){

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(authAppDatabaseConfiguration.getDriverClassName());
        basicDataSource.setUrl(authAppDatabaseConfiguration.getUrl());
        basicDataSource.setUsername(authAppDatabaseConfiguration.getUsername());
        basicDataSource.setPassword(authAppDatabaseConfiguration.getPassword());
        basicDataSource.addConnectionProperty("characterEncoding", "UTF-8");

        basicDataSource.setRemoveAbandonedOnMaintenance(true);
        basicDataSource.setRemoveAbandonedOnBorrow(true);
        basicDataSource.setRemoveAbandonedTimeout(30000);

        return basicDataSource;

    }




}

