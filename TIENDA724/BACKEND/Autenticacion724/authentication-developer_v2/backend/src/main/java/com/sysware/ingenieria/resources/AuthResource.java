package com.sysware.ingenieria.resources;

import com.sysware.ingenieria.businesses.UsuarioAppBusiness;
import com.sysware.ingenieria.entities.UsuarioApp;
import com.sysware.ingenieria.representations.AuthDTO;
import com.sysware.ingenieria.representations.UsuarioAppDTO;
import com.sysware.ingenieria.utils.exeptions.ExceptionResponse;
import com.sysware.ingenieria.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AuthResource {

    private UsuarioAppBusiness  usuarioAppBusiness;

    public AuthResource(UsuarioAppBusiness usuarioAppBusiness) {
        this.usuarioAppBusiness = usuarioAppBusiness;
    }

    @GET
    public Response obtenerListaUsuario(@QueryParam("id_usuario_app") Long id_usuario_app,
                                             @QueryParam("usuario") String usuario,
                                             @QueryParam("clave") String clave){
        Response response;

        Either<IException, List<UsuarioApp>> allViewOffertsEither = usuarioAppBusiness.obtenerUsuarioApp(id_usuario_app, usuario,clave);


        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @POST
    public Response crearUsuario(UsuarioAppDTO usuarioAppDTO) {
        Either<IException, Long> offerEither = usuarioAppBusiness.crearUsuarioApp( usuarioAppDTO);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }


    @POST
    @Path("login")
    public Response iniciarSession(AuthDTO authDTO){
        Response response;

        Either<IException, UsuarioApp> iniarSesionEither = usuarioAppBusiness.iniarSesion(authDTO);


        if (iniarSesionEither.isRight()){

            response=Response.status(Response.Status.OK).entity(iniarSesionEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(iniarSesionEither);
        }
        return response;
    }
}
