package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by luis on 4/05/17.
 */
public class AuthDTO {
    private String usuario;
    private String clave;

    @JsonCreator
    public AuthDTO(@JsonProperty("usuario") String usuario,@JsonProperty("clave") String clave) {
        this.usuario = usuario;
        this.clave = clave;
    }


    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
}
