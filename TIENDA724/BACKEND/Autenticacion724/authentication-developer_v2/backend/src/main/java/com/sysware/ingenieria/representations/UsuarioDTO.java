package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by luis on 3/05/17.
 */
public class UsuarioDTO {
    private Long id_aplicacion;
    private  String usuario;
    private String clave;
    private List<Long> id_roles;

    @JsonCreator
    public UsuarioDTO(@JsonProperty("id_aplicacion") Long id_aplicacion, @JsonProperty("usuario")String usuario,
                      @JsonProperty("clave")String clave, @JsonProperty("id_roles")List<Long> id_roles) {
        this.id_aplicacion = id_aplicacion;
        this.usuario = usuario;
        this.clave = clave;
        this.id_roles = id_roles;
    }

    public Long getId_aplicacion() {
        return id_aplicacion;
    }

    public void setId_aplicacion(Long id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public List<Long> getId_roles() {
        return id_roles;
    }

    public void setId_roles(List<Long> id_roles) {
        this.id_roles = id_roles;
    }
}
