package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.representations.DatoDTO;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

/**
 * Created by luis on 19/04/17.
 */
public interface DatoDAO {

    @SqlUpdate("INSERT INTO datos ( nombre,descripcion) VALUES (:dato.nombre,:dato.descripcion) ")
    long CREAR_DATO(@BindBean("dato") DatoDTO dato);
}
