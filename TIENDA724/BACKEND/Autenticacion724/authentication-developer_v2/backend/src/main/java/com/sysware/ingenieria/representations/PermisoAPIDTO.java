package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by luis on 4/05/17.
 */
public class PermisoAPIDTO {


    private Long   id_aplicacion;
    private Long   id_api;
    private String http_verb;
    private String http_uri;

    @JsonCreator
    public PermisoAPIDTO(@JsonProperty("id_aplicacion") Long id_aplicacion,@JsonProperty("id_api") Long id_api,
                         @JsonProperty("http_verb") String http_verb,@JsonProperty("http_uri") String http_uri) {
        this.id_aplicacion = id_aplicacion;
        this.id_api = id_api;
        this.http_verb = http_verb;
        this.http_uri = http_uri;
    }

    public Long getId_aplicacion() {
        return id_aplicacion;
    }

    public void setId_aplicacion(Long id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }

    public Long getId_api() {
        return id_api;
    }

    public void setId_api(Long id_api) {
        this.id_api = id_api;
    }

    public String getHttp_verb() {
        return http_verb;
    }

    public void setHttp_verb(String http_verb) {
        this.http_verb = http_verb;
    }

    public String getHttp_uri() {
        return http_uri;
    }

    public void setHttp_uri(String http_uri) {
        this.http_uri = http_uri;
    }
}
