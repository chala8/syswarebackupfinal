package com.sysware.ingenieria.DAOs;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

/**
 * Created by luis on 30/06/17.
 */
public interface DirectorioDAO {

    @SqlQuery("SELECT ID_DIRECTORIO FROM DIRECTORIO WHERE ID_DIRECTORIO IN (SELECT MAX( ID_DIRECTORIO ) FROM DIRECTORIO )\n ")
    Long getPkLast();

    @SqlUpdate("INSERT INTO directorio  ( id_persona) " +
            "        VALUES (:id_persona) ")
    Long CREAR_DIRECTORIO(@Bind("id_persona") Long id_persona);



    @SqlUpdate("DELETE FROM directorio " +
            "    WHERE (directorio.id_directorio=:id_directorio or :id_directorio IS NULL ) AND " +
            "      (directorio.id_persona = :id_persona or :id_persona IS NULL) ")
    int ELIMINAR_DIRECTORIO(@Bind("id_directorio")Long id_directorio, @Bind("id_persona")Long id_persona);


    @SqlQuery(" SELECT id_directorio FROM directorio  dir " +
            "    WHERE (dir.id_directorio=:id_directorio OR :id_directorio IS NULL ) AND " +
            "          (dir.id_persona=:id_persona OR :id_persona IS NULL )")
    Long OBTENER_DIRECTORIO(@Bind("id_directorio")Long id_directorio, @Bind("id_persona")Long id_persona);
}
