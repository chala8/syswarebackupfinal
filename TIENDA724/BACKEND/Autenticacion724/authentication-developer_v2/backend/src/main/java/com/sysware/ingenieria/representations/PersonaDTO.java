package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by luis on 30/06/17.
 */
public class PersonaDTO {
    private Integer uid_usuario;
    private String documento;
    private String  tipo_documento;
    private String  primer_nombre;
    private String segundo_nombre;
    private String primer_apellido;
    private String segundo_apellido;

    @JsonCreator
    public PersonaDTO(@JsonProperty("uid_usuario") Integer uid_usuario,
                      @JsonProperty("documento")  String documento, @JsonProperty("tipo_documento") String tipo_documento,
                      @JsonProperty("primer_nombre") String primer_nombre,@JsonProperty("segundo_nombre")  String segundo_nombre,
                      @JsonProperty("primer_apellido") String primer_apellido, @JsonProperty("segundo_apellido") String segundo_apellido) {
        this.uid_usuario = uid_usuario;
        this.documento = documento;
        this.tipo_documento = tipo_documento;
        this.primer_nombre = primer_nombre;
        this.segundo_nombre = segundo_nombre;
        this.primer_apellido = primer_apellido;
        this.segundo_apellido = segundo_apellido;
    }

    public Integer getUid_usuario() {
        return uid_usuario;
    }

    public void setUid_usuario(Integer uid_usuario) {
        this.uid_usuario = uid_usuario;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getPrimer_nombre() {
        return primer_nombre;
    }

    public void setPrimer_nombre(String primer_nombre) {
        this.primer_nombre = primer_nombre;
    }

    public String getSegundo_nombre() {
        return segundo_nombre;
    }

    public void setSegundo_nombre(String segundo_nombre) {
        this.segundo_nombre = segundo_nombre;
    }

    public String getPrimer_apellido() {
        return primer_apellido;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    public String getSegundo_apellido() {
        return segundo_apellido;
    }

    public void setSegundo_apellido(String segundo_apellido) {
        this.segundo_apellido = segundo_apellido;
    }
}
