package com.sysware.ingenieria.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by luis on 3/05/17.
 */
public class Directorio {
    private Long id_directorio;
    private List<Mail> mails;
    private List<Telefono> telefonos;

    @JsonCreator
    public Directorio(@JsonProperty("id_directorio") Long id_directorio, @JsonProperty("mails") List<Mail> mails, @JsonProperty("telefonos") List<Telefono> telefonos) {
        this.id_directorio = id_directorio;
        this.mails = mails;
        this.telefonos = telefonos;
    }

    public Long getId_directorio() {
        return id_directorio;
    }

    public void setId_directorio(Long id_directorio) {
        this.id_directorio = id_directorio;
    }

    public List<Mail> getMails() {
        return mails;
    }

    public void setMails(List<Mail> mails) {
        this.mails = mails;
    }

    public List<Telefono> getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(List<Telefono> telefonos) {
        this.telefonos = telefonos;
    }
}
