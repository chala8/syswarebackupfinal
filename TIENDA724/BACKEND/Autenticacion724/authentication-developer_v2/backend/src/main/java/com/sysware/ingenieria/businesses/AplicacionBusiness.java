package com.sysware.ingenieria.businesses;


import com.sysware.ingenieria.DAOs.AplicacionDAO;
import com.sysware.ingenieria.entities.Aplicacion;
import com.sysware.ingenieria.representations.AplicacionDTO;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.ManagementException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import fj.data.Either;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.sysware.ingenieria.utils.constans.K.*;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.*;
import static com.sysware.ingenieria.utils.security.TokenGenerator.tokenGenerate;


public class AplicacionBusiness {

    private AplicacionDAO aplicacionDAO;

    public AplicacionBusiness(AplicacionDAO aplicacionDAO) {
        this.aplicacionDAO = aplicacionDAO;
    }

    public Either<IException,List<Aplicacion>>  obtenerAplicaciones(Long id_applicacion, Long id_usuario_app, String nombre, String descripcion, String version, String fecha_creacion, String fecha_actualizacion, String key_aplicacion, String key_servidor){
        try {
            List<Aplicacion> aplicacionList = aplicacionDAO.OBTENER_APLICACIONES(
                    formatoLongSql(id_applicacion),formatoLongSql(id_usuario_app),formatoLIKESql( nombre), formatoLIKESql(descripcion),
                    formatoLIKESql(version), formatoLIKESql(fecha_creacion), formatoLIKESql(fecha_actualizacion), formatoStringSql(key_aplicacion), formatoStringSql(key_servidor));
            return Either.right(aplicacionList);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }




    public Either<IException, Long> crearAplicacion(Long id_ususario_app, AplicacionDTO aplicacionDTO) {
        List<String> msn=new ArrayList<>();
        try {

            if (formatoLongSql(id_ususario_app)==null){
                msn.add("El usaurio creador de aplicacion no se reconoce");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if (verificarApplicacionUsuario(id_ususario_app,aplicacionDTO.getNombre())){
                msn.add("La aplicacion ["+aplicacionDTO.getNombre()+"] ya existe");
                 return Either.left(new BussinessException(messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear Usuario    ***********************************");
            if (aplicacionDTO!=null){

                    long id_aplicacion = aplicacionDAO.CREAR_APLICACION(formatoLongSql(id_ususario_app),tokenGenerate(true),tokenGenerate(true), aplicacionDTO);

                    if (id_aplicacion>0){

                        return Either.right(aplicacionDAO.getPkLast());


                    }else{
                        msn.add("No se reconoce formato del usuario , esta mal formado");

                    }

                }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    private boolean verificarApplicacionUsuario(Long id_ususario_app, String nombre) {
        return (aplicacionDAO.VERIFICAR_APLICACION_USUARIO(id_ususario_app,nombre)==0)?false:true;
    }

    public Either<IException, Long> modificarAplicacion(Long id_aplicacion, Long id_usuario_app, AplicacionDTO aplicacionDTO) {
        try {
            List<String> msn=new ArrayList<>();
            if (formatoLongSql(id_usuario_app)==null){
                msn.add("El usaurio  de aplicacion no se reconoce");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if (formatoLongSql(id_aplicacion)==null){
                msn.add("La aplicacion a modificar  no se reconoce");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if (verificarApplicacionUsuario(id_usuario_app,aplicacionDTO.getNombre())){
                msn.add("La aplicacion ["+aplicacionDTO.getNombre()+"] ya existe");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            msn.add("OK");


            if (aplicacionDTO!=null){

                int filas_alteradas = aplicacionDAO.MODIFICAR_APLICACION(formatoLongSql(id_aplicacion), formatoLongSql(id_usuario_app), aplicacionDTO);

                if (filas_alteradas>0){

                    return Either.right(id_aplicacion);


                }else{
                    msn.add("No se reconoce formato del usuario , esta mal formado");

                }

            }
            return Either.left(new BussinessException(messages_errors(msn)));


        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }


    }
}





