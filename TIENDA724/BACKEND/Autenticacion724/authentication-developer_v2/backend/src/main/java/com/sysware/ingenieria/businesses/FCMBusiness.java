package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.FCM_DAO;
import com.sysware.ingenieria.entities.FCM;
import com.sysware.ingenieria.representations.FCM_DTO;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.sql.Timestamp;
import java.util.List;

import static com.sysware.ingenieria.utils.exeptions.ManagementException.catchException;

/**
 * Created by luis on 8/04/17.
 */
public class FCMBusiness {

    private FCM_DAO fcmDao ;

    public FCMBusiness(FCM_DAO fcmDao) {
        this.fcmDao = fcmDao;
    }

    public Either<IException, Long> createFCM(FCM_DTO fcmDto) {
        try {
            long id_fcm = fcmDao.CREATE_FCM(fcmDto);
            if (id_fcm>0) {
                    return Either.right(id_fcm);
            }else {
                    return Either.left(new TechnicalException("Could n't create  FCM!"));
                }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(catchException(e));
        }

    }

    public Either<IException, Long> updateFCM(FCM fcm) {
        try {
            long id_fcm = fcmDao.EDIT_FCM(fcm);
            if (id_fcm>0) {
                return Either.right(id_fcm);
            }else {
                return Either.left(new TechnicalException("Could n't edit  FCM!"));
            }
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(catchException(e));
        }
    }
    public Either<IException, Long> deleteFCM(Long id_fcm) {
        try {
            int row_affect = fcmDao.DELETE(id_fcm);
            if (id_fcm>0) {
                return Either.right(id_fcm);
            }else {
                return Either.left(new TechnicalException("Could n't delete  FCM!"));
            }
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(catchException(e));
        }
    }
    public Either<IException, List<FCM>> getFCM(Integer status, String type, Timestamp start, Timestamp end) {
        try {
            List<FCM> fcms = fcmDao.FCMS_LIST(status, type, start, end);

                return Either.right(fcms);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(catchException(e));
        }
    }





}
