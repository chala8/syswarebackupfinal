package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Telefono;
import com.sysware.ingenieria.entities.UsuarioApp;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 4/07/17.
 */
public class TelefonoMapper implements ResultSetMapper<Telefono> {
    @Override
    public Telefono map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Telefono(
                resultSet.getLong("id_telefono"),
                resultSet.getString("numero"),
                resultSet.getString("tipo"),
                resultSet.getInt("preferencia")
        );
    }
}
