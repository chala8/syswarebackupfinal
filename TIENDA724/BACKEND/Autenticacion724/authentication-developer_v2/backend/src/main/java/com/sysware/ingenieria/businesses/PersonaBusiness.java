package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.PersonaDAO;
import com.sysware.ingenieria.entities.Directorio;
import com.sysware.ingenieria.entities.PersonaUsuario;
import com.sysware.ingenieria.representations.*;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.sysware.ingenieria.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.sysware.ingenieria.utils.constans.K.messages_error;
import static com.sysware.ingenieria.utils.constans.K.messages_errors;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoLIKESql;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoLongSql;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoStringSql;

/**
 * Created by luis on 30/06/17.
 */
public class PersonaBusiness {

    private PersonaDAO personaDAO;
    private DirectorioBusiness directorioBusiness;

    public PersonaBusiness(PersonaDAO personaDAO, DirectorioBusiness directorioBusiness) {
        this.personaDAO = personaDAO;
        this.directorioBusiness = directorioBusiness;
    }


    public Either<IException, Long> crearPersona(PersonaFullDatosDTO personaDirectorioDTO ) {
        List<String> msn=new ArrayList<>();


        try {
            if (personaDirectorioDTO==null ){
                msn.add("No se reconoce formato del persona , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                Long id_persona = personaDAO.CREAR_USUARIO(personaDirectorioDTO.getPersonaDTO());

                if (id_persona>0){
                    id_persona=personaDAO.getPkLast();
                    // Iniciar proceso de creacion de directorio
                    Either<IException, Long> crearTelefonoEither = directorioBusiness.crearDirectorio(id_persona,personaDirectorioDTO.getDirectorioDTO());

                    return Either.right(id_persona);
                }else {
                    msn.add("Fallo el proceso de creacion de directorio");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> editarDirectorio(Long id_directorio, Directorio directorioDTO) {
        return directorioBusiness.editarDirectorio(id_directorio, directorioDTO);
    }

    public Either<IException, Long> editarPersona(Integer uuid_usuario, PersonaDTO personaDTO) {
        List<String> msn=new ArrayList<>();
        try {
            if (personaDTO==null || uuid_usuario==null){
                msn.add("No se reconoce formato del persona , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                long filas_afectadas = personaDAO.EDITAR_PERSONA(uuid_usuario,personaDTO);

                if (filas_afectadas>0){
                    return Either.right(filas_afectadas);
                }else {
                    msn.add("Fallo el proceso de creacion de directorio");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> eliminarDirectorio(Long id_directorio, Long id_mail, Long id_telefono) {
        return directorioBusiness.eliminarDirectorio(id_directorio,id_mail,id_telefono);
    }

    public Either<IException, Long> obtenerIdPersona( Long uuid_usuario){
        List<String> msn=new ArrayList<>();
        try {
            Long id_persona=new Long(0);
            if (formatoLongSql(uuid_usuario)==null){
                msn.add("No se reconoce formato de la Persona , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                long idpersona = personaDAO.OBTENER_IDPERSONA(formatoLongSql(uuid_usuario));
                if (idpersona>0){
                    return Either.right(idpersona);
                }else{
                    msn.add("No se encuantra coincidencias");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
            }
        }catch (Exception e){
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }

    }

    public Either<IException, Long> eliminarPersona(Long id_persona, Long uuid_usuario) {

        List<String> msn=new ArrayList<>();
        Long filas_afectadas=null;
        try {
            if ( formatoLongSql(uuid_usuario)==null ){
                msn.add("No se reconoce formato de la Persona , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                if (id_persona==null){
                    Either<IException, Long> idPersonaEither = obtenerIdPersona(uuid_usuario);
                    if (idPersonaEither.isRight()){

                        Either<IException, Long> directorioEither = directorioBusiness.obtenerDirectorio(null, idPersonaEither.right().value());
                        if (directorioEither.isRight()){
                            Long id_directorio = directorioEither.right().value();
                            Either<IException, Long> eliminarDirectorioEither = eliminarDirectorio(id_directorio, null, null);
                            if (eliminarDirectorioEither.isRight()){

                                filas_afectadas =Long.valueOf( personaDAO.ELIMINAR_PERSONA(formatoLongSql(id_persona), formatoLongSql(uuid_usuario)));
                                return Either.right(filas_afectadas);

                            }else{
                                msn.add("No se puede eliminar directorio");
                                return Either.left(new BussinessException(messages_errors(msn)));
                            }

                        }else{
                            msn.add("No se reconoce formato del directorio , esta mal formado");
                            return Either.left(new BussinessException(messages_errors(msn)));
                        }

                    }
                }else{
                    Either<IException, Long> directorioEither = directorioBusiness.obtenerDirectorio(null, id_persona);
                    if (directorioEither.isRight()){
                        Long id_directorio = directorioEither.right().value();
                        Either<IException, Long> eliminarDirectorioEither = eliminarDirectorio(id_directorio, null, null);
                        if (eliminarDirectorioEither.isRight()){

                            filas_afectadas =Long.valueOf( personaDAO.ELIMINAR_PERSONA(formatoLongSql(id_persona), formatoLongSql(uuid_usuario)));
                            return Either.right(filas_afectadas);

                        }else{
                            msn.add("No se puede eliminar directorio");
                            return Either.left(new BussinessException(messages_errors(msn)));
                        }

                    }else{
                        msn.add("No se reconoce formato del directorio , esta mal formado");
                        return Either.left(new BussinessException(messages_errors(msn)));
                    }

                }
                msn.add("No se reconoce formato de la Persona , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));

            }
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }



    }


    public Either<IException, List<PersonaUsuario>> ListaPersona(List<Long> uuid, Long id_aplicacion, String usuario, String clave, Long id_persona,
                                                                 String documento, String tipo_documento,
                                                                 String primer_nombre, String segundo_nombre,
                                                                 String primer_apellido, String segundo_apellido) {

        List<PersonaUsuario> personaUsuariosLista=new ArrayList<>();
        try {
            if (uuid==null || uuid.size()<=0 || uuid.isEmpty()){
                personaUsuariosLista=personaDAO.LISTA_PERSONA( formatoLongSql(id_aplicacion),
                        formatoLIKESql(usuario), formatoLIKESql(clave),  formatoLongSql(id_persona),
                        formatoLIKESql(documento),  formatoLIKESql(tipo_documento),
                        formatoLIKESql(primer_nombre),  formatoLIKESql(segundo_nombre),
                        formatoLIKESql(primer_apellido),  formatoLIKESql(segundo_apellido));
                return obtenerDirectorioPersonal(personaUsuariosLista);

            }else{
                personaUsuariosLista=personaDAO.LISTA_PERSONA_UUID( uuid, formatoLongSql(id_aplicacion),
                        formatoLIKESql(usuario), formatoLIKESql(clave),  formatoLongSql(id_persona),
                        formatoLIKESql(documento),  formatoLIKESql(tipo_documento),
                        formatoLIKESql(primer_nombre),  formatoLIKESql(segundo_nombre),
                        formatoLIKESql(primer_apellido),  formatoLIKESql(segundo_apellido));
                return obtenerDirectorioPersonal(personaUsuariosLista);
            }


        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }



    }

    private Either<IException, List<PersonaUsuario>> obtenerDirectorioPersonal(List<PersonaUsuario> personaUsuariosLista) {

        if (!personaUsuariosLista.isEmpty() && personaUsuariosLista.size()>0){
            for (int i = 0; i < personaUsuariosLista.size(); i++) {
                Either<IException, Directorio> directorioCompletoEither = directorioBusiness.obtenerDirectorioCompleto(null, personaUsuariosLista.get(i).getPersona().getId_persona());
                if (directorioCompletoEither.isRight()){
                    Directorio directorio = directorioCompletoEither.right().value();
                    if (directorio!=null){
                        personaUsuariosLista.get(i).setDirectorio(directorio);
                    }

                }

            }
        }
        return Either.right(personaUsuariosLista);
    }


}
