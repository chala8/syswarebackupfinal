package com.sysware.ingenieria.entities;


public class Aplicacion {

    private Long id_aplicacion;
    private String nombre;
    private String descripcion;
    private String version;
    private String fecha_creacion;
    private String fecha_actualizacion;
    private String key_aplicacion;
    private String key_servidor;


    public Aplicacion(Long id_aplicacion, String nombre, String descripcion, String version, String fecha_creacion, String fecha_actualizacion, String key_aplicacion, String key_servidor) {
        this.id_aplicacion = id_aplicacion;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.version = version;
        this.fecha_creacion = fecha_creacion;
        this.fecha_actualizacion = fecha_actualizacion;
        this.key_aplicacion = key_aplicacion;
        this.key_servidor = key_servidor;
    }

    public Long getId_aplicacion() {
        return id_aplicacion;
    }

    public void setId_aplicacion(Long id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getFecha_actualizacion() {
        return fecha_actualizacion;
    }

    public void setFecha_actualizacion(String fecha_actualizacion) {
        this.fecha_actualizacion = fecha_actualizacion;
    }

    public String getKey_aplicacion() {
        return key_aplicacion;
    }

    public void setKey_aplicacion(String key_aplicacion) {
        this.key_aplicacion = key_aplicacion;
    }

    public String getKey_servidor() {
        return key_servidor;
    }

    public void setKey_servidor(String key_servidor) {
        this.key_servidor = key_servidor;
    }
}
