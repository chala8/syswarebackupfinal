package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Menus;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 4/05/17.
 */
public class MenuMapper implements ResultSetMapper<Menus> {
    @Override
    public Menus map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Menus(
                resultSet.getLong("id_menu"),
                resultSet.getLong("id_aplicacion"),
                resultSet.getString("nombre"),
                resultSet.getInt("tipo"),
                resultSet.getString("ruta"),
                resultSet.getString("icono"),
                resultSet.getString("avatar"),
                resultSet.getString("background"),
                resultSet.getInt("estado"),
                resultSet.getString("dispositivo"),
                resultSet.getLong("ID_MENU_PADRE")
        );
    }
}
