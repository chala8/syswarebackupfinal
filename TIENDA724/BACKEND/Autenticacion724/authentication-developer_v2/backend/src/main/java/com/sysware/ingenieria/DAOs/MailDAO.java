package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Mail;
import com.sysware.ingenieria.mappers.MailMapper;
import com.sysware.ingenieria.mappers.TelefonoMapper;
import com.sysware.ingenieria.representations.MailDTO;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import java.util.List;


@UseStringTemplate3StatementLocator
@RegisterMapper(MailMapper.class)
public interface MailDAO {

    @SqlQuery("SELECT ID_MAIL FROM MAILS WHERE ID_MAIL IN (SELECT MAX( ID_MAIL ) FROM MAILS )\n ")
    Long getPkLast();

    @SqlBatch("INSERT INTO mails  ( mail, tipo, preferencia, id_directorio ) " +
            "        VALUES (:mail, :tipo, :preferencia,:id_directorio ) ")
    void CREAR_TELEFONO(@Bind("id_directorio") Long id_directorio, @Bind("mail") List<String> mailTelList,
                        @Bind("tipo") List<String> tipoList, @Bind("preferencia") List<Integer> preferenciaList);

    @SqlUpdate("UPDATE mails SET mail=:ma.mail, tipo=:ma.tipo, " +
            " preferencia=:ma.preferencia  WHERE  (id_directorio=:id_directorio AND id_mail=:ma.id_mail) ")
    Long EDITAR_MAIL(@Bind("id_directorio") Long id_directorio,@BindBean("ma") Mail mail);

    @SqlUpdate("DELETE  FROM  mails WHERE (id_directorio=:id_directorio or :id_directorio IS NULL ) AND (id_mail=:id_mail or :id_mail IS NULL )")
    Integer ELIMINAR_MAIL(@Bind("id_directorio") Long id_directorio, @Bind("id_mail")Long id_telefono);

    @SqlQuery(" SELECT  * FROM mails  mail " +
            " WHERE (mail.id_mail=:id_mail OR :id_mail IS NULL ) AND " +
            "      (mail.id_directorio=:id_directorio OR :id_directorio IS NULL ) AND " +
            "      (mail.mail LIKE :mail OR :mail IS NULL ) AND " +
            "      (mail.tipo LIKE :tipo OR :tipo IS NULL ) AND " +
            "      (mail.preferencia = :preferencia OR :preferencia IS NULL )")
    List<Mail> OBTENER_MAILS(@Bind("id_mail") Long id_mail,@Bind("id_directorio") Long id_directorio,
                                     @Bind("mail") String mail,@Bind("tipo") String tipo,
                                     @Bind("preferencia") Integer preferencia);
}
