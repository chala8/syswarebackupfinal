package com.sysware.ingenieria.entities;

import java.util.List;

/**
 * Created by luis on 3/05/17.
 */
public class Usuario {
    private Long _uuid;
    private Long id_aplicacion;
    private String usuario;
    private String clave;
    private List<Rol> roles;

    public Usuario(Long _uuid, Long id_aplicacion, String usuario, String clave, List<Rol> roles) {
        this._uuid = _uuid;
        this.id_aplicacion = id_aplicacion;
        this.usuario = usuario;
        this.clave = clave;
        this.roles = roles;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    public Long get_uuid() {
        return _uuid;
    }

    public void set_uuid(Long _uuid) {
        this._uuid = _uuid;
    }

    public Long getId_aplicacion() {
        return id_aplicacion;
    }

    public void setId_aplicacion(Long id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }


}
