package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sysware.ingenieria.entities.Directorio;

/**
 * Created by luis on 30/06/17.
 */
public class PersonaDirectorioDTO {
    private PersonaDTO personaDTO;
    private DirectorioDTO directorioDTO;

    @JsonCreator
    public PersonaDirectorioDTO(@JsonProperty("persona") PersonaDTO personaDTO, @JsonProperty("directorio") DirectorioDTO directorioDTO) {
        this.personaDTO = personaDTO;
        this.directorioDTO = directorioDTO;
    }

    public PersonaDTO getPersonaDTO() {
        return personaDTO;
    }

    public void setPersonaDTO(PersonaDTO personaDTO) {
        this.personaDTO = personaDTO;
    }

    public DirectorioDTO getDirectorioDTO() {
        return directorioDTO;
    }

    public void setDirectorioDTO(DirectorioDTO directorioDTO) {
        this.directorioDTO = directorioDTO;
    }
}
