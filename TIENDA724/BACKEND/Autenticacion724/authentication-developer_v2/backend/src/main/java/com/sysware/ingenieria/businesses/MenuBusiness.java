package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.MenuDAO;
import com.sysware.ingenieria.entities.Menus;
import com.sysware.ingenieria.representations.MenuDTO;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.ManagementException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.sysware.ingenieria.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.sysware.ingenieria.utils.constans.K.messages_error;
import static com.sysware.ingenieria.utils.constans.K.messages_errors;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.*;

/**
 * Created by luis on 4/05/17.
 */
public class MenuBusiness {

    private MenuDAO menuDAO;

    public MenuBusiness(MenuDAO menuDAO) {
        this.menuDAO = menuDAO;
    }


    public Either<IException,List<Menus>> obtenerMenus(Long id_menu, Long id_aplicacion, String nombre, Integer tipo, String ruta, String icono, String avatar, String background, Integer estado, String dispositivo){
        try {

            List<Menus> aplicacionList = menuDAO.OBTENER_MENUS( id_menu,  id_aplicacion,  nombre,  tipo,  ruta,  icono,  avatar,  background, estado, formatoStringSqlMAYUS(dispositivo) );
            return Either.right(aplicacionList);

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }

    public Either<IException, Long> crearMenu(Long id_usuario, MenuDTO menuDTO) {
        List<String> msn=new ArrayList<>();
        try {

            if (formatoLongSql(menuDTO.getId_aplicacion())==null){
                msn.add("La Aplicacion no se reconoce, para crearle un Menu");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            if (verificarApplicacionUsuario(menuDTO.getId_aplicacion(),menuDTO.getNombre())){
                msn.add("El menu ["+menuDTO.getNombre()+"] ya existe");
                return Either.left(new BussinessException(messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear MENU    ***********************************");
            if (menuDTO!=null){

                long id_rol = menuDAO.CREAR_MENU(formatoLongSql(id_usuario), menuDTO);

                if (id_rol>0){

                    return Either.right(menuDAO.getPkLast());


                }else{
                    msn.add("No se reconoce formato del Menu , esta mal formado");

                }

            }
            return Either.left(new BussinessException(messages_errors(msn)));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    private boolean verificarApplicacionUsuario(Long id_aplicacion, String nombre) {
        return (menuDAO.VERIFICAR_MENU(id_aplicacion,nombre)==0)?false:true;
    }

    public Either<IException, List<Menus>> obtenerListaMenusPorRol(Long id_rol, Long id_menu) {

        try{

           return Either.right( menuDAO.OBTENER_MENUS(formatoLongSql(id_menu),null,null,null,null,null,null,null,null,null));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }
}
