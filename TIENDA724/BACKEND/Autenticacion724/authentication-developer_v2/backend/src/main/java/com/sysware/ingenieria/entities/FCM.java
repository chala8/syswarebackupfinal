package com.sysware.ingenieria.entities;

import java.sql.Timestamp;

/**
 * Created by luis on 8/04/17.
 */
public class FCM {

    private Long id;
    private String title;
    private String description;
    private String data_info;
    private Integer status;
    private Timestamp date_sent;
    private String type;

    public FCM(Long id, String title, String description, String data_info, Integer status, Timestamp date_sent, String type) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.data_info = data_info;
        this.status = status;
        this.date_sent = date_sent;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getData_info() {
        return data_info;
    }

    public void setData_info(String data_info) {
        this.data_info = data_info;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Timestamp getDate_sent() {
        return date_sent;
    }

    public void setDate_sent(Timestamp date_sent) {
        this.date_sent = date_sent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
