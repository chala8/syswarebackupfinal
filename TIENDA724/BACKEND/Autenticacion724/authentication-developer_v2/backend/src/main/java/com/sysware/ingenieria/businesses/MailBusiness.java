package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.MailDAO;
import com.sysware.ingenieria.entities.Mail;
import com.sysware.ingenieria.representations.MailDTO;
import com.sysware.ingenieria.translators.TelefonoMailBulk;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import fj.data.Either;

import java.util.ArrayList;
import java.util.List;

import static com.sysware.ingenieria.translators.TelefonoMailBulk.traductorMails;
import static com.sysware.ingenieria.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.sysware.ingenieria.utils.constans.K.messages_error;
import static com.sysware.ingenieria.utils.constans.K.messages_errors;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoIntegerSql;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoLIKESql;
import static com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter.formatoLongSql;

/**
 * Created by luis on 30/06/17.
 */
public class MailBusiness {

    private MailDAO mailDAO;

    public MailBusiness(MailDAO mailDAO) {
        this.mailDAO = mailDAO;
    }

    public Either<IException, Long> crearMail(Long id_directorio, List<MailDTO> mailDTOList) {
        List<String> msn=new ArrayList<>();
        List<Long> id_rolesParaCrear=new ArrayList<>();

        try {
            if (id_directorio==null||id_directorio<=0 || mailDTOList==null || mailDTOList.isEmpty() || mailDTOList.size()<=0 ){
                msn.add("No se reconoce formato del Telefono , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                TelefonoMailBulk telefonoMailBulk = traductorMails(mailDTOList);

                mailDAO.CREAR_TELEFONO(id_directorio,telefonoMailBulk.getMailTelList(),telefonoMailBulk.getTipoList(),telefonoMailBulk.getPreferenciaList());
                return Either.right(id_directorio);
            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Long> editarMail(Long id_directorio, List<Mail> mailList) {
        List<String> msn=new ArrayList<>();

        try {
            if (id_directorio==null||id_directorio<=0 || mailList==null || mailList.isEmpty()|| mailList.size()<=0 ){
                msn.add("No se reconoce formato del Telefono , esta mal formado");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                Long filas_afectadas=new Long(0);
                for (Mail mail: mailList) {
                    filas_afectadas = mailDAO.EDITAR_MAIL(id_directorio, mail);

                }
                return Either.right(id_directorio);
            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, Integer> borrarMail( Long id_directorio,Long id_mail) {
        List<String> msn=new ArrayList<>();

        try {
            if (formatoLongSql(id_directorio)==null&&formatoLongSql(id_mail)==null){
                msn.add("Faltan datos  para eliminar el mail  ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                Integer filas = mailDAO.ELIMINAR_MAIL(formatoLongSql(id_directorio), formatoLongSql(id_mail));
                if (filas>0){
                    return Either.right(filas);
                }else {
                    msn.add("No se puede eliminar el Mail ");
                    return Either.left(new BussinessException(messages_errors(msn)));
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<Mail>> obtenerMails( Long id_directorio,Long id_mail,String mail,
                                                                String tipo, Integer preferencia) {
        List<Mail> mailList= new ArrayList<>();
        List<String> msn=new ArrayList<>();

        try {
            if (formatoLongSql(id_directorio)==null&&formatoLongSql(id_mail)==null){
                msn.add("Faltan datos  para eliminar el telefono  ");
                return Either.left(new BussinessException(messages_errors(msn)));
            }else{
                mailList = mailDAO.OBTENER_MAILS(formatoLongSql(id_mail), formatoLongSql(id_directorio),
                        formatoLIKESql(mail),formatoLIKESql(tipo), formatoIntegerSql(preferencia));
                return Either.right(mailList);

            }

        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



}
