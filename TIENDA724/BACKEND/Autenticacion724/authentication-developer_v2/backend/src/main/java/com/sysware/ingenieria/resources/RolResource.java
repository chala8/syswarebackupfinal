package com.sysware.ingenieria.resources;

import com.sysware.ingenieria.businesses.RolBusiness;
import com.sysware.ingenieria.entities.Rol;
import com.sysware.ingenieria.representations.RolDTO;
import com.sysware.ingenieria.utils.exeptions.ExceptionResponse;
import com.sysware.ingenieria.utils.exeptions.IException;
import fj.data.Either;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/roles")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RolResource {
    private RolBusiness rolBusiness;

    public RolResource(RolBusiness rolBusiness) {
        this.rolBusiness = rolBusiness;
    }

    @GET
    public Response obtenerListaRoles(@QueryParam("id_aplicacion")Long id_applicacion,
                                             @QueryParam("id_rol")Long id_rol,
                                             @QueryParam("rol") String rol,
                                             @QueryParam("descripcion") String descripcion){
        Response response;

        Either<IException, List<Rol>> allViewOffertsEither = rolBusiness.obtenerRoles(id_applicacion,id_rol, rol,descripcion);


        if (allViewOffertsEither.isRight()){
            System.out.println(allViewOffertsEither.right().value().size());
            response=Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @POST
    public Response crearRol( RolDTO rolDTO) {
        Either<IException, Long> offerEither = rolBusiness.crearRol(rolDTO);
        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @POST
    @Path("menu")
    public Response crearRolMenu(@QueryParam("id_rol")Long id_rol,List<Long>id_menuRolLista) {

        Either<IException, Long> offerEither = rolBusiness.crearRolMenu(id_rol,id_menuRolLista);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }

    @GET
    @Path("menu")
    public Response obtenerMenusPorRol(@QueryParam("id_rol")Long id_rol,@QueryParam("id_menu")Long id_menu) {

        Either<IException, List<Long>> offerEither = rolBusiness.obtenerIdMenusPorRol(id_rol,id_menu);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();

        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }



    @PUT
    @Path("menu")
    public Response borrarMenusPorRol(@QueryParam("id_rol")Long id_rol,List<Long>id_menuRolLista) {

        Either<IException, Integer> offerEither = rolBusiness.borrarMenusPorRol(id_rol,id_menuRolLista);

        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }




    @POST
    @Path("api")
    public Response crearPermiso(@QueryParam("id_rol")Long id_rol, @QueryParam("id_permiso_api")Long id_permiso_api) {
        Either<IException, Long> offerEither = rolBusiness.crearPermiso(id_rol,id_permiso_api);
        if(offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        } else {
            return ExceptionResponse.createErrorResponse(offerEither);
        }
    }
}
