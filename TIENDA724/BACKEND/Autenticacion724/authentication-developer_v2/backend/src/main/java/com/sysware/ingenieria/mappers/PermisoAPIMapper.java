package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.PermisoAPI;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 4/05/17.
 */
public class PermisoAPIMapper implements ResultSetMapper<PermisoAPI> {

    @Override
    public PermisoAPI map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new PermisoAPI(
                resultSet.getLong("id_permiso_api"),
                resultSet.getLong("id_aplicacion"),
                resultSet.getLong("id_api"),
                resultSet.getString("http_verb"),
                resultSet.getString("http_uri")
        );
    }
}
