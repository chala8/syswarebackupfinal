package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Usuario;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by luis on 14/05/17.
 */
public class UsuarioMapper implements ResultSetMapper<Usuario>{
    @Override
    public Usuario map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
        return new Usuario(
                 resultSet.getLong("UUID"),
                resultSet.getLong("ID_APLICACION"),
                resultSet.getString("USUARIO"),
                resultSet.getString("CLAVE"),null

        );
    }
}
