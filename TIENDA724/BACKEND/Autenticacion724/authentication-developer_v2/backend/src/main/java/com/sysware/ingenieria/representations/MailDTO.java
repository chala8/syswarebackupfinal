package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by luis on 30/06/17.
 */
public class MailDTO {
    private String mail;
    private String tipo;
    private Integer preferencia;

    @JsonCreator
    public MailDTO(@JsonProperty("mail") String mail,@JsonProperty("tipo") String tipo,
                   @JsonProperty("preferencia") Integer preferencia) {
        this.mail = mail;
        this.tipo = tipo;
        this.preferencia = preferencia;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getPreferencia() {
        return preferencia;
    }

    public void setPreferencia(Integer preferencia) {
        this.preferencia = preferencia;
    }
}
