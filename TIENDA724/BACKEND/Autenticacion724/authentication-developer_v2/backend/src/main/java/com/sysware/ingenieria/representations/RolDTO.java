package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by luis on 3/05/17.
 */
public class RolDTO {

    private Long id_aplicacion;
    private String rol;
    private String descripcion;

    @JsonCreator
    public RolDTO(@JsonProperty("id_aplicacion") Long id_aplicacion,
                  @JsonProperty("rol") String rol,@JsonProperty("descripcion") String descripcion) {
        this.id_aplicacion = id_aplicacion;
        this.rol = rol;
        this.descripcion=descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getId_aplicacion() {
        return id_aplicacion;
    }

    public void setId_aplicacion(Long id_aplicacion) {
        this.id_aplicacion = id_aplicacion;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
}
