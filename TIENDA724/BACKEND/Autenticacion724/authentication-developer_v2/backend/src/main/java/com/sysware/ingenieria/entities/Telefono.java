package com.sysware.ingenieria.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by luis on 3/05/17.
 */
public class Telefono {
    private Long id_telefono;
    private String numero;
    private String tipo;
    private Integer preferencia;

    @JsonCreator
    public Telefono(@JsonProperty("id_telefono") Long id_telefono,@JsonProperty("numero") String numero, @JsonProperty("tipo") String tipo,
                       @JsonProperty("preferencia") Integer preferencia) {
        this.id_telefono=id_telefono;
        this.numero = numero;
        this.tipo = tipo;
        this.preferencia = preferencia;
    }

    public Long getId_telefono() {
        return id_telefono;
    }

    public void setId_telefono(Long id_telefono) {
        this.id_telefono = id_telefono;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getPreferencia() {
        return preferencia;
    }

    public void setPreferencia(Integer preferencia) {
        this.preferencia = preferencia;
    }
}
