package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sysware.ingenieria.entities.Mail;
import com.sysware.ingenieria.entities.Telefono;

import java.util.List;


public class DirectorioDTO {
    private List<MailDTO> mailDTOList;
    private List<TelefonoDTO> telefonoDTOS;

    @JsonCreator
    public DirectorioDTO(@JsonProperty("mails") List<MailDTO> mailDTOList,@JsonProperty("telefonos") List<TelefonoDTO> telefonoDTOS) {
        this.mailDTOList = mailDTOList;
        this.telefonoDTOS = telefonoDTOS;
    }

    public List<MailDTO> getMailDTOList() {
        return mailDTOList;
    }

    public void setMailDTOList(List<MailDTO> mailDTOList) {
        this.mailDTOList = mailDTOList;
    }

    public List<TelefonoDTO> getTelefonoDTOS() {
        return telefonoDTOS;
    }

    public void setTelefonoDTOS(List<TelefonoDTO> telefonoDTOS) {
        this.telefonoDTOS = telefonoDTOS;
    }
}
