/*     */ package com.sysware.ingenieria.entities;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Menus
/*     */ {
/*     */   private Long id_menu;
/*     */   private Long id_aplicacion;
/*     */   private String nombre;
/*     */   private Integer tipo;
/*     */   private String ruta;
/*     */   private String icono;
/*     */   private String avatar;
/*     */   private String background;
/*     */   private Integer estado;
/*     */   private String dispositivo;
/*     */   private Long id_menu_padre;
/*     */   
/*     */   public Menus(Long id_menu, Long id_aplicacion, String nombre, Integer tipo, String ruta, String icono, String avatar, String background, Integer estado, String dispositivo, Long id_menu_padre) {
/*  21 */     this.id_menu = id_menu;
/*  22 */     this.id_aplicacion = id_aplicacion;
/*  23 */     this.nombre = nombre;
/*  24 */     this.tipo = tipo;
/*  25 */     this.ruta = ruta;
/*  26 */     this.icono = icono;
/*  27 */     this.avatar = avatar;
/*  28 */     this.background = background;
/*  29 */     this.estado = estado;
/*  30 */     this.dispositivo = dispositivo;
/*  31 */     this.id_menu_padre = id_menu_padre;
/*     */   }
/*     */ 
/*     */   
/*  35 */   public Long getId_menu_padre() { return this.id_menu_padre; }
/*     */ 
/*     */ 
/*     */   
/*  39 */   public void setId_menu_padre(Long id_menu_padre) { this.id_menu_padre = id_menu_padre; }
/*     */ 
/*     */ 
/*     */   
/*  43 */   public String getDispositivo() { return this.dispositivo; }
/*     */ 
/*     */ 
/*     */   
/*  47 */   public void setDispositivo(String dispositivo) { this.dispositivo = dispositivo; }
/*     */ 
/*     */ 
/*     */   
/*  51 */   public Long getId_menu() { return this.id_menu; }
/*     */ 
/*     */ 
/*     */   
/*  55 */   public void setId_menu(Long id_menu) { this.id_menu = id_menu; }
/*     */ 
/*     */ 
/*     */   
/*  59 */   public Long getId_aplicacion() { return this.id_aplicacion; }
/*     */ 
/*     */ 
/*     */   
/*  63 */   public void setId_aplicacion(Long id_aplicacion) { this.id_aplicacion = id_aplicacion; }
/*     */ 
/*     */ 
/*     */   
/*  67 */   public String getNombre() { return this.nombre; }
/*     */ 
/*     */ 
/*     */   
/*  71 */   public void setNombre(String nombre) { this.nombre = nombre; }
/*     */ 
/*     */ 
/*     */   
/*  75 */   public Integer getTipo() { return this.tipo; }
/*     */ 
/*     */ 
/*     */   
/*  79 */   public void setTipo(Integer tipo) { this.tipo = tipo; }
/*     */ 
/*     */ 
/*     */   
/*  83 */   public String getRuta() { return this.ruta; }
/*     */ 
/*     */ 
/*     */   
/*  87 */   public void setRuta(String ruta) { this.ruta = ruta; }
/*     */ 
/*     */ 
/*     */   
/*  91 */   public String getIcono() { return this.icono; }
/*     */ 
/*     */ 
/*     */   
/*  95 */   public void setIcono(String icono) { this.icono = icono; }
/*     */ 
/*     */ 
/*     */   
/*  99 */   public String getAvatar() { return this.avatar; }
/*     */ 
/*     */ 
/*     */   
/* 103 */   public void setAvatar(String avatar) { this.avatar = avatar; }
/*     */ 
/*     */ 
/*     */   
/* 107 */   public String getBackground() { return this.background; }
/*     */ 
/*     */ 
/*     */   
/* 111 */   public void setBackground(String background) { this.background = background; }
/*     */ 
/*     */ 
/*     */   
/* 115 */   public Integer getEstado() { return this.estado; }
/*     */ 
/*     */ 
/*     */   
/* 119 */   public void setEstado(Integer estado) { this.estado = estado; }
/*     */ }


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\entities\Menus.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */