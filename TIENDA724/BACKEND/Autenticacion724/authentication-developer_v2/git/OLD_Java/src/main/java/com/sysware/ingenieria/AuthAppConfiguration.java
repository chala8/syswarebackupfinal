package com.sysware.ingenieria;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;



public class AuthAppConfiguration
/*    */   extends Configuration
{
/*    */   @Valid
/*    */   @NotNull
/*    */   @JsonProperty
/* 17 */   private DataSourceFactory database = new DataSourceFactory();


/*    */   
/*    */   @Valid
/*    */   @NotNull
/*    */   private AuthAppDatabaseConfiguration databaseConfiguration;



/*    */   
/* 28 */   public DataSourceFactory getDataSourceFactory() { return this.database; }

/*    */   
/* 31 */   public AuthAppDatabaseConfiguration getDatabaseConfiguration() { return this.databaseConfiguration; }
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\AuthAppConfiguration.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */