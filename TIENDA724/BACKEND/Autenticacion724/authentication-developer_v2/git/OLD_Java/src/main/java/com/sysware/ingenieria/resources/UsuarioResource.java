package com.sysware.ingenieria.resources;
import com.sysware.ingenieria.businesses.UsuarioBusiness;
import com.sysware.ingenieria.entities.Usuario;
import com.sysware.ingenieria.representations.*;

import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.ExceptionResponse;
import com.sysware.ingenieria.utils.exeptions.IException;
import fj.data.Either;
import java.util.List;
import javax.ws.rs.*;





import javax.ws.rs.core.Response;

@Path("/usuarios")
@Produces({"application/json"})
@Consumes({"application/json"})
public class UsuarioResource {
    public UsuarioResource(UsuarioBusiness usuarioBusiness) { this.usuarioBusiness = usuarioBusiness; }
    private UsuarioBusiness usuarioBusiness;

    @GET
    public Response obtenerListaUsuario(@QueryParam("id_aplicacion") Long id_aplicacion, @QueryParam("id_usuario") Long id_usuario, @QueryParam("usuario") String usuario, @QueryParam("clave") String clave) {
        Response response;
        Either<IException, List<Usuario>> allViewOffertsEither = this.usuarioBusiness.obtenerUsuarioApp(id_aplicacion, id_usuario, usuario, clave);

        if (allViewOffertsEither.isRight()) {
            System.out.println(allViewOffertsEither.right().value().size());
            response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
        }
        return response;
    }

    @POST
    public Response crearUsuario(UsuarioDTO usuarioAppDTO) {
        Either<IException, Long> offerEither = this.usuarioBusiness.crearUsuarioApp(usuarioAppDTO);

        if (offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        }
        return ExceptionResponse.createErrorResponse(offerEither);
    }

//    @DELETE
//    public Response eliminarUsuario(@QueryParam("uuid_usuario") Long uuid_usuario) {
//        Either<IException, Long> offerEither = this.usuarioBusiness.eliminarUsuario(uuid_usuario);
//
//        if (offerEither.isRight()) {
//            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
//        }
//        return ExceptionResponse.createErrorResponse(offerEither);
//    }

    @PUT
    public Response editarUsuario(@QueryParam("id_usuario") Integer _uuid, UsuarioDTO usuarioAppDTO) {
        Either<IException, Integer> offerEither = this.usuarioBusiness.editarUsuario(_uuid, usuarioAppDTO, false);

        if (offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        }
        return ExceptionResponse.createErrorResponse(offerEither);
    }

    @PUT
    @Path("my-rol")
    public Response editarUsuarioCambiarRol(@QueryParam("id_usuario") Integer _uuid, UsuarioDTO usuarioAppDTO) {
        Either<IException, Integer> offerEither = this.usuarioBusiness.editarUsuario(_uuid, usuarioAppDTO, true);

        if (offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        }
        return ExceptionResponse.createErrorResponse(offerEither);
    }

    @POST
    @Path("rol")
    public Response crearRolMenu(@QueryParam("id_usaurio") Integer _uuid, List<Long> id_rolUsuarioLista) {
        Either<IException, Long> offerEither = this.usuarioBusiness.crearUsuarioRoles(null, _uuid, id_rolUsuarioLista);

        if (offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        }
        return ExceptionResponse.createErrorResponse(offerEither);
    }

    @GET
    @Path("rol")
    public Response obtenerIdRolesPorUsuario(@QueryParam("id_rol") Long id_rol, @QueryParam("id_usuario") Long id_usuario) {
        Either<IException, List<Long>> offerEither = this.usuarioBusiness.obtenerIdRolesPorUsuario(null, id_rol, id_usuario);

        if (offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        }

        return ExceptionResponse.createErrorResponse(offerEither);
    }

    @PUT
    @Path("rol")
    public Response borrarMenusPorRol(@QueryParam("id_usuario") Long id_usuario, List<Long> id_menuRolLista) {
        Either<IException, Integer> offerEither = this.usuarioBusiness.borrarRolesPorUsuario(id_usuario, id_menuRolLista);

        if (offerEither.isRight()) {
            return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
        }
        return ExceptionResponse.createErrorResponse(offerEither);
    }

    @POST
    @Path("login")
    public Response iniciarSession(@QueryParam("id_aplicacion") Long id_aplicacion, @QueryParam("dispositivo") String dispositivo, AuthDTO authDTO) {
        Response response;
        Either<IException, TokenDTO> iniarSesionEither = this.usuarioBusiness.iniarSesion(id_aplicacion, dispositivo, authDTO);

        if (iniarSesionEither.isRight()) {
            response = Response.status(Response.Status.OK).entity(iniarSesionEither.right().value()).build();
        }
        else if (((IException)iniarSesionEither.left().value()).getClass().equals(BussinessException.class)) {
            response = Response.status(Response.Status.OK).entity(iniarSesionEither.left().value().getMessage()).build();
        } else {
            response = ExceptionResponse.createErrorResponse(iniarSesionEither);
        }
        return response;
    }
}