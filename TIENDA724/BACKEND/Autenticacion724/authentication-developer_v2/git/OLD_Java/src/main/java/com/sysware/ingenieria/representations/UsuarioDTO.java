package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;



/*    */ 
/*    */ 
/*    */ public class UsuarioDTO
/*    */ {
/*    */   private Long id_aplicacion;
/*    */   private String usuario;
/*    */   private String clave;
/*    */   private List<Long> id_roles;
/*    */   
/*    */   @JsonCreator
/*    */   public UsuarioDTO(@JsonProperty("id_aplicacion") Long id_aplicacion, @JsonProperty("usuario") String usuario, @JsonProperty("clave") String clave, @JsonProperty("id_roles") List<Long> id_roles) {
/* 20 */     this.id_aplicacion = id_aplicacion;
/* 21 */     this.usuario = usuario;
/* 22 */     this.clave = clave;
/* 23 */     this.id_roles = id_roles;
/*    */   }
/*    */ 
/*    */   
/* 27 */   public Long getId_aplicacion() { return this.id_aplicacion; }
/*    */ 
/*    */ 
/*    */   
/* 31 */   public void setId_aplicacion(Long id_aplicacion) { this.id_aplicacion = id_aplicacion; }
/*    */ 
/*    */ 
/*    */   
/* 35 */   public String getUsuario() { return this.usuario; }
/*    */ 
/*    */ 
/*    */   
/* 39 */   public void setUsuario(String usuario) { this.usuario = usuario; }
/*    */ 
/*    */ 
/*    */   
/* 43 */   public String getClave() { return this.clave; }
/*    */ 
/*    */ 
/*    */   
/* 47 */   public void setClave(String clave) { this.clave = clave; }
/*    */ 
/*    */ 
/*    */   
/* 51 */   public List<Long> getId_roles() { return this.id_roles; }
/*    */ 
/*    */ 
/*    */   
/* 55 */   public void setId_roles(List<Long> id_roles) { this.id_roles = id_roles; }
/*    */ }


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\representations\UsuarioDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */