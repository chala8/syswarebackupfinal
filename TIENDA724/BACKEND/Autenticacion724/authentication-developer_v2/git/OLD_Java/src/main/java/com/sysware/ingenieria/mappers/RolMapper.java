package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Rol;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;



public class RolMapper
/*    */   implements ResultSetMapper<Rol>
{
/*    */   public Rol map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
/* 15 */     return new Rol(
/* 16 */         Long.valueOf(resultSet.getLong("id_rol")), 
/* 17 */         Long.valueOf(resultSet.getLong("id_aplicacion")), resultSet
/* 18 */         .getString("rol"), resultSet
/* 19 */         .getString("descripcion"));
/*    */   }
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\mappers\RolMapper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */