package com.sysware.ingenieria.utils.exeptions;

import java.util.HashMap;













public class ManagementException
{
/* 19 */   public static IException catchException(Exception e) { return new TechnicalException(e.getMessage()); }



/*    */   
/* 24 */   public static IException noFoundException(String object) { return new BussinessException(object + " not found"); }

/*    */   
/*    */   public static HashMap<String, String> okException(final String KEY, final String VALUE) {
/* 28 */     return new HashMap<String, String>() {  }
/*    */       ;
/*    */   }
/*    */   public static HashMap<String, Long> okException(final String KEY, final long VALUE) {
/* 32 */     return new HashMap<String, Long>() {  }
/*    */       ;
/*    */   }
/*    */   
/* 36 */   public static IException acctionFailedException(String auth) { return new BussinessException(auth + " failed please try again"); }

/*    */   
/* 39 */   public static IException INTERNAL_ERROR_MESSAGE() { return new TechnicalException("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros"); }

/*    */   
/* 42 */   public static IException INVALID_USER() { return new BussinessException("El usuario es Invalido"); }

/*    */   
/* 45 */   public static IException INVALID_USER_PASS() { return new BussinessException("El usuario y/o clave no es valido"); }
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieri\\utils\exeptions\ManagementException.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */