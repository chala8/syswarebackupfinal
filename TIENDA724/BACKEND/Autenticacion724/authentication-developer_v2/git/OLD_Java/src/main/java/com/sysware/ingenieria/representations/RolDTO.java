package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;





/*    */ public class RolDTO
/*    */ {
/*    */   private Long id_aplicacion;
/*    */   private String rol;
/*    */   private String descripcion;
/*    */   
/*    */   @JsonCreator
/*    */   public RolDTO(@JsonProperty("id_aplicacion") Long id_aplicacion, @JsonProperty("rol") String rol, @JsonProperty("descripcion") String descripcion) {
/* 18 */     this.id_aplicacion = id_aplicacion;
/* 19 */     this.rol = rol;
/* 20 */     this.descripcion = descripcion;
/*    */   }
/*    */ 
/*    */   
/* 24 */   public String getDescripcion() { return this.descripcion; }
/*    */ 
/*    */ 
/*    */   
/* 28 */   public void setDescripcion(String descripcion) { this.descripcion = descripcion; }
/*    */ 
/*    */ 
/*    */   
/* 32 */   public Long getId_aplicacion() { return this.id_aplicacion; }
/*    */ 
/*    */ 
/*    */   
/* 36 */   public void setId_aplicacion(Long id_aplicacion) { this.id_aplicacion = id_aplicacion; }
/*    */ 
/*    */ 
/*    */   
/* 40 */   public String getRol() { return this.rol; }
/*    */ 
/*    */ 
/*    */   
/* 44 */   public void setRol(String rol) { this.rol = rol; }
/*    */ }


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\representations\RolDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */