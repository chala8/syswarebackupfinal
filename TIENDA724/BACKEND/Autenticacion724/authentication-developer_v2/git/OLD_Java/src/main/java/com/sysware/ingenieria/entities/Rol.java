package com.sysware.ingenieria.entities;




public class Rol
{
/*    */   private Long id_rol;
/*    */   private Long id_aplicacion;
/*    */   private String rol;
/*    */   private String descripcion;
/*    */   
/*    */   public Rol(Long id_rol, Long id_aplicacion, String rol, String descripcion) {
/* 14 */     this.id_rol = id_rol;
/* 15 */     this.id_aplicacion = id_aplicacion;
/* 16 */     this.rol = rol;
/* 17 */     this.descripcion = descripcion;
/*    */   }

/*    */   
/* 21 */   public Long getId_rol() { return this.id_rol; }
/*    */ 
/*    */ 
/*    */   
/* 25 */   public void setId_rol(Long id_rol) { this.id_rol = id_rol; }
/*    */ 
/*    */ 
/*    */   
/* 29 */   public Long getId_aplicacion() { return this.id_aplicacion; }
/*    */ 
/*    */ 
/*    */   
/* 33 */   public void setId_aplicacion(Long id_aplicacion) { this.id_aplicacion = id_aplicacion; }
/*    */ 
/*    */ 
/*    */   
/* 37 */   public String getRol() { return this.rol; }
/*    */ 
/*    */ 
/*    */   
/* 41 */   public void setRol(String rol) { this.rol = rol; }
/*    */ 
/*    */ 
/*    */   
/* 45 */   public String getDescripcion() { return this.descripcion; }
/*    */ 
/*    */ 
/*    */   
/* 49 */   public void setDescripcion(String descripcion) { this.descripcion = descripcion; }
/*    */ }


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\entities\Rol.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */