package com.sysware.ingenieria.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;



public class AuthDTO
{
/*    */   private String usuario;
/*    */   private String clave;
/*    */   
/*    */   @JsonCreator
/*    */   public AuthDTO(@JsonProperty("usuario") String usuario, @JsonProperty("clave") String clave) {
/* 15 */     this.usuario = usuario;
/* 16 */     this.clave = clave;
/*    */   }


/*    */   
/* 21 */   public String getUsuario() { return this.usuario; }


/*    */   
/* 25 */   public void setUsuario(String usuario) { this.usuario = usuario; }


/*    */   
/* 29 */   public String getClave() { return this.clave; }


/*    */   
/* 33 */   public void setClave(String clave) { this.clave = clave; }
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\representations\AuthDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */