package com.sysware.ingenieria;
import com.sysware.ingenieria.DAOs.MenuDAO;
import com.sysware.ingenieria.DAOs.RolDAO;
import com.sysware.ingenieria.DAOs.UsuarioDAO;
import com.sysware.ingenieria.businesses.*;

import com.sysware.ingenieria.resources.*;
import io.dropwizard.Application;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.java8.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.skife.jdbi.v2.DBI;

import java.util.EnumSet;
import static com.sysware.ingenieria.utils.constans.K.URI_BASE;

public class AuthApp extends Application<AuthAppConfiguration> {
   public static void main(String[] args) throws Exception { (new AuthApp()).run(args); }
   
   public void initialize(Bootstrap<AuthAppConfiguration> bootstrap) {
     bootstrap.addBundle(new Java8Bundle());
     bootstrap.addBundle(new MultiPartBundle());
   }

   public void run(AuthAppConfiguration configuration, Environment environment) {
     environment.jersey().setUrlPattern(URI_BASE + "/*");
     
     FilterRegistration.Dynamic corsFilter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);

     corsFilter.setInitParameter("allowedOrigins", "*");
     corsFilter.setInitParameter("Access-Control-Allow-Origin", "*");
     corsFilter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
     corsFilter.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");
     corsFilter.setInitParameter("allowCredentials", "true");
     
     corsFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
     
     DBIFactory factory = new DBIFactory();
     DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mysql");
     
     RolDAO rolDAO = jdbi.onDemand(RolDAO.class);
     RolBusiness rolBusiness = new RolBusiness(rolDAO);
     
     MenuDAO menuDAO = jdbi.onDemand(MenuDAO.class);
     MenuBusiness menuBusiness = new MenuBusiness(menuDAO);

     UsuarioDAO usuarioDAO = jdbi.onDemand(UsuarioDAO.class);
     UsuarioBusiness usuarioBusiness = new UsuarioBusiness(usuarioDAO, rolBusiness, menuBusiness);

     
     environment.jersey().register(new RolResource(rolBusiness));
     environment.jersey().register(new UsuarioResource(usuarioBusiness));
   }
 }