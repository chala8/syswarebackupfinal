package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Menus;
import com.sysware.ingenieria.mappers.MenuMapper;
import com.sysware.ingenieria.representations.MenuDTO;
import java.util.List;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

@RegisterMapper({MenuMapper.class})
public interface MenuDAO {
  @SqlQuery("SELECT ID_MENU FROM MENUS WHERE ID_MENU IN (SELECT MAX( ID_MENU ) FROM MENUS )\n ")
  Long getPkLast();
  
  @SqlQuery("SELECT  * FROM menus  menu  WHERE (menu.id_menu=:id_menu OR :id_menu IS NULL) AND (menu.id_aplicacion=:id_aplicacion OR :id_aplicacion IS NULL) AND (menu.nombre=:nombre OR :nombre IS NULL) AND (menu.tipo=:tipo OR :tipo IS NULL) AND (menu.ruta=:ruta OR :ruta IS NULL) AND (menu.icono=:icono OR :icono IS NULL) AND (menu.avatar=:avatar OR :avatar IS NULL) AND (menu.background=:background OR :background IS NULL) AND (menu.dispositivo=:dispositivo OR :dispositivo IS NULL) AND (menu.estado=:estado OR :estado IS NULL)")
  List<Menus> OBTENER_MENUS(@Bind("id_menu") Long paramLong1, @Bind("id_aplicacion") Long paramLong2, @Bind("nombre") String paramString1, @Bind("tipo") Integer paramInteger1, @Bind("ruta") String paramString2, @Bind("icono") String paramString3, @Bind("avatar") String paramString4, @Bind("background") String paramString5, @Bind("estado") Integer paramInteger2, @Bind("dispositivo") String paramString6);
  
  @SqlUpdate("INSERT INTO menus ( id_aplicacion, nombre, tipo, ruta, icono, avatar, background,estado) VALUES (:menu.id_aplicacion, :menu.nombre, :menu.tipo, :menu.ruta, :menu.icono, :menu.avatar, :menu.background,:menu.estado)")
  long CREAR_MENU(@Bind("id_usuario") Long paramLong, @BindBean("menu") MenuDTO paramMenuDTO);
  
  @SqlQuery("SELECT count(id_aplicacion) FROM menus  menu   WHERE  (menu.id_aplicacion=:id_aplicacion AND  menu.nombre=:nombre) ")
  Integer VERIFICAR_MENU(@Bind("id_aplicacion") Long paramLong, @Bind("nombre") String paramString);
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\DAOs\MenuDAO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */