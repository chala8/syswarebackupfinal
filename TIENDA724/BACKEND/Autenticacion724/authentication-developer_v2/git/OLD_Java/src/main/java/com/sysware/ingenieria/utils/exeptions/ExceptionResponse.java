package com.sysware.ingenieria.utils.exeptions;

import fj.data.Either;
import javax.ws.rs.core.Response;





public class ExceptionResponse
{
/*    */   public static Response createErrorResponse(Either<IException, ?> iExceptionEither) {
/* 13 */     IException iException = (IException)iExceptionEither.left().value();
/*    */     
/* 15 */     Response.Status statusCode = (iException instanceof BussinessException) ? Response.Status.BAD_REQUEST : Response.Status.INTERNAL_SERVER_ERROR;
/* 16 */     return Response.status(statusCode).entity(iException).build();
/*    */   }
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieri\\utils\exeptions\ExceptionResponse.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */