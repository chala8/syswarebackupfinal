/*    */ package com.sysware.ingenieria.representations;
/*    */ 
/*    */ import com.sysware.ingenieria.entities.Menus;
/*    */ import com.sysware.ingenieria.entities.Rol;
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class TokenDTO
/*    */ {
/*    */   private Long id_usuario;
/*    */   private Long id_aplicacion;
/*    */   private String usuario;
/*    */   private List<Menus> menus;
/*    */   private List<Rol> roles;
/*    */   
/*    */   public TokenDTO(Long id_usuario, Long id_aplicacion, String usuario, List<Menus> menus, List<Rol> roles) {
/* 20 */     this.id_usuario = id_usuario;
/* 21 */     this.id_aplicacion = id_aplicacion;
/* 22 */     this.usuario = usuario;
/* 23 */     this.menus = menus;
/* 24 */     this.roles = roles;
/*    */   }
/*    */ 
/*    */   
/* 28 */   public Long getId_usuario() { return this.id_usuario; }
/*    */ 
/*    */ 
/*    */   
/* 32 */   public void setId_usuario(Long id_usuario) { this.id_usuario = id_usuario; }
/*    */ 
/*    */ 
/*    */   
/* 36 */   public Long getId_aplicacion() { return this.id_aplicacion; }
/*    */ 
/*    */ 
/*    */   
/* 40 */   public void setId_aplicacion(Long id_aplicacion) { this.id_aplicacion = id_aplicacion; }
/*    */ 
/*    */ 
/*    */   
/* 44 */   public String getUsuario() { return this.usuario; }
/*    */ 
/*    */ 
/*    */   
/* 48 */   public void setUsuario(String usuario) { this.usuario = usuario; }
/*    */ 
/*    */ 
/*    */   
/* 52 */   public List<Menus> getMenus() { return this.menus; }
/*    */ 
/*    */ 
/*    */   
/* 56 */   public void setMenus(List<Menus> menus) { this.menus = menus; }
/*    */ 
/*    */ 
/*    */   
/* 60 */   public List<Rol> getRoles() { return this.roles; }
/*    */ 
/*    */ 
/*    */   
/* 64 */   public void setRols(List<Rol> roles) { this.roles = roles; }
/*    */ }


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\representations\TokenDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */