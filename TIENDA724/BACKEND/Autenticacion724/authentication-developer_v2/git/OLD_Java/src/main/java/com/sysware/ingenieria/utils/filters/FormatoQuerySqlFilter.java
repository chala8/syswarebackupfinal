package com.sysware.ingenieria.utils.filters;

import java.sql.Timestamp;
import java.time.LocalDateTime;




public class FormatoQuerySqlFilter
{
/*    */   public static String formatoLIKESql(String input) {
/* 12 */     if (input != null)
/* 13 */       return input.isEmpty() ? null : ("%" + input + "%"); 
/* 14 */     return null;
/*    */   }
/*    */   public static String formatoDouebleLIKESql(Double input) {
/* 17 */     if (input != null)
/* 18 */       return (input.doubleValue() < 0.0D) ? null : ("%" + input + "%"); 
/* 19 */     return null;
/*    */   }
/*    */   
/*    */   public static String formatoStringSql(String input) {
/* 23 */     if (input != null && !input.isEmpty())
/* 24 */       return (input == "") ? null : input; 
/* 25 */     return null;
/*    */   }
/*    */   
/*    */   public static String formatoStringSqlMAYUS(String input) {
/* 29 */     if (input != null && !input.isEmpty())
/* 30 */       return (input == "") ? null : input.toUpperCase(); 
/* 31 */     return null;
/*    */   }




/*    */   
/*    */   public static Long formatoLongSql(Long input) {
/* 39 */     if (input != null)
/* 40 */       return (input.longValue() < 1L) ? null : Long.valueOf(input.longValue()); 
/* 41 */     return null;
/*    */   }
/*    */   public static Integer formatoIntegerSql(Integer input) {
/* 44 */     if (input != null)
/* 45 */       return (input.intValue() < 1) ? null : Integer.valueOf(input.intValue()); 
/* 46 */     return null;
/*    */   }
/*    */   public static Double formatoDoubleSql(Double input) {
/* 49 */     if (input != null)
/* 50 */       return (input.doubleValue() < 0.0D) ? null : Double.valueOf(input.doubleValue()); 
/* 51 */     return null;
/*    */   }
/*    */   
/* 54 */   public static Timestamp formatoFechaFinSql(Long input) { return (input != null) ? new Timestamp(input.longValue()) : null; }


/*    */   
/* 58 */   public static Timestamp formatoFechaInicioSql(Long input) { return (input == null) ? Timestamp.valueOf(LocalDateTime.now()) : new Timestamp(input.longValue()); }
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieri\\utils\filters\FormatoQuerySqlFilter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */