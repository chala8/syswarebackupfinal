package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;




public class UsuarioMapper
/*    */   implements ResultSetMapper<Usuario>
{
/*    */   public Usuario map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
/* 16 */     return new Usuario(
/* 17 */         Long.valueOf(resultSet.getLong("UUID")), 
/* 18 */         Long.valueOf(resultSet.getLong("ID_APLICACION")), resultSet
/* 19 */         .getString("USUARIO"), resultSet
/* 20 */         .getString("CLAVE"), null);
/*    */   }
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\mappers\UsuarioMapper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */