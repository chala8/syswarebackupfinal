package com.sysware.ingenieria.businesses;
import com.sysware.ingenieria.DAOs.UsuarioDAO;
import com.sysware.ingenieria.entities.Menus;
import com.sysware.ingenieria.entities.Rol;
import com.sysware.ingenieria.entities.Usuario;
import com.sysware.ingenieria.representations.AuthDTO;
import com.sysware.ingenieria.representations.TokenDTO;
import com.sysware.ingenieria.representations.UsuarioDTO;
import com.sysware.ingenieria.utils.constans.K;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.ManagementException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter;
import com.sysware.ingenieria.utils.security.CodeGenerator;
import fj.data.Either;
import java.util.ArrayList;
import java.util.List;

public class UsuarioBusiness {
    private UsuarioDAO usuarioDAO;
    private RolBusiness rolBusiness;

    public UsuarioBusiness(UsuarioDAO usuarioDAO, RolBusiness rolBusiness, MenuBusiness menuBusiness) {
        this.usuarioDAO = usuarioDAO;
        this.rolBusiness = rolBusiness;
        this.menuBusiness = menuBusiness;
    }
    private MenuBusiness menuBusiness;
    public Either<IException, List<Usuario>> obtenerUsuarioApp(Long id_aplicacion, Long id_usuario_app, String usuario, String clave) {
        try {
            List<Usuario> aplicacionList = this.usuarioDAO.OBTENER_USUARIO(FormatoQuerySqlFilter.formatoLongSql(id_aplicacion), FormatoQuerySqlFilter.formatoLongSql(id_usuario_app), FormatoQuerySqlFilter.formatoLIKESql(usuario), FormatoQuerySqlFilter.formatoLIKESql(clave));
            return Either.right(aplicacionList);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }


    public Either<IException, Long> crearUsuarioApp(UsuarioDTO usuarioAppDTO) {
        List<String> msn = new ArrayList<>();

        try {
            if (usuarioAppDTO.getId_aplicacion() == null || usuarioAppDTO.getUsuario() == null || usuarioAppDTO.getUsuario().isEmpty() || usuarioAppDTO.getClave() == null || usuarioAppDTO.getClave().isEmpty()) {
                msn.add("No se reconoce formato del usuario , esta mal formado");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            Either<IException, List<Usuario>> usuarioApp = verificarUsuario(FormatoQuerySqlFilter.formatoLongSql(usuarioAppDTO.getId_aplicacion()), null, usuarioAppDTO.getUsuario().toLowerCase(), null);

            System.out.println("            Inicia a crear Usuario    ***********************************");
            if (usuarioApp.isRight() && usuarioApp.right().value().isEmpty()) {


                //noinspection ConstantConditions
                if (usuarioAppDTO != null) {
                    Integer _uuid = CodeGenerator._uuid();

                    long id_usuario = this.usuarioDAO.CREAR_USUARIOAPP(FormatoQuerySqlFilter.formatoLongSql(usuarioAppDTO.getId_aplicacion()), _uuid, usuarioAppDTO.getUsuario().toLowerCase(), usuarioAppDTO.getClave());

                    if (id_usuario > 0L) {

                        if (usuarioAppDTO.getId_roles() != null) {
                            if(usuarioAppDTO.getId_roles().size() > 0) {
                                if (usuarioAppDTO.getId_roles().size() > 0) {
                                    //noinspection unused
                                    Either<IException, Long> either = crearUsuarioRoles(id_usuario, _uuid, usuarioAppDTO.getId_roles());
                                }
                            }
                        }
                        return Either.right((long) _uuid);
                    }


                    msn.add("No se reconoce formato del usuario , esta mal formado");
                }

            } else {

                msn.add("El Usuario [" + usuarioAppDTO.getUsuario() + "] ya existe!");
            }
            return Either.left(new BussinessException(K.messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(K.messages_error("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros")));
        }
    }

    public Either<IException, List<Usuario>> verificarUsuario(Long id_aplicacion, Integer _uuid, String nombre, String clave) {
        try {
            if (FormatoQuerySqlFilter.formatoLongSql(id_aplicacion) != null && FormatoQuerySqlFilter.formatoStringSql(nombre) != null && FormatoQuerySqlFilter.formatoStringSql(clave) != null) {
                return Either.right(this.usuarioDAO.VEFICAR_CREDENCIALES(FormatoQuerySqlFilter.formatoLongSql(id_aplicacion), FormatoQuerySqlFilter.formatoStringSql(nombre), FormatoQuerySqlFilter.formatoStringSql(clave)));
            }
            if (FormatoQuerySqlFilter.formatoLongSql(id_aplicacion) != null && FormatoQuerySqlFilter.formatoStringSql(nombre) != null) {

                if (FormatoQuerySqlFilter.formatoIntegerSql(_uuid) != null) {
                    return Either.right(this.usuarioDAO.OBTENER_USUARIO_POR_NOMBRE(FormatoQuerySqlFilter.formatoLongSql(id_aplicacion), FormatoQuerySqlFilter.formatoIntegerSql(_uuid), nombre.toLowerCase()));
                }
                return Either.right(this.usuarioDAO.OBTENER_USUARIO_POR_NOMBRE(FormatoQuerySqlFilter.formatoLongSql(id_aplicacion), FormatoQuerySqlFilter.formatoIntegerSql(_uuid), nombre.toLowerCase()));
            }


            if (FormatoQuerySqlFilter.formatoLongSql(id_aplicacion) != null && FormatoQuerySqlFilter.formatoStringSql(clave) != null) {
                return Either.right(this.usuarioDAO.OBTENER_USUARIO_POR_NOMBRE(FormatoQuerySqlFilter.formatoLongSql(id_aplicacion), FormatoQuerySqlFilter.formatoIntegerSql(_uuid), clave.toLowerCase()));
            }
            return Either.left(new BussinessException("No se puede identificar el usuario con los parametros."));


        }
        catch (Exception e) {
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }

    public Either<IException, Integer> editarUsuario(Integer _uuid, UsuarioDTO usuarioAppDTO, boolean actualizarRol) {
        List<String> msn = new ArrayList<>();
        try {
            if (_uuid == null || usuarioAppDTO.getId_aplicacion() == null || usuarioAppDTO.getUsuario() == null || usuarioAppDTO.getUsuario().isEmpty() || usuarioAppDTO.getClave() == null || usuarioAppDTO.getClave().isEmpty()) {
                msn.add("No se reconoce formato del usuario , esta mal formado");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            Either<IException, List<Usuario>> usuarioApp = verificarUsuario(usuarioAppDTO.getId_aplicacion(), _uuid, usuarioAppDTO.getUsuario(), null);

            System.out.println("            Inicia a modificar roles    ***********************************");
            if (usuarioApp.isRight() && usuarioApp.right().value().isEmpty()) {
                //noinspection ConstantConditions
                if (usuarioAppDTO != null)
                {
                    int id_oferta = this.usuarioDAO.EDITAR_USUARIO(FormatoQuerySqlFilter.formatoLongSql(usuarioAppDTO.getId_aplicacion()), FormatoQuerySqlFilter.formatoIntegerSql(_uuid), usuarioAppDTO.getUsuario().toLowerCase(), usuarioAppDTO.getClave().toLowerCase());
                    if (id_oferta > 0) {
                        if (usuarioAppDTO.getId_roles() != null)
                        {
                            if(usuarioAppDTO.getId_roles().size() > 0){
                                if (actualizarRol) {
                                    //noinspection unused
                                    Either<IException, Long> either = editarUsuarioRoles(null, _uuid, usuarioAppDTO.getId_roles());
                                } else {
                                    //noinspection unused
                                    Either<IException, Long> either = crearUsuarioRoles(null, _uuid, usuarioAppDTO.getId_roles());
                                }
                            }
                        }
                        return Either.right(_uuid);
                    }
                    msn.add("No se reconoce formato del usuario , esta mal formado");
                }
            } else {
                msn.add("El Usuario [" + usuarioAppDTO.getUsuario() + "] ya existe!");
            }
            return Either.left(new BussinessException(K.messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(K.messages_error("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros")));
        }
    }

    public Either<IException, Long> editarUsuarioRoles(Long id_usuario, Integer _uuid, List<Long> id_rolLista) {
        List<String> msn = new ArrayList<>();
        List<Long> id_rolListaFinal = new ArrayList<>();

        try {
            if (FormatoQuerySqlFilter.formatoIntegerSql(_uuid) == null || id_rolLista.size() <= 0) {
                msn.add("Fatan datos  id rol y/o id rol");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            for (Long id_rol : id_rolLista) {
                if (verificarUsuarioRoles(id_usuario, _uuid, id_rol)) {
                    msn.add("El rol " + id_rol + " ya existe");
                    continue;
                }
                id_rolListaFinal.add(id_rol);
            }

            if (id_rolListaFinal.size() == 0) {
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear Rol    ***********************************");

            long id_rol_menu = 0L;
            for (Long id_rol : id_rolListaFinal) {
                id_rol_menu = this.usuarioDAO.EDITAR_USUARIO_ROL(FormatoQuerySqlFilter.formatoLongSql(id_rol), null, FormatoQuerySqlFilter.formatoIntegerSql(_uuid));
            }
            if (id_rol_menu > 0L)
            {
                return Either.right(id_rol_menu);
            }
            msn.add("No se reconoce formato del rol , esta mal formado");
            return Either.left(new BussinessException(K.messages_errors(msn)));
        }
        catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(K.messages_error("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros")));
        }
    }



    public Either<IException, Long> crearUsuarioRoles(Long id_usuario, Integer _uuid, List<Long> id_rolLista) {
        List<String> msn = new ArrayList<>();
        List<Long> id_rolListaFinal = new ArrayList<>();

        try {
            if (FormatoQuerySqlFilter.formatoIntegerSql(_uuid) == null || id_rolLista.size() <= 0) {
                msn.add("Fatan datos  id rol y/o id menu");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            for (Long id_rol : id_rolLista) {
                if (verificarUsuarioRoles(id_usuario, _uuid, id_rol)) {
                    msn.add("El menu " + id_rol + " ya existe");
                    continue;
                }
                id_rolListaFinal.add(id_rol);
            }

            if (id_rolListaFinal.size() == 0) {
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear Rol    ***********************************");

            long id_rol_menu = 0L;
            for (Long id_rol : id_rolListaFinal) {
                id_rol_menu = this.usuarioDAO.CREAR_USUARIO_ROL(FormatoQuerySqlFilter.formatoLongSql(id_usuario), FormatoQuerySqlFilter.formatoIntegerSql(_uuid), FormatoQuerySqlFilter.formatoLongSql(id_rol));
            }


            if (id_rol_menu > 0L)
            {
                return Either.right(this.usuarioDAO.getPkLastUSUARIOS_ROLES());
            }


            msn.add("No se reconoce formato del rol , esta mal formado");
            return Either.left(new BussinessException(K.messages_errors(msn)));



        }
        catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(K.messages_error("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros")));
        }
    }


    private boolean verificarUsuarioRoles(Long id_usuario, Integer _uuid, Long id_rol) { return !(this.usuarioDAO.VERIFICAR_USUARIO_ROL(FormatoQuerySqlFilter.formatoLongSql(id_usuario), _uuid, id_rol) == 0); }



    public Either<IException, List<Long>> obtenerIdRolesPorUsuario(Long id_usuario_rol, Long id_rol, Long _uuid) {
        List<Long> idMenus_Lista;
        List<String> msn = new ArrayList<>();

        try {
            if (FormatoQuerySqlFilter.formatoLongSql(id_rol) == null && FormatoQuerySqlFilter.formatoLongSql(_uuid) == null) {
                msn.add("Faltan datos  id rol y/o id usuario ");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            idMenus_Lista = this.usuarioDAO.OBTENER_ID_ROLES_POR_USUARIO(FormatoQuerySqlFilter.formatoLongSql(id_usuario_rol), FormatoQuerySqlFilter.formatoLongSql(id_rol), FormatoQuerySqlFilter.formatoLongSql(_uuid));
            return Either.right(idMenus_Lista);
        }
        catch (Exception e) {
            return Either.left(new TechnicalException(K.messages_error("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros")));
        }
    }


    public Either<IException, Integer> borrarRolesPorUsuario(Long _uuid, List<Long> id_menuRolLista) {
        //noinspection MismatchedQueryAndUpdateOfCollection
        List<Integer> idMenus_ListaFinal = new ArrayList<>();
        List<String> msn = new ArrayList<>();

        try {
            if (FormatoQuerySqlFilter.formatoLongSql(_uuid) == null) {
                msn.add("Faltan datos  id rol  ");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            if (id_menuRolLista != null && id_menuRolLista.size() > 0) {
                for (Long id_rol : id_menuRolLista) {
                    idMenus_ListaFinal.add(this.usuarioDAO.ELIMINAR_ROL_USUARIO(null, FormatoQuerySqlFilter.formatoLongSql(id_rol), FormatoQuerySqlFilter.formatoLongSql(_uuid)));
                }
                return Either.right(id_menuRolLista.size());
            }
            Integer filas_afectadas = this.usuarioDAO.ELIMINAR_ROL_USUARIO(null, FormatoQuerySqlFilter.formatoLongSql(null), FormatoQuerySqlFilter.formatoLongSql(_uuid));
            return Either.right(filas_afectadas);


        }
        catch (Exception e) {
            return Either.left(new TechnicalException(K.messages_error("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros")));
        }
    }

    public Either<IException, TokenDTO> iniarSesion(Long id_aplicacion, String dispositivo, AuthDTO authDTO) {
        try {
            TokenDTO tokenDTO = new TokenDTO(null, null, null, null, null);
            String usuario = authDTO.getUsuario();
            String clave = authDTO.getClave();
            if (FormatoQuerySqlFilter.formatoLIKESql(usuario) != null && FormatoQuerySqlFilter.formatoLIKESql(clave) != null) {
                List<Usuario> usuarioApps = this.usuarioDAO.VEFICAR_CREDENCIALES(id_aplicacion, usuario, clave);
                if (usuarioApps.size() == 1) {
                    Usuario usuarioModel = usuarioApps.get(0);
                    tokenDTO.setId_aplicacion(usuarioModel.getId_aplicacion());
                    Long toke = usuarioModel.get_uuid();
                    tokenDTO.setId_usuario(toke);
                    tokenDTO.setUsuario(usuarioModel.getUsuario());

                    List<Rol> rolList = new ArrayList<>();
                    List<Long> id_roles = new ArrayList<>();
                    Either<IException, List<Long>> idRolesPorUsuarioEither = obtenerIdRolesPorUsuario(null, null, usuarioModel.get_uuid());
                    if (idRolesPorUsuarioEither.isRight() && idRolesPorUsuarioEither.right().value().size() > 0) {
                        id_roles = idRolesPorUsuarioEither.right().value();
                        for (Long id_rol : id_roles) {
                            Either<IException, List<Rol>> obtenerRolesEither = this.rolBusiness.obtenerRoles(usuarioModel.getId_aplicacion(), id_rol, null, null);
                            if (obtenerRolesEither.isRight() && obtenerRolesEither.right().value().size() > 0) {
                                rolList.addAll(obtenerRolesEither.right().value());
                            }
                        }
                    }
                    List<Menus> menusList = new ArrayList<>();
                    if (rolList.size() > 0) {
                        tokenDTO.setRols(rolList);
                        for (Long id_rol : id_roles) {
                            Either<IException, List<Long>> menusPorRolEither = this.rolBusiness.obtenerIdMenusPorRol(id_rol, null);

                            if (menusPorRolEither.isRight() && menusPorRolEither.right().value().size() > 0) {
                                List<Long> id_menus = menusPorRolEither.right().value();
                                for (Long id_menu : id_menus) {
                                    Either<IException, List<Menus>> obtenerMenusEither = this.menuBusiness.obtenerMenus(id_menu, null, null, null, null, null, null, null, null, FormatoQuerySqlFilter.formatoStringSqlMAYUS(dispositivo));

                                    if (obtenerMenusEither.isRight() && obtenerMenusEither.right().value().size() > 0) {
                                        menusList.addAll(obtenerMenusEither.right().value());
                                        tokenDTO.setMenus(menusList);
                                    }
                                }
                            }
                        }
                    }
                    return Either.right(tokenDTO);
                }
                return Either.left(new BussinessException("El usuario y/o clave no es valido"));
            }

            return Either.left(new BussinessException("No se puede identificar el usuario con los parametros."));

        }
        catch (Exception e) {
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }
}