package com.sysware.ingenieria.resources;

import com.sysware.ingenieria.businesses.MenuBusiness;
import com.sysware.ingenieria.entities.Menus;
import com.sysware.ingenieria.representations.MenuDTO;
import com.sysware.ingenieria.utils.exeptions.ExceptionResponse;
import com.sysware.ingenieria.utils.exeptions.IException;
import fj.data.Either;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Path("/menus")
@Produces({"application/json"})
@Consumes({"application/json"})
public class MenuResource {
/* 22 */   public MenuResource(MenuBusiness menuBusiness) { this.menuBusiness = menuBusiness; }



/*    */   
/*    */   private MenuBusiness menuBusiness;


/*    */   
/*    */   @GET
/*    */   public Response obtenerListaMenus(@QueryParam("id_menu") Long id_menu, @QueryParam("id_aplicacion") Long id_aplicacion, @QueryParam("nombre") String nombre, @QueryParam("tipo") Integer tipo, @QueryParam("ruta") String ruta, @QueryParam("icono") String icono, @QueryParam("avatar") String avatar, @QueryParam("background") String background, @QueryParam("estado") Integer estado, @QueryParam("dispositivo") String dispositivo) {
/*    */     Response response;
/* 34 */     Either<IException, List<Menus>> allViewOffertsEither = this.menuBusiness.obtenerMenus(id_menu, id_aplicacion, nombre, tipo, ruta, icono, avatar, background, estado, dispositivo);

/*    */     
/* 37 */     if (allViewOffertsEither.isRight()) {
/* 38 */       System.out.println(((List)allViewOffertsEither.right().value()).size());
/* 39 */       response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
/*    */     } else {
/* 41 */       response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
/*    */     } 
/* 43 */     return response;
/*    */   }
/*    */   
/*    */   @POST
/*    */   public Response crearMenu(@QueryParam("id_usuario_app") Long id_usuario, MenuDTO menuDTO) {
/* 48 */     Either<IException, Long> offerEither = this.menuBusiness.crearMenu(id_usuario, menuDTO);
/* 49 */     if (offerEither.isRight()) {
/* 50 */       return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
/*    */     }
/* 52 */     return ExceptionResponse.createErrorResponse(offerEither);
/*    */   }


/*    */   
/*    */   @GET
/*    */   @Path("/rol")
/*    */   public Response obtenerListaMenusPorRol(@QueryParam("id_rol") Long id_rol, @QueryParam("id_menu") Long id_menu) {
/*    */     Response response;
/* 61 */     Either<IException, List<Menus>> allViewOffertsEither = this.menuBusiness.obtenerListaMenusPorRol(id_rol, id_menu);

/*    */     
/* 64 */     if (allViewOffertsEither.isRight()) {
/* 65 */       System.out.println(((List)allViewOffertsEither.right().value()).size());
/* 66 */       response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
/*    */     } else {
/* 68 */       response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
/*    */     } 
/* 70 */     return response;
/*    */   }
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\resources\MenuResource.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */