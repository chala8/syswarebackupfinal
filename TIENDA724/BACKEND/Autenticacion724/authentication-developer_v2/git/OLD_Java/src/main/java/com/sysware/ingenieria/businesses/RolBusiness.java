package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.RolDAO;
import com.sysware.ingenieria.entities.Rol;
import com.sysware.ingenieria.representations.RolDTO;
import com.sysware.ingenieria.utils.constans.K;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.ManagementException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter;
import fj.data.Either;
import java.util.ArrayList;
import java.util.List;

public class RolBusiness
{
    private RolDAO rolDAO;

    public RolBusiness(RolDAO rolDAO) { this.rolDAO = rolDAO; }


    public Either<IException, List<Rol>> obtenerRoles(Long id_applicacion, Long id_rol, String rol, String des) {
        try {
            List<Rol> aplicacionList = this.rolDAO.OBTENER_ROL(FormatoQuerySqlFilter.formatoLongSql(id_applicacion), FormatoQuerySqlFilter.formatoLongSql(id_rol), FormatoQuerySqlFilter.formatoLIKESql(des));
            return Either.right(aplicacionList);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }






    public Either<IException, List<Rol>> obtenerRolesByUUID(Long id_applicacion, Long id_rol, String rol, String des, Long id_usuario_rol, Long id_usuario, Long uuid) {
        try {
            List<Rol> aplicacionList = this.rolDAO.OBTENER_ROL_COMPLETO_BY_UUID(FormatoQuerySqlFilter.formatoLongSql(id_applicacion), FormatoQuerySqlFilter.formatoLongSql(id_rol), FormatoQuerySqlFilter.formatoLIKESql(rol),
                    FormatoQuerySqlFilter.formatoLongSql(id_usuario_rol),
                    FormatoQuerySqlFilter.formatoLongSql(id_usuario),
                    FormatoQuerySqlFilter.formatoLongSql(uuid));
            return Either.right(aplicacionList);
        }
        catch (Exception e) {
            e.printStackTrace();
            return Either.left(ManagementException.catchException(e));
        }
    }





    public Either<IException, Long> crearRol(RolDTO rolDTO) {
        List<String> msn = new ArrayList<>();

        try {
            if (FormatoQuerySqlFilter.formatoLongSql(rolDTO.getId_aplicacion()) == null) {
                msn.add("La Aplicacion creador de aplicacion no se reconoce");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            if (verificarApplicacionUsuario(rolDTO.getId_aplicacion(), rolDTO.getRol())) {
                msn.add("La aplicacion [" + rolDTO.getRol() + "] ya existe");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear Rol    ***********************************");
            if (rolDTO != null) {

                long id_rol = this.rolDAO.CREAR_ROL(FormatoQuerySqlFilter.formatoLongSql(rolDTO.getId_aplicacion()), rolDTO.getRol(), rolDTO.getDescripcion());
                return Either.right(this.rolDAO.getPkLast());
            }


            return Either.left(new BussinessException(K.messages_errors(msn)));
        } catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(K.messages_error("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros")));
        }
    }


    private boolean verificarApplicacionUsuario(Long id_aplicacion, String nombre) { return !(this.rolDAO.VERIFICAR_ROL(id_aplicacion, nombre).intValue() == 0); }


    public Either<IException, Long> crearRolMenu(Long id_rol, List<Long> id_menuLista) {
        List<String> msn = new ArrayList<>();
        List<Long> id_menuListaFinal = new ArrayList<>();

        try {
            if (FormatoQuerySqlFilter.formatoLongSql(id_rol) == null || id_menuLista.size() <= 0) {
                msn.add("Fatan datos  id rol y/o id menu");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            for (Long id_menu : id_menuLista) {
                if (verificarRolMenu(id_rol, id_menu)) {
                    msn.add("El menu " + id_menu + " ya existe");
                    continue;
                }
                id_menuListaFinal.add(id_menu);
            }

            if (id_menuListaFinal.size() == 0) {
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear Rol    ***********************************");

            long id_rol_menu = 0L;
            for (Long id_menu : id_menuListaFinal) {
                id_rol_menu = this.rolDAO.CREAR_ROL_MENU(FormatoQuerySqlFilter.formatoLongSql(id_rol), FormatoQuerySqlFilter.formatoLongSql(id_menu));
            }


            return Either.right(this.rolDAO.getPkLast_ROLES_MENU());

        }
        catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(K.messages_error("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros")));
        }
    }


    private boolean verificarRolMenu(Long id_rol, Long id_menu) { return !(this.rolDAO.VERIFICAR_ROL_MENU(id_rol, id_menu).intValue() == 0); }


    public Either<IException, Long> crearPermiso(Long id_rol, Long id_permiso_api) {
        List<String> msn = new ArrayList<>();

        try {
            if (FormatoQuerySqlFilter.formatoLongSql(id_rol) == null || FormatoQuerySqlFilter.formatoLongSql(id_permiso_api) == null) {
                msn.add("Faltan datos  id rol y/o id permiso api");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            if (verificarRolPermiso(id_rol, id_permiso_api)) {
                msn.add("El menu  ya existe");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            msn.add("OK");
            System.out.println("            Inicia a crear Permiso    ***********************************");


            long id_rol_menu = this.rolDAO.CREAR_ROL_PERMISO_API(FormatoQuerySqlFilter.formatoLongSql(id_rol), FormatoQuerySqlFilter.formatoLongSql(id_permiso_api));

            return Either.right(this.rolDAO.getPkLast_ROL_PERMISO_API());

        }
        catch (Exception e) {
            e.printStackTrace();
            return Either.left(new TechnicalException(K.messages_error("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros")));
        }
    }



    private boolean verificarRolPermiso(Long id_rol, Long id_permiso_api) { return !(this.rolDAO.VERIFICAR_ROL_PERMISO_API(id_rol, id_permiso_api).intValue() == 0); }



    public Either<IException, List<Long>> obtenerIdMenusPorRol(Long id_rol, Long id_menu) {
        List<Long> idMenus_Lista = new ArrayList<>();
        List<String> msn = new ArrayList<>();

        try {
            if (FormatoQuerySqlFilter.formatoLongSql(id_rol) == null && FormatoQuerySqlFilter.formatoLongSql(id_menu) == null) {
                msn.add("Faltan datos  id rol y id menu ");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            idMenus_Lista = this.rolDAO.OBTENER_MENU_ROL(FormatoQuerySqlFilter.formatoLongSql(id_menu), FormatoQuerySqlFilter.formatoLongSql(id_rol), FormatoQuerySqlFilter.formatoLongSql(id_menu));
            return Either.right(idMenus_Lista);
        }
        catch (Exception e) {
            return Either.left(new TechnicalException(K.messages_error("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros")));
        }
    }



    public Either<IException, Integer> borrarMenusPorRol(Long id_rol, List<Long> id_menuLista) {
        List<Integer> idMenus_ListaFinal = new ArrayList<>();
        List<String> msn = new ArrayList<>();

        try {
            if (FormatoQuerySqlFilter.formatoLongSql(id_rol) == null) {
                msn.add("Faltan datos  id rol  ");
                return Either.left(new BussinessException(K.messages_errors(msn)));
            }
            if (id_menuLista.size() > 0) {
                for (Long id_menu : id_menuLista) {
                    idMenus_ListaFinal.add(this.rolDAO.ELIMINAR_MENU_ROL(null, FormatoQuerySqlFilter.formatoLongSql(id_rol), FormatoQuerySqlFilter.formatoLongSql(id_menu)));
                }
                return Either.right(Integer.valueOf(id_menuLista.size()));
            }
            msn.add("Faltan datos  id menu ");
            return Either.left(new BussinessException(K.messages_errors(msn)));


        }
        catch (Exception e) {
            return Either.left(new TechnicalException(K.messages_error("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros")));
        }
    }
}