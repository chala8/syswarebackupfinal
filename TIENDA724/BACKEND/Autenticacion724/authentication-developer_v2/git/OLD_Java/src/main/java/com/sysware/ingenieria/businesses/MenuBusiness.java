package com.sysware.ingenieria.businesses;

import com.sysware.ingenieria.DAOs.MenuDAO;
import com.sysware.ingenieria.entities.Menus;
import com.sysware.ingenieria.representations.MenuDTO;
import com.sysware.ingenieria.utils.constans.K;
import com.sysware.ingenieria.utils.exeptions.BussinessException;
import com.sysware.ingenieria.utils.exeptions.IException;
import com.sysware.ingenieria.utils.exeptions.ManagementException;
import com.sysware.ingenieria.utils.exeptions.TechnicalException;
import com.sysware.ingenieria.utils.filters.FormatoQuerySqlFilter;
import fj.data.Either;
import java.util.ArrayList;
import java.util.List;


public class MenuBusiness
{
   private MenuDAO menuDAO;
   
   public MenuBusiness(MenuDAO menuDAO) { this.menuDAO = menuDAO; }

   public Either<IException, List<Menus>> obtenerMenus(Long id_menu, Long id_aplicacion, String nombre, Integer tipo, String ruta, String icono, String avatar, String background, Integer estado, String dispositivo) {
     try {
       List<Menus> aplicacionList = this.menuDAO.OBTENER_MENUS(id_menu, id_aplicacion, nombre, tipo, ruta, icono, avatar, background, estado, FormatoQuerySqlFilter.formatoStringSqlMAYUS(dispositivo));
       return Either.right(aplicacionList);
     }
     catch (Exception e) {
       e.printStackTrace();
       return Either.left(ManagementException.catchException(e));
     } 
   }
   
   public Either<IException, Long> crearMenu(Long id_usuario, MenuDTO menuDTO) {
     List<String> msn = new ArrayList<>();
     
     try {
       if (FormatoQuerySqlFilter.formatoLongSql(menuDTO.getId_aplicacion()) == null) {
         msn.add("La Aplicacion no se reconoce, para crearle un Menu");
         return Either.left(new BussinessException(K.messages_errors(msn)));
       } 
       if (verificarApplicacionUsuario(menuDTO.getId_aplicacion(), menuDTO.getNombre())) {
         msn.add("El menu [" + menuDTO.getNombre() + "] ya existe");
         return Either.left(new BussinessException(K.messages_errors(msn)));
       } 
       msn.add("OK");
       System.out.println("            Inicia a crear MENU    ***********************************");
       if (menuDTO != null) {

         long id_rol = this.menuDAO.CREAR_MENU(FormatoQuerySqlFilter.formatoLongSql(id_usuario), menuDTO);

         if (id_rol > 0L)
         {
           return Either.right(this.menuDAO.getPkLast());
         }


         msn.add("No se reconoce formato del Menu , esta mal formado");
       }
       return Either.left(new BussinessException(K.messages_errors(msn)));
     } catch (Exception e) {
       e.printStackTrace();
       return Either.left(new TechnicalException(K.messages_error("Se presentó un error en el Servidor. Por favor intente más tarde o Comunícate con nosotros")));
     } 
   }

   private boolean verificarApplicacionUsuario(Long id_aplicacion, String nombre) { return !(this.menuDAO.VERIFICAR_MENU(id_aplicacion, nombre).intValue() == 0); }
   
   public Either<IException, List<Menus>> obtenerListaMenusPorRol(Long id_rol, Long id_menu) {
     try {
       return Either.right(this.menuDAO.OBTENER_MENUS(FormatoQuerySqlFilter.formatoLongSql(id_menu), null, null, null, null, null, null, null, null, null));
     } catch (Exception e) {
       e.printStackTrace();
       return Either.left(ManagementException.catchException(e));
     } 
   }
}