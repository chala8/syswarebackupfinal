package com.sysware.ingenieria.entities;

import java.security.Principal;
import java.util.List;



public class Usuario implements Principal
{
/*    */   private Long _uuid;
/*    */   private Long id_aplicacion;
/*    */   private String usuario;
/*    */   private String clave;
/*    */   private List<Rol> roles;
/*    */   
/*    */   public Usuario(Long _uuid, Long id_aplicacion, String usuario, String clave, List<Rol> roles) {
/* 16 */     this._uuid = _uuid;
/* 17 */     this.id_aplicacion = id_aplicacion;
/* 18 */     this.usuario = usuario;
/* 19 */     this.clave = clave;
/* 20 */     this.roles = roles;
/*    */   }

/*    */   
/* 24 */   public List<Rol> getRoles() { return this.roles; }


/*    */   
/* 28 */   public void setRoles(List<Rol> roles) { this.roles = roles; }


/*    */   
/* 32 */   public Long get_uuid() { return this._uuid; }


/*    */   
/* 36 */   public void set_uuid(Long _uuid) { this._uuid = _uuid; }


/*    */   
/* 40 */   public Long getId_aplicacion() { return this.id_aplicacion; }


/*    */   
/* 44 */   public void setId_aplicacion(Long id_aplicacion) { this.id_aplicacion = id_aplicacion; }


/*    */   
/* 48 */   public String getUsuario() { return this.usuario; }


/*    */   
/* 52 */   public void setUsuario(String usuario) { this.usuario = usuario; }


/*    */   
/* 56 */   public String getClave() { return this.clave; }


/*    */   
/* 60 */   public void setClave(String clave) { this.clave = clave; }

    @Override
    public String getName() {
        return null;
    }
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\entities\Usuario.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */