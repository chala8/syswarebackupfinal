package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Usuario;
import com.sysware.ingenieria.mappers.UsuarioMapper;
import java.util.List;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

@RegisterMapper({UsuarioMapper.class})
public interface UsuarioDAO {
  @SqlQuery("SELECT ID_USUARIO FROM USUARIOS WHERE ID_USUARIO IN (SELECT MAX( ID_USUARIO ) FROM USUARIO)\n ")
  Long getPkLast();
  
  @SqlQuery(" SELECT *  FROM  USUARIOS us_ap    WHERE  (us_ap.UUID=:id_usuario or :id_usuario IS NULL ) AND         (us_ap.ID_APLICACION=:id_aplicacion or :id_aplicacion IS NULL ) AND         (us_ap.USUARIO=:usuario or :usuario IS NULL ) AND         (us_ap.CLAVE=:clave or :clave IS NULL ) ")
  List<Usuario> OBTENER_USUARIO(@Bind("id_aplicacion") Long paramLong1, @Bind("id_usuario") Long paramLong2, @Bind("usuario") String paramString1, @Bind("clave") String paramString2);
  
  @SqlUpdate("INSERT INTO usuarios ( id_aplicacion,uuid,usuario, clave) VALUES (:id_aplicacion,:_uuid,:usuario, :clave)")
  long CREAR_USUARIOAPP(@Bind("id_aplicacion") Long paramLong, @Bind("_uuid") Integer paramInteger, @Bind("usuario") String paramString1, @Bind("clave") String paramString2);
  
  @SqlQuery("SELECT *  FROM usuarios  us_ap   WHERE            (us_ap.id_aplicacion=:id_aplicacion) AND          (us_ap.usuario=:usuario) AND          (us_ap.clave=:clave)")
  List<Usuario> VEFICAR_CREDENCIALES(@Bind("id_aplicacion") Long paramLong, @Bind("usuario") String paramString1, @Bind("clave") String paramString2);
  
  @SqlQuery("SELECT * FROM usuarios  us   WHERE           (us.uuid!=:_uuid) AND          (us.id_aplicacion=:id_aplicacion) AND          (us.usuario=:usuario )")
  List<Usuario> OBTENER_USUARIO_POR_NOMBRE(@Bind("id_aplicacion") Long paramLong, @Bind("_uuid") Integer paramInteger, @Bind("usuario") String paramString);
  
  @SqlUpdate("UPDATE usuarios SET usuario=:usuario, clave=:clave WHERE  (id_aplicacion=:id_aplicacion AND  uuid=:_uuid) ")
  int EDITAR_USUARIO(@Bind("id_aplicacion") Long paramLong, @Bind("_uuid") Integer paramInteger, @Bind("usuario") String paramString1, @Bind("clave") String paramString2);
  
  @SqlQuery("SELECT ID_USUARIO_ROL FROM USUARIOS_ROLES WHERE ID_USUARIO_ROL IN (SELECT MAX( ID_USUARIO_ROL ) FROM USUARIOS_ROLES) ")
  Long getPkLastUSUARIOS_ROLES();
  
  @SqlUpdate("INSERT INTO usuarios_roles ( id_rol,id_usuario,uuid ) VALUES (:id_rol,:id_usuario,:_uuid)")
  long CREAR_USUARIO_ROL(@Bind("id_usuario") Long paramLong1, @Bind("_uuid") Integer paramInteger, @Bind("id_rol") Long paramLong2);
  
  @SqlUpdate(" UPDATE usuarios_roles SET  id_rol=:id_rol, uuid=:_uuid     WHERE         (uuid=:_uuid OR :_uuid IS NULL ) AND         (id_rol=:id_rol_old OR :id_rol_old IS NULL )")
  long EDITAR_USUARIO_ROL(@Bind("id_rol") Long paramLong1, @Bind("id_rol_old") Long paramLong2, @Bind("_uuid") Integer paramInteger);
  
  @SqlQuery("SELECT count(id_usuario_rol) FROM usuarios_roles   WHERE  ((usuarios_roles.id_usuario=:id_usuario or :id_usuario IS NULL ) AND  usuarios_roles.uuid=:_uuid AND  usuarios_roles.id_rol=:id_rol)")
  Integer VERIFICAR_USUARIO_ROL(@Bind("id_usuario") Long paramLong1, @Bind("_uuid") Integer paramInteger, @Bind("id_rol") Long paramLong2);
  
  @SqlQuery("SELECT (id_rol) FROM  usuarios_roles  us_rol WHERE (us_rol.id_usuario_rol=:id_usuario_rol or :id_usuario_rol IS NULL ) AND (us_rol.id_rol=:id_rol or :id_rol IS NULL ) AND (us_rol.uuid=:_uuid or :_uuid IS NULL ) ")
  List<Long> OBTENER_ID_ROLES_POR_USUARIO(@Bind("id_usuario_rol") Long paramLong1, @Bind("id_rol") Long paramLong2, @Bind("_uuid") Long paramLong3);
  
  @SqlUpdate("DELETE  FROM  usuarios_roles WHERE (id_usuario_rol=:id_usuario_rol or :id_usuario_rol IS NULL ) AND (id_rol=:id_rol or :id_rol IS NULL ) AND (uuid=:_uuid or :_uuid IS NULL )")
  Integer ELIMINAR_ROL_USUARIO(@Bind("id_usuario_rol") Long paramLong1, @Bind("id_rol") Long paramLong2, @Bind("_uuid") Long paramLong3);
  
  @SqlUpdate("DELETE  FROM  usuarios WHERE (id_usuario=:id_usuario or :id_usuario IS NULL ) AND (uuid=:_uuid or :_uuid IS NULL )")
  int ELIMINAR_USUARIO(@Bind("id_usuario") Long paramLong1, @Bind("_uuid") Long paramLong2);
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\DAOs\UsuarioDAO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */