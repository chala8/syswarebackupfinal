package com.sysware.ingenieria.mappers;

import com.sysware.ingenieria.entities.Menus;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;



/*    */ 
/*    */ public class MenuMapper
/*    */   implements ResultSetMapper<Menus>
/*    */ {
/*    */   public Menus map(int index, ResultSet resultSet, StatementContext ctx) throws SQLException {
/* 16 */     return new Menus(
/* 17 */         Long.valueOf(resultSet.getLong("id_menu")), 
/* 18 */         Long.valueOf(resultSet.getLong("id_aplicacion")), resultSet
/* 19 */         .getString("nombre"), 
/* 20 */         Integer.valueOf(resultSet.getInt("tipo")), resultSet
/* 21 */         .getString("ruta"), resultSet
/* 22 */         .getString("icono"), resultSet
/* 23 */         .getString("avatar"), resultSet
/* 24 */         .getString("background"), 
/* 25 */         Integer.valueOf(resultSet.getInt("estado")), resultSet
/* 26 */         .getString("dispositivo"), 
/* 27 */         Long.valueOf(resultSet.getLong("ID_MENU_PADRE")));
/*    */   }
/*    */ }


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\mappers\MenuMapper.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */