package com.sysware.ingenieria.DAOs;

import com.sysware.ingenieria.entities.Rol;
import com.sysware.ingenieria.mappers.RolMapper;
import java.util.List;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

@RegisterMapper({RolMapper.class})
public interface RolDAO {
  @SqlQuery("SELECT ID_ROL FROM ROLES WHERE ID_ROL IN (SELECT MAX( ID_ROL ) FROM ROLES)\n ")
  Long getPkLast();
  
  @SqlUpdate("INSERT INTO roles ( id_aplicacion, rol,descripcion) VALUES (:id_aplicacion,:rol,:descripcion) ")
  long CREAR_ROL(@Bind("id_aplicacion") Long paramLong, @Bind("rol") String paramString1, @Bind("descripcion") String paramString2);
  
  @SqlQuery("SELECT count(id_aplicacion) FROM roles  rol   WHERE  (rol.id_aplicacion=:id_aplicacion AND  rol.rol=:rol)")
  Integer VERIFICAR_ROL(@Bind("id_aplicacion") Long paramLong, @Bind("rol") String paramString);
  
  @SqlQuery("SELECT * FROM  roles  rol WHERE (rol.id_rol=:id_rol or :id_rol IS NULL ) AND (rol.id_aplicacion=:id_aplicacion or :id_aplicacion IS NULL ) AND (rol.rol LIKE  :rol or :rol IS NULL )")
  List<Rol> OBTENER_ROL(@Bind("id_aplicacion") Long paramLong1, @Bind("id_rol") Long paramLong2, @Bind("rol") String paramString);
  
  @SqlQuery("SELECT * FROM roles  rol WHERE   (rol.id_rol IN  (SELECT id_rol FROM usuarios_roles  us_rol   WHERE  (us_rol.id_usuario_rol=:id_usuario_rol OR :id_usuario_rol IS NULL ) AND          (us_rol.id_usuario=:id_usuario OR :id_usuario IS NULL ) AND          (us_rol.uuid LIKE :uuid OR :uuid IS NULL ) AND          (us_rol.id_rol LIKE :id_rol OR :id_rol IS NULL )   )  ) AND (rol.id_aplicacion=:id_aplicacion or :id_aplicacion IS NULL ) AND (rol.rol LIKE  :rol or :rol IS NULL) ")
  List<Rol> OBTENER_ROL_COMPLETO_BY_UUID(@Bind("id_aplicacion") Long paramLong1, @Bind("id_rol") Long paramLong2, @Bind("rol") String paramString, @Bind("id_usuario_rol") Long paramLong3, @Bind("id_usuario") Long paramLong4, @Bind("uuid") Long paramLong5);
  
  @SqlQuery("SELECT ID_ROL_MENU FROM ROLES_MENU WHERE ID_ROL_MENU IN (SELECT MAX( ID_ROL_MENU ) FROM ROLES_MENU) ")
  Long getPkLast_ROLES_MENU();
  
  @SqlUpdate("INSERT INTO roles_menu ( id_rol,id_menu ) VALUES (:id_rol,:id_menu)")
  long CREAR_ROL_MENU(@Bind("id_rol") Long paramLong1, @Bind("id_menu") Long paramLong2);
  
  @SqlQuery("SELECT count(id_rol_menu) FROM roles_menu   WHERE  (roles_menu.id_rol=:id_rol AND  roles_menu.id_menu=:id_menu)")
  Integer VERIFICAR_ROL_MENU(@Bind("id_rol") Long paramLong1, @Bind("id_menu") Long paramLong2);
  
  @SqlQuery("SELECT count(id_rol_permiso) FROM roles_permisos_api  rol_pe_API WHERE  (rol_pe_API.id_rol=:id_rol AND  rol_pe_API.id_permiso_api=:id_permiso_api)")
  Integer VERIFICAR_ROL_PERMISO_API(@Bind("id_rol") Long paramLong1, @Bind("id_permiso_api") Long paramLong2);
  
  @SqlQuery("SELECT ID_ROL_PERMISO FROM ROLES_PERMISOS_API WHERE ID_ROL_PERMISO IN (SELECT MAX( ID_ROL_PERMISO ) FROM ROLES_PERMISOS_API) ")
  Long getPkLast_ROL_PERMISO_API();
  
  @SqlUpdate("INSERT INTO roles_permisos_api ( id_rol,id_permiso_api ) VALUES (:id_rol,:id_permiso_api)")
  long CREAR_ROL_PERMISO_API(@Bind("id_rol") Long paramLong1, @Bind("id_permiso_api") Long paramLong2);
  
  @SqlQuery("SELECT id_menu FROM  roles_menu rol_menu WHERE (rol_menu.id_rol_menu=:id_rol_menu or :id_rol_menu IS NULL ) AND (rol_menu.id_rol=:id_rol or :id_rol IS NULL ) AND (rol_menu.id_menu=:id_menu or :id_menu IS NULL ) ")
  List<Long> OBTENER_MENU_ROL(@Bind("id_rol_menu") Long paramLong1, @Bind("id_rol") Long paramLong2, @Bind("id_menu") Long paramLong3);
  
  @SqlUpdate("DELETE  FROM  roles_menu WHERE (id_rol_menu=:id_rol_menu or :id_rol_menu IS NULL ) AND (id_rol=:id_rol or :id_rol IS NULL ) AND (id_menu=:id_menu or :id_menu IS NULL )")
  Integer ELIMINAR_MENU_ROL(@Bind("id_rol_menu") Long paramLong1, @Bind("id_rol") Long paramLong2, @Bind("id_menu") Long paramLong3);
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\DAOs\RolDAO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */