package com.sysware.ingenieria.utils.security;

import java.util.UUID;





public class TokenGenerator
{
/* 11 */   public static String uuid() { return UUID.randomUUID().toString().replaceAll("-", ""); }


/*    */   
/*    */   public static String tokenGenerate(boolean isDached) {
/* 16 */     String codeTrial = uuid();
/* 17 */     String TOKEN = "";
/* 18 */     int codeLength = codeTrial.length();
/* 19 */     for (int i = 1; i < codeLength; i++) {
/* 20 */       TOKEN = TOKEN + codeTrial.charAt(i);
/*    */       
/* 22 */       if (i == 6 && i < codeLength && isDached) {
/* 23 */         TOKEN = TOKEN + ":";
/*    */       }
/* 25 */       else if (i > 8 && i % 9 == 0 && i < codeLength && isDached) {
/* 26 */         TOKEN = TOKEN + "-";
/*    */       } 
/*    */     } 

/*    */     
/* 31 */     return TOKEN.toUpperCase();
/*    */   }
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieri\\utils\security\TokenGenerator.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */