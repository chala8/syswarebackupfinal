/*     */ package com.sysware.ingenieria.representations;
/*     */ 
/*     */ import com.fasterxml.jackson.annotation.JsonCreator;
/*     */ import com.fasterxml.jackson.annotation.JsonProperty;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class MenuDTO
/*     */ {
/*     */   private Long id_aplicacion;
/*     */   private String nombre;
/*     */   private Integer tipo;
/*     */   private String ruta;
/*     */   private String icono;
/*     */   private String avatar;
/*     */   private String background;
/*     */   private Integer estado;
/*     */   private String dispositivo;
/*     */   private Long id_menu_padre;
/*     */   
/*     */   @JsonCreator
/*     */   public MenuDTO(@JsonProperty("id_aplicacion") Long id_aplicacion, @JsonProperty("nombre") String nombre, @JsonProperty("tipo") Integer tipo, @JsonProperty("ruta") String ruta, @JsonProperty("icono") String icono, @JsonProperty("avatar") String avatar, @JsonProperty("background") String background, @JsonProperty("estado") Integer estado, @JsonProperty("id_menu_padre") Long id_menu_padre) {
/*  27 */     this.id_aplicacion = id_aplicacion;
/*  28 */     this.nombre = nombre;
/*  29 */     this.tipo = tipo;
/*  30 */     this.ruta = ruta;
/*  31 */     this.icono = icono;
/*  32 */     this.avatar = avatar;
/*  33 */     this.background = background;
/*  34 */     this.estado = estado;
/*  35 */     this.id_menu_padre = id_menu_padre;
/*     */   }
/*     */ 
/*     */   
/*  39 */   public Long getId_menu_padre() { return this.id_menu_padre; }
/*     */ 
/*     */ 
/*     */   
/*  43 */   public void setId_menu_padre(Long id_menu_padre) { this.id_menu_padre = id_menu_padre; }
/*     */ 
/*     */ 
/*     */   
/*  47 */   public String getDispositivo() { return this.dispositivo; }
/*     */ 
/*     */ 
/*     */   
/*  51 */   public void setDispositivo(String dispositivo) { this.dispositivo = dispositivo; }
/*     */ 
/*     */ 
/*     */   
/*  55 */   public Long getId_aplicacion() { return this.id_aplicacion; }
/*     */ 
/*     */ 
/*     */   
/*  59 */   public void setId_aplicacion(Long id_aplicacion) { this.id_aplicacion = id_aplicacion; }
/*     */ 
/*     */ 
/*     */   
/*  63 */   public String getNombre() { return this.nombre; }
/*     */ 
/*     */ 
/*     */   
/*  67 */   public void setNombre(String nombre) { this.nombre = nombre; }
/*     */ 
/*     */ 
/*     */   
/*  71 */   public Integer getTipo() { return this.tipo; }
/*     */ 
/*     */ 
/*     */   
/*  75 */   public void setTipo(Integer tipo) { this.tipo = tipo; }
/*     */ 
/*     */ 
/*     */   
/*  79 */   public String getRuta() { return this.ruta; }
/*     */ 
/*     */ 
/*     */   
/*  83 */   public void setRuta(String ruta) { this.ruta = ruta; }
/*     */ 
/*     */ 
/*     */   
/*  87 */   public String getIcono() { return this.icono; }
/*     */ 
/*     */ 
/*     */   
/*  91 */   public void setIcono(String icono) { this.icono = icono; }
/*     */ 
/*     */ 
/*     */   
/*  95 */   public String getAvatar() { return this.avatar; }
/*     */ 
/*     */ 
/*     */   
/*  99 */   public void setAvatar(String avatar) { this.avatar = avatar; }
/*     */ 
/*     */ 
/*     */   
/* 103 */   public String getBackground() { return this.background; }
/*     */ 
/*     */ 
/*     */   
/* 107 */   public void setBackground(String background) { this.background = background; }
/*     */ 
/*     */ 
/*     */   
/* 111 */   public Integer getEstado() { return this.estado; }
/*     */ 
/*     */ 
/*     */   
/* 115 */   public void setEstado(Integer estado) { this.estado = estado; }
/*     */ }


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\representations\MenuDTO.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */