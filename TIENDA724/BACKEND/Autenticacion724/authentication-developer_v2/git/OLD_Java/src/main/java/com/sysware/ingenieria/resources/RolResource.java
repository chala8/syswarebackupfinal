/*     */ package com.sysware.ingenieria.resources;
/*     */ import com.sysware.ingenieria.businesses.RolBusiness;
/*     */ import com.sysware.ingenieria.entities.Rol;
/*     */ import com.sysware.ingenieria.representations.RolDTO;
/*     */ import com.sysware.ingenieria.utils.exeptions.ExceptionResponse;
/*     */ import com.sysware.ingenieria.utils.exeptions.IException;
/*     */ import fj.data.Either;
/*     */ import java.util.List;
/*     */ import javax.ws.rs.Consumes;
/*     */ import javax.ws.rs.GET;
/*     */ import javax.ws.rs.POST;
/*     */ import javax.ws.rs.PUT;
/*     */ import javax.ws.rs.Path;
/*     */ import javax.ws.rs.Produces;
/*     */ import javax.ws.rs.QueryParam;
/*     */ import javax.ws.rs.core.Response;
/*     */ 
/*     */ @Path("/roles")
/*     */ @Produces({"application/json"})
/*     */ @Consumes({"application/json"})
/*     */ public class RolResource {
/*  22 */   public RolResource(RolBusiness rolBusiness) { this.rolBusiness = rolBusiness; }
/*     */ 
/*     */ 
/*     */   
/*     */   private RolBusiness rolBusiness;
/*     */ 
/*     */   
/*     */   @GET
/*     */   public Response obtenerListaRoles(@QueryParam("id_aplicacion") Long id_applicacion, @QueryParam("id_rol") Long id_rol, @QueryParam("rol") String rol, @QueryParam("descripcion") String descripcion) {
/*     */     Response response;
/*  32 */     Either<IException, List<Rol>> allViewOffertsEither = this.rolBusiness.obtenerRoles(id_applicacion, id_rol, rol, descripcion);
/*     */ 
/*     */     
/*  35 */     if (allViewOffertsEither.isRight()) {
/*  36 */       System.out.println(((List)allViewOffertsEither.right().value()).size());
/*  37 */       response = Response.status(Response.Status.OK).entity(allViewOffertsEither.right().value()).build();
/*     */     } else {
/*  39 */       response = ExceptionResponse.createErrorResponse(allViewOffertsEither);
/*     */     } 
/*  41 */     return response;
/*     */   }
/*     */   
/*     */   @POST
/*     */   public Response crearRol(RolDTO rolDTO) {
/*  46 */     Either<IException, Long> offerEither = this.rolBusiness.crearRol(rolDTO);
/*  47 */     if (offerEither.isRight()) {
/*  48 */       return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
/*     */     }
/*  50 */     return ExceptionResponse.createErrorResponse(offerEither);
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   @POST
/*     */   @Path("menu")
/*     */   public Response crearRolMenu(@QueryParam("id_rol") Long id_rol, List<Long> id_menuRolLista) {
/*  58 */     Either<IException, Long> offerEither = this.rolBusiness.crearRolMenu(id_rol, id_menuRolLista);
/*     */     
/*  60 */     if (offerEither.isRight()) {
/*  61 */       return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
/*     */     }
/*  63 */     return ExceptionResponse.createErrorResponse(offerEither);
/*     */   }
/*     */ 
/*     */ 
/*     */   
/*     */   @GET
/*     */   @Path("menu")
/*     */   public Response obtenerMenusPorRol(@QueryParam("id_rol") Long id_rol, @QueryParam("id_menu") Long id_menu) {
/*  71 */     Either<IException, List<Long>> offerEither = this.rolBusiness.obtenerIdMenusPorRol(id_rol, id_menu);
/*     */     
/*  73 */     if (offerEither.isRight()) {
/*  74 */       return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
/*     */     }
/*     */     
/*  77 */     return ExceptionResponse.createErrorResponse(offerEither);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   @PUT
/*     */   @Path("menu")
/*     */   public Response borrarMenusPorRol(@QueryParam("id_rol") Long id_rol, List<Long> id_menuRolLista) {
/*  87 */     Either<IException, Integer> offerEither = this.rolBusiness.borrarMenusPorRol(id_rol, id_menuRolLista);
/*     */     
/*  89 */     if (offerEither.isRight()) {
/*  90 */       return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
/*     */     }
/*  92 */     return ExceptionResponse.createErrorResponse(offerEither);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   @POST
/*     */   @Path("api")
/*     */   public Response crearPermiso(@QueryParam("id_rol") Long id_rol, @QueryParam("id_permiso_api") Long id_permiso_api) {
/* 102 */     Either<IException, Long> offerEither = this.rolBusiness.crearPermiso(id_rol, id_permiso_api);
/* 103 */     if (offerEither.isRight()) {
/* 104 */       return Response.status(Response.Status.OK).entity(offerEither.right().value()).build();
/*     */     }
/* 106 */     return ExceptionResponse.createErrorResponse(offerEither);
/*     */   }
/*     */ }


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieria\resources\RolResource.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */