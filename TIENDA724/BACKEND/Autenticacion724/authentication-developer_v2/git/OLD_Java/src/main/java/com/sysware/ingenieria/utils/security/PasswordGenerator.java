package com.sysware.ingenieria.utils.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;




public class PasswordGenerator
{
/*    */   public static byte[] gerarHash(String frase, String algoritmo) {
/*    */     try {
/* 13 */       MessageDigest md = MessageDigest.getInstance(algoritmo);
/* 14 */       md.update(frase.getBytes());
/* 15 */       return md.digest();
/* 16 */     } catch (NoSuchAlgorithmException e) {
/* 17 */       return null;
/*    */     } 
/*    */   }
/*    */   
/*    */   public static String stringHexa(byte[] bytes) {
/* 22 */     StringBuilder s = new StringBuilder();
/* 23 */     for (int i = 0; i < bytes.length; i++) {
/* 24 */       int parteAlta = (bytes[i] >> 4 & 0xF) << 4;
/* 25 */       int parteBaixa = bytes[i] & 0xF;
/* 26 */       if (parteAlta == 0) s.append('-'); 
/* 27 */       s.append(Integer.toHexString(parteAlta | parteBaixa));
/*    */     } 
/* 29 */     return s.toString();
/*    */   }

/*    */   
/* 33 */   public static String passwordGenerator(String paramters, String algorithm) { return stringHexa(gerarHash(paramters, algorithm)); }
}


/* Location:              C:\Users\LENOVO\Desktop\Auth2.0\auth-app-1.0-version.jar!\com\sysware\ingenieri\\utils\security\PasswordGenerator.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.2
 */