// lib/server.ts
import app from "./app";
const PORT = process.env.PORT || 8449;

//app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));

var fs = require('fs');
var https = require('https');

var options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem'),
}
https.createServer(options, app).listen(PORT,function () {
    console.log('Auth corriendo en HTTPS:'+PORT)
});
