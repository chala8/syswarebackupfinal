import {
    Column, Connection,
    Entity,
    JoinColumn,
    JoinTable,
    ManyToMany, ManyToOne, OneToMany,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn, Unique
} from "typeorm";
import {Rol} from "./Rol";
import {Application} from "./Application";
import {FCMtokens} from "./FCMtokens";

@Entity()
@Unique(["id_third", "id_applicacion"])
@Unique(["usuario", "id_applicacion"])
export class Usuario {
    @PrimaryColumn()
        // @ts-ignore
    uuid: number;

    @Column({
        type:'number',
        nullable:false,
    })
    id_usuario: number;

    @Column({
        type:'number',
        nullable:false
    })
    id_third: number;

    @Column({
        type:'varchar2',
        nullable:false,
        length:200
    })
    usuario: string;

    @Column({
        type:'varchar2',
        nullable:false,
        length:600
    })
    clave: string;

    @Column({
        type:Date,
        nullable:true
    })
    createDate!: Date;

    @OneToMany(type => FCMtokens, token => token.Usuario)
    FCMtokens!: FCMtokens[];

    @ManyToMany(type => Rol, rol => rol.usuarios)
    @JoinTable()
    roles: Rol[];

    @ManyToOne(type => Application)
    id_applicacion: Application;

    constructor(id_person: number, usuario: string,clave: string,roles: Rol[],id_applicacion: Application,id_third:number) {
        this.id_usuario = id_person;
        this.usuario = usuario;
        this.clave = clave;
        this.roles = roles;
        this.id_applicacion = id_applicacion;
        this.id_third = id_third;
        // @ts-ignore
        this.uuid = null;
    }

    public static async CheckIFTriggerOk(db : Connection){
        let Conteo = await db.manager.query("SELECT count(*) FROM user_triggers where trigger_name = '"+this.name.toLocaleUpperCase()+"_TRIG'");
        Conteo = Conteo[0]["COUNT(*)"];
        if(Conteo == 0){//No existe, se crea
            let ResultadoCrear = await db.manager.query(
                "create trigger "+this.name+"_Trig\n" +
                "    before insert\n" +
                "    on \""+this.name.toLocaleLowerCase()+"\"\n" +
                "    for each row\n" +
                "begin\n" +
                "    if :NEW.\"uuid\" is null then\n" +
                "      select \"USUARIOS_SEQ\".nextval into :NEW.\"uuid\" from dual;\n" +
                "    end if;\n" +
                "  end;"
            );
            console.log("Trigger Creado: '"+this.name+"_Trig'");
            console.log(ResultadoCrear);

        }else{
            console.log("Trigger '"+this.name+"_Trig' al dia");
        }
    }
}

export interface UsuarioInterface {
    uuid: number;
    id_usuario: number;
    usuario: string;
    clave: string;
    roles: number[];
    id_applicacion: number;
}