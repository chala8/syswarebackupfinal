import {
    Column, Connection,
    Entity,
    PrimaryColumn,
} from "typeorm";

@Entity()
export class Application {
    @PrimaryColumn()
        // @ts-ignore
    id: number;

    @Column({
        type:'varchar2',
        nullable:false,
        length:200
    })
    name: string;

    @Column({
        type:'varchar2',
        nullable:false,
        length:200
    })
    Description: string;

    @Column({
        type:'varchar2',
        nullable:false,
        length:50
    })
    Version: string;

    @Column({
        type:Date,
        nullable:false
    })
    Creation_Date: Date;

    @Column({
        type:Date,
        nullable:false
    })
    Update_Date: Date;

    @Column({
        type:'varchar2',
        nullable:false,
        length:600
    })
    Key_Application: string;

    @Column({
        type:'varchar2',
        nullable:false,
        length:600
    })
    Key_Server: string;

    //constructor(props);
    constructor(name: string, Description: string, Version: string, Creation_Date: Date, Update_Date: Date, Key_Application: string, Key_Server: string) {
        this.name = name;
        this.Description = Description;
        this.Version = Version;
        this.Creation_Date = Creation_Date;
        this.Update_Date = Update_Date;
        this.Key_Application = Key_Application;
        this.Key_Server = Key_Server;
    }

    public SetFromInterface(inter:ApplicationInterface){
        if(inter.name != null){this.name = inter.name;}
        if(inter.Description != null){this.Description = inter.Description;}
        if(inter.Version != null){this.Version = inter.Version;}
        if(inter.Creation_Date != null){this.Creation_Date = inter.Creation_Date;}
        if(inter.Update_Date != null){this.Update_Date = inter.Update_Date;}
        if(inter.Key_Application != null){this.Key_Application = inter.Key_Application;}
        if(inter.Key_Server != null){this.Key_Server = inter.Key_Server;}
    }

    public static async CheckIFTriggerOk(db : Connection){
        let Conteo = await db.manager.query("SELECT count(*) FROM user_triggers where trigger_name = '"+this.name.toLocaleUpperCase()+"_TRIG'");
        Conteo = Conteo[0]["COUNT(*)"];
        if(Conteo == 0){//No existe, se crea
            let ResultadoCrear = await db.manager.query(
                "create trigger "+this.name+"_Trig\n" +
                "    before insert\n" +
                "    on \""+this.name.toLocaleLowerCase()+"\"\n" +
                "    for each row\n" +
                "begin\n" +
                "    if :NEW.\"id\" is null then\n" +
                "      select \"APPLICATION_SEQ\".nextval into :NEW.\"id\" from dual;\n" +
                "    end if;\n" +
                "  end;"
            );
            console.log("Trigger Creado: '"+this.name+"_Trig'");
            console.log(ResultadoCrear);

        }else{
            console.log("Trigger '"+this.name+"_Trig' al dia");
        }
    }
}

export interface ApplicationInterface {
    name: string;
    Description: string;
    Version: string;
    Creation_Date: Date;
    Update_Date: Date;
    Key_Application: string;
    Key_Server: string;
}
