import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Usuario} from "./Usuario";

@Entity()
export class PasswordRecoveryToken {
    // CREATE EXTENSION "pgcrypto";
    @PrimaryGeneratedColumn('uuid')
        // @ts-ignore
    id: string;

    @Column({type:'number',nullable:false,default:0})
    used: number;

    @Column({type:Date,nullable:false})
    createDate: Date;

    @Column({type:Date,nullable:false})
    expirationDate: Date;

    @Column({type:Date,nullable:true})
    useDate: Date | null;

    @ManyToOne(type => Usuario)//Ejemplo Patient
    User: Usuario;

    constructor(used: number, createDate: Date, expirationDate: Date, useDate: Date | null, User: Usuario) {
        this.used = used;
        this.createDate = createDate;
        this.expirationDate = expirationDate;
        this.useDate = useDate;
        this.User = User;
    }
}