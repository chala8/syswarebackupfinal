import {
    Column,
    Connection,
    Entity,
    JoinColumn,
    ManyToMany, ManyToOne, OneToMany,
    OneToOne,
    PrimaryColumn
} from "typeorm";
import {Rol} from "./Rol";
import {Application} from "./Application";

@Entity()
export class Menu {
    @PrimaryColumn()
        // @ts-ignore
    id: number;

    @Column({
        type:'varchar2',
        nullable:false,
        length:100
    })
    nombre: string;

    @Column({
        type:'number',
        nullable:false
    })
    tipo: number;

    @Column({
        type:'varchar2',
        nullable:false,
        length:500
    })
    ruta: string;

    @Column({
        type:'varchar2',
        nullable:true,
        length:1000
    })
    icono: string;

    @Column({
        type:'varchar2',
        nullable:true,
        length:1000
    })
    avatar: string;

    @Column({
        type:'varchar2',
        nullable:true,
        length:1000
    })
    background: string;

    @Column({
        type:'number',
        nullable:true
    })
    estado: number;

    @Column({
        type:'varchar2',
        nullable:false,
        length:100
    })
    dispositivo: string;

    @ManyToOne(type => Menu, rol => rol.ids_hijos)
    id_padre: Menu;

    @OneToMany(type => Menu, rol => rol.id_padre)
    ids_hijos: Menu[];

    @ManyToMany(type => Rol, rol => rol.menus)
    roles: Rol[];

    @ManyToOne(type => Application)
    id_applicacion: Application;

    constructor(nombre: string, tipo: number, ruta: string, icono: string, avatar: string, background: string, estado: number, dispositivo: string, id_padre: Menu,roles: Rol[],id_applicacion: Application,ids_hijos: Menu[]) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.ruta = ruta;
        this.icono = icono;
        this.avatar = avatar;
        this.background = background;
        this.estado = estado;
        this.dispositivo = dispositivo;
        this.id_padre = id_padre;
        this.roles = roles;
        this.id_applicacion = id_applicacion;
        this.ids_hijos = ids_hijos;
    }

    public static async CheckIFTriggerOk(db : Connection){
        let Conteo = await db.manager.query("SELECT count(*) FROM user_triggers where trigger_name = '"+this.name.toLocaleUpperCase()+"_TRIG'");
        Conteo = Conteo[0]["COUNT(*)"];
        if(Conteo == 0){//No existe, se crea
            let ResultadoCrear = await db.manager.query(
                "create trigger "+this.name+"_Trig\n" +
                "    before insert\n" +
                "    on \""+this.name.toLocaleLowerCase()+"\"\n" +
                "    for each row\n" +
                "begin\n" +
                "    if :NEW.\"id\" is null then\n" +
                "      select \"MENUS_SEQ\".nextval into :NEW.\"id\" from dual;\n" +
                "    end if;\n" +
                "  end;"
            );
            console.log("Trigger Creado: '"+this.name+"_Trig'");
            console.log(ResultadoCrear);

        }else{
            console.log("Trigger '"+this.name+"_Trig' al dia");
        }
    }
}

export interface MenuInterface {
    nombre: string;
    tipo: number;
    ruta: string;
    icono: string;
    avatar: string;
    background: string;
    estado: number;
    dispositivo: string;
    id_padre: number;
    roles: number[];
    id_applicacion: number;
    ids_hijos: number[];
}
