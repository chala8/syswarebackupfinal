import {
    Column, Connection,
    Entity,
    JoinColumn,
    JoinTable,
    ManyToMany, ManyToOne,
    OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from "typeorm";
import {Menu} from "./Menu";
import {Usuario} from "./Usuario";
import {Application} from "./Application";

@Entity()
export class Rol {
    @PrimaryColumn()
        // @ts-ignore
    id: number;

    @Column({
        type:'varchar2',
        nullable:false,
        length:100
    })
    Rol: string;

    @Column({
        type:'varchar2',
        nullable:false,
        length:200
    })
    Descripcion: string;

    @ManyToMany(type => Menu, munu => munu.roles)
    @JoinTable()
    menus: Menu[];

    @ManyToMany(type => Usuario, rol => rol.roles)
    usuarios: Usuario[];

    @ManyToOne(type => Application)
    id_applicacion: Application;

    constructor(Rol: string, Descripcion: string, menus: Menu[],usuarios: Usuario[],id_applicacion: Application) {
        this.Rol = Rol;
        this.Descripcion = Descripcion;
        this.menus = menus;
        this.usuarios = usuarios;
        this.id_applicacion = id_applicacion;
    }

    public static async CheckIFTriggerOk(db : Connection){
        let Conteo = await db.manager.query("SELECT count(*) FROM user_triggers where trigger_name = '"+this.name.toLocaleUpperCase()+"_TRIG'");
        Conteo = Conteo[0]["COUNT(*)"];
        if(Conteo == 0){//No existe, se crea
            let ResultadoCrear = await db.manager.query(
                "create trigger "+this.name+"_Trig\n" +
                "    before insert\n" +
                "    on \""+this.name.toLocaleLowerCase()+"\"\n" +
                "    for each row\n" +
                "begin\n" +
                "    if :NEW.\"id\" is null then\n" +
                "      select \"ROLES_SEQ\".nextval into :NEW.\"id\" from dual;\n" +
                "    end if;\n" +
                "  end;"
            );
            console.log("Trigger Creado: '"+this.name+"_Trig'");
            console.log(ResultadoCrear);

        }else{
            console.log("Trigger '"+this.name+"_Trig' al dia");
        }
    }
}

export interface RolInterface {
    Rol: string;
    Descripcion: string;
    menus: number[];
    usuarios: number[];
    id_applicacion: number;
}