import {
    Column,
    Entity,
    ManyToOne,
    PrimaryColumn, PrimaryGeneratedColumn,
} from "typeorm";

import {Usuario} from "./Usuario";

@Entity()
export class FCMtokens {
    @PrimaryGeneratedColumn('uuid')
        // @ts-ignore
    id: number;

    @Column({
        type:'varchar2',
        nullable:false,
        length:2000
    })
    Token: string;

    @Column({
        type:Date,
        nullable:false
    })
    AddDate: Date;

    @ManyToOne(type => Usuario,usuario => usuario.FCMtokens)
    Usuario: Usuario;

    constructor(Token: string, AddDate: Date, Usuario: Usuario) {
        this.Token = Token;
        this.AddDate = AddDate;
        this.Usuario = Usuario;
    }
}