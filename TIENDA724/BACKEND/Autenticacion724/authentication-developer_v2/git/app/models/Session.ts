import {Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Application} from "./Application";
import {Usuario} from "./Usuario";

@Entity()
export class Session {
    @PrimaryGeneratedColumn('uuid')
        // @ts-ignore
    id: string;

    @Column({
        type:'varchar',
        nullable:false,
        length:100
    })

    userID: string;

    @Column({
        type:'varchar',
        nullable:false,
        length:100
    })
    roles: string;

    @Column({
        type:'varchar',
        nullable:false,
        length:1500
    })
    token: string;

    @Column({
        type:Date,
        nullable:false
    })
    LogIn_Date: Date;

    @Column({
        type:Date,
        nullable:true
    })
    LogOut_Date: Date;

    @ManyToOne(type => Application)
    id_applicacion: Application;

    constructor(userID: string,token: string,roles: string,id_applicacion: Application,LogIn_Date: Date,LogOut_Date: Date) {
        this.userID = userID;
        this.token = token;
        this.id_applicacion = id_applicacion;
        this.roles = roles;
        this.LogIn_Date = LogIn_Date;
        this.LogOut_Date = LogOut_Date;
    }
}
export interface SessionInterface {
    userID: string;
    token: string;
    roles: number[];
    id_applicacion: number;
    LogIn_Date: Date;
    LogOut_Date: Date;
}
