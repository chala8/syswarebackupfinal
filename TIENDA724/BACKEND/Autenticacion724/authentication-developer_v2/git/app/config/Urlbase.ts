let UsarPruebas = true;
let Prod = (process.env.SYSWARE_SERVER != null && process.env.SYSWARE_SERVER == "prod");
export const URLS = {
    //URLS["auth"]
    auth:'https://'+(Prod ? 'tienda724.com':UsarPruebas ? 'pruebas.tienda724.com':'localhost') + ':8449',
    //URLS["tercero"]
    tercero:'https://'+(Prod ? 'tienda724.com':UsarPruebas ? 'pruebas.tienda724.com':'localhost') + ':8446/v1',
    //URLS["tercero"]
    facturacion:'https://'+(Prod ? 'tienda724.com':UsarPruebas ? 'pruebas.tienda724.com':'localhost') + ':8448/v1',
};
