// lib/config/routes.ts
import {SessionsController} from "../controllers/SessionsController";
import {Connection} from "typeorm";
import {connection} from "./database";
import {ApplicationsController} from "../controllers/ApplicationsController";
import {MenusController} from "../controllers/MenusController";
import {RolesController} from "../controllers/RolesController";
import {UsuaiosController} from "../controllers/UsuaiosController";
import {AuthController} from "../controllers/AuthController";
import {AuthVerify} from "../middleware/AuthVerify";
import {RolesCheck} from "../middleware/RolesCheck";
import {NextFunction} from "express";
import {NotificationsController} from "../controllers/NotificationsController";

export class Routes {
    public db : Connection = connection;
    public sessionsController: SessionsController;
    public applicationsController: ApplicationsController;
    public menusController: MenusController;
    public rolesController: RolesController;
    public usuariosController: UsuaiosController;
    public authController: AuthController;
    public authVerify: AuthVerify;
    public rolesVerify: RolesCheck;
    public notificationsController: NotificationsController;

    constructor(db: Connection){
        this.db = db;
        this.sessionsController = new SessionsController(this.db);
        this.applicationsController = new ApplicationsController(this.db);
        this.menusController = new MenusController(this.db);
        this.rolesController = new RolesController(this.db);
        this.usuariosController = new UsuaiosController(this.db);
        this.authController = new AuthController(this.db);
        this.notificationsController = new NotificationsController(this.db);
        this.authVerify = new AuthVerify(this.db);
        this.rolesVerify = new RolesCheck();
    }

    public routes(app:any): void {
        /////////////////////////////////////
        //////// RUTAS DE SESSIONS //////////
        /////////////////////////////////////
        app.route("/sessions")
            .get(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.sessionsController.index)
            .post(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.sessionsController.create);

        // app.route("/sessions/:id")
        //     .get(this.sessionsController.show)
        //     .put(this.sessionsController.update)
        //     .delete(this.sessionsController.delete);


        /////////////////////////////////////////
        //////// RUTAS DE APPLICATIONS //////////
        /////////////////////////////////////////
        app.route("/applications")
            .get(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.applicationsController.index)
            .post(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.applicationsController.create);

        app.route("/applications/:id")
            .get(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.applicationsController.show)
            .put(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.applicationsController.update)
            .delete(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.applicationsController.delete);

        //////////////////////////////////
        //////// RUTAS DE MENUS //////////
        //////////////////////////////////
        app.route("/menus")
            .get(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.menusController.index)
            .post(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.menusController.create);

        app.route("/menus/:id")
            .get(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.menusController.show)
            .put(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.menusController.update)
            .delete(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.menusController.delete);

        //////////////////////////////////
        //////// RUTAS DE ROLES //////////
        //////////////////////////////////
        app.route("/roles")
            .get(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.rolesController.index)
            .post(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.rolesController.create);

        app.route("/roles/:id")
            .get(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.rolesController.show)
            .put(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.rolesController.update)
            .delete(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.rolesController.delete);

        /////////////////////////////////////
        //////// RUTAS DE USUARIOS //////////
        /////////////////////////////////////
        app.route("/usuarios")
            .get(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.usuariosController.index)
            .post(this.authVerify.MiddleVerify,
                //(req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.usuariosController.create);

        app.route("/usuarios/:id")
            .get(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.usuariosController.show)
            .put(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.usuariosController.update)
            .delete(this.authVerify.MiddleVerify,
                (req:Request, res:Response, next:NextFunction)=>{this.rolesVerify.MiddleCheckRoles(req,res,next,["7777"])},
                this.usuariosController.delete);

        app.route("/userExists").post(this.usuariosController.userExists);

        ///////////////////////////////////////////
        //////// RUTAS DE AUTHCONTROLLER //////////
        ///////////////////////////////////////////
        app.route("/CreateClient").post(this.authController.CreateClient);
        app.route("/loginClient").post(this.authController.LoginAsClient);
        app.route("/login").post(this.authController.Login);
        app.route("/verify").post(this.authController.Verify);
        app.route("/CheckSession").post(this.authController.VerifySession);//es lo mismo que verify pero por cookie y destinada a la app movil
        app.route("/CheckPerson").post(this.authController.CheckPerson);
        app.route("/logout").post(this.authVerify.MiddleVerify,this.authController.Logout);
        app.route("/passrecovery").post(this.authController.RecoverPassword);
        app.route("/checkrecoverytoken").post(this.authController.CheckRecoveryToken);
        app.route("/userecoverytoken").post(this.authController.UseRecoveryToken);

        //////////////////////////////////////////
        //////// RUTAS DE NOTIFICATIONS //////////
        //////////////////////////////////////////
        app.route("/UpdateFCMtoken").post(this.authVerify.MiddleVerify,this.notificationsController.UpdateFCMtoken);
        app.route("/SendFCMpush").post(this.authVerify.MiddleVerify,this.notificationsController.SendFCMpush);
        app.route("/SendFCMpushFromClient").post(this.authVerify.MiddleVerify,this.notificationsController.SendFCMpushFromClient);
    }
}
