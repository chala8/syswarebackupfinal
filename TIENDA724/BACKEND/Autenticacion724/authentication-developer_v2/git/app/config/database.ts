import {ConnectionManager} from "typeorm";

const connectionManager = new ConnectionManager();

console.log("Variable de entorno SYSWARE_SERVER es: '"+process.env.SYSWARE_SERVER+"'");

export const connection = connectionManager.create({
    type: "oracle",
    // host: "sysware.cw2rwsqphepo.us-east-1.rds.amazonaws.com",
    host: process.env.SYSWARE_SERVER == "prod" ? "sysware.cw2rwsqphepo.us-east-1.rds.amazonaws.com":"sysware-backup.cw2rwsqphepo.us-east-1.rds.amazonaws.com",
    port: 1521,
    username: "auth2",
    password: "auth2",
    database: "AUTH2",
    schema: "AUTH2",
    // sid: "orcl",
    sid: process.env.SYSWARE_SERVER == "prod" ? "orcl":"BACKUP1",
    synchronize: true,
    logging: ["error"],
    entities: [
        __dirname + "/../models/*"
    ],
    migrations: [__dirname + "/../migration/*"],
    cli: {
        entitiesDir: __dirname + "/../entity",
        migrationsDir: __dirname + "/../migration"
    }
});