import { Request, Response } from "express";
import {Session, SessionInterface} from "../models/Session";
import {Connection} from "typeorm";
import {connection} from "../config/database";
import {Repository} from "typeorm/repository/Repository";
import {Application} from "../models/Application";

export class SessionsController {
    public db : Connection = connection;
    static SessionRepo:Repository<Session>;
    constructor(db: Connection) {
        this.db = db;
        SessionsController.SessionRepo = db.manager.getRepository(Session);
    }
    public async index(req: Request, res: Response) {
        try{
            let sessions = await SessionsController.SessionRepo.find();
            res.json(sessions);
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e)
        }
    }
    public async create(req: Request, res: Response) {
        const params: SessionInterface = req.body;
        let Resultado = await this.CreateMethod(params);
        if(Resultado[0] == 1){
            res.status(201).json(Resultado[1]);
        }else{
            res.status(500).json(Resultado[1])
        }
    }

    public async CreateMethod(body:SessionInterface){
        try{
            let app = await this.db.manager.getRepository(Application).findOne(body.id_applicacion);
            if(app == null){
                // noinspection ExceptionCaughtLocallyJS
                throw "App not found";
            }
            let Sesion = new Session(body.userID,body.token,body.roles.join(','),app,body.LogIn_Date,body.LogOut_Date);
            let sesion = await SessionsController.SessionRepo.save(Sesion);
            return [1,sesion];
        }catch (e) {
            console.log(e.toString());
            return [-1, e.toString()];
        }
    }
    // public async show(req: Request, res: Response) {
    //     const sessionID: number = parseInt(req.params.id);
    //     try{
    //         let Session = await Session.findByPrimary(sessionID);
    //         if (Session) {
    //             res.json(Session);
    //         } else {
    //             res.status(404).json({ errors: ["Session not found"] });
    //         }
    //     }catch (e) {
    //         res.status(500).json(e.toString())
    //     }
    // }
    // public async update(req: Request, res: Response) {
    //     const nodeId: number = parseInt(req.params.id);
    //     const params: SessionInterface = req.body;
    //
    //     try{
    //         let Session = await Session.update(params, {
    //             where: { id: nodeId },
    //             limit: 1
    //         });
    //         res.status(202).json({ data: "success" });
    //     }catch (e) {
    //         res.status(500).json(e.toString())
    //     }
    // }
    // public async delete(req: Request, res: Response) {
    //     const nodeId: number = parseInt(req.params.id);
    //     try{
    //         let Session = await Session.destroy({
    //             where: { id: nodeId },
    //             limit: 1
    //         });
    //         res.status(204).json({ data: "success" });
    //     }catch (e) {
    //         res.status(500).json(e.toString())
    //     }
    // }
}
