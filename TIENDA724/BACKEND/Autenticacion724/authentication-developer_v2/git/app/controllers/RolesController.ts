import { Request, Response } from "express";
import {Connection} from "typeorm";
import {connection} from "../config/database";
import {Repository} from "typeorm/repository/Repository";
import {Rol, RolInterface} from "../models/Rol";
import {Menu} from "../models/Menu";
import {Application} from "../models/Application";
import {Usuario} from "../models/Usuario";

export class RolesController {
    static db : Connection = connection;
    static Repo:Repository<Rol>;

    constructor(db: Connection) {
        RolesController.db = db;
        RolesController.Repo = db.manager.getRepository(Rol);
    }

    public async index(req: Request, res: Response) {
        try{
            let roles = await RolesController.Repo.find();
            res.json(roles);
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e)
        }
    }

    public async create(req: Request, res: Response) {
        console.log("LLEGUE A CREATE")
        try{
            const params: RolInterface = req.body;
            //Obtengo los menus que le asigna
            let menus : Menu[] = [];
            let RepoMenus = RolesController.db.manager.getRepository(Menu);
            for(let n = 0;n<params.menus.length;n++){
                let menu = await RepoMenus.findOne(params.menus[n])
                if(menu == null){continue;}
                menus.push(menu);
            }
            //Obtengo la app a la que pertenece
            let app = await RolesController.db.manager.getRepository(Application).findOne(params.id_applicacion);
            if(app == null){
                res.status(404).json({ errors: ["App not found"] });
                return;
            }
            //Creo el rol
            let Rol_Creado = new Rol(
                params.Rol,
                params.Descripcion,
                menus,
                [],
                app
            );
            let rol = await RolesController.Repo.save(Rol_Creado);
            res.status(201).json(rol);
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e.toString())
        }
    }

    public async show(req: Request, res: Response) {
        const ID: number = parseInt(req.params.id);
        try{
            let Rol = await RolesController.Repo.findOne(ID);
            if (Rol) {
                res.json(Rol);
            } else {
                res.status(404).json({ errors: ["Rol not found"] });
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }
    public async update(req: Request, res: Response) {
        try{
            const id: number = parseInt(req.params.id);
            const params: RolInterface = req.body;
            let Rol = await RolesController.Repo.findOne(id);
            if(Rol == null){
                res.status(404).json({ data: "Not Found" });
            }
            else{
                //Verifico cada campo posible, si no es null, se asigna
                if(params.Rol != null){Rol.Rol = params.Rol}
                if(params.Descripcion != null){Rol.Descripcion = params.Descripcion}
                if(params.menus != null){
                    let menus : Menu[] = [];
                    let RepoMenus = RolesController.db.manager.getRepository(Menu);
                    for(let n = 0;n<params.menus.length;n++){
                        let menu = await RepoMenus.findOne(params.menus[n])
                        if(menu == null){continue;}
                        menus.push(menu);
                    }
                    Rol.menus = menus
                }
                if(params.usuarios != null){
                    let users : Usuario[] = [];
                    let RepoUser = RolesController.db.manager.getRepository(Usuario);
                    for(let n = 0;n<params.usuarios.length;n++){
                        let user = await RepoUser.findOne(params.usuarios[n])
                        if(user == null){continue;}
                        users.push(user);
                    }
                    Rol.usuarios = users
                }
                if(params.id_applicacion != null){
                    //Obtengo la app a la que pertenece
                    let app = await RolesController.db.manager.getRepository(Application).findOne(params.id_applicacion);
                    if(app == null){
                        res.status(404).json({ errors: ["App not found"] });
                        return;
                    }
                    Rol.id_applicacion = app
                }
                let Resultado = await RolesController.Repo.save(Rol);
                res.status(202).json({ data: "success" });
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }
    public async delete(req: Request, res: Response) {
        try{
            const id: number = parseInt(req.params.id);
            let Rol = await RolesController.Repo.findOne(id);
            if(Rol == null){
                res.status(404).json({ data: "Not Found" });
            }
            else{
                let Resultado = await RolesController.Repo.remove(Rol);
                res.status(204).json({ data: "success" });
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }
}
