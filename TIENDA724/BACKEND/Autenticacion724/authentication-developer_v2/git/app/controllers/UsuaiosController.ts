import {Request, Response} from "express";
import {Connection} from "typeorm";
import {connection} from "../config/database";
import {Repository} from "typeorm/repository/Repository";
import {Application} from "../models/Application";
import {Usuario, UsuarioInterface} from "../models/Usuario";
import {Rol} from "../models/Rol";
import bcrypt = require("bcryptjs");

export class UsuaiosController {
    static db : Connection = connection;
    static Repo:Repository<Usuario>;

    constructor(db: Connection) {
        UsuaiosController.db = db;
        UsuaiosController.Repo = db.manager.getRepository(Usuario);
    }

    public async index(req: Request, res: Response) {
        try{
            let usuarios = await UsuaiosController.Repo.find();
            res.json(usuarios);
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e)
        }
    }

    public async create(req: Request, res: Response) {
        try{
            throw "Not Implemented";
            // const params: UsuarioInterface = req.body;
            // //Obtengo los menus que le asigna
            // let roles : Rol[] = [];
            // let RepoRoles = UsuaiosController.db.manager.getRepository(Rol);
            // for(let n = 0;n<params.roles.length;n++){
            //     let rol = await RepoRoles.findOne(params.roles[n]);
            //     if(rol == null){continue;}
            //     roles.push(rol);
            // }
            // //Obtengo la app a la que pertenece
            // let app = await UsuaiosController.db.manager.getRepository(Application).findOne(params.id_applicacion);
            // if(app == null){
            //     res.status(404).json({ errors: ["App not found"] });
            //     return;
            // }
            // //Creo el rol
            // let PassHasheada = await bcrypt.hash(req.body.clave,12);
            // let Usuario_Creado = new Usuario(
            //     params.id_usuario,
            //     params.usuario,
            //     PassHasheada,
            //     [],
            //     app
            // );
            // Usuario_Creado.createDate = new Date();
            // let usr = await UsuaiosController.Repo.save(Usuario_Creado);
            // usr = (await UsuaiosController.Repo.find({where:{
            //     usuario:usr.usuario,
            //     id_applicacion:usr.id_applicacion
            // }}))[0];
            // usr.roles = roles;
            // usr = await UsuaiosController.Repo.save(usr);
            // res.status(201).json(usr);
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e.toString())
        }
    }

    public async show(req: Request, res: Response) {
        try{
            const ID: number = parseInt(req.params.id);
            const AppID: number = parseInt(req.params.id_app);
            let where:any = {
                id:ID
            };
            if(AppID != null){where["id_applicacion"] = AppID;}
            let Usuario = await UsuaiosController.Repo.find({
                relations:["roles"],
                where:where
            });
            if (Usuario.length > 0) {
                res.json(Usuario[0]);
            } else {
                res.status(404).json({ errors: ["Rol not found"] });
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }

    public async userExists(req: Request, res: Response) {
        try{
            if(req.body.usuario == ""){ throw "Bad Input"}
            let where:any = {
                usuario:req.body.usuario
            };
            if(req.body.AppID != null){where["id_applicacion"] = req.body.AppID;}
            let Usuario = await UsuaiosController.Repo.find({where:where});
            if (Usuario.length > 0) {
                res.json({result:true});
            } else {
                res.json({result:false});
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }

    public async update(req: Request, res: Response) {
        try{
            const id: number = parseInt(req.params.id);
            const params: UsuarioInterface = req.body;
            let Usuario = await UsuaiosController.Repo.findOne(id);
            if(Usuario == null){
                res.status(404).json({ data: "Not Found" });
            }
            else{
                //Verifico cada campo posible, si no es null, se asigna
                if(params.usuario != null){Usuario.usuario = params.usuario}
                if(params.clave != null){
                    Usuario.clave = await bcrypt.hash(req.body.clave, 12);
                }
                if(params.roles != null){
                    let roles : Rol[] = [];
                    let RepoRoles = UsuaiosController.db.manager.getRepository(Rol);
                    for(let n = 0;n<params.roles.length;n++){
                        let rol = await RepoRoles.findOne(params.roles[n]);
                        if(rol == null){continue;}
                        roles.push(rol);
                    }
                    Usuario.roles = roles
                }
                if(params.id_applicacion != null){
                    //Obtengo la app a la que pertenece
                    let app = await UsuaiosController.db.manager.getRepository(Application).findOne(params.id_applicacion);
                    if(app == null){
                        res.status(404).json({ errors: ["App not found"] });
                        return;
                    }
                    Usuario.id_applicacion = app
                }
                let Resultado = await UsuaiosController.Repo.save(Usuario);
                res.status(202).json({ data: "success" });
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }
    public async delete(req: Request, res: Response) {
        try{
            const id: number = parseInt(req.params.id);
            let Usuario = await UsuaiosController.Repo.findOne(id);
            if(Usuario == null){
                res.status(404).json({ data: "Not Found" });
            }
            else{
                let Resultado = await UsuaiosController.Repo.remove(Usuario);
                res.status(204).json({ data: "success" });
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }
}
