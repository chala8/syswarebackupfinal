import { Request, Response } from "express";
import {Application, ApplicationInterface} from "../models/Application";
import {Connection} from "typeorm";
import {connection} from "../config/database";
import {Repository} from "typeorm/repository/Repository";

export class ApplicationsController {
    //public db : Connection = connection;
    static Repo:Repository<Application>;

    constructor(db: Connection) {
        //this.db = db;
        ApplicationsController.Repo = db.manager.getRepository(Application);
    }

    public async index(req: Request, res: Response) {
        try{
            let apps = await ApplicationsController.Repo.find();
            res.json(apps);
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e)
        }
    }

    public async create(req: Request, res: Response) {
        try{
            const params: ApplicationInterface = req.body;
            let App = new Application(
                params.name,
                params.Description,
                params.Version,
                params.Creation_Date,
                params.Update_Date,
                params.Key_Application,
                params.Key_Server
            );
            let app = await ApplicationsController.Repo.save(App);
            res.status(201).json(app);
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e.toString())
        }
    }

    public async show(req: Request, res: Response) {
        const ID: number = parseInt(req.params.id);
        try{
            let Application = await ApplicationsController.Repo.findOne(ID);
            if (Application) {
                res.json(Application);
            } else {
                res.status(404).json({ errors: ["Application not found"] });
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }
    public async update(req: Request, res: Response) {
        try{
            const id: number = parseInt(req.params.id);
            const params: ApplicationInterface = req.body;
            let Application = await ApplicationsController.Repo.findOne(id);
            if(Application == null){
                res.status(404).json({ data: "Not Found" });
            }
            else{
                Application.SetFromInterface(params);
                let Resultado = await ApplicationsController.Repo.save(Application);
                res.status(202).json({ data: "success" });
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }
    public async delete(req: Request, res: Response) {
        try{
            const id: number = parseInt(req.params.id);
            let Application = await ApplicationsController.Repo.findOne(id);
            if(Application == null){
                res.status(404).json({ data: "Not Found" });
            }
            else{
                let Resultado = await ApplicationsController.Repo.remove(Application);
                res.status(204).json({ data: "success" });
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }
}
