import { Request, Response } from "express";
import {Connection} from "typeorm";
import {connection} from "../config/database";
import {Repository} from "typeorm/repository/Repository";
import {Menu, MenuInterface} from "../models/Menu";
import {Application} from "../models/Application";
import {Rol} from "../models/Rol";

export class MenusController {
    static db : Connection = connection;
    static Repo:Repository<Menu>;
    static RepoRoles:Repository<Rol>;

    constructor(db: Connection) {
        MenusController.db = db;
        MenusController.Repo = db.manager.getRepository(Menu);
        MenusController.RepoRoles = db.manager.getRepository(Rol);
    }

    public async index(req: Request, res: Response) {
        try{
            let menus = await MenusController.Repo.find();
            res.json(menus);
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e)
        }
    }

    public async create(req: Request, res: Response) {
        try{
            const params: MenuInterface = req.body;
            //Obtengo el manu padre, si tiene
            // @ts-ignore
            let menu:Menu = null;
            if(params.id_padre != null && params.id_padre != -1){
                // @ts-ignore
                menu = await MenusController.Repo.findOne(params.id_padre);
            }
            //Obtengo los roles que le asigna. si tiene
            let roles : Rol[] = [];
            for(let n = 0;n<params.roles.length;n++){
                let rol = await MenusController.RepoRoles.findOne(params.roles[n])
                if(rol == null){continue;}
                roles.push(rol);
            }
            //Obtengo los hijos que le asigna. si tiene
            let hijos : Menu[] = [];
            for(let n = 0;n<params.ids_hijos.length;n++){
                let hijo = await MenusController.Repo.findOne(params.ids_hijos[n])
                if(hijo == null){continue;}
                hijos.push(hijo);
            }
            //Obtengo la app a la que pertenece
            let app = await MenusController.db.manager.getRepository(Application).findOne(params.id_applicacion);
            if(app == null){
                res.status(404).json({ errors: ["App not found"] });
                return;
            }
            //Creo el rol
            let Menu_Creado = new Menu(
                params.nombre,
                params.tipo,
                params.ruta,
                params.icono,
                params.avatar,
                params.background,
                params.estado,
                params.dispositivo,
                menu,
                roles,
                app,
                hijos
            );
            let rol = await MenusController.Repo.save(Menu_Creado);
            res.status(201).json(rol);
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e.toString())
        }
    }

    public async show(req: Request, res: Response) {
        const ID: number = parseInt(req.params.id);
        try{
            let Rol = await MenusController.Repo.findOne(ID);
            if (Rol) {
                res.json(Rol);
            } else {
                res.status(404).json({ errors: ["Rol not found"] });
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }
    public async update(req: Request, res: Response) {
        try{
            const id: number = parseInt(req.params.id);
            const params: MenuInterface = req.body;
            let Rol = await MenusController.Repo.findOne(id);
            if(Rol == null){
                res.status(404).json({ data: "Not Found" });
            }
            else{
                //Verifico cada campo posible, si no es null, se asigna
                if(params.nombre!= null){ Rol.nombre = params.nombre; }
                if(params.tipo!= null){ Rol.tipo = params.tipo; }
                if(params.ruta!= null){ Rol.ruta = params.ruta; }
                if(params.icono!= null){ Rol.icono = params.icono; }
                if(params.avatar!= null){ Rol.avatar = params.avatar; }
                if(params.background!= null){ Rol.background = params.background; }
                if(params.estado!= null){ Rol.estado = params.estado; }
                if(params.dispositivo!= null){ Rol.dispositivo = params.dispositivo; }
                if(params.id_padre != null && params.id_padre != -1){
                    // @ts-ignore
                    let menu = await MenusController.db.manager.getRepository(Menu).findOne(params.id_padre);
                    // @ts-ignore
                    Rol.id_padre = menu;
                }
                //Obtengo los roles que le asigna. si tiene
                if(params.roles!= null){
                    let roles : Rol[] = [];
                    for(let n = 0;n<params.roles.length;n++){
                        let rol = await MenusController.RepoRoles.findOne(params.roles[n])
                        if(rol == null){continue;}
                        roles.push(rol);
                    }
                    Rol.roles = roles;
                }
                //Obtengo la app a la que pertenece
                if(params.id_applicacion != null){
                    //Obtengo la app a la que pertenece
                    let app = await MenusController.db.manager.getRepository(Application).findOne(params.id_applicacion);
                    if(app == null){
                        res.status(404).json({ errors: ["App not found"] });
                        return;
                    }
                    Rol.id_applicacion = app
                }
                let Resultado = await MenusController.Repo.save(Rol);
                res.status(202).json({ data: "success" });
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }
    public async delete(req: Request, res: Response) {
        try{
            const id: number = parseInt(req.params.id);
            let Rol = await MenusController.Repo.findOne(id);
            if(Rol == null){
                res.status(404).json({ data: "Not Found" });
            }
            else{
                let Resultado = await MenusController.Repo.remove(Rol);
                res.status(204).json({ data: "success" });
            }
        }catch (e) {
            res.status(500).json(e.toString())
        }
    }
}
