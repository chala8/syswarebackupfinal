import {Repository} from "typeorm/repository/Repository";
import {Usuario} from "../models/Usuario";
import {Application} from "../models/Application";
import {Menu} from "../models/Menu";
import {Rol} from "../models/Rol";
import {Session} from "../models/Session";
import {Between, Connection} from "typeorm";
import {connection} from "../config/database";
import {Request, Response} from "express";
import bcrypt = require("bcryptjs");
import {AuthVerify} from "../middleware/AuthVerify";
import {URLS} from "../config/Urlbase";
const jwt = require('jsonwebtoken');
var request = require("request");
import { addYears, subYears } from 'date-fns';
import {FCMtokens} from "../models/FCMtokens";
import {PasswordRecoveryToken} from "../models/PasswordRecoveryToken";

var fs = require('fs');
var path = require('path');
var ketPath = path.join(__dirname, '..','..', 'jwt', 'private.key');
const privateKey = fs.readFileSync(ketPath, 'utf8');

//gpT3Ydb_jgA!'qAhWT CLAVE DE GENERADO DE LAS LLAVES DEL JWT, NO BORRAR O GUARDAR EN OTRO LADO

export class AuthController {
    static db : Connection = connection;
    static RepoUsuario:Repository<Usuario>;
    static RepoApplication:Repository<Application>;
    static RepoMenu:Repository<Menu>;
    static RepoRol:Repository<Rol>;
    static RepoSession:Repository<Session>;
    static authVerify: AuthVerify;
    static RepoPassTokens:Repository<PasswordRecoveryToken>;

    constructor(db: Connection) {
        AuthController.db = db;
        AuthController.RepoUsuario = db.manager.getRepository(Usuario);
        AuthController.RepoApplication = db.manager.getRepository(Application);
        AuthController.RepoMenu = db.manager.getRepository(Menu);
        AuthController.RepoRol = db.manager.getRepository(Rol);
        AuthController.RepoSession = db.manager.getRepository(Session);
        AuthController.authVerify = new AuthVerify(AuthController.db);
        AuthController.RepoPassTokens = db.manager.getRepository(PasswordRecoveryToken);
    }

    public async Verify(req: Request, res: Response){
        try{
            if(req.body.token == null){
                res.status(404).json("Token missing");
                return;
            }
            res.status(200).json(await AuthController.authVerify.ManuelVerify(req.body.token));
        }catch (e) {
            console.log(e.toString());
            res.status(404).json("Not found");
        }
    }

    public async VerifySession(req: Request, res: Response){
        try{
            let error = false;
            let token:any = "";
            if(req.cookies['SESSIONID'] == null){
                error = true;
            }else{token = req.cookies['SESSIONID'];}
            if(error && req.headers["authorization"] == null){
                res.status(404).json("Token missing");
                return;
            }else{if(error){token = req.headers["authorization"];}}
            res.status(200).json(await AuthController.authVerify.ManuelVerify(token));
        }catch (e) {
            console.log(e.toString());
            res.status(404).json("Not found");
        }
    }

    public async Logout(req: Request, res: Response){
        try{
            // @ts-ignore
            let sesion: Session = req.session;
            sesion.LogOut_Date = new Date();
            AuthController.RepoSession.save(sesion);
            res.status(200).json("Session ended successfully");
            //Elimino los tokends FCM
            let User = await AuthController.RepoUsuario.find({where:{uuid:parseInt(sesion.userID)},relations:["FCMtokens"]});
            let eltoken = req.body.token;
            for(let n = User[0].FCMtokens.length-1;n>=0;n--) {
                let token = User[0].FCMtokens[n];
                if(token.Token == eltoken){
                    let ToDeleteToken = User[0].FCMtokens.pop();
                    if(ToDeleteToken != null){await AuthController.db.getRepository(FCMtokens).remove(ToDeleteToken);}
                }
            }
            await AuthController.RepoUsuario.save(User[0]);
        }catch (e) {
            console.log(e.toString());
            res.status(404).json("Not found");
        }
    }

    public async Login(req: Request, res: Response){//PARA FRONT TIENDA 724
        try {
            //Buscamos la aplicación
            let app = await AuthController.RepoApplication.findOne(req.query.id_aplicacion);
            if(app == null){ res.status(404).json({ errors: ["App not found"] });return;}
            /////////////////////////
            let UsuarioEncontrado = await AuthController.RepoUsuario.find({
                where:{
                    usuario: req.body.usuario,
                    id_applicacion: app,
                },
                relations: ["roles"]
            });
            if(!bcrypt.compareSync(req.body.clave,UsuarioEncontrado[0].clave)){
                res.status(404).json("Not found");
                return;
            }
            let Peticion = {
                url: URLS.tercero+"/thirds/logginData?id_third="+UsuarioEncontrado[0].id_third,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    "Authorization":"3020D4:0DD-2F413E82B-A1EF04559-78CA",
                    "Key_Server":"3020D4:1FG-2U113E822-A1TE04529-68CF"
                },
                json:{}
            };
            let response:any= await new Promise((resolve, reject) => {
                request.get(Peticion, function (error: any, response: any, body: any) {
                    if (error || response.statusCode != 200){
                        console.log("[logginData]Error: ")
                        console.log(error || response.body)
                        reject("Body Null or Error");
                    }
                    else{
                        resolve(response);
                    }
                })
            });
            let logginData:{
                "docnumberfather": string,
                "first_LASTNAME": string,
                "second_LASTNAME": string,
                "docutypefather": string,
                "document_NUMBER": string,
                "fullnamefather": string,
                "id_THIRD_FATHER": number,
                "fullname": string,
                "document_TYPE": string,
                "id_PERSON": number,
                "second_NAME": string,
                "img": string,
                "first_NAME": string
            } = response.body[0];
            //Listado de roles
            let roles = UsuarioEncontrado[0].roles;
            let RolesFormateados = [];
            let MenusFormateados = [];
            let idsRoles: number[] = [];
            //Obtengo los menús asociados a los roles y los convierto de nuevo al formato anterior
            for(let n = 0;n < roles.length; n++){
                RolesFormateados.push({
                    id_rol:roles[n].id,
                    id_aplicacion:app.id,
                    rol:roles[n].Rol,
                    descripcion:roles[n].Descripcion
                });
                idsRoles.push(roles[n].id);
                //Obtengo de nuevo el rol pero con sus menus
                let MenusDelRol = await AuthController.RepoRol.findOne(roles[n].id,{relations: ["menus"]});
                if(MenusDelRol == null){continue;}
                for(let m = 0;m<MenusDelRol.menus.length;m++){
                    let ElMenu = MenusDelRol.menus[m];
                    MenusFormateados.push({
                        id_menu: ElMenu.id,
                        id_aplicacion: app.id,
                        nombre: ElMenu.nombre,
                        tipo: ElMenu.tipo,
                        ruta: ElMenu.ruta,
                        icono: ElMenu.icono,
                        avatar: ElMenu.avatar,
                        background: ElMenu.background,
                        estado: ElMenu.estado,
                        dispositivo: ElMenu.dispositivo,
                        id_menu_padre: ElMenu.id_padre || 0
                    });
                }
            }
            //Finalmente creo la sesión y genero su token
            
            let sesion = new Session(
                UsuarioEncontrado[0].uuid+"",
                "Pendiente",
                idsRoles.join(','),
                app,
                new Date(),
                new Date(Date.now()+(1000*60*60*15000))
            );
            let sessionGuardada = await AuthController.RepoSession.save(sesion);
            //Genero el JWT
            const token = jwt.sign(
                {
                    sessionid: sessionGuardada.id
                },
                privateKey,
                { expiresIn: '15000h',algorithm: 'RS256' }
            );
            sessionGuardada.token = token;
            sessionGuardada = await AuthController.RepoSession.save(sesion);
            //Lo ajusto al antiguo formato para evitar problemas
            let Salida = {
                id_usuario:UsuarioEncontrado[0].uuid,
                id_aplicacion:app.id,
                usuario:UsuarioEncontrado[0].usuario,
                menus:MenusFormateados,
                roles: RolesFormateados,
                id_third:UsuarioEncontrado[0].id_third,
                third:{
                    fist_name:logginData.first_NAME,
                    second_name:logginData.second_NAME,
                    last_name:logginData.first_LASTNAME,
                    second_last_name:logginData.second_LASTNAME,
                    fullname:logginData.fullname,
                    document_type:logginData.document_TYPE,
                    document:logginData.document_NUMBER,
                    id_third_father:logginData.id_THIRD_FATHER,
                    id_person:logginData.id_PERSON,
                    img:logginData.img
                },
                third_father:{
                    document:logginData.docnumberfather,
                    document_type:logginData.docutypefather,
                    fullname:logginData.fullname
                },
                token
            };
            res.cookie("SESSIONID", token, {httpOnly:true, secure:true, sameSite:"none"}).status(200).json(Salida);
        }catch (e) {
            console.log(e.toString());
            res.status(404).json("Not found");
        }
    }

    public async Signin(req: Request, res: Response){
        try{
            let Input:{
                method:number,
                email: string,
                phone:string,
                password:string,
                third:{}
            } = req.body;
            // @ts-ignore
            //postear tercero
            //crear cuenta
            res.status(201).json("Created");
        }catch (e) {
            console.log(e.toString());
            res.status(500).json("Not found");
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////// TODO PARA LOS LA APP DE CLIENTES DE TIENDA 724 Y DENLINEA WEB ///////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public async LoginAsClient(req: Request, res: Response){
        try {
            //Buscamos la aplicación
            let app = await AuthController.RepoApplication.findOne(req.query.id_aplicacion);
            if(app == null){ res.status(404).json({ errors: ["App not found"] });return;}
            /////////////////////////
            let UsuarioEncontrado = await AuthController.RepoUsuario.find({
                where:{
                    usuario: req.body.usuario,
                    id_applicacion: app,
                },
                relations: ["roles"]
            });
            if(!bcrypt.compareSync(req.body.clave,UsuarioEncontrado[0].clave)){
                res.status(404).json("Not found");
                return;
            }
            //Verificamos si tiene alguna sesión activa
            // let PrevSession = await AuthController.RepoSession.find({
            //     where:{
            //         userID:UsuarioEncontrado[0].id_usuario,
            //         LogOut_Date:AfterDate(new Date())
            //     }
            // });
            // for(let n = 0;n<PrevSession.length;n++){
            //     let Session = PrevSession[n];
            //     Session.LogOut_Date = new Date();
            //     await AuthController.RepoSession.save(Session);
            // }
            //Listado de roles
            let roles = UsuarioEncontrado[0].roles;
            let RolesFormateados = [];
            let idsRoles: number[] = [];
            //Obtengo los menús asociados a los roles y los convierto de nuevo al formato anterior
            for(let n = 0;n < roles.length; n++){
                RolesFormateados.push({
                    id_rol:roles[n].id,
                    id_aplicacion:app.id,
                    rol:roles[n].Rol,
                    descripcion:roles[n].Descripcion
                });
                idsRoles.push(roles[n].id);
            }
            //Finalmente creo la sesión y genero su token
            let DateAdelantado = new Date();
            DateAdelantado.setFullYear(2030);
            let sesion = new Session(
                UsuarioEncontrado[0].uuid+"",
                "Pendiente",
                idsRoles.join(','),
                app,
                new Date(),
                DateAdelantado
            );
            let sessionGuardada = await AuthController.RepoSession.save(sesion);
            //Genero el JWT
            const token = jwt.sign(
                {
                    sessionid: sessionGuardada.id
                },
                privateKey,
                {algorithm: 'RS256' }
            );
            sessionGuardada.token = token;
            sessionGuardada = await AuthController.RepoSession.save(sessionGuardada);
            let Peticion = {
                url: URLS.tercero+"/persons/searchPersonFromId_Third?id_third="+UsuarioEncontrado[0].id_third,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    "Authorization":"3020D4:0DD-2F413E82B-A1EF04559-78CA",
                    "Key_Server":"3020D4:1FG-2U113E822-A1TE04529-68CF"
                },
                json:{}
            };
            let PersonConIDThird:any= await new Promise((resolve, reject) => {
                request.get(Peticion, function (error: any, response: any, body: any) {
                    if (error || response.statusCode != 200){
                        console.log("[PersonConIDThird]Error: ")
                        console.log(error || response.body)
                        reject("Body Null or Error");
                    }
                    else{
                        resolve(response);
                    }
                })
            });
            let elPerson:any = PersonConIDThird.body[0];
            //OBTENEMOS EL TERCERO COMPLETO CON SU PERSON Y DEMAS
            Peticion = {
                url: URLS.tercero+"/thirds?id_third="+UsuarioEncontrado[0].id_third,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    "Authorization":"3020D4:0DD-2F413E82B-A1EF04559-78CA",
                    "Key_Server":"3020D4:1FG-2U113E822-A1TE04529-68CF"
                },
                json:{}
            };
            let TerceroCompleto:any = await new Promise((resolve, reject) => {
                request.get(Peticion, function (error: any, response: any, body: any) {
                    if (error || response.statusCode != 200){
                        console.log("[TerceroCompleto]Error: ")
                        console.log(error || response.body)
                        reject("Body Null or Error");
                    }
                    else{
                        resolve(response);
                    }
                })
            });
            //Lo ajusto al antiguo formato para evitar problemas
            console.log("elPerson es ")
            console.log(elPerson)
            console.log(Object.keys(elPerson))
            let Salida = {
                uuid:UsuarioEncontrado[0].uuid,
                roles: RolesFormateados,
                third:TerceroCompleto.body,
                address:elPerson.address,
                phone:elPerson.phone,
                email:elPerson.city,
                genero:elPerson.genero,
                latitud:elPerson.latitud,
                longitud:elPerson.longitud,
                city_ID:elPerson["city_ID"],
                city_NAME:elPerson["city_NAME"],
                authheader:req.body.forceHeader ? token:false
            };
            // console.log("req.body.forceHeader es ")
            // console.log(req.body.forceHeader)
            // console.log("Salida es ")
            // console.log(Salida)
            res.cookie("SESSIONID", token, {httpOnly:true, secure:true, sameSite:"none"}).status(200).json(Salida);
        }catch (e) {
            console.log(e.toString());
            res.status(404).json("Not found");
        }
    }

    async RecoverPassword(req: Request, res: Response){
        try{
            let input:{mail:string,appid:number} = req.body;
            let usuario = await AuthController.RepoUsuario.find({
                where:{usuario:input.mail,id_applicacion:input.appid||26}
            });
            if(usuario.length == 1){
                AuthController.GenerateRecoveryLink(usuario[0]);
                res.json({status:"ok"});
            }else{
                throw "No user found";
            }
        }catch (e) {
            console.log(e.toString());
            res.status(404).json("Not found");
        }
    }

    private static async GenerateRecoveryLink(Usuario:Usuario){
        //Generamos el token
        let Token = new PasswordRecoveryToken(
            0,
            new Date(),
            new Date(Date.now()+(1000*60*60*2)),//2 horas para usar el token
            null,
            Usuario
        );
        Token = await AuthController.RepoPassTokens.save(Token);
        //Lo enviamos al correo
        let req = {
            url: URLS.facturacion+"/pedidos/SendEmail",
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "Key_Server":"3020D4:1FG-2U113E822-A1TE04529-68CF"
            },
            json: {
                "Destinatario": [Usuario.usuario],
                "CuerpoCorreo":"Se ha solicitado recuperar contraseña, para realizar el cambio, ingrese al siguiente link:<br>" +
                    "https://pruebas.tienda724.com/app/#/recovery?token="+Token.id+"<br>" +
                    "Si ud no ha solicitado el cambio, por favor ignore el correo.",
                "Asunto":"Recuperación Contraseña"
            }
        };
        let respuesta:any = await new Promise(resolve => {
            request(req, function (error: any, response: any, body: any) {
                if (error || (response.statusCode != 200 && response.statusCode != 201)){
                    console.log("llega a error");
                    console.log(error);
                    resolve([false,body]);
                }
                else{
                    console.log("llega a no error");
                    resolve([true,body]);
                }
            })
        });
        if(!respuesta[0]){
            console.log("Ocurrió un error al enviar el link de recuperación de contraseña:");
            console.log(respuesta[1])
        }
    }

    public async CheckRecoveryToken(req: Request, res: Response){
        try {
            let resultado = await AuthController.RepoPassTokens.find({
                id:req.body.id,
                expirationDate: AfterDate(new Date()),
                used:0
            })
            if(resultado.length > 0){
                res.status(200).json();
            }else{
                res.status(404).json("Not found");
            }
        }catch (e) {
            console.log(e.toString());
            res.status(404).json("Not found");
        }
    }

    public async UseRecoveryToken(req: Request, res: Response){
        try {
            let resultado = await AuthController.RepoPassTokens.find({
                where:{
                    id:req.body.id,
                    expirationDate: AfterDate(new Date()),
                    used:0
                },
                relations:["User"]
            })
            if(resultado.length == 1){
                //Encontró un token valido, procedemos a cambiar la clave del usuario e invalidar el token
                console.log("")
                await AuthController.db.transaction(async transactionalEntityManager => {
                    let Token = resultado[0];
                    Token.used = 1;
                    Token.useDate = new Date();

                    let UsuarioAOperar = resultado[0].User;
                    if(UsuarioAOperar == null){ throw "User not found"; }
                    UsuarioAOperar.clave = await bcrypt.hash(req.body.pass, 12);
                    await transactionalEntityManager.save(UsuarioAOperar);
                    await transactionalEntityManager.save(Token);
                    res.status(200).send();
                });
            }else{
                res.status(404).json("Not found");
            }
        }catch (e) {
            console.log(e.toString());
            res.status(404).json("Not found");
        }
    }

    async CheckPerson(req: Request, res: Response){//Esto es propiamente para la app
        try {
            let resultado = await AuthController.CheckPersonMethod(req.body.docnumber);
            if(resultado == null){
                res.send({response:"Not Found"});
            }else{
                let UsuarioBusqueda:Usuario[] = await AuthController.RepoUsuario.find({where:{id_usuario:resultado.id_PERSON_REAL,id_applicacion:req.body.appid||26}});
                if(UsuarioBusqueda.length == 0){
                    res.send(resultado);
                }else{
                    res.status(404).json("Used");
                }
            }
        }catch (e) {
            console.log(e.toString());
            res.status(404).json("Not found");
        }
    }

    static async CheckPersonMethod(docnumber:string){
        let Peticion = {
            url: URLS.tercero+"/persons/search?doc_person="+docnumber,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                "Authorization":"3020D4:0DD-2F413E82B-A1EF04559-78CA",
                "Key_Server":"3020D4:1FG-2U113E822-A1TE04529-68CF"
            },
            json:{}
        };
        let respuesta:any = await new Promise((resolve, reject) => {
            request.get(Peticion, function (error: any, response: any, body: any) {
                if (error || response.statusCode != 200){
                    console.log("[respuesta]Error: ")
                    console.log(error || response.body)
                    reject("Body Null or Error");
                }
                else{
                    resolve(response);
                }
            })
        });
        // console.log("respuesta es ");
        // console.log(respuesta.body);
        let personExiste = false;
        //VERIFICACIÓN ADICIONAL, esto ya que retorna a personas con documentos similares
        let personBusqueda:any = null;
        for(let n = 0;n<respuesta.body.length;n++){
            let person = respuesta.body[n];
            if(person.document_NUMBER == docnumber){
                personBusqueda = person;
                break;
            }
        }
        return personBusqueda;
        //
    }

    async CreateClient(req: Request, res: Response){
        try{
            let BodyInput:{
                usuario:string,
                clave:string,
                appid:number,
                third:{
                    firstname:string,
                    secondname:string,
                    firstlastname:string,
                    secondlastname:string,
                    iddocumenttype:number,
                    docnumber:string,
                    direccion:string,
                    idcity:string,
                    telefono:string,
                    email:string,
                    birthDate:Date,
                    gender:number,
                    lat:number,
                    lng:number
                }
            };
            BodyInput = req.body;
            //Primero se verifica si existe el tercero
            let Peticion = {};
            let personBusqueda:any = await AuthController.CheckPersonMethod(BodyInput.third.docnumber);
            let RespuestaCreacionUserThird:any;
            let personPreviamenteCreada = false;
            if(personBusqueda == null){
                personPreviamenteCreada = false;
                //EL PERSON NO EXISTE, SE CREA
                Peticion = {
                    url: URLS.tercero+"/thirds/CreateThirdUser",
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        "Authorization":"3020D4:0DD-2F413E82B-A1EF04559-78CA",
                        "Key_Server":"3020D4:1FG-2U113E822-A1TE04529-68CF"
                    },
                    json:BodyInput.third
                };
                console.log("BodyInput.third es ");
                console.log(BodyInput.third);
                //Esto retorna la ID
                RespuestaCreacionUserThird = await new Promise((resolve, reject) => {
                    request.post(Peticion, function (error: any, response: any, body: any) {
                        if (error || response.statusCode != 200){
                            console.log("[RespuestaCreacionUserThird]Error: ")
                            console.log(error || response.body)
                            reject("Body Null or Error");
                        }
                        else{
                            resolve(response);
                        }
                    })
                });
                console.log("RespuestaCreacionUserThirdPre es ");
                console.log(RespuestaCreacionUserThird.body);
                //Verificamos si hubo error
                if(RespuestaCreacionUserThird.body.message != null){
                    if(RespuestaCreacionUserThird.message.includes("ALertas del sistema") || RespuestaCreacionUserThird.message.includes("ALerta del sistema")){
                        throw "Error at request";
                    }
                }
            }else{
                //EL PERSON EXISTE, SEGUIMOS CON VERIFICAR EL USUARIO
                RespuestaCreacionUserThird = {body:personBusqueda.id_PERSON};//esto es el id_THIRD
                personPreviamenteCreada = true;
            }
            //VERIFICAR EL USUARIO
            //OBTENEMOS EL TERCERO COMPLETO CON SU PERSON Y DEMAS
            Peticion = {
                url: URLS.tercero+"/thirds?id_third="+RespuestaCreacionUserThird.body,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    "Authorization":"3020D4:0DD-2F413E82B-A1EF04559-78CA",
                    "Key_Server":"3020D4:1FG-2U113E822-A1TE04529-68CF"
                },
                json:{}
            };
            let TerceroCompleto:any = await new Promise((resolve, reject) => {
                request.get(Peticion, function (error: any, response: any, body: any) {
                    if (error || response.statusCode != 200){
                        console.log("[TerceroCompleto]Error: ")
                        console.log(error || response.body)
                        reject("Body Null or Error");
                    }
                    else{
                        resolve(response);
                    }
                })
            });
            console.log("RespuestaCreacionUserThird es ");
            console.log(RespuestaCreacionUserThird.body);
            console.log("TerceroCompleto es ");
            console.log(TerceroCompleto.body);
            let UUID =  TerceroCompleto.body[0].profile.id_person;
            //Busco el usuario con ese UUID
            let UsuarioBusqueda:Usuario[] = await AuthController.RepoUsuario.find({where:{uuid:UUID,id_applicacion:BodyInput.appid||26}});
            if(UsuarioBusqueda.length > 0){//Significa que encontró el usuario
                if(personBusqueda != null){throw "Document Used"}
                throw "User used";
            }else{//No existe, se crea con dicho UUID
                //Actualizo los datos del person con lo que llegó
                if(personPreviamenteCreada){
                    let body = {
                        id_person:UUID,
                        phone:BodyInput.third.telefono,
                        mail:BodyInput.usuario,
                        address:BodyInput.third.direccion,
                        id_city:BodyInput.third.idcity,
                        lat:BodyInput.third.lat,
                        lng:BodyInput.third.lng,
                        genero:BodyInput.third.gender,
                        birthDate:BodyInput.third.birthDate
                    }
                    console.log("[ActualizarDatos]URL es : ")
                    console.log(body)
                    Peticion = {
                        url: URLS.tercero+"/persons/actualizar_person",
                        method: 'PUT',
                        headers: {
                            'Content-Type': 'application/json',
                            "Authorization":"3020D4:0DD-2F413E82B-A1EF04559-78CA",
                            "Key_Server":"3020D4:1FG-2U113E822-A1TE04529-68CF"
                        },
                        json:body
                    };
                    let ActualizarDatos:any = await new Promise((resolve, reject) => {
                        request.post(Peticion, function (error: any, response: any, body: any) {
                            if (error || response.statusCode != 200){
                                console.log("[ActualizarDatos]Error: ")
                                console.log(error || response.body)
                                reject("Body Null or Error");
                            }
                            else{
                                resolve(response);
                            }
                        })
                    });
                }
                //Obtengo la app a la que pertenece
                let app = await AuthController.RepoApplication.findOne(BodyInput.appid||26);
                if(app == null){
                    res.status(404).json({ errors: ["App not found"] });
                    return;
                }
                //Obtengo el rol default para este caso
                let Rol = await AuthController.RepoRol.find({where:{
                    id:2870
                    }});
                if(Rol.length == 0){
                    res.status(404).json({ errors: ["Rol not found"] });
                    return;
                }
                let PassHasheada = await bcrypt.hash(BodyInput.clave,12);
                let UsuarioNuevo = new Usuario(UUID,BodyInput.usuario,PassHasheada,[],app,RespuestaCreacionUserThird.body);
                UsuarioNuevo.createDate = new Date();
                UsuarioNuevo = await AuthController.RepoUsuario.save(UsuarioNuevo);
                UsuarioNuevo = (await AuthController.RepoUsuario.find({where:{
                        usuario:UsuarioNuevo.usuario,
                        id_applicacion:UsuarioNuevo.id_applicacion
                    }}))[0];
                UsuarioNuevo.roles = Rol;
                UsuarioNuevo = await AuthController.RepoUsuario.save(UsuarioNuevo);
                if(UsuarioNuevo == null){
                    throw "Error Al Crear Usuario";
                }
                //EL CORREO DE BIENVENIDA
                Peticion = {
                    url: URLS.facturacion+"/pedidos/SendEmail",
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        "Authorization":"3020D4:0DD-2F413E82B-A1EF04559-78CA",
                        "Key_Server":"3020D4:1FG-2U113E822-A1TE04529-68CF"
                    },
                    json:{
                        "Destinatario": [BodyInput.usuario],
                        "Asunto":"Bienvenido a Tiendas 724",
                        "CuerpoCorreo":"Hola "+BodyInput.third.firstname+", bienvenido a Tiendas 724. \n\nTu cuenta ha sido creada correctamente."
                    }
                };
                let SendMail:any = await new Promise((resolve, reject) => {
                    request.post(Peticion, function (error: any, response: any, body: any) {
                        if (error || response.statusCode != 200){
                            console.log("[ActualizarDatos]Error: ")
                            console.log(error || response.body)
                            reject("Body Null or Error");
                        }
                        else{
                            resolve(response);
                        }
                    })
                });
            }
            res.status(200).json();
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e)
        }
    }
}

export const AfterDate = (date: Date) => Between(date, addYears(date, 100));
export const BeforeDate = (date: Date) => Between(subYears(date, 100), date);