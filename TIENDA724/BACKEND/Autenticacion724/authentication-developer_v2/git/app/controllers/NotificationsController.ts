import { Request, Response } from "express";
import {Connection} from "typeorm";
import {Repository} from "typeorm/repository/Repository";
import {Usuario} from "../models/Usuario";
import {Session} from "../models/Session";
import {FCMtokens} from "../models/FCMtokens";
import * as admin from 'firebase-admin';
import path from "path";
import {URLS} from "../config/Urlbase";
var request = require("request");

export class NotificationsController {
    //public db : Connection = connection;
    static RepoUsers:Repository<Usuario>;
    static RepoFCMTokens:Repository<FCMtokens>;
    static MessagingService:admin.messaging.Messaging;

    constructor(db: Connection) {
        //this.db = db;c
        NotificationsController.RepoUsers = db.manager.getRepository(Usuario);
        NotificationsController.RepoFCMTokens = db.manager.getRepository(FCMtokens);
        //
        var serviceAccount = path.join(__dirname, '..','..', 'jwt', 'tienda724-2b59f-firebase-adminsdk-isu4a-7f2f11d5ee.json');
        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: "https://tienda724-2b59f.firebaseio.com"
        });
        NotificationsController.MessagingService = admin.messaging();
    }

    public async UpdateFCMtoken(req: Request, res: Response) {
        try{
            // @ts-ignore
            let Session:Session = req.session;
            let User = await NotificationsController.RepoUsers.find({where:{uuid:parseInt(Session.userID)},relations:["FCMtokens"]});
            if(User[0] != null && User[0].id_third != null){
                if(User[0].FCMtokens.length > 10){
                    let ToDeleteToken = User[0].FCMtokens.pop();
                    if(ToDeleteToken != null){await NotificationsController.RepoFCMTokens.remove(ToDeleteToken);}
                }
                let encontrado = false;
                for(let n = User[0].FCMtokens.length-1;n>=0;n--){
                    let token = User[0].FCMtokens[n];
                    if(token.Token == req.body.token){
                        encontrado = true;
                    }
                    if((token.AddDate.getTime()+(1000*60*60*9)) < Date.now()){//Expiró la sesión
                        let ToDeleteToken = User[0].FCMtokens.pop();
                        if(ToDeleteToken != null){await NotificationsController.RepoFCMTokens.remove(ToDeleteToken);}
                    }
                }
                if(!encontrado){
                    let Token = new FCMtokens(req.body.token,new Date(),User[0]);
                    Token = await  NotificationsController.RepoFCMTokens.save(Token);
                    User[0].FCMtokens.push(Token);
                    User[0] = await NotificationsController.RepoUsers.save(User[0]);
                }
            }
            res.json({status:"OK"});
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e)
        }
    }
    public async SendFCMpushFromClient(req: Request, res: Response) {
        try{
            let body:{
                id_store:number,
                title:string,
                body:string,
                data:any
            } = req.body;
            //Obtengo todos los id_third relacionados a la tienda
            console.log(body)
            console.log("URL ES ",URLS.tercero+"/thirds/IDThirdsFromIDStore?id_store="+body.id_store)
            let Peticion = {
                url: URLS.tercero+"/thirds/IDThirdsFromIDStore?id_store="+body.id_store,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    "Authorization":"3020D4:0DD-2F413E82B-A1EF04559-78CA",
                    "Key_Server":"3020D4:1FG-2U113E822-A1TE04529-68CF"
                },
                json:{}
            };
            let ids:any = await new Promise( (resolve, reject) => {
                request.get(Peticion, function (error: any, response: any, body: any) {
                    if (error || response.statusCode != 200){
                        console.log("[ids]Error: ")
                        console.log(error || response.body)
                        reject("Body Null or Error");
                    }
                    else{
                        resolve(response);
                    }
                })
            });
            ids = ids.body;
            console.log(ids)
            ///////
            const Usuarios = await NotificationsController.RepoUsers
                .createQueryBuilder("Usuario")
                .leftJoinAndSelect("Usuario.FCMtokens","FCMtokens")
                .where("Usuario.id_third IN (:...ids) and Usuario.id_applicacion = 21", {
                    ids: ids,
                })
                .getMany();
            //
            let tokens:Array<string> = [];
            // @ts-ignore
            //let UsuarioWithTokens = await NotificationsController.RepoUsers.find({where:{id_third:body.id_third},relations:["FCMtokens"]});
            for(let n = 0;n<Usuarios.length;n++){
                let Usuario = Usuarios[n];
                for(let n = 0;n<Usuario.FCMtokens.length;n++){
                    if(Usuario.FCMtokens[n].Token == "-1"){continue;}
                    tokens.push(Usuario.FCMtokens[n].Token);
                }
            }
            let Mensaje:admin.messaging.MulticastMessage = {
                tokens: tokens,
                notification:{
                    title: body.title,
                    body:body.body
                },
                data:body.data
            };
            if(tokens.length == 0){
                res.json({status:"Empty"});
            }else{
                //
                await NotificationsController.MessagingService.sendMulticast(Mensaje);
                res.json({status:"OK"});
            }
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e)
        }
    }

    public async SendFCMpush(req: Request, res: Response) {
        try{
            let body:{
                id_third:number,
                title:string,
                body:string,
                data:any,
                idapp:number
            } = req.body;
            // @ts-ignore
            let UsuarioWithTokens = await NotificationsController.RepoUsers.find({where:{id_third:body.id_third,id_applicacion:body.idapp||26},relations:["FCMtokens"]});
            if(UsuarioWithTokens.length == 1){
                let tokens:Array<string> = [];
                for(let n = 0;n<UsuarioWithTokens[0].FCMtokens.length;n++){
                    if(UsuarioWithTokens[0].FCMtokens[n].Token == "-1"){continue;}
                    tokens.push(UsuarioWithTokens[0].FCMtokens[n].Token);
                }
                //tokens.push("f0aocu_pQSmitVWfkJftcn:APA91bFn6c5YhN1s5m6e22nAGcP_Im8w4mTj9xUkUK6DS-_0YiGeBsBXfYEbNkvuuD0hB-lXkmoFk-wuIN44Mzx1Lqf2wRbpdiqx2Y1fIAETTrJ_FjusuLR5VKvlnTFoSaxSvK16E3H3");

                let Mensaje:admin.messaging.MulticastMessage = {
                    tokens: tokens,
                    notification:{
                        title: body.title,
                        body:body.body
                    },
                    data:body.data,
                    android:{
                        notification:{
                            channelId:"push",
                            //channelId:"fcm_fallback_notification_channel",
                            //sound:"alert.wav",
                            //notificationCount: 1,
                            //defaultSound:true
                        },
                        ttl: 20000,
                    },
                    apns: {
                        payload: {
                            aps: {
                                badge: 1,
                                sound: 'default'
                            }
                        }
                    },
                };
                if(tokens.length == 0){
                    res.json({status:"Empty"});
                }else{
                    //
                    let resultado = await NotificationsController.MessagingService.sendMulticast(Mensaje);
                    console.log(resultado)
                    res.json({status:"OK", resultado:resultado,tokens:tokens});
                }
            }else if(UsuarioWithTokens.length == 0){res.json({status:"Empty"});}
            else{throw "Data Error"}
        }catch (e) {
            console.log(e.toString());
            res.status(500).json(e)
        }
    }
}
