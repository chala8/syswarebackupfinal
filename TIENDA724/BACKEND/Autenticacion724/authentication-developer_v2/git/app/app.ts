// lib/app.ts
import express from "express";
import cors from "cors";
import * as bodyParser from "body-parser";
import { Routes } from "./config/routes";
import "reflect-metadata";
import {Connection} from "typeorm";
import {connection} from "./config/database";
import {Application} from "./models/Application";
import {Menu} from "./models/Menu";
import {Rol} from "./models/Rol";
import {Usuario} from "./models/Usuario";
import cookieParser from "cookie-parser";

class App {
  public app: express.Application;
  // @ts-ignore
  public routePrv: Routes;
  public db : Connection = connection;

  constructor() {
    //Iniciolizo express como tal, como libreria
    this.app = express();
    //Defino los parametros iniciales
    this.Setup();
  }

  async Setup(){
    //Defino la configuracón basica de las peticiones
    this.config();
    //Conecto el servidor con la base de datos e inicializo todos los modelos
    try {
      //Espero a que se realice la conexión
      await this.db.connect();
      //Acá ya garantiza que está conectado
      console.log("Conexión a la DB establecida");
      //Verifico que los triggers estén creados
      await this.CheckTriggers();
      //Defino las rutas
      this.routePrv = new Routes(this.db);
      this.routePrv.routes(this.app);
    }catch (e) {
      console.log("Error en la Conexión a la DB: "+e)
    }
  }

  OrigenesPermitidos = [
    "http://localhost:4200",
    "http://localhost:8200",
    "http://localhost:8100",
    "http://localhost",
    "https://www.tienda724.com",
    "https://tienda724.com",
    "www.tienda724.com",
    "https://pruebas.tienda724.com",
    "pruebas.tienda724.com",
    "https://www.denlinea.com",
    "https://denlinea.com",
    "www.denlinea.com",
    "denlinea.com",
    "https://restobar724.com",
    "https://www.restobar724.com",
    "www.restobar724.com",
    "restobar724.com",
    "mesas.restobar724.com"
  ]

  private config(): void {
    //Configuración del cors
    this.app.use(cors({credentials:true,origin:this.OrigenesPermitidos}));
    this.app.use((req, res, next) => {
      res.setHeader(
          'Access-Control-Allow-Methods',
          'OPTIONS, GET, POST, PUT, PATCH, DELETE'
      );
      // res.setHeader('Access-Control-Allow-Credentials', "true");
      res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
      next();
    });
    //Casteo del bodu a un objeto json y las cookies
    this.app.use(bodyParser.json());
    this.app.use(cookieParser());
    this.app.use(bodyParser.urlencoded({ extended: false }));
  }

  private async CheckTriggers(){
    //Verificar Application
    await Application.CheckIFTriggerOk(this.db);
    //Verificar Menu
    await Menu.CheckIFTriggerOk(this.db);
    //Verificar Rol
    await Rol.CheckIFTriggerOk(this.db);
    //Verificar Usuario
    await Usuario.CheckIFTriggerOk(this.db);
  }
}

export default new App().app;
