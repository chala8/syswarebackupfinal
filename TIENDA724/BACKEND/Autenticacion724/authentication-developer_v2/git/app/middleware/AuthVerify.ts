import {Connection} from "typeorm";
import {connection} from "../config/database";
import {Session} from "../models/Session";
import {NextFunction} from "express";

const jwt = require('jsonwebtoken');
var fs = require('fs');
var path = require('path');
var ketPath = path.join(__dirname, '..','..', 'jwt', 'private.key.pub');
const publicKey = fs.readFileSync(ketPath, 'utf8');
const verifyOptions = {
    algorithms: ['RS256']
};

export class AuthVerify {
    public static db : Connection = connection;

    constructor(db: Connection) {
        AuthVerify.db = db;
    }

    static async Verify(authHeader:any){
        try{
            //OBTENEMOS EL TOKEN
            if (!authHeader) {
                // noinspection ExceptionCaughtLocallyJS
                throw new Error('Not authenticated.');
            }
            //const token = authHeader.split(' ')[1];
            const token = authHeader;
            //VAR A GUARDAR
            let decodedToken;
            try {
                //VERIFICAMOS SI EL TOKEN ES VALIDO Y NO HA EXPIRADO
                decodedToken = jwt.verify(token, publicKey,verifyOptions);
            } catch (err) {
                // noinspection ExceptionCaughtLocallyJS
                throw err;
            }
            if (!decodedToken) {
                // noinspection ExceptionCaughtLocallyJS
                throw new Error('Not authenticated.');
            }
            //////////////////////
            let session = await AuthVerify.db.manager.getRepository(Session).find({id:decodedToken.sessionid});
            if (!decodedToken.sessionid) {
                // noinspection ExceptionCaughtLocallyJS
                throw new Error('Not Found');
            }
            if(session[0].LogOut_Date.getTime() < Date.now()){
                // noinspection ExceptionCaughtLocallyJS
                throw new Error('Session ended.');
            }else{
                return [1,session[0]];
            }
        }catch (e) {
            return [-1,e.toString()];
        }
    }

    public async ManuelVerify(token:any){
        // @ts-ignore
        let retorno : any[] = await AuthVerify.Verify(token);
        if(retorno[0] == 1){
            return retorno[1];
        }else{
            // noinspection ExceptionCaughtLocallyJS
            throw retorno[1];
        }
    }

    public async MiddleVerify(req:Request, res:Response, next:NextFunction){
        try {
            // @ts-ignore
            let retorno : any[] = await AuthVerify.Verify(req.cookies['SESSIONID']);
            if(retorno[0] == 1){
                // @ts-ignore
                req.session = retorno[1];
                next();
            }else{
                console.log("req.headers[\"authorization\"] es ")
                // @ts-ignore
                console.log(req.headers["authorization"])
                // @ts-ignore
                if(req.headers["authorization"] != null){
                    // @ts-ignore
                    let retorno : any[] = await AuthVerify.Verify(req.headers["authorization"]);
                    if(retorno[0] == 1){
                        // @ts-ignore
                        req.session = retorno[1];
                        next();
                    }else{
                        // @ts-ignore
                        if(req.headers["key_server"] != null && req.headers["key_server"] == "3020D4:1FG-2U113E822-A1TE04529-68CF"){
                            next();
                        }else{
                            // noinspection ExceptionCaughtLocallyJS
                            throw retorno[1];
                        }
                    }
                }else{
                    // @ts-ignore
                    if(req.headers["key_server"] != null && req.headers["key_server"] == "3020D4:1FG-2U113E822-A1TE04529-68CF"){
                        next();
                    }else{
                        // noinspection ExceptionCaughtLocallyJS
                        throw retorno[1];
                    }
                }
            }

        } catch (error) {
            //console.log(error);
            // @ts-ignore
            res.status(404).json({ message: error});
        }
    }
}