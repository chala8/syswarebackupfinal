import {Connection} from "typeorm";
import {connection} from "../config/database";
import {Session} from "../models/Session";
import {NextFunction} from "express";


export class RolesCheck {

    constructor() {
    }

    public async MiddleCheckRoles(req:Request, res:Response, next:NextFunction,rolesList:Array<string>){
        try{
            // @ts-ignore
            let sesion: Session = req.session;
            let rolesSession = sesion.roles.split('|');
            let authorized = false;
            for(let n = 0;n<rolesSession.length;n++){
                let rolSession = rolesSession[n];
                for(let j = 0;j<rolesList.length;j++){
                    if(rolSession == rolesList[j]){
                        authorized = true;
                    }
                    if(authorized){break;}
                }
                if(authorized){break;}
            }
            if(authorized){
                next();
            }else{
                // noinspection ExceptionCaughtLocallyJS
                throw "Not Authorized";
            }

        } catch (error) {
            //console.log(error);
            // @ts-ignore
            res.status(404).json({ message: error});
        }
    }
}