rmdir /s /q "build" & (
	npm run tsc && (
		copy package.json build & (
			del auth.zip & (
				zip -r auth.zip build jwt & (
					pause
				)
			)
		)
	) || (
	  pause
	)
)