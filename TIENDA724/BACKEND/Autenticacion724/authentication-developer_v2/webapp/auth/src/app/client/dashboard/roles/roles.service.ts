import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable'

// Constante
import {urlbase} from '../../../shared/url';
import { LocalStorage } from '../../../shared/localStorage';
import { Token } from '../../../shared/token';

// Data model
import { Rol } from './rol'
import { RolDTO } from './rolDTO'

import { ResponseAPI } from '../../../shared/utils/ResponseAPI';


@Injectable()
export class RolesService {
  
    private api_uri = urlbase[0] + '/roles';


    private headers = new Headers({'Content-Type': 'application/json'});
    private options: RequestOptions;
    private token: Token;

      constructor(private http: Http, private locStorage: LocalStorage) {
        let aux =  this.locStorage.getData();
        /*}let headers = new Headers({
          'Content-Type': 'application/json',
          'Authorization' : this.locStorage.getTokenValue()
        });*/
        if(aux===null){

        }else{
            
        }

            this.token= new Token(null,null,null);
            this.token.id_usuario_app=aux.id_usuario_app;
            this.token.usuario=aux.usuario;
            this.options = new RequestOptions(this.headers);
        
      }

      public getRoles= (id_aplicacion:number|null,  id_rol:number|null, rol:string|null, descripcion:string|null): Observable<Rol[]> => {

            let params: URLSearchParams = new URLSearchParams();
            params.set('id_aplicacion',id_aplicacion==null?null:id_aplicacion+'');
            params.set('id_rol',  id_rol==null?null:id_rol+'');
            params.set('rol',rol==null?null:rol+'');
            params.set('descripcion', descripcion==null?null:descripcion+'');
            
            
            let myOption: RequestOptions = this.options;
            myOption.search = params;
            

        return this.http.get(this.api_uri, myOption)
            .map((response: Response) => <Rol[]>response.json())
            .catch(this.handleError);
    }

       public getMenusPorRol= (id_rol:number|null,  id_menu:number|null): Observable<number[]> => {

            let params: URLSearchParams = new URLSearchParams();
            
            params.set('id_rol',  id_rol==null?null:id_rol+'');
            params.set('id_menu',id_menu==null?null:id_menu+'');
            
            
            let myOption: RequestOptions = this.options;
            myOption.search = params;
            

        return this.http.get(`${this.api_uri}/menu`, myOption)
            .map((response: Response) => <number[]>response.json())
            .catch(this.handleError);
    }



      public postRol = (rolDTO: RolDTO): Observable<Response> => {
        let toAdd = JSON.stringify({rolDTO});
         let url=`${this.api_uri}?id_usuario_app=${this.token.id_usuario_app}`
        return this.http.post(url, rolDTO, this.options)
            .map((response: Response) => {
              
                  
                  return response;
                })
            .catch(this.handleError);

    }

      public putRolesMenu = (id_rol:number, id_menuRolLista:number[]): Observable<Response> => {
        let toAdd = JSON.stringify({id_menuRolLista});
         let url=`${this.api_uri}/menu?id_rol=${id_rol}`
        return this.http.post(url, id_menuRolLista, this.options)
            .map((response: Response) => {
              
                  
                  return response;
                })
            .catch(this.handleError);

    }

     public deleteRolesMenu = (id_rol:number, id_menuRolLista:number[]): Observable<Response> => {
        let toAdd = JSON.stringify({id_menuRolLista});
         let url=`${this.api_uri}/menu?id_rol=${id_rol}`
        return this.http.put(url, id_menuRolLista, this.options)
            .map((response: Response) => {
              
                  
                  return response;
                })
            .catch(this.handleError);

    }

    

    private handleError (error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
        errMsg = error.message ? error.message : error.toString();
        }
        alert(errMsg);
        return Observable.throw(errMsg);
    }

}
