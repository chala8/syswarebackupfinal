import { BrowserModule } from '@angular/platform-browser';
import {HttpModule, Http} from '@angular/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { MdlModule } from '@angular-mdl/core';
import 'hammerjs'



/***************************************************************
 * Carga el Componente Principal y Rutas de toda al APP  *
 ***************************************************************/

import { AppComponent } from './app.component';
import { AppRouting } from './app.routing';
import { LocalStorage } from './shared/localStorage';
import { AuthGuard } from './shared/guard/auth.guard';

/***************************************************************
 * Llamar a todos los Modulos que tiene la App  *

 ***************************************************************/
import { MyOwnCustomMaterialModule } from './app.material.module';
import { AplicacionModule } from './client/profile/aplicacion/aplicacion.module'
import { AuthenticationModule } from './authentication/authentication.module'
import { DashboardModule } from './client/dashboard/index';
import { ProfileModule } from './client/profile/profile.module'
import { WelcomeModule } from './welcome/welcome.module';




// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        MdlModule,
         TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [Http]
            }
        }),
        RouterModule.forRoot(AppRouting),
        BrowserAnimationsModule,
       
        MyOwnCustomMaterialModule,
        WelcomeModule,
        AuthenticationModule,
        DashboardModule,
        ProfileModule,
        AplicacionModule
    ],
    providers: [AuthGuard,LocalStorage],
    bootstrap: [AppComponent]
})
export class AppModule { }
