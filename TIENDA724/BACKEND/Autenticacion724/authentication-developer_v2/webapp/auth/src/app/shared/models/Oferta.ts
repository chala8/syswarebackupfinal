import { Dato } from './Dato';
export class Oferta{

    id_oferta:number;
    dato:Dato;
    imagen_url:string;
    fecha_inicio:number;
    fecha_fin:number;
    es_porcentaje:number;
    valor:number;
    estado:number;

    constructor(
        dato:Dato,
        imagen_url:string,
        fecha_inicio:number,
        fecha_fin:number,
        es_porcentaje:number,
        valor:number,
        estado:number
    ){}
}