import { Dato } from './Dato';
import { Marca } from './Marca';
import { Categoria } from './Categoria';
export class Producto{

        id_producto:number;
        imagen_url:string;
        dato:Dato;
        marca:Marca;
        categoria:Categoria;

    constructor(
        id_producto:number,
        imagen_url:string,
        dato:Dato,
        marca:Marca,
        categoria:Categoria
    ){}


}
