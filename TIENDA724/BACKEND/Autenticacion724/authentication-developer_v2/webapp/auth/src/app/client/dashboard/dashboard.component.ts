import { Component, OnInit, ElementRef, ViewEncapsulation } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  moduleId: module.id,
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls:['dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    constructor(
    private router: Router,
    private _element: ElementRef) {
     }

  ngOnInit() {
  }
  

  Login() {
    let link = ['/auth'];
    this.router.navigate(link);
  }

 Logout() {
   
  }

  goIndex() {
    let link = ['/'];
    this.router.navigate(link);
  }
  goDashboard() {
    let link = ['/dashboard'];
    this.router.navigate(link);
  }

  navItems = [
    {icon_nav: 'dashboard', name: 'Dashboard', route: '/dashboard/home'},
    //{icon: 'users/avatars/01-avatar.svg', name: 'Usuarios', route: '/dashboard/users',description: 'Usuarios de Plataforms',img: 'users/02-user.jpg'},
   
  ];

    toggleFullscreen() {
    let elem = this._element.nativeElement.querySelector('.demo-content');
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.webkitRequestFullScreen) {
      elem.webkitRequestFullScreen();
    } else if (elem.mozRequestFullScreen) {
      elem.mozRequestFullScreen();
    } else if (elem.msRequestFullScreen) {
      elem.msRequestFullScreen();
    }
  }
}
