import { Component, OnInit,Inject } from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { FormGroup, FormBuilder , Validators } from '@angular/forms';
import { Location } from '@angular/common';
import {MdDialog, MdDialogRef,MD_DIALOG_DATA} from '@angular/material';


/********************************
 *          Constantes
 ********************************/
import { LocalStorage }  from '../../../../shared/localStorage';

/********************************
 *          Modelos
 ********************************/
import { Usuario } from '../usuario';
import { UsuarioDTO } from '../usuarioDTO';

import { Rol } from '../../roles/rol';

/********************************
 *          Servicios
 ********************************/
import { UsuariosService } from '../usuarios.service';
import { RolesService } from '../../roles/roles.service';

@Component({
  selector: 'app-usuario-nuevo',
  templateUrl: './usuario-nuevo.component.html',
  styleUrls: ['./usuario-nuevo.component.scss']
})
export class UsuarioNuevoComponent implements OnInit {


   usuario:Usuario;
   usuarioDTO:UsuarioDTO;
   id_aplicacion=null;
   action = 'Crear';
   title = 'Nuevo Usuario';
   form: FormGroup;
   esFormEditable=false;
   id_usuario=null;





      constructor(public dialogRef: MdDialogRef<UsuarioNuevoComponent>,
              public fb: FormBuilder,
              public locStorage: LocalStorage,
              public dialog: MdDialog,
              private route: ActivatedRoute,
              public router: Router,
              public location: Location,
              private rolesService:RolesService,
              @Inject(MD_DIALOG_DATA)public data: any,
              private usuariosService:UsuariosService) {

                this.usuarioDTO= new UsuarioDTO(null,null,null,null,null);
                this.controlsCreate();
                this.esFormEditable=data['editable'];

                console.log(data['editable']);
                  if(this.esFormEditable){
                    this.usuario=data['usuario'];
                    this.loadData();
                    this.action="Modificar";
                    this.title="Modificar Usuario "+this.usuario.usuario;
                  }

                 console.log(this.usuario);
                 this.obtenerQueryParams();

              }


  ngOnInit() {

  }

      loadData(){
        this.form.patchValue({
            usuario:this.usuario.usuario,
            clave:this.usuario.clave
        });
        this.id_usuario=this.usuario._uuid;

    }


   obtenerQueryParams(){
        let seb= this.route.queryParams.subscribe(params => {

            // Defaults to 0 if no query param provided.
            this.id_aplicacion = +params['app'] || null;
        });

        if(this.id_aplicacion==null){
            alert("Faltan Mas datos de la Aplicación");
            this.aPerfil();
        }else{


                this.obtenerRoles();



                if(this.id_usuario!=null){

                  this.obtenerIdRolesUsuario(null,this.id_usuario)
              }else{
                  alert("No Se reconoce el UUID");
              }

        }

  }

   // Obtiene los Roles de una aplicación
      public obtenerRoles(){
        this.rolesService.getRoles(this.id_aplicacion,null,null,null)
            .subscribe((data: Rol[]) => this.roles = data,
              error => console.log(error),
                    () => {

                      //console.log('Get all ROLES complete');
                  });

      }



  addRolUsuario(index,id_rol:number){
     let enLista=null;
     if(this.estaRolEnLista(this.listaRolesPorUsuario,id_rol)){ //Es para borrar



        if (this.estaRolEnLista(this.BorrarlistaRoles,id_rol)){ //Sacar de la lista

            let posicion=this.BorrarlistaRoles.indexOf(id_rol);
            if (posicion!==-1){
                this.BorrarlistaRoles.splice(posicion,1);
            }

        }else{ // Lo agragamos a la lista
          this.BorrarlistaRoles.push(id_rol)
        }

      }else{



        enLista=this.id_RolUsuarioLista.find( elem => elem==id_rol);
        console.log("Encontrar " +enLista)

        // Si ya existe quiere decir que se desa eliminar o desmarcar
        if(enLista){
          let posicion=this.id_RolUsuarioLista.indexOf(id_rol);
          if (posicion!==-1){
            this.id_RolUsuarioLista.splice(posicion,1);
          }

        }else{
            this.id_RolUsuarioLista.push(id_rol);

        }
    }


          if(this.BorrarlistaRoles.length==0){
              console.log("Sin Borrar" )

          }else{
              console.log("Para Borrar" )

              for(let i in this.BorrarlistaRoles){
                console.log(this.BorrarlistaRoles[i])
              }
          }


          if(this.id_RolUsuarioLista.length==0 && this.BorrarlistaRoles.length==0){
              console.log("Desa Boton" )
                   this.btnSaveMenusRol=false;
          }else{
            console.log("Activar Boton de Guardar ")
            this.btnSaveMenusRol=true;

              for(let i in this.id_RolUsuarioLista){
                  console.log(this.id_RolUsuarioLista[i])
              }

          }


  }

  activarBtnGuardar(){
    return (this.id_RolUsuarioLista.length==0 && this.BorrarlistaRoles.length==0)?false:true;
  }

  estaRolEnLista(array,id_rol:number){

    return (array.indexOf(id_rol)>-1)?true:false;

  }


   aPerfil() {
        let link = ['/perfil'];
        this.router.navigate(link);
    }

            // start controls from from
  public controlsCreate() {
    this.form = this.fb.group({
       usuario: ['', Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-]{0,48}')
          ])],

      clave: ['', Validators.compose([
             Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-(,).]{0,98}')
          ])]
    });

  }

  // Con Base a un usuario, carga los Id de los roles asiganados a este
  public obtenerIdRolesUsuario(id_rol,id_usuario){
    console.log("Cargar los Checklist para el PASO 3")

      this.esActivoCheck=true;
     this.usuariosService.getIdRolesPorUsuario(id_rol,id_usuario)
        .subscribe((data: number[]) => this.listaRolesPorUsuario = data,
          error => console.log(error),
                () => {
                     console.log(this.listaRolesPorUsuario);
                     console.log('Get all IDS ROLES complete');

              });


  }



  public guardarUsuario(){
    //this.obtenerIdRolesUsuario(null,this.id_usuario);
    alert("Save");
    this.usuarioDTO.id_aplicacion=this.id_aplicacion;
    this.usuarioDTO.usuario=this.form.value['usuario'];
    this.usuarioDTO.clave=this.form.value['clave'];

    if(this.id_RolUsuarioLista.length>0){
      this.usuarioDTO.id_roles=this.id_RolUsuarioLista;

    }else{
      this.usuarioDTO.id_roles=[];
    }
    if(this.esFormEditable){
        this.guardarCambios();
    }else{
        this.save();
    }


    console.log("Dimension.->"+this.usuarioDTO);


    //

  }


    /******************************************************************
     * TODO: Esto debe ser otro componente
     *
     * Manejo de los Roles a sus respectivos Usuarios
     ********************************************************************
     */
      id_rol=null;
      roles:Rol[];
      esActivoCheck=false;
      btnSaveMenusRol=false;
      id_RolUsuarioLista:any[]=[];
      listaRolesPorUsuario:number[]=[];
      BorrarlistaRoles:number[]=[];




    save(){
          console.log(this.usuarioDTO)

          this.usuariosService.postUsuario(this.usuarioDTO)
        .subscribe(  result => {
                  if ( result.json() ) {
                    //alert('Creado Oferta Satisfactoriamente! ');
                      this.dialogRef.close('OK');


                  }else {
                      alert('Error al  crear la Aplicación');
                       this.dialogRef.close('ERROR');
                  }

                });
      console.log(this.usuarioDTO)
  }

    guardarCambios(){

      this.usuarioDTO
            console.log(this.usuarioDTO)

        this.usuariosService.putUsuario(this.id_usuario,this.usuarioDTO)
          .subscribe(  result => {
                    if ( result.json() ) {
                      //alert('Creado Oferta Satisfactoriamente! ');

                         if(this.BorrarlistaRoles.length>0){
                              this.eliminarRolesPorUsuario();

                            }else{
                                 this.dialogRef.close('OK');
                            }


                    }else {
                        alert('Error al  crear la Aplicación');
                        this.dialogRef.close('ERROR');
                    }
                  });

        console.log(this.usuarioDTO)
    }

    eliminarRolesPorUsuario(){



        this.usuariosService.deleteRolesUsuario(this.id_usuario,this.BorrarlistaRoles)
          .subscribe(  result => {
                    if ( result.json() ) {
                      alert('Cambios Guardados Satisfactoriamente! ');
                        this.BorrarlistaRoles=[];

                        this.btnSaveMenusRol=false;
                         this.dialogRef.close('OK');


                    }else {

                        alert('Error al  crear la Aplicación');
                         this.dialogRef.close('ERROR');
                    }

                  });


    }





}
