import { Routes, RouterModule } from '@angular/router';
import { AuthenticationComponent } from './index';
import { LoginComponent } from './login/login.component';
import { LogupComponent } from './logup/logup.component';
import { NotAuthGuard } from './not-auth.guard';

export const AuthenticationRouting: Routes = [
  { path: 'auth', component: AuthenticationComponent,
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full'},
      { path: 'login', component: LoginComponent,canActivate: [NotAuthGuard]},
      { path: 'registrar', component: LogupComponent},
    ]
   }
];
