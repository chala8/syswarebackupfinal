import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';

/********************************
 *          Componentes 
 ********************************/
import { RolListaComponent } from './rol-lista/rol-lista.component';
import { RolNuevoComponent } from './rol-nuevo/rol-nuevo.component';
import { RolDetalleComponent } from './rol-detalle/rol-detalle.component';

/********************************
 *          Modelos 
 ********************************/

/********************************
 *          Servicios 
 ********************************/
import { RolesService } from './roles.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
  ],
  declarations: [RolListaComponent, RolNuevoComponent, RolDetalleComponent],
  exports: [RolListaComponent],
   providers: [RolesService]
})
export class RolesModule { }
