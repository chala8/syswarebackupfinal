import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { FormGroup, FormBuilder, Validators }   from '@angular/forms';
import { Location } from '@angular/common';
import {MdDialog, MdDialogRef} from '@angular/material';

/********************************
 *          Constantes 
 ********************************/
import { LocalStorage }  from '../../../../shared/localStorage';
/********************************
 *          Componentes 
 ********************************/
import { MenuNuevoComponent }  from '../menu-nuevo/menu-nuevo.component';


/********************************
 *          Modelos 
 ********************************/
import { Menu } from '../menu';
import { MenuDTO } from '../menuDTO';
import { Rol } from '../../roles/rol';

/********************************
 *          Servicios 
 ********************************/
import { MenusService } from '../menus.service';
import { RolesService } from '../../roles/roles.service';

@Component({
  moduleId:module.id,
  selector: 'app-menu-lista',
  templateUrl: './menu-lista.component.html',
  styleUrls: ['./menu-lista.component.scss']
})
export class MenuListaComponent implements OnInit {

  menus:Menu[];
  form: FormGroup;
  menuDTO:MenuDTO;
  id_aplicacion=null;
  esActivAsigMenus=false;

  constructor(public fb: FormBuilder,
              public locStorage: LocalStorage,
              public dialog: MdDialog,
              private route: ActivatedRoute,
              public router: Router,
              public location: Location,
              private menusService:MenusService,
              private rolesService:RolesService) { 

                this.menuDTO= new MenuDTO(null,null,null,null,null,null,null,null);

                    this.createControls();
                    this.changeRol();
              }

  ngOnInit() {
    this.obtenerQueryParams();
     this.locStorage.getData();
     this.obtenerMenus();
     
     
  }
  obtenerQueryParams(){
        let seb= this.route.queryParams.subscribe(params => {

            // Defaults to 0 if no query param provided.
            this.id_aplicacion = +params['app'] || null;
        });

        if(this.id_aplicacion==null){
            alert("Faltan Mas datos de la Aplicación");
            this.aPerfil();
        }else{
                
                // this.loadData(); 
        
        }

  }

    aPerfil() {
        let link = ['/perfil'];
        this.router.navigate(link);
    }

   save(){
       console.log(this.menuDTO)

          this.menusService.postMenu(this.menuDTO)
        .subscribe(  result => {
                  if ( result.json() ) {
                    alert('Creado Oferta Satisfactoriamente! ');
                      //this.volver();
                      //this.aMenu();
                      console.log(result.json())
                      this.obtenerMenus();
                      
                  }else {

                      alert('Error al  crear la Aplicación');
                       this.openDialog()
                  }
                  
                });
      console.log(this.menuDTO)
  }

   aMenu(){
      
         this.router.navigate(['/perfil']);
      
    }

  openDialog() {
    let dialogRef = this.dialog.open(MenuNuevoComponent);
    dialogRef.afterClosed().subscribe(result => {
      
      console.log(result)
     
      if(result!='cerrar'){
          this.menuDTO.id_aplicacion=this.id_aplicacion;
          this.menuDTO.nombre=result["nombre"];
          this.menuDTO.tipo=result["tipo"];
          this.menuDTO.ruta=result["ruta"];
          this.menuDTO.icono=result["icono"];
          this.menuDTO.avatar=result["avatar"];
          this.menuDTO.background=result["background"];
          this.menuDTO.estado=result["estado"];
          console.log(result["nombre"])
          console.log("result")

          console.log(this.menuDTO)
          this.save()
        }
    });
  }

     // llama el metodo GET del servicio para obtener los datos
    obtenerMenus(): void {

        this.menusService.getMenus(null,this.id_aplicacion,null,null,null,null,null,null,null)
        .subscribe((data: Menu[]) => this.menus = data,
          error => console.log(error),
                () => {
                  console.log(this.menus);
                  console.log('Get all Items complete');
              });
    }


    /******************************************************************
     * TODO: Esto debe ser otro componente
     * 
     * Manejo de los Menus a sus respectivos Roles
     ********************************************************************
     */

    id_rol=null;
    roles:Rol[];
    esActivoCheck=false;
    btnSaveMenusRol=false;
    id_menuRolLista:any[]=[];
    listaMenusPorRol:number[]=[];
    BorrarlistaMenus:number[]=[];

      createControls() {
        this.form = this.fb.group({
          rol: ['', Validators.compose([
            
          ])]
          
        });
      }

    chngeValueAsigMenus(){
      this.form.patchValue({
        rol:''
      });
      this.esActivAsigMenus=!this.esActivAsigMenus
      if(this.esActivAsigMenus && !this.roles){
        this.listaRoles(this.id_aplicacion,null,null,null);

      }
      
      this.esActivoCheck=false;
    }

     listaRoles(id_aplicacion,id_rol,rol,descripcion){
    
       this.rolesService.getRoles(id_aplicacion,id_rol,rol,descripcion)
        .subscribe((data: Rol[]) => this.roles = data,
          error => console.log(error),
                () => {
                 
                 
              });

    }

    changeRol() {
     
 
      
    
      

     this.form.valueChanges.subscribe((value) => {
      
    
         if(this.id_rol==null ||( this.id_rol!=value['rol'])){
              if(value['rol']){
                  console.log("Cambio el ROl -> "+this.id_rol+" al ROl "+value['rol']);
                  this.id_rol=value['rol'];
                  this.listaRolMenu(value['rol'],null);
                 
              }
             
         }
            

     });

   }

 
   

   listaRolMenu(id_rol,id_menu){
     this.esActivoCheck=true;
     this.rolesService.getMenusPorRol(id_rol,id_menu)
        .subscribe((data: number[]) => this.listaMenusPorRol = data,
          error => console.log(error),
                () => {
                 
                 
              });
     console.log("Cargar los Checklist para el PASO 3")
   }

   estaMenuEnLista(array,id_menu){
     return (array.indexOf(id_menu)>-1)?true:false;
   }

   

   addMenuRol(index,id_menu){
      let enLista=null;

      if(this.estaMenuEnLista(this.listaMenusPorRol,id_menu)){ //Es para borrar
        

        if (this.estaMenuEnLista(this.BorrarlistaMenus,id_menu)){ //Sacar de la lista

            let posicion=this.BorrarlistaMenus.indexOf(id_menu);
            if (posicion!==-1){
                this.BorrarlistaMenus.splice(posicion,1);
            }

        }else{ // Lo agragamos a la lista
          this.BorrarlistaMenus.push(id_menu)
        }
      
      }else{

     

        enLista=this.id_menuRolLista.find( elem => elem==id_menu);
        console.log("Encontrar " +enLista)

        // Si ya existe quiere decir que se desa eliminar o desmarcar
        if(enLista){
          let posicion=this.id_menuRolLista.indexOf(id_menu);
          if (posicion!==-1){
            this.id_menuRolLista.splice(posicion,1);
          }
          
        }else{
            this.id_menuRolLista.push(id_menu);

        }
    }

      


          if(this.BorrarlistaMenus.length==0){
              console.log("Sin Borrar" )
                   
          }else{
              console.log("Para Borrar" )

              for(let i in this.BorrarlistaMenus){
                console.log(this.BorrarlistaMenus[i])
              }
          }

        
          if(this.id_menuRolLista.length==0 && this.BorrarlistaMenus.length==0){
              console.log("Desa Boton" )
                   this.btnSaveMenusRol=false;
          }else{
            console.log("Activar Boton de Guardar ")
            this.btnSaveMenusRol=true;
              for(let i in this.id_menuRolLista){
              console.log(this.id_menuRolLista[i])
          }
          
          }
      

     
   

    

   }

   guardarMenusRol(){
      console.log("Guardar los Menus del Rol "+this.form.value['rol']);
      if(this.id_menuRolLista.length>0){
      this.rolesService.putRolesMenu(this.form.value['rol'],this.id_menuRolLista)
        .subscribe(  result => {
                  if ( result.json() ) {
                    
                    this.id_menuRolLista=[];
                    if(this.BorrarlistaMenus.length<=0){
                      alert('Cambios Guardados Satisfactoriamente! ');
                      this.listaRolMenu(this.form.value['rol'],null);
                      this.btnSaveMenusRol=false;
                    }
                     
                      
                  }else {

                      alert('Error al  crear la Aplicación');
                       this.openDialog()
                  }
                  
                });
    }
      if(this.BorrarlistaMenus.length>0){
        this.eliminarMenusRol();
      }
   }

    eliminarMenusRol(){
    console.log("Guardar los Menus del Rol "+this.form.value['rol']);

    this.rolesService.deleteRolesMenu(this.form.value['rol'],this.BorrarlistaMenus)
        .subscribe(  result => {
                  if ( result.json() ) {
                    alert('Cambios Guardados Satisfactoriamente! ');
                      this.BorrarlistaMenus=[];
                      this.listaRolMenu(this.form.value['rol'],null);
                      this.btnSaveMenusRol=false;
                     
                      
                  }else {

                      alert('Error al  crear la Aplicación');
                       this.openDialog()
                  }
                  
                });
   }
     
}
