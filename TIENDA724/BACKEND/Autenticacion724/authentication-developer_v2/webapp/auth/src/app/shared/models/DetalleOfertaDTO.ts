export class DetalleOfertaDTO{

    id_oferta:number;
    id_producto:number;
    id_unidad:number;
    valor_real:number;
    valor_ofertado:number;

    constructor(
        id_oferta:number,
        id_producto:number,
        id_unidad:number,
        valor_real:number,
        valor_ofertado:number
    ){}
}