import { Routes } from '@angular/router';

/********************************
 *          Componentes 
 ********************************/
import { RolListaComponent } from './rol-lista/rol-lista.component';
import { RolDetalleComponent } from './rol-detalle/rol-detalle.component';
import { RolNuevoComponent } from './rol-nuevo/rol-nuevo.component';

/********************************
 *          Modelos 
 ********************************/

/********************************
 *          Servicios 
 ********************************/

export const RolesRouting: Routes = [
    {
      path: 'rol',
      component: null,
      children: [
        {path: '', redirectTo: 'lista', pathMatch: 'full'},
        {path: 'lista', component: RolListaComponent},
        { path: 'detalle/:id',     component: RolDetalleComponent },
        { path: 'nuevo',     component: RolNuevoComponent},
      ]
    }
];
