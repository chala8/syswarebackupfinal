import { Injectable } from '@angular/core';
import { Http, Headers, Response, URLSearchParams, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable'


import {urlbase} from '../../../shared/url';
import { LocalStorage } from '../../../shared/localStorage';
import { Token } from '../../../shared/token';

// Data model
import { Aplicacion } from './aplicacion'
import { AplicacionDTO } from './aplicacionDTO'

import { ResponseAPI } from '../../../shared/utils/ResponseAPI';


@Injectable()
export class AplicacionSevice {
  
    private api_uri = urlbase[0] + '/aplicaciones';


    private headers = new Headers({'Content-Type': 'application/json'});
    private options: RequestOptions;
    private token: Token;

      constructor(private http: Http, private locStorage: LocalStorage) {
        let aux =  this.locStorage.getData();
        /*}let headers = new Headers({
          'Content-Type': 'application/json',
          'Authorization' : this.locStorage.getTokenValue()
        });*/
        if(aux===null){

        }else{
            this.token= new Token(null,null,null);
            this.token.id_usuario_app=aux.id_usuario_app;
            this.token.usuario=aux.usuario;
            this.options = new RequestOptions(this.headers);
            
        }

           
        
      }

    public getAplicaciones= (id_aplicacion:number|null,  id_usuario_app:number|null, nombre:string|null, descripcion:string|null,
    version:string|null,fecha_creacion:string|null, fecha_actualizacion:string|null,key_aplicacion:string|null,key_servidor:string|null): Observable<Aplicacion[]> => {

            let params: URLSearchParams = new URLSearchParams();
            params.set('id_aplicacion',id_aplicacion==null?null:id_aplicacion+'');
            params.set('id_usuario_app',  id_usuario_app==null?null:id_usuario_app+'');
            params.set('nombre',nombre==null?null:nombre+'');
            params.set('descripcion', descripcion==null?null:descripcion+'');
            params.set('version',  version==null?null:version+'');
            params.set('fecha_creacion',  fecha_creacion==null?null:fecha_creacion+'');
            params.set('fecha_actualizacion',  fecha_actualizacion==null?null:fecha_actualizacion+'');
            params.set('key_aplicacion',  key_aplicacion==null?null:key_aplicacion+'');
            params.set('key_servidor',  key_servidor==null?null:key_servidor+'');
            
            let myOption: RequestOptions = this.options;
            myOption.search = params;
            

        return this.http.get(this.api_uri, myOption)
            .map((response: Response) => <Aplicacion[]>response.json())
            .catch(this.handleError);
    }


    public postAplicaion = (aplicacion: AplicacionDTO): Observable<Response> => {
        let toAdd = JSON.stringify({aplicacion});
         let url=`${this.api_uri}?id_usuario=${this.token.id_usuario_app}`
        return this.http.post(url, aplicacion, this.options)
            .map((response: Response) => {
              
                  
                  return response;
                })
            .catch(this.handleError);

    }

     public patchAplicaion = (id_aplicacion: number,aplicacion: AplicacionDTO): Observable<Response> => {
        let toAdd = JSON.stringify({aplicacion});
        

         let params: URLSearchParams = new URLSearchParams();
            params.set('id_aplicacion',id_aplicacion==null?null:id_aplicacion+'');
            params.set('id_usuario_app',  this.token.id_usuario_app==null?null:this.token.id_usuario_app+'');

         let myOption2: RequestOptions = this.options;
            myOption2.search = params;
        return this.http.put(this.api_uri, aplicacion, myOption2)
            .map((response: Response) => {
                  return response;
                })
            .catch(this.handleError);

    }


     private handleError (error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
        errMsg = error.message ? error.message : error.toString();
        }
        alert(errMsg);
        return Observable.throw(errMsg);
    }
}

