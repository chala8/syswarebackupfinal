import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { FormGroup, FormBuilder , Validators } from '@angular/forms';
import { Location } from '@angular/common';
import {MdDialog, MdDialogRef} from '@angular/material';


import { LocalStorage }  from '../../../shared/localStorage';

import { AplicacionDTO } from '../aplicacion/aplicacionDTO';


import { AplicacionSevice } from '../aplicacion/aplicaiones.service';


 
@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
  providers:[AplicacionSevice]

})
export class PerfilComponent implements OnInit {


    selectedOption: string;
    form: FormGroup;
    aplicacion:AplicacionDTO;

  constructor(public fb: FormBuilder,
              public locStorage: LocalStorage,
              public dialog: MdDialog,
              public router: Router,
              public location: Location,
              private aplicacionSevice:AplicacionSevice) {

                  if  (this.locStorage.getData()===null){
                            alert("Debe Iniciar sesión")
                            this.router.navigate(['/auth']);

                        }else{
                          this.locStorage.getData();
                        }

                 this.aplicacion=new AplicacionDTO(null,null,null);
               }

 


  openDialog() {
    let dialogRef = this.dialog.open(DialogResultExampleDialog);
    dialogRef.afterClosed().subscribe(result => {
      
      console.log(result)
     
      if(result!='cerrar'){
          this.aplicacion=result;
          this.save()
        }
    });
  }

    save(){
       console.log(this.aplicacion)

          this.aplicacionSevice.postAplicaion(this.aplicacion)
        .subscribe(  result => {
                  if ( result.json() ) {
                    alert('Creado Oferta Satisfactoriamente! '+result.json());
                      //this.volver();
                      console.log(result.json())
                      this.goDashboard(result.json());
                      
                  }else {

                      alert('Error al  crear la Aplicación');
                       this.openDialog()
                  }
                  
                });
    console.log(this.aplicacion)
  }
  

  ngOnInit() {
  
   
  }

   goDashboard(id_app){
      
         
          this.router.navigate(['/dashboard'],
          { queryParams: { app: id_app } }
          );
      
    }

}



@Component({
  selector: 'dialog-result-example-dialog',
  templateUrl: './dialog-result-example-dialog.html',
})
export class DialogResultExampleDialog {

   action = 'Crear';
   title = 'Registrar una Nueva Aplicación';
   form: FormGroup;

   

  constructor(public dialogRef: MdDialogRef<DialogResultExampleDialog>,
  public fb: FormBuilder,) {
     this.controlsCreate();
  }


         // start controls from from
  public controlsCreate() {
    this.form = this.fb.group({
       nombre: ['', Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(50),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-]{0,48}')
          ])],
      
      descripcion: ['', Validators.compose([
             Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-(,).]{0,98}')
          ])],
    version: ['', Validators.compose([
             Validators.required,
            Validators.minLength(1),
            Validators.maxLength(10),
            Validators.pattern('[a-zA-Z]{1}[a-z A-Z0-9-(,).]{0,9}')
          ])],
    }); 

  }


}
