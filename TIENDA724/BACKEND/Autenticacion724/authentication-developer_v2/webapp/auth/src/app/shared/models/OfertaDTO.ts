import { DatoDTO } from './DatoDTO';
export class OfertaDTO{

    dato:DatoDTO;
    imagen_url:string;
    fecha_inicio:number;
    fecha_fin:number;
    es_porcentaje:number;
    valor:number;
    estado:number;

    constructor(
        dato:DatoDTO,
        imagen_url:string,
        fecha_inicio:number,
        fecha_fin:number,
        es_porcentaje:number,
        valor:number,
        estado:number
    ){}

}