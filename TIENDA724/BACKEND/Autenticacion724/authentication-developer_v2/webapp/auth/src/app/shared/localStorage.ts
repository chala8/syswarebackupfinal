import { Injectable } from '@angular/core';


import { Token } from '../shared/token';


@Injectable()
export class LocalStorage {
  private token: Token;
  constructor() {
    this.token = JSON.parse(localStorage.getItem('currentUserApp3')) ;
  }
  getData() {
    this.token = JSON.parse(localStorage.getItem('currentUserApp3')) ;
    return this.token;
  }

  getTokenValue() {
    return this.token['key_TOKEN'];
  }
  getTypePrio(){
    return localStorage.getItem('typePrio');
  }
  setTypePrio(type: number){
     localStorage.setItem('typePrio', JSON.stringify(type));
  }
  getRoleName() {
    let role: string;
    switch ( this.token['id_role']) {
      case 1: case 2:
          role = 'SECURITY';
        break;
      case 3:
          role = 'SUPERADMIN';
        break;
      case 4:
          role = 'ADMIN';
        break;
      case 5:
          role = 'MONITOR';
        break;
      default:
          role = 'NOT_FOUNT';
        break;
    }
    return role;
  }
 
  getIdUsuarioApp() {
    return this.token['id_usuario_app'];
  }
  getIdRoute() {
    return this.token['id_route'];
  }

  getUsuario() {
    return this.token['usuario'];
  }

  getFullName() {
    return this.token['fullName'];
  }
   getLastName() {
    return this.token['lastName'];
  }

  setUsername(username: string) {
    this.token['usuario'] = username;
    localStorage.setItem('currentUserApp3', JSON.stringify(this.token));
  }

  setFullName(fullName: string) {
    this.token['fullName'] = fullName;
    localStorage.setItem('currentUserApp3', JSON.stringify(this.token));
  }
  setLastName(lastName: string) {
    this.token['lastName'] = lastName;
    localStorage.setItem('currentUserApp3', JSON.stringify(this.token));
  }

   isSession() {
    if ( localStorage.getItem('currentUserApp3') !== null ) {
      return true;
    }
    return false;
  }

  cleanSession() {
    localStorage.removeItem('currentUserApp3');
    localStorage.removeItem('typePrio');
  }

}
