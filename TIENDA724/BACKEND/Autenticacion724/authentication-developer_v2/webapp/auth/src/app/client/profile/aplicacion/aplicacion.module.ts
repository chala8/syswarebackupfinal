import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';

/**Componentes */
import { AplicacionComponent } from './aplicacion/aplicacion.component';
/**Modulos */

/**Servicios */


import { AplicacionSevice } from './aplicaiones.service';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule,
  
  ],
  declarations: [],
  providers:[]
})
export class AplicacionModule { }
