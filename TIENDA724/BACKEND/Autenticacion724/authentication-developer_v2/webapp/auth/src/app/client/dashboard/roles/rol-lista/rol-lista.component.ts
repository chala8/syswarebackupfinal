import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params }   from '@angular/router';
import { FormGroup, FormBuilder, Validators }   from '@angular/forms';
import { Location } from '@angular/common';
import { MdDialog, MdDialogRef } from '@angular/material';

/********************************
 *          Constantes 
 ********************************/
import { LocalStorage }  from '../../../../shared/localStorage';
/********************************
 *          Componentes 
 ********************************/
import { RolNuevoComponent }  from '../rol-nuevo/rol-nuevo.component';


/********************************
 *          Modelos 
 ********************************/
import { Rol } from '../rol';
import { RolDTO } from '../rolDTO';
/********************************
 *          Servicios 
 ********************************/
import { RolesService } from '../roles.service';

@Component({
  moduleId:module.id,
  selector: 'app-rol-lista',
  templateUrl: './rol-lista.component.html',
  styleUrls: ['./rol-lista.component.scss']
})
export class RolListaComponent implements OnInit {
  roles:Rol[];
  form: FormGroup;
  rolDTO:RolDTO;
  id_aplicacion=null;

  constructor(public fb: FormBuilder,
              public locStorage: LocalStorage,
              public dialog: MdDialog,
              private route: ActivatedRoute,
              public router: Router,
              public location: Location,
              private rolesService:RolesService) { 
                this.rolDTO= new RolDTO(null,null,null);
              }

  ngOnInit() {
     this.obtenerQueryParams();
     this.locStorage.getData();
     this.obtenerRoles();
     
  }

    obtenerQueryParams(){
        let seb= this.route.queryParams.subscribe(params => {

            // Defaults to 0 if no query param provided.
            this.id_aplicacion = +params['app'] || null;
        });

        if(this.id_aplicacion==null){
            alert("Faltan Mas datos de la Aplicación");
            this.aPerfil();
        }else{
                
                // this.loadData(); 
        
        }

  }

     aPerfil() {
        let link = ['/perfil'];
        this.router.navigate(link);
    }

   save(){
       console.log(this.rolDTO)

          this.rolesService.postRol(this.rolDTO)
        .subscribe(  result => {
                  if ( result.json() ) {
                    //alert('Creado Oferta Satisfactoriamente! ');
                      
                      this.obtenerRoles();
                      
                  }else {

                      alert('Error al  crear la Aplicación');
                       this.openDialog()
                  }
                  
                });
      console.log(this.rolDTO)
  }

   aMenu(){
      
         this.router.navigate(['/perfil']);
      
    }

  openDialog() {
    let dialogRef = this.dialog.open(RolNuevoComponent);
    dialogRef.afterClosed().subscribe(result => {
      
      console.log(result)
     
      if(result!='cerrar'){

          this.rolDTO.id_aplicacion=this.id_aplicacion;
          this.rolDTO.rol=result["rol"];
          this.rolDTO.descripcion=result["descripcion"];
          console.log(result)
          console.log("result")
          console.log(this.rolDTO)
          this.save()
        }
    });
  }


      // llama el metodo GET del servicio para obtener los datos
    obtenerRoles(): void {

        this.rolesService.getRoles(this.id_aplicacion,null,null,null)
        .subscribe((data: Rol[]) => this.roles = data,
          error => console.log(error),
                () => {
                  console.log(this.roles);
                  console.log('Get all Items complete');
              });
    }

}
