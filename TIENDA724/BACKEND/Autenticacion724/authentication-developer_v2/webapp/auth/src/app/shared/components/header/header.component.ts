import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { LocalStorage }  from '../../../shared/localStorage';

@Component({
    moduleId: module.id,
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    constructor(private translate: TranslateService, public locStorage: LocalStorage,   private route: ActivatedRoute,
    private router: Router) { }

    ngOnInit() {}

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('push-right');
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
         this.locStorage.cleanSession();
         this.volver();
        localStorage.removeItem('isLoggedin');

    }
     volver(){
     let link = ['/'];
    this.router.navigate(link);

  }

    changeLang(language: string) {
        this.translate.use(language);
    }
}
