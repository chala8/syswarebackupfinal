import { Routes } from '@angular/router';

/********************************
 *          Componentes 
 ********************************/
import { UsuarioListaComponent } from './usuario-lista/usuario-lista.component';
import { UsuarioDetalleComponent } from './usuario-detalle/usuario-detalle.component';
import { UsuarioNuevoComponent } from './usuario-nuevo/usuario-nuevo.component';

/********************************
 *          Modelos 
 ********************************/

/********************************
 *          Servicios 
 ********************************/

export const UsuariosRouting: Routes = [
    {
      path: 'usuario',
      component: null,
      children: [
        {path: '', redirectTo: 'lista', pathMatch: 'full'},
        {path: 'lista', component: UsuarioListaComponent},
        { path: 'detalle/:id',     component: UsuarioDetalleComponent },
        { path: 'nuevo',     component: UsuarioNuevoComponent},
      ]
    }
];
