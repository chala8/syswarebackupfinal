import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '@angular/material';
/**
 *  Principal
 */
import { HomeComponent } from './home.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
       MaterialModule,
  ],
  declarations: [HomeComponent],
exports: [HomeComponent]
})
export class HomeModule { }
