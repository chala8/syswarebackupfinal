import { Routes } from '@angular/router';


import { WelcomeComponent } from './welcome/welcome.component';
import { IndexComponent } from './index/index.component';
import { AboutComponent } from './about/about.component';




export const WelcomeRouting: Routes = [
  { path: '', component: WelcomeComponent,
      children: [
         { path: 'index', component: IndexComponent },
        { path: 'about', component: AboutComponent },

      ]
  },

];
