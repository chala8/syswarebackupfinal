import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

/**
 *  Import component principal
 */
import { WelcomeComponent } from './welcome/welcome.component';


/**
 *  Import other components
 */
import { IndexComponent } from './index/index.component';
import { AboutComponent } from './about/about.component';



@NgModule({
  imports: [
    CommonModule,RouterModule
  ],
  declarations: [
     AboutComponent,
     IndexComponent,
     WelcomeComponent
     ],
   exports: [IndexComponent]
})
export class WelcomeModule { }
