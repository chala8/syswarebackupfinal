import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
/**
 *  Principal
 */
import { AuthenticationComponent } from './authentication.component';

/**
 *  Import other components
 */
import { LoginComponent } from './login/login.component';
import { LogupComponent } from './logup/logup.component';


import {AuthService} from './auth.service';
import { NotAuthGuard } from './not-auth.guard';

@NgModule({
  imports: [  
    CommonModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule, 
    ],
  declarations: [
    AuthenticationComponent,
    LoginComponent,
    LogupComponent
    ],
    providers: [ AuthService, NotAuthGuard ],
  exports: [ AuthenticationComponent ]
})
export class AuthenticationModule { }

