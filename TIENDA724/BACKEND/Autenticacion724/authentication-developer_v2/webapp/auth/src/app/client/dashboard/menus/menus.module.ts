import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';

/********************************
 *          Componentes 
 ********************************/
import { MenuListaComponent } from './menu-lista/menu-lista.component';
import { MenuNuevoComponent } from './menu-nuevo/menu-nuevo.component';
import { MenuDetalleComponent } from './menu-detalle/menu-detalle.component';

/********************************
 *          Modelos 
 ********************************/

/********************************
 *          Servicios 
 ********************************/
import { MenusService } from './menus.service';
import { RolesService } from '../roles/roles.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
  ],
  declarations: [MenuListaComponent, MenuNuevoComponent, MenuDetalleComponent],
  exports: [MenuListaComponent],
  providers: [ MenusService,RolesService]
})
export class MenusModule { }
