import { Routes } from '@angular/router';

/***************************************************
 * Llamar el Componente que se muestra por defecto *
 ***************************************************/

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/
import { PerfilComponent } from './perfil/perfil.component';






export const ProfileRouting: Routes = [
  {
    path: 'perfil' ,
    component: PerfilComponent}
];
