import { Dato } from './Dato';
import { Producto } from './Producto';
import { Unidad } from './Unidad';
export class DetalleOferta{

    id_detalle_oferta: number;
    id_oferta:number;
    producto:Producto;
    unidad:Unidad;
    valor_real:number;
    valor_ofertado:number;

    constructor(
        id_detalle_oferta: number,
        id_oferta:number,
        producto:Producto,
        unidad:Unidad,
        valor_real:number,
        valor_ofertado:number
    ){}
}