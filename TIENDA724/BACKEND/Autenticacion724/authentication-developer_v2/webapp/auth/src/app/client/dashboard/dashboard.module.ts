import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';

/********************************
 *       Componente Principal
 ********************************/
import { DashboardComponent } from './dashboard.component';

/********************************
 *          Componentes 
 ********************************/
import { PageHeaderComponent } from '../../shared/modules/page-header/page-header.component';
import { HeaderComponent } from '../../shared/components/header/header.component';
import { StatComponent } from '../../shared/modules/stat/stat.component';
import { SidebarComponent } from '../../shared/components/sidebar/sidebar.component';
import { DetalleAppComponent } from '../profile/aplicacion/detalle-app/detalle-app.component';

/********************************
 *          Modelos 
 ********************************/
import { HomeModule } from './home/index';
import { MenusModule } from './menus/index';
import { RolesModule } from './roles/index';
import { UsuariosModule } from './usuarios/index';
/********************************
 *          Servicios 
 ********************************/



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    HomeModule, TranslateModule, NgbDropdownModule.forRoot(),
    MenusModule,
    RolesModule,
    UsuariosModule
    
  
    ],
  declarations: [
    DashboardComponent,
    HeaderComponent,
    SidebarComponent,
    DetalleAppComponent,
    PageHeaderComponent,
    StatComponent
    
    ],
  exports: [ DashboardComponent ]
})
export class DashboardModule { }


