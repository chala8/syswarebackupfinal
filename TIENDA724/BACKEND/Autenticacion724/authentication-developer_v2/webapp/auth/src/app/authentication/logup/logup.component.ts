import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/********************************
 *          Constantes 
 ********************************/
import { LocalStorage }  from '../../shared/localStorage';

import { AuthService } from '../auth.service';

import { Auth } from '../auth';
@Component({
  moduleId: module.id,
  selector: 'app-logup',
  templateUrl: './logup.component.html',
  styleUrls:['./logup.component.scss'],
  encapsulation: ViewEncapsulation.Native
})
export class LogupComponent implements OnInit {

   title: string = 'Iniciar Session';
  isLoggedIn: boolean = false;
  error: string= '';
  form: FormGroup;
  action:string='Iniciar Sesion'
  auth: Auth;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    public locStorage: LocalStorage,
    private authService: AuthService) {
        this.auth= new Auth(null,null);
     }



  ngOnInit() {
   this.controlsCreate();
  }

   // start controls from from
    controlsCreate() {
      this.form = this.fb.group({
        nombre: ['', Validators.required],
        usuario: ['', Validators.required],
        clave: ['', Validators.required],
        repetir_clave: ['', Validators.required]
      });
    }
  validarClave(){
    return (this.form.value['clave']===this.form.value['repetir_clave'])?true:false;
  }

  register() {
     this.locStorage.cleanSession();
         this.auth.usuario = this.form.value['usuario'];
          this.auth.clave = this.form.value['clave'];

          this.authService.register(this.auth)
                            .subscribe(
                              result => {
                                console.log(result)
                                if (result === true ) {
                                 this.login(this.auth)


                                  return;
                                }else {
                                  //this.openDialog();
                                  return;
                                  }
                              })
  }

   login(auth) {
     this.locStorage.cleanSession();
    this.authService.login(auth)
                      .subscribe(
                        result => {
                         
                          if (result === true ) {
                            this.cargarPerfil();
                            return;
                          }else {
                            //this.openDialog();
                            return;
                            }
                        })




  }

   cargarPerfil( ) {
    alert('Angular Auth');
     this.router.navigate(['/perfil']);

  }

  volver(){
     let link = ['/'];
    this.router.navigate(link);

  }



      

}
