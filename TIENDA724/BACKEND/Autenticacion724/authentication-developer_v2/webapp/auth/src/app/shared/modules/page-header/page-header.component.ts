import { Component, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'app-page-header',
    templateUrl: './page-header.component.html',
    styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent {
    @Input() heading: string;
    @Input() icon: string;
}
