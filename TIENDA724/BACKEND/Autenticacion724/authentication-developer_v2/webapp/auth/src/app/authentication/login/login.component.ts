import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


/********************************
 *          Constantes 
 ********************************/
import { LocalStorage }  from '../../shared/localStorage';

import { AuthService } from '../auth.service';

import { Auth } from '../auth';

@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls:['./login.component.scss'],
  encapsulation: ViewEncapsulation.Native
})
export class LoginComponent implements OnInit {

  title: string = 'Iniciar Session';
  isLoggedIn: boolean = false;
  error: string= '';
  form: FormGroup;
  action:string='Iniciar Sesion'
  auth: Auth;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    public locStorage: LocalStorage,
    private authService: AuthService) {
        this.auth= new Auth(null,null);
     }


   ngOnInit() {
    //this.authService.logout();
    this.controlsCreate();
  }

  // start controls from from
  controlsCreate() {
    this.form = this.fb.group({
      usuario: ['', Validators.required],
       clave: ['', Validators.required],
    });
  }

   login() {
         this.locStorage.cleanSession();
    this.auth.usuario = this.form.value['usuario'];
    this.auth.clave = this.form.value['clave'];

    this.authService.login(this.auth)
                      .subscribe(
                        result => {
                          console.log(result)
                          if (result === true ) {
                            this.cargarPerfil();
                            return;
                          }else {
                            //this.openDialog();
                            return;
                            }
                        })




  }


   cargarPerfil( ) {
    alert('Angular Auth');
     this.router.navigate(['/perfil']);

  }

  volver(){
     let link = ['/'];
      this.router.navigate(link);

  }

}
