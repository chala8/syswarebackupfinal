import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'EDS-1A';


  mobile = false;
  ngOnInit() {
    if (window.innerWidth <= 600) { // 768px portrait
      this.mobile = true;
    }
  }

  @HostListener('window:resize', ['$event'])
  validateRender(event){
    //@ts-ignore
    if (event.target.innerWidth <= 600) { // 768px portrait
      this.mobile = true;  
    }else{
      this.mobile = false;
    }
  }

  
function(){}
}
