import { Injectable } from '@angular/core';
//import {FCM} from '@ionic-native/fcm/ngx';
import {Subject} from 'rxjs';
import {Urlbase} from '../utils/urls';
import {HttpClient} from '@angular/common/http';

import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed } from '@capacitor/core';
const { PushNotifications } = Plugins;


@Injectable({
  providedIn: 'root'
})
export class FCMService {

    currentMessage: Subject<{}> = new Subject<{}>();
    TokenUpdate: Subject<{}> = new Subject<{}>();

    ChanelSetup = false;
    FCMtoken = "-1";

    constructor(
      //private fcm: FCM,
      private http: HttpClient
    ) {
        console.log("CONSTRUCTOR FCM SERVICE");
        this.TokenUpdate.subscribe(async (value:PushNotificationToken) => {
            try {
                if(this.ChanelSetup){return;}
                if(value.value != null && value.value != ""){
                    this.ChanelSetup = true;
                    this.FCMtoken = value.value;
                    let channelsList = await PushNotifications.listChannels();
                    console.log('channelsList1 es : ' + JSON.stringify(channelsList));
                    await PushNotifications.createChannel({
                        id:"push",
                        name: "push",
                        //sound: "content://settings/system/notification_sound",
                        //sound: "alert.wav",
                        importance: 5,
                        visibility: 1,
                        lights: true,
                        vibration: true,
                    });
                    console.log('channelsList2 es : ' + JSON.stringify(channelsList));
                    //this.UpdateFCMtoken(value);
                }
            }catch (e) {
                console.log("TokenUpdate.subscribe",e.toString());
            }
        })

    // Request permission to use push notifications
    // iOS will prompt user and return if they granted permission or not
    // Android will just grant without prompting
    PushNotifications.requestPermission().then( result => {
      if (result.granted) {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
        console.log("PushNotifications.register()");
      } else {
          alert("No se pudo solicitar el permiso para las notificaciones");
        // Show some error
      }
    });

    // On success, we should be able to receive notifications
    PushNotifications.addListener('registration',
        async (token: PushNotificationToken) => {
            console.log('Push registration success, token: ' + token.value);
            this.TokenUpdate.next(token);
        }
    );

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError',
        (error: any) => {
          console.log('Error on registration: ' + JSON.stringify(error));
        }
    );

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener('pushNotificationReceived',
        (notification: PushNotification) => {
          console.log('Push received: ' + JSON.stringify(notification));
          this.currentMessage.next(notification);
        }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener('pushNotificationActionPerformed',
        (notification: PushNotificationActionPerformed) => {
            console.log('Push action performed: ' + JSON.stringify(notification));
            notification.notification.data.fromOutside = true;
            this.currentMessage.next(notification.notification);
        }
    );
  }

  async UpdateFCMtoken(token:string){
      console.log("UpdateFCMtoken - token: ",token);
      try {
          let resultado = await new Promise((resolve, reject) => {
              console.log("POST UpdateFCMtoken");
              this.http.post(Urlbase.auth+"/UpdateFCMtoken",{token:token||"-1"}).subscribe(value1 => {
                  console.log("resolve UpdateFCMtoken");
                  resolve(value1);
              },error2 => {
                  console.log("reject UpdateFCMtoken");
                  reject(error2);
              })
          })
          console.log("OK - resultado");
          console.log(resultado);
      }catch (e) {
          console.log("ERRRO FCMupdate: ");
          console.log(e);
          console.log(JSON.stringify(e));
      }
  }

  async SendFCMpushFromClient(title,body,data,id_store,nombre){
    try {
      console.log("title")
      console.log(title)
      console.log("body")
      console.log(body)
      console.log("data")
      console.log(data)
      console.log("id_store")
      console.log(id_store)
      //Ajuste
      let Llaves = Object.keys(data);
      for(let n = 0;n<Llaves.length;n++){
        data[Llaves[n]] += "";//esto es para estar seguros de que todo se vaya como string
      }
      //
      let Resultado = await new Promise((resolve, reject) => {
        this.http.post(Urlbase.auth+"/SendFCMpushFromClient",{
          id_store:id_store,
          title:title,
          body:body,
          data:data
        }).subscribe(value => resolve(value),error => reject(error))
      })
      if(Resultado != "Empty"){
        //enviado
      }
    }catch (e) {
      //Error
      console.log("ERROR AL ENVIAR FCM")
      console.log(e.toString())
      console.log(JSON.stringify(e))
    }
  }

}



// // get FCM token
// this.fcm.getToken().then(token => {
//   localStorage.setItem("FCMtoken",token);
//   console.log(token);
//   this.http.post(Urlbase.auth+"/UpdateFCMtoken",{token:token||"-1"}).subscribe(value1 => {
//     console.log("OK");console.log(value1);
//   },error2 => {
//     console.log("ERRRO FCMupdate: ");console.log(error2);
//   })
// });
//
// // ionic push notification example
// this.fcm.onNotification().subscribe(data => {
//   console.log(data);
//   if (data.wasTapped) {
//     console.log('Received in background');
//   } else {
//     console.log('Received in foreground');
//   }
//   this.currentMessage.next(data);
// });
//
// // refresh the FCM token
// this.fcm.onTokenRefresh().subscribe(token => {
//   console.log(token);
// });
