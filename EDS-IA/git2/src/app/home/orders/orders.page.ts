import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {BillingService, Pedidos} from '../../Services/billing.service';
import {ModalController} from '@ionic/angular';
import {OrderViewerComponent} from '../../common/order-viewer/order-viewer.component';
import * as moment from 'moment';
import {AccountService} from '../../Services/AccountService';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {

  constructor(
    private router:Router,
    private httpClient: HttpClient,
    public billingService : BillingService,
    public modalController: ModalController,
    private accountService: AccountService
  ) { }

  async ngOnInit() {
    this.ActualizarActivos(null);
    this.ActualizarFinalizadas(null);
  }

  async ActualizarActivos(evento){
    this.billingService.ListadoPedidosActivos = [];
    await this.billingService.GetActiveOrderList(this.accountService.usuarioCompleto.third.id_third);
    if(evento != null){evento.target.complete();}
  }


  async ActualizarFinalizadas(evento){
    await this.billingService.GetEndedOrderList(this.accountService.usuarioCompleto.third.id_third);
    if(evento != null){evento.target.complete();}
  }

  GoToHome(){
    this.router.navigateByUrl("/home");
  }

  BillStateToText(id:number){
    switch (id) {
      case 801: return "Recibido";
      case 807: return "Procesado Con Novedad";
      case 802: return "Procesado";
      case 803: return "En Camino";
      case 808: return "Entregado Con Novedad";
      case 804: return "Entregado";
      case 902: return "Alistando el pedido"
      case 705: return "Calificado";
      case 99: return "Cancelado";
      default: return id;
    }
  }

  FormatedDate(fecha:Date){
    return moment(fecha).format("YYYY-MM-DD hh:mm a");
  }

  async ClickOrder(pedido:Pedidos,From:number){//from 0 es normal, from 1 es repetir, from 2 es calificar
    const modal = await this.modalController.create({
      component: OrderViewerComponent,
      componentProps:{
        From:From,
        Pedido:pedido,
      }
    });
    return await modal.present();
  }
}
