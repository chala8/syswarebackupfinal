package com.sysware.eds1a;

import android.os.Bundle;

import com.getcapacitor.BridgeActivity;
import com.getcapacitor.Plugin;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

public class MainActivity extends BridgeActivity {
  private FirebaseAnalytics mFirebaseAnalytics;
  @Override
  public void onCreate(Bundle savedInstanceState) {
    mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    super.onCreate(savedInstanceState);

    // Initializes the Bridge
    this.init(savedInstanceState, new ArrayList<Class<? extends Plugin>>() {{
      // Additional plugins you've installed go here
      // Ex: add(TotallyAwesomePlugin.class);
      add(jp.rdlabo.capacitor.plugin.firebase.crashlytics.FirebaseCrashlyticsPlugin.class);
    }});
  }
}
