import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/services/LocalStorage';
/// <reference path ="../../node_modules/@types/jquery/index.d.ts"/>
declare var $: any 

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.css']
})
export class GaleriaComponent implements OnInit {

  constructor(private router: Router,private httpClient: HttpClient,
    private locStorage: LocalStorage) { }

  listaPublicacionesPadre;
  ngOnInit() {
    this.locStorage.setmapStatus(0);
    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesGalery?idList=2").subscribe(
      response =>{
        //@ts-ignore
        this.listaPublicacionesPadre = response
      }
    )
  }

  irMapa(){
    this.router.navigateByUrl('/map');

  }


  

}
