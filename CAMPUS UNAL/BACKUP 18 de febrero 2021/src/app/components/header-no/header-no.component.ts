import { Component, OnInit } from '@angular/core';
import { LocalStorage } from 'src/app/services/LocalStorage';

@Component({
  selector: 'app-header-no',
  templateUrl: './header-no.component.html',
  styleUrls: ['./header-no.component.css']
})
export class HeaderNoComponent implements OnInit {

  constructor(public locStorage: LocalStorage) { }

  ngOnInit() {
  }
 
}
