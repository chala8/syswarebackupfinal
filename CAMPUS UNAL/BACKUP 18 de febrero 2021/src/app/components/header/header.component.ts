import { Component, OnInit } from '@angular/core';
import { LocalStorage } from 'src/app/services/LocalStorage';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public locStorage: LocalStorage) { }

  ngOnInit() {
  }

}
