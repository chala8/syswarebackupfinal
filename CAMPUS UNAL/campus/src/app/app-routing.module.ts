import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdministratorComponent } from './components/administrator/administrator.component';
import { GaleriaComponent } from './components/galeria/galeria.component';
import { LandingComponent } from './components/landing/landing.component';
import { MapComponent } from './components/map/map.component';
import { ModalComponent } from './components/modal/modal.component';


const routes: Routes = [ 
  { path: '', component: LandingComponent },
  { path: 'landing', component: LandingComponent },
  { path: 'map', component: MapComponent },
  { path: 'galery', component: GaleriaComponent },
  { path: 'admin', component: AdministratorComponent },
  { path: 'modal', component: ModalComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


