import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.css']
})
export class GaleriaComponent implements OnInit {

  constructor(private httpClient: HttpClient) { }

  listaPublicacionesPadre;
  ngOnInit() {
    this.httpClient.get("http://168.176.6.92:8580/v1/campus/getPublicaciones?idList=2").subscribe(
      response =>{
        //@ts-ignore
        this.listaPublicacionesPadre = response
      }
    )
  }


  

}
