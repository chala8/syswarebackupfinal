import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ModalComponent } from '../modal/modal.component';



@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor(private httpClient: HttpClient,
    private router: Router,
    public dialog: MatDialog,) { }

  user="";
  password="";

  ngOnInit() {
  }

  ingresar(){
    this.httpClient.post("http://168.176.6.92:8580/v1/campus/login?username="+this.user+"&password="+this.password,{}).subscribe(
      response => {
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Login Exitoso!", message:"Bienvenido "+this.user+", tu rol es administrador."},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
        }else{
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Login Exitoso!", message:"Bienvenido "+this.user+", tu rol es usuario."},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
        }
        
        this.httpClient.get("http://168.176.6.92:8580/v1/campus/getCategorias").subscribe(
          response2 => {
            console.log("THIS IS LIST 2: ", response2)
            this.router.navigateByUrl('/admin');
          }
        )
      }
    )
  }

}
