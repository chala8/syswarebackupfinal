import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatCheckboxChange, MatDialog } from '@angular/material';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  constructor(private httpClient: HttpClient,
    public dialog: MatDialog,) { }
  
  stringTextoPublicacion = "";

  idPregunta = -1;
  pregunta = "";
  latitud = 0.0;
  longitud = 0.0;
  distancia = 0.0;
  username = "";
  listaPublicacionesPadre;
  listaPreguntas;
  preguntaElement;

  userList = ["jpelaez","daachalabu","gernanp","faleti"];

  ngOnInit() {
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.get("http://168.176.6.92:8580/v1/campus/getPregunta").subscribe(
      response =>{
        //@ts-ignore
        this.listaPreguntas = response
        console.log("THIS IS PREGUNTA LIST: "+this.listaPreguntas);
        //**SE SETEA UNA PREGUNTA INICIAL
        this.preguntaElement = response[0];
        this.idPregunta = response[0].id_pregunta;
        this.pregunta = response[0].pregunta;
      }
    )
    //**SE LEE EL USERNAME DE LOCALSTORAGE
    let posicion = Math.ceil(Math.random() * 4)-1;
    this.username = this.userList[posicion];
    
    //**SE CARGAN LAS PUBLICACIONES
    this.httpClient.get("http://168.176.6.92:8580/v1/campus/getPublicaciones?idList=2").subscribe(
      response =>{
        //@ts-ignore
        this.listaPublicacionesPadre = response
        console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
      }
    )
    //**SE CAPTURA LATITUD Y LONGITUD
    this.latitud = Math.ceil(Math.random() * 90) * (Math.round(Math.random()) ? 1 : -1);
    this.longitud = Math.ceil(Math.random() * 180) * (Math.round(Math.random()) ? 1 : -1);

    // this.getPosition().then(pos=>
    //   {
    //      console.log(`Positon: ${pos.lng} ${pos.lat}`);
    //      this.latitud = pos.lat;
    //      this.longitud = pos.lng;
    //   }
    // )
    //**SE CALCULA LA DISTANCIA

    this.httpClient.get("http://168.176.6.92:8580/v1/campus/calculadistanciagps?lat="+this.latitud+"&longi="+this.longitud).subscribe(
      response =>{
        //@ts-ignore
        this.distancia = Math.round(response * 10)/10;
        console.log("THIS IS DISTANCIA: "+this.distancia)
      }
    )
  }

  postStructure = {
    imgUrl: "",
    name: "",
    km: "",
    post: "",
    idPublication: "",
    listChildPost: []
  }


  setPregunta(){
    console.log("EXECUTING");
    let item = this.listaPreguntas.find(element => element.id_pregunta == this.idPregunta)

    this.pregunta  = item.pregunta;

  }


  fileBase64String = null;
  fileExtension = null;
  urlUploaded = "Sin Archivo";
  imgName;
  imgURL;
  setFileImage(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
        console.log(reader.result);
        console.log("EXTENSION: ", event.target.files[0].name.split(".").pop());
        this.fileBase64String = reader.result;
        this.fileExtension = event.target.files[0].name.split(".").pop();
        this.urlUploaded = event.target.files[0].name;
        console.log("FILE NAME: ",event.target.files[0].name);
        this.imgName = event.target.files[0].name;
        this.imgURL = reader.result;
    };
  }

  getPublicacionesHijas(idPadre){

    this.httpClient.get("http://168.176.6.92:8580/v1/campus/getPublicacionesHijo?idpubpadre="+idPadre).subscribe(
      response =>{
        //@ts-ignore
        this.postStructure.listChildPost = response
        console.log("THIS IS PUBLICATIONS LIST: "+this.postStructure.listChildPost )
      }
    )

  }

  setModalInfo(item){

    this.getPublicacionesHijas(item.id_PUBLICACION);

    this.postStructure.imgUrl = item.img_URL;
    this.postStructure.name = item.username;
    this.postStructure.km = item.distancia_KMS;
    this.postStructure.post = item.publicacion;
    this.postStructure.idPublication = item.id_PUBLICACION
    console.log("I AM ITEM: ",item)

  }

  

  stringPublicationModal = "";
  postModalPublication(){
    this.httpClient.post("http://168.176.6.92:8580/v1/campus/postPublicacion?IDPREGUNTA="+this.idPregunta+"&IDPUBLICACIONPADRE="+this.postStructure.idPublication+"&FECHAPUBLICACION="+(new Date()).toString()+"&USERNAME="+this.username+"&PUBLICACION="+this.stringPublicationModal+"&IMGURL=%20&IDESTADOPUBLICACION=1&LATITUD="+this.latitud+"&LONGITUD="+this.longitud+"&KMS="+this.distancia,{}).subscribe(
      response =>{
        if(response == 1){
          this.getPublicacionesHijas(this.postStructure.idPublication);
          this.stringPublicationModal = "";

        }else{
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Se presento un error al cargar la publicacion.", message:"Vuelve a Intentarlo en unos minutos o contactate con el administrador."},
            disableClose: true
          }).afterClosed().subscribe(response=> {
          });
        }
      }
    )
  }

  generateMapCoordinates(distancia){

    let x = 0;
    let y = 0;


     if(distancia <= 9000){
       let r = 13*Math.pow(Math.random(),1/2);
       let theta = Math.random()*2*Math.PI;
       let x = 118 + r * Math.cos(theta);
       let y = 49.5 + r * Math.sin(theta);
       console.log("distancia: ",distancia);
       console.log("X: ",x,", Y: ",y);
       return {y : y, x :x}
     };

     if(distancia <= 9001 && distancia>10000){

       let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

       let a = 34.5;
       let b = 16
    
       let x = 118+a*Math.cos(theta);
       let y = 49.5+b*Math.sin(theta);
       console.log("distancia: ",distancia);
       console.log("X: ",x,", Y: ",y);
       return {y : y, x :x}

     };


      if(distancia <= 10001 && distancia>20000){

        let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

        let a = 49;
        let b = 22

        let x = 118+a*Math.cos(theta);
        let y = 49.5+b*Math.sin(theta);
    
        console.log("distancia: ",distancia);
        console.log("X: ",x,", Y: ",y);
        return {y : y, x :x}

      };


      if(distancia <= 20001 && distancia>70000){

        let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

        let a = 61;
        let b = 27;

        let x = 118+a*Math.cos(theta);
        let y = 49.5+b*Math.sin(theta);
      
        console.log("distancia: ",distancia);
        console.log("X: ",x,", Y: ",y);
        return {y : y, x :x}

      };


     if(distancia <= 70001 && distancia>100000){

       let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

       let a = 70;
       let b = 32;

       let x = 118+a*Math.cos(theta);
       let y = 49.5+b*Math.sin(theta);

      // x = 118-70;
      // y = 49.5;
     
       console.log("distancia: ",distancia);
       console.log("X: ",x,", Y: ",y);
       return {y : y, x :x}

     };


    
    if(distancia <= 100001 ){

      let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

      let a = 82;
      let b = 37.5;

      let x = 118+a*Math.cos(theta);
      let y = 49.5+b*Math.sin(theta);

      console.log("distancia: ",distancia);
      console.log("X: ",x,", Y: ",y);
      return {y : y, x :x}

    };

  }



  getMapCoordinates(item){
    console.log({top : item.mapay+'%', left :item.mapax+'%'});
    return  {top : item.mapay+'%', left :item.mapax+'%'}
  }

  
  async postPublicacionInicial(){
    let x =0;
    let y =0;

    let responseXY = await this.generateMapCoordinates(this.distancia);
    
    await this.postPublicacionInicial2(responseXY);
  }


  async postPublicacionInicial2(responseXY){
    

    this.httpClient.post("http://168.176.6.92:8580/v1/campus/postPublicacion?IDPREGUNTA="+this.idPregunta+"&IDPUBLICACIONPADRE="+-1+"&FECHAPUBLICACION="+(new Date()).toString()+"&USERNAME="+this.username+"&PUBLICACION="+this.stringTextoPublicacion+"&IMGURL="+this.urlUploaded+"&IDESTADOPUBLICACION=1&LATITUD="+this.latitud+"&LONGITUD="+this.longitud+"&KMS="+this.distancia+"&X="+responseXY.x+"&Y="+responseXY.y,{file: this.fileBase64String, fileName: this.urlUploaded, format: this.fileExtension}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"¡Publicación exitosa!", message:"En las próximas 24 horas habilitaremos tu publicación"},
            disableClose: true
          }).afterClosed().subscribe(response=> {
          });
          this.httpClient.get("http://168.176.6.92:8580/v1/campus/getPublicaciones?idList=1,2").subscribe(
            response =>{
              //@ts-ignore
              this.listaPublicacionesPadre = response
              console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
              this.httpClient.post("http://168.176.6.92:8580/v1/campus/email?sender=daachalabu&recipient=smforiguab&message=se%20registro%20una%20publicacion%20nueva",{}).subscribe(
                response =>{
                  if(response == 1){
                    console.log("EMAIL SENT")
                  }else{
                    console.log("EMAIL NOT SENT")
                  }
                }
              )

            }
          )
        }else{
          let dialogRef = this.dialog.open(ModalComponent, {
            width: '60vw',
            data: {title:"Se presento un error al cargar la publicacion.", message:"Vuelve a Intentarlo en unos minutos o contactate con el administrador."},
            disableClose: true
          }).afterClosed().subscribe(response=> {
          });
        }
      }
    )
  }



  getMapLocation(latitud,longitud){

    let latitudStep1 =(45+(latitud/10))
    let longitudStep1 =(115+(longitud/10))
  

    let latitudFinal = latitudStep1;
    let longitudFinal = longitudStep1;

    console.log("top :", latitudFinal+'%', "left :" ,longitudFinal+'%')
    return {top : latitudFinal+'%', left :longitudFinal+'%'}
  }


  getPosition(): Promise<any>
  {
    return new Promise((resolve, reject) => {

      navigator.geolocation.getCurrentPosition(resp => {

          resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
        },
        err => {
          reject(err);
          console.log("ERORR: ",err)
        });
    });
  }
  

}
