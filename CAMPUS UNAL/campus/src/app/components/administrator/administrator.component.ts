import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.css']
})
export class AdministratorComponent implements OnInit {

  constructor(private httpClient: HttpClient,
    public dialog: MatDialog,) { }

  listaPreguntas;

  listaPublicacionesPadre;

  listaCategorias;
  idCategoria;

  ngOnInit() {

    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.get("http://168.176.6.92:8580/v1/campus/getPregunta").subscribe(
      response =>{
        //@ts-ignore
        this.listaPreguntas = response
        console.log("THIS IS PREGUTNAS LIST: "+response)
      }
    )

    this.httpClient.get("http://168.176.6.92:8580/v1/campus/getCategorias").subscribe(
      response =>{
        //@ts-ignore
        this.listaCategorias = response
        console.log("THIS IS PREGUTNAS LIST: "+response)
      }
    )

    this.httpClient.get("http://168.176.6.92:8580/v1/campus/getPublicaciones?idList=1,2,3").subscribe(
      response =>{
        //@ts-ignore
        this.listaPublicacionesPadre = response
        console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
      }
    )

  }


  pregunta = "";
  id_categoria = "";

  postPregunta(){
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.post("http://168.176.6.92:8580/v1/campus/crearPregunta?pregunta="+this.pregunta+"&id_categoria="+this.idCategoria,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Exito!", message:"Pregunta creada con exito."},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
          this.httpClient.get("http://168.176.6.92:8580/v1/campus/getPregunta").subscribe(
            response =>{
              //@ts-ignore
              this.listaPreguntas = response
            }
          )
        }           
      }
    )
  }


  categoria = "";

  postCategoria(){
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.post("http://168.176.6.92:8580/v1/campus/crearCategoria?categoria="+this.categoria,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Exito!", message:"Categoria creada con exito."},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
          this.httpClient.get("http://168.176.6.92:8580/v1/campus/getCategorias").subscribe(
            response =>{
              //@ts-ignore
              this.listaCategorias = response
            }
          )
        }           
      }
    )
  }






  deletePregunta(id){
        //**SE CARGA LA LISTA DE PREGUNTAS */
        this.httpClient.post("http://168.176.6.92:8580/v1/campus/eliminarPregunta?id_pregunta="+id,{}).subscribe(
          response =>{
            if(response == 1){
              let dialogRef = this.dialog.open(ModalComponent, {
                data: {title:"Exito!", message:"Pregunta Eliminada con exito."},
                disableClose: false
              }).afterClosed().subscribe(response=> {
              });
              this.httpClient.get("http://168.176.6.92:8580/v1/campus/getPregunta").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaPreguntas = response
                }
              )
            }           
          }
        )
  }


  aceptPublication(item){

    this.httpClient.post("http://168.176.6.92:8580/v1/campus/ACTUALIZAR_ESTADO_PUBLICACION?idpublicacion="+item.id_PUBLICACION+"&idestadopublicacion="+2,{}).subscribe(
          response =>{
            if(response == 1){
              this.httpClient.get("http://168.176.6.92:8580/v1/campus/email?sender=daachalabu&recipient="+item.username+"&message=se%20acepto%20el%20registro&20de&20tu%20una%20publicacion%20nueva").subscribe(
                response =>{
                  if(response == 1){
                    console.log("EMAIL SENT")
                  }else{
                    console.log("EMAIL NOT SENT")
                  }
                }
              )
              let dialogRef = this.dialog.open(ModalComponent, {
                data: {title:"Exito!", message:"Publicacion creada con exito."},
                disableClose: false
              }).afterClosed().subscribe(response=> {
              });
              this.httpClient.get("http://168.176.6.92:8580/v1/campus/getPublicaciones?idList=1,2,3").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaPublicacionesPadre = response
                  
                }
              )
            }           
          }
        )

  }


  noPublication(item){

    console.log()
    this.httpClient.post("http://168.176.6.92:8580/v1/campus/ACTUALIZAR_ESTADO_PUBLICACION?idpublicacion="+item.id_PUBLICACION+"&idestadopublicacion="+3,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Exito!", message:"Pregunta rechazada con exito."},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
          this.httpClient.get("http://168.176.6.92:8580/v1/campus/getPublicaciones?idList=1,2,3").subscribe(
            response =>{
              //@ts-ignore
              this.listaPublicacionesPadre = response
            }
          )
        }           
      }
    )


  }




  postName="";
  postDistance="";
  postMessage="";
  listChildPost;
  imgUrl="";


  setModalData(item){

    this.postName=item.username;
    this.postDistance=item.distancia_KMS;
    this.imgUrl=item.img_URL;
    this.postMessage=item.publicacion;


    this.httpClient.get("http://168.176.6.92:8580/v1/campus/getPublicacionesHijo?idpubpadre="+item.id_PREGUNTA).subscribe(
      response =>{
        //@ts-ignore
        this.listChildPost = response
        console.log("THIS IS PUBLICATIONS LIST: "+this.listChildPost )
      }
    )

  }

  
}
