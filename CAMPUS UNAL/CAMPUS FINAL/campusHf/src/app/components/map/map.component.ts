import { HttpClient } from '@angular/common/http';
import { Component, OnInit, } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/services/LocalStorage';
import * as $ from "jquery";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit  {

  constructor(private httpClient: HttpClient,
    private router: Router,
    public dialog: MatDialog,
    public locStorage: LocalStorage) { }

  ngOnInit() {   

  $('.login_sup').addClass('login_sup_cp');
    
  this.loadCSS("assets/css/frontend.css");
  this.loadCSS("assets/css/accesibilidad.css");
  this.loadCSS("assets/css/bootstrap.min.css");
  this.loadCSS("assets/css/reset.css");
  this.loadCSS("assets/css/normalize.css");
  this.loadCSS("assets/css/unal.css");
  this.loadCSS("assets/css/base.css");
  this.loadCSS("assets/css/tablet.css");
  this.loadCSS("assets/css/phone.css");
  this.loadCSS("assets/css/small.css");
  this.loadCSS("assets/css/printer.css");

  this.loadCSS("assets/css/mapa.css");
  this.loadCSS("assets/css/adjuntos.css");
  this.loadCSS("assets/css/animate.css");
  this.loadCSS("assets/css/nice-select.css");



  this.loadScript('assets/js/jquery.js');
  this.loadScript('assets/js/unal.js');
  this.loadScript('assets/js/accesibilidad.js');
  this.loadScript('assets/js/bootstrap.bundle.min.js');
  this.loadScript('assets/js/jquery.easing.1.3.js');
  this.loadScript('assets/js/jquery.stellar.min.js');
  this.loadScript('assets/js/mapa.js');
  this.loadScript('assets/js/aos.js');
  this.loadScript('assets/js/main.js');
  this.loadScript('assets/js/jsSelect/jquery.nice-select.js');

  }

  public unloadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.removeChild(script);
  }

  
  public loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

  loadCSS(url) {
    // Create link
    let link = document.createElement('link');
    link.href = url;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    
    let head = document.getElementsByTagName('head')[0];
    let links = head.getElementsByTagName('link');
    let style = head.getElementsByTagName('style')[0];
    
    // Check if the same style sheet has been loaded already.
    let isLoaded = false;  
    for (var i = 0; i < links.length; i++) {
      var node = links[i];
      if (node.href.indexOf(link.href) > -1) {
        isLoaded = true;
      }
    }
    if (isLoaded) return;
    head.insertBefore(link, style);
  }


  logout(){
    this.locStorage.setUsername("");
    this.locStorage.setLogginState(0);
    this.router.navigateByUrl("/landing");
  }


}
