import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/services/LocalStorage';
import { ModalComponent } from '../modal/modal.component';
import * as $ from "jquery";

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor(private httpClient: HttpClient,
    private router: Router,
    private dialog: MatDialog,
    public locStorage: LocalStorage) { }


  user="";
  password="";


  ngOnInit() {

    this.loadCSS("assets/css/frontend.css");
    this.loadCSS("assets/css/accesibilidad.css");
    this.loadCSS("assets/css/bootstrap.min.css");
    this.loadCSS("assets/css/reset.css");
    this.loadCSS("assets/css/normalize.css");
    this.loadCSS("assets/css/unal.css");
    this.loadCSS("assets/css/base.css");
    this.loadCSS("assets/css/tablet.css");
    this.loadCSS("assets/css/phone.css");
    this.loadCSS("assets/css/small.css");
    this.loadCSS("assets/css/printer.css");



    this.loadScript('assets/js/jquery.js');
    this.loadScript('assets/js/unal.js');
    this.loadScript('assets/js/accesibilidad.js');
    this.loadScript('assets/js/bootstrap.bundle.min.js');
    this.loadScript('assets/js/jquery.easing.1.3.js');
    this.loadScript('assets/js/jquery.stellar.min.js');
    this.loadScript('assets/js/aos.js');
    this.loadScript('assets/js/main.js');

  }

  public loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

  loadCSS(url) {
    // Create link
    let link = document.createElement('link');
    link.href = url;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    
    let head = document.getElementsByTagName('head')[0];
    let links = head.getElementsByTagName('link');
    let style = head.getElementsByTagName('style')[0];
    
    // Check if the same style sheet has been loaded already.
    let isLoaded = false;  
    for (var i = 0; i < links.length; i++) {
      var node = links[i];
      if (node.href.indexOf(link.href) > -1) {
        isLoaded = true;
      }
    }
    if (isLoaded) return;
    head.insertBefore(link, style);
  }


  
  irMapa(){
    this.router.navigateByUrl('/map');

  }

  ingresar(){
    let encodedUser = btoa(this.user);
    let encodedPassword = btoa(this.password);
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/login?username="+encodedUser+"&password="+encodedPassword,{}).subscribe(
      response => {

        //SE AUTENTICO BIEN Y ES ADMIN
        if(response == 1){
          this.locStorage.setUsername(this.user);
          this.locStorage.setLogginState(2);
           let dialogRef = this.dialog.open(ModalComponent, {
            panelClass: "dialog-responsive",
            data: {title:"Autenticacion Exitosa!", message:"Bienvenido "+this.user+", tu rol es administrador."},
            disableClose: false
           }).afterClosed().subscribe(response=> {
            this.router.navigateByUrl('/admin');
          
           });
        }


        //SE AUTENTICO BIEN Y NO ES ADMIN
        if(response == 0){
          this.locStorage.setUsername(this.user);
          this.locStorage.setLogginState(1);
           let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Autenticacion Exitosa!", message:"Bienvenido "+this.user+", tu rol es usuario."},
            disableClose: false
           }).afterClosed().subscribe(response=> {
            this.router.navigateByUrl('/map');

           });
        }


        //NO AUTENTICO POR ERROR EN BACKEND
        if(response == -1){
          this.locStorage.setLogginState(0);
           let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Error Autenticando!", message:"Se presento un error en el servicio de autenticacion."},
            disableClose: false
           }).afterClosed().subscribe(response=> {
           });
        }

        //NO AUTENTICO POR DATOS INCORRECTOS
        if(response == 2){
          this.locStorage.setLogginState(0);
           let dialogRef = this.dialog.open(ModalComponent, {
             
            data: {title:"Error Autenticando!", message:"Revisa tu usuario y contraseña."},
            disableClose: false
           }).afterClosed().subscribe(response=> {
           });
        }
      })
  }


  mapNoLoggin(){
    this.locStorage.setLogginState(0);
  
    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getCategorias").subscribe(
      response2 => {
        console.log("THIS IS LIST 2: ", response2)
        this.router.navigateByUrl('/map');

      }
    )
  }
  

}
