import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  title = "";
  message = "";

  ngOnInit() {
    this.loadCSS("assets/css/frontend.css");
    this.loadCSS("assets/css/accesibilidad.css");
    this.loadCSS("assets/css/bootstrap.min.css");
    this.loadCSS("assets/css/reset.css");
    this.loadCSS("assets/css/normalize.css");
    this.loadCSS("assets/css/unal.css");
    this.loadCSS("assets/css/base.css");
    this.loadCSS("assets/css/tablet.css");
    this.loadCSS("assets/css/phone.css");
    this.loadCSS("assets/css/small.css");
    this.loadCSS("assets/css/printer.css");
    this.loadCSS("assets/css/mapa.css");


    this.title = this.data.title;
    this.message = this.data.message;
  }

  public loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    body.appendChild(script);
  }

  loadCSS(url) {
    // Create link
    let link = document.createElement('link');
    link.href = url;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    
    let head = document.getElementsByTagName('head')[0];
    let links = head.getElementsByTagName('link');
    let style = head.getElementsByTagName('style')[0];
    
    // Check if the same style sheet has been loaded already.
    let isLoaded = false;  
    for (var i = 0; i < links.length; i++) {
      var node = links[i];
      if (node.href.indexOf(link.href) > -1) {
        isLoaded = true;
      }
    }
    if (isLoaded) return;
    head.insertBefore(link, style);
  }

  close(){
    this.dialogRef.close();
  }

}
