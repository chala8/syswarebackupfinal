import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PathLocationStrategy, LocationStrategy } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './components/landing/landing.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MapComponent } from './components/map/map.component';
import { AdministratorComponent } from './components/administrator/administrator.component';
import { GaleriaComponent } from './components/galeria/galeria.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';
import { CanActivateViaAuthGuardService } from './services/CanActivateViaAuthGuard/can-activate-via-auth-guard.service';
import { MatDialogModule, MatProgressSpinnerModule, MatSelectModule } from '@angular/material';
import { ModalComponent } from './components/modal/modal.component';
import * as $ from "jquery";


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    MapComponent,
    AdministratorComponent,
    GaleriaComponent,
    ModalComponent,
  ],
  imports: [
    // MatProgressSpinnerModule,
    NgxPaginationModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    // MatSelectModule,
  ],
  providers: [],
  entryComponents: [ModalComponent],
  bootstrap: [AppComponent]
})

export class AppModule { }