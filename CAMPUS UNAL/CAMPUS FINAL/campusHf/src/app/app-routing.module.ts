import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LandingComponent } from './components/landing/landing.component';

import { AdministratorComponent } from './components/administrator/administrator.component';
import { GaleriaComponent } from './components/galeria/galeria.component';
import { MapComponent } from './components/map/map.component';
import { CanActivateViaAuthGuardService } from './services/CanActivateViaAuthGuard/can-activate-via-auth-guard.service';
import { LandingProtectGuardService } from './services/LandingProtectGuard/landing-protect-guard.service';

//import { ModalComponent } from './components/modal/modal.component';


const routes: Routes = [ 
  { path: '', component: LandingComponent,  },
  { path: 'landing', component: LandingComponent, },
  { path: 'map', component: MapComponent },
  { path: 'galery', component: GaleriaComponent },
  { path: 'admin', component: AdministratorComponent,  },
  // { path: 'modal', component: ModalComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }