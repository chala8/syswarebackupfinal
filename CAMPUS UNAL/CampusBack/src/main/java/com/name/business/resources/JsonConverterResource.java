package com.name.business.resources;

import com.codahale.metrics.annotation.Timed;
import com.name.business.businesses.JsonConverterBusiness;
//import com.name.business.entities.*;
import com.name.business.entities.*;
import com.name.business.representations.Base64FileDTO;
import com.name.business.representations.itemDTO;
import com.name.business.utils.exeptions.ExceptionResponse;
import com.name.business.utils.exeptions.IException;
import fj.data.Either;
import org.apache.commons.codec.binary.Base64;
import org.skife.jdbi.v2.sqlobject.Bind;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.naming.*;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;


@Path("/campus")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JsonConverterResource {
    private JsonConverterBusiness jsonConverterBusiness;

    public JsonConverterResource(JsonConverterBusiness jsonConverterBusiness) {
        this.jsonConverterBusiness = jsonConverterBusiness;
    }


    @Path("/login")
    @POST
    @Timed
    @PermitAll()
    public Response login(@QueryParam("username") String username,
                          @QueryParam("password") String password){

        Response response;
        Long getResponseGeneric;
        String userLDAP="APPuser136";
        String passwordLDAP="2J6XNAP5THKM2TT4";

        //AUTENTICO AL USUARIO
        Boolean isAuth = jsonConverterBusiness.getAuthLDAP(username,password);

        //VERIFICO SI ES ADMIN
        if(isAuth){
            Long isAdmin = jsonConverterBusiness.getAdmin(new String(Base64.decodeBase64(username)));
            getResponseGeneric = isAdmin;
        }else{
            getResponseGeneric = new Long(2);
        }

        /*
        //RECIBO DATOS DE LDAP
            //--AUTENTICAR USUARIO LDAP CON PRIVILEGIOS
            Hashtable env = new Hashtable(11);

            env.put(Context.PROVIDER_URL, "ldap://168.176.239.1:389");
            env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");

            // Authenticate as S. User and password "mysecret"
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, "uid="+userLDAP+", ou=services, o=unal.edu.co");
            env.put(Context.SECURITY_CREDENTIALS, passwordLDAP);




            try {
                DirContext ctx = new InitialDirContext(env);
                ctx.addToEnvironment("java.naming.referral","follow");

                NamingEnumeration nenum = ctx.list("givenName");

                while(nenum.hasMore()){
                    NameClassPair ncp = (NameClassPair) nenum.nextElement();
                    //Mostrar los nombres de las entradas
                    System.out.println("- "+ncp.getName());
                }


                ctx.getAttributes("principal");
                ctx.close();
            } catch (NamingException e) {
                e.printStackTrace();
            }


         */

        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;

    }

    @Path("/usuario")
    @GET
    @Timed
    @PermitAll()
    public Response getUsuario(@QueryParam("username") String username){

        Response response;
        String getResponseGeneric;
        String userLDAP="APPuser136";
        String passwordLDAP="2J6XNAP5THKM2TT4";


        //RECIBO DATOS DE LDAP
            //--AUTENTICAR USUARIO LDAP CON PRIVILEGIOS
            Hashtable env = new Hashtable(11);

            env.put(Context.PROVIDER_URL, "ldap://168.176.239.1:389");
            env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");

            // Authenticate as S. User and password "mysecret"
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, "uid="+userLDAP+", ou=services, o=unal.edu.co");
            env.put(Context.SECURITY_CREDENTIALS, passwordLDAP);


            try {
                Attributes attrs;
                DirContext ctx = new InitialDirContext(env);
                attrs = ctx.getAttributes( "uid="+username+", ou=people, o=unal.edu.co" );
                String nombre= attrs.get( "givenName" ).toString();
                String apellido= attrs.get( "sn" ).toString();
                System.out.println(nombre.replace("givenName: ","").split(" ")[0] +" "+apellido.replace("sn: ","").charAt(0)+".");
                getResponseGeneric = nombre.replace("givenName: ","").split(" ")[0] +" "+apellido.replace("sn: ","").charAt(0)+".";
                response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
                ctx.close();
                return response;
            } catch (NamingException e) {
                e.printStackTrace();
            }



        response=Response.status(Response.Status.OK).entity(" ").build();
        return response;



    }

    @Path("/getCategorias")
    @GET
    @Timed
    @PermitAll()
    public Response getCategorias(){

        Response response;
        Either<IException, List<categoria>> getResponseGeneric = jsonConverterBusiness.getCategorias();

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/getPregunta")
    @GET
    @Timed
    @PermitAll()
    public Response getPregunta(){

        Response response;
        Either<IException, List<pregunta>> getResponseGeneric = jsonConverterBusiness.getPreguntas();

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/getPreguntaString")
    @GET
    @Timed
    @PermitAll()
    public Response getPreguntaString(){

        Response response;
        Either<IException, String> getResponseGeneric = jsonConverterBusiness.getPreguntaString();

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }





    @Path("/changeStatePublicaciones")
    @POST
    @Timed
    @PermitAll()
    public Response changeStatePublicaciones(@QueryParam("list") String list,@QueryParam("state") Long state) {

        Response response;

        Long getResponseGeneric;
        try{
            String str[] = list.split(",");
            List<String> al = new ArrayList<String>();
            al = Arrays.asList(str);
            for(String s: al){
                    jsonConverterBusiness.ACTUALIZAR_ESTADO_PUBLICACION(new Long(s), state);

            }

            getResponseGeneric = new Long(1);
        }catch(Exception e){
            e.printStackTrace();

            getResponseGeneric = new Long(0);
        }



            response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();


        return response;
    }



    @Path("/email")
    @POST
    @Timed
    @PermitAll()
    public Response email(@QueryParam("sender") String sender,
                          @QueryParam("recipient") String recipient,
                          @QueryParam("message") String message ){

        Response response;

        Long getResponseGeneric = jsonConverterBusiness.sendEmail(sender,recipient,message);

        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;

    }





    @Path("/crearPregunta")
    @POST
    @Timed
    @PermitAll()
    public Response crear_pregunta(@QueryParam("pregunta") String pregunta,
                                   @QueryParam("id_categoria") Long id_categoria){

        Response response;
        Long getResponseGeneric = jsonConverterBusiness.crear_pregunta(pregunta,id_categoria);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;

    }


    @Path("/eliminarPregunta")
    @POST
    @Timed
    @PermitAll()
    public Response eliminar_pregunta(@QueryParam("id_pregunta") Long id_pregunta){

        Response response;
        Long getResponseGeneric = jsonConverterBusiness.eliminar_pregunta(id_pregunta);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;

    }



    @Path("/eliminarPreguntasCheck")
    @POST
    @Timed
    @PermitAll()
    public Response eliminarPreguntasCheck(@QueryParam("list") String list){


        Response response;

        Long getResponseGeneric;
        try{
            String str[] = list.split(",");
            List<String> al = new ArrayList<String>();
            al = Arrays.asList(str);
            for(String s: al){
                jsonConverterBusiness.eliminar_pregunta(new Long(s));

            }

            getResponseGeneric = new Long(1);
        }catch(Exception e){
            e.printStackTrace();

            getResponseGeneric = new Long(0);
        }



        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();


        return response;


    }


    @Path("/eliminarUsersCheck")
    @POST
    @Timed
    @PermitAll()
    public Response eliminarUsersCheck(@QueryParam("list") String list){


        Response response;

        Long getResponseGeneric;
        try{
            String str[] = list.split(",");
            List<String> al = new ArrayList<String>();
            al = Arrays.asList(str);
            System.out.println(list);
            for(String s: al){

                System.out.println(s);
                jsonConverterBusiness.eliminar_administrador(s);

            }

            getResponseGeneric = new Long(1);
        }catch(Exception e){
            e.printStackTrace();

            getResponseGeneric = new Long(0);
        }



        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();


        return response;


    }



    @Path("/getExcelURl")
    @POST
    @Timed
    @PermitAll()
    public Response getExcel(){

        Response response;
        Either<IException, String> getResponseGeneric = jsonConverterBusiness.getExcel();

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




    @Path("/modificarPregunta")
    @POST
    @Timed
    @PermitAll()
    public Response modificar_pregunta(@QueryParam("pregunta") String pregunta,
                                       @QueryParam("id_categoria") Long id_categoria,
                                       @QueryParam("id_pregunta") Long id_pregunta){

        Response response;
        Long getResponseGeneric = jsonConverterBusiness.modificar_pregunta(pregunta,id_categoria,id_pregunta);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;

    }



    @Path("/crearCategoria")
    @POST
    @Timed
    @PermitAll()
    public Response crear_categoria(@QueryParam("categoria") String categoria){

        Response response;
        Long getResponseGeneric = jsonConverterBusiness.crear_categoria(categoria);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;

    }




    @Path("/getPublicaciones")
    @GET
    @Timed
    @PermitAll()
    public Response getPublicaciones(@QueryParam("idList") String idList){

        Response response;

        String[] SplittedList = idList.split(",");
        List<Long> CastedList = new ArrayList<>();
        for(int n = 0;n<SplittedList.length;n++){
            CastedList.add(Long.parseLong(SplittedList[n]));
        }

        Either<IException, List<publicacion>> getResponseGeneric = jsonConverterBusiness.getPublicaciones(CastedList);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }

    @Path("/getPublicacionesPregunta")
    @GET
    @Timed
    @PermitAll()
    public Response getPublicacionesPregunta(@QueryParam("idList") String idList,@QueryParam("pregList") String pregList){

        Response response;

        String[] SplittedList = idList.split(",");
        List<Long> CastedList = new ArrayList<>();
        for(int n = 0;n<SplittedList.length;n++){
            CastedList.add(Long.parseLong(SplittedList[n]));
        }


        String[] SplittedList2 = pregList.split(",");
        List<Long> CastedList2 = new ArrayList<>();
        for(int n = 0;n<SplittedList2.length;n++){
            CastedList2.add(Long.parseLong(SplittedList2[n]));
        }

        Either<IException, List<publicacion>> getResponseGeneric = jsonConverterBusiness.getPublicacionesPregunta(CastedList,CastedList2);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/getPublicacionesById")
    @GET
    @Timed
    @PermitAll()
    public Response getPublicacionesById(@QueryParam("idPub") Long idPub){

        Response response;


        Either<IException, publicacion> getResponseGeneric = jsonConverterBusiness.getPublicacionesById(idPub);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/getPublicacionesGalery")
    @GET
    @Timed
    @PermitAll()
    public Response getPublicacionesGalery(@QueryParam("idList") String idList){

        Response response;

        String[] SplittedList = idList.split(",");
        List<Long> CastedList = new ArrayList<>();
        for(int n = 0;n<SplittedList.length;n++){
            CastedList.add(Long.parseLong(SplittedList[n]));
        }

        Either<IException, List<publicacion>> getResponseGeneric = jsonConverterBusiness.getPublicacionesGalery(CastedList);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @Path("/calculadistanciagps")
    @GET
    @Timed
    @PermitAll()
    public Response calculadistanciagps(@QueryParam("lat") Float lat,@QueryParam("longi") Float longi){

        Response response;

        System.out.println("LATITUD: "+lat);

        System.out.println("LONGITUD: "+longi);

        Either<IException, Float> getResponseGeneric = jsonConverterBusiness.calculadistanciagps(lat,longi);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/getPublicacionesHijo")
    @GET
    @Timed
    @PermitAll()
    public Response getPublicacionesHijo(@QueryParam("idpubpadre") Long idList){

        Response response;

        Either<IException, List<publicacion>> getResponseGeneric = jsonConverterBusiness.getPublicacionesHijo(idList);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/getEstadosPublicacion")
    @GET
    @Timed
    @PermitAll()
    public Response getEstadosPublicacion(){

        Response response;
        Either<IException, List<estadoPublicacion>> getResponseGeneric = jsonConverterBusiness.getEstadosPublicacion();

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/postPublicacion")
    @POST
    @Timed
    @PermitAll()
    public Response postPsublicacion(@QueryParam("IDPREGUNTA") Long IDPREGUNTA,
                                    @QueryParam("IDPUBLICACIONPADRE") Long IDPUBLICACIONPADRE,
                                    @QueryParam("FECHAPUBLICACION") String FECHAPUBLICACION,
                                    @QueryParam("USERNAME") String USERNAME,
                                    @QueryParam("PUBLICACION") String PUBLICACION,
                                    @QueryParam("IMGURL") String IMGURL,
                                    @QueryParam("IDESTADOPUBLICACION") Long IDESTADOPUBLICACION,
                                    @QueryParam("LATITUD") Float LATITUD,
                                    @QueryParam("LONGITUD") Float LONGITUD,
                                    @QueryParam("KMS") Float KMS,
                                    @QueryParam("X") Float X,
                                    @QueryParam("Y") Float Y,
                                    @QueryParam("USER") String USER,
                                    Base64FileDTO dto){

        Response response;
        if(dto.getFileName()!=null){
            Either<IException, Long>  imagePosted = jsonConverterBusiness.postImage(dto);
        }
        Long getResponseGeneric = jsonConverterBusiness.postPublicacion(IDPREGUNTA,
                IDPUBLICACIONPADRE,
                FECHAPUBLICACION,
                USERNAME,
                PUBLICACION,
                IMGURL,
                IDESTADOPUBLICACION,
                LATITUD,
                LONGITUD,
                KMS,X,Y,USER);



        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;

    }


    @Path("/ACTUALIZAR_ESTADO_PUBLICACION")
    @POST
    @Timed
    @PermitAll()
    public Response ACTUALIZAR_ESTADO_PUBLICACION(@QueryParam("idpublicacion") Long idpublicacion,
                                                  @QueryParam("idestadopublicacion") Long idestadopublicacion){

        Response response;
        Long getResponseGeneric = jsonConverterBusiness.ACTUALIZAR_ESTADO_PUBLICACION( idpublicacion,
                idestadopublicacion );
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;

    }


    @Path("/crear_administrador")
    @POST
    @Timed
    @PermitAll()
    public Response crear_administrador(@QueryParam("user") String user,@QueryParam("nombre") String nombre) {

        Response response;
        Long getResponseGeneric;
        getResponseGeneric = jsonConverterBusiness.crear_administrador(user,nombre);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }

    @Path("/eliminar_administrador")
    @POST
    @Timed
    @PermitAll()
    public Response eliminar_administrador(@QueryParam("user") String user) {

        Response response;
        Long getResponseGeneric;
        getResponseGeneric = jsonConverterBusiness.eliminar_administrador(user);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }



    @Path("/getUsers")
    @GET
    @Timed
    @PermitAll()
    public Response getUsers(){

        Response response;
        Either<IException, List<user>> getResponseGeneric = jsonConverterBusiness.getUsers();

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/getUsers2")
    @GET
    @Timed
    @PermitAll()
    public Response getUsersByUsername(@QueryParam("user") String user){

        Response response;
        Either<IException, List<user>> getResponseGeneric = jsonConverterBusiness.getUsersByUsername(user);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




    @Path("/getPublicationLike")
    @GET
    @Timed
    @PermitAll()
    public Response getPublicationLike(@QueryParam("phrase") String phrase){

        Response response;

        Either<IException, List<publicacionData>> getResponseGeneric = jsonConverterBusiness.getPublicationLike(phrase);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }




    @Path("/fixMapCoordinates")
    @POST
    @Timed
    @PermitAll()
    public Response fixMapCoordinates(@QueryParam("idPublicacion") Long idPublicacion,
                                      @QueryParam("mapax") Double mapax,
                                      @QueryParam("mapay") Double mapay){

        Long getResponseGeneric = jsonConverterBusiness.fixMapCoordinates(idPublicacion,mapax,mapay);
        return Response.status(Response.Status.OK).entity(getResponseGeneric).build();

    }



    @Path("/getPublicacionPosicion")
    @GET
    @Timed
    @PermitAll()
    public Response getPublicationPosicion(){

        Response response;

        Either<IException, List<publicacionPosicion>> getResponseGeneric = jsonConverterBusiness.getPublicationPosicion();

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @Path("/getPublicationPosicionIdPregunta")
    @GET
    @Timed
    @PermitAll()
    public Response getPublicationPosicionIdPregunta(@QueryParam("ID_PREGUNTA") Long ID_PREGUNTA){

        Response response;

        Either<IException, List<publicacionPosicion>> getResponseGeneric = jsonConverterBusiness.getPublicationPosicionIdPregunta(ID_PREGUNTA);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @Path("/getPublicacionPosicionById")
    @GET
    @Timed
    @PermitAll()
    public Response getPublicationPosicionById(@QueryParam("id_publicacion_posicion") Long ID_PUBLICACION_POSICION){

        Response response;

        Either<IException, List<publicacionPosicion>> getResponseGeneric = jsonConverterBusiness.getPublicationPosicionById(ID_PUBLICACION_POSICION);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/getPublicationPosicionByPublicacion")
    @GET
    @Timed
    @PermitAll()
    public Response getPublicationPosicionByPublicacion(@QueryParam("id_publicacion") Long ID_PUBLICACION){

        Response response;

        Either<IException, List<publicacionPosicion>> getResponseGeneric = jsonConverterBusiness.getPublicationPosicionByPublicacion(ID_PUBLICACION);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/CREAR_PUBLICACION_POSICION")
    @POST
    @Timed
    @PermitAll()
    public Response CREAR_PUBLICACION_POSICION(@QueryParam("X") Double X,
                                               @QueryParam("Y") Double Y,
                                               @QueryParam("ID_PUB") Long ID_PUB,
                                               @QueryParam("ID_PREG") Long ID_PREG) {

        Response response;
        Long getResponseGeneric;
        getResponseGeneric = jsonConverterBusiness.CREAR_PUBLICACION_POSICION(X,Y,ID_PUB,ID_PREG);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }




    @Path("/LIMPIAR_PUBLICACION_POSICION")
    @POST
    @Timed
    @PermitAll()
    public Response LIMPIAR_PUBLICACION_POSICION(@QueryParam("ID_PUB") Long ID_PUB) {

        Response response;
        Long getResponseGeneric;
        getResponseGeneric = jsonConverterBusiness.LIMPIAR_PUBLICACION_POSICION(ID_PUB);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }




    @Path("/ACTUALIZAR_PUB_POSICION")
    @POST
    @Timed
    @PermitAll()
    public Response ACTUALIZAR_PUB_POSICION(@QueryParam("X") Double X,
                                        @QueryParam("Y") Double Y,
                                        @QueryParam("ID_PUB_POS") Long ID_PUB_POS) {

        Response response;
        Long getResponseGeneric;
        getResponseGeneric = jsonConverterBusiness.ACTUALIZAR_PUB_POSICION(X,Y,ID_PUB_POS);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }




    @Path("/postCoordinates")
    @GET
    @Timed
    @PermitAll()
    public Response postCoordinates(){

        Response response;

        Either<IException, Long> getResponseGeneric = jsonConverterBusiness.postCoordinates();

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @Path("/updatePublicacionOnPosicion")
    @POST
    @Timed
    @PermitAll()
    public Response updatePublicacionOnPosicion(@QueryParam("ID_PUBLICACION") Long ID_PUBLICACION,
                                                @QueryParam("ID_PUBLICACION_POSICION") Long ID_PUBLICACION_POSICION) {
        Response response;
        Long getResponseGeneric;
        getResponseGeneric = jsonConverterBusiness.updatePublicacionOnPosicion(ID_PUBLICACION,ID_PUBLICACION_POSICION);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }



    @Path("/rechazarTodas")
    @POST
    @Timed
    @PermitAll()
    public Response rechazarTodas(itemDTO item){

        Response response;
        Long getResponseGeneric = jsonConverterBusiness.rechazarTodos(item.getlistStore());
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;

    }

    @Path("/aceptarTodos")
    @POST
    @Timed
    @PermitAll()
    public Response aceptarTodos(itemDTO item){

        Response response;
        Long getResponseGeneric = jsonConverterBusiness.aceptarTodos(item.getlistStore());
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;

    }



    @Path("/getZip")
    @POST
    @Timed
    @PermitAll()
    public Response getZip(){

        Response response;
        Either<IException, String> getResponseGeneric = jsonConverterBusiness.getZip();

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/getZip/list")
    @POST
    @Timed
    @PermitAll()
    public Response getZipList(@QueryParam("files") String files){

        Response response;
        Either<IException, String> getResponseGeneric = jsonConverterBusiness.getZipList(files);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/getLikes")
    @GET
    @Timed
    @PermitAll()
    public Response getLikes(@QueryParam("id_pub") Long id_pub){

        Response response;
        Long getResponseGeneric = jsonConverterBusiness.likes(id_pub);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;
    }


    @Path("/findLikes")
    @GET
    @Timed
    @PermitAll()
    public Response findLikes(@QueryParam("id_pub") Long id_pub,@QueryParam("usern") String usern){

        Response response;
        Long getResponseGeneric = jsonConverterBusiness.findLike(id_pub,usern);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();

        return response;
    }


    @Path("/crear_like")
    @POST
    @Timed
    @PermitAll()
    public Response crear_like(@QueryParam("id_pub") Long id_pub,@QueryParam("usern") String usern) {

        Response response;
        Long getResponseGeneric;
        getResponseGeneric = jsonConverterBusiness.crear_like(id_pub,usern);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }


    @Path("/eliminar_like")
    @POST
    @Timed
    @PermitAll()
    public Response eliminar_like(@QueryParam("id_pub") Long id_pub,@QueryParam("usern") String usern) {

        Response response;
        Long getResponseGeneric;
        getResponseGeneric = jsonConverterBusiness.eliminar_like(id_pub,usern);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }


    @Path("/crear_notificaciones")
    @POST
    @Timed
    @PermitAll()
    public Response crear_notificaciones(@QueryParam("TIPONOT") Long TIPONOT,
                                         @QueryParam("IDPUB") Long IDPUB,
                                         @QueryParam("ESTADONOT") Long ESTADONOT,
                                         @QueryParam("IDUSERPROP") String IDUSERPROP,
                                         @QueryParam("IDUSERNOT") String IDUSERNOT,
                                         @QueryParam("PROP") String PROP,
                                         @QueryParam("NOTIFIER") String NOTIFIER) {

        Response response;
        Long getResponseGeneric;
        getResponseGeneric = jsonConverterBusiness.crear_notificacion(TIPONOT,IDPUB,ESTADONOT,IDUSERPROP,IDUSERNOT,PROP,NOTIFIER);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }



    @Path("/update_notificaciones")
    @POST
    @Timed
    @PermitAll()
    public Response update_notificaciones(@QueryParam("id_usuario_propietario") String id_usuario_propietario) {

        Response response;
        Long getResponseGeneric;
        getResponseGeneric = jsonConverterBusiness.updateNotificacion(id_usuario_propietario);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }


    @Path("/getNotificacionData")
    @POST
    @Timed
    @PermitAll()
    public Response getNotificacionData(@QueryParam("id_usuario_propietario") String id_usuario_propietario){

        Response response;
        Either<IException, List<NotificacionData>> getResponseGeneric = jsonConverterBusiness.getNotificacionData(id_usuario_propietario);

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/crearIslaContent")
    @POST
    @Timed
    @PermitAll()
    public Response crearIslaContent(@QueryParam("ID_ISLA") Long ID_ISLA,
                                     @QueryParam("CONTENIDO") String CONTENIDO,
                                     @QueryParam("ORDEN") Long ORDEN,
                                     @QueryParam("MOSTRAR") Long MOSTRAR) {

        Response response;
        Long getResponseGeneric;
        getResponseGeneric = jsonConverterBusiness.crearIslaContent(  ID_ISLA,
                CONTENIDO,
                ORDEN,
                MOSTRAR);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }


    @Path("/updateIslaContent")
    @POST
    @Timed
    @PermitAll()
    public Response updateIslaContent(@QueryParam("ID_ISLA") Long ID_ISLA,
                                     @QueryParam("CONTENIDO") String CONTENIDO,
                                     @QueryParam("ORDEN") Long ORDEN,
                                     @QueryParam("MOSTRAR") Long MOSTRAR) {

        Response response;
        Long getResponseGeneric;
        getResponseGeneric = jsonConverterBusiness.updateIslaContent(  ID_ISLA,
                CONTENIDO,
                ORDEN,
                MOSTRAR);
        response=Response.status(Response.Status.OK).entity(getResponseGeneric).build();
        return response;
    }


    @Path("/getIslaData")
    @POST
    @Timed
    @PermitAll()
    public Response getIslaData(){

        Response response;
        Either<IException, List<Islas>> getResponseGeneric = jsonConverterBusiness.getIslaData();

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }



    @Path("/getIslaData/generic")
    @POST
    @Timed
    @PermitAll()
    public Response getIslaDatageneric(){

        Response response;
        Either<IException, List<IslaData>> getResponseGeneric = jsonConverterBusiness.getIslaDataGeneric();

        if (getResponseGeneric.isRight()){
            response=Response.status(Response.Status.OK).entity(getResponseGeneric.right().value()).build();
        }else {
            response= ExceptionResponse.createErrorResponse(getResponseGeneric);
        }
        return response;
    }


    @Path("/imagenIsla")
    @POST
    @Timed
    @PermitAll()
    public Response postImagen(Base64FileDTO dto){

        Response response;

        try{
            Either<IException, Long>  imagePosted = jsonConverterBusiness.postImageIsla(dto);
            response=Response.status(Response.Status.OK).entity(1).build();
        }catch(Exception e){
            e.printStackTrace();
            response=Response.status(Response.Status.OK).entity(0).build();
        }

        return response;

    }
}