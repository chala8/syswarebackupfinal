package com.name.business.mappers;


import com.name.business.entities.categoria;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class categoriasMapper implements ResultSetMapper<categoria> {

    @Override
    public categoria map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new categoria(
                resultSet.getLong("ID_CATEGORIA"),
                resultSet.getString("CATEGORIA")
        );
    }
}
