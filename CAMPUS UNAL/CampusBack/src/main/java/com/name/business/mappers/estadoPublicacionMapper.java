package com.name.business.mappers;


import com.name.business.entities.estadoPublicacion;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class estadoPublicacionMapper implements ResultSetMapper<estadoPublicacion> {

    @Override
    public estadoPublicacion map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new estadoPublicacion(
                resultSet.getLong("ID_ESTADO_PUBLICACION"),
                resultSet.getString("ESTADO_PUBLICACION")
        );
    }
}
