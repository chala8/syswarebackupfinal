package com.name.business.entities;

public class IslaData {
    private Long ID_ISLA;
    private String CONTENIDO;
    private Long ORDEN;
    private Long MOSTRAR;

    public IslaData(Long ID_ISLA, String CONTENIDO, Long ORDEN, Long MOSTRAR) {
        this.ID_ISLA = ID_ISLA;
        this.CONTENIDO = CONTENIDO;
        this.ORDEN = ORDEN;
        this.MOSTRAR = MOSTRAR;
    }

    public Long getID_ISLA() {
        return ID_ISLA;
    }

    public void setID_ISLA(Long ID_ISLA) {
        this.ID_ISLA = ID_ISLA;
    }

    public String getCONTENIDO() {
        return CONTENIDO;
    }

    public void setCONTENIDO(String CONTENIDO) {
        this.CONTENIDO = CONTENIDO;
    }

    public Long getORDEN() {
        return ORDEN;
    }

    public void setORDEN(Long ORDEN) {
        this.ORDEN = ORDEN;
    }

    public Long getMOSTRAR() {
        return MOSTRAR;
    }

    public void setMOSTRAR(Long MOSTRAR) {
        this.MOSTRAR = MOSTRAR;
    }
}
