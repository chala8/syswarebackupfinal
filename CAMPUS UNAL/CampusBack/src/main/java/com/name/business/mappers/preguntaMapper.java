package com.name.business.mappers;


import com.name.business.entities.pregunta;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class preguntaMapper implements ResultSetMapper<pregunta> {

    @Override
    public pregunta map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new pregunta(
                resultSet.getLong("ID_PREGUNTA"),
                resultSet.getString("PREGUNTA"),
                resultSet.getLong("ID_CATEGORIA"),
                resultSet.getString("CATEGORIA"),
                resultSet.getLong("NUMPUBLICACIONES")
        );
    }
}
