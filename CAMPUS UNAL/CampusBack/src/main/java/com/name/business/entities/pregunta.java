package com.name.business.entities;

public class pregunta {

    private Long id_pregunta;
    private String pregunta;
    private Long id_categoria;
    private String categoria;
    private Long numPublicaciones;
    private Boolean check;
    public pregunta(Long id_pregunta, String pregunta, Long id_categoria, String categoria,Long numPublicaciones) {
        this.id_pregunta = id_pregunta;
        this.pregunta = pregunta;
        this.id_categoria = id_categoria;
        this.categoria = categoria;
        this.numPublicaciones = numPublicaciones;
        this.check = false;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public Long getNumPublicaciones() {
        return numPublicaciones;
    }

    public void setNumPublicaciones(Long numPublicaciones) {
        this.numPublicaciones = numPublicaciones;
    }

    public Long getId_pregunta() {
        return id_pregunta;
    }

    public void setId_pregunta(Long id_pregunta) {
        this.id_pregunta = id_pregunta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public Long getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(Long id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
