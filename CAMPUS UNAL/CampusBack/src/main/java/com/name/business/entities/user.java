package com.name.business.entities;

public class user {


    private String USERNAME;
    private String NOMBRE;
    private Boolean check;

    public user(String USERNAME, String NOMBRE) {
        this.USERNAME = USERNAME;
        this.NOMBRE = NOMBRE;
        this.check = false;
    }


    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public void setUSERNAME(String USERNAME) {
        this.USERNAME = USERNAME;
    }

    public String getNOMBRE() {
        return NOMBRE;
    }

    public void setNOMBRE(String NOMBRE) {
        this.NOMBRE = NOMBRE;
    }


}
