package com.name.business.entities;

public class NotificacionData {

    private Long ID_NOTIFICACION;
    private Long TIPO_NOTIFICACION;
    private Long ID_PUBLICACION;
    private Long ESTADO_NOTIFICACION;
    private String ID_USUARIO_PROPIETARIO;
    private String ID_USUARIO_NOTIFICADOR;
    private String PROPIETARIO;
    private String NOTIFICADOR;


    public NotificacionData(Long ID_NOTIFICACION, Long TIPO_NOTIFICACION, Long ID_PUBLICACION, Long ESTADO_NOTIFICACION, String ID_USUARIO_PROPIETARIO, String ID_USUARIO_NOTIFICADOR, String PROPIETARIO, String NOTIFICADOR) {
        this.ID_NOTIFICACION = ID_NOTIFICACION;
        this.TIPO_NOTIFICACION = TIPO_NOTIFICACION;
        this.ID_PUBLICACION = ID_PUBLICACION;
        this.ESTADO_NOTIFICACION = ESTADO_NOTIFICACION;
        this.ID_USUARIO_PROPIETARIO = ID_USUARIO_PROPIETARIO;
        this.ID_USUARIO_NOTIFICADOR = ID_USUARIO_NOTIFICADOR;
        this.PROPIETARIO = PROPIETARIO;
        this.NOTIFICADOR = NOTIFICADOR;
    }

    public Long getID_NOTIFICACION() {
        return ID_NOTIFICACION;
    }

    public void setID_NOTIFICACION(Long ID_NOTIFICACION) {
        this.ID_NOTIFICACION = ID_NOTIFICACION;
    }

    public Long getTIPO_NOTIFICACION() {
        return TIPO_NOTIFICACION;
    }

    public void setTIPO_NOTIFICACION(Long TIPO_NOTIFICACION) {
        this.TIPO_NOTIFICACION = TIPO_NOTIFICACION;
    }

    public Long getID_PUBLICACION() {
        return ID_PUBLICACION;
    }

    public void setID_PUBLICACION(Long ID_PUBLICACION) {
        this.ID_PUBLICACION = ID_PUBLICACION;
    }

    public Long getESTADO_NOTIFICACION() {
        return ESTADO_NOTIFICACION;
    }

    public void setESTADO_NOTIFICACION(Long ESTADO_NOTIFICACION) {
        this.ESTADO_NOTIFICACION = ESTADO_NOTIFICACION;
    }

    public String getID_USUARIO_PROPIETARIO() {
        return ID_USUARIO_PROPIETARIO;
    }

    public void setID_USUARIO_PROPIETARIO(String ID_USUARIO_PROPIETARIO) {
        this.ID_USUARIO_PROPIETARIO = ID_USUARIO_PROPIETARIO;
    }

    public String getID_USUARIO_NOTIFICADOR() {
        return ID_USUARIO_NOTIFICADOR;
    }

    public void setID_USUARIO_NOTIFICADOR(String ID_USUARIO_NOTIFICADOR) {
        this.ID_USUARIO_NOTIFICADOR = ID_USUARIO_NOTIFICADOR;
    }

    public String getPROPIETARIO() {
        return PROPIETARIO;
    }

    public void setPROPIETARIO(String PROPIETARIO) {
        this.PROPIETARIO = PROPIETARIO;
    }

    public String getNOTIFICADOR() {
        return NOTIFICADOR;
    }

    public void setNOTIFICADOR(String NOTIFICADOR) {
        this.NOTIFICADOR = NOTIFICADOR;
    }
}
