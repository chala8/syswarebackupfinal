package com.name.business.entities;

public class publicacionPosicion {

    private Long ID_PUBLICACION_POSICION;
    private Double MAPAX;
    private Double MAPAY;
    private Long ID_PUBLICACION;
    private Long ID_PREGUNTA;

    public publicacionPosicion(Long ID_PUBLICACION_POSICION, Double MAPAX, Double MAPAY, Long ID_PUBLICACION,Long ID_PREGUNTA) {
        this.ID_PUBLICACION_POSICION = ID_PUBLICACION_POSICION;
        this.MAPAX = MAPAX;
        this.MAPAY = MAPAY;
        this.ID_PUBLICACION = ID_PUBLICACION;
        this.ID_PREGUNTA =  ID_PREGUNTA;
    }

    public Long getID_PREGUNTA() {
        return ID_PREGUNTA;
    }

    public void setID_PREGUNTA(Long ID_PREGUNTA) {
        this.ID_PREGUNTA = ID_PREGUNTA;
    }

    public Long getID_PUBLICACION_POSICION() {
        return ID_PUBLICACION_POSICION;
    }

    public void setID_PUBLICACION_POSICION(Long ID_PUBLICACION_POSICION) {
        this.ID_PUBLICACION_POSICION = ID_PUBLICACION_POSICION;
    }

    public Double getMAPAX() {
        return MAPAX;
    }

    public void setMAPAX(Double MAPAX) {
        this.MAPAX = MAPAX;
    }

    public Double getMAPAY() {
        return MAPAY;
    }

    public void setMAPAY(Double MAPAY) {
        this.MAPAY = MAPAY;
    }

    public Long getID_PUBLICACION() {
        return ID_PUBLICACION;
    }

    public void setID_PUBLICACION(Long ID_PUBLICACION) {
        this.ID_PUBLICACION = ID_PUBLICACION;
    }
}
