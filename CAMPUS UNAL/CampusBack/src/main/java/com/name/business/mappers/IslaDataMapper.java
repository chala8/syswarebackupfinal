package com.name.business.mappers;


import com.name.business.entities.IslaData;
import com.name.business.entities.categoria;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class IslaDataMapper implements ResultSetMapper<IslaData> {

    @Override
    public IslaData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new IslaData(
                resultSet.getLong("ID_ISLA"),
                resultSet.getString("CONTENIDO"),
                resultSet.getLong("ORDEN"),
                resultSet.getLong("MOSTRAR")
        );
    }
}
