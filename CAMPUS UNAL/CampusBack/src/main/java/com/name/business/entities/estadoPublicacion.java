package com.name.business.entities;

public class estadoPublicacion {


    private Long ID_ESTADO_PUBLICACION;
    private String ESTADO_PUBLICACION;

    public estadoPublicacion(Long ID_ESTADO_PUBLICACION, String ESTADO_PUBLICACION) {
        this.ID_ESTADO_PUBLICACION = ID_ESTADO_PUBLICACION;
        this.ESTADO_PUBLICACION = ESTADO_PUBLICACION;
    }

    public Long getID_ESTADO_PUBLICACION() {
        return ID_ESTADO_PUBLICACION;
    }

    public void setID_ESTADO_PUBLICACION(Long ID_ESTADO_PUBLICACION) {
        this.ID_ESTADO_PUBLICACION = ID_ESTADO_PUBLICACION;
    }

    public String getESTADO_PUBLICACION() {
        return ESTADO_PUBLICACION;
    }

    public void setESTADO_PUBLICACION(String ESTADO_PUBLICACION) {
        this.ESTADO_PUBLICACION = ESTADO_PUBLICACION;
    }
}
