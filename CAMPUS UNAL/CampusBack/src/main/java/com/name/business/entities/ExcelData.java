package com.name.business.entities;

public class ExcelData {
    private Long ID_PUBLICACION;
    private String PREGUNTA;
    private Long ID_PUBLICACION_PADRE;
    private String FECHA_PUBLICACION;
    private String USERNAME;
    private String PUBLICACION;
    private String IMG_URL;
    private String ESTADO_PUBICACION;
    private String LATITUD;
    private String LONGITUD;
    private Double DISTANCIA_KMS;
    private Long likes;


    public ExcelData(Long ID_PUBLICACION, String PREGUNTA, Long ID_PUBLICACION_PADRE, String FECHA_PUBLICACION, String USERNAME, String PUBLICACION, String IMG_URL, String ESTADO_PUBICACION, String LATITUD, String LONGITUD, Double DISTANCIA_KMS) {
        this.ID_PUBLICACION = ID_PUBLICACION;
        this.PREGUNTA = PREGUNTA;
        this.ID_PUBLICACION_PADRE = ID_PUBLICACION_PADRE;
        this.FECHA_PUBLICACION = FECHA_PUBLICACION;
        this.USERNAME = USERNAME;
        this.PUBLICACION = PUBLICACION;
        this.IMG_URL = IMG_URL;
        this.ESTADO_PUBICACION = ESTADO_PUBICACION;
        this.LATITUD = LATITUD;
        this.LONGITUD = LONGITUD;
        this.DISTANCIA_KMS = DISTANCIA_KMS;
        this.likes = new Long(0);
    }

    public Long getLikes() { return likes; }

    public void setLikes(Long likes) { this.likes = likes; }

    public Long getID_PUBLICACION() {
        return ID_PUBLICACION;
    }

    public void setID_PUBLICACION(Long ID_PUBLICACION) {
        this.ID_PUBLICACION = ID_PUBLICACION;
    }

    public String getPREGUNTA() {
        return PREGUNTA;
    }

    public void setPREGUNTA(String PREGUNTA) {
        this.PREGUNTA = PREGUNTA;
    }

    public Long getID_PUBLICACION_PADRE() {
        return ID_PUBLICACION_PADRE;
    }

    public void setID_PUBLICACION_PADRE(Long ID_PUBLICACION_PADRE) {
        this.ID_PUBLICACION_PADRE = ID_PUBLICACION_PADRE;
    }

    public String getFECHA_PUBLICACION() {
        return FECHA_PUBLICACION;
    }

    public void setFECHA_PUBLICACION(String FECHA_PUBLICACION) {
        this.FECHA_PUBLICACION = FECHA_PUBLICACION;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public void setUSERNAME(String USERNAME) {
        this.USERNAME = USERNAME;
    }

    public String getPUBLICACION() {
        return PUBLICACION;
    }

    public void setPUBLICACION(String PUBLICACION) {
        this.PUBLICACION = PUBLICACION;
    }

    public String getIMG_URL() {
        return IMG_URL;
    }

    public void setIMG_URL(String IMG_URL) {
        this.IMG_URL = IMG_URL;
    }

    public String getESTADO_PUBICACION() {
        return ESTADO_PUBICACION;
    }

    public void setESTADO_PUBICACION(String ESTADO_PUBICACION) {
        this.ESTADO_PUBICACION = ESTADO_PUBICACION;
    }

    public String getLATITUD() {
        return LATITUD;
    }

    public void setLATITUD(String LATITUD) {
        this.LATITUD = LATITUD;
    }

    public String getLONGITUD() {
        return LONGITUD;
    }

    public void setLONGITUD(String LONGITUD) {
        this.LONGITUD = LONGITUD;
    }

    public Double getDISTANCIA_KMS() {
        return DISTANCIA_KMS;
    }

    public void setDISTANCIA_KMS(Double DISTANCIA_KMS) {
        this.DISTANCIA_KMS = DISTANCIA_KMS;
    }
}
