package com.name.business.mappers;


import com.name.business.entities.categoria;
import com.name.business.entities.user;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class userMapper implements ResultSetMapper<user> {

    @Override
    public user map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new user(
                resultSet.getString("USERNAME"),
                resultSet.getString("NOMBRE")
        );
    }
}
