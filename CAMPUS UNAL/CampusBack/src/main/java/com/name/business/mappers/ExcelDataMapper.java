package com.name.business.mappers;


import com.name.business.entities.ExcelData;
import com.name.business.entities.estadoPublicacion;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ExcelDataMapper implements ResultSetMapper<ExcelData> {

    @Override
    public ExcelData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new ExcelData(
                resultSet.getLong("ID_PUBLICACION"),
                resultSet.getString("PREGUNTA"),
                resultSet.getLong("ID_PUBLICACION_PADRE"),
                resultSet.getString("FECHA_PUBLICACION"),
                resultSet.getString("USERNAME"),
                resultSet.getString("PUBLICACION"),
                resultSet.getString("IMG_URL"),
                resultSet.getString("ESTADO_PUBICACION"),
                resultSet.getString("LATITUD"),
                resultSet.getString("LONGITUD"),
                resultSet.getDouble("DISTANCIA_KMS")
        );
    }
}
