package com.name.business.entities;

public class publicacion {

private Long ID_PUBLICACION;
private Long ID_PREGUNTA;
private Long ID_PUBLICACION_PADRE;
private String FECHA_PUBLICACION;
private String USERNAME;
private String PUBLICACION;
private String IMG_URL;
private Long ID_ESTADO_PUBLICACION;
private Float LATITUD;
private Float LONGITUD;
private Float DISTANCIA_KMS;
private Float MAPAX;
private Float MAPAY;
private Boolean CHECK;
private String pregunta;
private Long likes;
private String USUARIO;

    public publicacion(Long ID_PUBLICACION, Long ID_PREGUNTA, Long ID_PUBLICACION_PADRE, String FECHA_PUBLICACION, String USERNAME, String PUBLICACION, String IMG_URL, Long ID_ESTADO_PUBLICACION, Float LATITUD, Float LONGITUD, Float DISTANCIA_KMS, Float MAPAX,Float MAPAY,String pregunta,String USUARIO) {
        this.pregunta = pregunta;
        this.MAPAX = MAPAX;
        this.MAPAY = MAPAY;
        this.ID_PUBLICACION = ID_PUBLICACION;
        this.ID_PREGUNTA = ID_PREGUNTA;
        this.ID_PUBLICACION_PADRE = ID_PUBLICACION_PADRE;
        this.FECHA_PUBLICACION = FECHA_PUBLICACION;
        this.USERNAME = USERNAME;
        this.PUBLICACION = PUBLICACION;
        this.IMG_URL = IMG_URL;
        this.ID_ESTADO_PUBLICACION = ID_ESTADO_PUBLICACION;
        this.LATITUD = LATITUD;
        this.LONGITUD = LONGITUD;
        this.DISTANCIA_KMS = DISTANCIA_KMS;
        this.CHECK = false;
        this.likes = new Long(0);
        this.USUARIO = USUARIO;
    }

    public String getUSUARIO() { return USUARIO; }

    public void setUSUARIO(String USUARIO) { this.USUARIO = USUARIO; }

    public Long getLikes() { return likes; }

    public void setLikes(Long likes) { this.likes = likes; }

    public String getPregunta() { return pregunta; }

    public void setPregunta(String pregunta) { this.pregunta = pregunta; }

    public Boolean getCHECK() {
        return CHECK;
    }

    public void setCHECK(Boolean CHECK) {
        this.CHECK = CHECK;
    }

    public Float getMAPAX() {
        return MAPAX;
    }

    public void setMAPAX(Float MAPAX) {
        this.MAPAX = MAPAX;
    }

    public Float getMAPAY() {
        return MAPAY;
    }

    public void setMAPAY(Float MAPAY) {
        this.MAPAY = MAPAY;
    }

    public Long getID_PUBLICACION() {
        return ID_PUBLICACION;
    }

    public void setID_PUBLICACION(Long ID_PUBLICACION) {
        this.ID_PUBLICACION = ID_PUBLICACION;
    }

    public Long getID_PREGUNTA() {
        return ID_PREGUNTA;
    }

    public void setID_PREGUNTA(Long ID_PREGUNTA) {
        this.ID_PREGUNTA = ID_PREGUNTA;
    }

    public Long getID_PUBLICACION_PADRE() {
        return ID_PUBLICACION_PADRE;
    }

    public void setID_PUBLICACION_PADRE(Long ID_PUBLICACION_PADRE) {
        this.ID_PUBLICACION_PADRE = ID_PUBLICACION_PADRE;
    }

    public String getFECHA_PUBLICACION() {
        return FECHA_PUBLICACION;
    }

    public void setFECHA_PUBLICACION(String FECHA_PUBLICACION) {
        this.FECHA_PUBLICACION = FECHA_PUBLICACION;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public void setUSERNAME(String USERNAME) {
        this.USERNAME = USERNAME;
    }

    public String getPUBLICACION() {
        return PUBLICACION;
    }

    public void setPUBLICACION(String PUBLICACION) {
        this.PUBLICACION = PUBLICACION;
    }

    public String getIMG_URL() {
        return IMG_URL;
    }

    public void setIMG_URL(String IMG_URL) {
        this.IMG_URL = IMG_URL;
    }

    public Long getID_ESTADO_PUBLICACION() {
        return ID_ESTADO_PUBLICACION;
    }

    public void setID_ESTADO_PUBLICACION(Long ID_ESTADO_PUBLICACION) {
        this.ID_ESTADO_PUBLICACION = ID_ESTADO_PUBLICACION;
    }

    public Float getLATITUD() {
        return LATITUD;
    }

    public void setLATITUD(Float LATITUD) {
        this.LATITUD = LATITUD;
    }

    public Float getLONGITUD() {
        return LONGITUD;
    }

    public void setLONGITUD(Float LONGITUD) {
        this.LONGITUD = LONGITUD;
    }

    public Float getDISTANCIA_KMS() {
        return DISTANCIA_KMS;
    }

    public void setDISTANCIA_KMS(Float DISTANCIA_KMS) {
        this.DISTANCIA_KMS = DISTANCIA_KMS;
    }
}
