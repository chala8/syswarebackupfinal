package com.name.business.entities;

public class publicacionData {

    private Long id_publicacion;
    private String username;
    private String publicacion;
    private String mapax;
    private String mapay;

    public publicacionData(Long id_publicacion, String username, String publicacion, String mapax, String mapay) {
        this.id_publicacion = id_publicacion;
        this.username = username;
        this.publicacion = publicacion;
        this.mapax = mapax;
        this.mapay = mapay;
    }

    public Long getId_publicacion() {
        return id_publicacion;
    }

    public void setId_publicacion(Long id_publicacion) {
        this.id_publicacion = id_publicacion;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(String publicacion) {
        this.publicacion = publicacion;
    }

    public String getMapax() {
        return mapax;
    }

    public void setMapax(String mapax) {
        this.mapax = mapax;
    }

    public String getMapay() {
        return mapay;
    }

    public void setMapay(String mapay) {
        this.mapay = mapay;
    }
}
