package com.name.business.entities;

import java.util.List;

public class Islas {
    private String titulo;
    private String url;
    private String descripcion;
    private String imagenes;
    private String subtitulo1;
    private String info1;
    private String info2;
    private String info3;
    private String info4;
    private String info5;
    private String info6;
    private String subtitulo2;
    private String redes;
    private String contactenos;
    private String correos;
    private String URLSitioWeb;
    private List<Boolean> mostrar;
    private String tituloProgramacion;
    private String programacion1;
    private String programacion2;
    private String imagen1;
    private String imagen2;
    private String imagen3;
    private String imagen4;
    private String imagen5;
    private String imagen6;
    private String facebook;
    private String instagram;
    private String youtube;
    private String twitter;
    private String background;


    public Islas(String titulo, String url, String descripcion, String imagenes, String subtitulo1, String info1, String info2, String info3, String info4, String info5, String info6, String subtitulo2, String redes, String contactenos, String correos, String URLSitioWeb, String tituloProgramacion, String programacion1, String programacion2, String imagen1, String imagen2, String imagen3, String imagen4, String imagen5, String imagen6, String facebook, String instagram, String youtube, String twitter, String background) {
        this.titulo = titulo;
        this.url = url;
        this.descripcion = descripcion;
        this.imagenes = imagenes;
        this.subtitulo1 = subtitulo1;
        this.info1 = info1;
        this.info2 = info2;
        this.info3 = info3;
        this.info4 = info4;
        this.info5 = info5;
        this.info6 = info6;
        this.subtitulo2 = subtitulo2;
        this.redes = redes;
        this.contactenos = contactenos;
        this.correos = correos;
        this.URLSitioWeb = URLSitioWeb;
        this.tituloProgramacion = tituloProgramacion;
        this.programacion1 = programacion1;
        this.programacion2 = programacion2;
        this.imagen1 = imagen1;
        this.imagen2 = imagen2;
        this.imagen3 = imagen3;
        this.imagen4 = imagen4;
        this.imagen5 = imagen5;
        this.imagen6 = imagen6;
        this.facebook = facebook;
        this.instagram = instagram;
        this.youtube = youtube;
        this.twitter = twitter;
        this.background = background;
    }

    public String getBackground() { return background; }

    public void setBackground(String background) { this.background = background; }

    public String getImagen1() {
        return imagen1;
    }

    public void setImagen1(String imagen1) {
        this.imagen1 = imagen1;
    }

    public String getImagen2() {
        return imagen2;
    }

    public void setImagen2(String imagen2) {
        this.imagen2 = imagen2;
    }

    public String getImagen3() {
        return imagen3;
    }

    public void setImagen3(String imagen3) {
        this.imagen3 = imagen3;
    }

    public String getImagen4() {
        return imagen4;
    }

    public void setImagen4(String imagen4) {
        this.imagen4 = imagen4;
    }

    public String getImagen5() {
        return imagen5;
    }

    public void setImagen5(String imagen5) {
        this.imagen5 = imagen5;
    }

    public String getImagen6() {
        return imagen6;
    }

    public void setImagen6(String imagen6) {
        this.imagen6 = imagen6;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getTituloProgramacion() {
        return tituloProgramacion;
    }

    public void setTituloProgramacion(String tituloProgramacion) {
        this.tituloProgramacion = tituloProgramacion;
    }

    public String getProgramacion1() {
        return programacion1;
    }

    public void setProgramacion1(String programacion1) {
        this.programacion1 = programacion1;
    }

    public String getProgramacion2() {
        return programacion2;
    }

    public void setProgramacion2(String programacion2) {
        this.programacion2 = programacion2;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagenes() {
        return imagenes;
    }

    public void setImagenes(String imagenes) {
        this.imagenes = imagenes;
    }

    public String getSubtitulo1() {
        return subtitulo1;
    }

    public void setSubtitulo1(String subtitulo1) {
        this.subtitulo1 = subtitulo1;
    }

    public String getInfo1() {
        return info1;
    }

    public void setInfo1(String info1) {
        this.info1 = info1;
    }

    public String getInfo2() {
        return info2;
    }

    public void setInfo2(String info2) {
        this.info2 = info2;
    }

    public String getInfo3() {
        return info3;
    }

    public void setInfo3(String info3) {
        this.info3 = info3;
    }

    public String getInfo4() {
        return info4;
    }

    public void setInfo4(String info4) {
        this.info4 = info4;
    }

    public String getInfo5() {
        return info5;
    }

    public void setInfo5(String info5) {
        this.info5 = info5;
    }

    public String getInfo6() {
        return info6;
    }

    public void setInfo6(String info6) {
        this.info6 = info6;
    }

    public String getSubtitulo2() {
        return subtitulo2;
    }

    public void setSubtitulo2(String subtitulo2) {
        this.subtitulo2 = subtitulo2;
    }

    public String getRedes() {
        return redes;
    }

    public void setRedes(String redes) {
        this.redes = redes;
    }

    public String getContactenos() {
        return contactenos;
    }

    public void setContactenos(String contactenos) {
        this.contactenos = contactenos;
    }

    public String getCorreos() {
        return correos;
    }

    public void setCorreos(String correos) {
        this.correos = correos;
    }

    public String getURLSitioWeb() {
        return URLSitioWeb;
    }

    public void setURLSitioWeb(String URLSitioWeb) {
        this.URLSitioWeb = URLSitioWeb;
    }

    public List<Boolean> getMostrar() { return mostrar; }

    public void setMostrar(List<Boolean> mostrar) { this.mostrar = mostrar; }
}
