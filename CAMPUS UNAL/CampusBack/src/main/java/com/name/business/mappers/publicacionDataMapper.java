package com.name.business.mappers;


import com.name.business.entities.publicacion;
import com.name.business.entities.publicacionData;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class publicacionDataMapper implements ResultSetMapper<publicacionData> {

    @Override
    public publicacionData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new publicacionData(
                resultSet.getLong("ID_PUBLICACION"),
                resultSet.getString("USERNAME"),
                resultSet.getString("PUBLICACION"),
                resultSet.getString("MAPAX"),
                resultSet.getString("MAPAY")
        );
    }
}
