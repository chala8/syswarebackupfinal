package com.name.business.mappers;


import com.name.business.entities.publicacion;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class publicacionesMapper implements ResultSetMapper<publicacion> {

    @Override
    public publicacion map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new publicacion(
                resultSet.getLong("ID_PUBLICACION"),
                resultSet.getLong("ID_PREGUNTA"),
                resultSet.getLong("ID_PUBLICACION_PADRE"),
                resultSet.getString("FECHA_PUBLICACION"),
                resultSet.getString("USERNAME"),
                resultSet.getString("PUBLICACION"),
                resultSet.getString("IMG_URL"),
                resultSet.getLong("ID_ESTADO_PUBLICACION"),
                resultSet.getFloat("LATITUD"),
                resultSet.getFloat("LONGITUD"),
                resultSet.getFloat("DISTANCIA_KMS"),
                resultSet.getFloat("MAPAX"),
                resultSet.getFloat("MAPAY"),
                resultSet.getString("pregunta"),
                resultSet.getString("nombre_usuario")
        );
    }
}
