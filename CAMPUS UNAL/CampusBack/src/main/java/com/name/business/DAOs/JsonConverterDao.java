package com.name.business.DAOs;
import com.name.business.entities.*;
import com.name.business.mappers.*;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;
import org.skife.jdbi.v2.unstable.BindIn;

import java.util.Date;
import java.util.List;

//import com.name.business.entities.*;
//import com.name.business.mappers.*;

//@RegisterMapper(CloseMapper.class)
@UseStringTemplate3StatementLocator
public interface JsonConverterDao {


    @SqlQuery(" select count(*) from usuarios_admin where username=:username ")
    Long getAdmin(@Bind("username") String username);

    @Mapper(categoriasMapper.class)
    @SqlQuery(" select id_categoria,categoria from categoria order by id_categoria desc")
    List<categoria> getCategorias();

    @Mapper(preguntaMapper.class)
    @SqlQuery(" select p.id_pregunta,pregunta,p.id_categoria,categoria,(select count(*) from publicacion where id_pregunta=p.id_pregunta) numpublicaciones\n" +
            "            from pregunta p, categoria c\n" +
            "            where p.id_categoria=c.id_categoria\n" +
            "            order by p.id_pregunta desc ")
    List<pregunta> getPreguntas();

    @SqlCall(" begin crear_pregunta(:pregunta,:id_categoria); end; ")
    void crear_pregunta(@Bind("pregunta") String pregunta,
                        @Bind("id_categoria") Long id_categoria);

    @SqlCall(" begin eliminar_pregunta(:id_pregunta); end; ")
    void eliminar_pregunta(@Bind("id_pregunta") Long id_pregunta);

    @SqlCall(" begin modificar_pregunta(:id_pregunta, :pregunta, :id_categoria); end; ")
    void modificar_pregunta(@Bind("pregunta") String pregunta,
                            @Bind("id_categoria") Long id_categoria,
                            @Bind("id_pregunta") Long id_pregunta);

    @SqlCall(" begin crear_categoria(:categoria); end; ")
    void crear_categoria(@Bind("categoria") String categoria);

    @SqlCall("begin getAuthLDAP(:username, :password); end;")
    void getAuthLDAP(@Bind("username") String username,
                     @Bind("password") String password);




    @SqlQuery(" select calculadistanciagps(:lat, :longit,4.6382,-74.0840) from dual ")
    Float calculadistanciagps(@Bind("lat") Float lat,
                              @Bind("longit") Float longit);


    @Mapper(publicacionesMapper.class)
    @SqlQuery("select ID_PUBLICACION,p.ID_PREGUNTA,ID_PUBLICACION_PADRE,FECHA_PUBLICACION,USERNAME,PUBLICACION, " +
            "IMG_URL,ID_ESTADO_PUBLICACION,LATITUD,LONGITUD,DISTANCIA_KMS,MAPAX,MAPAY,pr.pregunta,nombre_usuario  " +
            "from publicacion p,pregunta pr " +
            "where p.id_pregunta=pr.id_pregunta and " +
            "id_estado_publicacion in (<idList>) " +
            "order by FECHA_PUBLICACION desc")
    List<publicacion> getPublicaciones(@BindIn("idList") List<Long> idList);


    @Mapper(publicacionesMapper.class)
    @SqlQuery("select ID_PUBLICACION,p.ID_PREGUNTA,ID_PUBLICACION_PADRE,FECHA_PUBLICACION,USERNAME,PUBLICACION, " +
            "IMG_URL,ID_ESTADO_PUBLICACION,LATITUD,LONGITUD,DISTANCIA_KMS,MAPAX,MAPAY,pr.pregunta,nombre_usuario  " +
            "from publicacion p,pregunta pr " +
            "where p.id_pregunta=pr.id_pregunta and " +
            "ID_PUBLICACION = :idList " +
            "order by FECHA_PUBLICACION desc")
    publicacion getPublicacionesById(@Bind("idList") Long idList);


    @Mapper(publicacionesMapper.class)
    @SqlQuery(" select ID_PUBLICACION,p.ID_PREGUNTA,ID_PUBLICACION_PADRE,FECHA_PUBLICACION,USERNAME,PUBLICACION,IMG_URL," +
            "ID_ESTADO_PUBLICACION,LATITUD,LONGITUD,DISTANCIA_KMS,MAPAX,MAPAY,pr.pregunta,nombre_usuario from publicacion p, pregunta pr where p.id_pregunta=pr.id_pregunta and id_estado_publicacion in (<idList>) and p.ID_PREGUNTA in (<pregList>) order by FECHA_PUBLICACION desc ")
    List<publicacion> getPublicacionesPregunta(@BindIn("idList") List<Long> idList,
                                               @BindIn("pregList") List<Long> pregList);


    @Mapper(publicacionesMapper.class)
    @SqlQuery(" select ID_PUBLICACION,p.ID_PREGUNTA,ID_PUBLICACION_PADRE,FECHA_PUBLICACION,USERNAME,PUBLICACION,IMG_URL," +
            "ID_ESTADO_PUBLICACION,LATITUD,LONGITUD,DISTANCIA_KMS,MAPAX,MAPAY,pr.pregunta,nombre_usuario from publicacion p,pregunta pr where p.id_pregunta=pr.id_pregunta and id_estado_publicacion in (<idList>) and id_publicacion_padre is null ")
    List<publicacion> getPublicacionesGalery(@BindIn("idList") List<Long> idList);


    @Mapper(publicacionesMapper.class)
    @SqlQuery(" select ID_PUBLICACION,p.ID_PREGUNTA,ID_PUBLICACION_PADRE,FECHA_PUBLICACION,USERNAME,PUBLICACION,IMG_URL," +
            "ID_ESTADO_PUBLICACION,LATITUD,LONGITUD,DISTANCIA_KMS,MAPAX,MAPAY,pr.pregunta,nombre_usuario from publicacion p,pregunta pr where p.id_pregunta=pr.id_pregunta and id_publicacion_padre=:idpubpadre and ID_ESTADO_PUBLICACION=2 order by fecha_publicacion desc")
    List<publicacion> getPublicacionesHijo(@Bind("idpubpadre") Long idpubpadre);



    @Mapper(estadoPublicacionMapper.class)
    @SqlQuery(" select ID_ESTADO_PUBLICACION,ESTADO_PUBICACION from ESTADO_PUBLICACION")
    List<estadoPublicacion> getEstadosPublicacion();

    @SqlCall(" begin CREAR_PUBLICACION(:IDPREGUNTA," +
            ":IDPUBLICACIONPADRE," +
            ":FECHAPUBLICACION," +
            ":USERNAME," +
            ":PUBLICACION," +
            ":IMGURL," +
            ":IDESTADOPUBLICACION," +
            ":LATITUD," +
            ":LONGITUD," +
            ":KMS, :X, :Y, :USER); end; ")
    void postPublicacion(@Bind("IDPREGUNTA") Long IDPREGUNTA,
                         @Bind("IDPUBLICACIONPADRE") Long IDPUBLICACIONPADRE,
                         @Bind("FECHAPUBLICACION") Date FECHAPUBLICACION,
                         @Bind("USERNAME") String USERNAME,
                         @Bind("PUBLICACION") String PUBLICACION,
                         @Bind("IMGURL") String IMGURL,
                         @Bind("IDESTADOPUBLICACION") Long IDESTADOPUBLICACION,
                         @Bind("LATITUD") Float LATITUD,
                         @Bind("LONGITUD") Float LONGITUD,
                         @Bind("KMS") Float KMS,
                         @Bind("X") Float X,
                         @Bind("Y") Float Y,
                         @Bind("USER") String USER);



    @SqlCall(" begin ACTUALIZAR_ESTADO_PUBLICACION(:idpublicacion,:idestadopublicacion); end; ")
    void ACTUALIZAR_ESTADO_PUBLICACION(@Bind("idpublicacion") Long idpublicacion,
                                       @Bind("idestadopublicacion") Long idestadopublicacion);


    @SqlCall(" begin eliminar_administrador(:user); end; ")
    void eliminar_administrador(@Bind("user") String user);



    @SqlCall(" begin crear_administrador(:user,:nombre); end; ")
    void crear_administrador(@Bind("user") String user,
                        @Bind("nombre") String nombre);


    @Mapper(userMapper.class)
    @SqlQuery(" select username,nombre from USUARIOS_ADMIN ")
    List<user> getUsers();



    @Mapper(userMapper.class)
    @SqlQuery(" select username,nombre from USUARIOS_ADMIN where username=:username ")
    List<user> getUsersByUsername(@Bind("username") String username);



    @Mapper(publicacionDataMapper.class)
    @SqlQuery(" select id_publicacion,username,publicacion,mapax,mapay from publicacion where publicacion like :phrase ")
    List<publicacionData> getPublicationLike(@Bind("phrase") String phrase);



    @SqlUpdate(" update publicacion set mapax=:x ,mapay=:y where id_publicacion=:ippublicacion")
    void fixMapCoordinates(@Bind("ippublicacion") Long ippublicacion,
                                             @Bind("x") Double x,
                                             @Bind("y") Double y);

    @Mapper(publicacionPosicionMapper.class)
    @SqlQuery(" SELECT ID_PUBLICACION_POSICION,MAPAX,MAPAY,ID_PUBLICACION,ID_PREGUNTA \n" +
            " FROM PUBLICACION_POSICION order by ID_PUBLICACION_POSICION ")
    List<publicacionPosicion> getPublicationPosicion();

    @Mapper(publicacionPosicionMapper.class)
    @SqlQuery(" SELECT ID_PUBLICACION_POSICION,MAPAX,MAPAY,ID_PUBLICACION,ID_PREGUNTA \n" +
            " FROM PUBLICACION_POSICION WHERE ID_PREGUNTA = :ID_PREGUNTA order by ID_PUBLICACION_POSICION ")
    List<publicacionPosicion> getPublicationPosicionIdPregunta(@Bind("ID_PREGUNTA") Long ID_PREGUNTA);


    @Mapper(publicacionPosicionMapper.class)
    @SqlQuery(" SELECT ID_PUBLICACION_POSICION,MAPAX,MAPAY,ID_PUBLICACION,ID_PREGUNTA\n" +
            "    FROM PUBLICACION_POSICION WHERE ID_PUBLICACION_POSICION=:ID_PUBLICACION_POSICION order by ID_PUBLICACION_POSICION ")
    List<publicacionPosicion> getPublicationPosicionById(@Bind("ID_PUBLICACION_POSICION") Long ID_PUBLICACION_POSICION);


    @Mapper(publicacionPosicionMapper.class)
    @SqlQuery(" SELECT ID_PUBLICACION_POSICION,MAPAX,MAPAY,ID_PUBLICACION,ID_PREGUNTA " +
            "FROM PUBLICACION_POSICION WHERE ID_PUBLICACION=:ID_PUBLICACION ")
    List<publicacionPosicion> getPublicationPosicionByPublicacion(@Bind("ID_PUBLICACION") Long ID_PUBLICACION);


    @SqlCall(" begin CREAR_PUBLICACION_POSICION(:X,:Y,:ID_PUB,:ID_PREG); end; ")
    void CREAR_PUBLICACION_POSICION(@Bind("X") Double X,
                                    @Bind("Y") Double Y,
                                    @Bind("ID_PUB") Long ID_PUB,
                                    @Bind("ID_PREG") Long ID_PREG);


    @SqlCall(" begin LIMPIAR_PUBLICACION_POSICION(:ID_PUB); end; ")
    void LIMPIAR_PUBLICACION_POSICION(@Bind("ID_PUB") Long ID_PUB);


    @SqlCall(" begin ACTUALIZAR_PUB_POSICION(:X,:Y,:ID_PUB_POS); end; ")
    void ACTUALIZAR_PUB_POSICION(@Bind("X") Double X,
                                 @Bind("Y") Double Y,
                                 @Bind("ID_PUB_POS") Long ID_PUB_POS);


    @SqlUpdate(" update PUBLICACION_POSICION set ID_PUBLICACION=:ID_PUBLICACION where ID_PUBLICACION_POSICION=:ID_PUBLICACION_POSICION")
    void updatePublicacionOnPosicion(@Bind("ID_PUBLICACION") Long ID_PUBLICACION,
                                     @Bind("ID_PUBLICACION_POSICION") Long ID_PUBLICACION_POSICION);


    @SqlQuery(" SELECT MAX(ID_PUBLICACION) FROM publicacion ")
    Long getLatestPublication();


    @Mapper(ExcelDataMapper.class)
    @SqlQuery(" select ID_PUBLICACION\n" +
            ",PREGUNTA\n" +
            ",ID_PUBLICACION_PADRE\n" +
            ",to_char(FECHA_PUBLICACION,'DD-MM-YYYY HH24:MI:SS') FECHA_PUBLICACION\n" +
            ",USERNAME\n" +
            ",PUBLICACION\n" +
            ",IMG_URL\n" +
            ",E.ESTADO_PUBICACION\n" +
            ",LATITUD\n" +
            ",LONGITUD\n" +
            ",DISTANCIA_KMS\n" +
            "from publicacion P, PREGUNTA PR, ESTADO_PUBLICACION E\n" +
            "WHERE P.ID_PREGUNTA=PR.ID_PREGUNTA AND P.ID_ESTADO_PUBLICACION=E.ID_ESTADO_PUBLICACION ")
    List<ExcelData> getExcelData();


    @SqlCall(" begin crear_like(:id_pub,:usern); end; ")
    void crear_like(@Bind("id_pub") Long id_pub,
                    @Bind("usern") String usern);


    @SqlCall(" begin eliminar_like(:id_pub,:usern); end; ")
    void eliminar_like(@Bind("id_pub") Long id_pub,
                       @Bind("usern") String usern);


    @SqlQuery(" select count(*) from PUBLICACIONES_LIKES where id_publicacion=:idpub ")
    Long likes(@Bind("idpub") Long id_pub);


    @SqlQuery(" select count(*) from PUBLICACIONES_LIKES where id_publicacion=:idpub and username=:usern ")
    Long findLike(@Bind("idpub") Long id_pub,
                  @Bind("usern") String usern);


    @SqlUpdate(" update publicacion set nombre_usuario=:usuario where id_publicacion=:ippublicacion")
    void updateUserPostTable(@Bind("ippublicacion") Long ippublicacion,
                             @Bind("usuario") String user);






    @SqlCall(" begin CREAR_NOTIFICACION(:TIPONOT, :IDPUB, :ESTADONOT, :IDUSERPROP, :IDUSERNOT, :PROP, :NOTIFIER); end; ")
    void crear_notificacion(@Bind("TIPONOT") Long TIPONOT,
                            @Bind("IDPUB") Long IDPUB,
                            @Bind("ESTADONOT") Long ESTADONOT,
                            @Bind("IDUSERPROP") String IDUSERPROP,
                            @Bind("IDUSERNOT") String IDUSERNOT,
                            @Bind("PROP") String PROP,
                            @Bind("NOTIFIER") String NOTIFIER);


    @SqlUpdate(" update notificacion set estado_notificacion=1 where estado_notificacion=0 and id_usuario_propietario=:id_usuario_propietario")
    void updateNotificacion(@Bind("id_usuario_propietario") String id_usuario_propietario);


    @Mapper(NotificacionDataMapper.class)
    @SqlQuery("select * from notificacion where id_usuario_propietario=:id_usuario_propietario order by id_notificacion desc")
    List<NotificacionData> getNotificacionData(@Bind("id_usuario_propietario") String id_usuario_propietario);


    @SqlCall(" begin CREAR_ISLA(:ID_ISLA, :CONTENIDO, :ORDEN, :MOSTRAR ); end; ")
    void crearIslaContent(@Bind("ID_ISLA") Long ID_ISLA,
                          @Bind("CONTENIDO") String CONTENIDO,
                          @Bind("ORDEN") Long ORDEN,
                          @Bind("MOSTRAR") Long MOSTRAR);


    @SqlCall(" begin UPDATE_ISLA(:ID_ISLA, :ORDEN, :CONTENIDO, :MOSTRAR ); end; ")
    void updateIslaContent(@Bind("ID_ISLA") Long ID_ISLA,
                          @Bind("CONTENIDO") String CONTENIDO,
                          @Bind("ORDEN") Long ORDEN,
                          @Bind("MOSTRAR") Long MOSTRAR);


    @Mapper(IslaDataMapper.class)
    @SqlQuery("select * from isla order by id_isla,orden")
    List<IslaData> getIslaData();


}
