package com.name.business.mappers;
import com.name.business.entities.NotificacionData;
import com.name.business.entities.pregunta;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import java.sql.ResultSet;
import java.sql.SQLException;


public class NotificacionDataMapper implements ResultSetMapper<NotificacionData> {

    @Override
    public NotificacionData map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new NotificacionData(
                resultSet.getLong("ID_NOTIFICACION"),
                resultSet.getLong("TIPO_NOTIFICACION"),
                resultSet.getLong("ID_PUBLICACION"),
                resultSet.getLong("ESTADO_NOTIFICACION"),
                resultSet.getString("ID_USUARIO_PROPIETARIO"),
                resultSet.getString("ID_USUARIO_NOTIFICADOR"),
                resultSet.getString("PROPIETARIO"),
                resultSet.getString("NOTIFICADOR")
        );
    }
}
