package com.name.business.businesses;
import com.name.business.DAOs.JsonConverterDao;
import com.name.business.entities.*;
import com.name.business.representations.Base64FileDTO;
import com.name.business.representations.listItemDTO;
import com.name.business.utils.exeptions.IException;
import com.name.business.utils.exeptions.TechnicalException;
import fj.data.Array;
import fj.data.Either;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.DataFormat;
import org.json.JSONException;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.w3c.dom.Document;
import org.apache.commons.codec.binary.Base64;

import javax.imageio.ImageIO;
import javax.naming.*;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;
import org.json.XML;
import org.zeroturnaround.zip.FileSource;
import org.zeroturnaround.zip.ZipEntrySource;
import org.zeroturnaround.zip.ZipUtil;
import sun.misc.BASE64Decoder;


import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;




import static com.name.business.utils.constans.K.INTERNAL_ERROR_MESSAGE;
import static com.name.business.utils.constans.K.messages_error;


public class JsonConverterBusiness {

    private JsonConverterDao billDAO;

    //VARIABLES PARA CONEXIÓN CORREO



    public Long sendEmail(String sender, String recipient,  String messageToSend){


        final String username = "somoscampus_bog@unal.edu.co";
        final String password = "CYUchx48";
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(sender+"@unal.edu.co"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(recipient+"@unal.edu.co"));
            message.setSubject("Campus - Email");
            message.setText(messageToSend);
            Transport.send(message);
            System.out.println("Correcto!");
            return new Long(1);
        } catch (MessagingException e) {
            e.printStackTrace();
            return new Long(0);
        }


    }


    ///////////////////////////////////

    public JsonConverterBusiness(JsonConverterDao billDAO) {
        this.billDAO = billDAO;
    }


    public Long getAdmin(String username){
        try{
            Long response = billDAO.getAdmin( username );
            if(response>0)return new Long (1);
            return new Long (0);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(-1);
        }
    }

    public Either<IException, List<categoria>> getCategorias(){
        try{
            List<categoria> response = billDAO.getCategorias();
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<pregunta>> getPreguntas(){
        try{
            List<pregunta> response = billDAO.getPreguntas();
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, String> getPreguntaString(){
        try{
            List<pregunta> response = billDAO.getPreguntas();
            String responseString = "";
            for(int i = 0; i < response.size() ;i++){
                responseString += response.get(i).getId_pregunta() +",";
            }
            return Either.right(responseString);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Long crear_pregunta(String pregunta, Long id_categoria ){
        try{
            billDAO.crear_pregunta(pregunta,id_categoria );
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }

    public Long eliminar_pregunta(Long id_pregunta ){
        try{
            billDAO.eliminar_pregunta(id_pregunta);
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }

    public Long modificar_pregunta(String pregunta, Long id_categoria , Long id_pregunta ){
        try{
            billDAO.modificar_pregunta(pregunta,id_categoria,id_pregunta );
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }

    public Long crear_categoria(String categoria ){
        try{
            billDAO.crear_categoria(categoria );
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }

    public Boolean getAuthLDAP(String username, String password ){

            byte[] decodedUser = Base64.decodeBase64(username);
            byte[] decodedPassword = Base64.decodeBase64(password);



            Hashtable env = new Hashtable(11);

            env.put(Context.PROVIDER_URL, "ldap://168.176.239.1:389");
            env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");

            // Authenticate as S. User and password "mysecret"
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, "uid="+new String(decodedUser)+", ou=people, o=unal.edu.co");
            env.put(Context.SECURITY_CREDENTIALS, new String(decodedPassword));

            /*
            System.out.println("username: "+username);
            System.out.println("password: "+password);
            System.out.println("username: "+new String(decodedUser));
            System.out.println("password: "+new String(decodedPassword));
            */

            /*

            String idList = "1,2,3,4";

            String[] SplittedList = idList.split(",");
            List<Long> CastedList = new ArrayList<>();
            for(int n = 0;n<SplittedList.length;n++){
                CastedList.add(Long.parseLong(SplittedList[n]));
            }

            List<publicacion> response = billDAO.getPublicaciones(CastedList);
            for(publicacion a : response){
                a.setLikes(billDAO.likes(a.getID_PUBLICACION()));
            }*/

            try {
                // Create initial context
                DirContext ctx = new InitialDirContext(env);
                /*
                Attributes attrs;
                for(publicacion a : response){
                    attrs = ctx.getAttributes( "uid="+a.getUSERNAME()+", ou=people, o=unal.edu.co" );
                    String nombre= attrs.get( "givenName" ).toString();
                    String apellido= attrs.get( "sn" ).toString();
                    System.out.println(nombre.replace("givenName: ","").split(" ")[0] +" "+apellido.replace("sn: ","").charAt(0)+".");
                    billDAO.updateUserPostTable(a.getID_PUBLICACION(),nombre.replace("givenName: ","").split(" ")[0] +" "+apellido.replace("sn: ","").charAt(0)+".");
                }
                */
                ctx.close();

                return true;
            } catch (NamingException e) {
                e.printStackTrace();
                return false;
            }

    }

    public Either<IException, List<publicacion>> getPublicaciones(List<Long> idList){
        try{
            List<publicacion> response = billDAO.getPublicaciones(idList);
            for(publicacion a : response){
                a.setLikes(billDAO.likes(a.getID_PUBLICACION()));
            }
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, publicacion> getPublicacionesById(Long idPub){
        try{
            publicacion response = billDAO.getPublicacionesById(idPub);

            response.setLikes(billDAO.likes(response.getID_PUBLICACION()));

            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, List<publicacion>> getPublicacionesPregunta(List<Long> idList, List<Long> pregList){
        try{
            List<publicacion> response = billDAO.getPublicacionesPregunta(idList,pregList);
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, List<publicacion>> getPublicacionesGalery(List<Long> idList){
        try{
            List<publicacion> response = billDAO.getPublicacionesGalery(idList);
            for(publicacion a : response){
                a.setLikes(billDAO.likes(a.getID_PUBLICACION()));
            }
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, Float> calculadistanciagps(Float lat, Float longi){
        try{
            Float response = billDAO.calculadistanciagps(lat,longi);
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, List<publicacion>> getPublicacionesHijo(Long idList){
        try{
            List<publicacion> response = billDAO.getPublicacionesHijo(idList);
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

    public Either<IException, List<estadoPublicacion>> getEstadosPublicacion(){
        try{
            List<estadoPublicacion> response = billDAO.getEstadosPublicacion();
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Long postPublicacion(Long IDPREGUNTA,
                                Long IDPUBLICACIONPADRE,
                                String FECHAPUBLICACION,
                                String USERNAME,
                                String PUBLICACION,
                                String IMGURL,
                                Long IDESTADOPUBLICACION,
                                Float LATITUD,
                                Float LONGITUD,
                                Float KMS,
                                Float X,
                                Float Y,
                                String USER){
        try{
            System.out.println(IDPREGUNTA+","+
                    IDPUBLICACIONPADRE+","+
                    FECHAPUBLICACION+","+
                    USERNAME+","+
                    PUBLICACION+","+
                    IMGURL+","+
                    IDESTADOPUBLICACION+","+
                    LATITUD+","+
                    LONGITUD+","+
                    KMS +","+X+","+Y+","+USER);
            billDAO.postPublicacion(IDPREGUNTA,
                     IDPUBLICACIONPADRE,
                     new Date(),
                     USERNAME,
                     PUBLICACION,
                     "/galeria/"+IMGURL,
                     IDESTADOPUBLICACION,
                     LATITUD,
                     LONGITUD,
                     KMS,X,Y,USER);
            return billDAO.getLatestPublication();
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }


    public Either<IException,Long> postImage(Base64FileDTO dto) {
        try {

            String Base64File = dto.getFile();
            String extension = dto.getformat();
            String fileName = dto.getFileName();
            System.out.println("NOMBRE: "+fileName);
            System.out.println("NOMBRE JK TEST: "+fileName);
            System.out.println("EXTENSION: "+extension);

            // create a buffered image
            BufferedImage image = null;
            byte[] imageByte;

            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(Base64File.split(",")[1]);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();

            // write the image to a file
            File outputfile = new File("/opt/tomcat/apache-tomcat-8.5.63/webapps/campus/galeria/"+fileName);
            ImageIO.write(image, extension, outputfile);

            return Either.right(new Long(1));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage())));
        }
    }


    public Either<IException,Long> postImageIsla(Base64FileDTO dto) {
        try {

            String Base64File = dto.getFile();
            String extension = dto.getformat();
            String fileName = dto.getFileName();
            System.out.println("NOMBRE: "+fileName);
            System.out.println("NOMBRE JK TEST: "+fileName);
            System.out.println("EXTENSION: "+extension);

            // create a buffered image
            BufferedImage image = null;
            byte[] imageByte;

            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(Base64File.split(",")[1]);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();

            // write the image to a file
            File outputfile = new File("/opt/tomcat/apache-tomcat-8.5.63/webapps/campus/islas/"+fileName);
            ImageIO.write(image, extension, outputfile);

            return Either.right(new Long(1));
        }catch (Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(e.getMessage())));
        }
    }



    public Long ACTUALIZAR_ESTADO_PUBLICACION(Long idpublicacion,
                                Long idestadopublicacion ){
        try{

            billDAO.ACTUALIZAR_ESTADO_PUBLICACION( idpublicacion,
                                                   idestadopublicacion );
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }


    public Long eliminar_administrador(String user ){
        try{

            billDAO.eliminar_administrador( user);
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }


    public Long crear_administrador(String user, String nombre ){
        try{

            billDAO.crear_administrador( user, nombre );
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }


    public Either<IException, List<user>> getUsers(){
        try{
            List<user> response = billDAO.getUsers();
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, List<user>> getUsersByUsername(String username){
        try{
            List<user> response = billDAO.getUsersByUsername(username);
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Either<IException, List<publicacionData>> getPublicationLike(String phrase){
        try{
            List<publicacionData> response = billDAO.getPublicationLike("%"+phrase+"%");
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public Long fixMapCoordinates(Long idPublicacion,
                                   Double mapax,
                                   Double mapay){
        try{
            billDAO.fixMapCoordinates(idPublicacion,mapax,mapay);
            return new Long(1);
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }


    public Either<IException, List<publicacionPosicion>> getPublicationPosicion(){
        try{
            List<publicacionPosicion> response = billDAO.getPublicationPosicion();
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, List<publicacionPosicion>> getPublicationPosicionIdPregunta(Long ID_PREGUNTA){
        try{
            List<publicacionPosicion> response = billDAO.getPublicationPosicionIdPregunta(ID_PREGUNTA);
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, List<publicacionPosicion>> getPublicationPosicionById(Long ID_PUBLICACION_POSICION){
        try{
            List<publicacionPosicion> response = billDAO.getPublicationPosicionById(ID_PUBLICACION_POSICION);
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, List<publicacionPosicion>> getPublicationPosicionByPublicacion(Long ID_PUBLICACION){
        try{
            List<publicacionPosicion> response = billDAO.getPublicationPosicionByPublicacion(ID_PUBLICACION);
            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }




    public Long CREAR_PUBLICACION_POSICION(Double X,
                                           Double Y,
                                           Long ID_PUB,
                                           Long ID_PREG){
        try{

            billDAO.CREAR_PUBLICACION_POSICION( X,Y,ID_PUB,ID_PREG );
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }



    public Long LIMPIAR_PUBLICACION_POSICION(Long ID_PUB ){
        try{

            billDAO.LIMPIAR_PUBLICACION_POSICION( ID_PUB );
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }



    public Long ACTUALIZAR_PUB_POSICION(Double X,
                                        Double Y,
                                        Long ID_PUB_POS ){
        try{

            billDAO.ACTUALIZAR_PUB_POSICION( X,Y,ID_PUB_POS );
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }


    public Long updatePublicacionOnPosicion( Long ID_PUBLICACION,
                                             Long ID_PUBLICACION_POSICION ){
        try{

            billDAO.updatePublicacionOnPosicion( ID_PUBLICACION,
                                                 ID_PUBLICACION_POSICION  );
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }


    public Either<IException, Long> postCoordinates(){
        try{
            String list = "50.4,80;50.4,111;58.4,95.5;55.4,105;47.4,83;45,90;50.4,67;50.4,124;64,95.5;63,85;63,105.5;47.4,69;44.4,71;41.4,75;38.4,81;50.4,57;50.4,134;68.4,95.5;32.4,95.5;47.4,58;44.4,60;41.4,62;38.4,66;35.4,74;33,105.5;35,115.5;38,122;41,128;45,132;67.4,105.5;66,75.5;73.4,95.5;72.4,107.5;70.4,119.5;50.4,143;53.4,143;56.4,141.5;47.4,143;44.4,141.5;41.4,139;38.4,136;35.4,131;32.4,124;29.4,114;28,103;50.4,47;53.4,48;56.4,49;59.4,51;62.4,54;65.4,57.5;47.4,48;44.4,50;41.4,52;38.4,55;35.4,60;50.4,36.5;53.4,37;56.4,38;59.4,40;62.4,42;65.4,45.5;68.4,50;78,95.5;77.5,105.5;76,115.5;74,125.5;70,135.5;63,145.5;60,148.5;57,150.5;54,151.5;50.4,152.5;40.4,148.5;38,145.5;35,142.5;32,137;29,130;26,120;24,110;23.5,95.5;24,84.5;35,49;38,45;41,41;44,39;47,37.5;50.4,36.5;50.4,26.5;53.4,27;56.4,28;59.4,30;62.4,32;65.4,35;68.4,38;71.4,43;74.4,49;82.4,85;83,95.5;82,106;80,117;77,130;74,138;71,144;68,150;65,155;62,158;59,159;56,161;35,152;32,149;29,143;26,136;23,126;20,116;19,106;19,95.5;19.5,83.5;21,73.5;23,63.5;26,53.5;31,42;35,35;38,32;41,30;44,28.5;47,27";
            String [] splittedList = list.split(";");
            for(String item: splittedList){
                System.out.println("---------------------------------------------");
                String [] SplittedItem = item.split(",");
                System.out.println("ITEM: "+item);
                System.out.println("ITEM 1: "+SplittedItem[0]);
                System.out.println("ITEM 2: "+SplittedItem[1]);
                billDAO.CREAR_PUBLICACION_POSICION(new Double(SplittedItem[1]),new Double(SplittedItem[0]),null,new Long(102));
                System.out.println("X: "+new Double(SplittedItem[1]));
                System.out.println("Y: "+new Double(SplittedItem[0]));
                System.out.println("---------------------------------------------");
            }
            return Either.right(new Long(1));
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }



    public void rejectPublication(listItemDTO element){
        billDAO.ACTUALIZAR_ESTADO_PUBLICACION(element.getId_PUBLICACION(),new Long(3));
        if(element.getId_PUBLICACION_PADRE()!=new Long(0)){
            fixMapCoordinates(element.getId_PUBLICACION(),new Double(0), new Double(0));
            billDAO.LIMPIAR_PUBLICACION_POSICION(element.getId_PUBLICACION());
        }
    }


    public void aceptPublication(listItemDTO element){
        billDAO.ACTUALIZAR_ESTADO_PUBLICACION(element.getId_PUBLICACION(),new Long(2));
        if(element.getId_PUBLICACION_PADRE()!=new Long(0)){
            Double x = new Double(0);
            Double y = new Double(0);
            Long idPosicion = new Long(0);

            List<publicacionPosicion> list = billDAO.getPublicationPosicionIdPregunta(element.getId_PREGUNTA());

            if(element.getDistnacia_KMS()<= 4){
                List<publicacionPosicion> subList = list.subList(0,6);

                List<publicacionPosicion> emptyPositions = new ArrayList<>();

                for(int i = 0; i<subList.size() ;i++){
                    publicacionPosicion elementNow = subList.get(i);
                    if(elementNow.getID_PUBLICACION()==0){
                        emptyPositions.add(elementNow);
                    }
                }

                if(emptyPositions.size()>0){
                    //HAY POSICIONES VACIAS
                    x = emptyPositions.get(0).getMAPAX();
                    y = emptyPositions.get(0).getMAPAY();
                    //CAMBIAR PUBLICACION DE 0 A ID NUEVO
                    idPosicion = emptyPositions.get(0).getID_PUBLICACION_POSICION();
                }else{
                    //NO HAY POSICIONES VACIAS
                    Collections.sort(list, new Comparator<publicacionPosicion>() {
                        public int compare(publicacionPosicion c1, publicacionPosicion c2) {
                            if (c1.getID_PUBLICACION() > c2.getID_PUBLICACION()) return 1;
                            if (c1.getID_PUBLICACION() < c2.getID_PUBLICACION()) return -1;
                            return 0;
                        }
                    });
                    x = subList.get(0).getMAPAX();
                    y = subList.get(0).getMAPAY();
                    //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
                    idPosicion = subList.get(0).getID_PUBLICACION_POSICION();
                }

            }


            if(element.getId_PUBLICACION_PADRE() >4 && element.getId_PUBLICACION_PADRE() <=15){
                List<publicacionPosicion> subList = list.subList(6,15);

                List<publicacionPosicion> emptyPositions = new ArrayList<>();

                for(int i = 0; i<subList.size() ;i++){
                    publicacionPosicion elementNow = subList.get(i);
                    if(elementNow.getID_PUBLICACION()==0){
                        emptyPositions.add(elementNow);
                    }
                }

                if(emptyPositions.size()>0){
                    //HAY POSICIONES VACIAS
                    x = emptyPositions.get(0).getMAPAX();
                    y = emptyPositions.get(0).getMAPAY();
                    //CAMBIAR PUBLICACION DE 0 A ID NUEVO
                    idPosicion = emptyPositions.get(0).getID_PUBLICACION_POSICION();
                }else{
                    //NO HAY POSICIONES VACIAS
                    Collections.sort(list, new Comparator<publicacionPosicion>() {
                        public int compare(publicacionPosicion c1, publicacionPosicion c2) {
                            if (c1.getID_PUBLICACION() > c2.getID_PUBLICACION()) return 1;
                            if (c1.getID_PUBLICACION() < c2.getID_PUBLICACION()) return -1;
                            return 0;
                        }
                    });
                    x = subList.get(0).getMAPAX();
                    y = subList.get(0).getMAPAY();
                    //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
                    idPosicion = subList.get(0).getID_PUBLICACION_POSICION();
                }

            }


            if(element.getDistnacia_KMS() >15 && element.getDistnacia_KMS()<=100){
                List<publicacionPosicion> subList = list.subList(15,31);

                List<publicacionPosicion> emptyPositions = new ArrayList<>();

                for(int i = 0; i<subList.size() ;i++){
                    publicacionPosicion elementNow = subList.get(i);
                    if(elementNow.getID_PUBLICACION()==0){
                        emptyPositions.add(elementNow);
                    }
                }

                if(emptyPositions.size()>0){
                    //HAY POSICIONES VACIAS
                    x = emptyPositions.get(0).getMAPAX();
                    y = emptyPositions.get(0).getMAPAY();
                    //CAMBIAR PUBLICACION DE 0 A ID NUEVO
                    idPosicion = emptyPositions.get(0).getID_PUBLICACION_POSICION();
                }else{
                    //NO HAY POSICIONES VACIAS
                    Collections.sort(list, new Comparator<publicacionPosicion>() {
                        public int compare(publicacionPosicion c1, publicacionPosicion c2) {
                            if (c1.getID_PUBLICACION() > c2.getID_PUBLICACION()) return 1;
                            if (c1.getID_PUBLICACION() < c2.getID_PUBLICACION()) return -1;
                            return 0;
                        }
                    });
                    x = subList.get(0).getMAPAX();
                    y = subList.get(0).getMAPAY();
                    //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
                    idPosicion = subList.get(0).getID_PUBLICACION_POSICION();
                }

            }


            if(element.getDistnacia_KMS() >100 && element.getDistnacia_KMS()<=300){
                List<publicacionPosicion> subList = list.subList(31,57);

                List<publicacionPosicion> emptyPositions = new ArrayList<>();

                for(int i = 0; i<subList.size() ;i++){
                    publicacionPosicion elementNow = subList.get(i);
                    if(elementNow.getID_PUBLICACION()==0){
                        emptyPositions.add(elementNow);
                    }
                }

                if(emptyPositions.size()>0){
                    //HAY POSICIONES VACIAS
                    x = emptyPositions.get(0).getMAPAX();
                    y = emptyPositions.get(0).getMAPAY();
                    //CAMBIAR PUBLICACION DE 0 A ID NUEVO
                    idPosicion = emptyPositions.get(0).getID_PUBLICACION_POSICION();
                }else{
                    //NO HAY POSICIONES VACIAS
                    Collections.sort(list, new Comparator<publicacionPosicion>() {
                        public int compare(publicacionPosicion c1, publicacionPosicion c2) {
                            if (c1.getID_PUBLICACION() > c2.getID_PUBLICACION()) return 1;
                            if (c1.getID_PUBLICACION() < c2.getID_PUBLICACION()) return -1;
                            return 0;
                        }
                    });
                    x = subList.get(0).getMAPAX();
                    y = subList.get(0).getMAPAY();
                    //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
                    idPosicion = subList.get(0).getID_PUBLICACION_POSICION();
                }

            }


            if(element.getDistnacia_KMS() >300 && element.getDistnacia_KMS()<=1000){
                List<publicacionPosicion> subList = list.subList(57,88);

                List<publicacionPosicion> emptyPositions = new ArrayList<>();

                for(int i = 0; i<subList.size() ;i++){
                    publicacionPosicion elementNow = subList.get(i);
                    if(elementNow.getID_PUBLICACION()==0){
                        emptyPositions.add(elementNow);
                    }
                }

                if(emptyPositions.size()>0){
                    //HAY POSICIONES VACIAS
                    x = emptyPositions.get(0).getMAPAX();
                    y = emptyPositions.get(0).getMAPAY();
                    //CAMBIAR PUBLICACION DE 0 A ID NUEVO
                    idPosicion = emptyPositions.get(0).getID_PUBLICACION_POSICION();
                }else{
                    //NO HAY POSICIONES VACIAS
                    Collections.sort(list, new Comparator<publicacionPosicion>() {
                        public int compare(publicacionPosicion c1, publicacionPosicion c2) {
                            if (c1.getID_PUBLICACION() > c2.getID_PUBLICACION()) return 1;
                            if (c1.getID_PUBLICACION() < c2.getID_PUBLICACION()) return -1;
                            return 0;
                        }
                    });
                    x = subList.get(0).getMAPAX();
                    y = subList.get(0).getMAPAY();
                    //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
                    idPosicion = subList.get(0).getID_PUBLICACION_POSICION();
                }

            }


            if(element.getDistnacia_KMS() >1000){
                List<publicacionPosicion> subList = list.subList(88,127);

                List<publicacionPosicion> emptyPositions = new ArrayList<>();

                for(int i = 0; i<subList.size() ;i++){
                    publicacionPosicion elementNow = subList.get(i);
                    if(elementNow.getID_PUBLICACION()==0){
                        emptyPositions.add(elementNow);
                    }
                }

                if(emptyPositions.size()>0){
                    //HAY POSICIONES VACIAS
                    x = emptyPositions.get(0).getMAPAX();
                    y = emptyPositions.get(0).getMAPAY();
                    //CAMBIAR PUBLICACION DE 0 A ID NUEVO
                    idPosicion = emptyPositions.get(0).getID_PUBLICACION_POSICION();
                }else{
                    //NO HAY POSICIONES VACIAS
                    Collections.sort(list, new Comparator<publicacionPosicion>() {
                        public int compare(publicacionPosicion c1, publicacionPosicion c2) {
                            if (c1.getID_PUBLICACION() > c2.getID_PUBLICACION()) return 1;
                            if (c1.getID_PUBLICACION() < c2.getID_PUBLICACION()) return -1;
                            return 0;
                        }
                    });
                    x = subList.get(0).getMAPAX();
                    y = subList.get(0).getMAPAY();
                    //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
                    idPosicion = subList.get(0).getID_PUBLICACION_POSICION();
                }

            }

            billDAO.updatePublicacionOnPosicion(element.getId_PUBLICACION(),idPosicion);

            fixMapCoordinates(element.getId_PUBLICACION(),x, y);
        }
    }



    public Long rechazarTodos(List<listItemDTO> itemList){
        try{

            int listSize = itemList.size();

            for(int i =0; i < listSize ;i++){
                listItemDTO actualItem = itemList.get(i);
                rejectPublication(actualItem);
            }

            return new Long(1);
        }catch (Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }


    public Long aceptarTodos(List<listItemDTO> itemList){
        try{

            int listSize = itemList.size();

            for(int i =0; i < listSize ;i++){
                listItemDTO actualItem = itemList.get(i);
                aceptPublication(actualItem);
            }

            return new Long(1);
        }catch (Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }



    public Either<IException, String> getExcel( ){
        try{
            List<ExcelData> list = billDAO.getExcelData();


            for(ExcelData a : list){
                a.setLikes(billDAO.likes(a.getID_PUBLICACION()));
            }

            String response = "0";

            HSSFWorkbook libro = new HSSFWorkbook();
            HSSFSheet hoja = libro.createSheet();
            HSSFRow fila = hoja.createRow(0);

            HSSFCell celda1 = fila.createCell((short)0);
            HSSFCell celda2 = fila.createCell((short)1);
            HSSFCell celda3 = fila.createCell((short)2);
            HSSFCell celda4 = fila.createCell((short)3);
            HSSFCell celda5 = fila.createCell((short)4);
            HSSFCell celda6 = fila.createCell((short)5);
            HSSFCell celda7 = fila.createCell((short)6);
            HSSFCell celda8 = fila.createCell((short)7);
            HSSFCell celda9 = fila.createCell((short)8);
            HSSFCell celda10 = fila.createCell((short)9);
            HSSFCell celda11 = fila.createCell((short)10);
            HSSFCell celda12 = fila.createCell((short)11);

            HSSFRichTextString texto1 = new HSSFRichTextString("ID_PUBLICACION");
            HSSFRichTextString texto2 = new HSSFRichTextString("PREGUNTA");
            HSSFRichTextString texto3 = new HSSFRichTextString("ID_PUBLICACION_PADRE");
            HSSFRichTextString texto4 = new HSSFRichTextString("FECHA_PUBLICACION");
            HSSFRichTextString texto5 = new HSSFRichTextString("USERNAME");
            HSSFRichTextString texto6 = new HSSFRichTextString("PUBLICACION");
            HSSFRichTextString texto7 = new HSSFRichTextString("IMG_URL");
            HSSFRichTextString texto8 = new HSSFRichTextString("ESTADO_PUBICACION");
            HSSFRichTextString texto9 = new HSSFRichTextString("LATITUD");
            HSSFRichTextString texto10 = new HSSFRichTextString("LONGITUD");
            HSSFRichTextString texto11 = new HSSFRichTextString("DISTANCIA_KMS");
            HSSFRichTextString texto12 = new HSSFRichTextString("LIKES");


            celda1.setCellValue(texto1);
            celda2.setCellValue(texto2);
            celda3.setCellValue(texto3);
            celda4.setCellValue(texto4);
            celda5.setCellValue(texto5);
            celda6.setCellValue(texto6);
            celda7.setCellValue(texto7);
            celda8.setCellValue(texto8);
            celda9.setCellValue(texto9);
            celda10.setCellValue(texto10);
            celda11.setCellValue(texto11);
            celda12.setCellValue(texto12);



            int cont = 1;

            for (ExcelData data:list) {

                HSSFRow filaN = hoja.createRow(cont);

                HSSFCell celda1N = filaN.createCell((short)0);
                HSSFCell celda2N = filaN.createCell((short)1);
                HSSFCell celda3N = filaN.createCell((short)2);
                HSSFCell celda4N = filaN.createCell((short)3);
                HSSFCell celda5N = filaN.createCell((short)4);
                HSSFCell celda6N = filaN.createCell((short)5);
                HSSFCell celda7N = filaN.createCell((short)6);
                HSSFCell celda8N = filaN.createCell((short)7);
                HSSFCell celda9N = filaN.createCell((short)8);
                HSSFCell celda10N = filaN.createCell((short)9);
                HSSFCell celda11N = filaN.createCell((short)10);
                HSSFCell celda12N = filaN.createCell((short)11);

                HSSFRichTextString texto1N = new HSSFRichTextString(String.valueOf(data.getID_PUBLICACION()));
                HSSFRichTextString texto2N = new HSSFRichTextString(data.getPREGUNTA());
                HSSFRichTextString texto3N = new HSSFRichTextString(String.valueOf(data.getID_PUBLICACION_PADRE()));
                HSSFRichTextString texto4N = new HSSFRichTextString(data.getFECHA_PUBLICACION());
                HSSFRichTextString texto5N = new HSSFRichTextString(data.getUSERNAME());
                HSSFRichTextString texto6N = new HSSFRichTextString(data.getPUBLICACION());
                HSSFRichTextString texto7N = new HSSFRichTextString(data.getIMG_URL());
                HSSFRichTextString texto8N = new HSSFRichTextString(data.getESTADO_PUBICACION());
                HSSFRichTextString texto9N = new HSSFRichTextString(data.getLATITUD());
                HSSFRichTextString texto10N = new HSSFRichTextString(data.getLONGITUD());
                HSSFRichTextString texto11N = new HSSFRichTextString(String.valueOf(data.getDISTANCIA_KMS()));
                HSSFRichTextString texto12N = new HSSFRichTextString(String.valueOf(data.getLikes()));


                celda1N.setCellValue(texto1N);
                celda2N.setCellValue(texto2N);
                celda3N.setCellValue(texto3N);
                celda4N.setCellValue(texto4N);
                celda5N.setCellValue(texto5N);
                celda6N.setCellValue(texto6N);
                celda7N.setCellValue(texto7N);
                celda8N.setCellValue(texto8N);
                celda9N.setCellValue(texto9N);
                celda10N.setCellValue(texto10N);
                celda11N.setCellValue(texto11N);
                celda12N.setCellValue(texto12N);

                cont++;
            }




            try {
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
                Date fecha = new Date();
                String Name = dateFormat.format(fecha);
                FileOutputStream elFichero = new FileOutputStream("/opt/tomcat/apache-tomcat-8.5.63/webapps/campus/archivos/"+Name+"reporte.xls");
                //FileOutputStream elFichero = new FileOutputStream("./"+Name+".xls");
                libro.write(elFichero);
                elFichero.close();
                response = Name+"reporte.xls";
            } catch (Exception e) {
                e.printStackTrace();
            }








            return Either.right(response);
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Either<IException, String> getZip(){
        try{
            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);
            ZipUtil.pack(new File("/opt/tomcat/apache-tomcat-8.5.63/webapps/campus/galeria/"), new File("/opt/tomcat/apache-tomcat-8.5.63/webapps/campus/archivos/"+Name+"galeria.zip"));

            return Either.right(Name+"galeria.zip");

        }catch(Exception e){
            e.printStackTrace();

            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));

        }
    }

    public Either<IException, String> getZipList(String files){
        try{
            DateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
            Date fecha = new Date();
            String Name = dateFormat.format(fecha);

            List<String> fileList = Arrays.asList(files.substring(0,files.length()-1).split(","));

            ZipUtil.packEntry(new File("/opt/tomcat/apache-tomcat-8.5.63/webapps/campus/galeria/"+(fileList.get(0).replaceAll("/galeria/",""))), new File("/opt/tomcat/apache-tomcat-8.5.63/webapps/campus/archivos/"+Name+"galeria.zip"));

            List<ZipEntrySource> entriesAsList = new ArrayList<>();

            for(int i=1;i< fileList.size();i++){

                String file = fileList.get(i);

                String fileDir = "/opt/tomcat/apache-tomcat-8.5.63/webapps/campus/galeria/" + file.replaceAll("/galeria/","");

                ZipEntrySource entry =new FileSource(file.replaceAll("/galeria/",""), new File(fileDir));

                ZipUtil.addEntry(new File("/opt/tomcat/apache-tomcat-8.5.63/webapps/campus/archivos/"+Name+"galeria.zip"),entry);

            }

            return Either.right(Name+"galeria.zip");

        }catch(Exception e){
            e.printStackTrace();

            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));

        }
    }



    public Long likes(Long id_pub){
        try{
            return billDAO.likes(id_pub);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }


    public Long findLike(Long id_pub,String usern){
        try{
            return billDAO.findLike(id_pub,usern);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }


    public Long crear_like(Long id_pub,
                           String usern){
        try{
            billDAO.crear_like( id_pub,usern );
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();
            return new Long(0);
        }
    }


    public Long eliminar_like(Long id_pub,
                           String usern){
        try{
            billDAO.eliminar_like( id_pub,usern );
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }


    public Either<IException, List<NotificacionData>> getNotificacionData(String id_usuario_propietario){
        try{
            return Either.right(billDAO.getNotificacionData(id_usuario_propietario));
        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }


    public Long crear_notificacion(Long TIPONOT,Long IDPUB,Long ESTADONOT,String IDUSERPROP,String IDUSERNOT,String PROP,String NOTIFIER){
        try{
            billDAO.crear_notificacion(TIPONOT,IDPUB,ESTADONOT,IDUSERPROP,IDUSERNOT,PROP,NOTIFIER);
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }


    public Long updateNotificacion(String id_usuario_propietario){
        try{
            billDAO.updateNotificacion(id_usuario_propietario);
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }


    public Long crearIslaContent(Long ID_ISLA,
                                 String CONTENIDO,
                                 Long ORDEN,
                                 Long MOSTRAR){
        try{
            billDAO.crearIslaContent(  ID_ISLA,
                     CONTENIDO,
                     ORDEN,
                     MOSTRAR);
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }


    public Long updateIslaContent(Long ID_ISLA,
                                 String CONTENIDO,
                                 Long ORDEN,
                                 Long MOSTRAR){
        try{
            billDAO.updateIslaContent(  ID_ISLA,
                    CONTENIDO,
                    ORDEN,
                    MOSTRAR);
            return new Long (1);
        }catch(Exception e){
            e.printStackTrace();;
            return new Long(0);
        }
    }

    public Either<IException, List<IslaData>> getIslaDataGeneric(){
        return Either.right(billDAO.getIslaData());
    }

    public Either<IException, List<Islas>> getIslaData(){
        try{


            List<IslaData> list = billDAO.getIslaData();

            System.out.println(list.size());

            List<IslaData> islaData1 = list.subList(0,30);
            List<IslaData> islaData2 = list.subList(30,60);
            List<IslaData> islaData3 = list.subList(60,90);
            List<IslaData> islaData4 = list.subList(90,120);
            List<IslaData> islaData5 = list.subList(120,150);
            List<IslaData> islaData6 = list.subList(150,180);



            Islas isla1 = new Islas(islaData1.get(0).getCONTENIDO(),
                                    islaData1.get(1).getCONTENIDO(),
                                    islaData1.get(2).getCONTENIDO(),
                                    islaData1.get(3).getCONTENIDO(),
                                    islaData1.get(4).getCONTENIDO(),
                                    islaData1.get(5).getCONTENIDO(),
                                    islaData1.get(6).getCONTENIDO(),
                                    islaData1.get(7).getCONTENIDO(),
                                    islaData1.get(8).getCONTENIDO(),
                                    islaData1.get(9).getCONTENIDO(),
                                    islaData1.get(10).getCONTENIDO(),
                                    islaData1.get(11).getCONTENIDO(),
                                    islaData1.get(12).getCONTENIDO(),
                                    islaData1.get(13).getCONTENIDO(),
                                    islaData1.get(14).getCONTENIDO(),
                                    islaData1.get(15).getCONTENIDO(),
                                    islaData1.get(16).getCONTENIDO(),
                                    islaData1.get(17).getCONTENIDO(),
                                    islaData1.get(18).getCONTENIDO(),
                                    islaData1.get(19).getCONTENIDO(),
                                    islaData1.get(20).getCONTENIDO(),
                                    islaData1.get(21).getCONTENIDO(),
                                    islaData1.get(22).getCONTENIDO(),
                                    islaData1.get(23).getCONTENIDO(),
                                    islaData1.get(24).getCONTENIDO(),
                                    islaData1.get(25).getCONTENIDO(),
                                    islaData1.get(26).getCONTENIDO(),
                                    islaData1.get(27).getCONTENIDO(),
                                    islaData1.get(28).getCONTENIDO(),
                                    islaData1.get(29).getCONTENIDO());

            Islas isla2 = new Islas(islaData2.get(0).getCONTENIDO(),
                                    islaData2.get(1).getCONTENIDO(),
                                    islaData2.get(2).getCONTENIDO(),
                                    islaData2.get(3).getCONTENIDO(),
                                    islaData2.get(4).getCONTENIDO(),
                                    islaData2.get(5).getCONTENIDO(),
                                    islaData2.get(6).getCONTENIDO(),
                                    islaData2.get(7).getCONTENIDO(),
                                    islaData2.get(8).getCONTENIDO(),
                                    islaData2.get(9).getCONTENIDO(),
                                    islaData2.get(10).getCONTENIDO(),
                                    islaData2.get(11).getCONTENIDO(),
                                    islaData2.get(12).getCONTENIDO(),
                                    islaData2.get(13).getCONTENIDO(),
                                    islaData2.get(14).getCONTENIDO(),
                                    islaData2.get(15).getCONTENIDO(),
                                    islaData2.get(16).getCONTENIDO(),
                                    islaData2.get(17).getCONTENIDO(),
                                    islaData2.get(18).getCONTENIDO(),
                                    islaData2.get(19).getCONTENIDO(),
                                    islaData2.get(20).getCONTENIDO(),
                                    islaData2.get(21).getCONTENIDO(),
                                    islaData2.get(22).getCONTENIDO(),
                                    islaData2.get(23).getCONTENIDO(),
                                    islaData2.get(24).getCONTENIDO(),
                                    islaData2.get(25).getCONTENIDO(),
                                    islaData2.get(26).getCONTENIDO(),
                                    islaData2.get(27).getCONTENIDO(),
                                    islaData2.get(28).getCONTENIDO(),
                                    islaData2.get(29).getCONTENIDO());

            Islas isla3 = new Islas(islaData3.get(0).getCONTENIDO(),
                                    islaData3.get(1).getCONTENIDO(),
                                    islaData3.get(2).getCONTENIDO(),
                                    islaData3.get(3).getCONTENIDO(),
                                    islaData3.get(4).getCONTENIDO(),
                                    islaData3.get(5).getCONTENIDO(),
                                    islaData3.get(6).getCONTENIDO(),
                                    islaData3.get(7).getCONTENIDO(),
                                    islaData3.get(8).getCONTENIDO(),
                                    islaData3.get(9).getCONTENIDO(),
                                    islaData3.get(10).getCONTENIDO(),
                                    islaData3.get(11).getCONTENIDO(),
                                    islaData3.get(12).getCONTENIDO(),
                                    islaData3.get(13).getCONTENIDO(),
                                    islaData3.get(14).getCONTENIDO(),
                                    islaData3.get(15).getCONTENIDO(),
                                    islaData3.get(16).getCONTENIDO(),
                                    islaData3.get(17).getCONTENIDO(),
                                    islaData3.get(18).getCONTENIDO(),
                                    islaData3.get(19).getCONTENIDO(),
                                    islaData3.get(20).getCONTENIDO(),
                                    islaData3.get(21).getCONTENIDO(),
                                    islaData3.get(22).getCONTENIDO(),
                                    islaData3.get(23).getCONTENIDO(),
                                    islaData3.get(24).getCONTENIDO(),
                                    islaData3.get(25).getCONTENIDO(),
                                    islaData3.get(26).getCONTENIDO(),
                                    islaData3.get(27).getCONTENIDO(),
                                    islaData3.get(28).getCONTENIDO(),
                                    islaData3.get(29).getCONTENIDO());

            Islas isla4 = new Islas(islaData4.get(0).getCONTENIDO(),
                                    islaData4.get(1).getCONTENIDO(),
                                    islaData4.get(2).getCONTENIDO(),
                                    islaData4.get(3).getCONTENIDO(),
                                    islaData4.get(4).getCONTENIDO(),
                                    islaData4.get(5).getCONTENIDO(),
                                    islaData4.get(6).getCONTENIDO(),
                                    islaData4.get(7).getCONTENIDO(),
                                    islaData4.get(8).getCONTENIDO(),
                                    islaData4.get(9).getCONTENIDO(),
                                    islaData4.get(10).getCONTENIDO(),
                                    islaData4.get(11).getCONTENIDO(),
                                    islaData4.get(12).getCONTENIDO(),
                                    islaData4.get(13).getCONTENIDO(),
                                    islaData4.get(14).getCONTENIDO(),
                                    islaData4.get(15).getCONTENIDO(),
                                    islaData4.get(16).getCONTENIDO(),
                                    islaData4.get(17).getCONTENIDO(),
                                    islaData4.get(18).getCONTENIDO(),
                                    islaData4.get(19).getCONTENIDO(),
                                    islaData4.get(20).getCONTENIDO(),
                                    islaData4.get(21).getCONTENIDO(),
                                    islaData4.get(22).getCONTENIDO(),
                                    islaData4.get(23).getCONTENIDO(),
                                    islaData4.get(24).getCONTENIDO(),
                                    islaData4.get(25).getCONTENIDO(),
                                    islaData4.get(26).getCONTENIDO(),
                                    islaData4.get(27).getCONTENIDO(),
                                    islaData4.get(28).getCONTENIDO(),
                                    islaData4.get(29).getCONTENIDO());

            Islas isla5 = new Islas(islaData5.get(0).getCONTENIDO(),
                                    islaData5.get(1).getCONTENIDO(),
                                    islaData5.get(2).getCONTENIDO(),
                                    islaData5.get(3).getCONTENIDO(),
                                    islaData5.get(4).getCONTENIDO(),
                                    islaData5.get(5).getCONTENIDO(),
                                    islaData5.get(6).getCONTENIDO(),
                                    islaData5.get(7).getCONTENIDO(),
                                    islaData5.get(8).getCONTENIDO(),
                                    islaData5.get(9).getCONTENIDO(),
                                    islaData5.get(10).getCONTENIDO(),
                                    islaData5.get(11).getCONTENIDO(),
                                    islaData5.get(12).getCONTENIDO(),
                                    islaData5.get(13).getCONTENIDO(),
                                    islaData5.get(14).getCONTENIDO(),
                                    islaData5.get(15).getCONTENIDO(),
                                    islaData5.get(16).getCONTENIDO(),
                                    islaData5.get(17).getCONTENIDO(),
                                    islaData5.get(18).getCONTENIDO(),
                                    islaData5.get(19).getCONTENIDO(),
                                    islaData5.get(20).getCONTENIDO(),
                                    islaData5.get(21).getCONTENIDO(),
                                    islaData5.get(22).getCONTENIDO(),
                                    islaData5.get(23).getCONTENIDO(),
                                    islaData5.get(24).getCONTENIDO(),
                                    islaData5.get(25).getCONTENIDO(),
                                    islaData5.get(26).getCONTENIDO(),
                                    islaData5.get(27).getCONTENIDO(),
                                    islaData5.get(28).getCONTENIDO(),
                                    islaData5.get(29).getCONTENIDO());

            Islas isla6 = new Islas(islaData6.get(0).getCONTENIDO(),
                                    islaData6.get(1).getCONTENIDO(),
                                    islaData6.get(2).getCONTENIDO(),
                                    islaData6.get(3).getCONTENIDO(),
                                    islaData6.get(4).getCONTENIDO(),
                                    islaData6.get(5).getCONTENIDO(),
                                    islaData6.get(6).getCONTENIDO(),
                                    islaData6.get(7).getCONTENIDO(),
                                    islaData6.get(8).getCONTENIDO(),
                                    islaData6.get(9).getCONTENIDO(),
                                    islaData6.get(10).getCONTENIDO(),
                                    islaData6.get(11).getCONTENIDO(),
                                    islaData6.get(12).getCONTENIDO(),
                                    islaData6.get(13).getCONTENIDO(),
                                    islaData6.get(14).getCONTENIDO(),
                                    islaData6.get(15).getCONTENIDO(),
                                    islaData6.get(16).getCONTENIDO(),
                                    islaData6.get(17).getCONTENIDO(),
                                    islaData6.get(18).getCONTENIDO(),
                                    islaData6.get(19).getCONTENIDO(),
                                    islaData6.get(20).getCONTENIDO(),
                                    islaData6.get(21).getCONTENIDO(),
                                    islaData6.get(22).getCONTENIDO(),
                                    islaData6.get(23).getCONTENIDO(),
                                    islaData6.get(24).getCONTENIDO(),
                                    islaData6.get(25).getCONTENIDO(),
                                    islaData6.get(26).getCONTENIDO(),
                                    islaData6.get(27).getCONTENIDO(),
                                    islaData6.get(28).getCONTENIDO(),
                                    islaData6.get(29).getCONTENIDO());


            List<Boolean> mostrar1 = new ArrayList<>();
            List<Boolean> mostrar2 = new ArrayList<>();
            List<Boolean> mostrar3 = new ArrayList<>();
            List<Boolean> mostrar4 = new ArrayList<>();
            List<Boolean> mostrar5 = new ArrayList<>();
            List<Boolean> mostrar6 = new ArrayList<>();

            for(int i=0;i<30;i++){
                if(islaData1.get(i).getMOSTRAR()==1){
                    mostrar1.add(true);
                }
                else{
                    mostrar1.add(false);
                }
            }

            for(int i=0;i<30;i++){
                if(islaData2.get(i).getMOSTRAR()==1){
                    mostrar2.add(true);
                }
                else{
                    mostrar2.add(false);
                }
            }

            for(int i=0;i<30;i++){
                if(islaData3.get(i).getMOSTRAR()==1){
                    mostrar3.add(true);
                }
                else{
                    mostrar3.add(false);
                }
            }

            for(int i=0;i<30;i++){
                if(islaData4.get(i).getMOSTRAR()==1){
                    mostrar4.add(true);
                }
                else{
                    mostrar4.add(false);
                }
            }

            for(int i=0;i<30;i++){
                if(islaData5.get(i).getMOSTRAR()==1){
                    mostrar5.add(true);
                }
                else{
                    mostrar5.add(false);
                }
            }

            for(int i=0;i<30;i++){
                if(islaData6.get(i).getMOSTRAR()==1){
                    mostrar6.add(true);
                }
                else{
                    mostrar6.add(false);
                }
            }

            isla1.setMostrar(mostrar1);
            isla2.setMostrar(mostrar2);
            isla3.setMostrar(mostrar3);
            isla4.setMostrar(mostrar4);
            isla5.setMostrar(mostrar5);
            isla6.setMostrar(mostrar6);



            List<Islas> response = new ArrayList<>();
            response.add(isla1);
            response.add(isla2);
            response.add(isla3);
            response.add(isla4);
            response.add(isla5);
            response.add(isla6);



            return Either.right(response);

        }catch(Exception e){
            e.printStackTrace();
            return Either.left(new TechnicalException(messages_error(INTERNAL_ERROR_MESSAGE)));
        }
    }

}