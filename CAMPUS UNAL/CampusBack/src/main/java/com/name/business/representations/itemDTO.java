package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class itemDTO {


    List<listItemDTO> listStore;

    @JsonCreator
    public itemDTO(@JsonProperty("listItem") List<listItemDTO> listStore) {
        this.listStore = listStore;
    }
    //------------------------------------------------------------------------------------------------
    public List<listItemDTO>  getlistStore() {
        return listStore;
    }
    public void setlistStore(List<listItemDTO>  listStore) {
        this.listStore = listStore;
    }
    //------------------------------------------------------------------------------------------------



}
