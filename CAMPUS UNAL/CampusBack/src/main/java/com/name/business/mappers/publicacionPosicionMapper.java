package com.name.business.mappers;


import com.name.business.entities.publicacion;
import com.name.business.entities.publicacionData;
import com.name.business.entities.publicacionPosicion;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class publicacionPosicionMapper implements ResultSetMapper<publicacionPosicion> {

    @Override
    public publicacionPosicion map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new publicacionPosicion(
                resultSet.getLong("ID_PUBLICACION_POSICION"),
                resultSet.getDouble("MAPAX"),
                resultSet.getDouble("MAPAY"),
                resultSet.getLong("ID_PUBLICACION"),
                resultSet.getLong("ID_PREGUNTA")
        );
    }
}
