package com.name.business.representations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class listItemDTO {
    String pregunta;
    Boolean check;
    Double mapax;
    Double mapay;
    Long id_PUBLICACION;
    Long id_PREGUNTA;
    Long id_PUBLICACION_PADRE;
    String fecha_PUBLICACION;
    String username;
    String publicacion;
    String img_URL;
    Long id_ESTADO_PUBLICACION;
    Double latitud;
    Double longitud;
    Double distnacia_KMS;

    @JsonCreator
    public listItemDTO(@JsonProperty("pregunta") String pregunta,
                       @JsonProperty("check") Boolean check,
                       @JsonProperty("mapax") Double mapax,
                       @JsonProperty("mapay") Double mapay,
                       @JsonProperty("id_PUBLICACION") Long id_PUBLICACION,
                       @JsonProperty("id_PREGUNTA") Long id_PREGUNTA,
                       @JsonProperty("id_PUBLICACION_PADRE") Long id_PUBLICACION_PADRE,
                       @JsonProperty("fecha_PUBLICACION") String fecha_PUBLICACION,
                       @JsonProperty("username") String username,
                       @JsonProperty("publicacion") String publicacion,
                       @JsonProperty("img_URL") String img_URL,
                       @JsonProperty("id_ESTADO_PUBLICACION") Long id_ESTADO_PUBLICACION,
                       @JsonProperty("latitud") Double latitud,
                       @JsonProperty("longitud") Double longitud,
                       @JsonProperty("distancia_KMS") Double distnacia_KMS) {
        this.pregunta = pregunta;
        this.check = check;
        this.mapax = mapax;
        this.mapay = mapay;
        this.id_PUBLICACION = id_PUBLICACION;
        this.id_PREGUNTA = id_PREGUNTA;
        this.id_PUBLICACION_PADRE = id_PUBLICACION_PADRE;
        this.fecha_PUBLICACION = fecha_PUBLICACION;
        this.username = username;
        this.publicacion = publicacion;
        this.img_URL = img_URL;
        this.id_ESTADO_PUBLICACION = id_ESTADO_PUBLICACION;
        this.latitud = latitud;
        this.longitud = longitud;
        this.distnacia_KMS = distnacia_KMS;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public Double getMapax() {
        return mapax;
    }

    public void setMapax(Double mapax) {
        this.mapax = mapax;
    }

    public Double getMapay() {
        return mapay;
    }

    public void setMapay(Double mapay) {
        this.mapay = mapay;
    }

    public Long getId_PUBLICACION() {
        return id_PUBLICACION;
    }

    public void setId_PUBLICACION(Long id_PUBLICACION) {
        this.id_PUBLICACION = id_PUBLICACION;
    }

    public Long getId_PREGUNTA() {
        return id_PREGUNTA;
    }

    public void setId_PREGUNTA(Long id_PREGUNTA) {
        this.id_PREGUNTA = id_PREGUNTA;
    }

    public Long getId_PUBLICACION_PADRE() {
        return id_PUBLICACION_PADRE;
    }

    public void setId_PUBLICACION_PADRE(Long id_PUBLICACION_PADRE) {
        this.id_PUBLICACION_PADRE = id_PUBLICACION_PADRE;
    }

    public String getFecha_PUBLICACION() {
        return fecha_PUBLICACION;
    }

    public void setFecha_PUBLICACION(String fecha_PUBLICACION) {
        this.fecha_PUBLICACION = fecha_PUBLICACION;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(String publicacion) {
        this.publicacion = publicacion;
    }

    public String getImg_URL() {
        return img_URL;
    }

    public void setImg_URL(String img_URL) {
        this.img_URL = img_URL;
    }

    public Long getId_ESTADO_PUBLICACION() {
        return id_ESTADO_PUBLICACION;
    }

    public void setId_ESTADO_PUBLICACION(Long id_ESTADO_PUBLICACION) {
        this.id_ESTADO_PUBLICACION = id_ESTADO_PUBLICACION;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Double getDistnacia_KMS() {
        return distnacia_KMS;
    }

    public void setDistnacia_KMS(Double distnacia_KMS) {
        this.distnacia_KMS = distnacia_KMS;
    }
}
