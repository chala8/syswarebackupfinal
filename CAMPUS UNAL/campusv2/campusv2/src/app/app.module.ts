import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PathLocationStrategy, LocationStrategy } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './components/landing/landing.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MapComponent } from './components/map/map.component';
import { AdministratorComponent } from './components/administrator/administrator.component';
import { GaleriaComponent } from './components/galeria/galeria.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ModalComponent } from './components/modal/modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MatDialogRef, MatProgressSpinnerModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import * as bootstrap from "bootstrap";
import {NgxPaginationModule} from 'ngx-pagination';
import { CanActivateViaAuthGuardService } from './services/CanActivateViaAuthGuard/can-activate-via-auth-guard.service';
import { HeaderNoComponent } from './components/header-no/header-no.component';
import { FooterNoComponent } from './components/footer-no/footer-no.component';
import { InnerMapComponent } from './components/inner-map/inner-map.component';
import { LocalStorage } from './services/LocalStorage';
import { LoaderComponent } from './components/loader/loader.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonModule } from '@angular/material/button';
@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    MapComponent,
    AdministratorComponent,
    GaleriaComponent,
    HeaderComponent,
    FooterComponent,
    ModalComponent,
    HeaderNoComponent,
    FooterNoComponent,
    InnerMapComponent,
    LoaderComponent
  ],
  imports: [
    MatProgressSpinnerModule,
    NgxPaginationModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatSelectModule,
    FontAwesomeModule,
    MatSlideToggleModule,
    MatButtonModule
  ],
  providers: [CanActivateViaAuthGuardService,{ provide: LocationStrategy, useClass: PathLocationStrategy }],
  entryComponents: [ModalComponent,LoaderComponent],
  bootstrap: [AppComponent]
})

export class AppModule { }
