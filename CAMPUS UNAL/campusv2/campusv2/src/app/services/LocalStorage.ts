import { Injectable,EventEmitter  } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';


@Injectable({
  providedIn:'root'
})

export class LocalStorage {


  constructor( private http: HttpClient) { }

  private username = "";
  private logginState = 0;
  private mapStatus = 0;
  private usuario = "";
  private hasInner = true;

  getHasInner(){
    return this.hasInner;
  }

  setHasInner(has){
    this.hasInner = has;
  }

  getusuario(){
    return this.usuario;
  }

  setusuario(usuario){
    this.usuario = usuario;
  }

  getUsername(){
    return this.username;
  }

  setUsername(username){
    this.username = username;
  }

  getLogginState(){
      return this.logginState
  }

  setLogginState(state){
      this.logginState = state;
  }


  getmapStatus(){
    return this.mapStatus
  }

  setmapStatus(state){
    this.mapStatus = state;
  }


}
