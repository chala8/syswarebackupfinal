import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LocalStorage } from '../LocalStorage';

@Injectable({
  providedIn: 'root'
})

export class LandingProtectGuardService implements CanActivate {

  
  constructor(private router: Router, private locStorage: LocalStorage,) { }


  canActivate(){ 
    //console.log (this.locStorage.getLogginState());
    return this.locStorage.getLogginState() == 0 ? true : this.returnFalse()
  }


  returnFalse(){
    //console.log('Debes estar deslogueado para ver esto');
    this.router.navigate(['/map']);
    return false;
  }
  
}
 