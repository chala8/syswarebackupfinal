import { Component, OnInit } from '@angular/core';
import { LocalStorage } from 'src/app/services/LocalStorage';

@Component({
  selector: 'app-footer-no',
  templateUrl: './footer-no.component.html',
  styleUrls: ['./footer-no.component.scss']
})
export class FooterNoComponent implements OnInit {

  constructor(public locStorage: LocalStorage) { }

  ngOnInit() {
  }

}
