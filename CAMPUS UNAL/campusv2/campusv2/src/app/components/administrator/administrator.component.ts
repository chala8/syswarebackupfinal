import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/services/LocalStorage';
import { LoaderComponent } from '../loader/loader.component';
import { ModalComponent } from '../modal/modal.component';
import { MatSlideToggleChange, MatSlideToggleModule } from '@angular/material/slide-toggle';
/// <reference path ="../../node_modules/@types/jquery/index.d.ts"/>
declare var $: any


@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.scss']
})
export class AdministratorComponent implements OnInit {

  constructor(private httpClient: HttpClient,
    public dialog: MatDialog,
    private router: Router,
    public locStorage: LocalStorage) { }

    @ViewChild('ad1', {static: false}) InputVar: ElementRef;
    @ViewChild('ad2', {static: false}) InputVar2: ElementRef;
    @ViewChild('ad3', {static: false}) InputVar3: ElementRef;
    @ViewChild('ad4', {static: false}) InputVar4: ElementRef;
    @ViewChild('ad5', {static: false}) InputVar5: ElementRef;
    @ViewChild('ad6', {static: false}) InputVar6: ElementRef;
    @ViewChild('adBG', {static: false}) InputVarBG: ElementRef;

  idPregunta = -1;

  listaPreguntas;

  listaPublicacionesPadre;
  listaPublicacionesPadreCopy;

  listaCategorias;
  idCategoria;

  allSelected = false;
  allSelected2 = false;
  allSelected3 = false;
  preguntaNombre="";
  idCategoriaEdit;


  listaUsers;
  islaData;
  islaID = 1;

  mostrarTitulo = true;
  selectedIsla;


  UrlSitioWeb = [];

  toggleChanges($event: MatSlideToggleChange,order) {
    this.selectedIsla.mostrar[order] = $event.checked;
  }

  setIslaID(item){
    this.islaID = item;
    this.selectedIsla = this.islaData[item-1];
    this.UrlSitioWeb = this.selectedIsla.urlsitioWeb.split(",");
  }

  realoadIslas(){
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/getIslaData",{}).subscribe(response => {
        this.islaData = response;
        this.selectedIsla = response[0];
      }
    )
  }

  translateBoolean(item){
    console.log(item)
    if(item==true) return 1;
    if(!item==true) return 0;
  }

  updateIslaElement(element,contenido){
    console.log("https://postalunal.unal.edu.co:8780/v1/campus/updateIslaContent?ID_ISLA="+this.islaID+"&CONTENIDO="+contenido+"&ORDEN="+(element+1)+"&MOSTRAR="+this.translateBoolean(this.selectedIsla.mostrar[element]));
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/updateIslaContent?ID_ISLA="+this.islaID+"&CONTENIDO="+contenido+"&ORDEN="+(element+1)+"&MOSTRAR="+this.translateBoolean(this.selectedIsla.mostrar[element]),{}).subscribe(response => {

      }
    )
  }

  open(title,message){
    let dialogRef = this.dialog.open(ModalComponent, {
      data: {title:title, message:message},
      disableClose: false
    }).afterClosed().subscribe(response=> {
      $("#editEmployeeModal").modal("hide");
    });
  }


  ngOnInit() {
    this.locStorage.setmapStatus(0);
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
      response =>{
        //@ts-ignore
        this.listaPreguntas = response;
        //console.log("THIS IS PREGUTNAS LIST: "+response)
      }
    )


    //GENERATE ISLA CONTENT
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/getIslaData",{}).subscribe(response => {
        this.islaData = response;
        this.selectedIsla = response[0];
      }
    )


    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getUsers").subscribe(
      response =>{
        //@ts-ignore
        this.listaUsers = response;
        //console.log("THIS IS USER LIST: "+response)
      }
    )


    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getCategorias").subscribe(
      response =>{
        //@ts-ignore
        this.listaCategorias = response;
        this.idCategoriaEdit = response[0].id_categoria;
        //console.log("THIS IS PREGUTNAS LIST: "+response)
      }
    )

    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3,4").subscribe(
      response =>{
        //@ts-ignore
        console.log(response)
        this.listaPublicacionesPadre = response
        this.listaPublicacionesPadreCopy = response
        this.caseFilter = 0
        //console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
      }
    )

  }



  //imagen 1
  fileBase64String = null;
  fileExtension = null;
  urlUploaded = "Sin Archivo";
  imgName = null;
  imgURL = null;
  size = null;

  //imagen 2
  fileBase64String2 = null;
  fileExtension2 = null;
  urlUploaded2 = "Sin Archivo";
  imgName2 = null;
  imgURL2 = null;
  size2 = null;

  //imagen 3
  fileBase64String3 = null;
  fileExtension3 = null;
  urlUploaded3 = "Sin Archivo";
  imgName3 = null;
  imgURL3 = null;
  size3 = null;

  //imagen 4
  fileBase64String4 = null;
  fileExtension4 = null;
  urlUploaded4 = "Sin Archivo";
  imgName4 = null;
  imgURL4 = null;
  size4 = null;

  //imagen 5
  fileBase64String5 = null;
  fileExtension5 = null;
  urlUploaded5 = "Sin Archivo";
  imgName5 = null;
  imgURL5 = null;
  size5 = null;

  //imagen 6
  fileBase64String6 = null;
  fileExtension6 = null;
  urlUploaded6 = "Sin Archivo";
  imgName6 = null;
  imgURL6 = null;
  size6 = null;

  //imagen BG
  fileBase64StringBG = null;
  fileExtensionBG = null;
  urlUploadedBG = "Sin Archivo";
  imgNameBG = null;
  imgURLBG = null;
  sizeBG = null;


  setFileImage(event) {
    console.log("entre")
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

        this.size = Math.round((event.srcElement.files[0].size/1024/1024) * 100) / 100;



        // const img = new Image();
        // img.src = reader.result as string;
        // img.onload = () => {
        //   this.imgHeight = img.naturalHeight;
        //   this.imgWidth = img.naturalWidth;
        //   console.log('Width and Height', this.imgWidth, this.imgHeight);
        //   if(this.imgHeight > 100 || this.imgWidth > 100){
        //     this.open("Las Dimensiones de la imagen son mayores al limite permitido.","");
        //     this.clearImg();
        //     return;
        //   }
        // };
        //console.log(reader.result);
        //console.log("EXTENSION: ", event.target.files[0].name.split(".").pop());
        this.fileBase64String = reader.result;
        this.fileExtension = event.target.files[0].name.split(".").pop();
        this.urlUploaded = event.target.files[0].name;
        //console.log("FILE NAME: ",event.target.files[0].name);
        this.imgName = event.target.files[0].name;
        this.imgURL = reader.result;
        if(this.fileExtension == "jpg" || this.fileExtension == "jpeg" || this.fileExtension == "png"){
            console.log(this.imgName,+"-",+this.imgURL+"-"+this.urlUploaded+"-"+this.fileExtension);
        }else{
          alert("Formato de imagen no valido, los formatos permitidos son, JPG, JEPG y PNG.");
          this.clearImg();
          return;
        }
    };
  }

  setFileImage2(event) {
    console.log("entre")
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

        this.size2 = Math.round((event.srcElement.files[0].size/1024/1024) * 100) / 100;



        // const img = new Image();
        // img.src = reader.result as string;
        // img.onload = () => {
        //   this.imgHeight = img.naturalHeight;
        //   this.imgWidth = img.naturalWidth;
        //   console.log('Width and Height', this.imgWidth, this.imgHeight);
        //   if(this.imgHeight > 100 || this.imgWidth > 100){
        //     this.open("Las Dimensiones de la imagen son mayores al limite permitido.","");
        //     this.clearImg();
        //     return;
        //   }
        // };
        //console.log(reader.result);
        //console.log("EXTENSION: ", event.target.files[0].name.split(".").pop());
        this.fileBase64String2 = reader.result;
        this.fileExtension2 = event.target.files[0].name.split(".").pop();
        this.urlUploaded2 = event.target.files[0].name;
        //console.log("FILE NAME: ",event.target.files[0].name);
        this.imgName2 = event.target.files[0].name;
        this.imgURL2 = reader.result;
        if(this.fileExtension2 == "jpg" || this.fileExtension2 == "jpeg" || this.fileExtension2 == "png"){
            console.log(this.imgName2,+"-",+this.imgURL2+"-"+this.urlUploaded2+"-"+this.fileExtension2);
        }else{
          alert("Formato de imagen no valido, los formatos permitidos son, JPG, JEPG y PNG.");
          this.clearImg2();
          return;
        }
    };
  }


  setFileImage3(event) {
    console.log("entre")
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

        this.size3 = Math.round((event.srcElement.files[0].size/1024/1024) * 100) / 100;



        // const img = new Image();
        // img.src = reader.result as string;
        // img.onload = () => {
        //   this.imgHeight = img.naturalHeight;
        //   this.imgWidth = img.naturalWidth;
        //   console.log('Width and Height', this.imgWidth, this.imgHeight);
        //   if(this.imgHeight > 100 || this.imgWidth > 100){
        //     this.open("Las Dimensiones de la imagen son mayores al limite permitido.","");
        //     this.clearImg();
        //     return;
        //   }
        // };
        //console.log(reader.result);
        //console.log("EXTENSION: ", event.target.files[0].name.split(".").pop());
        this.fileBase64String3 = reader.result;
        this.fileExtension3 = event.target.files[0].name.split(".").pop();
        this.urlUploaded3 = event.target.files[0].name;
        //console.log("FILE NAME: ",event.target.files[0].name);
        this.imgName3 = event.target.files[0].name;
        this.imgURL3 = reader.result;
        if(this.fileExtension3 == "jpg" || this.fileExtension3 == "jpeg" || this.fileExtension3 == "png"){
            console.log(this.imgName3,+"-",+this.imgURL3+"-"+this.urlUploaded3+"-"+this.fileExtension3);
        }else{
          alert("Formato de imagen no valido, los formatos permitidos son, JPG, JEPG y PNG.");
          this.clearImg3();
          return;
        }
    };
  }


  setFileImage4(event) {
    console.log("entre")
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

        this.size4 = Math.round((event.srcElement.files[0].size/1024/1024) * 100) / 100;



        // const img = new Image();
        // img.src = reader.result as string;
        // img.onload = () => {
        //   this.imgHeight = img.naturalHeight;
        //   this.imgWidth = img.naturalWidth;
        //   console.log('Width and Height', this.imgWidth, this.imgHeight);
        //   if(this.imgHeight > 100 || this.imgWidth > 100){
        //     this.open("Las Dimensiones de la imagen son mayores al limite permitido.","");
        //     this.clearImg();
        //     return;
        //   }
        // };
        //console.log(reader.result);
        //console.log("EXTENSION: ", event.target.files[0].name.split(".").pop());
        this.fileBase64String4 = reader.result;
        this.fileExtension4 = event.target.files[0].name.split(".").pop();
        this.urlUploaded4 = event.target.files[0].name;
        //console.log("FILE NAME: ",event.target.files[0].name);
        this.imgName4 = event.target.files[0].name;
        this.imgURL4 = reader.result;
        if(this.fileExtension4 == "jpg" || this.fileExtension4 == "jpeg" || this.fileExtension4 == "png"){
            console.log(this.imgName4,+"-",+this.imgURL4+"-"+this.urlUploaded4+"-"+this.fileExtension4);
        }else{
          alert("Formato de imagen no valido, los formatos permitidos son, JPG, JEPG y PNG.");
          this.clearImg4();
          return;
        }
    };
  }


  setFileImage5(event) {
    console.log("entre")
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

        this.size5 = Math.round((event.srcElement.files[0].size/1024/1024) * 100) / 100;



        // const img = new Image();
        // img.src = reader.result as string;
        // img.onload = () => {
        //   this.imgHeight = img.naturalHeight;
        //   this.imgWidth = img.naturalWidth;
        //   console.log('Width and Height', this.imgWidth, this.imgHeight);
        //   if(this.imgHeight > 100 || this.imgWidth > 100){
        //     this.open("Las Dimensiones de la imagen son mayores al limite permitido.","");
        //     this.clearImg();
        //     return;
        //   }
        // };
        //console.log(reader.result);
        //console.log("EXTENSION: ", event.target.files[0].name.split(".").pop());
        this.fileBase64String5 = reader.result;
        this.fileExtension5 = event.target.files[0].name.split(".").pop();
        this.urlUploaded5 = event.target.files[0].name;
        //console.log("FILE NAME: ",event.target.files[0].name);
        this.imgName5 = event.target.files[0].name;
        this.imgURL5 = reader.result;
        if(this.fileExtension5 == "jpg" || this.fileExtension5 == "jpeg" || this.fileExtension5 == "png"){
            console.log(this.imgName5,+"-",+this.imgURL5+"-"+this.urlUploaded5+"-"+this.fileExtension5);
        }else{
          alert("Formato de imagen no valido, los formatos permitidos son, JPG, JEPG y PNG.");
          this.clearImg5();
          return;
        }
    };
  }



  setFileImage6(event) {
    console.log("entre")
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

        this.size6 = Math.round((event.srcElement.files[0].size/1024/1024) * 100) / 100;



        // const img = new Image();
        // img.src = reader.result as string;
        // img.onload = () => {
        //   this.imgHeight = img.naturalHeight;
        //   this.imgWidth = img.naturalWidth;
        //   console.log('Width and Height', this.imgWidth, this.imgHeight);
        //   if(this.imgHeight > 100 || this.imgWidth > 100){
        //     this.open("Las Dimensiones de la imagen son mayores al limite permitido.","");
        //     this.clearImg();
        //     return;
        //   }
        // };
        //console.log(reader.result);
        //console.log("EXTENSION: ", event.target.files[0].name.split(".").pop());
        this.fileBase64String6 = reader.result;
        this.fileExtension6 = event.target.files[0].name.split(".").pop();
        this.urlUploaded6 = event.target.files[0].name;
        //console.log("FILE NAME: ",event.target.files[0].name);
        this.imgName6 = event.target.files[0].name;
        this.imgURL6 = reader.result;
        if(this.fileExtension6 == "jpg" || this.fileExtension6 == "jpeg" || this.fileExtension6 == "png"){
            console.log(this.imgName6,+"-",+this.imgURL+"-"+this.urlUploaded6+"-"+this.fileExtension6);
        }else{
          alert("Formato de imagen no valido, los formatos permitidos son, JPG, JEPG y PNG.");
          this.clearImg6();
          return;
        }
    };
  }


  setFileImageBG(event) {
    console.log("entre")
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

        this.sizeBG = Math.round((event.srcElement.files[0].size/1024/1024) * 100) / 100;



        // const img = new Image();
        // img.src = reader.result as string;
        // img.onload = () => {
        //   this.imgHeight = img.naturalHeight;
        //   this.imgWidth = img.naturalWidth;
        //   console.log('Width and Height', this.imgWidth, this.imgHeight);
        //   if(this.imgHeight > 100 || this.imgWidth > 100){
        //     this.open("Las Dimensiones de la imagen son mayores al limite permitido.","");
        //     this.clearImg();
        //     return;
        //   }
        // };
        //console.log(reader.result);
        //console.log("EXTENSION: ", event.target.files[0].name.split(".").pop());
        this.fileBase64StringBG = reader.result;
        this.fileExtensionBG = event.target.files[0].name.split(".").pop();
        this.urlUploadedBG = event.target.files[0].name;
        //console.log("FILE NAME: ",event.target.files[0].name);
        this.imgNameBG = event.target.files[0].name;
        this.imgURLBG = reader.result;
        if(this.fileExtensionBG == "jpg" || this.fileExtensionBG == "jpeg" || this.fileExtensionBG == "png"){
            console.log(this.imgNameBG+"-"+this.imgURLBG+"-"+this.urlUploadedBG+"-"+this.fileExtensionBG);
        }else{
          alert("Formato de imagen no valido, los formatos permitidos son, JPG, JEPG y PNG.");
          this.clearImgBG();
          return;
        }
    };
  }


  clearImg(){
    this.fileBase64String = null;
    this.fileExtension = null;
    this.urlUploaded = "Sin Archivo";
    this.imgName = null;
    this.imgURL = null;
    this.size = null;
    this.InputVar.nativeElement.value = "";
  }

  clearImg2(){
    this.fileBase64String2 = null;
    this.fileExtension2 = null;
    this.urlUploaded2 = "Sin Archivo";
    this.imgName2 = null;
    this.imgURL2 = null;
    this.size2 = null;
    this.InputVar2.nativeElement.value = "";
  }

  clearImg3(){
    this.fileBase64String3 = null;
    this.fileExtension3 = null;
    this.urlUploaded3 = "Sin Archivo";
    this.imgName3 = null;
    this.imgURL3 = null;
    this.size3 = null;
    this.InputVar3.nativeElement.value = "";
  }


  clearImg4(){
    this.fileBase64String4 = null;
    this.fileExtension4 = null;
    this.urlUploaded4 = "Sin Archivo";
    this.imgName4 = null;
    this.imgURL4 = null;
    this.size4 = null;
    this.InputVar4.nativeElement.value = "";
  }


  clearImg5(){
    this.fileBase64String5 = null;
    this.fileExtension5 = null;
    this.urlUploaded5 = "Sin Archivo";
    this.imgName5 = null;
    this.imgURL5 = null;
    this.size5 = null;
    this.InputVar5.nativeElement.value = "";
  }

  clearImg6(){
    this.fileBase64String6 = null;
    this.fileExtension6 = null;
    this.urlUploaded6 = "Sin Archivo";
    this.imgName6 = null;
    this.imgURL6 = null;
    this.size6 = null;
    this.InputVar6.nativeElement.value = "";
  }

  clearImgBG(){
    this.fileBase64StringBG = null;
    this.fileExtensionBG = null;
    this.urlUploadedBG = "Sin Archivo";
    this.imgNameBG = null;
    this.imgURLBG = null;
    this.sizeBG = null;
    this.InputVarBG.nativeElement.value = "";
  }

  isla = "";
  imgNumber = 0;
  updateWithImage(order,contenido){
    if(this.islaID==1){
      this.isla = "cultura";
      this.imgNumber = order - 18;
    }
    if(this.islaID==2){
      this.isla = "deporte";
      this.imgNumber = order - 18;
    }
    if(this.islaID==3){
      this.isla = "salud";
      this.imgNumber = order - 18;
    }
    if(this.islaID==4){
      this.isla = "naturaleza";
      this.imgNumber = order - 18;
    }
    if(this.islaID==5){
      this.isla = "conocimiento";
      this.imgNumber = order - 18;
    }
    if(this.islaID==6){
      this.isla = "espacios";
      this.imgNumber = order - 18;
    }
    this.updateIslaElement(order,contenido)
    console.log({file: this.fileBase64String, fileName: this.isla+this.imgNumber+"."+this.fileExtension, format: this.fileExtension})
    if(this.imgURL != null){
      this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/imagenIsla",{file: this.fileBase64String, fileName: this.isla+this.imgNumber+"."+this.fileExtension, format: this.fileExtension}).subscribe(
        response =>{
          this.clearImg()
        }
      )
    }
  }



  updateWithImage2(order,contenido){
    if(this.islaID==1){
      this.isla = "cultura";
      this.imgNumber = order - 18;
    }
    if(this.islaID==2){
      this.isla = "deporte";
      this.imgNumber = order - 18;
    }
    if(this.islaID==3){
      this.isla = "salud";
      this.imgNumber = order - 18;
    }
    if(this.islaID==4){
      this.isla = "naturaleza";
      this.imgNumber = order - 18;
    }
    if(this.islaID==5){
      this.isla = "conocimiento";
      this.imgNumber = order - 18;
    }
    if(this.islaID==6){
      this.isla = "espacios";
      this.imgNumber = order - 18;
    }
    this.updateIslaElement(order,contenido)
    console.log({file: this.fileBase64String2, fileName: this.isla+this.imgNumber+"."+this.fileExtension2, format: this.fileExtension2})
    if(this.imgURL2 != null){
      console.log("updated!")
      this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/imagenIsla",{file: this.fileBase64String2, fileName: this.isla+this.imgNumber+"."+this.fileExtension2, format: this.fileExtension2}).subscribe(
        response =>{
          this.clearImg2()
        }
      )
    }
  }


  updateWithImageBG(order,contenido){
    if(this.islaID==1){
      this.isla = "cultura";
      this.imgNumber = order - 18;
    }
    if(this.islaID==2){
      this.isla = "deporte";
      this.imgNumber = order - 18;
    }
    if(this.islaID==3){
      this.isla = "salud";
      this.imgNumber = order - 18;
    }
    if(this.islaID==4){
      this.isla = "naturaleza";
      this.imgNumber = order - 18;
    }
    if(this.islaID==5){
      this.isla = "conocimiento";
      this.imgNumber = order - 18;
    }
    if(this.islaID==6){
      this.isla = "espacios";
      this.imgNumber = order - 18;
    }
    this.updateIslaElement(order,contenido)
    console.log({file: this.fileBase64StringBG, fileName: this.isla+"."+this.fileExtensionBG, format: this.fileExtensionBG})
    if(this.imgURLBG != null){
      console.log("updated!")
      this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/imagenIsla",{file: this.fileBase64StringBG, fileName: this.isla+"."+this.fileExtensionBG, format: this.fileExtensionBG}).subscribe(
        response =>{
          this.clearImgBG()
        }
      )
    }
  }

  updateWithImage3(order,contenido){
    if(this.islaID==1){
      this.isla = "cultura";
      this.imgNumber = order - 18;
    }
    if(this.islaID==2){
      this.isla = "deporte";
      this.imgNumber = order - 18;
    }
    if(this.islaID==3){
      this.isla = "salud";
      this.imgNumber = order - 18;
    }
    if(this.islaID==4){
      this.isla = "naturaleza";
      this.imgNumber = order - 18;
    }
    if(this.islaID==5){
      this.isla = "conocimiento";
      this.imgNumber = order - 18;
    }
    if(this.islaID==6){
      this.isla = "espacios";
      this.imgNumber = order - 18;
    }
    this.updateIslaElement(order,contenido)
    if(this.imgURL3 != null){
      this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/imagenIsla",{file: this.fileBase64String3, fileName: this.isla+this.imgNumber+"."+this.fileExtension3, format: this.fileExtension3}).subscribe(
        response =>{
          this.clearImg3()
        }
      )
    }
  }


  updateWithImage4(order,contenido){
    if(this.islaID==1){
      this.isla = "cultura";
      this.imgNumber = order - 18;
    }
    if(this.islaID==2){
      this.isla = "deporte";
      this.imgNumber = order - 18;
    }
    if(this.islaID==3){
      this.isla = "salud";
      this.imgNumber = order - 18;
    }
    if(this.islaID==4){
      this.isla = "naturaleza";
      this.imgNumber = order - 18;
    }
    if(this.islaID==5){
      this.isla = "conocimiento";
      this.imgNumber = order - 18;
    }
    if(this.islaID==6){
      this.isla = "espacios";
      this.imgNumber = order - 18;
    }
    this.updateIslaElement(order,contenido)
    if(this.imgURL4 != null){
      this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/imagenIsla",{file: this.fileBase64String4, fileName: this.isla+this.imgNumber+"."+this.fileExtension4, format: this.fileExtension4}).subscribe(
        response =>{
          this.clearImg4()
        }
      )
    }
  }



  updateWithImage5(order,contenido){
    if(this.islaID==1){
      this.isla = "cultura";
      this.imgNumber = order - 18;
    }
    if(this.islaID==2){
      this.isla = "deporte";
      this.imgNumber = order - 18;
    }
    if(this.islaID==3){
      this.isla = "salud";
      this.imgNumber = order - 18;
    }
    if(this.islaID==4){
      this.isla = "naturaleza";
      this.imgNumber = order - 18;
    }
    if(this.islaID==5){
      this.isla = "conocimiento";
      this.imgNumber = order - 18;
    }
    if(this.islaID==6){
      this.isla = "espacios";
      this.imgNumber = order - 18;
    }
    this.updateIslaElement(order,contenido)
    if(this.imgURL5 != null){
      this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/imagenIsla",{file: this.fileBase64String5, fileName: this.isla+this.imgNumber+"."+this.fileExtension5, format: this.fileExtension5}).subscribe(
        response =>{
          this.clearImg5()
        }
      )
    }
  }


  updateWithImage6(order,contenido){
    if(this.islaID==1){
      this.isla = "cultura";
      this.imgNumber = order - 18;
    }
    if(this.islaID==2){
      this.isla = "deporte";
      this.imgNumber = order - 18;
    }
    if(this.islaID==3){
      this.isla = "salud";
      this.imgNumber = order - 18;
    }
    if(this.islaID==4){
      this.isla = "naturaleza";
      this.imgNumber = order - 18;
    }
    if(this.islaID==5){
      this.isla = "conocimiento";
      this.imgNumber = order - 18;
    }
    if(this.islaID==6){
      this.isla = "espacios";
      this.imgNumber = order - 18;
    }
    this.updateIslaElement(order,contenido)
    if(this.imgURL6 != null){
      this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/imagenIsla",{file: this.fileBase64String6, fileName: this.isla+this.imgNumber+"."+this.fileExtension6, format: this.fileExtension6}).subscribe(
        response =>{
          this.clearImg6()
        }
      )
    }
  }



  idPosicion = 0;

  generateMapCoordinates2(distancia,id_pregunta){

    let x = 0;
    let y = 0;

    let listPositions;

    //HERE
    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicationPosicionIdPregunta?ID_PREGUNTA="+id_pregunta).subscribe(element => {
      listPositions = element;


    //console.log("POSITION 0: "+listPositions[0]);

    let subList;

     if(distancia <= 4){

      subList = listPositions.slice(0,6);

      let emptyPositions = [];

      for(let i = 0; i<subList.length ;i++){
        let elementNow = subList[i];
        if(elementNow.id_PUBLICACION==0){
          emptyPositions.push(elementNow);
        }
      }


      //console.log("length: "+emptyPositions.length)

      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }


     };



     if(distancia >4 && distancia<=15){

      subList = listPositions.slice(6,15);

      let emptyPositions = [];

      for(let i = 0; i<subList.length ;i++){
        let elementNow = subList[i];
        if(elementNow.id_PUBLICACION==0){
          emptyPositions.push(elementNow);
        }
      }

      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }


     };


      if(distancia >15 && distancia<=100){

        subList = listPositions.slice(15,31);

        let emptyPositions = [];

        for(let i = 0; i<subList.length ;i++){
          let elementNow = subList[i];
          if(elementNow.id_PUBLICACION==0){
            emptyPositions.push(elementNow);
          }
        }


      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }

      };


      if(distancia >100 && distancia<=300){

        subList = listPositions.slice(31,57);

        let emptyPositions = [];

        for(let i = 0; i<subList.length ;i++){
          let elementNow = subList[i];
          if(elementNow.id_PUBLICACION==0){
            emptyPositions.push(elementNow);
          }
        }


      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }


      };


     if(distancia >300 && distancia<=1000){

      subList = listPositions.slice(57,88);

      let emptyPositions = [];

      for(let i = 0; i<subList.length ;i++){
        let elementNow = subList[i];
        if(elementNow.id_PUBLICACION==0){
          emptyPositions.push(elementNow);
        }
      }


      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }

     };



    if(distancia >1000){

      subList = listPositions.slice(88,127);

      let emptyPositions = [];

      for(let i = 0; i<subList.length ;i++){
        let elementNow = subList[i];
        if(elementNow.id_PUBLICACION==0){
          emptyPositions.push(elementNow);
        }
      }


      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }
    };

    })




    return {y : y, x :x}
  }


  //Paginator Publicaciones

  page = 1;
  count = 0;
  tableSize = 20;

  onTableDataChange(event){
    this.page = event;
  }

  async saveAll(){
    try{
      await this.updateIslaElement(0,this.selectedIsla.titulo);
      await this.updateIslaElement(1,this.selectedIsla.url);
      await this.updateIslaElement(2,this.selectedIsla.descripcion);
      await this.updateIslaElement(3,'Imagenes');
      await this.updateWithImage(19,'url');
      await this.updateWithImage2(20,'url');
      await this.updateWithImage3(21,'url');
      await this.updateWithImage4(22,'url');
      await this.updateWithImage5(23,'url');
      await this.updateWithImage6(24,'url');
      await this.updateWithImageBG(29,'BG;url');
      await this.updateIslaElement(4,this.selectedIsla.subtitulo1);
      await this.updateIslaElement(5,this.selectedIsla.info1);
      await this.updateIslaElement(6,this.selectedIsla.info2);
      await this.updateIslaElement(7,this.selectedIsla.info3);
      await this.updateIslaElement(8,this.selectedIsla.info4);
      await this.updateIslaElement(9,this.selectedIsla.info5);
      await this.updateIslaElement(10,this.selectedIsla.info6);
      await this.updateIslaElement(11,this.selectedIsla.subtitulo2);
      await this.updateIslaElement(12,'redes');
      await this.updateIslaElement(25,this.selectedIsla.facebook);
      await this.updateIslaElement(26,this.selectedIsla.instagram);
      await this.updateIslaElement(27,this.selectedIsla.youtube);
      await this.updateIslaElement(28,this.selectedIsla.twitter);
      await this.updateIslaElement(13,this.selectedIsla.contactenos);
      await this.updateIslaElement(14,this.selectedIsla.correos);
      await this.updateIslaElement(15,this.UrlSitioWeb.join(','));
      await this.updateIslaElement(16,this.selectedIsla.tituloProgramacion);
      await this.updateIslaElement(17,this.selectedIsla.programacion1);
      await this.updateIslaElement(18,this.selectedIsla.programacion2);
      let dialogRef = await this.dialog.open(ModalComponent, {
        data: {title:"Se guardaron tus cambios exitosamente.", message: " "},
        disableClose: false
      }).afterClosed().subscribe(response=> {
        $("#editEmployeeModal").modal("hide");
        this.realoadIslas();
      });
    }catch(Exception){
      let dialogRef = await this.dialog.open(ModalComponent, {
        data: {title:"Se encontro un problema al intentar guardar tus cambios.", message: " "},
        disableClose: false
      }).afterClosed().subscribe(response=> {
        $("#editEmployeeModal").modal("hide");
        this.realoadIslas();
      });
    }

  }

  getAmountShown(){

    let max = Math.ceil( this.listaPublicacionesPadre.length/20);
    if(this.page == max){
      return this.listaPublicacionesPadre.length%20;
    }else{
      return 20;
    }

  }
  //Paginator publicaciones




  //Paginator preguntas

  page2 = 1;
  count2 = 0;
  tableSize2 = 20;

  onTableDataChange2(event){
    this.page2 = event;
  }

  getAmountShown2(){

    let max = Math.ceil( this.listaPreguntas.length/20);
    if(this.page2 == max){
      return this.listaPreguntas.length%20;
    }else{
      return 20;
    }

  }
  //Paginator preguntas



   //Paginator preguntas

   page3 = 1;
   count3 = 0;
   tableSize3 = 20;

   onTableDataChange3(event){
     this.page3 = event;
   }

   getAmountShown3(){

     let max = Math.ceil( this.listaUsers.length/20);
     if(this.page3 == max){
       return this.listaUsers.length%20;
     }else{
       return 20;
     }

   }
   //Paginator users


  idPreguntaEdit="";
  setPreguntaEditId(id){
    this.idPreguntaEdit = id;
    //console.log(this.idPreguntaEdit);
  }


  actualizarPregunta(){
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/modificarPregunta?pregunta="+this.preguntaNombre+"&id_categoria="+this.idCategoriaEdit+"&id_pregunta="+this.idPreguntaEdit,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Pregunta editada con exito.", message:" "},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            $("#editEmployeeModal").modal("hide");
          });
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
            response =>{
              //@ts-ignore
              this.listaPreguntas = response

            }
          )
        }
      }
    )
  }


  pregunta = "";
  id_categoria = "";

  postPregunta(){
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/crearPregunta?pregunta="+this.pregunta+"&id_categoria="+this.idCategoria,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Pregunta creada con exito.", message:" "},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            $("#addTemaModal").modal('hide');
          });
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
            response =>{
              //@ts-ignore
              this.listaPreguntas = response
            }
          )
        }
      }
    )
  }


  setAll(){
    this.allSelected = !this.allSelected;
    let value = false;
    if(this.allSelected){
      value = true;
    }
    for(let i = 0; i<this.listaPublicacionesPadre.length; i++){
      this.listaPublicacionesPadre[i].check=value
    }
  }


  deletePreguntas(){
    let listSize = this.listaPreguntas.length;
    let listString = ""

    //console.log(listSize);

    for(let i = 0;i<listSize;i++){
      if(this.listaPreguntas[i].check == true){
        listString += this.listaPreguntas[i].id_pregunta+",";
      }
    }

    //console.log("https://postalunal.unal.edu.co:8780/v1/campus/eliminarPreguntasCheck?list="+listString.substring(0,listString.length-1));

    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/eliminarPreguntasCheck?list="+listString.substring(0,listString.length-1),{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Preguntas eliminadas con exito.", message:" "},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            $("#deleteEmployeeModal").modal('hide');

            this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaPreguntas = response

                }
              )
          });
        }else{
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Fallo!", message:"Se presento un error al eliminar las preguntas."},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
        }
      })


  }


  deleteUsuarios(){
    let listSize = this.listaUsers.length;
    let listString = ""

    //console.log(listSize);

    for(let i = 0;i<listSize;i++){
      if(this.listaUsers[i].check == true){
        listString += this.listaUsers[i].username+",";
        //console.log("IM IN: ", listString);
      }
    }

    //console.log("https://postalunal.unal.edu.co:8780/v1/campus/eliminarUsersCheck?list="+listString.substring(0,listString.length-1));

    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/eliminarUsersCheck?list="+listString.substring(0,listString.length-1),{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Usuarios eliminados con exito.", message:" "},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            $("#deleteEmployeeModal2").modal('hide');

            this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getUsers").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaUsers = response

                }
              )
          });
        }else{
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Fallo!", message:"Se presento un error al eliminar los administradores."},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
        }
      })


  }



  setAll2(){
    this.allSelected2 = !this.allSelected2;
    let value = false;
    if(this.allSelected2){
      value = true;
    }
    for(let i = 0; i<this.listaPreguntas.length; i++){
      this.listaPreguntas[i].check=value
    }
  }


  setAll3(){
    this.allSelected3 = !this.allSelected3;
    let value = false;
    if(this.allSelected3){
      value = true;
    }
    for(let i = 0; i<this.listaUsers.length; i++){
      this.listaUsers[i].check=value
    }
  }

  goToMap(){
    this.router.navigate(['map']);
  }


  caseFilter = 0;
  filterTable(){


    this.idPregunta = -1;

    if(this.caseFilter == 0){
      this.listaPublicacionesPadre = this.listaPublicacionesPadreCopy;
    }

    if(this.caseFilter == 1){
      this.listaPublicacionesPadre = this.listaPublicacionesPadreCopy.filter(e => e.id_ESTADO_PUBLICACION == 1);
    }

    if(this.caseFilter == 2){
      this.listaPublicacionesPadre = this.listaPublicacionesPadreCopy.filter(e => e.id_ESTADO_PUBLICACION == 2);
    }

    if(this.caseFilter == 3){
      this.listaPublicacionesPadre = this.listaPublicacionesPadreCopy.filter(e => e.id_ESTADO_PUBLICACION == 3);
    }

    if(this.caseFilter == 4){
      this.listaPublicacionesPadre = this.listaPublicacionesPadreCopy.filter(e => e.id_ESTADO_PUBLICACION == 4);
    }

  }


  categoria = "";

  postCategoria(){
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/crearCategoria?categoria="+this.categoria,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Categoria creada con exito.", message:" "},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            $("#addCategoria").modal("hide");
          });
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getCategorias").subscribe(
            response =>{
              //@ts-ignore
              this.listaCategorias = response
            }
          )
        }
      }
    )
  }

  name = "";
  username = "";

  postUser(){
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/crear_administrador?user="+this.username+"&nombre="+this.name,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Administrador registrado con exito.", message:" "},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            $("#addAdminModal").modal("hide");
          });
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getUsers").subscribe(
            response =>{
              //@ts-ignore
              this.listaUsers = response
            }
          )
        }
      }
    )
  }






  deletePregunta(id){
        //**SE CARGA LA LISTA DE PREGUNTAS */
        this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/eliminarPregunta?id_pregunta="+id,{}).subscribe(
          response =>{
            if(response == 1){
              let dialogRef = this.dialog.open(ModalComponent, {
                data: {title:"Pregunta Eliminada con exito.", message:" "},
                disableClose: false
              }).afterClosed().subscribe(response=> {
              });
              this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaPreguntas = response
                }
              )
            }
          }
        )
  }



  deleteUser(id){
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/eliminar_administrador?user="+id,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Administrador Eliminado con exito.", message:" "},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getUsers").subscribe(
            response =>{
              //@ts-ignore
              this.listaUsers = response
            }
          )
        }
      }
    )
}



  // acceptAll(){
  //   let listSize = this.listaPublicacionesPadre.length;
  //   let listString = ""

  //   //console.log(listSize);

  //   for(let i = 0;i<listSize;i++){
  //     if(this.listaPublicacionesPadre[i].check == true){
  //       listString += this.listaPublicacionesPadre[i].id_PUBLICACION+",";
  //     }
  //   }

  //   //console.log("https://postalunal.unal.edu.co:8780/v1/campus/changeStatePublicaciones?list="+listString.substring(0,listSize-1)+"&state="+2);

  //   this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/changeStatePublicaciones?list="+listString.substring(0,listString.length-1)+"&state="+2,{}).subscribe(
  //     response =>{
  //       if(response == 1){
  //         let dialogRef = this.dialog.open(ModalComponent, {
  //           data: {title:"Publicaciones aceptadas con exito.", message:" "},
  //           disableClose: false
  //         }).afterClosed().subscribe(response=> {
  //           this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3").subscribe(
  //               response =>{
  //                 //@ts-ignore
  //                 this.listaPublicacionesPadre = response

  //                 this.listaPublicacionesPadreCopy = response
  //                 this.caseFilter = 0;
  //               }
  //             )
  //         });
  //       }else{
  //         let dialogRef = this.dialog.open(ModalComponent, {
  //           data: {title:"Fallo!", message:"Se presento un error al aceptar las publicaciones."},
  //           disableClose: false
  //         }).afterClosed().subscribe(response=> {
  //         });
  //       }
  //     })
  // }


  acceptAll(){
    let listSize = this.listaPublicacionesPadre.length;

    //console.log(listSize);

    for(let i = 0;i<listSize;i++){
      if(this.listaPublicacionesPadre[i].check == true){
        this.aceptPublication(this.listaPublicacionesPadre[i],false);
        //console.log(i)
      }
    }

    //console.log("OUT OF FOR")

    //console.log("https://postalunal.unal.edu.co:8780/v1/campus/changeStatePublicaciones?list="+listString.substring(0,listSize-1)+"&state="+2);

    let dialogRef = this.dialog.open(ModalComponent, {
      data: {title:"Publicaciones aceptadas con exito.", message:" "},
      disableClose: false
    }).afterClosed().subscribe(response=> {
      this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3,4").subscribe(
          response =>{
            //@ts-ignore
            this.listaPublicacionesPadre = response

            this.listaPublicacionesPadreCopy = response
            this.caseFilter = 0;
          }
        )
    });

  }



  // rejectAll(){
  //   let listSize = this.listaPublicacionesPadre.length;
  //   let listString = ""

  //   //console.log(listSize);

  //   for(let i = 0;i<listSize;i++){
  //     if(this.listaPublicacionesPadre[i].check == true){
  //       listString += this.listaPublicacionesPadre[i].id_PUBLICACION+",";
  //     }
  //   }

  //   //console.log("https://postalunal.unal.edu.co:8780/v1/campus/changeStatePublicaciones?list="+listString.substring(0,listString.length-1)+"&state="+3);

  //   this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/changeStatePublicaciones?list="+listString.substring(0,listString.length-1)+"&state="+3,{}).subscribe(
  //     response =>{
  //       if(response == 1){
  //         let dialogRef = this.dialog.open(ModalComponent, {
  //           data: {title:"Publicaciones Rechazadas con exito.", message:" "},
  //           disableClose: false
  //         }).afterClosed().subscribe(response=> {
  //           this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3").subscribe(
  //               response =>{
  //                 //@ts-ignore
  //                 this.listaPublicacionesPadre = response
  //                 this.listaPublicacionesPadreCopy = response
  //                 this.caseFilter = 0;
  //               }
  //             )
  //         });
  //       }else{
  //         let dialogRef = this.dialog.open(ModalComponent, {
  //           data: {title:"Fallo!", message:"Se presento un error al rechazar las publicaciones."},
  //           disableClose: false
  //         }).afterClosed().subscribe(response=> {
  //         });
  //       }
  //     })
  // }


  rejectAll(){
    //console.log(listSize);
    console.log({listItem : this.listaPublicacionesPadre.filter(publicacion => publicacion.check == true ) })
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/rechazarTodas",{listItem : this.listaPublicacionesPadre.filter(publicacion => publicacion.check == true ) }).subscribe(
      response =>{
        if(response == 1){
          //console.log("OUT OF FOR")
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Publicaciones Rechazadas con exito.", message:" "},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3,4").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaPublicacionesPadre = response
                  this.listaPublicacionesPadreCopy = response
                  this.caseFilter = 0;
                }
              )
          });
        }else{
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Se presento un error al rechazar las publicaciones.", message:" "},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
        }
      }
    )

  }



  aceptAll(){
    //console.log(listSize);
    console.log({listItem : this.listaPublicacionesPadre.filter(publicacion => publicacion.check == true ) })
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/aceptarTodos",{listItem : this.listaPublicacionesPadre.filter(publicacion => publicacion.check == true ) }).subscribe(
      response =>{
        if(response == 1){
          //console.log("OUT OF FOR")
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Publicaciones aceptadas con exito.", message:" "},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3,4").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaPublicacionesPadre = response
                  this.listaPublicacionesPadreCopy = response
                  this.caseFilter = 0;
                }
              )
          });
        }else{
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Se presento un error al aceptar las publicaciones.", message:" "},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
        }
      }
    )

  }


  aceptPublication(item,modal){

    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/ACTUALIZAR_ESTADO_PUBLICACION?idpublicacion="+item.id_PUBLICACION+"&idestadopublicacion="+2,{}).subscribe(
          response =>{
            if(response == 1){
              if(modal){
                let dialogRef = this.dialog.open(ModalComponent, {
                  data: {title:"Publicacion aprobada con exito.", message:" "},
                  disableClose: false
                }).afterClosed().subscribe(response=> {
                });
              }
               this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/email?sender="+"somoscampus_bog"+"&recipient="+this.locStorage.getUsername()+"&message=Tu%20publicación%20se%20ha%20realizado%20exitosamente.%20Te%20invitamos%20a%20seguir%20participando%20en:%20postalunal.unal.edu.co",{}).subscribe(
                 response =>{
                   if(response == 1){
                     //console.log("EMAIL SENT")
                   }else{
                     //console.log("EMAIL NOT SENT")
                   }
                 }
              )

              //HERE
              //let coordinates = this.generateMapCoordinates2(item.distancia_KMS,item.id_PREGUNTA);

              if(item.id_PUBLICACION_PADRE!=0){
                if(modal){
                this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3,4").subscribe(
                  response =>{
                    //@ts-ignore
                    this.listaPublicacionesPadre = response
                    this.listaPublicacionesPadreCopy = response
                    this.caseFilter = 0;
                  }

                )}
              }else{
              let x = 0;
              let y = 0;

              let listPositions;

              //HERE
              this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicationPosicionIdPregunta?ID_PREGUNTA="+item.id_PREGUNTA).subscribe(element => {
                listPositions = element;


              //console.log("POSITION 0: "+listPositions[0]);

              let subList;

               if(item.distancia_KMS <= 4){

                subList = listPositions.slice(0,6);

                let emptyPositions = [];

                for(let i = 0; i<subList.length ;i++){
                  let elementNow = subList[i];
                  if(elementNow.id_PUBLICACION==0){
                    emptyPositions.push(elementNow);
                  }
                }


                //console.log("length: "+emptyPositions.length)

                if(emptyPositions.length>0){
                  //HAY POSICIONES VACIAS
                  x = emptyPositions[0].mapax;
                  y = emptyPositions[0].mapay;
                  //CAMBIAR PUBLICACION DE 0 A ID NUEVO
                  this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
                }else{
                  //NO HAY POSICIONES VACIAS
                  subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
                  x = subList[0].mapax;
                  y = subList[0].mapay;
                  //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
                  this.idPosicion = subList[0].id_PUBLICACION_POSICION;
                }


               };



               if(item.distancia_KMS >4 && item.distancia_KMS<=15){

                subList = listPositions.slice(6,15);

                let emptyPositions = [];

                for(let i = 0; i<subList.length ;i++){
                  let elementNow = subList[i];
                  if(elementNow.id_PUBLICACION==0){
                    emptyPositions.push(elementNow);
                  }
                }

                if(emptyPositions.length>0){
                  //HAY POSICIONES VACIAS
                  x = emptyPositions[0].mapax;
                  y = emptyPositions[0].mapay;
                  //CAMBIAR PUBLICACION DE 0 A ID NUEVO
                  this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
                }else{
                  //NO HAY POSICIONES VACIAS
                  subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
                  x = subList[0].mapax;
                  y = subList[0].mapay;
                  //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
                  this.idPosicion = subList[0].id_PUBLICACION_POSICION;
                }


               };


                if(item.distancia_KMS >15 && item.distancia_KMS<=100){

                  subList = listPositions.slice(15,31);

                  let emptyPositions = [];

                  for(let i = 0; i<subList.length ;i++){
                    let elementNow = subList[i];
                    if(elementNow.id_PUBLICACION==0){
                      emptyPositions.push(elementNow);
                    }
                  }


                if(emptyPositions.length>0){
                  //HAY POSICIONES VACIAS
                  x = emptyPositions[0].mapax;
                  y = emptyPositions[0].mapay;
                  //CAMBIAR PUBLICACION DE 0 A ID NUEVO
                  this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
                }else{
                  //NO HAY POSICIONES VACIAS
                  subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
                  x = subList[0].mapax;
                  y = subList[0].mapay;
                  //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
                  this.idPosicion = subList[0].id_PUBLICACION_POSICION;
                }

                };


                if(item.distancia_KMS >100 && item.distancia_KMS<=300){

                  subList = listPositions.slice(31,57);

                  let emptyPositions = [];

                  for(let i = 0; i<subList.length ;i++){
                    let elementNow = subList[i];
                    if(elementNow.id_PUBLICACION==0){
                      emptyPositions.push(elementNow);
                    }
                  }


                if(emptyPositions.length>0){
                  //HAY POSICIONES VACIAS
                  x = emptyPositions[0].mapax;
                  y = emptyPositions[0].mapay;
                  //CAMBIAR PUBLICACION DE 0 A ID NUEVO
                  this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
                }else{
                  //NO HAY POSICIONES VACIAS
                  subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
                  x = subList[0].mapax;
                  y = subList[0].mapay;
                  //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
                  this.idPosicion = subList[0].id_PUBLICACION_POSICION;
                }


                };


               if(item.distancia_KMS >300 && item.distancia_KMS<=1000){

                subList = listPositions.slice(57,88);

                let emptyPositions = [];

                for(let i = 0; i<subList.length ;i++){
                  let elementNow = subList[i];
                  if(elementNow.id_PUBLICACION==0){
                    emptyPositions.push(elementNow);
                  }
                }


                if(emptyPositions.length>0){
                  //HAY POSICIONES VACIAS
                  x = emptyPositions[0].mapax;
                  y = emptyPositions[0].mapay;
                  //CAMBIAR PUBLICACION DE 0 A ID NUEVO
                  this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
                }else{
                  //NO HAY POSICIONES VACIAS
                  subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
                  x = subList[0].mapax;
                  y = subList[0].mapay;
                  //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
                  this.idPosicion = subList[0].id_PUBLICACION_POSICION;
                }

               };



              if(item.distancia_KMS >1000){

                subList = listPositions.slice(88,127);

                let emptyPositions = [];

                for(let i = 0; i<subList.length ;i++){
                  let elementNow = subList[i];
                  if(elementNow.id_PUBLICACION==0){
                    emptyPositions.push(elementNow);
                  }
                }


                if(emptyPositions.length>0){
                  //HAY POSICIONES VACIAS
                  x = emptyPositions[0].mapax;
                  y = emptyPositions[0].mapay;
                  //CAMBIAR PUBLICACION DE 0 A ID NUEVO
                  this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
                }else{
                  //NO HAY POSICIONES VACIAS
                  subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
                  x = subList[0].mapax;
                  y = subList[0].mapay;
                  //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
                  this.idPosicion = subList[0].id_PUBLICACION_POSICION;
                }
              };

              //console.log("ID POSICION: "+this.idPosicion);
              //console.log("URL: "+"https://postalunal.unal.edu.co:8780/v1/campus/updatePublicacionOnPosicion?ID_PUBLICACION="+item.id_PUBLICACION+"&ID_PUBLICACION_POSICION="+this.idPosicion)
                this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/updatePublicacionOnPosicion?ID_PUBLICACION="+item.id_PUBLICACION+"&ID_PUBLICACION_POSICION="+this.idPosicion,{}).subscribe(response => {
                  this.idPosicion = 0;
                  this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/fixMapCoordinates?idPublicacion="+item.id_PUBLICACION+"&mapax="+x+"&mapay="+y,{}).subscribe(response => {
                  if(modal){
                    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3,4").subscribe(
                      response =>{
                        //@ts-ignore
                        this.listaPublicacionesPadre = response
                        this.listaPublicacionesPadreCopy = response
                        this.caseFilter = 0;
                      }
                    )
                  }})
                })
              })
            }
          }
        }
        )

  }


  noPublication(item,modal){

    //console.log()
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/ACTUALIZAR_ESTADO_PUBLICACION?idpublicacion="+item.id_PUBLICACION+"&idestadopublicacion="+3,{}).subscribe(
      response =>{
        if(response == 1){
          if(modal){
            let dialogRef = this.dialog.open(ModalComponent, {
              data: {title:"Publicacion rechazada con exito.", message:" "},
              disableClose: false
            }).afterClosed().subscribe(response=> {
            });
          }

           this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/email?sender="+"somoscampus_bog"+"&recipient="+this.locStorage.getUsername()+"&message=Tu%20publicación%20ha%20sido%20suspendida.%20Tu%20publicación%20no%20cumple%20con%20los%20términos%20y%20condiciones%20de%20Postal%20Unal.%20Puedes%20consultarlos%20en%20%20postalunal.unal.edu.co",{}).subscribe(
                 response =>{
                   if(response == 1){
                     //console.log("EMAIL SENT")
                   }else{
                     //console.log("EMAIL NOT SENT")
                   }
                 }
               )



          //HERE
          if(item.id_PUBLICACION_PADRE!=0){
            if(modal){
            this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3,4").subscribe(
              response =>{
                //@ts-ignore
                this.listaPublicacionesPadre = response
                this.listaPublicacionesPadreCopy = response
                this.caseFilter = 0;
              }
            )}
          }else{
          this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/fixMapCoordinates?idPublicacion="+item.id_PUBLICACION+"&mapax=0&mapay=0",{}).subscribe(response => {
            if(response == 1){
              this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/LIMPIAR_PUBLICACION_POSICION?ID_PUB="+item.id_PUBLICACION,{}).subscribe(element => {
              if(modal){
                this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3,4").subscribe(
                  response =>{
                    //@ts-ignore
                    this.listaPublicacionesPadre = response
                    this.listaPublicacionesPadreCopy = response
                    this.caseFilter = 0;
                  }
                )
              }
              })
            }
          })
        }
      }}
    )


  }




  postName="";
  postDistance="";
  postMessage="";
  listChildPost;
  imgUrl="";


  setModalData(item){

    let object;
    //console.log("OBJECT: "+JSON.stringify(item));

    if(item.id_PUBLICACION_PADRE != 0){

      let itemGot = this.listaPublicacionesPadre.filter(x => x.id_PUBLICACION == item.id_PUBLICACION_PADRE && x.id_PUBLICACION_PADRE == 0)

      object = itemGot[0];

      this.postName=object.username;
      this.postDistance=object.distancia_KMS;
      this.imgUrl=object.img_URL;
      this.postMessage=object.publicacion;

      this.listChildPost = [];
      this.listChildPost.push(item);

    }else{
      object = item;
      this.postName=object.username;
      this.postDistance=object.distancia_KMS;
      this.imgUrl=object.img_URL;
      this.postMessage=object.publicacion;

      this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesHijo?idpubpadre="+object.id_PUBLICACION).subscribe(
        response =>{
          //@ts-ignore
          this.listChildPost = response
        }
      )

    }
  }


  excel(){


    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/getExcelURl",{},{ responseType: 'text'}).subscribe(
      response =>{
        console.log(response);
        window.open("/archivos/"+response);
      }
    )


  }


  setPregunta(){
    console.log(this.idPregunta);
    this.caseFilter = 0;
    if(this.idPregunta==-1){
      this.listaPublicacionesPadre = this.listaPublicacionesPadreCopy;
    }else{
      this.listaPublicacionesPadre = this.listaPublicacionesPadreCopy.filter(element => element.id_PREGUNTA == this.idPregunta );
    }

  }




  zip(){


    let dialogRef = this.dialog.open(LoaderComponent, {
      disableClose: true
    })
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/getZip",{},{ responseType: 'text'}).subscribe(
      response =>{
        console.log("https://postalunal.unal.edu.co/archivos/"+response);
        window.open("https://postalunal.unal.edu.co/archivos/"+response);
        this.dialog.closeAll();
      }
    )
  }



  partialZip(){
    let dialogRef = this.dialog.open(LoaderComponent, {
      disableClose: true
    })

    let partialList = this.listaPublicacionesPadre.filter(element => element.check);
    let fileNameList = "";

    for(let i =0; i<partialList.length;i++){
      let item = partialList[i];
      if(item.id_PUBLICACION_PADRE==0){
        fileNameList = fileNameList + item.img_URL + ",";
      }
    }
    console.log(fileNameList);
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/getZip/list?files="+fileNameList,{},{ responseType: 'text'}).subscribe(
      response =>{
        console.log("https://postalunal.unal.edu.co/archivos/"+response);
        window.open("https://postalunal.unal.edu.co/archivos/"+response);
        this.dialog.closeAll();
      }
    )
  }


}
