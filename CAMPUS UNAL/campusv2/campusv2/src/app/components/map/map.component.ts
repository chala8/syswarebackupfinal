import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/services/LocalStorage';
import { ModalComponent } from '../modal/modal.component';
import 'jquery';
import { Subject } from 'rxjs';
import { StorageService } from 'src/app/services/StorageService/storage.service';
import { EventEmitterService } from 'src/app/services/event-emitter/event-emitter.service';
import { DatePipe } from '@angular/common';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { DomSanitizer } from '@angular/platform-browser';
/// <reference path ="../../node_modules/@types/jquery/index.d.ts"/>
declare var $: any

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  providers: [DatePipe]
})

export class MapComponent implements OnInit {


  faHeart = faHeart;

  constructor(private httpClient: HttpClient,
    private router: Router,
    public dialog: MatDialog,
    public locStorage: LocalStorage,
    private eventEmitterService: EventEmitterService,
    private sanitizer: DomSanitizer) {
     }
    //public dialog: MatDialog,


  @ViewChild('imgItem', {static: false}) InputVar: ElementRef;

  @HostListener("window:message",["$event"])
  SampleFunction($event:MessageEvent) {
    //console.log("part 2");
    if($event.data.resp == "1"){
      this.openPostModal();
    }
    if($event.data.resp == "2"){
      this.setModalInfo($event.data.item);
      $('#Modal3').show();
    }
    if($event.data.resp == "3"){
      $('#ModalEspacios').show();
    }
    if($event.data.resp == "4"){
      $('#ModalNaturaleza').show();
    }
    if($event.data.resp == "5"){
      $('#ModalConocimiento').show();
    }
    if($event.data.resp == "6"){
      $('#ModalSalud').show();
    }
    if($event.data.resp == "7"){
      $('#ModalCultura').show();
    }
    if($event.data.resp == "8"){
      $('#ModalDeportes').show();
    }
 }


  StringPregunta = "";

    sizex = 0;
    sizey = 0;

  latitud = 0.0;
  stringTextoPublicacion = "";

  idPregunta = -1;
  pregunta = "";
  longitud = 0.0;
  distancia = 0.0;
  username = "";
  listaPublicacionesPadre;
  listaPreguntas;
  preguntaElement;
  coordList = [
  // CENTRO ARRIBA
  //{x:"95.5%",y:"50.4%"},
  //  1/4a
  // {x:"95.5%",y:"14.7%"},
  // {x:"81%",y:"15.5%"},
  // {x:"70%",y:"17%"},
  // {x:"63%",y:"18.4%"},
  // {x:"53%",y:"20.4%"},
  // {x:"44%",y:"23.4%"},
  // {x:"37%",y:"26.4%"},
  // {x:"31.5%",y:"29.4%"},
  // {x:"27%",y:"32.4%"},
  // {x:"23.5%",y:"35.4%"},
  // {x:"21%",y:"38.4%"},
  // {x:"19.5%",y:"41.4%"},
  // {x:"18.2%",y:"44.4%"},
  // {x:"17.6%",y:"47.4%"},
  // {x:"17%",y:"50.4%"},
  //  2/4a
  // {x:"110%",y:"15.5%"},
  // {x:"121%",y:"17%"},
  // {x:"132%",y:"18.8%"},
  // {x:"138%",y:"21%"},
  // {x:"147%",y:"23.4%"},
  // {x:"154%",y:"26.4%"},
  // {x:"159.5%",y:"29.4%"},
  // {x:"164%",y:"32.4%"},
  // {x:"167.5%",y:"35.4%"},
  // {x:"170%",y:"38.4%"},
  //  3/4a
  // {x:"95.5%",y:"86.1%"},
  // {x:"81%",y:"85.3%"},
  // {x:"70%",y:"83.8%"},
  // {x:"59.5%",y:"82.4%"},
  // {x:"50%",y:"79.7%"},
  // {x:"44%",y:"77.4%"},
  // {x:"37%",y:"74.4%"},
  // {x:"31.5%",y:"71.4%"},
  // {x:"27%",y:"68.4%"},
  // {x:"23.5%",y:"65.4%"},
  // {x:"21%",y:"62.4%"},
  // {x:"19.5%",y:"59.4%"},
  // {x:"18.2%",y:"56.4%"},
  // {x:"17.6%",y:"53.4%"},
  //  4/4a
  // {x:"110%",y:"85.5%"},
  // {x:"121%",y:"83.8%"},
  // {x:"128%",y:"82.4%"},
  // {x:"138%",y:"80.4%"},
  // {x:"147%",y:"77.4%"},
  // {x:"154%",y:"74.4%"},
  // {x:"159.5%",y:"71.4%"},
  // {x:"164%",y:"68.4%"},
  // {x:"167.5%",y:"65.4%"},
  // {x:"170%",y:"62.4%"},
  // {x:"171.5%",y:"59.4%"},
  // {x:"172.8%",y:"56.4%"},
  // {x:"173.4%",y:"53.4%"},
  //  1/4b
  // {x:"6%",y:"50.4%"},
  // {x:"6.8%",y:"47.4%"},
  // {x:"7.6%",y:"44.4%"},
  // {x:"8.8%",y:"41.4%"},
  // {x:"10.2%",y:"38.4%"},
  // {x:"12%",y:"35.4%"},
  // {x:"14.5%",y:"32.4%"},
  // {x:"18%",y:"29.4%"},
  // {x:"23%",y:"26.4%"},
  // {x:"28.5%",y:"23.4%"},
  // {x:"35%",y:"20.4%"},
  // {x:"42.5%",y:"17.4%"},
  // {x:"51%",y:"15%"},
  // {x:"61%",y:"13.25%"},
  // {x:"71%",y:"12%"},
  // {x:"82%",y:"11%"},
  // {x:"95.5%",y:"10.2%"},
  // 2/4b
  // {x:"185%",y:"50.4%"},
  // {x:"184.2%",y:"47.4%"},
  // {x:"183.4%",y:"44.4%"},
  // {x:"182.2%",y:"41.4%"},
  // {x:"180.8%",y:"38.4%"},
  // {x:"179%",y:"35.4%"},
  // {x:"176.5%",y:"32.4%"},
  // {x:"173%",y:"29.4%"},
  // {x:"168%",y:"26.4%"},
  // {x:"162.5%",y:"23.4%"},
  // {x:"156%",y:"20.4%"},
  // {x:"148.5%",y:"17.4%"},
  // {x:"140%",y:"15%"},
  // {x:"130%",y:"13.25%"},
  // {x:"120%",y:"12%"},
  // {x:"109%",y:"11%"},
  // 3/4b
  // {x:"6.8%",y:"53.4%"},
  // {x:"7.6%",y:"56.4%"},
  // {x:"8.8%",y:"59.4%"},
  // {x:"10.2%",y:"62.4%"},
  // {x:"12%",y:"65.4%"},
  // {x:"14.5%",y:"68.4%"},
  // {x:"18%",y:"71.4%"},
  // {x:"23%",y:"74.4%"},
  // {x:"28.5%",y:"77.4%"},
  // {x:"35%",y:"80.4%"},
  // {x:"42.5%",y:"83.4%"},
  // {x:"51%",y:"85.8%"},
  // {x:"61%",y:"87.55%"},
  // {x:"71%",y:"88.8%"},
  // {x:"82%",y:"89.8%"},
  // {x:"95.5%",y:"90.6%"},
  // // 4/4b
  // {x:"184.2%",y:"53.4%"},
  // {x:"183.4%",y:"56.4%"},
  // {x:"182.2%",y:"59.4%"},
  // {x:"180.8%",y:"62.4%"},
  // {x:"179%",y:"65.4%"},
  // {x:"176.5%",y:"68.4%"},
  // {x:"173%",y:"71.4%"},
  // {x:"168%",y:"74.4%"},
  // {x:"162.5%",y:"77.4%"},
  // {x:"156%",y:"80.4%"},
  // {x:"148.5%",y:"83.4%"},
  // {x:"140%",y:"85.8%"},
  // {x:"130%",y:"87.55%"},
  // {x:"120%",y:"88.8%"},
  // {x:"109%",y:"89.8%"},

]



  user;
  password;

  ingresar(){
    let encodedUser = btoa(this.user);
    let encodedPassword = btoa(this.password);
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/login?username="+encodedUser+"&password="+encodedPassword,{}).subscribe(
      response => {

        //SE AUTENTICO BIEN Y ES ADMIN
        if(response == 1){
          this.locStorage.setUsername(this.user);
          this.locStorage.setLogginState(2);
          //---I GOTTA FIND THIS
          this.open1("Autenticacion Exitosa!","Bienvenido "+this.user+", tu rol es administrador.");

        }


        //SE AUTENTICO BIEN Y NO ES ADMIN
        if(response == 0){
          this.locStorage.setUsername(this.user);
          this.locStorage.setLogginState(1);
          //---I GOTTA FIND THIS
          this.open2("Autenticacion Exitosa!","Bienvenido "+this.user+", tu rol es usuario.");

        }


        //NO AUTENTICO POR ERROR EN BACKEND
        if(response == -1){
          this.locStorage.setLogginState(0);
          this.open("Error Autenticando!","Se presento un error en el servicio de autenticacion.");
        }

        //NO AUTENTICO POR DATOS INCORRECTOS
        if(response == 2){
          this.locStorage.setLogginState(0);
          this.open("Error Autenticando!","Revisa tu usuario y contraseña.");
        }
      })
}
  preguntasToSend = "";


  @ViewChild('testing2', {static: true}) myDiv;





  generateSizeX(){
    return this.sizex+"px";
  }

  generateSizeY(){
    return this.sizey+"px";
  }

  notificaciones;

  islaData;

  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }


  getMails(element){

    return element.split(",")

  }

  ngOnInit() {


    //GENERATE ISLA CONTENT
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/getIslaData",{}).subscribe(response => {
        this.islaData = response;
      }
    )
    setTimeout(() => {
      this.sizex = document.documentElement.scrollWidth;
      this.sizey = document.documentElement.scrollHeight;
      //console.log("X: ",this.sizex)
      //console.log("Y: ",this.sizey)
      if(this.sizex>767){
        setTimeout(() => {
          window.scroll(700, 1200);
        },800);
      }
    },200);

    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/getNotificacionData?id_usuario_propietario="+this.locStorage.getUsername(),{}).subscribe(response => {
      this.notificaciones = response;

      if(this.notificaciones.length>=50){
        this.notificaciones = this.notificaciones.slice(0,50);
      }else{
        this.notificaciones = this.notificaciones;
      }



    })


    //this.myDiv.nativeElement.scrollIntoView({ behavior: "smooth", block: "end" })
    this.locStorage.setmapStatus(1);
    $('.login_sup').addClass('login_sup_cp');
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
      response =>{
        //@ts-ignore
        this.listaPreguntas = response
        //console.log("THIS IS PREGUNTA LIST: "+this.listaPreguntas);
        //**SE SETEA UNA PREGUNTA INICIAL
        this.preguntaElement = response[0];
        this.idPregunta = response[0].id_pregunta;
        this.StringPregunta = response[0].pregunta;
        this.pregunta = response[0].pregunta;
            //@ts-ignore
            this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesPregunta?idList=2&pregList="+response[0].id_pregunta).subscribe(
              response =>{
                //@ts-ignore
                this.listaPublicacionesPadre = response
                //console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
              }
            )

      }
    )
    //**SE LEE EL USERNAME DE LOCALSTORAGE
    let posicion = Math.ceil(Math.random() * 4)-1;
    this.username = this.locStorage.getUsername();


    //**SE CAPTURA LATITUD Y LONGITUD
    //this.latitud = Math.ceil(Math.random() * 90) * (Math.round(Math.random()) ? 1 : -1);
    //this.longitud = Math.ceil(Math.random() * 180) * (Math.round(Math.random()) ? 1 : -1);

    this.getPosition().then(pos=>
       {
          //console.log(`Positon: ${pos.lng} ${pos.lat}`);
          this.latitud = pos.lat;
          this.longitud = pos.lng;
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/calculadistanciagps?lat="+pos.lat+"&longi="+pos.lng).subscribe(
            response =>{
              //console.log("https://postalunal.unal.edu.co:8780/v1/campus/calculadistanciagps?lat="+pos.lat+"&longi="+pos.lng);
              //console.log("REPSONSE: g ",response);
              //@ts-ignore
              this.distancia = Math.round(response * 10)/10;
              //console.log("THIS IS DISTANCIA: "+this.distancia)
            }
          )
       }
    )
    //**SE CALCULA LA DISTANCIA


  }


  getNewNotif(){
    return this.notificaciones.filter( element => element.estado_NOTIFICACION == 0).length;
  }

  openPostModal(){
    //console.log("I CLICKED THE MODAL");
    $('#Modal4b').show();

  }

  scroll(el: HTMLElement) {
    //console.log(el);
    el.scrollIntoView({behavior: 'smooth'});
  }

  gotoTop() {
    //console.log("UP")
    let el = document.getElementById('mark');
    el.scrollTop = el.scrollHeight;
  }

  postStructure = {
    imgUrl: "",
    name: "",
    km: "",
    post: "",
    idPublication: "",
    listChildPost: [],
    username: ""
  }


  mapNoLoggin(){
    this.locStorage.setLogginState(0);

    this.router.navigateByUrl('/map');

  }


  logout(){
    this.locStorage.setUsername("");
    this.locStorage.setLogginState(0);
    this.router.navigateByUrl("/landing");
  }

  setPregunta(item){
    this.idPregunta = item.id_pregunta;
    this.StringPregunta = item.pregunta;
    //console.log("EXECUTING");


    let itemToSend;
    if(this.idPregunta==-1){
      this.pregunta  = "Todas";
      itemToSend = this.preguntasToSend.substring(0,this.preguntasToSend.length-1)
    }else{
      let item = this.listaPreguntas.find(element => element.id_pregunta == this.idPregunta)
      this.pregunta  = item.pregunta;
      itemToSend = "" + this.idPregunta;
    }

    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesPregunta?idList=2&pregList="+itemToSend).subscribe(
      response =>{
        //@ts-ignore
        this.listaPublicacionesPadre = response
        //console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
      })
  }


  fileBase64String = null;
  fileExtension = null;
  urlUploaded = "Sin Archivo";
  imgName = null;
  imgURL = null;
  size = null;
  setFileImage(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {

        this.size = Math.round((event.srcElement.files[0].size/1024/1024) * 100) / 100;

        if(this.size>=5){
          this.open("El tamaño de la imagen es mayor al limite permitido.","");
          this.clearImg();
          return;
        }

        // const img = new Image();
        // img.src = reader.result as string;
        // img.onload = () => {
        //   this.imgHeight = img.naturalHeight;
        //   this.imgWidth = img.naturalWidth;
        //   console.log('Width and Height', this.imgWidth, this.imgHeight);
        //   if(this.imgHeight > 100 || this.imgWidth > 100){
        //     this.open("Las Dimensiones de la imagen son mayores al limite permitido.","");
        //     this.clearImg();
        //     return;
        //   }
        // };
        //console.log(reader.result);
        //console.log("EXTENSION: ", event.target.files[0].name.split(".").pop());
        this.fileBase64String = reader.result;
        this.fileExtension = event.target.files[0].name.split(".").pop();
        this.urlUploaded = event.target.files[0].name;
        //console.log("FILE NAME: ",event.target.files[0].name);
        this.imgName = event.target.files[0].name;
        this.imgURL = reader.result;
        if(this.fileExtension == "jpg" || this.fileExtension == "jpeg" || this.fileExtension == "png"){
            console.log(this.imgName,+" ",+this.imgURL+" "+this.urlUploaded+" "+this.fileExtension);
        }else{
          this.open("Formato de imagen no valido, los formatos permitidos son, JPG, JEPG y PNG.","");
          this.clearImg();
          return;
        }
    };
  }


  clearImg(){
    this.InputVar.nativeElement.value = "";
    this.fileBase64String = null;
    this.fileExtension = null;
    this.urlUploaded = "Sin Archivo";
    this.imgName = null;
    this.imgURL = null;
    this.size = null;
  }

  getx(){
    return document.documentElement.scrollWidth;
  }

  gety(){
    return document.documentElement.scrollHeight;
  }

  clearPostModal(){
    this.clearImg();
    this.stringTextoPublicacion = "";
  }
  getPublicacionesHijas(idPadre){

    //console.log("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesHijo?idpubpadre="+idPadre);
    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesHijo?idpubpadre="+idPadre).subscribe(
      response =>{
        //@ts-ignore
        this.postStructure.listChildPost = response
        //console.log("THIS IS PUBLICATIONS LIST: "+this.postStructure.listChildPost )
      }
    )

  }



  setModalInfo2(notificacion){
    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesById?idPub="+notificacion.id_PUBLICACION).subscribe(
      response =>{
        this.setModalInfo(response);
      }
    )

  }


  likeCount = 0;
  hasLike = false;
  setModalInfo(item){
    console.log(item)
    this.getPublicacionesHijas(item.id_PUBLICACION);

    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getLikes?id_pub="+item.id_PUBLICACION).subscribe(
      response =>{
        //@ts-ignore
        this.likeCount = response
        console.log(response)
        //console.log("THIS IS PUBLICATIONS LIST: "+this.postStructure.listChildPost )
      }
    )

    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/findLikes?id_pub="+item.id_PUBLICACION+"&usern="+this.locStorage.getUsername()).subscribe(
      response =>{
        //@ts-ignore
        if(response>0){
          this.hasLike = true;
        }
        //console.log("THIS IS PUBLICATIONS LIST: "+this.postStructure.listChildPost )
      }
    )

    this.postStructure.imgUrl = item.img_URL;
    this.postStructure.name = item.usuario;
    this.postStructure.km = item.distancia_KMS;
    this.postStructure.post = item.publicacion;
    this.postStructure.idPublication = item.id_PUBLICACION;
    this.postStructure.username = item.username;
    //console.log("I AM ITEM: ",item)

  }

  resetHasLike(){
    this.hasLike = false;
  }

  createLike(){

    if(this.locStorage.getLogginState()==0){

    }else{
      this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/findLikes?id_pub="+this.postStructure.idPublication+"&usern="+this.locStorage.getUsername()).subscribe(
        response =>{
          //@ts-ignore
          if(response>0){
            this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/eliminar_like?id_pub="+this.postStructure.idPublication+"&usern="+this.locStorage.getUsername(),{}).subscribe(
              response =>{
                this.hasLike = !this.hasLike;
                this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getLikes?id_pub="+this.postStructure.idPublication,{}).subscribe(
                response =>{
                  //@ts-ignore
                  this.likeCount = response
                }
              )
            })
          }else{

            this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/crear_like?id_pub="+this.postStructure.idPublication+"&usern="+this.locStorage.getUsername(),{}).subscribe(
              response =>{
                this.hasLike = !this.hasLike;
                //@ts-ignore
                this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/crear_notificaciones?TIPONOT="+0+"&IDPUB="+this.postStructure.idPublication+"&ESTADONOT="+0+"&IDUSERPROP="+this.postStructure.username+"&IDUSERNOT="+this.locStorage.getUsername()+"&PROP="+this.postStructure.name+"&NOTIFIER="+this.locStorage.getusuario(),{}).subscribe(
                  response =>{
                    //@ts-ignore
                  }
                )

                this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getLikes?id_pub="+this.postStructure.idPublication,{}).subscribe(
                response =>{
                  //@ts-ignore
                  this.likeCount = response
                }
              )
              }
            )
          }
          //console.log("THIS IS PUBLICATIONS LIST: "+this.postStructure.listChildPost )
        }
      )
    }
  }

  updateNotificaciones(){
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/update_notificaciones?id_usuario_propietario="+this.username,{}).subscribe(
      response =>{

      }
    )
  }





  stringPublicationModal = "";
  lockPostModalPublicacion = false;
  postModalPublication(){
    if(this.lockPostModalPublicacion){
      return;
    }

    this.lockPostModalPublicacion = true;
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/postPublicacion?IDPREGUNTA="+this.idPregunta+"&IDPUBLICACIONPADRE="+this.postStructure.idPublication+"&FECHAPUBLICACION="+(new Date()).toString()+"&USERNAME="+this.username+"&PUBLICACION="+this.stringPublicationModal+"&IMGURL=%20&IDESTADOPUBLICACION=2&LATITUD="+this.latitud+"&LONGITUD="+this.longitud+"&KMS="+this.distancia+"&USER="+this.locStorage.getusuario(),{}).subscribe(
      response =>{
        if(response > 0){
          this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/crear_notificaciones?TIPONOT="+1+"&IDPUB="+this.postStructure.idPublication+"&ESTADONOT="+0+"&IDUSERPROP="+this.postStructure.username+"&IDUSERNOT="+this.locStorage.getUsername()+"&PROP="+this.postStructure.name+"&NOTIFIER="+this.locStorage.getusuario(),{}).subscribe(
          response =>{
            //@ts-ignore
            console.log(response)
          }
        )
          this.getPublicacionesHijas(this.postStructure.idPublication);
          this.open("Comentario realizado con éxito"," ")
          this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/email?sender="+"somoscampus_bog"+"&recipient="+this.postStructure.name+"&message=Tu%20publicación%20ha%20recibido%20un%20nuevo%20comentario.%20Puedes%20verlo%20en:%20Postalunal.unal.edu.co",{}).subscribe(
                response =>{
                  if(response == 1){
                    //console.log("EMAIL SENT")
                  }else{
                    //console.log("EMAIL NOT SENT")
                  }
                }
              )
          this.stringPublicationModal = "";
        }else{
          this.open("Se presento un error al cargar la publicacion.","Vuelve a Intentarlo en unos minutos o contactate con el administrador.")
        }
        this.lockPostModalPublicacion = false;
      }
    )
  }

  idPosicion = 0;

  generateMapCoordinates2(distancia){

    let x = 0;
    let y = 0;

    let listPositions;

    //HERE
    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicationPosicionIdPregunta?ID_PREGUNTA="+this.idPregunta).subscribe(element => {
      listPositions = element;


    //console.log("POSITION 0: "+listPositions[0]);

    let subList;

     if(distancia <= 4){

      subList = listPositions.slice(0,6);

      let emptyPositions = [];

      for(let i = 0; i<subList.length ;i++){
        let elementNow = subList[i];
        if(elementNow.id_PUBLICACION==0){
          emptyPositions.push(elementNow);
        }
      }


      //console.log("length: "+emptyPositions.length)

      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }


     };



     if(distancia >4 && distancia<=15){

      subList = listPositions.slice(6,15);

      let emptyPositions = [];

      for(let i = 0; i<subList.length ;i++){
        let elementNow = subList[i];
        if(elementNow.id_PUBLICACION==0){
          emptyPositions.push(elementNow);
        }
      }

      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }


     };


      if(distancia >15 && distancia<=100){

        subList = listPositions.slice(15,31);

        let emptyPositions = [];

        for(let i = 0; i<subList.length ;i++){
          let elementNow = subList[i];
          if(elementNow.id_PUBLICACION==0){
            emptyPositions.push(elementNow);
          }
        }


      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }

      };


      if(distancia >100 && distancia<=300){

        subList = listPositions.slice(31,57);

        let emptyPositions = [];

        for(let i = 0; i<subList.length ;i++){
          let elementNow = subList[i];
          if(elementNow.id_PUBLICACION==0){
            emptyPositions.push(elementNow);
          }
        }


      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }


      };


     if(distancia >300 && distancia<=1000){

      subList = listPositions.slice(57,88);

      let emptyPositions = [];

      for(let i = 0; i<subList.length ;i++){
        let elementNow = subList[i];
        if(elementNow.id_PUBLICACION==0){
          emptyPositions.push(elementNow);
        }
      }


      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }

     };



    if(distancia >1000 && distancia<=3000){

      subList = listPositions.slice(88,127);

      let emptyPositions = [];

      for(let i = 0; i<subList.length ;i++){
        let elementNow = subList[i];
        if(elementNow.id_PUBLICACION==0){
          emptyPositions.push(elementNow);
        }
      }


      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }
    };



    if(distancia >3000 && distancia<=5000){

      subList = listPositions.slice(127,181);

      let emptyPositions = [];

      for(let i = 0; i<subList.length ;i++){
        let elementNow = subList[i];
        if(elementNow.id_PUBLICACION==0){
          emptyPositions.push(elementNow);
        }
      }


      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }
    };



    if(distancia >5000){

      subList = listPositions.slice(181,246);

      let emptyPositions = [];

      for(let i = 0; i<subList.length ;i++){
        let elementNow = subList[i];
        if(elementNow.id_PUBLICACION==0){
          emptyPositions.push(elementNow);
        }
      }


      if(emptyPositions.length>0){
        //HAY POSICIONES VACIAS
        x = emptyPositions[0].mapax;
        y = emptyPositions[0].mapay;
        //CAMBIAR PUBLICACION DE 0 A ID NUEVO
        this.idPosicion = emptyPositions[0].id_PUBLICACION_POSICION;
      }else{
        //NO HAY POSICIONES VACIAS
        subList.sort((a,b) => a.id_PUBLICACION - b.id_PUBLICACION);
        x = subList[0].mapax;
        y = subList[0].mapay;
        //CAMBIAR PUBLICACION DE ID VIEJO A ID NUEVO
        this.idPosicion = subList[0].id_PUBLICACION_POSICION;
      }
    };

    })




    return {y : y, x :x}
  }

  generateMapCoordinates(distancia){

    let x = 0;
    let y = 0;


     if(distancia <= 4){
      let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

      let a = 13;
      let b = 6;

      let coefX = a*Math.cos(theta);

      let x = 95.5+a*Math.cos(theta);
      let y = 50.4+b*Math.sin(theta);
      //console.log("distancia: ",distancia);
      //console.log("X: ",x,", Y: ",y);

      if(((x>=95.5 && x<=95.5+29)&&(y<=50.4 && y>=50.4-13.5))||((x<=95.5 && x>=95.5-29)&&(y>=50.4 && y<=50.4-13.5))){
        x = 95.5-coefX;
       }

      return {y : y, x :x}

     };



     if(distancia >4 && distancia<=15){

       let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

       let a = 29;
       let b = 13.5


       let x = 95.5 + a*Math.cos(theta);
       let y = 50.4+b*Math.sin(theta);
       //console.log("distancia: ",distancia);
       //console.log("X: ",x,", Y: ",y);

       if(((x>=95.5 && x<=95.5+a)&&(y<=50.4 && y>=50.4-b))||((x<=95.5 && x>=95.5-a)&&(y>=50.4 && y<=50.4-b))){
        x = 95.5-a*Math.cos(theta);
      }

       return {y : y, x :x}

     };


      if(distancia >15 && distancia<=100){

        let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

        let a = 39;
        let b = 18.5

        let x = 95.5+a*Math.cos(theta);
        let y = 50.4+b*Math.sin(theta);

        //console.log("distancia: ",distancia);
        //console.log("X: ",x,", Y: ",y);


        //imagen 1
        //imagen 2
        if((y>=29 && y<=35)&&(x<=0&&x>=69)){
          x = 95.5-a*Math.cos(theta);
        }

        if((y>=56 && y<=69)&&(x<=139&&x>=110.5)){
          y = 50.4-b*Math.sin(theta);
        }


        return {y : y, x :x}

      };


      if(distancia >100 && distancia<=300){

        let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

        let a = 49;
        let b = 23.1;

        let x = 95.5+a*Math.cos(theta);
        let y = 50.4+b*Math.sin(theta);

        //console.log("distancia: ",distancia);
        //console.log("X: ",x,", Y: ",y);

        //imagen 1
        if((y>=25 && y<=36)&&(x<=96&&x>=58.5)){
          x = 95.5-a*Math.cos(theta);
        }

        //imagen 2
        if((y>=58 && y<=73)&&(x<=147&&x>=118)){
          y = 50.4-b*Math.sin(theta);
        }

        //imagen 3
         if((y>=68 && y<=76)&&(x<=98.5&&x>=58.5)){
          x = 95.5-a*Math.cos(theta);
          y = 50.4-b*Math.sin(theta);
        }


        return {y : y, x :x}

      };


     if(distancia >300 && distancia<=1000){

       let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

       let a = 58;
       let b = 27.6;

       let x = 95.5+a*Math.cos(theta);
       let y = 50.4+b*Math.sin(theta);

      // x = 95.5-70;
      // y = 50.4;

       //console.log("distancia: ",distancia);
       //console.log("X: ",x,", Y: ",y);

      //imagen 1
      if((y>=23 && y<=36)&&(x<=80&&x>=50)){
        x = 95.5-a*Math.cos(theta);
      }

      //imagen 2
      if((y>=60 && y<=73)&&(x<=147&&x>=118)){
        y = 50.4-b*Math.sin(theta);
      }

      //imagen 3
       if((y>=70 && y<=79)&&(x<=98.5&&x>=58.5)){
        x = 95.5-a*Math.cos(theta);
        y = 50.4-b*Math.sin(theta);
      }

      //imagen 4
      if((y>=40.4 && y<=47.9)&&(x<=155&&x>=150)){
        x = 95.5-a*Math.cos(theta);
      }

       return {y : y, x :x}

     };



    if(distancia >1000){

      let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

      let a = 68;
      let b = 31.6;

      let x = 95.5+a*Math.cos(theta);
      let y = 50.4+b*Math.sin(theta);

      //console.log("distancia: ",distancia);
      //console.log("X: ",x,", Y: ",y);

      //imagen 1
      if((y>=35.4 && y<=53)&&(x<=165&&x>=149)){
        x = 95.5-a*Math.cos(theta);
      }

      //imagen 2
      if((y>=73 && y<=83)&&(x<=98.5&&x>=58)){
        x = 95.5-a*Math.cos(theta);
        y = 50.4-b*Math.sin(theta);
      }
      return {y : y, x :x}

    };

  }



  getMapCoordinates(item){
    //console.log({top : item.mapay+'%', left :item.mapax+'%'});
    return  {top : item.mapay+'%', left :item.mapax+'%'}
  }

  async checkImg(){
    if(this.imgURL == null || this.stringTextoPublicacion == null){
      this.open("Debes subir una imagen o realizar un comentario en tu publicación","");
      return false;
    }else{
      return true;
    }
  }

  click = false;


  showCoordinates(){


    //console.log("COORDINATES - Latitud: "+this.latitud+" Longitud: "+this.longitud);


  }

  async postPublicacionInicialT2(element){

    if(this.click){
      return;
    }

    this.click = true;

    this.checkImg().then(r => {
      if(r){

        if(this.latitud == 0 && this.longitud == 0){
          //this.openWarning("Tus coordenadas no pudieron obtenerse, tu publicación ira en el anillo exterior.","");
          this.distancia = 10000;
        }

        let coordenates = this.generateMapCoordinates2(this.distancia)
        let elementList = this.listaPublicacionesPadre.filter(item => coordenates.x == item.mapax && coordenates.y == item.mapay)
        //let cont = 1;
        //let failed = false;


        // if(elementList.length > 0){
        //   console.log("IM IN NO EXT")
        //   this.noPublication(elementList[0])
        // }


          // while(elementList.length > 0 && failed == false){
          //   //console.log(cont)
          //   if(cont > 1000){
          //     failed = true;
          //   }
          //   coordenates = this.generateMapCoordinates(this.distancia);
          //   elementList = this.listaPublicacionesPadre.filter(item => (Math.pow((coordenates.x - item.mapax),2) + Math.pow((coordenates.y - item.mapay),2) < Math.pow(2,2)))
          //   cont++;
          //   }


          // if(failed){
          //   this.postPublicacionInicial3(coordenates,element);
          // }else{
          //   this.postPublicacionInicial2(coordenates,element);
          // }

          this.postPublicacionInicial2T2(coordenates,element);

      }else{
        this.click=false;
        return;
      }
    })

  }

  // async postPublicacionInicial(element){

  //   if(this.click){
  //     return;
  //   }

  //   this.click = true;

  //   this.checkImg().then(r => {
  //     if(r){

  //       if(this.latitud == 0 && this.longitud == 0){
  //         //this.openWarning("Tus coordenadas no pudieron obtenerse, tu publicación ira en el anillo exterior.","");
  //         this.distancia = 10000;
  //       }

  //       let coordenates = this.generateMapCoordinates(this.distancia);
  //       let elementList = this.listaPublicacionesPadre.filter(item => (Math.pow((coordenates.x - item.mapax),2) + Math.pow((coordenates.y - item.mapay),2) < Math.pow(2,2)))
  //       //let cont = 1;
  //       //let failed = false;


  //       if(elementList.length > 0){
  //         this.noPublication(elementList[0])
  //       }


  //       // while(elementList.length > 0 && failed == false){
  //       //   //console.log(cont)
  //       //   if(cont > 1000){
  //       //     failed = true;
  //       //   }
  //       //   coordenates = this.generateMapCoordinates(this.distancia);
  //       //   elementList = this.listaPublicacionesPadre.filter(item => (Math.pow((coordenates.x - item.mapax),2) + Math.pow((coordenates.y - item.mapay),2) < Math.pow(2,2)))
  //       //   cont++;
  //       //   }


  //       // if(failed){
  //       //   this.postPublicacionInicial3(coordenates,element);
  //       // }else{
  //       //   this.postPublicacionInicial2(coordenates,element);
  //       // }

  //       this.postPublicacionInicial2(coordenates,element);

  //     }else{
  //       this.click=false;
  //       return;
  //     }
  //   })

  // }


  noPublication(item){

    //console.log("IM IN NO DEEP")
    //console.log()

    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/ACTUALIZAR_ESTADO_PUBLICACION?idpublicacion="+item.id_PUBLICACION+"&idestadopublicacion="+2,{}).subscribe(
      response =>{
        if(response == 1){
          //HERE
          // this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/fixMapCoordinates?idPublicacion="+item.id_PUBLICACION+"&mapax=0&mapay=0",{}).subscribe(response => {
          //   if(response == 1){
          //     this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/LIMPIAR_PUBLICACION_POSICION?ID_PUB="+item.id_PUBLICACION,{}).subscribe(element => {

          //     })
          //   }
          // })
        }
      }
    )

  }

  open(title,message){
    this.setDialog(title,message);
    $('#Modal40').show();
    $("body").removeClass("modal-open");
  }

  open1(title,message){
    this.setDialog(title,message);
    $('#Modal41').show();
    $("body").removeClass("modal-open");
  }

  open2(title,message){
    this.setDialog(title,message);
    $('#Modal42').show();
    $("body").removeClass("modal-open");
  }

  open3(title,message){
    this.setDialog(title,message);
    $('#Modal43').show();
    $("body").removeClass("modal-open");
  }

  openWarning(title,message){
    this.setDialog(title,message);
    $('#ModalWarning').show();
    $("body").removeClass("modal-open");
  }

  close(){
    $('#Modal40').hide();
    $("body").removeClass("modal-open");
  }

  close1(){
    $('#Modal41').hide();
    $("body").removeClass("modal-open");
    this.router.navigateByUrl('/admin');
  }

  close2(){
    $('#Modal42').hide();
    $("body").removeClass("modal-open");
    this.router.navigateByUrl('/map');
  }

  close3(){
    $('#Modal43').hide();
    $("body").removeClass("modal-open");
    $('#Modal2').hide();
    $("body").removeClass("modal-open");
    this.router.navigateByUrl('/map');
  }

  closeWarning(){
    $('#ModalWarning').hide();
    $("body").removeClass("modal-open");
  }

  closeModal(){
    //console.log("CLOSING MODALS");
    $('#Modal4').hide();
    $("body").removeClass("modal-open");
    this.clearPostModal();
    //console.log("CLOSING MODALS ---")
  }



  closeModalb(){
    //console.log("CLOSING MODALS");
    $('#Modal4b').hide();
    $("body").removeClass("modal-open");
    this.clearPostModal();
    //console.log("CLOSING MODALS ---")
  }


  postOnIframe(){

    let ifr: any = document.getElementById("iframe1");
    ifr.contentWindow.postMessage({respP:"1"},"https://postalunal.unal.edu.co");

  }

  notesientesInput = "";

  postMensajeAyuda(){
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/email?sender="+"somoscampus_bog"+"&recipient="+"AAIntegral"+"&message=El%20usuario%20"+this.locStorage.getUsername()+"%20ha%20enviado%20un%20mensaje%20de%20ayuda. "+this.notesientesInput.split(' ').join('%20'), {} ).subscribe(
      response =>{
        if(response == 1){
          //console.log("EMAIL SENT")
          this.open3("Envio exitoso!","En las próximas 24 horas revisaremos tu mensaje");
        }else{
          //console.log("EMAIL SENT")
          this.open3("Se presento un error!","Se presento un error al enviar tu mensaje.");
        }
      }
    )
  }

  dialogTitle = "";
  dialogMessage = "";

  setDialog(title, message){
   this.dialogTitle = title;
   this.dialogMessage = message;
  }

  dialogQuestion(){
    this.open("Especifica tu pregunta","Debes elegir una pregunta en especifico para poder publicar.");
  }


  async postPublicacionInicial2(responseXY,element){


    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/postPublicacion?IDPREGUNTA="+this.idPregunta+"&IDPUBLICACIONPADRE="+-1+"&FECHAPUBLICACION="+(new Date()).toString()+"&USERNAME="+this.username+"&PUBLICACION="+this.stringTextoPublicacion+"&IMGURL="+this.username+(this.datePipe.transform(new Date(),'yyyy-MM-dd-HH-mm-ss')).toString().split("(").join("_").split(")").join("_").split(" ").join("_")+"."+this.fileExtension+"&IDESTADOPUBLICACION=1&LATITUD="+this.latitud+"&LONGITUD="+this.longitud+"&KMS="+this.distancia+"&X="+responseXY.x+"&Y="+responseXY.y+"&USER="+this.locStorage.getusuario(),{file: this.fileBase64String, fileName: this.username+(this.datePipe.transform(new Date(),'yyyy-MM-dd-HH-mm-ss')).toString().split("(").join("_").split(")").join("_").split(" ").join("_")+"."+this.fileExtension, format: this.fileExtension}).subscribe(
      response =>{
        if(response == 1){
          this.clearPostModal()
          this.click = false;
          if(this.latitud == 0 && this.longitud == 0){
            this.openWarning("Tus coordenadas no pudieron obtenerse, tu publicación ira en el anillo exterior.","");
            this.distancia = 10000;
          }else{
            this.open("¡Publicación exitosa!","");
          }
          let itemToSend;

          if(this.idPregunta==-1){
            itemToSend = this.preguntasToSend.substring(0,this.preguntasToSend.length-1)
          }else{
            itemToSend = "" + this.idPregunta;
          }

          if(element == 1){
            this.postOnIframe();
            this.closeModalb();
          }else{
            this.closeModal();
          }

          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesPregunta?idList=2&pregList="+itemToSend).subscribe(
            response =>{
              //@ts-ignore
              this.listaPublicacionesPadre = response
              //console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
              this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/email?sender="+"somoscampus_bog"+"&recipient="+this.locStorage.getUsername()+"&message=Tu%20publicación%20se%20ha%20realizado%20exitosamente.%20Te%20invitamos%20a%20seguir%20participando%20en:%20postalunal.unal.edu.co",{}).subscribe(
                response =>{
                  if(response == 1){
                    //console.log("EMAIL SENT")
                  }else{
                    //console.log("EMAIL NOT SENT")
                  }
                }
              )

            }
          )
        }else{
          this.click = false;
          this.open("Se presento un error al cargar la publicacion.","Vuelve a Intentarlo en unos minutos o contactate con el administrador.")
        }
      }
    )
  }

   datePipe: DatePipe = new DatePipe("en-US");

  async postPublicacionInicial2T2(responseXY,element){

    //console.log("responseXY: "+responseXY)
    //console.log("URL: "+"https://postalunal.unal.edu.co:8780/v1/campus/postPublicacion?IDPREGUNTA="+this.idPregunta+"&IDPUBLICACIONPADRE="+-1+"&FECHAPUBLICACION="+(new Date()).toString()+"&USERNAME="+this.username+"&PUBLICACION="+this.stringTextoPublicacion+"&IMGURL="+this.urlUploaded.split("(").join("_").split(")").join("_").split(" ").join("_")+"&IDESTADOPUBLICACION=1&LATITUD="+this.latitud+"&LONGITUD="+this.longitud+"&KMS="+this.distancia+"&X="+responseXY.x+"&Y="+responseXY.y)
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/postPublicacion?IDPREGUNTA="+this.idPregunta+"&IDPUBLICACIONPADRE="+-1+"&FECHAPUBLICACION="+(new Date()).toString()+"&USERNAME="+this.username+"&PUBLICACION="+this.stringTextoPublicacion+"&IMGURL="+this.username+(this.datePipe.transform(new Date(),'yyyy-MM-dd-HH-mm-ss')).toString().split("(").join("_").split(")").join("_").split(" ").join("_")+"."+this.fileExtension+"&IDESTADOPUBLICACION=1&LATITUD="+this.latitud+"&LONGITUD="+this.longitud+"&KMS="+this.distancia+"&X="+responseXY.x+"&Y="+responseXY.y+"&USER="+this.locStorage.getusuario(),{file: this.fileBase64String, fileName: this.username+(this.datePipe.transform(new Date(),'yyyy-MM-dd-HH-mm-ss')).toString().split("(").join("_").split(")").join("_").split(" ").join("_")+"."+this.fileExtension, format: this.fileExtension}).subscribe(
      response =>{
        if(response > 0){
          this.clearPostModal()
          this.click = false;
          if(this.latitud == 0 && this.longitud == 0){
            this.openWarning("Tus coordenadas no pudieron obtenerse, tu publicación ira en el anillo exterior.","");
            this.distancia = 10000;
            this.clearImg();
          }else{
            this.open("¡Publicación exitosa!","");
            this.clearImg();
          }
          let itemToSend;

          if(this.idPregunta==-1){
            itemToSend = this.preguntasToSend.substring(0,this.preguntasToSend.length-1)
          }else{
            itemToSend = "" + this.idPregunta;
          }

          if(element == 1){
            this.postOnIframe();
            this.closeModalb();
          }else{
            this.closeModal();
          }



        //HERE
        this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/updatePublicacionOnPosicion?ID_PUBLICACION="+response+"&ID_PUBLICACION_POSICION="+this.idPosicion,{}).subscribe(rta => {
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicationPosicionByPublicacion?id_publicacion="+response).subscribe(response2 => {
            this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/fixMapCoordinates?idPublicacion="+response+"&mapax="+response2[0].mapax+"&mapay="+response2[0].mapay,{}).subscribe(response3 => {
              this.idPosicion = 0;
              this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesPregunta?idList=2&pregList="+itemToSend).subscribe(
              response =>{
              //@ts-ignore
                  this.listaPublicacionesPadre = response
                  //console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
                  this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/email?sender="+"somoscampus_bog"+"&recipient="+this.locStorage.getUsername()+"&message=Tu%20publicación%20se%20ha%20realizado%20exitosamente.%20Te%20invitamos%20a%20seguir%20participando%20en:%20postalunal.unal.edu.co",{}).subscribe(
                    response =>{
                      if(response == 1){
                        //console.log("EMAIL SENT")
                      }else{
                        //console.log("EMAIL NOT SENT")
                      }
                    }
                  )
                }
              )
            })
          })
        })

        }else{
          this.click = false;
          this.open("Se presento un error al cargar la publicacion.","Vuelve a Intentarlo en unos minutos o contactate con el administrador.")
          this.clearImg()
        }
      }
    )
  }


  returnAsNumber(item){
    return Number(item);
  }

  async postPublicacionInicial3(responseXY,element){


    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/postPublicacion?IDPREGUNTA="+this.idPregunta+"&IDPUBLICACIONPADRE="+-1+"&FECHAPUBLICACION="+(new Date()).toString()+"&USERNAME="+this.username+"&PUBLICACION="+this.stringTextoPublicacion+"&IMGURL="+this.urlUploaded.split("(").join("_").split(")").join("_").split(" ").join("_")+"&IDESTADOPUBLICACION=1&LATITUD="+this.latitud+"&LONGITUD="+this.longitud+"&KMS="+this.distancia+"&X="+responseXY.x+"&Y="+responseXY.y+"&USER="+this.locStorage.getusuario(),{file: this.fileBase64String, fileName: this.urlUploaded.split("(").join("_").split(")").join("_").split(" ").join("_"), format: this.fileExtension}).subscribe(
      response =>{
        if(response == 1){
          this.click = false;
          this.open("¡Publicación exitosa!"," ");
          let itemToSend;

          if(this.idPregunta==-1){
            itemToSend = this.preguntasToSend.substring(0,this.preguntasToSend.length-1)
          }else{
            itemToSend = "" + this.idPregunta;
          }

          if(element == 1){
            this.postOnIframe();
            this.closeModalb();
          }else{
            this.closeModal();
          }
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesPregunta?idList=2&pregList="+itemToSend).subscribe(
            response =>{
              //@ts-ignore
              this.listaPublicacionesPadre = response
              //console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
              this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/email?sender="+"somoscampus_bog"+"&recipient="+"somoscampus_bog"+"&message=El%20usuario%20"+this.locStorage.getUsername()+"%20realizo%20una%20nueva%20publicacion.",{}).subscribe(
                response =>{
                  if(response == 1){
                    //console.log("EMAIL SENT")
                  }else{
                    //console.log("EMAIL NOT SENT")
                  }
                }
              )

            }
          )
        }else{
          this.click = false;
          this.open("Se presento un error al cargar la publicacion.","Vuelve a Intentarlo en unos minutos o contactate con el administrador.")
        }
      }
    )
  }



  getMapLocation(latitud,longitud){

    let latitudStep1 =(45+(latitud/10))
    let longitudStep1 =(115+(longitud/10))


    let latitudFinal = latitudStep1;
    let longitudFinal = longitudStep1;

    //console.log("top :", latitudFinal+'%', "left :" ,longitudFinal+'%')
    return {top : latitudFinal+'%', left :longitudFinal+'%'}
  }


  getPosition(): Promise<any>
  {
    return new Promise((resolve, reject) => {

      navigator.geolocation.getCurrentPosition(resp => {

          resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
        },
        err => {
          reject(err);
          //console.log("ERORR: ",err)
        });
    });
  }

}
