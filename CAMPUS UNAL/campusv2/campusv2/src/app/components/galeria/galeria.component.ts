import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/services/LocalStorage';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
/// <reference path ="../../node_modules/@types/jquery/index.d.ts"/>
declare var $: any

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.scss']
})
export class GaleriaComponent implements OnInit {

  constructor(private router: Router,private httpClient: HttpClient,
    private locStorage: LocalStorage) { }
    faHeart = faHeart;
  listaPublicacionesPadre;
  pregunta = "Todas";
  idPregunta = -1;
  listaPadreCopia;
  listaPreguntas;
  ngOnInit() {
    this.locStorage.setmapStatus(0);
    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesGalery?idList=2").subscribe(
      response =>{
        //@ts-ignore
        this.listaPublicacionesPadre = response.sort(() => Math.random() - 0.5)
        //@ts-ignore
        this.listaPadreCopia = response
        console.log(response)
      }
    )
    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
      response =>{
        //@ts-ignore
        this.listaPreguntas = response
      }
    )
  }

  irMapa(){
    this.router.navigateByUrl('/map');
  }

  setPregunta(item){
    console.log(item)
    console.log("IM IM")
    this.idPregunta = item.id_pregunta;
    this.pregunta = item.pregunta;
    if(this.idPregunta==-1){
      this.listaPublicacionesPadre = this.listaPadreCopia.sort(() => Math.random() - 0.5);
    }else{
      this.listaPublicacionesPadre = this.listaPadreCopia.filter(element => element.id_PREGUNTA == this.idPregunta ).sort(() => Math.random() - 0.5);
    }
  }




  createLike(item){

    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/findLikes?id_pub="+item.id_PUBLICACION+"&usern="+this.locStorage.getUsername()).subscribe(
      response =>{
        //@ts-ignore
        if(response>0){
          this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/eliminar_like?id_pub="+item.id_PUBLICACION+"&usern="+this.locStorage.getUsername(),{}).subscribe(
            response =>{
              this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getLikes?id_pub="+item.id_PUBLICACION,{}).subscribe(
              response =>{
                //@ts-ignore
                item.likes = response
              }
            )
          })
        }else{

          this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/crear_like?id_pub="+item.id_PUBLICACION+"&usern="+this.locStorage.getUsername(),{}).subscribe(
            response =>{
              //@ts-ignore
              this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/crear_notificaciones?TIPONOT="+0+"&IDPUB="+item.id_PUBLICACION+"&ESTADONOT="+0+"&IDUSERPROP="+item.username+"&IDUSERNOT="+this.locStorage.getUsername()+"&PROP="+item.usuario+"&NOTIFIER="+this.locStorage.getusuario(),{}).subscribe(
                response =>{
                  //@ts-ignore
                }
              )

              this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getLikes?id_pub="+item.id_PUBLICACION,{}).subscribe(
              response =>{
                //@ts-ignore
                item.likes = response
              }
            )
            }
          )
        }
        //console.log("THIS IS PUBLICATIONS LIST: "+this.postStructure.listChildPost )
      }
    )
  }




}
