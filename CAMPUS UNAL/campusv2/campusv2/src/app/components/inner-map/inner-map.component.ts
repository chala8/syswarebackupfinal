import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, EventEmitter, HostListener, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorage } from 'src/app/services/LocalStorage';
import { ModalComponent } from '../modal/modal.component';
import 'jquery';
import { EventEmitterService } from 'src/app/services/event-emitter/event-emitter.service';
/// <reference path ="../../node_modules/@types/jquery/index.d.ts"/>
declare var $: any

@Component({
  selector: 'app-inner-map',
  templateUrl: './inner-map.component.html',
  styleUrls: ['./inner-map.component.scss']
})
export class InnerMapComponent implements OnInit {
  param1: string;
  constructor(private httpClient: HttpClient,
    private router: Router,
    public dialog: MatDialog,
    public locStorage: LocalStorage,
    private route: ActivatedRoute,
    private eventEmitterService: EventEmitterService) {
    }

@Output() someEvent = new EventEmitter<string>();
sizex = 0;
sizey = 0;

latitud = 0.0;
stringTextoPublicacion = "";

idPregunta = -1;
pregunta = "";
longitud = 0.0;
distancia = 0.0;
username = "";
listaPublicacionesPadre;
listaPreguntas;
preguntaElement;



user;
password;


firstComponentFunction(){
  //console.log("part 1");
  window.parent.postMessage({resp:"1"},"https://postalunal.unal.edu.co");
}


secondComponentFunction(element){
  //console.log("part 1");
  window.parent.postMessage({resp:"2", item: element},"https://postalunal.unal.edu.co");
}



espaciosFunction(){
  //console.log("part 1");
  window.parent.postMessage({resp:"3"},"https://postalunal.unal.edu.co");
}

naturalezaFunction(){
  //console.log("part 1");
  window.parent.postMessage({resp:"4"},"https://postalunal.unal.edu.co");
}

conocimientoFunction(){
  //console.log("part 1");
  window.parent.postMessage({resp:"5"},"https://postalunal.unal.edu.co");
}

saludFunction(){
  //console.log("part 1");
  window.parent.postMessage({resp:"6"},"https://postalunal.unal.edu.co");
}

culturaFunction(){
  //console.log("part 1");
  window.parent.postMessage({resp:"7"},"https://postalunal.unal.edu.co");
}

deportesFunction(){
  //console.log("part 1");
  window.parent.postMessage({resp:"8"},"https://postalunal.unal.edu.co");
}


ingresar(){
  let encodedUser = btoa(this.user);
  let encodedPassword = btoa(this.password);
  this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/login?username="+encodedUser+"&password="+encodedPassword,{}).subscribe(
    response => {

      //SE AUTENTICO BIEN Y ES ADMIN
      if(response == 1){
        this.locStorage.setUsername(this.user);
        this.locStorage.setLogginState(2);
        //---I GOTTA FIND THIS
        this.open1("Autenticacion Exitosa!","Bienvenido "+this.user+", tu rol es administrador.");
        this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/usuario?username="+this.user,{ responseType: 'text'}).subscribe(
            response => {
              this.locStorage.setusuario(response);

            })
      }


      //SE AUTENTICO BIEN Y NO ES ADMIN
      if(response == 0){
        this.locStorage.setUsername(this.user);
        this.locStorage.setLogginState(1);
        //---I GOTTA FIND THIS
        this.open2("Autenticacion Exitosa!","Bienvenido "+this.user+", tu rol es usuario.");
        this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/usuario?username="+this.user,{ responseType: 'text'}).subscribe(
            response => {
              this.locStorage.setusuario(response);

            })
      }


      //NO AUTENTICO POR ERROR EN BACKEND
      if(response == -1){
        this.locStorage.setLogginState(0);
        this.open("Error Autenticando!","Se presento un error en el servicio de autenticacion.");
      }

      //NO AUTENTICO POR DATOS INCORRECTOS
      if(response == 2){
        this.locStorage.setLogginState(0);
        this.open("Error Autenticando!","Revisa tu usuario y contraseña.");
      }
    })
}
preguntasToSend = "";





ngOnInit() {

  //console.log('Called Constructor');
  this.route.queryParams.subscribe(params => {
      this.param1 = params['logged'];
      this.locStorage.setHasInner(false);
  });

  //console.log("IM LOGGED? ",this.locStorage.getLogginState());

  setTimeout(() => window.scroll(200, 900), 0);
  this.locStorage.setmapStatus(1);
  $('.login_sup').addClass('login_sup_cp');
  //**SE CARGA LA LISTA DE PREGUNTAS */
  this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
    response =>{
      //@ts-ignore
      this.listaPreguntas = response
      //console.log("THIS IS PREGUNTA LIST: "+this.listaPreguntas);
      //**SE SETEA UNA PREGUNTA INICIAL
      this.preguntaElement = response[0];
      this.idPregunta = response[0].id_pregunta;
      this.pregunta = response[0].pregunta
          //@ts-ignore
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesPregunta?idList=2&pregList="+response[0].id_pregunta).subscribe(
            response =>{
              //@ts-ignore
              this.listaPublicacionesPadre = response
              //console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
            }
          )

    }
  )
  //**SE LEE EL USERNAME DE LOCALSTORAGE
  let posicion = Math.ceil(Math.random() * 4)-1;
  this.username = this.locStorage.getUsername();


  //**SE CAPTURA LATITUD Y LONGITUD
  //this.latitud = Math.ceil(Math.random() * 90) * (Math.round(Math.random()) ? 1 : -1);
  //this.longitud = Math.ceil(Math.random() * 180) * (Math.round(Math.random()) ? 1 : -1);

  this.getPosition().then(pos=>
     {
        //console.log(`Positon: ${pos.lng} ${pos.lat}`);
        this.latitud = pos.lat;
        this.longitud = pos.lng;
        this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/calculadistanciagps?lat="+pos.lat+"&longi="+pos.lng).subscribe(
          response =>{
            //console.log("https://postalunal.unal.edu.co:8780/v1/campus/calculadistanciagps?lat="+pos.lat+"&longi="+pos.lng);
            //console.log("REPSONSE: g ",response);
            //@ts-ignore
            this.distancia = Math.round(response * 10)/10;
            //console.log("THIS IS DISTANCIA: "+this.distancia)
          }
        )
     }
  )
  //**SE CALCULA LA DISTANCIA


}

scroll(el: HTMLElement) {
  //console.log(el);
  el.scrollIntoView({behavior: 'smooth'});
}

gotoTop() {
  //console.log("UP")
  let el = document.getElementById('mark');
  el.scrollTop = el.scrollHeight;
}

postStructure = {
  imgUrl: "",
  name: "",
  km: "",
  post: "",
  idPublication: "",
  listChildPost: []
}


mapNoLoggin(){
  this.locStorage.setLogginState(0);

  this.router.navigateByUrl('/map');

}


logout(){
  this.locStorage.setUsername("");
  this.locStorage.setLogginState(0);
  this.router.navigateByUrl("/landing");
}

setPregunta(){
  //console.log("EXECUTING");


  let itemToSend;
  if(this.idPregunta==-1){
    this.pregunta  = "Todas";
    itemToSend = this.preguntasToSend.substring(0,this.preguntasToSend.length-1)
  }else{
    let item = this.listaPreguntas.find(element => element.id_pregunta == this.idPregunta)
    this.pregunta  = item.pregunta;
    itemToSend = "" + this.idPregunta;
  }

  this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesPregunta?idList=2&pregList="+itemToSend).subscribe(
    response =>{
      //@ts-ignore
      this.listaPublicacionesPadre = response
      //console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
    })
}


fileBase64String = null;
fileExtension = null;
urlUploaded = "Sin Archivo";
imgName = null;
imgURL = null;
setFileImage(event) {
  const file = event.target.files[0];
  const reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => {
      //console.log(reader.result);
      //console.log("EXTENSION: ", event.target.files[0].name.split(".").pop());
      this.fileBase64String = reader.result;
      this.fileExtension = event.target.files[0].name.split(".").pop();
      this.urlUploaded = event.target.files[0].name;
      //console.log("FILE NAME: ",event.target.files[0].name);
      this.imgName = event.target.files[0].name;
      this.imgURL = reader.result;
  };
}
clearImg(){
  this.fileBase64String = null;
  this.fileExtension = null;
  this.urlUploaded = "Sin Archivo";
  this.imgName = null;
  this.imgURL = null;
}

getPublicacionesHijas(idPadre){

  //console.log("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesHijo?idpubpadre="+idPadre);
  this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesHijo?idpubpadre="+idPadre).subscribe(
    response =>{
      //@ts-ignore
      this.postStructure.listChildPost = response
      //console.log("THIS IS PUBLICATIONS LIST: "+this.postStructure.listChildPost )
    }
  )

}

setModalInfo(item){

  this.getPublicacionesHijas(item.id_PUBLICACION);

  this.postStructure.imgUrl = item.img_URL;
  this.postStructure.name = item.username;
  this.postStructure.km = item.distancia_KMS;
  this.postStructure.post = item.publicacion;
  this.postStructure.idPublication = item.id_PUBLICACION
  //console.log("I AM ITEM: ",item)

}



stringPublicationModal = "";
postModalPublication(){
  this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/postPublicacion?IDPREGUNTA="+this.idPregunta+"&IDPUBLICACIONPADRE="+this.postStructure.idPublication+"&FECHAPUBLICACION="+(new Date()).toString()+"&USERNAME="+this.username+"&PUBLICACION="+this.stringPublicationModal+"&IMGURL=%20&IDESTADOPUBLICACION=2&LATITUD="+this.latitud+"&LONGITUD="+this.longitud+"&KMS="+this.distancia+"&USER="+this.locStorage.getusuario(),{}).subscribe(
    response =>{
      if(response == 1){
        this.getPublicacionesHijas(this.postStructure.idPublication);
        this.open("Comentario realizado con exito","Exito!")

        this.stringPublicationModal = "";
      }else{
        this.open("Se presento un error al cargar la publicacion.","Vuelve a Intentarlo en unos minutos o contactate con el administrador.")
      }
    }
  )
}

getMapCoordinates(item){
  //console.log({top : item.mapay+'%', left :item.mapax+'%'});
  return  {top : item.mapay+'%', left :item.mapax+'%'}
}


generateMapCoordinates(distancia){

  let x = 0;
  let y = 0;


   if(distancia <= 15){
    let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

    let a = 13;
    let b = 6;

    let coefX = a*Math.cos(theta);

    let x = 95.5+a*Math.cos(theta);
    let y = 50.4+b*Math.sin(theta);
    //console.log("distancia: ",distancia);
    //console.log("X: ",x,", Y: ",y);

    if(((x>=95.5 && x<=95.5+29)&&(y<=50.4 && y>=50.4-13.5))||((x<=95.5 && x>=95.5-29)&&(y>=50.4 && y<=50.4-13.5))){
      x = 95.5-coefX;
     }

    return {y : y, x :x}

   };



   if(distancia >15 && distancia<=100){

     let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

     let a = 29;
     let b = 13.5


     let x = 95.5 + a*Math.cos(theta);
     let y = 50.4+b*Math.sin(theta);
     //console.log("distancia: ",distancia);
     //console.log("X: ",x,", Y: ",y);

     if(((x>=95.5 && x<=95.5+a)&&(y<=50.4 && y>=50.4-b))||((x<=95.5 && x>=95.5-a)&&(y>=50.4 && y<=50.4-b))){
      x = 95.5-a*Math.cos(theta);
    }

     return {y : y, x :x}

   };


    if(distancia >100 && distancia<=300){

      let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

      let a = 39;
      let b = 18.5

      let x = 95.5+a*Math.cos(theta);
      let y = 50.4+b*Math.sin(theta);

      //console.log("distancia: ",distancia);
      //console.log("X: ",x,", Y: ",y);


      //imagen 1
      //imagen 2
      if((y>=29 && y<=35)&&(x<=0&&x>=69)){
        x = 95.5-a*Math.cos(theta);
      }

      if((y>=56 && y<=69)&&(x<=139&&x>=110.5)){
        y = 50.4-b*Math.sin(theta);
      }


      return {y : y, x :x}

    };


    if(distancia >300 && distancia<=1000){

      let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

      let a = 49;
      let b = 23.1;

      let x = 95.5+a*Math.cos(theta);
      let y = 50.4+b*Math.sin(theta);

      //console.log("distancia: ",distancia);
      //console.log("X: ",x,", Y: ",y);

      //imagen 1
      if((y>=25 && y<=36)&&(x<=96&&x>=58.5)){
        x = 95.5-a*Math.cos(theta);
      }

      //imagen 2
      if((y>=58 && y<=73)&&(x<=147&&x>=118)){
        y = 50.4-b*Math.sin(theta);
      }

      //imagen 3
       if((y>=68 && y<=76)&&(x<=98.5&&x>=58.5)){
        x = 95.5-a*Math.cos(theta);
        y = 50.4-b*Math.sin(theta);
      }


      return {y : y, x :x}

    };


   if(distancia >1000 && distancia<=1500){

     let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

     let a = 58;
     let b = 27.6;

     let x = 95.5+a*Math.cos(theta);
     let y = 50.4+b*Math.sin(theta);

    // x = 95.5-70;
    // y = 50.4;

     //console.log("distancia: ",distancia);
     //console.log("X: ",x,", Y: ",y);

    //imagen 1
    if((y>=23 && y<=36)&&(x<=80&&x>=50)){
      x = 95.5-a*Math.cos(theta);
    }

    //imagen 2
    if((y>=60 && y<=73)&&(x<=147&&x>=118)){
      y = 50.4-b*Math.sin(theta);
    }

    //imagen 3
     if((y>=70 && y<=79)&&(x<=98.5&&x>=58.5)){
      x = 95.5-a*Math.cos(theta);
      y = 50.4-b*Math.sin(theta);
    }

    //imagen 4
    if((y>=40.4 && y<=47.9)&&(x<=155&&x>=150)){
      x = 95.5-a*Math.cos(theta);
    }

     return {y : y, x :x}

   };



  if(distancia >1500){

    let theta = Math.floor(Math.random() * (2*Math.PI - 0) + 0)

    let a = 68;
    let b = 31.6;

    let x = 95.5+a*Math.cos(theta);
    let y = 50.4+b*Math.sin(theta);

    //console.log("distancia: ",distancia);
    //console.log("X: ",x,", Y: ",y);

    //imagen 1
    if((y>=35.4 && y<=53)&&(x<=165&&x>=149)){
      x = 95.5-a*Math.cos(theta);
    }

    //imagen 2
    if((y>=73 && y<=83)&&(x<=98.5&&x>=58)){
      x = 95.5-a*Math.cos(theta);
      y = 50.4-b*Math.sin(theta);
    }
    return {y : y, x :x}

  };

}



returnAsNumber(item){
  return Number(item);
}




@HostListener("window:message",["$event"])
  SampleFunction($event:MessageEvent) {
    //console.log("part 2");
    if($event.data.respP == "1"){
      let itemToSend;
      if(this.idPregunta==-1){
        itemToSend = this.preguntasToSend.substring(0,this.preguntasToSend.length-1)
      }else{
        itemToSend = "" + this.idPregunta;
      }
      this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesPregunta?idList=2&pregList="+itemToSend).subscribe(
        response =>{
          //@ts-ignore
          this.listaPublicacionesPadre = response
          //console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
        }
      )
    }
 }

open(title,message){
  this.setDialog(title,message);
  $('#Modal40').show();
  $("body").removeClass("modal-open");
}

open1(title,message){
  this.setDialog(title,message);
  $('#Modal41').show();
  $("body").removeClass("modal-open");
}

open2(title,message){
  this.setDialog(title,message);
  $('#Modal42').show();
  $("body").removeClass("modal-open");
}


close(){
  $('#Modal40').hide();
  $("body").removeClass("modal-open");
}

close1(){
  $('#Modal41').hide();
  $("body").removeClass("modal-open");
  this.router.navigateByUrl('/admin');
}

close2(){
  $('#Modal42').hide();
  $("body").removeClass("modal-open");
  this.router.navigateByUrl('/map');
}

closeModal(){
  //console.log("CLOSING MODALS");
  $('#Modal4').hide();
  $("body").removeClass("modal-open");
  //console.log("CLOSING MODALS ---")
}


notesientesInput = "";

postMensajeAyuda(){
  this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/email?sender="+"somoscampus_bog"+"&recipient="+"AAIntegral"+"&message=El%20usuario%20"+this.locStorage.getUsername()+"%20ha%20enviado%20un%20mensaje%20de%20ayuda. "+this.notesientesInput.split(' ').join('%20'), {} ).subscribe(
    response =>{
      if(response == 1){
        //console.log("EMAIL SENT")
        this.open("Envio exitoso!","En las próximas 24 horas revisaremos tu mensaje");
      }else{
        //console.log("EMAIL SENT")
        this.open("Se presento un error!","Se presento un error al enviar tu mensaje.");
      }
    }
  )
}

dialogTitle = "";
dialogMessage = "";

setDialog(title, message){
 this.dialogTitle = title;
 this.dialogMessage = message;
}

dialogQuestion(){
  this.open("Especifica tu pregunta","Debes elegir una pregunta en especifico para poder publicar.");
}


getMapLocation(latitud,longitud){

  let latitudStep1 =(45+(latitud/10))
  let longitudStep1 =(115+(longitud/10))


  let latitudFinal = latitudStep1;
  let longitudFinal = longitudStep1;

  //console.log("top :", latitudFinal+'%', "left :" ,longitudFinal+'%')
  return {top : latitudFinal+'%', left :longitudFinal+'%'}
}


getPosition(): Promise<any>
{
  return new Promise((resolve, reject) => {

    navigator.geolocation.getCurrentPosition(resp => {

        resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
      },
      err => {
        reject(err);
        //console.log("ERORR: ",err)
      });
  });
}

}
