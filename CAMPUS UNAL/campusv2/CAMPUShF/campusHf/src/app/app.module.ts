import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PathLocationStrategy, LocationStrategy } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingComponent } from './components/landing/landing.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MapComponent } from './components/map/map.component';
import { AdministratorComponent } from './components/administrator/administrator.component';
import { GaleriaComponent } from './components/galeria/galeria.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule, MatDialogRef, MatProgressSpinnerModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import * as bootstrap from "bootstrap"; 
import { NgxPaginationModule } from 'ngx-pagination';
import { CanActivateViaAuthGuardService } from './services/CanActivateViaAuthGuard/can-activate-via-auth-guard.service';


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    MapComponent,
    AdministratorComponent,
    GaleriaComponent
  ],
  imports: [
    MatProgressSpinnerModule,
    NgxPaginationModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatSelectModule,
  ],
  providers: [],
  entryComponents: [],
  bootstrap: [AppComponent]
})

export class AppModule { }