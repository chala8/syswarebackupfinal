import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn:'root'
})

export class LocalStorage {
 
  constructor( private http: HttpClient) { }

  private username = "";
  private logginState = 0;
  private mapStatus = 0;

  getUsername(){
    return this.username;
  }

  setUsername(username){
    this.username = username;
  }

  getLogginState(){
      return this.logginState
  }

  setLogginState(state){
      this.logginState = state;
  }


  getmapStatus(){
    return this.mapStatus
  }
  
  setmapStatus(state){
    this.mapStatus = state;
  }


}
