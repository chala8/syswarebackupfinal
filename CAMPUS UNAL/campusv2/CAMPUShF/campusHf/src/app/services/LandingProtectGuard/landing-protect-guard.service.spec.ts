import { TestBed } from '@angular/core/testing';

import { LandingProtectGuardService } from './landing-protect-guard.service';

describe('LandingProtectGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LandingProtectGuardService = TestBed.get(LandingProtectGuardService);
    expect(service).toBeTruthy();
  });
});
