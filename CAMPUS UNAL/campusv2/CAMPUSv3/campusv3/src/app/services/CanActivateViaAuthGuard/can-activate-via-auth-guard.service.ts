import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { LocalStorage } from '../LocalStorage';

@Injectable({
  providedIn: 'root'
})
export class CanActivateViaAuthGuardService implements CanActivate {

  constructor(private router: Router, private locStorage: LocalStorage,) { }


  canActivate(){ 
    console.log ("LOGGIN STATE: "+this.locStorage.getLogginState());
    return this.locStorage.getLogginState() == 2 ? true : this.returnFalse()
  }


  returnFalse(){
    console.log('No estás logueado o no tienes permisos para ver esto');
    this.router.navigate(['/']);
    return false;
  }



}
