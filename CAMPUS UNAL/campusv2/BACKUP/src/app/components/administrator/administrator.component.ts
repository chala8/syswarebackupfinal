import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { LocalStorage } from 'src/app/services/LocalStorage';
import { ModalComponent } from '../modal/modal.component';
/// <reference path ="../../node_modules/@types/jquery/index.d.ts"/>
declare var $: any 


@Component({
  selector: 'app-administrator',
  templateUrl: './administrator.component.html',
  styleUrls: ['./administrator.component.css']
})
export class AdministratorComponent implements OnInit {

  constructor(private httpClient: HttpClient,
    public dialog: MatDialog,
    private router: Router,
    public locStorage: LocalStorage) { }



  listaPreguntas;

  listaPublicacionesPadre;
  listaPublicacionesPadreCopy;

  listaCategorias;
  idCategoria;

  allSelected = false;
  allSelected2 = false;
  allSelected3 = false;
  preguntaNombre="";
  idCategoriaEdit;


  listaUsers;

  ngOnInit() {
    this.locStorage.setmapStatus(0);
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
      response =>{
        //@ts-ignore
        this.listaPreguntas = response;
        console.log("THIS IS PREGUTNAS LIST: "+response)
      }
    )


    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getUsers").subscribe(
      response =>{
        //@ts-ignore
        this.listaUsers = response;
        console.log("THIS IS USER LIST: "+response)
      }
    )


    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getCategorias").subscribe(
      response =>{
        //@ts-ignore
        this.listaCategorias = response;
        this.idCategoriaEdit = response[0].id_categoria;
        console.log("THIS IS PREGUTNAS LIST: "+response)
      }
    )

    this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3").subscribe(
      response =>{
        //@ts-ignore
        this.listaPublicacionesPadre = response
        this.listaPublicacionesPadreCopy = response
        this.caseFilter = 0
        console.log("THIS IS PUBLICATIONS LIST: "+this.listaPublicacionesPadre)
      }
    )

  }



  //Paginator Publicaciones

  page = 1;
  count = 0;
  tableSize = 20;

  onTableDataChange(event){
    this.page = event;
  }  

  getAmountShown(){

    let max = Math.ceil( this.listaPublicacionesPadre.length/20);
    if(this.page == max){
      return this.listaPublicacionesPadre.length%20;
    }else{
      return 20;
    }

  }
  //Paginator publicaciones




  //Paginator preguntas

  page2 = 1;
  count2 = 0;
  tableSize2 = 20;

  onTableDataChange2(event){
    this.page2 = event;
  }  

  getAmountShown2(){

    let max = Math.ceil( this.listaPreguntas.length/20);
    if(this.page2 == max){
      return this.listaPreguntas.length%20;
    }else{
      return 20;
    }

  }
  //Paginator preguntas



   //Paginator preguntas

   page3 = 1;
   count3 = 0;
   tableSize3 = 20;
 
   onTableDataChange3(event){
     this.page3 = event;
   }  
 
   getAmountShown3(){
 
     let max = Math.ceil( this.listaUsers.length/20);
     if(this.page3 == max){
       return this.listaUsers.length%20;
     }else{
       return 20;
     }
 
   }
   //Paginator users


  idPreguntaEdit="";
  setPreguntaEditId(id){
    this.idPreguntaEdit = id;
    console.log(this.idPreguntaEdit);
  }


  actualizarPregunta(){
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/modificarPregunta?pregunta="+this.preguntaNombre+"&id_categoria="+this.idCategoriaEdit+"&id_pregunta="+this.idPreguntaEdit,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Pregunta editada con exito.", message:"Exito!"},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            $("#editEmployeeModal").modal("hide");
          });
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
            response =>{
              //@ts-ignore
              this.listaPreguntas = response
              
            }
          )
        }           
      }
    )
  }


  pregunta = "";
  id_categoria = "";

  postPregunta(){
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/crearPregunta?pregunta="+this.pregunta+"&id_categoria="+this.idCategoria,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Pregunta creada con exito.", message:"Exito!"},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            $("#addTemaModal").modal('hide');
          });
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
            response =>{
              //@ts-ignore
              this.listaPreguntas = response
            }
          )
        }           
      }
    )
  }


  setAll(){
    this.allSelected = !this.allSelected;
    let value = false;
    if(this.allSelected){
      value = true;
    }
    for(let i = 0; i<this.listaPublicacionesPadre.length; i++){
      this.listaPublicacionesPadre[i].check=value
    }
  }


  deletePreguntas(){
    let listSize = this.listaPreguntas.length;
    let listString = ""

    console.log(listSize);

    for(let i = 0;i<listSize;i++){
      if(this.listaPreguntas[i].check == true){
        listString += this.listaPreguntas[i].id_pregunta+",";
      }
    }

    console.log("https://postalunal.unal.edu.co:8780/v1/campus/eliminarPreguntasCheck?list="+listString.substring(0,listString.length-1));

    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/eliminarPreguntasCheck?list="+listString.substring(0,listString.length-1),{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Preguntas eliminadas con exito.", message:"Exito!"},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            $("#deleteEmployeeModal").modal('hide');

            this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaPreguntas = response
                  
                }
              )
          });
        }else{
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Fallo!", message:"Se presento un error al eliminar las preguntas."},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
        }
      })      
  

  }


  deleteUsuarios(){
    let listSize = this.listaUsers.length;
    let listString = ""

    console.log(listSize);

    for(let i = 0;i<listSize;i++){
      if(this.listaUsers[i].check == true){
        listString += this.listaUsers[i].username+",";
        console.log("IM IN: ", listString);
      }
    }

    console.log("https://postalunal.unal.edu.co:8780/v1/campus/eliminarUsersCheck?list="+listString.substring(0,listString.length-1));

    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/eliminarUsersCheck?list="+listString.substring(0,listString.length-1),{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Usuarios eliminados con exito.", message:"Exito!"},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            $("#deleteEmployeeModal2").modal('hide');

            this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getUsers").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaUsers = response
                  
                }
              )
          });
        }else{
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Fallo!", message:"Se presento un error al eliminar los administradores."},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
        }
      })      
  

  }

  

  setAll2(){
    this.allSelected2 = !this.allSelected2;
    let value = false;
    if(this.allSelected2){
      value = true;
    }
    for(let i = 0; i<this.listaPreguntas.length; i++){
      this.listaPreguntas[i].check=value
    }
  }


  setAll3(){
    this.allSelected3 = !this.allSelected3;
    let value = false;
    if(this.allSelected3){
      value = true;
    }
    for(let i = 0; i<this.listaUsers.length; i++){
      this.listaUsers[i].check=value
    }
  }

  goToMap(){
    this.router.navigateByUrl('/map');

  }


  caseFilter = 0;
  filterTable(){
    if(this.caseFilter == 0){
      this.listaPublicacionesPadre = this.listaPublicacionesPadreCopy;
    }
    
    if(this.caseFilter == 1){
      this.listaPublicacionesPadre = this.listaPublicacionesPadreCopy.filter(e => e.id_ESTADO_PUBLICACION == 1);
    }
    
    if(this.caseFilter == 2){
      this.listaPublicacionesPadre = this.listaPublicacionesPadreCopy.filter(e => e.id_ESTADO_PUBLICACION == 2);
    }

    if(this.caseFilter == 3){
      this.listaPublicacionesPadre = this.listaPublicacionesPadreCopy.filter(e => e.id_ESTADO_PUBLICACION == 3);
    }
  }


  categoria = "";

  postCategoria(){
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/crearCategoria?categoria="+this.categoria,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Categoria creada con exito.", message:"Exito!"},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            $("#addCategoria").modal("hide");
          });
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getCategorias").subscribe(
            response =>{
              //@ts-ignore
              this.listaCategorias = response
            }
          )
        }           
      }
    )
  }

  name = "";
  username = "";

  postUser(){
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/crear_administrador?user="+this.username+"&nombre="+this.name,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Administrador registrado con exito.", message:"Exito!"},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            $("#addAdminModal").modal("hide");
          });
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getUsers").subscribe(
            response =>{
              //@ts-ignore
              this.listaUsers = response
            }
          )
        }           
      }
    )
  }






  deletePregunta(id){
        //**SE CARGA LA LISTA DE PREGUNTAS */
        this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/eliminarPregunta?id_pregunta="+id,{}).subscribe(
          response =>{
            if(response == 1){
              let dialogRef = this.dialog.open(ModalComponent, {
                data: {title:"Pregunta Eliminada con exito.", message:"Exito!"},
                disableClose: false
              }).afterClosed().subscribe(response=> {
              });
              this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPregunta").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaPreguntas = response
                }
              )
            }           
          }
        )
  }


  
  deleteUser(id){
    //**SE CARGA LA LISTA DE PREGUNTAS */
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/eliminar_administrador?user="+id,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Administrador Eliminado con exito.", message:"Exito!"},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getUsers").subscribe(
            response =>{
              //@ts-ignore
              this.listaUsers = response
            }
          )
        }           
      }
    )
}



  acceptAll(){
    let listSize = this.listaPublicacionesPadre.length;
    let listString = ""

    console.log(listSize);

    for(let i = 0;i<listSize;i++){
      if(this.listaPublicacionesPadre[i].check == true){
        listString += this.listaPublicacionesPadre[i].id_PUBLICACION+",";
      }
    }

    console.log("https://postalunal.unal.edu.co:8780/v1/campus/changeStatePublicaciones?list="+listString.substring(0,listSize-1)+"&state="+2);

    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/changeStatePublicaciones?list="+listString.substring(0,listSize-1)+"&state="+2,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Publicaciones aceptadas con exito.", message:"Exito!"},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaPublicacionesPadre = response
                  
                  this.listaPublicacionesPadreCopy = response
                  this.caseFilter = 0;
                }
              )
          });
        }else{
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Fallo!", message:"Se presento un error al aceptar las publicaciones."},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
        }
      })      
  }


  
  rejectAll(){
    let listSize = this.listaPublicacionesPadre.length;
    let listString = ""

    console.log(listSize);

    for(let i = 0;i<listSize;i++){
      if(this.listaPublicacionesPadre[i].check == true){
        listString += this.listaPublicacionesPadre[i].id_PUBLICACION+",";
      }
    }

    console.log("https://postalunal.unal.edu.co:8780/v1/campus/changeStatePublicaciones?list="+listString.substring(0,listString.length-1)+"&state="+3);

    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/changeStatePublicaciones?list="+listString.substring(0,listString.length-1)+"&state="+3,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Publicaciones Rechazadas con exito.", message:"Exito!"},
            disableClose: false
          }).afterClosed().subscribe(response=> {
            this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaPublicacionesPadre = response
                  this.listaPublicacionesPadreCopy = response
                  this.caseFilter = 0;
                }
              )
          });
        }else{
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Fallo!", message:"Se presento un error al rechazar las publicaciones."},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
        }
      })      
  }

  aceptPublication(item){

    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/ACTUALIZAR_ESTADO_PUBLICACION?idpublicacion="+item.id_PUBLICACION+"&idestadopublicacion="+2,{}).subscribe(
          response =>{
            if(response == 1){
              let dialogRef = this.dialog.open(ModalComponent, {
                data: {title:"Publicacion aprobada con exito.", message:"Exito!"},
                disableClose: false
              }).afterClosed().subscribe(response=> {
              });
              this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/email?sender="+"somoscampus_bog"+"&recipient="+this.locStorage.getUsername()+"&message=se%20acepto%20el%20registro&20de%20tu%20publicacion%20nueva",{}).subscribe(
                response =>{
                  if(response == 1){
                    console.log("EMAIL SENT")
                  }else{
                    console.log("EMAIL NOT SENT")
                  }
                }
              )
              
              this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3").subscribe(
                response =>{
                  //@ts-ignore
                  this.listaPublicacionesPadre = response
                  this.listaPublicacionesPadreCopy = response
                  this.caseFilter = 0;
                }
              )
            }           
          }
        )

  }


  noPublication(item){

    console.log()
    this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/ACTUALIZAR_ESTADO_PUBLICACION?idpublicacion="+item.id_PUBLICACION+"&idestadopublicacion="+3,{}).subscribe(
      response =>{
        if(response == 1){
          let dialogRef = this.dialog.open(ModalComponent, {
            data: {title:"Publicacion rechazada con exito.", message:"Exito!"},
            disableClose: false
          }).afterClosed().subscribe(response=> {
          });
          this.httpClient.post("https://postalunal.unal.edu.co:8780/v1/campus/email?sender="+"somoscampus_bog"+"&recipient="+this.locStorage.getUsername()+"&message=se%20rechazo%20el%20registro&20de%20tu%20publicacion%20nueva",{}).subscribe(
                response =>{
                  if(response == 1){
                    console.log("EMAIL SENT")
                  }else{
                    console.log("EMAIL NOT SENT")
                  }
                }
              )
          
          this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicaciones?idList=1,2,3").subscribe(
            response =>{
              //@ts-ignore
              this.listaPublicacionesPadre = response
              this.listaPublicacionesPadreCopy = response
              this.caseFilter = 0;
            }
          )
        }           
      }
    )


  }




  postName="";
  postDistance="";
  postMessage="";
  listChildPost;
  imgUrl="";


  setModalData(item){

    let object;
    console.log("OBJECT: "+JSON.stringify(item));

    if(item.id_PUBLICACION_PADRE != 0){

      let itemGot = this.listaPublicacionesPadre.filter(x => x.id_PUBLICACION == item.id_PUBLICACION_PADRE && x.id_PUBLICACION_PADRE == 0)
      
      object = itemGot[0];
      
      this.postName=object.username;
      this.postDistance=object.distancia_KMS;
      this.imgUrl=object.img_URL;
      this.postMessage=object.publicacion;
  
      this.listChildPost = [];
      this.listChildPost.push(item);
  
    }else{
      object = item;
      this.postName=object.username;
      this.postDistance=object.distancia_KMS;
      this.imgUrl=object.img_URL;
      this.postMessage=object.publicacion; 
  
      this.httpClient.get("https://postalunal.unal.edu.co:8780/v1/campus/getPublicacionesHijo?idpubpadre="+object.id_PUBLICACION).subscribe(
        response =>{
          //@ts-ignore
          this.listChildPost = response
        }
      )  

    }
  }

  
}
