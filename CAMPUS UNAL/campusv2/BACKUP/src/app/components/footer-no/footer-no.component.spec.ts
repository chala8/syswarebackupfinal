import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterNoComponent } from './footer-no.component';

describe('FooterNoComponent', () => {
  let component: FooterNoComponent;
  let fixture: ComponentFixture<FooterNoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterNoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterNoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
